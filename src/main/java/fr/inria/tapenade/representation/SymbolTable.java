/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.analysis.InOutAnalyzer;
import fr.inria.tapenade.analysis.PointerAnalyzer;

/**
 * Symbol Table. Scoping is implemented through layered Symbol Tables.
 * Each Symbol Table, except the root, has access to its enclosing Symbol Table.
 */
public final class SymbolTable {

    /**
     * Number of buckets.
     */
    private static final int NUM_BUCKETS = 100;

    /**
     * A unique number used mostly to identify this SymbolTable in debug and trace.
     * Positive number, default initialized to -1.
     */
    private int rank = -1;

    /**
     * @return A unique number used mostly to identify this SymbolTable in debug and trace.
     * Positive number, default initialized to -1.
     */
    public int rank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * The language of this SymbolTable. May be -1 for the root SymbolTable.
     */
    private int language = TapEnv.UNDEFINED;

    /**
     * The language of this SymbolTable. May be -1 for the root SymbolTable.
     */
    public int language() {
        return language;
    }

    /**
     * Set the language of this SymbolTable. May be -1 for the root SymbolTable.
     */
    public void setLanguage(int language) {
        this.language = language;
    }

    /**
     * The Fortran "implicit" typing information at this level.
     */
    public WrapperTypeSpec[] implicits;
    /**
     * List of save Tree's.
     */
    public TapList<Tree> saveList;
    /**
     * List of nameList Tree's.
     */
    public TapList<Tree> nameListList;
    /**
     * The enclosing Unit. Never null with translationUnit.
     */
    public Unit unit;
    /**
     * The name of the (past or future) container file. This is temporary until TranslationUnits become Units.
     */
    public String containerFileName;
    /**
     * Contains the Units for which we need to add a forward declaration (private? case)
     * This field is (non-empty/used) ONLY for a "TranslationUnit" SymbolTable
     * This field will probably move into the TranslationUnit Unit when we implement one.
     */
    public TapList<Unit> privateForwardDeclarations;
    /**
     * Contains the Units for which we need to add a forward declaration (non-private? case)
     * This field is (non-empty/used) ONLY for a "TranslationUnit" SymbolTable
     * This field will probably move into the TranslationUnit Unit when we implement one.
     */
    public TapList<Unit> nonPrivForwardDeclarations;
    /**
     * List of all types used in this SymbolTable that have not been finished yet
     * because they are circular. Must be solved when all types have been prepared.
     */
    public TapList<WrapperTypeSpec> danglingCircularTypes = null;
    /**
     * When this is the copy of the Symbol Table of an IMPORT'd class, contains this IMPORT'd class.
     */
    public Unit inheritedClass;
    /**
     * List of all NewSymbolHolder's that are waiting for declaration in this SymbolTable.
     */
    public TapList<NewSymbolHolder> newSymbolHolders = new TapList<>(null, null);
    /**
     * Ordered list of the NewSymbolHolder's used in this SymbolTable for
     * the indexes of nested do loops.
     */
    public TapList<NewSymbolHolder> nestedIntIndexSymbolHolders;
    public TapList<TypeSpec> associationByAddressTypes;
    /**
     * Block that contains the Instructions corresponding to these declarations.
     */
    public Block declarationsBlock;
    /**
     * When this is the SymbolTable of a nameSpace, holds the nameSpace name.
     */
    public String nameSpaceName;
    /**
     * List of data Tree's.
     */
    protected TapList<Tree> dataList;
    /**
     * List of SymbolDecl, to declare after importAllSymbolTable.
     */
    private TapList<SymbolDecl> addSymbolDecls;
    /**
     * True when this SymbolTable is case dependent.
     */
    private boolean caseDependent;
    /**
     * The array of the Buckets of the hash-table mechanism.
     */
    @SuppressWarnings("unchecked")
    private final TapList<SymbolDecl>[] buckets = new TapList[NUM_BUCKETS];
    /**
     * A short name to designate this SymbolTable.
     */
    private String shortName = "";
    /**
     * The enclosing level SymbolTable.
     */
    private SymbolTable basisSymbolTable;
    /**
     * When this is the root SymbolTable, contains the callGraph.
     */
    private CallGraph rootCallGraph;

    private boolean isTranslationUnit = false ;

    private boolean isGlobal = false ;
    /**
     * For any KIND, rank (plus one) of the last zone declared in this Symbol Table.
     */
    private int[] freeDeclaredZone = {0, 0, 0, 0};
    public int[] freeDeclaredZone4() {return freeDeclaredZone;}
    /**
     * For any KIND, rank of the first zone declared in this Symbol Table.
     */
    private int[] firstDeclaredZone = {0, 0, 0, 0};
    public int[] firstDeclaredZone4() {return firstDeclaredZone;}
    /**
     * Array of ZoneInfo's for each zone declared at this level.
     */
    private ZoneInfo[][] declaredZoneInfos = {null, null, null, null};
    /**
     * Temporary lists of ZoneInfo waiting to be included into this "importsSymbolTable"
     * or (in the C case) into this translation unit SymbolTable.
     */
    private TapList<ZoneInfo> waitingZoneInfos = null ;
    /**
     * Mixed language zones for bind(C), common, global variables: pas utilise'.
     */
    @SuppressWarnings("unchecked")
    private TapList<Int2ZoneInfo>[] additionalDuplicatedDeclaredZoneInfos = new TapList[4];
    /**
     * true to indicate the SymbolTable of formal parameters.
     */
    private boolean isFormalParams = false ;
    /**
     * true when this SymbolTable (basis of a public SymbolTable) is for symbols IMPORTed or for global variables
     */
    public boolean isImports = false ;
    /**
     * true when this SymbolTable is the public SymbolTable of some Unit
     * (contains public variables or formal parameters)
     */
    public boolean isPublic = false ;
    /**
     * Useful to remember the assigned var of an op_allocate.
     */
    private Tree assignDestination;
    /**
     * Useful during typeCheck() to remember that we are inside the definition
     * of the fields of a composite type.
     */
    private CompositeTypeSpec inCompositeTypeSpec;
    /**
     * Special for differentiation: the vector of the zones for which
     * the declarations seen here must provide a differentiated variable.
     */
    private TapList<TapPair<ActivityPattern, BoolVector>> requiredDiffVarsList;

    /**
     * @return the number of zones of kind "zoneKind" at the scope of this SymbolTable.
     */
    public final int zonesNb(int zoneKind) {
        return declaredZonesNb(zoneKind);
    }

    /** For lack of a better place, zones that need no multithread atomic */
    public BoolVector needNoAtomic = null ;

    /**
     * Creates a new SymbolTable as a layer under
     * the existing SymbolTable "basisSymbolTable".
     * If "basisSymbolTable" is null, creates a root SymbolTable.
     */
    public SymbolTable(SymbolTable basisSymbolTable) {
        super();
        this.basisSymbolTable = basisSymbolTable;
        if (basisSymbolTable != null) {
            this.language = basisSymbolTable.language;
            this.caseDependent = basisSymbolTable.caseDependent;
            TapEnv.setRelatedLanguage(basisSymbolTable);
        }
        for (int i = 0; i < NUM_BUCKETS; i++) {
            buckets[i] = new TapList<>(null, null);
        }
        // To be done only for FORTRAN:
        this.implicits = new WrapperTypeSpec[26];
        if (basisSymbolTable != null && basisSymbolTable.implicits != null) {
            System.arraycopy(basisSymbolTable.implicits, 0, implicits, 0, 26);
        }
    }

    /**
     * @return the index of the bucket for entry "key".
     * TODO: too many repetitive calls to hashKey() could be factored !
     */
    private static int hashKey(String key) {
        int code = key.hashCode() % NUM_BUCKETS;
        if (code < 0) {
            code += NUM_BUCKETS;
        }
        return code;
    }

    private static boolean checkScalarBoolean(WrapperTypeSpec type) {
        return type == null
                || !TypeSpec.isA(type, SymbolTableConstants.ARRAYTYPE) && type.checkBoolean();
    }

    private static boolean checkScalarArrayBoolean(WrapperTypeSpec type) {
        return type == null
                || !TypeSpec.isA(type, SymbolTableConstants.ARRAYTYPE) && type.checkBoolean()
                || TypeSpec.isA(type, SymbolTableConstants.ARRAYTYPE) &&
                type.wrappedType.elementType().checkBoolean();
    }

    private static boolean checkScalarNumericInt(WrapperTypeSpec type) {
        return type == null ||
                !TypeSpec.isA(type, SymbolTableConstants.ARRAYTYPE) && type.checkNumericInt();
    }

    /**
     * @return an IL expression which is a reference to the place with
     * the given "offset" into the memory object returned by
     * the given expression "refToStart" ("refToStart is of type "type").
     */
    public static Tree offsetRef(Tree refToStart, WrapperTypeSpec type, int offset) {
        if (type == null || type.wrappedType == null) {
            return refToStart;
        } else {
            TypeSpec actualType = type.wrappedType;
            if (TypeSpec.isA(actualType, SymbolTableConstants.ARRAYTYPE)) {
                // In IL, indices are ordered in the "natural" order, i.e.
                // the slowest-changing index first and the fastest-changing last.
                ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) actualType;
                ArrayDim[] dimensions = arrayTypeSpec.dimensions();
                int elementSize = arrayTypeSpec.elementType().size();
                int indexInArray = offset / elementSize;
                int[] aboveSizes = new int[dimensions.length];
                int dimSize;
                int dimLower;
                aboveSizes[dimensions.length - 1] = 1;
                for (int i = dimensions.length - 2; i >= 0; --i) {
                    if (dimensions[i + 1] == null) {
                        return refToStart;
                    }
                    dimSize = dimensions[i + 1].size();
                    if (dimSize <= 0) {
                        aboveSizes[i] = 1;
                    } else {
                        aboveSizes[i] = aboveSizes[i + 1] * dimSize;
                    }
                }
                Tree[] indexes = new Tree[dimensions.length];
                for (int i = 0; i < dimensions.length; ++i) {
                    if (dimensions[i].lower == null) {
                        dimLower = 1;
                    } else {
                        dimLower = dimensions[i].lower;
                    }
                    indexes[i] = ILUtils.build(ILLang.op_intCst,
                            indexInArray / aboveSizes[i] + dimLower);
                    indexInArray = indexInArray % aboveSizes[i];
                }
                Tree newTree = ILUtils.build(ILLang.op_arrayAccess,
                        refToStart,
                        ILUtils.build(ILLang.op_expressions, indexes));
                return offsetRef(newTree, arrayTypeSpec.elementType(),
                        offset % elementSize);
            } else if (TypeSpec.isA(actualType, SymbolTableConstants.MODIFIEDTYPE)) {
                return offsetRef(refToStart,
                        actualType.elementType(), offset);
            } else if (TypeSpec.isA(actualType, SymbolTableConstants.COMPOSITETYPE)) {
                CompositeTypeSpec recordTypeSpec = (CompositeTypeSpec) actualType;
                int i = 0;
                String fieldName = null;
                while (i < recordTypeSpec.fields.length &&
                        fieldName == null) {
                    if (offset < recordTypeSpec.fields[i].type().size()) {
                        fieldName = recordTypeSpec.fields[i].symbol;
                    } else {
                        offset -= recordTypeSpec.fields[i].type().size();
                    }
                }
                if (fieldName == null) {
                    TapEnv.toolWarning(-1, "(Rebuild offset expression) Offset exceeds record size");
                    fieldName = "??";
                    i = 0;
                }
                return offsetRef(
                        ILUtils.build(ILLang.op_fieldAccess, refToStart,
                                ILUtils.build(ILLang.op_ident, fieldName)),
                        recordTypeSpec.fields[i].type(), offset);
            } else if (TypeSpec.isA(actualType, SymbolTableConstants.PRIMITIVETYPE) ||
                    TypeSpec.isA(actualType, SymbolTableConstants.POINTERTYPE)) {
                return refToStart;
            } else {
                TapEnv.toolWarning(-1, "(Rebuild offset expression) Such a type has no offset: " + actualType);
                return refToStart;
            }
        }
    }

    /**
     * @return whether this SymbolTable is case dependent (e.g. c, java, ...)
     * instead of case independent (fortran77, 90, 95).
     */
    public boolean isCaseDependent() {
        return caseDependent;
    }

    /**
     * Sets the property whether this SymbolTable is case dependent.
     */
    public void setCaseDependent(boolean caseDependent) {
        this.caseDependent = caseDependent;
    }

    /**
     * When this SymbolTable's enclosing Unit results from differentiation
     * of an original Unit.
     *
     * @return this original Unit.
     */
    public Unit origUnit() {
        return unit == null ? null : unit.origUnit;
    }

    public String shortName() {
        String langName = (language != TapEnv.UNDEFINED
                ? TapEnv.languageName(language)
                : (unit != null
                ? TapEnv.languageName(unit.language())
                : "UNKNOWN"));
        return shortName + " (" + langName + ")";
    }

    /**
     * Set a short name to designate this SymbolTable.
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the enclosing level SymbolTable.
     */
    public SymbolTable basisSymbolTable() {
        return basisSymbolTable;
    }

    /**
     * Sets the enclosing level SymbolTable of this SymbolTable.
     */
    public void setBasisSymbolTable(SymbolTable basis) {
        this.basisSymbolTable = basis;
    }

    public void setRootCallGraph(CallGraph cg) {
        rootCallGraph = cg;
    }

    public CallGraph getCallGraph() {
        if (unit != null) {
            return unit.callGraph();
        } else {
            SymbolTable rootST = this;
            while (rootST.basisSymbolTable != null) {
                rootST = rootST.basisSymbolTable;
            }
            return rootST.rootCallGraph;
        }
    }

    public void setIsTranslationUnit() {
        isTranslationUnit = true;
    }

    public void setIsGlobal() {
        isGlobal = true;
    }

    public boolean isTranslationUnitSymbolTable() {
        return isTranslationUnit;
    }

    public boolean isGlobalSymbolTable() {
        return isGlobal;
    }

    public void setFreeDeclaredZone(int freeDZ, int kind) {
        freeDeclaredZone[kind] = freeDZ;
    }

    public ZoneInfo[] getDeclaredZoneInfos(int kind) {
        return declaredZoneInfos[kind];
    }

    public boolean hasNoDeclaredZoneInfos(int kind) {
        return declaredZoneInfos[kind] == null
                && additionalDuplicatedDeclaredZoneInfos[kind] == null;
    }

    public void setDeclaredZoneInfos(ZoneInfo[] zoneInfos, int kind) {
        declaredZoneInfos[kind] = zoneInfos;
    }

    public TapList<Int2ZoneInfo>[] getAdditionalDuplicatedDeclaredZoneInfos() {
        return additionalDuplicatedDeclaredZoneInfos;
    }

    public void setAdditionalDuplicatedDeclaredZoneInfos(TapList<Int2ZoneInfo> additionalZoneInfos, int kind) {
        additionalDuplicatedDeclaredZoneInfos[kind] = additionalZoneInfos;
    }

    public ZoneInfo localDeclaredZoneInfo(int zoneNumber, int whichKind) {
        ZoneInfo result = null;
        if (this.firstDeclaredZone[whichKind] <= zoneNumber && zoneNumber < this.freeDeclaredZone[whichKind]) {
            result = this.declaredZoneInfos[whichKind]
                    [zoneNumber - this.firstDeclaredZone[whichKind]];
        } else if (additionalDuplicatedDeclaredZoneInfos != null) {
            result = TapList.cassq(zoneNumber, additionalDuplicatedDeclaredZoneInfos[whichKind]);
        }
        return result;
    }

    public void addWaitingZoneInfo(ZoneInfo proxyZoneInfo) {
        waitingZoneInfos = new TapList<>(proxyZoneInfo, waitingZoneInfos) ;
    }

    public ZoneInfo retrieveWaitingPtrZoneInfo(int ptrZoneNb) {
        TapList<ZoneInfo> zoneInfos = waitingZoneInfos ;
        ZoneInfo found = null ;
        while (zoneInfos!=null && found==null) {
            if (zoneInfos.head.ptrZoneNb==ptrZoneNb) found = zoneInfos.head ;
            zoneInfos = zoneInfos.tail ;
        }
        return found ;
    }

    public void addPrimitiveTypes() {
        addSystemSymbolDecl(
                new TypeDecl("void", new WrapperTypeSpec(new VoidTypeSpec())));
        PrimitiveTypeSpec integerTypeSpec = new PrimitiveTypeSpec("integer");
        integerTypeSpec.setPrimitiveTypeSize(TapEnv.get().integerSize);
        addSystemSymbolDecl(
                new TypeDecl("integer", new WrapperTypeSpec(integerTypeSpec)));
        PrimitiveTypeSpec floatTypeSpec = new PrimitiveTypeSpec("float");
        floatTypeSpec.setPrimitiveTypeSize(TapEnv.get().realSize);
        addSystemSymbolDecl(
                new TypeDecl("float", new WrapperTypeSpec(floatTypeSpec)));
        addSystemSymbolDecl(
                new TypeDecl("complex",
                        new WrapperTypeSpec(new PrimitiveTypeSpec("complex"))));
        addSystemSymbolDecl(
                new TypeDecl("boolean",
                        new WrapperTypeSpec(new PrimitiveTypeSpec("boolean"))));
        addSystemSymbolDecl(
                new TypeDecl("character",
                        new WrapperTypeSpec(new PrimitiveTypeSpec("character"))));
    }

    public void addSystemSymbolDecl(SymbolDecl symbolDecl) {
        symbolDecl.setSystemPredefined();
        symbolDecl.setNoneInstruction(); // utile?
        this.addSymbolDecl(symbolDecl);
    }

    public BoolVector getRequiredDiffVars(ActivityPattern pattern) {
        return TapList.cassq(pattern, requiredDiffVarsList);
//         if (requiredDiffVarsList == null) {
//             return null;
//         }
//         TapList<TapPair<ActivityPattern, BoolVector>> itRequiredDiffVar = requiredDiffVarsList;
//         while (itRequiredDiffVar != null) {
//             if (itRequiredDiffVar.head.first == pattern) {
//                 return itRequiredDiffVar.head.second;
//             }
//             itRequiredDiffVar = itRequiredDiffVar.tail;
//         }
//         return null;
    }

    public void setOrCumulRequiredDiffVars(ActivityPattern pattern, BoolVector diffVars) {
        BoolVector tempDiffVars = getRequiredDiffVars(pattern);
        if (tempDiffVars == null) {
            requiredDiffVarsList = new TapList<>
                    (new TapPair<>(pattern, diffVars), requiredDiffVarsList);
        } else {
            tempDiffVars.cumulOr(diffVars);
        }
    }

    /**
     * @return the enclosing level SymbolTable
     * skips the imported symbolTables between this and result.
     */
    public SymbolTable basisNotImportedSymbolTable() {
        SymbolTable upperST = basisSymbolTable;
        while (upperST!=null && upperST.isImports) {
            upperST = upperST.basisSymbolTable;
        }
        return upperST;
    }

    /**
     * @return the rank of the last zone
     * of kind "whichKind" (ALLKIND, REALKIND or INTKIND) declared here, plus one.
     * This is also equal to the number of zones declared here and in
     * enclosing SymbolTable's.
     */
    public int freeDeclaredZone(int whichKind) {
        return freeDeclaredZone[whichKind];
    }

    /**
     * @return the number of declared zones of kind "whichKind",
     * visible from this SymbolTable level.
     */
    public int declaredZonesNb(int whichKind) {
        return freeDeclaredZone[whichKind];
    }

    /**
     * @return the rank of the first zone of kind "whichKind",
     * declared at this level.
     */
    public int firstDeclaredZone(int whichKind) {
        return firstDeclaredZone[whichKind];
    }

    /**
     * Computes the rank of the first zone of kind "whichKind",
     * declared at this level.
     */
    protected void computeFirstDeclaredZone(int[] globalFirstZone,
                                            SymbolTable basisNumberingST) {
	for (int i=0 ; i<4 ; ++i) { // i.e. for i in {ALLKIND, REALKIND, INTKIND, PTRKIND}
	    firstDeclaredZone[i] = globalFirstZone[i];
	    if (globalFirstZone[i] < basisNumberingST.freeDeclaredZone[i])
		firstDeclaredZone[i] = basisNumberingST.freeDeclaredZone[i] ;
	    freeDeclaredZone[i] = firstDeclaredZone[i];
	}
    }

    /**
     * @return the ZoneInfo for the zone of kind "whichKind" number "zone".
     */
    public ZoneInfo declaredZoneInfo(int zone, int whichKind) {
        SymbolTable symbolTable = this;
        ZoneInfo result = null, resultHere ;
        while (symbolTable != null) {
            resultHere = symbolTable.localDeclaredZoneInfo(zone, whichKind);
            if (resultHere==null) { //We have not found one at this level.
                if (symbolTable.isImports
                    || zone < symbolTable.freeDeclaredZone[whichKind]) { //Continue searching in basis levels:
                    symbolTable = symbolTable.basisSymbolTable;
                } else { //We haven't found it and there is no chance we find in at basis levels:
                    symbolTable = null ;
                }
            } else { //We have found one. stop.
                result = resultHere ;
                symbolTable = null ;
            }
        }
        return result;
    }

    /**
     * Declares that this current SymbolTable is the level that
     * contains the declarations of the formal parameters.
     * This is because this level has special behavior (for Fortran).
     */
    public void declareFormalParamsLevel() {
        isFormalParams = true;
    }

    /**
     * @return true when this current SymbolTable is the level that
     * contains the declarations of the formal parameters.
     */
    public boolean isFormalParamsLevel() {
        return isFormalParams;
    }

    /**
     * Adds a new NewSymbolHolder into the current
     * list of waiting NewSymbolHolder's.
     */
    public void addNewSymbolHolder(NewSymbolHolder newSymbolHolder) {
        if (!TapList.contains(newSymbolHolders.tail, newSymbolHolder)) {
            newSymbolHolders.placdl(newSymbolHolder);
        }
    }

    /**
     * Deletes a NewSymbolHolder from the current
     * list of waiting NewSymbolHolder's.
     */
    public void delNewSymbolHolder(NewSymbolHolder newSymbolHolder) {
        TapList<NewSymbolHolder> inNewSymbolHolders = newSymbolHolders;
        while (inNewSymbolHolders.tail != null) {
            if (inNewSymbolHolders.tail.head == newSymbolHolder) {
                inNewSymbolHolders.tail = inNewSymbolHolders.tail.tail;
            } else {
                inNewSymbolHolders = inNewSymbolHolders.tail;
            }
        }
    }

    /**
     * Solves all the NewSymbolHolder's that are
     * waiting for declaration in this SymbolTable.
     */
    public void solveNewSymbolHolders(boolean noCopySuffix) {
        TapList<NewSymbolHolder> orderedNewSymbolHolders =
                TapList.nreverse(newSymbolHolders.tail);
        newSymbolHolders.tail = null;
        while (orderedNewSymbolHolders != null) {
            orderedNewSymbolHolders.head.solve(this, noCopySuffix);
            orderedNewSymbolHolders = orderedNewSymbolHolders.tail;
        }
    }

    /**
     * @return an existing NewSymbolHolder of the given name and type
     * in this SymbolTable,
     * excluding the given "excluded" ones (e.g. because they are already in use).
     */
    public NewSymbolHolder reuseNewSymbolHolder(String name, WrapperTypeSpec type,
                                                TapList<NewSymbolHolder> excluded) {
        NewSymbolHolder symbolHolder = null;
        NewSymbolHolder oneSymbolHolder;
        TapList<NewSymbolHolder> inNewSymbolHolders = newSymbolHolders.tail;
        while (symbolHolder == null && inNewSymbolHolders != null) {
            oneSymbolHolder = inNewSymbolHolders.head;
            if (name.equals(oneSymbolHolder.probableName())
                    && !TapList.contains(excluded, oneSymbolHolder)
                    && oneSymbolHolder.typeSpec().equalsLiterally(type)) {
                symbolHolder = oneSymbolHolder;
            }
            inNewSymbolHolders = inNewSymbolHolders.tail;
        }
        return symbolHolder;
    }

    /**
     * @return true when this SymbolTable defines absolutely nothing new
     * with respect to its basisSymbolTable.
     */
    protected boolean isUseless() {
        boolean useless = !isImports && !isPublic && basisSymbolTable != null && nameSpaceName == null &&
                freeDeclaredZone[SymbolTableConstants.ALLKIND] == basisSymbolTable.freeDeclaredZone[SymbolTableConstants.ALLKIND];
        if (useless) {
            for (int i = 0; i < NUM_BUCKETS; i++) {
                if (buckets[i].tail != null) {
                    useless = false;
                }
            }
        }
        if (useless) {
            for (int i = 0; i <= 25; i++) {
                if (implicits[i] != basisSymbolTable.implicits[i]) {
                    useless = false;
                }
            }
        }
        return useless;
    }

    /**
     * Moves all SymbolDecls in symbolDecls, and all those recursively used,
     * into targetSymbolTable.
     * Sweeps all SymbolTables on the way from this to targetSymbolTable.
     * all SymbolDecls in symbolDecls or recursively used
     * are collected and removed from where they were.
     * Finally they are all inserted back into targetSymbolTable.
     */
    protected void moveSymbolDeclListUpRec(TapList<SymbolDecl> symbolDecls, SymbolTable targetSymbolTable) {
        SymbolTable fromSymbolTable = this;
        TapList<SymbolDecl> removedDecls = null;
        TapList<SymbolDecl> toSearchedDecls = new TapList<>(null, symbolDecls);
        TapList<SymbolDecl> inSearchedDecls;
        SymbolDecl searchedDecl;
        int i;
        TapList<SymbolDecl> bucket;
        TapList<SymbolDecl> dependsOn;

        // vmp: on remonte les this.addSymbolDecls qui sont dans les symbolDecls
        // sur la targetSymbolTable
        // sauf s'ils ont un modifier private
        TapList<SymbolDecl> moveAddSymbolDecls = null;
        TapList<SymbolDecl> tmpList = symbolDecls;
        while (tmpList != null) {
            if (tmpList.head != null) {
                if (TapList.contains(this.addSymbolDecls, tmpList.head)
                        || tmpList.head.hasPublicModifier(addSymbolDecls)
                        || tmpList.head.hasModifier("extern")) {
                    moveAddSymbolDecls = new TapList<>(tmpList.head, moveAddSymbolDecls);
                    if (tmpList.head.hasPublicModifier(addSymbolDecls)) {
                        SymbolDecl addSymbolDecl =
                                TapList.findNamedSymbolDecl(addSymbolDecls, tmpList.head.symbol);
                        moveAddSymbolDecls = new TapList<>(addSymbolDecl, moveAddSymbolDecls);
                        this.addSymbolDecls = TapList.delete(addSymbolDecl, this.addSymbolDecls);
                    }
                }
            }
            tmpList = tmpList.tail;
        }
        targetSymbolTable.addSymbolDecls = moveAddSymbolDecls;
        while (fromSymbolTable != targetSymbolTable) {
            inSearchedDecls = toSearchedDecls;
            while (inSearchedDecls.tail != null) {
                searchedDecl = inSearchedDecls.tail.head;
                /*vmp: searchedDecl peut etre null
                 que faut-il faire dans ce cas
                 */
                if (searchedDecl != null
                        && !searchedDecl.hasPrivateModifier(this.addSymbolDecls)) {
                    i = hashKey(searchedDecl.symbol);
                    bucket = fromSymbolTable.buckets[i];
                    while (bucket.tail != null &&
                            bucket.tail.head != searchedDecl) {
                        bucket = bucket.tail;
                    }
                    if (bucket.tail != null) {
                        /*if searchedDecl found in fromSymbolTable,
                         remove searchedDecl from fromSymbolTable:*/
                        bucket.tail = bucket.tail.tail;
                        /*add searchedDecl to removedDecls :*/
                        removedDecls = new TapList<>(searchedDecl,
                                removedDecls);
                        /*remove searchedDecl from toSearchedDecls :*/
                        inSearchedDecls.tail = inSearchedDecls.tail.tail;
                        /*add dependsOn of searchedDecl into toSearchedDecls :*/
                        dependsOn = searchedDecl.dependsOn();
                        while (dependsOn != null) {
                            inSearchedDecls.tail = new TapList<>(dependsOn.head,
                                    inSearchedDecls.tail);
                            dependsOn = dependsOn.tail;
                        }
                    } else {
                        inSearchedDecls = inSearchedDecls.tail;
                    }
                } else {
                    inSearchedDecls = inSearchedDecls.tail;
                }
            }
            fromSymbolTable = fromSymbolTable.basisSymbolTable;
        }
        /*insert all removed SymbolDecls into the targetSymbolTable:*/
        while (removedDecls != null) {
            searchedDecl = removedDecls.head;
            searchedDecl.recomputeDependsOn(targetSymbolTable);
            i = hashKey(searchedDecl.symbol);
            bucket = targetSymbolTable.buckets[i];
            bucket.placdl(searchedDecl);
            removedDecls = removedDecls.tail;
        }
    }

    /**
     * @return null if there is no common enclosing SymbolTable.
     */
    public SymbolTable getCommonRoot(SymbolTable otherSymbolTable) {
        TapList<SymbolTable> list1 = this.allSymbolTableRoots();
        TapList<SymbolTable> list2 = otherSymbolTable.allSymbolTableRoots();
        if (list1.head != list2.head) {
            //No common root !
            return null;
        }
        while (list1.tail != null && list2.tail != null &&
                list1.tail.head == list2.tail.head) {
            list1 = list1.tail;
            list2 = list2.tail;
        }
        return list1.head;
    }

    /**
     * @return the list of all the root SymbolTables of this SymbolTable,
     * including this SymbolTable.
     * The resulting list is ordered with the rootmost SymbolTable first.
     */
    public TapList<SymbolTable> allSymbolTableRoots() {
        SymbolTable current = this;
        TapList<SymbolTable> result = new TapList<>(current, null);
        while ((current = current.basisSymbolTable()) != null) {
            result = new TapList<>(current, result);
        }
        return result;
    }

    /**
     * Same as allSymbolTableRoots(), but stops going rootwards when reaching
     * the given SymbolTable "limit". The result is ordered rootmost SymbolTable first,
     * never contains the "limit", and contains "this" only when "this" is not the "limit".
     */
    public TapList<SymbolTable> allSymbolTableRoots(SymbolTable limit) {
        SymbolTable current = this;
        TapList<SymbolTable> result = null;
        while (current != limit && current != null) {
            result = new TapList<>(current, result);
            current = current.basisSymbolTable();
        }
        return result;
    }

    /**
     * @return true iff this SymbolTable is nested in the given "symbolTable".
     * In other words, "symbolTable" is "this" or a recursive root of "this".
     */
    public boolean nestedIn(SymbolTable symbolTable) {
        boolean nested = false;
        SymbolTable nestedST = this;
        while (nestedST != null && !nested) {
            nested = nestedST == symbolTable;
            nestedST = nestedST.basisSymbolTable();
        }
        return nested;
    }

    /**
     * Copy every symbol from parentClassSymbolTable to this. Add a marker containing the inherited class unit.
     *
     * @param parentClassSymbolTable
     * @param classUnit
     * @param inheritedClassUnit
     * @param isVirtualInheritance
     */
    protected void importAllClassSymbolTable(SymbolTable parentClassSymbolTable, Unit classUnit,
                                             Unit inheritedClassUnit, boolean isVirtualInheritance) {
        SymbolDecl virtualSymbolDecl = null;
        SymbolDecl symbolDecl = null;
        SymbolDecl oldSymbolDecl;

        InheritanceTree oldInheritanceTree;

        TapList<InheritanceTree> tmpOldVirtualChildren;
        InheritanceTree tmpOldVirtualChild;
        InheritanceTree copyOldVirtualChild;

        TapList<InheritanceTree> tmpOldChildren;
        InheritanceTree tmpOldChild;
        InheritanceTree copyOldChild;

        TapList<SymbolDecl> oldBucket;
        boolean virtualDeclAlreadyPresent = false;
        Unit declarationUnit;

        for (int i = 0; i < NUM_BUCKETS; i++) {
            // Each Bucket always starts with a null element :
            oldBucket = parentClassSymbolTable.buckets[i].tail;
            while (oldBucket != null) {
                oldSymbolDecl = oldBucket.head;
                oldInheritanceTree = oldSymbolDecl.inheritanceTree;

                if (oldInheritanceTree != null) {
                    // already an inherited symbol
                    tmpOldVirtualChildren = oldInheritanceTree.virtualChildren;
                    tmpOldChildren = oldInheritanceTree.children;

                    if (tmpOldVirtualChildren != null) {
                        declarationUnit = tmpOldVirtualChildren.head.declarationUnit;
                        virtualSymbolDecl = this.getTopVirtualSymbolDecl(declarationUnit, oldSymbolDecl);
                        virtualDeclAlreadyPresent = virtualSymbolDecl != null;

                        // TODO: do not search twice in the same ST
                        if (virtualSymbolDecl == null) {
                            virtualSymbolDecl = classUnit.privateSymbolTable().getTopVirtualSymbolDecl(declarationUnit, oldSymbolDecl);
                        }
                        if (virtualSymbolDecl == null) {
                            virtualSymbolDecl = classUnit.protectedSymbolTable().getTopVirtualSymbolDecl(declarationUnit, oldSymbolDecl);
                        }
                        if (virtualSymbolDecl == null) {
                            virtualSymbolDecl = classUnit.publicSymbolTable().getTopVirtualSymbolDecl(declarationUnit, oldSymbolDecl);
                        }
                        if (virtualSymbolDecl == null) {
                            virtualSymbolDecl = SymbolDecl.copy(oldSymbolDecl);
                            virtualSymbolDecl.inheritanceTree = new InheritanceTree();
                        }
                        while (tmpOldVirtualChildren != null) {
                            // we're looking for other virtual symbol
                            tmpOldVirtualChild = tmpOldVirtualChildren.head;
                            copyOldVirtualChild = fr.inria.tapenade.representation.InheritanceTree.copy(tmpOldVirtualChild);
                            copyOldVirtualChild.currentUnit = inheritedClassUnit;
                            if (!isVirtualInheritance)
                            // we change the declaration unit because this child is not virtually inherited:
                            {
                                copyOldVirtualChild.declarationUnit = classUnit;
                            }
                            virtualSymbolDecl.inheritanceTree.addChild(copyOldVirtualChild, isVirtualInheritance);
                            tmpOldVirtualChildren = tmpOldVirtualChildren.tail;
                        }
                    }
                    if (tmpOldChildren != null) {
                        symbolDecl = SymbolDecl.copy(oldSymbolDecl);
                        while (tmpOldChildren != null) {
                            tmpOldChild = tmpOldChildren.head;
                            copyOldChild = InheritanceTree.copy(tmpOldChild);
                            copyOldChild.currentUnit = inheritedClassUnit;
                            copyOldChild.declarationUnit = classUnit;
                            if (virtualSymbolDecl != null) {
                                virtualSymbolDecl.inheritanceTree.addChild(copyOldChild, isVirtualInheritance);
                            } else {
                                symbolDecl.inheritanceTree.addChild(copyOldChild, isVirtualInheritance);
                            }
                            tmpOldChildren = tmpOldChildren.tail;
                        }
                    }

                    // add the symbol to this ST
                    if (!virtualDeclAlreadyPresent && virtualSymbolDecl != null) {
                        this.addSymbolDecl(virtualSymbolDecl);
                    } else if (symbolDecl != null) {
                        this.addSymbolDecl(symbolDecl);
                    }

                } else {
                    // Not an inherited symbolDecl
                    symbolDecl = SymbolDecl.copy(oldSymbolDecl);

                    symbolDecl.inheritanceTree = new InheritanceTree();
                    symbolDecl.inheritanceTree.currentUnit = inheritedClassUnit;
                    if (isVirtualInheritance) {
                        symbolDecl.inheritanceTree.declarationUnit = inheritedClassUnit;
                    } else {
                        symbolDecl.inheritanceTree.declarationUnit = classUnit;
                    }

                    this.addSymbolDecl(symbolDecl);

                }
                oldBucket = oldBucket.tail;
            }
        }
    }

    protected void importModuleSymbolTable(TapTriplet<Unit, Tree, Instruction> oneUse, boolean checkExist) {
        Unit module = oneUse.first ;
        Tree renamedVisibles = oneUse.second ;
        Instruction instr = oneUse.third ;
        SymbolTable origSymbolTable = module.publicSymbolTable();
        boolean isOnly = (renamedVisibles.opCode()==ILLang.op_onlyVisibles) ;
        // Exit in weird cases (e.g. a "ONLY <nothing>" restriction)
        if (origSymbolTable == null || (isOnly && renamedVisibles.length() == 0)) return ;
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.printlnOnTrace("  Importing into SymbolTable @"+Integer.toHexString(hashCode())+" of Unit " + unit.name() + " symbols used from module " + module.name());
        }
        // Collect "onlyVisibles" and "renamings" lists by exploring the given import restrictions:
        Tree[] restrictions = renamedVisibles.children();
        Tree restriction ;
        TapList<String> onlyVisibles = null ;
        TapList<TapPair<String, TapList<Tree>>> renamings = null ;
        for (int i=restrictions.length-1 ; i>=0 ; --i) {
            restriction = restrictions[i] ;
            if (restriction!=null && restriction.opCode()==ILLang.op_binary) {
                // operator(userdefined):
                restriction = restriction.down(2) ;
            }
            if (restriction!=null) {
                switch (restriction.opCode()) {
                    case ILLang.op_ident:
                        // This can happen only inside a "ONLY"
                        onlyVisibles = new TapList<>(restriction.stringValue(), onlyVisibles) ;
                        // Name is "renamed as itself":
                        renamings = addNewRenaming(renamings, restriction.stringValue(), ILUtils.copy(restriction)) ;
                        break;
                    case ILLang.op_renamed: {
                        Tree origNameTree = restriction.down(2) ;
                        if (origNameTree.opCode()==ILLang.op_binary)
                            origNameTree = origNameTree.down(2) ;
                        String origName = origNameTree.stringValue() ;
                        Tree newNameTree = restriction.down(1) ;
                        if (newNameTree.opCode()==ILLang.op_binary)
                            newNameTree = newNameTree.down(2) ;
                        // origName is renamed as newNameTree:
                        renamings = addNewRenaming(renamings, origName, newNameTree) ;
                        if (isOnly) {
                            onlyVisibles = new TapList<>(origName, onlyVisibles) ;
                        }
                        break;
                    }
                    default: {
                        String operatorName = restriction.opName();
                        InterfaceDecl operatorDecl = origSymbolTable.getTopInterfaceDecl(operatorName);
                        if (operatorDecl==null) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, restriction, "Incorrect only declaration "+ILUtils.toString(restriction));
                        } else {
                            // This can happen only inside a "ONLY"
                            onlyVisibles = new TapList<>(operatorDecl.symbol, onlyVisibles) ;
                        }
                        break;
                    }
                }
            }
        }

        if (checkExist) {
            TapList<String> inOnlyVisibles = onlyVisibles;
            while (inOnlyVisibles != null) {
                String origSymbolName = inOnlyVisibles.head;
                if (origSymbolTable.getTopSymbolDecl(origSymbolName) == null
                    && module.importsSymbolTable().getTopSymbolDecl(origSymbolName) == null
                    //was && module.getImportedDecl(origSymbolName, SymbolTableConstants.SYMBOL) == null
                    ) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(DD17) Symbol " + origSymbolName + " is not defined in module " + module.name());
                }
                inOnlyVisibles = inOnlyVisibles.tail;
            }
        }

        TapList<SymbolDecl> inCurBucket, targetBucket;
        SymbolDecl origSymbolDecl, importedSymbolDecl;
        String origSymbolName ;
        TapList<TapPair<SymbolDecl, SymbolDecl>> waitingForDependsOn = null ;
        for (int i = 0; i < NUM_BUCKETS; i++) {
            inCurBucket = origSymbolTable.buckets[i].tail ;
            while (inCurBucket!=null) {
                origSymbolDecl = inCurBucket.head ;
                origSymbolName = origSymbolDecl.symbol ;
                if (!(origSymbolDecl.isA(SymbolTableConstants.FUNCTION) && ((FunctionDecl) origSymbolDecl).isModule())
                    &&
                    ((!TapList.containsString(origSymbolDecl.extraInfo(), "private", false)
                      && (TapList.containsString(origSymbolDecl.extraInfo(), "public", false)
                          || module.fortranStuff() == null
                          || module.fortranStuff().publicPrivateDefault == null
                          || !module.fortranStuff().publicPrivateDefault.equals("private")))
                     || origSymbolDecl.hasPublicModifier(origSymbolTable.addSymbolDecls))
                    &&
                    (onlyVisibles == null
                     || TapList.containsEquals(onlyVisibles, origSymbolName))) {
                    // import this symbol, one for each new name it is given:
		    TapList<Tree> newNameTrees = TapList.cassqString(origSymbolName, renamings) ;
                    if (newNameTrees==null) { // imported, but no renaming because not in "only":
                        newNameTrees = new TapList<>(ILUtils.build(ILLang.op_ident, origSymbolName), null) ;
                    }
                    while (newNameTrees!=null) {
                        Tree newNameTree = newNameTrees.head ;
                        // Protection because current mechanism duplicates calls to importModuleSymbolTable(). Fix that? :
                        if (!this.alreadyImports(newNameTree.stringValue(), origSymbolDecl, origSymbolTable)) {
                            // [llh 14Jan22] NOTE: Here we copy origSymbolDecl, even when it is not renamed.
                            // This has consequences in VarRefDifferentiator.collectSiblingDecls().
                            // Another option could be to copy origSymbolDecl ONLY when it is renamed.
                            importedSymbolDecl = buildImportedSymbolDecl(origSymbolDecl, newNameTree) ;
                            importedSymbolDecl.importedFrom = new TapPair<>(origSymbolDecl, origSymbolTable) ;
                            importedSymbolDecl.shareActivity(origSymbolDecl);
                            // Mechanism for keeping the "dependsOn" up to date:
                            // a) keep track of new SymbolDecl
                            if (importedSymbolDecl.symbol.equals(origSymbolDecl.symbol)) {
                                origSymbolDecl.copySymbolDecl = importedSymbolDecl ;
                            }
                            // b) remember that the "dependsOn" need to be updated:
                            if (importedSymbolDecl.dependsOn != null) {
                                waitingForDependsOn =
                                    new TapList<>(new TapPair<>(origSymbolDecl, importedSymbolDecl),
                                                  waitingForDependsOn) ;
                            }
                            // Check name conflicts with symbols of the USE'ing Unit or symbols already imported by the USE'ing Unit:
                            SymbolDecl conflictDecl = unit.publicSymbolTable().findConflictDecl(importedSymbolDecl) ;
                            if (conflictDecl==null) {
                                conflictDecl = this.findConflictDecl(importedSymbolDecl) ;
                            }
                            if (conflictDecl!=null) {
                                TapEnv.fileError(instr.tree, "(TCxx) conflicting declarations of "+importedSymbolDecl.symbol+" in USE of module "+ module.name());
                            }
                            this.buckets[hashKey(importedSymbolDecl.symbol)].placdl(importedSymbolDecl) ; // was addSymbolDecl(importedSymbolDecl) ;
                        }
                        newNameTrees = newNameTrees.tail ;
                    }
                }
                inCurBucket = inCurBucket.tail ;
            }
        }
        // Mechanism for keeping the "dependsOn" up to date.
        // Now that all symbols have been copied, update the needed "dependsOn"'s:
        while (waitingForDependsOn!=null) {
            origSymbolDecl = waitingForDependsOn.head.first ;
            importedSymbolDecl = waitingForDependsOn.head.second ;
            origSymbolDecl.copyLinksIntoCopy(importedSymbolDecl, null) ;
            waitingForDependsOn = waitingForDependsOn.tail ;
        }
        // Erase the "copySymbolDecl" fields, not needed any more:
        origSymbolTable.cleanCopySymbolDecls() ;

    }

    private boolean alreadyImports(String name, SymbolDecl origSymbolDecl, SymbolTable origSymbolTable) {
        TapList<SymbolDecl> inBucket = this.buckets[hashKey(name)].tail ;
        SymbolDecl curDecl ;
        boolean found = false ;
        while (!found && inBucket!=null) {
            curDecl = inBucket.head ;
            found = (curDecl.symbol.equals(name) && curDecl.importedFrom!=null
                     && curDecl.importedFrom.first==origSymbolDecl
                     && curDecl.importedFrom.second==origSymbolTable) ;
            inBucket = inBucket.tail ;
        }
        return found ;
    }

    private TapList<TapPair<String, TapList<Tree>>> addNewRenaming(TapList<TapPair<String, TapList<Tree>>> renamings,
                                                                   String origName, Tree newNameTree) {
        TapPair<String, TapList<Tree>> found = TapList.assqString(origName, renamings) ;
        if (found==null) {
            found = new TapPair<>(origName, null) ;
            renamings = new TapList<>(found, renamings) ;
        }
        found.second = new TapList<>(newNameTree, found.second) ;
        return renamings ;
    }

    private SymbolDecl findConflictDecl (SymbolDecl nextSymbolDecl) {
        String importedName = nextSymbolDecl.symbol ;
        SymbolDecl conflictDecl =
            this.getDecl(importedName, SymbolTableConstants.SYMBOL, true);
        if (conflictDecl!=null) {
            // discard non-conflicting situations
            if (// No conflict if this is a duplicate decl of the same object:
                SymbolDecl.sameOriginDecl(conflictDecl, nextSymbolDecl)
                || // No conflict if the conflict is between FunctionDecl's,
                   // because overloading is permitted. Also no conflict with InterfaceDecl's:
                ((conflictDecl instanceof FunctionDecl || conflictDecl instanceof InterfaceDecl) &&
                 (nextSymbolDecl instanceof FunctionDecl || nextSymbolDecl instanceof InterfaceDecl))
                || // Type names used in the USE'ing Unit give birth to a temporary NamedTypeSpec
                   // that does not cause a conflict with the true type when we find it in the USE'd module:
                (conflictDecl instanceof TypeDecl && ((TypeDecl)conflictDecl).typeSpec.isNamedType()))
                conflictDecl = null ;
        }
        return conflictDecl ;
    }

    private SymbolDecl buildImportedSymbolDecl(SymbolDecl origSymbolDecl, Tree newNameTree) {
        SymbolDecl newSymbolDecl;
        switch (origSymbolDecl.kind) {
            case SymbolTableConstants.FUNCTION: {
                newSymbolDecl = new FunctionDecl(newNameTree, ((FunctionDecl) origSymbolDecl).unit());
                break;
            }
            case SymbolTableConstants.TYPE: {
                newSymbolDecl = new TypeDecl(newNameTree, ((TypeDecl) origSymbolDecl).typeSpec, unit);
                break;
            }
            case SymbolTableConstants.CONSTANT: {
                VariableDecl newConstantDecl = new VariableDecl(newNameTree, origSymbolDecl.type());
                newConstantDecl.setConstant() ;
                newConstantDecl.setInitializationTree(((VariableDecl)origSymbolDecl).getInitializationTree()) ;
                newConstantDecl.setExtraInfo(origSymbolDecl.extraInfo());
                newConstantDecl.constantValue = ((VariableDecl)origSymbolDecl).constantValue ;
                newSymbolDecl = newConstantDecl ;
                break;
            }
            case SymbolTableConstants.VARIABLE: {
                newSymbolDecl = new VariableDecl(newNameTree, origSymbolDecl.type());
                ((VariableDecl) newSymbolDecl).adoptZones((VariableDecl) origSymbolDecl);
                newSymbolDecl.setExtraInfo(origSymbolDecl.extraInfo());
                break;
            }
            case SymbolTableConstants.FIELD: {
                newSymbolDecl = new FieldDecl(newNameTree, origSymbolDecl.type());
                break;
            }
            case SymbolTableConstants.INTERFACE: {
                newSymbolDecl = new InterfaceDecl(newNameTree);
                newSymbolDecl.nameTree = origSymbolDecl.nameTree;
                ((InterfaceDecl) newSymbolDecl).contents =
                        ((InterfaceDecl) origSymbolDecl).contents;
                ((InterfaceDecl) newSymbolDecl).functionDecls =
                        ((InterfaceDecl) origSymbolDecl).functionDecls;
                ((InterfaceDecl) newSymbolDecl).functionNames =
                        ((InterfaceDecl) origSymbolDecl).functionNames;
                ((InterfaceDecl) newSymbolDecl).definitionSymbolTable =
                        ((InterfaceDecl) origSymbolDecl).definitionSymbolTable;
                ((InterfaceDecl) newSymbolDecl).containerUnit =
                        ((InterfaceDecl) origSymbolDecl).containerUnit;
                break;
            }
            default: {
                TapEnv.toolWarning(-1, "(Import Symbol Decl) unexpected SymbolDecl: "+origSymbolDecl) ;
                newSymbolDecl = origSymbolDecl;
                break;
            }
        }
        newSymbolDecl.dependsOn = origSymbolDecl.dependsOn ;
	return newSymbolDecl ;
    }

    /**
     * @return the list of all public symbolDecl of the top level of this SymbolTable.
     */
    protected TapList<SymbolDecl> findPublicSymbolDecls() {
        TapList<SymbolDecl> publicSymbolDecls = null;
        TapList<SymbolDecl> curBucket;
        SymbolDecl symbolDecl;
        boolean caseSensitive = !unit.isFortran();
        String defaultAccess = "public";
        if (unit.isFortran()) {
            if (unit.fortranStuff() != null && unit.fortranStuff().publicPrivateDefault != null) {
                defaultAccess = unit.fortranStuff().publicPrivateDefault.toLowerCase();
            }
        } else if (unit.isCPlusPlus()) {
            if (this.unit.privacy != null && !this.unit.privacy.isEmpty()) {
                defaultAccess = this.unit.privacy;
            }
        }
        for (int i = NUM_BUCKETS - 1; i >= 0; --i) {
            curBucket = this.buckets[i].tail;
            while (curBucket != null) {
                symbolDecl = curBucket.head;
                if (TapList.containsString(symbolDecl.extraInfo(), "public", caseSensitive)
                        ||
                        symbolDecl.hasPublicModifier(this.addSymbolDecls)
                        ||
                        "public".equals(defaultAccess)
                                && !TapList.containsOneOfStrings(symbolDecl.extraInfo(),
                                new String[]{"private", "protected"}, caseSensitive)
                                && !symbolDecl.hasPrivateModifier(this.addSymbolDecls)
                                && !symbolDecl.hasProtectedModifier(this.addSymbolDecls)) {
                    publicSymbolDecls = new TapList<>(symbolDecl, publicSymbolDecls);
                }
                curBucket = curBucket.tail;
            }
        }
        return publicSymbolDecls;
    }

    /**
     * @return the list of all protected symbolDecl of the top level of this SymbolTable.
     */
    protected TapList<SymbolDecl> findProtectedSymbolDecls() {
        TapList<SymbolDecl> protectedSymbolDecls = null;
        TapList<SymbolDecl> curBucket;
        SymbolDecl symbolDecl;
        boolean caseSensitive = !unit.isFortran();
        String defaultAccess = "public";
        if (unit.isFortran()) {
            if (unit.fortranStuff() != null && unit.fortranStuff().publicPrivateDefault != null) {
                defaultAccess = unit.fortranStuff().publicPrivateDefault.toLowerCase();
            }
        } else if (unit.isCPlusPlus()) {
            if (this.unit.privacy != null && !this.unit.privacy.isEmpty()) {
                defaultAccess = this.unit.privacy;
            }
        }
        for (int i=NUM_BUCKETS-1 ; i>=0 ; --i) {
            curBucket = this.buckets[i].tail;
            while (curBucket != null) {
                symbolDecl = curBucket.head;
                if (TapList.containsString(symbolDecl.extraInfo(), "protected", caseSensitive)
                        ||
                        symbolDecl.hasProtectedModifier(this.addSymbolDecls)
                        ||
                        "protected".equals(defaultAccess)
                                && !TapList.containsOneOfStrings(symbolDecl.extraInfo(),
                                new String[]{"private", "public"}, caseSensitive)
                                && !symbolDecl.hasPrivateModifier(this.addSymbolDecls)
                                && !symbolDecl.hasPublicModifier(this.addSymbolDecls)) {
                    protectedSymbolDecls = new TapList<>(symbolDecl, protectedSymbolDecls);
                }
                curBucket = curBucket.tail;
            }
        }
        return protectedSymbolDecls;
    }

    /**
     * @return true if translationUnit contains global variables definitions
     * either static (variableDecl defined in this translationUnit-symbolTable
     * or global (variable defined in this translationUnit-symbolTable.declarationsBlock
     * with variableDecl in rootCSymbolTable).
     */
    public boolean isEmptyTranslationUnitST() {
// [llh 16Fev2018] we will say it is empty (bad name) if file contains only procedure definitions:
        return allUnitPlaceHolders(declarationsBlock.instructions);
//         boolean isEmpty = (declarationsBlock.instructions==null) ;
//         for (int i=this.NUM_BUCKETS-1 ; i>=0 && isEmpty ; --i) {
//             isEmpty = (buckets[i].tail==null) ;
//         }
//         return isEmpty ;
    }

    /**
     * @return true if translationUnit file must be created.
     */
    public boolean mustBeCreated() {
        return declarationsBlock.instructions != null;
    }

    /**
     * @return true if all instructions in the list are place holders for Unit definitions
     */
    private boolean allUnitPlaceHolders(TapList<Instruction> instrs) {
        boolean allPlaceHolders = true;
        while (instrs != null && allPlaceHolders) {
            allPlaceHolders = instrs.head.tree != null && instrs.head.tree.getAnnotation("Unit") != null;
            instrs = instrs.tail;
        }
        return allPlaceHolders;
    }

    /**
     * @return null if no declaration is found.
     */
    public SymbolDecl getSymbolDecl(String name) {
        return getDecl(name, SymbolTableConstants.SYMBOL, false);
    }

    /**
     * @param name symbol name
     * @return all symbols with the same name.
     */
    private TapList<SymbolDecl> getAllSymbolDecls(String name) {
        TapList<SymbolDecl> result = null;
        TapList<SymbolDecl> curBucket;
        SymbolDecl symbolDecl;
        for (int i = 0; i < NUM_BUCKETS; i++) {
            curBucket = this.buckets[i].tail;
            while (curBucket != null) {
                symbolDecl = curBucket.head;
                if (symbolDecl.symbol.equals(name)) {
                    result = new TapList<>(symbolDecl, result);
                }
                curBucket = curBucket.tail;
            }
        }
        return result;
    }

    private TapList<TapPair<SymbolDecl, InheritanceTree>> eliminateSymbolDeclsRec(TapList<TapPair<SymbolDecl,
            InheritanceTree>> possibleSymbolDecls, Tree instrOrExpr) {
        if (possibleSymbolDecls == null || TapList.length(possibleSymbolDecls) == 0) {
            return possibleSymbolDecls;
        }

        TapList<TapPair<SymbolDecl, InheritanceTree>> tmpPossibleSymbolDecls = possibleSymbolDecls;
        TapList<TapPair<SymbolDecl, InheritanceTree>> possibleSymbolDeclsResult = new TapList<>();
        SymbolDecl symbol;
        InheritanceTree tree;
        TapList<InheritanceTree> tmpChildren;
        TapList<InheritanceTree> tmpVirtualChildren;

        while (tmpPossibleSymbolDecls != null) {
            symbol = tmpPossibleSymbolDecls.head.first;
            tree = tmpPossibleSymbolDecls.head.second;
            String searchedString = ILUtils.getIdentString(instrOrExpr.down(2));

            tmpChildren = tree == null ? null : tree.children;
            while (tmpChildren != null) {
                if (tmpChildren.head.getName().equals(searchedString)) {
                    possibleSymbolDeclsResult = new TapList<>(new TapPair<>(symbol, tmpChildren.head), possibleSymbolDeclsResult);
                }
                tmpChildren = tmpChildren.tail;
            }

            tmpVirtualChildren = tree == null ? null : tree.virtualChildren;
            while (tmpVirtualChildren != null) {
                if (tmpVirtualChildren.head.getName().equals(searchedString)) {
                    possibleSymbolDeclsResult = new TapList<>(new TapPair<>(symbol, tmpVirtualChildren.head), possibleSymbolDeclsResult);
                }
                tmpVirtualChildren = tmpVirtualChildren.tail;
            }

            tmpPossibleSymbolDecls = tmpPossibleSymbolDecls.tail;
        }

        if (instrOrExpr.down(1).opCode() != ILLang.op_scopeAccess) {
            return possibleSymbolDeclsResult;
        }

        possibleSymbolDecls = eliminateSymbolDeclsRec(possibleSymbolDeclsResult, instrOrExpr.down(1));
        return possibleSymbolDecls;
    }

    private SymbolDecl getTopVirtualSymbolDecl(Unit declarationUnit, SymbolDecl oldSymbolDecl) {

        TapList allSymbolDecls;
        TapList<InheritanceTree> virtualChildren;
        if (oldSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
            FunctionDecl oldFunctionDecl = (FunctionDecl) oldSymbolDecl;
            FunctionTypeSpec functionTypeSpec = oldFunctionDecl.functionTypeSpec();
            allSymbolDecls = this.getTopFunctionDecl(oldFunctionDecl.symbol, functionTypeSpec.returnType, functionTypeSpec.argumentsTypes, true);
        } else {
            allSymbolDecls = getAllSymbolDecls(oldSymbolDecl.symbol); // several symbol with the same name in case of inherited symbols.
        }

        while (allSymbolDecls != null) {
            virtualChildren = ((SymbolDecl) allSymbolDecls.head).inheritanceTree.virtualChildren;
            if (virtualChildren != null) // all the symbol's virtual children have the same declarationUnit
            {
                if (virtualChildren.head != null && virtualChildren.head.declarationUnit == declarationUnit) {
                    return (SymbolDecl) allSymbolDecls.head;
                }
            }
            allSymbolDecls = allSymbolDecls.tail;
        }

        return null;
    }

    /**
     * @return the declaration of variable "name" in
     * this SymbolTable. @return null if no declaration is found.
     */
    public VariableDecl getVariableDecl(String name) {
        return (VariableDecl) getDecl(name, SymbolTableConstants.VARIABLE, false);
    }

    /**
     * @return the declaration of constant "name" in
     * this SymbolTable. @return null if no declaration is found.
     */
    public VariableDecl getConstantDecl(String name) {
        return (VariableDecl) getDecl(name, SymbolTableConstants.CONSTANT, false);
    }

    /**
     * @return the declaration of variable or constant "name" in
     * this SymbolTable. @return null if no declaration is found.
     */
    public VariableDecl getVariableOrConstantDecl(String name) {
        return (VariableDecl) getDecl(name, SymbolTableConstants.VARIABLE, SymbolTableConstants.CONSTANT, false, null);
    }

    /**
     * @return null if no declaration is found.
     */
    protected VariableDecl getVariableOrConstantOrFuncNameDecl(String name) {
        return (VariableDecl) getDecl(name, SymbolTableConstants.VARIABLE, SymbolTableConstants.CONSTANT, SymbolTableConstants.FUNCNAME, false);
    }

    /**
     * @return the declarations of function/method "name" in
     * this SymbolTable with a "returnType" and some "argumentsType". @return null if no declaration is found.
     */
    public TapList<FunctionDecl> getFunctionDecl(String name, TypeSpec returnType, TypeSpec[] argumentsTypes,
                                                 boolean checkArgsTypes) {
        return getAllPossibleTypedFunctionDecl(name, returnType, argumentsTypes, this, null, checkArgsTypes, false, false, true, null, false);
    }

    public TapList<FunctionDecl> getFunctionDecl(String name, TypeSpec returnType, TypeSpec[] argumentsTypes,
                                                 String className, boolean checkArgsTypes) {
        return getAllPossibleTypedFunctionDecl(name, returnType, argumentsTypes, this, null, checkArgsTypes, false, false, true, className, true);
    }

    /**
     * Same as getFunctionDecl, but for the C case, looks for
     * a variable declared of type function or pointer to function.
     */
    private VariableDecl getFuncVarDecl(String name) {
        VariableDecl varDecl = getVariableOrConstantOrFuncNameDecl(name);
        if (varDecl != null && WrapperTypeSpec.isFunctionOrPointerToFunction(varDecl.type())) {
            return varDecl;
        } else {
            return null;
        }
    }

    /**
     * Same as getFunctionDecl, but discards interfaces.
     */
    protected FunctionDecl getFunctionNotInterfaceDecl(String name) {
        return (FunctionDecl) getDecl(name, SymbolTableConstants.FUNCTION_NOT_INTERFACE, false);
    }

    /**
     * Same as getFunctionDecl, but discards external.
     */
    protected FunctionDecl getFunctionNotExternalNorInterfaceDecl(String name) {
        return (FunctionDecl) getDecl(name, SymbolTableConstants.FUNCTION_NOT_EXTERNAL_NOR_INTERFACE, false);
    }

    /**
     * @return the declaration of module "name" in
     * this SymbolTable. @return null if no declaration is found.
     */
    public FunctionDecl getModuleDecl(String name) {
        return (FunctionDecl) getDecl(name, SymbolTableConstants.MODULE, false);
    }

    /**
     * @return the scope, i.e. a SymbolTable, in which must be found the symbols prefixed by "prefixTree::".
     * For instance if looking for "prefix::a", getPrefixedNamedScope(ident:"prefix") is the scope
     * in which we expect to find declaration of "a".
     * "prefix" may be the name of a nameSpace or of a class. We first search "prefix" as a class name,
     * then as a nameSpace name. If not found (error), we create a nameSpace with that name.
     */
    public SymbolTable getPrefixedNamedScope(Tree prefixTree) {
        SymbolTable searchedSymbolTable;
        if (prefixTree.opCode() == ILLang.op_scopeAccess) {
            searchedSymbolTable = getPrefixedNamedScope(prefixTree.down(1));
            prefixTree = prefixTree.down(2);
        } else {
            searchedSymbolTable = this;
        }
        String prefixName = ILUtils.getIdentString(prefixTree);
        SymbolTable scopeST = searchedSymbolTable.getNamedScope(prefixName);
        if (scopeST == null && this.language == TapEnv.CPLUSPLUS) {
            Unit enclosingUnit = searchedSymbolTable.unit;
            TapEnv.fileError(prefixTree, "(TCxx) Adding undeclared namespace " + prefixName + " into " + searchedSymbolTable.shortName());
            scopeST = new SymbolTable(searchedSymbolTable);
            scopeST.setShortName("SymbolTable of nameSpace " + prefixName);
            scopeST.nameSpaceName = prefixName;
            scopeST.unit = enclosingUnit;
            enclosingUnit.addDerivedSymbolTable(scopeST);
            searchedSymbolTable.addNameSpaceDecl(prefixName, scopeST);
        }
        return scopeST;
    }

    public SymbolTable getNamedScope(String prefixName) {
        ClassDecl classDecl = getClassDecl(prefixName);
        SymbolTable result = null;
        if (classDecl != null) {
            if (classDecl.unit != null) {
                result = classDecl.unit.privateSymbolTable();
            }
        } else if (this.language == TapEnv.CPLUSPLUS) {
            NameSpaceDecl nameSpaceDecl = getNameSpaceDecl(prefixName);
            if (nameSpaceDecl != null) {
                result = nameSpaceDecl.symbolTable;
            }
        }
        return result;
    }

    public ClassDecl getClassDecl(Tree classNameTree) {
        SymbolTable targetSymbolTable = this;
        if (classNameTree.opCode() == ILLang.op_scopeAccess) {
            targetSymbolTable = getPrefixedNamedScope(classNameTree.down(1));
            classNameTree = classNameTree.down(2);
        }
        return targetSymbolTable == null ? null : targetSymbolTable.getClassDecl(ILUtils.getIdentString(classNameTree));
    }

    public ClassDecl getClassDecl(String name) {
        return (ClassDecl) getDecl(name, SymbolTableConstants.CLASS, false);
    }

    public NameSpaceDecl getSetNameSpaceDecl(Tree nameSpaceTree) {
        SymbolTable targetSymbolTable = this;
        if (nameSpaceTree.opCode() == ILLang.op_scopeAccess) {
            targetSymbolTable = getPrefixedNamedScope(nameSpaceTree.down(1));
            nameSpaceTree = nameSpaceTree.down(2);
        }
        String nameSpaceName = ILUtils.getIdentString(nameSpaceTree);
        assert targetSymbolTable != null;
        NameSpaceDecl result = targetSymbolTable.getNameSpaceDecl(nameSpaceName);
        if (result == null) {
            Unit enclosingUnit = targetSymbolTable.unit;
            SymbolTable nameSpaceST = new SymbolTable(targetSymbolTable);
            nameSpaceST.setShortName("SymbolTable of nameSpace " + nameSpaceName);
            nameSpaceST.nameSpaceName = nameSpaceName;
            nameSpaceST.unit = enclosingUnit;
            enclosingUnit.addDerivedSymbolTable(nameSpaceST);
            result = targetSymbolTable.addNameSpaceDecl(nameSpaceName, nameSpaceST);
        }
        return result;
    }

    public NameSpaceDecl getNameSpaceDecl(String name) {
        return (NameSpaceDecl) getDecl(name, SymbolTableConstants.NAMESPACE, false);
    }

    /**
     * When this SymbolTable sees a module named "name".
     *
     * @return its Unit.
     */
    protected Unit getModule(String name) {
        FunctionDecl modDecl = getModuleDecl(name);
        return modDecl == null ? null : modDecl.unit();
    }

    /**
     * @return all the visible declarations of "name" as an interface.
     */
    public TapList<InterfaceDecl> getAllInterfaceDecls(String name) {
        TapList<InterfaceDecl> result = null;
        String searchedName ;
        TapList<SymbolDecl> toBucket;
        SymbolDecl symbolDecl ;
        SymbolTable currentST = this;
        while (currentST.basisSymbolTable != null) {
            searchedName = name ;
            if (!currentST.caseDependent) searchedName = searchedName.toLowerCase() ;
            toBucket = currentST.buckets[hashKey(searchedName)] ;
            while (toBucket.tail!=null) {
                symbolDecl = toBucket.tail.head ;
                if (symbolDecl.isA(SymbolTableConstants.INTERFACE)
                    && symbolDecl.symbol.equals(searchedName)) {
                    result = new TapList<>((InterfaceDecl)symbolDecl, result) ;
                }
                toBucket = toBucket.tail ;
            }
            currentST = currentST.basisSymbolTable;
        }
        return result;
    }

    /**
     * @return null if no declaration is found.
     */
    public TypeDecl getTypeDecl(String name) {
        return (TypeDecl) getDecl(name, SymbolTableConstants.TYPE, false);
    }

    public SymbolDecl getTopDecl(String name, int kind) {
        return getDecl(name, kind, true);
    }

    /**
     * @return the declaration of symbol "name" in the toplevel of
     * this SymbolTable. @return null if no declaration is found.
     */
    public SymbolDecl getTopSymbolDecl(String name) {
        return getDecl(name, SymbolTableConstants.SYMBOL, true);
    }

    /**
     * @return the declaration of variable "name" in the toplevel of
     * this SymbolTable or null if no declaration is found.
     */
    public VariableDecl getTopVariableDecl(String name) {
        return (VariableDecl) getDecl(name, SymbolTableConstants.VARIABLE, true);
    }

    /**
     * @return the declaration of variable or constant "name" in the toplevel of
     * this SymbolTable or null if no declaration is found.
     */
    public VariableDecl getTopVariableOrConstantDecl(String name) {
        return (VariableDecl) getDecl(name, SymbolTableConstants.VARIABLE, SymbolTableConstants.CONSTANT, true, null);
    }

    /**
     * @return null if no declaration is found.
     */
    public TapList<FunctionDecl> getTopFunctionDecl(String name, TypeSpec returnType, TypeSpec[] argumentsTypes,
                                                    boolean checkArgsTypes) {
        return getAllPossibleTypedFunctionDecl(name, returnType, argumentsTypes, this, null, checkArgsTypes, true, false, true, null, false);
    }

    /**
     * @return null if no declaration is found.
     */
    public TapList<FunctionDecl> getTopFunctionDecl(String name, TypeSpec returnType, TypeSpec[] argumentsTypes,
                                                    String className, boolean checkArgsTypes) {
        return getAllPossibleTypedFunctionDecl(name, returnType, argumentsTypes, this, null, checkArgsTypes, true, false, true, className, true);
    }

    /**
     * @return the declaration of module "name" in the toplevel of
     * this SymbolTable. @return null if no declaration is found.
     */
    public FunctionDecl getTopModuleDecl(String name) {
        return (FunctionDecl) getDecl(name, SymbolTableConstants.MODULE, true);
    }

    public ClassDecl getTopClassDecl(String name) {
        return (ClassDecl) getDecl(name, SymbolTableConstants.CLASS, true);
    }

    public NameSpaceDecl getTopNameSpaceDecl(String name) {
        return (NameSpaceDecl) getDecl(name, SymbolTableConstants.NAMESPACE, true);
    }

    /**
     * @return the declaration of interface "name" in
     * this SymbolTable. @return null if no declaration is found.
     */
    public InterfaceDecl getTopInterfaceDecl(String name) {
        return (InterfaceDecl) getDecl(name, SymbolTableConstants.INTERFACE, true);
    }

    /**
     * @return the declaration of type "name" in the toplevel of
     * this SymbolTable. @return null if no declaration is found.
     */
    protected TypeDecl getTopTypeDecl(String name) {
        return (TypeDecl) getDecl(name, SymbolTableConstants.TYPE, true);
    }

    /**
     * Searches symbol "name" in this SymbolTable.
     * Searches and returns the declaration of symbol "name"
     * as a "kind", that may be TYPE, VARIABLE, FUNCTION,
     * or SYMBOL(meaning any kind).
     * The search is performed only in the top level of this SymbolTable
     * when "top" is true. At all levels otherwise.
     *
     * @return the SymbolDecl when found, null otherwise.
     */
    public SymbolDecl getDecl(String name, int kind, boolean top) {
        TapList<SymbolDecl> toBucket = getToDecl(name, kind, top, null);
        return (toBucket==null ? null : toBucket.tail.head) ;
    }

    /**
     * Same as getDecl(name,kind,top), but returns in "toST" a handle to the container SymbolTable.
     */
    public SymbolDecl getDecl(String name, int kind,
                        boolean top, ToObject<SymbolTable> toST) {
        TapList<SymbolDecl> toBucket = getToDecl(name, kind, top, toST);
        return (toBucket==null ? null : toBucket.tail.head) ;
    }

    /**
     * @return the declaration of function "name" in
     * this SymbolTable. @return null if no declaration is found.
     */
    public FunctionDecl getTypedFunctionDecl(String name, WrapperTypeSpec returnType, WrapperTypeSpec[] argumentsTypes,
                                             boolean top, Tree callTree) {
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("Search Typed FunctionDecl of " + name + new FunctionTypeSpec(returnType, argumentsTypes)
                    + " in " + (top ? "TOP of " : "") + this.addressChain());
            TapEnv.setTraceIndent(TapEnv.traceIndent() + 2);
        }
        FunctionDecl fDecl = getTypedFunctionDeclMoreOrLess(name, returnType, argumentsTypes, top, true, true, callTree);
        if (fDecl == null) {
            fDecl = getTypedFunctionDeclMoreOrLess(name, returnType, argumentsTypes, top, false, true, callTree);
        }
        if (fDecl == null) {
            fDecl = getTypedFunctionDeclMoreOrLess(name, returnType, argumentsTypes, top, false, false, callTree);
        }

        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.setTraceIndent(TapEnv.traceIndent() - 2);
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("Found " + fDecl);
        }
        return fDecl;
    }

    /**
     * Finds in this SymbolTable (and only at the top level if "top" is true)
     * the FunctionDecl that best matches the present called function signature
     * "name":"argumentsTypes"&rarr;"returnType".
     * <p>
     * This is not the search in overloaded "interfaces", and matching is different:<br>
     * In a 1st sweep, matching is granted if number AND types of args match.<br>
     * In a 2nd sweep, matching is granted if simply number of args match !<br>
     * "alsoMatchTypes" is true for 1st sweep, false for 2nd sweep.
     */
    private FunctionDecl getTypedFunctionDeclMoreOrLess(String name, WrapperTypeSpec returnType, WrapperTypeSpec[] argumentsTypes,
                                                        boolean top, boolean alsoMatchTypes, boolean sameNumberOfArgs, Tree callTree) {
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("search watching "
                    + (alsoMatchTypes ? "arguments and result types " : sameNumberOfArgs ? "only number of arguments " : "only function name ")
                    + " in " + (top ? "TOP of " : "") + this.addressChain());
            TapEnv.setTraceIndent(TapEnv.traceIndent() + 2);
        }
        String searchedName = name;
        int index = hashKey(name);
        SymbolTable curSymbolTable = this;
        TapList<SymbolDecl> toBucket;
        SymbolDecl symbolDecl;
        SymbolDecl resultDecl = null;
        SymbolDecl externalDecl = null;
        // iterate on the nested enclosing SymbolTable's
        int nbArgs = argumentsTypes.length;
        // si !sameNumberOfArgs on cherche la FunctionDecl
        //  qui a le nb d'arguments qui s'en rapproche le mieux
        SymbolDecl oneResultDecl = null;
        int bestNbArgs = -1;
        while (resultDecl == null && curSymbolTable != null) {
            // En C/C++ la symbolTable est toujours sensible a la casse.
            if (!curSymbolTable.caseDependent) {
                searchedName = name.toLowerCase();
                index = hashKey(searchedName);
            }
            toBucket = curSymbolTable.buckets[index];
            if (toBucket != null) {
                // iterate into this level bucket:
                while (resultDecl == null && toBucket.tail != null) {
                    symbolDecl = toBucket.tail.head;
                    if (symbolDecl.symbol.equals(searchedName)) {
                        if (symbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                            boolean matches;
                            if (alsoMatchTypes) {
                                matches =
                                        ((FunctionDecl) symbolDecl).declarationMatchesCall(returnType, argumentsTypes);
                            } else {
                                matches = nbArgsOf(((FunctionDecl) symbolDecl).functionTypeSpec()) == nbArgs;
                                if (matches && nbArgs > 0) {
                                    WrapperTypeSpec arg1Type
                                            = ((FunctionDecl) symbolDecl).functionTypeSpec().argumentsTypes[0];
                                    if (TypeSpec.isA(arg1Type, SymbolTableConstants.ARRAYTYPE)
                                            && !TypeSpec.isA(argumentsTypes[0], SymbolTableConstants.ARRAYTYPE)) {
                                        matches = false;
                                    }
                                }
                            }
                            if (matches) {
                                resultDecl = symbolDecl;
                                if (symbolDecl.isExternal()
                                        && !MPIcallInfo.isMessagePassingFunction(searchedName, language)) {
                                    externalDecl = resultDecl;
                                    resultDecl = null;
                                }
                            }
                            if (!sameNumberOfArgs && !matches) {
                                int symbolNbArgs = nbArgsOf(((FunctionDecl) symbolDecl).functionTypeSpec());
                                if (oneResultDecl == null || Math.abs(nbArgs - symbolNbArgs) < Math.abs(nbArgs - bestNbArgs)) {
                                    oneResultDecl = symbolDecl;
                                    bestNbArgs = symbolNbArgs;
                                }
                            }
                        } else if (symbolDecl.isA(SymbolTableConstants.INTERFACE)) {
                            if (alsoMatchTypes) {
                                resultDecl = ((InterfaceDecl) symbolDecl).findFunctionDecl(argumentsTypes, returnType, callTree);
                            }
                        }
                    }
                    toBucket = toBucket.tail;
                }
            }
            if (resultDecl == null) {
                if (top) {
                    curSymbolTable = null;
                } else {
                    curSymbolTable = curSymbolTable.basisSymbolTable();
                }
            }
        }
        if (resultDecl == null) {
            resultDecl = externalDecl;
        }
        if (resultDecl == null && oneResultDecl != null && !sameNumberOfArgs) {
            resultDecl = oneResultDecl;
        }
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.setTraceIndent(TapEnv.traceIndent() - 2);
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("found " + resultDecl);
        }
        return (FunctionDecl) resultDecl;
    }

    private int nbArgsOf(FunctionTypeSpec fts) {
        return fts == null || fts.argumentsTypes == null ? -1 : fts.argumentsTypes.length;
    }

    /**
     * @param name                function name
     * @param returnType          function return type
     * @param argumentsTypes      function arguments types
     * @param currentSymbolTable  contains the symbole table in which we're searching
     * @param resultFunctionDecls contains the functions that match (null at the begining)
     * @param checkArgsTypes      true iff the arguments' types matter
     * @param top                 if true, do not continue searching in enclosing SymbolTables
     * @param checkArgsNumber     true iff the number of argument matters
     * @param isBottomUpAnalysis  indicates if we have to search among currentSymbolTable's parent or children;
     * @param checkClassName      true iff the class name matters
     * @return all possible methods/functions that match the name and the type given in arguments
     * In Fortran/C, this method returns at most 1 "functionDecl". In C++, could be methods overloaded
     * in some inherited classes.
     */
    private TapList<FunctionDecl> getAllPossibleTypedFunctionDecl(String name, TypeSpec returnType,
                                                                  TypeSpec[] argumentsTypes, SymbolTable currentSymbolTable, TapList<FunctionDecl> resultFunctionDecls,
                                                                  boolean checkArgsTypes, boolean top, boolean checkArgsNumber, boolean isBottomUpAnalysis, String className, boolean checkClassName) {

        String searchedName = name;
        int index = hashKey(name);
        TapList<SymbolDecl> toBucket;
        SymbolDecl symbolDecl;

        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("getAllPossibleTypedFunctionDecl of " + name + " "
                    + new FunctionTypeSpec((WrapperTypeSpec) returnType, (WrapperTypeSpec[]) argumentsTypes) +
                    " in " + currentSymbolTable.addressChain());
            TapEnv.setTraceIndent(TapEnv.traceIndent() + 2);
        }

        // Iterate on the nested enclosing SymbolTable's:
        while (currentSymbolTable != null) {
            // In C/C++, symbolTable is always "case dependent".
            if (!currentSymbolTable.caseDependent) {
                searchedName = name.toLowerCase();
                index = hashKey(searchedName);
            }
            toBucket = currentSymbolTable.buckets[index];
            if (toBucket != null) {
                /* iterate into this level bucket: */
                while (toBucket.tail != null) {
                    symbolDecl = toBucket.tail.head;
                    // if we are on a function/method with the same name:
                    if (symbolDecl.symbol.equals(searchedName) && symbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                        boolean matches = true;
                        if (checkClassName) {
                            matches = className == null ? symbolDecl.className == null : className.equals(symbolDecl.className);
                        }
                        if (matches && checkArgsNumber) {
                            matches = ((FunctionDecl) symbolDecl).functionTypeSpec() != null &&
                                    ((FunctionDecl) symbolDecl).functionTypeSpec().argumentsTypes.length == argumentsTypes.length;
                        }
                        if (matches && checkArgsTypes) {
                            if (TapEnv.traceTypeCheckAnalysis()) {
                                TapEnv.indentOnTrace(TapEnv.traceIndent());
                                TapEnv.printlnOnTrace("Testing matching function declaration " + symbolDecl + " of type "
                                        + ((FunctionDecl) symbolDecl).functionTypeSpec() + " against call "
                                        + new FunctionTypeSpec((WrapperTypeSpec) returnType,
                                        (WrapperTypeSpec[]) argumentsTypes));
                            }
                            matches = ((FunctionDecl) symbolDecl).declarationMatchesCall(returnType, argumentsTypes);
                            if (TapEnv.traceTypeCheckAnalysis()) {
                                TapEnv.indentOnTrace(TapEnv.traceIndent());
                                TapEnv.printlnOnTrace("--> returns " + matches);
                            }
                        }
                        if (matches) {
                            resultFunctionDecls = new TapList<>((FunctionDecl) symbolDecl, resultFunctionDecls);
                        }
                    }
                    toBucket = toBucket.tail;
                }
            }
            if (top) {
                // if recursion in enclosing SymbolTables is forbidden:
                currentSymbolTable = null;
            } else if (resultFunctionDecls != null && this.unit != null && this.unit.isFortran()) {
                // In Fortran, there is no overloading, so if we find one match in the current SymbolTable,
                // we must not look further cause we might find "default" externals(cf F90:v442):
                currentSymbolTable = null;
            } else if (isBottomUpAnalysis) {
                currentSymbolTable = currentSymbolTable.basisSymbolTable();
            } else {
                TapList<Unit> childrenClasses = currentSymbolTable.unit == null ? null : currentSymbolTable.unit.childrenClasses;
                while (childrenClasses != null) {
                    resultFunctionDecls = getAllPossibleTypedFunctionDecl(name, returnType, argumentsTypes, childrenClasses.head.publicSymbolTable(),
                            resultFunctionDecls, checkArgsTypes, false, checkArgsNumber, false, className, checkClassName);
                    resultFunctionDecls = getAllPossibleTypedFunctionDecl(name, returnType, argumentsTypes, childrenClasses.head.protectedSymbolTable(),
                            resultFunctionDecls, checkArgsTypes, false, checkArgsNumber, false, className, checkClassName);
                    childrenClasses = childrenClasses.tail;
                }
                currentSymbolTable = null;
            }
        }
        if (resultFunctionDecls != null && resultFunctionDecls.tail != null) {
            // cas des varFunction avec InterfaceUnit
            while (resultFunctionDecls.head.isInterface() && resultFunctionDecls.tail != null) {
                resultFunctionDecls = resultFunctionDecls.tail;
            }
        }


        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.setTraceIndent(TapEnv.traceIndent() - 2);
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("getAllPossibleTypedFunctionDecl returns " + resultFunctionDecls);
        }

        return resultFunctionDecls;
    }

    /**
     * Searches symbol "name" in this SymbolTable.
     * Searches and returns the declaration of symbol "name"
     * as a "kind", that may be TYPE, VARIABLE, FUNCTION,
     * or SYMBOL(meaning any kind).
     * The search is performed only in the top level of this SymbolTable
     * when "top" is true. At all levels otherwise.
     *
     * @return the SymbolDecl when found, null otherwise.
     */
    public SymbolDecl getDecl(String name, int kind1, int kind2,
                              boolean top, ToObject<SymbolTable> toST) {
        TapList<SymbolDecl> toBucket = getToDecl(name, kind1, kind2, top, toST);
        if (toBucket != null) {
            return toBucket.tail.head;
        } else {
            return null;
        }
    }

    /**
     * Searches symbol "name" in this SymbolTable.
     * Searches and returns the declaration of symbol "name"
     * as a "kind", that may be TYPE, VARIABLE, FUNCTION,
     * or SYMBOL(meaning any kind).
     * The search is performed only in the top level of this SymbolTable
     * when "top" is true. At all levels otherwise.
     *
     * @return the SymbolDecl when found, null otherwise.
     */
    public SymbolDecl getDecl(String name, int kind1, int kind2, int kind3,
                              boolean top) {
        TapList<SymbolDecl> toBucket = getToDecl(name, kind1, kind2, kind3, top, null);
        if (toBucket != null) {
            return toBucket.tail.head;
        } else {
            return null;
        }
    }

    /**
     * Removes an entry of this SymbolTable.
     */
    public void removeDecl(String name, int kind, boolean top) {
        TapList<SymbolDecl> toBucket = getToDecl(name, kind, top, null);
        if (toBucket != null) {
            toBucket.tail = toBucket.tail.tail;
        }
    }

    /**
     * If toST is provided non-null, fills its head with the definition SymbolTable.
     */
    protected TapList<SymbolDecl> getToDecl(String name, int kind, boolean top, ToObject<SymbolTable> toST) {
        String searchedName = name;
        int index = hashKey(name);
        SymbolTable curSymbolTable = this;
        boolean found = false;
        TapList<SymbolDecl> toBucket = null;
        SymbolDecl symbolDecl;
        // iterate on the nested enclosing SymbolTable's
        while (!found && curSymbolTable != null) {
            if (!curSymbolTable.caseDependent) {
                searchedName = name.toLowerCase();
                index = hashKey(searchedName);
            }
            toBucket = curSymbolTable.buckets[index];
            if (toBucket != null) {
                // iterate into this level bucket:
                while (!found && toBucket.tail != null) {
                    symbolDecl = toBucket.tail.head;
                    if (symbolDecl.symbol.equals(searchedName)
                        && (symbolDecl.isA(kind)
                            //TEMPORARY HACK UNTIL ModuleDecl's exist !
                            || (kind == SymbolTableConstants.MODULE && symbolDecl.isA(SymbolTableConstants.FUNCTION)
                                && ((FunctionDecl) symbolDecl).unit() != null
                                && (((FunctionDecl) symbolDecl).unit().isModule()
                                    || ((FunctionDecl) symbolDecl).unit().isUndefined()))
                            //Hack to discard FUNCTIONs that are interfaces or forwards:
                            || (kind == SymbolTableConstants.FUNCTION_NOT_INTERFACE && symbolDecl.isA(SymbolTableConstants.FUNCTION)
                                && ((FunctionDecl) symbolDecl).unit() != null
                                && !((FunctionDecl) symbolDecl).unit().isInterface())
                            || (kind == SymbolTableConstants.FUNCTION_NOT_EXTERNAL_NOR_INTERFACE
                                && symbolDecl.isA(SymbolTableConstants.FUNCTION)
                                && ((FunctionDecl) symbolDecl).unit() != null
                                && !((FunctionDecl) symbolDecl).unit().isInterface()
                                && !((FunctionDecl) symbolDecl).unit().isExternal()))
                            ) {
                        // if we have found the searched symbolDecl:
                        found = true;
                        if (toST != null) {
                            toST.setObj(curSymbolTable);
                        }
                    } else {
                        toBucket = toBucket.tail;
                    }
                }
            }
            if (!found) {
                toBucket = null;
                if (top) {
                    // if recursion in enclosing SymbolTables is forbidden:
                    curSymbolTable = null;
                } else {
                    curSymbolTable = curSymbolTable.basisSymbolTable();
                }
            }
        }
        return toBucket;
    }

    protected TapList<SymbolDecl> getToDecl(String name, int kind1, int kind2,
                                          boolean top, ToObject<SymbolTable> toST) {
        String searchedName = name;
        int index = hashKey(name);
        SymbolTable curSymbolTable = this;
        boolean found = false;
        TapList<SymbolDecl> toBucket = null;
        SymbolDecl symbolDecl;
        /*iterate on the nested enclosing SymbolTable's */
        while (!found && curSymbolTable != null) {
            if (!curSymbolTable.caseDependent) {
                searchedName = name.toLowerCase();
                index = hashKey(searchedName);
            }
            toBucket = curSymbolTable.buckets[index];
            if (toBucket != null) {
                /*iterate into this level bucket: */
                while (!found && toBucket.tail != null) {
                    symbolDecl = toBucket.tail.head;
                    if ((symbolDecl.isA(kind1) || symbolDecl.isA(kind2)) &&
                            symbolDecl.symbol.equals(searchedName)) {
                        /*if we have found the searched symbolDecl: */
                        found = true;
                        if (toST != null) {
                            toST.setObj(curSymbolTable);
                        }
                    } else {
                        toBucket = toBucket.tail;
                    }
                }
            }
            if (!found) {
                toBucket = null;
                if (top) {
                    // if recursion in enclosing SymbolTables is forbidden:
                    curSymbolTable = null;
                } else {
                    curSymbolTable = curSymbolTable.basisSymbolTable();
                }
            }
        }
        return toBucket;
    }

    protected TapList<SymbolDecl> getToDecl(String name, int kind1, int kind2, int kind3,
                                          boolean top, ToObject<SymbolTable> toST) {
        String searchedName = name;
        int index = hashKey(name);
        SymbolTable curSymbolTable = this;
        boolean found = false;
        TapList<SymbolDecl> toBucket = null;
        SymbolDecl symbolDecl;
        // iterate on the nested enclosing SymbolTable's
        while (!found && curSymbolTable != null) {
            if (!curSymbolTable.caseDependent) {
                searchedName = name.toLowerCase();
                index = hashKey(searchedName);
            }
            toBucket = curSymbolTable.buckets[index];
            if (toBucket != null) {
                // iterate into this level bucket:
                while (!found && toBucket.tail != null) {
                    symbolDecl = toBucket.tail.head;
                    if ((symbolDecl.isA(kind1) || symbolDecl.isA(kind2) ||
                            symbolDecl.isA(kind3)) &&
                            symbolDecl.symbol.equals(searchedName)) {
                        // if we have found the searched symbolDecl:
                        found = true;
                        if (toST != null) {
                            toST.setObj(curSymbolTable);
                        }
                    } else {
                        toBucket = toBucket.tail;
                    }
                }
            }
            if (!found) {
                toBucket = null;
                if (top) {
                    // if recursion in enclosing SymbolTables is forbidden:
                    curSymbolTable = null;
                } else {
                    curSymbolTable = curSymbolTable.basisSymbolTable();
                }
            }
        }
        return toBucket;
    }

    /**
     * Plainly inserts the given "symbolDecl" into this SymbolTable.
     */
    protected void addNewSymbolDecl(SymbolDecl symbolDecl) {
        buckets[hashKey(symbolDecl.symbol)].placdl(symbolDecl);
    }

    /**
     * Inserts the given "symbolDecl" into this SymbolTable.
     * Inserts it at the current top level of the SymbolTable,
     * except for special case of Fortran that lets one declare
     * local variable mixed with formal params and commons.
     * Checks for double declaration. Combines declarations
     * when done in multiple steps (e.g. type then  dimension).
     */
    public void addSymbolDecl(SymbolDecl symbolDecl) {
        if (!caseDependent) {
            symbolDecl.symbol = symbolDecl.symbol.toLowerCase();
        }
        String symbolName = symbolDecl.symbol;
        // Attention: special Fortran SymbolTable declaration management:
        // declarations of variables that are formal parameters may
        // be encountered in the local declarations part: These
        // declarations must be added into the publicSymbolTable instead,
        // and recursively needed declarations must be moved up to
        // the publicSymbolTable.
        TapList<SymbolDecl> toOldSymbolDecl = getToDecl(symbolName, SymbolTableConstants.SYMBOL, true, null);
        TapList<SymbolDecl> targetBucket;
        if (isFormalParam(symbolName, toOldSymbolDecl)) {
            // if this (partly) declares a formal parameter, insertion must
            // be done into basisSymbolTable, which is the publicSymbolTable.
            // Also necessary declarations must be moved up to there:
            moveSymbolDeclListUpRec(symbolDecl.dependsOn(),
                    basisSymbolTable);
            symbolDecl.recomputeDependsOn(basisSymbolTable);
            targetBucket = basisSymbolTable.buckets[hashKey(symbolName)];
            toOldSymbolDecl =
                    basisSymbolTable.getToDecl(symbolName, SymbolTableConstants.SYMBOL, true, null);
            toOldSymbolDecl.tail.head.setInstruction(symbolDecl);
            // function declaration:
            if (unit.name().equals(symbolName)) {
                FunctionTypeSpec functionTypeSpec = unit.functionTypeSpec();
                WrapperTypeSpec returnType = functionTypeSpec.returnType;
                if (returnType.wrappedType == null) {
                    functionTypeSpec.returnType =
                            symbolDecl.type().combineWith(returnType, this);
                }
            }
        } else {
            targetBucket = buckets[hashKey(symbolName)];
        }
        // Now do the standard insertion of this new SymbolDecl.
        // When the symbol is not already present, insert it.
        // Otherwise, try and combine the new declaration
        // into the existing one. When combining returns false,
        // this means the new declaration is independent from the
        // old one, and therefore must/can be inserted.
        if (toOldSymbolDecl == null || !combinesWith(toOldSymbolDecl, symbolDecl)) {
            targetBucket.placdl(symbolDecl);
        }
    }

    private boolean isFormalParam(String symbolName,
                                  TapList<SymbolDecl> toOldSymbolDecl) {
        if (toOldSymbolDecl == null && !isFormalParams
                && basisSymbolTable != null && basisSymbolTable.isFormalParams) {
            SymbolDecl symbolDeclAbove = basisSymbolTable.getTopSymbolDecl(symbolName);
            return symbolDeclAbove != null
                    && (symbolDeclAbove.isA(SymbolTableConstants.VARIABLE)
                    || symbolDeclAbove.isA(SymbolTableConstants.CONSTANT)
                    || symbolDeclAbove.isA(SymbolTableConstants.FUNCTION)
                    || symbolDeclAbove.isA(SymbolTableConstants.INTERFACE));
        } else {
            return false;
        }
    }

    //[llh] TODO: only finds if the root var is ALLOCATABLE.
    // should be refined to find if patternTree is a field of
    // a structure, which is declared allocatable.

    /**
     * Tries to combine two SymbolDecl's that speak of the same
     * symbol name. Tries to combine newSymbolDecl with and into
     * the next SymbolDecl in toTargetSymbolDecl.
     * If combination is possible, performs it in place
     * into toTargetSymbolDecl, and returns true ;
     * When combination is not possible, and the newSymbolDecl
     * must be added independently (by the calling routine)
     * returns false. Otherwise complains and returns true, therefore
     * meaning that the newSymbolDecl is incompatible and
     * must be ignored. The calling routine, when receiving
     * true, does nothing, and thus new decl is ignored.
     */
    private boolean combinesWith(
            TapList<SymbolDecl> toTargetSymbolDecl, SymbolDecl newSymbolDecl) {
        SymbolDecl oldSymbolDecl = toTargetSymbolDecl.tail.head;
        boolean oldIsExtern = oldSymbolDecl.isExtern();
        boolean newIsExtern = newSymbolDecl.isExtern();
        if (oldSymbolDecl.isExtern() && !newSymbolDecl.isExtern()) {
            oldSymbolDecl.setExtraInfo(TapList.cleanExtraInfoValue(oldSymbolDecl.extraInfo(), "extern"));
        } else if (newSymbolDecl.isExtern() && !oldSymbolDecl.isExtern()) {
            newSymbolDecl.setExtraInfo(TapList.cleanExtraInfoValue(newSymbolDecl.extraInfo(), "extern"));
        }
        newSymbolDecl.addExtraInfo(oldSymbolDecl.extraInfo());
        oldSymbolDecl.setExtraInfo(newSymbolDecl.extraInfo());
        String symbol = newSymbolDecl.symbol;
        if (newSymbolDecl.isA(SymbolTableConstants.VARIABLE) && (newSymbolDecl.type() == null
                || newSymbolDecl.type().wrappedType == null)) {
            if (newSymbolDecl.isPrivate() || newSymbolDecl.isPublic()) {
                return true;
            }
        }
        boolean oldIsInterface = ((oldSymbolDecl.isA(SymbolTableConstants.INTERFACE)
                                   && ((InterfaceDecl)oldSymbolDecl).functionNames==null)
                                  || (oldSymbolDecl.isA(SymbolTableConstants.FUNCTION)
                                      && (((FunctionDecl) oldSymbolDecl).isInterface()))) ;
        boolean newIsInterface = ((newSymbolDecl.isA(SymbolTableConstants.INTERFACE)
                                   && ((InterfaceDecl)newSymbolDecl).functionNames==null)
                                  || (newSymbolDecl.isA(SymbolTableConstants.FUNCTION)
                                      && (((FunctionDecl) newSymbolDecl).isInterface()))) ;
        if (oldSymbolDecl.isA(SymbolTableConstants.VARIABLE) || oldSymbolDecl.isA(SymbolTableConstants.CONSTANT)) {
            VariableDecl oldVariableDecl = (VariableDecl) oldSymbolDecl;
            if (newSymbolDecl.isA(SymbolTableConstants.VARIABLE) || newSymbolDecl.isA(SymbolTableConstants.CONSTANT)) {
                VariableDecl newVariableDecl = (VariableDecl) newSymbolDecl;
                WrapperTypeSpec sumTypeSpec =
                        oldVariableDecl.type().combineWith(newVariableDecl.type(), this);
                if (sumTypeSpec == null) {
                    if (!((unit == null || unit.isC())
                            && TapEnv.inIncludeFile())) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD01) Cannot combine successive declarations of variable " + symbol + ": " + oldVariableDecl.type().showType() + " and " + newVariableDecl.type().showType() + " (ignored new)");
                    }
                } else {
                    if (unit == null && !(oldIsExtern || newIsExtern)
                            || unit != null && unit.isC() && oldVariableDecl.type() == null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD06) Multiple definition of " + newSymbolDecl.symbol);
                    }
                    oldVariableDecl.setType(sumTypeSpec);
                    oldVariableDecl.dependsOn =
                            TapList.prependNoDups(newSymbolDecl.dependsOn(), oldVariableDecl.dependsOn());
                }
                if (newSymbolDecl.isA(SymbolTableConstants.CONSTANT) && !oldSymbolDecl.isA(SymbolTableConstants.CONSTANT)) {
                    ((VariableDecl) oldSymbolDecl).setConstant();
                }
                if (oldVariableDecl.isReturnVarFromFunction != null) {
                    oldVariableDecl.isReturnVarFromFunction.functionTypeSpec().returnType = oldVariableDecl.isReturnVarFromFunction.
                            functionTypeSpec().returnType.combineWith(newVariableDecl.type(), this);
                }
                newSymbolDecl.setInstruction(oldSymbolDecl);
                return true;
            } else if (newIsInterface) {
                TapList<String> extraInfo = oldSymbolDecl.extraInfo();
                newSymbolDecl.addExtraInfo(extraInfo);
                //Dirty because of DUPLICATE representations for interface decls...
                if (newSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                    ((FunctionDecl) newSymbolDecl).formalArgRank =
                        ((VariableDecl) oldSymbolDecl).formalArgRank;
                } else {
                    ((InterfaceDecl) newSymbolDecl).formalArgRank =
                        ((VariableDecl) oldSymbolDecl).formalArgRank;
                }
                toTargetSymbolDecl.tail.head = newSymbolDecl;
                newSymbolDecl.setInstruction(oldSymbolDecl);
                return true;
            } else if (newSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                Unit newUnit = ((FunctionDecl) newSymbolDecl).unit();
                WrapperTypeSpec sumTypeSpec = null;
                if (newUnit.functionTypeSpec() != null) {
                    sumTypeSpec = oldVariableDecl.type().combineWith(newUnit.functionTypeSpec().returnType, this);
                }
                if (sumTypeSpec == null) {
                    if (newUnit.functionTypeSpec() != null) {
                        // si c'est une variableDecl avec un FunctionTypeSpec
                        // c'est une declaration forward a supprimer
                        if (!TypeSpec.isA(oldVariableDecl.type(), SymbolTableConstants.FUNCTIONTYPE)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD06) Cannot combine successive declarations of function " + symbol + " return type: " + oldVariableDecl.type().showType() + " and " + newUnit.functionTypeSpec().returnType.showType() + " (overwritten previous)");
                        }
                    } else {
                        if (!TypeSpec.isA(oldVariableDecl.type(), SymbolTableConstants.FUNCTIONTYPE)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD06) Cannot combine successive declarations of " + symbol + " of type: " + oldVariableDecl.type().showType() + " and as a module " + newUnit.name() + " (overwritten previous)");
                        }
                    }
                } else {
                    newUnit.functionTypeSpec().returnType = sumTypeSpec;
                    newSymbolDecl.dependsOn =
                            TapList.prependNoDups(newSymbolDecl.dependsOn, oldVariableDecl.dependsOn());
                }
                ((FunctionDecl) newSymbolDecl).formalArgRank =
                        ((VariableDecl) oldSymbolDecl).formalArgRank;
                toTargetSymbolDecl.tail.head = newSymbolDecl;
                newSymbolDecl.setInstruction(oldSymbolDecl);
                return true;
            } else {
                TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(TC23) Symbol " + symbol + ", formerly used as a variable, now used for another object");
                return false;
            }
        } else if (oldIsInterface) {
            if (newIsInterface) {
                if (oldSymbolDecl.isA(SymbolTableConstants.INTERFACE) && newSymbolDecl.isA(SymbolTableConstants.INTERFACE)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD20) Cannot combine successive declarations of interface " + symbol + " (ignored new)");
                }
            } else if (newSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                toTargetSymbolDecl.tail.head = newSymbolDecl;
                newSymbolDecl.setInstruction(oldSymbolDecl);
                return true;
            } else if (isAnAccessDecl(newSymbolDecl)) {
                return true;
            } else {
                TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD19) This combination of two declarations for symbol " + symbol + " is not implemented yet");
            }
            return false;
        } else if (oldSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
            FunctionDecl oldFunctionDecl = (FunctionDecl) oldSymbolDecl;
            Unit oldUnit = oldFunctionDecl.unit();
            if (oldUnit != null && oldUnit.isUndefined()) {
                // Case of existing undefModule and new Module: don't combine => add new decl
                return false;
            }
            if (newIsInterface) {
                return true; // means merging done, since the new interfaceDecl is subsumed by existing old function decl.
            } else if (newSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                Unit newUnit = ((FunctionDecl) newSymbolDecl).unit();
                if (oldUnit == null) {
                    //oldFunctionDecl.setUnit(newUnit) ;
                    oldFunctionDecl.shareUnitFrom((FunctionDecl) newSymbolDecl);
                    newSymbolDecl.setInstruction(oldSymbolDecl);
                    return true;
                } else if (newUnit == null) {
                    ((FunctionDecl) newSymbolDecl).shareUnitFrom(oldFunctionDecl);
                    return true;
                } else if (oldUnit.functionTypeSpec() == null) {
                    oldUnit.setFunctionTypeSpec(newUnit.functionTypeSpec());
                    newSymbolDecl.setInstruction(oldSymbolDecl);
//                     if (newUnit!=oldUnit) getCallGraph().deleteUnit(newUnit) ;
                    return true;
                } else if (newUnit.isModule() || newUnit.isUndefined()) {
                    if (oldUnit.isIntrinsic()) {
                        return false;
                    }
                } else if (oldUnit.functionTypeSpec().argumentsTypes == null) {
                    if (newUnit.functionTypeSpec().argumentsTypes != null) {
                        int nbArgs = newUnit.functionTypeSpec().argumentsTypes.length;
                        WrapperTypeSpec[] newArgTypes = new WrapperTypeSpec[nbArgs];
                        for (int i = nbArgs - 1; i >= 0; i--) {
                            newArgTypes[i] = new WrapperTypeSpec(null);
                        }
                        oldUnit.functionTypeSpec().argumentsTypes = newArgTypes;
                    }
                } else if (newUnit.functionTypeSpec().argumentsTypes == null) {
                    int nbArgs = oldUnit.functionTypeSpec().argumentsTypes.length;
                    WrapperTypeSpec[] newArgTypes = new WrapperTypeSpec[nbArgs];
                    for (int i = nbArgs - 1; i >= 0; i--) {
                        newArgTypes[i] = new WrapperTypeSpec(null);
                    }
                    newUnit.functionTypeSpec().argumentsTypes = newArgTypes;
                }
                if (oldUnit.functionTypeSpec().equalsCompilDep(newUnit.functionTypeSpec())) {
                    if (oldUnit.isStandard() && newUnit.isStandard()) {
                        if (oldUnit != newUnit) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD04) Double definition of procedure " + symbol + " (ignored new)");
                            ((FunctionDecl) newSymbolDecl).deleteUnitIfDifferent(oldFunctionDecl.unit(), getCallGraph());
                        }
                    } else if (oldUnit.isOutside() && newUnit.isStandard()) {
                        //oldFunctionDecl.setUnit(newUnit) ;
                        oldFunctionDecl.deleteUnitIfDifferent(newUnit, getCallGraph());
                        oldFunctionDecl.shareUnitFrom((FunctionDecl) newSymbolDecl);
                    } else {
                        ((FunctionDecl) newSymbolDecl).deleteUnitIfDifferent(oldFunctionDecl.unit(), getCallGraph());
                    }
                } else if (oldUnit.isExternal()) {
                    if (MPIcallInfo.isMessagePassingFunction(symbol, oldUnit.language())) {
                        // on ne fusionne pas les differentes declarations des functions mpi_xxx
                        // definies dans F77GeneralLib
                        return false;
                    } else {
                        // Si oldUnit est EXTERNAL OU INTERFACE et newUnit est STANDARD, on doit
                        // fusionner les deux uniquement si la nouvelle unit est correctement analysee:
                        //[llh 15/5/09]: now keep the interface Unit even after the true Unit is found.???
                        TapEnv.printlnOnTrace(25, "SETP 3 prevDecl " + oldFunctionDecl + " unit:" + newUnit);
                        getCallGraph().setPreviousFunctionDeclAndNewUnit(oldFunctionDecl, newUnit);
                    }
                } else {
                    if (oldUnit.isIntrinsic() &&
                            (newUnit.isOutsideTBD() || newUnit.isIntrinsic() || newUnit.isStandard())) {
                        if (newUnit.isStandard()) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD09) Incorrect use of intrinsic procedure name: " + symbol);
                        }
                        return false;
                    }
                    if (oldUnit.isStandard() && newUnit.isIntrinsic()) {
                        // on garde la oldUnit standard
                        newSymbolDecl.setInstruction(oldSymbolDecl);
                        return false;
                    }
                    // on peut avoir une unit de meme nom qu'une varFunction:
                    if (oldUnit.isVarFunction()) {
                        if (newUnit.isInterface() || newUnit.isExternal()) {
                            newSymbolDecl.setInstruction(oldSymbolDecl);
                            return false;
                        } else {
                            return false;
                        }
                    }
                    if (newUnit.isVarFunction()) {
                        if (oldUnit.isInterface()) {
                            newSymbolDecl.setInstruction(oldSymbolDecl);
                            return true;
                        } else {
                            return false;
                        }
                    }
                    if (!oldUnit.isInterface()) {
                        if (!newUnit.isInterface() && !newUnit.isIntrinsic() && !newUnit.isOutsideTBD() && oldUnit.upperLevelUnit() == newUnit.upperLevelUnit()) {
                            if (oldUnit.language() != newUnit.language()) {
                                return false;
                            } else if (oldUnit.language() == TapEnv.CPLUSPLUS) {
                                return false;
                            } else {
                                if (TapEnv.get().combineDeclMessagesSwitch) {
                                    // Inhibit these messages if importing C standard includes:
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD05) Cannot combine successive declarations of procedure " + symbol + " (ignored new)");
                                }
                                //return false;
                            }
                        }
                    } else {
                        return false;
                    }
                }
                newSymbolDecl.setInstruction(oldSymbolDecl); // Useless since newSymbolDecl will be discarded ? (return true)
                return true;
            } else {
                // cas external f suivi de real f
                if (newSymbolDecl.isA(SymbolTableConstants.VARIABLE)
                        && oldUnit.functionTypeSpec() != null
                        && (oldUnit.functionTypeSpec().returnType == null ||
                        oldUnit.functionTypeSpec().returnType.receivesNoInferenceNoVector(newSymbolDecl.type()))
                        && oldUnit.functionTypeSpec().returnType.equalsCompilIndep(newSymbolDecl.type())
                ) {
                    if (!TypeSpec.isA(newSymbolDecl.type(), SymbolTableConstants.FUNCTIONTYPE)) {
                        oldUnit.functionTypeSpec().returnType = newSymbolDecl.type();
                    }
                    newSymbolDecl.setInstruction(oldSymbolDecl);
                    return true;
                } else {
                    // pb C v90
                    if (!oldUnit.isExternal() && newSymbolDecl.isA(SymbolTableConstants.VARIABLE)
                            && !TypeSpec.isA(newSymbolDecl.type(), SymbolTableConstants.FUNCTIONTYPE)) {
                        if (!isAnAccessDecl(newSymbolDecl)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(DD06) Cannot combine successive declarations of function " + oldSymbolDecl.symbol + " return type: " + oldUnit.functionTypeSpec().returnType.showType() + " and " + newSymbolDecl + " (ignored new)");
                        }
                    }
                    newSymbolDecl.setInstruction(oldSymbolDecl);
                    return true;
                }
            }
        } else if (oldSymbolDecl.isA(SymbolTableConstants.TYPE)) {
            if (newSymbolDecl.isA(SymbolTableConstants.TYPE)) {
                boolean compares = ((TypeDecl)oldSymbolDecl).typeSpec.receives(((TypeDecl)newSymbolDecl).typeSpec, null, null) ;
                if (compares) {
                    oldSymbolDecl.setInstruction(newSymbolDecl);
                    return true ;
                } else {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD05) Cannot combine successive declarations of type " + symbol + " (ignored new)");
                    return false ;
                }
            } else {
                TapEnv.fileWarning(TapEnv.MSG_WARN, newSymbolDecl.defPositions.head, "(DD05) Cannot combine existing declaration of type " + symbol + " with new "+newSymbolDecl+" (ignored new)");
                return false ;
            }
        } else {
            // Don't complain about a new declaration of a procedure with
            //  the same name as an existing type or class or interface_moduleProcedure:
            if (!((oldSymbolDecl.isA(SymbolTableConstants.TYPE)
                   || oldSymbolDecl.isA(SymbolTableConstants.CLASS)
                   || (oldSymbolDecl.isA(SymbolTableConstants.INTERFACE)
                       && ((InterfaceDecl)oldSymbolDecl).functionNames!=null))
                  && newSymbolDecl.isA(SymbolTableConstants.FUNCTION))) {
                if (isAnAccessDecl(newSymbolDecl)) {
                    return true;
                } else {
                    TapEnv.toolWarning(-1, "(DD19) This combination of two declarations for symbol " + symbol + " is not implemented yet" + " old:" + oldSymbolDecl + " new:" + newSymbolDecl);
                }
            }
            return false;
        }
    }

    private boolean isAnAccessDecl(SymbolDecl newSymbolDecl) {
        return (newSymbolDecl.type() == null || newSymbolDecl.type().wrappedType == null)
                && (newSymbolDecl.isPrivate() || newSymbolDecl.isPublic());
    }

    public NameSpaceDecl addNameSpaceDecl(String name, SymbolTable nameSpaceSymbolTable) {
        NameSpaceDecl nameSpaceDecl = new NameSpaceDecl(name, nameSpaceSymbolTable);
        addSymbolDecl(nameSpaceDecl);
        return nameSpaceDecl;
    }

    /**
     * Creates and inserts the SymbolDecl coming from the variable declaration Tree "decl".
     */
    public TapList<WrapperTypeSpec> addVarDeclaration(Tree decl, Instruction instrDecl,
                                                      boolean isATrueVariable, ToBool addInstruction) {
        TapList<WrapperTypeSpec> paramTypeSpecList = new TapList<>(null, null);
        TapList<WrapperTypeSpec> paramsTail = paramTypeSpecList;
        WrapperTypeSpec lhsTypeSpec, copiedLhsTypeSpec;
        WrapperTypeSpec typeSpec;
        TapList<SymbolDecl> toTypeUsedSymbols = new TapList<>(null, null);
        TapList<String> toInfos = new TapList<>(null, null);
        TapList<String> toAccess = new TapList<>(null, null);
        ToBool isPointer = new ToBool(false);
        int declarationKind = SymbolTableConstants.VARIABLE;
        boolean declarationIsPrivate = false;
        boolean declarationIsExtern;
        Tree modifiers = decl.down(1);
        if (!ILUtils.isNullOrNone(modifiers)) {
            Tree[] modifiersList = modifiers.children();
            for (Tree tree : modifiersList) {
                String modifierString = ILUtils.getIdentString(tree);
                if (modifierString != null) {
                    toInfos.placdl(modifierString);
                }
            }
        }
        if (toInfos.tail != null && "privateEnumElement".equals(toInfos.tail.head) && decl.down(2).opCode() == ILLang.op_none) {
            // ^ The above tests detect the special case of the declaration of the constants that compose an enum type.
            declarationIsPrivate = true;
            toInfos.tail = null;
        }
        Tree typeTree = decl.down(2);
        // Exit more or less gracefuly after C2IL parsing error cf set11/vmp18. TODO: fix C parser
        if (typeTree.opCode() == ILLang.op_declarators) {
            TapEnv.tapenadeExit(1);
        }
        if (typeTree.opCode() == ILLang.op_none) {
            // When we are on a list of formal params with no type, they must not share
            // their future type. Example: function f(a,b)... int a...real b...end.
            // Other Example: dimension[20] : U,V ... real U... int V...
            lhsTypeSpec = null;
        } else {
            TapList<SymbolDecl> newSymbolDecls = null;
            if (typeTree.opCode() == ILLang.op_referenceType &&
                    typeTree.down(1).opCode() == ILLang.op_modifiedType) {
                newSymbolDecls = declareAllSymbolsIn(typeTree.down(1),
                        typeTree.down(1), null, null, SymbolTableConstants.VARIABLE,
                        true, false, instrDecl);
            } else if (typeTree.opCode() == ILLang.op_modifiedType) {
                newSymbolDecls = declareAllSymbolsIn(typeTree,
                        typeTree, null, null, SymbolTableConstants.VARIABLE,
                        true, false, instrDecl);
            }
            toTypeUsedSymbols = new TapList<>(null, newSymbolDecls);
            SymbolDecl.addUsedSymbolsInExpr(typeTree, toTypeUsedSymbols, this, null, false, true);
            lhsTypeSpec = TypeSpec.build(typeTree, this, instrDecl,
                    toTypeUsedSymbols, toInfos, toAccess, isPointer, null);
            declarationKind = analyzeModifiers(toInfos.tail);
            if (TypeSpec.isA(lhsTypeSpec, SymbolTableConstants.FUNCTIONTYPE) && !isPointer.get()) {
                declarationKind = SymbolTableConstants.FUNCTION ;
            }
            if (TypeSpec.isA(lhsTypeSpec, SymbolTableConstants.COMPOSITETYPE)) {
                TypeDecl definedTypeDecl = null;
                String typeSpecName = ILUtils.buildTypeName(typeTree);
                if (typeSpecName != null) {
                    definedTypeDecl = getTypeDecl(typeSpecName);
                }
                String typeDeclName = null;
                if (definedTypeDecl == null) {
                    typeDeclName = ILUtils.buildTypeName(typeTree);
                    if (typeDeclName != null) {
                        definedTypeDecl = getTypeDecl(typeDeclName);
                    }
                }
                if (definedTypeDecl == null) {
                    if (typeDeclName != null) {
                        definedTypeDecl = new TypeDecl(typeDeclName, lhsTypeSpec);
                        addSymbolDecl(definedTypeDecl);
                        definedTypeDecl.setInstruction(instrDecl);
                    }
                }
            }
            if (unit != null && toInfos.tail == null) {
                if (unit.fortranStuff() != null && unit.fortranStuff().publicPrivateDefault != null) {
                    toInfos.tail = new TapList<>(unit.fortranStuff().publicPrivateDefault, null);
                } else if (!unit.privacy.isEmpty()) {
                    toInfos.tail = new TapList<>(unit.privacy, null);
                }
            }
        }
        declarationIsPrivate = declarationIsPrivate ||
                (unit == null || unit.isTranslationUnit())
                        &&
                        (TapList.containsEquals(toAccess.tail, "private")
                                || TapList.containsEquals(toInfos.tail, "private"));
        declarationIsExtern = TapList.containsEquals(toInfos.tail, "extern");

        SymbolTable hostSymbolTable;
        SymbolTable sharingSymbolTable;
        if (unit != null && !unit.isTranslationUnit() && !declarationIsExtern) {
            // if this is a declaration of a (local or argument) variable within a code Unit (procedure, module, class...),
            // then host SymbolTable is this enclosing Unit's SymbolTable, which is "this" SymbolTable, and there's no sharing needed.
            // The symbolDecl may have to be moved up later according to its "privacy" in the containing class.
            hostSymbolTable = this;
            sharingSymbolTable = null;
        } else if (TapEnv.currentTranslationUnitSymbolTable() != null && declarationIsPrivate) {
            // else we are close to global level, i.e. outside any code Unit: if there is a TranslationUnitSymbolTable
            // (i.e. we are parsing a file which is a package) and this symbol must remain local to this package,
            // then host SymbolTable is this package's SymbolTable, and there's no sharing needed:
            hostSymbolTable = this;
            sharingSymbolTable = null;
        } else if (instrDecl != null && instrDecl.isInStdCInclude()) {
            // else this declaration is contained in a standard C include, and we may want to put it directly
            // at the C language level (to save space?) although this might be incorrect if some variables are static?
            hostSymbolTable = getCallGraph().cRootSymbolTable();
            sharingSymbolTable = null;
        } else if (TapEnv.currentTranslationUnitSymbolTable() != null) {
            // else we are close to global level, this file is a package, but this symbol is global (to this language)
            // then host SymbolTable is this package's SymbolTable, and this symbol is shared at the language's SymbolTable level:
            hostSymbolTable = this;
            sharingSymbolTable = getCallGraph().languageRootSymbolTable(this.language);
        } else {
            // else there is no notion of TranslationUnit for this language (this case may disappear in the future)
            // and by default we choose that the symbol is directly hosted by the language's SymbolTable level,
            // and there's no sharing needed.
            hostSymbolTable = this; // "this" is the current language's SymbolTable level.
            sharingSymbolTable = null;
        }
        Tree[] declarators = decl.down(3).children();
        boolean containsOpNone = false;
        ToBool isAReference = new ToBool() ;
        // special case of a varDeclaration that specifies only the type -> implicitly 1 variable:
        if (declarators.length == 0) {
            paramsTail = paramsTail.placdl(lhsTypeSpec);
        }
        for (int i = 0; i < declarators.length; i++) {
            copiedLhsTypeSpec = lhsTypeSpec == null || i + 1 == declarators.length ? lhsTypeSpec : (WrapperTypeSpec) lhsTypeSpec.copyStopOnComposite(null);
            Tree declaratorI = declarators[i];
            int opCode = declaratorI.opCode();
            if (opCode == ILLang.op_star) {
                paramsTail = paramsTail.placdl(new WrapperTypeSpec(new LabelTypeSpec()));
            } else {
                Tree initTree;
                if (opCode == ILLang.op_assign || opCode == ILLang.op_pointerAssign) {
                    declaratorI = declarators[i].down(1);
                    initTree = declarators[i];
                } else {
                    declaratorI = declarators[i];
                    initTree = null;
                }
                TapPair<Tree, WrapperTypeSpec> declaratorAndType = new TapPair<>(declaratorI, copiedLhsTypeSpec);
                this.peelDeclaratorToIdent(declaratorAndType, instrDecl, new TapList<>(null, null), isAReference);
                String symbolName = ILUtils.baseName(declaratorAndType.first);
                WrapperTypeSpec symbolTypeSpec = declaratorAndType.second;
                // [llh] TODO: whenever possible, use symbolTypeSpec rather than this typeSpec
                // cf next comment on next SymbolDecl.build()
                typeSpec = copiedLhsTypeSpec;

                SymbolDecl symbolDecl = null;
                int symbolDeclKind;
                FunctionDecl functionDecl = null;
                TapList<FunctionDecl> functionDecls;
                VariableDecl paramDecl = null;

                if (typeSpec != null && (opCode == ILLang.op_ident || opCode == ILLang.op_metavar)) {
                    // patch: in Fortran, when typeSpec==null, this means we are on
                    // the function's parameter list or a dimension.
                    // We are then sure that this name is not a function's name

                    //[bd] The following code specifies the return type of a previously declared function.
                    // Even for object, there can be only one such prototype.

                    functionDecls = getTopFunctionDecl(symbolName, typeSpec, null, false);
                    // WARNING: SHOULD DEAL WITH ALL FUNCTIONS:
                    functionDecl = functionDecls == null ? null : functionDecls.head;
                    // Special case of functions passed as argument: the symbol can be
                    // in the formal parameters, and it can be declared as a function.
                    if (functionDecl == null) {
                        SymbolTable formalSymbolTable = this;
                        while (formalSymbolTable != null && !formalSymbolTable.isFormalParams) {
                            formalSymbolTable = formalSymbolTable.basisSymbolTable;
                        }
                        if (formalSymbolTable != null) {
                            functionDecls = formalSymbolTable.getTopFunctionDecl(symbolName, typeSpec, null, false);
                            functionDecl = functionDecls == null ? null : functionDecls.head;
                            if (functionDecl == null) {
                                paramDecl = formalSymbolTable.getTopVariableDecl(symbolName);
                            }
                        }
                    }
                }
                if (functionDecl != null) {
                    functionDecl.setInstruction(instrDecl);
                    if (functionDecl.isExternal() || functionDecl.isIntrinsic() || functionDecl.isVarFunction()) {
                        // If this declaration concerns an external function:
                        if (isPointer.get()) {
                            typeSpec = new WrapperTypeSpec(new PointerTypeSpec(typeSpec, null));
                        }
                        if (functionDecl.functionTypeSpec() == null) {
                            functionDecl.unit().setFunctionTypeSpec(new FunctionTypeSpec(typeSpec));
                        } else {
                            functionDecl.functionTypeSpec().setReturnTypeSpec(typeSpec, declaratorI);
                        }
                    }
                }

                if ((!TypeSpec.isA(symbolTypeSpec, SymbolTableConstants.FUNCTIONTYPE)
                     || declaratorI.opCode() != ILLang.op_functionDeclarator)
                    && (declarationKind == SymbolTableConstants.VARIABLE
                        || declarationKind == SymbolTableConstants.CONSTANT)) {
                    // [llh] TODO: this SymbolDecl.build seems redundant with previous peelDeclaratorToIdent()
                    // cf above comment typeSpec vs symbolTypeSpec.
                    symbolDecl = SymbolDecl.build(
                                     (typeSpec==null ? new WrapperTypeSpec(null) : typeSpec),
                                     toAccess.tail, isPointer.get(),
                                     declaratorI, declarationKind, this, instrDecl, toTypeUsedSymbols.tail);
                } else {
                    // This is a FUNCTION declaration: find out if there is already a global declaration,
                    // in which case we will reuse its Unit instead of creating a new one.
                    SymbolTable curSymbolTable = this;
                    TapList<FunctionDecl> globalFunctionDecls = null;
                    while (curSymbolTable != null && globalFunctionDecls == null) {
                        globalFunctionDecls = curSymbolTable.getTopFunctionDecl(symbolName, null, null, null, false);
                        if (curSymbolTable.isFormalParams) {
                            paramDecl = curSymbolTable.getTopVariableDecl(symbolName);
                        }
                        // If this is a formal params level and symbolName is a formal argument, keep it and don't go upper.
                        // Also if declaration is private, don't look outside the file's SymbolTable:
                        if (paramDecl != null || declarationIsPrivate && curSymbolTable.isTranslationUnitSymbolTable()) {
                            curSymbolTable = null;
                        } else {
                            curSymbolTable = curSymbolTable.basisSymbolTable();
                        }
                    }
                    // WARNING: SHOULD ALSO TREAT THE OTHER FUNCTIONS:
                    FunctionDecl globalFunctionDecl = globalFunctionDecls == null ? null : globalFunctionDecls.head;
                    Unit globalUnit;
                    if (globalFunctionDecl != null) {
                        globalUnit = globalFunctionDecl.unit();
                    } else {
                        if (paramDecl != null) {
                            globalUnit = Unit.makeVarFunction(symbolName, unit);
                        } else {
                            addExternalOrForwardDeclaration(declaratorI, instrDecl, declarationIsPrivate);
                            if (declarationIsPrivate) {
                                globalFunctionDecls = this.
                                        getTopFunctionDecl(symbolName, null, null, null, false);
                            } else {
                                SymbolTable langSymbolTable =
                                    this.getCallGraph().languageRootSymbolTable(TapEnv.relatedLanguage()) ;
                                globalFunctionDecls = null ;
                                while (globalFunctionDecls == null && langSymbolTable!=null) {
                                    globalFunctionDecls =
                                        langSymbolTable.getTopFunctionDecl(symbolName, null, null, null, false);
                                    langSymbolTable = langSymbolTable.basisSymbolTable ;
                                }
                            }
                            globalFunctionDecl = globalFunctionDecls.head;
                            globalUnit = globalFunctionDecl.unit();
                        }
                        if (TapEnv.inIncludeFile() && TapEnv.isStdCInclude(TapEnv.currentIncludeInstruction())) {
                            globalUnit.setInStdCIncludeFile();
                        }
                        FunctionTypeSpec functionType;
                        if (TypeSpec.isA(symbolTypeSpec, SymbolTableConstants.FUNCTIONTYPE)) {
                            functionType = (FunctionTypeSpec) symbolTypeSpec.wrappedType;
                        } else {
                            functionType = new FunctionTypeSpec(symbolTypeSpec);
                        }
                        globalUnit.setFunctionTypeSpec(functionType);
                    }
                    if (unit == globalUnit && unit.isC()) {
                        // ignore header of the current unit in the current unit, cf set11/vpc06:
                        symbolDeclKind = SymbolTableConstants.FUNCTIONTYPE;
                        TapEnv.fileWarning(TapEnv.MSG_WARN, decl, "(DD08) Procedure header ignored: " + unit.name());
                        if (TapEnv.currentIncludeInstruction() != null) {
                            TapEnv.get().expandAllIncludeFile = true;
                        }
                        addInstruction.set(false);
                    } else {
                        symbolDecl = new FunctionDecl(ILUtils.baseTree(declaratorI), globalUnit);
                        symbolDecl.addExtraInfo(toAccess.tail);
                    }
                }
                boolean isExtern;
                if (symbolDecl != null) {
                    symbolDecl.addExtraInfo(toInfos.tail);
                    symbolDeclKind = symbolDecl.kind;
                    if (isAReference.get()) symbolDecl.setReference() ;
                    isExtern = symbolDecl.isExtern();

                    if (symbolDecl.hasModifier("value")) {
                        unit.setLanguageAndUp(TapEnv.FORTRAN2003);
                    }
                    SymbolDecl oldSymbolDecl = hostSymbolTable.getDecl(symbolName, symbolDeclKind, true);
                    boolean oldIsExtern = oldSymbolDecl != null && oldSymbolDecl.isExtern();
                    symbolDecl.setInstruction(instrDecl);
                    hostSymbolTable.addSymbolDecl(symbolDecl);
                    if (oldSymbolDecl != null && oldSymbolDecl != symbolDecl && !(isExtern || oldIsExtern)) {
                        oldSymbolDecl.setInstruction(instrDecl);
                    }

                    // ?? We need a comment here to explain/motivate what follows!
                    if (this.basisSymbolTable == null && !isExtern && oldSymbolDecl != null && !oldIsExtern) {
                        decl.down(3).cutChild(i + 1);
                        opCode = ILLang.op_none;
                        containsOpNone = true;
                    }
                    // Accumulate symbolDecl's type into the list of arguments types:
                    paramsTail = paramsTail.placdl(symbolDeclKind == SymbolTableConstants.FUNCTION ?
                            ((FunctionDecl) symbolDecl).type()
                            : symbolDecl.type());

                    // Set the extraInfo, "isATrueSymbolDecl", and initial value for a VARIABLE declaration:
                    if (symbolDeclKind == SymbolTableConstants.CONSTANT || symbolDeclKind == SymbolTableConstants.VARIABLE) {
                        VariableDecl variableDecl = (VariableDecl) symbolDecl;
                        variableDecl.addExtraInfo(symbolDecl.extraInfo());
                        variableDecl.isATrueSymbolDecl = isATrueVariable && !TapEnv.inStdCIncludeFile();
                        if (initTree != null) {
                            if (unit == null || unit.isFortran() || unit.isC() && variableDecl.isCconst()) {
                                // c'est une constante ou une declaration globale en c
                                if (variableDecl.initializationTree() != null) {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, decl, "(DD06) Multiple definition of " + variableDecl.symbol + "=" + ILUtils.toString(variableDecl.initializationTree()) + " " + ILUtils.toString(initTree));
                                }
                                variableDecl.setInitializationTree(initTree);
                                // seulement si le type est int:
                                if (variableDecl.type().isIntegerBase() || ILUtils.seemsInteger(initTree)) {
                                    variableDecl.constantValue = computeIntConstant(initTree.down(2));
                                }
                            }
                            if (!WrapperTypeSpec.isFunctionOrPointerToFunction(variableDecl.type())) {
                                // Don't search for used symbols in e.g.
                                //     void (*func)(int, float) = defaultFunc;
                                // because that will eventually declare defaultFunc as a variable instead of a function.
                                TapList<SymbolDecl> newSymbolDecls = declareAllSymbolsIn(initTree.down(2), initTree.down(2),
                                        null, null, SymbolTableConstants.VARIABLE, true, false, instrDecl);
                                variableDecl.dependsOn =
                                        TapList.prependNoDups(newSymbolDecls, variableDecl.dependsOn);
                            }
                            if (unit != null
                                    && unit.isFortran()
                                    && !TapList.containsEquals(variableDecl.extraInfo(), "constant")
                                    && variableDecl.kind != SymbolTableConstants.CONSTANT
                                    && !isFormalParam(symbolName, getToDecl(symbolName, SymbolTableConstants.SYMBOL, true, null))
                                    && !unit.isModule()
                                    && this == unit.privateSymbolTable()) {
                                unit.fortranStuff().computeSaveVarDecl(this, variableDecl);
                                // pb on le perd sur la variable differentiee
                                variableDecl.addExtraInfo(new TapList<>("save", null));
                            }
                        }
                        if (TapList.containsEquals(variableDecl.extraInfo(), "save") && !unit.isC()) {
                            unit.fortranStuff().computeSaveVarDecl(this, variableDecl);
                        }
                        if (unit != null && unit.isC()
                                && isFormalParam(symbolName, getToDecl(symbolName, SymbolTableConstants.SYMBOL, true, null))) {
                            ILUtils.removeTreeDeclOf(symbolName, instrDecl.tree);
                        }
                    }

                    // Organize sharing of global declarations (e.g. "extern")
                    //  via a shared copy SymbolDecl in the language-root SymbolTable:
                    // TODO: check if this mechanism is still needed now that we use the "ZoneInfoAccessElements" mechanism.
                    if (sharingSymbolTable != null) {
                        symbolDecl.setShared();
                        if (symbolDeclKind == SymbolTableConstants.VARIABLE || symbolDeclKind == SymbolTableConstants.CONSTANT) {
                            VariableDecl variableDecl = (VariableDecl) symbolDecl;
                            VariableDecl sharedDecl = (VariableDecl) sharingSymbolTable.getDecl(symbolName, SymbolTableConstants.VARIABLE, SymbolTableConstants.CONSTANT, true, null);
                            if (isExtern && variableDecl.getInitializationTree() != null) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, decl,
                                        "(DD06) " + variableDecl.symbol + " initialized and declared extern");
                            }
                            if (sharedDecl == null) {
                                // insert a copy with shared info into sharingSymbolTable:
                                sharedDecl = new VariableDecl(ILUtils.build(ILLang.op_ident, symbolName), variableDecl.type());
                                sharedDecl.setInitializationTree(variableDecl.getInitializationTree());
                                sharedDecl.constantValue = variableDecl.constantValue;
                                sharedDecl.adoptZones(variableDecl);
                                sharedDecl.isATrueSymbolDecl = variableDecl.isATrueSymbolDecl;
                                sharingSymbolTable.addSymbolDecl(sharedDecl);
                                if (isExtern) {
                                    sharedDecl.addExtraInfo(new TapList<>("extern", null));
                                }
                            } else {
                                // retrieve shared info from the shared copy from sharingSymbolTable:
                                if (variableDecl.isATrueSymbolDecl && !(sharedDecl.isExtern() || variableDecl.isExtern() || isExtern)) {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, variableDecl.defPositions.head, "(DD06) Multiple definition of " +
                                            variableDecl.symbol);
                                }
                                variableDecl.adoptZones(sharedDecl);
                                if (!isExtern) {
                                    sharedDecl.setType(variableDecl.type());
                                }
                                if (sharedDecl.isExtern() && !isExtern) {
                                    sharedDecl.setExtraInfo(TapList.cleanExtraInfoValue(sharedDecl.extraInfo(), "extern"));
                                }
                                if (sharedDecl.constantValue != null) {
                                    variableDecl.constantValue = sharedDecl.constantValue;
                                }
                            }
                            // In the present implementation, if sharingSymbolTable!=null, it is the global SymbolTable of C.
                            // Store the link between variableDecl and sharedDecl, so that zone allocation will
                            // give them the same zones and will manage property HIDDEN just like for Fortran COMMONs.
                            TapList<TapList<ZoneInfoAccessElements>> maps = getCallGraph().globalCMaps ;
                            TapList<ZoneInfoAccessElements> thisDeclMap = getCallGraph().getCglobalMap(sharedDecl) ;
                            ZoneInfoAccessElements oneElem ;
                            if (thisDeclMap==null) {
                                oneElem = new ZoneInfoAccessElements(sharedDecl, declaratorAndType.first, sharingSymbolTable) ;
                                thisDeclMap = new TapList<>(oneElem, null) ;
                                maps = new TapList<>(thisDeclMap, maps) ;
                            }
                            oneElem = new ZoneInfoAccessElements(variableDecl, declaratorAndType.first, hostSymbolTable) ;
                            thisDeclMap.tail = new TapList<>(oneElem, thisDeclMap.tail) ;
                            getCallGraph().globalCMaps = maps ;
                        } else if (symbolDeclKind == SymbolTableConstants.FUNCTION) {
                            Unit functionUnit = ((FunctionDecl) symbolDecl).unit();
                            TapList<FunctionDecl> existingDecls = sharingSymbolTable.getTopFunctionDecl(symbolName, null, null, null, false);
                            FunctionDecl sharedDecl = existingDecls == null ? null : existingDecls.head;
                            if (sharedDecl == null) {
                                if (functionUnit == null) {
                                    functionUnit = buildExternalUnit(symbolName, false);
                                    ((FunctionDecl) symbolDecl).setUnit(functionUnit);
                                }
                                // insert a copy with shared info into sharingSymbolTable:
                                sharedDecl = new FunctionDecl(ILUtils.build(ILLang.op_ident, symbolName), functionUnit);
                                sharedDecl.shareUnitFrom((FunctionDecl) symbolDecl);
                                sharedDecl.setZones(symbolDecl.zones());
                                sharedDecl.isATrueSymbolDecl = symbolDecl.isATrueSymbolDecl;
                                sharingSymbolTable.addSymbolDecl(sharedDecl);
                            } else {
                                // retrieve shared info from the shared copy from sharingSymbolTable:
                                if (functionUnit != null && functionUnit != sharedDecl.unit()) {
                                    Unit mergedUnit = sharedDecl.unit();
                                    mergedUnit.absorb(functionUnit);
                                    // Maybe absorb() should care more about the "kind" of the 2 merged Units ?
                                    sharedDecl.setUnit(mergedUnit);
                                    getCallGraph().deleteUnit(functionUnit);
                                }
                                ((FunctionDecl) symbolDecl).shareUnitFrom(sharedDecl);
                            }
                        }
                    }

                    // Merge an existing declaration ("funVarDecl") of this symbolName as a variable of "pointer-to-function" type
                    // with the present declaration ("varFunDecl") as a function.
                    // If initial value(s) are here assigned to this function, collect them into varFunDecl.
                    if (isAFunctionDeclarator(declaratorI)
                            // Don't do this when this is a C definition
                            // of a variable that contains a pointer to function(s).
                        && !(symbolDecl.isA(SymbolTableConstants.VARIABLE) &&
                             (TypeSpec.isA(symbolDecl.type(), SymbolTableConstants.POINTERTYPE) ||
                              TypeSpec.isA(symbolDecl.type(), SymbolTableConstants.ARRAYTYPE)))) {
                        VariableDecl funVarDecl = getVariableDecl(symbolName);
                        FunctionDecl varFunDecl =
                                hostSymbolTable.addFunctionDeclarator(decl, typeSpec, declarators[i],
                                        this, toTypeUsedSymbols.tail, instrDecl, declarationIsPrivate);
                        if (varFunDecl != null) {
                            if (varFunDecl.isVarFunction() && funVarDecl != null && TypeSpec.isA(funVarDecl.type(), SymbolTableConstants.POINTERTYPE)) {
                                varFunDecl.unit().setFunctionTypeSpec(
                                        (FunctionTypeSpec) ((PointerTypeSpec) funVarDecl.type().wrappedType).destinationType.wrappedType);
                            }
                            if (opCode == ILLang.op_assign) {
                                Tree[] initializers;
                                if (initTree.down(2).opCode() == ILLang.op_multiCast) {
                                    initializers = initTree.down(2).down(2).children();
                                } else if (initTree.down(2).opCode() == ILLang.op_arrayConstructor) {
                                    initializers = initTree.down(2).children();
                                } else {
                                    initializers = new Tree[]{initTree.down(2)};
                                }
                                varFunDecl.initFunctionDecls = new FunctionDecl[initializers.length];
                                for (int j = 0; j < initializers.length; ++j) {
                                    String initName = ILUtils.baseName(initializers[j]);
                                    FunctionDecl initFD = null;
                                    if (initName != null) {
                                        TapList<FunctionDecl> initFDs = getFunctionDecl(initName, null, null, false);
                                        initFD = initFDs == null ? null : initFDs.head;
                                    }
                                    varFunDecl.initFunctionDecls[j] = initFD;
                                    if (initFD == null) {
                                        TapEnv.fileWarning(TapEnv.MSG_WARN, decl, "(TC94) Incorrect initialization of function " + symbolName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (containsOpNone) {
            int k = decl.down(3).length();
            while (k > 0) {
                if (decl.down(3).down(k).opCode() == ILLang.op_none) {
                    decl.down(3).removeChild(k);
                }
                --k;
            }
            if (addInstruction != null && decl.down(3).length() == 0) {
                addInstruction.set(false);
            }
        }
        return paramTypeSpecList.tail;
    }

    public SymbolDecl getSymbolDecl(Tree tree) {
        SymbolDecl result = null;
        switch (tree.opCode()) {
            case ILLang.op_fieldAccess:
                WrapperTypeSpec rootTypeSpec = this.typeOf(tree.down(1));
                if (rootTypeSpec.isArray()) {//elemental use of fieldAccess
                    rootTypeSpec = rootTypeSpec.elementType() ;
                }
                int fieldRank = ILUtils.getFieldRank(tree.down(2));
                if (fieldRank != -1) {
                    result = ((CompositeTypeSpec) rootTypeSpec.wrappedType).fields[fieldRank];
                } else {
                    TapEnv.toolError("getSymbolDecl " + ILUtils.toString(tree));
                }
                break;
            case ILLang.op_arrayAccess:
                break;
            case ILLang.op_pointerAccess:
                result = getSymbolDecl(tree.down(1));
                break;
            case ILLang.op_ident:
                String baseName = ILUtils.baseName(tree);
                result = this.getVariableDecl(baseName);
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * @param assignedVar tree containing a Variable.
     * @param modifier    String "allocatable", "target", ...
     * @return true if the corresponding Variable has a modifier equals to
     * the modifier parameter.
     */
    public boolean varHasModifier(Tree assignedVar, String modifier) {
        boolean result = false;
        SymbolDecl symbolDecl = getSymbolDecl(assignedVar);
        if (symbolDecl != null) {
            result = symbolDecl.hasModifier(modifier);
        } else {
            NewSymbolHolder newSH = NewSymbolHolder.getNewSymbolHolder(assignedVar);
            if (newSH != null && newSH.newVariableDecl() != null) {
                result = newSH.newVariableDecl().hasModifier(modifier);
            }
        }
        return result;
    }

    public boolean isAllocatableAndTarget(Tree assignedVar) {
        return varHasModifier(assignedVar, "allocatable")
                && varHasModifier(assignedVar, "target");
    }

    private boolean isAFunctionDeclarator(Tree declarator) {
        int opCode = declarator.opCode();
        return opCode == ILLang.op_functionDeclarator
                || opCode == ILLang.op_pointerDeclarator
                && isAFunctionDeclarator(declarator.down(1))
                || opCode == ILLang.op_assign
                && isAFunctionDeclarator(declarator.down(1));
    }

    /**
     * @return true if modifiers define a FunctionDecl and not a VariableDecl.
     */
    private int analyzeModifiers(TapList<String> modifiers) {
        if (TapList.containsEquals(modifiers, "external")) {
            return SymbolTableConstants.FUNCTION;
        } else if (TapList.containsEquals(modifiers, "constant")) {
            return SymbolTableConstants.CONSTANT;
        } else {
            return SymbolTableConstants.VARIABLE;
        }
    }

    /**
     * Peel the given Tree and in parallel grow its type, until the tree is an op_ident.
     */
    private void peelDeclaratorToIdent(TapPair<Tree, WrapperTypeSpec> declaratorAndType,
                                       Instruction instrDecl, TapList<SymbolDecl> toTypeUsedSymbols,
                                       ToBool isAReference) {
        Tree declarator = declaratorAndType.first;
        WrapperTypeSpec typeSpec = declaratorAndType.second;
        isAReference.set(false) ;
        while (declarator != null) {
            switch (declarator.opCode()) {
                case ILLang.op_pointerDeclarator:
                    typeSpec = new WrapperTypeSpec(new PointerTypeSpec(typeSpec, null));
                    declarator = declarator.down(1);
                    break;
                case ILLang.op_functionDeclarator: {
                    WrapperTypeSpec[] argumentsTypes;
                    ToBool isVariableArgList = new ToBool(false);
                    // [llh 11Sept2017] it seems declarators F(void) or F() mean unspecified number of args (cf C:v163)
                    // params Tree = op_none or (op_void,...) ==> unspecified number of args
                    //             = op_argDeclarations[]     ==> unspecified number of args
                    //             = op_argDeclarations[...]  ==> N arguments
                    if (ILUtils.isNullOrNoneOrEmptyList(declarator.down(2)) ||
                            declarator.down(2).down(1) != null && declarator.down(2).down(1).opCode() == ILLang.op_void) {
                        argumentsTypes = null;
                    } else {
                        TapList<WrapperTypeSpec> argList =
                                SymbolDecl.buildTypeSpecFromArgs(this, instrDecl, toTypeUsedSymbols,
                                        declarator.down(2).children(), isVariableArgList);
                        argumentsTypes = new WrapperTypeSpec[TapList.length(argList)];
                        for (int i = TapList.length(argList) - 1; i >= 0; --i) {
                            argumentsTypes[i] = argList.head;
                            argList = argList.tail;
                        }
                    }
                    FunctionTypeSpec newFunctionTypeSpec =
                            new FunctionTypeSpec(typeSpec, argumentsTypes, isVariableArgList.get());
                    typeSpec = new WrapperTypeSpec(newFunctionTypeSpec);
                    declarator = declarator.down(1);
                    break;
                }
                case ILLang.op_arrayDeclarator: {
                    Tree[] dimDecls = declarator.down(2).children();
                    ArrayDim[] dims = new ArrayDim[dimDecls.length];
                    String identNext = ILUtils.getIdentString(declarator.down(1));
                    for (int i = dimDecls.length - 1; i >= 0; --i) {
                        dims[i] = new ArrayDim(dimDecls[i], null, null, (identNext==null ? null : declarator.down(1)), i + 1, dimDecls.length, 1);
                    }
                    typeSpec = new WrapperTypeSpec(new ArrayTypeSpec(typeSpec, dims));
                    declarator = declarator.down(1);
                    break;
                }
                case ILLang.op_modifiedDeclarator: {
                    TapPair<Tree, ArrayDim[]> sizeAndDims = new TapPair<>(null, null);
                    TapList<String> toSymbolDeclInfos = new TapList<>(null, null);
                    TapList<String> toSymbolDeclAccess = new TapList<>(null, null);
                    ToBool isPointer = new ToBool(false);
                    int typeSign = TypeSpec.dispatchModifiers(declarator.down(1).children(), declarator.down(2), sizeAndDims,
                            isPointer, toSymbolDeclInfos, toSymbolDeclAccess, toTypeUsedSymbols,
                            declarator, instrDecl, this);
                    // sizeAndDims.first contains the sizeTypeModifier Tree, and .second contains the ArrayDim[] dimensions.
                    // TODO: so far, we don't use the modifier infos returned in "isPointer", "toSymbolDeclAccess", "toSymbolDeclInfos".
                    if (sizeAndDims.first != null || typeSign != 0) {
                        ModifiedTypeSpec modifiedType = new ModifiedTypeSpec(typeSpec, sizeAndDims.first, this);
                        modifiedType.typeSign = typeSign;
                        typeSpec = new WrapperTypeSpec(modifiedType);
                    }
                    if (sizeAndDims.second != null) {
                        typeSpec = new WrapperTypeSpec(new ArrayTypeSpec(typeSpec, sizeAndDims.second));
                    }
                    declarator = declarator.down(2);
                    break;
                }
                case ILLang.op_sizedDeclarator:
                    typeSpec = new WrapperTypeSpec(new ModifiedTypeSpec(typeSpec, declarator.down(2), this));
                    declarator = declarator.down(1);
                    break;
                case ILLang.op_referenceDeclarator:
                    isAReference.set(true) ;
                    declarator = declarator.down(1);
                    break ;
                case ILLang.op_none:
                case ILLang.op_metavar:
                case ILLang.op_ident:
                    declaratorAndType.first = declarator;
                    declaratorAndType.second = typeSpec;
                    declarator = null;
                    break;
                default:
                    TapEnv.toolWarning(10, "(Analyze function declarator) Unexpected operator:" + declarator.opName());
                    declaratorAndType.first = ILUtils.baseTree(declarator);
                    declaratorAndType.second = typeSpec;
                    declarator = null;
            }
        }
    }

    private FunctionDecl addFunctionDeclarator(Tree decl, WrapperTypeSpec typeSpec, Tree declaratorI,
                                               SymbolTable refSymbolTable, TapList<SymbolDecl> typeUsedSymbols,
                                               Instruction instrDecl, boolean isPrivate) {
        FunctionDecl functionDecl = null;
        TapList<FunctionDecl> functionDecls;
        int opCode = declaratorI.opCode();
        Tree declarator = declaratorI;
        if (opCode == ILLang.op_assign) {
            declarator = declarator.down(1);
        }

        TapList<SymbolDecl> toTypeUsedSymbols = new TapList<>(null, typeUsedSymbols);
        TapPair<Tree, WrapperTypeSpec> declaratorAndType = new TapPair<>(declarator, typeSpec);
        refSymbolTable.peelDeclaratorToIdent(declaratorAndType, instrDecl, toTypeUsedSymbols, new ToBool());
        declarator = declaratorAndType.first;
        typeSpec = declaratorAndType.second;

        WrapperTypeSpec[] argumentsTypes;
        WrapperTypeSpec resultType;
        if (TypeSpec.isA(typeSpec, SymbolTableConstants.POINTERTYPE) &&
                TypeSpec.isA(((PointerTypeSpec) typeSpec.wrappedType).destinationType, SymbolTableConstants.FUNCTIONTYPE)) {
            // In C, function variables are declared as "pointer to function"
            FunctionTypeSpec functionType =
                    (FunctionTypeSpec) ((PointerTypeSpec) typeSpec.wrappedType).destinationType.wrappedType;
            argumentsTypes = functionType.argumentsTypes;
            resultType = functionType.returnType;
        } else if (TypeSpec.isA(typeSpec, SymbolTableConstants.FUNCTIONTYPE)) {
            argumentsTypes = ((FunctionTypeSpec) typeSpec.wrappedType).argumentsTypes;
            resultType = ((FunctionTypeSpec) typeSpec.wrappedType).returnType;
        } else {
            argumentsTypes = null;
            resultType = typeSpec;
        }

        functionDecls = getFunctionDecl(declarator.stringValue(), resultType, argumentsTypes, true);
        TapList<FunctionDecl> tmpFunctionDecls = functionDecls;
        while (tmpFunctionDecls != null && functionDecl == null) {
            functionDecl = tmpFunctionDecls.head;
            if (functionDecl.className != null && functionDecl.className.equals(unit.name()) && functionDecl.unit() != null && functionDecl.unit().isUndefined()) {
                Unit functionUnit = functionDecl.unit();
                functionUnit.setUpperLevelUnit(unit);
                functionUnit.publicSymbolTable().setBasisSymbolTable(unit.privateSymbolTable());
                functionUnit.setKind(SymbolTableConstants.FUNCTION);
            } else {
                functionDecl = null;
            }
            tmpFunctionDecls = tmpFunctionDecls.tail;
        }

//         if (functionDecl==null && unit!=null && unit.isClass()) {
//             MethodDecl methodDecl = new MethodDecl(ILUtils.baseName(declarator), METHOD, typeSpec, argumentsTypes, unit, decl.down(1), unit.privacy);
//             addSymbolDecl(methodDecl);
//         }

        // WARNING: SHOULD DEAL WITH ALL FUNCTIONS:
        functionDecl = functionDecls == null ? null : functionDecls.head;
        if (functionDecl != null && functionDecl.unit().isFortran()
                && functionDecl.isIntrinsic()) {
            // cas functionDecl fortran et functionDeclarator C, on garde les 2 declarations:
            functionDecl = null;
        }
        if (functionDecl == null && (unit == null || !unit.isClass())) {
            addExternalOrForwardDeclaration(declarator, instrDecl, isPrivate);
            functionDecls = getFunctionDecl(ILUtils.baseName(declarator), resultType, argumentsTypes, true);
            if (functionDecls == null) {
                // [llh 11Sept2017] Recovery when inconsistent *.h and *Lib files !
                functionDecls = getFunctionDecl(ILUtils.baseName(declarator), resultType, null, false);
            }
            functionDecl = functionDecls.head;
            // If the functionDecl found deals with an already well-built standard Unit,
            // don't do the following because it would damage this Unit.
            // (This might happen in bizarre cases where one defines a
            //  well-built stub to replace an intrinsic or external)
            if (!functionDecl.unit().isStandard()) {
                functionDecl.isATrueSymbolDecl = !TapEnv.inStdCIncludeFile();
                functionDecl.setInstruction(instrDecl);
                SymbolTable newPublicSymbolTable = new SymbolTable(this);
                newPublicSymbolTable.unit = functionDecl.unit();
                newPublicSymbolTable.setCaseDependent(!functionDecl.unit().isFortran());
                newPublicSymbolTable.declareFormalParamsLevel();
                functionDecl.unit().addDerivedSymbolTable(newPublicSymbolTable);
                functionDecl.unit().setPublicSymbolTable(newPublicSymbolTable);
                functionDecl.unit().publicSymbolTable().setShortName("Formal params SymbolTable of external " + declarator);
                functionDecl.declarator = decl;
                if (TapEnv.inIncludeFile() && TapEnv.isStdCInclude(TapEnv.currentIncludeInstruction())) {
                    functionDecl.unit().setInStdCIncludeFile();
                }
            }
        } else if (functionDecl != null && (unit == null || !unit.isCPlusPlus())) {
            FunctionTypeSpec newFunctionTypeSpec = new FunctionTypeSpec(resultType, argumentsTypes);
            boolean forwardDefCorrect =
                    functionDecl.functionTypeSpec().receives(newFunctionTypeSpec, null, null);
            if (!forwardDefCorrect) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, decl, "(DD06) Cannot combine successive declarations of function " + functionDecl.symbol + " " + functionDecl.functionTypeSpec().showType() + " and " + newFunctionTypeSpec.showType() + " (ignored new)");
            }
            // force canReceive on the return type:
            // functionDecl.unit().updateExternalShape() ;
            if (TapEnv.inIncludeFile() && TapEnv.isStdCInclude(TapEnv.currentIncludeInstruction())) {
                functionDecl.unit().setInStdCIncludeFile();
            }
        }
        if (functionDecl != null && opCode == ILLang.op_assign) {
            SymbolDecl.addUsedSymbolsInExpr(declaratorI.down(2), toTypeUsedSymbols, this, null, false, true);
            functionDecl.dependsOn =
                    TapList.prependNoDups(toTypeUsedSymbols.tail, functionDecl.dependsOn);
        }
        return functionDecl;
    }

    /**
     * Creates and inserts the SymbolDecl
     * coming from the variable dimension declaration Tree "decl".
     */
    public void addVarDimDeclaration(Tree decl, Instruction instrDecl) {
        SymbolDecl symbolDecl;
        Tree[] declarators;
        String name;
        VariableDecl varDecl;
        boolean isPointer = false;
        int i;
        TapList<SymbolDecl> newSymbolDecls = null;
        TapList<SymbolDecl> toTypeUsedSymbols;
        declarators = decl.children();
        for (i = 0; i < declarators.length; i++) {
            /*Le type du tableau peut venir apres, donc on n'utilise
             pas les "implicits" tout de suite, mais plus tard,
             pendant la phase "fixImplicits".
             vmp: il faut declarer les variables dans les dimColon */
            if (declarators[i].opCode() == ILLang.op_arrayDeclarator) {
                newSymbolDecls = declareAllSymbolsIn(declarators[i].down(2), declarators[i].down(2), null,
                        null, SymbolTableConstants.VARIABLE, true, true, instrDecl);
            } else {
                TapEnv.fileWarning(TapEnv.MSG_WARN, decl, "(DD30) Incorrect dimension statement");
            }
            toTypeUsedSymbols = new TapList<>(null, newSymbolDecls);
            SymbolDecl.addUsedSymbolsInExpr(decl.down(1), toTypeUsedSymbols, this, null, false, true);
            name = ILUtils.baseName(declarators[i]);
            varDecl = getTopVariableDecl(name);
            if (varDecl != null) {
                isPointer = TypeSpec.isA(varDecl.type(), SymbolTableConstants.POINTERTYPE);
            }
            symbolDecl = SymbolDecl.build(new WrapperTypeSpec(null), null, isPointer,
                    declarators[i], SymbolTableConstants.VARIABLE, this, instrDecl, toTypeUsedSymbols.tail);
            addSymbolDecl(symbolDecl);
            SymbolDecl newSymbolDecl = getDecl(symbolDecl.symbol, symbolDecl.kind, false);
            if (newSymbolDecl != null) {
                newSymbolDecl.setInstruction(instrDecl);
            }
        }
    }

    public void addAllocatableDeclaration(Tree decl, Instruction instrDecl) {
        SymbolDecl symbolDecl;
        Tree[] declarators;
        int i;
        TapList<SymbolDecl> newSymbolDecls = null;
        TapList<SymbolDecl> toTypeUsedSymbols;
        declarators = decl.down(2).children();
        for (i = 0; i < declarators.length; i++) {
            /*Le type du tableau peut venir apres, donc on n'utilise
             pas les "implicits" tout de suite, mais plus tard,
             pendant la phase "fixImplicits".
             vmp: il faut declarer les variables dans les dimColon */
            if (declarators[i].opCode() == ILLang.op_arrayDeclarator) {
                newSymbolDecls = declareAllSymbolsIn(declarators[i].down(2), declarators[i].down(2), null,
                        decl.down(1), SymbolTableConstants.VARIABLE, true, true, instrDecl);
            } else {
                TapEnv.toolWarning(-1, "It is clearer to specify dimensions on the type declaration statements");
            }
            toTypeUsedSymbols = new TapList<>(null, newSymbolDecls);
            SymbolDecl.addUsedSymbolsInExpr(decl.down(1), toTypeUsedSymbols, this, null, false, true);
            symbolDecl = SymbolDecl.build(new WrapperTypeSpec(null), null, false,
                    declarators[i], SymbolTableConstants.VARIABLE, this, instrDecl, toTypeUsedSymbols.tail);
            addSymbolDecl(symbolDecl);
            SymbolDecl newSymbolDecl = getDecl(symbolDecl.symbol, symbolDecl.kind, false);
            if (newSymbolDecl != null) {
                wrapPointerType(decl.down(1), newSymbolDecl);
                newSymbolDecl.setInstruction(instrDecl);
            }
        }
    }

    public void addTypeDeclaration(Tree typeNameTree, Tree typeDef, Instruction instrDecl,
                                   TapList<String> modifiers) {
        // First, prepare the TypeSpec of the type being declared:
        TapList<SymbolDecl> toTypeUsedSymbols = new TapList<>(null, null);
        TapList<String> toInfos = new TapList<>(null, null);
        ToBool isPointer = new ToBool(false);
        WrapperTypeSpec typeSpec =
                TypeSpec.build(typeDef, this, instrDecl,
                        toTypeUsedSymbols, toInfos, new TapList<>(null, null), isPointer, null);
        if (isPointer.get()) {
            typeSpec = new WrapperTypeSpec(new PointerTypeSpec(typeSpec, null));
        }
        if (toInfos.tail == null && unit != null
                && unit.fortranStuff() != null && unit.fortranStuff().publicPrivateDefault != null) {
            toInfos.tail = new TapList<>(unit.fortranStuff().publicPrivateDefault, null);
        }
        toInfos = toInfos.tail;
        if (instrDecl.isInStdCInclude()) {
            TypeSpec declaredType = typeSpec;
            while (declaredType instanceof WrapperTypeSpec) {
                declaredType = ((WrapperTypeSpec) declaredType).wrappedType;
            }
            if (declaredType instanceof CompositeTypeSpec) {
                String compositeName = ((CompositeTypeSpec) declaredType).name;
                if ("struct _IO_FILE".equals(compositeName)
                        || "struct _IO_FILE_plus".equals(compositeName)
                        || "struct _IO_lock_t".equals(compositeName)
                        || "struct _IO_jump_t".equals(compositeName)
                        || "struct _IO_marker".equals(compositeName)
                        // The following and the above should be mutually exclusive, but naming of C structs is still buggy, so we keep both:
                        || "_IO_FILE".equals(compositeName)
                        || "_IO_FILE_plus".equals(compositeName)
                        || "_IO_lock_t".equals(compositeName)
                        || "_IO_jump_t".equals(compositeName)
                        || "_IO_marker".equals(compositeName)
                ) {
                    ((CompositeTypeSpec) declaredType).isAnIOType = true;
                }
            }
        }
        // then insert one TypeDecl with the type name given in the type name declaration (if present):
        String typeName1 = typeNameTree == null ? null : ILUtils.getIdentString(typeNameTree);
        if (typeName1 != null) {
            addOneTypeDeclaration(typeName1, typeDef, instrDecl, typeSpec,
                    toTypeUsedSymbols.tail, toInfos, modifiers);
        }
        // then insert another TypeDecl with the type name given in the type itself (if present, e.g. "struct ttt"):
        String typeName2 = ILUtils.buildTypeName(typeDef);
        // Very special case of an enum with no name: it must exist, just to be the type of its elements!
        if (typeName2 == null && typeName1 == null && typeDef.opCode() == ILLang.op_enumType) {
            typeName2 = "enum null";
        }
        if (typeName2 != null) {
            addOneTypeDeclaration(typeName2, typeDef, instrDecl, typeSpec,
                    toTypeUsedSymbols.tail, toInfos, modifiers);
        }
    }

    private void addOneTypeDeclaration(String typeName, Tree typeDef,
                                       Instruction instrDecl, WrapperTypeSpec typeSpec,
                                       TapList<SymbolDecl> typeUsedSymbols, TapList<String> infos,
                                       TapList<String> modifiers) {
        if (!caseDependent) {
            typeName = typeName.toLowerCase();
        }
        TapList<SymbolDecl> bucket = buckets[hashKey(typeName)];
        TypeDecl oldDecl = getTopTypeDecl(typeName);
        SymbolDecl oldSymbolDecl = getTopSymbolDecl(typeName);
        String name1 = null;
        if (oldDecl == null) {
            name1 = ILUtils.typeName(typeDef);
            if (name1 != null) {
                oldDecl = getTopTypeDecl(name1);
                if (oldSymbolDecl == null) {
                    oldSymbolDecl = getTopSymbolDecl(name1);
                }
            }
        }
        if (oldDecl == null) {
            if (oldSymbolDecl != null) {
                removeDecl(typeName, SymbolTableConstants.SYMBOL, false);   // ???
            }
            TypeDecl typeDecl = new TypeDecl(typeName, typeSpec);
            if (infos != null) {
                typeDecl.addExtraInfo(infos);
            }
            if (modifiers != null && (modifiers.head != null || modifiers.tail != null)) {
                typeDecl.addExtraInfo(modifiers);
            }
            typeDecl.dependsOn =
                    TapList.prependNoDups(typeUsedSymbols, typeDecl.dependsOn());
            typeDecl.isATrueSymbolDecl = !TapEnv.inStdCIncludeFile();
            typeDecl.setInstruction(instrDecl);
            if (oldSymbolDecl != null) {
                typeDecl.addExtraInfo(oldSymbolDecl.extraInfo());
            }
            // rajoute le typeDecl dans la SymbolTable (pourquoi pas addSymbolDecl?)
            bucket.placdl(typeDecl);
        } else if (TypeSpec.isA(oldDecl.typeSpec.wrappedType, SymbolTableConstants.NAMEDTYPE)) {
            TapList<TapPair<Unit, String>> oldNames = oldDecl.typeSpec.wrappedType.typeDeclNames();
            oldDecl.typeSpec.wrappedType = typeSpec.wrappedType;
            oldDecl.typeSpec.wrappedType.setTypeDeclNames(oldNames);
            if (oldDecl.getTypeDeclName() == null) {
                oldDecl.setTypeDeclName(typeName);
            }
            oldDecl.dependsOn =
                    TapList.prependNoDups(typeUsedSymbols, oldDecl.dependsOn());
            oldDecl.isATrueSymbolDecl = !TapEnv.inStdCIncludeFile();
            oldDecl.setInstruction(instrDecl);
            if (oldSymbolDecl != null) {
                oldDecl.addExtraInfo(oldSymbolDecl.extraInfo());
            }
            oldDecl.addExtraInfo(modifiers);
            if (name1 != null) {
                oldDecl.symbol = typeName;
            } else if (unit == null || unit.isC()) {
                TapEnv.fileWarning(TapEnv.MSG_TRACE, typeDef, "(TC47) Definition of type " + typeName + " comes a little too late");
            }
        } else if (TypeSpec.isA(typeSpec, SymbolTableConstants.COMPOSITETYPE)
                && TypeSpec.isA(oldDecl.typeSpec.wrappedType, SymbolTableConstants.COMPOSITETYPE)) {
            if (((CompositeTypeSpec) oldDecl.typeSpec.wrappedType).fields.length == 0) {
                String oldName = oldDecl.typeSpec.wrappedType.typeDeclName();
                TapList<TapPair<Unit, String>> oldNames = oldDecl.typeSpec.wrappedType.typeDeclNames();
                oldDecl.typeSpec.wrappedType = typeSpec.wrappedType;
                oldDecl.typeSpec.wrappedType.setTypeDeclNames(oldNames);
                oldDecl.setTypeDeclName(oldName);
                oldDecl.dependsOn =
                        TapList.prependNoDups(typeUsedSymbols, oldDecl.dependsOn());
                oldDecl.isATrueSymbolDecl = !TapEnv.inStdCIncludeFile();
                oldDecl.setInstruction(instrDecl);
                if (oldSymbolDecl != null) {
                    oldDecl.addExtraInfo(oldSymbolDecl.extraInfo());
                }
                oldDecl.addExtraInfo(modifiers);
            } else {
                if (oldDecl.typeSpec != typeSpec
                        && !((unit == null || unit.isC()) && TapEnv.inIncludeFile())) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, typeDef, "(DD12) Cannot combine successive declarations of type " + typeName + " (ignored new)");
                }
            }
        } else if (TypeSpec.isA(typeSpec, SymbolTableConstants.ENUMTYPE) && TypeSpec.isA(oldDecl.typeSpec.wrappedType, SymbolTableConstants.ENUMTYPE)) {
            if (typeName.equals("enum null")
                    && oldDecl.symbol.equals("enum null")) {
                if (oldSymbolDecl != null) {
                    removeDecl(typeName, SymbolTableConstants.SYMBOL, false);
                }
                TypeDecl typeDecl = new TypeDecl(typeName, typeSpec);
                if (infos != null) {
                    typeDecl.addExtraInfo(infos);
                }
                if (modifiers != null && (modifiers.head != null || modifiers.tail != null)) {
                    typeDecl.addExtraInfo(modifiers);
                }
                typeDecl.dependsOn =
                        TapList.prependNoDups(typeUsedSymbols, typeDecl.dependsOn());
                typeDecl.isATrueSymbolDecl = !TapEnv.inStdCIncludeFile();
                typeDecl.setInstruction(instrDecl);
                if (oldSymbolDecl != null) {
                    typeDecl.addExtraInfo(oldSymbolDecl.extraInfo());
                }
                // rajoute le typeDecl dans la SymbolTable
                bucket.placdl(typeDecl);
            } else {
                if (oldDecl.typeSpec != typeSpec
                        && !((unit == null || unit.isC()) && TapEnv.inIncludeFile())) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, typeDef, "(DD12) Cannot combine successive declarations of type " + typeName + " (ignored new)");
                }
            }
        } else {
            if (!((unit == null || unit.isC()) && TapEnv.inIncludeFile())) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, typeDef, "(DD12) Cannot combine successive declarations of type " + typeName + " (ignored new)");
            }
        }
        // For C, approximate patch so that the "same" type in two different
        // compilation units will be differentiated in the same way.
        // When the current CallGraph has already seen a type with the same name,
        // (and assuming this type is the same as the given "typeSpec")
        // make sure these types share their fields activity info:
        TapList<TapPair<String, TypeSpec>> toSharingTypes = getCallGraph().toSharingTypes;
        TapPair<String, TypeSpec> found = null;
        while (found == null && toSharingTypes.tail != null) {
            if (typeName.equals(toSharingTypes.tail.head.first)) {
                found = toSharingTypes.tail.head;
            }
            toSharingTypes = toSharingTypes.tail;
        }
        if (found == null) {
            toSharingTypes.placdl(new TapPair<String, TypeSpec>(typeName, typeSpec));
        } else {
            typeSpec.shareActiveFields(found.second);
        }
    }

    /**
     * Creates and inserts the new SymbolDecl
     * coming from the constant declaration Tree "decl".
     * or adds the constant declaration into the variableDecl.
     */
    public void addConstDeclaration(Tree decl, Instruction instrDecl) {
        /*This will soon be subsumed by addVarDeclaration ... */
        WrapperTypeSpec typeSpec;
        SymbolDecl symbolDecl;
        Tree[] declarators;
        TapList<SymbolDecl> toTypeUsedSymbols;
        int i;
        declarators = decl.down(2).children();
        VariableDecl oldVariableDecl;
        Tree declarator;
        Tree initTree;
        String varName;
        for (i = 0; i < declarators.length; i++) {
            TapList<String> toInfos = new TapList<>(null, null);
            ToBool isPointer = new ToBool(false);
            toTypeUsedSymbols = new TapList<>(null, null);
            typeSpec = TypeSpec.build(decl.down(1), this, instrDecl,
                    toTypeUsedSymbols, toInfos, new TapList<>(null, null), isPointer, null);
            if (toInfos.tail == null && unit.fortranStuff().publicPrivateDefault != null) {
                toInfos.tail = new TapList<>(unit.fortranStuff().publicPrivateDefault, null);
            }
            if (declarators[i].opCode() == ILLang.op_assign) {
                declarator = declarators[i].down(1);
                initTree = declarators[i];
            } else {
                declarator = declarators[i];
                initTree = null;
            }
            varName = ILUtils.getIdentString(declarator);
            oldVariableDecl = getVariableDecl(varName);
            if (oldVariableDecl == null) {
                symbolDecl = SymbolDecl.build(typeSpec, null, isPointer.get(), declarators[i],
                        SymbolTableConstants.CONSTANT, this, instrDecl,
                        toTypeUsedSymbols.tail);
                symbolDecl.addExtraInfo(toInfos.tail);
                symbolDecl.isATrueSymbolDecl = !TapEnv.inStdCIncludeFile();
                /*if (TapList.containsString(toInfos.tail, "public"))
                 basisSymbolTable.addSymbolDecl(symbolDecl);
                 else
                 addSymbolDecl(symbolDecl);*/
                addSymbolDecl(symbolDecl);
                symbolDecl.setInstruction(instrDecl);
                oldVariableDecl = getConstantDecl(varName);
            } else {
                if (typeSpec.wrappedType == null) {
                    typeSpec = oldVariableDecl.type();
                } else if (oldVariableDecl.type().wrappedType != null) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, declarators[i], "(DD03) Cannot combine successive declarations of constant " + ILUtils.toString(declarator) + " type: " + oldVariableDecl.type().wrappedType.showType() + " and " + typeSpec.wrappedType.showType() + " (ignored new)");
                } else {
                    oldVariableDecl.setType(typeSpec);
                }
                oldVariableDecl.setConstantKind();
                oldVariableDecl.setInstruction(instrDecl);
            }
            /*Set the constant's value: */
            if (initTree != null) {
                assert oldVariableDecl != null;
                oldVariableDecl.setInitializationTree(initTree);
                if (oldVariableDecl.type().isIntegerBase()
                        || ILUtils.seemsInteger(initTree)) {
                    oldVariableDecl.constantValue = computeIntConstant(initTree.down(2));
                }
                SymbolDecl.addUsedSymbolsInExpr(initTree.down(2), toTypeUsedSymbols, this, null, false, true);
                oldVariableDecl.dependsOn =
                        TapList.prependNoDups(toTypeUsedSymbols.tail, oldVariableDecl.dependsOn());
            }
        }
    }

    /**
     * Processes a DATA declaration. Adds it into the list of DATA trees,
     * and declares all variable names found inside "decl" into this SymbolTable
     */
    public void addData(Instruction instr) {
        Tree decl = instr.tree;
        if (dataList == null) {
            dataList = new TapList<>(decl, null);
        } else {
            dataList.newR(decl);
        }
        TapList<String> commonNames = getCallGraph().usedCommonNames;
        declareAllSymbolsIn(decl, decl, commonNames, null, SymbolTableConstants.VARIABLE,
                true, true, instr);

    }

    /**
     * Processes a SAVE declaration. Adds it into the list of SAVE trees,
     * and declares all variable names found inside "decl" into this SymbolTable.
     */
    public void addSave(Instruction instr) {
        Tree decl = instr.tree;
        if (saveList == null) {
            saveList = new TapList<>(decl, null);
        } else {
            saveList.newR(decl);
        }
        if (decl.length() == 0) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, decl, "(DD28) Empty SAVE statement may save extra variables, please use \"save::varname\" ");
        }
        TapList<String> commonNames = getCallGraph().usedCommonNames;
        declareAllSymbolsIn(decl, decl, commonNames, null, SymbolTableConstants.VARIABLE, true, true, instr);
    }

    /**
     * Processes a namelist declaration. Adds it into the list of namelist trees,
     * and declares all variable names found addSymbolDecls inside "decl" into this SymbolTable.
     */
    public void addNameList(Instruction instr) {
        Tree decl = instr.tree;
        if (nameListList == null) {
            nameListList = new TapList<>(decl, null);
        } else {
            nameListList.newR(decl);
        }
        declareAllSymbolsIn(decl, decl, null, null, SymbolTableConstants.VARIABLE, true, false, instr);
    }

    /**
     * Searches inside "decl" for all used identifiers that are not yet declared
     * in this SymbolTable (at this level or above). Each missing SymbolDecl
     * is created and then either inserted in this SymbolTable or returned in
     * the result, depending on the value of "delayedDeclaration".
     *
     * @param decl               the declaration expression in which symbols must be searched.
     * @param topDeclTree        the "decl" passed to the initial top recursive call.
     * @param commonNames        collected name of enclosing common ??
     *                           ([llh] not sure it is used correctly?)
     * @param modifier           collected modifier to be applied to identifiers found.
     *                           ([llh] not sure it is used correctly?)
     * @param symbolKind         the kind of declaration "decl" e.g. VARIABLE,
     * @param delayedDeclaration true when new SymbolDecl's must be
     *                           returned instead of inserted.
     * @param noFunctionCall     true when the expression "decl" is in a place that guarantees
     *                           it cannot be a function call (e.g. left of assign, overwritten...)
     * @param instr              the Instruction that contains the given expression "decl".
     * @return the list of missing SymbolDecl's that have been found but not
     * yet inserted because delayedDeclaration is true.
     */
    public TapList<SymbolDecl> declareAllSymbolsIn(Tree decl,
                                                   Tree topDeclTree, TapList<String> commonNames, Tree modifier, int symbolKind,
                                                   boolean delayedDeclaration, boolean noFunctionCall, Instruction instr) {
        TapList<SymbolDecl> newAddSymbolDecls = null;
        switch (decl.opCode()) {
            case ILLang.op_do:
            case ILLang.op_arrayConstructor:
            case ILLang.op_multiCast:
            case ILLang.op_dimColons:
            case ILLang.op_varDeclarators:
            case ILLang.op_expressions: {
                Tree[] listOfVar = decl.children();
                for (Tree tree : listOfVar) {
                    newAddSymbolDecls = TapList.append(newAddSymbolDecls,
                            declareAllSymbolsIn(tree, tree,
                                    commonNames, modifier, symbolKind, delayedDeclaration, noFunctionCall, instr));
                }
                break;
            }
            case ILLang.op_modifiers: {
                Tree[] listOfVar = decl.children();
                for (Tree tree : listOfVar) {
                    if (tree.opCode() == ILLang.op_dimColons) {
                        newAddSymbolDecls = TapList.append(newAddSymbolDecls,
                                declareAllSymbolsIn(tree, tree,
                                        commonNames, modifier, symbolKind,
                                        delayedDeclaration, false, instr));
                    }
                }
                break;
            }
            case ILLang.op_save: {
                Tree[] listOfVar = decl.children();
                for (Tree tree : listOfVar) {
                    newAddSymbolDecls = TapList.append(newAddSymbolDecls,
                            declareAllSymbolsIn(tree, tree,
                                    commonNames, modifier, symbolKind, delayedDeclaration, true, instr));
                }
//                 unit.fortranStuff().addSave(this, instr);
                if (listOfVar.length == 0) {
                    unit.fortranStuff().setSaveAll();
                }
                break;
            }
            case ILLang.op_nameList: {
                Tree[] listOfVar = decl.down(2).children();
                for (Tree tree : listOfVar) {
                    newAddSymbolDecls = TapList.append(newAddSymbolDecls,
                            declareAllSymbolsIn(tree, tree,
                                    commonNames, modifier, symbolKind, delayedDeclaration, noFunctionCall, instr));
                }
                break;
            }
            case ILLang.op_arrayAccess:
                // C'est peut-etre un arrayAccess qui devrait etre un call
                // mais c'est TROP TOT pour le dire: il peut rester des USE non resolus
                newAddSymbolDecls = declareAllSymbolsIn(decl.down(2), decl.down(2),
                        commonNames, modifier, symbolKind, delayedDeclaration, false, instr);
                break;
            case ILLang.op_data:
                newAddSymbolDecls = declareAllSymbolsIn(decl.down(1), topDeclTree, commonNames,
                        modifier, symbolKind, delayedDeclaration, true, instr);
//                 unit.fortranStuff().addSave(this, instr);
                break;
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_modifiedType:
                newAddSymbolDecls = declareAllSymbolsIn(decl.down(1), topDeclTree, commonNames,
                        modifier, symbolKind, delayedDeclaration, true, instr);
                break;
            case ILLang.op_fieldAccess:
                newAddSymbolDecls = declareAllSymbolsIn(decl.down(1), decl.down(1), commonNames,
                        modifier, symbolKind, delayedDeclaration, false, instr);
                break;
            case ILLang.op_arrayDeclarator: {
                newAddSymbolDecls = declareAllSymbolsIn(decl.down(1), decl.down(1),
                        commonNames, modifier, symbolKind, delayedDeclaration, true, instr);
                SymbolDecl symbolDecl = SymbolDecl.build(new WrapperTypeSpec(null), null, false,
                        decl, SymbolTableConstants.VARIABLE, this, instr, newAddSymbolDecls);
                addSymbolDecl(symbolDecl);
                newAddSymbolDecls = TapList.append(newAddSymbolDecls,
                        declareAllSymbolsIn(decl.down(2), decl.down(2),
                                commonNames, null, symbolKind, delayedDeclaration, false, instr));
                break;
            }
            case ILLang.op_assign:
            case ILLang.op_pointerAssign:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_power:
            case ILLang.op_concat:
            case ILLang.op_functionType:
            case ILLang.op_lt:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_ge:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_dimColon:
                if (delayedDeclaration
                        && decl.down(1).opCode() == ILLang.op_none
                        && decl.down(2).opCode() == ILLang.op_none
                        && isOverloadedOperator(decl.operator())) {
                    // operator overloading, il faut declarer le symbolDecl:
                    Tree interfaceTree = ILUtils.build(ILLang.op_ident, decl.opName());
                    newAddSymbolDecls = declareAllSymbolsIn(interfaceTree, interfaceTree,
                            commonNames, modifier, symbolKind,
                            true, false, instr);
                } else {
                    newAddSymbolDecls = declareAllSymbolsIn(decl.down(1), decl.down(1),
                            commonNames, modifier, symbolKind,
                            delayedDeclaration, false, instr);
                    newAddSymbolDecls = TapList.append(newAddSymbolDecls,
                            declareAllSymbolsIn(decl.down(2), decl.down(2),
                                    commonNames, modifier, symbolKind,
                                    delayedDeclaration, false, instr));
                }
                break;
            case ILLang.op_none:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_realCst:
            case ILLang.op_bitCst:
            case ILLang.op_letter:
            case ILLang.op_star:
            case ILLang.op_metavar:
            case ILLang.op_minus:
            case ILLang.op_nameEq:
            case ILLang.op_call:
                break;
            case ILLang.op_ident: {
                String name = decl.stringValue();
                if (symbolKind == SymbolTableConstants.VARIABLE) {
                    VariableDecl variableDecl = getVariableOrConstantDecl(name);
                    if (variableDecl == null) {
                        if (!TapList.containsEquals(commonNames, name)) {
                            boolean isCommonName = false;
                            if (instr != null && instr.tree != null
                                    && instr.tree.opCode() == ILLang.op_save) {
                                isCommonName = name.startsWith("/") && name.endsWith("/");
                            }
                            if (!isCommonName) {
                                SymbolDecl newSymbolDecl = TapList.findNamedSymbolDecl(addSymbolDecls, name);
                                if (newSymbolDecl == null) {
                                    WrapperTypeSpec implicitTypeSpec = getImplicitTypeSpec(name);
                                    newSymbolDecl = SymbolDecl.build(implicitTypeSpec, null, false, topDeclTree, SymbolTableConstants.VARIABLE,
                                            this, instr, null);
                                    if (delayedDeclaration) {
                                        addSymbolDecls = new TapList<>(newSymbolDecl, addSymbolDecls);
                                        newAddSymbolDecls = new TapList<>(newSymbolDecl, null);
                                    } else {
                                        addSymbolDecl(newSymbolDecl);
                                    }
                                } else {
                                    if (delayedDeclaration) {
                                        newAddSymbolDecls = new TapList<>(newSymbolDecl, null);
                                    }
                                }
                                if (modifier != null && modifier.opCode() == ILLang.op_ident) {
                                    newSymbolDecl.addExtraInfo(new TapList<>(modifier.stringValue(), null));
                                }
                                if (instr != null && instr.tree != null
                                        && (instr.tree.opCode() == ILLang.op_accessDecl
                                        || instr.tree.opCode() == ILLang.op_save)) {
                                    newSymbolDecl.setInstruction(instr);
                                }
                            }
                        }
                    } else {
                        if (modifier != null && modifier.opCode() == ILLang.op_ident) {
                            variableDecl.addExtraInfo(new TapList<>(modifier.stringValue(), null));
                        }
                        if (instr != null && instr.tree != null
                                && (instr.tree.opCode() == ILLang.op_accessDecl
                                || instr.tree.opCode() == ILLang.op_save)) {
                            variableDecl.setInstruction(instr);
                        }
                    }
                } else if (symbolKind == SymbolTableConstants.SYMBOL) {
                    SymbolDecl symbolDecl = getSymbolDecl(name);
                    if (symbolDecl == null && !TapList.containsEquals(commonNames, name)) {
                        SymbolDecl newSymbolDecl =
                                SymbolDecl.build(new WrapperTypeSpec(null), null, false,
                                        topDeclTree, SymbolTableConstants.SYMBOL, this, instr, null);
                        if (delayedDeclaration) {
                            addSymbolDecls = new TapList<>(newSymbolDecl,
                                    addSymbolDecls);
                            newAddSymbolDecls = new TapList<>(newSymbolDecl,
                                    null);
                        } else {
                            addSymbolDecl(newSymbolDecl);
                        }
                        if (modifier != null && modifier.opCode() == ILLang.op_ident) {
                            newSymbolDecl.addExtraInfo(new TapList<>(modifier.stringValue(), null));
                        }
                        if (instr != null && instr.tree != null
                                && instr.tree.opCode() == ILLang.op_accessDecl) {
                            newSymbolDecl.setInstruction(instr);
                        }
                    } else if (modifier != null && modifier.opCode() == ILLang.op_ident) {
                        assert symbolDecl != null;
                        symbolDecl.addExtraInfo(new TapList<>(modifier.stringValue(), null));
                        symbolDecl.setInstruction(instr);
                    }
                }
                break;
            }
            default:
                break;
        }
        return newAddSymbolDecls;
    }

    private boolean isOverloadedOperator(Operator oper) {
        return oper.code == ILLang.op_assign
                || oper.code == ILLang.op_add
                || oper.code == ILLang.op_sub
                || oper.code == ILLang.op_mul
                || oper.code == ILLang.op_div
                || oper.code == ILLang.op_ge
                || oper.code == ILLang.op_gt
                || oper.code == ILLang.op_le
                || oper.code == ILLang.op_lt
                || oper.code == ILLang.op_minus;
    }

    public TapIntList systemPredefinedZones() {
        ZoneInfo zoneInfo ;
        String varName;
        SymbolDecl symbolDecl;
        TapIntList result = null;
        for (int i=freeDeclaredZone[SymbolTableConstants.ALLKIND]-1 ; i>=0 ; --i) {
            zoneInfo = declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
            if (zoneInfo!=null) {
                varName = zoneInfo.bestVarName();
                if (varName != null) {
                    symbolDecl = getSymbolDecl(varName);
                    if (symbolDecl != null && symbolDecl.isSystemPredefined()) {
                        result = new TapIntList(i, result);
                    }
                }
            }
        }
        return result ;
    }
                

    public TapIntList inCommonZones() {
        ZoneInfo zoneInfo ;
        TapIntList result = null;
        for (int i=freeDeclaredZone[SymbolTableConstants.ALLKIND]-1 ; i>=0 ; --i) {
            zoneInfo = declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
            if (zoneInfo!=null && zoneInfo.isCommon()) {
                result = new TapIntList(i, result);
            }
        }
        return result;
    }

    public TapIntList constantZones() {
        ZoneInfo zoneInfo ;
        TapIntList result = null;
        for (int i=freeDeclaredZone[SymbolTableConstants.ALLKIND]-1 ; i>=0 ; --i) {
            zoneInfo = declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
            if (zoneInfo!=null && zoneInfo.isConstant()) {
                result = new TapIntList(i, result);
            }
        }
        return result;
    }

    private void wrapPointerType(Tree modifier, SymbolDecl newSymbolDecl) {
        if (modifier.stringValue().equals("allocatable")) {
            WrapperTypeSpec typeSpec = newSymbolDecl.type();
            PointerTypeSpec ptrTypeSpec = new PointerTypeSpec(typeSpec, null);
            ((VariableDecl) newSymbolDecl).setType(new WrapperTypeSpec(ptrTypeSpec));
        }
    }

    /**
     * Declares a list of external function names.
     * or a forward or external function (for C/C++).
     */
    public void addExternalOrForwardDeclaration(Tree externalTree, Instruction instrDecl, boolean isPrivate) {
        Tree[] functionNames;
        if (externalTree.opCode() == ILLang.op_external) {
            functionNames = externalTree.children();
        } else {
            // if arriving here, instrDecl contains a op_varDeclaration
            Tree nameTree = ILUtils.baseTree(externalTree);
            functionNames = new Tree[]{nameTree};
        }
        String name;
        Unit functionUnit;
        SymbolDecl paramSymbolDecl;
        FunctionDecl localFunctionDecl;
        TapList<FunctionDecl> localFunctionDecls;
        TapList<FunctionDecl> rootFunctionDecls;
        FunctionDecl functionDecl;
        SymbolTable rootSymbolTable = this;
        SymbolTable publicSymbolTable = null;
        if (!isPrivate) {
            while (rootSymbolTable.basisSymbolTable != getCallGraph().globalRootSymbolTable()) {
                if (rootSymbolTable.isFormalParams) {
                    publicSymbolTable = rootSymbolTable;
                }
                rootSymbolTable = rootSymbolTable.basisSymbolTable;
            }
        }

        if (publicSymbolTable == null) {
            publicSymbolTable = rootSymbolTable;
        }
        for (Tree functionName : functionNames) {
            name = ILUtils.baseName(functionName);
            localFunctionDecls = getTopFunctionDecl(name, null, null, false);
            localFunctionDecl = localFunctionDecls == null ? null : localFunctionDecls.head;
            if (localFunctionDecl != null) {
                paramSymbolDecl = null;
                rootFunctionDecls = rootSymbolTable.getTopFunctionDecl(name, null, null, false);
                FunctionDecl rootFunctionDecl = rootFunctionDecls == null ? null : rootFunctionDecls.head;
                functionDecl = localFunctionDecl;
                if (rootFunctionDecl != null && localFunctionDecl.unit() != rootFunctionDecl.unit()) {
                    //"rootFunctionDecl" and "localFunctionDecl" MUST point to the SAME Unit
                    // ==> If we arrive here, this is a bug in Tapenade
                    TapEnv.toolWarning(-1, "Erroneous analysis of function declarations");
                }
            } else {
                paramSymbolDecl = publicSymbolTable.isFormalParams ? publicSymbolTable.getTopSymbolDecl(name) : null;
                if (paramSymbolDecl != null) {
                    rootFunctionDecls = null;
                    functionDecl = null;
                } else {
                    rootFunctionDecls = rootSymbolTable.getTopFunctionDecl(name, null, null, false);
                    functionDecl = rootFunctionDecls == null ? null : rootFunctionDecls.head;
                }
            }
            if (functionDecl != null && functionDecl.isIntrinsic() && functionDecl.unit().isFortran()) {
                // cas functionDecl fortran et external C, on garde les 2 declarations:
                functionDecl = null;
            }
            if (functionDecl != null && functionDecl.unit() != null) {
                if (functionDecl.isExternal()) {
                    if (localFunctionDecl == null) {
                        //If function is unknown here and already known in the rootSymbolTable as an external,
                        // then copy here all the rootSymbolTable's declaration(s) for this function.
                        TapList<FunctionDecl> inRootFunctionDecls = rootFunctionDecls;
                        while (inRootFunctionDecls != null) {
                            FunctionDecl rootFunctionDecl = inRootFunctionDecls.head;
                            addSymbolDecl(rootFunctionDecl);
                            rootFunctionDecl.setInstruction(instrDecl);
                            inRootFunctionDecls = inRootFunctionDecls.tail;
                        }
                    } else if (this != getCallGraph().cRootSymbolTable()) {
                        //If function is declared as external
                        // here and at the root: double external
                        // declaration: do not add new declaration, and
                        // complain except when adding into the C root SymbolTable, which happens naturally.
                        TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD08) Double declaration of external procedure " + name);
                    }
                } else if (functionDecl.isIntrinsic()) {
                    //Otherwise complain, and do as if this
                    // external declaration had disappeared.
                    TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD07) Cannot combine successive declarations of procedure " + name + " kind: INTRINSIC and EXTERNAL (ignored new)");
                } else if (functionDecl.isStandard() || functionDecl.isInterface()) {
                    addSymbolDecl(functionDecl);
                    functionDecl.setInstruction(instrDecl);
                } else {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD07) Cannot combine successive declarations of procedure " + name + " kind: USER and EXTERNAL (ignored new)");
                }
            } else if (paramSymbolDecl != null) {
                // "funcName" is a formal parameter of the current subroutine
                if (paramSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                    functionDecl = (FunctionDecl) paramSymbolDecl;
                    if (functionDecl.isExternal()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD08) Double declaration of external procedure " + name);
                    } else if (functionDecl.isIntrinsic()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD07) Cannot combine successive declarations of procedure " + name + " kind: INTRINSIC and EXTERNAL (ignored new)");
                    }
                } else if (paramSymbolDecl.isA(SymbolTableConstants.VARIABLE)) {
                    if (publicSymbolTable.isFormalParams) {
                        Unit varFunctionUnit = Unit.makeVarFunction(name, unit);
                        varFunctionUnit.setFunctionTypeSpec(
                                new FunctionTypeSpec(paramSymbolDecl.type()));
                        functionDecl = new FunctionDecl(functionName, varFunctionUnit);
                        publicSymbolTable.addSymbolDecl(functionDecl);
                        // Refresh the functionTypeSpec of the current subroutine for this formal parameter:
                        TapEnv.relatedUnit().exportOneArgumentType(name,
                                new WrapperTypeSpec(varFunctionUnit.functionTypeSpec()));
                    } else {
                        // c'est une declaration de function extern ou forward vue comme une variable:
                        functionUnit = buildExternalUnit(name, isPrivate);
                        functionDecl = new FunctionDecl(functionName, functionUnit);
                        FunctionTypeSpec varFunTypeSpec =
                                TypeSpec.findFunctionTypeSpecUnderTypeSpec(paramSymbolDecl.type());
                        functionUnit.setFunctionTypeSpec(
                                new FunctionTypeSpec(varFunTypeSpec.returnType, varFunTypeSpec.argumentsTypes, varFunTypeSpec.variableArgList));
                        SymbolDecl oldVarDecl = getDecl(functionDecl.symbol, SymbolTableConstants.VARIABLE, true);
                        TapList<SymbolDecl> dependsOn = null;
                        if (oldVarDecl != null) {
                            dependsOn = oldVarDecl.dependsOn();
                            publicSymbolTable.removeDecl(functionDecl.symbol, SymbolTableConstants.VARIABLE, true);
                        }
                        publicSymbolTable.addSymbolDecl(functionDecl);
                        functionDecl.dependsOn = dependsOn;
                    }
                }
                assert functionDecl != null;
                functionDecl.setInstruction(instrDecl);
                if (functionDecl.isExternal()) {
                    functionDecl.unit().setForwardDeclaration(instrDecl);
                }
            } else {
                functionUnit = buildExternalUnit(name, isPrivate);
                boolean hasVarDeclInstr = false;
                if (functionUnit.functionTypeSpec() == null) {
                    VariableDecl varFunDecl = getVariableDecl(functionUnit.name());
                    if (varFunDecl != null && varFunDecl.type() != null) {
                        hasVarDeclInstr = varFunDecl.hasInstructionWithRootOperator(ILLang.op_varDeclaration, null);
                        FunctionTypeSpec varFunTypeSpec =
                                TypeSpec.findFunctionTypeSpecUnderTypeSpec(varFunDecl.type());
                        if (varFunTypeSpec != null) {
                            functionUnit.setFunctionTypeSpec(
                                    new FunctionTypeSpec(varFunTypeSpec.returnType,
                                            varFunTypeSpec.argumentsTypes.length == 0 ? null
                                                    : varFunTypeSpec.argumentsTypes,
                                            varFunTypeSpec.variableArgList));
                        } else {
                            functionUnit.setFunctionTypeSpec(
                                    new FunctionTypeSpec(new WrapperTypeSpec(null)));
                            functionUnit.functionTypeSpec().returnType = varFunDecl.type();

                        }
                    } else {
                        functionUnit.setFunctionTypeSpec(
                                new FunctionTypeSpec(new WrapperTypeSpec(null)));
                    }
                }
                functionDecl = new FunctionDecl(functionName, functionUnit, rootSymbolTable);
                SymbolDecl oldVarDecl = getDecl(functionDecl.symbol, SymbolTableConstants.VARIABLE, true);
                TapList<SymbolDecl> dependsOn = null;
                if (oldVarDecl != null) {
                    dependsOn = oldVarDecl.dependsOn();
                    removeDecl(functionDecl.symbol, SymbolTableConstants.VARIABLE, true);
                }
                addSymbolDecl(functionDecl);

                functionDecl.dependsOn = dependsOn;
                if (hasVarDeclInstr) {
                    functionDecl.setMainDeclInstr(true);
                }
                functionDecl.setInstruction(instrDecl);
                if (functionUnit.isExternal()) {
                    functionUnit.setForwardDeclaration(instrDecl);
                }
                rootSymbolTable.addSymbolDecl(functionDecl);
            }
        }
    }

    public Unit buildExternalUnit(String name, boolean isPrivate) {
        Unit functionUnit;
        if (unit != null && !unit.isTranslationUnit()) {
            functionUnit = unit.callGraph().getAnyNonVarUnit(name, unit);
            if (functionUnit == null) {
                functionUnit = Unit.makeExternal(name, unit.callGraph(), language, isPrivate);
            }
        } else {
            functionUnit = Unit.makeExternal(name, TapEnv.get().origCallGraph(), TapEnv.C, isPrivate);
        }
        return functionUnit;
    }

    /**
     * Declares an interface name
     * Assumes interfaceTree is an op_ident.
     * For generic interfaces: declare the generic name of the interfaceDecl.
     */
    protected InterfaceDecl addInterfaceDecl(Tree interfaceTreeOrig,
                                             Unit containerUnit) {
        Tree interfaceTree = interfaceTreeOrig;
        InterfaceDecl interfaceDecl;
        if (interfaceTree != null) {
            if (interfaceTree.opCode() == ILLang.op_binary) {
                interfaceTree = ILUtils.build(ILLang.op_ident,
                        interfaceTree.down(2).stringValue());
            } else if (interfaceTree.opCode() != ILLang.op_ident) {
                interfaceTree = ILUtils.build(ILLang.op_ident,
                        interfaceTree.opName());
            }
        }
        interfaceDecl = new InterfaceDecl(interfaceTree, null, this, containerUnit);
        addSymbolDecl(interfaceDecl);
        interfaceDecl.nameTree = interfaceTreeOrig;
        return interfaceDecl;
    }

    /**
     * Declares a list of intrinsic function names.
     * Assumes intrinsicTree is an op_intrinsic.
     */
    public void addIntrinsicDeclaration(Tree intrinsicTree, Instruction instrDecl) {
        Tree[] functionNames = intrinsicTree.children();
        String name;
        Unit functionUnit;
        SymbolDecl paramSymbolDecl;
        FunctionDecl localFunctionDecl;
        TapList<FunctionDecl> localFunctionDecls;
        FunctionDecl rootFunctionDecl;
        TapList<FunctionDecl> rootFunctionDecls;
        FunctionDecl functionDecl;
        SymbolTable rootSymbolTable = this;
        SymbolTable publicSymbolTable = null;
        while (rootSymbolTable.basisSymbolTable != getCallGraph().globalRootSymbolTable()) {
            if (rootSymbolTable.isFormalParams) {
                publicSymbolTable = rootSymbolTable;
            }
            rootSymbolTable = rootSymbolTable.basisSymbolTable;
        }
        for (Tree functionName : functionNames) {
            name = ILUtils.baseName(functionName);
            localFunctionDecls = getTopFunctionDecl(name, null, null, false);
            localFunctionDecl = localFunctionDecls == null ? null : localFunctionDecls.head;
            if (localFunctionDecl != null) {
                paramSymbolDecl = null;
                rootFunctionDecls = rootSymbolTable.getTopFunctionDecl(name, null, null, false);
                rootFunctionDecl = rootFunctionDecls == null ? null : rootFunctionDecls.head;
                functionDecl = localFunctionDecl;
                if (rootFunctionDecl != null && localFunctionDecl.unit() != rootFunctionDecl.unit()) {
                    //"rootFunctionDecl" and "localFunctionDecl" MUST point to the SAME Unit
                    // ==> If we arrive here, this is a bug in Tapenade
                    TapEnv.toolWarning(-1, "Erroneous analysis of function declarations (2)");
                }
            } else {
                paramSymbolDecl = publicSymbolTable == null ? null : publicSymbolTable.getTopSymbolDecl(name);
                if (paramSymbolDecl != null) {
                    rootFunctionDecl = null;
                    functionDecl = null;
                } else {
                    rootFunctionDecls = rootSymbolTable.getTopFunctionDecl(name, null, null, false);
                    rootFunctionDecl = rootFunctionDecls == null ? null : rootFunctionDecls.head;
                    functionDecl = rootFunctionDecl;
                }
            }
            if (functionDecl != null) {
                if (functionDecl.isIntrinsic()) {
                    if (localFunctionDecl == null) {
                        //If function is already known,
                        // and as an external, and at the root
                        // level only: ok! add the declaration here.
                        addSymbolDecl(rootFunctionDecl);
                    } else {
                        //If function is declared as external
                        // here and at the root: double external
                        // declaration: complain and do nothing.
                        TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD08) Double declaration of intrinsic procedure " + name);
                    }
                } else if (functionDecl.isExternal()) {
                    //Otherwise complain, and do as if this
                    // external declaration had disappeared.
                    TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD07) Cannot combine successive declarations of procedure " + name + " kind: EXTERNAL and INTRINSIC (ignored new)");
                } else {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD07) Cannot combine successive declarations of procedure " + name + " kind: USER and INTRINSIC (ignored new)");
                }
                functionDecl.setInstruction(instrDecl);
            } else if (paramSymbolDecl != null) {
                // "funcName" is a formal parameter of the current subroutine
                if (paramSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                    functionDecl = (FunctionDecl) paramSymbolDecl;
                    if (functionDecl.isIntrinsic()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD08) Double declaration of intrinsic procedure " + name);
                    } else if (functionDecl.isExternal()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, functionName, "(DD07) Cannot combine successive declarations of procedure " + name + " kind: EXTERNAL and INTRINSIC (ignored new)");
                    }
                } else if (paramSymbolDecl.isA(SymbolTableConstants.VARIABLE)) {
                    Unit varFunctionUnit = Unit.makeVarFunction(name, unit);
                    varFunctionUnit.setFunctionTypeSpec(
                            new FunctionTypeSpec(paramSymbolDecl.type()));
                    functionDecl = new FunctionDecl(functionName,
                            varFunctionUnit);
                    publicSymbolTable.addSymbolDecl(functionDecl);
                    functionDecl.setInstruction(instrDecl);
                }
            } else {
                functionUnit = unit.callGraph().getAnyIntrinsicUnit(name);
                if (functionUnit == null) {
                    functionUnit = unit.callGraph().createNewIntrinsicUnit(name, language);
                }
                if (functionUnit.functionTypeSpec() == null) {
                    functionUnit.setFunctionTypeSpec(new FunctionTypeSpec(new WrapperTypeSpec(null)));
                }
                functionDecl = new FunctionDecl(functionName, functionUnit);
                FunctionDecl newFuncDecl;
                TapList<FunctionDecl> newFuncDecls;
                addSymbolDecl(functionDecl);
                newFuncDecls = getTopFunctionDecl(functionDecl.symbol, null, null, false);
                newFuncDecl = newFuncDecls == null ? null : newFuncDecls.head;
                assert newFuncDecl != null;
                newFuncDecl.setInstruction(instrDecl);
                rootSymbolTable.addSymbolDecl(functionDecl);
                newFuncDecls = rootSymbolTable.getFunctionDecl(functionDecl.symbol, null, null, false);
                newFuncDecl = newFuncDecls == null ? null : newFuncDecls.head;
                assert newFuncDecl != null;
                newFuncDecl.setInstruction(instrDecl);
            }
        }
    }

    /**
     * Updates this symbolTable with respect to the
     * implicit typing declaration Tree "decl". This typing is
     * analyzed with respect to SymbolTable "refST".
     */
    public void addImplicit(Tree decl, SymbolTable refST) {
        Tree[] declarators;
        Tree[] letters;
        int i;
        int j;
        WrapperTypeSpec typeSpec;
        declarators = decl.children();
        if (declarators.length == 0) {
            setImplicitLetters('a', 'z', new WrapperTypeSpec(null));
        } else {
            for (i = 0; i < declarators.length; i++) {
                TapList<String> toInfos = new TapList<>(null, null);
                TapList<String> toAccess = new TapList<>(null, null);
                ToBool isPointer = new ToBool(false);
                typeSpec = TypeSpec.build(declarators[i].down(1),
                        refST, new Instruction(decl), new TapList<>(null, null), toInfos, toAccess, isPointer, null);
                if (toInfos.tail != null || toAccess.tail != null) {
                    TapEnv.toolWarning(-1, "Cannot handle this implicit declaration " + ILUtils.toString(declarators[i]));
                }
                letters = declarators[i].down(2).children();
                for (j = 0; j < letters.length; j++) {
                    if (letters[j].opCode() == ILLang.op_letter) {
                        setImplicitLetters(letters[j].stringValue()
                                        .charAt(0),
                                letters[j].stringValue().charAt(0), typeSpec);
                    } else if (letters[j].opCode() == ILLang.op_letterRange) {
                        setImplicitLetters(letters[j].down(1).stringValue().charAt(0),
                                letters[j].down(2).stringValue().charAt(0),
                                typeSpec);
                    }
                }
            }
        }
    }

    /**
     * Sets all implicit definitions in this current SymbolTable,
     * from letter "fromStr" to letter "toStr", to type "defaultType".
     */
    public void setImplicitLetters(char fromChar, char toChar, WrapperTypeSpec defaultType) {
        int from = Character.getNumericValue(fromChar) -
                Character.getNumericValue('a');
        int to = Character.getNumericValue(toChar) -
                Character.getNumericValue('a');
        for (int i = from; i <= to; i++) {
            implicits[i] = defaultType;
        }
    }

    /**
     * @return the implicit type declaration available for
     * variable named "name". @return null when no implicit
     * type is available, or when this type is null or WrapperTypeSpec(null).
     */
    private WrapperTypeSpec getImplicitTypeSpec(String name) {
        WrapperTypeSpec implicitType = null;
        if (name != null && !name.isEmpty() && implicits != null) {
            int index = Character.getNumericValue(name.charAt(0)) -
                    Character.getNumericValue('a');
            if (index >= 0 && index <= 25 && implicits[index] != null &&
                    implicits[index].wrappedType != null) {
                implicitType = implicits[index];
            }
        }
        return implicitType;
    }

    /**
     * Fixes all incomplete types in the top level of this SymbolTable,
     * that can be fixed using the implicit typing mechanism.
     */
    protected void fixImplicits() {
        TapList<SymbolDecl> bucket;
        SymbolDecl symbolDecl;
        WrapperTypeSpec typeSpec;
        WrapperTypeSpec baseTypeSpec;
        WrapperTypeSpec scalarTypeSpec;
        WrapperTypeSpec implicitTypeSpec;
        for (int i = 0; i < NUM_BUCKETS; i++) {
            bucket = buckets[i].tail;
            while (bucket != null) {
                symbolDecl = bucket.head;
                // Phase 1 : get/set the WrapperTypeSpec of this SymbolDecl when relevant.
                //  Notice: do not set an implicit type for the return of functions,
                //  because we might discover later that it must be a subroutine.
                if (symbolDecl == null) {
                    typeSpec = null;
                } else if (symbolDecl.isA(SymbolTableConstants.VARIABLE) ||
                        symbolDecl.isA(SymbolTableConstants.CONSTANT)) {
                    typeSpec = symbolDecl.type();
                    if (typeSpec == null) {
                        typeSpec = new WrapperTypeSpec(null);
                        ((VariableDecl) symbolDecl).setType(typeSpec);
                    }
                } else if (symbolDecl.isA(SymbolTableConstants.FIELD)) {
                    typeSpec = symbolDecl.type();
                    if (typeSpec == null) {
                        typeSpec = new WrapperTypeSpec(null);
                        ((FieldDecl) symbolDecl).setType(typeSpec);
                    }
                } else {
                    typeSpec = null;
                }
                // Phase 2 : instantiate typeSpec when necessary, using implicit rules.
                if (typeSpec != null) {
                    baseTypeSpec = typeSpec.baseTypeSpec(false);
                    if (baseTypeSpec == null || baseTypeSpec.wrappedType == null 
                        || (fr.inria.tapenade.representation.TypeSpec.isA(baseTypeSpec.wrappedType,
                                   fr.inria.tapenade.representation.SymbolTableConstants.PRIMITIVETYPE)
                            && baseTypeSpec.wrappedType.isUndefinedNumeric())) {
                        implicitTypeSpec = getImplicitTypeSpec(symbolDecl.symbol);
                        if (implicitTypeSpec == null) {
                            if (unit == null || unit.isC()) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, symbolDecl.defPositions.head, "(TC21) Incomplete declaration of variable " + symbolDecl.symbol);
                            } else {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, symbolDecl.defPositions.head, "(TC21) No implicit rule available to complete the declaration of variable " + symbolDecl.symbol);
                            }
                        } else {
                            scalarTypeSpec = typeSpec.scalarTypeSpec(false, this);
                            if (scalarTypeSpec.wrappedType != null &&
                                    !scalarTypeSpec.receives(implicitTypeSpec, null, null)) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, symbolDecl.defPositions.head, "(TC22) Variable " + symbolDecl.symbol + ", declared implicitly as " + implicitTypeSpec.showType() + ", is used as " + scalarTypeSpec.showType());
                            }
                            scalarTypeSpec.wrappedType = implicitTypeSpec.wrappedType;
                            symbolDecl.setFromImplicitType(implicitTypeSpec);
                            Tree newDecl;
                            Tree declarator = ILUtils.build(ILLang.op_ident, symbolDecl.symbol);
                            if (symbolDecl.isA(SymbolTableConstants.VARIABLE) && ((VariableDecl) symbolDecl).isFunctionName) {
                                declarator.setAnnotation("isFunctionName", Boolean.TRUE);
                            }
                            if (symbolDecl.hasOtherInstruction()) {
                                newDecl = ILUtils.build(ILLang.op_varDeclaration,
                                        ILUtils.build(ILLang.op_modifiers),
                                        scalarTypeSpec.wrappedType.generateTree(this,
                                                null, null, true, null),
                                        ILUtils.build(ILLang.op_declarators, declarator));
                            } else if (language == TapEnv.FORTRAN && TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
                                Tree fullTypeTree = typeSpec.wrappedType.generateTree(this, null, null, true, null);
                                newDecl = ILUtils.build(ILLang.op_varDeclaration,
                                        ILUtils.build(ILLang.op_modifiers),
                                        fullTypeTree.cutChild(1),
                                        ILUtils.build(ILLang.op_declarators,
                                                ILUtils.build(ILLang.op_arrayDeclarator,
                                                        declarator,
                                                        fullTypeTree.cutChild(2))));
                            } else {
                                newDecl = ILUtils.build(ILLang.op_varDeclaration,
                                        ILUtils.build(ILLang.op_modifiers),
                                        typeSpec.wrappedType.generateTree(this, null, null, true, null),
                                        ILUtils.build(ILLang.op_declarators, declarator));
                            }
                            Instruction newInstr = new Instruction(newDecl);
                            if (symbolDecl.fromInclude() != null && symbolDecl.fromInclude().tree != null) {
                                symbolDecl.fromInclude().setIncludeOfDiffInstructions(newInstr, false, null, null);
                                // When there was an incomplete type declaration in a include file,
                                // and we just fixed it using implicit rules,
                                // then we must force creation of a new "differentiated" (or "-p") include file.
                                symbolDecl.fromInclude().forceDiffInclude(
                                        TapEnv.modeIsNoDiff() ? TapEnv.get().preprocessFileSuffix : TapEnv.get().diffFileSuffix);
                            }
                            this.declarationsBlock.addInstrDeclTlBeforeUse(newInstr,
                                    new TapList<>(symbolDecl, null),
                                    null, this, true);
                            symbolDecl.setInstruction(newInstr);
                            this.unit.fortranStuff().implicitDeclarations = new TapList<>(
                                    new TapPair<>(symbolDecl.symbol, newInstr),
                                    this.unit.fortranStuff().implicitDeclarations);
                        }
                    }
                }
                bucket = bucket.tail;
            }
        }
    }

    /**
     * Fixes all incomplete types in the top level of this SymbolTable,
     * that can be fixed using the imported symbolTables.
     */
    protected void fixImportedDeclarations() {
        TapList<SymbolDecl> toBucket;
        SymbolDecl symbolDecl;
        WrapperTypeSpec typeSpec;
        TypeDecl importedTypeDecl;
        boolean removeThisSymbol;
        for (int i = 0; i < NUM_BUCKETS; i++) {
            toBucket = buckets[i];
            while (toBucket.tail != null) {
                symbolDecl = toBucket.tail.head;
                removeThisSymbol = false;
                if (symbolDecl != null) {
                    switch (symbolDecl.kind) {
                        case SymbolTableConstants.TYPE:
                            typeSpec = ((TypeDecl) symbolDecl).typeSpec;
                            if (TypeSpec.isA(typeSpec, SymbolTableConstants.NAMEDTYPE)) {
                                importedTypeDecl =
                                        (TypeDecl) unit.getImportedDecl(((NamedTypeSpec) typeSpec.wrappedType).name(),
                                                SymbolTableConstants.TYPE);
                                if (importedTypeDecl != null) {
                                    typeSpec.wrappedType = importedTypeDecl.typeSpec.wrappedType;
                                    removeThisSymbol = true;
                                }
                            }
                            if (typeSpec != null && !removeThisSymbol) {
                                typeSpec.updateAfterImports(this, null);
                            }
                            break;
                        case SymbolTableConstants.VARIABLE:
                            typeSpec = symbolDecl.type();
                            if (typeSpec != null) {
                                typeSpec.updateAfterImports(this, null);
                            }
                            TapList<SymbolDecl> dependsOn = symbolDecl.dependsOn();
                            SymbolDecl dependsOnDecl;
                            SymbolDecl importedDecl;
                            TapList<SymbolDecl> fixedDependsOn = null;
                            while (dependsOn != null) {
                                dependsOnDecl = dependsOn.head;
                                if (dependsOnDecl.type() == null) {
                                    importedDecl = this.getSymbolDecl(dependsOnDecl.symbol);
                                    if (importedDecl != null) {
                                        fixedDependsOn = new TapList<>(importedDecl, fixedDependsOn);
                                    } else {
                                        fixedDependsOn = new TapList<>(dependsOnDecl, fixedDependsOn);
                                    }
                                } else {
                                    fixedDependsOn = new TapList<>(dependsOnDecl, fixedDependsOn);
                                }
                                dependsOn = dependsOn.tail;
                            }
                            symbolDecl.dependsOn = fixedDependsOn;
                            break;
                        case SymbolTableConstants.FUNCTION:
                            typeSpec = ((FunctionDecl) symbolDecl).type();
                            if (typeSpec != null) {
                                typeSpec.updateAfterImports(this, null);
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (removeThisSymbol) {
                    toBucket.tail = toBucket.tail.tail;
                } else {
                    toBucket = toBucket.tail;
                }
            }
        }
    }

    private ZoneInfo[] initZoneInfosArray(int modelLength) {
        ZoneInfo[] copyZoneInfos = new ZoneInfo[modelLength];
        for (int i = modelLength - 1; i >= 0; --i) {
            copyZoneInfos[i] = null;
        }
        return copyZoneInfos;
    }

    protected void setEmptyImportedZones() {
        
    }

    /**
     * Fills the declaredZoneInfos attached to this SymbolTable, which is some Unit's importsSymbolTable)
     * @param zoneNumbers the total numbers (one per kind) of ZoneInfo's that will
     * be declared by this import SymbolTable.
     * @param allStaticZones is the list of all static zones created, ordered from zoneNb 0 up.
     */
    protected void fillImportedZones(int[] zoneNumbers, TapList<ZoneInfo> allStaticZones) {
	for (int i=0 ; i<4 ; ++i) { // i.e. for i in {ALLKIND, REALKIND, INTKIND, PTRKIND}
	    firstDeclaredZone[i] = 0 ;
	    freeDeclaredZone[i] = zoneNumbers[i] ;
	    declaredZoneInfos[i] = initZoneInfosArray(zoneNumbers[i]) ;
	}

        TapList<SymbolDecl> curBucket;
        SymbolDecl symbolDecl, origSymbolDecl ;
	SymbolTable origSymbolTable ;
        String symbolName, origSymbolName;
	TapList<?> symbolZonesTree ;
        TapIntList zones;
        ZoneInfo origZoneInfo, newZoneInfo, newTargetOf;
	int index, firstIndexInOrig ;

        TapList<TapPair<ZoneInfo, ZoneInfo>> memoCopies = new TapList<>(null, null);

        // Fill the ZoneInfo's that come from importation commands (e.g. USE module)
        // according to the renamings done by the importation command.
        for (int i=0 ; i<NUM_BUCKETS ; ++i) {
            curBucket = buckets[i].tail;
            while (curBucket != null) {
                symbolDecl = curBucket.head;
                symbolName = symbolDecl.symbol;
		symbolZonesTree = symbolDecl.zones() ;
                origSymbolDecl = symbolDecl ;
                origSymbolTable = this ;
                if (symbolDecl.importedFrom!=null) {
                    origSymbolDecl = symbolDecl.importedFrom.first ;
                    origSymbolTable = symbolDecl.importedFrom.second ;
                }
		if (symbolDecl.isA(SymbolTableConstants.VARIABLE)) {
		    symbolZonesTree = includePointedInitialsInTree(symbolZonesTree, origSymbolTable) ;
		}
		zones = ZoneInfo.listAllZones(symbolZonesTree, true) ;
		firstIndexInOrig = origSymbolTable.firstDeclaredZone[SymbolTableConstants.ALLKIND] ;
                while (zones != null) {
                    index = zones.head ;
                    origZoneInfo = null ;
                    if (index<firstIndexInOrig) {
                        // This may happen for zones that are at the same time exported by the MODULE but also put in COMMON.
                        // they have been "allocated" before the zones of the MODULE, and have received a zone number
                        // smaller that the zones of the module.
                        origZoneInfo = origSymbolTable.declaredZoneInfo(index, SymbolTableConstants.ALLKIND) ;
                    } else if (index >= 0) {
                        origZoneInfo = origSymbolTable.declaredZoneInfos[SymbolTableConstants.ALLKIND][index-firstIndexInOrig];
                    }
                    if (origZoneInfo!=null) {
                        if (origSymbolDecl != null) {
                            origSymbolName = origSymbolDecl.symbol;
                            newZoneInfo = origZoneInfo.copy();
                            newZoneInfo.shareActivity(origZoneInfo) ;
                            memoCopies.placdl(new TapPair<>(origZoneInfo, newZoneInfo));
                            if (origZoneInfo.targetZoneOf != null) {
                                newTargetOf = TapList.cassq(origZoneInfo.targetZoneOf, memoCopies.tail);
                                if (newTargetOf != null) {
                                    newZoneInfo.targetZoneOf = newTargetOf;
                                } else {
                                    TapEnv.toolWarning(-1, "Wrong order in copy ZoneInfo " + origZoneInfo + ", target of " + origZoneInfo.targetZoneOf);
                                }
                            }
                            newZoneInfo.setVariableNames(new TapList<>(symbolName,
                                    TapList.deleteString(origSymbolName, newZoneInfo.variableNames())));
                            newZoneInfo.accessTree =
                                    ILUtils.modifyRootName(newZoneInfo.accessTree, origSymbolName, symbolName);
                        } else {
                            newZoneInfo = origZoneInfo;
                        }
                        declaredZoneInfos[SymbolTableConstants.ALLKIND][origZoneInfo.zoneNb] = newZoneInfo ;
                    }
		    zones = zones.tail ;
		}
                curBucket = curBucket.tail ;
	    }
        }

        // Fill the ZoneInfo's with the ZoneInfo of all
        // globals (Common, Equivalence, Data, Save, Bind(C)...) that have been prepared
        // by MemMap.allocateZones() and stored waiting here in "waitingZoneInfos".
        while (waitingZoneInfos!=null) {
            ZoneInfo proxyZoneInfo = waitingZoneInfos.head ;
            index = proxyZoneInfo.zoneNb ;
            //Fill the proxy's "targetZonesTree" field after that of its reference,
            // because it couldn't be set upon creation.
            CallGraph callGraph = getCallGraph() ;
            SymbolTable langRootSymbolTable = (callGraph==null ? null : callGraph.languageRootSymbolTable(language)) ;
            if (langRootSymbolTable!=null) {
                ZoneInfo refZoneInfo = langRootSymbolTable.declaredZoneInfo(index, SymbolTableConstants.ALLKIND) ;
                if (refZoneInfo!=null) {
                    proxyZoneInfo.targetZonesTree = refZoneInfo.targetZonesTree ;
                }
            }
            if (declaredZoneInfos[SymbolTableConstants.ALLKIND][index]==null) {
                // Careful not to ovewrite an alrady found ZoneInfo (case of Module vars that are also declared save or common)
                declaredZoneInfos[SymbolTableConstants.ALLKIND][index] = proxyZoneInfo;
            }
            waitingZoneInfos = waitingZoneInfos.tail ;
        }

        boolean copyAsHidden ;
        ZoneInfo originalForCopy ;
        // Fill all the other ZoneInfo's, either with the same ZoneInfo found in an enclosing
        // (imports?)SymbolTable, or with an inaccessible ("hidden") copy of the original static ZoneInfo.
        // Also fill the "shortcut" arrays of ZoneInfo specialized for int/real/pointers
        for (int i=0; i<freeDeclaredZone[0] ; ++i) {
// BUG: pourquoi sol_scalar dans set03/lh065 n'arrive pas a recuperer les visibles de l'enclosing module "SUB".
            newZoneInfo = declaredZoneInfos[SymbolTableConstants.ALLKIND][i] ;
            if (newZoneInfo==null) {
              newZoneInfo = this.declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
              if (newZoneInfo==null) {
                  // First case: zone number i is not declared anywhere in this SymbolTable or enclosing.
                  // This means it is a static zone, and there is no way to access it directly except
                  // through side-effect. Prepare for a "hidden" copy, with a detailed description text:
                  originalForCopy = (ZoneInfo)TapList.nth(allStaticZones, i) ;
                  copyAsHidden = true ;
              } else if (accessTreeIsHiddenByDeclarations(newZoneInfo.accessTree, this.unit.publicSymbolTable())) {
                  // Second case: zone number i is visible from (an enclosing
                  // level of) this SymbolTable, but the formal parameters of this import SymbolTable's Unit
                  // declare a name that hides access to this zone.
                  // Prepare for a "hidden" copy of this zone, with a detailed description text:
                  originalForCopy = newZoneInfo ;
                  copyAsHidden = true ;
              } else if (this.unit.upperLevelUnit!=null && !this.unit.upperLevelUnit.isPackage()
                         && !newZoneInfo.isChannelOrIO) {
                  // Third case: zone number i is visible from an enclosing level of this SymbolTable,
                  // but we judge this is may not be obvious for the user, so we build a non-hidden
                  // copy of this zone, only to give it a more detailed description text:
                  originalForCopy = newZoneInfo ;
                  copyAsHidden = false ;
              } else {
                  // Default case: zone number i is visible from (an enclosing
                  // level of) this SymbolTable. We just place a reference to the same ZoneInfo
                  // at the level of this imports SymbolTable, for quicker access.
                  // Don't build a copy, don't make it hidden, keep description text.
                  originalForCopy = null ;
                  copyAsHidden = false ;
              }
              if (originalForCopy!=null) {
                  newZoneInfo = originalForCopy.copy() ;
                  newZoneInfo.shareActivity(originalForCopy) ;
                  memoCopies.placdl(new TapPair<>(originalForCopy, newZoneInfo));
                  if (originalForCopy.targetZoneOf != null) {
                      newTargetOf = TapList.cassq(originalForCopy.targetZoneOf, memoCopies.tail);
                      if (newTargetOf != null) {
                          newZoneInfo.targetZoneOf = newTargetOf;
                      } else {
                          TapEnv.toolWarning(-1, "Wrong order in copy ZoneInfo " + originalForCopy + ", target of " + originalForCopy.targetZoneOf);
                      }
                  }
                  newZoneInfo.isHidden = copyAsHidden ; // make it hidden.
                  newZoneInfo.description = originalForCopy.description ;
                  if (originalForCopy.declarationUnit!=null
                      && originalForCopy.declarationUnit.isModule()
                      && !originalForCopy.isRemanentLocal()) {
                      // Here we priorize description "[save in Mod]" over "[from module Mod]".
                      // We could do the other way as well...
                      newZoneInfo.description = newZoneInfo.description+"[from module "+originalForCopy.declarationUnit.name()+"]" ;
                  }
              }
              declaredZoneInfos[SymbolTableConstants.ALLKIND][i] = newZoneInfo ;
            }
            index = newZoneInfo.intZoneNb ;
            if (index>=0) declaredZoneInfos[SymbolTableConstants.INTKIND][index] = newZoneInfo;
            index = newZoneInfo.realZoneNb ;
            if (index>=0) declaredZoneInfos[SymbolTableConstants.REALKIND][index] = newZoneInfo;
            index = newZoneInfo.ptrZoneNb ;
            if (index>=0) declaredZoneInfos[SymbolTableConstants.PTRKIND][index] = newZoneInfo;
        }
    }

    /** True when the root name used in accessTree is also (re)defined at top of the symbolTable. */
    private boolean accessTreeIsHiddenByDeclarations(Tree accessTree, SymbolTable symbolTable) {
        if (accessTree==null) return true ;
        String name = ILUtils.baseName(accessTree) ;
        return (name==null || symbolTable.getTopVariableOrConstantDecl(name)!=null) ;
    }

    private void copyZoneInfosOfCommon(ZoneInfo[] modelZones, ZoneInfo[] copyZones) {
        for (int i = modelZones.length - 1; i >= 0; --i) {
            if (copyZones[i] == null && modelZones[i] != null && modelZones[i].isCommon()) {
                copyZones[i] = modelZones[i];
            }
        }
    }

    protected void declareAllSymbolDecls() {
        SymbolDecl symbolDecl;
        SymbolDecl previousDecl;
        while (addSymbolDecls != null) {
            symbolDecl = addSymbolDecls.head;
            if (symbolDecl != null) {
                previousDecl = getDecl(symbolDecl.symbol, symbolDecl.kind, false);
                if (symbolDecl.kind == SymbolTableConstants.VARIABLE && previousDecl == null) {
                    previousDecl = getDecl(symbolDecl.symbol, SymbolTableConstants.CONSTANT, false);
                }
                if (previousDecl == null) {
                    previousDecl = getDecl(symbolDecl.symbol, SymbolTableConstants.TYPE, false);
                }
                // rajouter des tests combine...
                if (previousDecl == null) {
                    addSymbolDecl(symbolDecl);
                }
            }
            addSymbolDecls = addSymbolDecls.tail;
        }
    }

    protected void solveTypeExtends() {
        TapList<SymbolDecl> curBucket;
        SymbolDecl symbolDecl;
        for (int i=NUM_BUCKETS-1 ; i>=0 ; --i) {
            curBucket = this.buckets[i].tail;
            while (curBucket != null) {
                symbolDecl = curBucket.head;
                if (symbolDecl.isA(SymbolTableConstants.TYPE)) {
                    WrapperTypeSpec type = ((TypeDecl)symbolDecl).typeSpec ;
                    if (TypeSpec.isA(type, SymbolTableConstants.COMPOSITETYPE)) {
                        ((CompositeTypeSpec)type.wrappedType).solveTypeExtends(this) ;
                    }
                }
                curBucket = curBucket.tail;
            }
        }
    }

    private Tree preTypeCheck(Tree instrOrExpr, boolean noFunctionCall,
                              Instruction instruction, ToBool isBeginning) {
        if (unit.isFortran()) {
            // Transform each arrayAccess which is indeed a call into a true call:
            if (instrOrExpr.opCode() == ILLang.op_arrayAccess
                    && !noFunctionCall && unit.fortranStuff() != null) {
                Tree newTree = unit.fortranStuff().checkArrayAccessOrCall(this, instrOrExpr);
                if (newTree != instrOrExpr && newTree != null) {
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.indentOnTrace(TapEnv.traceIndent());
                        TapEnv.printlnOnTrace("Understand " + instrOrExpr + " as function call " + newTree);
                    }
                    instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
                }
            }
            // Register each assignment which is indeed a statement function definition, and erase it:
            if (instrOrExpr.opCode() == ILLang.op_assign &&
                    isBeginning.get() && unit.fortranStuff() != null) {
                boolean isStmtFuncDecl = unit.fortranStuff().checkStatementFunctionDecl(this, instrOrExpr);
                if (isStmtFuncDecl) {
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.indentOnTrace(TapEnv.traceIndent());
                        TapEnv.printlnOnTrace("Skip Statement-Function def " + instrOrExpr + " -> none()");
                    }
                    instrOrExpr = instruction.replaceTree(instrOrExpr, ILUtils.build(ILLang.op_none));
                    if (instruction.preComments != null) {
                        unit.addLostComment(instruction.preComments);
                        instruction.preComments = null;
                    }
                    if (instruction.postComments != null) {
                        unit.addLostComment(instruction.postComments);
                        instruction.postComments = null;
                    }
                } else if (instruction.tree != null
                        && instruction.tree.opCode() != ILLang.op_varDeclaration) {
                    isBeginning.set(false);
                }
            }
            // Transform each call to a statement function into its expanded form:
            if (instrOrExpr.opCode() == ILLang.op_call &&
                    unit.fortranStuff() != null) {
                Tree newTree = unit.fortranStuff().checkStatementFunctionCall(instrOrExpr);
                if (newTree != instrOrExpr && newTree != null) {
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.indentOnTrace(TapEnv.traceIndent());
                        TapEnv.printlnOnTrace("Expand Statement-Function call " + instrOrExpr + " into " + newTree);
                    }
                    instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
                    instrOrExpr = preTypeCheck(instrOrExpr, false, instruction, isBeginning);
                }
            }

            // Transform c_loc(x) to address(x)
            if (unit.isFortran() && instrOrExpr.opCode() == ILLang.op_call
                    && instrOrExpr.down(2).opCode() == ILLang.op_ident
                    && instrOrExpr.down(2).stringValue().equals("c_loc")) {
                Tree newTree = ILUtils.build(ILLang.op_address, ILUtils.copy(instrOrExpr.down(3).down(1)));
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.indentOnTrace(TapEnv.traceIndent());
                    TapEnv.printlnOnTrace("Transform Fortran " + instrOrExpr + " into IL " + newTree);
                }
                newTree.setAnnotation("sourcetree", ILUtils.copy(instrOrExpr));
                instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
            }
            // Add the missing size and type info into each allocate:
            // fait apres le typeCheck, dans Block splitInstruction
            // addSizeAndTypeInAllocate(instrOrExpr);
        }

        if (unit.isC()) {
            Tree newTree = null;
            if (CStuff.isAnIoCall(instrOrExpr)) {
                newTree = CStuff.buildIOCall(instrOrExpr);
            } else if (CStuff.isAMalloc(instrOrExpr)) {
                // Semantics of the IL operator op_allocate:
                // op_allocate(<total_size_in_bytes>,
                //             op_expressions[<dimension_length>*],
                //             <elementary_type(not pointer, but destination type!)>,
                //             <control_name_eqs>
                //             <complete call of constructor function>)
                Tree originalInstrOrExpr = ILUtils.copy(instrOrExpr) ;
                // For a Cuda malloc, instrOrExpr is not an assign, but a call.
                // First transform it into something with the shape of an assign to a normal malloc:
                if (ILUtils.isCallingString(instrOrExpr, "cudaMalloc", true)) {
                    Tree[] arguments = ILUtils.getArguments(instrOrExpr).children() ;
                    Tree lhs = ILUtils.addPointerAccess(ILUtils.copy(arguments[0])) ;
                    Tree mallocTree = ILUtils.buildCall(null,
                                   ILUtils.build(ILLang.op_ident, "malloc"),
                                   ILUtils.build(ILLang.op_expressions,
                                     ILUtils.copy(arguments[1]))) ;
                    Tree newAssignMallocTree = ILUtils.build(ILLang.op_assign, lhs, mallocTree) ;
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.indentOnTrace(TapEnv.traceIndent());
                        TapEnv.printlnOnTrace("Transform Cuda " + instrOrExpr + " into C " + newAssignMallocTree);
                    }
                    instruction.setTree(newAssignMallocTree) ;
                    assignDestination = lhs ;
                    instrOrExpr = mallocTree ;
                }
                int typeSize = 0;
                if (assignDestination!=null) {
                    WrapperTypeSpec resultType = typeOf(assignDestination);
                    // Normally, resultType must be a pointer. Peel off this pointer level:
                    if (TypeSpec.isA(resultType, SymbolTableConstants.POINTERTYPE)) {
                        assert resultType != null;
                        resultType = ((PointerTypeSpec) resultType.wrappedType).destinationType;
                    }
                    // Then peel off every array type:
                    if (TypeSpec.isA(resultType, SymbolTableConstants.ARRAYTYPE)) {
                        assert resultType != null;
                        resultType = resultType.wrappedType.elementType();
                    }
                    if (resultType != null) {
                        typeSize = resultType.size();
                    }
                    if (typeSize < 0) typeSize = 0 ;
                }
                newTree = CStuff.buildAllocate(instrOrExpr, typeSize, this, instruction);
                newTree.setAnnotation("sourcetree", originalInstrOrExpr) ;
            } else if (CStuff.isAFree(instrOrExpr)) {
                Tree originalInstrOrExpr = ILUtils.copy(instrOrExpr) ;
                newTree = CStuff.buildDeallocate(instrOrExpr);
                newTree.setAnnotation("sourcetree", originalInstrOrExpr) ;
            } else if (CStuff.isAPow(this, instrOrExpr)) {
                // op_call("pow",...) => op_power
                newTree = CStuff.buildPow(instrOrExpr);
            } else if (CStuff.isaModifiedFloatFloat(instrOrExpr)) {
                newTree = ILUtils.copy(instrOrExpr.down(2)) ;
            }
            if (newTree != null) {
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.indentOnTrace(TapEnv.traceIndent());
                    TapEnv.printlnOnTrace("Transform C " + instrOrExpr + " into IL " + newTree);
                }
                instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
            }
        }

        return instrOrExpr;
    }
// [End FunctionDecl mode.]

//     private WrapperTypeSpec setReturnTypeIfFunction(Tree instrOrExpr,
//             String functionName, Unit functionUnit, FunctionDecl functionDecl,
//             WrapperTypeSpec returnType) {
//         // When a result is expected, and return type
//         // isn't known, use implicit rules:
//         FunctionTypeSpec declaredReturnType = functionDecl.functionTypeSpec();
//         if (declaredReturnType != null) {
//             returnType = declaredReturnType.returnType;
//         }
//         if (returnType == null || returnType.actualTypeSpec == null) {
//             WrapperTypeSpec implicitType = getImplicitTypeSpec(functionName);
//             if ((implicitType != null) &&
//                 (implicitType.actualTypeSpec != null)) {
//                 returnType.actualTypeSpec = implicitType.actualTypeSpec;
//                 //returnType = implicitType;
//                 if (!functionUnit.isIntrinsic()) {
//                      TapEnv.fileWarning(TapEnv.MSG_15, instrOrExpr.down(1), "(TC37) Return type of function "+functionName+" set by implicit rule to "+returnType.actualTypeSpec.showType()) ;
//                 }
//             }
//         }
//         return returnType;
//     }

    /**
     * Type-checks expression "instrOrExpr", which is an
     * atomic instruction or an expression, in the context of
     * this SymbolTable. @return the computed type for "instrOrExpr",
     * or null when it has no meaningful type, or when type-checking
     * returned an error. Caution: the returned type may also be
     * (or contain) WrapperTypeSpec(null), which is completely different, and
     * represents an unknown type, that may be infered by side-effect later.
     *
     * @param instrOrExpr    the expression that must be type-checked.
     * @param expectResult   true when the expression is expected to return a value by its context.
     * @param noFunctionCall true means caller guarantees that the expression is in a
     *                       place that cannot be a function call (e.g. left of assign, overwritten...)
     * @param onType         true when the expression is a type.
     * @param instruction    the Instruction that contains the given expression instrOrExpr.
     * @param isBeginning    true when the place of the instruction is such that
     *                       it may be the definition of a statement-function.
     * @return the type of the given instrOrExpr.
     */
    protected WrapperTypeSpec typeCheck(Tree instrOrExpr, boolean expectResult, boolean noFunctionCall, boolean onType,
                                        Instruction instruction, ToBool isBeginning, Unit currentUnit) {
        // [llh 22Fev2018] TODO: regroup (expectResult,noFunctionCall,onType,...) into a bitset(int) "context" argument.
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("TypeCheck of " + instrOrExpr);
            TapEnv.setTraceIndent(TapEnv.traceIndent() + 2);
        }
        WrapperTypeSpec returnedType = null;
        if (unit != null) {
            instrOrExpr = preTypeCheck(instrOrExpr, noFunctionCall, instruction, isBeginning);
        }
        switch (instrOrExpr.opCode()) {
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_if: {
                WrapperTypeSpec testType;
                testType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                if (this.unit != null && !this.unit.isC() && !checkScalarBoolean(testType)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC02) Expression is expected to be boolean, and is here " + testType.showType());
                }
                break;
            }
            case ILLang.op_ifExpression: {
                WrapperTypeSpec testType;
                testType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                if (this.unit != null && !this.unit.isC() && !checkScalarBoolean(testType)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC02) Expression is expected to be boolean, and is here " + testType.showType());
                }
                WrapperTypeSpec typeSpec2 = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                WrapperTypeSpec typeSpec3 = typeCheck(instrOrExpr.down(3), true, false, false, instruction, isBeginning, currentUnit);
                // todo comparer typeSpec2 et typeSpec3
                returnedType = typeSpec2;
                break;
            }
            case ILLang.op_where: {
                WrapperTypeSpec testType;
                testType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                if (this.unit != null && !this.unit.isC() && !checkScalarArrayBoolean(testType)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC02) Expression is expected to be boolean, and is here " + testType.showType());
                }
                break;
            }
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                // [llh] TODO: maybe type-check uses inside the OpenMP clauses
                break;
            case ILLang.op_loop:
                typeCheck(instrOrExpr.down(3), false, false, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_forall: {
                TapEnv.toolWarning(-1, "(DD99) Warning: feature of Fortran 90/95 not supported: forall");
                Tree[] ranges = instrOrExpr.down(1).children();
                for (Tree range : ranges) {
                    typeCheck(range, false, false, false, instruction, isBeginning, currentUnit);
                }
                typeCheck(instrOrExpr.down(2), false, false, false, instruction, isBeginning, currentUnit);
                break;
            }
            case ILLang.op_forallRangeTriplet:
            case ILLang.op_do: {
                WrapperTypeSpec type;
                type = typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                if (!checkScalarNumericInt(type)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC06) Loop index is expected to be integer, and is here " + type.showType());
                }
                for (int i = 2; i < 5; i++) {
                    if (instrOrExpr.down(i).opCode() != ILLang.op_none) {
                        type = typeCheck(instrOrExpr.down(i), true, false, false, instruction, isBeginning, currentUnit);
                        if (!checkScalarNumericInt(type)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(i), "(TC07) Loop bound is expected to be integer, and is here " + type.showType());
                        }
                    }
                }
                break;
            }
            case ILLang.op_forallRangeSet:
                typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                typeCheck(instrOrExpr.down(2), true, true, false, instruction, isBeginning, currentUnit);
                // TODO check that 2nd child's type is a "set" of 1st child's type.
                break;
            case ILLang.op_for: {
                WrapperTypeSpec type;
                type = typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                if (!checkScalarNumericInt(type) && !TypeSpec.isA(type, SymbolTableConstants.POINTERTYPE)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC06) Loop index is expected to be integer, and is here " + type.showType());
                }
                typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                typeCheck(instrOrExpr.down(3), true, false, false, instruction, isBeginning, currentUnit);
                break;
            }
            case ILLang.op_times: {
                WrapperTypeSpec expType;
                expType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                expType = checkPointerInFortranExpression(expType, instrOrExpr, 1);
                if (!checkScalarNumericInt(expType)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC08) Loop times expression is expected to be integer, and is here " + expType.showType());
                }
                break;
            }
            case ILLang.op_nameEq:
                returnedType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_return:
                typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                returnedType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_format:
            case ILLang.op_break:
            case ILLang.op_continue:
            case ILLang.op_stop:
            case ILLang.op_throw:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_compGoto:
            case ILLang.op_assignLabelVar:
            case ILLang.op_statementFunctionDeclaration:
            case ILLang.op_none:
            case ILLang.op_formatElem:
            case ILLang.op_directive:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_star:
            case ILLang.op_functionType:
                break;
            case ILLang.op_iterativeVariableRef: {
                WrapperTypeSpec topTypeSpec = typeCheck(instrOrExpr.down(1), true, noFunctionCall, false, instruction, isBeginning, currentUnit);
                typeCheck(instrOrExpr.down(2), true, noFunctionCall, false, instruction, isBeginning, currentUnit);
                Tree doTree = instrOrExpr.down(2);
                Tree sizeTree = ILUtils.buildSizeTree(doTree.down(2), doTree.down(3), doTree.down(4));
                ArrayDim[] newDimensions = new ArrayDim[1];
                newDimensions[0] = new ArrayDim(ILUtils.build(ILLang.op_dimColon),
                        1, computeIntConstant(sizeTree), null, -1, 0, 0);
                returnedType = new WrapperTypeSpec(new ArrayTypeSpec(topTypeSpec, newDimensions));
                break;
            }
            case ILLang.op_pointerAssign: {
                // Temporary operator that occurs in Fortran90 only.
                // At this place, it is replaced by an op_assign with the appropriate pointer derefs.
                WrapperTypeSpec lhsType;
                WrapperTypeSpec rhsType;
                FunctionDecl functionDecl = null;
                InterfaceDecl interfaceDecl = null;
                // Transform the pointerAssign into assign operator
                Tree newTree = ILUtils.build(ILLang.op_assign,
                        instrOrExpr.cutChild(1),
                        instrOrExpr.cutChild(2));
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.indentOnTrace(TapEnv.traceIndent());
                    TapEnv.printlnOnTrace("Transform Fortran " + instrOrExpr + " into IL assign " + newTree);
                }
                instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
                // Update the initialization tree that is kept in the SymbolDecl:
                String lhsBaseName = ILUtils.baseName(instrOrExpr.down(1));
                if (lhsBaseName != null) {
                    if (inCompositeTypeSpec != null
                            && inCompositeTypeSpec.namedFieldType(lhsBaseName) != null) {
                        FieldDecl initializationSymbolDecl =
                                inCompositeTypeSpec.namedFieldDecl(lhsBaseName);
                        if (initializationSymbolDecl != null) {
                            initializationSymbolDecl.setInitializationTree(instrOrExpr);
                        }
                    } else if (instruction.tree.opCode() == ILLang.op_varDeclaration) {
                        VariableDecl initializationSymbolDecl =
                                getVariableOrConstantDecl(lhsBaseName);
                        // seulement dans une op_varDeclaration
                        if (initializationSymbolDecl != null) {
                            initializationSymbolDecl.setInitializationTree(instrOrExpr);
                        }
                    }
                }

                lhsType =
                        typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                assignDestination = instrOrExpr.down(1);
                if (assignDestination.opCode() == ILLang.op_arrayDeclarator ||
                        assignDestination.opCode() == ILLang.op_pointerDeclarator ||
                        assignDestination.opCode() == ILLang.op_functionDeclarator ||
                        assignDestination.opCode() == ILLang.op_modifiedDeclarator) {
                    // When in a declaration e.g. "float *p = malloc() ;",
                    // the assignment that takes place is in fact: "p = malloc();" :
                    assignDestination = ILUtils.baseTree(assignDestination);
                }
                rhsType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                assignDestination = null;
                if (lhsType != null && rhsType != null) {
                    boolean pointerAssignOk;
                    if (TypeSpec.isA(lhsType, SymbolTableConstants.POINTERTYPE) && TypeSpec.isA(rhsType, SymbolTableConstants.POINTERTYPE)) {
                        pointerAssignOk = lhsType.receives(rhsType, null, null);
                    } else {
                        WrapperTypeSpec lhsBaseTypeSpec = lhsType;
                        if (TypeSpec.isA(lhsType, SymbolTableConstants.POINTERTYPE)) {
                            lhsBaseTypeSpec = ((PointerTypeSpec) lhsType.wrappedType).destinationType;
                        } else {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC60) " + ILUtils.toString(instrOrExpr.down(1)) + " must be a pointer");
                        }
                        WrapperTypeSpec rhsBaseTypeSpec = rhsType;
                        if (TypeSpec.isA(rhsType, SymbolTableConstants.POINTERTYPE)) {
                            rhsBaseTypeSpec = ((PointerTypeSpec) rhsType.wrappedType).destinationType;
                        }
                        pointerAssignOk = lhsBaseTypeSpec.receives(rhsBaseTypeSpec, null, null);
                    }
                    if (!pointerAssignOk) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC16) Type mismatch in pointer assignment: " + lhsType.showType() + " receives " + rhsType.showType());
                    }

                    if (instrOrExpr.down(2).opCode() == ILLang.op_pointerAccess) {
                        Tree withoutPointerAccessTree = ILUtils.copy(instrOrExpr
                                .down(2).down(1));
                        instrOrExpr.setChild(withoutPointerAccessTree, instrOrExpr
                                .down(2).rankInParent());
                    } else {
                        if (!TypeSpec.isA(rhsType, SymbolTableConstants.POINTERTYPE)) {
                            if (ILUtils.isAVarRef(instrOrExpr.down(2), this)) {
                                Tree addressTree = ILUtils.build(ILLang.op_address,
                                        ILUtils.copy(instrOrExpr.down(2)));
                                instrOrExpr.setChild(addressTree, instrOrExpr.down(2)
                                        .rankInParent());
                            } else {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC60) Cannot take the address of non-reference expression " + ILUtils.toString(instrOrExpr.down(2)));
                            }
                        }
                    }

                    TapList<InterfaceDecl> interfaceDecls = getAllInterfaceDecls(instrOrExpr.opName());
                    // TODO: Peut-on avoir plusieurs interfaceDecls???
                    if (interfaceDecls != null) {
                        interfaceDecl = interfaceDecls.head;
                    }
                    if (interfaceDecl != null) {
                        WrapperTypeSpec[] types = new WrapperTypeSpec[2];
                        types[0] = lhsType;
                        types[1] = rhsType;
                        functionDecl = interfaceDecl.findFunctionDecl(types, null, instrOrExpr);
                    }
                    if (functionDecl != null) {
                        Tree newBinaryTree = ILUtils.build(ILLang.op_binary,
                                instrOrExpr.down(1).copy(),
                                ILUtils.build(ILLang.op_ident, instrOrExpr.opName()),
                                instrOrExpr.down(2).copy());
                        newTree = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_none),
                                ILUtils.build(ILLang.op_ident, instrOrExpr.opName()),
                                ILUtils.build(ILLang.op_expressions,
                                        instrOrExpr.down(1).copy(),
                                        instrOrExpr.down(2).copy()));
                        if (TapEnv.traceTypeCheckAnalysis()) {
                            TapEnv.indentOnTrace(TapEnv.traceIndent());
                            TapEnv.printlnOnTrace("Replace " + instrOrExpr + " with explicit call " + newTree);
                        }
                        instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
                        instrOrExpr.setAnnotation("sourcetree", newBinaryTree);
                        CallArrow callArrow = CallGraph.addCallArrow(unit, SymbolTableConstants.CALLS, functionDecl.unit());
                        callArrow.setSrcCallName("pointerAssign");
                        instrOrExpr.setAnnotation("callArrow", callArrow);
                        returnedType = functionDecl.functionTypeSpec().returnType;
                    }
                } else {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC16) Type mismatch in pointer assignment: " + (lhsType == null ? "?" : lhsType.showType()) + " receives " + (rhsType == null ? "?" : rhsType.showType()));
                }
                if (returnedType == null) {
                    returnedType = lhsType != null ? lhsType : rhsType;
                }
                break;
            }
            case ILLang.op_assign: {
                WrapperTypeSpec lhsType =
                        typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                InterfaceDecl interfaceDecl;
                TapList<FunctionDecl> functionDecls;
                FunctionDecl functionDecl = null;
                assignDestination = instrOrExpr.down(1);
                if (assignDestination.opCode() == ILLang.op_arrayDeclarator ||
                    assignDestination.opCode() == ILLang.op_pointerDeclarator ||
                    assignDestination.opCode() == ILLang.op_functionDeclarator ||
                    assignDestination.opCode() == ILLang.op_modifiedDeclarator) {
                    // When in a declaration e.g. "float *p = malloc() ;",
                    // the assignment that takes place is in fact: "p = malloc();" :
                    assignDestination = ILUtils.baseTree(assignDestination);
                }
                WrapperTypeSpec rhsType =
                        typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                if (unit != null
                        && language == TapEnv.FORTRAN
                        && TypeSpec.isA(lhsType, SymbolTableConstants.ARRAYTYPE) && !unit.isIntrinsic()
                        && !isCallNotElemental(instrOrExpr.down(2))
                        && instrOrExpr.down(2).opCode() != ILLang.op_arrayConstructor
                        && !lhsType.isString() && !lhsType.isCharacter()) {
                    TapEnv.warningFortran77WithFortran90Features(unit) ;
                }
                assignDestination = null;

                lhsType = checkPointerInFortranExpression(lhsType, instrOrExpr, 1);
                rhsType = checkPointerInFortranExpression(rhsType, instrOrExpr, 2);
                if (lhsType != null && rhsType != null) {
                    // overloading?
                    TapList<InterfaceDecl> interfaceDecls = getAllInterfaceDecls(instrOrExpr.opName());
                    WrapperTypeSpec[] types = new WrapperTypeSpec[2];
                    types[0] = lhsType;
                    types[1] = rhsType;
                    while (interfaceDecls != null && functionDecl == null) {
                        interfaceDecl = interfaceDecls.head;
                        functionDecl = interfaceDecl.findFunctionDecl(types, null, instrOrExpr);
                        interfaceDecls = interfaceDecls.tail;
                    }
                }

                if (lhsType != null && rhsType != null) {
                    if (functionDecl != null) {
                        Tree newBinaryTree = ILUtils.build(ILLang.op_binary,
                                instrOrExpr.down(1).copy(),
                                ILUtils.build(ILLang.op_ident, instrOrExpr.opName()),
                                instrOrExpr.down(2).copy());
                        Tree newTree = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_none),
                                ILUtils.build(ILLang.op_ident, instrOrExpr.opName()),
                                ILUtils.build(ILLang.op_expressions,
                                        instrOrExpr.down(1).copy(),
                                        instrOrExpr.down(2).copy()));
                        if (TapEnv.traceTypeCheckAnalysis()) {
                            TapEnv.indentOnTrace(TapEnv.traceIndent());
                            TapEnv.printlnOnTrace("Replace " + instrOrExpr + " with explicit call " + newTree);
                        }
                        instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
                        instrOrExpr.setAnnotation("sourcetree", newBinaryTree);
                        CallArrow callArrow = CallGraph.addCallArrow(unit, SymbolTableConstants.CALLS, functionDecl.unit());
                        callArrow.setSrcCallName("assign");
                        instrOrExpr.setAnnotation("callArrow", callArrow);
                        returnedType = functionDecl.functionTypeSpec().returnType;
                        break;
                    } else if (!lhsType.receives(rhsType, null, null)) {
                        if (!(instruction != null && instruction.tree != null
                                && instruction.tree.opCode() != ILLang.op_varDeclaration
                                && TypeSpec.isA(lhsType, SymbolTableConstants.ENUMTYPE))) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC16) Type mismatch in assignment: " + lhsType.showType() + " receives " + rhsType.showType());
                            returnedType = null;
                        } else {
                            returnedType = lhsType;
                        }
                        break;
                    }
                } else {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC16) Type mismatch in assignment: " + (lhsType == null ? "?" : lhsType.showType()) + " receives " + (rhsType == null ? "?" : rhsType.showType()));
                }

                if ((TapEnv.relatedLanguageIsC() || TapEnv.relatedLanguageIsCPLUSPLUS()) &&
                        WrapperTypeSpec.isFunctionOrPointerToFunction(lhsType)) {
                    String funcVarName = ILUtils.baseName(instrOrExpr.down(1));
                    functionDecls = getFunctionDecl(funcVarName, null, null, false);
                    functionDecl = functionDecls == null ? null : functionDecls.head;
                    VariableDecl funcVarDecl = null;
                    if (functionDecl == null) {
                        funcVarDecl = getFuncVarDecl(funcVarName);
                    }
                    String rightName = ILUtils.baseName(instrOrExpr.down(2));
                    FunctionDecl initFunDecl = null;
                    TapList<FunctionDecl> initFunDecls;
                    if (rightName != null) {
                        initFunDecls = getFunctionDecl(rightName, null, null, false);
                        initFunDecl = initFunDecls == null ? null : initFunDecls.head;
                    }
                    if (instrOrExpr.down(2).opCode() == ILLang.op_ident
                            && initFunDecl != null && (functionDecl != null || funcVarDecl != null)) {
                        if (functionDecl != null) {
                            functionDecl.initFunctionDecls = new FunctionDecl[]{initFunDecl};
                        } else if (funcVarDecl != null) {
                            funcVarDecl.initFunctionDecls = new FunctionDecl[]{initFunDecl};
                        }
//                     } else {
//                         TapEnv.toolWarning(-1, "(DD99) Warning: feature of C/C++ not supported: function assignment " + ILUtils.toString(instrOrExpr));
                    }
                }
                returnedType = lhsType != null ? lhsType : rhsType;
                break;
            }
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_modAssign:
            case ILLang.op_bitAndAssign:
            case ILLang.op_bitOrAssign:
            case ILLang.op_bitXorAssign:
            case ILLang.op_leftShiftAssign:
            case ILLang.op_rightShiftAssign: {
                int opRank;
                switch (instrOrExpr.opCode()) {
                    case ILLang.op_plusAssign:
                        opRank = ILLang.op_add;
                        break;
                    case ILLang.op_minusAssign:
                        opRank = ILLang.op_sub;
                        break;
                    case ILLang.op_timesAssign:
                        opRank = ILLang.op_mul;
                        break;
                    case ILLang.op_divAssign:
                        opRank = ILLang.op_div;
                        break;
                    case ILLang.op_modAssign:
                        opRank = ILLang.op_mod;
                        break;
                    case ILLang.op_bitAndAssign:
                        opRank = ILLang.op_bitAnd;
                        break;
                    case ILLang.op_bitOrAssign:
                        opRank = ILLang.op_bitOr;
                        break;
                    case ILLang.op_bitXorAssign:
                        opRank = ILLang.op_bitXor;
                        break;
                    case ILLang.op_leftShiftAssign:
                        opRank = ILLang.op_leftShift;
                        break;
                    case ILLang.op_rightShiftAssign:
                        opRank = ILLang.op_rightShift;
                        break;
                    default:
                        opRank = ILLang.op_add;
                        break;
                }
                WrapperTypeSpec lhsType = typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                WrapperTypeSpec rhsType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                if (lhsType != null && rhsType != null
                        && canCombineNumeric(lhsType, rhsType, instrOrExpr) == null) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC16) Type mismatch in op-assignment: " + lhsType.showType() + " receives " + rhsType.showType());
                }
                Tree oldTree = instrOrExpr;
                Tree newTree = ILUtils.build(ILLang.op_assign,
                        instrOrExpr.down(1).copy(),
                        ILUtils.build(opRank,
                                instrOrExpr.down(1).copy(),
                                instrOrExpr.down(2).copy()));
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.indentOnTrace(TapEnv.traceIndent());
                    TapEnv.printlnOnTrace("Expand " + instrOrExpr + " into explicit " + newTree);
                }
                instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
                instrOrExpr.setAnnotation("sourcetree", oldTree);
                returnedType = lhsType != null ? lhsType : rhsType;
                break;
            }
            case ILLang.op_switch: {
                WrapperTypeSpec switchType;
                WrapperTypeSpec caseType;
                switchType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                Tree[] switchCasesTree = instrOrExpr.down(2).children();
                int numberExp;
                for (Tree tree : switchCasesTree) {
                    Tree[] expressions = tree.down(1).children();
                    numberExp = expressions.length;
                    for (int j = 0; j < numberExp; j++) {
                        caseType = typeCheck(expressions[j], true, noFunctionCall, false, instruction, isBeginning, currentUnit);
                        if (caseType != null && switchType != null &&
                                !TypeSpec.canCompare(switchType, caseType)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC17) Type mismatch in case: " + switchType.showType() + " compared to " + caseType.showType());
                        }
                    }
                }
                returnedType = switchType;
                break;
            }
            case ILLang.op_call: {
                //TC CALL 0: Start for this call: Type-check actual arguments and obtain their actual types, plus actual expected return type :
                String functionName = ILUtils.getCallFunctionNameString(instrOrExpr);
                if (functionName!=null) {
                    TypeSpec funcNameType = typeOf(ILUtils.getCalledName(instrOrExpr)) ;
                    if (funcNameType!=null && funcNameType.isPointerToFunction()) {
                        functionName = null;
                    }
                }
                boolean isUnknownFunctionName = false ;
                if (functionName == null) {
                    functionName = "UnknownProcedureName" ;
                    isUnknownFunctionName = true ;
                }
                if (functionName.startsWith("operator") && TapEnv.relatedLanguageIsCPLUSPLUS()) {
                    TapEnv.toolError("Type-checking of operators (" + functionName + ") is not yet implemented");
                    break;
                }
                if (unit != null && !isUnknownFunctionName) {
                    // If this is a MPI call, prepare the default communication channel and attach it to this call.
                    // If there is a "$AD CHANNEL" directive attached to this instruction, it will be analyzed later
                    // and the new channel will be created then and attached to this call instead of the default.
                    MPIcallInfo messagePassingInfo =
                            MPIcallInfo.getMessagePassingMPIcallInfo(functionName, instrOrExpr,
                                                                     language, instruction.block);
                    if (messagePassingInfo != null && messagePassingInfo.isPointToPoint()) {
                        messagePassingInfo.registerMessagePassingChannel(unit.callGraph());
                    }
                }
                Tree[] actualArgs = ILUtils.getArguments(instrOrExpr).children();
                int nbActualArgs = actualArgs.length;
                WrapperTypeSpec[] actualArgsTypes = new WrapperTypeSpec[nbActualArgs];
                WrapperTypeSpec[] nameEqTypes = new WrapperTypeSpec[nbActualArgs];
                int rankOfFirstNameEq = -1;
                for (int i = 0; i < nbActualArgs; i++) {
                    WrapperTypeSpec actualArgType =
                            typeCheck(actualArgs[i], true, false, false, instruction, isBeginning, currentUnit);
                    actualArgType =
                            arrayTypeIfElementalCall(instrOrExpr, i, actualArgType);
                    if (actualArgs[i].opCode() == ILLang.op_nameEq) {
                        if (rankOfFirstNameEq == -1) {
                            rankOfFirstNameEq = i;
                        }
                        nameEqTypes[i] = actualArgType;
                        actualArgsTypes[i] = null;
                    } else if (actualArgs[i].opCode() == ILLang.op_none) {
                        // optional args in C++
                        actualArgsTypes[i] = new WrapperTypeSpec(null);
                    } else {
                        actualArgsTypes[i] = actualArgType;
                    }
                }
                if (rankOfFirstNameEq == -1) {
                    rankOfFirstNameEq = nbActualArgs;
                }
                WrapperTypeSpec actualReturnTypeExpected = null;
                if (instrOrExpr.parent() != null
                        && (instrOrExpr.parent().opCode() == ILLang.op_assign
                        || instrOrExpr.parent().opCode() == ILLang.op_pointerAssign)) {
                    actualReturnTypeExpected = typeOf(instrOrExpr.parent().down(1));
                }

                //TC CALL 1: Find the local declared SymbolDecl of the called function, either as "localFunctionDecl"
                //           or as "localInterfaceDecl". Also find the global declared SymbolDecl as "functionDecl".
                //           Strangely, in the case of a varFunction, create and install the global functionDecl
                //           and the varFunctionUnit (apparently possible duplication bug there!)

                //The following must change for Object-Oriented and/or overloading.
                // The function must be searched with a getFunctionDecl(functionName, actualArgsTypes)
                SymbolTable currentSymbolTable = this;
                TapList<FunctionDecl> localFunctionDecls = null;
                InterfaceDecl localInterfaceDecl = null;
                TapList<FunctionDecl> functionDecls = null;                // common to C/C++ and Fortran
                FunctionDecl functionDecl = null;                        // only for C/Fortran research
                Unit[] functionUnits = null;
                boolean needVarDeclInstruction = true;
                FunctionTypeSpec[] functionTypeSpecs = null;        // only one functionTypeSpec for all functions found but different pointers
                boolean isMixedLanguageCall = false;
                Tree objectTree = ILUtils.getObject(instrOrExpr);
                Unit objectClassUnit = null;

                WrapperTypeSpec objectType = typeCheck(objectTree, false, false, false, instruction, isBeginning, currentUnit);

                if (TypeSpec.isA(objectType, SymbolTableConstants.CLASSTYPE)) {
                    objectClassUnit = ((ClassTypeSpec) objectType.wrappedType).unit;
                }
                if (objectClassUnit != null) {
                    // special case for "this"
                    if (ILUtils.isIdent(objectTree, "this", true)) {
                        currentSymbolTable = objectClassUnit.privateSymbolTable();
                    } else {
                        currentSymbolTable = objectClassUnit.publicSymbolTable();
                    }
                } else {
                    Tree calledName = ILUtils.getCalledName(instrOrExpr);
                    if (calledName.opCode() == ILLang.op_scopeAccess) {
                        currentSymbolTable = getPrefixedNamedScope(calledName.down(1));
                    }
                }

                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.indentOnTrace(TapEnv.traceIndent());
                    TapEnv.printlnOnTrace("Search for the function declaration(s) that match call " + functionName
                            + new FunctionTypeSpec(actualReturnTypeExpected, actualArgsTypes)
                            + " on object " + objectTree + ":" + objectClassUnit
                            + " in SymbolTable:" + currentSymbolTable.addressChain());
                    TapEnv.setTraceIndent(TapEnv.traceIndent() + 2);
                }

                // First look for the duplicate FunctionDecl that exists for each procedure called
                // by a procedure P, and which is located in the private/local SymbolTable of P.
                FunctionDecl mixedLanguageFunctionDecl = null;
                boolean symbolFoundAsParam = false;
                if (!isUnknownFunctionName) {
                    if (unit != null && unit.isCPlusPlus()) {
                        if (currentSymbolTable == null) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, ILUtils.getCalledName(instrOrExpr), "(TC67) Wrong access to the called function " + functionName);
                            returnedType = actualReturnTypeExpected;
                            break;
                        }
                        // On regarde d'abord si des fonctions matchent (avec les memes types d'args, opt args inclus)
                        //  dans la table des symboles actuelle et celles des classes filles.
                        SymbolTable tmpSymbolTable = currentSymbolTable;
                        // TODO[bd] En cas d'heritage, il faut prendre en compte l'ambiguite des methodes
                        // (methodes definies dans une classe parente mais pas dans une autre et on tente
                        // d'y acceder par cette derniere) => check scopeAccess sur le deuxieme fils

                        localFunctionDecls = 
                                currentSymbolTable.getAllPossibleTypedFunctionDecl(functionName, actualReturnTypeExpected,
                                        actualArgsTypes, tmpSymbolTable, null, true, true, false, false, null, false);
                        if (localFunctionDecls == null) {
                            // Si on ne trouve rien, on regarde dans la table des parents
                            tmpSymbolTable = currentSymbolTable;
                            while (tmpSymbolTable.basisSymbolTable != null && localFunctionDecls == null) {
                                tmpSymbolTable = tmpSymbolTable.basisSymbolTable;
                                localFunctionDecls =
                                        currentSymbolTable.getAllPossibleTypedFunctionDecl(functionName, actualReturnTypeExpected,
                                                actualArgsTypes, tmpSymbolTable, null, true, true, false, true, null, false);
                            }
                        }
                    } else {
                        while (localFunctionDecls == null && localInterfaceDecl == null
                                && currentSymbolTable.basisSymbolTable != null && !currentSymbolTable.isFormalParams) {
                            FunctionDecl localFunctionDecl =
                                    currentSymbolTable.getTypedFunctionDecl(functionName, actualReturnTypeExpected, actualArgsTypes,
                                            true, instrOrExpr);
                            if (localFunctionDecl != null && localFunctionDecl.kind() != SymbolTableConstants.INTERFACE) {
                                localFunctionDecls = new TapList<>(localFunctionDecl, localFunctionDecls);
                            }

                            localInterfaceDecl = currentSymbolTable.getTopInterfaceDecl(functionName);
                            if (localInterfaceDecl == null) {
                                TapList<InterfaceDecl> interfaceDecls = getAllInterfaceDecls(functionName);
                                if (interfaceDecls != null) {
                                    localFunctionDecls = null;
                                }
                            }
                            currentSymbolTable = currentSymbolTable.basisSymbolTable;
                        }
                    }
                    // En mixed C et Fortran, il n'y a, au plus, qu'une localFunction possible.
                    // fonction fortran appelee depuis c declaree extern localement:
                    mixedLanguageFunctionDecl = mixedLanguageCall(functionName,
                                                                (localFunctionDecls == null ? null : localFunctionDecls.head));
                    isMixedLanguageCall = (mixedLanguageFunctionDecl!=null) ;

                    while (currentSymbolTable.basisSymbolTable != null && functionDecls == null) {

                        // if we didn't find the duplicate FunctionDecl, look now for the global SymbolDecl.
                        // We first look for a declaration of this name as a formal parameter which holds a function name.
                        // !!! pour un module, ce n'est pas un "parameter" mais un symbolDecl dans la publicSymbolTable
                        SymbolDecl paramSymbolDecl = null;
                        if (currentSymbolTable.isFormalParams && !currentSymbolTable.unit.isModule()) {
                            paramSymbolDecl = currentSymbolTable.getTopSymbolDecl(functionName);
                            if (paramSymbolDecl == null) {
                                paramSymbolDecl = currentSymbolTable.getTopInterfaceDecl(functionName);
                            }
                        }
                        if (paramSymbolDecl != null) {
                            symbolFoundAsParam = true;
                            if (paramSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                                functionDecl = (FunctionDecl) paramSymbolDecl;
                            } else if (paramSymbolDecl.isA(SymbolTableConstants.INTERFACE)) {
                                // on est dans le cas VarFunctionUnit
                                functionDecl = ((InterfaceDecl) paramSymbolDecl).functionDecl();
                                // Setting functionTypeSpec will prevent adding a local declaration of
                                // the called unit in the declaration instructions of the current Unit:
                                if (functionDecl.unit().isTotallyDeclared) {
                                    functionTypeSpecs = new FunctionTypeSpec[]{functionDecl.functionTypeSpec()};
                                } else {
                                    functionTypeSpecs = new FunctionTypeSpec[]{new FunctionTypeSpec(new WrapperTypeSpec(null))};
                                }
                                Unit varFunctionUnit = Unit.makeVarFunction(functionName, unit);
                                varFunctionUnit.setFunctionTypeSpec(functionTypeSpecs[0]);
                                functionDecl = new FunctionDecl(ILUtils.getCalledName(instrOrExpr), varFunctionUnit);
                                functionDecl.setInterfaceInstr(true);
                                if (unit != null && unit.isFortran()) {
                                    currentSymbolTable.addSymbolDecl(functionDecl);
                                }
                            } else if (paramSymbolDecl.isA(SymbolTableConstants.VARIABLE) && !paramSymbolDecl.symbol.equals(this.unit.name())) {
                                if (TypeSpec.isA(paramSymbolDecl.type(), SymbolTableConstants.POINTERTYPE)
                                    && TypeSpec.isA(((PointerTypeSpec)paramSymbolDecl.type().wrappedType()).destinationType, SymbolTableConstants.FUNCTIONTYPE)) {
                                    // F2003-style variable of type pointer to function:
                                    // Build a temporary dummy FunctionDecl
                                    Unit dummyUnit = Unit.makeDummyUnit(functionName, unit.callGraph(), language) ;
                                    dummyUnit.dummyAccessedAs = instrOrExpr ;
                                    dummyUnit.setFunctionTypeSpec((FunctionTypeSpec)((PointerTypeSpec)paramSymbolDecl.type().wrappedType()).destinationType.wrappedType()) ;
                                    functionDecl = new FunctionDecl(ILUtils.getCalledName(instrOrExpr), dummyUnit) ;
                                } else {
                                    // F77-style VARFUNCTION's
                                    Unit varFunctionUnit = Unit.makeVarFunction(functionName, unit);
                                    varFunctionUnit.setFunctionTypeSpec(
                                        new FunctionTypeSpec(paramSymbolDecl.type()));
                                    functionDecl = new FunctionDecl(ILUtils.getCalledName(instrOrExpr), varFunctionUnit);
                                    if (unit != null && unit.isFortran()) {
                                        currentSymbolTable.addSymbolDecl(functionDecl);
                                    }
                                }
                            }
                            if (functionDecl != null) {
                                functionDecls = new TapList<>(functionDecl, null);
                            }
                        } else {
                            // if we still didn't find the duplicate FunctionDecl, we look for the
                            // principal FunctionDecl, which is found either in the root SymbolTable
                            // or in the private or public SymbolTable of the module defining the procedure
                            // or in the copied public SymbolTables of USE'd modules.
                            if (functionDecls == null) {
                                InterfaceDecl interfaceDecl = currentSymbolTable.getTopInterfaceDecl(functionName);
                                if (interfaceDecl != null) {
                                    functionDecl = interfaceDecl.findFunctionDecl(actualArgsTypes, null, instrOrExpr);
                                    if (functionDecl != null) {
                                        functionDecls = new TapList<>(functionDecl, null);
                                    }
                                }
                            }
                            if (functionDecls == null) {
                                functionDecls = currentSymbolTable.getTopFunctionDecl(functionName, actualReturnTypeExpected, actualArgsTypes, null, true);
                                // Patch: when we get several functionDecls while looking up in the
                                // (language) root SymbolTable, we want no overloading permitted,
                                // so we will keep only the one that "matches exactly" (cf InlinedFunctions),
                                // or the first one. This is only to remain consistent with the inlining
                                // mechanism, and could be cleaned up...
                                if (TapList.length(functionDecls)>1
                                    && (currentSymbolTable == getCallGraph().fortranRootSymbolTable()
                                        || currentSymbolTable == getCallGraph().globalRootSymbolTable())) {
                                    FunctionDecl exactMatch = null ;
                                    TapList<FunctionDecl> inMatches = functionDecls ;
                                    while (exactMatch==null && inMatches!=null) {
                                        if (inMatches.head.functionTypeSpec().matchesCallExactly(actualReturnTypeExpected,
                                                                                                 actualArgsTypes)
                                            && !FunctionTypeSpec.complexAbsAmbiguous(functionName,
                                                         inMatches.head.functionTypeSpec().argumentsTypes, actualArgsTypes)) {
                                            exactMatch = inMatches.head ;
                                        }
                                            inMatches = inMatches.tail ;
                                    }
                                    if (exactMatch==null) {
                                        functionDecls = new TapList<>(functionDecls.head, null) ;
                                    } else {
                                        functionDecls = new TapList<>(exactMatch, null) ;
                                    }
                                }
                            }
                            if (functionDecls == null) {
                                functionDecls = currentSymbolTable.getTopFunctionDecl(functionName, null, null, null, false);
                            }
                        }
                        currentSymbolTable = currentSymbolTable.basisSymbolTable;
                    }

                    // TODO verifier le test isC()
                    if (functionDecls == null
                            && localFunctionDecls == null
                            && isMixedLanguageCall
                            && mixedLanguageFunctionDecl.unit().isC()) {
                        functionDecl = mixedLanguageFunctionDecl;
                        functionDecls = new TapList<>(functionDecl, null);
                        TapEnv.fileWarning(TapEnv.MSG_DEFAULT, ILUtils.getCalledName(instrOrExpr), "(TC33) External C procedure " + functionName + " is not declared");
                    }
                }

                //TC CALL 2 : Trap a few weird cases where one can still find the global "functionDecl".
                //            otherwise, finally create a brand new one and install its new declaration Instruction.

                if (functionDecls == null && functionTypeSpecs == null) {
                    //[llh] This getTypedFunctionDecl() will trigger again a large number of
                    // interfaceDecl.findFunctionDecl() which are time-consuming and probably redundant:
                    // TODO: try and eliminate them!
                    functionDecl = this.getTypedFunctionDecl(functionName, null, actualArgsTypes, false, instrOrExpr);
                    // [vmp] c'est peut-etre un arrayAccess dont le
                    // tableau a ete declare dans un module importe
                    // dans ce cas, le unit.fortranStuff.checkArrayAccessOrCall()
                    // a remplace le arrayAccess par un call,
                    // il faut remettre un arrayAccess
                    if (functionDecl == null) {
                        if (isUnknownFunctionName) {
                            Unit newFunctionUnit = Unit.makeDummyUnit(functionName, unit.callGraph(), language);
                            newFunctionUnit.dummyAccessedAs = instrOrExpr ;
                            newFunctionUnit.setFunctionTypeSpec(new FunctionTypeSpec(null));
                            functionUnits = new Unit[]{newFunctionUnit};
                            functionDecl = new FunctionDecl(ILUtils.build(ILLang.op_ident, functionName), newFunctionUnit);
                        } else {
                            VariableDecl vDecl = null;
                            if (unit != null) {
                                vDecl = (VariableDecl) unit.getImportedDecl(functionName, SymbolTableConstants.VARIABLE);
                            }
                            if (vDecl != null) {
                                Tree arrayTree = ILUtils.build(ILLang.op_arrayAccess,
                                    ILUtils.getCalledName(instrOrExpr),
                                    ILUtils.getArguments(instrOrExpr));
                                Tree father = instrOrExpr.parent();
                                if (father != null) {
                                    father.setChild(arrayTree, instrOrExpr.rankInParent());
                                }
                                returnedType = typeCheck(arrayTree, true, true, false, instruction, isBeginning, currentUnit);
                                break;
                            } else {
                                TapList<InterfaceDecl> interfaceDecls = getAllInterfaceDecls(functionName);
                                if (interfaceDecls == null) {
                                    VariableDecl varDecl = this.getVariableDecl(functionName);
                                    if (functionUnits == null) {
                                        if (varDecl == null ||
                                            (!varDecl.isExternal()
                                             && !TypeSpec.isA(varDecl.type(), SymbolTableConstants.POINTERTYPE)
                                             && !TypeSpec.isA(varDecl.type(), SymbolTableConstants.FUNCTIONTYPE))) {
                                            TapEnv.fileWarning(TapEnv.MSG_DEFAULT, ILUtils.getCalledName(instrOrExpr), "(TC33) External procedure " + functionName + " is not declared");
                                        }
                                        Unit newFunctionUnit = buildExternalUnit(functionName, false);
                                        if (varDecl != null) {
                                            if (TypeSpec.isA(varDecl.type(), SymbolTableConstants.FUNCTIONTYPE)) {
                                                newFunctionUnit.setFunctionTypeSpec((FunctionTypeSpec) varDecl.type().wrappedType);
                                            } else {
                                                newFunctionUnit.setFunctionTypeSpec(new FunctionTypeSpec(varDecl.type()));
                                            }
                                        } else {
                                            newFunctionUnit.setFunctionTypeSpec(new FunctionTypeSpec(null));
                                        }
                                        functionUnits = new Unit[]{newFunctionUnit};
                                    }
                                } else {
                                    functionDecl = interfaceDecls.head.findOneFunctionDecl(actualArgsTypes, instrOrExpr);
                                    if (functionDecl == null) {
                                        functionUnits = new Unit[]{buildExternalUnit(functionName, false)};
                                    }
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, ILUtils.getCalledName(instrOrExpr), "(TC34) Found no specific procedure that matches generic interface call: " + ILUtils.toString(instrOrExpr) + "; Took fallback choice: " + (functionDecl == null ? "null" : functionDecl.symbol));
                                }
                            }
                            if (functionDecl == null) {
                                functionDecl = new FunctionDecl(ILUtils.baseTree(ILUtils.getCalledName(instrOrExpr)), functionUnits[0]);
                                // Install this new global functionDecl into the language root SymbolTable:
                                SymbolTable langSymbolTable = getCallGraph().languageRootSymbolTable(currentSymbolTable.language);
                                if (functionDecl.symbol != null) {
                                    langSymbolTable.addSymbolDecl(functionDecl);
                                } else {
                                    TapEnv.toolWarning(-1, "(DD99) Warning: feature of C not supported: non-immediate function name in " + ILUtils.toString(instrOrExpr));
                                }
                                if (unit != null && !unit.isC() && this.declarationsBlock != null
                                    && !functionDecl.hasInstructionWithRootOperator(ILLang.op_external, null)
                                    && !functionDecl.hasInstructionWithRootOperator(ILLang.op_intrinsic, null)
                                    && !functionDecl.isExternalDeclared(this.declarationsBlock.instructions)
                                    && !functionDecl.hasInterfaceInstruction()) {
                                    Instruction newInstr = new Instruction(
                                          ILUtils.build(ILLang.op_external,
                                            ILUtils.build(ILLang.op_ident, functionName)));
                                    if ("%val".equals(functionName)) newInstr.isPhantom = true;
                                    this.unit.privateSymbolTable().declarationsBlock.addInstrDeclTlBeforeUse(newInstr,
                                            new TapList<>(functionDecl, null), null, this, true);
                                    functionDecl.setInstruction(newInstr);
                                }
                                // C'est trop tot: A faire apres addSymbolDecl s'il y a deja une varDecl correspondante:
                                if (functionUnits[0].functionTypeSpec() == null) {
                                        functionUnits[0].setFunctionTypeSpec(new FunctionTypeSpec(new WrapperTypeSpec(null)));
                                }
                            }
                        }
                    }
                    functionDecls = new TapList<>(functionDecl, null);
                }

                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.setTraceIndent(TapEnv.traceIndent() - 2);
                    TapEnv.indentOnTrace(TapEnv.traceIndent());
                    TapEnv.printlnOnTrace("Found function declaration(s) that match this call:" + functionDecls);
                }

                //From here, only functionDecls matters. It has to be filled by at least one functionDecl.

                //TC CALL 3 : For Fortran, if the local decl is absent, create it as a copy of the global decl.
                //            Also add the missing "external" or "intrinsic" declaration Instruction.
                if (localFunctionDecls == null && functionTypeSpecs == null && unit != null && unit.isFortran()) {
                    functionDecl = functionDecls.head;
                    if (functionDecl.isVarFunction()) {
                        // ne pas copier
                        localFunctionDecls = new TapList<>(functionDecl, null);
                    } else {
                        localFunctionDecls = new TapList<>((FunctionDecl) functionDecl.copy(null, null), null);
                        // pour pouvoir refaire une vraie copie le coup suivant
                        functionDecl.copySymbolDecl = null;
                    }
                    if (unit.isModule()) {
                        // Do not add the dummy FunctionDecl built for a F2003 pointer to function: it is only temporary
                        if (!localFunctionDecls.head.unit().isDummy()) {
                            unit.publicSymbolTable().addSymbolDecl(localFunctionDecls.head);
                        }
                    } else if (localFunctionDecls.head.symbol != null) {
// [Duplic FunctionDecl mode:]
                        if (isMixedLanguageCall) {
                            localFunctionDecls.head.symbol = functionName;
                            localFunctionDecls.head.importedFrom =
                                new TapPair<>(functionDecl, functionDecl.unit().functionDeclHostSymbolTable()) ;
                        }
                        if (!localFunctionDecls.head.unit().isDummy()) {
                            unit.privateSymbolTable().addSymbolDecl(localFunctionDecls.head);
                        }
// [Unique FunctionDecl mode:]
// [End FunctionDecl mode.]
                        // Add into this Unit the "phantom" of the local declaration instruction
                        // of the called function. Do that for intrinsics ( ? maybe also for externals ?).
                        if (this.declarationsBlock != null
                            && !this.unit.isC()
                            && (localFunctionDecls.head.isIntrinsic()
                            && !localFunctionDecls.head.isIntrinsicDeclared(this.declarationsBlock.instructions)
                            ||
                            localFunctionDecls.head.isExternal()
                                    && !localFunctionDecls.head.isExternalDeclared(this.declarationsBlock.instructions)
                            ||
                            functionDecls.head.isVarFunction()
                                    && !localFunctionDecls.head.isIntrinsicDeclared(this.declarationsBlock.instructions)
                                    && !localFunctionDecls.head.isExternalDeclared(this.declarationsBlock.instructions)
                                    && !localFunctionDecls.head.hasInterfaceInstruction())
                            && !instruction.isADeclaration()
                        ) {
                            Instruction newInstr =
                                    new Instruction(
                                            ILUtils.build(localFunctionDecls.head.isIntrinsic() ? ILLang.op_intrinsic : ILLang.op_external,
                                                    ILUtils.build(ILLang.op_ident, functionName)));
                            // Set isPhantom to true to hide this intrinsic declaration in the produced code.
                            // Currently we hide only for those in "inlined" code, and "%VAL".
                            if (localFunctionDecls.head.isIntrinsic() &&
                                (currentUnit.isMadeForInline || "%val".equals(functionName))) {
                                newInstr.isPhantom = true;
                            }
                            this.unit.privateSymbolTable().declarationsBlock.addInstrDeclTlBeforeUse(newInstr, null, null,
                                    null, true);
                            localFunctionDecls.head.setInstruction(newInstr);
                        }
                    }
                }

                int nbFunctionDecls = TapList.length(functionDecls);
                functionUnits = new Unit[nbFunctionDecls];
                if (nbFunctionDecls > 0 && functionDecls != null) {
                    mixedLanguageFunctionDecl = mixedLanguageCall(functionName, functionDecls.head); //only for C/Fortran
                    isMixedLanguageCall = (mixedLanguageFunctionDecl!=null) ;
                    //We preserve the functionDecls's order for functionUnits
                    TapList<FunctionDecl> tmpFunctionDecls = functionDecls;
                    for (int index = 0; index < nbFunctionDecls; ++index) {
                        functionUnits[index] = tmpFunctionDecls.head.unit();
                        tmpFunctionDecls = tmpFunctionDecls.tail;
                    }
                }

                //TC CALL 4 : Do the actual checking of actual Types versus formal Types.
                //            Also build the equivalent call Tree that solves away the nameEq and optional args.
                //            Also set the CallArrow's from current Unit to callee.
                FunctionTypeSpec functionTypeSpec = null;
                if (functionTypeSpecs == null && nbFunctionDecls > 0) {
                    functionTypeSpecs = new FunctionTypeSpec[nbFunctionDecls];
                    functionTypeSpec = functionUnits[0].functionTypeSpec(); // all functionDecls have the same typeSpec;
                }
                if (functionTypeSpec == null) {
                    for (int k = nbFunctionDecls - 1; k >= 0; --k) {
                        functionTypeSpec = new FunctionTypeSpec(new WrapperTypeSpec(null));
                        functionUnits[k].setFunctionTypeSpec(functionTypeSpec);
                        functionTypeSpecs[k] = functionTypeSpec;
                    }
                } else {
                    for (int k = nbFunctionDecls - 1; k >= 0; --k) {
                        functionTypeSpecs[k] = functionUnits[k].functionTypeSpec();
                    }
                }
                WrapperTypeSpec localizedFormalReturnType = ((functionTypeSpec == null) ? null : functionTypeSpec.returnType);
                WrapperTypeSpec[] localizedFormalArgsTypes = null;
                FunctionTypeSpec[] localizedFormalFunctionTypeSpecs = new FunctionTypeSpec[nbFunctionDecls];
                if (functionTypeSpec != null && functionTypeSpec.argumentsTypes == null) {
                    // Backward type inference on the function's formal arguments types.
                    // Note: this should not be done when overloading (nbFunctionDecls>1) ?
                    for (int k = nbFunctionDecls - 1; k >= 0; --k) {
                        localizedFormalArgsTypes = new WrapperTypeSpec[nbActualArgs];
                        for (int i = 0; i < nbActualArgs; i++) {
                            localizedFormalArgsTypes[i] = new WrapperTypeSpec(null);
                        }
                        functionTypeSpecs[k].argumentsTypes = localizedFormalArgsTypes;
                        localizedFormalFunctionTypeSpecs[k] = functionTypeSpecs[k];
                        localizedFormalReturnType = functionTypeSpecs[k].returnType;
                    }
                } else {

                    for (int k = 0; k < nbFunctionDecls; k++) {
                        FunctionTypeSpec annotFTS =
                                instrOrExpr.getAnnotation("functionTypeSpec");
                        if (annotFTS == null) {
                            TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadySolved =
                                    new TapList<>(null, null);
                            if (TapEnv.traceTypeCheckAnalysis()) {
                                TapEnv.indentOnTrace(TapEnv.traceIndent());
                                TapEnv.printlnOnTrace("Declared function Type:" + functionTypeSpecs[k]);
                            }
                            if (functionTypeSpecs[k].containsMetaType(null)
                                || functionTypeSpecs[k].containsUnknownDimension()) {
                                localizedFormalFunctionTypeSpecs[k] =
                                        (FunctionTypeSpec) functionTypeSpecs[k].localize(toAlreadySolved, new ToBool(false));
                                if (TapEnv.traceTypeCheckAnalysis()) {
                                    TapEnv.indentOnTrace(TapEnv.traceIndent());
                                    TapEnv.printlnOnTrace("Localized function Type:" + localizedFormalFunctionTypeSpecs[k]);
                                }
                            } else {
                                localizedFormalFunctionTypeSpecs[k] = functionTypeSpecs[k];
                            }
                            localizedFormalFunctionTypeSpecs[k].matchArgumentsTypes(actualArgsTypes, unit, functionUnits[k]);
                            FunctionTypeSpec.finishLastMetaTypes(toAlreadySolved, this);
                            if (TapEnv.traceTypeCheckAnalysis()) {
                                TapEnv.indentOnTrace(TapEnv.traceIndent());
                                TapEnv.printlnOnTrace("Matched function Type:" + localizedFormalFunctionTypeSpecs[k]);
                            }
                            localizedFormalReturnType = localizedFormalFunctionTypeSpecs[k].returnType;
                            if (unit != null && unit.isFortran()) {
                                // Fortran intrinsics on which our "lib" mechanism looses precision information:
                                if ("real".equals(functionName) || "aimag".equals(functionName) || "abs".equals(functionName)) {
                                    ToObject<ModifiedTypeSpec> toArgModifiedType = new ToObject<>(null) ;
                                    WrapperTypeSpec argBaseType =
                                        TypeSpec.peelSizeModifier(actualArgsTypes[0], toArgModifiedType) ;
                                    if (argBaseType.isComplexBase() && toArgModifiedType.obj()!=null) {
                                        localizedFormalReturnType = localizedFormalReturnType.baseTypeSpec(true) ;
                                        Tree resultModifierTree = toArgModifiedType.obj().complexHalfModifier() ;
                                        localizedFormalReturnType =
                                            new WrapperTypeSpec(
                                                new ModifiedTypeSpec(localizedFormalReturnType,
                                                                     resultModifierTree, this)) ;
                                    }
                                }
//                                 // cf set02/v012: comment this out if, according to standard, CMPLX always returns
//                                 // a plain precision complex, regardless of arguments precision,
//                                 // or takes precision only from optional 3rd "KIND" argument
//                                 if ("cmplx".equals(functionName)) {
//                                     ToObject<ModifiedTypeSpec> toArgModifiedType = new ToObject<>(null) ;
//                                     WrapperTypeSpec argBaseType =
//                                         TypeSpec.peelSizeModifier(actualArgsTypes[0], toArgModifiedType) ;
//                                     if (argBaseType.isRealBase() && toArgModifiedType.obj()!=null) {
//                                         localizedFormalReturnType = localizedFormalReturnType.baseTypeSpec(true) ;
//                                         Tree resultModifierTree = toArgModifiedType.obj().complexDuplModifier() ;
//                                         localizedFormalReturnType =
//                                             new WrapperTypeSpec(
//                                                 new ModifiedTypeSpec(localizedFormalReturnType,
//                                                                      resultModifierTree, this)) ;
//                                     }
//                                 }
                                // F90 intrinsics that play with dimensions:
                                if ("sum".equals(functionName) || "spread".equals(functionName)) {
                                    if (unit.isFortran9x()) {
                                        actualArgsTypes[0] =
                                                checkPointerInFortranExpression(actualArgsTypes[0], ILUtils.getArguments(instrOrExpr), 1);
                                        actualArgs[0] = ILUtils.getArguments(instrOrExpr).down(1);
                                    }
                                    if ("sum".equals(functionName)) {
                                        localizedFormalReturnType =
                                                sumResultType(actualArgsTypes[0], ILUtils.getOptionalDim(actualArgs, this));
                                    } else if ("spread".equals(functionName)) {
                                        Tree newDim = ILUtils.build(ILLang.op_dimColon,
                                                ILUtils.build(ILLang.op_intCst, 1),
                                                ILUtils.copy(actualArgs[2]));
                                        localizedFormalReturnType =
                                                spreadResultType(actualArgsTypes[0], actualArgs[1], newDim);
                                    }
                                }
                                localizedFormalFunctionTypeSpecs[k].returnType = localizedFormalReturnType;
                            }
                            // Remove possible "undefinedNumeric" on the return type:
                            if (localizedFormalReturnType != null) {
                                WrapperTypeSpec baseReturnType = localizedFormalReturnType.baseTypeSpec(false);
                                if (TypeSpec.isA(baseReturnType, SymbolTableConstants.PRIMITIVETYPE)) {
                                    baseReturnType.wrappedType.setUndefinedNumeric(false);
                                }
                            }

                            if (localizedFormalFunctionTypeSpecs[k] != functionTypeSpecs[k]) {
                                instrOrExpr.setAnnotation("functionTypeSpec",
                                        localizedFormalFunctionTypeSpecs[k]);
                            }
                        } else {
                            localizedFormalFunctionTypeSpecs[k] = annotFTS;
                            localizedFormalReturnType = localizedFormalFunctionTypeSpecs[k].returnType;
                        }
                    }
                    if (nbFunctionDecls > 0) {
                        localizedFormalArgsTypes = new WrapperTypeSpec[localizedFormalFunctionTypeSpecs[0].argumentsTypes.length];
                        System.arraycopy(localizedFormalFunctionTypeSpecs[0].argumentsTypes, 0,
                                localizedFormalArgsTypes, 0,
                                localizedFormalFunctionTypeSpecs[0].argumentsTypes.length);
                    }
                }
                //"localizedFormalArgsTypes"  and  "localizedFormalReturnType" are unique for all functions.

                WrapperTypeSpec arrayReturnTypeSpec = null;
                boolean elementalCall = false;
                if (localizedFormalArgsTypes != null) {
                    int nbFormalArgs = localizedFormalArgsTypes.length;
                    int maxi;
                    // pour ne pas perdre les annotations sur le call
                    Tree newCallTree = ILUtils.copy(instrOrExpr);
                    Tree optionalTree = ILUtils.copy(newCallTree);
                    for (int i = 0; i < localizedFormalArgsTypes.length; i++) {
                        ILUtils.getArguments(optionalTree).setChild(ILUtils.build(ILLang.op_none), i + 1);
                    }
                    boolean needAnnot = false;
                    int optNb = 0;
                    ToBool callUsesNameEq = new ToBool(false);
                    if (functionUnits[0] != null
                            && !functionUnits[0].isExternal()
                            && (rankOfFirstNameEq < nbActualArgs
                            || nbFormalArgs > nbActualArgs)) { // never happens in C++ (optional args are "none" but still exist)
                        actualArgsTypes = functionUnits[0].typeCheckNameEqOptionalArguments(this, actualArgs, actualArgsTypes,
                                nameEqTypes, rankOfFirstNameEq, newCallTree, optionalTree, callUsesNameEq);
                        needAnnot = true;
                    } else {
                        newCallTree = ILUtils.copy(instrOrExpr);
                    }
                    Tree[] sons = ILUtils.getArguments(newCallTree).children();
                    for (int i = sons.length; i > 0; i--) {
                        if (sons[i - 1] == null ||
                                sons[i - 1].opCode() == ILLang.op_nameEq //nevers happens in C++
                                        && sons[i - 1].down(2).opCode() == ILLang.op_none) {
                            ILUtils.getArguments(newCallTree).setChild(ILUtils.build(ILLang.op_none), i);
                        }
                    }
                    if (functionUnits[0] != null && (nbFormalArgs != nbActualArgs || functionUnits[0].isCPlusPlus())) {
                        functionUnits[0].typeCheckOptionalArguments(
                                actualArgsTypes, newCallTree, optionalTree, callUsesNameEq);
                        optNb = functionUnits[0].isCPlusPlus() ? 0 : functionUnits[0].getOptionalParameterDeclsNumber();
                        boolean ok = nbFormalArgs - optNb <= nbActualArgs && nbActualArgs <= nbFormalArgs;
                        if (!functionUnits[0].functionTypeSpec().variableArgList) {
                            if (!ok) {
                                if (functionUnits[0].isC() && functionUnits[0].isExternal()
                                        && functionUnits[0].functionTypeSpec().argumentsTypes.length == 0) {
                                    functionUnits[0].functionTypeSpec().argumentsTypes =
                                            new WrapperTypeSpec[nbActualArgs];
                                    System.arraycopy(actualArgsTypes, 0, functionUnits[0].functionTypeSpec().argumentsTypes, 0, nbActualArgs);
                                }
                                TapEnv.fileWarning(TapEnv.MSG_DEFAULT, instrOrExpr, "(TC32) Conflicting numbers of arguments for procedure " + functionName + ", expected " + nbFormalArgs + ", is here " + nbActualArgs);
                            } else {
                                needAnnot = true;
                            }
                        }
                        if (nbFormalArgs != nbActualArgs) {
                            maxi = Math.min(nbFormalArgs, nbActualArgs);
                        }
                    }
                    if (needAnnot) {
                        sons = ILUtils.getArguments(newCallTree).children();
                        for (int i = 0; i < sons.length; i++) {
                            if (sons[i] == null) {
                                ILUtils.getArguments(newCallTree).removeChild(i + 1);
                            }
                        }
                        // [llh 24Nov2017] Don't understand why this becomes necessary only now (on F90:v85), but it makes sense:
                        while (ILUtils.getArguments(optionalTree).length() > nbFormalArgs) {
                            ILUtils.getArguments(optionalTree).removeChild(nbFormalArgs + 1);
                        }
                        Tree oldTree = instrOrExpr;
                        if (TapEnv.traceTypeCheckAnalysis()) {
                            TapEnv.indentOnTrace(TapEnv.traceIndent());
                            TapEnv.printlnOnTrace("Replace " + instrOrExpr + " with call with explicit arguments " + newCallTree);
                        }
                        instrOrExpr = instruction.replaceTree(instrOrExpr, newCallTree);
                        instrOrExpr.setAnnotation("sourcetree", ILUtils.copy(oldTree));
                        if (optNb != 0) {
                            instrOrExpr.setAnnotation("optionaltree", optionalTree);
                        }
                        // pb il faudra mettre a jour cette annotation si on splitte apres!!!
                    }
                    boolean elementalArg;
                    maxi = Math.min(nbFormalArgs, nbActualArgs);
                    if (functionDecls != null && functionDecls.head.unit() != null
                            && functionDecls.head.functionTypeSpec() != null
                            && functionDecls.head.functionTypeSpec().argumentsTypes != null) {
                        maxi = Math.min(maxi, functionDecls.head.functionTypeSpec().argumentsTypes.length);
                    }
                    boolean warn999 = false ;
                    for (int i = 0; i < maxi; i++) {
                        // Check actual argument for Fortran implicit deref of pointers:
                        if (unit != null && unit.isFortran9x()
                            && localizedFormalArgsTypes[i].wrappedType != null
                            && !TypeSpec.isA(localizedFormalArgsTypes[i], SymbolTableConstants.POINTERTYPE)) {
                            actualArgsTypes[i] =
                                checkPointerInFortranExpression(actualArgsTypes[i], ILUtils.getArguments(instrOrExpr), i + 1);
                            actualArgs[i] = ILUtils.getArguments(instrOrExpr).down(i + 1);
                        }
                        elementalArg =
                            (functionDecls != null && functionDecls.head.unit() != null &&
                             !functionDecls.head.isIntrinsicNotElemental() &&
                             actualArgsTypes[i] != null &&
                             (functionDecls.head.isElemental() || functionDecls.head.isIntrinsic()) &&
                             TypeSpec.isA(actualArgsTypes[i], SymbolTableConstants.ARRAYTYPE) &&
                             !actualArgsTypes[i].isString() &&
                             localizedFormalArgsTypes[i].receives(actualArgsTypes[i].wrappedType.elementType(), null, null)) ;
                        if (elementalArg) {
                            elementalCall = true;
                            // !!! real(int(:)) -> real(:)
                            // !!! len_trim(char(:)) -> int
                            // cas func(..., kind=...)
                            Tree kindTree = functionUnits[0].getElementalIntrinsicTypeConverter(actualArgs); //only for Fortran
                            if (kindTree != null) {
                                localizedFormalReturnType =
                                        new WrapperTypeSpec(new ModifiedTypeSpec(localizedFormalReturnType, kindTree, this));
                            }
                            if (!TypeSpec.isA(localizedFormalReturnType, SymbolTableConstants.ARRAYTYPE)) {
                                arrayReturnTypeSpec = new WrapperTypeSpec(new ArrayTypeSpec(localizedFormalReturnType,
                                        ((ArrayTypeSpec) actualArgsTypes[i].wrappedType).dimensions()));
                            } else {
                                arrayReturnTypeSpec = localizedFormalReturnType;
                            }
                        }
                        if (actualArgsTypes[i] == null ||
                            TypeSpec.isA(localizedFormalArgsTypes[i], SymbolTableConstants.VOIDTYPE) ||
                            localizedFormalArgsTypes[i].receives(actualArgsTypes[i], null, null) ||
                            // array notation
                            elementalArg) {
                            if (actualArgsTypes[i] != null) {
                                actualArgsTypes[i].shareActiveFields(localizedFormalArgsTypes[i]);
                            }
                            if (functionUnits != null &&
                                    //functionUnits[0].isVarFunction() &&
                                    TypeSpec.isA(actualArgsTypes[i], SymbolTableConstants.FUNCTIONTYPE)) {
                                // Set a CallArrow from called Unit to the Unit passed as argument:
                                String argFunctionName = ILUtils.getIdentString(actualArgs[i]);
                                if (argFunctionName != null) {
                                    TapList<FunctionDecl> argFunctionDecls = getFunctionDecl(argFunctionName, null, null, false);
                                    FunctionDecl argFunctionDecl = argFunctionDecls == null ? null : argFunctionDecls.head;
                                    if (argFunctionDecl != null && !argFunctionDecl.isInterface()) {
                                        CallArrow callArrow = CallGraph.addCallArrow(functionUnits[0], SymbolTableConstants.CALLS,
                                                argFunctionDecl.unit());
                                        callArrow.setSrcCallName("functionName");
                                        // TODO vmp a verifier, la called unit n'appelle pas la Unit passee en argument
                                        // on rajoute une CallArrow de la curUnit vers la Unit passee en argument
                                        callArrow = CallGraph.addCallArrow(unit, SymbolTableConstants.CALLS, argFunctionDecl.unit());
                                        callArrow.setSrcCallName("functionName");
                                    }
                                }
                            }
                        } else {
                            if (!isMixedLanguageCall) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, actualArgs[i], "(TC30) Type mismatch in argument " + (i + 1) + " of procedure " + functionName + ", expected " + localizedFormalArgsTypes[i].showType() + ", is here " + actualArgsTypes[i].showType());
                            }
                        }

                        Unit otherLangUnit = (isMixedLanguageCall ? mixedLanguageFunctionDecl.unit() : null) ;
                        if (isMixedLanguageCall &&
                            ((unit.isFortran() && otherLangUnit.isC())
                             || (unit.isC() && otherLangUnit.isFortran()))) {
                            FunctionTypeSpec otherFunctionTypeSpec = otherLangUnit.functionTypeSpec() ;
                            WrapperTypeSpec actualArgType = actualArgsTypes[i] ;
                            WrapperTypeSpec expectedArgType =
                                (otherFunctionTypeSpec.argumentsTypes!=null && i<otherFunctionTypeSpec.argumentsTypes.length
                                 ? otherFunctionTypeSpec.argumentsTypes[i] : null) ;
                            boolean simpleTypesMatch =
                                (unit.isC()
                                  ? unit.callGraph().naturalBindFortranCType(expectedArgType, actualArgType)
                                  : unit.callGraph().naturalBindFortranCType(actualArgType, expectedArgType)) ;
                            if (!simpleTypesMatch) {
                                if (unit.callGraph().bindFortranCType()==null) {
                                    warn999 = true ;
                                    // TapEnv.toolWarning(-1,
                                    // "(DD999) Warning: type-checking of mixed language call not yet implemented, for call "
                                    //+ ILUtils.toString(instrOrExpr)
                                    //+", passed "+(unit.isC()?"C":"Fortran")+" type "+actualArgType.showType()
                                    //+", expected "+(otherLangUnit.isC()?"C":"Fortran")+" type "+(expectedArgType!=null ? expectedArgType.showType() : "Null")) ;
                                } else if (!unit.callGraph().checkBindFortranCType(actualArgType, expectedArgType)) {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, actualArgs[i],
                                             "(TC30) Type mismatch in argument " + (i + 1) + " of procedure " + functionName
                                             +", passed "+(unit.isC()?"C":"Fortran")+" type "+actualArgType.showType()
                                             +", expected "+(otherLangUnit.isC()?"C":"Fortran")+" type "+expectedArgType.showType()) ;
                                }
                            }
                        }

                    }
                    if (warn999) {
                        TapEnv.toolWarning(-1,
                            "(DD999) Warning: type-checking of mixed language call not yet implemented, for some arguments of call "
                                           + ILUtils.toString(instrOrExpr)) ;
                    }
                }
                if (functionUnits != null && this.unit != null) {
                    TapList<FunctionDecl> tmpFunctionDecls = functionDecls;
                    int index = 0;
                    CallArrow trueFunctionCallArrow = null;
                    while (tmpFunctionDecls != null) {
                        if (index >= functionUnits.length) {
                            TapEnv.toolWarning(10, "Unexpected number of called functions, expected " + functionUnits.length + ", is here at least " + index + 1);
                        } else {
                            CallArrow callArrow ;
                            if (functionUnits[index].isInterface() && isMixedLanguageCall) {
                                callArrow =
                                    setCallArrow(instrOrExpr, mixedLanguageFunctionDecl.unit(), mixedLanguageFunctionDecl) ;
                                if (trueFunctionCallArrow == null) {
                                    trueFunctionCallArrow = callArrow ;
                                }
                            } else {
                                callArrow =
                                    setCallArrow(instrOrExpr, functionUnits[index], tmpFunctionDecls.head);
                                if (trueFunctionCallArrow == null || !callArrow.destination.isInterface()) {
                                    trueFunctionCallArrow = callArrow;
                                }
                            }
                            instrOrExpr.setAnnotation("callArrow", trueFunctionCallArrow);
                            ++index;
                        }
                        tmpFunctionDecls = tmpFunctionDecls.tail;
                    }
                    // Not-so-clean way to ensure the annotated CallArrow goes to the "true" called Unit:
                    // instrOrExpr.setAnnotation("callArrow", trueFunctionCallArrow);
                }
                if (localizedFormalReturnType == null) {
                    for (int k = 0; k < nbFunctionDecls; ++k) {
                        functionTypeSpecs[k].returnType = new WrapperTypeSpec(null);
                    }

                    if (nbFunctionDecls > 0) 
                        localizedFormalReturnType = functionTypeSpecs[0].returnType; // all the same return type.
                } else {
                    //ne devrait pas etre possible
                    if (TypeSpec.isA(localizedFormalReturnType, SymbolTableConstants.METATYPE)) {
                        localizedFormalReturnType = new WrapperTypeSpec(null);
                    }
                }

                //TC CALL 5 : Combine with pre-existing VariableDecl of the function as its return type (FORTRAN only):

                if (unit != null && unit.isFortran()
                    // If we are on a varFunction, there's no pre-existing VariableDecl. (cf F90:v288)
                    && !(functionDecls != null && functionDecls.head.isVarFunction())
                    // and likewise if we are on a dummy FunctionDecl built for a F2003 pointer to function
                    && !(functionDecls != null && functionDecls.head.unit().isDummy())
                    ) {
                    VariableDecl funcVarDecl = getVariableDecl(functionName);
                    if (funcVarDecl != null) {
                        // When there is a declaration of the function name,
                        // just like a variable, which indicates its return type:
                        funcVarDecl.isFunctionName = true;
                        funcVarDecl.kind = SymbolTableConstants.FUNCNAME;
                        WrapperTypeSpec funReturnType = funcVarDecl.type();
                        if (TypeSpec.isA(funcVarDecl.type(), SymbolTableConstants.FUNCTIONTYPE)) {
                            funReturnType = ((FunctionTypeSpec) funcVarDecl.type().wrappedType).returnType;
                        }
                        if (!funReturnType.receives(localizedFormalReturnType, null, null)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, ILUtils.getCalledName(instrOrExpr), "(DD06) Cannot combine successive declarations of function " + functionName + " return type: " + localizedFormalReturnType.showType() + " and " + funcVarDecl.type().showType() + " (overwritten previous)");
                        }
                        if (!TypeSpec.isA(funcVarDecl.type(), SymbolTableConstants.FUNCTIONTYPE)) {
                            funcVarDecl.setType(new WrapperTypeSpec(functionTypeSpecs[0]));
                        }
                    }
                }

                //TC CALL 6 : Type-check functions vs procedures

                if (localizedFormalReturnType != null) {
                    if (expectResult) {
                        if (localizedFormalReturnType.wrappedType == null) {
                            // When a result is expected and return type isn't known, use implicit rules:
                            WrapperTypeSpec implicitType = getImplicitTypeSpec(functionName);
                            if (implicitType != null &&
                                    implicitType.wrappedType != null) {
                                localizedFormalReturnType.wrappedType = implicitType.wrappedType;
                                if (!functionUnits[0].isIntrinsic()) {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, ILUtils.getCalledName(instrOrExpr), "(TC37) Return type of function " + functionName + " set by implicit rule to " + localizedFormalReturnType.wrappedType.showType());
                                }
                            }
                            //needVarDeclInstruction = true;
                        } else if (TypeSpec.isA(localizedFormalReturnType.wrappedType, SymbolTableConstants.VOIDTYPE)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC35) Subroutine " + functionName + " used as a function");
                            localizedFormalReturnType = null;
                        } else {
                            needVarDeclInstruction = !functionDecls.head.hasInstructionWithRootOperator(ILLang.op_varDeclaration, null);
                        }
                    } else {
                        if (localizedFormalReturnType.wrappedType == null || symbolFoundAsParam) {
                            localizedFormalReturnType.wrappedType = new VoidTypeSpec();
                        } else if (!TypeSpec.isA(localizedFormalReturnType.wrappedType, SymbolTableConstants.VOIDTYPE)) {
                            if (unit != null
                                    && (unit.isFortran() && mixedLanguageFunctionDecl == null
                                    || mixedLanguageFunctionDecl != null && mixedLanguageFunctionDecl.unit().isFortran())) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC36) " + localizedFormalReturnType.showType() + " function " + functionName + " used as a subroutine");
                            }
                            localizedFormalReturnType = null;
                        }
                    }
                
                    if (elementalCall && expectResult) {
                        functionUnits[0].usedAsElementalResult = true;
                    } else if (functionUnits[0] != null) {
                        // modif sinon g95 ne compile pas
                        functionUnits[0].usedAsElementalResult = functionUnits[0].isElemental();
                    }
                    if (elementalCall) {
                        instrOrExpr.setAnnotation("arrayReturnTypeSpec", arrayReturnTypeSpec);
                        localizedFormalReturnType = arrayReturnTypeSpec;
                    }

                    Tree kindTree = functionUnits[0].getElementalIntrinsicTypeConverter(actualArgs);
                    if (!elementalCall && kindTree != null) {
                        localizedFormalReturnType = new WrapperTypeSpec(new ModifiedTypeSpec(localizedFormalReturnType, kindTree, this));
                        FunctionTypeSpec annotFTypeSpec = instrOrExpr.getAnnotation("functionTypeSpec");
                        // pb modifie le returnType de la Unit et pas seulement l'annotation
                        if (annotFTypeSpec != null) {
                            annotFTypeSpec.returnType = localizedFormalReturnType;
                        }
                    }
                }

                //TC CALL 7 : Add a declaration Instruction of the return Type of the called function,
                //            sometimes as a yet undefined type in which case we annotate with the WrapperTypeSpec for later.
                //            Add a declaration Instruction as "external" in some strange case where it is still missing.
                TapList<FunctionDecl> functionDeclsToDeclare = localFunctionDecls != null && localFunctionDecls.head != null ? localFunctionDecls : functionDecls;
                // TODO simplifier ce test horrible
                if (needVarDeclInstruction && expectResult && unit != null
                    && !unit.isC()
                    && !functionDeclsToDeclare.head.isInterface()
                    && localInterfaceDecl == null
                    && !functionDeclsToDeclare.head.isIntrinsic()
                    && !functionDeclsToDeclare.head.hasInstructionWithRootOperator(ILLang.op_varDeclaration, null)
                    && !functionDeclsToDeclare.head.isContainedFunctionDeclDeclared(this.unit)
                    && !functionDeclsToDeclare.head.isVarDeclared(this.declarationsBlock.instructions)
                    && (functionDeclsToDeclare.head.unit().otherReturnVar() == null
                        || functionDeclsToDeclare.head.unit().language() != language)
                    && functionDeclsToDeclare.head.symbol.equals(functionName)
                    && (comesFromRootSymbolTable(functionDeclsToDeclare.head) || functionDeclsToDeclare.head.isVarFunction())
                    && localizedFormalReturnType != null
                    && !TypeSpec.isA(localizedFormalReturnType, SymbolTableConstants.VOIDTYPE)) {
                    Tree functionNameTree = ILUtils.build(ILLang.op_ident, functionName);
                    functionNameTree.setAnnotation("isFunctionName", Boolean.TRUE);
                    Tree typeTree = ILUtils.build(ILLang.op_none);
                    typeTree.setAnnotation("typeSpec", localizedFormalReturnType);
                    // Some time later, after typeCheck(), one must replace this "none" typeTree with
                    // the true final return type (final localizedFormalReturnType), as the 2nd son
                    // of the varDeclaration Tree. See method terminateFunctionDeclarationInstructions().
                    Instruction newInstr = new Instruction(
                            ILUtils.build(ILLang.op_varDeclaration,
                                    ILUtils.build(ILLang.op_modifiers),
                                    typeTree,
                                    ILUtils.build(ILLang.op_declarators, functionNameTree)));
                    if ("%val".equals(functionName)) newInstr.isPhantom = true;
                    this.declarationsBlock.addInstrDeclTlBeforeUse(newInstr, null, null, null, true);
                    functionDeclsToDeclare.head.setInstruction(newInstr);
                }
                TapList<FunctionDecl> tmpFunctionDeclsToDeclare = functionDeclsToDeclare;
                while (tmpFunctionDeclsToDeclare != null) {
                    ILUtils.checkFunctionName(tmpFunctionDeclsToDeclare.head, this);
                    tmpFunctionDeclsToDeclare = tmpFunctionDeclsToDeclare.tail;
                }
                tmpFunctionDeclsToDeclare = functionDeclsToDeclare;
                if (this.basisSymbolTable != null && this.basisSymbolTable.isFormalParams) {
                    while (tmpFunctionDeclsToDeclare != null) {
                        ILUtils.checkFunctionName(tmpFunctionDeclsToDeclare.head, this.basisSymbolTable);
                        tmpFunctionDeclsToDeclare = tmpFunctionDeclsToDeclare.tail;
                    }
                }
                if (unit != null && !unit.isC()
                    && (functionDeclsToDeclare.head.isExternal() || TapEnv.inputLanguage() == TapEnv.MIXED && functionDeclsToDeclare.head.unit().isC())
                    && !functionDeclsToDeclare.head.hasOtherInstruction()
                    && !functionDeclsToDeclare.head.hasInstructionWithRootOperator(ILLang.op_external, null)
                    && !functionDeclsToDeclare.head.isExternalDeclared(this.declarationsBlock.instructions)
                    && !functionDeclsToDeclare.head.hasInterfaceInstruction()
                    && !(this.basisSymbolTable != null && functionDeclsToDeclare.head.isExternalDeclared(this.basisSymbolTable.declarationsBlock.instructions))
                ) {
                    Instruction newInstr = new Instruction(
                            ILUtils.build(ILLang.op_external,
                                    ILUtils.build(ILLang.op_ident, functionName)));
                    this.unit.privateSymbolTable().declarationsBlock.addInstrDeclTlBeforeUse(newInstr, null, null,
                            null, true);
                    functionDeclsToDeclare.head.setInstruction(newInstr);
                }

                //TC CALL 8 : Do last cleanings about pointers and unneeded implicit declarations.

                if (ILUtils.getCalledName(instrOrExpr).opCode() == ILLang.op_pointerAccess) {
                    if (TypeSpec.isA(localizedFormalReturnType, SymbolTableConstants.POINTERTYPE)) {
                        localizedFormalReturnType = ((PointerTypeSpec) localizedFormalReturnType.wrappedType).destinationType;
                        if (TypeSpec.isA(localizedFormalReturnType, SymbolTableConstants.FUNCTIONTYPE)) {
                            localizedFormalReturnType = ((FunctionTypeSpec) localizedFormalReturnType.wrappedType).returnType;
                        }
                    }
                }
                if (this.unit != null && this.unit.fortranStuff() != null && functionDecl != null
                        && TypeSpec.isA(localizedFormalReturnType, SymbolTableConstants.VOIDTYPE)
                        && functionDecl.hasInstructionWithRootOperator(ILLang.op_varDeclaration, null)) {
                    // enlever les declarations de parametres rajoutees dans la publicSymbolTable
                    // par fixImplicit avant le typeCheck cf F77:lh44
                    Instruction implicitInstr = TapList.cassqString(functionDecl.symbol,
                            this.unit.fortranStuff().implicitDeclarations);
                    if (implicitInstr != null) {
                        implicitInstr.tree = ILUtils.build(ILLang.op_none);
                    }
                }
                //             if (functionDecl!=null && functionDecl.unit()!=null) {
                //                 if (functionDecl.functionTypeSpec() != null) {
                //                     functionDecl.unit().accumulateInferredFunctionTypeSpec(
                //                                           functionTypeSpec, unit, instrOrExpr);
                //                 } else {
                //                     functionDecl.unit().setFunctionTypeSpec(functionTypeSpec);
                //                 }
                //             }

                // TC CALL 9 : Finished TypeCheck of this call.
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapList<FunctionDecl> tmpFunctionDecls = functionDecls;
                    int index = 0;
                    while (tmpFunctionDecls != null) {

                        TapEnv.indentOnTrace(TapEnv.traceIndent());
                        TapEnv.printlnOnTrace(instrOrExpr + " calls: " + tmpFunctionDecls.head);
                        TapEnv.indentOnTrace(TapEnv.traceIndent() + 2);
                        TapEnv.printlnOnTrace("which has declared type:" + functionTypeSpecs[index]);
                        TapEnv.indentOnTrace(TapEnv.traceIndent() + 2);
                        TapEnv.printlnOnTrace("...instantiated here as:" + localizedFormalFunctionTypeSpecs[index]);

                        tmpFunctionDecls = tmpFunctionDecls.tail;
                        ++index;
                    }
                }
                returnedType = localizedFormalReturnType;

                break;
            }
            case ILLang.op_constructorCall: {
                String className = ILUtils.getClassNameString(instrOrExpr.down(1));
                if (className == null) {
                    TapEnv.fileError(instrOrExpr, "(TC57) null constructor name call " + ILUtils.toString(instrOrExpr));
                    break;
                }
                // Type-Check actual arguments
                Tree[] actualArgs = ILUtils.getArguments(instrOrExpr).children();
                int nbActualArgs = actualArgs.length;
                WrapperTypeSpec[] actualArgsTypes = new WrapperTypeSpec[nbActualArgs];
                for (int i = 0; i < nbActualArgs; i++) {
                    if (actualArgs[i].opCode() == ILLang.op_none)
                    // optional args in C++
                    {
                        actualArgsTypes[i] = new WrapperTypeSpec(null);
                    } else {
                        actualArgsTypes[i] =
                                typeCheck(actualArgs[i], true, false, false, instruction, isBeginning, currentUnit);
                    }
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.indentOnTrace(TapEnv.traceIndent());
                        TapEnv.printlnOnTrace("actual arg" + (i + 1) + ": " + actualArgsTypes[i]);
                    }
                }

                // Find the local declared SymbolDecl of the called constructor.
                WrapperTypeSpec[] formalArgsTypes = null;
                ClassDecl classDecl = this.getClassDecl(className);
                if (classDecl != null) {
                    // This is a Class constructor:
                    String constructorKind = ILUtils.getIdentString(instrOrExpr.down(2));
                    returnedType = new WrapperTypeSpec(classDecl.unit.classTypeSpec());
                    SymbolTable classSymbolTable;
                    if (currentUnit == null) {
                        classSymbolTable = classDecl.unit.publicSymbolTable();
                    } else {
                        Unit upperClassUnit = currentUnit.getUpperLevelClassUnit();
                        if (upperClassUnit != null && upperClassUnit == classDecl.unit) {
                            classSymbolTable = upperClassUnit.privateSymbolTable();
                        } else if (upperClassUnit != null && upperClassUnit.subClassOf(classDecl.unit)) {
                            classSymbolTable = classDecl.unit.protectedSymbolTable();
                        } else {
                            classSymbolTable = classDecl.unit.publicSymbolTable();
                        }
                    }
                    FunctionDecl constructorDecl;
                    TapList<FunctionDecl> localConstructorDecls =
                            classSymbolTable.getFunctionDecl(className, new WrapperTypeSpec(new VoidTypeSpec()),
                                    actualArgsTypes, className, true);
                    if (localConstructorDecls == null) {
                        if (constructorKind == null || nbActualArgs == 0 && constructorKind.equals("default")) {
                            //on peut avoir un constructeur par defaut ou par copie implicite.
                        } else if (nbActualArgs > 0 && constructorKind.equals("default")) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(2), "(TC58) No default constructor defined in class " + className);
                        } else if (nbActualArgs == 1 && constructorKind.equals("copy")) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(2), "(TC60) No copy constructor defined in class " + className);
                        } else if (nbActualArgs != 1 && constructorKind.equals("copy")) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC63) Unexpected number of arguments in copy constructor  " + className);
                        } else if (constructorKind.equals("other")) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC64) No constructor declared in class " + className);
                        }
                    } else {
                        if (localConstructorDecls.tail != null) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC57) Several constructors found in class " + className);
                        }
                        constructorDecl = localConstructorDecls.head;
                        CallArrow callArrow = CallGraph.addCallArrow(unit, SymbolTableConstants.CALLS, constructorDecl.unit());
                        callArrow.setSrcCallName(className);
                        instrOrExpr.setAnnotation("callArrow", callArrow);
                        FunctionTypeSpec constructorTypeSpec = constructorDecl.functionTypeSpec();
                        if (constructorTypeSpec != null) {
                            formalArgsTypes = constructorTypeSpec.argumentsTypes;
                        }
                    }
                } else {
                    TypeDecl typeDecl = this.getTypeDecl(className);
                    if (typeDecl != null && TypeSpec.isA(typeDecl.typeSpec, SymbolTableConstants.COMPOSITETYPE)
                            && ((CompositeTypeSpec) typeDecl.typeSpec.wrappedType).isRecordType()) {
                        // This is a Record-type constructor:
                        returnedType = typeDecl.typeSpec;
                        CompositeTypeSpec recordType = (CompositeTypeSpec) returnedType.wrappedType;
                        if (recordType.fields != null) {
                            formalArgsTypes = new WrapperTypeSpec[recordType.fields.length];
                            for (int i = recordType.fields.length - 1; i >= 0; --i) {
                                if (recordType.fields[i] != null) {
                                    formalArgsTypes[i] = recordType.fields[i].type();
                                } else {
                                    formalArgsTypes[i] = new WrapperTypeSpec(null);
                                }
                            }
                        }
                    } else {
                        TapEnv.fileError(instrOrExpr, "(TC65) Class or Structured type not found: " + className);
                        returnedType = null;
                    }
                }

                // Do the actual checking of actual Types versus formal Types.
                if (formalArgsTypes != null) {
                    for (int i = formalArgsTypes.length - 1; i >= 0; --i) {
                        if (formalArgsTypes[i] != null && formalArgsTypes[i].wrappedType != null
                                && i < actualArgsTypes.length && actualArgsTypes[i] != null
                                && !formalArgsTypes[i].wrappedType.receivesNoInferenceNoVector(actualArgsTypes[i])) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, actualArgs[i], "(TC30) Type mismatch in argument " + (i + 1) + " of constructor " + className + ", expected " + formalArgsTypes[i].showType() + ", is here " + actualArgsTypes[i].showType());
                        }
                    }
                }
                break;
            }
            case ILLang.op_ioCall:
                String functionName = ILUtils.getCallFunctionNameString(instrOrExpr);
                TapList<FunctionDecl> ioFunctionDecls = getFunctionDecl(functionName, null, null, false); //only one function expected with this name.
                FunctionDecl ioFunctionDecl = ioFunctionDecls == null ? null : ioFunctionDecls.head;
                if (ioFunctionDecl == null && (unit == null || unit.isC())) {
                    TapEnv.fileWarning(TapEnv.MSG_DEFAULT, instrOrExpr.down(1), "(TC33) External IO procedure " + functionName + " is not declared");
                    Unit functionUnit =
                            Unit.makeExternal(functionName, unit.callGraph(), language, false);
                    functionUnit.setFunctionTypeSpec(new FunctionTypeSpec(new WrapperTypeSpec(null)));
                    FunctionDecl functionDecl = new FunctionDecl(instrOrExpr.down(1), functionUnit);
                    unit.privateSymbolTable().addSymbolDecl(functionDecl);
                }
                typeCheck(instrOrExpr.down(3), true,
                        ILUtils.isIORead(instrOrExpr), false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_modifiers: {
                int nbSons = instrOrExpr.length();
                for (int i = 1; i <= nbSons; i++) {
                    if (instrOrExpr.down(i).opCode() != ILLang.op_ident) {
                        TypeSpec modifierType = typeCheck(instrOrExpr.down(i), true, false, false, instruction, isBeginning, currentUnit);
                        // Special checking of types of "kind" modifiers
                        if (instrOrExpr.down(i).opCode() == ILLang.op_nameEq && ILUtils.isIdent(instrOrExpr.down(i).down(1), "kind", false)) {
                            if (!modifierType.baseTypeName().equals("integer")) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC16) Type mismatch in named argument: " + ILUtils.toString(instrOrExpr.down(i)) + " expects an integer and is " + modifierType.showType());
                            }
                        }
                    }
                }
                break;
            }
            case ILLang.op_varDimDeclaration:
            case ILLang.op_dimColons:
            case ILLang.op_varDeclarations:
            case ILLang.op_declarations:
            case ILLang.op_declarators:
            case ILLang.op_expressions: {
                WrapperTypeSpec lastTypeSpec = null;
                int nbSons = instrOrExpr.length();
                for (int i = 1; i <= nbSons; i++) {
                    lastTypeSpec = typeCheck(instrOrExpr.down(i), true, noFunctionCall, false, instruction, isBeginning, currentUnit);
                }
                returnedType = lastTypeSpec;
                break;
            }
            case ILLang.op_arrayDeclarator:
                typeCheck(instrOrExpr.down(2), false, noFunctionCall, false, instruction, isBeginning, currentUnit);
                returnedType = typeCheck(instrOrExpr.down(1), false, noFunctionCall, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_pointerDeclarator:
            case ILLang.op_functionDeclarator:
                returnedType = typeCheck(instrOrExpr.down(1), false, noFunctionCall, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_modifiedDeclarator:
                returnedType = typeCheck(instrOrExpr.down(2), false, noFunctionCall, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_sizedDeclarator:
                returnedType = typeCheck(instrOrExpr.down(1), false, noFunctionCall, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_complexConstructor: {
                WrapperTypeSpec realType;
                WrapperTypeSpec imagType;
                realType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                if (realType != null && !realType.checkNumeric()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC09) Real part of complex constructor is expected to be numeric, and is here " + realType.showType());
                }
                imagType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                if (imagType != null && !imagType.checkNumeric()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(2), "(TC10) Imaginary part of complex constructor is expected to be numeric, and is here " + imagType.showType());
                }
                returnedType = getTypeDecl("complex").typeSpec;
                break;
            }
            case ILLang.op_unary: {
                WrapperTypeSpec rhsType;
                rhsType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                String unaryCallName = instrOrExpr.down(1).stringValue();
                if ((unit == null || unit.isC())
                        && (unaryCallName.equals("++postfix")
                        || unaryCallName.equals("++prefix")
                        || unaryCallName.equals("--postfix")
                        || unaryCallName.equals("--prefix")
                        || unaryCallName.equals("+")
                        || unaryCallName.equals("~"))) {
                    returnedType = rhsType;
                } else {
                    Tree oldTree = instrOrExpr;
                    FunctionDecl functionDecl = null;
                    if (rhsType != null) {
                        WrapperTypeSpec[] types = new WrapperTypeSpec[1];
                        types[0] = rhsType;
                        Tree newTree = ILUtils.buildCall(
                                instrOrExpr.down(1).copy(),
                                ILUtils.build(ILLang.op_expressions,
                                        instrOrExpr.down(2).copy()));
                        if (TapEnv.traceTypeCheckAnalysis()) {
                            TapEnv.indentOnTrace(TapEnv.traceIndent());
                            TapEnv.printlnOnTrace("Understand unary " + instrOrExpr + " as function call " + newTree);
                        }
                        instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
                        instrOrExpr.setAnnotation("sourcetree", oldTree);
                        TapList<InterfaceDecl> interfaceDecls = getAllInterfaceDecls(unaryCallName);
                        InterfaceDecl interfaceDecl = null;
                        while (interfaceDecls != null && functionDecl == null) {
                            interfaceDecl = interfaceDecls.head;
                            functionDecl = interfaceDecl.findFunctionDecl(types, null, instrOrExpr);
                            interfaceDecls = interfaceDecls.tail;
                        }
                        if (interfaceDecl != null && functionDecl == null) { //last chance: only check number of arguments :
                            functionDecl = interfaceDecl.findOneFunctionDecl(types, instrOrExpr);
                            if (functionDecl != null) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC34) Found no specific procedure that matches generic interface call: " + ILUtils.toString(instrOrExpr) + "; Took fallback choice: " + functionDecl.symbol);
                            }
                        }
                    }
                    if (functionDecl == null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, oldTree, "(TC39) Overloaded operator " + oldTree.down(1).stringValue() + " undefined for type " + rhsType.showType());
                        returnedType = null;
                    } else {
                        returnedType = typeCheck(instrOrExpr, true, false, false, instruction, isBeginning, currentUnit);
                    }
                }
                break;
            }
            case ILLang.op_binary: {
                int rankLeft = 1;
                int rankOp = 2;
                int rankRight = 3;
                String binaryCallName = null;
                if (unit == null || unit.isC()) {
                    // en C/C++ l'arbre est son1=op, left, right
                    // en Fortran l'arbre est left, son2=op, right
                    rankLeft = 2;
                    rankOp = 1;
                    binaryCallName = instrOrExpr.down(1).stringValue();
                }
                WrapperTypeSpec lhsType =
                        typeCheck(instrOrExpr.down(rankLeft), true, false, false, instruction, isBeginning, currentUnit);
                WrapperTypeSpec rhsType =
                        typeCheck(instrOrExpr.down(rankRight), true, false, false, instruction, isBeginning, currentUnit);
                lhsType = checkPointerInFortranExpression(lhsType, instrOrExpr, rankLeft);
                rhsType = checkPointerInFortranExpression(rhsType, instrOrExpr, rankRight);

                if ((unit == null || unit.isC())
                        && (binaryCallName.equals("&")
                        || binaryCallName.equals("|")
                        || binaryCallName.equals("^"))) {
                    returnedType = lhsType;
                } else {
                    FunctionDecl functionDecl = null;
                    Tree oldTree = instrOrExpr;
                    if (lhsType != null && rhsType != null) {
                        TapList<InterfaceDecl> interfaceDecls =
                                getAllInterfaceDecls(instrOrExpr.down(rankOp).stringValue());
                        WrapperTypeSpec[] types = new WrapperTypeSpec[2];
                        types[0] = lhsType;
                        types[1] = rhsType;
                        Tree newTree = ILUtils.buildCall(
                                instrOrExpr.down(rankOp).copy(),
                                ILUtils.build(ILLang.op_expressions,
                                        instrOrExpr.down(rankLeft).copy(),
                                        instrOrExpr.down(rankRight).copy()));
                        if (TapEnv.traceTypeCheckAnalysis()) {
                            TapEnv.indentOnTrace(TapEnv.traceIndent());
                            TapEnv.printlnOnTrace("Understand binary " + instrOrExpr + " as function call " + newTree);
                        }
                        instrOrExpr = instruction.replaceTree(instrOrExpr, newTree);
                        instrOrExpr.setAnnotation("sourcetree", oldTree);
                        InterfaceDecl interfaceDecl = null;
                        while (interfaceDecls != null && interfaceDecl == null) {
                            interfaceDecl = interfaceDecls.head;
                            functionDecl = interfaceDecl.findFunctionDecl(types, null, instrOrExpr);
                            interfaceDecls = interfaceDecls.tail;
                        }
                        if (interfaceDecl != null && functionDecl == null) { //last chance: only check number of arguments :
                            functionDecl =
                                    interfaceDecl.findOneFunctionDecl(types, instrOrExpr);
                            if (functionDecl != null) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC34) Found no specific procedure that matches generic interface call: " + ILUtils.toString(instrOrExpr) + "; Took fallback choice: " + functionDecl.symbol);
                            }
                        }
                    }
                    if (functionDecl == null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, oldTree, "(TC39) Overloaded operator " + oldTree.down(rankOp).stringValue() + " undefined for types " + " " + lhsType.showType() + " " + rhsType.showType());
                        returnedType = null;
                    } else {
                        returnedType = typeCheck(instrOrExpr, true, false, false, instruction, isBeginning, currentUnit);
                    }
                }
                break;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_power: {
                WrapperTypeSpec lhsType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                WrapperTypeSpec rhsType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                lhsType = checkPointerInFortranExpression(lhsType, instrOrExpr, 1);
                rhsType = checkPointerInFortranExpression(rhsType, instrOrExpr, 2);
                if (TapEnv.relatedLanguageIsFortran()
                        && lhsType != null && rhsType != null
                        && (!lhsType.checkNumeric() || !rhsType.checkNumeric())) {
                    WrapperTypeSpec[] types = new WrapperTypeSpec[2];
                    types[0] = lhsType;
                    types[1] = rhsType;
                    returnedType = getOverloadedWrapperTypeSpec(instrOrExpr, instruction, instrOrExpr.opName(), types);
                } else {
                    returnedType = canCombineNumeric(lhsType, rhsType, instrOrExpr);
                }
                break;
            }
            case ILLang.op_eq:
            case ILLang.op_neq: {
                WrapperTypeSpec rhsType;
                WrapperTypeSpec lhsType;
                lhsType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                rhsType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                if (lhsType != null && rhsType != null) {
                    if (!TypeSpec.canCompare(lhsType, rhsType)) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC18) Type mismatch in comparison: " + lhsType.showType() + " compared to " + rhsType.showType());
                    }
                    if (lhsType.isRealOrComplexBase() || rhsType.isRealOrComplexBase()) {
                        TapEnv.fileWarning(TapEnv.MSG_TRACE, instrOrExpr, "(TC19) Equality test on reals is not reliable");
                    }
                    // annotation utile pour decompiler .eqv. si comparaison entre boolean en fortran
                    if (lhsType.isBooleanBase() && rhsType.isBooleanBase()) {
                        instrOrExpr.setAnnotation("boolean", "boolean");
                    }
                }
                returnedType =
                        setConformingDimension(getTypeDecl("boolean").typeSpec, lhsType, rhsType, instrOrExpr);
                break;
            }
            case ILLang.op_and:
            case ILLang.op_xor:
            case ILLang.op_or: {
                WrapperTypeSpec rhsType;
                WrapperTypeSpec lhsType;
                lhsType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                rhsType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                if (this.unit != null && !this.unit.isC() && lhsType != null && !lhsType.checkBoolean()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC15) Argument of logical operator is expected to be boolean, and is here " + lhsType.showType());
                }
                if (this.unit != null && !this.unit.isC() && rhsType != null && !rhsType.checkBoolean()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(3), "(TC15) Argument of logical operator is expected to be boolean, and is here " + rhsType.showType());
                }
                returnedType =
                        setConformingDimension(getTypeDecl("boolean").typeSpec, lhsType, rhsType, instrOrExpr);
                break;
            }
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt: {
                WrapperTypeSpec rhsType;
                WrapperTypeSpec lhsType;
                lhsType = typeCheck(instrOrExpr.down(1), true, false, false,
                        instruction, isBeginning, currentUnit);
                rhsType = typeCheck(instrOrExpr.down(2), true, false, false,
                        instruction, isBeginning, currentUnit);
                if (TapEnv.relatedLanguageIsFortran()
                        &&
                        (lhsType != null && !lhsType.isMetaType() &&
                                lhsType.wrappedType != null && !lhsType.isNumericBase()
                                ||
                                rhsType != null && !rhsType.isMetaType() &&
                                        rhsType.wrappedType != null && !rhsType.isNumericBase())) {
                    WrapperTypeSpec[] types = new WrapperTypeSpec[2];
                    types[0] = lhsType;
                    types[1] = rhsType;
                    returnedType = getOverloadedWrapperTypeSpec(instrOrExpr, instruction, instrOrExpr.opName(), types);
                } else {
                    if (lhsType != null && !lhsType.isMetaType() &&
                            lhsType.wrappedType != null && !lhsType.isNumericBase()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1),
                                "(TC01) Expression is expected to be numeric, and is here " + lhsType.showType());
                    }
                    if (rhsType != null && !rhsType.isMetaType() &&
                            rhsType.wrappedType != null && !rhsType.isNumericBase()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(2),
                                "(TC01) Expression is expected to be numeric, and is here " + rhsType.showType());
                    }
                    returnedType =
                            setConformingDimension(getTypeDecl("boolean").typeSpec, lhsType, rhsType, instrOrExpr);
                }
                break;
            }
            case ILLang.op_mod: {
                WrapperTypeSpec rhsType;
                WrapperTypeSpec lhsType;
                lhsType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                rhsType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                if (lhsType != null && !lhsType.isIntegerBase()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC01) Expression is expected to be integer, and is here " + lhsType.showType());
                }
                if (rhsType != null && !rhsType.isIntegerBase()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(2), "(TC01) Expression is expected to be integer, and is here " + rhsType.showType());
                }
                returnedType = lhsType;
                break;
            }
            case ILLang.op_minus: {
                WrapperTypeSpec subType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                subType = checkPointerInFortranExpression(subType, instrOrExpr, 1);
                if (TapEnv.relatedLanguageIsFortran() && WrapperTypeSpec.isA(subType, SymbolTableConstants.COMPOSITETYPE)) {
                    WrapperTypeSpec[] types = new WrapperTypeSpec[1];
                    types[0] = subType;
                    returnedType = getOverloadedWrapperTypeSpec(instrOrExpr, instruction, "sub", types);
                } else {
                    if (subType != null
                            && !subType.isMetaType()
                            && subType.wrappedType != null
                            && !subType.isNumericBase()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC01) Expression is expected to be numeric, and is here "
                                + subType.showType());
                    }
                    returnedType = subType;
                }
                break;
            }
            case ILLang.op_not: {
                WrapperTypeSpec subType;
                subType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                if (this.unit != null && !this.unit.isC() && subType != null && !subType.checkBoolean()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(2), "(TC02) Expression is expected to be boolean, and is here " + subType.showType());
                    returnedType = setConformingDimension(
                            getTypeDecl("boolean").typeSpec,
                            subType, null, instrOrExpr);
                } else {
                    returnedType = subType;
                }
                break;
            }
            case ILLang.op_leftShift:
            case ILLang.op_rightShift: {
                WrapperTypeSpec leftType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                returnedType = leftType;
                break;
            }

            case ILLang.op_scopeAccess: {
                // [llh 14Mar19] TODO !
                returnedType = null;
                break;
            }
            case ILLang.op_fieldAccess: {
                WrapperTypeSpec recordType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                recordType = checkPointerInFortranExpression(recordType, instrOrExpr, 1);
                WrapperTypeSpec fieldType = null;
                if (recordType == null) {
                    fieldType = new WrapperTypeSpec(null);
                } else {
                    TapPair<WrapperTypeSpec, ArrayDim[]> afterPeel = TypeSpec.peelArrayDimsAroundComposite(recordType);
                    recordType = afterPeel.first;
                    if (TypeSpec.isA(recordType, SymbolTableConstants.POLYMORPHICTYPE)) {
                        // Incomplete patch: consider only the base type of this polymorphic type!
                        recordType = (WrapperTypeSpec)((PolymorphicTypeSpec)recordType.wrappedType).rootType ;
                    }
                    if (!recordType.checkRecord()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC03) Expression is expected to be a record, and is here " + recordType.showType());
                        fieldType = new WrapperTypeSpec(null);
                    } else {
                        if (TypeSpec.isA(recordType.wrappedType, SymbolTableConstants.COMPOSITETYPE)) {
                            fieldType = ((CompositeTypeSpec) recordType.wrappedType)
                                    .checkNamedFieldType(instrOrExpr, this, recordType);
                        } else if (TypeSpec.isA(recordType.wrappedType, SymbolTableConstants.CLASSTYPE)) {
                            ClassTypeSpec classType = (ClassTypeSpec) recordType.wrappedType;
                            SymbolTable currentSymbolTable = classType.unit.publicSymbolTable();
                            if (currentUnit != null) {
                                Unit upperClassUnit = currentUnit.getUpperLevelClassUnit();
                                if (upperClassUnit != null && upperClassUnit == classType.unit) {
                                    currentSymbolTable = upperClassUnit.privateSymbolTable();
                                } else if (upperClassUnit != null && upperClassUnit.subClassOf(classType.unit)) {
                                    currentSymbolTable = classType.unit.protectedSymbolTable();
                                }
                            }
                            fieldType = currentSymbolTable.typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, ((ClassTypeSpec) recordType.wrappedType).unit);
                        }
                        if (fieldType == null) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC45) No field named " + ILUtils.getIdentString(instrOrExpr.down(2)) + " in " + recordType.showType());
                        }
                    }
                    if (afterPeel.second != null)
                    //Case of a vectorial field access on an array of records:
                    {
                        fieldType = new WrapperTypeSpec(new ArrayTypeSpec(fieldType, afterPeel.second));
                    }
                }
                returnedType = fieldType;
                break;
            }
            case ILLang.op_arrayAccess: {
                WrapperTypeSpec elementType;
                WrapperTypeSpec indexType;
                int i;
                int realN;
                Tree indices;
                TapList<ArrayDim> newDimensionsList;
                ArrayDim[] newDimensions;
                indices = instrOrExpr.down(2);
                realN = indices.length();
                Tree varName = instrOrExpr.down(1);
                WrapperTypeSpec topType = typeCheck(varName, true, false, false, instruction, isBeginning, currentUnit);
                newDimensionsList = null;
                if (topType == null) {
                    elementType = null;
                } else {
                    topType = checkPointerInFortranExpression(topType, instrOrExpr, 1);
                    if (ILUtils.oneIsArrayTriplet(instrOrExpr.down(2)) && TypeSpec.isA(topType, SymbolTableConstants.FUNCTIONTYPE)) {
                        topType = changeFunctionToArrayTypeSpec(instrOrExpr.down(1), topType);
                    }
                    if (!topType.checkArray()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC05) Expression " + ILUtils.toString(varName) + " is expected to be an array, and is here " + topType.showType());
                        if (TypeSpec.isA(topType, SymbolTableConstants.FUNCTIONTYPE)) {
                            elementType = ((FunctionTypeSpec) topType.wrappedType).returnType;
                        } else {
                            elementType = topType;
                        }
                    } else {
                        elementType = topType.wrappedType.elementType();
                        if (((ArrayTypeSpec) topType.wrappedType).dimensions() == null) {
                            newDimensions = new ArrayDim[realN];
                            String identNext = ILUtils.getIdentString(varName);
                            for (i = realN - 1; i >= 0; i--) {
                                newDimensions[i] = new ArrayDim(ILUtils.build(ILLang.op_dimColon),
                                                                1, null, (identNext==null ? null : varName), i + 1, realN, 1);
                            }
                            ((ArrayTypeSpec) topType.wrappedType).setDimensions(newDimensions);
                        } else {
                            int formalN = ((ArrayTypeSpec) topType.wrappedType).dimensions().length;
                            if (formalN != realN) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC24) Wrong number of dimensions for array " + ILUtils.toString(varName) + ": " + formalN + " expected, and " + realN + " given");
                            }
                        }
                    }
                }

                ArrayDim newDim;
                for (i = 1; i <= realN; i++) {
                    indexType = typeCheck(indices.down(i), true, false, false, instruction, isBeginning, currentUnit);
                    if (indexType != null && indexType.wrappedType != null &&
                            !indexType.checkNumericInt()) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, indices.down(i), "(TC12) Array index is expected to be integer, and is here " + indexType.showType());
                    }
                    if (indices.down(i).opCode() == ILLang.op_arrayTriplet) {
                        newDim = ((ArrayTypeSpec) indexType.wrappedType).dimensions()[0] ;

                        if (!newDim.isSufficientlyDefined() //definedArrayTriplet(newDim)
                                && TypeSpec.isA(topType, SymbolTableConstants.ARRAYTYPE)
                                && i <= ((ArrayTypeSpec) topType.wrappedType).dimensions().length) {
                            newDim = ((ArrayTypeSpec) topType.wrappedType).dimensions()[i - 1];
                        }
                    } else {
                        newDim = null;
                    }
                    if (newDim != null && TypeSpec.isA(indexType, SymbolTableConstants.ARRAYTYPE)) {
                        newDimensionsList = new TapList<>(newDim, newDimensionsList);
                    }
                }
                if (newDimensionsList == null) {
                    returnedType = elementType;
                } else {
                    newDimensions = new ArrayDim[TapList.length(newDimensionsList)];
                    for (i = newDimensions.length - 1; i >= 0; i--) {
                        newDimensions[i] = newDimensionsList.head;
                        newDimensionsList = newDimensionsList.tail;
                    }
                    returnedType = new WrapperTypeSpec(new ArrayTypeSpec(elementType, newDimensions));
                }
                break;
            }
            case ILLang.op_substringAccess: {
                WrapperTypeSpec baseType;
                WrapperTypeSpec indexType;
                baseType = typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                for (int i = 2; i <= 3; i++) {
                    if (instrOrExpr.down(i).opCode() != ILLang.op_none) {
                        indexType = typeCheck(instrOrExpr.down(i), true, false, false, instruction, isBeginning, currentUnit);
                        if (!checkScalarNumericInt(indexType)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(i), "(TC14) Substring index is expected to be numeric, and is here " + indexType.showType());
                        }
                    }
                }
                returnedType = baseType;
                break;
            }
            case ILLang.op_arrayTriplet: {
                WrapperTypeSpec indexType;
                ArrayDim[] dimensions = new ArrayDim[1];
                for (int i = 1; i <= 3; ++i) {
                    if (instrOrExpr.down(i).opCode() != ILLang.op_none) {
                        indexType = typeCheck(instrOrExpr.down(i), true, false, false, instruction, isBeginning, currentUnit);
                        if (!checkScalarNumericInt(indexType)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(i), "(TC13) Triplet element is expected to be numeric, and is here " + indexType.showType());
                        }
                    }
                }
                Tree lengthTree =
                    ILUtils.buildSizeTree(instrOrExpr.down(1),
                                          instrOrExpr.down(2),
                                          instrOrExpr.down(3)) ;
                // dimension in a type can only hold a op_dimColon:
                dimensions[0] = new ArrayDim(ILUtils.build(ILLang.op_dimColon,
                                               ILUtils.build(ILLang.op_intCst, 1),
                                               lengthTree),
                                             1, null, null, -1, 0, 0) ;
                returnedType = new WrapperTypeSpec(new ArrayTypeSpec(getTypeDecl("integer").typeSpec, dimensions));
                break;
            }
            case ILLang.op_arrayConstructor: {
                WrapperTypeSpec resultTypeSpec = null;
                WrapperTypeSpec componentTypeSpec;
                Tree[] elements = instrOrExpr.children();
                // totalLength is the length of the result of the arrayConstructor, -1 if dynamic:
                int totalLength = 0;
                int elementLength;
                for (int i = elements.length - 1; i >= 0; --i) {
                    componentTypeSpec = typeCheck(elements[i], true, false, false, instruction,
                            isBeginning, currentUnit);
                    if (TypeSpec.isA(componentTypeSpec, SymbolTableConstants.ARRAYTYPE) && !(unit == null || unit.isC())) {
                        // In Fortran, an array component of a arrayConstructor stands for all its array elements:
                        if (totalLength >= 0) {
                            elementLength =
                                    ArrayTypeSpec.totalArrayLength(componentTypeSpec.wrappedType.getAllDimensions());
                            totalLength = elementLength == -1 ? -1 : totalLength + elementLength;
                        }
                        componentTypeSpec = componentTypeSpec.wrappedType.elementType();
                    } else {
                        if (totalLength >= 0) {
                            ++totalLength;
                        }
                    }
                    if (resultTypeSpec == null) {
                        resultTypeSpec = componentTypeSpec;
                    } else if (!resultTypeSpec.equalsCompilIndep(componentTypeSpec)) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC16) Type mismatch in array constructor: " + resultTypeSpec.showType() + " and " + componentTypeSpec.showType());
                    }
                }
                if (unit == null || unit.isC()) {
                    returnedType =
                            new WrapperTypeSpec(new PointerTypeSpec(resultTypeSpec,
                                    new ArrayDim(null, 0, (totalLength<=0 ? null : totalLength-1), null, -1, 0, 0)));
                } else {
                    returnedType =
                            new WrapperTypeSpec(new ArrayTypeSpec(resultTypeSpec,
                                    new ArrayDim[]{new ArrayDim(null, 1,
                                            totalLength <= 0 ? null : totalLength, null, -1, 0, 0)}
                            ));
                }
                break;
            }
            case ILLang.op_save:
                TapList<String> commonNames = getCallGraph().usedCommonNames;
                Tree[] sons = instrOrExpr.children();
                for (Tree son : sons) {
                    if (son != null &&
                            !(son.opCode() == ILLang.op_ident &&
                                    TapList.containsEquals(commonNames,
                                            son.stringValue()))) {
                        typeCheck(son, true, true, false, instruction, isBeginning, currentUnit);
                    }
                }
                break;
            case ILLang.op_ident:
            case ILLang.op_metavar: {
                String varName = ILUtils.getIdentString(instrOrExpr);
                if (inCompositeTypeSpec != null && inCompositeTypeSpec.namedFieldType(varName) != null) {
                    returnedType = inCompositeTypeSpec.namedFieldType(varName);
                    break;
                }
                if (onType) {
                    TypeDecl typeDecl = getTypeDecl(varName);
                    ClassDecl classDecl = getClassDecl(varName);
                    if (typeDecl != null && (typeDecl.typeSpec == null || TypeSpec.isA(typeDecl.typeSpec, SymbolTableConstants.NAMEDTYPE))) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC62) Undeclared type: " + varName);
                    }
                    if (typeDecl!=null || classDecl!=null) {
                        break;
                    }
                }

                // Get the variable's declaration in the current symbolTable:
                //  If the variable is not (sufficiently) declared, try the "implicit" mechanism.
                //  If everything else fails, complain and insert an "unknown" declaration.
                NewSymbolHolder sHolder = NewSymbolHolder.getNewSymbolHolder(instrOrExpr) ;
                VariableDecl symbolDecl = (sHolder!=null
                                           ? sHolder.newVariableDecl()
                                           : getVariableOrConstantOrFuncNameDecl(varName)) ;
                WrapperTypeSpec typeSpec;
                if (symbolDecl != null) {
                    returnedType = symbolDecl.type();
                    // check C definition and declaration of varName, cf examplesC v266:
                    if (TapEnv.relatedLanguageIsC() && symbolDecl.isATrueSymbolDecl && sHolder==null) {
                        VariableDecl tuSymbolDecl = null;
                        SymbolTable localSymbolTable = this;
                        if (this.unit == null) {
                            tuSymbolDecl = getTopVariableOrConstantDecl(varName); // this ==  translationUnitSymbolTable
                            if (tuSymbolDecl == null && !symbolDecl.isExtern()) {
                                returnedType = null;
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC20) Variable " + varName + " is not extern");
                                typeSpec = new WrapperTypeSpec(null);
                                VariableDecl newVarDecl = new VariableDecl(instrOrExpr, typeSpec);
                                this.addSymbolDecl(newVarDecl);
                                Tree newDecl = ILUtils.build(ILLang.op_varDeclaration,
                                        ILUtils.build(ILLang.op_modifiers),
                                        typeSpec.generateTree(this, null, null, true, null),
                                        ILUtils.build(ILLang.op_declarators,
                                                ILUtils.build(ILLang.op_ident, varName)));
                                Instruction instr = new Instruction(newDecl);
                                this.declarationsBlock.addInstrDeclTlBeforeUse(instr, new TapList<>(newVarDecl, null),
                                        null, this, true);
                                newVarDecl.setInstruction(instr);
                            }
                        } else if (this.unit.translationUnitSymbolTable() != null) {
                            while (tuSymbolDecl == null
                                    && localSymbolTable != this.unit.translationUnitSymbolTable().basisSymbolTable()) {
                                tuSymbolDecl = localSymbolTable.getTopVariableOrConstantDecl(varName);
                                localSymbolTable = localSymbolTable.basisSymbolTable;
                            }
                            if (tuSymbolDecl == null && !symbolDecl.isExtern()) {
                                returnedType = null;
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC20) Variable " + varName + " is not extern");
                                typeSpec = new WrapperTypeSpec(null);
                                this.unit.bodySymbolTable().addSymbolDecl(new VariableDecl(instrOrExpr, typeSpec));
                            }
                        }
                    }
                } else {
                    // vmp : on cherche funcDecl dans la Unit courante et pas au dessus
                    // varFunction faut chercher egalement dans les importedSymbolTables cf F90 v62
                    Unit enclosingClass = currentUnit;
                    if (currentUnit != null && !currentUnit.isClass()) {
                        enclosingClass = currentUnit.getUpperLevelClassUnit();
                    }
                    if (varName != null && enclosingClass != null && varName.equals("this")) {
                        // Case of pointer 'this' inside a class:
                        returnedType = new WrapperTypeSpec(new PointerTypeSpec(new WrapperTypeSpec(enclosingClass.classTypeSpec()), null));
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC75) Pointer 'this' not loaded in class " + enclosingClass.name());
                    } else {
                        TapList<FunctionDecl> funcDecls = getTopFunctionDecl(varName, null, null, false);
                        if (funcDecls == null) {
                            funcDecls = getFunctionDeclInUnit(varName, null, null, false, unit);
                        }
                        // on regarde dans les unit englobantes et leurs importedSymbolTables
                        if (funcDecls == null && unit != null) {
                            TapList<Unit> enclosingUnits = unit.enclosingUnits();
                            while (funcDecls == null && enclosingUnits != null) {
                                funcDecls = getFunctionDeclInUnit(varName, null, null, false, enclosingUnits.head);
                                enclosingUnits = enclosingUnits.tail;
                            }
                        }
                        // C/C++ sinon on regarde dans les globales
                        if (funcDecls == null && (unit == null || unit.isC())) {
                            funcDecls = getFunctionDecl(varName, null, null, false);
                        }

                        FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;

                        if (funcDecl != null) {
                            typeSpec = funcDecl.type();
                            returnedType = typeSpec;
                        } else if (instrOrExpr.parent() != null
                                && ((instrOrExpr.parent().opCode() == ILLang.op_cast
                                || instrOrExpr.parent().opCode() == ILLang.op_sizeof)
                                && instrOrExpr.rankInParent() == 1
                                || (instrOrExpr.parent().opCode() == ILLang.op_varDeclaration
                                || instrOrExpr.parent().opCode() == ILLang.op_modifiedType)
                                && instrOrExpr.rankInParent() == 2)) {
                            TypeDecl typeDecl = getTypeDecl(varName);
                            if (typeDecl == null || TypeSpec.isA(typeDecl.typeSpec, SymbolTableConstants.NAMEDTYPE)) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC62) Undeclared type: " + varName);
                            }
                            returnedType = null;
                        } else {
                            typeSpec = getImplicitTypeSpec(varName);
                            SymbolTable locSymbolTable = unit == null ? this : null;
                            if (locSymbolTable == null) {
                                locSymbolTable = unit.privateSymbolTable();
                            }
                            if (locSymbolTable == null) {
                                locSymbolTable = unit.publicSymbolTable();
                            }
                            if (typeSpec == null) {
                                TapEnv.fileWarning(TapEnv.MSG_DEFAULT, instrOrExpr, "(TC20) Variable " + varName + " is not declared");
                                typeSpec = new WrapperTypeSpec(null);
                                locSymbolTable.addSymbolDecl(new VariableDecl(instrOrExpr, typeSpec));
                            } else {
                                VariableDecl newVarDecl = new VariableDecl(instrOrExpr, typeSpec);
                                locSymbolTable.addSymbolDecl(newVarDecl);
                                Tree newDecl = ILUtils.build(ILLang.op_varDeclaration,
                                        ILUtils.build(ILLang.op_modifiers),
                                        typeSpec.generateTree(this, null, null, true, null),
                                        ILUtils.build(ILLang.op_declarators,
                                                ILUtils.build(ILLang.op_ident, newVarDecl.symbol)));
                                Instruction instr = new Instruction(newDecl);
                                this.declarationsBlock.addInstrDeclTlBeforeUse(instr, new TapList<>(newVarDecl, null),
                                        null, locSymbolTable, true);
                                newVarDecl.setInstruction(instr);
                            }
                            returnedType = typeSpec;
                        }
                    }
                }
                break;
            }
            case ILLang.op_label:
                returnedType = new WrapperTypeSpec(new LabelTypeSpec());
                break;
            case ILLang.op_intCst:
            case ILLang.op_bitCst:
                returnedType = getTypeDecl("integer").typeSpec;
                break;
            case ILLang.op_realCst:
                returnedType = realCstTypeSpec(instrOrExpr);
                break;
            case ILLang.op_boolCst:
                returnedType = getTypeDecl("boolean").typeSpec;
                break;
            case ILLang.op_concat: {
                typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                WrapperTypeSpec charType = getTypeDecl("character").typeSpec;
                if (unit == null || unit.isC()) {
                    returnedType = new WrapperTypeSpec(new PointerTypeSpec(charType,
                            new ArrayDim(null, 0, null, null, -1, 0, 0)));
                } else {
                    returnedType = new WrapperTypeSpec(new ArrayTypeSpec(charType,
                            new ArrayDim[]{new ArrayDim(null, 1, 0, null, -1, 0, 0)}));
                }
                break;
            }
            case ILLang.op_stringCst: {
                int length = instrOrExpr.isAtom() ? instrOrExpr.stringValue().length() : 0;
                WrapperTypeSpec charType = getTypeDecl("character").typeSpec;
                if (length == 1)
                // [vmp] on fait le choix length==1 -> character
                //  et pas tableau de taille 1 de character
                {
                    returnedType = charType;
                } else if (unit == null || unit.isC()) {
                    returnedType = new WrapperTypeSpec(new PointerTypeSpec(charType,
                            new ArrayDim(null, 0, null, null, -1, 0, 0)));
                } else {
                    returnedType = new WrapperTypeSpec(new ArrayTypeSpec(charType,
                            new ArrayDim[]{new ArrayDim(null, 1, length, null, -1, 0, 0)}));
                }
                break;
            }
            case ILLang.op_letter:
                returnedType = getTypeDecl("character").typeSpec;
                break;
            case ILLang.op_data: {
                // TODO: in all cases, check that lhsElemType can RECEIVE rhsType.
                Tree[] listOfVar = instrOrExpr.down(1).children();
                Tree[] listOfValue = instrOrExpr.down(2).children();
                SymbolTable locSymbolTable = this;
                if (this.unit.isModule() && locSymbolTable.isFormalParamsLevel()) {
                    locSymbolTable = this.unit.privateSymbolTable();
                }
                for (Tree value : listOfVar) {
                    locSymbolTable.typeCheck(value, true, true, false, instruction, isBeginning, currentUnit);
                }
                for (Tree tree : listOfValue) {
                    if (tree.opCode() == ILLang.op_repeatedValue) {
                        locSymbolTable.typeCheck(tree.down(2), true, false, false, instruction, isBeginning, currentUnit);
                    } else {
                        locSymbolTable.typeCheck(tree, true, false, false, instruction, isBeginning, currentUnit);
                    }
                }
                break;
            }
            case ILLang.op_nameList: {
                Tree[] listOfVar = instrOrExpr.down(2).children();
                for (Tree tree : listOfVar) {
                    typeCheck(tree, true, noFunctionCall, false, instruction, isBeginning, currentUnit);
                }
                break;
            }
            case ILLang.op_strings: {
                // May happen for multi-line strings
                Tree[] listOfString = instrOrExpr.children();
                int length = 0;
                for (Tree tree : listOfString) {
                    length = tree.stringValue().length();
                }
                if (length == 1) {
                    returnedType = getTypeDecl("character").typeSpec;
                } else {
                    // [vmp] on fait le choix length==1 -> character
                    //  et pas tableau de taille 1 de character
                    ArrayDim[] dims = new ArrayDim[1];
                    if (unit == null || unit.isC()) {
                        dims[0] = new ArrayDim(null, 1, null, null, -1, 0, 0);
                    } else {
                        dims[0] = new ArrayDim(null, 1, length, null, -1, 0, 0);
                    }
                    returnedType = new WrapperTypeSpec(new ArrayTypeSpec(getTypeDecl("character").typeSpec,
                            dims));
                }
                break;
            }
            case ILLang.op_allocate: {
                // Type-checking of op_allocate does 2 things:
                //  -- as expected, returns a "returnedType" for its result
                //  -- and accumulates info used to build the allocated zones
                //       later in CallGraph.prepareAllocationPoints(). In this
                //       info is the "allocatedType" for this allocated zone.
                // If allocating a scalar of type T,
                //         allocatedType=T            and returnedType=pointer to T.
                // At present [17/7/2012] behavior is different (depending
                // on C vs. F90) for arrays T[*] of objects of type T:
                //     C:  allocatedType=pointer to T and returnedType=pointer to T
                //   F90:  allocatedType=T[*]         and returnedType=pointer to T[*]
                typeCheck(instrOrExpr.down(1), expectResult, noFunctionCall, false, instruction, isBeginning, currentUnit);
                unit.setAllocDealloc();
                WrapperTypeSpec allocatedType;
                ArrayDim[] newDimensions;
                TapList<ArrayDim> newDimensionsList = null;
                Tree dimensions = instrOrExpr.down(2);
//                 if (assignDestination == null) {
//                     break; //Temporary, for BLN to "pass"
//                 }
                WrapperTypeSpec topType =
                        typeCheck(assignDestination, true, true, false, instruction, isBeginning, currentUnit);

                // TC of the constructor called, if any
                typeCheck(instrOrExpr.down(5), expectResult, noFunctionCall, false, instruction, isBeginning, currentUnit);
//                 TapEnv.printlnOnTrace(25, "  ALLOCATION POINT: " + instrOrExpr
//                         + " ASSIGNED INTO: " + assignDestination + " (in " + instrOrExpr.parent() + ")");
//                 TapEnv.printlnOnTrace(25, "   OF TYPE (topType):" + topType);
                if (dimensions.opCode() != ILLang.op_none) {
                    if (!unit.isC() && TypeSpec.isA(topType, SymbolTableConstants.POINTERTYPE)) {
                        // If we are in Fortran, allocatable arrays are pointers on arrays:
                        // we must peel the top "pointer" layer off, to expose the "array" layer.
                        topType = ((PointerTypeSpec) topType.wrappedType).destinationType;
                    }
                    int actualN = dimensions.length();
                    int formalN = 0;
                    WrapperTypeSpec elementType = null;
                    // Perform type inference inside topType, compute the element type,
                    // and send error messages:
                    if (topType != null) {
                        if (ILUtils.oneIsArrayTriplet(dimensions) && TypeSpec.isA(topType, SymbolTableConstants.FUNCTIONTYPE)) {
                            topType = changeFunctionToArrayTypeSpec(assignDestination, topType);
                        }
                        if (topType.checkArray()) {
                            //topType contains an ArrayTypeSpec :
                            elementType = topType.wrappedType.elementType();
                            if (((ArrayTypeSpec) topType.wrappedType).dimensions() == null) {
                                newDimensions = new ArrayDim[actualN];
                                String identNext = ILUtils.getIdentString(assignDestination);
                                for (int i = actualN - 1; i >= 0; i--) {
                                    newDimensions[i] = new ArrayDim(ILUtils.build(ILLang.op_dimColon),
                                            1, null, (identNext==null ? null : assignDestination), i + 1, actualN, 1);
                                }
                                ((ArrayTypeSpec) topType.wrappedType).setDimensions(newDimensions);
                            } else {
                                formalN = ((ArrayTypeSpec) topType.wrappedType).dimensions().length;
                                if (formalN != actualN) {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC24) Wrong number of dimensions for array " + ILUtils.toString(ILUtils.baseTree(assignDestination)) + ": " + formalN + " expected, and " + actualN + " given");
                                }
                            }
                        } else if (topType.checkPointer()) {
                            //topType contains an PointerTypeSpec :
                            elementType = ((PointerTypeSpec) topType.wrappedType).destinationType;
                        } else if (TypeSpec.isA(topType, SymbolTableConstants.FUNCTIONTYPE)) {
                            elementType = ((FunctionTypeSpec) topType.wrappedType).returnType;
                        } else {
                            elementType = topType;
                        }
                    }
                    // Build the returned WrapperTypeSpec:
                    //  pointer on (array of known dimensions of elementType):
                    for (int i = 1; i <= actualN; ++i) {
                        Tree index = dimensions.down(i);
                        WrapperTypeSpec indexType = typeCheck(index, true, false, false, instruction, isBeginning, currentUnit);
                        if (indexType != null && indexType.wrappedType != null &&
                                !indexType.checkNumericInt()) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, index, "(TC12) Array dimension is expected to be integer, and is here " + indexType.showType());
                        }
                        ArrayDim newDim;
                        if (index.opCode() == ILLang.op_arrayTriplet) {
                            newDim = new ArrayDim(ILUtils.copy(index), 1, null, null, -1, 0, 0);
                            //[llh 20Oct17] I'm not sure about the following refineTripletDimension():
                            // for an allocate, newDim should save the firstIndex (and stride) of the allocate,
                            // rather than the computed size = lastIndex-firstIndex+1
                            refineTripletDimension(index, newDim, false);
                            if (!newDim.isSufficientlyDefined()//definedArrayTriplet(newDim)
                                    && TypeSpec.isA(topType, SymbolTableConstants.ARRAYTYPE) && i <= formalN) {
                                newDim = ((ArrayTypeSpec) topType.wrappedType).dimensions()[i - 1];
                            }
                        } else if (index.opCode() == ILLang.op_dimColon) {
                            newDim = new ArrayDim(ILUtils.copy(index), 1, null, null, -1, 0, 0);
                        } else {
                            newDim = new ArrayDim(ILUtils.build(ILLang.op_dimColon,
                                    ILUtils.build(ILLang.op_intCst, 1),
                                    ILUtils.copy(index)),
                                    1, null, null, -1, 0, 0);
                        }
                        newDimensionsList = new TapList<>(newDim, newDimensionsList);
                    }
                    newDimensions = new ArrayDim[TapList.length(newDimensionsList)];
                    for (int i = newDimensions.length - 1; i >= 0; i--) {
                        newDimensions[i] = newDimensionsList.head;
                        newDimensionsList = newDimensionsList.tail;
                    }
                    // Behavior for C and Fortran90 are different.
                    // cf F90:cm16 and C:lh23 ...
                    if (this.unit.isC()) {
                        allocatedType =
                                new WrapperTypeSpec(new PointerTypeSpec(elementType, newDimensions[0]));
                        returnedType = allocatedType;
                    } else {
                        allocatedType = new WrapperTypeSpec(new ArrayTypeSpec(elementType, newDimensions));
                        returnedType = new WrapperTypeSpec(new PointerTypeSpec(allocatedType, null));
                    }
                } else {
                    // Case of allocation of a scalar object (no dimension):
                    allocatedType = topType;
                    if (allocatedType.checkPointer()) {
                        allocatedType =
                                ((PointerTypeSpec) allocatedType.wrappedType).destinationType;
                    }
                    returnedType = topType;
                }
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.printlnOnTrace("   -> ALLOCATED TYPE: " + allocatedType + " RETURNED TYPE: " + returnedType);
                }
                // Register for future creation of the allocated zone:
                unit.callGraph().addAllocationPoint(instrOrExpr, assignDestination, allocatedType, this);
                // The "no allocation allowed" test for "assignDestination" is done later, in PointerAnalyzer.
                break;
            }
            case ILLang.op_deallocate: {
                unit.setAllocDealloc();
                typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                // The "no allocation allowed" for an expression is done in the pointer Analyzer,
                // because in the case of fortran allocatables, we must access to the symbolDecl
                // which is used in this expression.
                break;
            }
            case ILLang.op_nullify: {
                WrapperTypeSpec typeSpec;
                int nbSons = instrOrExpr.length();
                for (int i = 1; i <= nbSons; i++) {
                    typeSpec = typeCheck(instrOrExpr.down(i), true, true, false, instruction, isBeginning, currentUnit);
                    if (!TypeSpec.isA(typeSpec, SymbolTableConstants.POINTERTYPE)) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC48) Nullify a non-pointer expression " + ILUtils.toString(instrOrExpr));
                    }
                }
                break;
            }
            case ILLang.op_pointerAccess: {
                if (instrOrExpr.down(1).opCode() == ILLang.op_add
                        && instrOrExpr.down(2).opCode() == ILLang.op_none) {
                    WrapperTypeSpec indexTypeInAdd =
                            typeCheck(instrOrExpr.down(1).down(2), true, false, false, instruction, isBeginning, currentUnit);
                    if (indexTypeInAdd != null && indexTypeInAdd.wrappedType != null
                            && indexTypeInAdd.checkNumericInt()) {
                        // *(a+i)           pointerAccess(add(ident a,ident i),none())
                        // est transforme' en
                        // a[i]             pointerAccess(ident a,ident i)
                        instrOrExpr.setChild(ILUtils.copy(instrOrExpr.down(1).down(2)), 2);
                        instrOrExpr.setChild(ILUtils.copy(instrOrExpr.down(1).down(1)), 1);
                    }
                }
                WrapperTypeSpec pointerTypeSpec =
                        typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                WrapperTypeSpec indexType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                if (indexType != null && indexType.wrappedType != null
                        && !indexType.checkNumericInt()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(2), "(TC12) Array index is expected to be integer, and is here " + indexType.showType());
                }
                if (TypeSpec.isA(pointerTypeSpec, SymbolTableConstants.POINTERTYPE)) {
                    returnedType = ((PointerTypeSpec) pointerTypeSpec.wrappedType).destinationType;
                } else if (TypeSpec.isA(pointerTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                    // PB ce tableau devrait etre transforme en pointer!!!!
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC40) Pointer access to an array expression " + ILUtils.toString(instrOrExpr));
                    returnedType = pointerTypeSpec.wrappedType.elementType();
                } else {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC40) Pointer access to a non-pointer expression " + ILUtils.toString(instrOrExpr));
                    returnedType = null;
                }
                break;
            }
            case ILLang.op_address: {
                WrapperTypeSpec addressedType;
                addressedType = typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                returnedType = new WrapperTypeSpec(new PointerTypeSpec(addressedType, null));
                break;
            }
            case ILLang.op_cast:
            case ILLang.op_multiCast: {
                typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                WrapperTypeSpec typeInside =
                    typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                WrapperTypeSpec castTypeCheck = null;
                String castTypeName;
                if (instrOrExpr.down(1).opCode() == ILLang.op_ident) {
                    castTypeName = instrOrExpr.down(1).stringValue();
                    TypeDecl castTypeDecl = getTypeDecl(castTypeName);
                    if (castTypeDecl != null) {
                        castTypeCheck = castTypeDecl.typeSpec;
                    }
                } else if (instrOrExpr.down(1).opCode() == ILLang.op_void) {
                    TypeDecl castTypeDecl = getTypeDecl("void");
                    if (castTypeDecl != null) {
                        castTypeCheck = castTypeDecl.typeSpec;
                    }
                } else {
                    castTypeCheck = TypeSpec.build(instrOrExpr.down(1), this, instruction,
                            new TapList<>(null, null), new TapList<>(null, null),
                            new TapList<>(null, null), new ToBool(false), null);
                }
                if (instrOrExpr.opCode()==ILLang.op_multiCast && castTypeCheck!=null) {
                    castTypeCheck =
                        new WrapperTypeSpec(
                            new ArrayTypeSpec(castTypeCheck,
                                     ((ArrayTypeSpec)typeInside.wrappedType).dimensions())) ;
                } else if (castTypeCheck == null) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC61) Illegal cast expression " + ILUtils.toString(instrOrExpr));
                    castTypeCheck = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                }
                returnedType = castTypeCheck;
                break;
            }
            case ILLang.op_varDeclaration: {
                typeCheck(instrOrExpr.down(2), true, true, true, instruction, isBeginning, currentUnit);
                Tree[] declarators = instrOrExpr.down(3).children();
                for (Tree declarator : declarators) {
                    if (declarator.opCode() == ILLang.op_assign
                        || declarator.opCode() == ILLang.op_pointerAssign
                        || declarator.opCode() == ILLang.op_arrayDeclarator) {
                        typeCheck(declarator, false, noFunctionCall, false, instruction, isBeginning, currentUnit);
                    } else if (declarator.opCode() == ILLang.op_ident) {
// [Duplic FunctionDecl mode:]
                        TapList<FunctionDecl> funcDecls =
                                    getTopFunctionDecl(declarator.stringValue(), null, null, false);
                        FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head; // assume there's only 1 function that matches.
                        if (funcDecl != null
                            //|| (unit!=null && declarator.stringValue().equals(unit.name())) // CA SERAIT MIEUX AVEC CE TEST
                            ) {
                            declarator.setAnnotation("isFunctionName", Boolean.TRUE);
                        }
// [Unique FunctionDecl mode:]
//                        SymbolDecl symbolDecl = getSymbolDecl(declarator.stringValue()) ;
//                        if (symbolDecl!=null && (symbolDecl.isA(FUNCTION) || symbolDecl.isA(FUNCNAME)))
//                            declarator.createAnnotValue("isFunctionName", Boolean.TRUE);
// [End FunctionDecl mode.]
                    }
                }
                break;
            }
            case ILLang.op_typeDeclaration: {
                if (instrOrExpr.down(2).opCode() != ILLang.op_ident) {
                    typeCheck(instrOrExpr.down(2), true, true, false, instruction, isBeginning, currentUnit);
                }
                break;
            }
            case ILLang.op_sizeof:
                typeCheck(instrOrExpr.down(1), expectResult, noFunctionCall, true, instruction, isBeginning, currentUnit);
                returnedType = getTypeDecl("integer").typeSpec;
                break;
            case ILLang.op_pointerType:
                typeCheck(instrOrExpr.down(1), true, true, true, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_modifiedType:
                typeCheck(instrOrExpr.down(1), true, true, false, instruction, isBeginning, currentUnit);
                typeCheck(instrOrExpr.down(2), true, false, true, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_dimColon:
                typeCheck(instrOrExpr.down(1), true, false, false, instruction, isBeginning, currentUnit);
                returnedType = typeCheck(instrOrExpr.down(2), true, false, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_recordType: {
                CompositeTypeSpec inPreviousCompositeTypeSpec = inCompositeTypeSpec;
                Object annot = instrOrExpr.getAnnotation("WrapperTypeSpec");
                if (annot != null) {
                    inCompositeTypeSpec = (CompositeTypeSpec) ((WrapperTypeSpec) annot).wrappedType;
                } else if (instrOrExpr.down(3).opCode() != ILLang.op_none) {
                    // cf set10/v298
                    TapEnv.toolError("Type checking " + ILUtils.toString(instrOrExpr));
                }
                typeCheck(instrOrExpr.down(3), true, true, false, instruction, isBeginning, currentUnit);
                inCompositeTypeSpec = inPreviousCompositeTypeSpec;
                break;
            }
            case ILLang.op_function:
            case ILLang.op_program:
            case ILLang.op_module:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
            case ILLang.op_nameSpace:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            case ILLang.op_arrayType:
            case ILLang.op_enumType:
                typeCheck(instrOrExpr.down(2), true, true, false, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_unionType:
                CompositeTypeSpec inPreviousCompositeTypeSpec = inCompositeTypeSpec;
                inCompositeTypeSpec = (CompositeTypeSpec) ((WrapperTypeSpec) instrOrExpr.getAnnotation("WrapperTypeSpec")).wrappedType;
                typeCheck(instrOrExpr.down(2), true, true, false, instruction, isBeginning, currentUnit);
                inCompositeTypeSpec = inPreviousCompositeTypeSpec;
                break;
            case ILLang.op_interfaceDecl:
                // [vmp] on ne typechecke pas l'instruction interfaceDecl,
                // on typeCheck l'interfaceUnit creee dans FlowGraphBuilder
                // en meme temps que l'instruction op_interfaceDecl
                break;
            case ILLang.op_classType:
                typeCheck(instrOrExpr.down(1), false, true, true, instruction, isBeginning, currentUnit);
                break;
            case ILLang.op_useDecl:
                break;
            default:
                TapEnv.toolWarning(-1, "(Type checking) Unexpected operator: " + instrOrExpr.opName());
                break;
        }
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.setTraceIndent(TapEnv.traceIndent() - 2);
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("TypeCheck of " + instrOrExpr + " returns " + returnedType);
        }
        return returnedType;
    }

    public void collectPointedFunctionNames(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_assign:
            case ILLang.op_pointerAssign:
                collectWhenFunctionName(tree.down(2)) ;
                break ;
            case ILLang.op_call: {
                Tree[] actualArgs = ILUtils.getArguments(tree).children();
                for (int i=actualArgs.length-1 ; i>=0 ; --i) {
                    collectWhenFunctionName(actualArgs[i]) ;
                }
                break ;
            }
        }
        Tree[] children = tree.children() ;
        if (children!=null) {
            for (int i=children.length-1 ; i>=0 ; --i) {
                collectPointedFunctionNames(children[i]) ;
            }
        }
    }

    /** 
     * When "tree" is an immediate procedure name, i.e. the actual name of a procedure in the code
     * (and not a variable containing a function name nor a pointer pointing to a function)
     * then add the Unit of this procedure name into the global list "callGraph.allPointedUnits".
     * All Units in this list will later receive a special zone.
     * This zone will allow pointer destination analysis to propagate destinations of pointers-to-procedure. */
    private void collectWhenFunctionName(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_ident: {
                ToObject<SymbolTable> toST = new ToObject<>(null) ;
                TapList<SymbolDecl> toBucket = getToDecl(ILUtils.getIdentString(tree), SymbolTableConstants.FUNCTION, false, toST) ;
                FunctionDecl decl = (toBucket==null ? null : (FunctionDecl)toBucket.tail.head) ;
                SymbolTable funcDeclST = toST.obj() ;
                if (decl!=null
                    // Do not collect if this is the name of a formal argument (this is not an immediate name of procedure):
                    && !(funcDeclST.isPublic && funcDeclST.unit!=null && funcDeclST.unit.isStandard())
                    // Do not collect if this is the name of the current unit (this is its return value)
                    && !(unit.isFortran() && ILUtils.getIdentString(tree).equals(unit.name()))
                    ) {
                    Unit unit = decl.unit() ;
                    CallGraph callGraph = getCallGraph() ;
                    if (unit!=null && !TapList.contains(callGraph.allPointedUnits, unit)) {
                        callGraph.allPointedUnits = new TapList<>(unit, callGraph.allPointedUnits) ;
                    }
                }
                break;
            }
            case ILLang.op_address: 
            case ILLang.op_pointerAccess: {
                collectWhenFunctionName(tree.down(1));
                break;
            }
            case ILLang.op_arrayConstructor: {
                Tree[] children = tree.children();
                for(int i =0; i < children.length; ++i) {
                    collectWhenFunctionName(children[i]);
                }
                break;
            }
        }
    }

    private WrapperTypeSpec getOverloadedWrapperTypeSpec(Tree instrOrExpr, Instruction instruction,
                                                         String overloadedName,
                                                         WrapperTypeSpec[] types) {
        WrapperTypeSpec returnedType;
        TapList<InterfaceDecl> interfaceDecls = getAllInterfaceDecls(overloadedName);
        FunctionDecl functionDecl = null;
        InterfaceDecl interfaceDecl = null;
        while (interfaceDecls != null && functionDecl == null) {
            interfaceDecl = interfaceDecls.head;
            functionDecl = interfaceDecl.findFunctionDecl(types, null, instrOrExpr);
            interfaceDecls = interfaceDecls.tail;
        }
        if (interfaceDecl != null && functionDecl == null) { //last chance: only check number of arguments :
            functionDecl =
                    interfaceDecl.findOneFunctionDecl(types, instrOrExpr);
            if (functionDecl != null) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC34) Found no specific procedure that matches generic interface call: "
                        + ILUtils.toString(instrOrExpr) + "; Took fallback choice: " + functionDecl.symbol);
            }
        }
        if (functionDecl != null) {
            Tree newUnaryOrBinaryTree;
            Tree newTree;
            if (types.length == 1) {
                newUnaryOrBinaryTree = ILUtils.build(ILLang.op_unary,
                        ILUtils.build(ILLang.op_ident, overloadedName),
                        instrOrExpr.down(1).copy());
                newTree = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, overloadedName),
                        ILUtils.build(ILLang.op_expressions,
                                instrOrExpr.down(1).copy()));
            } else {
                newUnaryOrBinaryTree = ILUtils.build(ILLang.op_binary,
                        instrOrExpr.down(1).copy(),
                        ILUtils.build(ILLang.op_ident, overloadedName),
                        instrOrExpr.down(2).copy());
                newTree = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, overloadedName),
                        ILUtils.build(ILLang.op_expressions,
                                instrOrExpr.down(1).copy(),
                                instrOrExpr.down(2).copy()));
            }
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.indentOnTrace(TapEnv.traceIndent());
                TapEnv.printlnOnTrace("Replace " + instrOrExpr + " with explicit call " + newTree);
            }
            newTree.setAnnotation("sourcetree", newUnaryOrBinaryTree);
            CallArrow callArrow = CallGraph.addCallArrow(unit, SymbolTableConstants.CALLS, functionDecl.unit());
            callArrow.setSrcCallName(overloadedName);
            newTree.setAnnotation("callArrow", callArrow);
            instruction.replaceTree(instrOrExpr, newTree);
            returnedType = functionDecl.functionTypeSpec().returnType;
        } else {
            String showTypes = types[0].showType();
            if (types.length == 2) {
                showTypes = showTypes + " " + types[1].showType();
            }
            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr, "(TC39) Overloaded operator " + instrOrExpr.opName()
                    + " undefined for types " + showTypes);
            returnedType = null;
        }
        return returnedType;
    }

    // [Unique FunctionDecl mode:]
    private void removeDuplicateFunctionDecls() {
        TapList<SymbolDecl> allDecls = getAllTopSymbolDecls();
        TapList<SymbolDecl> allDuplicateFunctionDecls = null;
        SymbolDecl symbolDecl;
        TapList<FunctionDecl> rootFunctionDecls;
        while (allDecls != null) {
            symbolDecl = allDecls.head;
            if (symbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                rootFunctionDecls = basisSymbolTable.getFunctionDecl(symbolDecl.symbol, ((FunctionDecl) symbolDecl).returnTypeSpec(), ((FunctionDecl) symbolDecl).argumentsTypesSpec(), true);
                while (rootFunctionDecls != null) {
                    if (rootFunctionDecls.head == symbolDecl) {
                        allDuplicateFunctionDecls = new TapList<>(symbolDecl, allDuplicateFunctionDecls);
                    }
                    rootFunctionDecls = rootFunctionDecls.tail;
                }

            }
            allDecls = allDecls.tail;
        }
        while (allDuplicateFunctionDecls != null) {
            symbolDecl = allDuplicateFunctionDecls.head;
            removeDecl(symbolDecl.symbol, SymbolTableConstants.FUNCTION, true);
            allDuplicateFunctionDecls = allDuplicateFunctionDecls.tail;
        }
    }

    /**
     * True when this declaration declares a constructor or a destructor of class unit.
     */
    private boolean okWithNoType(Tree declTree, Unit unit) {
        if (unit.isCPlusPlus() && unit.isClass()) {
            Tree declarators = declTree.down(3);
            if (declarators.length() == 1 &&
                    declarators.down(1).opCode() == ILLang.op_functionDeclarator) {
                Tree funcDeclarator = declarators.down(1);
                String funcName = ILUtils.getIdentString(funcDeclarator.down(1));
                String className = unit.name();
                return funcName.equals(className) || funcName.equals("~" + className);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Terminate Instructions that declare a function name, i.e. :
     * (a) annotate function names with "isFunctionName" true.
     * (b) replace "none" typeTree with the true final return type (without dimensions!).
     */
    protected void terminateFunctionDeclarationInstructions(Tree declTree) {
        switch (declTree.opCode()) {
            case ILLang.op_ident: {
                SymbolDecl symbolDecl = getSymbolDecl(ILUtils.getIdentString(declTree));
                if (symbolDecl != null && (symbolDecl.isA(SymbolTableConstants.FUNCTION) || symbolDecl.isA(SymbolTableConstants.FUNCNAME))) {
                    declTree.setAnnotation("isFunctionName", Boolean.TRUE);
                }
                break;
            }
            case ILLang.op_varDeclaration:
                if (declTree.down(2) != null && declTree.down(2).opCode() == ILLang.op_none
                        // ... and don't set an UnknownType for perfectly legal untyped C++ constructors and destructors:
                        && !okWithNoType(declTree, unit)
                        // ... even stronger (subsumes the above), never do this in C++ :
                        && (this.unit == null || this.unit.language() != TapEnv.CPLUSPLUS)) {
                    WrapperTypeSpec returnType = declTree.down(2).getAnnotation("typeSpec");
                    // Declaration of the function return type must not have dimensions,
                    // otherwise Fortran thinks the function is an array.
                    if (TypeSpec.isA(returnType, SymbolTableConstants.ARRAYTYPE) && this.unit.isFortran()) {
                        returnType = ((ArrayTypeSpec)returnType.wrappedType).elementType() ;
                    }
                    Tree returnTypeTree;
                    if (returnType != null && returnType.wrappedType != null) {
                        returnTypeTree = returnType.generateTree(this, null, null, true, null);
                    } else {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, declTree, "(TC49) Unknown return type for function " + ILUtils.toString(declTree.down(3)));
                        returnTypeTree = ILUtils.build(ILLang.op_ident, "UnknownType");
                    }
                    declTree.setChild(returnTypeTree, 2);
                }
                terminateFunctionDeclarationInstructions(declTree.down(3));
                break;
            case ILLang.op_intrinsic:
            case ILLang.op_external:
            case ILLang.op_declarators: {
                Tree[] sons = declTree.children();
                for (int i = sons.length - 1; i >= 0; --i) {
                    terminateFunctionDeclarationInstructions(sons[i]);
                }
                break;
            }
            case ILLang.op_assign:
            case ILLang.op_pointerAssign:
            case ILLang.op_star:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_bitfieldDeclarator:
            case ILLang.op_modifiedDeclarator:
            case ILLang.op_constructorDeclarator:
            case ILLang.op_destructorDeclarator:
            case ILLang.op_arrayDeclarator:
            case ILLang.op_sizedDeclarator:
            case ILLang.op_varDimDeclaration:
                break;
            default:
//             TapEnv.toolWarning(-1, "(terminateFunctionDeclarationInstructions) Unexpected operator: "+declTree.opName()) ;
                break;
        }
    }

    private CallArrow setCallArrow(Tree instrOrExpr, Unit functionUnit,
                                   FunctionDecl functionDecl) {
        FunctionDecl[] initialValues = functionDecl.initFunctionDecls;
        if (initialValues == null && functionDecl.symbol != null) {
            VariableDecl funcVarDecl = getFuncVarDecl(functionDecl.symbol);
            initialValues = funcVarDecl == null ? null : funcVarDecl.initFunctionDecls;
        }
        int index = 0;
        if (instrOrExpr.down(1).opCode() == ILLang.op_arrayAccess) {
            index = instrOrExpr.down(1).down(2).down(1).intValue();
        }
        FunctionDecl initializer = null;
        if (initialValues != null && 0 <= index && index < initialValues.length) {
            initializer = initialValues[index];
        }
        CallArrow mainCallArrow = CallGraph.addCallArrow(unit, SymbolTableConstants.CALLS, 
                                    (initializer != null ? initializer.unit() : functionUnit));
        String origCallName = ILUtils.getCallFunctionNameString(instrOrExpr);
        if (origCallName != null) 
            mainCallArrow.setSrcCallName(origCallName);
        return mainCallArrow;
    }

    private WrapperTypeSpec sumResultType(WrapperTypeSpec actualArgType, Tree dim) {
        WrapperTypeSpec returnType = actualArgType.elementType();
        if (dim != null) {
            TapList<ArrayDim> dimensions = actualArgType.getAllDimensions();
            int dimValue = dim.intValue();
            if (dimValue == -1) {
                TapEnv.fileWarning(TapEnv.MSG_ALWAYS, dim, "(Type Checking of SUM) statically unknown DIM rank: " + ILUtils.toString(dim));
                dimValue = TapList.length(dimensions); // Arbitrary fallback
            } else if (dimValue <= 0 || dimValue > TapList.length(dimensions)) {
                TapEnv.fileWarning(TapEnv.MSG_ALWAYS, dim, "(Type Checking of SUM) DIM rank out of range: " + ILUtils.toString(dim));
                dimValue = TapList.length(dimensions); // Arbitrary fallback
            }
            dimValue = TapList.length(dimensions) - dimValue; // Fortran array dimensions are reversed!
            TapList<ArrayDim> toNewDimensions = new TapList<>(null, dimensions);
            TapList<ArrayDim> inNewDimensions = toNewDimensions;
            while (dimValue > 0) {
                inNewDimensions = inNewDimensions.tail;
                --dimValue;
            }
            inNewDimensions.tail = inNewDimensions.tail.tail;
            inNewDimensions = toNewDimensions.tail;
            if (inNewDimensions != null) {
                ArrayDim[] dimsArray = new ArrayDim[TapList.length(inNewDimensions)];
                int i = 0;
                while (inNewDimensions != null) {
                    dimsArray[i] = inNewDimensions.head;
                    ++i;
                    inNewDimensions = inNewDimensions.tail;
                }
                returnType = new WrapperTypeSpec(new ArrayTypeSpec(returnType, dimsArray));
            }
        }
        return returnType;
    }

    private WrapperTypeSpec spreadResultType(WrapperTypeSpec actualArgType, Tree dim, Tree length) {
        if (dim.opCode() == ILLang.op_nameEq) {
            dim = dim.down(2);
        }
        if (length.opCode() == ILLang.op_nameEq) {
            length = length.down(2);
        }
        WrapperTypeSpec returnType = actualArgType.elementType();
        TapList<ArrayDim> dimensions = actualArgType.getAllDimensions();
        int dimValue = dim.intValue();
        if (dimValue == -1) {
            TapEnv.fileWarning(TapEnv.MSG_ALWAYS, dim, "(Type Checking of SPREAD) statically unknown DIM rank: " + ILUtils.toString(dim));
            dimValue = TapList.length(dimensions) + 1; // Arbitrary fallback
        }
        dimValue = TapList.length(dimensions) + 1 - dimValue; // Fortran array dimensions are reversed!
        TapList<ArrayDim> toNewDimensions = new TapList<>(null, dimensions);
        TapList<ArrayDim> inNewDimensions = toNewDimensions;
        while (dimValue > 0) {
            inNewDimensions = inNewDimensions.tail;
            --dimValue;
        }
        inNewDimensions.tail = new TapList<>(new ArrayDim(length, null, null, null, -1, 0, 0), inNewDimensions.tail);
        inNewDimensions = toNewDimensions.tail;
        ArrayDim[] dimsArray = new ArrayDim[TapList.length(inNewDimensions)];
        int i = 0;
        while (inNewDimensions != null) {
            dimsArray[i] = inNewDimensions.head;
            ++i;
            inNewDimensions = inNewDimensions.tail;
        }
        returnType = new WrapperTypeSpec(new ArrayTypeSpec(returnType, dimsArray));
        return returnType;
    }

    /**
     * @return true if given "funcName" in the context of this SymbolTable is actually
     * a reference to a function named "refFuncName". Can be nontrivial with mixed language calls.
     */
    public boolean isFunctionNamed(String funcName, String refFuncName) {
        SymbolDecl symbolDecl = getSymbolDecl(funcName);
        return (symbolDecl instanceof FunctionDecl
                && symbolDecl.symbol.equals(refFuncName));
    }

    /**
     * @return true if mixed language call
     */
    private FunctionDecl mixedLanguageCall(String functionName,
                                           FunctionDecl localFunctionDecl) {
        boolean result;
        FunctionDecl calledFunctionDecl = null;
        if (TapEnv.inputLanguage() == TapEnv.MIXED) {
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.printlnOnTrace("  Checking Mixed-language call: " + functionName + " " + localFunctionDecl);
            }
            if (this.unit == null || this.unit.isC()) {
                // C calls Fortran:
                TapPair<String, FunctionDecl> fortranFuncName;
                String fortranName;
                FunctionDecl fortranFunctionDecl;
                TapList<FunctionDecl> fortranFunctionDecls;
                fortranFuncName = getCallGraph().getMixedLanguageFunctionName(functionName,
                                                                              TapEnv.C, TapEnv.FORTRAN);
                fortranFunctionDecl = fortranFuncName.second;
                fortranName = fortranFuncName.first;
                if (fortranFunctionDecl == null && fortranName != null) {
                    fortranFunctionDecls = getFunctionDecl(fortranName, null, null, false); // we assume to have only one prototype of fortranName.
                    fortranFunctionDecl = fortranFunctionDecls == null ? null : fortranFunctionDecls.head;
                    if (fortranFunctionDecl == null || fortranFunctionDecl.isExternal()) {
                        fortranFunctionDecl = getFunctionNotExternalNorInterfaceDecl(functionName);
                        if (fortranFunctionDecl == null || fortranFunctionDecl.isExternal()) {
                            fortranFunctionDecls = getCallGraph().fortranRootSymbolTable().getFunctionDecl(fortranName, null, null, false);
                            fortranFunctionDecl = fortranFunctionDecls == null ? null : fortranFunctionDecls.head;
                        }
                    }
                }
                if (fortranFunctionDecl != null
                        && localFunctionDecl != null
                        && localFunctionDecl.isExternal()
                        && (this.unit == null || fortranFunctionDecl.unit().language() != this.language)) {
                    getCallGraph().deleteUnit(localFunctionDecl.unit());
                    SymbolTable functionDeclHostSymbolTable = fortranFunctionDecl.unit().functionDeclHostSymbolTable() ;
                    while (localFunctionDecl.importedFrom!=null) {
                        localFunctionDecl = (FunctionDecl)localFunctionDecl.importedFrom.first ;
                    }
                    localFunctionDecl.importedFrom = new TapPair<>(fortranFunctionDecl, functionDeclHostSymbolTable);
                    localFunctionDecl.setUnit(fortranFunctionDecl.unit());
                    TapList<FunctionDecl> globalFunctionDecls = getCallGraph().cRootSymbolTable().
                            getFunctionDecl(functionName, null, null, false);
                    if (globalFunctionDecls != null) {
                        globalFunctionDecls.head.importedFrom = new TapPair<>(fortranFunctionDecl, functionDeclHostSymbolTable) ; // test dans mix02
                    }
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.printlnOnTrace("    Mixed-language call:" + this.unit + " calls " + fortranFunctionDecl.unit());
                    }
                    fortranFunctionDecl.unit().setCalledFromOtherLanguage();
                }
                result = fortranFunctionDecl != null &&
                        (this.unit == null || fortranFunctionDecl.unit().language() != this.language);
                if (result) {
                    calledFunctionDecl = fortranFunctionDecl;
                }
            } else if (this.unit.isFortran()) {
                // Fortran calls C:
                // Fortran 77 gcc style:
                TapList<FunctionDecl> fortranFunctionDecls = getFunctionDecl(functionName, null, null, false);
                FunctionDecl fortranFunctionDecl = fortranFunctionDecls == null ? null : fortranFunctionDecls.head;
                TapList<FunctionDecl> cFunctionDecls;
                FunctionDecl cFunctionDecl = null;
                TapPair<String, FunctionDecl> cFuncName =
                    getCallGraph().getMixedLanguageFunctionName(functionName, this.language, TapEnv.C);
                String cName = cFuncName.first;
                if (cName != null) {
                    cFunctionDecls = getCallGraph().cRootSymbolTable().getFunctionDecl(cName, null, null, false);
                    cFunctionDecl = cFunctionDecls == null ? null : cFunctionDecls.head;
                }
                if (cFunctionDecl != null
                        && localFunctionDecl != null
                        && (localFunctionDecl.isExternal() || localFunctionDecl.isInterface())
                        && (this.unit != null || cFunctionDecl.unit().language() != this.language)) {
                    while (localFunctionDecl.importedFrom!=null) {
                        localFunctionDecl = (FunctionDecl)localFunctionDecl.importedFrom.first ;
                    }
                    if (localFunctionDecl!=cFunctionDecl) { //this might happen, cf set10/mix41 & mix50...
                        localFunctionDecl.importedFrom = new TapPair<>(cFunctionDecl, getCallGraph().cRootSymbolTable());
                        localFunctionDecl.setUnit(cFunctionDecl.unit());
                    }
                    if (fortranFunctionDecl != null
                            && fortranFunctionDecl.isInterface()
                            && fortranFunctionDecl.unit() != cFunctionDecl.unit()) {
                        // cf set10/mix68: il faut differentier la unit C avant l'interface fortran
                        CallGraph.addCallArrow(fortranFunctionDecl.unit(), SymbolTableConstants.CALLS,
                                cFunctionDecl.unit());
                    }
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.printlnOnTrace("    Mixed-language call: " + this.unit + " calls " + " " + cFunctionDecl.unit());
                    }
                }
                if (cFunctionDecl != null && cFunctionDecl.unit() != null) {
                    cFunctionDecl.unit().setCalledFromOtherLanguage();
                }

                result = cFunctionDecl != null &&
                        (this.unit != null || cFunctionDecl.unit().language() != this.language);
                if (result) {
                    calledFunctionDecl = cFunctionDecl;
                }
            }
        }
        return calledFunctionDecl;
    }

    private WrapperTypeSpec changeFunctionToArrayTypeSpec(Tree instrOrExpr, WrapperTypeSpec topType) {
        String name = ILUtils.baseName(instrOrExpr);
        TapList<FunctionDecl> funcDecls = getTopFunctionDecl(name, null, null, false);
        FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
        if (funcDecl == null) {
            funcDecls = getFunctionDeclInUnit(name, null, null, false, unit);
            funcDecl = funcDecls == null ? null : funcDecls.head;
        }
        topType = ((FunctionTypeSpec) topType.wrappedType).returnType;
        if (funcDecl != null) {
            if (funcDecl.hasInstructionWithRootOperator(ILLang.op_external, null)) {
                this.declarationsBlock.removeExternalInstr(funcDecl);
            }
            this.removeDecl(name, SymbolTableConstants.FUNCTION, false);
            this.addSymbolDecl(new VariableDecl(instrOrExpr, topType));
        }
        return topType;
    }

    /**
     * complex + real/integer &rarr; complex
     * real + integer &rarr; real
     * ...
     */
    private WrapperTypeSpec canCombineNumeric(WrapperTypeSpec lhsType, WrapperTypeSpec rhsType, Tree instrOrExpr) {
        if (unit != null && unit.isC()
                &&
                (lhsType != null && rhsType != null)
                &&
                ((lhsType.isPointer() && rhsType.isIntegerBase())
                        || (rhsType.isPointer() && lhsType.isIntegerBase()))
                &&
                (instrOrExpr.opCode() == ILLang.op_add || instrOrExpr.opCode() == ILLang.op_sub)
        ) {
            // Case of C pointer arithmetic:
            return (lhsType.isPointer() ? lhsType : rhsType);
        }
        if (lhsType != null && !lhsType.checkNumeric()) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(1), "(TC11) Argument of arithmetic operator is expected to be numeric, and is here " + lhsType.showType());
        }
        if (rhsType != null && !rhsType.checkNumeric()) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, instrOrExpr.down(2), "(TC11) Argument of arithmetic operator is expected to be numeric, and is here " + rhsType.showType());
        }
        if (lhsType == null || !lhsType.checkNumeric()) {
            if (rhsType == null || !rhsType.checkNumeric()) {
                return null;
            } else {
                return rhsType;
            }
        } else if (rhsType == null || !rhsType.checkNumeric()) {
            return lhsType;
        } else if (lhsType.baseTypeName().equals("complex") ||
                rhsType.baseTypeName().equals("complex")) {
            if (lhsType.baseTypeName().equals("complex") &&
                    rhsType.baseTypeName().equals("complex")
                    || lhsType.baseTypeName().equals("float")
                    || rhsType.baseTypeName().equals("float")) {
                if (lhsType.baseTypeName().equals("float")) {
                    lhsType = lhsType.realToComplex(new TapList<>(null, null),
                            rhsType.baseTypeSpec(false));

                } else if (rhsType.baseTypeName().equals("float")) {
                    rhsType = rhsType.realToComplex(new TapList<>(null, null),
                            lhsType.baseTypeSpec(false));
                }
                return setConformingDimension(
                        setUnionModifier(getTypeDecl("complex").typeSpec, lhsType, rhsType, false),
                        lhsType, rhsType, instrOrExpr);
            } else {
                WrapperTypeSpec modifierTypeSpec;
                if (lhsType.baseTypeName().equals("integer")) {
                    modifierTypeSpec = rhsType.modifiedBaseTypeSpec();
                } else if (rhsType.baseTypeName().equals("integer")) {
                    modifierTypeSpec = lhsType.modifiedBaseTypeSpec();
                } else {
                    modifierTypeSpec = getTypeDecl("complex").typeSpec;
                }
                return setConformingDimension(modifierTypeSpec, lhsType, rhsType, instrOrExpr);
            }
        } else if (lhsType.baseTypeName().equals("float") ||
                rhsType.baseTypeName().equals("float")) {
            if (lhsType.baseTypeName().equals("float") &&
                    rhsType.baseTypeName().equals("float")) {
                return setConformingDimension(
                        setUnionModifier(getTypeDecl("float").typeSpec, lhsType, rhsType, false),
                        lhsType, rhsType, instrOrExpr);
            } else {
                WrapperTypeSpec modifierTypeSpec;
                if (lhsType.baseTypeName().equals("integer")) {
                    modifierTypeSpec = rhsType.modifiedBaseTypeSpec();
                } else if (rhsType.baseTypeName().equals("integer")) {
                    modifierTypeSpec = lhsType.modifiedBaseTypeSpec();
                } else {
                    modifierTypeSpec = getTypeDecl("float").typeSpec;
                }
                return setConformingDimension(modifierTypeSpec, lhsType, rhsType, instrOrExpr);
            }
        } else if (TypeSpec.isA(lhsType, SymbolTableConstants.POINTERTYPE)) {
            return lhsType;
        } else if (TypeSpec.isA(rhsType, SymbolTableConstants.POINTERTYPE)) {
            return rhsType;
        } else if (TypeSpec.isA(lhsType, SymbolTableConstants.ENUMTYPE)) {
            return rhsType;
        } else if (TypeSpec.isA(rhsType, SymbolTableConstants.ENUMTYPE)) {
            return lhsType;
        } else if (lhsType.baseTypeName().equals("integer") ||
                lhsType.baseTypeName().equals("boolean") ||
                lhsType.isIntegerBase() ||
                rhsType.baseTypeName().equals("integer") ||
                rhsType.baseTypeName().equals("boolean") ||
                rhsType.isIntegerBase()) {
            return setConformingDimension(
                    setUnionModifier(getTypeDecl("integer").typeSpec, lhsType, rhsType, false),
                    lhsType, rhsType, instrOrExpr);
        } else {
            return lhsType;
        }
    }

    /**
     * Very similar to canCombineNumeric(), but doesn't modify lhsType nor rhsType.
     */
    public TypeSpec combineNumeric(TypeSpec lhsType, TypeSpec rhsType, Tree instrOrExpr) {
        if (unit != null && unit.isC()
            && ((lhsType.isPointer() && rhsType.isIntegerBase())
                || (rhsType.isPointer() && lhsType.isIntegerBase()))
            && (instrOrExpr.opCode() == ILLang.op_add
                || instrOrExpr.opCode() == ILLang.op_sub)) {
            // Case of C pointer arithmetic:
            TypeSpec resultType = (lhsType.isPointer() ? lhsType : rhsType);
            // C pointer arithmetic must loose explicit sizes:
            WrapperTypeSpec elementTypeSpec = null ;
            if (TypeSpec.isA(resultType, SymbolTableConstants.ARRAYTYPE)) {
                elementTypeSpec = resultType.elementType() ;
            } else if (TypeSpec.isA(resultType, SymbolTableConstants.POINTERTYPE)) {
                elementTypeSpec = ((PointerTypeSpec)((WrapperTypeSpec)resultType).wrappedType).destinationType;
            }
            if (elementTypeSpec!=null) {
                resultType = new WrapperTypeSpec(new PointerTypeSpec(elementTypeSpec, null)) ;
            }
            return resultType ;
        } else {
            //[llh] TODO: for consistency, canCombineNumeric() should follow the structure
            // of the following addWith() (see mix of known and unknown type accuracies):
            return ((WrapperTypeSpec) lhsType).addWith(((WrapperTypeSpec) rhsType));
        }
    }

    private boolean comesFromRootSymbolTable(SymbolDecl symbolDecl) {
        boolean result;
        SymbolTable upperST = this;
        SymbolDecl upperSymbolDecl = null;
        upperST = upperST.basisSymbolTable;
        while (upperST != getCallGraph().globalRootSymbolTable()
                && upperST.basisSymbolTable != null && upperSymbolDecl == null) {
            upperSymbolDecl = upperST.getDecl(symbolDecl.symbol, symbolDecl.kind, true);
            if (upperSymbolDecl == null) {
                upperST = upperST.basisSymbolTable;
            }
        }
        //[llh 8mar2018] removed the test below because duplicated symbolDecl's need not be == any more. Hope it's ok!
        /* && (upperSymbolDecl==symbolDecl || upperSymbolDecl == null)*/
        result = upperST.basisSymbolTable == null || upperST.basisSymbolTable == getCallGraph().globalRootSymbolTable();
        return result;
    }

    private WrapperTypeSpec arrayTypeIfElementalCall(Tree upperCallTree, int rank, WrapperTypeSpec typeSpec) {
        WrapperTypeSpec result = typeSpec;
        Tree callTree = ILUtils.getArguments(upperCallTree).down(rank + 1);
        if (callTree.opCode() == ILLang.op_call) {
            Object o = callTree.getAnnotation("arrayReturnTypeSpec");
            if (o != null) {
                result = (WrapperTypeSpec) o;
            }
        }
        return result;
    }

    private WrapperTypeSpec realCstTypeSpec(Tree instrOrExpr) {
        WrapperTypeSpec result;
        WrapperTypeSpec elemType = getTypeDecl("float").typeSpec;
        String value = instrOrExpr.stringValue();
        int index = value.indexOf('_');
        Tree modifier;
        if (index > 0) {
            String valueString = value.substring(index + 1);
            try {
                Integer.parseInt(value.substring(0, index));
                elemType = getTypeDecl("integer").typeSpec;
            } catch (NumberFormatException ignored) {
            }
            try {
                int valueInt = Integer.parseInt(valueString);
                modifier = ILUtils.build(ILLang.op_intCst, valueInt);
            } catch (NumberFormatException e) {
                valueString = valueString.toLowerCase();
                modifier = ILUtils.build(ILLang.op_ident, valueString);
            }
            result = new WrapperTypeSpec(new ModifiedTypeSpec(elemType, modifier, this));
        } else if (value.indexOf('d') > 0 || value.indexOf('D') > 0) {
            modifier = ILUtils.build(ILLang.op_ident, "double");
            result = new WrapperTypeSpec(new ModifiedTypeSpec(elemType, modifier, this));
        } else if (value.indexOf('i') > 0 || value.indexOf('j') > 0) {
            result = getTypeDecl("complex").typeSpec;
        } else {
            try {
                Integer.parseInt(value);
                elemType = getTypeDecl("integer").typeSpec;
            } catch (NumberFormatException ignored) {
            }
            elemType.wrappedType.setHasUndefinedSize(true);
            result = elemType;
        }
        return result;
    }

    private boolean isCallNotElemental(Tree tree) {
        boolean result = tree.opCode() == ILLang.op_call;
        if (result) {
            String calledUnitName = ILUtils.baseName(ILUtils.getCalledName(tree));
            TapList<FunctionDecl> funcDecls = getFunctionDecl(calledUnitName, null, null, false);
            FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head; //we assume to have only 1 prototype that matches with the func name.
            if (funcDecl != null) {
                result = !funcDecl.isElemental() || funcDecl.isIntrinsicNotElemental();
            }
        }
        return result;
    }

    /**
     * Look for some FunctionDecl of name "varName" in the symbolTables of "unit"
     * i.e. in unit's private, public, and IMPORT'ed SymbolTable's
     */
    private TapList<FunctionDecl> getFunctionDeclInUnit(String varName, WrapperTypeSpec returnType,
                                                        WrapperTypeSpec[] argumentsTypes, boolean checkArgsTypes, Unit unit) {
        TapList<FunctionDecl> funcDecls = null;
        SymbolTable inSymTab;
        if (unit != null) {
            inSymTab = unit.privateSymbolTable();
            if (inSymTab != null) {
                funcDecls = inSymTab.getTopFunctionDecl(varName, returnType, argumentsTypes, checkArgsTypes);
            }
            if (funcDecls == null) {
                inSymTab = unit.protectedSymbolTable();
                if (inSymTab != null) {
                    funcDecls = inSymTab.getTopFunctionDecl(varName, returnType, argumentsTypes, checkArgsTypes);
                }
            }
            if (funcDecls == null) {
                inSymTab = unit.publicSymbolTable();
                if (inSymTab != null) {
                    funcDecls = inSymTab.getTopFunctionDecl(varName, returnType, argumentsTypes, checkArgsTypes);
                    inSymTab = inSymTab.basisSymbolTable;
                    while (funcDecls == null && inSymTab != null && inSymTab.isImports) {
                        funcDecls = inSymTab.getTopFunctionDecl(varName, returnType, argumentsTypes, checkArgsTypes);
                        inSymTab = inSymTab.basisSymbolTable;
                    }
                }
            }
        }
        return funcDecls;
    }

    /**
     * For Fortran, when one child of an expression is expected to return a
     * variable and it actually returns a pointer, we must deref this child.
     *
     * @param expressionType type of the expression's child.
     * @param expression     the expression
     * @param rank           the rank of the child inside the expression
     * @return the type of the (possibly deref'ed) subexpression.
     */
    private WrapperTypeSpec checkPointerInFortranExpression(WrapperTypeSpec expressionType,
                                                            Tree expression, int rank) {
        if (unit != null && unit.isFortran()
                && TypeSpec.isA(expressionType, SymbolTableConstants.POINTERTYPE)) {
            expression.setChild(
                    ILUtils.build(ILLang.op_pointerAccess,
                            expression.cutChild(rank),
                            ILUtils.build(ILLang.op_none)),
                    rank);
            expressionType = ((PointerTypeSpec) expressionType.wrappedType).destinationType;
        }
        return expressionType;
    }

    // Not sure this code is correct: when the dimension bounds are known as Integers, does nothing,
    // and when they are just Tree's, reoffsets them from a:b to 1:b-a+1.
    // Moreover, doesn't seem to take the triplet stride into account ?
    // modif: when resetOffset==true, then reoffsets even when the bounds are Integers
    private void refineTripletDimension(Tree tree, ArrayDim dim, boolean resetOffset) {
        dim.lower = ILUtils.isNullOrNone(tree.down(1)) ? Integer.valueOf(1) : this.computeIntConstant(tree.down(1));
        dim.upper = this.computeIntConstant(tree.down(2));
        if (dim.lower == null || dim.upper == null) {
            if (tree.down(2).opCode() != ILLang.op_none) {
                Tree sizeTree = ILUtils.copy(tree.down(2));
                if (!ILUtils.isNullOrNone(tree.down(1))) {
                    sizeTree = ILUtils.addTree(ILUtils.subTree(sizeTree, ILUtils.copy(tree.down(1))),
                            ILUtils.build(ILLang.op_intCst, 1));
                }
                Integer intLength = this.computeIntConstant(sizeTree);
                if (intLength != null) {
                    dim.lower = 1;
                    dim.upper = intLength;
                }
            }
        } else if (resetOffset) {
            dim.upper = dim.upper - dim.lower + 1;
            dim.lower = 1;
        }
    }

    /**
     * Si type1 (resp type2) est un ModifiedTypeSpec resultat d'un typeCheck/Of(op_realCst)
     * sans la taille du real
     * le type resultat a le modifier de type2 (resp type1)
     * s'applique si les trois types ont le meme baseTypeSpec
     * real*8 + real &rarr; real...
     * When precisionUp is true, the result has the modifier with the best accuracy.
     */
    private WrapperTypeSpec setUnionModifier(WrapperTypeSpec resultType, WrapperTypeSpec type1, WrapperTypeSpec type2,
                                             boolean precisionUp) {
        boolean resultHasUndefinedSize = type1.hasUndefinedSize() && type2.hasUndefinedSize();
        boolean equalTypes = false;
        while (TypeSpec.isA(type1, SymbolTableConstants.ARRAYTYPE) && TypeSpec.isA(type2, SymbolTableConstants.ARRAYTYPE)) {
            type1 = type1.wrappedType.elementType();
        }
        while (TypeSpec.isA(type2, SymbolTableConstants.ARRAYTYPE)) {
            type2 = type2.wrappedType.elementType();
        }
        if (TypeSpec.isA(type1, SymbolTableConstants.MODIFIEDTYPE) && type1.hasUndefinedSize()
                && TypeSpec.isA(type2, SymbolTableConstants.MODIFIEDTYPE)) {
            ((ModifiedTypeSpec) type1.wrappedType).getModifierFrom((ModifiedTypeSpec) type2.wrappedType);
            type1.setHasUndefinedSize(type2.hasUndefinedSize());
            equalTypes = true;
        }
        if (TypeSpec.isA(type2, SymbolTableConstants.MODIFIEDTYPE) && type2.hasUndefinedSize()
                && TypeSpec.isA(type1, SymbolTableConstants.MODIFIEDTYPE)) {
            ((ModifiedTypeSpec) type2.wrappedType).getModifierFrom((ModifiedTypeSpec) type1.wrappedType);
            type2.setHasUndefinedSize(type1.hasUndefinedSize());
            equalTypes = true;
        }
        int mod1 = -1, mod2 = -1;
        boolean hasKindEqual = false;
        ToObject<ModifiedTypeSpec> toMod = new ToObject<>(null);
        TypeSpec.peelSizeModifier(type1, toMod);
        if (toMod.obj() != null) {
            mod1 = toMod.obj().sizeModifierValue();
            if (toMod.obj().hasKindEqual()) {
                hasKindEqual = true;
            }
        }
        toMod.setObj(null);
        TypeSpec.peelSizeModifier(type2, toMod);
        if (toMod.obj() != null) {
            mod2 = toMod.obj().sizeModifierValue();
            if (toMod.obj().hasKindEqual()) {
                hasKindEqual = true;
            }
        }
        int resultMod;
        if (mod1 == -2 && mod2 == -2) {
            resultMod = -2;
        } else if (type1.hasUndefinedSize()) {
            resultMod = mod2;
        } else if (type2.hasUndefinedSize()) {
            resultMod = mod1;
        } else if (mod1 == -2) {
            resultMod = mod2;
        } else if (mod2 == -2) {
            resultMod = mod1;
        } else if (precisionUp ? mod1 > mod2 : mod1 < mod2) {
            resultMod = mod1;
        } else {
            resultMod = mod2;
        }
        if (resultMod != -1) {
            Tree modifier;
            if (resultMod == -2) {
                modifier = ILUtils.build(ILLang.op_ident, "double");
            } else {
                modifier = ILUtils.build(ILLang.op_intCst, resultMod);
                if (hasKindEqual) {
                    modifier = ILUtils.build(ILLang.op_nameEq,
                            ILUtils.build(ILLang.op_ident, "kind"),
                            modifier);
                }
            }
            resultType =
                    new WrapperTypeSpec(new ModifiedTypeSpec(resultType, modifier, this));
        } else if (TypeSpec.isA(type1, SymbolTableConstants.MODIFIEDTYPE)
                && TypeSpec.isA(type2, SymbolTableConstants.MODIFIEDTYPE) && equalTypes) {
            resultType = new WrapperTypeSpec(
                    new ModifiedTypeSpec(resultType, (ModifiedTypeSpec) type1.wrappedType));
        } else if (TypeSpec.isA(type1, SymbolTableConstants.MODIFIEDTYPE)
                && TypeSpec.isA(type2, SymbolTableConstants.MODIFIEDTYPE)
                && type1.equalsLiterally(type2)) {
            resultType = type1;
        }
        resultType.setHasUndefinedSize(resultHasUndefinedSize);
        return resultType;
    }

    protected WrapperTypeSpec setConformingDimension(WrapperTypeSpec resultType,
                                                     WrapperTypeSpec type1, WrapperTypeSpec type2, Tree expression) {
        WrapperTypeSpec conformingTypeSpec = resultType.conformingTypeSpec(type1, type2);
        if (conformingTypeSpec != null) {
            return conformingTypeSpec;
        } else {
            TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(TC25) Dimensions mismatch in array expression, " + type1.showType() + " combined with " + type2.showType());
            return resultType;
        }
    }

    /**
     * @return the (deep) SymbolDecl of reference expression "instrOrExpr".
     */
    protected SymbolDecl symbolDeclOf(Tree instrOrExpr) {
        if (instrOrExpr == null) {
            return null;
        }
        switch (instrOrExpr.opCode()) {
            case ILLang.op_ident: {
                String symbol = ILUtils.getIdentString(instrOrExpr);
                SymbolDecl symbolDecl = getVariableDecl(symbol);
                TapList<FunctionDecl> functionDecls;
                if (symbolDecl == null) {
                    symbolDecl = getConstantDecl(symbol);
                }
                if (symbolDecl == null) {
                    functionDecls = getFunctionDecl(symbol, null, null, false);
                    symbolDecl = functionDecls == null ? null : functionDecls.head;
                }
                return symbolDecl;
            }
            case ILLang.op_arrayAccess:
                return symbolDeclOf(instrOrExpr.down(1));
            case ILLang.op_fieldAccess: {
                WrapperTypeSpec compositeType = typeOf(instrOrExpr.down(1));
                SymbolDecl symbolDecl = null;
                if (compositeType != null) {
                    TapPair<WrapperTypeSpec, ArrayDim[]> afterPeel =
                            TypeSpec.peelArrayDimsAroundComposite(compositeType);
                    compositeType = afterPeel.first;
                    if (TypeSpec.isA(compositeType, SymbolTableConstants.COMPOSITETYPE)) {
                        symbolDecl = ((CompositeTypeSpec) compositeType.wrappedType)
                                .namedFieldDecl(ILUtils.getIdentString(instrOrExpr.down(2)));
                    }
                }
                return symbolDecl;
            }
            default:
                return null;
        }
    }

    /**
     * @return the type of expression "instrOrExpr"
     * or unKnownType if no type is found.
     */
    public WrapperTypeSpec typeOf(Tree instrOrExpr) {
        WrapperTypeSpec unKnownType = new WrapperTypeSpec(new NamedTypeSpec("UnKnownType"));
        WrapperTypeSpec result;
        if (instrOrExpr == null) {
            result = unKnownType;
        } else {
            switch (instrOrExpr.opCode()) {
                case ILLang.op_ident: {
                    WrapperTypeSpec resultType = null;
                    String identStr = ILUtils.getIdentString(instrOrExpr);
                    VariableDecl variableDecl = getVariableDecl(identStr);
                    if (variableDecl == null) {
                        variableDecl = getConstantDecl(identStr);
                    }
                    if (variableDecl == null) {
                        NewSymbolHolder newSymbHolder = NewSymbolHolder.getNewSymbolHolder(instrOrExpr);
                        if (newSymbHolder != null) {
                            variableDecl = newSymbHolder.newVariableDecl();
                            if (variableDecl == null) {
                                TapList<FunctionDecl> funcDecls = getFunctionDecl(identStr, null, null, false);
                                // we assume to have only 1 prototype: the searched one !
                                FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
                                if (funcDecl != null && funcDecl.unit() != null) {
                                    resultType = new WrapperTypeSpec(funcDecl.functionTypeSpec());
                                }
                            } else {
                                resultType = variableDecl.type();
                            }
                        } else {
                            TapList<FunctionDecl> funcDecls = getFunctionDecl(identStr, null, null, false);
                            // we assume to have only 1 prototype: the searched one !
                            FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
                            if (funcDecl != null && funcDecl.unit() != null) {
                                resultType = new WrapperTypeSpec(funcDecl.functionTypeSpec());
                            } else {
                                VariableDecl otherVar = this.unit.otherReturnVar();
                                if (otherVar != null && identStr.equals(otherVar.symbol)) {
                                    resultType = otherVar.type();
                                } else {
                                    TypeDecl typeDecl = getTypeDecl(identStr);
                                    if (typeDecl != null) {
                                        resultType = typeDecl.typeSpec;
                                    }
                                }
                            }
                        }
                    } else {
                        resultType = variableDecl.type();
                    }
                    result = resultType;
                    break;
                }
                case ILLang.op_dimColon:
                    instrOrExpr = ILUtils.build(ILLang.op_arrayTriplet,
                            ILUtils.copy(instrOrExpr.down(1)),
                            ILUtils.copy(instrOrExpr.down(2)),
                            ILUtils.build(ILLang.op_none));
                    //WARNING: this case continues into next case:
                case ILLang.op_arrayTriplet: {
                    Integer lowerI;
                    Integer upperI;
                    Integer strideI;
                    Integer lengthI;
                    Tree lengthTree = null;
                    if (ILUtils.isNullOrNone(instrOrExpr.down(1))) {
                        lowerI = null;
                    } else {
                        lowerI = computeIntConstant(instrOrExpr.down(1));
                    }
                    if (ILUtils.isNullOrNone(instrOrExpr.down(2))) {
                        upperI = null;
                    } else {
                        upperI = computeIntConstant(instrOrExpr.down(2));
                    }
                    if (ILUtils.isNullOrNone(instrOrExpr.down(3))) {
                        strideI = 1;
                    } else {
                        strideI = computeIntConstant(instrOrExpr.down(3));
                    }
                    if (lowerI != null && upperI != null && strideI != null) {
                        int lower = lowerI;
                        int upper = upperI;
                        int stride = strideI;
                        if (lower > upper) {
                            lower = upper;
                            upper = lowerI;
                        }
                        if (stride < 0) {
                            stride = -stride;
                        }
                        int length = (upper - lower) / stride + 1;
                        // on essaye de garder les constantes et pas leur valeur
                        if (length == upperI) {
                            lengthTree = ILUtils.copy(instrOrExpr.down(2));
                        } else if (length == lowerI) {
                            lengthTree = ILUtils.copy(instrOrExpr.down(1));
                        } else {
                            lengthTree = ILUtils.build(ILLang.op_intCst, length);
                        }
                        lengthI = length;
                    } else {
                        lengthTree =
                            ILUtils.buildSizeTree(instrOrExpr.down(1),
                                                  instrOrExpr.down(2),
                                                  instrOrExpr.down(3)) ;
                        lengthI = null;
                    }
                    // dimension in a type can only hold a op_dimColon:
                    ArrayDim dimension =
                        new ArrayDim(ILUtils.build(ILLang.op_dimColon,
                                       ILUtils.build(ILLang.op_intCst, 1),
                                       lengthTree),
                                     1, lengthI, null, -1, 0, 0);
                    // If lengthTree cannot be evaluated at declaration time, don't use it as declaration length:
                    dimension.detectUnDeclarableLength(this) ;
                    result = new WrapperTypeSpec(
                                 new ArrayTypeSpec(
                                     getTypeDecl("integer").typeSpec, new ArrayDim[]{dimension})) ;
                    break;
                }
                case ILLang.op_arrayAccess: {
                    WrapperTypeSpec resultType;
                    WrapperTypeSpec arrayType = typeOf(instrOrExpr.down(1));
                    if (TypeSpec.isA(arrayType, SymbolTableConstants.ARRAYTYPE)) {
                        ArrayDim arrayDim;
                        ArrayDim newArrayDim;
                        Tree index;
                        ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) arrayType.wrappedType;
                        TapList<ArrayDim> remainingDimensions = null;
                        Tree[] indexes = instrOrExpr.down(2).children();
                        int indexlength = arrayTypeSpec.dimensions().length;
                        if (indexlength > indexes.length) {
                            indexlength = indexes.length;
                        }
                        for (int i = indexlength - 1; i >= 0; i--) {
                            arrayDim = arrayTypeSpec.dimensions()[i];
                            index = indexes[i];
                            if (index.opCode() == ILLang.op_dimColon ||
                                    index.opCode() == ILLang.op_arrayTriplet
                                            && ILUtils.isNullOrNone(index.down(1))
                                            && ILUtils.isNullOrNone(index.down(2))
                                            && ILUtils.isNullOrNone(index.down(3))) {
                                remainingDimensions = new TapList<>(arrayDim, remainingDimensions);
                            } else {
                                WrapperTypeSpec indexType;
                                if (index.opCode() == ILLang.op_arrayTriplet) {
                                    Tree cpIndex = ILUtils.copy(index);
                                    if (ILUtils.isNullOrNone(cpIndex.down(1))) {
                                        cpIndex.setChild(ILUtils.copy(arrayDim.lowerTree()), 1);
                                    }
                                    if (ILUtils.isNullOrNone(cpIndex.down(2))) {
                                        cpIndex.setChild(ILUtils.copy(arrayDim.upperTree()), 2);
                                    }
                                    indexType = typeOf(cpIndex);
                                } else {
                                    indexType = typeOf(index);
                                }
                                if (TypeSpec.isA(indexType, SymbolTableConstants.ARRAYTYPE)) {
                                    newArrayDim = ((ArrayTypeSpec) indexType.wrappedType).dimensions()[0];
                                    if (newArrayDim.unDeclarableLength) {
                                        if (newArrayDim.isNullOrNoneOrStar())
                                            newArrayDim.overEstimate =
                                                "SIZE "+(i+1)+" OF "+ILUtils.toString(instrOrExpr.down(1)) ;
                                        else if (arrayDim.size()==-1) {
                                            newArrayDim.overEstimate = ILUtils.toString(arrayDim.getSize()) ;
                                        } else {
                                            newArrayDim.overEstimate = ""+arrayDim.size() ;
                                        }
                                    }
                                    remainingDimensions =
                                        new TapList<>(newArrayDim, remainingDimensions);
                                }
                            }
                        }
                        if (remainingDimensions != null) {
                            ArrayDim[] newDimensions = new ArrayDim[TapList.length(remainingDimensions)];
                            for (int i = 0; i < newDimensions.length; i++) {
                                newDimensions[i] = remainingDimensions.head;
                                remainingDimensions = remainingDimensions.tail;
                            }
                            result = new WrapperTypeSpec(new ArrayTypeSpec(
                                    arrayTypeSpec.elementType(), newDimensions));
                        } else {
                            result = arrayTypeSpec.elementType();
                        }
                    } else {
                        result = arrayType;
                    }
                    break;
                }
                case ILLang.op_fieldAccess: {
                    WrapperTypeSpec compositeType = typeOf(instrOrExpr.down(1));
                    WrapperTypeSpec fieldType = null;
                    if (compositeType != null) {
                        TapPair<WrapperTypeSpec, ArrayDim[]> afterPeel =
                                TypeSpec.peelArrayDimsAroundComposite(compositeType);
                        compositeType = afterPeel.first;
                        if (TypeSpec.isA(compositeType, SymbolTableConstants.POLYMORPHICTYPE)) {
                            // Incomplete patch: consider only the base type of this polymorphic type!
                            compositeType = (WrapperTypeSpec)((PolymorphicTypeSpec)compositeType.wrappedType).rootType ;
                        }
                        if (TypeSpec.isA(compositeType, SymbolTableConstants.COMPOSITETYPE)) {
                            fieldType = ((CompositeTypeSpec) compositeType.wrappedType)
                                    .namedFieldType(ILUtils.getIdentString(instrOrExpr.down(2)));
                        }
                        if (afterPeel.second != null) {
                            //Case of a vectorial field access on an array of records:
                            fieldType = new WrapperTypeSpec(new ArrayTypeSpec(fieldType, afterPeel.second));
                        }
                    }
                    result = fieldType;
                    break;
                }
                case ILLang.op_address:
                    result = new WrapperTypeSpec(new PointerTypeSpec(typeOf(instrOrExpr.down(1)), null));
                    break;
                case ILLang.op_arrayDeclarator: {
                    WrapperTypeSpec typeInside = typeOf(instrOrExpr.down(1));
                    if ((unit == null || unit.isC()) && typeInside != null
                            && TypeSpec.isA(typeInside.wrappedType, SymbolTableConstants.POINTERTYPE)) {
                        typeInside = ((PointerTypeSpec) typeInside.wrappedType).destinationType;
                        Tree[] dimDecls = instrOrExpr.down(2).children();
                        ArrayDim[] dims = new ArrayDim[dimDecls.length];
                        for (int i = dimDecls.length - 1; i >= 0; --i) {
                            dims[i] = new ArrayDim(dimDecls[i], null, null, null, -1, 0, 0);
                        }
                        typeInside = new WrapperTypeSpec(new ArrayTypeSpec(typeInside, dims));
                    }
                    result = typeInside;
                    break;
                }
                case ILLang.op_assign:
                case ILLang.op_minus:
                case ILLang.op_leftShift:
                case ILLang.op_rightShift:
                case ILLang.op_concat:
                    result = typeOf(instrOrExpr.down(1));
                    break;
                case ILLang.op_nameEq:
                case ILLang.op_unary:
                    result = typeOf(instrOrExpr.down(2));
                    break;
                case ILLang.op_pointerAccess: {
                    WrapperTypeSpec ptrType = typeOf(instrOrExpr.down(1));
                    ArrayDim[] dims = null;
                    if (TypeSpec.isA(ptrType, SymbolTableConstants.ARRAYTYPE) &&
                            TypeSpec.isA(ptrType.wrappedType.elementType(), SymbolTableConstants.POINTERTYPE)) {
                        // The pointerAccess on an array of pointers is possible;
                        // It returns the array of destinations:
                        dims = ((ArrayTypeSpec) ptrType.wrappedType).dimensions();
                        ptrType = ptrType.wrappedType.elementType();
                    }
                    if (TypeSpec.isA(ptrType, SymbolTableConstants.POINTERTYPE)) {
                        result = ((PointerTypeSpec) ptrType.wrappedType).destinationType;
                    } else {
                        // Cas d'un arbre mal construit ??
                        // on a deja affiche' un message TC20 ou TC40
                        //TapEnv.toolWarning(-1, "(Typing expression) Wrong tree structure: "+instrOrExpr) ;
                        result = ptrType;
                    }
                    WrapperTypeSpec indexType = typeOf(instrOrExpr.down(2));
                    if (TypeSpec.isA(indexType, SymbolTableConstants.ARRAYTYPE)) {
                        result = new WrapperTypeSpec(
                                new ArrayTypeSpec(result,
                                        ((ArrayTypeSpec) indexType.wrappedType).dimensions()));
                    } else if (dims != null) {
                        result = new WrapperTypeSpec(new ArrayTypeSpec(result, dims));
                    }
                    break;
                }
                case ILLang.op_add:
                case ILLang.op_sub:
                case ILLang.op_mul:
                case ILLang.op_div:
                case ILLang.op_power: {
                    WrapperTypeSpec resultType;
                    WrapperTypeSpec type1 = typeOf(instrOrExpr.down(1));
                    WrapperTypeSpec type2 = typeOf(instrOrExpr.down(2));
                    if (type1 == null || !type1.checkNumeric()) {
                        resultType = type2;
                    } else if (type2 == null || !type2.checkNumeric()) {
                        resultType = type1;
                    } else {
                        resultType = (WrapperTypeSpec) combineNumeric(type1, type2, instrOrExpr);
                    }
                    result = resultType;
                    break;
                }
                case ILLang.op_intCst:
                case ILLang.op_sizeof:
                case ILLang.op_bitCst:
                    result = getTypeDecl("integer").typeSpec;
                    break;
                case ILLang.op_realCst:
                    result = realCstTypeSpec(instrOrExpr);
                    break;
                case ILLang.op_stringCst: {
                    int length = instrOrExpr.stringValue().length();
                    WrapperTypeSpec charType = getTypeDecl("character").typeSpec;
                    if (length == 1) {
                        // [vmp] on fait le choix length==1 -> character
                        //  et pas tableau de taille 1 de character
                        result = charType;
                    } else if (unit == null || unit.isC()) {
                        result = new WrapperTypeSpec(new PointerTypeSpec(charType,
                                new ArrayDim(null, 0, null, null, -1, 0, 0)));
                    } else {
                        result = new WrapperTypeSpec(new ArrayTypeSpec(charType,
                                new ArrayDim[]{new ArrayDim(null, 1, length, null, -1, 0, 0)}));
                    }
                    break;
                }
                case ILLang.op_strings: {
                    Tree[] listOfString = instrOrExpr.children();
                    int length = 0;
                    for (Tree tree : listOfString) {
                        length = tree.stringValue().length();
                    }
                    if (length == 1) {
                        result = getTypeDecl("character").typeSpec;
                    } else {
                        // [vmp] on fait le choix length==1 -> character
                        //  et pas tableau de taille 1 de character
                        ArrayDim[] dims = new ArrayDim[1];
                        if (unit == null || unit.isC()) {
                            dims[0] = new ArrayDim(null, 1, null, null, -1, 0, 0);
                        } else {
                            dims[0] = new ArrayDim(null, 1, length, null, -1, 0, 0);
                        }
                        result = new WrapperTypeSpec(new ArrayTypeSpec(getTypeDecl("character").typeSpec,
                                dims));
                    }
                    break;
                }
                case ILLang.op_formatElem:
                case ILLang.op_letter:
                    result = getTypeDecl("character").typeSpec;
                    break;
                case ILLang.op_boolCst:
                    result = getTypeDecl("boolean").typeSpec;
                    break;
                case ILLang.op_eq:
                case ILLang.op_neq:
                case ILLang.op_ge:
                case ILLang.op_gt:
                case ILLang.op_le:
                case ILLang.op_lt:
                case ILLang.op_and:
                case ILLang.op_or:
                case ILLang.op_xor:
                case ILLang.op_bitAnd:
                case ILLang.op_bitOr:
                case ILLang.op_bitXor:
                    result = getTypeDecl("boolean").typeSpec.
                            conformingTypeSpec(typeOf(instrOrExpr.down(1)),
                                    typeOf(instrOrExpr.down(2)));
                    break;
                case ILLang.op_not:
                    result = getTypeDecl("boolean").typeSpec.
                            conformingTypeSpec(typeOf(instrOrExpr.down(1)), null);
                    break;
                case ILLang.op_call: {
                    Object f;
                    FunctionTypeSpec functionTypeSpec = null;
                    WrapperTypeSpec returnType = null;
                    Unit functionUnit;
                    String functionName = ILUtils.getCallFunctionNameString(instrOrExpr);
                    Tree[] actualArgs = ILUtils.getArguments(instrOrExpr).children();
                    if (unit != null && unit.isFortran9x() && "sum".equals(functionName)) {
                        return sumResultType(typeOf(actualArgs[0]), ILUtils.getOptionalDim(actualArgs, this));
                    } else if (unit != null && unit.isFortran9x() && "spread".equals(functionName)) {
                        Tree newDim = ILUtils.build(ILLang.op_dimColon,
                                ILUtils.build(ILLang.op_intCst, 1),
                                ILUtils.copy(actualArgs[2]));
                        return spreadResultType(typeOf(actualArgs[0]), actualArgs[1], newDim);
                    } else if ((f = instrOrExpr.getAnnotation("functionTypeSpec")) != null) {
                        functionTypeSpec = (FunctionTypeSpec) f;
                    } else if ((f = instrOrExpr.getAnnotation("callArrow")) != null) {
                        CallArrow callArrow = (CallArrow) f;
                        functionUnit = callArrow.destination;
                        // annotation "callArrow" not up-to-date in diffCallGraph!
                        if (unit.callGraph() == functionUnit.callGraph()) {
                            functionTypeSpec = functionUnit.functionTypeSpec();
                        }
                    }
                    if (functionTypeSpec == null) {
                        // vmp: rattrapage utile quand on perd les annotations en differentiant
                        // functionName = ILUtils.getCallFunctionNameString(instrOrExpr);
                        WrapperTypeSpec[] types = typesOf(ILUtils.getArguments(instrOrExpr));
                        FunctionDecl functionDecl = ((functionName==null) ? null 
                                                    : getTypedFunctionDecl(functionName, null, types, false, instrOrExpr) );
                        functionUnit = ((functionDecl == null) ? null : functionDecl.unit()) ;
                        if (functionUnit != null) {
                            functionTypeSpec = functionUnit.functionTypeSpec();
                            // cas special intrinsic real, int avec 2 arguments
                            // ou cmplx avec 2 ou 3 arguments
                            // le returnTypeSpec est modifie' par l'argument kind
                            Tree kindTree = functionUnit.getElementalIntrinsicTypeConverter(ILUtils.getArguments(instrOrExpr).children());
                            if (functionTypeSpec!=null && kindTree != null) {
                                returnType = new WrapperTypeSpec(new ModifiedTypeSpec(functionTypeSpec.returnType, kindTree, this));
                            }
                        }
                        // lors de la differentiation, on cree des op_call(sum,...)
                        // sans l'annotation functionTypeSpec
                        // et on ne veut pas recuperer un metaType real pour sum:
                        if ("sum".equals(functionName)
                                && TypeSpec.isA(types[0], SymbolTableConstants.ARRAYTYPE)) {
                            return types[0].wrappedType.elementType();
                        }
                    }
                    if (functionTypeSpec != null && returnType==null) {
                        returnType = functionTypeSpec.returnType;
                    }
                    WrapperTypeSpec preComputedType =
                        instrOrExpr.getAnnotation("arrayReturnTypeSpec") ;
                    if (preComputedType!=null) {
                        returnType = preComputedType ;
                        if (TypeSpec.isA(returnType, SymbolTableConstants.ARRAYTYPE)) {
                            ArrayDim[] dimensions = ((ArrayTypeSpec)returnType.wrappedType).dimensions() ;
                            for (int j=0 ; j<dimensions.length ; ++j) {
                                dimensions[j].detectUnDeclarableLength(this) ;
                            }
                        }
                    }
                    result = returnType;
                    break;
                }
                case ILLang.op_constructorCall: {
                    TypeDecl typeDecl = getTypeDecl(ILUtils.baseTree(instrOrExpr.down(1)).stringValue());
                    if (typeDecl != null) {
                        result = typeDecl.typeSpec;
                    } else {
                        result = unKnownType;
                    }
                    break;
                }
                case ILLang.op_none:
                case ILLang.op_star:
                case ILLang.op_metavar:
                case ILLang.op_if:
                case ILLang.op_where:
                case ILLang.op_deallocate:
                case ILLang.op_expressions:
                case ILLang.op_ioCall:
                case ILLang.op_parallelRegion:
                case ILLang.op_parallelLoop:
                case ILLang.op_include:
                case ILLang.op_varDeclaration:
                case ILLang.op_varDimDeclaration:
                case ILLang.op_constDeclaration:
                case ILLang.op_typeDeclaration:
                case ILLang.op_interfaceDecl:
                case ILLang.op_accessDecl:
                case ILLang.op_intrinsic:
                case ILLang.op_external:
                case ILLang.op_common:
                case ILLang.op_equivalence:
                case ILLang.op_data:
                case ILLang.op_do:
                case ILLang.op_loop:
                case ILLang.op_for:
                case ILLang.op_until:
                case ILLang.op_forall:
                case ILLang.op_forallRanges:
                case ILLang.op_forallRangeTriplet:
                case ILLang.op_forallRangeSet:
                case ILLang.op_while:
                case ILLang.op_times:
                case ILLang.op_compGoto:
                case ILLang.op_assignLabelVar:
                case ILLang.op_gotoLabelVar:
                case ILLang.op_switch:
                case ILLang.op_continue:
                case ILLang.op_stop:
                case ILLang.op_return:
                case ILLang.op_useDecl:
                case ILLang.op_nameList:
                case ILLang.op_save:
                case ILLang.op_arrayType:
                case ILLang.op_modifiedType:
                case ILLang.op_void:
                case ILLang.op_integer:
                case ILLang.op_float:
                case ILLang.op_complex:
                case ILLang.op_boolean:
                case ILLang.op_character:
                case ILLang.op_pointerDeclarator:
                case ILLang.op_referenceDeclarator:
                case ILLang.op_functionDeclarator:
                case ILLang.op_modifiedDeclarator:
                case ILLang.op_sizedDeclarator:
                    result = unKnownType;
                    break;
                case ILLang.op_arrayConstructor: {
                    WrapperTypeSpec resultTypeSpec = null;
                    WrapperTypeSpec componentTypeSpec;
                    Tree[] elements = instrOrExpr.children();
                    // totalLength is the length of the result of the arrayConstructor, -1 if dynamic:
                    int totalLength = 0;
                    int elementLength;
                    for (int i = elements.length - 1; i >= 0; --i) {
                        componentTypeSpec = typeOf(elements[i]);
                        if (TypeSpec.isA(componentTypeSpec, SymbolTableConstants.ARRAYTYPE) && !(unit == null || unit.isC())) {
                            // In Fortran, an array component of a arrayConstructor stands for all its array elements:
                            if (totalLength >= 0) {
                                elementLength =
                                        ArrayTypeSpec.totalArrayLength(componentTypeSpec.wrappedType.getAllDimensions());
                                totalLength = elementLength == -1 ? -1 : totalLength + elementLength;
                            }
                            componentTypeSpec = componentTypeSpec.wrappedType.elementType();
                        } else {
                            if (totalLength >= 0) {
                                ++totalLength;
                            }
                        }
                        if (resultTypeSpec == null) {
                            resultTypeSpec = componentTypeSpec;
                        }
                    }
                    if (unit == null || unit.isC()) {
                        resultTypeSpec =
                                new WrapperTypeSpec(new PointerTypeSpec(resultTypeSpec,
                                        new ArrayDim(null, 0, (totalLength<=0 ? null : totalLength-1), null, -1, 0, 0)));
                    } else {
                        resultTypeSpec =
                                new WrapperTypeSpec(new ArrayTypeSpec(resultTypeSpec,
                                        new ArrayDim[]{new ArrayDim(null, 1,
                                                (totalLength<=0 ? null : totalLength), null, -1, 0, 0)}
                                ));
                    }
                    result = resultTypeSpec;
                    break;
                }
                case ILLang.op_complexConstructor:
                    result = getTypeDecl("complex").typeSpec;
                    break;
                case ILLang.op_ifExpression:
                    // todo typeof de down2 ou down3
                    result = typeOf(instrOrExpr.down(2));
                    break;
                case ILLang.op_allocate: {
                    result = new WrapperTypeSpec(new PointerTypeSpec(new WrapperTypeSpec(null), null));
                    break;
                }
                case ILLang.op_nullify: {
                    result = unKnownType;
                    break;
                }
                case ILLang.op_label:
                    result = new WrapperTypeSpec(new LabelTypeSpec());
                    break;
                case ILLang.op_cast:
                case ILLang.op_multiCast: {
                    WrapperTypeSpec castTypeCheck = null;
                    String castTypeName;
                    if (instrOrExpr.down(1).opCode() == ILLang.op_ident) {
                        castTypeName = instrOrExpr.down(1).stringValue();
                        TypeDecl castTypeDecl = getTypeDecl(castTypeName);
                        if (castTypeDecl != null) {
                            castTypeCheck = castTypeDecl.typeSpec;
                        }
                    } else if (instrOrExpr.down(1).opCode() == ILLang.op_void) {
                        TypeDecl castTypeDecl = getTypeDecl("void");
                        if (castTypeDecl != null) {
                            castTypeCheck = castTypeDecl.typeSpec;
                        }
                    } else if (ILUtils.hasATypeOperator(instrOrExpr.down(1))) {
                        castTypeCheck =
                                TypeSpec.build(instrOrExpr.down(1), this, new Instruction(instrOrExpr),
                                        new TapList<>(null, null), new TapList<>(null, null),
                                        new TapList<>(null, null), new ToBool(false), null);
                    }
                    WrapperTypeSpec typeInside = typeOf(instrOrExpr.down(2));
                    if (instrOrExpr.opCode()==ILLang.op_multiCast && castTypeCheck!=null) {
                        castTypeCheck =
                            new WrapperTypeSpec(
                                new ArrayTypeSpec(castTypeCheck,
                                     ((ArrayTypeSpec)typeInside.wrappedType).dimensions())) ;

                    } else if (castTypeCheck==null) {
                        castTypeCheck = typeInside ;
                    }
                    result = castTypeCheck;
                    break;
                }
                case ILLang.op_mod: {
                    result = typeOf(instrOrExpr.down(1));
                    break;
                }
                case ILLang.op_pointerType:
                    result = TypeSpec.build(instrOrExpr, this, new Instruction(instrOrExpr),
                            new TapList<>(null, null), new TapList<>(null, null),
                            new TapList<>(null, null), new ToBool(false), null);
                    break;
                case ILLang.op_iterativeVariableRef: {
                    WrapperTypeSpec topTypeSpec = typeOf(instrOrExpr.down(1).down(1));
                    Tree doTree = instrOrExpr.down(2);
                    Tree sizeTree = ILUtils.buildSizeTree(doTree.down(2), doTree.down(3), doTree.down(4));
                    ArrayDim[] newDimensions = new ArrayDim[1];
                    newDimensions[0] = new ArrayDim(ILUtils.build(ILLang.op_dimColon),
                            1, computeIntConstant(sizeTree), null, -1, 0, 0);
                    result = new WrapperTypeSpec(new ArrayTypeSpec(topTypeSpec, newDimensions));
                    break;
                }
                case ILLang.op_function:
                case ILLang.op_class:
                case ILLang.op_constructor:
                case ILLang.op_destructor:
                    // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                    result = unKnownType;
                    break;
                default:
                    TapEnv.toolWarning(-1, "(Typing expression) Unexpected operator: " + instrOrExpr.opName() + " in " + (instrOrExpr.parent() == null ? "()" : ILUtils.toString(instrOrExpr.parent())) + " i.e. " + instrOrExpr.parent());
                    result = unKnownType;
                    break;
            }
        }
        if (result == null) {
            result = unKnownType;
        }
        return result;
    }

    /**
     * @return the types of expression "instrOrExpr".
     */
    public WrapperTypeSpec[] typesOf(Tree instrOrExpr) {
        switch (instrOrExpr.opCode()) {
            case ILLang.op_expressions: {
                WrapperTypeSpec[] typeSpec;
                int length = instrOrExpr.length();
                typeSpec = new WrapperTypeSpec[length];
                for (int i = 1; i <= length; i++) {
                    typeSpec[i - 1] = typeOf(instrOrExpr.down(i));
                }
                return typeSpec;
            }
            case ILLang.op_none:
                return new WrapperTypeSpec[0];
            default:
                TapEnv.toolWarning(-1, "(Typing expressions) Unexpected operator: " + instrOrExpr.opName());
                return new WrapperTypeSpec[0];
        }
    }

    protected int refOffset(Tree ref) {
        TapIntList toOffset = new TapIntList(0, null);
        refTypeAndOffsetRec(ref, toOffset);
        return toOffset.head;
    }

    private WrapperTypeSpec refTypeAndOffsetRec(Tree ref, TapIntList toOffset) {
        switch (ref.opCode()) {
            case ILLang.op_ident:
                toOffset.head = 0;
                return getVariableDecl(ILUtils.getIdentString(ref)).type();
            case ILLang.op_arrayAccess: {
                // In IL, indices are ordered in the "natural" order, i.e.
                //  with the slowest-changing index first, and fastest last.
                ArrayTypeSpec arrayType =
                        (ArrayTypeSpec) refTypeAndOffsetRec(ref.down(1), toOffset)
                                .wrappedType;
                ArrayDim[] dims = arrayType.dimensions();
                int elemRank = 0;
                Tree[] indexTrees = ref.down(2).children();
                for (int i = 0; i < indexTrees.length; ++i) {
                    if (i > 0) {
                        if (dims[i] != null) {
                            elemRank *= dims[i].size();
                        } else {
                            TapEnv.toolWarning(-1, "null dimension in " + arrayType);
                        }
                    }
                    Integer constValueI = computeIntConstant(indexTrees[i]);
                    if (constValueI == null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, indexTrees[i], "(DD14) Undefined constant value " + ILUtils.toString(indexTrees[i]));
                    } else {
                        elemRank += constValueI - dims[i].lower;
                    }
                }
                toOffset.head += elemRank * arrayType.elementType().size();
                return arrayType.elementType();
            }
            case ILLang.op_substringAccess: {
                WrapperTypeSpec subType = refTypeAndOffsetRec(ref.down(1), toOffset);
                if (ref.down(2).opCode() != ILLang.op_none) {
                    int strOffset = computeIntConstant(ref.down(2)) - 1;
                    toOffset.head += strOffset;
                }
                return subType;
            }
            case ILLang.op_fieldAccess: {
                CompositeTypeSpec recordType = (CompositeTypeSpec) refTypeAndOffsetRec(
                        ref.down(1), toOffset).wrappedType;
                String fieldName = ILUtils.getIdentString(ref.down(2));
                toOffset.head += recordType.namedFieldOffset(fieldName);
                return recordType.namedFieldType(fieldName);
            }
            default:
                TapEnv.toolWarning(-1, "(Type and offset of VariableRef) Unexpected operator: " + ref.opName());
                return null;
        }
    }

    /**
     * Assuming that "refExpr" is a reference to a memory location, returns the list
     * of the zones that contain the value of "refExpr". Otherwise complains and returns null.
     *
     * @param refExpr     the given memory reference expression.
     * @param total       Contains true upon exit iff the value is the complete
     *                    contents of "refExpr" (out). May be null, in which case it is not used.
     * @param instruction The actual Instruction which contains "refExpr".
     *                    Used to find dynamic info, such as pointer destinations (in).
     *                    May be null, in which case conservative assumptions are made.
     * @return "extendedDeclared" zones as defined in DataFlowAnalyzer.
     */
    public TapIntList listOfZonesOfValue(Tree refExpr, ToBool total, Instruction instruction) {
        return ZoneInfo.listAllZones(treeOfZonesOfValue(refExpr, total, instruction, null), true);
    }

    /**
     * @param instrOrExpr The var-reference expression whose tree of zones is requested (in).
     * @param total       Contains true upon exit iff the value is the complete
     *                    contents of all these zones. May be null, in which case it is not used.
     * @param instruction The actual Instruction which contains "instrOrExpr".
     *                    Used to find dynamic info, such as pointer destinations (in).
     *                    May be null, in which case conservative assumptions are made.
     * @param pointerInfo The BoolMatrix of pointer destinations.
     *                    Used to find dynamic info on pointer destinations (in).
     *                    Needed only when the pointer info is not stored in the instruction,
     *                    i.e. during pointer analysis itself. Can be null when pointer analysis is done.
     * @return the tree of the (structured) zones of the value of "instrOrExpr".
     * Therefore returns a TapList-tree with TapIntList leaves.
     */
    public TapList<?> treeOfZonesOfValue(Tree instrOrExpr, ToBool total,
                                         Instruction instruction, BoolMatrix pointerInfo) {
        if (instrOrExpr == null) {
            return null;
        }
        switch (instrOrExpr.opCode()) {
            case ILLang.op_ident: {
                if (total != null) {
                    total.set(true);
                }
                if (NewSymbolHolder.isANewSymbolRef(instrOrExpr)) {
                    int tmpZoneNum = NewSymbolHolder.getNewSymbolHolder(instrOrExpr).zone;
                    return new TapList<>(new TapIntList(tmpZoneNum, null), null);
                } else {
                    VariableDecl variableDecl = getVariableDecl(ILUtils.getIdentString(instrOrExpr));
                    return (variableDecl==null ? null : variableDecl.zones()) ;
                }
            }
            case ILLang.op_arrayDeclarator: {
                TapList<?> result =
                        treeOfZonesOfValue(instrOrExpr.down(1), total, instruction, pointerInfo);
                WrapperTypeSpec typeInside = typeOf(instrOrExpr.down(1));
                if (unit != null && language == TapEnv.C && typeInside != null
                        && TypeSpec.isA(typeInside.wrappedType, SymbolTableConstants.POINTERTYPE)) {
                    result = TapList.copyTree(result);
                    DataFlowAnalyzer.includePointedElementsInTree(result, null, null, false, this, instruction, false, true);
                    result = new TapList<>(ZoneInfo.listAllZones(result, true), null);
                }
                if (total != null) {
                    total.set(true);
                }
                return result;
            }
            case ILLang.op_arrayAccess: {
                TapList<?> result =
                        treeOfZonesOfValue(instrOrExpr.down(1), total, instruction, pointerInfo);
                if (total != null) {
                    WrapperTypeSpec typeSpec = typeOf(instrOrExpr.down(1));
                    boolean tot = total.get();
                    if (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE) && tot) {
                        ArrayDim[] dims = ((ArrayTypeSpec) typeSpec.wrappedType).dimensions();
                        Tree[] sons = instrOrExpr.down(2).children();
                        tot &= dims.length == sons.length;
                        int i = 0;
                        while (i < sons.length && tot) {
                            tot &= dims[i].fullArrayTriplet(sons[i]);
                            i++;
                        }
                    }
                    total.set(tot);
                }
                return result;
            }
            case ILLang.op_fieldAccess: {
                TapList<?> recordZones =
                        treeOfZonesOfValue(instrOrExpr.down(1), total, instruction, pointerInfo);
                int fieldRank = ILUtils.getFieldRank(instrOrExpr.down(2));
                if (fieldRank == -1) {
                    // on a deja un message TC undefined type  [vmp] sauf si on a perdu l'annotation "fieldRank"
                    return recordZones;
                } else if (recordZones != null && TapList.nth(recordZones, fieldRank) instanceof TapList) {
                    return (TapList) TapList.nth(recordZones, fieldRank);
                } else {
                    //Sometimes the computed recordZones is truncated when there are no zones for the last field(s).
                    return null;
                }
            }
            case ILLang.op_allocate:
                return instrOrExpr.getAnnotation("allocatedZones");
            case ILLang.op_assign:
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_deallocate:
                return treeOfZonesOfValue(instrOrExpr.down(1), total, instruction, pointerInfo);
            case ILLang.op_expressions: {
                TapList result = null;
                Tree[] sons = instrOrExpr.children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    result =
                            new TapList<>(treeOfZonesOfValue(sons[i], total, instruction, pointerInfo),
                                    result);
                }
                return result;
            }
            case ILLang.op_address:
                // cf CONVENTION in DataFlowAnalyzer.includePointedElementsInTreeRec()
                return new TapList<>(null,
                        treeOfZonesOfValue(instrOrExpr.down(1), total, instruction, pointerInfo));

            case ILLang.op_pointerAccess: {
                TapList<?> treeOfZones =
                        treeOfZonesOfValue(instrOrExpr.down(1), total, instruction, pointerInfo);
                if (treeOfZones == null) {
                    return null;
                }
                TapIntList pointerZones = ZoneInfo.listAllZones(new TapList<>(treeOfZones.head, null), true);
                TapList res;
                if (treeOfZones.tail != null) {
                    // If the destination zones are already in place, e.g there was an op_address
                    // REMAIN IN SYNC with CONVENTION in DataFlowAnalyzer.includePointedElementsInTreeRec()
                    res = treeOfZones.tail;
                } else {
                    res = getPointerDestinationZonesTreeHere(pointerZones, instruction, null,
                            pointerInfo, typeOf(instrOrExpr), null, true);
                }
                if (total != null) {
                    // If we collect the destinations of several pointers, or of one array of pointers,
                    // or of a pointer "inside" an array (i.e. A[i] in C),
                    // then the current access is not total:
                    if (pointerZones != null
                            && (pointerZones.tail != null
                            || !isScalarPointerZone(pointerZones.head)
                            || isArrayPointerUse(instrOrExpr)
                            || isArrayPointer(pointerZones.head))) {
                        total.set(false);
                    }
                    // If  the collected destinations hold more than one zone per leaf
                    // then the current access is not total:
                    if (!TapList.allSingletonLeaves(res)) {
                        total.set(false);
                    }
                    // If the declared shape of the pointer's initial destination is not the same as the current
                    // destination's (F90:lha82),
                    // then the current access is not total:
                    if (pointerZones != null) {
                        ZoneInfo initialDest = declaredZoneInfo(pointerZones.head, SymbolTableConstants.ALLKIND) ;
                        if (initialDest != null && !isSameShape(initialDest.targetZonesTree, res)) {
                            total.set(false);
                        }
                    }
                }
                return res;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
                // Only in the case of pointer arithmetic, e.g. (2+&x)
                if (TypeSpec.isA(typeOf(instrOrExpr.down(1)), SymbolTableConstants.POINTERTYPE)) {
                    return treeOfZonesOfValue(instrOrExpr.down(1), total, instruction, pointerInfo);
                }
                if (TypeSpec.isA(typeOf(instrOrExpr.down(2)), SymbolTableConstants.POINTERTYPE)) {
                    return treeOfZonesOfValue(instrOrExpr.down(2), total, instruction, pointerInfo);
                }
                return null;
            case ILLang.op_nameEq:
            case ILLang.op_cast:
                return treeOfZonesOfValue(instrOrExpr.down(2), total, instruction, pointerInfo);
            default:
                return null;
        }
    }

    //TODO: IS THIS REDUNDANT WITH DataFlowAnalyzer.includePointedElementsInTree() ???
    /**
     * Builds a copy of the given "zonesTree", in which all pointer zones are
     * followed with the zonesTree of their possible destinations at the current point.
     * The current point in the code is represented by either the 'instruction" or
     * the current "pointerInfo" matrix. If both "instruction" and "pointerInfo"
     * are null, we make the conservative assumption that all "target" vars may be destinations.
     *
     * @param instruction The actual Instruction of the expression which gave the "zonesTree".
     *                    Used to find current pointer destinations, which is dynamic.
     * @param pointerInfo The BoolMatrix of pointer destinations.
     *                    Needed only when the pointer info is not stored in the instruction,
     *                    i.e. during pointer analysis itself. Can be null when pointer analysis is done.
     * @param upstream    true when one must use the pointer destinations upstream the instruction.
     */
    public TapList includePointedElementsInZonesTree(TapList<?> zonesTree,
                                                     Instruction instruction, BoolMatrix pointerInfo, boolean upstream) {
        return ipeiztRec(zonesTree, null, instruction, pointerInfo, upstream);
    }

    private TapList ipeiztRec(TapList<?> zonesTree, TapIntList dejaVuZones,
                              Instruction instruction, BoolMatrix pointerInfo, boolean upstream) {
        if (zonesTree == null) {
            return null;
        } else if (zonesTree.head instanceof TapList) {//Recursive case:
            TapList hdSubTrees = new TapList<>(null, null);
            TapList tlSubTrees = hdSubTrees;
            while (zonesTree != null) {
                tlSubTrees = tlSubTrees.placdl(ipeiztRec((TapList) zonesTree.head, dejaVuZones,
                        instruction, pointerInfo, upstream));
                zonesTree = zonesTree.tail;
            }
            return hdSubTrees.tail;
        } else {// head of zonesTree is a leaf, must be a TapIntList. We may be on a pointer:
            TapList result = new TapList<>(zonesTree.head, null);
            if (zonesTree.tail != null) {
                result.tail = ipeiztRec(zonesTree.tail, dejaVuZones, instruction, pointerInfo, upstream);
            } else if (!TapIntList.contains(dejaVuZones, (TapIntList) zonesTree.head)) {
                TapIntList pointerZoneRanks = (TapIntList) zonesTree.head;
                ZoneInfo pzi = null;
                WrapperTypeSpec type;
                if (pointerZoneRanks != null) {
                    pzi = declaredZoneInfo(pointerZoneRanks.head, SymbolTableConstants.ALLKIND);
                }
                if (pzi != null && TypeSpec.isA(pzi.type, SymbolTableConstants.POINTERTYPE)) {
                    type = ((PointerTypeSpec) pzi.type.wrappedType).destinationType;
                    result.tail =
                            getPointerDestinationZonesTreeHere((TapIntList) zonesTree.head, instruction,
                                    instruction.block, pointerInfo, type, null, upstream);
                    result.tail =
                            ipeiztRec(result.tail,
                                    TapIntList.quickUnion(dejaVuZones, (TapIntList) zonesTree.head),
                                    instruction, pointerInfo, upstream);
                }
            }
            return result;
        }
    }

    /**
     * Utility identical to the op_pointerAccess case of treeOfZonesOfValue().
     */
    public TapList zonesOfDestination(TapList<?> treeOfZones, WrapperTypeSpec pointerType,
                                      Instruction instruction, BoolMatrix pointerInfo) {
        if (treeOfZones == null) {
            return null;
        } else if (treeOfZones.tail != null)
        // If the destination zones are already in place, e.g there was an op_address
        // REMAIN IN SYNC with CONVENTION in DataFlowAnalyzer.includePointedElementsInTreeRec()
        {
            return treeOfZones.tail;
        } else {
            TapIntList pointerZones = ZoneInfo.listAllZones(treeOfZones, true);
            WrapperTypeSpec destType =
                    TypeSpec.isA(pointerType, SymbolTableConstants.POINTERTYPE)
                            ? ((PointerTypeSpec) pointerType.wrappedType).destinationType
                            : null;
            return getPointerDestinationZonesTreeHere(pointerZones, instruction,
                    null, pointerInfo, destType, null, true);
        }
    }

    /**
     * @param pointerZones The list of the extended declared ALLKIND zones of the pointer(s).
     * @param instruction  (Optional) The current Instruction (at which these destinations are required).
     * @param block        The current Block (which contains the current Instruction).
     * @param pointerInfo  (Optional) The current info on pointers destination.
     * @param type         The type of these pointers' destination.
     * @param calledUnit   When on a call, the called Unit. May be null otherwise.
     * @param upstream     true when one must use the pointer destinations upstream the instruction.
     * @return the TapList tree of zones of the destination of the pointer(s)
     * whose zone(s) is in "pointerZones". The type of the destination of the pointer
     * must be given in "type". This search is done in the pointer destination context
     * pointerInfo, or that of Instruction "instruction", or of the beginning of Block "block".
     * If instruction and block and pointerInfo are null, we have no pointer info
     * available, and therefore we return all possible "target" variables.
     */
    public TapList getPointerDestinationZonesTreeHere(TapIntList pointerZones, Instruction instruction,
                                                      Block block, BoolMatrix pointerInfo,
                                                      WrapperTypeSpec type, Unit calledUnit, boolean upstream) {
        if (instruction != null) {
            block = instruction.block;
        }
        TapList destZonesTree = null;
        if (pointerInfo == null && (block == null || block.pointerInfosIn == null)) {
            // If there is no pointer-destination info available, return
            // the initial destination tree plus all possible TARGET's
            // TODO: This approximation may be wrong in general!
            ZoneInfo pointerZone;
            // Collect the default destination trees of all the true pointer zones in the list:
            while (pointerZones != null) {
                if (pointerZones.head != unit.callGraph().zoneNbOfAllIOStreams) { // Neglect the IO zone!
                    pointerZone = declaredZoneInfo(pointerZones.head, SymbolTableConstants.ALLKIND);
                    if (pointerZone != null && pointerZone.targetZonesTree != null) {
                        if (destZonesTree == null) {
                            destZonesTree = TapList.copyTree(pointerZone.targetZonesTree);
                        } else {
                            destZonesTree = TapList.cumulWithOper(destZonesTree, pointerZone.targetZonesTree, SymbolTableConstants.CUMUL_OR);
                        }
                    }
                }
                pointerZones = pointerZones.tail;
            }
            TapList<SymbolDecl> targetDecls = getAllTargetDecls(type);
            SymbolDecl targetDecl;
            while (targetDecls != null) {
                targetDecl = targetDecls.head;
                if (targetDecl.isA(SymbolTableConstants.VARIABLE)) {
                    destZonesTree = TapList.cumulWithOper(destZonesTree,
                            targetDecl.zones(), SymbolTableConstants.CUMUL_OR);
                } else if (targetDecl.isA(SymbolTableConstants.FUNCTION) && targetDecl.zones() != null) {
                    destZonesTree = TapList.cumulWithOper(destZonesTree,
                            targetDecl.zones(), SymbolTableConstants.CUMUL_OR);
                }
                targetDecls = targetDecls.tail;
            }
        } else {
            BoolVector cumulPointedZonesVector = null;
            BoolVector pointedZonesVector;
            int pointerRowIndex;
            boolean hasIOpointer = pointerZones != null && pointerZones.head == unit.callGraph().zoneNbOfAllIOStreams; // Neglect the IO zone!
            while (pointerZones != null) {
                pointerRowIndex =
                        DataFlowAnalyzer.zoneRkToKindZoneRk(pointerZones.head, SymbolTableConstants.PTRKIND, this);
                if (pointerRowIndex >= 0) {
                    pointedZonesVector =
                            getPointerDestinationVector(pointerRowIndex, instruction, block, pointerInfo, upstream);
                    if (cumulPointedZonesVector == null) {
                        cumulPointedZonesVector = pointedZonesVector.copy();
                    } else {
                        cumulPointedZonesVector.cumulOr(pointedZonesVector);
                    }
                }
                pointerZones = pointerZones.tail;
            }
            if (cumulPointedZonesVector != null) {
                destZonesTree = treeOfPointedZones(cumulPointedZonesVector, declaredZonesNb(SymbolTableConstants.ALLKIND), type);
            } else if (hasIOpointer) {
                destZonesTree = null;
            } else {
                //If the search returns empty, return Undef destination:
                destZonesTree = new TapList<>(new TapIntList(getCallGraph().zoneNbOfUnknownDest, null), null);
            }
        }
        return destZonesTree;
    }

    /**
     * @param upstream true when one must use the pointer destinations upstream the instruction.
     * @return null when not found only: when the pointer dests row is null,
     * meaning implicit-identity, returns explicit-identity non-null BoolVector.
     * This function can be called in 2 ways/contexts:
     * EITHER (during pointer analysis) given the complete pointer info matrix
     * at this time (in arg "pointerInfo"),
     * OR (during any later analysis), with a null "pointerInfo", but given the
     * current "instruction" and its containing "block". In this case, the search
     * goes upstream in the instructions and if no info is found, the
     * pointer destination info at the begining of the block is used.
     */
    public BoolVector getPointerDestinationVector(int pointerRowIndex,
                                                  Instruction instruction, Block block, BoolMatrix pointerInfo, boolean upstream) {
        BoolVector result = null;
        TapPair<TapIntList, BoolVector> cell;
        boolean found = false;
        if (pointerInfo == null && instruction != null) {
            TapList<Instruction> instructionsUp = null;
            TapList<Instruction> allInstructions = block.instructions;
            while (allInstructions != null && allInstructions.head != instruction) {
                instructionsUp =
                        new TapList<>(allInstructions.head,
                                instructionsUp);
                allInstructions = allInstructions.tail;
            }
            //When "upstream" is false, we must take the pointer info AFTER the current instruction:
            if (!upstream && allInstructions != null) {
                instructionsUp =
                        new TapList<>(allInstructions.head, instructionsUp);
            }
            while (!found && instructionsUp != null) {
                cell = TapList.assqOne(pointerRowIndex,
                        instructionsUp.head.pointerDestsChanges);
                if (cell != null) {
                    result = cell.second;
                    found = true;
                }
                instructionsUp = instructionsUp.tail;
            }
        }
        if (!found) {
            if (pointerInfo == null) {
                pointerInfo = block.pointerInfosIn;
            }
            if (pointerInfo != null) {
                found = true;
                result = pointerInfo.getRow(pointerRowIndex);
            }
        }
        if (found && result == null) { //Transform implicit-Identity into explicit-Identity:
            result = PointerAnalyzer.buildExplicitIdentityDestsRow(pointerRowIndex, this);
        }
        return result;
    }

    private boolean isScalarPointerZone(int zone) {
        ZoneInfo zi = declaredZoneInfo(zone, SymbolTableConstants.ALLKIND);
        return zi != null && zi.accessIndexes == null;
    }

    private boolean isArrayPointerUse(Tree pointerUse) {
        return !ILUtils.isNullOrNone(pointerUse.down(2));
    }

    private boolean isArrayPointer(int zone) {
        ZoneInfo zi = declaredZoneInfo(zone, SymbolTableConstants.ALLKIND);
        return zi != null && TypeSpec.isA(zi.type, SymbolTableConstants.POINTERTYPE)
                && ((PointerTypeSpec) zi.type.wrappedType).offsetLength != null;
    }

    private boolean isSameShape(TapList<?> initialDest, TapList<?> currentDest) {
        boolean sameShape = true;
        while (sameShape && initialDest != null && currentDest != null) {
            if (initialDest.head instanceof TapList && currentDest.head instanceof TapList) {
                sameShape = isSameShape((TapList) initialDest.head, (TapList) currentDest.head);
            } else if (initialDest.head instanceof TapIntList && currentDest.head instanceof TapIntList) {
                sameShape = isSameShape((TapIntList) initialDest.head, (TapIntList) currentDest.head);
            } else {
                sameShape = false;
            }
            initialDest = initialDest.tail;
            currentDest = currentDest.tail;
            if ((initialDest == null) == (currentDest != null)) {
                sameShape = false;
            }
        }
        return sameShape;
    }

    private boolean isSameShape(TapIntList initialDest, TapIntList currentDest) {
        if (initialDest.tail != null || currentDest.tail != null) {
            return false;
        }
        ZoneInfo initialDestZone = declaredZoneInfo(initialDest.head, SymbolTableConstants.ALLKIND);
        ZoneInfo currentDestZone = declaredZoneInfo(currentDest.head, SymbolTableConstants.ALLKIND);
        int initialDestSize = initialDestZone == null ? -1 : initialDestZone.knownSize(this);
        int currentDestSize = currentDestZone == null ? -1 : currentDestZone.knownSize(this);
        return initialDestSize > 0 && currentDestSize == initialDestSize;
    }

    /**
     * @return a COPY of the given zonesTree, extended with the zones of
     * the initial destinations of any pointer inside this zonesTree, recursively.
     */
    private TapList includePointedInitialsInTree(TapList<?> zonesTree, SymbolTable origSymbolTable /*was ZoneInfo[] declaredAllKindArray*/) {
        return includePointedInitialsInTreeRec(zonesTree, new TapIntList(-1, null), origSymbolTable /*was declaredAllKindArray*/);
    }

    private TapList includePointedInitialsInTreeRec(TapList<?> zonesTree, TapIntList dejaVu,
                                                    SymbolTable origSymbolTable /*was ZoneInfo[] declaredAllKindArray*/) {
        if (zonesTree == null) {
            return null;
        }
        if (zonesTree.head instanceof TapList) {
            TapList result = new TapList<>(null, null);
            TapList tlResult = result;
            while (zonesTree != null) {
                tlResult =
                        tlResult.placdl(includePointedInitialsInTreeRec((TapList) zonesTree.head, dejaVu,
									origSymbolTable /*was declaredAllKindArray*/));
                zonesTree = zonesTree.tail;
            }
            return result.tail;
        } else {// head of zonesTree is a leaf, must be a TapIntList. We may be on a pointer:
            TapIntList zonesList = (TapIntList) zonesTree.head;
            TapIntList zonesListCopy = TapIntList.copy(zonesList);
            ZoneInfo zi;
            TapList cumulTargets = null;
	    int firstIndexInOrig = origSymbolTable.firstDeclaredZone[SymbolTableConstants.ALLKIND] ;
            while (zonesList != null) {
                if (!TapIntList.contains(dejaVu.tail, zonesList.head)) {
                    dejaVu.tail = new TapIntList(zonesList.head, dejaVu.tail);
                    zi = declaredZoneInfo(zonesList.head, SymbolTableConstants.ALLKIND);
                    if (zi == null && zonesList.head >= firstDeclaredZone[SymbolTableConstants.ALLKIND]) {
                        // Due to the system that avoids copying zoneInfos of hidden imported variables,
                        // declaredZoneInfo(..) may return null. In that case, search it into
                        // the original, non-hidden array of declared ZoneInfos.
                        zi = origSymbolTable.declaredZoneInfos[SymbolTableConstants.ALLKIND][zonesList.head - firstIndexInOrig];
                    }
                    if (zi != null && zi.targetZonesTree != null) {
                        cumulTargets = TapList.cumulWithOper(cumulTargets, zi.targetZonesTree, SymbolTableConstants.CUMUL_OR);
                    }
                }
                zonesList = zonesList.tail;
            }
            if (cumulTargets != null) {
                cumulTargets = includePointedInitialsInTreeRec(cumulTargets, dejaVu, origSymbolTable /*was declaredAllKindArray*/);
            }
            return new TapList<>(zonesListCopy, cumulTargets);
        }
    }

    /**
     * Same as treeOfPointedZones(), but returns only the sub-tree of zones that concerns the
     * access sub-path "pathToSubTree".
     * Maybe we can rewrite more efficiently without constructing the complete pointerDestsTree?
     */
    public TapList subTreeOfPointedZones(BoolVector destsRow, int rowLength, WrapperTypeSpec destinationType,
                                         Tree pathToSubTree) {
        TapList pointerDestsTree =
            new TapList<>(null, treeOfPointedZones(destsRow, rowLength, destinationType));
        return TapList.getSetFieldLocation(pointerDestsTree, pathToSubTree, false);
    }

    /**
     * @return a reference expression that may be used from the scope of this SymbolTable,
     * to access (exactly) the given zone (i.e. zoneInfo).
     * In most cases this will be the same as zoneInfo.accessTree,
     * but not always e.g. when the variable is declared extern, with a type
     * which is different from its type in its basis, reference declaration.
     * cf non-regression set08/lh068.
     * Warning: The current mechanism uses the initial pointer destination zones, so this mechanism
     * might still be wrong if pointers have been redirected, in which case the returned expression
     * might point to a different location, and the given zone may even become inaccessible !!
     */
    public Tree buildZoneVisibleAccessTree(ZoneInfo zoneInfo) {
        Tree visibleAccessTree = null;
        TapList<String> varNames = zoneInfo.variableNames();
        String varName;
        VariableDecl varDecl;
        TapList varZones;
        TapList<Tree> reversedTreeNodes;
        Tree treeNode;
        WrapperTypeSpec rootType;
        if (varNames == null) {
            varNames = new TapList<>(ILUtils.baseName(zoneInfo.accessTree), null);
        }
        while (visibleAccessTree == null && varNames != null) {
            varName = varNames.head;
            if (varName != null) {
                varDecl = getTopVariableDecl(varName);
                if (varDecl != null) {
                    varZones = includePointedInitialsInTree(varDecl.zones(), this /*was declaredZoneInfos[SymbolTableConstants.ALLKIND]*/);
                    if (TapIntList.contains(ZoneInfo.listAllZones(varZones, true), zoneInfo.zoneNb)) {
                        visibleAccessTree = ILUtils.copy(zoneInfo.accessTree);
                        reversedTreeNodes = ILUtils.reversedExprAccess(visibleAccessTree);
                        // If the name used at the root of the visibleAccessTree (reversedTreeNodes.head) differs from
                        // the root access name available locally (varName), then modify the root of visibleAccessTree:
                        treeNode = reversedTreeNodes.head;
                        if (treeNode.opCode() == ILLang.op_ident && !varName.equals(treeNode.stringValue())) {
                            treeNode.setValue(varName);
                        }
                        reversedTreeNodes = reversedTreeNodes.tail;
                        rootType = varDecl.type();
                        while (reversedTreeNodes != null) {
                            treeNode = reversedTreeNodes.head;
                            if (treeNode.opCode() == ILLang.op_fieldAccess && rootType.wrappedType.kind() == SymbolTableConstants.COMPOSITETYPE) {
                                // For any fieldAccess field name used in the visibleAccessTree (treeNode),
                                // if it differs from the field name defined in the local structured type
                                // definition (fieldDecl.symbol), then modify it with the local field name:
                                TapList foundFieldZoneTree = null;
                                int fieldRank = -1;
                                while (varZones != null && foundFieldZoneTree == null) {
                                    ++fieldRank;
                                    if (TapIntList.contains(ZoneInfo.listAllZones((TapList) varZones.head, true), zoneInfo.zoneNb)) {
                                        foundFieldZoneTree = (TapList) varZones.head;
                                    }
                                    varZones = varZones.tail;
                                }
                                FieldDecl fieldDecl = ((CompositeTypeSpec) rootType.wrappedType).fields[fieldRank];
                                if (treeNode.down(2).opCode() == ILLang.op_ident &&
                                        !fieldDecl.symbol.equals(treeNode.down(2).stringValue())) {
                                    treeNode.down(2).setValue(fieldDecl.symbol);
                                }
                                rootType = fieldDecl.type();
                                varZones = foundFieldZoneTree;
                                reversedTreeNodes = reversedTreeNodes.tail;
                                break;
                            } else if (treeNode.opCode() == ILLang.op_pointerAccess
                                    && rootType.wrappedType.kind() == SymbolTableConstants.POINTERTYPE) {
                                assert varZones != null;
                                if (TapIntList.contains((TapIntList) varZones.head, zoneInfo.zoneNb)) {
                                    // Stop exploration:
                                    reversedTreeNodes = null;
                                } else {
                                    rootType = ((PointerTypeSpec) rootType.wrappedType).destinationType;
                                    varZones = varZones.tail;
                                    reversedTreeNodes = reversedTreeNodes.tail;
                                }
                            } else if (treeNode.opCode() == ILLang.op_arrayAccess && rootType.wrappedType.kind() == SymbolTableConstants.ARRAYTYPE) {
                                rootType = rootType.wrappedType.elementType();
                                reversedTreeNodes = reversedTreeNodes.tail;
                            } else if (rootType.wrappedType.kind() == SymbolTableConstants.MODIFIEDTYPE) {
                                rootType = rootType.wrappedType.elementType();
                                reversedTreeNodes = reversedTreeNodes.tail;
                            } else {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(Visible access tree) local type " + varDecl.type() + " of " + varName + " incompatible with global " + zoneInfo.accessTree);
                                reversedTreeNodes = null;
                            }
                        }
                    }
                }
            }
            varNames = varNames.tail;
        }
        if (visibleAccessTree == null && basisSymbolTable != null &&
                // In C, don't look for symbols in the C root SymbolTable: they are not visible by default!
                //  a visible symbol must have been at least declared extern at the file SymbolTable level.
                !(TapEnv.isC(language) && isTranslationUnitSymbolTable())) {
            visibleAccessTree = basisSymbolTable.buildZoneVisibleAccessTree(zoneInfo);
        }
        return visibleAccessTree;
    }

    /**
     * Given a flat BoolVector "destsRow" designating zone numbers,
     * and knowing that these are the zones of a pointer destination
     * which is of type "destinationType", reorganizes these zones as a
     * TapList-tree of TapIntList, following the type's structure.
     * NOTE: in a rare case (for PointerAnalyzer), rowLength may be greater than
     * declaredZonesNb(ALLKIND). This happens when PointerAnalyzer analyzes a procedure call
     * and the excess ranks represent the elementary formal arguments at the call site.
     * @param destsRow        A BoolVector whose "true" elements are the given zones.
     * @param rowLength       The length of destsRow.
     * @param destinationType The type of the pointer destination.
     * @return The given zone numbers reorganized as a TapList-tree of TapIntList.
     */
    private TapList treeOfPointedZones(BoolVector destsRow, int rowLength,
                                       WrapperTypeSpec destinationType) {
        TapList resultTree = new TapList<>(null, null);
        TapIntList forgottenZones = null;
        ZoneInfo zoneInfo;
        Tree subAccessTree;
        for (int indexInRow=rowLength-1 ; indexInRow>=0 ; --indexInRow) {
            if (destsRow.get(indexInRow)) {
                // ^ if this indexInRow in pointed to by this destination row
                if (indexInRow<declaredZonesNb(SymbolTableConstants.ALLKIND)) { // Standard case:
                    zoneInfo = declaredZoneInfo(indexInRow, SymbolTableConstants.ALLKIND);
                } else { // Special case of FP function call parameters in PointerAnalyzer:
                    zoneInfo = null ; //bizarre, but code always worked with that ?!
                }
                //Find where this zone fits inside the destinationType:
// System.out.println(" ZI FOR INDEX "+indexInRow+" IN ROW IS "+zoneInfo) ;
                if (zoneInfo == null) {
                    subAccessTree = null;
                } else if (zoneInfo.targetZoneOf != null) {
                    WrapperTypeSpec pointingTypeSpec = zoneInfo.targetZoneOf.type;
                    pointingTypeSpec = pointingTypeSpec.baseTypeSpec(true);
                    subAccessTree = findTypeInTypedTree(destinationType, zoneInfo.accessTree,
                                ((PointerTypeSpec) pointingTypeSpec.wrappedType).destinationType,
                                null, null, true);
                } else {
                    subAccessTree = findTypeInTypedTree(destinationType, zoneInfo.accessTree,
                                                        zoneInfo.rootAccessType, null, null, true);
                }
                if (subAccessTree == null) {
                    // Neglect the IO zone, because its accessTree does not let us find
                    // its location in the future tree of zones being built.
                    if (indexInRow != getCallGraph().zoneNbOfAllIOStreams) {
                        forgottenZones = new TapIntList(indexInRow, forgottenZones);
                    }
                } else {
                    // add this zone at its correct place in the resultTree:
                    TapList leaf = TapList.getSetFieldLocation(resultTree, subAccessTree, true);
                    TapList.addIntoLeaves(leaf, new TapIntList(indexInRow, null));
                }
            }
        }
        if (forgottenZones != null) {
            TapList.addIntoLeaves(resultTree, forgottenZones);
        }
        return resultTree;
    }

    /**
     * @return a list of Strings that designates all the possible pointer destinations
     * found in the zone vector "destsRow", including possibly NULL, Undef, or Unknown.
     */
    public TapList<String> listOfPointedDestinations(BoolVector destsRow, int nDZ,
                                                     WrapperTypeSpec destinationType) {
        ZoneInfo zoneInfo;
        TapList<Tree> dTrees = null;
        TapList<String> visibleDests = null;
        TapList<String> specialDests = null;
        TapPair<Tree, TapList> accessAndZones = new TapPair<>(null, null);
        BoolVector destsRowCopy = destsRow.copy();
        boolean pointsToNull = false ;
        boolean pointsToUndef = false ;
        boolean pointsToUnknown = false ;
        for (int zoneIndex=nDZ-1 ; zoneIndex>=0 ; --zoneIndex) {
                if (destsRowCopy.get(zoneIndex)) {  // if this zoneIndex in pointed to by this destsRow:
                    zoneInfo = declaredZoneInfo(zoneIndex, SymbolTableConstants.ALLKIND) ;
                    //TODO SIMPLIFY THIS 1st IF CASE AWAY WHEN USING NEW ZONES FOR NULL & UNKNOWN DEST.
                    if (zoneInfo==null) {
                        if (zoneIndex == getCallGraph().zoneNbOfUnknownDest) {
                            pointsToUndef = true ;
                        } else if (zoneIndex == getCallGraph().zoneNbOfNullDest) {
                            pointsToNull = true ;
                        } else {
                            pointsToUnknown = true ;
                        }
                    } else if (zoneIndex == getCallGraph().zoneNbOfUnknownDest) {
                        pointsToUndef = true ;
                    } else if (zoneIndex == getCallGraph().zoneNbOfNullDest) {
                        pointsToNull = true ;
                    } else if (zoneInfo.accessTree==null) {
                        specialDests = new TapList<>(zoneInfo.description, specialDests) ;
                    } else {
                        accessAndZones.first = null ;
                        accessAndZones.second = null ;
                        if (zoneInfo.targetZoneOf != null) {
                            findTypeInTypedTree(destinationType, zoneInfo.accessTree,
                                    ((PointerTypeSpec) zoneInfo.targetZoneOf.type.wrappedType).destinationType,
                                    zoneInfo.targetZoneOf.targetZonesTree, accessAndZones, false);
                        } else if (zoneInfo.ownerSymbolDecl != null) {
                            findTypeInTypedTree(destinationType, zoneInfo.accessTree,
                                    zoneInfo.rootAccessType,
                                    zoneInfo.ownerSymbolDecl.zones(), accessAndZones, false);
                        }
                        if (accessAndZones.first == null) { // error case recovery!
                            accessAndZones.first = zoneInfo.accessTree;
                            accessAndZones.second = null;
                        }
                        TapIntList zonesDone = ZoneInfo.listAllZonesWithNU(accessAndZones.second, true);
                        destsRowCopy.set(zonesDone, false);
                        Tree destTree = accessAndZones.first;
                        if (zoneInfo.isHidden) {
                            // If zone is Hidden here, we prefer to refer to it through its description.
                            destTree = ILUtils.replaceRootWithNewRoot(destTree, zoneInfo.accessTree,
                                                              ILUtils.build(ILLang.op_ident, zoneInfo.description)) ;
                        }
                        if (!TapList.containsTree(dTrees, destTree)) {
                            dTrees = new TapList<>(destTree, dTrees);
                            visibleDests =
                                new TapList<>(ILUtils.toString(destTree, language), visibleDests) ;
                        }
                    }
                }
        }
        if (pointsToNull) specialDests = TapList.addLast(specialDests, "NULL") ;
        if (pointsToUndef) specialDests = TapList.addLast(specialDests, "Undef") ;
        if (pointsToUnknown) specialDests = TapList.addLast(specialDests, "Unknown") ;
        return TapList.append(visibleDests, specialDests);
    }

    /**
     * Searches for a subtree of "accessTree" with WrapperTypeSpec "searchedType".
     * When found, returns this found subtree, an access tree from the "accessTree"
     * to the found subtree, and the TapList-tree of zones for this found subtree.
     *
     * @param searchedType   The type of the subtree we are looking for in "accessTree".
     * @param accessTree     The root Tree in which we are searching for the subtree of type "searchedType".
     * @param givenType      The type of the root of "accessTree".
     * @param accessAndZones When non-null, a pair filled upon exit with
     *                       the accessTree to the subtree and its tree of zones.
     * @return The found subtree
     */
    private Tree findTypeInTypedTree(WrapperTypeSpec searchedType, Tree accessTree, WrapperTypeSpec givenType,
                                     TapList<?> zonesTree, TapPair<Tree, TapList> accessAndZones,
                                     boolean peelAllocate) {
        if (accessAndZones != null) {
            accessAndZones.first = null;
            accessAndZones.second = null;
        }
        WrapperTypeSpec searchedTypeElement = null;
        if (TypeSpec.isA(searchedType, SymbolTableConstants.ARRAYTYPE)) {
            searchedTypeElement = searchedType.wrappedType.elementType();
        }
        TapList<Tree> reversedAccesses = ILUtils.reversedExprAccessFromPointerAccess(accessTree, peelAllocate);
// System.out.println("REVERSEDACCESSES FROM "+accessTree+" IS "+reversedAccesses) ;
        Tree subAccessTree = null; // remains null until searchedType is found.
        while (reversedAccesses != null && givenType != null && givenType.wrappedType != null) {
            //If the searchedType is found NOW, initiate building of the subAccessTree and store the access to subTree:
            // We must also say that we have found it when the searched type is an array
            // and the current givenType is equal to its element's types:
            if (subAccessTree == null &&
                    (givenType.equalsCompilIndep(searchedType)
                            ||
                            searchedTypeElement != null &&
                                    givenType.equalsCompilIndep(searchedTypeElement))) {
                subAccessTree = ILUtils.build(ILLang.op_none);
                if (accessAndZones != null) {
                    accessAndZones.first = reversedAccesses.head;
                    accessAndZones.second = zonesTree;
                }
            }
            switch (givenType.wrappedType.kind()) {
                case SymbolTableConstants.ARRAYTYPE:
//                     reversedAccesses = reversedAccesses.tail;
                    if (reversedAccesses.tail != null &&
                            reversedAccesses.tail.head.opCode() == ILLang.op_arrayAccess) {
                        reversedAccesses = reversedAccesses.tail;
//                         givenType = givenType.wrappedType.elementType();
                        if (subAccessTree != null) {
                            subAccessTree = ILUtils.build(ILLang.op_arrayAccess, subAccessTree, null);
                        }
//                     } else {
//                         givenType = null;
                    }
                    givenType = givenType.wrappedType.elementType();
                    break;
                case SymbolTableConstants.COMPOSITETYPE:
                    reversedAccesses = reversedAccesses.tail;
                    if (reversedAccesses != null &&
                            reversedAccesses.head.opCode() == ILLang.op_fieldAccess) {
                        Tree field = reversedAccesses.head.down(2);
                        givenType = ((CompositeTypeSpec) givenType.wrappedType).getFieldType(field);
                        if (subAccessTree != null) {
                            subAccessTree = ILUtils.build(ILLang.op_fieldAccess, subAccessTree, ILUtils.copy(field));
                            ILUtils.setFieldRank(subAccessTree.down(2), ILUtils.getFieldRank(field));
                        }
                        if (zonesTree != null) {
                            zonesTree = (TapList) TapList.nth(zonesTree, ILUtils.getFieldRank(field));
                        }
                    } else {
                        givenType = null;
                    }
                    break;
                case SymbolTableConstants.MODIFIEDTYPE:
                    givenType = givenType.wrappedType.elementType();
                    break;
                case SymbolTableConstants.POINTERTYPE:
                case SymbolTableConstants.PRIMITIVETYPE:
                case SymbolTableConstants.FUNCTIONTYPE:
                    //case METATYPE: // Impossible case(s), should never occur !
                default:
                    givenType = null;
                    break;
            }
        }
        return subAccessTree;
    }

    /**
     * Collects the ZoneInfo of all expressions of which "tree" takes the address (e.g. "&amp;v").
     */
    public TapList<ZoneInfo> collectAddressTakenZones(Tree tree,
                                                      Instruction instruction,
                                                      TapList<ZoneInfo> collected) {
        if (tree != null) {
            if (tree.opCode() == ILLang.op_address) {
                TapIntList addressTakenZones = listOfZonesOfValue(tree.down(1), null, instruction);
                while (addressTakenZones != null) {
                    ZoneInfo zi = declaredZoneInfo(addressTakenZones.head, SymbolTableConstants.ALLKIND);
                    if (!TapList.contains(collected, zi)) {
                        collected = new TapList<>(zi, collected);
                    }
                    addressTakenZones = addressTakenZones.tail;
                }
            } else if (!tree.isAtom()) {
                Tree[] subTrees = tree.children();
                for (int i = subTrees.length - 1; i >= 0; --i) {
                    collected = collectAddressTakenZones(subTrees[i], instruction, collected);
                }
            }
        }
        return collected;
    }

    /** Returns a translation of the given "vectorAllKind", which is based on the ALLKIND zones numbering,
     * into its subset dealing only with "kind" zones, and using the "kind" zones numbering. */
    public BoolVector focusToKind(BoolVector vectorAllKind, int kind) {
        if (kind==SymbolTableConstants.ALLKIND) return vectorAllKind ;
        int kindLength = declaredZonesNb(kind) ;
        BoolVector result = new BoolVector(kindLength) ;
        for (int i=kindLength-1 ; i>=0 ; --i) {
            ZoneInfo zi = declaredZoneInfo(i, kind) ;
            if (zi!=null && vectorAllKind.get(zi.zoneNb)) {
                result.set(i, true) ;
            }
        }
        return result ;
    }

    /** Reciprocal of focusToKind(). Fills the other zones with false */
    public BoolVector unfocusFromKind(BoolVector vectorKind, int kind) {
        if (kind==SymbolTableConstants.ALLKIND) return vectorKind ;
        int length = declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        BoolVector result = new BoolVector(length) ;
        for (int i=length-1 ; i>=0 ; --i) {
            ZoneInfo zi = declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
            if (zi!=null) {
                int ki = zi.kindZoneNb(kind) ;
                if (ki>=0 && vectorKind.get(ki)) {
                    result.set(i, true) ;
                }
            }
        }
        return result ;
    }

    /**
     * @return a BoolVector of all the (private) (extended declared) (ALLKIND) zones that
     * are used (i.e. actually read) during the evaluation of expression "instrOrExpr".
     * Each zone is encoded as a single bit in the resulting BoolVector.
     * The map of the zones in the resulting BoolVector is: {0 ; 0 ; nDZones}.
     */
    public BoolVector zonesUsedByExp(Tree instrOrExpr, Instruction instruction) {
        BoolVector usedZones = new BoolVector(declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
        TapList zonesOfExp = zUBErec(instrOrExpr, usedZones, instruction);
        // Set as used the zones returned by instrOrExpr, but not their destination if pointer.
        usedZones.set(ZoneInfo.listAllZones(zonesOfExp, false), true);
        return usedZones;
    }

    /**
     * Private recursive utility for "zonesUsedByExp()".
     * Adds into "usedZones" all the zones that are needed
     * "in passing" to obtain the value of instrOrExpr.
     *
     * @return the TapList-tree of instrOrExpr's value.
     */
    private TapList zUBErec(Tree instrOrExpr, BoolVector usedZones,
                            Instruction instruction) {
        switch (instrOrExpr.opCode()) {
            case ILLang.op_assign: {
                zUBErec(instrOrExpr.down(1), usedZones, instruction);
                TapList rhsz = zUBErec(instrOrExpr.down(2), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rhsz, false), true);
                return rhsz;
            }
            case ILLang.op_return: {
                TapList rz = zUBErec(instrOrExpr.down(1), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                return rz;
            }
            case ILLang.op_ident:
                if (NewSymbolHolder.isANewSymbolRef(instrOrExpr)) {
                    return null;
                } else {
                    VariableDecl variableDecl =
                            getVariableDecl(ILUtils.getIdentString(instrOrExpr));
                    if (variableDecl == null)
                    // e.g. if instrOrExpr is a CONSTANT
                    {
                        return null;
                    } else {
                        return variableDecl.zones();
                    }
                }
            case ILLang.op_arrayAccess:
            case ILLang.op_iterativeVariableRef:
                zUBErec(instrOrExpr.down(2), usedZones, instruction);
                return zUBErec(instrOrExpr.down(1), usedZones, instruction);
            case ILLang.op_fieldAccess: {
                TapList rz = zUBErec(instrOrExpr.down(1), usedZones, instruction);
                int fieldRank = ILUtils.getFieldRank(instrOrExpr.down(2));
                if (fieldRank != -1 && rz != null && TapList.nth(rz, fieldRank) instanceof TapList) {
                    rz = (TapList) TapList.nth(rz, fieldRank);
                }
                return rz;
            }
            case ILLang.op_address:
                return new TapList<>(null,
                                     zUBErec(instrOrExpr.down(1), usedZones, instruction));

            case ILLang.op_pointerAccess: {
                TapList rz = zUBErec(instrOrExpr.down(2), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                rz = zUBErec(instrOrExpr.down(1), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                return treeOfZonesOfValue(instrOrExpr, null, instruction, null);
            }
            case ILLang.op_dimColon:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_complexConstructor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_power: {
                TapList rz = zUBErec(instrOrExpr.down(1), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                rz = zUBErec(instrOrExpr.down(2), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                return null;
            }
            case ILLang.op_ifExpression: {
                TapList rz = zUBErec(instrOrExpr.down(1), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                TapList rzTrue = zUBErec(instrOrExpr.down(2), usedZones, instruction);
                rz = zUBErec(instrOrExpr.down(3), usedZones, instruction);
                return TapList.cumulWithOper(TapList.copyTree(rz), rzTrue, SymbolTableConstants.CUMUL_OR);
            }
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_switchCase:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_sizeof: {
                TapList rz = zUBErec(instrOrExpr.down(1), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                return null;
            }
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_compGoto:
            case ILLang.op_unary: {
                TapList rz = zUBErec(instrOrExpr.down(2), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                return null;
            }
            case ILLang.op_switch: {
                TapList rz = zUBErec(instrOrExpr.down(1), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                zUBErec(instrOrExpr.down(2), usedZones, instruction);
                return null;
            }
            case ILLang.op_recordType:
            case ILLang.op_varDeclaration:
                zUBErec(instrOrExpr.down(2), usedZones, instruction);
                zUBErec(instrOrExpr.down(3), usedZones, instruction);
                return null;
            case ILLang.op_constDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_arrayDeclarator:
            case ILLang.op_modifiedType:
            case ILLang.op_arrayType:
            case ILLang.op_sizedDeclarator:
                zUBErec(instrOrExpr.down(1), usedZones, instruction);
                zUBErec(instrOrExpr.down(2), usedZones, instruction);
                return null;
            case ILLang.op_pointerType:
                zUBErec(instrOrExpr.down(1), usedZones, instruction);
                return null;
            case ILLang.op_switchCases:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_varDeclarations:
            case ILLang.op_declarations:
            case ILLang.op_declarators:
            case ILLang.op_dimColons: {
                Tree[] subtrees = instrOrExpr.children();
                for (int i = subtrees.length - 1; i >= 0; --i) {
                    zUBErec(subtrees[i], usedZones, instruction);
                }
                return null;
            }
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_label:
            case ILLang.op_star:
            case ILLang.op_allocate:
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_stop:
            case ILLang.op_none:
            case ILLang.op_strings:
            case ILLang.op_functionDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_intrinsic:
            case ILLang.op_concatIdent:
            case ILLang.op_external:
            case ILLang.op_interfaceDecl:
            case ILLang.op_continue:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_useDecl:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_parallelLoop:
            case ILLang.op_parallelRegion:
                return null;
            case ILLang.op_nameEq:
                return zUBErec(instrOrExpr.down(2), usedZones, instruction);
            case ILLang.op_loop:
                return zUBErec(instrOrExpr.down(3), usedZones, instruction);
            case ILLang.op_constructorCall:
                zUBErec(instrOrExpr.down(2), usedZones, instruction);
                return null;
            case ILLang.op_arrayTriplet:
            case ILLang.op_arrayConstructor:
            case ILLang.op_expressions:
            case ILLang.op_blockStatement:
            case ILLang.op_for:
            case ILLang.op_modifiers: {
                Tree[] expressions = instrOrExpr.children();
                TapList iz;
                for (int i = expressions.length - 1; i >= 0; --i) {
                    iz = zUBErec(expressions[i], usedZones, instruction);
                    usedZones.set(ZoneInfo.listAllZones(iz, false), true);
                }
                return null;
            }
            case ILLang.op_do: {
                TapList rz = zUBErec(instrOrExpr.down(1), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                rz = zUBErec(instrOrExpr.down(2), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                rz = zUBErec(instrOrExpr.down(3), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                rz = zUBErec(instrOrExpr.down(4), usedZones, instruction);
                usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                return null;
            }
            case ILLang.op_call: {
                Tree[] arguments = ILUtils.getArguments(instrOrExpr).children();
                TapList[] az = new TapList[arguments.length];
                // Arguments must be at least evaluated, even if they are not used by the call:
                for (int i = arguments.length - 1; i >= 0; --i) {
                    az[i] = zUBErec(arguments[i], usedZones, instruction);
                }
                // then the zones of the arguments are used if calledUnit.unitR says so:
                Unit calledUnit = DataFlowAnalyzer.getCalledUnit(instrOrExpr, this);
                CallArrow callArrow = null;
                if (calledUnit != null && calledUnit.unitInOutR != null) {
                    callArrow = CallGraph.getCallArrow(unit, calledUnit);
                }
                if (callArrow != null) {
                    // Use info on calledUnit to find exactly which zones are used:
                    BoolVector callUsedZones = new BoolVector(declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
                    DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyR(), callUsedZones,
                                                                   null, arguments, true,
                                                                   instrOrExpr, instruction, callArrow,
                                                                   SymbolTableConstants.ALLKIND,
                                                                   null, null, true, false) ;
                    usedZones.cumulOr(callUsedZones);
                } else {
                    // if info on calledUnit is missing, assume each argument is used:
                    for (int i = az.length - 1; i >= 0; --i) {
                        usedZones.set(ZoneInfo.listAllZones(az[i], true), true);
                    }
                }
                return null;
            }
            case ILLang.op_ioCall: {
                String ioAction = ILUtils.getIdentString(instrOrExpr.down(1));
                boolean writesInVars = ioAction.equals("read") ||
                        ioAction.equals("accept") || ioAction.equals("decode");
                Tree[] ioArgs = instrOrExpr.down(3).children();
                TapList rz;
                for (int i = ioArgs.length - 1; i >= 0; i--) {
                    rz = zUBErec(ioArgs[i], usedZones, instruction);
                    if (!writesInVars) {
                        usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                    }
                }
                Tree[] ioSpecs = instrOrExpr.down(2).children();
                ToBool totally = new ToBool(false);
                ToBool written = new ToBool(false);
                Tree ioSpecTree;
                for (int i = ioSpecs.length - 1; i >= 0; i--) {
                    ioSpecTree = InOutAnalyzer.getIOeffectOfIOspec(ioSpecs[i], i, ioAction,
                            written, totally, this);
                    rz = zUBErec(ioSpecTree, usedZones, instruction);
                    if (!written.get()) {
                        usedZones.set(ZoneInfo.listAllZones(rz, false), true);
                    }
                }
                return null;
            }
            default:
                TapEnv.toolWarning(-1, "(Zones used by expression) Unexpected operator: " + instrOrExpr.opName());
                return null;
        }
    }

    /**
     * @return the TapIntList of the "whichKind"-kind zones for all zones
     * in "zones" (ALLKIND declared zones) that are actually of type "whichKind".
     * [llh] TODO: This method shouldn't be used, it should be redesigned because for "tmp" zones,
     * there is no notion of whichKind-zones, so WHEN "withTmps" IS TRUE, we return the "zone"
     * itself, whatever "whichKind" is!! Also this method doesn't take into account side-effect zones.
     * It should be replaced by something like DataFlowAnalyzer.mapZoneRkToKindZoneRk(...).
     */
    public TapIntList getDeclaredKindZones(TapIntList zones, int whichKind, boolean withTmps) {
        TapIntList result = new TapIntList(-1, null);
        TapIntList tlResult = result;
        int kindZone;
        ZoneInfo zi;
        while (zones != null) {
            if (zones.head >= declaredZonesNb(SymbolTableConstants.ALLKIND)) {
                if (withTmps) {
                    tlResult = tlResult.placdl(zones.head);
                }
            } else {
                zi = declaredZoneInfo(zones.head, SymbolTableConstants.ALLKIND);
                if (zi != null) {
                    kindZone = zi.kindZoneNb(whichKind);
                    if (kindZone >= 0) {
                        tlResult = tlResult.placdl(kindZone);
                    }
                }
            }
            zones = zones.tail;
        }
        return result.tail;
    }

    /**
     * @return true when this "varTree" is an identifier which is
     * known as a function name (or a variable that holds a function name)
     * in the current symbolTable.
     */
    public boolean identIsAFunction(Tree varTree) {
        if (varTree.opCode() == ILLang.op_ident) {
            SymbolDecl varDecl = getSymbolDecl(ILUtils.getIdentString(varTree));
            return varDecl != null && varDecl.isA(SymbolTableConstants.FUNCTION);
        } else {
            return false;
        }
    }

    /**
     * @return true when this "varTree" is a reference to a variable
     * which is declared as "INTENT IN".
     */
    public boolean varIsIntentIn(Tree varTree) {
        if (ILUtils.isAVarRef(varTree, this)) {
            VariableDecl variableDecl = getVariableDecl(ILUtils.baseName(varTree));
            return variableDecl != null && variableDecl.hasModifier("in");
        } else {
            return false;
        }
    }

    /**
     * @return true if write reference "tree1" completely hides "tree2".
     * Assumes tree1 and tree2 refer to a common zone.
     */
    public boolean hidesRef(Tree tree1, Tree tree2) {
        return false; /*TODO: refine...*/
    }

    /**
     * @return the symbol declarations in the top level of this SymbolTable.
     */
    public TapList<SymbolDecl> getAllTopSymbolDecls() {
        return getAllDecls(SymbolTableConstants.SYMBOL, true);
    }

    /**
     * @return the variable declarations in the top level of this SymbolTable.
     */
    public TapList<SymbolDecl> getAllTopVariableDecls() {
        return getAllDecls(SymbolTableConstants.VARIABLE, true);
    }

    /**
     * @return the variable declarations from this SymbolTable.
     */
    public TapList<SymbolDecl> getAllVariableDecls() {
        return getAllDecls(SymbolTableConstants.VARIABLE, false);
    }

    /**
     * @return the all type declarations in this SymbolTable.
     */
    public TapList<SymbolDecl> getAllTypeDecls() {
        return getAllDecls(SymbolTableConstants.TYPE, false);
    }

    public VariableDecl getVariableDeclOfRank(int rank) {
        TapList<SymbolDecl> allVarDecls = getAllTopVariableDecls();
        VariableDecl result = null;
        while (allVarDecls != null && result == null) {
            if (((VariableDecl) allVarDecls.head).formalArgRank == rank) {
                result = (VariableDecl) allVarDecls.head;
            }
            allVarDecls = allVarDecls.tail;
        }
        return result;
    }

    private TapList<SymbolDecl> getAllDecls(int kind, boolean top) {
        SymbolTable curSymbolTable = this;
        TapList<SymbolDecl> resList = new TapList<>(null, null);
        TapList<SymbolDecl> tailRes = resList;
        int i;
        TapList<SymbolDecl> curBucket;
        /*iterate on the nested enclosing SymbolTable's */
        while (curSymbolTable != null) {
            for (i = 0; i < NUM_BUCKETS; i++) {
                curBucket = curSymbolTable.buckets[i].tail;
                while (curBucket != null) {
                    if (curBucket.head.isA(kind)) {
                        tailRes = tailRes.placdl(curBucket.head);
                    }
                    curBucket = curBucket.tail;
                }
            }
            if (top) {
                // if recursion in enclosing SymbolTables is forbidden:
                curSymbolTable = null;
            } else {
                curSymbolTable = curSymbolTable.basisSymbolTable();
            }
        }
        return resList.tail;
    }

    /**
     * @return the function declarations in the top level of this SymbolTable.
     */
    public TapList<SymbolDecl> getAllTopFunctionDecls() {
        return getAllDecls(SymbolTableConstants.FUNCTION, true);
    }

    /**
     * @return the FunctionDecl's declarations of this SymbolTable that have an interface.
     */
    public TapList<FunctionDecl> getAllInterfacedFunctionDecls() {
        TapList<SymbolDecl> intfs = getAllDecls(SymbolTableConstants.INTERFACE, true);
        TapList functions = getAllDecls(SymbolTableConstants.FUNCTION, true);
        FunctionDecl funcDecl;
        InterfaceDecl interfaceDecl;
        while (intfs != null) {
            if (intfs.head instanceof FunctionDecl) {
                funcDecl = (FunctionDecl) intfs.head;
                if (funcDecl.hasInterfaceInstruction()) {
                    functions = new TapList<>(funcDecl, functions);
                }
            } else {
                interfaceDecl = (InterfaceDecl) intfs.head;
                functions = TapList.append(interfaceDecl.functionDecls, functions);
            }
            intfs = intfs.tail;
        }
        return functions;
    }

    /**
     * @return all SymbolDecl's that are declared as TARGET
     * and whose type matches "typeSpec".
     * If typeSpec == null, any type matches.
     */
    private TapList<SymbolDecl> getAllTargetDecls(WrapperTypeSpec typeSpec) {
        SymbolTable curSymbolTable = this;
        TapList<SymbolDecl> resList = new TapList<>(null, null);
        TapList<SymbolDecl> tailRes = resList;
        int i;
        TapList<SymbolDecl> curBucket;
        SymbolDecl symbolDecl;
        /*iterate on the nested enclosing SymbolTable's */
        while (curSymbolTable != null) {
            for (i = 0; i < NUM_BUCKETS; i++) {
                curBucket = curSymbolTable.buckets[i].tail;
                while (curBucket != null) {
                    symbolDecl = curBucket.head;
                    if (symbolDecl != null && (!TapEnv.isFortran9x(language) || symbolDecl.isTarget(typeSpec))) {
                        tailRes = tailRes.placdl(symbolDecl);
                    }
                    curBucket = curBucket.tail;
                }
            }
            curSymbolTable = curSymbolTable.basisSymbolTable();
        }
        return resList.tail;
    }

    protected void allocateZones(ZoneAllocator zoneAllocator, Unit declarationUnit) {
        TapList<SymbolDecl> curBucket;
        TapList regionZones;
        for (int i = 0; i < NUM_BUCKETS; i++) {
            curBucket = buckets[i].tail;
            while (curBucket != null) {
                if (curBucket.head.isA(SymbolTableConstants.VARIABLE)) {
                    VariableDecl variableDecl = (VariableDecl) curBucket.head;
                    if (!variableDecl.isSystemPredefined()
                            && variableDecl.type() != null
                            && !TypeSpec.isA(variableDecl.type(), SymbolTableConstants.LABELTYPE)) {
			variableDecl.allocateZones(zoneAllocator, this, declarationUnit) ;
                    }
                } else if (curBucket.head.isA(SymbolTableConstants.FUNCTION)) {
                    FunctionDecl functionDecl = (FunctionDecl) curBucket.head;
                    if (functionDecl.isVarFunction()) {
                        // int argRank = functionDecl.formalArgRank;
			            functionDecl.allocateZones(zoneAllocator, this, declarationUnit) ;
                    }
                } else if (curBucket.head.isA(SymbolTableConstants.INTERFACE)) {
                    InterfaceDecl interfaceDecl = (InterfaceDecl) curBucket.head;
		    FunctionDecl functionDecl = interfaceDecl.functionDecl();
                    if (isFormalParams && unit.isStandard() && functionDecl!=null) {
                        // This is an interface but also a function passed as argument.
			functionDecl.allocateZones(zoneAllocator, this, declarationUnit) ;
                    }
                }
                curBucket = curBucket.tail;
            }
        }
    }

    protected Object staticValue(Tree tree) {
        Object valueAnnotation = tree.getAnnotation("staticValue");
        if (valueAnnotation == null) {
            valueAnnotation = doCompute(tree);
            tree.setAnnotation("staticValue", valueAnnotation);
        }
        return valueAnnotation;
    }

    private Object doCompute(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue();
            case ILLang.op_stringCst:
                return tree.stringValue();
            case ILLang.op_realCst:
                return null;
            case ILLang.op_ident: {
                String name = ILUtils.getIdentString(tree);
                VariableDecl variableDecl = getVariableOrConstantDecl(name);
                if (variableDecl != null && variableDecl.kind == SymbolTableConstants.CONSTANT &&
                        variableDecl.initializationTree() != null) {
                    return doCompute(variableDecl.initializationTree());
                } else {
                    return null;
                }
            }
            case ILLang.op_eq: {
                Object valueLeft = doCompute(tree.down(1));
                Object valueRight = doCompute(tree.down(2));
                if (valueLeft != null &&
                        valueRight != null) {
                    return valueLeft.equals(valueRight);
                } else {
                    return null;
                }
            }
            default:
                return null;
        }
    }

    public Integer computeIntConstant(Tree tree) {
        Integer i1;
        Integer i2;
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue();
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_strings:
            case ILLang.op_realCst:
            case ILLang.op_bitCst:
            case ILLang.op_boolCst:
            case ILLang.op_expressions:
            case ILLang.op_multiCast:
            case ILLang.op_arrayConstructor:
            case ILLang.op_star:
            case ILLang.op_sizeof:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_complexConstructor:
            case ILLang.op_concat:
            case ILLang.op_none:
            case ILLang.op_address:
            case ILLang.op_ifExpression:
            case ILLang.op_concatIdent:
            case ILLang.op_ioCall:
                return null;
            case ILLang.op_ident: {
                //[llh] not sure this may happen, but we protect here against
                // op_ident's that in fact contain a numeric constant:
                String sval = tree.stringValue();
                int ival = -1;
                boolean wasAnInteger;
                try {
                    ival = Integer.parseInt(sval);
                    wasAnInteger = true;
                } catch (NumberFormatException e) {
                    wasAnInteger = false;
                }
                if (wasAnInteger) {
                    return ival;
                } else {
                    VariableDecl variableDecl = getVariableOrConstantDecl(tree.stringValue());
                    if (variableDecl != null && variableDecl.constantValue != null) {
                        return variableDecl.constantValue;
                    } else {
                        return null;
                    }
                }
            }
            case ILLang.op_add: {
                i1 = computeIntConstant(tree.down(1));
                i2 = computeIntConstant(tree.down(2));
                if (i1 != null && i2 != null) {
                    return i1 + i2;
                } else {
                    return null;
                }
            }
            case ILLang.op_sub: {
                i1 = computeIntConstant(tree.down(1));
                i2 = computeIntConstant(tree.down(2));
                if (i1 != null && i2 != null) {
                    return i1 - i2;
                } else {
                    return null;
                }
            }
            case ILLang.op_mul: {
                i1 = computeIntConstant(tree.down(1));
                i2 = computeIntConstant(tree.down(2));
                if (i1 != null && i2 != null) {
                    return i1 * i2;
                } else {
                    return null;
                }
            }
            case ILLang.op_minus: {
                i1 = computeIntConstant(tree.down(1));
                if (i1 != null) {
                    return -i1;
                } else {
                    return null;
                }
            }
            case ILLang.op_div: {
                i1 = computeIntConstant(tree.down(1));
                i2 = computeIntConstant(tree.down(2));
                if (i1 != null && i2 != null && i2 != 0) {
                    return i1 / i2;
                } else {
                    return null;
                }
            }
            case ILLang.op_power:
                i1 = computeIntConstant(tree.down(1));
                i2 = computeIntConstant(tree.down(2));
                if (i1 != null && i2 != null) {
                    return (int) Math.pow(i1, i2);
                } else {
                    return null;
                }
            case ILLang.op_nameEq:
            case ILLang.op_cast:
                return computeIntConstant(tree.down(2));
            /* vmp: on peut avoir un arrayAccess a la place d'un op_call
             * si on a une function intrinsic non declaree
             * par exemple kind(1)
             */
            case ILLang.op_arrayAccess:
                // At this stage, in Fortran, some function calls may still be mistaken as array accesses:
                // Take care of this for the important case of "kind()" and "size()" :
                if ((unit == null || unit.isFortran()) &&
                        ILUtils.isIdentOf(tree.down(1), new String[]{"kind", "size"}, false)) {
                    return computeIntConstant(ILUtils.buildCall(null,
                            ILUtils.copy(tree.down(1)),
                            ILUtils.copy(tree.down(2))));
                } else {
                    return null;
                }
            case ILLang.op_call: {
                //TODO: On ne traite pas pour le moment l'appel a une fonction dans la
                // definition d'une dimension
                // par exemple en fortran 90 DIMENSION(SIZE(Mat,1)):: MatInv
                Unit calledUnit = DataFlowAnalyzer.getCalledUnit(tree, this);
                if (calledUnit != null && calledUnit.isIntrinsic() && calledUnit.name().equals("kind")) {
                    WrapperTypeSpec typeSpec = this.typeOf(ILUtils.getArguments(tree).down(1));
                    if (typeSpec != null) {
                        return typeSpec.size();
                    }
                }
                return null;
            }
            default:
                TapEnv.fileWarning(TapEnv.MSG_WARN, tree, "(Compute constant) Unexpected operator: " + tree.opName());
                return null;
        }
    }

    /**
     * @return true when expressions "exp1" and "exp2" can be proved to return equal values.
     */
    public boolean equalValues(Tree exp1, Tree exp2) {
        if (exp1.equalsTree(exp2)) {
            return true;
        } else {
            Integer val1 = computeIntConstant(exp1);
            Integer val2 = computeIntConstant(exp2);
            return val1 != null && val2 != null
                    && val1.intValue() == val2.intValue();
        }
    }

    public final SymbolTable getExistingCopy(TapList<TapPair<SymbolTable, SymbolTable>> associationsST) {
        return TapList.cassq(this, associationsST);
    }

    public SymbolTable getExistingOrig(TapList<TapPair<SymbolTable, SymbolTable>> associationsST) {
        TapPair<SymbolTable, SymbolTable> foundPair = TapList.rassq(this, associationsST);
        return foundPair == null ? null : foundPair.first;
    }

    /**
     * @return a copy of this SymbolTable.
     * When the basis SymbolTable has also been copied,
     * the basis of the copy is set to the copy of the basis.
     * Similarly for copied CallGraph and Unit's.
     */
    public SymbolTable smartCopy(TapList<TapPair<SymbolTable, SymbolTable>> associationsST,
				 Object targetUnitOrCG, CallGraph targetCG, UnitStorage<Unit> associationsUnits, String copyPrefix) {
// System.out.println("SMARTCOPY OF "+shortName+" "+addressChain()) ;
// TapList<TapPair<SymbolTable, SymbolTable>> inassocs = associationsST ;
// if (inassocs.head==null) inassocs = inassocs.tail ;
// while (inassocs!=null) {
//     System.out.println("     KNOWING "+inassocs.head.first.addressChain()+" COPIED AS "+inassocs.head.second.addressChain()) ;
//     inassocs = inassocs.tail ;
// }
        SymbolTable copyST = this.getExistingCopy(associationsST);
        if (copyST == null) {
            SymbolTable copyBasisST = null;
            if (basisSymbolTable != null) {
                Object basisTargetUnitOrCG = targetUnitOrCG;
                if (basisSymbolTable.unit != this.unit && targetUnitOrCG instanceof Unit) {
                    basisTargetUnitOrCG = ((Unit) targetUnitOrCG).upperLevelUnit();
                    if (basisTargetUnitOrCG == null) {
                        basisTargetUnitOrCG = targetCG;
                    }
                }
                copyBasisST =
                        basisSymbolTable.smartCopy(associationsST, basisTargetUnitOrCG, targetCG, associationsUnits, copyPrefix);
            }
            copyST = copy(associationsST, targetUnitOrCG, associationsUnits, copyPrefix);
            copyST.setBasisSymbolTable(copyBasisST);
// System.out.println("  BUILT "+copyST.addressChain()) ;
// } else {
// System.out.println("  FOUND "+copyST.addressChain()) ;
        }
        copyST.rootCallGraph = targetCG;
        return copyST;
    }

    /** Show for trace the associations context from original SymbolTable's to (smart)copied SymbolTable's */
    public static void traceShowAssociationsST(TapList<TapPair<SymbolTable,SymbolTable>> associationsST) {
        if (associationsST!=null && associationsST.head==null) associationsST = associationsST.tail ;
        while (associationsST!=null) {
            TapEnv.printlnOnTrace("      "+associationsST.head.first) ;
            TapEnv.printlnOnTrace("      ====>> "+associationsST.head.second) ;
            associationsST = associationsST.tail ;
        }
    }

    /**
     * Build a new SymbolTable, copy of this SymbolTable.
     * When associationsST==null, it means that the copied ST will live in the
     * same CallGraph as this SymbolTable. Otherwise the copied ST will live in
     * a different CallGraph and the copy must be deeper.
     */
    private SymbolTable copy(TapList<TapPair<SymbolTable, SymbolTable>> associationsST, Object targetUnitOrCG,
                             UnitStorage<Unit> associationsUnits, String copyPrefix) {
        SymbolTable copyST = new SymbolTable(null);
        if (associationsST != null) {
            associationsST.placdl(new TapPair<>(this, copyST));
        }
        copyST.setShortName(copyPrefix+this.shortName);
        copyST.language = language;
        copyST.implicits = implicits;
        copyST.caseDependent = caseDependent;
        copyST.isFormalParams = isFormalParams;
        copyST.dataList = this.dataList;
        copyST.isImports = this.isImports ;
        copyST.isPublic = this.isPublic ;
        Unit targetUnit = (targetUnitOrCG instanceof Unit ? (Unit) targetUnitOrCG : null);
        if (associationsST == null) {
            copyST.inheritedClass = inheritedClass;
            copyST.unit = unit;
        } else {
            copyST.unit = targetUnit;
        }

        if (this.isTranslationUnitSymbolTable()) {
            copyST.setIsTranslationUnit();
        }

        /* firstDeclaredZone est un tableau pour ALLKIND, REALKIND, INTKIND et PTRKIND */
        if (associationsST == null) {
            copyST.firstDeclaredZone = this.firstDeclaredZone;
            copyST.freeDeclaredZone = this.freeDeclaredZone;
            copyST.additionalDuplicatedDeclaredZoneInfos = this.additionalDuplicatedDeclaredZoneInfos;
        }
        copyST.declaredZoneInfos = this.declaredZoneInfos;
        if (declarationsBlock != null) {
            Block copyDeclarationsBlock = null;
            // Find if the orig declarationsBlock has already been copied in the targetUnit:
            if (targetUnit != null) {
                if (targetUnit.orig2copiedBlocks != null && declarationsBlock.rank >= -1) {
                    copyDeclarationsBlock =
                            targetUnit.orig2copiedBlocks.retrieve(declarationsBlock);
                }
                if (copyDeclarationsBlock == null
                        && targetUnit.origPublicSTDeclarationBlock == declarationsBlock) {
                    copyDeclarationsBlock = targetUnit.copyPublicSTDeclarationBlock;
                }
            }
            // if not already copied, create a copy now:
            if (copyDeclarationsBlock == null) {
                copyDeclarationsBlock = new BasicBlock(copyST, declarationsBlock.parallelControls, null);
                copyDeclarationsBlock.rank = -81;
                // pour garder les declarations rajoutees par fixImplicit
                // dans la publicSymbolTable parce que son declarationsBlock
                // n'est pas dans le flowGraph
                if (this.isFormalParams
                        || this.basisSymbolTable != null
                        && this.basisSymbolTable.isFormalParams
                        && this.unit.isModule())
                // 2nd argument should be the global association list of orig-to-copied include instructions:
                {
                    copyDeclarationsBlock.copyInstructions(declarationsBlock, new TapList<>(null, null));
                }
            }
            copyST.declarationsBlock = copyDeclarationsBlock;
        }
        SymbolDecl oldSymbolDecl;
        SymbolDecl newSymbolDecl;
        TapList<SymbolDecl> oldBucket;
        TapList<SymbolDecl> copyBucket;
        //First pass: copy every Bucket. If associationsST!=null, which means that
        // the copied ST will live in a different CallGraph than the original ST "this",
        // then copy each SymbolDecl inside. Else keep the original SymbolDecl.
        for (int i = 0; i < NUM_BUCKETS; i++) {
            // Each Bucket always starts with a null element :
            oldBucket = buckets[i].tail;
            copyBucket = new TapList<>(null, null);
            copyST.buckets[i] = copyBucket;
            while (oldBucket != null) {
                oldSymbolDecl = oldBucket.head;
                if (associationsST != null) {
		    if (oldSymbolDecl.copySymbolDecl != null) {
			// if oldSymbolDecl appeared in another SymbolTable and was
			//  therefore already copied, share the same copy
			newSymbolDecl = oldSymbolDecl.copySymbolDecl ;
		    } else {
			newSymbolDecl = oldSymbolDecl.copy(associationsST, associationsUnits);
		    }
                } else {
                    newSymbolDecl = oldSymbolDecl;
                }
                copyBucket = copyBucket.placdl(newSymbolDecl);
                oldBucket = oldBucket.tail;
            }
        }

        //Second pass: copy various links between copied SymbolDecl's
        if (associationsST != null) {
            for (int i = 0; i < NUM_BUCKETS; i++) {
                oldBucket = buckets[i].tail;
                copyBucket = copyST.buckets[i].tail;
                while (oldBucket != null) {
                    oldBucket.head.copyLinksIntoCopy(copyBucket.head, associationsST);
                    oldBucket = oldBucket.tail;
                    copyBucket = copyBucket.tail;
                }
            }
        }

        // Necessary only because Activity has already created
        // differentiated NewSymbolHolders for types: when this
        // is done later, during differentiatiation, this call
        // to migrateToCopySymbolTable() will become useless !
        if (this.newSymbolHolders.tail != null) {
            TapList<NewSymbolHolder> newSHs = this.newSymbolHolders.tail;
            while (newSHs != null) {
                newSHs.head.migrateToCopySymbolTable(copyST);
                newSHs = newSHs.tail;
            }
        }
        return copyST;
    }

    /** At the end of copying for differentiation, update the "importedFrom" pointers from
     * each imported SymbolDecl to the original SymbolDecl that it imports.
     */
    public void updateImportedFroms(TapList<TapPair<SymbolTable, SymbolTable>> associationsST) {
	TapList<SymbolDecl> bucket;
	SymbolDecl symbolDecl ;
	TapPair<SymbolDecl, SymbolTable> origImportedFrom ;
// System.out.println("  UPDATEIMPORTEDFROMS IN "+this) ;
// System.out.println("  KNOWING ASSOCIATIONST:");
// SymbolTable.traceShowAssociationsST(associationsST) ;
        for (int i = 0; i < NUM_BUCKETS; i++) {
            bucket = buckets[i].tail;
            while (bucket != null) {
		symbolDecl = bucket.head ;
		origImportedFrom = symbolDecl.importedFrom ;
		if (origImportedFrom!=null) {
// System.out.println("  UPDATEIMPORTEDFROMS OF "+symbolDecl+" IMPORTED FROM "+origImportedFrom) ;
                    SymbolDecl copyImportedDecl = origImportedFrom.first.copySymbolDecl ;
                    // Special case e.g. for TypeDecl's :
                    if (copyImportedDecl==null) copyImportedDecl = origImportedFrom.first ;
                    SymbolTable copyImportST = TapList.cassq(origImportedFrom.second, associationsST) ;
                    // Special case, symbolDecl's importedFrom make have been already adapted to the new symbolTables:
                    if (copyImportST==null) copyImportST = origImportedFrom.second ;
		    symbolDecl.importedFrom = new TapPair<>(copyImportedDecl, copyImportST) ;
// System.out.println("     RESULTS IN "+symbolDecl) ;
		}
                bucket = bucket.tail;
            }
        }
    }

    /**
     * After copying, cleans the association between origin SymbolDecl's and their copies.
     */
    public void cleanCopySymbolDecls() {
        TapList<SymbolDecl> bucket;
        for (int i = 0; i < NUM_BUCKETS; i++) {
            bucket = buckets[i].tail;
            while (bucket != null) {
                bucket.head.copySymbolDecl = null;
                bucket = bucket.tail;
            }
        }
    }

    protected WrapperTypeSpec findCompositeTypeSpec(String name) {
        WrapperTypeSpec result = null;
        TapList allVars = getAllTopVariableDecls();
        VariableDecl var;
        WrapperTypeSpec typeSpec;
        while (allVars != null && result == null) {
            var = (VariableDecl) allVars.head;
            typeSpec = var.type();
            if (TypeSpec.isA(typeSpec, SymbolTableConstants.COMPOSITETYPE)
                    && ((CompositeTypeSpec) typeSpec.wrappedType).name != null
                    && ((CompositeTypeSpec) typeSpec.wrappedType).name.equals(name)) {
                result = typeSpec;
            }
            allVars = allVars.tail;
        }
        return result;
    }

    /**
     * For debugging, returns the chain of hashcodes of the SymbolTable's
     * from this to root SymbolTable.
     */
    public String addressChain() {
        String result = "@" + Integer.toHexString(hashCode());
        if (basisSymbolTable != null) {
            result = result + "==>" + basisSymbolTable.addressChain();
        }
        return result;
    }

    protected void checkTypeSpecValidity() {
        // Terminate the circular building of dangling circular types:
        while (danglingCircularTypes != null) {
            WrapperTypeSpec danglingType = danglingCircularTypes.head;
            CompositeTypeSpec tempType = (CompositeTypeSpec) danglingType.wrappedType;
            String typeName = tempType.name;
            String explicitTypeName = (tempType.isRecordType() ? "struct" : "union") + " _" + typeName;
            TypeDecl typeDecl = getTypeDecl(typeName);
            if (typeDecl == null) {
                typeDecl = getTypeDecl(explicitTypeName);
            }
            TypeSpec actualType =
                    (typeDecl != null ? typeDecl.typeSpec : findCompositeTypeSpec(typeName));
            while (actualType != null && actualType instanceof WrapperTypeSpec) {
                actualType = ((WrapperTypeSpec) actualType).wrappedType();
            }
            if (actualType != null) {
                danglingType.wrappedType = actualType;
            }
            danglingCircularTypes = danglingCircularTypes.tail;
        }
        for (int j = buckets.length - 1; j >= 0; --j) {
            TapList<SymbolDecl> bucket = buckets[j];
            while (bucket != null) {
                SymbolDecl symbolDecl = bucket.head;
                WrapperTypeSpec typeSpec = null;
                if (symbolDecl != null) {
                    switch (symbolDecl.kind) {
                        case SymbolTableConstants.FUNCTION: {
                            typeSpec = ((FunctionDecl) symbolDecl).type();
                            break;
                        }
                        case SymbolTableConstants.FIELD:
                        case SymbolTableConstants.FUNCNAME:
                        case SymbolTableConstants.VARIABLE:
                        case SymbolTableConstants.CONSTANT: {
                            typeSpec = symbolDecl.type();
                            break;
                        }
                        case SymbolTableConstants.TYPE: {
                            typeSpec = ((TypeDecl) symbolDecl).typeSpec;
                            break;
                        }
                        case SymbolTableConstants.METHOD:
                        case SymbolTableConstants.INTERFACE:
                        case SymbolTableConstants.NAMESPACE:
                        case SymbolTableConstants.CLASS: {
                            // if it makes sense, we must first define this sort of SymbolDecl !
                            break;
                        }
                        default: {
                            TapEnv.toolWarning(-1, "(checkTypeSpecValidity) Not yet implemented " + symbolDecl + " " + symbolDecl.kind);
                            break;
                        }
                    }
                    if (typeSpec != null) {
                        typeSpec.checkTypeSpecValidity(null);
                    }
                }
                bucket = bucket.tail;
            }
        }
    }

    // Heuristic code that, given a zone "hiddenZoneInfo" which is hidden and therefore not accessible
    // from this current scope, tries to find a zone which is accessible from this current scope and that
    // is in fact equivalent (i.e. has the same memory location). */
    public ZoneInfo findProxyForHiddenZone(ZoneInfo hiddenZoneInfo, Instruction instr, boolean upstream) {
        // If no pointer analysis was done, return null:
        if (instr==null || instr.block==null || instr.block.pointerInfosIn==null) return null ;
        ZoneInfo equivZoneInfo = null ;
        ZoneInfo visibleZoneInfo ;
        TapIntList seenZones ;
        for (int i=declaredZonesNb(SymbolTableConstants.ALLKIND)-1 ; i>=0 && equivZoneInfo==null ; --i) {
            visibleZoneInfo = declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
            if (!visibleZoneInfo.comesFromAllocate() && visibleZoneInfo.accessTree!=null && !visibleZoneInfo.isHidden) {
                seenZones = listOfZonesOfValue(visibleZoneInfo.accessTree, null, instr); //TODO: use the "upstream" info!!
                if (seenZones!=null && seenZones.tail==null && seenZones.head==hiddenZoneInfo.zoneNb)
                    equivZoneInfo = visibleZoneInfo ;
            }
        }
        return equivZoneInfo ;
    }

    protected SymbolTable findAssocAddresTypeDeclSymbolTable() {
        int lang = (unit == null ? language : unit.language());
        if (lang == -1) {
            return null;
        }
        return TapEnv.assocAddressDiffTypesUnit(lang).publicSymbolTable();
    }

    /**
     * Prints in detail the contents of this SymbolTable, onto TapEnv.curOutputStream().
     *
     * @param indent the amount of indentation to be used for this printing.
     */
    public void dump(int indent) throws java.io.IOException {
        if (unit != null) {
            TapEnv.pushRelatedUnit(unit);
        }
        TapEnv.indentPrintln(indent, addressChain()+", Case dependent:"+caseDependent+" isImports:"+isImports);
        TapList<SymbolDecl> symbolDecls = getAllTopSymbolDecls();
        while (symbolDecls != null) {
            TapEnv.indentPrint(indent, "- ");
            symbolDecls.head.dump(0);
            TapEnv.println();
            symbolDecls = symbolDecls.tail;
        }
        if (newSymbolHolders.tail != null) {
            TapEnv.indentPrint(indent, "NEW SYMBOLS WAITING FOR SOLVE:" + newSymbolHolders.tail);
        }
        if (nameListList != null) {
            TapEnv.indentPrintln(indent + 4, "-----NameList:------");
            TapEnv.indentPrint(indent + 4, nameListList.toString());
        }
        ZoneInfo[] zoneInfos = declaredZoneInfos[SymbolTableConstants.ALLKIND];
        int offset = firstDeclaredZone(SymbolTableConstants.ALLKIND);
        if (zoneInfos!=null) {
            TapEnv.indentPrintln(indent+4, "-----Zones:------ ["
                                 +firstDeclaredZone[0]+" "+firstDeclaredZone[1]+" "+firstDeclaredZone[2]+" "+firstDeclaredZone[3]+" ; "
                                 +freeDeclaredZone[0]+" "+freeDeclaredZone[1]+" "+freeDeclaredZone[2]+" "+freeDeclaredZone[3]+"[");
            for (int i = 0; i < zoneInfos.length; ++i) {
                TapEnv.indentPrintln(indent+6, TapEnv.str3(i+offset)+"> "
                                     +(zoneInfos[i]!=null ? zoneInfos[i] : " null, meaning hidden")) ;
            }
        }
        if (additionalDuplicatedDeclaredZoneInfos != null
                && additionalDuplicatedDeclaredZoneInfos[SymbolTableConstants.ALLKIND] != null) {
            TapEnv.indentPrintln(indent+4, "-----Additional Zones:------");
            TapList<Int2ZoneInfo> additionalZoneInfos = additionalDuplicatedDeclaredZoneInfos[SymbolTableConstants.ALLKIND];
            while (additionalZoneInfos != null) {
                TapEnv.indentPrintln(indent+6, TapEnv.str3(additionalZoneInfos.head.getZoneNumber())+"> "
                                     +additionalZoneInfos.head.getZoneInfo());
                additionalZoneInfos = additionalZoneInfos.tail;
            }
        }
        if (waitingZoneInfos!=null) {
            TapEnv.indentPrintln(indent+4, "-----ZoneInfos waiting for registration:-----");
            TapList<ZoneInfo> inWaitingZoneInfos = waitingZoneInfos ;
            while (inWaitingZoneInfos!=null) {
                TapEnv.indentPrintln(indent+6, TapEnv.str3(inWaitingZoneInfos.head.zoneNb)+"> "
                                     +inWaitingZoneInfos.head) ;
                inWaitingZoneInfos = inWaitingZoneInfos.tail ;
            }
        }
        TapEnv.indentPrintln(indent+4, "-----DeclBlock:------"
                             +(declarationsBlock==null ? " null." : declarationsBlock+" (@"+Integer.toHexString(declarationsBlock.hashCode())+')')) ;
        if (declarationsBlock != null) {
            TapEnv.indentPrintln(indent+8, (declarationsBlock.instructions == null ? "empty" : declarationsBlock.instructions.toString()));
        }
        SymbolTable basisST = basisSymbolTable();
        if (basisST != null) {
            TapEnv.indentPrintln(indent, "--------------");
            if (TapEnv.getSeenSymbolTableShortName(basisST) != null) {
                TapEnv.indentPrint(indent, basisST.shortName());
                //TapEnv.print(" (@"+Integer.toHexString(basisST.hashCode())+")") ;
            } else {
                basisST.dump(indent);
            }
        }
        if (unit != null) {
            TapEnv.popRelatedUnit();
        }
    }

    public void dumpDeclaredZones(String stRanks) throws java.io.IOException {
        ZoneInfo[] zoneInfos = declaredZoneInfos[SymbolTableConstants.ALLKIND];
        ZoneInfo zoneInfo;
        TapEnv.indentPrintln(2, "Declared zones of " + shortName() + ':');
        if (zoneInfos != null && zoneInfos.length != 0) {
            for (ZoneInfo info : zoneInfos) {
                zoneInfo = info;
                if (zoneInfo != null) {
                    TapEnv.indentPrint(6, stRanks + ":a" + zoneInfo.zoneNb);
                    if (zoneInfo.realZoneNb != -1) {
                        TapEnv.print("r" + zoneInfo.realZoneNb);
                    }
                    if (zoneInfo.intZoneNb != -1) {
                        TapEnv.print("i" + zoneInfo.intZoneNb);
                    }
                    if (zoneInfo.ptrZoneNb != -1) {
                        TapEnv.print("p" + zoneInfo.ptrZoneNb);
                    }
                    TapEnv.print(" : ");
                    zoneInfo.dump();
                    TapEnv.println();
                }
            }
        } else {
            TapEnv.indentPrintln(6, "empty");
        }
    }

    @Override
    public String toString() {
        String result;
        result = "SymbolTable[" + addressChain() + "]:" + shortName();
        /*
         if (unit!=null) {
             result = result+" (enclosingUnit "+unit.toString() ;
             if (origUnit()!=null) result = result+" (from orig unit "+origUnit().toString()+") " ;
             result = result+") " ;
         }
         TapList<SymbolDecl> curBucket;
         for (int i = 0; i < NUM_BUCKETS; i++) {
             curBucket = buckets[i].tail;
             while (curBucket != null) {
                 result = result+","+curBucket.head.toString();
                 curBucket = curBucket.tail;
             }
         }
        */
        return result;
    }
}
