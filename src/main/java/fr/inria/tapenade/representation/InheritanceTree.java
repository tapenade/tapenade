/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

public class InheritanceTree {

    /**
     * in case of simple inheritance, only one child.
     */
    public TapList<InheritanceTree> children;
    public TapList<InheritanceTree> virtualChildren;
    /**
     * inherited class unit  for the current symbol.
     */
    public Unit currentUnit;
    /**
     * Unit where the symbol was declared.
     */
    public Unit declarationUnit;

    public static InheritanceTree copy(InheritanceTree inheritanceTree) {
        InheritanceTree copy = new InheritanceTree();
        copy.children = inheritanceTree.children;
        copy.virtualChildren = inheritanceTree.virtualChildren;
        copy.currentUnit = inheritanceTree.currentUnit;
        copy.declarationUnit = inheritanceTree.declarationUnit;
        return copy;
    }

    public String getName() {
        return currentUnit == null ? "" : currentUnit.name();
    }

    public void addChild(InheritanceTree newChild, boolean virtual) {
        if (newChild != null) {
            if (virtual) {
                virtualChildren = new TapList<>(newChild, virtualChildren);
            } else {
                children = new TapList<>(newChild, children);
            }
        }
    }
}
