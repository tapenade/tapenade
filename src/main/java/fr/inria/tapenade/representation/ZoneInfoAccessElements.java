/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/**
 * Collection of infos that will be needed for building ZoneInfo's during zone allocation.
 */
public final class ZoneInfoAccessElements {
    public SymbolDecl symbolDecl ;
    public Tree accessTree ;
    public SymbolTable symbolTable ;
    public TapList<TapPair<Tree, Tree>> accessIndexes = null ;

    /** A temporary storage useful to build the accessTree when an array is split into pieces
     * e.g. by an EQUIVALENCE or by a different COMMON layout */
    public int startOffsetInArray = -1 ;
    public int baseSize = -1 ;

    public ZoneInfoAccessElements(SymbolDecl symbolDecl, Tree accessTree, SymbolTable symbolTable) {
        this.symbolDecl = symbolDecl ;
        this.accessTree = accessTree ;
        this.symbolTable = symbolTable ;
    }

    public ZoneInfoAccessElements buildIndex(ArrayTypeSpec arrayType) {
        Tree indexedAccessTree ;
        TapList<TapPair<Tree, Tree>> indexedAccessIndexes = null;
        ArrayDim[] typeDimensions = arrayType.dimensions();
        if (accessTree != null &&
            accessTree.opCode() == ILLang.op_arrayAccess) {
            if (accessTree.down(2).children().length >= typeDimensions.length) {
                // When accessTree is an arrayAccess and accessType is an ARRAYTYPE,
                // e.g. when accessTree is an array section A(i,10:20,T(2:8),:)
                // thus of dimension [10:20,2:8,*] */
                Tree[] indexTrees = accessTree.down(2).children();
                for (int i=indexTrees.length-1 ; i>=0 ; --i) {
                    TapList<Tree> arrayTriplets = ILUtils.arrayTripletsInside(indexTrees[i]);
                    if (arrayTriplets != null) {
                        indexedAccessIndexes =
                            new TapList<>(new TapPair<>(arrayTriplets.head, null), indexedAccessIndexes);
                    }
                }
            } else {
                // This case arises in EQUIVALENCEs, when a new arrayAccess has been built to
                // access just a subpart of the original variable (of type arrayType).
                // We choose to do nothing and keep the new arrayAccess
            }
            indexedAccessTree = accessTree ;
        } else {
            Tree indexTree;
            TapList<Tree> arrayIndexes = null;
            for (int i = typeDimensions.length - 1; i >= 0; i--) {
                indexTree = typeDimensions[i].tree();
                // Turn a dimColon into an arrayTriplet:
                indexTree = ILUtils.build(ILLang.op_arrayTriplet,
                              ILUtils.copy(indexTree.down(1)),
                              ILUtils.copy(indexTree.down(2)),
                              ILUtils.build(ILLang.op_none));
                arrayIndexes = new TapList<>(indexTree, arrayIndexes);
                indexedAccessIndexes =
                    new TapList<>(new TapPair<>(indexTree, null), indexedAccessIndexes);
            }
            boolean inAllocate = (accessTree!=null && accessTree.opCode()==ILLang.op_allocate) ;
            if (inAllocate) {
                indexedAccessTree = ILUtils.copy(accessTree) ;
//                 Tree subTree = indexedAccessTree.cutChild(1);
//                 subTree = ILUtils.build(ILLang.op_arrayAccess,
//                             subTree,
//                             ILUtils.build(ILLang.op_expressions, arrayIndexes));
//                 indexedAccessTree.setChild(subTree, 1);
            } else {
                indexedAccessTree = ILUtils.build(ILLang.op_arrayAccess,
                                  ILUtils.copy(accessTree),
                                  ILUtils.build(ILLang.op_expressions, arrayIndexes));
            }
        }
        ZoneInfoAccessElements result =
            new ZoneInfoAccessElements(symbolDecl, indexedAccessTree, symbolTable) ;
        result.accessIndexes = indexedAccessIndexes ;
        return result ;
    }

    public ZoneInfoAccessElements buildField(FieldDecl field, int i) {
        Tree fieldAccessTree = null ;
        TapList<TapPair<Tree, Tree>> fieldAccessIndexes = accessIndexes;
        if (accessTree != null) {
            TapPair<Tree, TapList<TapPair<Tree, Tree>>> copyTreeIndexes =
                ILUtils.copyWatchingSubtrees(accessTree, fieldAccessIndexes);
            fieldAccessTree = copyTreeIndexes.first;
            fieldAccessIndexes = copyTreeIndexes.second;
        }
        fieldAccessTree = ILUtils.build(ILLang.op_fieldAccess,
                            fieldAccessTree,
                            ILUtils.build(ILLang.op_ident, field.symbol)) ;
        ILUtils.setFieldRank(fieldAccessTree.down(2), i);
        ZoneInfoAccessElements result =
            new ZoneInfoAccessElements(symbolDecl, fieldAccessTree, symbolTable) ;
        result.accessIndexes = fieldAccessIndexes ;
        return result ;

    }

    public ZoneInfoAccessElements buildDeref(PointerTypeSpec pointerType) {
        Tree derefAccessTree =
            ILUtils.build(ILLang.op_pointerAccess,
              ILUtils.copy(accessTree),
              (pointerType.offsetLength == null ?
               ILUtils.build(ILLang.op_none) :
               ILUtils.copy(pointerType.offsetLength.tree()))) ;
        TapList<TapPair<Tree, Tree>> derefAccessIndexes = accessIndexes ;
        if (pointerType.offsetLength != null) {
            derefAccessIndexes =
                TapList.append(derefAccessIndexes,
                               new TapList<>(new TapPair<>(pointerType.offsetLength.tree(), null), null));
        }
        ZoneInfoAccessElements result =
            new ZoneInfoAccessElements(symbolDecl, derefAccessTree, symbolTable) ;
        result.accessIndexes = derefAccessIndexes ;
        return result ;
    }

    @Override
    public String toString() {
        return symbolDecl+" :: "+ILUtils.toString(accessTree)+" ("+startOffsetInArray+" "+baseSize+") FOR ST:"+symbolTable ;
    }
}
