/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/**
 * Memory map manager. Deals with the relative layout of variables in memory.
 * In particular, accepts the declaration of a new COMMON block,
 * or the declaration that some variables are EQUIVALENCEd, DATA, or SAVEd.
 */
public final class MemoryMaps {

    /** The (hatted) list of memory maps built so far */
    public TapList<MemMap> maps = new TapList<>(null, null) ;

    /** Fast access from a VariableDecl to its start AlignmentBoundary in one of the maps.
     * This is a hatted list for easy add/remove of entries */
    public TapList<VarStartBoundary> mapAccesses = new TapList<>(null, null) ;

    /** Finds or creates the MemMap with name "commonName" in the current maps.
     * When commonName is given null, always creates a new MemMap, even if maps
     * already contains other MemMap's with a null name. */
    public MemMap getSetMemMap(String commonName) {
        MemMap result = getMemMap(commonName) ;
        if (result==null) {
            result = new MemMap(commonName) ;
            maps.placdl(result) ;
        }
        return result ;
    }

    public MemMap getMemMap(String commonName) {
        if (commonName!=null) {
            TapList<MemMap> inMaps = maps ;
            while (inMaps.tail!=null) {
                if (commonName.equals(inMaps.tail.head.name))
                    return inMaps.tail.head ;
                inMaps = inMaps.tail ;
            }
        }
        return null ;
    }

    /** Adds the new "entry" into this MemoryMap's mapAccesses */
    public void addRegistration(VarStartBoundary entry) {
        mapAccesses.tail = new TapList<>(entry, mapAccesses.tail) ;
    }

    /** Retrieves the (first) entry associated to the given "symbolDecl" in this MemoryMap's mapAccesses */
    public VarStartBoundary getRegistration(SymbolDecl symbolDecl) {
        TapList<VarStartBoundary> inMapAccesses = mapAccesses.tail ;
        VarStartBoundary entry = null ;
        while (entry==null && inMapAccesses!=null) {
            if (inMapAccesses.head.variableDecl==symbolDecl) entry = inMapAccesses.head ;
            inMapAccesses = inMapAccesses.tail ;
        }
        return entry ;
    }

    /** Retrieves the (first) entry associated to the given "symbolDecl" in this MemoryMap's mapAccesses.
     * If the entry is found, remove it from this MemoryMap's mapAccesses */
    public VarStartBoundary delRegistration(SymbolDecl symbolDecl) {
        TapList<VarStartBoundary> inMapAccesses = mapAccesses ;
        VarStartBoundary entry = null ;
        while (entry==null && inMapAccesses.tail!=null) {
            if (inMapAccesses.tail.head.variableDecl==symbolDecl) {
                entry = inMapAccesses.tail.head ;
                inMapAccesses.tail = inMapAccesses.tail.tail ;
            } else {
                inMapAccesses = inMapAccesses.tail ;
            }
        }
        return entry ;
    }

    /** Place into the MemoryMaps the given memory layout "declaration",
     * which appeared in the context of the given "declSymbolTable".
     * "declaration" may be a COMMON, EQUIVALENCE, SAVE, or DATA declaration.
     * The info may end up into this Unit's local maps (if it is purely an EQUIVALENCE)
     * or into the CallGraph's global maps (if it is a COMMON, SAVE, DATA, or BIND(C) declaration. */
    public static void placeCEDSIntoMemoryMaps(Tree declaration, SymbolTable declSymbolTable, Unit declUnit,
                                               MemoryMaps unitMaps, CallGraph callGraph) {
        switch (declaration.opCode()) {
            case ILLang.op_common: {
                Tree commonNameTree = declaration.down(1) ;
                String commonName = (ILUtils.isNullOrNone(commonNameTree) ? "" : commonNameTree.stringValue()) ;
                placeCommonIntoMemoryMap(declaration, commonName,
                                         declSymbolTable, declUnit, unitMaps, callGraph.globalFortranMaps);
                break;
            }
            case ILLang.op_equivalence:
                placeEquivalenceIntoMemoryMap(declaration.children(),
                                              declSymbolTable, declUnit, unitMaps, callGraph.globalFortranMaps);
                break;
            case ILLang.op_save:
            case ILLang.op_data:
//                 if (unit==null || !unit.isModule()) {
//                     // ILUtils.copy cf set11/v02, op_iterativeVariableRef
                placeDSIntoMemoryMap(declaration,
                                     declSymbolTable, declUnit, unitMaps, callGraph.globalFortranMaps);
//                 }
                break ;
            case ILLang.op_accessDecl:  // this case is for BIND(C) declarations
                if (ILUtils.isIdent(declaration.down(1).down(1), "bind", false)) {
                    placeBindCIntoMemoryMap(declaration.down(1).down(2), declaration.down(2),
                                            declSymbolTable, declUnit, unitMaps, callGraph);
                }
                break ;
        }
    }

    /** Place into the MemoryMaps the layout of the given COMMON declaration "commonNameTree":"commonVars". */
    private static void placeCommonIntoMemoryMap(Tree declaration, String commonName, SymbolTable declSymbolTable,
                                                 Unit declUnit, MemoryMaps unitMaps, MemoryMaps globalMaps) {
        MemMap commonMap = globalMaps.getSetMemMap(commonName) ;
        Tree[] commonVars = declaration.down(2).children() ;
        String varName;
        VariableDecl varDecl;
//         WrapperTypeSpec baseTypeSpec = null;
        int varSize;
        int offset = 0 ;
        boolean infiniteEndOffset = false ;
        for (int i=0 ; i<commonVars.length ; ++i) {
            varName = ILUtils.baseName(commonVars[i]);
            varDecl = declSymbolTable.getVariableDecl(varName);
            varSize = ((varDecl!=null && varDecl.type()!=null) ? varDecl.type().size() : -1) ;
//             varSize = -1;
//             if (varDecl!=null && varDecl.type()!=null) {
//                 varSize = varDecl.type().size();
// // [llh] Comprends pas a quoi ca sert ?
// //                 if (baseTypeSpec == null) {
// //                     baseTypeSpec = varDecl.type().baseTypeSpec(true);
// //                 } else {
// //                     baseTypeSpec = declSymbolTable.getTypeDecl("float").typeSpec.
// //                             conformingTypeSpec(baseTypeSpec,
// //                                     varDecl.type().baseTypeSpec(true));
// //                 }
//             }
            if (varSize < 0) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, declaration, "Cannot compute the size of variable " + varName + " in common " + commonName);
                infiniteEndOffset = true ;
            } else {
                VarStartBoundary previousRegistration ;
                if (unitMaps!=null
                    && (previousRegistration = unitMaps.getRegistration(varDecl)) != null) {
                    // varDecl is already registered in an EQUIVALENCE MemMap of the Unit. Merge the two maps:
                    commonMap.absorb(previousRegistration.map, previousRegistration.boundary.offset-offset, unitMaps, globalMaps) ;
                } else if ((previousRegistration = globalMaps.getRegistration(varDecl)) != null) {
                    // varDecl is already registered in a global MemMap, because it appeared in another common:
                    if (previousRegistration.map.name.startsWith("/Fortran_Save_")) {
                        previousRegistration.map.removeVariableFrom(varDecl, globalMaps) ;
                        commonMap.insertVariableAt(offset, varSize, varDecl, declSymbolTable, globalMaps) ;
                    } else {
                        TapEnv.fileError(commonVars[i],
                                     "(TC28) Variable "+varDecl.symbol+" cannot be added to common "+commonName
                                     +" because it is already in common "+previousRegistration.map.name) ;
                    }
                } else {
                    // (standard case:) first time varDecl is registered in some MemMaps:
                    commonMap.insertVariableAt(offset, varSize, varDecl, declSymbolTable, globalMaps) ;
                }
                offset += varSize ;
            }
        }
        ToBool toCommonMapInfiniteEndOffset = new ToBool(false) ;
        int commonMapLastOffset = commonMap.getLastOffset(toCommonMapInfiniteEndOffset) ;
        if (!infiniteEndOffset && !toCommonMapInfiniteEndOffset.get() && commonMapLastOffset!=offset) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC29) Common "+commonName+" declared with two different sizes: here "+offset+" vs "+commonMapLastOffset+" elsewhere") ;
        }
//         if (declaration.down(1).opCode() != ILLang.op_none) {
//             addBindCDecl(declSymbolTable, declaration.down(1));
//         }
    }

    private static void placeBindCIntoMemoryMap(Tree bindExprs, Tree localExprs, SymbolTable declSymbolTable,
                                                Unit declUnit, MemoryMaps unitMaps, CallGraph callGraph) {
        MemoryMaps globalFortranMaps = callGraph.globalFortranMaps ;
        String bindLang = ILUtils.getIdentString(bindExprs.down(1)) ; //So far, assume it is always C.
        SymbolTable bindSymbolTable = callGraph.languageRootSymbolTable(TapEnv.C) ;
        Tree localExpr = localExprs.down(1) ;
        String localName = ILUtils.getIdentString(localExpr) ;
        boolean localNameIsCommon = (localName.startsWith("/") && localName.endsWith("/")) ;
        // Find the (C) VariableDecl to bind with:
        Tree bindExpr = ILUtils.getNamedElement(bindExprs, "name") ;
        if (bindExpr==null && bindExprs.length()>=2) {
            bindExpr = bindExprs.down(2) ;
        }
        String bindName = (bindExpr==null ? localName : bindExpr.stringValue()) ;
        VariableDecl bindDecl = bindSymbolTable.getVariableDecl(bindName);
// System.out.println("PLACEBINDCINTOMEMORYMAP "+bindExprs) ;
// System.out.println("  (C) BINDEXPR:"+bindExpr+" name:"+bindName+" decl:"+bindDecl) ;
// System.out.println("  (F)LOCALEXPR:"+localExpr+" name:"+localName) ;
        if (bindDecl==null) {
            TapEnv.fileError(bindExprs, "(TCxx) No C variable named "+bindName+" to bind with") ;
        } else {

            TapList<ZoneInfoAccessElements> bindDeclCMap = callGraph.getCglobalMap(bindDecl) ;
            // Find or create the (Fortran) MemMap that will receive the (C) map to bind with:
            MemMap receivingMap = null ;
            int receivingOffset = 0 ;
            if (localNameIsCommon) {
                receivingMap = globalFortranMaps.getSetMemMap(localName) ;
                receivingOffset = 0 ;
            } else {
                VariableDecl localDecl = declSymbolTable.getVariableDecl(localName) ;
                if (localDecl==null) {
                    TapEnv.fileError(localExprs, "(TCxx) No Fortran variable named "+localName+" to bind") ;
                } else {
                    VarStartBoundary registration0 = (unitMaps==null ? null : unitMaps.getRegistration(localDecl)) ;
                    VarStartBoundary registration0g = (registration0==null ? globalFortranMaps.getRegistration(localDecl) : null) ;
                    if (registration0g==null) { // Because of the present "bind", Fortran symbol must be registered globally
                        MemMap newMap = globalFortranMaps.getSetMemMap(null) ;
                        if (registration0!=null) {
                            newMap.absorb(registration0.map, 0, unitMaps, globalFortranMaps) ;
                        } else {
                            int localSize = (localDecl.type()!=null ? localDecl.type().size() : -1) ;
                            if (localSize<0) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, localExprs, "Cannot compute the size of variable " + localName) ;
                                localSize = 0 ;
                            }
                            newMap.insertVariableAt(0, localSize, localDecl, declSymbolTable, globalFortranMaps) ;
                        }
                        registration0g = globalFortranMaps.getRegistration(localDecl) ;
                    }
                    receivingMap = registration0g.map ;
                    receivingOffset = registration0g.boundary.offset ;
                }
            }
            if (receivingMap!=null) {
                while (bindDeclCMap!=null) {
                    ZoneInfoAccessElements cAccess = bindDeclCMap.head ;

                    placeCAccessIntoFortranMap(cAccess, receivingMap, bindName, receivingOffset, globalFortranMaps) ;

                    bindDeclCMap = bindDeclCMap.tail ;
                }
            }
        }
    }

    public static void placeCAccessIntoFortranMap(ZoneInfoAccessElements cAccess, MemMap receivingMap, String bindName, int receivingOffset, MemoryMaps globalFortranMaps) {
        VariableDecl oneCdecl = (VariableDecl)cAccess.symbolDecl ;
        SymbolTable oneCaccessSymbolTable = cAccess.symbolTable ;
        TapList<TapPair<Integer,SubVariableDecl>> toParts = new TapList<>(null, null) ;
        splitStructs(oneCdecl.type(), bindName, ILUtils.build(ILLang.op_ident, bindName), null,
                     toParts, oneCdecl, oneCaccessSymbolTable) ;
        toParts = toParts.tail;
        int offsetInMap = receivingOffset ;
        while (toParts!=null) {
            SubVariableDecl subBindDecl = toParts.head.second ;
            int subBindSize = toParts.head.first.intValue() ;
            receivingMap.insertVariableAt(offsetInMap, subBindSize, subBindDecl,
                                          oneCaccessSymbolTable, globalFortranMaps) ;
            offsetInMap += subBindSize ;
            toParts = toParts.tail ;
        }
    }

    private static TapList<TapPair<Integer,SubVariableDecl>> splitStructs(TypeSpec type, String name, Tree accessTree, TapIntList revItinerary,
                       TapList<TapPair<Integer,SubVariableDecl>> tlParts, VariableDecl varDecl, SymbolTable symbolTable) {
        TapList<TapPair<Integer,SubVariableDecl>> hdResult = new TapList<>(null, null) ;
        TapList<TapPair<Integer,SubVariableDecl>> tlResult = hdResult ;
        switch (type.kind()) {
            case SymbolTableConstants.WRAPPERTYPE: {
                tlParts = splitStructs(((WrapperTypeSpec)type).wrappedType, name, accessTree, revItinerary, tlParts, varDecl, symbolTable) ;
                break ;
            }
            case SymbolTableConstants.ARRAYTYPE:  // ARRAYTYPE case is dubious, but should not happen in C anyway!
            case SymbolTableConstants.MODIFIEDTYPE: {
                tlParts = splitStructs(type.elementType(), name, accessTree, revItinerary, tlParts, varDecl, symbolTable) ;
                break ;
            }
            case SymbolTableConstants.COMPOSITETYPE: {
                CompositeTypeSpec compositeTypeSpec = (CompositeTypeSpec)type;
                for (int i=0 ; i<compositeTypeSpec.fields.length ; ++i) {
                    FieldDecl fieldDecl = compositeTypeSpec.fields[i];
                    Tree fieldTree = ILUtils.build(ILLang.op_ident, fieldDecl.symbol) ;
                    ILUtils.setFieldRank(fieldTree, i) ;
                    tlParts = splitStructs(fieldDecl.type(), name+"%"+fieldDecl.symbol,
                                           ILUtils.build(ILLang.op_fieldAccess,
                                             ILUtils.copy(accessTree),
                                             fieldTree),
                                           new TapIntList(i, revItinerary),
                                           tlParts, varDecl, symbolTable) ;
                }
                break ;
            }
            default: {
                int size = type.size() ;
                Tree dummyNameTree = ILUtils.build(ILLang.op_ident, name) ;
                SubVariableDecl dummyVarDecl =
                    new SubVariableDecl(dummyNameTree, new WrapperTypeSpec(type),
                                        varDecl, TapIntList.reverse(revItinerary), accessTree) ;                    
                tlParts = tlParts.placdl(new TapPair<>(size, dummyVarDecl)) ;
            }
        }
        return tlParts ;
    }


    /** Place into the MemoryMaps the layout of the given EQUIVALENCE declaration "equivalenceDecl". */
    private static void placeEquivalenceIntoMemoryMap(Tree[] equivalenceDecls, SymbolTable declSymbolTable,
                                                      Unit declUnit, MemoryMaps unitMaps, MemoryMaps globalMaps) {
        Tree[] equivSet;
        Tree lastChild;
        VariableDecl varDecl0, varDecl1;
        int i, j;
        int varSize0, varSize1;
        int offset0, offset1;
        VarStartBoundary registration0, registration0g, registration1, registration1g ;
        boolean success;

        for (i = equivalenceDecls.length - 1; i >= 0; i--) {
            equivSet = equivalenceDecls[i].children();
            lastChild = equivSet[equivSet.length - 1];
            varDecl0 = declSymbolTable.getVariableDecl(ILUtils.baseName(lastChild));
            varSize0 = ((varDecl0==null || varDecl0.type()==null) ? -1 : varDecl0.type().size()) ;
            offset0 = declSymbolTable.refOffset(lastChild);
            registration0 = (unitMaps==null ? null : unitMaps.getRegistration(varDecl0)) ;
            registration0g = (registration0==null ? globalMaps.getRegistration(varDecl0) : null) ;
            for (j = equivSet.length - 2; j >= 0; j--) {
                varDecl1 = declSymbolTable.getVariableDecl(ILUtils.baseName(equivSet[j]));
                varSize1 = ((varDecl1==null || varDecl1.type()==null) ? -1 : varDecl1.type().size()) ;
                offset1 = declSymbolTable.refOffset(equivSet[j]);
                registration1 = (unitMaps==null ? null : unitMaps.getRegistration(varDecl1)) ;
                registration1g = (registration1==null ? globalMaps.getRegistration(varDecl1) : null) ;
                if (registration0!=null) {
                    if (registration1!=null) {
                        if (registration0.map != registration1.map) {
                            registration0.map.absorb(registration1.map,
                                (registration1.boundary.offset+offset1)-(registration0.boundary.offset+offset0),
                                unitMaps, unitMaps) ;
                        }
                    } else if (registration1g!=null) {
                        registration1g.map.absorb(registration0.map,
                            (registration0.boundary.offset+offset0)-(registration1g.boundary.offset+offset1),
                            unitMaps, globalMaps) ;
                    } else {
                        registration0.map.insertVariableAt(registration0.boundary.offset+offset0-offset1,
                                                             varSize1, varDecl1, declSymbolTable, unitMaps) ;
                    }
                } else if (registration0g!=null) {
                    if (registration1!=null) {
                        registration0g.map.absorb(registration1.map,
                            (registration1.boundary.offset+offset1)-(registration0g.boundary.offset+offset0),
                            unitMaps, globalMaps) ;
                    } else if (registration1g!=null) {
                        if (registration0g.map != registration1g.map) {
                            registration0g.map.absorb(registration1g.map,
                                (registration1g.boundary.offset+offset1)-(registration0g.boundary.offset+offset0),
                                globalMaps, globalMaps) ;
                        }
                    } else {
                        registration0g.map.insertVariableAt(registration0g.boundary.offset+offset0-offset1,
                                                              varSize1, varDecl1, declSymbolTable, globalMaps) ;
                    }
                } else {
                    if (registration1!=null) {
                        registration1.map.insertVariableAt(registration1.boundary.offset+offset1-offset0,
                                                             varSize0, varDecl0, declSymbolTable, unitMaps) ;
                    } else if (registration1g!=null) {
                        registration1g.map.insertVariableAt(registration1g.boundary.offset+offset1-offset0,
                                                             varSize0, varDecl0, declSymbolTable, globalMaps) ;
                    } else {
                        MemMap newMap = unitMaps.getSetMemMap(null) ;
                        newMap.insertVariableAt(-offset0,  varSize0, varDecl0, declSymbolTable, unitMaps) ;
                        newMap.insertVariableAt(-offset1,  varSize1, varDecl1, declSymbolTable, unitMaps) ;
                    }
                }
            }
        }
    }

    /** Place into the MemoryMaps an automatic pseudo-COMMON declaration for
     * each variable in the op_save or op_data declaration "dsTree".
     * Note: in general, this will create one different pseudo-COMMON per SAVE variable,
     * albeit with the same pseudo-COMMON name */
    private static void placeDSIntoMemoryMap(Tree dsTree, SymbolTable declSymbolTable,
                                             Unit declUnit, MemoryMaps unitMaps, MemoryMaps globalMaps) {
        Tree[] savedVars ;
        if (dsTree.opCode() == ILLang.op_data) {
            Tree[] dataElems = dsTree.down(1).children();
            savedVars = new Tree[dataElems.length] ;
            for (int i=dataElems.length-1 ; i>=0 ; --i) {
                savedVars[i] = ILUtils.baseTree(dataElems[i]);
            }
        } else {
            savedVars = dsTree.children();
        }
        String varName;
        VariableDecl varDecl;
        int varSize;
        for (int i=savedVars.length-1 ; i>=0 ; --i) {
            varName = savedVars[i].stringValue();
            varDecl = declSymbolTable.getVariableDecl(varName);
            if (varDecl!=null) {
                varSize = ((varDecl!=null && varDecl.type()!=null) ? varDecl.type().size() : -1) ;
                // no save on an "automatic object"
                if (varSize < 0) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, dsTree, "Cannot compute the size of variable "+varName+" in "+ILUtils.toString(dsTree)) ;
                    varSize = 9;
                }
                if (varDecl != null) {
                    VarStartBoundary previousRegistration ;
                    if (unitMaps!=null &&
                        (previousRegistration = unitMaps.getRegistration(varDecl)) != null) {
                        // varDecl is already registered in an EQUIVALENCE MemMap of the Unit.
                        // Create a new pseudo-COMMON (empty) and merge the EQUIVALENCE MemMap into it:
                        MemMap commonMap = globalMaps.getSetMemMap(null) ;
                        commonMap.name = "/Fortran_Save"+(declUnit==null?"":"_In_"+declUnit.name()+"/") ;
                        commonMap.absorb(previousRegistration.map, 0, unitMaps, globalMaps) ;
                    } else if ((previousRegistration = globalMaps.getRegistration(varDecl)) != null) {
                        // varDecl is already registered in a global MemMap, because it appeared in another common.
                        // Then do nothing, as the variable is already global and therefore saved.
                    } else {
                        // (standard case:) first time varDecl is registered in some MemMaps:
                        MemMap commonMap = globalMaps.getSetMemMap(null) ;
                        commonMap.name = "/Fortran_Save"+(declUnit==null?"":"_In_"+declUnit.name()+"/") ;
                        commonMap.insertVariableAt(0, varSize, varDecl, declSymbolTable, globalMaps) ;
                    }
                }
            }
        }
    }

    public void allocateZones(ZoneAllocator zoneAllocator, Unit declarationUnit) {
        TapList<MemMap> memMaps = maps ;
        while (memMaps.tail!=null) {
            if (!zoneAllocator.targetSymbolTable.isGlobalSymbolTable()
                && !memMaps.tail.head.touchesSymbolTable(zoneAllocator.targetSymbolTable)) {
                // if we are on a localMaps, and all the uses of a MemMap it contains are in a deeper scope,
                // then defer zone allocation till we allocate zones for this deeper scope.
                memMaps = memMaps.tail ;
            } else {
                memMaps.tail.head.allocateZones(zoneAllocator, declarationUnit) ;
                memMaps.tail = memMaps.tail.tail ;
            }
        }
    }

    public String toString() {
        String result = "" ;
        TapList<MemMap> inMaps = maps.tail ;
        while (inMaps!=null) {
            result = result+"\n        -- "+inMaps.head ;
            inMaps = inMaps.tail ;
        }
        return "MemoryMaps:"+result ;
    }

}
