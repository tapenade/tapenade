/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Tapenade directives, mostly about AD.
 */
public class Directive {

    /**
     * Prefix that identifies all AD directives.
     */
    private static final String AD_DIRECTIVE_HEAD = "$AD ";

    /**
     * Independent-Iterations parallel loops.
     */
    public static final int IILOOP = 13;

    /**
     * Time-stepping loops to be adjoined with binomial checkpointing.
     */
    public static final int BINOMIALCKP = 15;

    /**
     * Start of a piece of code to be checkpointed.
     */
    public static final int CHECKPOINTSTART = 16;

    /**
     * End of a piece of code to be checkpointed.
     */
    public static final int CHECKPOINTEND = 17;

    /**
     * Subroutine to be checkpointed.
     */
    public static final int CHECKPOINT = 11;

    /**
     * Subroutine NOT to be checkpointed.
     */
    public static final int NOCHECKPOINT = 12;

    /**
     * Subroutine for which there may be
     * specialized differentiations for each activity pattern.
     */
    public static final int SPECIALIZEACTIVITY = 27;

    /**
     * Start of a piece of code NOT to be differentiated.
     */
    public static final int NODIFFSTART = 21;

    /**
     * End of a piece of code NOT to be differentiated.
     */
    public static final int NODIFFEND = 22;

    /**
     * Piece of code containing a Fixed Point loop.
     */
    public static final int FIXEDPOINTLOOP = 23;

    /**
     * Start of a piece of code identified with a label
     */
    public static final int LABEL_START = 28;

    /**
     * End of a piece of code identified with a label
     */
    public static final int LABEL_END = 29;

    /**
     * Trigger debug AD at the current point.
     */
    public static final int DEBUGHERE = 18;

    /**
     * Trigger debug AD of the current call.
     */
    public static final int DEBUGCALL = 19;

    /**
     * Associate a channel name to a MessagePassing call.
     */
    private static final int MESSAGE_PASSING = 20;

    /**
     * Specify a list of independent (active input) variables  (TODO).
     */
    private static final int INDEPENDENT = 1;
    /**
     * Specify a list of dependent (active output) variables  (TODO).
     */
    private static final int DEPENDENT = 2;
    /**
     * Specify a list of dead variables  (TODO).
     */
    private static final int DEADVARIABLE = 3;
    /**
     * Specify a list of dead-adjoint variables  (TODO).
     */
    private static final int DEADADJOINTVARIABLE = 4;
    /**
     * Specify a list of variables NOT to snapshot  (TODO).
     */
    private static final int NOSNAPSHOT = 5;
    /**
     * Specify a list of variables to be passivated at this point  (TODO).
     */
    private static final int PASSIVEVARIABLE = 7;
    /**
     * Specify a list of IN (i.e. read) variables  (TODO).
     */
    private static final int IN = 9;
    /**
     * Specify a list of OUT (i.e. overwritten) variables  (TODO).
     */
    private static final int OUT = 10;

    /**
     * Specify a piece of code that should be made a procedure ("outlined") (TODO).
     */
    private static final int PROCEDURE = 30;
    /**
     * Specify the name, and regex of variables to RESET, or not to reset.
     * ? cf Benjamin's save-restore
     */
    private static final int RESET = 14;
    /**
     * Specify the name of the original line number for the next instruction.
     */
    private static final int LINE = 15;
    /**
     * Directive that overrides multithread memory layout decision for tangent differentiated variables
     */
    public static final int MULTITHREAD_TGT = 25;
    /**
     * Directive that overrides multithread memory layout decision for adjoint differentiated variables
     */
    public static final int MULTITHREAD_ADJ = 26;
    /**
     * Other directive regarding multithread (not used yet?)
     */
    private static final int MULTITHREAD = 24;

    private static final Pattern splitPattern = Pattern.compile("( +,? *|, *)");
    /**
     * Directive String (all characters after $AD).
     */
    private String directiveCommand;
    /**
     * Type of Directive.
     */
    public int type;
    /**
     * Arguments linked to a directive.
     */
    public Tree[] arguments;
    /**
     * name of a part, procedure.
     */
    public String label;

    /**
     * For stopping criterion of adjoint fixed-point loops
     */
    public Float adjResidual = null ;  //replaces old "accuracy"
    public Float adjReduction = null ;

    /**
     * Constructor.
     *
     * @param directiveType Directive Type.
     */
    public Directive(int directiveType) {
        super();
        type = directiveType;
    }

    /**
     * @return true if some of the given comments trees are indeed directives.
     */
    protected static boolean containDirectives(TapList<Tree> commentsList) {
        boolean found = false;
        while (commentsList != null && !found) {
            found = getDirectiveText(commentsList.head) != null;
            commentsList = commentsList.tail;
        }
        return found;
    }

    /**
     * @return true if some contents of the given commentTree are indeed directives.
     */
    protected static boolean containDirectives(Tree commentTree) {
        if (commentTree == null) {
            return false;
        }
        boolean found = false;
        Tree[] comments = commentTree.children();
        for (int i = comments.length - 1; i >= 0 && !found; --i) {
            found = getDirectiveText(comments[i]) != null;
        }
        return found;
    }

    /**
     * @return a "kind" directive in a list of Directive's.
     */
    public static Directive hasDirective(TapList<Directive> directives, int kind) {
        Directive directive;
        while (directives != null) {
            directive = directives.head;
            if (directive.type == kind) {
                return directive;
            }
            directives = directives.tail;
        }
        return null;
    }

    /**
     * @return a "kind" directive with the given "label" in a list of Directive's.
     */
    public static Directive hasDirective(TapList<Directive> directives, int kind, String label) {
        Directive directive;
        while (directives != null) {
            directive = directives.head;
            if (directive!=null && directive.type == kind && (directive.label==null ? label==null : directive.label.equals(label))) {
                return directive;
            }
            directives = directives.tail;
        }
        return null;
    }

    public static int startKind(int endKind) {
        switch (endKind) {
            case Directive.CHECKPOINTEND:
                return Directive.CHECKPOINTSTART ;
            case Directive.NODIFFEND:
                return Directive.NODIFFSTART ;
            case Directive.LABEL_END:
                return Directive.LABEL_START ;
            default:
                return -1 ;
        }
    }

    /**
     * Analyze the comments attached to the header of "unit", to find the AD directives.
     */
    protected static void analyzeUnitDirectives(Unit unit, Tree unitComments) {
        if (unit.hasSource() && unitComments != null) {
            Tree comment;
            String directiveText;
            String directiveTextUC;
            Directive node;
            TapList<Tree> directivesToCut = null;
            Instruction headerInstruction = unit.entryBlock().headInstr();
            Tree[] comments = unitComments.children();
            for (Tree tree : comments) {
                comment = tree;
                directiveText = getDirectiveText(comment);
                if (directiveText != null) {
                    // remove initial white spaces:
                    while (directiveText.startsWith(" ")) {
                        directiveText = directiveText.substring(1);
                    }
                    directiveTextUC = directiveText.toUpperCase();
                    node = null;
                    if (directiveTextUC.startsWith("INDEPENDENT ")) {
                        node = buildIndependent(directiveText.substring(12));
                    } else if (directiveTextUC.startsWith("IN ")) {
                        node = buildWithArgs(Directive.IN, directiveText.substring(3));
                    } else if (directiveTextUC.startsWith("OUT ")) {
                        node = buildWithArgs(Directive.OUT, directiveText.substring(4));
                    } else if (directiveTextUC.startsWith("NOSNAPSHOT")) {
                        node = buildWithArgs(Directive.NOSNAPSHOT, directiveText.substring(10));
                    } else if (directiveTextUC.startsWith("SPECIALIZEACTIVITY")) {
                        node = buildAtomic(Directive.SPECIALIZEACTIVITY);
                    } else if (directiveTextUC.startsWith("CHECKPOINT")) {
                        node = buildCheckpoint(headerInstruction);
                        unit.maybeNoCheckpointed = false;
                        unit.maybeCheckpointed = true;
                    } else if (directiveTextUC.startsWith("NOCHECKPOINT")) {
                        node = buildNoCheckpoint(headerInstruction);
                        unit.maybeNoCheckpointed = true;
                        unit.maybeCheckpointed = false;
                    } else if (directiveTextUC.startsWith("RESET ")) {
                        node = buildSaveRestoreReset(directiveText.substring(6));
                    } else if (directiveTextUC.startsWith("MULTITHREAD ") || directiveTextUC.startsWith("MULTITHREAD_")) {
                        String subText = directiveText.substring(12);
                        // remove initial white spaces:
                        while (subText.startsWith(" ")) {
                            subText = subText.substring(1);
                        }
                        String subTextUC = subText.toUpperCase();
                        if (subTextUC.startsWith("TGT ")) {
                            node = buildWithMultithreadScopings(Directive.MULTITHREAD_TGT, subText.substring(4));
                        } else if (subTextUC.startsWith("TANGENT ")) {
                            node = buildWithMultithreadScopings(Directive.MULTITHREAD_TGT, subText.substring(8));
                        } else if (subTextUC.startsWith("ADJ ")) {
                            node = buildWithMultithreadScopings(Directive.MULTITHREAD_ADJ, subText.substring(4));
                        } else if (subTextUC.startsWith("ADJOINT ")) {
                            node = buildWithMultithreadScopings(Directive.MULTITHREAD_ADJ, subText.substring(8));
                        } else {
                            node = buildWithArgs(Directive.MULTITHREAD, subText);
                        }
                    }
                    if (node != null) {
                        directivesToCut = new TapList<>(comment, directivesToCut);
                        node.directiveCommand = directiveText;
                        unit.directives = new TapList<>(node, unit.directives);
                    }
                }
            }
            while (directivesToCut != null) {
                comment = directivesToCut.head;
                unitComments.removeChild(comment.rankInParent());
                directivesToCut = directivesToCut.tail;
            }
        }
    }

    /**
     * Analyze the comments attached to the "instruction", to find the AD directives.
     */
    protected static void analyzeInstructionDirectives(Instruction instruction, Tree instructionComments) {
        if (instructionComments != null) {
            String directiveText;
            String directiveTextUC;
            Directive node;
            TapList<Tree> directivesToCut = null;
            Tree[] comments = instructionComments.children();
            for (Tree comment : comments) {
                directiveText = getDirectiveText(comment);
                if (directiveText != null) {
                    // remove initial white spaces:
                    while (directiveText.startsWith(" ")) {
                        directiveText = directiveText.substring(1);
                    }
                    directiveTextUC = directiveText.toUpperCase();
                    node = null;
                    if (directiveTextUC.startsWith("II-LOOP") && TapEnv.doActivity()) {
                        node = buildIILoop(directiveText.substring(7), instruction);
                    } else if (directiveTextUC.startsWith("BINOMIAL-CKP ")) {
                        node = buildBinomialCkp(directiveText.substring(13), instruction);
                    } else if (directiveTextUC.startsWith("CHECKPOINT-START")) {
                        node = buildAtomic(Directive.CHECKPOINTSTART);
                    } else if (directiveTextUC.startsWith("CHECKPOINT-END")) {
                        node = buildAtomic(Directive.CHECKPOINTEND);
                    } else if (directiveTextUC.startsWith("SPECIALIZEACTIVITY")) {
                        node = buildAtomic(Directive.SPECIALIZEACTIVITY);
                    } else if (directiveTextUC.startsWith("CHECKPOINT")) {
                        node = buildCheckpoint(instruction);
                        if (ILUtils.isCall(instruction.tree)) {
                            SymbolTable symbolTable = instruction.block.symbolTable;
                            // TODO use getTypedFunctionDecl !!!!!
                            TapList<FunctionDecl> funcDecls =
                                    symbolTable.getFunctionDecl(ILUtils.getCalledNameString(ILUtils.getCall(instruction.tree)), null, null, false);
                            FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
                            if (funcDecl != null) {
                                funcDecl.unit().maybeCheckpointed = true;
                            }
                        }
                    } else if (directiveTextUC.startsWith("NOCHECKPOINT")) {
                        node = buildNoCheckpoint(instruction);
                        if (ILUtils.isCall(instruction.tree)) {
                            SymbolTable symbolTable = instruction.block.symbolTable;
                            // TODO use getTypedFunctionDecl !!!!!
                            TapList<FunctionDecl> funcDecls =
                                    symbolTable.getFunctionDecl(ILUtils.getCalledNameString(ILUtils.getCall(instruction.tree)), null, null, false);
                            FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
                            if (funcDecl != null) {
                                funcDecl.unit().maybeNoCheckpointed = true;
                            }
                        }
                    } else if (directiveTextUC.startsWith("DO-NOT-DIFF")) {
                        node = buildWithLabel(Directive.NODIFFSTART, directiveText.substring(11));
                    } else if (directiveTextUC.startsWith("END-DO-NOT-DIFF")) {
                        node = buildWithLabel(Directive.NODIFFEND, directiveText.substring(15));
                    } else if (directiveTextUC.startsWith("FP-LOOP")) {
                        node = buildFixedPoint(directiveText.substring(7), instruction) ;
                    } else if (directiveTextUC.startsWith("LABEL")) {
                        node = buildWithLabel(Directive.LABEL_START, directiveText.substring(6));
                    } else if (directiveTextUC.startsWith("END-LABEL")) {
                        node = buildWithLabel(Directive.LABEL_END, directiveText.substring(10));
                    } else if (directiveTextUC.startsWith("DEBUG-HERE")) {
                        node = buildWithArgs(Directive.DEBUGHERE, directiveText.substring(10));
                    } else if (directiveTextUC.startsWith("DEBUG-CALL")) {
                        node = buildWithArgs(Directive.DEBUGCALL, directiveText.substring(10));
                    } else if (directiveTextUC.startsWith("INDEPENDENT ")) {
                        node = buildIndependent(directiveText.substring(12), instruction);
                    } else if (directiveTextUC.startsWith("MAKE-PROCEDURE ")) {
                        node = buildMakeProcedure(directiveText.substring(15));
                    } else if (directiveTextUC.startsWith("IN ")) {
                        node = buildWithArgs(Directive.IN, directiveText.substring(3));
                    } else if (directiveTextUC.startsWith("OUT ")) {
                        node = buildWithArgs(Directive.OUT, directiveText.substring(4));
                    } else if (directiveTextUC.startsWith("NOSNAPSHOT")) {
                        node = buildWithArgs(Directive.NOSNAPSHOT, directiveText.substring(10));
                    } else if (directiveTextUC.startsWith("DEAD ")) {
                        node = buildWithLabel(Directive.DEADVARIABLE, directiveText.substring(5));
                    } else if (directiveTextUC.startsWith("DEADADJOINT ")) {
                        node = buildWithLabel(Directive.DEADADJOINTVARIABLE, directiveText.substring(12));
                    } else if (directiveTextUC.startsWith("PASSIVE ")) {
                        node = buildWithArgs(Directive.PASSIVEVARIABLE, directiveText.substring(8));
                    } else if (directiveTextUC.startsWith("CHANNEL ")) {
                        node = buildMessagePassingChannel(directiveText.substring(8), instruction);
                    } else if (directiveTextUC.startsWith("LINE ")) {
                        if (ILUtils.isCall(instruction.tree)) {
                            int lineNumber = Integer.parseInt(directiveText.substring(5));
                            ILUtils.getCall(instruction.tree).down(1).setAnnotation("line",
                                    ILUtils.build(ILLang.op_intCst, lineNumber));
                        }
                        node = null;
                    } else if (directiveTextUC.startsWith("MULTITHREAD ") || directiveTextUC.startsWith("MULTITHREAD_")) {
                        String subText = directiveText.substring(12);
                        // remove initial white spaces:
                        while (subText.startsWith(" ")) {
                            subText = subText.substring(1);
                        }
                        String subTextUC = subText.toUpperCase();
                        if (subTextUC.startsWith("TGT ")) {
                            node = buildWithMultithreadScopings(Directive.MULTITHREAD_TGT, subText.substring(4));
                        } else if (subTextUC.startsWith("TANGENT ")) {
                            node = buildWithMultithreadScopings(Directive.MULTITHREAD_TGT, subText.substring(8));
                        } else if (subTextUC.startsWith("ADJ ")) {
                            node = buildWithMultithreadScopings(Directive.MULTITHREAD_ADJ, subText.substring(4));
                        } else if (subTextUC.startsWith("ADJOINT ")) {
                            node = buildWithMultithreadScopings(Directive.MULTITHREAD_ADJ, subText.substring(8));
                        } else {
                            node = buildWithArgs(Directive.MULTITHREAD, subText);
                        }
                    }
                    if (node != null) {
                        if (!directiveTextUC.startsWith("CHANNEL ")) {
                            directivesToCut = new TapList<>(comment, directivesToCut);
                        }
                        node.directiveCommand = directiveText;
                        instruction.directives = TapList.append(instruction.directives, new TapList<>(node, null)) ;
//                         instruction.directives = new TapList<>(node, instruction.directives);
                    }
                }
            }
            while (directivesToCut != null) {
                Tree comment = directivesToCut.head;
                instructionComments.removeChild(comment.rankInParent());
                directivesToCut = directivesToCut.tail;
            }
        }
    }

    protected static boolean isIILoopDirective(Tree comment) {
        String directiveText = getDirectiveText(comment) ;
        return (directiveText!=null && directiveText.startsWith("II-LOOP"));
    }

    protected static boolean isEndOfRegionDirective(Tree comment) {
        String directiveText = getDirectiveText(comment) ;
        return (directiveText!=null && directiveText.startsWith("END-"));
    }

    /**
     * When "comment" is an AD directive, returns the directive text after the "AD_DIRECTIVE_HEAD".
     * Otherwise returns null.
     *
     * @param comment A comment.
     * @return The text of the directive
     */
    protected static String getDirectiveText(Tree comment) {
        if (!comment.isStringAtom()) {
            return null;
        }
        String text = comment.stringValue();
        // remove white space in front:
        while (text.startsWith(" ")) {
            text = text.substring(1);
        }
        if (!text.toUpperCase().startsWith(Directive.AD_DIRECTIVE_HEAD)) {
            return null;
        }
        // remove AD_DIRECTIVE_HEAD prefix:
        text = text.substring(Directive.AD_DIRECTIVE_HEAD.length());
        // remove white space in front:
        while (text.startsWith(" ")) {
            text = text.substring(1);
        }
        return text;
    }

    public String rebuildFullDirective() {
        return Directive.AD_DIRECTIVE_HEAD+directiveCommand ;
    }

    private static Directive buildAtomic(int dirName) {
        return new Directive(dirName);
    }

    private static Directive buildWithLabel(int dirName, String args) {
        Directive newDir = new Directive(dirName);
        newDir.label = args.trim();
        return newDir;
    }

    private static Directive buildWithArgs(int dirName, String args) {
        Directive newDir = new Directive(dirName);
        parseArguments(newDir, args);
        return newDir;
    }

    private static Directive buildWithMultithreadScopings(int dirName, String args) {
        Directive newDir = new Directive(dirName);
        while (args.startsWith(" ")) {
            args = args.substring(1);
        }
        newDir.arguments = new Tree[]{ILUtils.build(ILLang.op_ident, args)};
        return newDir;
    }

    private static Directive buildIILoop(String args, Instruction instruction) {
        Block block = instruction.block;
        if (block.isALoop() && block.headInstr() == instruction) {
            Directive newDir = new Directive(Directive.IILOOP);
            newDir.label = args.trim();
            return newDir;
        } else {
            TapEnv.fileWarning(TapEnv.MSG_WARN, instruction.tree, "(XX00) Misplaced AD II-LOOP directive, ignored");
            return null;
        }
    }

    private static Directive buildBinomialCkp(String args, Instruction instruction) {
        Block block = instruction.block;
        if (block.isALoop() && block.headInstr() == instruction) {
            Directive newDir = new Directive(Directive.BINOMIALCKP);
            parseArguments(newDir, args);
            return newDir;
        } else {
            TapEnv.fileWarning(TapEnv.MSG_WARN, instruction.tree, "(XX00) Misplaced AD BINOMIAL-CKP directive, ignored");
            return null;
        }
    }

    private static Directive buildFixedPoint(String args, Instruction instruction) {
        Block block = instruction.block;
        if (block.isALoop() && block.headInstr() == instruction) {
            ToObject<String> remainingArgs = new ToObject<>(args) ;
            Directive newDir = new Directive(Directive.FIXEDPOINTLOOP) ;
            String residualString = tokenAfter(remainingArgs, " adj_residual=") ;
            if (residualString==null) residualString = tokenAfter(remainingArgs, " adjResidual=") ;
            if (residualString!=null) {
                try {
                    newDir.adjResidual = Float.valueOf(residualString) ;
                } catch (final NumberFormatException e) {
                    TapEnv.fileError(instruction.tree, "(XX00) Incorrect number format for adj_residual:"+residualString) ;
                    newDir.adjResidual = Float.valueOf("1.0e-6") ;
                }
            }
            String reductionString = tokenAfter(remainingArgs, " adj_reduction=") ;
            if (reductionString==null) reductionString = tokenAfter(remainingArgs, " adjReduction=") ;
            if (reductionString!=null) {
                try {
                    newDir.adjReduction = Float.valueOf(reductionString) ;
                } catch (final NumberFormatException e) {
                    TapEnv.fileError(instruction.tree, "(XX00) Incorrect number format for adj_reduction:"+reductionString) ;
                    newDir.adjReduction = Float.valueOf("1.0e-6") ;
                }
            }
            parseArguments(newDir, remainingArgs.obj());
            return newDir ;
        } else {
            TapEnv.fileError(instruction.tree, "(XX00) Misplaced AD FP-LOOP directive, ignored");
            return null;
        }
    }

    private static Directive buildCheckpoint(Instruction inst) {
        Directive newDir = new Directive(Directive.CHECKPOINT);
        Block block = inst.block;
// The following could provide CHECKPOINT on a loop ?
//         if (block.isALoop() && block.headInstr() == inst) {
//             newDir.setExpression(args) ;
//         }
        return newDir;
    }

    private static Directive buildNoCheckpoint(Instruction inst) {
        if (TapEnv.get().staticTape) {
            TapEnv.commandWarning(-1, "staticTape option deactivated on units with nocheckpoint option");
            TapEnv.get().staticTape = false; // TODO only on units with nocheckpoint option
        }
        Directive newDir = new Directive(Directive.NOCHECKPOINT);
        Block block = inst.block;
// The following could provide NOCHECKPOINT on a loop ?
//         if (block.isALoop() && block.headInstr() == inst) {
//             newDir.setExpression(args) ;
//         }
        return newDir;
    }

    private static Directive buildIndependent(String args, Instruction instruction) {
        Block block = instruction.block;
        if (block != null && block.isALoop() && block.headInstr() == instruction) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, instruction.tree, "(XX00) AD directive INDEPENDENT not yet implemented on loops, ignored");
            return null;
        } else {
            return buildIndependent(args);
        }
    }

    private static Directive buildIndependent(String args) {
        Directive newDir = new Directive(Directive.INDEPENDENT);
        StringTokenizer namesToken = new StringTokenizer(args, " ");
        String name;
        while (namesToken.hasMoreElements()) {
            name = namesToken.nextToken();
//            newDir.addPartName(name);
        }
        return newDir;
    }

    /**
     * Should be used to "outline" a piece of code as a new procedure.
     */
    private static Directive buildMakeProcedure(String args) {
        Directive newDir = new Directive(Directive.PROCEDURE);
        newDir.label = args.trim();
        return newDir;
    }

    private static Directive buildMessagePassingChannel(String args, Instruction instruction) {
        Directive newDir = new Directive(Directive.MESSAGE_PASSING);
        Tree channelStringTree = null;
        if (ILUtils.isCall(instruction.tree)) {
            Tree callTree = ILUtils.getCall(instruction.tree);
            channelStringTree = ILUtils.build(ILLang.op_stringCst, args);
            callTree.setAnnotation("channel", channelStringTree);
            Unit unit = instruction.block.unit();
            if (unit != null) {
                callTree.removeAnnotation("MPIcallInfo");
                MPIcallInfo messagePassingInfo =
                        MPIcallInfo.getMessagePassingMPIcallInfo(ILUtils.getCalledNameString(callTree), callTree,
                                unit.language(), instruction.block);
                if (messagePassingInfo != null && messagePassingInfo.isPointToPoint()) {
                    messagePassingInfo.registerMessagePassingChannel(unit.callGraph());
                }
            }
        }
        newDir.arguments = new Tree[]{channelStringTree};
        return newDir;
    }

    private static Directive buildSaveRestoreReset(String args) {
        Directive newDir = new Directive(Directive.RESET);
        splitPattern.split(args);
        return newDir;
    }

    /**
     * Called by BlockDifferentiator to analyze the contents of an "$AD MULTITHREAD TGT_or_ADJ" directive.
     * Current syntax is: <br><ul>
     * <li> global(variables) : diff of variables must be OMP shared or Cuda globals </li>
     * <li> noconflict(variables) : same as global(variables) </li>
     * <li> shared(variables) : same as global(variables), but deprecated because "shared" meaning is different in Cuda</li>
     * <li> atomic(variables) : diff of variables must be OMP shared or Cuda globals with all usages atomic </li>
     * <li> private(variables) : diff of variables must be OMP private or Cuda registers (non-global) </li>
     * <li> firstprivate(variables) : diff of variables must be OMP firstprivate</li>
     * <li> lastprivate(variables) : diff of variables must be OMP lastprivate</li>
     * <li> reduction(op:variables) : diff of variables must be OMP reduction with given op</li>
     * </ul>
     * @param scopingsStringTree         A tree of given scopings e.g. "atomic(X,Y) reduction(+:Z,R) global(V)"
     * @param caseSensitive              false for Fortran.
     * @param toClausesGivenScoping      will contain the corresponding future new scoping clauses.
     * @param toForcedAtomicScopingNames will contain the names of variables that are forced to (global)ATOMIC.
     * @param toForcedOtherScopingNames  will contain the names of variables that are forced to another scoping.
     */
    public static void analyzeMultithreadScopings(Tree scopingsStringTree, boolean caseSensitive,
                                          TapList<Tree> toClausesGivenScoping,
                                          TapList<String> toForcedAtomicScopingNames,
                                          TapList<String> toForcedOtherScopingNames) {
        String scopingsString =
                (scopingsStringTree == null ? null : scopingsStringTree.stringValue());
        ToObject<String> toScopings = new ToObject<>(scopingsString);
        TapList<String> varsNames;
        int scopingOp;
        String reducOp;
        boolean inAtomic;
        skipCommaWhites(toScopings);
        while (toScopings.obj() != null && !toScopings.obj().isEmpty()) {
            reducOp = null;
            inAtomic = false;
            if (startsWithCaseIndep(toScopings.obj(), "private")) {
                scopingOp = ILLang.op_privateVars;
                toScopings.setObj(toScopings.obj().substring(7));
                String varsString = getInParentheses(toScopings);
                varsNames = getVarNamesInCommas(varsString, caseSensitive);
            } else if (startsWithCaseIndep(toScopings.obj(), "firstprivate")) {
                scopingOp = ILLang.op_firstPrivateVars;
                toScopings.setObj(toScopings.obj().substring(12));
                String varsString = getInParentheses(toScopings);
                varsNames = getVarNamesInCommas(varsString, caseSensitive);
            } else if (startsWithCaseIndep(toScopings.obj(), "lastprivate")) {
                scopingOp = ILLang.op_lastPrivateVars;
                toScopings.setObj(toScopings.obj().substring(11));
                String varsString = getInParentheses(toScopings);
                varsNames = getVarNamesInCommas(varsString, caseSensitive);
            } else if (startsWithCaseIndep(toScopings.obj(), "reduction")) {
                scopingOp = ILLang.op_reductionVars;
                toScopings.setObj(toScopings.obj().substring(9));
                String varsString = getInParentheses(toScopings);
                ToObject<String> toReducInside = new ToObject<>(varsString);
                reducOp = getBeforeColon(toReducInside);
                varsNames = getVarNamesInCommas(toReducInside.obj(), caseSensitive);
            } else if (startsWithCaseIndep(toScopings.obj(), "noconflict")) {
                scopingOp = ILLang.op_sharedVars;
                toScopings.setObj(toScopings.obj().substring(10));
                String varsString = getInParentheses(toScopings);
                varsNames = getVarNamesInCommas(varsString, caseSensitive);
            } else if (startsWithCaseIndep(toScopings.obj(), "global")
                       || /*DEPRECATED: AMBIGUOUS WITH CUDA SHARED!*/ startsWithCaseIndep(toScopings.obj(), "shared")) {
                scopingOp = ILLang.op_sharedVars;
                toScopings.setObj(toScopings.obj().substring(6));
                String varsString = getInParentheses(toScopings);
                varsNames = getVarNamesInCommas(varsString, caseSensitive);
            } else if (startsWithCaseIndep(toScopings.obj(), "atomic")) {
                scopingOp = ILLang.op_sharedVars;
                inAtomic = true;
                toScopings.setObj(toScopings.obj().substring(6));
                String varsString = getInParentheses(toScopings);
                varsNames = getVarNamesInCommas(varsString, caseSensitive);
            } else {
                TapEnv.fileError(null, "Syntax error in $AD OMP clause at:" + toScopings.obj());
                toScopings.setObj(null);
                scopingOp = ILLang.op_none;
                varsNames = null;
            }
            TapList<Tree> toIdents = new TapList<>(null, null);
            while (varsNames != null) {
                if (inAtomic) {
                    toForcedAtomicScopingNames.newR(varsNames.head);
                } else {
                    toForcedOtherScopingNames.newR(varsNames.head);
                }
                toIdents.newR(ILUtils.build(ILLang.op_ident, varsNames.head));
                varsNames = varsNames.tail;
            }
            if (toIdents.tail != null) {
                TapList<Tree> newClauseArgs = new TapList<>(ILUtils.build(ILLang.op_idents, toIdents.tail), null);
                if (scopingOp == ILLang.op_reductionVars) {
                    newClauseArgs = new TapList<>(ILUtils.build(ILLang.op_ident, reducOp), newClauseArgs);
                }
                toClausesGivenScoping.newR(ILUtils.build(scopingOp, newClauseArgs));
            }
            skipCommaWhites(toScopings);
        }
    }

    private static void skipCommaWhites(ToObject<String> toScopings) {
        String scopingsString = toScopings.obj();
        while (scopingsString != null && (scopingsString.startsWith(" ") || scopingsString.startsWith(","))) {
            scopingsString = scopingsString.substring(1);
        }
        toScopings.setObj(scopingsString);
    }

    private static String getInParentheses(ToObject<String> toScopings) {
        String scopingsString = toScopings.obj();
        while (scopingsString != null && scopingsString.startsWith(" ")) {
            scopingsString = scopingsString.substring(1);
        }
        assert scopingsString != null;
        if (scopingsString.startsWith("(")) {
            int closingPosition = scopingsString.indexOf(')');
            if (closingPosition > 0) {
                String contents = scopingsString.substring(1, closingPosition);
                toScopings.setObj(scopingsString.substring(closingPosition + 1));
                return contents;
            }
        }
        toScopings.setObj(scopingsString);
        return null;
    }

    private static String getBeforeColon(ToObject<String> toContents) {
        String contentsString = toContents.obj();
        while (contentsString != null && contentsString.startsWith(" ")) {
            contentsString = contentsString.substring(1);
        }
        assert contentsString != null;
        int colonPos = contentsString.indexOf(':');
        String reducOp = contentsString.substring(0, colonPos);
        int wPos = reducOp.indexOf(' ');
        if (wPos > 0) {
            reducOp = reducOp.substring(0, wPos);
        }
        toContents.setObj(contentsString.substring(colonPos + 1));
        return reducOp;
    }

    private static boolean startsWithCaseIndep(String str, String prefix) {
        if (prefix.length() > str.length()) return false;
        return prefix.equals(str.substring(0, prefix.length()).toLowerCase()) ;
    }

    private static TapList<String> getVarNamesInCommas(String identsString, boolean caseSensitive) {
        TapList<String> toResult = new TapList<>(null, null);
        StringTokenizer identsTokenizer = new StringTokenizer(identsString, ", ");
        String varName ;
        while (identsTokenizer.hasMoreElements()) {
            varName = identsTokenizer.nextToken() ;
            if (caseSensitive) varName = varName.toLowerCase() ;
            toResult.newR(varName) ;
        }
        return toResult.tail;
    }

    private static void parseArguments(Directive directive, String args) {
        // Remove initial white spaces and possible double quotes:
        while (args.startsWith(" ")) {
            args = args.substring(1);
        }
        if (args.startsWith("\"")) {
            int lastDoubleQuote = args.lastIndexOf('\"');
            args = args.substring(1, lastDoubleQuote);
        }
        StringTokenizer argumentsTokenizer = new StringTokenizer(args, ", ");
        Tree expressionTree;
        TapList<Tree> expressionsReversed = null;
        while (argumentsTokenizer.hasMoreElements()) {
            if (directive.type == Directive.FIXEDPOINTLOOP) {
                //TODO: this is too early. Analysis of string "mod%var%field" should be postponed
                // till SymbolTables are ready.
                TapList<String> referenceComponents = TapList.parseIdentifier(argumentsTokenizer.nextToken());
                expressionTree = ILUtils.build(ILLang.op_ident, referenceComponents.head);
                referenceComponents = referenceComponents.tail;
                while (referenceComponents != null) {
                    expressionTree = ILUtils.build(ILLang.op_fieldAccess, expressionTree,
                            ILUtils.build(ILLang.op_ident, referenceComponents.head));
                    referenceComponents = referenceComponents.tail;
                }
            } else {
                expressionTree = ILUtils.build(ILLang.op_ident, argumentsTokenizer.nextToken());
            }

            expressionsReversed = new TapList<>(expressionTree, expressionsReversed);
        }
        Tree[] arguments = new Tree[TapList.length(expressionsReversed)];
        for (int i = arguments.length - 1; i >= 0; --i) {
            assert expressionsReversed != null;
            arguments[i] = expressionsReversed.head;
            expressionsReversed = expressionsReversed.tail;
        }
        directive.arguments = arguments;
    }


    private static String tokenAfter(ToObject<String> toArgs, String key) {
        String args = toArgs.obj() ;
        int keyPos = args.indexOf(key) ;
        if (keyPos<0) return null ;
        String sequel = args.substring(keyPos+key.length()) ;
        int whitePos = sequel.indexOf(" ,") ;
        toArgs.setObj(args.substring(0,keyPos)+(whitePos<0 ? "" : args.substring(whitePos))) ;
        if (whitePos<0) {
            return sequel ;
        } else {
            return sequel.substring(0, whitePos) ;
        }
    }

    @Override
    public String toString() {
        StringBuilder argsString = new StringBuilder();
        if (arguments != null) {
            argsString.append(" args:[");
            for (int i = 0; i < arguments.length; ++i) {
                argsString.append(arguments[i]);
                if (i + 1 < arguments.length) {
                    argsString.append("; ");
                }
            }
            argsString.append(']');
        }
        if (label != null) {
            argsString.append(" label:").append(label);
        }
        return "<Directive "+directiveCommand+" type:"+type+argsString;
    }
}
