/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

/**
 * A vectorial mask, used to guard vectorial instructions.
 */
public final class InstructionMask {

    /**
     * The control of this mask.
     * Control must be an expression Tree of type (array of) boolean.
     * It is placed in an Instruction to allow all controlled Instructions
     * to share any modification of this control.
     */
    public Instruction controlInstruction = new Instruction();
    /**
     * When true, this mask designates the "then" where branch of its control,
     * otherwise the "else" where branch.
     */
    public boolean isTrueBranch = true;
    /**
     * The enclosing mask level, if any.
     */
    public InstructionMask enclosingMask;

    /**
     * Create a mask level enclosed in enclosingMask, controlled by localControl.
     */
    public InstructionMask(Tree localControl, InstructionMask enclosingMask) {
        controlInstruction.setTree(localControl) ;
        this.enclosingMask = enclosingMask;
    }

    public static boolean equalMasks(InstructionMask mask1, InstructionMask mask2) {
        if (mask1 == null || mask1.isEmpty()) {
            return mask2 == null || mask2.isEmpty();
        } else {
            return mask2 != null && !mask2.isEmpty()
                    && mask1.controlInstruction.tree == mask2.controlInstruction.tree
                    && mask1.isTrueBranch == mask2.isTrueBranch;
        }
    }

    /**
     * @return the control Tree of this mask.
     */
    public Tree getControlTree() {
        return controlInstruction.tree;
    }

    /**
     * Sets the control Tree of this mask.
     */
    public void setControlTree(Tree tree) {
        controlInstruction.setTree(tree) ;
    }

    /**
     * @return a copy of this mask, sharing the controlInstruction,
     * but designating the "else" branch.
     */
    public InstructionMask createElseMask() {
        InstructionMask elseMask = copy();
        elseMask.isTrueBranch = false;
        return elseMask;
    }

    /**
     * @return the depth of the enclosing mask levels.
     */
    public int depth() {
        return (enclosingMask == null ? 1 : 1+enclosingMask.depth());
    }

    /**
     * @return true if this mask level is empty i.e. has no control.
     */
    public boolean isEmpty() {
        return controlInstruction == null || ILUtils.isNullOrNone(controlInstruction.tree);
    }

    /**
     * @return a copy of this mask, but the copy shares the controlInstruction.
     */
    protected InstructionMask copy() {
        InstructionMask result = new InstructionMask(null, enclosingMask);
        result.controlInstruction = controlInstruction;
        result.isTrueBranch = isTrueBranch;
        return result;
    }

    public Tree rebuildTree() {
        Tree result = ILUtils.copy(controlInstruction.tree);
        if (!isTrueBranch) {
            result = ILUtils.build(ILLang.op_not, result);
        }
        Tree tree1 = enclosingMask == null ? null : enclosingMask.rebuildTree();
        if (tree1 != null) {
            result = ILUtils.build(ILLang.op_and, tree1, result);
        }
        return result;
    }

    /**
     * Prints in detail the contents of this InstructionMask, onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
//         if (enclosingMask != null) enclosingMask.dump(0) ;
//         TapEnv.print("<") ;
//         if (!isTrueBranch) TapEnv.print("not ") ;
//         if (getControlTree()==null)
//             TapEnv.print("?") ;
//         else
//             ILUtils.dump(getControlTree(), 0) ;
//         TapEnv.print(">") ;
    }

    @Override
    public String toString() {
        return (enclosingMask == null ? "" : enclosingMask.toString()) //+"@"+Integer.toHexString(hashCode())
                + "<" + (isTrueBranch ? "" : "not ") //+"@I"+ Integer.toHexString(controlInstruction.hashCode())+" "
                + ILUtils.toString(getControlTree()) + ">";
    }
}
