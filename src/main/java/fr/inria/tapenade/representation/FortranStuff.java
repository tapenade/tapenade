/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */
package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * Fortran language specific methods.
 */
public final class FortranStuff {
    /** The Unit that holds this FortranStuff. TODO REMOVE ? */
    public Unit unit = null ;
    /** The current CallGraph */
    public CallGraph callGraph = null ;
    /** The related Unit's public SymbolTable, of else the fortranRootSymbolTable */
    public SymbolTable refSymbolTable = null ;

    /**
     * List of this Unit's statement-functions (in reverse order)
     */
    protected TapList<Tree> statementFunctions;
    protected String publicPrivateDefault;
    protected TapList<TapPair<String, Instruction>> implicitDeclarations;
    private boolean checkArrayAccessOrCall = true;
    /** List of Fortran declarations of Common, Equivalence, Data, or Save,
     * that are kept here for future, delayed usage to build the MemoryMaps */
    public TapList<TapPair<Tree, SymbolTable>> listFortranCEDS;
    /**
     * true si on a le statement save sans argument
     * toutes les variables public et private sont "save.
     */
    private boolean saveAll;
    private TapList<TapPair<SymbolTable, Tree>> bindCDeclarationList;

    /**
     * Inserts predefined FORTRAN stuff into this initial symbolTable.
     */
    public static void initFortranSymbolTable(SymbolTable symbolTable) {
        symbolTable.setImplicitLetters('a', 'h', symbolTable.getTypeDecl("float").typeSpec);
        symbolTable.setImplicitLetters('i', 'n', symbolTable.getTypeDecl("integer").typeSpec);
        symbolTable.setImplicitLetters('o', 'z', symbolTable.getTypeDecl("float").typeSpec);
    }

    protected void activateArrayAccessOrCall(boolean val) {
        checkArrayAccessOrCall = val;
    }

    /** Register for future use a Fortran declaration of Common, Equivalence, Data, Save, Bind, etc. */
    protected void collectFortranCEDSForLater(Instruction instr, SymbolTable symbolTable) {
        listFortranCEDS = new TapList<>(new TapPair<>(instr.tree, symbolTable),
                                        listFortranCEDS);
        processVarsInFortranCEDS(instr.tree, symbolTable, instr);
    }

    private void processVarsInFortranCEDS(Tree declTree, SymbolTable symbolTable, Instruction instr) {
        switch (declTree.opCode()) {
            case ILLang.op_accessDecl:
                // Case of a "bind C" declaration. Nothing to do here (?):
                break ;
            case ILLang.op_save: {
                Tree[] subDecls = declTree.children();
                for (Tree subDecl : subDecls) {
                    processVarsInFortranCEDS(subDecl, symbolTable, instr);
                }
                break;
            }
            case ILLang.op_data: {
                Tree[] subDecls = declTree.down(1).children();
                Tree subDecli;
                for (Tree subDecl : subDecls) {
                    subDecli = ILUtils.baseTree(subDecl);
                    assert subDecli != null;
                    processVarsInFortranCEDS(subDecli, symbolTable, instr);
                }
                break;
            }
            case ILLang.op_common: {
                Tree[] subDecls = declTree.down(2).children();
                for (Tree subDecl : subDecls) {
                    processVarsInFortranCEDS(subDecl, symbolTable, instr);
                }
                break;
            }
            case ILLang.op_expressions:
            case ILLang.op_dimColons:
            case ILLang.op_equivalence: {
                Tree[] subDecls = declTree.children();
                for (int i = subDecls.length - 1; i >= 0; i--) {
                    processVarsInFortranCEDS(subDecls[i], symbolTable, instr);
                }
                break;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_dimColon: {
                processVarsInFortranCEDS(declTree.down(1), symbolTable, instr);
                processVarsInFortranCEDS(declTree.down(2), symbolTable, instr);
                break;
            }
            case ILLang.op_arrayDeclarator:
            case ILLang.op_arrayAccess:
            case ILLang.op_substringAccess: {
                processVarsInFortranCEDS(declTree.down(2), symbolTable, instr);
                // attention ca continue dans case op_ident:
            }
            case ILLang.op_ident: {
                String name = ILUtils.baseName(declTree);
                assert name != null;
                VariableDecl variableDecl = symbolTable.getVariableOrConstantDecl(name);
                boolean isCommonName = false;
                if (variableDecl == null) {
                    isCommonName = callGraph.isCommonName(name)
                        || (instr!=null && instr.tree!=null && instr.tree.opCode()==ILLang.op_save
                            && name.startsWith("/") && name.endsWith("/"));
                }
                if (!isCommonName) {
                    // Attention: special Fortran SymbolTable declaration management:
                    // declarations of variables in common must be
                    // moved up to the publicSymbolTable, along with all recursively
                    // needed declarations.
                    boolean InEquivalence = (instr!=null && instr.tree!=null && instr.tree.opCode()==ILLang.op_equivalence) ;
                    if (!InEquivalence && variableDecl != refSymbolTable.getTopVariableOrConstantDecl(name)) {
                        // When the variable is not declared at the enclosing publicSymbolTable level,
                        // it must be moved upwards to there, along with all the other declarations it depends on:
                        symbolTable.moveSymbolDeclListUpRec(
                                new TapList<>(variableDecl, null), refSymbolTable);
                    }
                    // vmp: si variableDecl == null, permet de la declarer correctement
                    // un peu tordu comme methode mais ca marche
                    SymbolDecl newVariableDecl = SymbolDecl.build(
                            new WrapperTypeSpec(null), null, false, declTree, SymbolTableConstants.VARIABLE,
                            symbolTable, instr, null);
                    assert newVariableDecl != null;
                    if (!InEquivalence) {
                        symbolTable.moveSymbolDeclListUpRec(
                                newVariableDecl.dependsOn(), refSymbolTable);
                    }
                    // A variable in a COMMON or EQUIVALENCE is certainly not the name of a function:
                    if (newVariableDecl.isA(SymbolTableConstants.VARIABLE)) {
                        ((VariableDecl) newVariableDecl).isVariableName = true;
                    }
                    SymbolTable targetSymbolTable = (InEquivalence ? symbolTable : refSymbolTable) ;
                    targetSymbolTable.addSymbolDecl(newVariableDecl);
                    VariableDecl publicVariableDecl =
                        targetSymbolTable.getTopVariableDecl(newVariableDecl.symbol);
                    if (publicVariableDecl != null
                            && instr != null
                            && instr.tree != null
                            && instr.tree.opCode() == ILLang.op_common) {
                        publicVariableDecl.setCommonInstr(true);
                        if (ILUtils.containsArrayDeclarator(publicVariableDecl,
                                instr.tree)) {
                            publicVariableDecl
                                    .setCommonWithArrayDeclaratorInstr(true);
                        }
                    }
                }
                break;
            }
            case ILLang.op_intCst: {
                break;
            }
            case ILLang.op_minus:
                processVarsInFortranCEDS(declTree.down(1), symbolTable, instr);
                break;
            case ILLang.op_pointerAccess:
            default:
                TapEnv.toolWarning(-1, "(declare COMMON or EQUIVALENCE) Unexpected operator: " + declTree.opName());
                break;
        }
    }

    private void declareCommonEquivsSaveRec(VariableDecl variableDecl,
                                            SymbolTable symbolTable) {
        // Attention: special Fortran SymbolTable declaration management:
        // declarations of variables in common or equivalences must be
        // moved up to the publicSymbolTable, along with all recursively
        // needed declarations.
        if (variableDecl
                != refSymbolTable.getTopVariableDecl(variableDecl.symbol)) {
            // When the variable is not declared at the enclosing publicSymbolTable
            // level, it must be moved upwards to there, along with all the
            // other declarations it depends on:
            symbolTable.moveSymbolDeclListUpRec(
                    new TapList<>(variableDecl, null),
                    refSymbolTable);
        }
        SymbolDecl newVariableDecl = SymbolDecl.build(
                new WrapperTypeSpec(null), null, false,
                ILUtils.build(ILLang.op_ident, variableDecl.symbol), SymbolTableConstants.VARIABLE,
                symbolTable, null, null);
        symbolTable.moveSymbolDeclListUpRec(
                variableDecl.dependsOn(), refSymbolTable);
        // A variable in a COMMON or EQUIVALENCE is certainly not the name of a function.
        assert newVariableDecl != null;
        if (newVariableDecl.isA(SymbolTableConstants.VARIABLE)) {
            ((VariableDecl) newVariableDecl).isVariableName = true;
        }
        refSymbolTable.addSymbolDecl(newVariableDecl);
    }

//     protected TapList<String> getCommonNames() {
//         TapList<String> commonNames = null;
//         TapList<TapPair<Tree, SymbolTable>> localCommonEquivList = listFortranCEDS;
//         Tree commonEquiv;
//         Tree commonNameTree;
//         String commonName;
//         while (localCommonEquivList != null) {
//             commonEquiv = localCommonEquivList.head.first;
//             if (commonEquiv.opCode() == ILLang.op_common) {
//                 commonNameTree = commonEquiv.down(1);
//                 if (commonNameTree.opCode() == ILLang.op_ident) {
//                     commonName = commonNameTree.stringValue();
//                     commonNames = new TapList<>(commonName, commonNames);
//                 }
//             }
//             localCommonEquivList = localCommonEquivList.tail;
//         }
//         return commonNames;
//     }

//     private boolean isCommonName(String name) {
//         return TapList.containsEquals(getCommonNames(), name);
//     }

    private Tree[] getSavedVars(Tree saveOrDataTree) {
        Tree[] results = null;
        if (saveOrDataTree.opCode() == ILLang.op_save) {
            results = saveOrDataTree.children();
        } else if (saveOrDataTree.opCode() == ILLang.op_data) {
            results = saveOrDataTree.down(1).children();
            for (int i = 0; i < results.length; i++) {
                results[i] = ILUtils.baseTree(results[i]);
            }
        }
        return results;
    }

    protected void setSaveAll() {
        saveAll = true;
    }

    protected boolean getSaveAll() {
        return saveAll;
    }

    protected void computeSaveVarDecls(SymbolTable symbolTable, TapList<SymbolDecl> varDecls) {
        while (varDecls != null) {
            TapList<FunctionDecl> funDecls =
                    symbolTable.getTopFunctionDecl(varDecls.head.symbol, null, null, false);
            if (funDecls == null) {
                computeSaveVarDecl(symbolTable, (VariableDecl) varDecls.head);
            }
            varDecls = varDecls.tail;
        }
    }

    protected void computeSaveVarDecl(SymbolTable symbolTable, VariableDecl varDecl) {
        if (varDecl.formalArgRank<0) { //Don't turn formal args into SAVE variables:
            listFortranCEDS =
                new TapList<>(
                        new TapPair<>(ILUtils.build(ILLang.op_save,
                                ILUtils.build(ILLang.op_ident, varDecl.symbol)),
                                symbolTable),
                        listFortranCEDS);
            declareCommonEquivsSaveRec(varDecl, symbolTable);
        }
    }

    /**
     * Assume tree is an arrayAccess. When its array name
     * is not declared as an array, and no index is an arrayTriplet,
     * transform tree into a procedure or function call.
     * Because Fortran indices are in reverse of natural IL order,
     * inverse the order of indexes in tree.down(2) if tree remains an arrayAccess.
     */
    protected Tree checkArrayAccessOrCall(SymbolTable symbolTable, Tree tree) {
        Tree result = tree;
        if (checkArrayAccessOrCall) {
            boolean hasNoTriplet = !ILUtils.oneIsArrayTriplet(tree.down(2));
            boolean hasNoPointerAccess = tree.down(1).opCode() != ILLang.op_pointerAccess;
            if (hasNoTriplet && hasNoPointerAccess) {
                Tree arrayTree = tree.down(1);
                if (arrayTree.isAtom()) {
                    SymbolDecl symbolDecl =
                            symbolTable.getSymbolDecl(arrayTree.stringValue());
                    if (!(symbolDecl instanceof VariableDecl &&
                            (((VariableDecl) symbolDecl).isVariableName
                                    || WrapperTypeSpec.hasAnArrayDimension(symbolDecl.type())))) {
                        TypeDecl typeDecl = symbolTable.getTypeDecl(arrayTree.stringValue());
                        if (typeDecl != null && TypeSpec.isA(typeDecl.typeSpec, SymbolTableConstants.COMPOSITETYPE)) {
                            result = ILUtils.build(ILLang.op_constructorCall,
                                    ILUtils.copy(tree.down(1)),
                                    ILUtils.copy(tree.down(1)),
                                    ILUtils.reverseArrayComponentOrder(tree.down(2)));
                        } else {
                            result = ILUtils.buildCall(ILUtils.copy(tree.down(1)),
                                    ILUtils.reverseArrayComponentOrder(tree.down(2)));
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Return true when tree is in reality a statement function declaration,
     * i.e. tree is an assign, its left-hand side is an arrayAccess, but it was not declared as an array.
     */
    protected static boolean isStatementFunctionDecl(SymbolTable symbolTable, Tree tree) {
        if (tree.opCode() == ILLang.op_assign
            && tree.down(1).opCode() == ILLang.op_arrayAccess
            && tree.down(1).down(1).isAtom()) {
            VariableDecl variableDecl =
                    symbolTable.getVariableDecl(tree.down(1).down(1).stringValue());
            if ((variableDecl == null
                 || (variableDecl.type() != null
                     && variableDecl.type().wrappedType != null
                     && !TypeSpec.isA(variableDecl.type().peelPointer(), SymbolTableConstants.ARRAYTYPE)))
                && allPlainIdents(tree.down(1).down(2).children())) {
                return true ;
            }
        }
        return false ;
    }

    /**
     * When it is in reality a statement function declaration,
     * transform tree into a statement function declaration.
     */
    protected boolean checkStatementFunctionDecl(SymbolTable symbolTable, Tree tree) {
        if (isStatementFunctionDecl(symbolTable, tree)) {
            // We must undo the reversal of the array indices that was done for Fortran->IL
            Tree indices = ILUtils.reverseArrayComponentOrder(tree.down(1).down(2));
            Tree newStmtFunc = ILUtils.build(ILLang.op_statementFunctionDeclaration,
                                 ILUtils.buildCall(
                                   ILUtils.copy(tree.down(1).down(1)),
                                   indices),
                                 ILUtils.copy(tree.down(2))) ;
            statementFunctions = new TapList<>(newStmtFunc, statementFunctions);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return true iff all Trees in args are plain identifiers,
     * i.e. they can be considered as formal arguments of a statement function.
     */
    private static boolean allPlainIdents(Tree[] args) {
        boolean result = true;
        for (Tree arg : args) {
            result = arg.opCode() == ILLang.op_ident && result;
        }
        return result;
    }

    /*Assume tree is a call.
     * When the function called is declared as a statement function,
     * transform tree into a statementFunctionCall. */
    protected Tree checkStatementFunctionCall(Tree tree) {
        Tree defTree = null;
        String funName = null;
        if (ILUtils.getCalledName(tree).isAtom()) {
            funName = ILUtils.getCalledName(tree).stringValue();
            defTree = getStatementFunctionDecl(funName);
        }
        Tree result = null;
        if (defTree != null) {
            Tree[] vars = ILUtils.getArguments(tree).children();
            Tree[] params = ILUtils.getArguments(defTree.down(1)).children();
            result = ILUtils.copy(defTree);
            assert result != null;
            result = result.down(2);
            if (vars.length != params.length) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, tree, "(TC32) Conflicting numbers of arguments for statement function "
                        + funName + ", expected " + params.length + ", is here " + vars.length);
            }
            for (int i = 0; i < Math.min(vars.length, params.length); i++) {
                InFunction.replaceAllIdent(result, params[i], vars[i]);
            }
            result = ILUtils.copy(result);
            ILUtils.setOrigTree(result, tree);
        }
        return result;
    }

    private Tree getStatementFunctionDecl(String functionName) {
        TapList<Tree> inStatementFunctions = statementFunctions;
        Tree found = null;
        while (found == null && inStatementFunctions != null) {
            if (ILUtils.getCalledName(inStatementFunctions.head.down(1)).stringValue().equals(functionName)) {
                found = inStatementFunctions.head;
            }
            inStatementFunctions = inStatementFunctions.tail;
        }
        return found;
    }

    protected void addPublicPrivateDecl(
            String publicPrivateDecl, Tree instructionTree) {
        if (publicPrivateDefault == null) {
            publicPrivateDefault = publicPrivateDecl;
        } else {
            TapEnv.fileWarning(TapEnv.MSG_SEVERE, instructionTree, "(DD18) Incorrect " + publicPrivateDecl + " declaration");
        }
    }

    protected void addBindCDecl(SymbolTable symbolTable, Tree instructionTree) {
        bindCDeclarationList = new TapList<>(new TapPair<>(symbolTable, instructionTree), bindCDeclarationList);
    }

    private String getCNameFromCommon(String commonName) {
        String fortranName;
        String cName = null;
        TapList<TapPair<SymbolTable, Tree>> bindCList = bindCDeclarationList;
        Tree bindTree;
        String cNameTmp = commonName.substring(1, commonName.length() - 1);
        if (bindCList == null) {
            cName = callGraph.getOtherLanguageName(cNameTmp,
                    TapEnv.FORTRAN, TapEnv.C);
        } else {
            while (bindCList != null && cName == null) {
                bindTree = bindCList.head.second;
                int nbVar = 1;
                if (bindTree.opCode() == ILLang.op_varDeclaration) {
                    nbVar = bindTree.down(3).length();
                }
                for (int i = 1; i <= nbVar; i++) {
                    if (bindTree.opCode() == ILLang.op_varDeclaration) {
                        fortranName = ILUtils.baseName(bindTree.down(3).down(i));
                    } else if (bindTree.opCode() == ILLang.op_ident) {
                        fortranName = ILUtils.baseName(bindTree);
                        cName = callGraph.getOtherLanguageName(cNameTmp,
                                TapEnv.FORTRAN, TapEnv.C);
                    } else {
                        fortranName = ILUtils.baseName(bindTree.down(2).down(i));
                    }
                    assert fortranName != null;
                    if (cName == null && fortranName.equals(commonName)) {
                        cName = ILUtils.getBindCValue(bindCList.head.second, i);
                    }
                }
                bindCList = bindCList.tail;
            }
        }
        return cName;
    }

    //[llh] this method is bound to disappear with new nopubliczones! ?
    // Actually, it seems it is called only for BIND(C) of type declarations-> simplify.
    /**
     * Global mixed-language variables CVarDecl and fortranVarDecl are declared
     * in the globalRootSymbolTable
     * equivalence between CVarDecl and fortranVarDecl (or fortran common) is declared
     * in callGraph.declaredMemoryMap.
     */
    protected void declareBindCInMemoryMap() {
        TapList<TapPair<SymbolTable, Tree>> bindCList = bindCDeclarationList;
        String fortranName;
        String cName;
        VariableDecl fortranVarDecl;
        VariableDecl cVarDecl = null;
        TypeDecl fortranTypeDecl;
        TypeDecl cTypeDecl;
        Tree bindTree;
        SymbolTable bindST;
        MemoryMaps globalCMemoryMap = callGraph.globalFortranMaps ;
        while (bindCList != null) {
            bindST =  bindCList.head.first;
            bindTree = bindCList.head.second;
            int nbVar = 1;
            if (bindTree.opCode() == ILLang.op_varDeclaration) {
                nbVar = bindTree.down(3).length();
            }
            for (int i = 1; i <= nbVar; i++) {
                if (bindTree.opCode() == ILLang.op_varDeclaration) {
                    fortranName = ILUtils.baseName(bindTree.down(3).down(i));
                    unit.setLanguageAndUp(TapEnv.FORTRAN2003);
                    unit.translationUnitSymbolTable().setLanguage(TapEnv.FORTRAN2003);
                } else if (bindTree.opCode() == ILLang.op_ident) {
                    fortranName = bindTree.stringValue();
                } else {
                    fortranName = ILUtils.baseName(bindTree.down(2).down(i));
                    unit.setLanguageAndUp(TapEnv.FORTRAN2003);
                    unit.translationUnitSymbolTable().setLanguage(TapEnv.FORTRAN2003);
                }
                assert fortranName != null;
                fortranVarDecl = bindST.getVariableDecl(fortranName);
                cName = ILUtils.getBindCValue(bindTree, i);
                if (cName == null) {
                    cName = getCNameFromCommon(fortranName);
                }
                if (callGraph.cRootSymbolTable() != null) {
                    cVarDecl = callGraph.cRootSymbolTable().getVariableDecl(cName);
                }
                if (cVarDecl != null) {
                    if (fortranVarDecl != null) {
//                         fortranVarDecl.importedFrom.first = cVarDecl;
//                         cVarDecl.importedFrom.first = fortranVarDecl;
//                         globalCMemoryMap.setEquivalence(
//                                 fortranVarDecl, 0, cVarDecl, 0,
//                                 refSymbolTable,
//                                 callGraph.cRootSymbolTable());
//                         declaredMemoryMap().setEquivalence(
//                                 fortranVarDecl, 0, cVarDecl, 0,
//                                 callGraph.cRootSymbolTable(),
//                                 callGraph.cRootSymbolTable());
                    } else {
//                         declaredMemoryMap().bindVariableAndCommon(fortranName, cVarDecl, unit);
//                         if (bindTree.opCode() == ILLang.op_ident) {
//                             assert fortranName != null;
//                             String commonName = fortranName.substring(1, fortranName.length() - 1);
//                             cVarDecl.importedFrom.first = cVarDecl.copy(null, null);
//                             cVarDecl.importedFrom.first.symbol = commonName;
//                             cVarDecl.importedFrom.first.kind = SymbolTableConstants.COMMON;
//                         }
                    }
                } else {
                    fortranTypeDecl = bindST.getTypeDecl(fortranName);
                    // les typeDecls sont declares dans les translationUnit symbolTables
                    // et pas dans la cRootSymbolTable:
                    TapList<SymbolTable> tuSymbolTables = callGraph.getTranslationUnitSymbolTables();
                    TapList<TypeDecl> cTypeDecls = null;
                    while (tuSymbolTables != null) {
                        if (tuSymbolTables.head.unit.isC()) {
                            cTypeDecl = tuSymbolTables.head.getTypeDecl(cName);
                            if (cTypeDecl == null) {
                                cTypeDecl = tuSymbolTables.head.getTypeDecl("struct " + cName);
                            }
                            if (cTypeDecl != null) {
                                callGraph.addBindFortranCType(fortranTypeDecl, cTypeDecl);
                                cTypeDecls = new TapList<>(cTypeDecl, cTypeDecls);
                            }
                        }
                        tuSymbolTables = tuSymbolTables.tail;
                    }
                    if (cTypeDecls == null && bindTree.opCode() != ILLang.op_ident) {
                        callGraph.addBindFortranCType(fortranTypeDecl, null);
                        if (bindTree.down(2).opCode() != ILLang.op_typeDeclaration) {
                            TapEnv.fileWarning(TapEnv.MSG_SEVERE, bindTree, "(TC26) bind(C) " + fortranName + " with " + cName + " ignored");
                        }
                        // le cTypeDecl sera rempli lors du typeCheck des mixed-language calls
                    }
                }
            }
            bindCList = bindCList.tail;
        }
    }
}
