/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * Constants used for all symbols in symbolTable.
 */
public final class SymbolTableConstants {
    public static final int SYMBOL = 0;
    public static final int VARIABLE = 1;
    public static final int FIELD = 2;
    public static final int FUNCTION = 3;
    public static final int TYPE = 4;
    public static final int CONSTANT = 5;
    public static final int FUNCNAME = 6;
    public static final int PARAMETER = 7;
    public static final int DECLARED = 8;
    public static final int RESULT = 10;
    public static final int CONTROL = 11;
    public static final int LOCAL = 12;
    public static final int GLOBAL = 13;
    public static final int NULL = 14;
    public static final int IDENTITY = 15;
    public static final int INTERFACE = 16;
    public static final int MODULE = 17;
    public static final int CLASS = 18;
    public static final int METHOD = 19;
    public static final int NAMESPACE = 20;
    public static final int FUNCTION_NOT_INTERFACE = -3;
    public static final int FUNCTION_NOT_EXTERNAL_NOR_INTERFACE = -4;
    /**
     * Warning: the following are used as indexes in arrays: don't modify them !
     */
    public static final int ALLKIND = 0;
    public static final int REALKIND = 1;
    public static final int INTKIND = 2;
    public static final int PTRKIND = 3;

    public static final int UNKNOWNTYPE = -1;
    public static final int ACTUALTYPE = 0;

    /**
     * Constants that identify the various kinds of (derived types from) TypeSpec.
     */
    public static final int ARRAYTYPE = 2;
    public static final int FUNCTIONTYPE = 3;
    public static final int LABELTYPE = 4;
    public static final int MODIFIEDTYPE = 5;
    public static final int POINTERTYPE = 6;
    public static final int PRIMITIVETYPE = 7;
    public static final int RECORDTYPE = 8;
    public static final int VOIDTYPE = 9;
    public static final int METATYPE = 10;
    public static final int NAMEDTYPE = 11;
    public static final int ENUMTYPE = 12;
    public static final int UNIONTYPE = 13;
    public static final int WRAPPERTYPE = 14;
    public static final int COMPOSITETYPE = 21;
    public static final int REFERENCETYPE = 22;
    public static final int CLASSTYPE = 23;
    public static final int POLYMORPHICTYPE = 24;

    /**
     * Following constants are the ranks of the classes of indices in pointer destination matrices.
     * Rank 0 is reserved for a convenience class such that the "last index, plus 1" of class 0 is always 0.
     */
    //public static final int SE_CLASS = 1; // NOT USED ANY MORE!
    //public static final int K_CLASS = 2; // NOT USED ANY MORE!
    public static final int D_CLASS = 3;
    public static final int FP_CLASS = 4;

    public static final int CUMUL_OR = 91;
    public static final int CUMUL_AND = 92;
    public static final int CUMUL_MINUS = 93;

    public static final int CALLS = 1;
    public static final int IMPORTS = 2;
    public static final int CONTAINS = 3;

    // Positive ranks reserved for true formal args, 0 for Fortran "result-function-name" variable:
    public static final int NOT_A_FORMAL_ARG = -2;
    public static final int UNDEFINED_FORMAL_ARG_RANK = -1;

    protected static final int UNSPECIFIED_SIGNED = 0;
    protected static final int SIGNED = 1;
    protected static final int UNSIGNED = 2;

    private SymbolTableConstants() {
    }
}
