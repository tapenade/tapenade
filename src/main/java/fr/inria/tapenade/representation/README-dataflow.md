Data-flow methods - <span style="color:green">**<Step 1>**</span>
=================================================================

```
       CallGraph.terminateTypesAndZones()
       --> Unit.import*SymbolTables()
       --> Unit.typeCheck()
       --> Unit.allocate*Zones()
       --> Unit.computeZoneInterfaces()
       --> Unit.computeArrowZoneInterfaces() 
```


`Unit.importAllSymbolTables()` and
`Unit.importInheritedClassesSymbolTables()` deal with `USE`'d `MODULE`'s
or with inherited classes. In this case, (a part of) the `SymbolTable`
of the used or inherited `Unit` must be made available to the using
`Unit`. This is done by copying (some of) its `SymbolDecl`'s into a
special new `SymbolTable` that is inserted into the `SymbolTable`
hierarchy, jut on top of the rootmost (i.e. in general public or
parameter) `SymbolTable` of the using `Unit`.

`Unit.typeCheck()` can then be started, since all symbols used in a
`Block` should now be visible in the `SymbolTable` of this `Block` or in
some enclosing `SymbolTable`. `Unit.typeCheck()` checks for type
consistency and emits warning and error messages. It also tries to do
type inference in case of errors. This type inference behavior may not
be such a good idea because it makes the implementation complex. This
also implies that `Unit.typeCheck()` actually modifies the `IR`.
Therefore it should not be called for only finding the type of an
expression. Use `SymbolTable.typeOf()` instead.

When all types are clear and agreed upon, Tapenade studies the machine
memory layout that results from it. As a result, it defines a number of
so-called memory *zones* and assigns to each declared variable its set
of zones. This is done by `Unit.allocateDeclaredZones()` and
`Unit.allocateSideEffectZones()` See below for a detailled description
of these zones.

Missing: distinction between declared zones and side-effect zones

Finally `Unit.computeZoneInterfaces()` and
`Unit.computeArrowZoneInterfaces()` build the convenience arrays that
are useful in Data-Flow analysis to translate Data-Flow information
through subroutine calls. At any call site, the Data-Flow information
based on the variables of the caller function must be translated to be
based on the variables of the callee, and vice versa upon exit. This
uses correspondence tables:\
`Unit.externalShape, Unit.translator, Unit.transferMatrix, CallArrow.translator`\
that are built by these two methods.

