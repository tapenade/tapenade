/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/**
 * Detects the loop nesting and dfst ordering between the Blocks of a newly built FlowGraph.
 * Also detects dead (not connected) blocks.
 */
public final class FlowGraphNormalizer {
    private static final int CLEAR = 0;
    private static final int VISITING = 1;
    private static final int VISITED = 2;
    private final EntryBlock entryBlock;
    private final ExitBlock exitBlock;
    private TapList<Block> allBlocks;
    private TapList<Block> visitingStack;

    /** A list of dummy FGArrows that have been added temporarily to this Flow Graph
     * to make sure that if a scope is defined inside a loop, then all Block's with
     * this scope are inside the loop too (even if they are indeed exiting the loop) */
    private TapList<FGArrow> scopeDummyArrows ;
    private TapList<TapPair<Block, Block>> neededScopeDummyArrows ;

    /**
     * Holds the list of all Blocks that have been selected to become
     * loop headers. For each of them (plus "null", representing
     * the topmost level of the routine), holds also the
     * list of Blocks in the visiting stack when this loop
     * was first met by the visit (first time "visiting"),
     * and the list of Block's directly contained in the loop,
     * in DFST order.
     */
    private FlowGraphNormalizer.LoopMemoCell loopMemos;

    /**
     * Creates a FlowGraphNormalizer for the FlowGraph starting
     * at "entryBlock", ending at "exitBlock" and knowing that
     * "allBlocks" is the list of all blocks in this FlowGraph.
     */
    protected FlowGraphNormalizer(EntryBlock entryBlock, TapList<Block> allBlocks,
                                  ExitBlock exitBlock) {
        super();
        this.entryBlock = entryBlock;
        this.exitBlock = exitBlock;
        this.allBlocks = allBlocks;
    }

    /**
     * Make sure block 0 can serve as host for additional local declarations.
     * If not (loop header, many entries...) then insert another empty
     * block 0 that will do the job.
     *
     * @param unit current unit.
     */
    protected void makeSurePrivateDeclarationsBlock(Unit unit) {
        TapList<FGArrow> flow = entryBlock.flow();
        FGArrow arrow = flow == null ? null : flow.head;
        Block block0 = arrow == null ? null : arrow.destination;
        if (block0 != null && !unit.isTranslationUnit() &&
                (TapList.length(block0.backFlow()) != 1
                        || block0.symbolTable != unit.privateSymbolTable())) {
            //Then we must insert an empty block that will serve
            // as host for additional local declarations.
            Block newB0 = new BasicBlock(unit.privateSymbolTable(), null, null);
            arrow.insertBlockAtDest(newB0, FGConstants.NO_TEST, 0);
            allBlocks = new TapList<>(newB0, allBlocks);
        }
    }

    /**
     * Sweeps through the FlowGraph, using the allBlocks list,
     * looking for mergings of BasicBlock's, and of FGArrow's,
     * and removing empty Block's and empty IF's and Loop's .
     *
     * @param finalGraph true to get a more aggressive condensationin the end.
     */
    protected void condenseFG(boolean finalGraph) {
        TapList<Block> inAllBlocks;
        boolean foundCondensation;
        Block curBlock;
        foundCondensation = true;
        TapList<Block> topAllBlocks = new TapList<>(null, allBlocks);
        while (foundCondensation) {
            foundCondensation = false;
            inAllBlocks = topAllBlocks;
            while (inAllBlocks != null && inAllBlocks.tail != null) {
                curBlock = inAllBlocks.tail.head;
                // Don't try to condense if this block has already been destroyed.
                if (curBlock.instructions != null
                        || curBlock.flow() != null
                        || curBlock.backFlow() != null) {
                    if (condensedArrows(curBlock)) {
                        foundCondensation = true;
                    }
                    if (condensedBlock(curBlock)) {
                        foundCondensation = true;
                    } else if (skippedEmptyBlock(curBlock, finalGraph)) {
                        inAllBlocks.tail = inAllBlocks.tail.tail;
                        foundCondensation = true;
                    } else if (deleteEmptyIfs(curBlock)) {
                        foundCondensation = true;
                    } else if (deleteEmptyParallelRegion(curBlock)) {
                        foundCondensation = true;
                    } else if (deleteEmptyLoops(curBlock)) {
                        foundCondensation = true;
                        if (curBlock instanceof HeaderBlock) {
                            // We destroy the headerBlock and turn it into a basic block
                            // For example: replace "do i=1,n ..." with "i+n+1"
                            BasicBlock newBasicBlock =
                                    new BasicBlock(curBlock.symbolTable, curBlock.parallelControls, null);
                            newBasicBlock.replaceInFlowGraph(curBlock);
                            newBasicBlock.newDeclarationsBlock(curBlock);
                            inAllBlocks.tail.head = newBasicBlock;
                        }
                    }
                }
                inAllBlocks = inAllBlocks.tail;
            }
        }
        allBlocks = topAllBlocks.tail;
    }


    /**
     * Removes all empty op_if constructs from inside Block block.
     *
     * @return true if modified.
     */
    private boolean deleteEmptyIfs(Block block) {
        Instruction instruction;
        Tree tree;
        Tree exprIF;
        TapList<Instruction> inInstructions;
        TapList<Instruction> toInstructions;
        toInstructions = new TapList<>(null, block.instructions);
        inInstructions = toInstructions;
        boolean deletedIf = false;
        while (inInstructions.tail != null) {
            instruction = inInstructions.tail.head;
            tree = instruction.tree;
            if (tree != null && tree.opCode() == ILLang.op_if
                    && ILUtils.isNullOrNoneOrEmptyList(tree.down(2)) && ILUtils.isNullOrNoneOrEmptyList(tree.down(3))
                    // ^ if this if has empty "then" and "else", and
                    && (inInstructions.tail.tail != null
                    // ^ this if is not the last instruction of the Block
                    ||
                    block.flow() != null && block.flow().tail == null
                            && block.flow().head.test == FGConstants.NO_TEST
                    // ^ or block has only one successor and the arrow has no test
            )) {
                exprIF = tree.down(1);
                if (ILUtils.instrHasSideEffect(exprIF)) {
                    inInstructions = inInstructions.tail;
                } else {
                    // before destroying the instruction we add to the list of instructions
                    // of dead code. Additionally, the comments associated with that instruction
                    // are added to the list of lostComments
                    block.unit().addDeadTree(instruction.tree);
                    if (instruction.preComments != null) {
                        block.unit().addLostComment(instruction.preComments);
                    }
                    if (instruction.postComments != null) {
                        block.unit().addLostComment(instruction.postComments);
                    }
                    inInstructions.tail = inInstructions.tail.tail;
                    deletedIf = true;
                }
            } else {
                inInstructions = inInstructions.tail;
            }
        }
        block.instructions = toInstructions.tail;
        return deletedIf;
    }

    /**
     * Removes all empty parallel constructs.
     *
     * @return true if modified.
     */
    private boolean deleteEmptyParallelRegion(Block block) {
        if (block.isParallelController()) {
            Instruction instruction = block.instructions.head;
            if (block.flow() != null) {
                if (block.flow().head.destination.parallelControls == null ||
                    !TapList.contains(block.flow().head.destination.parallelControls, instruction)) {
                    block.unit().addDeadTree(instruction.tree);
                    if (instruction.preComments != null) {
                        block.unit().addLostComment(instruction.preComments);
                    }
                    if (instruction.postComments != null) {
                        block.unit().addLostComment(instruction.postComments);
                    }
                    block.instructions = null;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Removes all empty op_loop constructs from inside Block block.
     *
     * @return true if modified.
     */
    private boolean deleteEmptyLoops(Block block) {
        Tree accountTimes;
        Tree tree;
        boolean deletedLoop = false;
        TapList<Instruction> inInstructions;
        TapList<Instruction> toInstructions;
        toInstructions = new TapList<>(null, block.instructions);
        inInstructions = toInstructions;
        while (inInstructions.tail != null) {
            tree = inInstructions.tail.head.tree;
            deletedLoop = false;
            // if this instruction is a do-loop or a times-loop ...
            if (tree != null && tree.opCode() == ILLang.op_loop
                    && (tree.down(3).opCode() == ILLang.op_do
                    || tree.down(3).opCode() == ILLang.op_times)
                    //  ... and it's not a closed do (standalone statement) ...
                    && !ILUtils.isNotNoneNorEmpty(tree.down(4))
                    && (     // ... and either it is not the last instruction of its Block ...
                    inInstructions.tail.tail != null
                            ||   // ... OR block has only two successors, and one is block itself...*/
                            block.flow() != null
                                    && block.flow().tail != null
                                    && block.flow().tail.tail == null
                                    && (block.flow().head.destination == block
                                    && block.flow().head.cases.head == FGConstants.NEXT
                                    ||
                                    block.flow().tail.head.destination == block
                                            && block.flow().tail.head.cases.head == FGConstants.NEXT))) {
                // ... then we can try and delete this instruction:
                if (tree.down(3).opCode() == ILLang.op_do) {
                    Tree duplicableLastIndex =
                            ILUtils.hasDuplicableLastIndex(tree.down(3),
                                    true);
                    if (duplicableLastIndex != null) {
                        /* Then we are going to destroy this empty loop header.
                         * Declare it as dead code : */
                        block.unit().addDeadTree(
                                inInstructions.tail.head.tree);
                        boolean onLastInstr =
                                inInstructions.tail.tail == null;
                        if (block instanceof HeaderBlock
                                && ((HeaderBlock) block).loopIndexUnusedAfterLoop) {
                            inInstructions.tail = inInstructions.tail.tail;
                        } else {
                            inInstructions.tail.head.tree =
                                    ILUtils.build(ILLang.op_assign,
                                            ILUtils.copy(tree.down(3).down(1)),
                                            duplicableLastIndex);
                        }
                        if (onLastInstr) {
                            if
                            (block.flow().head.destination == block
                                    && block.flow().head.cases.head
                                    == FGConstants.NEXT) {
                                block.flow().tail.head.cases = null;
                                block.flow().tail.head.test =
                                        FGConstants.NO_TEST;
                                block.flow().head.delete();
                            } else if
                            (block.flow().tail.head.destination
                                            == block
                                            && block.flow().tail.head.cases.
                                            head == FGConstants.NEXT) {
                                block.flow().head.cases = null;
                                block.flow().head.test =
                                        FGConstants.NO_TEST;
                                block.flow().tail.head.delete();
                            }
                        }
                        deletedLoop = true;
                    }
                }
                if (tree.down(3).opCode() == ILLang.op_times) {
                    accountTimes = tree.down(3).down(1);
                    if (!ILUtils.instrHasSideEffect(accountTimes)) {
                        /*avant de detruire l'instruction on l'ajoute a la liste des instructions
                        du dead code, et on ajoute egalement les commentaires associes a cette
                        instruction a la liste des lostComments */
                        block.unit().addDeadTree(
                                inInstructions.tail.head.tree);
                        if
                        (inInstructions.tail.head.preComments
                                != null) {
                            block.unit().addLostComment(
                                    inInstructions.tail.head.
                                            preComments);
                        }
                        if
                        (inInstructions.tail.head.postComments
                                != null) {
                            block.unit().addLostComment(
                                    inInstructions.tail.head.
                                            postComments);
                        }
                        inInstructions.tail = inInstructions.tail.tail;
                        if (inInstructions.tail == null) {
                            if
                            (block.flow().head.destination == block
                                    && block.flow().head.cases.head
                                    == FGConstants.NEXT) {
                                block.flow().tail.head.cases = null;
                                block.flow().tail.head.test =
                                        FGConstants.NO_TEST;
                                block.flow().head.delete();
                            } else if
                            (block.flow().tail.head.destination
                                            == block
                                            && block.flow().tail.head.cases.
                                            head == FGConstants.NEXT) {
                                block.flow().head.cases = null;
                                block.flow().head.test =
                                        FGConstants.NO_TEST;
                                block.flow().tail.head.delete();
                            }
                        }
                        deletedLoop = true;
                    }
                }
                if (!deletedLoop) {
                    inInstructions = inInstructions.tail;
                }
            } else {
                inInstructions = inInstructions.tail;
            }
        }
        block.instructions = toInstructions.tail;
        return deletedLoop;
    }

    /**
     * Sweeps through the FGArrow's exiting from "block", looking
     * for FGArrow's that can be merged. If so, does the merge.
     *
     * @return true if merged, otherwise false.
     */
    private boolean condensedArrows(Block block) {
        FGArrow arrow1;
        FGArrow arrow2;
        TapList<FGArrow> flowArrows1 = block.flow();
        TapList<FGArrow> flowArrows2;
        while (flowArrows1 != null) {
            arrow1 = flowArrows1.head;
            flowArrows2 = flowArrows1.tail;
            while (flowArrows2 != null) {
                arrow2 = flowArrows2.head;
                if (arrow1.destination == arrow2.destination
                        && arrow1.destination != null
                        && arrow1.inACycle == arrow2.inACycle
                        && arrow1.test != FGConstants.CALL
                        && arrow2.test != FGConstants.CALL
                        && arrow1.test != FGConstants.COMP_GOTO
                        && arrow2.test != FGConstants.COMP_GOTO
                        && arrow1.test != FGConstants.IO_GOTO
                        && arrow2.test != FGConstants.IO_GOTO
                        && consecutiveIfSwitchCases(arrow1, arrow2, block)) {
                    boolean merge = true;
                    if (arrow1.test == FGConstants.CASE && arrow2.test == FGConstants.CASE) {
                        merge = !TapIntList.contains(arrow1.cases, FGConstants.NOMATCH)
                                && !TapIntList.contains(arrow2.cases, FGConstants.NOMATCH);
                    }
                    if (merge) {
                        // then condense the two arrows:
                        arrow1.delete();
                        arrow2.mergeCases(arrow1);
                        // return that a condensation occured:
                        return true;
                    }
                }
                flowArrows2 = flowArrows2.tail;
            }
            flowArrows1 = flowArrows1.tail;
        }
        // return that no condensation was possible:
        return false;
    }

    /**
     * Looks for a possible merging of "block" with its successor Block.
     *
     * @return true if the merging is possible, does it, and returns true.
     * Otherwise returns false.
     */
    private boolean condensedBlock(Block block) {
        if (block.flow() != null                         // if "block" has successors,
                && block.flow().tail == null             // and "block" has only one successor,
                && block.flow().head.cases == null       // and there is no "case" information on this arrow,
                && !(block instanceof EntryBlock)      // and "block" is not the entry block,
                && !(block instanceof HeaderBlock)     // and "block" is not the header of a structured loop (orig. for restruct),
                && !block.isALoop()                    // and "block" is not a syntactic loop head, even when not really looping (exits...),
                && !blockDelimitatesFuturePart(block)  // and "block" is not frontier of some "part" (e.g. OpenMP)
        ) {
            Block nextBlock = block.flow().head.destination;
            if (nextBlock != null && nextBlock.backFlow().tail == null
                    // if ^ "nextBlock" has only one predecessor (which is "block"),
                    && nextBlock.symbolTable == block.symbolTable
                    // and ^ "block" and "nextBlock" have the same symbol table,
                    && nextBlock.parallelControls == block.parallelControls
                    // and ^ "block" and "nextBlock" have the same parallel control context,
                    && !(nextBlock instanceof ExitBlock)
                    // and ^ "nextBlock" is not the exit block,
                    && !(nextBlock instanceof HeaderBlock)
                    // and ^ "nextBlock" is not the header of a structured loop,
                    && !nextBlock.isALoop()
                    // and ^ "nextBlock" is not a syntactic loop head, even when not really looping (exits...),
                    && !blockDelimitatesFuturePart(nextBlock)
                // and ^ "nextBlock" is not frontier of some "part" (e.g. OpenMP)
            ) {
                // then "block" and "nextBlock" must be merged:
                block.mergeWithSuccessor(nextBlock);
                return true;
            }
        }
        return false;
    }

    /**
     * @param block Block.
     * @return true if block is frontier of some part (e.g. OpenMP).
     */
    private boolean blockDelimitatesFuturePart(Block block) {
        Instruction headInstr = block.headInstr();
        if (headInstr == null) {
            return false;
        }
        return ILUtils.isParallelController(headInstr.tree) ||
                commentDelimitatesFuturePart(headInstr.preComments) ||
                commentDelimitatesFuturePart(headInstr.preCommentsBlock);
    }

    private boolean commentDelimitatesFuturePart(Tree comments) {
        boolean found = false;
        if (comments != null) {
            Tree[] commentsArray = comments.children();
            String commentText;
            for (int i = commentsArray.length - 1; i >= 0 && !found; --i) {
                String directiveText = Directive.getDirectiveText(commentsArray[i]) ;
                if (directiveText!=null &&
                    (directiveText.startsWith("CHECKPOINT-START")
                     || directiveText.startsWith("LABEL")
                     || directiveText.startsWith("DO-NOT-DIFF")
                     || directiveText.startsWith("CHECKPOINT-END")
                     || directiveText.startsWith("END-")
                     || directiveText.startsWith("FP-LOOP"))) {
                    found = true;
                }
            }
        }
        return found;
    }

    /**
     * Looks for the case where "block" may be just skipped,
     * because it is empty, and there is no weird problem.
     * In this case, skips "block" and returns true,
     * otherwise returns false.
     */
    private boolean skippedEmptyBlock(Block block, boolean finalGraph) {
        if (block.instructions == null         // "block" has no instructions, and
                && !(block instanceof ExitBlock)   // "block" is not the exit block, and
                && block.flow() != null && block.flow().tail == null    // "block" has only one successor, and
                && block.flow().head.destination != block // this successor is not "block" itself, and
                && !isAssGotoDestination(block) // "block" is not used as the destination of an assigned goto
                && (finalGraph || isNotLoopCyclePoint(block))       // when the successor is a "op_loop",
            // this block is not needed as the destination of "cycle" instructions
            // example : Do 200 .... goto 200 .... 200 continue.
            // This restriction is not needed when there is no analysis remaining to do.
            // cf F77:m03
        ) {
            block.skipEmpty();
            return true;
        }
        return false;
    }

    /**
     * @return true if this Block is the destination of an assigned goto.
     * If so, then even if the Block may be empty, it must not be skipped
     * because that would loose the label destination of the assigned goto
     */
    private boolean isAssGotoDestination(Block block) {
        if (block.origLabel() == null) {
            return false;
        }
        TapList<FGArrow> backFlow = block.backFlow();
        boolean origAssGoto = false;
        Instruction lastInstr;
        while (backFlow != null && !origAssGoto) {
            lastInstr = backFlow.head.origin == null ? null : backFlow.head.origin.lastInstr();
            origAssGoto = lastInstr != null && lastInstr.tree != null && lastInstr.tree.opCode() == ILLang.op_gotoLabelVar;
            backFlow = backFlow.tail;
        }
        return origAssGoto;
    }

    /**
     * @return true if this empty Block, when its unique next block is
     * a loop header, is not used as the destination of
     * "goto next iteration" jumps (e.g. "cycle").
     * Otherwise, this block, even though empty, must not be skipped!
     */
    private boolean isNotLoopCyclePoint(Block block) {
        if (block.flow().head.destination.isALoop()) {
            return
                    block.backFlow() != null && block.backFlow().tail == null
                            && block.flow().head.destination
                            == block.backFlow().head.origin;
        } else {
            return true;
        }
    }

    /**
     * @return false iff arrow1 and arrow2 are switch-case arrows coming
     * from block, and at the same time cannot be merged because they
     * are separated by another case betwwen them.
     */
    private boolean consecutiveIfSwitchCases(FGArrow arrow1, FGArrow arrow2, Block block) {
        boolean consecutive = true;
        if (arrow1.test == FGConstants.CASE && arrow2.test == FGConstants.CASE
                && arrow1.cases != null && arrow2.cases != null) {
            if (arrow1.cases.head < 0 ||
                    arrow2.cases.head >= 0 && arrow1.cases.head > arrow2.cases.head) {
                FGArrow tmp = arrow2;
                arrow2 = arrow1;
                arrow1 = tmp;
            }
            Tree switchTree = block.lastInstr().tree;
            if (switchTree.opCode() == ILLang.op_switch) {
                int nbCases = switchTree.down(2).length();
                if (ILUtils.isNullOrEmptyList(switchTree.down(2).down(nbCases).down(1))) {
                    nbCases--;
                }
                int max1 = TapIntList.maxElem(arrow1.cases);
                TapIntList cases2 = arrow2.cases;
                if (cases2.head < 0) {
                    cases2 = new TapIntList(nbCases, cases2.tail);
                }
                int min2 = TapIntList.minElem(cases2);
                if (max1 < min2 - 1) {
                    consecutive = false;
                }
            }
        }
        return consecutive;
    }

    /**
     * For each Block, sorts the flow FGarrows that exit from
     * this block. The resulting conventional order will give a
     * better block numbering during the DFST.
     */
    protected void sortFlowArrows() {
        TapList<Block> inAllBlocks = allBlocks;
        Block curBlock;
        TapList<FGArrow> flow;
        Block defaultDest;
        while (inAllBlocks != null) {
            curBlock = inAllBlocks.head;
            flow = curBlock.flow();
            defaultDest = sensitiveDefaultDest(flow);
            if (defaultDest != null) {
                curBlock.setFlow(putSensitiveDestLast(flow, defaultDest));
            }
            inAllBlocks = inAllBlocks.tail;
        }
    }

    private Block sensitiveDefaultDest(TapList<FGArrow> flowArrows) {
        Block result = null;
        FGArrow arrow;
        while (result == null && flowArrows != null) {
            arrow = flowArrows.head;
            if (arrow.containsCase(FGConstants.DEFAULT) &&
                    //[llh] I think we must not reorder FGArrows out of a CASE:
                    (arrow.test == FGConstants.COMP_GOTO ||
                            arrow.test == FGConstants.IO_GOTO ||
                            arrow.test == FGConstants.CALL)) {
                result = arrow.destination;
            }
            flowArrows = flowArrows.tail;
        }
        return result;
    }

    private TapList<FGArrow> putSensitiveDestLast(TapList<FGArrow> flowArrows, Block sensitiveDest) {
        TapList<FGArrow> hdFirstHalf = new TapList<>(null, null);
        TapList<FGArrow> tlFirstHalf = hdFirstHalf;
        TapList<FGArrow> hdSecndHalf = new TapList<>(null, null);
        TapList<FGArrow> tlSecndHalf = hdSecndHalf;
        FGArrow arrow;
        while (flowArrows != null) {
            arrow = flowArrows.head;
            if (arrow.destination == sensitiveDest) {
                tlSecndHalf = tlSecndHalf.placdl(arrow);
            } else {
                tlFirstHalf = tlFirstHalf.placdl(arrow);
            }
            flowArrows = flowArrows.tail;
        }
        return TapList.append(hdFirstHalf.tail, hdSecndHalf.tail);
    }

    /**
     * Explores the nesting of loops. Makes a reasonable
     * decision about which Block's are in which loops,
     * and which Block's should become the HeaderBlock's
     * of these loops. Also computes the DFST order of
     * the FlowGraph for this particular decision of
     * loops. When this function returns, all blocks have
     * been turned into BasicBlock's or HeaderBlock's
     * according to the above decision, LoopBlock's are
     * created and now contain in their "inside" field the
     * DFST list of their immediately enclosed Block's.
     * The output of this function is the DFST list
     * of the top level Block's.
     */
    protected TapList<Block> findOptimalLoops() {
        boolean foundOptimalHeaders = false ;
        boolean addedScopeDummyArrows = false ;
        scopeDummyArrows = null ;
        TapList<Block> topBlocks;
        while (!foundOptimalHeaders || addedScopeDummyArrows) {
            dfstInit();
            foundOptimalHeaders = dfst(entryBlock);
            addedScopeDummyArrows = insertNeededScopeDummyArrows() ;
        }
        removeScopeDummyArrows() ;
        // Now turn the selected loop header Blocks to HeaderBlock's.
        // The other Blocks must all become BasicBlock's now.
        topBlocks = loopMemos.loopMemo(null).inside;
        redefineLoops(topBlocks);
        return topBlocks;
    }

    private boolean insertNeededScopeDummyArrows() {
        boolean addedScopeDummyArrows = false ;
        while (neededScopeDummyArrows!=null) {
            Block fromBlock = neededScopeDummyArrows.head.first ;
            Block toBlock = neededScopeDummyArrows.head.second ;
            if (!fromBlock.flowsTo(toBlock)) {
                FGArrow dummyArrow =
                    new FGArrow(fromBlock, FGConstants.NO_TEST, 0, toBlock) ;
// System.out.println("ADDED SCOPE DUMMY ARROW "+dummyArrow) ;
                addedScopeDummyArrows = true ;
                scopeDummyArrows = new TapList<>(dummyArrow, scopeDummyArrows) ;
            }
            neededScopeDummyArrows = neededScopeDummyArrows.tail ;
        }
        return addedScopeDummyArrows ;
    }

    private void removeScopeDummyArrows() {
        FGArrow dummyArrow ;
        while (scopeDummyArrows!=null) {
            dummyArrow = scopeDummyArrows.head ;
            dummyArrow.delete() ;
            scopeDummyArrows = scopeDummyArrows.tail ;
        }
    }

    /**
     * Must be run just after findOptimalLoops.
     *
     * @return the list of Block's detected as dead,
     * because they are unreachable, i.e.
     * not marked "VISITED" at the end of "findOptimalLoops".
     */
    protected TapList<Block> getDeadBlocks() {
        TapList<Block> inAllBlocks = allBlocks;
        TapList<Block> deadBlocks = null;
        Block block;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            if (block.visit != VISITED) {
                deadBlocks = new TapList<>(block, deadBlocks);
            }
            block.visit = CLEAR;
            inAllBlocks = inAllBlocks.tail;
        }
        return deadBlocks;
    }

    /**
     * According to the loop choices described in "loopMemos",
     * creates the necessary HeaderBlock's and LoopBlock's,
     * and sets the correct "inside" fields of the LoopBlock's.
     */
    private void redefineLoops(TapList<Block> blocks) {
        Block curBlock;
        FlowGraphNormalizer.LoopMemoCell blockMemo;
        while (blocks != null) {
            curBlock = blocks.head;
            blockMemo = loopMemos.loopMemo(curBlock);
            if (blockMemo == null) {
                // null blockMemo means that this Block must be a BasicBlock
                if (!(curBlock instanceof BasicBlock)) {
                    BasicBlock basicBlock =
                            new BasicBlock(curBlock.symbolTable, curBlock.parallelControls, null);
                    basicBlock.replaceInFlowGraph(curBlock);
                    basicBlock.newDeclarationsBlock(curBlock);
                    blocks.head = basicBlock;
                }
            } else {
                // This means we must create a loop level
                HeaderBlock headerBlock =
                        new HeaderBlock(curBlock.symbolTable, curBlock.parallelControls, null);
                LoopBlock loopBlock = new LoopBlock(new TapList<>(headerBlock, blockMemo.inside));
                headerBlock.replaceInFlowGraph(curBlock);
                headerBlock.newDeclarationsBlock(curBlock);
                if (curBlock instanceof HeaderBlock) {
                    headerBlock.setOrigCycleLabel(((HeaderBlock) curBlock).origCycleLabel());
                }
                blocks.head = loopBlock;
                // Recursively redefine loops inside this header "block":
                redefineLoops(blockMemo.inside);
            }
            blocks = blocks.tail;
        }
    }

    /**
     * Initialization or re-initialization before one depth-first sweep
     * through the FlowGraph, starting from the EntryBlock till the
     * Exit Block. Resets the list of Blocks elected to become
     * loop headers (loopMemos), resets all the visit fields to
     * "CLEAR", except for Entry and Exit Blocks.
     */
    private void dfstInit() {
        TapList<Block> inAllBlocks = allBlocks;
        entryBlock.visit = VISITED;
        exitBlock.visit = VISITED;
        while (inAllBlocks != null) {
            inAllBlocks.head.visit = CLEAR;
            inAllBlocks.head.setEnclosingBlock(null);
            inAllBlocks = inAllBlocks.tail;
        }
        visitingStack = new TapList<>(entryBlock, null);
        loopMemos = new FlowGraphNormalizer.LoopMemoCell(null, null, null);
    }

    /**
     * Depth-First sweep in the FlowGraph. This calls for Depth-First
     * search from "prevBlock".
     * The output of this function is in "loopMemos", that contains:<br>
     * 1) the list of all the Block's that have been
     * chosen to become HeaderBlocks.<br>
     * 2) The visiting stack when this block was first visited.<br>
     * 3) For each of these Block's, the dfst-ordered
     * list of the blocks immediately enclosed.
     * (For Block = null, this list is the "topBlocks" list).
     *
     * @return false if we must re-run the whole dfst sweep because we have decided
     * to change the header of some loop for a better possible header.
     */
    private boolean dfst(Block prevBlock) {
        Block nextBlock;
        TapList<FGArrow> flowArrows = prevBlock.flow();
        flowArrows = reorderFlowForDfst(flowArrows);
        boolean foundOptimalHeaders = true;
        while (flowArrows != null && foundOptimalHeaders) {
            nextBlock = flowArrows.head.destination;
            if (nextBlock.visit == VISITING) {
                // Then we have found a loop.
                // If language allows for local scopes, make sure that each scope
                // that intersects this loop is actually fully included in this loop.
                if (!TapEnv.relatedUnit().isFortran()) {
                    TapList<SymbolTable> exitedScopes = null ;
                    if (prevBlock.symbolTable.nestedIn(nextBlock.symbolTable)) {
                        exitedScopes = prevBlock.symbolTable.allSymbolTableRoots(nextBlock.symbolTable) ;
                    }
                    SymbolTable exitedScope ;
                    while (exitedScopes!=null) {
                        exitedScope = exitedScopes.head ;
                        if (!exitedScope.isUseless()) {
                            TapList<Block> inAllBlocks = allBlocks;
                            while (inAllBlocks!=null) {
                                Block blockInScope = inAllBlocks.head ;
                                if (blockInScope.symbolTable.nestedIn(exitedScope)
                                    && blockInScope.flowsOutOfScope(exitedScope)) {
                                    // Register the need for a temporary dummy FGArrow
                                    //  from blockInScope to the future loop header:
                                    neededScopeDummyArrows =
                                        new TapList<>(new TapPair<>(blockInScope, nextBlock),
                                                      neededScopeDummyArrows) ;
                                }
                                inAllBlocks = inAllBlocks.tail ;
                            }
                        }
                        exitedScopes = exitedScopes.tail ;
                    }
                }
                // then create sructure for this loop.
                if (loopMemos.loopMemo(nextBlock) == null) {
                    loopMemos = new FlowGraphNormalizer.LoopMemoCell(nextBlock, null, loopMemos);
                }
                //Declare all blocks in the visiting stack as members
                // of this loop, upto "nextBlock" (not included).
                declareVisitingInLoopUntil(nextBlock);
            } else if (nextBlock.visit == VISITED) {
                // For each loop level "loopBlock" around "nextBlock"
                // that is in "VISITED" mode, this implies that we
                // are jumping from outside "loopBlock",
                // (otherwise "loopBlock" would still be "visiting")
                // into inside "loopBlock". Therefore this is an
                // alternate loop entry. Check this loop entry is not
                // better than the other entry:
                Block loopBlock = nextBlock;
                while (foundOptimalHeaders
                        && (loopBlock = loopBlock.enclosingBlock()) != null
                        && loopBlock.visit != VISITING) {
                    foundOptimalHeaders = checkAlternateEntry(loopBlock, nextBlock);
                }
                // If there remains a loop level around "nextBlock",
                // then it is certainly in "visiting" mode. Then we
                // are just reaching a visited part of this loop,
                // and therefore we must declare the visiting stack
                // till this "loopBlock" as contents of "loopBlock":
                if (foundOptimalHeaders && loopBlock != null) {
                    declareVisitingInLoopUntil(loopBlock);
                }
            } else {
                FlowGraphNormalizer.LoopMemoCell inLoopMemos;
                // Else this is the first visit to "nextBlock".
                // Declare it as "visiting":
                nextBlock.visit = VISITING;
                visitingStack = new TapList<>(nextBlock, visitingStack);
                // Restart recursively, depth first from "nextBlock":
                foundOptimalHeaders = dfst(nextBlock);
                if (foundOptimalHeaders) {
                    // When recursion returns, set "nextBlock" to "VISITED",
                    // set its rank, and put it into the ordered list.
                    visitingStack = visitingStack.tail;
                    nextBlock.visit = VISITED;
                    inLoopMemos = loopMemos.loopMemo(nextBlock.enclosingBlock());
                    inLoopMemos.inside =
                            new TapList<>(nextBlock, inLoopMemos.inside);
                    // When "nextBlock" is a loop header, remember this
                    // "visitingStack" that led to it. It will be used
                    // in case of loops with two entries, to find the
                    // cfg arrows that must be switched.
                    inLoopMemos = loopMemos.loopMemo(nextBlock);
                    if (inLoopMemos != null) {
                        inLoopMemos.visitStack = visitingStack;
                    }
                }
            }
            flowArrows = flowArrows.tail;
        }
        return foundOptimalHeaders;
    }

    /**
     * Force the order of the flowArrows (which all flow from the same Block)
     * so that dfst order is correct.
     * A strong constraint is that the "exit" from a loop header must be put last.
     * This is to force the rank of the natural sequel of a loop to be lower (before)
     * than the rank of other possible sequels (goto's, etc). cf C:v217.
     * A weaker constraint is that the "true" branch of an if must be put last.
     */
    private TapList<FGArrow> reorderFlowForDfst(TapList<FGArrow> flowArrows) {
        TapList<FGArrow> hdResult = new TapList<>(null, null);
        TapList<FGArrow> tlResult = hdResult;
        FGArrow arrow;
        FGArrow putLast = null;
        while (flowArrows != null) {
            arrow = flowArrows.head;
            if (arrow.test == FGConstants.LOOP && arrow.containsCase(FGConstants.EXIT)
                    ||
                    arrow.test == FGConstants.IF && arrow.containsCase(FGConstants.TRUE)) {
                putLast = arrow;
            } else {
                tlResult = tlResult.placdl(arrow);
            }
            flowArrows = flowArrows.tail;
        }
        if (putLast != null) {
            tlResult.placdl(putLast);
        }
        return hdResult.tail;
    }

    /**
     * For a given loop, a possible header "newLoopHeader" is better
     * than another "oldLoopHeader", iff the new is a DO, and the old
     * was not, and the old was syntactically inside this DO.
     *
     * @return false if we want to do this header change.
     */
    private boolean checkAlternateEntry(Block oldLoopHeader, Block newLoopHeader) {
        Instruction oldHead = null;
        Instruction newHead = null;
        if (oldLoopHeader.instructions != null) {
            oldHead = oldLoopHeader.headInstr();
        }
        if (newLoopHeader.instructions != null) {
            newHead = newLoopHeader.headInstr();
        }
        if (!(oldHead != null && oldHead.isALoop())
                && newHead != null && newHead.isALoop()
                && (oldHead == null || newHead.syntaxControls(oldHead))) {
            // Then we know we must prefer "newLoopHeader" as loop header
            TapList<Block> oldStack;
            TapList<Block> newStack;
            oldStack = loopMemos.loopMemo(oldLoopHeader).visitStack;
            oldStack = new TapList<>(oldLoopHeader, oldStack);
            newStack = new TapList<>(newLoopHeader, visitingStack);
            while (!TapList.contains(newStack, oldStack.tail.head)) {
                oldStack = oldStack.tail;
            }
            while (newStack.tail.head != oldStack.tail.head) {
                newStack = newStack.tail;
            }
            newStack.tail.head.exchangeFlowArrowsTo(oldStack.head, newStack.head);
            return false;
        } else {
            return true;
        }
    }

    /**
     * For each block in the visitingStack, from the top down until
     * "limitLoopBlock" excluded, say that this block is directly
     * enclosed into loop "limitLoopBlock". This is done by filling
     * the "enclosingBlock" field of the block to "limitLoopBlock".
     * However, this field is set only for local use during this
     * call to "findOptimalLoops". This field is not a result
     * of "findOptimalLoops", and may be overwritten later.
     */
    private void declareVisitingInLoopUntil(Block limitLoopBlock) {
        TapList<Block> inVisitingList;
        inVisitingList = visitingStack;
        while (inVisitingList.head != limitLoopBlock) {
            inVisitingList.head.setEnclosingBlockProtected(limitLoopBlock);
            inVisitingList = inVisitingList.tail;
        }
    }

    /**
     * Local class LoopMemoCell: Cell of a list of loop information,
     * each composed of a Block that is currently considered as a loop,
     * and of the list of Blocks in the visiting stack when this loop
     * was first met by the visit (first time "visiting"),
     * and the list of Block's directly contained in the loop,
     * in DFST order.
     */
    private static class LoopMemoCell {
        private final Block loop;
        private final FlowGraphNormalizer.LoopMemoCell next;
        protected TapList<Block> visitStack;
        protected TapList<Block> inside;

        LoopMemoCell(Block loop, TapList<Block> visitStack, FlowGraphNormalizer.LoopMemoCell next) {
            super();
            this.loop = loop;
            this.visitStack = visitStack;
            this.next = next;
            inside = null;
        }

        FlowGraphNormalizer.LoopMemoCell loopMemo(Block loopHeader) {
            FlowGraphNormalizer.LoopMemoCell curCell = this;
            while (curCell != null && curCell.loop != loopHeader) {
                curCell = curCell.next;
            }
            return curCell;
        }

        @Override
        public String toString() {
            FlowGraphNormalizer.LoopMemoCell curCell = this;
            StringBuilder result = new StringBuilder();
            while (curCell != null) {
                result.append(';').append(curCell.loop);
                curCell = curCell.next;
            }
            return result.toString();
        }
    }
}
