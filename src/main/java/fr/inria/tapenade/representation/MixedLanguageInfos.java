/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * Mixed language information.
 */
public final class MixedLanguageInfos {
    private final int origLangCaller;
    private final int destLangCallee;
    /**
     * "byRef" if all parameters are always passed by reference, else "byValue".
     */
    private final String parameterPassing;
    private final String functionBinding;
    private final String suffix;

    public MixedLanguageInfos(int caller, int callee,
                              String passing, String binding, String suffix) {
        super();
        this.origLangCaller = caller;
        this.destLangCallee = callee;
        this.parameterPassing = passing;
        this.functionBinding = binding;
        this.suffix = suffix;
    }

    public static TapList<MixedLanguageInfos> defaultMixedLangInfos() {
        MixedLanguageInfos fortranInfos = new MixedLanguageInfos(TapEnv.FORTRAN, TapEnv.FORTRAN,
                "byRef", "", null);
        MixedLanguageInfos fortran90Infos = new MixedLanguageInfos(TapEnv.FORTRAN90, TapEnv.FORTRAN90,
                "byRef", "", null);
        MixedLanguageInfos fortran2003Infos = new MixedLanguageInfos(TapEnv.FORTRAN2003, TapEnv.FORTRAN2003,
                "byRef", "", null);  // byRef sauf si modifier "value"
        MixedLanguageInfos cInfos = new MixedLanguageInfos(TapEnv.C, TapEnv.C,
                "byValue", "", null);
        MixedLanguageInfos cFortranInfos = new MixedLanguageInfos(TapEnv.C, TapEnv.FORTRAN,
                "byRef", "remove", "_");
        MixedLanguageInfos fortranCInfos = new MixedLanguageInfos(TapEnv.FORTRAN, TapEnv.C,
                "byValue", "add", "_");
        MixedLanguageInfos cFortran90Infos = new MixedLanguageInfos(TapEnv.C, TapEnv.FORTRAN90,
                "byRef", "remove", "_");
        MixedLanguageInfos fortran90CInfos = new MixedLanguageInfos(TapEnv.FORTRAN90, TapEnv.C,
                "byValue", "add", "_");
        MixedLanguageInfos cFortran2003Infos = new MixedLanguageInfos(TapEnv.C, TapEnv.FORTRAN2003,
                "byValue", "remove", "_");
        MixedLanguageInfos fortran2003CInfos = new MixedLanguageInfos(TapEnv.FORTRAN2003, TapEnv.C,
                "byValue", "add", "_");
        MixedLanguageInfos cPlusPlusInfos = new MixedLanguageInfos(TapEnv.CPLUSPLUS, TapEnv.CPLUSPLUS,
                "byValue", "", null);
        MixedLanguageInfos cPlusPlusCInfos = new MixedLanguageInfos(TapEnv.CPLUSPLUS, TapEnv.C,
                "byValue", "", null);
        TapEnv.addMixedLanguageInfos(fortranInfos);
        TapEnv.addMixedLanguageInfos(fortran90Infos);
        TapEnv.addMixedLanguageInfos(fortran2003Infos);
        TapEnv.addMixedLanguageInfos(cInfos);
        TapEnv.addMixedLanguageInfos(cFortranInfos);
        TapEnv.addMixedLanguageInfos(fortranCInfos);
        TapEnv.addMixedLanguageInfos(cFortran90Infos);
        TapEnv.addMixedLanguageInfos(fortran90CInfos);
        TapEnv.addMixedLanguageInfos(cFortran2003Infos);
        TapEnv.addMixedLanguageInfos(fortran2003CInfos);
        TapEnv.addMixedLanguageInfos(cPlusPlusInfos);
        TapEnv.addMixedLanguageInfos(cPlusPlusCInfos);
        return TapEnv.mixedLanguageInfos();
    }

    public static boolean passesByValue(int callerLanguage, int calledLanguage) {
        String parameterPassing =
            MixedLanguageInfos.getParameterPassing(callerLanguage, calledLanguage, TapEnv.mixedLanguageInfos()) ;
        return (parameterPassing == null || parameterPassing.toLowerCase().startsWith("byvalue")) ;
    }

    public static String getParameterPassing(int origLang, int destLang, TapList<MixedLanguageInfos> infos) {
        String result = null;
        TapList<MixedLanguageInfos> mlInfos = infos;
        MixedLanguageInfos mixedLanguageInfos;
        if (TapEnv.isFortran(origLang) && TapEnv.isFortran(destLang)) {
            destLang = origLang ;
        }
        while (result == null && mlInfos != null) {
            mixedLanguageInfos = mlInfos.head;
            if (mixedLanguageInfos.getOrigLangCaller() == origLang
                    && mixedLanguageInfos.getDestLangCallee() == destLang) {
                result = mixedLanguageInfos.getParameterPassing();
            }
            mlInfos = mlInfos.tail;
        }
        return result;
    }

    public int getOrigLangCaller() {
        return origLangCaller;
    }

    public int getDestLangCallee() {
        return destLangCallee;
    }

    private String getParameterPassing() {
        return parameterPassing;
    }

    public String getFunctionBinding() {
        return functionBinding;
    }

    public String getSuffix() {
        return suffix;
    }

    @Override
    public String toString() {
        return this.origLangCaller + "->" + this.destLangCallee + " "
                + this.parameterPassing + " " + this.functionBinding + " " + this.suffix;
    }
}
