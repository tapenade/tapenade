/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.Tree;

/**
 * C language specific methods.
 */
public final class CStuff {
    private static final String[] ioCall = {
            "remove", "rename", "tmpfile", "tmpnam", "fclose", "fflush", "fopen",
            "freopen", "setbuf", "setvbuf", "fprintf", "sprintf", "vfprintf",
            "vprintf", "vsprintf", "snprintf", "vsnprintf", "fscanf", "scanf",
            "sscanf", "vfscanf", "vscanf", "vsscanf", "fgetc", "getc", "getchar",
            "fputc", "putc", "putchar", "fgets", "gets", "fputs", "puts", "ungetc",
            "fread", "fwrite", "fseek", "ftell", "rewind", "fgetpos", "fsetpos",
            "clearerr", "feof", "ferror", "perror", "printf"};

    /**
     * Inserts predefined C stuff into this initial symbolTable.
     */
    public static void initCSymbolTable(SymbolTable symbolTable) {
        PrimitiveTypeSpec integerTypeSpec = new PrimitiveTypeSpec("integer");
        integerTypeSpec.setPrimitiveTypeSize(TapEnv.get().integerSize);
        VariableDecl ioVarDecl;
        ioVarDecl =
                new VariableDecl(ILUtils.build(ILLang.op_ident, "__FILE__"),
                        new WrapperTypeSpec(new PointerTypeSpec(
                                new WrapperTypeSpec(new PrimitiveTypeSpec("character")),
                                null)), false);
        ioVarDecl.setZones(new TapList<>(new TapIntList(TapEnv.get().origCallGraph().zoneNbOfAllIOStreams, null), null));
        symbolTable.addSystemSymbolDecl(ioVarDecl);
        ioVarDecl =
                new VariableDecl(ILUtils.build(ILLang.op_ident, "__LINE__"),
                        new WrapperTypeSpec(integerTypeSpec), false);
        ioVarDecl.setZones(new TapList<>(new TapIntList(TapEnv.get().origCallGraph().zoneNbOfAllIOStreams, null), null));
        symbolTable.addSystemSymbolDecl(ioVarDecl);
        ioVarDecl =
                new VariableDecl(ILUtils.build(ILLang.op_ident, "__func__"),
                        new WrapperTypeSpec(new PointerTypeSpec(
                                new WrapperTypeSpec(new FunctionTypeSpec(
                                        new WrapperTypeSpec(new VoidTypeSpec()))),
                                null)), false);
        // Not sure __func__ is related to IO ??
        ioVarDecl.setZones(new TapList<>(new TapIntList(TapEnv.get().origCallGraph().zoneNbOfAllIOStreams, null), null));
        symbolTable.addSystemSymbolDecl(ioVarDecl);
        symbolTable.addSystemSymbolDecl(
                new VariableDecl(ILUtils.build(ILLang.op_ident, "__PRETTY_FUNCTION__"),
                        new WrapperTypeSpec(new PointerTypeSpec(
                                new WrapperTypeSpec(new FunctionTypeSpec(
                                        new WrapperTypeSpec(new VoidTypeSpec()))),
                                null)), false));
        symbolTable.addSystemSymbolDecl(
                new VariableDecl(ILUtils.build(ILLang.op_ident, "__tzname"),
                        new WrapperTypeSpec(null), false));
        symbolTable.addSystemSymbolDecl(
                new VariableDecl(ILUtils.build(ILLang.op_ident, "ourIssmMPIStatusIgnore"),
                        new WrapperTypeSpec(null), false));
        symbolTable.addSystemSymbolDecl(
                new VariableDecl(ILUtils.build(ILLang.op_ident, "__timezone"),
                        new WrapperTypeSpec(null), false));
        symbolTable.addSystemSymbolDecl(
                new VariableDecl(ILUtils.build(ILLang.op_ident, "__daylight"),
                        new WrapperTypeSpec(null), false));
        symbolTable.addSystemSymbolDecl(
                new VariableDecl(ILUtils.build(ILLang.op_ident, "__environ"),
                        new WrapperTypeSpec(null), false));
    }

    public static boolean isAnIoCall(Tree callTree) {
        boolean isIO = false;
        if (callTree != null && callTree.opCode() == ILLang.op_call) {
            String functionName = ILUtils.getCalledNameString(callTree);
            if (functionName != null) {
                for (int i = ioCall.length - 1; !isIO && i >= 0; --i) {
                    isIO = functionName.equals(ioCall[i]);
                }
            }
        }
        return isIO;
    }

    protected static Tree buildIOCall(Tree callTree) {
        return ILUtils.build(ILLang.op_ioCall,
                ILUtils.copy(ILUtils.getCalledName(callTree)),
                ILUtils.build(ILLang.op_expressions),
                ILUtils.copy(ILUtils.getArguments(callTree)));
    }

    protected static boolean isAMalloc(Tree mallocTree) {
        boolean isMalloc = false;
        if (mallocTree != null) {
            while (mallocTree.opCode() == ILLang.op_cast) {
                mallocTree = mallocTree.down(2);
            }
            if (mallocTree.opCode() == ILLang.op_call) {
                Tree functionName = ILUtils.getCalledName(mallocTree);
                isMalloc = ILUtils.isIdentOf(functionName, new String[]{"malloc", "calloc", "cudaMalloc"}, true) ;
            }
        }
        return isMalloc;
    }

    /** Builds a transformed copy of the C-syntax mallocTree, using the IL syntax op_allocate
     * @param mallocTree a call to malloc or calloc, possibly enclosed into a cast.
     * @return an op_allocate tree, containing in particular the cast type if present.
     */
    protected static Tree buildAllocate(Tree mallocTree, int typeSize,
                                        SymbolTable symbolTable, Instruction instruction) {
        Tree type = null;
        Tree result = ILUtils.build(ILLang.op_allocate);
        if (mallocTree.opCode() == ILLang.op_cast) {
            type = ILUtils.copy(mallocTree.down(1));
            if (type != null) {
                if (type.opCode() == ILLang.op_pointerType || type.opCode() == ILLang.op_arrayType) {
                    // Peel off the "pointer_to" or "array_of" on the type:
                    type = type.down(1);
                } else {
                    //The type used in the cast is not explicitly a pointer: explicit it!
                    WrapperTypeSpec typeSpec =
                            TypeSpec.build(type, symbolTable, instruction,
                                    new TapList<>(null, null), new TapList<>(null, null),
                                    new TapList<>(null, null), new ToBool(false), null);
                    if (typeSpec != null) {
                        if (TypeSpec.isA(typeSpec, SymbolTableConstants.POINTERTYPE)) {
                            typeSpec = ((PointerTypeSpec) typeSpec.wrappedType).destinationType;
                        } else if (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
                            typeSpec = typeSpec.wrappedType.elementType();
                        }
                    }
                    type = typeSpec == null ? null : typeSpec.generateTree(symbolTable, null, null, true, null);
                }
            }
            mallocTree = mallocTree.down(2);
        }
        String funcName = ILUtils.getCalledNameString(mallocTree);
        Tree allocArgs = ILUtils.getArguments(mallocTree);
        Tree sizeInBytes = null;
        Tree sizeInObjects = null;
        if (funcName.equals("malloc")) {
            TapPair<Tree, Tree> foundSizes = new TapPair<>(null, null);
            sizeInBytes = allocArgs.down(1);
            sizeInObjects = extractElementSize(sizeInBytes, foundSizes, type, typeSize);
            if (foundSizes.first != null) {
                Tree sizeOfObject = foundSizes.first;
                if (isaModifiedFloatFloat(sizeOfObject.down(1))) {
                    sizeOfObject.setChild(sizeOfObject.cutChild(1).cutChild(2), 1);
                }
                if (type == null) {
                    type = ILUtils.copy(sizeOfObject.down(1));
                }
                if (foundSizes.second != null) {
                    if (sizeInObjects != null) {
                        sizeInObjects = ILUtils.build(ILLang.op_mul, sizeInObjects, foundSizes.second);
                    } else {
                        sizeInObjects = foundSizes.second;
                    }
                }
            } else if (foundSizes.second == null) {
                if (typeSize != 0) {
                    sizeInObjects = ILUtils.build(ILLang.op_div, sizeInObjects,
                            ILUtils.build(ILLang.op_intCst, typeSize));
                } else {
                    sizeInObjects = ILUtils.build(ILLang.op_intCst, 1);
                }
            }
        } else if (funcName.equals("calloc")) {
            sizeInObjects = allocArgs.down(1);
            Tree sizeOfObject;
            int nbArgs = allocArgs.length();
            if (nbArgs == 2) {
                sizeOfObject = allocArgs.down(2);
                if (sizeOfObject.opCode() == ILLang.op_sizeof) {
                    if (isaModifiedFloatFloat(sizeOfObject.down(1))) {
                        sizeOfObject.setChild(sizeOfObject.cutChild(1).cutChild(2), 1);
                    }
                    if (type == null) {
                        type = ILUtils.copy(sizeOfObject.down(1));
                    }
                }
            } else {
                TapEnv.fileError(mallocTree, "(TC32) Conflicting numbers of arguments for calloc, expected 2, is here "
                        + allocArgs.length());
                return mallocTree;
            }
            sizeInBytes = ILUtils.build(ILLang.op_mul,
                    ILUtils.copy(sizeInObjects),
                    ILUtils.copy(sizeOfObject));
            result.setAnnotation("sourcetree", allocArgs);
        }
        if (sizeInObjects != null) {
            sizeInObjects = ILUtils.build(ILLang.op_expressions, ILUtils.copy(sizeInObjects));
        }
        result.setChild(ILUtils.copy(sizeInBytes), 1);
        if (sizeInObjects != null) {
            result.setChild(sizeInObjects, 2);
        }
        if (type != null) {
            result.setChild(type, 3);
        }
        return result;
    }

    /** Builds a transformed copy of the IL syntax op_allocate, as a C-syntax mallocTree.
     * Decision of the C-style allocation (malloc, calloc, or cudaMalloc)
     * is based the "sourcetree" annotation of the allocateTree.
     * @param allocateTree an op_allocate tree a call to malloc or calloc, possibly enclosed into a cast.
     * @param assigned the variable that receives the allocated memory. Used only when regenerating a cudaMalloc.
     * @return a call to malloc or calloc, possibly enclosed into a cast, or a call to cudaMalloc
     */
    public static Tree buildMalloc(Tree allocateTree, Tree assigned) {
        Tree sourceTree = allocateTree.getAnnotation("sourcetree") ;
        while (sourceTree!=null && sourceTree.opCode()==ILLang.op_cast) {
            sourceTree = sourceTree.down(2) ;
        }
        if (ILUtils.isCallingString(sourceTree, "cudaMalloc", true)
            || ILUtils.isCallingString(sourceTree, "cudaFree", true)) {
            Tree lhs = ILUtils.addAddressOf(ILUtils.copy(assigned)) ;
            Tree newCudaMalloc =
                ILUtils.buildCall(null,
                  ILUtils.build(ILLang.op_ident, "cudaMalloc"),
                  ILUtils.build(ILLang.op_expressions,
                    lhs,
                    ILUtils.copy(allocateTree.down(1)))) ;
            return newCudaMalloc ;
        } else if (ILUtils.isCallingString(sourceTree, "malloc", true)
                   || ILUtils.isCallingString(sourceTree, "free", true)) {
            Tree newMalloc =
                ILUtils.buildCall(null,
                  ILUtils.build(ILLang.op_ident, "malloc"),
                  ILUtils.build(ILLang.op_expressions,
                    ILUtils.copy(allocateTree.down(1)))) ;
            if (!ILUtils.isNullOrNone(allocateTree.down(3))) {
                newMalloc =
                  ILUtils.build(ILLang.op_cast,
                    ILUtils.build(ILLang.op_pointerType,
                      ILUtils.copy(allocateTree.down(3))),
                    newMalloc) ;
            }
            return newMalloc ;
        } else if (ILUtils.isCallingString(sourceTree, "calloc", true)) {
            Tree newCalloc ;
            if (ILUtils.isNullOrNone(allocateTree.down(3))) {
                // bizarre case. Reuse the source form.
                newCalloc = ILUtils.copy(sourceTree) ;
            } else {
                Tree baseType = ILUtils.copy(allocateTree.down(3)) ;
                newCalloc =
                    ILUtils.build(ILLang.op_cast,
                      ILUtils.build(ILLang.op_pointerType, 
                        ILUtils.copy(baseType)),
                      ILUtils.buildCall(null,
                        ILUtils.build(ILLang.op_ident, "calloc"),
                        ILUtils.build(ILLang.op_expressions,
                          ILUtils.copy(allocateTree.down(2)),
                          ILUtils.buildCall(null,
                            ILUtils.build(ILLang.op_ident, "sizeof"),
                            ILUtils.build(ILLang.op_expressions,
                              ILUtils.copy(baseType)))))) ;
            }
            return newCalloc ;
        } else {
            // Error case, should not happen:
            return ILUtils.copy(allocateTree) ;
        }
    }

    private static Tree extractElementSize(Tree sizeInBytes, TapPair<Tree, Tree> foundSizes, Tree type, int typeSize) {
        if (sizeInBytes.opCode() == ILLang.op_mul) {
            Tree x2 = extractElementSize(sizeInBytes.down(2), foundSizes, type, typeSize);
            Tree x1 = extractElementSize(sizeInBytes.down(1), foundSizes, type, typeSize);
            return x1 == null ? x2 : x2 == null ? x1 : ILUtils.build(ILLang.op_mul, x1, x2);
        } else if (sizeInBytes.opCode() == ILLang.op_div) {
            Tree x1 = extractElementSize(sizeInBytes.down(1), foundSizes, type, typeSize);
            if (x1 == null) {
                x1 = ILUtils.build(ILLang.op_intCst, 1);
            }
            return ILUtils.build(ILLang.op_div, x1, ILUtils.copy(sizeInBytes.down(2)));
        } else if (sizeInBytes.opCode() == ILLang.op_sizeof) {
            if (foundSizes.first == null) {
                foundSizes.first = ILUtils.copy(sizeInBytes);
                return null;
            } else {
                return ILUtils.copy(sizeInBytes);
            }
        } else if (sizeInBytes.opCode() == ILLang.op_intCst) {
            if (foundSizes.first == null && foundSizes.second == null && typeSize != 0 && sizeInBytes.intValue() == typeSize) {
                foundSizes.second = ILUtils.copy(sizeInBytes);
                return null;
            } else {
                return ILUtils.copy(sizeInBytes);
            }
        } else {
            return ILUtils.copy(sizeInBytes);
        }
    }

    protected static boolean isAFree(Tree callTree) {
        boolean isFree = false;
        if (callTree != null && callTree.opCode() == ILLang.op_call) {
            Tree functionName = ILUtils.getCalledName(callTree);
            isFree = ILUtils.isIdentOf(functionName, new String[]{"free", "cudaFree"}, true) ;
        }
        return isFree;
    }

    protected static boolean isAPow(SymbolTable symbolTable, Tree callTree) {
        // true seulement si pow est definie dans un include C standard:
        boolean isPow = false;
        if (callTree != null && callTree.opCode() == ILLang.op_call) {
            String functionName = ILUtils.getCalledNameString(callTree);
            isPow = "pow".equals(functionName);
            if (isPow) {
                TapList<FunctionDecl> powFuncDecls =
                        symbolTable.getFunctionDecl("pow", null, null, false);
                FunctionDecl powFuncDecl = powFuncDecls == null ? null : powFuncDecls.head;
                isPow = powFuncDecl != null && (powFuncDecl.isExternal() || powFuncDecl.isIntrinsic())
                        && powFuncDecl.unit().inStdCIncludeFile();
            }
        }
        return isPow;
    }

    protected static Tree buildDeallocate(Tree callFreeTree) {
        Tree container = ILUtils.copy(ILUtils.getArguments(callFreeTree).down(1));
        return ILUtils.build(ILLang.op_deallocate, container, null);
    }

    protected static Tree buildPow(Tree callPowTree) {
        Tree son1 = ILUtils.copy(ILUtils.getArguments(callPowTree).down(1));
        Tree son2 = ILUtils.copy(ILUtils.getArguments(callPowTree).down(2));
        return ILUtils.build(ILLang.op_power, son1, son2);
    }

    /**
     * Tests that instrOrExpr is op_modifiedType(op_list[op_ident:"float"], float())
     * [llh 4Sept2017] This is only because of a bug? introduced by version 6119
     * that creates "float real"s in C instead of plain "float"s.
     */
    protected static boolean isaModifiedFloatFloat(Tree instrOrExpr) {
        boolean result = instrOrExpr != null && instrOrExpr.opCode() == ILLang.op_modifiedType;
        if (result) {
            Tree instrOrExprDown1 = instrOrExpr.down(1);
            int length = instrOrExprDown1.length();
            result = instrOrExpr.down(2).opCode() == ILLang.op_float
                    && (length == 0
                    || length == 1
                    && ILUtils.isIdent(instrOrExprDown1.down(1), "float", false));
        }
        return result;
    }

    private CStuff() {
    }
}
