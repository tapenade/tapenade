/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.Tree;

/**
 * Undefined type used in Tapenade libraries.
 */
public final class MetaTypeSpec extends TypeSpec {

    public final String name;

    public MetaTypeSpec(String name) {
        super(SymbolTableConstants.METATYPE);
        this.name = name;
    }

    @Override
    protected String baseTypeName() {
        return "";
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's:
        toOther = peelWrapperTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        if (testIsReceives(comparison) && !testTypesLitteral(comparison)) {
            // A MetaTypeSpec can receive any Type (except arrays?) :
            return true;//other==null || !other.containsArray() ;
        } else {
            return (name!=null && other instanceof MetaTypeSpec) ;
        }
    }

    @Override
    public MetaTypeSpec copy() {
        return this;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    protected boolean containsMetaType(TapList<TypeSpec> dejaVu) {
        return true;
    }

    protected TypeSpec makeLocalizedTypeSpec() {
        TypeSpec localizedType = localize(new TapList<>(null, null), new ToBool(false));
        return ((WrapperTypeSpec) localizedType).wrappedType;
    }

    @Override
    protected TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        TypeSpec copiedResult = findAlreadyCopiedType(toAlreadyCopied, containsMeta);
        if (copiedResult == null) {
            TypeSpec localizedType = null;
            if (name != null) {
                if (name.startsWith("void")) {
                    localizedType = new VoidTypeSpec();
                } else if (name.startsWith("pointer")) {
                    localizedType = new PointerTypeSpec(new WrapperTypeSpec(null), null);
                } else {
                    String proposed = null;
                    if (name.startsWith("integer")) {
                        proposed = "integer";
                    } else if (name.startsWith("float") || name.startsWith("real")) {
                        proposed = "float";
                    } else if (name.startsWith("complex")) {
                        proposed = "complex";
                    }
                    // Mechanism: if a library function type, in addition to using metavars,
                    // uses a metavar named "integer...", "float..." or "complex...", this metavar type
                    // cannot be instantiated by a numeric type less general than the metavar's name.
                    // E.g. if the name of the metavar is "float", or "complex", then the
                    // instantiated type cannot be "integer".
                    // Example: sin(metavar float)->metavar float is such that sin(1) returns a float and not an int!
                    if (proposed != null) {
                        localizedType = new PrimitiveTypeSpec(proposed);
                        localizedType.setUndefinedNumeric(true);
                    }
                }
            }
            copiedResult = new WrapperTypeSpec(localizedType);
            toAlreadyCopied.placdl(new TapTriplet<>(this, copiedResult, Boolean.TRUE));
        }
        containsMeta.set(true);
        return copiedResult;
    }

    @Override
    public boolean containsUndefinedType() {
        return true;
    }

    @Override
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable, int diffUnitSort, String fSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax, String hintArrayNameInText,
                                                 String hintArrayNameInIdent, Tree hintArrayNameTree, Tree nameTree) {
        if (TapEnv.associationByAddress()) {
            FieldDecl[] fields = new FieldDecl[2];
            fields[0] = new FieldDecl(TapEnv.assocAddressValueSuffix(), new WrapperTypeSpec(this));
            fields[1] = new FieldDecl(TapEnv.assocAddressDiffSuffix(), new WrapperTypeSpec(this));
            diffTypeSpec = new WrapperTypeSpec(new CompositeTypeSpec(null, fields, SymbolTableConstants.RECORDTYPE, null, null));
            this.createOrGetDiffTypeDeclSymbolHolder(
                    symbolTable, diffTypeSpec, fSuffix);
            return diffTypeSpec;
        } else {
            return null;
        }
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        // It may happen that we want to regenerate a Tree for a meta type
        // that has not been solved into a normal type. This is unfortunate.
        // In that case, turn it into a primitive type and generate the Tree for it.
        Tree result = null;
        if (name != null) {
            if (name.startsWith("integer")) {
                result = ILUtils.build(ILLang.op_integer);
            } else if (name.startsWith("float") || name.startsWith("real")) {
                result = ILUtils.build(ILLang.op_float);
            } else if (name.startsWith("complex")) {
                result = ILUtils.build(ILLang.op_complex);
            } else if (name.startsWith("boolean")) {
                result = ILUtils.build(ILLang.op_boolean);
            } else if (name.startsWith("character")) {
                result = ILUtils.build(ILLang.op_character);
            } else {
                result = ILUtils.build(ILLang.op_ident, name);
            }
        }
        return result;
    }

    @Override
    public String showType() {
        return name;
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "meta" //+"@"+Integer.toHexString(hashCode())
                + ':' + name;
    }

}
