/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.analysis.InOutAnalyzer;
import fr.inria.tapenade.analysis.LoopArrayAccessAnalyzer;
import fr.inria.tapenade.analysis.PointerAnalyzer;
import fr.inria.tapenade.frontend.TreeProtocol;

/**
 * Top of internal representation, holding the call graph for a given program or library.
 */
public final class CallGraph {

    private SymbolTable globalRootSymbolTable;

    public SymbolTable globalRootSymbolTable() {
        return globalRootSymbolTable;
    }

    public void setGlobalRootSymbolTable(SymbolTable globalRootSymbolTable) {
        this.globalRootSymbolTable = globalRootSymbolTable;
    }

    private SymbolTable fortranRootSymbolTable;

    public SymbolTable fortranRootSymbolTable() {
        return fortranRootSymbolTable;
    }

    public void setFortranRootSymbolTable(SymbolTable fortranRootSymbolTable) {
        this.fortranRootSymbolTable = fortranRootSymbolTable;
//         this.fortranStuff.refSymbolTable = fortranRootSymbolTable;
    }

    private SymbolTable cRootSymbolTable;

    public SymbolTable cRootSymbolTable() {
        return cRootSymbolTable;
    }

    public void setCRootSymbolTable(SymbolTable cRootSymbolTable) {
        this.cRootSymbolTable = cRootSymbolTable;
    }

    private SymbolTable cudaRootSymbolTable;

    public SymbolTable cudaRootSymbolTable() {
        return cudaRootSymbolTable;
    }

    public void setCudaRootSymbolTable(SymbolTable cudaRootSymbolTable) {
        this.cudaRootSymbolTable = cudaRootSymbolTable;
    }

    private SymbolTable cPlusPlusRootSymbolTable;

    public SymbolTable cPlusPlusRootSymbolTable() {
        return cPlusPlusRootSymbolTable;
    }

    public void setCPlusPlusRootSymbolTable(SymbolTable cPlusPlusRootSymbolTable) {
        this.cPlusPlusRootSymbolTable = cPlusPlusRootSymbolTable;
    }

    /** The memory maps built for all COMMON, SAVE, and BIND(C) variables (Fortran-specific) */
    public MemoryMaps globalFortranMaps;

    /** The "maps" built for C global vars that are visible across files (Translation Units). */
    public TapList<TapList<ZoneInfoAccessElements>> globalCMaps ;

    public TapList<String> usedCommonNames = null ;

    public boolean isCommonName(String name) {
        return TapList.containsEquals(usedCommonNames, name);
    }

    /** The ordered list of all global zones that an external routine may see */
    public TapList<ZoneInfo> allGlobalsForExternals = null ;

    /** The number of global zones in allGlobalsForExternals (together with the 3 kind-specific numbers) */
    public int[] globalsForExternalsNumber4 ;

    /** The number of global zones in this CallGraph (together with the 3 kind-specific numbers) */
    public int[] globalZonesNumber4 ;

    /**
     * The (hatted) list of all named types encountered,
     * used to organize sharing of CompositeTypeSpec's activities
     */
    public TapList<TapPair<String, TypeSpec>> toSharingTypes = new TapList<>(null, null);

    /**
     * When this CallGraph was built by transformation from some origin CallGraph,
     * this A-list may hold the link from each Unit of the origin CallGraph to
     * the list of the related Units of this new CallGraph.
     */
    public TapList<TapPair<Unit, TapList<Unit>>> backAssociations;
    /**
     * Block that will contain global declarations.
     */
    public Block topBlock;
    public TapList<Unit> MPIRecvCalls;
    /**
     * The tree of comments pre-attached to this CallGraph.
     */
    public Tree topPreComments;
    /**
     * The tree of comments post-attached to this CallGraph.
     */
    public Tree topPostComments;
    /**
     * The tree of block-comments pre-attached to this CallGraph.
     */
    public Tree topPreCommentsBlock;
    /**
     * The tree of block-comments post-attached to this CallGraph.
     */
    public Tree topPostCommentsBlock;
    /**
     * index of the special memory zone that represents destination of NULL.
     */
    public int zoneNbOfNullDest ;
    /**
     * index of the special memory zone that represents destination of an uninitialized pointer.
     */
    public int zoneNbOfUnknownDest ;
    /**
     * index of the special memory zone that represents all I-O files.
     */
    public int zoneNbOfAllIOStreams;

    /** The number of dummy zones of the root SymbolTable, i.e. the destinations
     * of NULL, Undef, the IO stream(s), and the MPI channels. */
    public int numberOfDummyRootZones;

    /** The name of this CallGraph */
    private final String callGraphName;
    /**
     * The unique main Unit (e.g. main() or "program") of this CallGraph, if any.
     */
    private Unit mainUnit;
    /**
     * Number of Units defined in this CallGraph.
     */
    private int nbUnits;
    /**
     * Unordered list of all Units present in this CallGraph.
     */
    private TapList<Unit> units;
    /**
     * List of all top Units in this CallGraph.
     */
    private TapList<Unit> topUnits;
    /**
     * Unordered list of all intrinsic Units used in this CallGraph.
     */
    private TapList<Unit> intrinsicUnits = null ;

    /** All dummy units built for "unknown" procedures */
    public TapList<Unit> dummyUnits = null ;

    /**
     * Unordered list of all interface Units in this CallGraph.
     */
    private TapList<Unit> interfaceUnits;
    /**
     * Unordered list of all Fortran2003 functions name with associated C Unit Name
     * first: Fortran name String
     * second: C name String
     * third: FunctionDecl.
     */
    private TapList<TapTriplet<String, String, FunctionDecl>> bindFortranCFunctions;
    /**
     * Array of all Units (including externals) present in this CallGraph, sorted.
     * Sorting can vary, but basically top-down call order for Fortran77,
     * bottom-up call order for Fortran90 and C.
     */
    private Unit[] sortedUnits;
    private TapList<Unit> recursivityUnits;
    private TapList<SymbolTable> translationUnitSymbolTables;
    /**
     * The TreeProtocol input stream through which incoming units arrive.
     */
    private TreeProtocol inputStream;
    /**
     * Stack of TreeProtocol's for push/pop of input streams.
     */
    private TapList<TreeProtocol> inputStreams;
    /**
     * Allocation points for pointers.
     * TapList of quadruplets containing:
     * <pre>
     * -- The op_allocate(...) Tree
     * -- The access path to the allocated array or scalar
     * -- Its type, which should be a WrapperTypeSpec(PointerTypeSpec(...))
     * -- The SymbolTable that encloses the op_allocate().
     * </pre>
     */
    private TapList<TapPair<TapPair<Tree, Tree>, TapPair<TypeSpec, SymbolTable>>> allocationPoints;

    /**
     * TapList of all channels. For each channel.
     */
    private TapList<TapPair<MPIcallInfo, TapIntList>> allMessagePassingChannels;
    /**
     * External or Forward or Interface Unit contained in FunctionDecl must be deleted after creation
     * of the standard Unit with the same name if the creation is correctly done.
     */
    private TapList<TapTriplet<FunctionDecl, Unit, TapPair<FunctionDecl, SymbolTable>>>
            previousFunctionDeclAndNewUnitList;

    /**
     * Level of debugging to debug sorting of Units.
     */
    private int debuglevel;
    // List of differentiated units with bind(C): they have the same name in mixed-language calls between
    // Fortran2003 and C.
    private TapList<Unit> bindFortranCDiffUnits;
    // List of interoperable fortran - C typeDecls.
    private TapList<TapPair<TypeDecl, TypeDecl>> bindFortranCType;

    // List of all Units that may be used through a pointer
    public TapList<Unit> allPointedUnits = null ;

//     public void bilanFC() {
//         TapList<TapTriplet<String, String, FunctionDecl>> lo1 = bindFortranCFunctions ;
//         System.out.println("BINDFORTRANCFUNCTIONS:") ;
//         while (lo1!=null) {
//             System.out.println("   -- "+lo1.head) ;
//             lo1 = lo1.tail ;
//         }
//         TapList<Unit> lo2 = bindFortranCDiffUnits ;
//         System.out.println("BINDFORTRANCDIFFUNITS:") ;
//         while (lo2!=null) {
//             System.out.println("   -- "+lo2.head) ;
//             lo2 = lo2.tail ;
//         }
//         TapList<TapPair<TypeDecl, TypeDecl>> lo3 = bindFortranCType ;
//         System.out.println("BINDFORTRANCTYPE:") ;
//         while (lo3!=null) {
//             System.out.println("   -- "+lo3.head) ;
//             lo3 = lo3.tail ;
//         }
//     }

    /**
     * Creates a new CallGraph called "name".
     */
    public CallGraph(String name) {
        super();
        this.callGraphName = name;

        // TODO: Review what the topBlock is doing in here. Is it really needed?
        topBlock = new BasicBlock(null, null, null);
        topBlock.rank = 54;
//         fortranStuff = new FortranStuff() ;
//         fortranStuff.callGraph = this ;
//         declaredMemoryMap = new MemoryMap() ; //OBSOLETE
        globalFortranMaps = new MemoryMaps() ;
        globalCMaps = null ;
        usedCommonNames = null ;
    }

    /**
     * Adds a new CallArrow into the CallGraph, from "origin" to "destination".
     * If such an arrow already exists, no arrow is created.
     * "kind" is the kind of arrow, belongs to {CALLS, IMPORTS, CONTAINS, -1}.
     * If this arrow CALLS, the number of calls is incremented.
     */
    public static CallArrow addCallArrow(Unit origin, int kind, Unit destination) {
        CallArrow foundArrow = getCallArrow(origin, destination);
        if (foundArrow == null) {
            foundArrow = new CallArrow(origin, destination);
        }
        foundArrow.addKind(kind);
        if (kind == SymbolTableConstants.CALLS) {
            foundArrow.setTimes(foundArrow.times() + 1);
        }
        return foundArrow;
    }

    /**
     * Removes a CallArrow from the CallGraph, from "origin" to "destination".
     * "kind" is the kind of arrow, belongs to {CALLS, IMPORTS, CONTAINS, -1}.
     * If this arrow CALLS, the number of calls is decremented.
     */
    private static void delCallArrow(Unit origin, int kind, Unit destination) {
        CallArrow foundArrow = getCallArrow(origin, destination);
        assert foundArrow != null;
        if (kind == SymbolTableConstants.CALLS) {
            foundArrow.setTimes(foundArrow.times() - 1);
        }
        foundArrow.delKind(kind);
        if (foundArrow.times() == 0 && !foundArrow.isImport() && !foundArrow.isContain()) {
            foundArrow.delete();
        }
    }

    /**
     * @return the CallArrow from "callerUnit" to "calleeUnit".
     * In the special case where "calleeUnit" is an intrinsic, there is no
     * CallArrow coming from each routine that uses it. Therefore,
     * we return a "generic" arrow with its origin set to "callerUnit".
     */
    public static CallArrow getCallArrow(Unit callerUnit, Unit calleeUnit) {
        boolean found = false;
        CallArrow res = null;
        if (calleeUnit.isIntrinsic() && !calleeUnit.hasSource()) {
            res = calleeUnit.genericArrow;
            res.origin = callerUnit;
        } else {
            // general case: find the arrow in the callers of "calleeUnit":
            for (TapList<CallArrow> tmpList = calleeUnit.callers(); tmpList != null
                    && !found; tmpList = tmpList.tail) {
                if (tmpList.head.origin == callerUnit) {
                    found = true;
                    res = tmpList.head;
                }
            }
        }
        return res;
    }

    private static MixedLanguageInfos functionNameBinding(int callerLanguage, int calleeLanguage) {
        TapList<MixedLanguageInfos> mixedLanguageInfos = TapEnv.mixedLanguageInfos();
        MixedLanguageInfos infos = null;
        boolean found = false;
        while (mixedLanguageInfos != null && !found) {
            infos = mixedLanguageInfos.head;
            if (infos.getOrigLangCaller() == callerLanguage
                    && infos.getDestLangCallee() == calleeLanguage) {
                found = true;
            }
            mixedLanguageInfos = mixedLanguageInfos.tail;
        }
        return infos;
    }

    /**
     * Dumps a compact textual form of the call graph,
     * i.e. the nodes (the Units) and the CallArrows.
     */
    public static void dumpCompactCallGraph(TapList<Unit> unitsToDump) throws java.io.IOException {
        Unit unit;
        Unit containerUnit;
        String containerString;
        TapList<CallArrow> callArrows;
        CallArrow arrow;
        boolean hasGlobalArgs;
        TapList<Unit> inUnitsToDump;
        FunctionTypeSpec funcType;
        WrapperTypeSpec[] argumentsTypes;
        inUnitsToDump = unitsToDump;
        TapEnv.println("Nodes :");
        while (inUnitsToDump != null) {
            unit = inUnitsToDump.head;
            TapEnv.print(" [" + unit.rank() + "] ");
            if (unit.isTranslationUnit()) {
                TapEnv.print(" File (\"Translation Unit\") ");
            } else if (unit.isStandard()) {
                TapEnv.print(" Procedure ");
            } else if (unit.isExternal()) {
                TapEnv.print(" External procedure ");
            } else if (unit.isModule()) {
                TapEnv.print(" Module ");
            } else if (unit.isInterface()) {
                TapEnv.print(" Interface ");
            } else if (unit.isVarFunction()) {
                TapEnv.print(" VarFunction ");
            }
            containerUnit = unit.upperLevelUnit();
            if (containerUnit != null) {
                containerString = "";
                while (containerUnit != null) {
                    containerString =
                            containerUnit.name() + "[" + containerUnit.rank() + "]:" + containerString;
                    containerUnit = containerUnit.upperLevelUnit();
                }
                TapEnv.print(containerString);
            }
            unit.cite();
            if (!unit.isModule() && unit.functionTypeSpec() != null) {
                funcType = unit.functionTypeSpec();
                hasGlobalArgs = unit.hasGlobalArgs();
                argumentsTypes = funcType.argumentsTypes;
                TapEnv.print(" ");
                if (argumentsTypes != null) {
                    TapEnv.print("(");
                    for (int i = 0; i < argumentsTypes.length; ++i) {
                        TapEnv.print(argumentsTypes[i] + (i < argumentsTypes.length - 1 ? "," : ""));
                    }
                    if (hasGlobalArgs) {
                        TapEnv.print(" + Globals");
                    }
                    TapEnv.print(")");
                }
                TapEnv.print("=>" + funcType.returnType);
            }
            TapEnv.println();
            inUnitsToDump = inUnitsToDump.tail;
        }
        TapEnv.println();
        TapEnv.println("Call graph :");
        inUnitsToDump = unitsToDump;
        while (inUnitsToDump != null) {
            unit = inUnitsToDump.head;
            callArrows = unit.callees();
            while (callArrows != null) {
                arrow = callArrows.head;
                if (arrow.isCall() &&
                        TapList.contains(unitsToDump, arrow.destination)) {
                    TapEnv.print(" [" + unit.rank() + "] ");
                    unit.cite();
                    TapEnv.print(" calls [" + arrow.destination.rank() + "] ");
                    arrow.destination().cite();
                    TapEnv.println();
                }
                callArrows = callArrows.tail;
            }
            inUnitsToDump = inUnitsToDump.tail;
        }
        TapEnv.println();
        TapEnv.println("Use graph :");
        inUnitsToDump = unitsToDump;
        while (inUnitsToDump != null) {
            unit = inUnitsToDump.head;
            callArrows = unit.callees();
            while (callArrows != null) {
                arrow = callArrows.head;
                if (arrow.isImport()
                        && TapList.contains(unitsToDump, arrow.destination)) {
                    TapEnv.println(" [" + unit.rank() + "] " + unit.name() + " imports ["
                            + arrow.destination.rank() + "] " + arrow.destination.name());
                }
                callArrows = callArrows.tail;
            }
            inUnitsToDump = inUnitsToDump.tail;
        }
    }

    /**
     * @return Some given name for this CallGraph.
     */
    public String name() {
        return callGraphName;
    }

    /**
     * @return main Unit if this CallGraph has a main Unit. Otherwise returns null.
     */
    public Unit getMainUnit() {
        return mainUnit;
    }

    /**
     * Sets "unit" as the main Unit of this CallGraph.
     */
    public void setMainUnit(Unit unit) {
        mainUnit = unit;
    }

    /**
     * @return Number of Units present in this CallGraph
     * Includes procedures, classes, externals, interfaces, but not intrinsics.
     */
    public int nbUnits() {
        return nbUnits;
    }

    /**
     * @return the unordered list of all Units present in this CallGraph.
     */
    public TapList<Unit> units() {
        return units;
    }

    /**
     * @return the list of all top Units in this CallGraph.
     * i.e. the Units of the topmost scope.
     */
    public TapList<Unit> topUnits() {
        return topUnits;
    }

    /**
     * @return the unordered list of all intrinsic Units used in this CallGraph.
     */
    public TapList<Unit> intrinsicUnits() {
        return intrinsicUnits;
    }

    /**
     * Adds the given unit into the list of all intrinsic Units used in this CallGraph.
     */
    public void addIntrinsicUnit(Unit unit) {
        intrinsicUnits = new TapList<>(unit, intrinsicUnits);
    }

    /**
     * @return the unordered list of all interface Units in this CallGraph.
     */
    public TapList<Unit> interfaceUnits() {
        return interfaceUnits;
    }

    /**
     * Sets the unordered list of all interface Units in this CallGraph.
     */
    public void setInterfaceUnits(TapList<Unit> units) {
        interfaceUnits = units;
    }

    /**
     * @return the unordered list of all Fortran2003 Units with bind(c) in this CallGraph.
     */
    public TapList<TapTriplet<String, String, FunctionDecl>> bindFortranCFunctions() {
        return bindFortranCFunctions;
    }

    public void setBindCFortranCFunctions(TapList<TapTriplet<String, String, FunctionDecl>> origBindC) {
        bindFortranCFunctions = origBindC;
    }

    /**
     * @return the Unit at index "rank" (from 0 up) in the current
     * sorted list of all Units of this CallGraph.
     * Sorting is recomputed by each call to sortUnits().
     */
    public Unit sortedUnit(int rank) {
        if (rank >= sortedUnits.length) {
            return null;
        } else {
            return sortedUnits[rank];
        }
    }

    /**
     * @return the ordered list of all Units in this CallGraph.
     */
    public TapList<Unit> sortedUnits() {
        return TapList.fromObjectArray(sortedUnits);
    }

    /**
     * @return a TapList of all computational units in this CallGraph
     * in top-down call order, topmost first.
     */
    public TapList<Unit> sortedComputationalUnits() {
        TapList<Unit> result = null;
        Unit unit;
        for (int i = nbUnits - 1; i >= 0; --i) {
            unit = sortedUnits[i];
            if (unit!=null &&
                    (unit.isStandard() || unit.isOutside())) {
                result = new TapList<>(unit, result);
            }
        }
        return result;
    }

    public TapList<Unit> getRecursivityUnits() {
        return recursivityUnits;
    }

    public SymbolTable languageRootSymbolTable(int language) {
        switch (language) {
            case TapEnv.FORTRAN:
            case TapEnv.FORTRAN90:
            case TapEnv.FORTRAN2003:
                return fortranRootSymbolTable;
            case TapEnv.C:
                return cRootSymbolTable;
            case TapEnv.CUDA:
                return cudaRootSymbolTable;
            case TapEnv.CPLUSPLUS:
                return cPlusPlusRootSymbolTable;
            default:
                return globalRootSymbolTable;
        }
    }

    public void createRootSymbolTables() {
        globalRootSymbolTable = new SymbolTable(null);
        globalRootSymbolTable.setLanguage(TapEnv.UNDEFINED);
        globalRootSymbolTable.setCaseDependent(true);
        globalRootSymbolTable.setShortName("Root SymbolTable");
        globalRootSymbolTable.setIsGlobal();
        globalRootSymbolTable.declarationsBlock = topBlock;
        globalRootSymbolTable.setRootCallGraph(this);
        globalRootSymbolTable.addPrimitiveTypes();
        topBlock.symbolTable = globalRootSymbolTable;

        fortranRootSymbolTable = new SymbolTable(globalRootSymbolTable);
        fortranRootSymbolTable.setLanguage(TapEnv.FORTRAN);
        fortranRootSymbolTable.setCaseDependent(false);
        fortranRootSymbolTable.setShortName("Fortran language SymbolTable");
        fortranRootSymbolTable.setIsGlobal();
        fortranRootSymbolTable.declarationsBlock = new BasicBlock(fortranRootSymbolTable, null, null);
        fortranRootSymbolTable.implicits = new WrapperTypeSpec[26];

        cRootSymbolTable = new SymbolTable(globalRootSymbolTable);
        cRootSymbolTable.setLanguage(TapEnv.C);
        cRootSymbolTable.setCaseDependent(true);
        cRootSymbolTable.setShortName("C language SymbolTable");
        cRootSymbolTable.setIsGlobal();
        cRootSymbolTable.declarationsBlock = new BasicBlock(cRootSymbolTable, null, null);

        cudaRootSymbolTable = new SymbolTable(cRootSymbolTable);
        cudaRootSymbolTable.setLanguage(TapEnv.CUDA);
        cudaRootSymbolTable.setCaseDependent(true);
        cudaRootSymbolTable.setShortName("Cuda language SymbolTable");
        cudaRootSymbolTable.setIsGlobal();
        cudaRootSymbolTable.declarationsBlock = new BasicBlock(cudaRootSymbolTable, null, null);

        cPlusPlusRootSymbolTable = new SymbolTable(globalRootSymbolTable);
        cPlusPlusRootSymbolTable.setLanguage(TapEnv.CPLUSPLUS);
        cPlusPlusRootSymbolTable.setCaseDependent(true);
        cPlusPlusRootSymbolTable.setShortName("C++ language SymbolTable");
        cPlusPlusRootSymbolTable.setIsGlobal();
        cPlusPlusRootSymbolTable.declarationsBlock = new BasicBlock(cPlusPlusRootSymbolTable, null, null);
    }

    public void addTranslationUnitSymbolTable(SymbolTable symbolTable) {
        translationUnitSymbolTables = new TapList<>(symbolTable, translationUnitSymbolTables);
    }

    public TapList<SymbolTable> getTranslationUnitSymbolTables() {
        return translationUnitSymbolTables;
    }

    /**
     * Sets the inputStream to "stream".
     */
    public void setInputStream(TreeProtocol stream) {
        inputStream = stream;
    }

    /** True when the given code contains CUDA operations */
    public boolean hasCuda = false ;

    /**
     * @return the list of all Message-Passing channels.
     */
    public TapList<TapPair<MPIcallInfo, TapIntList>> getAllMessagePassingChannels() {
        return allMessagePassingChannels;
    }

    /**
     * Sets the list of all Message-Passing channels.
     */
    public void setAllMessagePassingChannels(TapList<TapPair<MPIcallInfo, TapIntList>> allChannels) {
        allMessagePassingChannels = allChannels;
    }

    /**
     * @return the list of all the zones representing a message-passing channel.
     */
    public TapIntList getAllMessagePassingChannelZones() {
        TapList<TapPair<MPIcallInfo, TapIntList>> inAllChannels = allMessagePassingChannels;
        TapIntList result = null;
        while (inAllChannels != null) {
            result = TapIntList.append(inAllChannels.head.second, result);
            inAllChannels = inAllChannels.tail;
        }
        return result;
    }

    /**
     * @return true if "zoneNb" represents a message-passing channel.
     */
    public boolean isAMessagePassingChannelZone(int zoneNb) {
        TapList<TapPair<MPIcallInfo, TapIntList>> inAllChannels = allMessagePassingChannels;
        boolean isMPZ = false;
        while (inAllChannels != null && !isMPZ) {
            isMPZ = TapIntList.contains(inAllChannels.head.second, zoneNb);
            inAllChannels = inAllChannels.tail;
        }
        return isMPZ;
    }

//     public boolean isAPtrUtilityZone(int zoneNb) {
//         return zoneNb==zoneNbOfNullDest || zoneNb==zoneNbOfUnknownDest ;
//     }

    /**
     * Create a new intrinsic Unit for this CallGraph.
     */
    public Unit createNewIntrinsicUnit(String name, int language) {
        Unit newUnit = new Unit(this, -1, null, language);
        newUnit.genericArrow = new CallArrow(null, newUnit);
        newUnit.setIntrinsic();
        newUnit.setName(name);
        newUnit.activityPatterns = null;
        addIntrinsicUnit(newUnit);
        return newUnit;
    }

    /**
     * Create a new Unit into this CallGraph, contained in upperLevelUnit.
     */
    public Unit createNewUnit(Unit upperLevelUnit, int language) {
        Unit newUnit = new Unit(this, nbUnits, upperLevelUnit, language);
        units = new TapList<>(newUnit, units);
        attachToUpperUnit(newUnit, upperLevelUnit);
        if (TapEnv.inIncludeFile()) {
            newUnit.setFromInclude(
                    Instruction.fromIncludeCopy(TapEnv.currentIncludeInstruction(), true));
        }
        ++nbUnits;
        return newUnit;
    }

    /**
     * Re-attach the given Unit to the given new upper-level unit.
     */
    public void detachFromUpperUnit(Unit unit) {
        if (unit.upperLevelUnit() != null) {
            unit.upperLevelUnit().lowerLevelUnits =
                    TapList.delete(unit, unit.upperLevelUnit().lowerLevelUnits);
            delCallArrow(unit.upperLevelUnit(), SymbolTableConstants.CONTAINS, unit);
        } else {
            topUnits = TapList.delete(unit, topUnits);
        }
        unit.setUpperLevelUnit(null);
    }

    public void attachToUpperUnit(Unit unit, Unit upper) {
        unit.setUpperLevelUnit(upper);
        if (upper == null) {
            topUnits = new TapList<>(unit, topUnits);
        } else {
            upper.lowerLevelUnits =
                    new TapList<>(unit, upper.lowerLevelUnits);
            addCallArrow(upper, SymbolTableConstants.CONTAINS, unit);
        }
    }

    /**
     * Delete the given unit, and all its lower-level Units, from this CallGraph.
     */
    public void deleteUnit(Unit unit) {
//         TapEnv.printlnOnTrace("UNITS:" + units + " (NB:" + nbUnits + ") BEFORE DELETE:" + unit + "@" + Integer.toHexString(unit.hashCode()) + (unit.upperLevelUnit() == null ? "" : " INSIDE UPPER:" + unit.upperLevelUnit().lowerLevelUnits));
        // Make sure never to delete a Unit twice !
        if (unit.isIntrinsic() || !unit.wasDeleted()) {
            if (!unit.isIntrinsic()) {
                unit.setDeleted();
            }
            if (!unit.isInterface()) {
                deleteBindFortranCFunction(unit);
            }
            TapList<Unit> lowerUnits = unit.lowerLevelUnits;
            while (lowerUnits != null) {
                deleteUnit(lowerUnits.head);
                lowerUnits = lowerUnits.tail;
            }
            unit.lowerLevelUnits = null;
            if (TapEnv.relatedUnit() == unit) {
                TapEnv.popRelatedUnit();
            }
            TapList<PositionAndMessage> msgs = unit.messages.tail;
            if (msgs != null) {
                while (msgs != null) {
                    msgs.head.setPosition(-1);
                    msgs = msgs.tail;
                }
                TapEnv.get().danglingMessages = TapList.append(
                        TapEnv.get().danglingMessages, unit.messages.tail);
                unit.messages.tail = null;
            }
            if (unit.upperLevelUnit() != null) {
                unit.upperLevelUnit().lowerLevelUnits =
                        TapList.delete(unit, unit.upperLevelUnit().lowerLevelUnits);
            } else {
                topUnits = TapList.delete(unit, topUnits);
            }
            if (unit.callers != null) {
                for (TapList<CallArrow> callerArrows = unit.callers;
                     callerArrows != null;
                     callerArrows = callerArrows.tail) {
                    CallArrow callerArrow = callerArrows.head;
                    if (callerArrow.origin != null) {
                        callerArrow.origin.callees = TapList.delete(callerArrow, callerArrow.origin.callees);
                    }
                }
            }
            units = TapList.delete(unit, units);
            unit.setRank(-1);
            if (!unit.isIntrinsic()) {
                --nbUnits;
//                 TapEnv.printlnOnTrace("       RELEASED rk:" + nbUnits);
            }
//             TapEnv.printlnOnTrace("UNITS:" + units + " (NB:" + nbUnits + ") AFTER DELETE:"
//                     + unit + "@" + Integer.toHexString(unit.hashCode()) + (unit.upperLevelUnit() == null ? "" : " INSIDE UPPER:"
//                     + unit.upperLevelUnit().lowerLevelUnits));
        }
    }

    /**
     * Delete the declaration of the given unit from the SymbolTable that contains these declarations.
     */
    public void deleteUnitFromSymbolTable(Unit unit) {
        SymbolTable unitSymbolTable = unit.publicSymbolTable();
        if (unitSymbolTable == null) {
            unitSymbolTable = languageRootSymbolTable(unit.language());
        }
        unitSymbolTable.removeDecl(unit.name(), SymbolTableConstants.FUNCTION,
                false);
    }

    public void resetPreviousFunctionDeclAndNewUnit() {
        previousFunctionDeclAndNewUnitList = null;
    }

// Following code about final sorting of File Units, if code must be placed into a single file:

    public TapTriplet<FunctionDecl, Unit, TapPair<FunctionDecl, SymbolTable>>
    getPreviousFunctionDeclAndNewUnit(String name) {
        TapList<TapTriplet<FunctionDecl, Unit, TapPair<FunctionDecl, SymbolTable>>> list
                = previousFunctionDeclAndNewUnitList;
        TapTriplet<FunctionDecl, Unit, TapPair<FunctionDecl, SymbolTable>> previousFunctionDeclAndNewUnit = null;
        while (list != null && previousFunctionDeclAndNewUnit == null) {
            if (previousFunctionDeclAndNewUnitList.head.first.symbol.equals(name)) {
                previousFunctionDeclAndNewUnit = previousFunctionDeclAndNewUnitList.head;
            }
            list = list.tail;
        }
        return previousFunctionDeclAndNewUnit;
    }

    public void setPreviousFunctionDeclAndNewUnit(FunctionDecl oldFunctionDecl, Unit newUnit) {
//         if (previousFunctionDeclAndNewUnitList == null) {
        previousFunctionDeclAndNewUnitList = new TapList<>(
                new TapTriplet<>(
                        oldFunctionDecl, newUnit, null),
                previousFunctionDeclAndNewUnitList);
//                     null);
//         }
    }

//     public void fillPreviousFunctionDeclAndNewUnit(FunctionDecl funcDecl, SymbolTable symbTable) {
//         TapTriplet<FunctionDecl, Unit, TapPair<FunctionDecl, SymbolTable>> previousFunctionDeclAndNewUnit
//                 = getPreviousFunctionDeclAndNewUnit(funcDecl.symbol);
//         if (previousFunctionDeclAndNewUnit != null) {
//             previousFunctionDeclAndNewUnit.third = new TapPair<>(funcDecl, symbTable);
//         }
//     }

    public void absorbAndDeleteAfterCreateNewUnit() {
        TapTriplet<FunctionDecl, Unit, TapPair<FunctionDecl, SymbolTable>> previousFunctionDeclAndNewUnit;
        while (previousFunctionDeclAndNewUnitList != null) {
            previousFunctionDeclAndNewUnit = previousFunctionDeclAndNewUnitList.head;
            FunctionDecl oldFunctionDecl = previousFunctionDeclAndNewUnit.first;
            Unit finalUnit = previousFunctionDeclAndNewUnit.second;
            TapPair<FunctionDecl, SymbolTable> newPair = previousFunctionDeclAndNewUnit.third;
            solveAndRemoveInterface(new TapList<>(oldFunctionDecl, null), newPair == null ? null : newPair.first,
                    finalUnit, newPair == null ? null : newPair.second);
            previousFunctionDeclAndNewUnitList = previousFunctionDeclAndNewUnitList.tail;
        }
    }

    public void fixRemainingInterfaces() {
// System.out.println("NON FINAL CG UNITS:"+units()) ;
// System.out.println("NON FINAL CG INTERFACEUNITS:"+interfaceUnits()) ;
        Unit interfaceUnit, trueUnit, enclosingUnit ;
        String interfaceName ;
        FunctionDecl interfaceDecl ;
        TapList<Unit> toInterfaces = new TapList<> (null, interfaceUnits) ;
        TapList<Unit> inInterfaces = toInterfaces ;
        TapList<Unit> inUnits ;
        while (inInterfaces.tail != null) {
            interfaceUnit = inInterfaces.tail.head ;
            interfaceName = interfaceUnit.name() ;
            enclosingUnit = interfaceUnit.enclosingUnitOfInterface ;
            interfaceDecl = (FunctionDecl)enclosingUnit.publicSymbolTable().getDecl(
                                                  interfaceName, SymbolTableConstants.FUNCTION, true);
            if (interfaceDecl==null)
                interfaceDecl = (FunctionDecl)enclosingUnit.privateSymbolTable().getDecl(
                                                      interfaceName, SymbolTableConstants.FUNCTION, true);
// System.out.println("  REMAINING INTERFACE UNIT:"+interfaceUnit+" TYPE:"+interfaceUnit.functionTypeSpec()+" DECL:"+interfaceDecl) ;
            trueUnit = null ;
            // 1st try, strict match on name and type signature:
            inUnits = units ;
            while (trueUnit==null && inUnits!=null) {
                if (inUnits.head!=null && !inUnits.head.wasDeleted() && !inUnits.head.isInterface() && !inUnits.head.isVarFunction()
                    && interfaceName.equals(inUnits.head.name())) {
// System.out.println("    CANDIDATE TRUE UNIT:"+inUnits.head+" TYPE:"+inUnits.head.functionTypeSpec()) ;
                    if (interfaceUnit.functionTypeSpec().receives(inUnits.head.functionTypeSpec(), null, null)) {
                        trueUnit = inUnits.head ;
                    }
                }
                inUnits = inUnits.tail ;
            }
            // 2nd try, laxist match on name only:
            inUnits = units ;
            while (trueUnit==null && inUnits!=null) {
                if (inUnits.head!=null && !inUnits.head.wasDeleted() && !inUnits.head.isInterface() && !inUnits.head.isVarFunction()
                    && interfaceName.equals(inUnits.head.name())) {
                    trueUnit = inUnits.head ;
                }
                inUnits = inUnits.tail ;
            }
// System.out.println("    MATCH:"+trueUnit) ;
            if (trueUnit!=null) {

                if (trueUnit != interfaceDecl.unit()) {
                    trueUnit.absorb(interfaceUnit);
                    deleteUnit(interfaceUnit);
                    interfaceDecl.setUnit(trueUnit);
                    interfaceDecl.setNoneInstruction();
                }

                inInterfaces.tail = inInterfaces.tail.tail ;
            } else {
                inInterfaces = inInterfaces.tail ;
            }
        }
        interfaceUnits = toInterfaces.tail ;
// System.out.println("FINAL CG INTERFACEUNITS:"+interfaceUnits()) ;
// System.out.println("FINAL CG UNITS:"+units()) ;
    }

// End code about final sorting of File Units.

    private void solveAndRemoveInterface(TapList<FunctionDecl> waitingDecls, FunctionDecl finalDecl, Unit finalUnit,
                                         SymbolTable hostSymbolTable) {
        TapList<Unit> alreadyDeletedUnits = null;
        while (waitingDecls != null) {
            FunctionDecl oldFunctionDecl = waitingDecls.head;
            if (finalUnit != oldFunctionDecl.unit()) {
                finalUnit.absorb(oldFunctionDecl.unit());
                deleteUnit(oldFunctionDecl.unit());
                alreadyDeletedUnits = new TapList<>(oldFunctionDecl.unit(), alreadyDeletedUnits);
                oldFunctionDecl.setUnit(finalUnit);
                oldFunctionDecl.setNoneInstruction();
            }
            waitingDecls = waitingDecls.tail;
        }
        if (finalDecl != null) {
            hostSymbolTable.addSymbolDecl(finalDecl);
            // In C, a new non-private unit must be declared in the sharingSymbolTable too
            if (hostSymbolTable == TapEnv.currentTranslationUnitSymbolTable() && !finalUnit.isPrivate()
                    && hostSymbolTable.basisSymbolTable() == cRootSymbolTable) {
                // Copied from SymbolTable.java: refactor this!
                TapList<FunctionDecl> existingDecls = cRootSymbolTable.getTopFunctionDecl(finalUnit.name(), null, null, null, false);
                FunctionDecl sharedDecl = existingDecls == null ? null : existingDecls.head;
                if (sharedDecl == null) {
                    // insert a copy with shared info into cRootSymbolTable:
                    sharedDecl = new FunctionDecl(ILUtils.build(ILLang.op_ident, finalUnit.name()), finalUnit);
                    sharedDecl.shareUnitFrom(finalDecl);
                    sharedDecl.setZones(finalDecl.zones());
                    sharedDecl.isATrueSymbolDecl = finalDecl.isATrueSymbolDecl;
                    cRootSymbolTable.addSymbolDecl(sharedDecl);
                } else {
                    // retrieve shared info from the shared copy from cRootSymbolTable:
                    if (finalUnit != sharedDecl.unit()) {
                        Unit unitToRemove = sharedDecl.unit();
                        finalUnit.absorb(unitToRemove);
                        // Maybe absorb() should care more about the "kind" of the 2 merged Units ?
                        sharedDecl.setUnit(finalUnit);
                        if (!TapList.contains(alreadyDeletedUnits, unitToRemove)) {
                            this.deleteUnit(unitToRemove);
                        }
                    }
                    finalDecl.shareUnitFrom(sharedDecl);
                }
                // end Copied from SymbolTable.java: refactor this!
            }
        }
    }

    /**
     * Detect Recursivity initialization.
     */
    private static final int SORT_INIT = 0;
    /**
     * Detecting recursivity.
     */
    private static final int SORT_RECURSIVITY = 1;
    /**
     * No recursivity.
     */
    private static final int SORT_OK = 2;

    /**
     * Look for recusivity in this CallGraph. Sends messages.
     *
     * @param inModule true for recursivity between modules.
     */
    private void detectRecursivity(boolean inModule) {
        TapList<Unit> allUnits = units;
        Unit unit;
        while (allUnits != null) {
            allUnits.head.sortRk = SORT_INIT;
            allUnits = allUnits.tail;
        }
        allUnits = units;
        while (allUnits != null) {
            unit = allUnits.head;
            if (unit.isStandard() || inModule && unit.isModule()) {
                detectRecursivityRecFrom(unit, null, inModule);
            }
            allUnits = allUnits.tail;
        }
    }

    /**
     * Calls by detectRecursivity.
     *
     * @param unit     current visited unit.
     * @param visiting visiting units list.
     * @param inModule true for recursivity between modules.
     * @return true if recursivity in the callgraph.
     */
    private boolean detectRecursivityRecFrom(Unit unit, TapList<Unit> visiting, boolean inModule) {
        boolean result = false;
        if (unit.isStandard() || inModule && unit.isModule()) {
            switch (unit.sortRk) {
                case SORT_OK: {
                    break;
                }
                case SORT_RECURSIVITY: {
                    TapList<Unit> inVisiting = visiting;
                    String recurChain = unit.name();
                    TapList<Unit> recurChainUnit = new TapList<>(unit, null);
                    boolean recurChainContainsModule = false;
                    assert inVisiting != null;
                    while (inVisiting.head != unit) {
                        if (inVisiting.head.isModule()) {
                            recurChainContainsModule = true;
                        }
                        recurChain = inVisiting.head.name() + "-->" + recurChain;
                        recurChainUnit = new TapList<>(inVisiting.head, recurChainUnit);
                        inVisiting = inVisiting.tail;
                    }
                    TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC42) Recursivity: " + unit.name() + "-->" + recurChain);
                    recursivityUnits = TapList.prependNoDups(recurChainUnit, recursivityUnits);
                    if (inModule && recurChainContainsModule) {
                        result = true;
                    }
                    break;
                }
                case SORT_INIT: {
                    unit.sortRk = 1;
                    visiting = new TapList<>(unit, visiting);
                    TapList<CallArrow> arrows = unit.callees();
                    CallArrow arrow;
                    while (arrows != null) {
                        arrow = arrows.head;
                        if (arrow.isCall() || inModule) {
                            if (detectRecursivityRecFrom(arrow.destination, visiting, inModule)) {
                                result = true;
                            }
                        }
                        arrows = arrows.tail;
                    }
                    if (inModule && unit.isModule()) {
                        // pour un module contenant une interface:
                        TapList<SymbolDecl> functionDecls = unit.publicSymbolTable().getAllTopFunctionDecls();
                        functionDecls = TapList.append(unit.privateSymbolTable().getAllTopFunctionDecls(), functionDecls);
                        Unit declUnit;
                        while (functionDecls != null) {
                            declUnit = ((FunctionDecl) functionDecls.head).unit();
                            if (detectRecursivityRecFrom(declUnit, visiting, inModule)) {
                                result = true;
                            }
                            functionDecls = functionDecls.tail;
                        }
                    }
                    unit.sortRk = 2;
                    break;
                }
                default:
                    break;
            }
        }
        return result;
    }

    /**
     * @return a reordered list of the Translation Units in "fileUnits",
     * ordered so that the USE'd and CALL'ed Units occur before their usage.
     * This is useful if the user wants all code generated in a single file.
     */
    public TapList<Unit> orderedFiles() {
        TapList<CallGraph.FileUnit> fileUnits = null;
        Unit unit;
        Unit nextUnit;
        Unit topUnit;
        Unit nextTopUnit;
        CallGraph.FileUnit fileUnit;
        CallGraph.FileUnit nextFileUnit;
        // Collect all "file" Units into a TapList<FileUnit>:
        for (TapList<Unit> allUnits = units; allUnits != null; allUnits = allUnits.tail) {
            unit = allUnits.head;
            if (unit.isTranslationUnit()) {
                fileUnits = new TapList<>(new CallGraph.FileUnit(unit), fileUnits);
            }
        }
        // Set all dependencies between the FileUnit's:
        for (TapList<Unit> allUnits = units; allUnits != null; allUnits = allUnits.tail) {
            unit = allUnits.head;
            if (!unit.isTranslationUnit() && (unit.isStandard() || unit.isPackage())) {
                topUnit = unit.upperLevelUnit();
                while (!topUnit.isTranslationUnit()) {
                    topUnit = topUnit.upperLevelUnit();
                }
                fileUnit = getFileUnit(topUnit, fileUnits);
                for (TapList<Unit> nextUnits = collectNextUnits(unit, true, unit.isFortran() ? 0 : 1, 1, 0);
                     nextUnits != null;
                     nextUnits = nextUnits.tail) {
                    nextUnit = nextUnits.head;
                    if (!nextUnit.isTranslationUnit() && (nextUnit.isStandard() || nextUnit.isPackage())) {
                        nextTopUnit = nextUnit.upperLevelUnit();
                        while (!nextTopUnit.isTranslationUnit()) {
                            nextTopUnit = nextTopUnit.upperLevelUnit();
                        }
                        nextFileUnit = getFileUnit(nextTopUnit, fileUnits);
                        // nextFileUnit may be null in the case of a CallArrow that goes from the diff CallGraph
                        //  to the orig CallGraph. cf "Danger [19Mar18]" in CallGraphDifferentiator.
                        if (nextFileUnit != null && nextFileUnit != fileUnit && !TapList.contains(fileUnit.usedFiles, nextFileUnit)) {
                            fileUnit.usedFiles = new TapList<>(nextFileUnit, fileUnit.usedFiles);
                        }
                    }
                }
            }
        }
        // Initialize sorting (set visting status to "not visited yet"):
        for (TapList<CallGraph.FileUnit> inFileUnits = fileUnits; inFileUnits != null; inFileUnits = inFileUnits.tail) {
            inFileUnits.head.rank = 0;
        }
        TapList<Unit> toVisited = new TapList<>(null, null);
        // Sort: traverse in DFST order and collect sorted Unit's into toVisited:
        for (TapList<CallGraph.FileUnit> inFileUnits = fileUnits; inFileUnits != null; inFileUnits = inFileUnits.tail) {
            sortFileUnitsRec(inFileUnits.head, toVisited);
        }
        return TapList.nreverse(toVisited.tail);
    }

    private CallGraph.FileUnit getFileUnit(Unit unit, TapList<CallGraph.FileUnit> fileUnits) {
        CallGraph.FileUnit found = null;
        while (found == null && fileUnits != null) {
            if (fileUnits.head.unit == unit) {
                found = fileUnits.head;
            }
            fileUnits = fileUnits.tail;
        }
        return found;
    }

    private void sortFileUnitsRec(CallGraph.FileUnit fileUnit, TapList<Unit> toVisited) {
        switch (fileUnit.rank) {
            case 2:
                break;
            case 1:
                TapEnv.fileWarning(TapEnv.MSG_WARN, null, "Circular dependence in generated file");
                break;
            case 0:
                // first encounter: set state to "visiting":
                fileUnit.rank = 1;
                for (TapList<CallGraph.FileUnit> nextFiles = fileUnit.usedFiles; nextFiles != null; nextFiles = nextFiles.tail) {
                    sortFileUnitsRec(nextFiles.head, toVisited);
                }
                // set state to "visited":
                fileUnit.rank = 2;
                toVisited.tail = new TapList<>(fileUnit.unit, toVisited.tail);
                break;
            default:
                break;
        }
    }

    /**
     * Sorts into field "sortedUnits" all Unit's of this CallGraph,
     * following the sorting criteria defined by "callsDir", "importsDir", and "containsDir".
     * When a CallArrow from A to B says that "A xxxx'es B", then:
     * if xxxxsDir is 1 then A will be before B;
     * if xxxxsDir is -1 then B will be before A;
     * if xxxxsDir is 0, this doesn't constrain A B order.
     * When order is free, places the Units by increasing original "positions".
     * Re-assigns the ranks of the Units according to this order.
     */
    public void sortUnits(boolean keepAllUnits,
                          int callsDir, int importsDir, int containsDir) {
        TapList<Unit> visited;
        TapList<Unit> toAllUnits = new TapList<>(null, units);
        TapList<Unit> allUnits = toAllUnits;
        Unit unit;
        int i;

        // Initialize "rank" marker. Throw away unused Units if keepAllUnits==false :
        while (allUnits.tail != null) {
            unit = allUnits.tail.head;
            if (!keepAllUnits
                && !unit.isPackage()
                && !(unit.isVarFunction() && unit.callers() != null)
                && (unit.functionTypeSpec() == null
                    || unit.functionTypeSpec().argumentsTypes == null)) {
                // if this unknown (external) function is used nowhere, throw it away:
                unit.setRank(-1);
                allUnits.tail = allUnits.tail.tail;
                nbUnits--;
            } else {
                // otherwise set unit state to "not visited yet":
                unit.setRank(0);
                allUnits = allUnits.tail;
            }
        }

        // Actual sorting, recursive call to sortUnitsRec() :
        units = toAllUnits.tail;
        allUnits = sortDecreasingPositions(units);
        debuglevel = 0;
        visited = new TapList<>(null, null);
        while (allUnits != null) {
            unit = allUnits.head;
            if (unit.rank() != -1) {
                sortUnitsRec(unit, true, null, visited,
                        callsDir, importsDir, containsDir);
            }
            allUnits = allUnits.tail;
        }
        visited = visited.tail;
        if (nbUnits < 0) {
            nbUnits = 0;
        }
        sortedUnits = new Unit[nbUnits];
        for (i=0 ; i<nbUnits && visited!=null ; ++i) {
            sortedUnits[i] = visited.head;
            sortedUnits[i].setRank(i);
            visited = visited.tail;
        }

        // sort topUnits and lowerLevelUnits :
        topUnits = sortByIncreasingRank(topUnits);
        for (i = 0; i < nbUnits; ++i) {
            if (sortedUnits[i] != null) {
                sortedUnits[i].lowerLevelUnits =
                        sortByIncreasingRank(sortedUnits[i].lowerLevelUnits);
            }
        }
        units = sortByIncreasingRank(units);
    }

    /**
     * @return a sorted list of all the units found in the "units" field of this CallGraph,
     * following the sorting criteria defined by "callsDir", "importsDir", and "containsDir".
     * When a CallArrow from A to B says that "A xxxx'es B", then:
     * if xxxxsDir is 1 then A will be before B;
     * if xxxxsDir is -1 then B will be before A;
     * if xxxxsDir is 0, this doensn't constrain A B order.
     * When order is free, places the Units by increasing original "positions".
     * Beware of code duplication with sortUnits() !!
     */
    public TapList<Unit> buildSortedUnitsList(
            int callsDir, int importsDir, int containsDir) {
        TapList<Unit> allUnits = units;
        while (allUnits != null) {
            allUnits.head.sortRk = 0;
            allUnits = allUnits.tail;
        }
        TapList<Unit> visited = new TapList<>(null, null);
        allUnits = sortDecreasingPositions(units);
        while (allUnits != null) {
            sortUnitsRec(allUnits.head, false, null, visited,
                    callsDir, importsDir, containsDir);
            allUnits = allUnits.tail;
        }
        return visited.tail;
    }

    /**
     * When boolean inrk is true, use "unit.rank" as marker, otherwise use "unit.sortRk".
     */
    private void sortUnitsRec(Unit unit, boolean inrk,
                              TapList<Unit> visiting, TapList<Unit> visited,
                              int callsDir, int importsDir, int containsDir) {
        ++debuglevel;
        Unit nextUnit;
        switch (inrk ? unit.rank() : unit.sortRk) {
            case 2:
                // means the unit is in "visited" state:
                break;
            case 1:
                // means the unit is in "visiting" state:
                break;
            case 0: {
                // means this is first encounter of unit. Set unit state to "visiting":
                if (inrk) {
                    unit.setRank(1);
                } else {
                    unit.sortRk = 1;
                }
                TapList<Unit> aroundUnit = new TapList<>(unit, unit.enclosingUnits());
                TapList<Unit> nextUnits = collectNextUnits(unit, false, callsDir, importsDir, containsDir);
                while (nextUnits != null) {
                    nextUnit = nextUnits.head;
                    while (nextUnit != null && !nextUnit.isTopInFile()
                            && !TapList.contains(aroundUnit, nextUnit.upperLevelUnit())) {
                        nextUnit = nextUnit.upperLevelUnit();
                    }
                    if (nextUnit != null) {
                        sortUnitsRec(nextUnit, inrk, new TapList<>(unit, visiting),
                                visited, callsDir, importsDir, containsDir);
                    }
                    nextUnits = nextUnits.tail;
                }
                // Set unit state to "visited", and add it at the head of the result "visited" list:
                if (inrk) {
                    unit.setRank(2);
                } else {
                    unit.sortRk = 2;
                }
                visited.tail = new TapList<>(unit, visited.tail);
                break;
            }
            default:
                break;
        }
        --debuglevel;
    }

    public void decideForwardDeclarations() {
        SymbolTable tuST;
        TapList<Unit> alreadyDeclared;
        for (TapList<SymbolTable> tuSTs = translationUnitSymbolTables; tuSTs != null; tuSTs = tuSTs.tail) {
            tuST = tuSTs.head;
            if (TapEnv.isC(tuST.language())) {
                tuST.privateForwardDeclarations = null;
                tuST.nonPrivForwardDeclarations = null;
                alreadyDeclared = checkUnitsDefUse(topBlock, null, null);
                if (cRootSymbolTable != null) {
                    alreadyDeclared = checkUnitsDefUse(cRootSymbolTable.declarationsBlock, alreadyDeclared, null);
                }
// I believe tuST.declarationsBlock is also in tuST.unit.allBlocks:
//                 alreadyDeclared = checkUnitsDefUse(tuST.declarationsBlock, alreadyDeclared, tuST) ;
                for (TapList<Block> topBlocks = tuST.unit.allBlocks; topBlocks != null; topBlocks = topBlocks.tail) {
                    alreadyDeclared = checkUnitsDefUse(topBlocks.head, alreadyDeclared, tuST);
                }
            }
        }
    }

    private TapList<Unit> checkUnitsDefUse(Block block, TapList<Unit> forwardDeclared, SymbolTable tuSymbolTable) {
        if (block != null) {
            TapList<Instruction> instructions = block.instructions;
            Instruction instr;
            while (instructions != null) {
                instr = instructions.head;
                Unit instrUnit = instr.isUnitDefinitionStub();
                if (tuSymbolTable != null) {
                    TapList<Unit> usedUnits;
                    if (instrUnit != null) {
                        usedUnits = collectNextUnits(instrUnit, true, 1, 0, 0);
                    } else {
                        usedUnits = Instruction.usedUnits(instr.tree, null, block.symbolTable);
                    }
                    while (usedUnits != null) {
                        Unit usedUnit = usedUnits.head;
                        if (!TapList.contains(forwardDeclared, usedUnit)) {
                            if (usedUnit.isPrivate()) {
                                if (!TapList.contains(tuSymbolTable.privateForwardDeclarations, usedUnit)) {
                                    tuSymbolTable.privateForwardDeclarations =
                                            new TapList<>(usedUnit, tuSymbolTable.privateForwardDeclarations);
                                }
                            } else {
                                if (!TapList.contains(tuSymbolTable.nonPrivForwardDeclarations, usedUnit)) {
                                    tuSymbolTable.nonPrivForwardDeclarations =
                                            new TapList<>(usedUnit, tuSymbolTable.nonPrivForwardDeclarations);
                                }
                            }
                        }
                        usedUnits = usedUnits.tail;
                    }
                }
                TapList<Unit> declaredUnits;
                if (instrUnit != null) {
                    declaredUnits = new TapList<>(instrUnit, null);
                } else {
                    declaredUnits = instr.declaresUnits(block.symbolTable);
                }
                while (declaredUnits != null) {
                    if (!TapList.contains(forwardDeclared, declaredUnits.head)) {
                        forwardDeclared = new TapList<>(declaredUnits.head, forwardDeclared);
                    }
                    declaredUnits = declaredUnits.tail;
                }
                instructions = instructions.tail;
            }
        }
        return forwardDeclared;
    }

    /**
     * returns the TapList of Unit's that follow after "unit",
     * according to the sorting criteria defined by "callsDir", "importsDir", and "containsDir".
     *
     * @param notWhenForwardDecl when true, neglect ordering "A calls B" when B has a forward declaration.
     * @return the Units in decreasing "position" order, so that the "sorting" that calls
     * this function will prefer an order with INCREASING positions.
     */
    private TapList<Unit> collectNextUnits(Unit unit, boolean notWhenForwardDecl, int callsDir,
                                           int importsDir, int containsDir) {
        TapList<Unit> toResult = new TapList<>(null, null);
        TapList<Unit> forwardDeclared = null;
        if (notWhenForwardDecl) {
            // Collect the Unit's that are defined in the preliminary "topLevel" code of this unit's file:
            forwardDeclared = checkUnitsDefUse(topBlock, null, null);
            if (cRootSymbolTable != null) {
                forwardDeclared = checkUnitsDefUse(cRootSymbolTable.declarationsBlock, forwardDeclared, null);
            }
            forwardDeclared = checkUnitsDefUse(unit.translationUnitSymbolTable().declarationsBlock, forwardDeclared, null);
        }
        TapList<CallArrow> arrows = unit.callees();
        while (arrows != null) {
            if (arrows.head.followsDirections(callsDir, importsDir, containsDir, forwardDeclared)) {
                insertDecreasingPositions(arrows.head.destination, toResult);
            }
            arrows = arrows.tail;
        }
        arrows = unit.callers();
        while (arrows != null) {
            if (arrows.head.followsDirections(-callsDir, -importsDir, -containsDir, forwardDeclared)) {
                insertDecreasingPositions(arrows.head.origin, toResult);
            }
            arrows = arrows.tail;
        }
        return toResult.tail;
    }

    private TapList<Unit> sortDecreasingPositions(TapList<Unit> unitTapList) {
        TapList<Unit> toResult = new TapList<>(null, null);
        while (unitTapList != null) {
            insertDecreasingPositions(unitTapList.head, toResult);
            unitTapList = unitTapList.tail;
        }
        return toResult.tail;
    }

    private void insertDecreasingPositions(Unit newUnit, TapList<Unit> toListUnits) {
        int newPos = newUnit.getPosition();
        while (toListUnits.tail != null && toListUnits.tail.head.getPosition() > newPos) {
            toListUnits = toListUnits.tail;
        }
        toListUnits.tail = new TapList<>(newUnit, toListUnits.tail);
    }

    /**
     * @return the sorted list of Unit by increasing rank.
     */
    private TapList<Unit> sortByIncreasingRank(TapList<Unit> unitTapList) {
        TapList<Unit> sortedList = new TapList<>(null, null);
        TapList<Unit> insortedList;
        int rank;
        while (unitTapList != null) {
            rank = unitTapList.head.rank();
            insortedList = sortedList;
            while (insortedList.tail != null
                    && insortedList.tail.head.rank() < rank) {
                insortedList = insortedList.tail;
            }
            insortedList.placdl(unitTapList.head);
            unitTapList = unitTapList.tail;
        }
        return sortedList.tail;
    }

    /**
     * Final preparation of the CallGraph.
     * Prepare the types. Set all the remaining CallArrows.
     * Prepare the zones, zone numbering, zone translations.
     * Must be done after all parsing and before all analyses.
     */
    public void terminateTypesAndZones() {
        Unit unit;
        Unit iUnit;
        int nbSortedUserUnits = nbUnits;

        if (TapEnv.get().traceTypeCheck != null && TapEnv.get().traceTypeCheck.head.equals("%all%")) {
            TapEnv.setTraceTypeCheckAnalysis(true);
        }

        // Fix global declared variables with incomplete type:
        if (fortranRootSymbolTable != null) {
            fortranRootSymbolTable.fixImplicits();
        }

        // This sorting is partial because only "imports" and "contains" CallArrow's are set!(ChannelInfo)
        sortUnits(true, 0, -1, 1);
        // Must be done for the importED modules before the importER modules:
        for (int i = 0; i < nbSortedUserUnits; ++i) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
            unit.importAllSymbolTables();
            unit.importInheritedClassesSymbolTables();
        }

        for (int i = 0; i < nbSortedUserUnits; ++i) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
            unit.declareAllSymbolDecls();
            unit.solveTypeExtends();
        }

        TapEnv.printlnOnTrace(15, "@@ Checking types for global declarations") ;
        globalRootSymbolTable.checkTypeSpecValidity();
        topBlock.typeCheck(globalRootSymbolTable);
        TapEnv.setRelatedLanguage(TapEnv.FORTRAN);
        fortranRootSymbolTable.declarationsBlock.typeCheck(fortranRootSymbolTable);
        fortranRootSymbolTable.checkTypeSpecValidity();
        TapEnv.setRelatedLanguage(TapEnv.C);
        cRootSymbolTable.declarationsBlock.typeCheck(cRootSymbolTable);
        cRootSymbolTable.checkTypeSpecValidity();
        TapEnv.setRelatedLanguage(TapEnv.CUDA);
        cudaRootSymbolTable.declarationsBlock.typeCheck(cudaRootSymbolTable);
        cudaRootSymbolTable.checkTypeSpecValidity();
        TapEnv.setRelatedLanguage(TapEnv.CPLUSPLUS);
        cPlusPlusRootSymbolTable.declarationsBlock.typeCheck(cPlusPlusRootSymbolTable);
        cPlusPlusRootSymbolTable.checkTypeSpecValidity();
        TapList<SymbolTable> tuSTs = translationUnitSymbolTables;
        while (tuSTs != null) {
            TapEnv.printlnOnTrace(15, "@@ Checking types for file "+tuSTs.head.unit.name()) ;
            TapEnv.setRelatedLanguage(tuSTs.head.language());
            tuSTs.head.declarationsBlock.typeCheck(tuSTs.head);
            tuSTs.head.checkTypeSpecValidity();
            tuSTs = tuSTs.tail;
        }
        TapEnv.setRelatedLanguage(null);

        TapEnv.setTraceTypeCheckAnalysis(false);
        TapEnv.get().tracedTypeCheckUnits = getUnits(TapEnv.get().traceTypeCheck);

        // fixImplicit pour une Unit doit etre fait avant le typeCheck des calls
        // de cette unit sinon pb comme dans set06/v334
        // on infere des types pour des parametres qui sont correctement declare's
        // par un implicit
        // fixImplicit doit etre fait apres importAllSymbolTables des modules
        // importe's
        for (int i = 0; i < nbSortedUserUnits; ++i) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
            if (unit.isStandard() || unit.isIntrinsic() || unit.isModule() || unit.isInterface()) {
                // then fix declared variables with incomplete type:
                unit.fixImplicits();
                unit.exportFunctionTypeSpec();
            }
        }
        TapEnv.setRelatedUnit(null);

        TapEnv.setTraceInlineAnalysis(false);
        TapEnv.get().tracedInlineUnits = getUnits(TapEnv.get().traceInline);

        TapList<Block> blocks;
        TapList<Instruction> instructions;
        Instruction instr;
        for (int i=0 ; i<nbSortedUserUnits ; ++i) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
            if (unit.isStandard() || unit.isIntrinsic() || unit.isModule() || unit.isClass()) {
                unit.typeCheck();
                // statement function, inline, expressions-split, etc...
                unit.preprocess();
                // now analyse all directives in unit:
                Directive.analyzeUnitDirectives(unit, unit.preComments);
                Directive.analyzeUnitDirectives(unit, unit.preCommentsBlock);
                blocks = unit.allBlocks();
                while (blocks != null) {
                    instructions = blocks.head.instructions;
                    while (instructions != null) {
                        instr = instructions.head;
                        Directive.analyzeInstructionDirectives(instr, instr.preComments);
                        Directive.analyzeInstructionDirectives(instr, instr.preCommentsBlock);
                        instructions = instructions.tail;
                    }
                    blocks = blocks.tail;
                }
                // now look at MPI message-passing calls and channels:
                unit.checkMessagePassingCalls(this);
            } else if (unit.isInterface()) {
                unit.typeCheckInterface();
            }

            // pour toutes les units on construit le functionTypeSpec
            // a partir des inferredFunctionTypeSpec construit lors
            // du typeCheck des op_call
            //unit.accumulateInferredFunctionTypeSpec();

            if (unit.isStandard() && !unit.isTopInFile()) {
                //Check for conflicting duplicate definitions of this procedure:
                // sauf dans le cas interface + unit complete
                Unit enclosingUnit = unit.upperLevelUnit();
                SymbolTable enclosingScope = enclosingUnit.externalSymbolTable();
                FunctionDecl conflictUnit = enclosingScope.getFunctionNotInterfaceDecl(unit.name());
                if (conflictUnit != null && !conflictUnit.isModule() && conflictUnit.unit() != unit) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, unit.entryBlock().headTree(), "(DD25) Definition of " + unit.name() + " in " + enclosingUnit.name() + " overrides a definition in an enclosing scope.");
                }
            }

            TapList<SymbolTable> unitSymbolTables = unit.symbolTablesBottomUp();
            while (unitSymbolTables != null) {
                unitSymbolTables.head.checkTypeSpecValidity();
                unitSymbolTables = unitSymbolTables.tail;
            }

        }
        TapEnv.setRelatedUnit(null);
        //this.fortranStuff.commonEquivsIntoMemoryMap() ;

        // System.out.println("REMAINING INTERFACE UNITS:"+interfaceUnits) ;

        // Now we can do the final sorting because (almost, see pointer analysis...) all CallArrows are set:
        sortUnits(true, 1, 1, 1);

        detectRecursivity(false);

        // Recursivity detection about modules:
        detectRecursivity(true);

        // typeCheck des interfaces des units completement definies:
        TapList<Unit> intfUnits = interfaceUnits;
        while (intfUnits != null) {
            unit = intfUnits.head;
            if (unit.rank() == -1) {
                TapEnv.setRelatedUnit(unit);
                unit.importAllSymbolTables();
                unit.importInheritedClassesSymbolTables();
                unit.typeCheckInterface();
                TapEnv.setRelatedUnit(null);
            }
            intfUnits = intfUnits.tail;
        }

        // Bind extern C variables that have a trailing _ with a Fortran COMMON name, if it exists:
        TapList<TapList<ZoneInfoAccessElements>> toCMaps = new TapList<>(null, globalCMaps) ;
        TapList<TapList<ZoneInfoAccessElements>> inCMaps = toCMaps ;
        TapList<ZoneInfoAccessElements> cMap ;
        boolean danglingExtern ;
        TapList<ZoneInfoAccessElements> inCMap;
        ZoneInfoAccessElements cAccess ;
        while (inCMaps.tail!=null) {
            cMap = inCMaps.tail.head ;
            // Detect when this cMap is about a variable that is declared but not defined in the C part:
            danglingExtern = true ;
            inCMap = cMap ;
            cAccess = null ;
            while (inCMap!=null) {
                cAccess = inCMap.head ;
                if (cAccess.symbolDecl instanceof VariableDecl && !cAccess.symbolDecl.isExtern())
                    danglingExtern = false ;
                inCMap = inCMap.tail ;
            }
            // Only if not defined in C, look for this variable (plus _) in a Fortran COMMON:
            MemMap receivingMap = null ;
            String cName = null ;
            if (danglingExtern && cAccess!=null) {
                cName = cAccess.symbolDecl.symbol ;
                if (cName.endsWith("_")) {
                    receivingMap = globalFortranMaps.getMemMap("/"+cName.substring(0, cName.length()-1)+"/") ;
                }
            }
            if (receivingMap!=null) {
                inCMap = cMap ;
                while (inCMap!=null) {
                    cAccess = inCMap.head ;
                    ((VariableDecl)cAccess.symbolDecl).underscoreToFortran = true ;
                    MemoryMaps.placeCAccessIntoFortranMap(cAccess, receivingMap, cName, 0, globalFortranMaps) ;

                    inCMap = inCMap.tail ;
                }
                inCMaps.tail = inCMaps.tail.tail ;
            } else {
                inCMaps = inCMaps.tail ;
            }
        }
        globalCMaps = toCMaps.tail ;

        // Allocate zones for all declared variables,
        // top-down on the tree of SymbolTables, i.e.
        // root SymbolTable first,
        // then Fortran language root SymbolTable,
        // then C language root SymbolTable, then sub-C languages root SymbolTables (CUDA)
        // then C++ language root SymbolTable,
        // then Translation Unit (i.e. files) SymbolTables,
        // then all packages (Modules, Classes...) starting from the most enclosing Modules,
        // then all procedures starting from the most enclosing, followed by their CONTAIN'ed procedures.
        globalZonesNumber4 = new int[]{0, 0, 0, 0};
        TapList<ZoneInfo> toAllocatedStaticZones = new TapList<>(null, null) ;
        // Allocate zones for root and language-root SymbolTables
        initGlobalRootSymbolTable(toAllocatedStaticZones);
        //Initialize fortranRoot before cRoot, because this is when BIND(C) zones are prepared:
        initFortranRootSymbolTable(toAllocatedStaticZones);
        initCRootSymbolTable(toAllocatedStaticZones);
        initOtherRootSymbolTable(cudaRootSymbolTable, toAllocatedStaticZones);
        initOtherRootSymbolTable(cPlusPlusRootSymbolTable, toAllocatedStaticZones);
        // We have now created all the global zones that an external may see.
        // Collect them and record their numbers:
        this.allGlobalsForExternals = TapList.copy(toAllocatedStaticZones.tail) ;
        this.globalsForExternalsNumber4 = cPlusPlusRootSymbolTable.freeDeclaredZone4() ;

        // Allocate zones for each file ("Translation Unit"):
        tuSTs = translationUnitSymbolTables;
        while (tuSTs != null) {
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.printlnOnTrace("  allocate memory zones for Translation Unit "+tuSTs.head.unit+" starting at z="+globalZonesNumber4[0]);
            }
            initOtherRootSymbolTable(tuSTs.head, toAllocatedStaticZones);
            tuSTs = tuSTs.tail;
        }

        // Redirect basis SymbolTable of unsolved INTERFACEs:
        // they must not see the variables of their declaration context:
        for (int i=nbUnits-1 ; i>=0 ; --i) {
            unit = sortedUnits[i];
            if (unit.isInterface()) {
                SymbolTable interfaceRootSymbolTable = unit.publicSymbolTable() ;
                if (interfaceRootSymbolTable.basisSymbolTable()!=null
                    && interfaceRootSymbolTable.basisSymbolTable().isImports) {
                    interfaceRootSymbolTable = interfaceRootSymbolTable.basisSymbolTable();
                }
                if (interfaceRootSymbolTable!=null) {
                    interfaceRootSymbolTable.setBasisSymbolTable(globalRootSymbolTable()) ;
                }
            }
        }

        // Sort Units (CONTAINers and IMPORTers first, neglecting cycles) so that zones are allocated top-down.
        sortUnits(true, 0, 1, 1);
        // Allocate zones of Packages. Do not worry about importsSymbolTable's yet:
        for (int i = nbUnits - 1; i >= 0; i--) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
	    // If Unit is a Package (except one enclosed in another Package):
            if ((unit.isModule() || unit.isClass())
		&& (unit.upperLevelUnit==null || unit.upperLevelUnit.isTranslationUnit())) {
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.printlnOnTrace("  allocate memory zones for Package "+unit.name()+" starting at z="+globalZonesNumber4[0]);
                }
		// allocate zones for this Package, then recursively for its enclosed Packages if any.
		// These ranks are static, i.e. representing heap memory.
                unit.allocateDeclaredZones(globalZonesNumber4, true, toAllocatedStaticZones);
            }
        }

        // Sort again, Used modules first, Containers first, so that imports are filled in the right order.
        sortUnits(true, 0, -1, 1);
        // Prepare all importsSymbolTable's links to ZoneInfo's.
        TapList<ZoneInfo> allStaticZones = TapList.nreverse(toAllocatedStaticZones.tail) ;
        for (int i=0 ; i<nbUnits ; ++i) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
            if (unit.isTranslationUnit() && TapEnv.isC(unit.language())) {
                // Special for C: global variables are "imported" at the translationUnit's top level:
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.printlnOnTrace("  collect imported memory zones for C file " + unit.name()+" up to z="+globalZonesNumber4[0]);
                }
                unit.translationUnitSymbolTable().fillImportedZones(globalZonesNumber4, allStaticZones) ;
            }
            // General case: imports are done inside the "importsSymbolTable" of each procedure or module Unit,
            if (unit.importsSymbolTable()!=null) {
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.printlnOnTrace("  collect imported memory zones for unit " + unit.name()+" up to z="+globalZonesNumber4[0]);
                }
                unit.importsSymbolTable().fillImportedZones(globalZonesNumber4, allStaticZones) ;
            }

	    // Zones are already allocated for Packages, not yet for Procedures. Do it now.
	    // If Unit is a Procedure (except one enclosed in another Procedure):
	    if (unit.isProcedure() || unit.isInterface()) {
		if (TapEnv.traceTypeCheckAnalysis()) {
		    TapEnv.printlnOnTrace("  allocate memory zones for " + unit.name()+" starting at z="+globalZonesNumber4[0]);
		}
		// allocate zones for this Procedure, then recursively for its enclosed Procedures if any.
		// These ranks are dynamic, i.e. representing stack memory.
		unit.allocateDeclaredZones(copyZone4Ranks(globalZonesNumber4), false, null);
	    }
            if (unit.isStandard() || unit.isTranslationUnit()) {
                // compute predecessors, successors, dominators, postDominators, and reachable blocks:
                unit.computeAllDominatorlikeInfo();
            }
	}

        // Sort again, this time with CALLers first.
        sortUnits(true, 1, 1, 1);

        // At this point all declared zones should be allocated, the
        // SymbolTable.declaredZoneInfos[][] arrays should be ready.
        for (int i=nbUnits-1 ; i>=0 ; --i) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
            if (unit.isStandard()) {
                unit.computeZonesUsedInDeclarations();
            }
        }
        // Compute each Unit's externalShape
        // Done bottom-up on CallGraph, because
        // the caller needs the callee's externalShape
        TapList<Unit> iUnits = TapList.append(dummyUnits, intrinsicUnits);
        while (iUnits != null) {
            iUnit = iUnits.head;
            TapEnv.setRelatedUnit(iUnit);
            iUnit.computeZoneInterfaces();
            iUnits = iUnits.tail;
        }
        for (int i = nbUnits - 1; i >= 0; i--) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
            if (unit.isStandard() || unit.isOutside() || unit.isInterface() || unit.isModule() || unit.isVarFunction()) {
                unit.computeZoneInterfaces();
            }
        }
        for (int i = nbUnits - 1; i >= 0; i--) {
            unit = sortedUnits[i];
            TapEnv.setRelatedUnit(unit);
            if (unit.isStandard() || unit.isOutside() || unit.isInterface()) {
                unit.computeArrowZoneInterfaces();
            }
        }
        // Fixed-point iteration to get the final "relevantZones" of each Unit.
        // TODO: "relevantZones" is for now only computed for the "allKind" zone numbering
        //       we will need it for other numberings as well!
        boolean runAgainCG = true;
        TapList<CallArrow> callees ;
        Unit calledUnit, unitContainer ;
        while (runAgainCG) {
            runAgainCG = false ;
            for (int i=nbUnits-1 ; i>=0 ; --i) {
                unit = sortedUnits[i];
                if (unit.relevantZones!=null) {
                    unitContainer = unit ;
                    while (unitContainer.upperLevelUnit!=null && unitContainer.upperLevelUnit.isStandard()) {
                        unitContainer = unitContainer.upperLevelUnit ;
                    }
                    callees = unit.callees ;
                    while (callees!=null) {
                        if (callees.head.isCall()) {
                            calledUnit = callees.head.destination ;
                            if (calledUnit.relevantZones!=null) {
                                runAgainCG =
                                    unit.relevantZones.cumulOrGrows(calledUnit.relevantZones,
                                              unitContainer.globalZonesNumber(SymbolTableConstants.ALLKIND)) ;
                            }
                        }
                        callees = callees.tail ;
                    }
                }
            }
        }
        TapEnv.setRelatedUnit(null);
    }

    // Copy rks, which is an int[4] of equivalent globalRank, intRank, realRank, pointerRank.
    public static final int[] copyZone4Ranks(int[] rks) {
	return new int[]{rks[0], rks[1], rks[2], rks[3]} ;
    }

    private void initGlobalRootSymbolTable(TapList<ZoneInfo> toAllocatedStaticZones) {
        if (globalRootSymbolTable!=null) {
            globalRootSymbolTable.computeFirstDeclaredZone(globalZonesNumber4, globalRootSymbolTable);
            ZoneAllocator zoneAllocator = new ZoneAllocator(globalRootSymbolTable);
            zoneAllocator.setAllocateZonesForPointed(true) ; // a global pointer may have an initial destination
            // Allocate NULL_DEST zone:
            Tree dummy = ILUtils.build(ILLang.op_ident, "%NULL_DEST%") ;
            WrapperTypeSpec dummyType = new WrapperTypeSpec(null) ;
            ZoneInfoAccessElements access =
                new ZoneInfoAccessElements(new VariableDecl(dummy, dummyType), null, globalRootSymbolTable) ;
            TapList zones =
                zoneAllocator.allocateOneZone(dummyType, -1, new TapList<>(access, null),
                                              null, -1, false, false, false, false, null) ;
            ZoneInfo lastZoneInfo = zoneAllocator.lastAllocated() ;
            lastZoneInfo.description = "null_dest" ;
            zoneNbOfNullDest = lastZoneInfo.zoneNb ;

             // Allocate UNKNOWN_DEST zone:
            dummy = ILUtils.build(ILLang.op_ident, "%UNKNOWN_DEST%") ;
            dummyType = new WrapperTypeSpec(null) ;
            access =
                new ZoneInfoAccessElements(new VariableDecl(dummy, dummyType), null, globalRootSymbolTable) ;
            zones =
                zoneAllocator.allocateOneZone(dummyType, -1, new TapList<>(access, null),
                                              null, -1, false, false, false, false, null) ;
            lastZoneInfo = zoneAllocator.lastAllocated() ;
            lastZoneInfo.description = "unknown_dest" ;
            zoneNbOfUnknownDest = lastZoneInfo.zoneNb ;

            this.numberOfDummyRootZones = zoneAllocator.nextZone ;

            // Allocate IO-stream and MPI channels zones:
            allocateGlobalMPChannelsAndIOStreams(globalRootSymbolTable, zoneAllocator);

            // Allocate one zone per procedural Unit,
            // to permit destination analysis of pointers-to-procedure:
            // Note: we restrict to interfaces and "user" procedures, e.g. not MPI_* stuff
            while (allPointedUnits!=null) {
                Unit unit = allPointedUnits.head ;
                TapList<FunctionDecl> unitDecls = unit.externalSymbolTable().getFunctionDecl(unit.name(), null, null, false);
                FunctionDecl unitDecl = ((unitDecls == null) ? null : unitDecls.head);
                access =
                    new ZoneInfoAccessElements(unitDecl, unit.headTree(), globalRootSymbolTable) ;
                TapList regionZones = zoneAllocator.allocateOneZone(new WrapperTypeSpec(unit.functionTypeSpec()),
                                              -1, new TapList<>(access, null),
                                              null, -1, false, false, false, false, null) ;
                if (unitDecl != null) unitDecl.accumulateZones(regionZones);
                lastZoneInfo = zoneAllocator.lastAllocated() ;
                lastZoneInfo.description = "procedure "+unit.name() ;
                allPointedUnits = allPointedUnits.tail ;
            }

            // Allocate zones for other variables declared here, if any:
            globalRootSymbolTable.allocateZones(zoneAllocator, null);
            // analyse allocation points
            prepareAllocationPoints(zoneAllocator);
            zoneAllocator.extractAllZoneInfosIntoTargetSymbolTable(toAllocatedStaticZones);
            for (int i=0 ; i<4 ; ++i) { // i.e. for i in {ALLKIND, REALKIND, INTKIND, PTRKIND}
                globalZonesNumber4[i] = globalRootSymbolTable.freeDeclaredZone(i);
            }
        }
    }

    private void initFortranRootSymbolTable(TapList<ZoneInfo> toAllocatedStaticZones) {
        if (fortranRootSymbolTable!=null) {
            fortranRootSymbolTable.computeFirstDeclaredZone(globalZonesNumber4, fortranRootSymbolTable);
            ZoneAllocator zoneAllocator = new ZoneAllocator(fortranRootSymbolTable);
            zoneAllocator.setAllocateZonesForPointed(true) ; // a global pointer may have an initial destination
            globalFortranMaps.allocateZones(zoneAllocator, null) ;
            //globalFortranMaps = null ; // usage of this MemoryMaps finishes here.
            fortranRootSymbolTable.allocateZones(zoneAllocator, null);
            // analyse allocation points
            prepareAllocationPoints(zoneAllocator);
            zoneAllocator.extractAllZoneInfosIntoTargetSymbolTable(toAllocatedStaticZones);
            for (int i=0 ; i<4 ; ++i) { // i.e. for i in {ALLKIND, REALKIND, INTKIND, PTRKIND}
                globalZonesNumber4[i] = fortranRootSymbolTable.freeDeclaredZone(i);
            }
        }
    }

    private void initCRootSymbolTable(TapList<ZoneInfo> toAllocatedStaticZones) {
        if (cRootSymbolTable!=null) {
            cRootSymbolTable.computeFirstDeclaredZone(globalZonesNumber4, cRootSymbolTable);
            ZoneAllocator zoneAllocator = new ZoneAllocator(cRootSymbolTable);
            zoneAllocator.setAllocateZonesForPointed(true) ; // a global pointer may have an initial destination
            zoneAllocator.ambiguousWithoutIndices = false ;
            zoneAllocator.setOffsets(-1, -1, false) ;
            zoneAllocator.setCommonName(null) ;
            TapList<ZoneInfoAccessElements> sharedAccesses ;
            VariableDecl varDecl ;
            WrapperTypeSpec varType ;
            boolean isInt ,isReal, isPtr ;
            TapList regionZones;
            while (globalCMaps!=null) {
                sharedAccesses = globalCMaps.head ;
                varDecl = (VariableDecl)sharedAccesses.head.symbolDecl ;
                if (varDecl.zones() != null) { // If varDecl has already been allocated (e.g. by BIND(C))
                } else {
                    varType = varDecl.type() ;
                    isInt = varType.isIntegerBase() ;
                    isReal = varType.isRealOrComplexBase() ;
                    isPtr = varType.isPointer() ;
                    regionZones =
                        zoneAllocator.allocateZones(varType, -1, varDecl.extraInfo(), sharedAccesses,
                                                    varType, -1, isInt, isReal, isPtr, null, null, null) ;
                    while (sharedAccesses!=null) {
                        varDecl = (VariableDecl)sharedAccesses.head.symbolDecl ;
                        varDecl.accumulateZones(regionZones);
                        sharedAccesses = sharedAccesses.tail ;
                    }
                }
                globalCMaps = globalCMaps.tail ;
            }
            cRootSymbolTable.allocateZones(zoneAllocator, null);
            // analyse allocation points
            prepareAllocationPoints(zoneAllocator);
            zoneAllocator.extractAllZoneInfosIntoTargetSymbolTable(toAllocatedStaticZones);
            for (int i=0 ; i<4 ; ++i) { // i.e. for i in {ALLKIND, REALKIND, INTKIND, PTRKIND}
                globalZonesNumber4[i] = cRootSymbolTable.freeDeclaredZone(i);
            }
        }
    }

    private void initOtherRootSymbolTable(SymbolTable rootSymbolTable,
                                          TapList<ZoneInfo> toAllocatedStaticZones) {
        if (rootSymbolTable!=null) {
            rootSymbolTable.computeFirstDeclaredZone(globalZonesNumber4, rootSymbolTable);
            ZoneAllocator zoneAllocator = new ZoneAllocator(rootSymbolTable);
            zoneAllocator.setAllocateZonesForPointed(true) ; // a global pointer may have an initial destination
            rootSymbolTable.allocateZones(zoneAllocator, null);
            // analyse allocation points
            prepareAllocationPoints(zoneAllocator);
            zoneAllocator.extractAllZoneInfosIntoTargetSymbolTable(toAllocatedStaticZones);
            for (int i=0 ; i<4 ; ++i) { // i.e. for i in {ALLKIND, REALKIND, INTKIND, PTRKIND}
                globalZonesNumber4[i] = rootSymbolTable.freeDeclaredZone(i);
            }
        }
    }

    public TapList<ZoneInfoAccessElements> getCglobalMap(SymbolDecl sharedDecl) {
        TapList<ZoneInfoAccessElements> found = null ;
        TapList<TapList<ZoneInfoAccessElements>> inMaps = globalCMaps ;
        while (found==null && inMaps!=null) {
            if (inMaps.head!=null && inMaps.head.head.symbolDecl==sharedDecl) {
                found = inMaps.head ;
            }
            inMaps = inMaps.tail ;
        }
        return found ;
    }

    public static boolean isMessagePassingChannelName(String name) {
        return name!=null && name.startsWith("%message_passing_channel_");
    }

    private void allocateGlobalMPChannelsAndIOStreams(SymbolTable symbolTable, ZoneAllocator zoneAllocator) {
        // allocate the zone for zoneOfAllIOStreams
        Tree ioDummy = ILUtils.build(ILLang.op_ident, "%all_io_streams%") ;
        WrapperTypeSpec ioType = new WrapperTypeSpec(null) ;
        ZoneInfoAccessElements access =
            new ZoneInfoAccessElements(new VariableDecl(ioDummy, ioType), null/*ioDummy*/, symbolTable) ;
        TapList zones =
                zoneAllocator.allocateOneZone(ioType, -1, new TapList<>(access, null),
                                              null, -1, false, false, false, false, null) ;
        ZoneInfo lastZoneInfo = zoneAllocator.lastAllocated() ;
        lastZoneInfo.description = "all_io" ;
        lastZoneInfo.isChannelOrIO = true ;
        zoneNbOfAllIOStreams = lastZoneInfo.zoneNb ;
        // allocate the zones for the MPI channels
        prepareChannelCutting();
        TapList<TapPair<MPIcallInfo, TapIntList>> inAllChannels = allMessagePassingChannels;
        int channelsNb = MPIcallInfo.checkNumberOfChannels(this);
        TapPair<MPIcallInfo, TapIntList> oneChannels;
        int channelNum;
        while (inAllChannels != null && channelsNb > 0) {
            oneChannels = inAllChannels.head;
            channelNum = oneChannels.second.head;
            ioDummy = ILUtils.build(ILLang.op_ident, "%message_passing_channel_"+channelNum+"%") ;
            ioType = symbolTable.getTypeDecl("float").typeSpec ;
            access =
                new ZoneInfoAccessElements(new VariableDecl(ioDummy, ioType), null, symbolTable) ;
            zones = zoneAllocator.allocateOneZone(ioType, -1, new TapList<>(access, null),
                                                  null, -1, false, false, false, false, null) ;
            lastZoneInfo = zoneAllocator.lastAllocated() ;
            lastZoneInfo.description = "channel_"+channelNum ;
            lastZoneInfo.isChannelOrIO = true ;
            inAllChannels = inAllChannels.tail;
            channelsNb = channelsNb - 1;
        }
    }

    public TapIntList zoneRksOfIO() {
        return new TapIntList(zoneNbOfAllIOStreams, null) ;
    }

    public TapIntList zoneRksOfChannelOrIO() {
        TapIntList result = null ;
        ZoneInfo zi ;
        for (int i=globalRootSymbolTable.freeDeclaredZone(SymbolTableConstants.ALLKIND)-1 ; i>=0 ; --i) {
            zi = globalRootSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
            if (zi.isChannelOrIO) result = new TapIntList(i, result) ;
        }
        return result ;
    }

    private void prepareChannelCutting() {
        //[llh] TODO: temporary. We must cut into parts to capture intersection of channels.
        TapList<TapPair<MPIcallInfo, TapIntList>> inAllChannels = allMessagePassingChannels;
        TapPair<MPIcallInfo, TapIntList> oneChannels;
        int i = zoneNbOfAllIOStreams+1; // future rank(s) of the future allocated zone(s) for channels
        while (inAllChannels != null) {
            oneChannels = inAllChannels.head;
            oneChannels.second = new TapIntList(i, null);
            ++i;
            inAllChannels = inAllChannels.tail;
        }
    }

    /**
     * Run all general-purpose analyses.
     */
    public void prepareAnalyses(TapList<Unit> rootUnits) {
        // Prepare in-out info for intrinsics and for dummy "unknown" units:
        TapList<Unit> iUnits = TapList.append(dummyUnits, intrinsicUnits);
        Unit iUnit;
        while (iUnits != null) {
            iUnit = iUnits.head;
            TapEnv.setRelatedUnit(iUnit);
            if (iUnit.unitInOutN() == null) {
                InOutAnalyzer.setExternalOrIntrinsicDefaultInOutInfo(iUnit);
            }
            iUnits = iUnits.tail;
        }
        TapEnv.setRelatedUnit(null);
        // Transform appropriate for-loops into do-loops:
        turnForLoopsIntoDoLoops(rootUnits);
        // Run analysis of loop localized zones:
        LoopArrayAccessAnalyzer.runAnalysis(this,
                (TapEnv.mustContext() || TapEnv.modeIsNoDiff() ? null : rootUnits));
        // Run pointers destinations analysis:
        PointerAnalyzer.runAnalysis(this,
                (TapEnv.mustContext() || TapEnv.modeIsNoDiff() ? null : rootUnits));
        // Sort units again, because PointerAnalyzer on function pointers may have added new CallArrow's:
        sortUnits(true, 1, 1, 1);
        // Run In-Out analysis:
        InOutAnalyzer.runAnalysis(this,
                TapEnv.mustContext() || TapEnv.modeIsNoDiff() ? null : rootUnits);
        // Transform appropriate for-loops into do-loops again, because in-out may help find more!
        turnForLoopsIntoDoLoops(rootUnits);
    }

    /**
     * Transform for-loops with an appropriate pattern into do-loops.
     */
    private void turnForLoopsIntoDoLoops(TapList<Unit> rootUnits) {
        TapList<Unit> aUnits =
                rootUnits == null ? this.units : Unit.allCalleesMulti(rootUnits);
        TapList<Block> blocks;
        TapList<Instruction> instructions;
        Tree instrTree;
        while (aUnits != null) {
            blocks = aUnits.head.allBlocks;
            while (blocks != null) {
                instructions = blocks.head.instructions;
                while (instructions != null) {
                    instrTree = instructions.head.tree;
                    if (instrTree != null
                            && instrTree.opCode() == ILLang.op_loop
                            && instrTree.down(3) != null
                            && instrTree.down(3).opCode() == ILLang.op_for
                            && ILUtils.mayBeTransformed2Do(instrTree.down(3))) {
                        instrTree.setChild(ILUtils.transformFor2Do(instrTree.down(3)), 3);
                    }
                    instructions = instructions.tail;
                }
                blocks = blocks.tail;
            }
            aUnits = aUnits.tail;
        }
    }

    /**
     * @return the "package" Unit (i.e. a file, i.e. a compilation unit) with the given fileName.
     */
    public Unit getFileUnit(String fileName) {
        TapList<Unit> inUnits = units;
        Unit found = null;
        while (found == null && inUnits != null) {
            if (fileName.equals(inUnits.head.name())) {
                found = inUnits.head;
            }
            inUnits = inUnits.tail;
        }
        return found;
    }


    /**
     * @return the Unit corresponding to the given full name "funcName".
     * A full name (path) of a procedure is prefixed with
     * the names of the containing modules or procedures as in "module.func".
     */
    public Unit getUnit(String funcName) {
        return getUnit(TapList.parseIdentifier(funcName));
    }

    /**
     * @return the Unit corresponding to the given path "funcPath",
     * starting from the namespace of the root SymbolTable of this CallGraph.
     * If finds no Unit, complains and returns any unit with the same name.
     * May return an external Unit or even null!
     */
    public Unit getUnit(TapList<String> funcPath) {
        String foundPathString = "";
        SymbolTable curST = null;
        String subName = funcPath.head;
        String funcPathString = subName;
        Unit deepestFoundUnit = null;
        funcPath = funcPath.tail;
        if (TapEnv.inputLanguage() == TapEnv.MIXED || TapEnv.inputLanguage() == TapEnv.C) {
            FunctionDecl subUnitDecl = cRootSymbolTable.getFunctionNotExternalNorInterfaceDecl(subName);
            if (subUnitDecl != null) {
                Unit subUnit = subUnitDecl.unit();
                if (funcPath == null) {
                    deepestFoundUnit = subUnit;
                }
                curST = subUnit.privateSymbolTable();
                foundPathString = "[C]";
            }
        }
        if (curST == null && (TapEnv.inputLanguage() == TapEnv.MIXED || TapEnv.inputLanguage() == TapEnv.CPLUSPLUS)) {
            if (funcPath != null) {
                curST = cPlusPlusRootSymbolTable.getNamedScope(subName);
            }
            if (curST == null) {
                FunctionDecl subUnitDecl = cPlusPlusRootSymbolTable.getFunctionNotExternalNorInterfaceDecl(subName);
                if (subUnitDecl != null) {
                    Unit subUnit = subUnitDecl.unit();
                    if (funcPath == null) {
                        deepestFoundUnit = subUnit;
                    }
                    curST = subUnit == null ? null : subUnit.privateSymbolTable();
                }
            }
            if (curST != null) {
                foundPathString = "[C++]";
            }
        }
        if (curST == null && (TapEnv.inputLanguage() == TapEnv.MIXED || TapEnv.isFortran(TapEnv.inputLanguage()))) {
            if (funcPath != null) {
                FunctionDecl subUnitDecl = fortranRootSymbolTable.getModuleDecl(subName.toLowerCase());
                if (subUnitDecl != null) {
                    Unit subUnit = subUnitDecl.unit();
                    curST = subUnit == null ? null : subUnit.privateSymbolTable();
                }
            }
            if (curST == null) {
                FunctionDecl subUnitDecl = fortranRootSymbolTable.getFunctionNotExternalNorInterfaceDecl(subName.toLowerCase());
                if (subUnitDecl != null) {
                    Unit subUnit = subUnitDecl.unit();
                    if (funcPath == null) {
                        deepestFoundUnit = subUnit;
                    }
                    curST = subUnit == null ? null : subUnit.privateSymbolTable();
                }
            }
            if (curST != null) {
                foundPathString = "[Fortran]";
            }
        }
        TapList<SymbolTable> tuSTs = translationUnitSymbolTables;
        while (curST == null && tuSTs != null) {
            if (funcPath != null) {
                curST = tuSTs.head.getNamedScope(subName);
            }
            if (curST == null && funcPath != null) {
                FunctionDecl subUnitDecl = tuSTs.head.getModuleDecl(subName.toLowerCase());
                if (subUnitDecl != null) {
                    Unit subUnit = subUnitDecl.unit();
                    curST = subUnit == null ? null : subUnit.privateSymbolTable();
                }
            }
            if (curST == null) {
                FunctionDecl subUnitDecl = tuSTs.head.getFunctionNotExternalNorInterfaceDecl(
                        tuSTs.head.isCaseDependent() ? subName : subName.toLowerCase());
                if (subUnitDecl != null) {
                    Unit subUnit = subUnitDecl.unit();
                    if (funcPath == null) {
                        deepestFoundUnit = subUnit;
                    }
                    curST = subUnit == null ? null : subUnit.privateSymbolTable();
                }
            }
            if (curST != null) {
                foundPathString = "[file " + tuSTs.head.containerFileName + TapEnv.outputExtension(tuSTs.head.language()) + "]";
            }
            tuSTs = tuSTs.tail;
        }

        while (curST != null && funcPath != null) {
            SymbolTable searchedST = curST;
            curST = null;
            subName = funcPath.head;
            funcPathString = funcPathString + "." + subName;
            funcPath = funcPath.tail;
            if (funcPath != null) {
                curST = searchedST.getNamedScope(subName);
            }
            if (curST == null && funcPath != null) {
                FunctionDecl subUnitDecl = searchedST.getModuleDecl(subName.toLowerCase());
                if (subUnitDecl != null) {
                    Unit subUnit = subUnitDecl.unit();
                    curST = subUnit == null ? null : subUnit.privateSymbolTable();
                }
            }
            if (curST == null) {
                FunctionDecl subUnitDecl = searchedST.getFunctionNotExternalNorInterfaceDecl(
                        searchedST.isCaseDependent() ? subName : subName.toLowerCase());
                if (subUnitDecl != null) {
                    Unit subUnit = subUnitDecl.unit();
                    if (funcPath == null) {
                        deepestFoundUnit = subUnit;
                    }
                    curST = subUnit == null ? null : subUnit.privateSymbolTable();
                }
            }
            if (curST != null) {
                foundPathString = foundPathString + "." + subName;
            }
        }

        if (!(deepestFoundUnit != null && funcPath == null)) {
            // fallback: return any Unit with the searched name!
            if (funcPath != null) {
                subName = funcPath.head;
                funcPath = funcPath.tail;
                funcPathString = funcPathString + "." + subName;
            }
            if (subName != null && TapEnv.isFortran(TapEnv.inputLanguage())) {
                subName = subName.toLowerCase();
            }
            deepestFoundUnit = subName == null ? null : getAnyUnit(subName);
            if (deepestFoundUnit != null) {
                String fallBackName = deepestFoundUnit.name();
                if (!deepestFoundUnit.isTopInFile()
                        && deepestFoundUnit.upperLevelUnit().name() != null
                        && !deepestFoundUnit.upperLevelUnit().name().isEmpty()) {
                    fallBackName = deepestFoundUnit.upperLevelUnit().name() + "." + deepestFoundUnit.name();
                }
                TapEnv.commandWarning(-1, "Procedure " + funcPathString + " understood as " + fallBackName);
            }
        }
        return deepestFoundUnit;
    }

    /**
     * @return 'null' if not found or if it finds an interface.
     */
    public Unit getUnit(String name, FunctionTypeSpec functionTypeSpec) {
        TapList<Unit> allUnits = units;
        boolean found = false;
        Unit unit = null;
        while (!found && allUnits != null) {
            unit = allUnits.head;
            found = unit.name().equals(name) && !unit.isInterface() && !unit.isVarFunction() &&
                    unit.functionTypeSpec().equalsCompilDep(functionTypeSpec);
            allUnits = allUnits.tail;
        }
        return found ? unit : null;
    }

    /**
     * @return a standard Unit with name 'name' from this CallGraph, 'null' if not found.
     * This method is dirty and should be used only for debugging,
     * because it does not use the scoping mechanism and therefore several
     * Unit's can have the same name.
     */
    public Unit getAnyUnit(String name) {
        TapList<Unit> allUnits = units;
        boolean found = false;
        Unit unit = null;
        while (!found && allUnits != null) {
            unit = allUnits.head;
            found = unit.name().equals(name) && !unit.isInterface() && !unit.isVarFunction();
            allUnits = allUnits.tail;
        }
        return found ? unit : null;
    }

    /**
     * @return one Unit with name 'name' from the CallGraph, provided it is not a VarFunctionUnit.
     * This method is dirty and should be used only for debugging,
     * because it does not use the scoping mechanism and therefore several
     * Unit's can have the same name.
     */
    public Unit getAnyNonVarUnit(String name, Unit inUnit) {
        // [vmp] nonVarUnit() est utilisee dans SymbolTable.buildExternalUnit
        // on suppose qu'on ne declare en external que des units non contenues dans un module
        // mais c'est tout aussi "dirty&dangerous". cf nonRegrF90 v442 et dump
        TapList<Unit> allUnits = units;
        boolean found = false;
        Unit unit = null;
        while (!found && allUnits != null) {
            unit = allUnits.head;
            found = unit.name().equals(name) && unit.isTopInFile() && !unit.isInterface() && !unit.isVarFunction()
                    && inUnit.sameLanguage(unit.language());
            allUnits = allUnits.tail;
        }
        return found ? unit : null;
    }

    /**
     * @return the intrinsic Unit with name 'name', 'null' if not found.
     * This method is dirty and should be used only for debugging,
     * because it does not use the scoping mechanism and therefore several
     * Unit's can have the same name.
     */
    public Unit getAnyIntrinsicUnit(String name) {
        // vmp: ne plus utiliser, il peut y avoir plusieurs intrinsics de meme nom !
        TapList<Unit> allUnits = intrinsicUnits;
        boolean found = false;
        Unit unit = null;
        while (!found && allUnits != null) {
            unit = allUnits.head;
            found = unit.name().equals(name);
            allUnits = allUnits.tail;
        }
        return found ? unit : null;
    }

    /**
     * Allocates global zones that will represent the memory
     * returned by each allocate that appears in the program.
     * For each allocate expression, search the corresponding target type.
     * Allocate it and link it to the tree with annotation.
     * Build a new varDeclaration for each allocated zones.
     */
    private void prepareAllocationPoints(ZoneAllocator zoneAllocator) {
        TapList zones;
        TapPair<TapPair<Tree, Tree>, TapPair<TypeSpec, SymbolTable>> allocationPoint;
        SymbolTable allocationSymbolTable;
        Tree allocatedRoot;
        Tree allocationTree;
        Tree allocatedTree;
        WrapperTypeSpec allocatedType;
        // one "ALLOCATE" may be called several times in general, thus ZoneInfos's must be multiple:
        // however, this is unfortunate on F90:ht05 because THAT allocated var is not multiple!
        zoneAllocator.setPossiblyMultiple(true) ;
        zoneAllocator.setNeedsAccessTree(true) ;
        // When a malloc allocates a pointer, it does not initialize its destination, which is
        // therefore always undefined. Therefore it is not necessary to create (an) additional zone(s) for
        // its destination(s) just after allocation. This is different from a pointer which is in a formal
        // parameter, for which we need these destination zones to represent its destination(s) upon entry
        // into the procedure.
        while (allocationPoints != null) {
            allocationPoint = allocationPoints.head;
            // allocationTree is the complete ALLOCATE tree e.g. "ALLOCATE(,(10,50),,stat=status)" :
            allocationTree = allocationPoint.first.first;
            // allocatedRoot is the allocated root tree, e.g. A%x :
            allocatedRoot = allocationPoint.first.second;
            allocatedType = (WrapperTypeSpec) allocationPoint.second.first;
            allocationSymbolTable = allocationPoint.second.second;
            // We build a dummy "allocate" tree, with a wrong syntax, that will be used
            // only in the ZoneInfo of the allocated zones....
            allocatedTree = ILUtils.build(ILLang.op_allocate,
                              ILUtils.build(ILLang.op_pointerAccess,
                                ILUtils.copy(allocatedRoot),
                                ILUtils.build(ILLang.op_none)),
                              ILUtils.build(ILLang.op_stringCst, allocationSymbolTable.unit.shortName()));
            // Prepare the location for the future "allocatedZones" annotation:
            TapList zonesPlaceHolder = new TapList<>(null, null);
            allocatedTree.setAnnotation("allocatedZones", zonesPlaceHolder);
            if (TypeSpec.isA(allocatedType, SymbolTableConstants.POINTERTYPE)) {
                allocatedType = ((PointerTypeSpec) allocatedType.wrappedType).destinationType;
            }
            VariableDecl allocVarDecl = new VariableDecl(allocatedRoot, allocatedType);
            ZoneInfoAccessElements access =
                new ZoneInfoAccessElements(allocVarDecl, allocatedTree, allocationSymbolTable) ;
            zones = zoneAllocator.allocateZones(allocatedType, -1, null, new TapList<>(access, null),
                                                allocatedType, -1, false, false, false, null, allocationSymbolTable, null) ;
            allocVarDecl.setZones(zones);
            if (zones == null) {
                TapEnv.fileError(null, "(TCxx) Could not allocate memory for " + allocatedTree + " in " + allocationSymbolTable.unit.name());
            } else {
                // now set the final "allocatedZones" annotation:
                zonesPlaceHolder.head = zones.head;
                zonesPlaceHolder.tail = zones.tail;
                allocationTree.setAnnotation("allocatedZones", zones);
            }
            allocationPoints = allocationPoints.tail;
        }
        zoneAllocator.setPossiblyMultiple(false) ;
        zoneAllocator.setNeedsAccessTree(false) ;
    }

    protected void addAllocationPoint(Tree allocateTree, Tree recipientTree, TypeSpec allocatedType, SymbolTable symbolTable) {
        allocationPoints = new TapList<>(new TapPair<>(new TapPair<>(allocateTree, recipientTree),
						       new TapPair<>(allocatedType, symbolTable)),
					 allocationPoints);
    }

    protected void replaceInAllocationPoints(Tree oldAllocate, Tree newAllocate) {
        TapPair<Tree, Tree> found = null;
        TapList<TapPair<TapPair<Tree, Tree>, TapPair<TypeSpec, SymbolTable>>> inAllocationPoints = allocationPoints;
        while (found == null && inAllocationPoints != null) {
            if (inAllocationPoints.head.first.first == oldAllocate) {
                found = inAllocationPoints.head.first;
            }
            inAllocationPoints = inAllocationPoints.tail;
        }
        if (found != null) {
            found.first = newAllocate;
        } else {
            TapEnv.toolError("Lost allocated zones for " + newAllocate);
        }
    }

//TODO: getUnits() below is incorrect because it doesn't return all units with a given name,
// for instance it returns only one of the intrinsic Units named "sum" !
    /** Returns the list of all Units that match one name or one unit rank in "unitNames".
     * Returns null if unitNames is null or no Unit is found.
     * If the given list "unitNames" starts with special String "%all%, then this returns
     * a one-element list containing null, which  must be understood as "all Units".
     */
    public TapList<Unit> getUnits(TapList<String> unitNames) {
        if (unitNames == null) {
            return null;
        } else if (unitNames.head.equals("%all%")) {
            return new TapList<>(null, null);
        } else {
            TapList<Unit> hdResult = new TapList<>(null, null);
            TapList<Unit> tlResult = hdResult;
            Unit unitToTraceSameCase;
            Unit unitToTraceLowerCase;
            Unit unitToTraceFileName;
            while (unitNames != null) {
                try {
                    int unitRk = Integer.parseInt(unitNames.head);
                    Unit unitToTrace = null;
                    for (int i = sortedUnits.length - 1; i >= 0 && unitToTrace == null; --i) {
                        if (sortedUnits[i].rank() == unitRk) {
                            unitToTrace = sortedUnits[i];
                        }
                    }
                    if (unitToTrace != null
                            && !TapList.contains(hdResult.tail, unitToTrace)) {
                        tlResult = tlResult.placdl(unitToTrace);
                    }
                    if (unitToTrace == null) {
                        TapEnv.commandWarning(10, "Can't find unit of rank " + unitRk);
                    }
                } catch (NumberFormatException e) {
                    unitToTraceSameCase = getUnit(unitNames.head);
                    if (unitToTraceSameCase != null
                            && !TapList.contains(hdResult.tail, unitToTraceSameCase)) {
                        tlResult = tlResult.placdl(unitToTraceSameCase);
                    }
                    unitToTraceLowerCase = getUnit(unitNames.head.toLowerCase());
                    if (unitToTraceLowerCase != null
                            && unitToTraceLowerCase.isFortran()
                            && unitToTraceLowerCase != unitToTraceSameCase
                            && !TapList.contains(hdResult.tail, unitToTraceLowerCase)) {
                        tlResult = tlResult.placdl(unitToTraceLowerCase);
                    }
                    unitToTraceFileName = getFileUnit(unitNames.head);
                    if (unitToTraceFileName != null
                            && !TapList.contains(hdResult.tail, unitToTraceFileName)) {
                        tlResult = tlResult.placdl(unitToTraceFileName);
                    }
                    if (unitToTraceSameCase == null
                            && (unitToTraceLowerCase == null || !unitToTraceLowerCase.isFortran())
                            && unitToTraceFileName == null) {
                        TapEnv.commandWarning(10, "Can't find any unit named " + unitNames.head);
                    }
                }
                unitNames = unitNames.tail;
            }
            return hdResult.tail;
        }
    }

    public void addBindFortranCFunction(String unitFortranName, String unitCName, FunctionDecl funcDecl) {
        bindFortranCFunctions = new TapList<>
                (new TapTriplet<>(unitFortranName, unitCName, funcDecl),
                        bindFortranCFunctions);
    }

    public void addBindFortranCDiffUnit(Unit diffUnit) {
        bindFortranCDiffUnits = new TapList<>(diffUnit, bindFortranCDiffUnits);
    }

    public TapList<TapPair<TypeDecl, TypeDecl>> bindFortranCType() {
        return bindFortranCType;
    }

    public void setBindFortranCType(TapList<TapPair<TypeDecl, TypeDecl>> origBindCType) {
        bindFortranCType = origBindCType;
    }

    public void addBindFortranCType(TypeDecl fortranTypeDecl, TypeDecl cTypeDecl) {
        bindFortranCType = new TapList<>(new TapPair<>(fortranTypeDecl, cTypeDecl), bindFortranCType);
    }

    /** Returns true if fType and cType are based on primitive types
     *  and naturally match, without any need for BIND declaration. */
    public boolean naturalBindFortranCType(WrapperTypeSpec fType, WrapperTypeSpec cType) {
        return (fType!=null && cType!=null
                && TypeSpec.isA(fType.baseTypeSpec(true), SymbolTableConstants.PRIMITIVETYPE)
                && (fType.equalsLiterally(cType)
                    || (TypeSpec.isA(cType, SymbolTableConstants.POINTERTYPE)
                        && fType.equalsLiterally(((PointerTypeSpec)cType.wrappedType).destinationType)))) ;
    }

    public boolean checkBindFortranCType(WrapperTypeSpec fortranTypeSpec, WrapperTypeSpec cTypeSpec) {
        boolean result = true;
        boolean found = false;
        TapList<TapPair<TypeDecl, TypeDecl>> bindTypes = bindFortranCType;
        while (bindTypes != null && !found) {
            if (bindTypes.head.first.typeSpec.wrappedType.equalsLiterally(fortranTypeSpec)) {
                if (bindTypes.head.second == null) {
                    String name = cTypeSpec.genTypeDeclName();
                    bindTypes.head.second = new TypeDecl(name, cTypeSpec);
                } else if (bindTypes.head.second.typeSpec.wrappedType.equalsLiterally(cTypeSpec)) {
                    result = true;
                    found = true;
                } else if (TypeSpec.isA(cTypeSpec, SymbolTableConstants.POINTERTYPE)
                        && bindTypes.head.second.typeSpec.wrappedType.equalsLiterally(((PointerTypeSpec) cTypeSpec.wrappedType).destinationType)) {
                    result = true;
                    found = true;
                } else {
                    result = false;
                    found = true;
                }
            }
            bindTypes = bindTypes.tail;
        }
        return result;
    }

    public TypeDecl getOtherBindType(WrapperTypeSpec typeSpec) {
        TypeDecl result = null;
        TapList<TapPair<TypeDecl, TypeDecl>> bindTypes = bindFortranCType;
        while (bindTypes != null && result == null) {
            if (bindTypes.head.first.typeSpec.wrappedType.equalsLiterally(typeSpec)) {
                result = bindTypes.head.second;
            } else if (bindTypes.head.second.typeSpec.wrappedType.equalsLiterally(typeSpec)) {
                result = bindTypes.head.first;
            }
            bindTypes = bindTypes.tail;
        }
        return result;
    }

    private void deleteBindFortranCFunction(Unit unit) {
        TapList<TapTriplet<String, String, FunctionDecl>> toResult = new TapList<>(null, bindFortranCFunctions);
        TapList<TapTriplet<String, String, FunctionDecl>> inResult = toResult;
        TapTriplet<String, String, FunctionDecl> bindTriplet;
        while (inResult.tail != null) {
            bindTriplet = inResult.tail.head;
            if (bindTriplet.third != null
                    && bindTriplet.third.unit() == unit) {
                bindTriplet.third = null;
                inResult.tail = inResult.tail.tail;
            } else {
                inResult = inResult.tail;
            }
        }
        bindFortranCFunctions = toResult.tail;
    }

    public TapTriplet<String, String, FunctionDecl> getOtherLangFunctionDeclFromBindC(String funcName, int calleeLang) {
        TapTriplet<String, String, FunctionDecl> result = null;
        TapList<TapTriplet<String, String, FunctionDecl>> bindFortranCFuncs = bindFortranCFunctions;
        while (result == null && bindFortranCFuncs != null) {
            if (calleeLang != TapEnv.C && funcName.equals(bindFortranCFuncs.head.second)) {
                result = bindFortranCFuncs.head;
            } else if (calleeLang == TapEnv.C && funcName.equals(bindFortranCFuncs.head.first)) {
                result = bindFortranCFuncs.head;
            }
            bindFortranCFuncs = bindFortranCFuncs.tail;
        }
        if (result == null) {
            TapList<Unit> bindFortranCUnits = bindFortranCDiffUnits;
            while (bindFortranCUnits != null) {
                if (bindFortranCUnits.head.name().equals(funcName)
                        || bindFortranCUnits.head.name().equals("#" + funcName + "#")) {
                    result = new TapTriplet<>(funcName, funcName, null);
                }
                bindFortranCUnits = bindFortranCUnits.tail;
            }
        }
        return result;
    }

    /**
     * @param functionName   name of function in a call
     * @param callerLanguage TapEnv.C, TapEnv.FORTRAN, ...
     * @param calleeLanguage TapEnv.C, TapEnv.FORTRAN, ...
     * @return the corresponding Unit.name, i.e.
     * <pre>
     * if callerLanguage == TapEnv.C, the Fortran name {@code FOO}
     * if callerLanguage == TapEnv.FORTRAN, the C name {@code BAR} or bind's result
     * + FunctionDecl
     * </pre>
     */
    public TapPair<String, FunctionDecl> getMixedLanguageFunctionName(String functionName,
                                                                      int callerLanguage, int calleeLanguage) {
        String result;
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.printOnTrace("    Mixed-language " + callerLanguage + "->" + calleeLanguage + " " + functionName + " ");
        }
        TapTriplet<String, String, FunctionDecl> otherlangFuncDeclName =
                getOtherLangFunctionDeclFromBindC(functionName, calleeLanguage);
        if (otherlangFuncDeclName != null) {
            result = otherlangFuncDeclName.second;
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.printlnOnTrace("    Mixed-language 2003 -> " + result + " " + otherlangFuncDeclName.third);
            }
            return new TapPair<>(result, otherlangFuncDeclName.third);
        }

        result = getOtherLanguageName(functionName, callerLanguage, calleeLanguage);
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.printlnOnTrace("    Mixed-language -> " + result);
        }
        return new TapPair<>(result, null);
    }

    public String getOtherLanguageName(String symbolName, int callerLanguage,
                                       int calleeLanguage) {
        String result = symbolName;
        MixedLanguageInfos infos = functionNameBinding(callerLanguage, calleeLanguage);
        if (infos != null) {
            String bindingName = infos.getFunctionBinding();
            boolean addSuffix = bindingName.startsWith("add");
            String suffix = infos.getSuffix();
            if (suffix != null) {
                if (addSuffix) {
                    result = symbolName + suffix;
                } else if (symbolName.endsWith(suffix)) {
                    result = symbolName.substring(0, symbolName.length() - suffix.length());
                } else {
                    result = null;
                }
            } else {
                bindingName = bindingName.toLowerCase();
                if (bindingName.startsWith("upper")) {
                    result = result.toUpperCase();
                } else if (bindingName.startsWith("lower")) {
                    result = result.toLowerCase();
                }
            }
        }
        return result;
    }

    /**
     * Assigns a unique number to each SymbolTable. This number is
     * used mostly to identify SymbolTable's in debug and trace
     */
    public void numberSymbolTables() {
        int rk = 0;
        // Root symbolTable receives number 0:
        globalRootSymbolTable.setRank(rk);
        ++rk;
        if (cRootSymbolTable != null) {
            cRootSymbolTable.setRank(rk);
            ++rk;
        }
        if (cPlusPlusRootSymbolTable != null) {
            cPlusPlusRootSymbolTable.setRank(rk);
            ++rk;
        }
        if (fortranRootSymbolTable != null) {
            fortranRootSymbolTable.setRank(rk);
            ++rk;
        }
        if (cudaRootSymbolTable != null) {
            cudaRootSymbolTable.setRank(rk);
            ++rk;
        }

        TapList<Unit> allUnitsList = units();
        Unit unit;
        while (allUnitsList != null) {
            unit = allUnitsList.head;
            if (!MPIcallInfo.isMessagePassingFunction(unit.name(), unit.language())
                    && !unit.inStdCIncludeFile()
                    && (unit.isStandard() || unit.isExternal() || unit.isModule() || unit.isInterface())
            ) {
                SymbolTable externalSymbolTable = unit.externalSymbolTable();
                TapList<SymbolTable> externals = null;
                while (externalSymbolTable != null && externalSymbolTable.isImports) {
                    externals = new TapList<>(externalSymbolTable, externals);
                    externalSymbolTable = externalSymbolTable.basisSymbolTable();
                }
                while (externals != null) {
                    externals.head.setRank(++rk);
                    externals = externals.tail;
                }
                if (unit.publicSymbolTable() != null) {
                    unit.publicSymbolTable().setRank(++rk);
                }
                if (unit.privateSymbolTable() != null) {
                    unit.privateSymbolTable().setRank(++rk);
                    TapList<SymbolTable> seenSymbolTables = new TapList<>(unit.privateSymbolTable(), null);
                    TapList<Block> allBlocks = unit.allBlocks;
                    Block block;
                    while (allBlocks != null) {
                        block = allBlocks.head;
                        if (block != null && block.symbolTable != null &&
                                !TapList.contains(seenSymbolTables, block.symbolTable)) {
                            block.symbolTable.setRank(++rk);
                            seenSymbolTables = new TapList<>(block.symbolTable, seenSymbolTables);
                        }
                        allBlocks = allBlocks.tail;
                    }
                }
            }
            allUnitsList = allUnitsList.tail;
        }
    }

    /**
     * Dumps the hierarchy of SymbolTable's.
     */
    public void dumpAllSymbolTables(TapList<Unit> unitsToDump, boolean dumpAll,
                                    int indent) throws java.io.IOException {
        if (globalRootSymbolTable != null) {
            TapEnv.indentPrint(indent, globalRootSymbolTable.shortName() + " :");
            TapEnv.dumpSymbolTableIfNew(globalRootSymbolTable, indent);
            TapEnv.println();
        }
        TapEnv.indentPrint(indent, "FORTRAN GLOBAL MEMORY MAPS:") ;
        if (globalFortranMaps.maps.tail==null) {
            TapEnv.println(" none") ;
        } else {
            TapEnv.println() ;
            for (MemMap oneMap : globalFortranMaps.maps.tail) {
                TapEnv.indentPrintln(indent+2, oneMap.toString()) ;
            }
        }
        TapEnv.println();
        if (fortranRootSymbolTable != null) {
            TapEnv.indentPrint(indent, fortranRootSymbolTable.shortName() + " :");
            TapEnv.dumpSymbolTableIfNew(fortranRootSymbolTable, indent);
            TapEnv.println();
        }
//         TapEnv.indentPrint(indent, "C GLOBALS:") ;
//         if (globalCMaps==null) {
//             TapEnv.println(" none") ;
//         } else {
//             TapEnv.println() ;
//             for (TapList<ZoneInfoAccessElements> sharedAccesses : globalCMaps) {
//                 TapEnv.indentPrintln(indent+2, sharedAccesses.toString()) ;
//             }
//         }
//         TapEnv.println();
        if (cRootSymbolTable != null) {
            TapEnv.indentPrint(indent, cRootSymbolTable.shortName() + " :");
            TapEnv.dumpSymbolTableIfNew(cRootSymbolTable, indent);
            TapEnv.println();
        }
        if (cudaRootSymbolTable != null) {
            TapEnv.indentPrint(indent, cudaRootSymbolTable.shortName() + " :");
            TapEnv.dumpSymbolTableIfNew(cudaRootSymbolTable, indent);
            TapEnv.println();
        }
        if (cPlusPlusRootSymbolTable != null) {
            TapEnv.indentPrint(indent, cPlusPlusRootSymbolTable.shortName() + " :");
            TapEnv.dumpSymbolTableIfNew(cPlusPlusRootSymbolTable, indent);
            TapEnv.println();
        }

        TapList<SymbolTable> tuSTs = translationUnitSymbolTables;
        while (tuSTs != null) {
            TapEnv.indentPrint(indent, tuSTs.head.shortName() + " :");
            TapEnv.dumpSymbolTableIfNew(tuSTs.head, indent);
            TapEnv.println();
            tuSTs = tuSTs.tail;
        }

        Unit unit;
        while (unitsToDump != null) {
            unit = unitsToDump.head;
            // When dumping all, don't dump MPI and other std SymbolTables:
            if (!unit.isTranslationUnit() // translationUnitSymbolTables have already been dumped.
                && !(dumpAll &&
                     (MPIcallInfo.isMessagePassingFunction(unit.name(), unit.language())
                      || unit.inStdCIncludeFile()))) {
                // Dump SymbolTable of Imports:
                if (unit.importsSymbolTable()!=null) {
                    TapEnv.indentPrint(indent, unit.importsSymbolTable().shortName() + " :");
                    TapEnv.dumpSymbolTableIfNew(unit.importsSymbolTable(), indent+2);
                    TapEnv.println();
                } else {
                    TapEnv.indentPrintln(indent, "Imports SymbolTable of Unit " + unit + " is NULL");
                }

                if (unit.publicSymbolTable() != null) {
                    TapEnv.indentPrint(indent, unit.publicSymbolTable().shortName() + " :");
                    TapEnv.dumpSymbolTableIfNew(unit.publicSymbolTable(), indent + 2);
                    TapEnv.println();
                } else {
                    TapEnv.indentPrintln(indent, "Public SymbolTable of Unit " + unit + " is NULL");
                }
                if (unit.protectedSymbolTable() != null) {
                    TapEnv.indentPrint(indent, unit.protectedSymbolTable().shortName() + " :");
                    TapEnv.dumpSymbolTableIfNew(unit.protectedSymbolTable(), indent + 2);
                    TapEnv.println();
                }
                if (unit.privateSymbolTable() != null) {
                    TapEnv.indentPrint(indent, unit.privateSymbolTable().shortName() + " :");
                    TapEnv.dumpSymbolTableIfNew(unit.privateSymbolTable(), indent + 2);
                    TapEnv.println();
                }
                TapList<Block> allBlocks = unit.allBlocks;
                Block block;
                while (allBlocks != null) {
                    block = allBlocks.head;
                    if (block != null && block.symbolTable != null
                            && TapEnv.getSeenSymbolTableShortName(block.symbolTable) == null) {
                        if (block.symbolTable.shortName() != null
                                && block.symbolTable.shortName().contains("on top of")) {
                            TapEnv.indentPrint(indent, "Local scope SymbolTable of block #" + block.rank + " of " + unit.name() + ": ");
                        } else {
                            TapEnv.indentPrint(indent, block.symbolTable.shortName() + " :");
                        }
                        TapEnv.dumpSymbolTableIfNew(block.symbolTable, indent + 4);
                        TapEnv.println();
                    }
                    allBlocks = allBlocks.tail;
                }
            }
            unitsToDump = unitsToDump.tail;
        }
    }

    /**
     * Dumps all the contents of "callGraph", restricted to all Units in "unitsToDump".
     * Dumps SymbolTable hierarchy first, then all flow-graphs
     */
    public void dumpAllFlowGraphs(TapList<Unit> unitsToDump, int indent)
            throws java.io.IOException {
        TapEnv.println("CallGraph top Block: " + topBlock + " (@" + Integer.toHexString(topBlock.hashCode()) + ")");
        topBlock.dumpInstructions(indent + 2);
        TapEnv.println();
        Block langBlock = fortranRootSymbolTable.declarationsBlock;
        TapEnv.println("Fortran language top Block: " + langBlock + " (@" + Integer.toHexString(langBlock.hashCode()) + ")");
        langBlock.dumpInstructions(indent + 2);
        TapEnv.println();
        TapEnv.println();
        langBlock = cRootSymbolTable.declarationsBlock;
        TapEnv.println("C language top Block: " + langBlock + " (@" + Integer.toHexString(langBlock.hashCode()) + ")");
        langBlock.dumpInstructions(indent + 2);
        TapEnv.println();
        langBlock = cudaRootSymbolTable.declarationsBlock;
        TapEnv.println("Cuda language top Block: " + langBlock + " (@" + Integer.toHexString(langBlock.hashCode()) + ")");
        langBlock.dumpInstructions(indent + 2);
        TapEnv.println();
        langBlock = cPlusPlusRootSymbolTable.declarationsBlock;
        TapEnv.println("C++ language top Block: " + langBlock + " (@" + Integer.toHexString(langBlock.hashCode()) + ")");
        langBlock.dumpInstructions(indent + 2);
        TapEnv.println();
        for (TapList<SymbolTable> tuSTs = translationUnitSymbolTables; tuSTs != null; tuSTs = tuSTs.tail) {
            SymbolTable tuST = tuSTs.head;
            TapEnv.println("Top Block of file (\"Translation Unit\") " + tuST.containerFileName + "(" + TapEnv.outputExtension(tuST.language()) + "): " + tuST.declarationsBlock + " (@" + Integer.toHexString(tuST.declarationsBlock.hashCode()) + ")");
            tuST.declarationsBlock.dumpInstructions(indent + 2);
            TapEnv.println();
        }

        while (unitsToDump != null) {
            unitsToDump.head.dump(indent);
            unitsToDump = unitsToDump.tail;
            if (unitsToDump != null) {
                TapEnv.println();
            }
        }
    }

    /**
     * Dumps the data-flow info of the Units of this CallGraph,
     * restricted to all Units in "unitsToDump".
     */
    public void dumpAllDataFlows(TapList<Unit> unitsToDump) throws java.io.IOException {
        while (unitsToDump != null) {
            if (!unitsToDump.head.isModule() && !unitsToDump.head.isDummy()) {
                unitsToDump.head.dumpDataFlow(
                        TapEnv.get().dumpPointers, TapEnv.get().dumpInOut, TapEnv.get().dumpDeps, /*dumpInside=*/true);
            }
            unitsToDump = unitsToDump.tail;
        }
    }

    /**
     * Prints in detail the contents of this CallGraph, onto TapEnv.curOutputStream().
     *
     * @param indent the amount of indentation to be used for this printing.
     */
    public void dump(int indent) throws java.io.IOException {
        TapEnv.indentPrintln(indent, "DUMP of " + callGraphName + " :");
        TapEnv.indentPrintln(indent, "==========================================");
        TapEnv.resetSeenSymbolTables();
        TapEnv.indentPrint(indent, "Root Symbol Table: ");
        if (globalRootSymbolTable != null) {
            TapEnv.dumpSymbolTableIfNew(globalRootSymbolTable, indent + 2);
            TapEnv.println();
        }
        TapEnv.dumpSymbolTableIfNew(languageRootSymbolTable(TapEnv.inputLanguage()), indent + 2);
        TapEnv.indentPrint(indent, "CallGraph topBlock: ");
        topBlock.dump(indent);
        TapList<Unit> allUnits = sortedUnits();
        Unit unit;
        if (allUnits == null) {
            allUnits = units();
        }
        while (allUnits != null) {
            unit = allUnits.head;
            if (!MPIcallInfo.isMessagePassingFunction(unit.name(), unit.language())
                    && !unit.inStdCIncludeFile()) {
                TapEnv.println();
                unit.dump(indent + 2);
            }
            allUnits = allUnits.tail;
        }
    }

    /**
     * Utility for orderedFiles(): nodes of the graph that we will sort.
     */
    private static final class FileUnit {
        private Unit unit;
        private TapList<CallGraph.FileUnit> usedFiles;
        private int rank;

        private FileUnit(Unit unit) {
            super();
            this.unit = unit;
        }

        @Override
        public String toString() {
            String uses = null;
            TapList<CallGraph.FileUnit> isUsed = usedFiles;
            while (isUsed != null) {
                uses = uses == null ? "" : uses + ";";
                uses = uses + (isUsed.head == null ? "NULL!!" : isUsed.head.unit);
                isUsed = isUsed.tail;
            }
            return "FU[" + unit + " USES " + uses + "]";
        }
    }
}
