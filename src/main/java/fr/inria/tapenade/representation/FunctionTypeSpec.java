/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.Tree;

/**
 * A type for functions and procedures, for instance (arg1T, arg2T...)&rarr;returnT.
 */
public final class FunctionTypeSpec extends TypeSpec {

    /**
     * The return type of this function type.
     */
    public WrapperTypeSpec returnType;

    /**
     * The array of arguments types of this function type.
     */
    public WrapperTypeSpec[] argumentsTypes;

    /**
     * True if this the type of a function with a variable number of arguments.
     */
    public boolean variableArgList;

    /**
     * Create a new function type, empty.
     */
    public FunctionTypeSpec() {
        super(SymbolTableConstants.FUNCTIONTYPE);
    }

    /**
     * Create a new function type, with the given return type.
     */
    public FunctionTypeSpec(WrapperTypeSpec returnType) {
        super(SymbolTableConstants.FUNCTIONTYPE);
        this.returnType = returnType;
        this.argumentsTypes = null;
    }

    /**
     * Create a new function type, with the given arguments types and return type.
     */
    public FunctionTypeSpec(WrapperTypeSpec returnType, WrapperTypeSpec[] argumentsTypes) {
        super(SymbolTableConstants.FUNCTIONTYPE);
        this.returnType = returnType;
        this.argumentsTypes = argumentsTypes;
    }

    /**
     * Create a new function type of variable arguments length, with the given arguments types and return type.
     */
    public FunctionTypeSpec(WrapperTypeSpec returnType, WrapperTypeSpec[] argumentsTypes, boolean varArgList) {
        super(SymbolTableConstants.FUNCTIONTYPE);
        this.returnType = returnType;
        this.argumentsTypes = argumentsTypes;
        this.variableArgList = varArgList;
    }

    /**
     * Create a new function type, with the given arguments types and return type.
     */
    public FunctionTypeSpec(WrapperTypeSpec returnType, TapList<WrapperTypeSpec> paramsTypes) {
        super(SymbolTableConstants.FUNCTIONTYPE);
        this.returnType = returnType;
        argumentsTypes = new WrapperTypeSpec[TapList.length(paramsTypes)];
        for (int i = 0; i < argumentsTypes.length; i++) {
            argumentsTypes[i] = paramsTypes.head;
            paramsTypes = paramsTypes.tail;
        }
    }

    /**
     * Put as much info as possible from the actual type into the formal type,
     * but trying not to lose useful info already in the formalType.
     */
    private static void refineFormalWithActual(WrapperTypeSpec formalType, WrapperTypeSpec actualType,
                                               TapList<TypeSpec> dejaVu) {
        if (!TapList.contains(dejaVu, formalType)) {
            if (formalType.wrappedType == null) {
                formalType.wrappedType =
                        actualType.wrappedType == null ? null : actualType.wrappedType.copy();
            } else {
                //[lh] TODO: see if this is similar/redundant with TypeSpec.addWith()
                if (isA(formalType, SymbolTableConstants.ARRAYTYPE)) {
                    ArrayDim[] formalDims = ((ArrayTypeSpec) formalType.wrappedType).dimensions();
                    ArrayDim[] actualDims = null;
                    WrapperTypeSpec formalElementType = formalType.wrappedType.elementType();
                    WrapperTypeSpec actualElementType = actualType;
                    if (isA(actualType, SymbolTableConstants.ARRAYTYPE)) {
                        actualDims = ((ArrayTypeSpec) actualType.wrappedType).dimensions();
                        actualElementType = actualType.wrappedType.elementType();
                    }
                    if (formalDims == null) {
                        ((ArrayTypeSpec) formalType.wrappedType).setDimensions(actualDims);
                    } else if (actualDims != null && formalDims.length == actualDims.length) {
                        for (int i = formalDims.length - 1; i >= 0; --i) {
                            if (formalDims[i] == null || formalDims[i].isUnknown()) {
                                formalDims[i] = actualDims[i];
                            }
                        }
                    }
                    refineFormalWithActual(formalElementType, actualElementType,
                            new TapList<>(formalType, dejaVu));
                } else if (isA(formalType, SymbolTableConstants.PRIMITIVETYPE) && formalType.wrappedType.isUndefinedNumeric()) {
                    WrapperTypeSpec insideActualType = actualType;
                    ModifiedTypeSpec actualModif = null;
                    ArrayTypeSpec actualArray = null;
                    PrimitiveTypeSpec actualPrimitive = null;
                    if (isA(insideActualType, SymbolTableConstants.ARRAYTYPE)) {
                        actualArray = (ArrayTypeSpec) insideActualType.wrappedType;
                        insideActualType = actualArray.elementType();
                    }
                    if (isA(insideActualType, SymbolTableConstants.MODIFIEDTYPE)) {
                        actualModif = (ModifiedTypeSpec) insideActualType.wrappedType;
                        insideActualType = actualModif.elementType();
                    }
                    if (isA(insideActualType, SymbolTableConstants.PRIMITIVETYPE) && !insideActualType.wrappedType.isUndefinedNumeric()) {
                        actualPrimitive = (PrimitiveTypeSpec) insideActualType.wrappedType;
                    }
                    if (actualPrimitive != null) {
                        formalType.wrappedType = ((PrimitiveTypeSpec) formalType.wrappedType).addWith(insideActualType);
                        formalType.wrappedType.setUndefinedNumeric(true);
                    }
                    if (actualModif != null) {
                        formalType.wrappedType =
                                new ModifiedTypeSpec(new WrapperTypeSpec(formalType.wrappedType), actualModif);
                    }
                    if (actualArray != null) {
                        formalType.wrappedType = new ArrayTypeSpec(new WrapperTypeSpec(formalType.wrappedType), actualArray.dimensions());
                    }
                } else {
                    // ?other cases?
                }
            }
        }
        formalType.setHasUndefinedSize(actualType.hasUndefinedSize());
    }

    /**
     * Replace remaining unsolved meta types with primitive types deduced from
     * the name of the meta type e.g. "float..." &rarr; PrimitiveTypeSpec("float").
     */
    protected static void finishLastMetaTypes(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadySolved,
                                              SymbolTable symbolTable) {
        toAlreadySolved = toAlreadySolved.tail;
        while (toAlreadySolved != null) {
            TapTriplet<TypeSpec, TypeSpec, Boolean> metaTriplet = toAlreadySolved.head;
            if (metaTriplet.first instanceof MetaTypeSpec) {
                WrapperTypeSpec foundType = (WrapperTypeSpec) metaTriplet.second;
                if (foundType.wrappedType == null) {
                    String metaName = ((MetaTypeSpec) metaTriplet.first).name;
                    TypeSpec baseType = null;
                    if (metaName.startsWith("real") || metaName.startsWith("float")) {
                        baseType =
                                symbolTable.getTypeDecl("float").typeSpec.wrappedType;
                    } else if (metaName.startsWith("int")) {
                        baseType =
                                symbolTable.getTypeDecl("integer").typeSpec.wrappedType;
                    } else if (metaName.startsWith("complex")) {
                        baseType =
                                symbolTable.getTypeDecl("complex").typeSpec.wrappedType;
                    } else if (metaName.startsWith("bool")) {
                        baseType =
                                symbolTable.getTypeDecl("boolean").typeSpec.wrappedType;
                    } else if (metaName.startsWith("char")) {
                        baseType =
                                symbolTable.getTypeDecl("character").typeSpec.wrappedType;
                    }
                    if (baseType!=null) {
                        ModifiedTypeSpec modTypeSpec =
                            new ModifiedTypeSpec(new WrapperTypeSpec(baseType),
                                    ILUtils.build(ILLang.op_ident, TapEnv.UNDEFINEDSIZE), symbolTable);
                        modTypeSpec.setHasUndefinedSize(true);
                        foundType.wrappedType = modTypeSpec;
                    }
                }
            }
            toAlreadySolved = toAlreadySolved.tail;
        }
    }

    /**
     * @return true when this has a non-void return type.
     */
    public boolean isAFunction() {
        return returnType.wrappedType == null || !isA(returnType, SymbolTableConstants.VOIDTYPE);
    }

    public void setReturnTypeSpec(WrapperTypeSpec typeSpec, Tree declaratorI) {
        if (returnType == null) {
            returnType = new WrapperTypeSpec(typeSpec.wrappedType);
        } else if (returnType.wrappedType == null) {
            returnType.wrappedType = typeSpec.wrappedType;
        } else if (typeSpec != null && !returnType.receives(typeSpec, null, null)) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, declaratorI, "(DD06) Cannot combine successive declarations of function " + ILUtils.baseName(declaratorI) + " return type: " + returnType.showType() + " and " + typeSpec.showType() + " (overwritten previous)");
            returnType = typeSpec;
        }
    }

    /**
     * Tests that this function type can be called with the given actual types,
     * i.e. the arguments number matches and each arg formal type is larger than
     *  (i.e. is more general than, or "contains", or could receive) the actual arg type.
     * @param callResultType The expected result type at call site. May be null. (not used at present)
     * @param callArgTypes   The array of the actual arguments types at call site. May be null if unknown.
     * @return true if this function type matches the actual call,
     * or may be made to match it by instantiating its "meta" types..
     */
    public boolean matchesCall(TypeSpec callResultType, TypeSpec[] callArgTypes) {
        boolean matches = true;
        // Test arguments types:
        if (argumentsTypes != null && callArgTypes != null) {
            matches = argumentsTypes.length == callArgTypes.length;
            for (int i = argumentsTypes.length - 1; matches && i >= 0; --i) {
                if (argumentsTypes[i] != null) {
                    // Specially for Fortran pointers, when formal arg type is a non-pointer type Tf
                    // and actual arg is a pointer on type Ta, then the actual arg will be deref'ed
                    // so that matching is ok if Tf is larger than Ta.
                    TypeSpec formalArgType = argumentsTypes[i];
                    TypeSpec actualArgType = callArgTypes[i];
                    if (TapEnv.relatedLanguageIsFortran() && actualArgType != null && actualArgType.isPointer() && !formalArgType.isPointer()) {
                        actualArgType = actualArgType.peelPointer();
                    }
                    matches = formalArgType.receivesNoVectorNeglectSizes(actualArgType, null, null);
                }
            }
        }
        // It is unreasonable to test on the return type.
        return matches;
    }

    // Same as matchesCall, but requires exact match of types.
    // This is to find the best match when looking in the Lib files.
    // TODO: to be cleaned up and merged with other type comparisons.
    public boolean matchesCallExactly(TypeSpec callResultType, TypeSpec[] callArgTypes) {
        boolean matches = true;
        if (argumentsTypes != null && callArgTypes != null) {
            matches = argumentsTypes.length == callArgTypes.length;
            for (int i = argumentsTypes.length - 1; matches && i >= 0; --i) {
                if (argumentsTypes[i] != null) {
                    // Specially for Fortran pointers, when formal arg type is a non-pointer type Tf
                    // and actual arg is a pointer on type Ta, then the actual arg will be deref'ed
                    // so that matching is ok if Tf is equal Ta.
                    TypeSpec formalArgType = argumentsTypes[i];
                    TypeSpec actualArgType = callArgTypes[i];
                    if (TapEnv.relatedLanguageIsFortran() && actualArgType != null && actualArgType.isPointer() && !formalArgType.isPointer()) {
                        actualArgType = actualArgType.peelPointer();
                    }
                    matches = formalArgType.equalsNeglectSizes(actualArgType);
                }
            }
        }
        return matches;
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around an Function* make little sense, so we may peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison) && testIsEquals(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return testAcceptsUnspecified(comparison);
            }
        }
        // else other must be of matching kind:
        if (!(other instanceof FunctionTypeSpec)) {
            return false;
        }
        FunctionTypeSpec otherFunctionType = (FunctionTypeSpec) other;
        boolean comparesWell = true;

        // test arguments lengthes:
        if (argumentsTypes == null) {
            // if this's argumentsTypes are unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                if (otherFunctionType.argumentsTypes != null) {
                    argumentsTypes = new WrapperTypeSpec[otherFunctionType.argumentsTypes.length];
                    for (int i = argumentsTypes.length - 1; i >= 0; --i) {
                        argumentsTypes[i] = new WrapperTypeSpec(null);
                    }
                }
            } else {
                comparesWell = testAcceptsUnspecified(comparison);
            }
        } else if (otherFunctionType.argumentsTypes == null) {
            // if other's argumentsTypes are unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                otherFunctionType.argumentsTypes = new WrapperTypeSpec[argumentsTypes.length];
                for (int i = argumentsTypes.length - 1; i >= 0; --i) {
                    otherFunctionType.argumentsTypes[i] = new WrapperTypeSpec(null);
                }
            } else {
                comparesWell = false;
            }
        } else {
            comparesWell = argumentsTypes.length == otherFunctionType.argumentsTypes.length;
        }

        // test arguments types:
        if (argumentsTypes != null) { // cf above "Test arguments lengthes" when (!testHasInference(comparison) && testIsReceives(comparison))
            for (int i = argumentsTypes.length - 1; comparesWell && i >= 0; --i) {
                if (argumentsTypes[i] == null) {
                    // if one argumentsType of this is unspecified, it all depends on type inference:
                    if (testHasInference(comparison)) {
                        TypeSpec otherArgType = otherFunctionType.argumentsTypes[i];
                        argumentsTypes[i].setWrappedType(otherArgType);
                    } else {
                        comparesWell = testAcceptsUnspecified(comparison);
                    }
                } else if (otherFunctionType.argumentsTypes[i] == null) {
                    // if one argumentsType of the other is unspecified, it all depends on type inference:
                    if (testHasInference(comparison)) {
                        otherFunctionType.argumentsTypes[i].setWrappedType(argumentsTypes[i].weakenForInference(comparison));
                    } else {
                        comparesWell = false;
                    }
                } else {
                    //Warning: reversed testing direction for arguments !
                    comparesWell =
                            otherFunctionType.argumentsTypes[i].comparesWith(argumentsTypes[i], comparison,
                                    null, null, dejaVu);
                }
            }
        }

        // test returned type
        if (comparesWell) {
            if (returnType == null) {
                // if the returned type of this is unspecified, it all depends on type inference:
                if (testHasInference(comparison)) {
                    TypeSpec otherReturnType = otherFunctionType.returnType;
                    setWrappedType(otherReturnType == null ? null : otherReturnType.weakenForInference(comparison));
                } else {
                    comparesWell = testAcceptsUnspecified(comparison);
                }
            } else if (otherFunctionType.returnType == null) {
                // if the returned type of the other is unspecified, it all depends on type inference:
                if (testHasInference(comparison)) {
                    otherFunctionType.setWrappedType(returnType);
                } else {
                    comparesWell = false;
                }
            } else {
                comparesWell = returnType.comparesWith(otherFunctionType.returnType, comparison,
                        this, otherFunctionType, dejaVu);
            }
        }
        return comparesWell;
    }

    // Apparently only used during reading a lib specification file.
    // TODO cleanup/merge with standardized comparesWith()
    public boolean sameTypes(WrapperTypeSpec otherReturnType, WrapperTypeSpec[] otherArgumentsTypes) {
        boolean matches = argumentsTypes == null && otherArgumentsTypes == null;
        boolean argMatches;
        if (otherArgumentsTypes != null && argumentsTypes != null) {
            matches = otherArgumentsTypes.length == argumentsTypes.length;
            if (matches) {
                for (int i = 0; i < otherArgumentsTypes.length; i++) {
                    if (otherArgumentsTypes[i] == null) {
                        argMatches = true;
                    } else {
                        WrapperTypeSpec otherArgType = otherArgumentsTypes[i];
                        WrapperTypeSpec thisArgType = argumentsTypes[i];

                        //Fortran specific: pointer actual argument implicitely
                        // deref'ed when formal argument is not a pointer:
                        // TODO rajouter TapEnv.relatedLanguageIsFortran()?:
                        if (isA(otherArgType, SymbolTableConstants.POINTERTYPE) &&
                                !isA(thisArgType, SymbolTableConstants.POINTERTYPE) &&
                                !thisArgType.isMetaType()) {
                            otherArgType =
                                    ((PointerTypeSpec) otherArgType.wrappedType)
                                            .destinationType;
                        }
                        // We're still a little lazy: When both types are arrays,
                        // we just compare element types, not dimensions:
                        if (isA(otherArgType, SymbolTableConstants.ARRAYTYPE)
                                && isA(thisArgType, SymbolTableConstants.ARRAYTYPE)) {
                            thisArgType =
                                    thisArgType.wrappedType
                                            .elementType();
                            otherArgType =
                                    otherArgType.wrappedType
                                            .elementType();
                        }
                        //C++ Specific : if one wrappedType is null, it means that's an
                        // opt arg
                        argMatches =
                            (thisArgType.wrappedType == null
                             || otherArgType.wrappedType == null
                             || otherArgType.equalsCompilIndep(thisArgType)) ;
                    }
                    matches = argMatches && matches;
                }
            }
        }
        return matches;
    }

    /**
     * Instantiate this formal type of a function with the actual types "actualArgsTypes"
     * of a particular call. This function formal type must be localized i.e. it is a copy
     * that will not influence by unification the true, central, formal type of the function.
     */
    protected void matchArgumentsTypes(WrapperTypeSpec[] actualArgsTypes,
                                       Unit ctxtUnit, Unit calledUnit) {
        WrapperTypeSpec localFormalType;
        WrapperTypeSpec actualArgType;
        for (int i = 0; i < argumentsTypes.length && i < actualArgsTypes.length; ++i) {
            localFormalType = argumentsTypes[i];
            if (localFormalType != null) {
                actualArgType = actualArgsTypes[i];
                // In F90, deref pointers before sending them to an intrinsic function:
                if ((ctxtUnit == null || ctxtUnit.isFortran9x())
                        && isA(actualArgType, SymbolTableConstants.POINTERTYPE)
                        && calledUnit.isIntrinsic()) {
                    actualArgType =
                            ((PointerTypeSpec) actualArgType.wrappedType).destinationType;
                }
                boolean alreadyUnified = false;
                // Put as much information as possible from actualArgType into localFormalType:
                if (isA(localFormalType, SymbolTableConstants.ARRAYTYPE)) {
                    ArrayTypeSpec localFormalArrayType =
                            (ArrayTypeSpec) localFormalType.wrappedType;
                    if (WrapperTypeSpec.isFree(localFormalArrayType.elementType())) {
                        if (isA(actualArgType, SymbolTableConstants.ARRAYTYPE)) {
                            localFormalArrayType.setElementType(actualArgType.wrappedType.elementType());
                        } else {
                            localFormalArrayType.setElementType(actualArgType);
                        }
                        // Forbid further type unification between localFormalType and actualArgType,
                        // because they now share a part, and this may lead to circular types!
                        alreadyUnified = true;
                    }
                    if ((localFormalArrayType.dimensions() == null
                            || localFormalArrayType.dimensions().length == 0)
                            && isA(actualArgType, SymbolTableConstants.ARRAYTYPE)) {
                        localFormalArrayType.setDimensions(((ArrayTypeSpec) actualArgType.wrappedType).dimensions());
                    }
                }
                if (actualArgType != null && !alreadyUnified) {
                    boolean canRefine = localFormalType.receives(actualArgType, null, null);
                    if (!canRefine) {
                        //Also accept refinement in the following case so that e.g. sin(float) accepts a float[*]:
                        // [llh 30/09/2014] TODO this is awkward. Maybe it works? but it should be done better.
                        canRefine = actualArgType.receives(localFormalType, null, null);
                    }
                    if (canRefine) {
                        refineFormalWithActual(localFormalType, actualArgType, null);
                    }
                }
            }
        }
    }

    @Override
    public TypeSpec wrappedType() {
        return returnType;
    }

    @Override
    public void setWrappedType(TypeSpec type) {
        returnType = type instanceof WrapperTypeSpec ? (WrapperTypeSpec) type : new WrapperTypeSpec(type);
    }

    @Override
    protected String baseTypeName() {
        return "";
    }

    @Override
    public WrapperTypeSpec baseTypeSpec(boolean stopOnPointer) {
        return returnType.baseTypeSpec(stopOnPointer);
    }

    @Override
    public boolean containsUnknownDimension() {
        return returnType != null && returnType.containsUnknownDimension();
    }

    @Override
    public void doUpdateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        if (argumentsTypes != null) {
            for (int i = argumentsTypes.length - 1; i >= 0; --i) {
                if (argumentsTypes[i] != null) {
                    argumentsTypes[i].updateAfterImports(symbolTable, dejaVu);
                }
            }
        }
        if (returnType != null) {
            returnType.updateAfterImports(symbolTable, dejaVu);
        }
    }

    @Override
    protected int computeSize() {
        //not very coherent with the others "computeSize()"...
        return returnType.size();
    }

    @Override
    public boolean isFunction() {
        return true;
    }

    /**
     * Shallow copy of this function type.
     */
    @Override
    public TypeSpec copy() {
        FunctionTypeSpec result = new FunctionTypeSpec();
        if (this.returnType != null) {
            result.returnType = new WrapperTypeSpec(this.returnType.wrappedType);
        }
        if (this.argumentsTypes != null) {
            result.argumentsTypes = new WrapperTypeSpec[this.argumentsTypes.length];
            for (int i = 0; i < this.argumentsTypes.length; i++) {
                result.argumentsTypes[i] = new WrapperTypeSpec(this.argumentsTypes[i].wrappedType);
            }
        }
        result.variableArgList = this.variableArgList;
        return result;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    protected boolean containsMetaType(TapList<TypeSpec> dejaVu) {
        if (TapList.contains(dejaVu, this)) {
            return false;
        }
        dejaVu = new TapList<>(this, dejaVu);
        boolean contains = returnType != null && returnType.containsMetaType(dejaVu);
        if (argumentsTypes != null) {
            for (int i = 0; !contains && i < argumentsTypes.length; ++i) {
                if (argumentsTypes[i] != null) {
                    contains = argumentsTypes[i].containsMetaType(dejaVu);
                }
            }
        }
        return contains;
    }

    @Override
    public TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        FunctionTypeSpec copiedResult = (FunctionTypeSpec) findAlreadyCopiedType(toAlreadyCopied, containsMeta);
        if (copiedResult == null) {
            // Insert right away an unfinished copy into "toAlreadyCopied", to avoid infinite looping:
            copiedResult = new FunctionTypeSpec(null,
                    (argumentsTypes==null ? null : new WrapperTypeSpec[argumentsTypes.length]),
                    variableArgList);
            copiedResult.setOrAddTypeDeclName(typeDeclName());
            TapTriplet<TypeSpec, TypeSpec, Boolean> alreadyRef = new TapTriplet<>(this, copiedResult, Boolean.FALSE);
            toAlreadyCopied.placdl(alreadyRef);
            if (returnType != null) {
                copiedResult.returnType = (WrapperTypeSpec) returnType.localize(toAlreadyCopied, containsMeta);
            }
            ToBool subTypeContainsMeta;
            if (argumentsTypes != null) {
                for (int i = argumentsTypes.length - 1; i >= 0; --i) {
                    if (argumentsTypes[i] == null) {
                        copiedResult.argumentsTypes[i] = null;
                    } else {
                        subTypeContainsMeta = new ToBool(false);
                        copiedResult.argumentsTypes[i] =
                                (WrapperTypeSpec) argumentsTypes[i].localize(toAlreadyCopied, subTypeContainsMeta);
                        if (subTypeContainsMeta.get()) {
                            containsMeta.set(true);
                        }
                    }
                }
            }
            if (containsMeta.get()) {
                // If this type contains a meta, write it in the memo of already localized types:
                alreadyRef.third = Boolean.TRUE;
            } else {
                // Now that we have avoided infinite looping in this type, if this type contains no Meta type,
                // we remove it from toAlreadyCopied to make sure that only copies of types with Meta are shared.
                alreadyRef.first = null;
            }
        }
        return copiedResult;
    }

    @Override
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable, int diffUnitSort, String fSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax, String hintArrayNameInText,
                                                 String hintArrayNameInIdent, Tree hintArrayNameTree, Tree nameTree) {
        WrapperTypeSpec result = null;
        if (TapEnv.associationByAddress()) {
            diffTypeSpec = new WrapperTypeSpec(this);
            result = diffTypeSpec;
        }
        return result;
    }

    @Override
    protected boolean checkTypeSpecValidity(TapList<TypeSpec> dejaVu) {
        if (returnType != null && returnType.wrappedType == this) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC41) Illegal recursive type definition " + this.showType());
            return false;
        } else {
            if (returnType != null) {
                if (!returnType.checkTypeSpecValidity(dejaVu)) {
                    return false;
                }
            }
            if (argumentsTypes != null) {
                for (int i = argumentsTypes.length - 1; i >= 0; i--) {
                    if (argumentsTypes[i] != null) {
                        if (!argumentsTypes[i].checkTypeSpecValidity(dejaVu)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /** Returns true when this unitName, called with actualArgsTypes that possibly match
     * its formalArgsTypes, and therefore that seems to match the call site,
     * actually does NOT match because it is an "abs" (absolute value) actually
     * called on floats whereas this Unit is about "abs" called on complex.
     * The fact that we need this test may be seen as a weakness
     * or a bug in the actual type-checking process */
    public static boolean complexAbsAmbiguous(String unitName,
                                 TypeSpec[] formalArgsTypes, TypeSpec[] actualArgsTypes) {
        return (("abs".equals(unitName) || "dabs".equals(unitName))
                && formalArgsTypes!=null && formalArgsTypes[0]!=null
                && formalArgsTypes[0].isComplexBase()
                && actualArgsTypes!=null && actualArgsTypes[0]!=null
                && !actualArgsTypes[0].isComplexBase()) ;
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        Tree result;
        if (TapList.contains(dejaVu, this)) {
            TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) circular type");
            result = ILUtils.build(ILLang.op_ident, "CircularType");
        } else {
            dejaVu = new TapList<>(this, dejaVu);
            result = ILUtils.build(ILLang.op_functionType,
                    returnType.generateTree(symbolTable, null, shortNames, true, dejaVu),
                    ILUtils.build(ILLang.op_argDeclarations));
            if (argumentsTypes != null) {
                for (int i = 0; i < argumentsTypes.length; i++) {
                    result.down(2).setChild(
                            argumentsTypes[i].generateTree(symbolTable, null, shortNames, true, dejaVu),
                            i + 1);
                }
            }
            if (variableArgList) {
                result.down(2).addChild(ILUtils.build(ILLang.op_variableArgList), result.down(2).length() + 1);
            }
        }
        return result;
    }

    @Override
    public String showType() {
        String argsString;
        if (argumentsTypes == null) {
            argsString = "??";
        } else {
            argsString = variableArgList ? "...)" : ")";
            for (int i = argumentsTypes.length - 1; i >= 0; --i) {
                argsString = argumentsTypes[i] == null ? "?" : argumentsTypes[i].showType() + argsString;
                if (i > 0) {
                    argsString = "," + argsString;
                }
            }
            argsString = "(" + argsString;
        }
        return argsString + "=>" + (returnType == null ? "?" : returnType.showType());
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        StringBuilder argsString;
        if (argumentsTypes == null) {
            argsString = new StringBuilder("??");
        } else {
            argsString = new StringBuilder(variableArgList ? "...)" : ")");
            for (int i = argumentsTypes.length - 1; i >= 0; --i) {
                argsString.insert(0, argumentsTypes[i]);
                if (i > 0) {
                    argsString.insert(0, ",");
                }
            }
            argsString.insert(0, "(");
        }
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + argsString //+"@"+Integer.toHexString(hashCode())
                + "=>" + (returnType == null ? "?" : returnType.toString());
    }
}
