/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * Class that holds all info about a new symbol that we are
 * going to add into a subroutine. For each new variable name
 * that we wish to add, we create one such object. Then each
 * time we want to insert a new reference to this new variable,
 * we ask this object to build this new reference. Finally,
 * when all references are built, we ask this object to choose
 * the definitive name of the new variable, and insert
 * its SymbolDecl into the correct symbolTable.
 */
public final class NewSymbolHolder {
    /**
     * The decoration used to highlight non-solved symbols.
     */
    private static final String UNSOLVED_MARK = "#";
    /**
     * True when this new symbol is built after an existing model symbol
     * i.e. in general is differentiated (or copied) from another symbol.
     */
    private final boolean isDerived;
    /**
     * The SymbolDecl of the symbol that gives its base name for this new symbol.
     */
    public SymbolDecl derivationFrom;
    /**
     * When this new symbol is built after an existing model symbol,
     * holds the sort of transformation that goes from the model to this new symbol.
     * This sort of transformation is in general a form of differentiation,
     * such as  (_D, _FWD, _B, ...), including ORIGCOPY, which is a form of transformation.
     * When this new symbol is built independently (e.g. "temp" or "adFrom"),
     * then diffSymbolSort is set to ORIGCOPY==0 by convention.
     */
    private final int diffSymbolSort;
    /**
     * The probable future name of this new symbol.
     */
    public String probableName;
    /**
     * If needed, the VariableDecl for this future symbol.
     */
    public VariableDecl newVariableDecl;
    public VariableDecl newSolvedVariableDecl;
    /**
     * When this new symbol is declared by a module (of the target CallGraph), keeps this module.
     */
    public Unit definitionModule;
    /**
     * When non-null, this tree is used to build the ISIZEnOF... variables that represent
     * unknown dimension sizes, and the messages that come with them.
     */
    public Tree hintRootTree;
    /**
     * The Tree that must be used each time a variable must be created to
     * with size(hintArrayTreeForCallSize).
     */
    public Tree hintArrayTreeForCallSize;
    /**
     * If false, do not declare this new symbol (e.g. the end-user will do it).
     */
    public boolean declare = true;
    /**
     * a temporary zone number for this new symbol.
     */
    public int zone = -1;
    /**
     * The definitive name of this new symbol.
     */
    private String finalName;
    /**
     * The TapList of Tree's that refer to this new symbol.
     */
    private TapList<Tree> usageTrees;
    /**
     * The TapList of SymbolTable's from which the new symbol must be seen.
     */
    private TapList<SymbolTable> symbolUsageSymbolTables;
    /**
     * The SymbolTable level at which the current NewSymbolHolder must be solved.
     */
    private SymbolTable symbolSolvingRootSymbolTable;
    /**
     * A list of Instructions that use this new symbol: its declaration must be before.
     */
    private TapList<Instruction> usageInstructions;
    /**
     * A TapList of SymbolTable's in which the new VariableDecl must be placed.
     */
    private TapList<SymbolTable> newVariableDeclSymbolTables;
    /**
     * A TapList of SymbolTable's for which the declaration
     * instruction of the new VariableDecl is already placed in the final FlowGraph.
     */
    private TapList<SymbolTable> varDeclTreeAlreadyPlacedFor = null;
    /**
     * If needed, the FunctionDecl for this future symbol.
     */
    private FunctionDecl newFunctionDecl;
    /**
     * A TapList of SymbolTable's in which the new FunctionDecl must be placed.
     */
    private TapList<SymbolTable> newFunctionDeclSymbolTables;
    /**
     * If needed, the InterfaceDecl for this future symbol.
     */
    private InterfaceDecl newInterfaceDecl;
    /**
     * A TapList of SymbolTable's in which the new InterfaceDecl must be placed.
     */
    private TapList<SymbolTable> newInterfaceDeclSymbolTables;
    /**
     * If needed, the TypeDecl for this future symbol.
     */
    private TypeDecl newTypeDecl;
    /**
     * A TapList of SymbolTable's in which the new TypeDecl must be placed.
     */
    private TapList<SymbolTable> newTypeDeclSymbolTables;
    /**
     * A TapList of SymbolTable's for which the declaration
     * instruction of the new TypeDecl is already placed in the final FlowGraph.
     */
    private TapList<SymbolTable> typeDeclTreeAlreadyPlacedFor;
    /**
     * The SymbolTable level at which the main declaration of the current NewSymbolHolder must be put.
     */
    private SymbolTable varDeclarationRootSymbolTable;
    /**
     * lors de split fwd/bwd, il faut declarer le NewSymbolHolder dans les 2 units.
     */
    private SymbolTable otherDefSymbolTable;
    /**
     * The suffix that is appended to the end of base symbol.
     */
    private String derivationSuffix = "";
    /**
     * L'instruction associee a ce NewSymbolHolder
     * construite, mais pas encore inseree dans un Block.
     */
    private Instruction instruction;
    /**
     * When true, forces new references to be a C-like reference &amp;x.
     */
    private boolean usesRefTo;

    /**
     * This NewSymbolHolder depends on this TapList of symbolDecl.
     */
    private TapList<SymbolDecl> dependsOn;
    /**
     * Contains extraInfo of the future symbol
     * useful: when creating the NewSymbolHolder, the symbolDecl is null
     * and so it is not possible to use symbolDecl.extraInfo.
     */
    private TapList<String> extraInfo;

    /**
     * Builds a NewSymbolHolder for a future symbol of probable name "probableName"
     * "diffSymbolSort" is the sort (e.g. _FWD, _B, ...) of derivative that this
     * new symbol stands for.
     */
    public NewSymbolHolder(String probableName, int diffSymbolSort) {
        super();
        this.isDerived = true;
        this.probableName = probableName;
        this.diffSymbolSort = diffSymbolSort;
    }

    /**
     * Builds a NewSymbolHolder for a future symbol of probable name "probableName".
     */
    public NewSymbolHolder(String probableName) {
        super();
        this.isDerived = false;
        this.probableName = probableName;
        this.diffSymbolSort = 0/*DiffConstants.ORIGCOPY*/;
    }

    /**
     * Builds a NewSymbolHolder that will be placed inside hostUnit,
     * of probable name "probableName", of the given type, possibly with a size modifier.
     */
    public NewSymbolHolder(String probableName, Unit hostUnit, WrapperTypeSpec type, int sizeModifier) {
        super();
        this.isDerived = false;
        this.probableName = probableName;
        this.diffSymbolSort = 0/*DiffConstants.ORIGCOPY*/;
        if (sizeModifier != -1) {
            type = new WrapperTypeSpec(new ModifiedTypeSpec(type, ILUtils.build(ILLang.op_intCst, sizeModifier), null));
        }
        this.setAsVariable(type, null);
        this.prepareDeclarationInstr(hostUnit);
    }

    /**
     * @return true if the given "refTree" is a reference to a NewSymbolHolder.
     */
    public static boolean isANewSymbolRef(Tree refTree) {
        return refTree.getAnnotation("NewSymbolHolder") != null;
    }

    /**
     * Assuming that the given "refTree" is a reference to a NewSymbolHolder.
     *
     * @return this NewSymbolHolder.
     */
    public static NewSymbolHolder getNewSymbolHolder(Tree refTree) {
        return refTree.getAnnotation("NewSymbolHolder");
    }

    public static boolean sameNSH(Tree tree1, Tree tree2) {
        NewSymbolHolder nsh1 = tree1.getAnnotation("NewSymbolHolder");
        NewSymbolHolder nsh2 = tree2.getAnnotation("NewSymbolHolder");
        return nsh1 == nsh2;
    }

    /**
     * Checks that the array dimensions in typeSpec use only numerical constants.
     *
     * @param treeInCallSize
     * @param symbolTable
     */
    public static WrapperTypeSpec checkVariableDimension(
            WrapperTypeSpec typeSpec, String arrayNameInIdent, String arrayNameInText, Tree treeInCallSize,
            Unit diffUnit, Unit origUnit, SymbolTable symbolTable) {
        if (typeSpec != null) {
            TypeSpec actualTypeSpec = typeSpec.wrappedType;
            if (actualTypeSpec != null) {
                switch (actualTypeSpec.kind()) {
                    case SymbolTableConstants.MODIFIEDTYPE: {
                        ModifiedTypeSpec oldModifiedType = (ModifiedTypeSpec) actualTypeSpec;
                        WrapperTypeSpec newElementType =
                                checkVariableDimension(oldModifiedType.elementType(),
                                        arrayNameInIdent, arrayNameInText,
                                        treeInCallSize, diffUnit, origUnit, symbolTable);
                        if (newElementType == oldModifiedType.elementType()) {
                            return typeSpec;
                        } else {
                            return new WrapperTypeSpec(new ModifiedTypeSpec(newElementType, oldModifiedType));
                        }
                    }
                    case SymbolTableConstants.ARRAYTYPE: {
                        ArrayTypeSpec oldArrayType = (ArrayTypeSpec) actualTypeSpec;
                        ArrayDim[] dimensions = oldArrayType.dimensions();
                        ArrayDim[] newDimensions = new ArrayDim[dimensions.length];
                        boolean oneDimensionChanged = false;
                        int dimRank;
                        for (int i = dimensions.length - 1; i >= 0; i--) {
                            if (origUnit.isC()) {
                                dimRank = i + 1;
                            } else {
                                // on inverse en fortran
                                dimRank = dimensions.length - i;
                            }
                            newDimensions[i] =
                                    checkArrayDimension(dimensions[i], dimRank,
                                            arrayNameInIdent, arrayNameInText,
                                            treeInCallSize, diffUnit, origUnit, symbolTable);
                            if (newDimensions[i] != dimensions[i]) {
                                oneDimensionChanged = true;
                            }
                        }
                        WrapperTypeSpec newElementType =
                                checkVariableDimension(oldArrayType.elementType(),
                                        arrayNameInIdent, arrayNameInText,
                                        treeInCallSize, diffUnit, origUnit, symbolTable);
                        if
                        (oneDimensionChanged
                                || newElementType != oldArrayType.elementType()) {
                            return
                                    new WrapperTypeSpec(
                                            new ArrayTypeSpec(newElementType, newDimensions));
                        } else {
                            return typeSpec;
                        }
                    }
                    default:
                        return typeSpec;
                }
            } else {
                return typeSpec;
            }
        } else {
            return null;
        }
    }

    private static ArrayDim checkArrayDimension(ArrayDim arrayDim,
                                                int dimRank, String arrayNameInIdent, String arrayNameInText,
                                                Tree treeInSize, Unit diffUnit, Unit origUnit,
                                                SymbolTable symbolTable) {
        Tree lengthTree = null ;
        if (arrayDim.unDeclarableLength || ILUtils.isNullOrNoneOrStar(arrayDim.tree())) {
            if (TapEnv.get().noisize77 && treeInSize != null && ILUtils.isAVarRef(treeInSize, symbolTable)) {
                // then size will be a call to "size" function, whatever it means in F77:
                lengthTree = ILUtils.buildCall(
                               ILUtils.build(ILLang.op_ident, "size"),
                               ILUtils.build(ILLang.op_expressions,
                                 ILUtils.copy(treeInSize)));
            } else if (!TapEnv.get().noisize77) {
                // then size will be a ISIZEnOFvar, except if user insisted on TapEnv.get().noisize77:
                if (arrayDim.upper == null && !isNBDirsMax(arrayDim.tree())) {
                    lengthTree = createIsizeLengthTree(arrayDim, dimRank, arrayNameInIdent,
                                                       arrayNameInText, diffUnit, origUnit);
                }
            }
        }
        if (lengthTree!=null) {
            if (arrayDim.unDeclarableLength) {
                arrayDim.setSizeForDeclaration(lengthTree);
            } else {
                arrayDim.setSize(lengthTree);
            }
        }
        return arrayDim;
    }

    private static Tree createIsizeLengthTree(ArrayDim arrayDim, int dimRank,
                                                String arrayNameInIdent, String arrayNameInText,
                                                Unit diffUnit, Unit origUnit) {
        String sizeHint = "the value of "+ILUtils.toString(arrayDim.getSize())
            +(arrayDim.overEstimate==null ? "" : ", overestimate:"+arrayDim.overEstimate);
        if (TapEnv.relatedUnit() != null) {
            origUnit = TapEnv.relatedUnit();
        }
        String sizeName =
                "ISIZE" + dimRank + "OF" + arrayNameInIdent + "IN" + origUnit.name();
        TapEnv.fileWarning(TapEnv.MSG_WARN, null,
            "(AD11) User help requested: constant "+sizeName+" must hold the size of dimension "+dimRank
            +" of "+arrayNameInText +" (should be "+sizeHint+")");
        diffUnit.userHelp.placdl(new TapPair<>(sizeHint, sizeName));
        return ILUtils.build(ILLang.op_ident, sizeName) ;
    }

    /**
     * @return true iff this array dim tree represents the maximum directions number.
     * This code is approximative. More accurate code may prove necessary...
     */
    private static boolean isNBDirsMax(Tree dimTree) {
        if (dimTree == null) {
            return false;
        }
        switch (dimTree.opCode()) {
            case ILLang.op_dimColon:
                return isNBDirsMax(dimTree.down(2));
            case ILLang.op_add:
                return isNBDirsMax(dimTree.down(1)) || isNBDirsMax(dimTree.down(2));
            case ILLang.op_sub:
                return isNBDirsMax(dimTree.down(1));
            case ILLang.op_ident: {
                String name = ILUtils.getIdentString(dimTree).toLowerCase();
                return name.equals("#nbdirsmax#") || name.equals("nbdirsmax");
            }
            default:
                return false;
        }
    }

    /**
     * If there is a "NewSymbolHolder" annotation on "fromTree", put a similar
     * annotation on "toTree", and register "toTree" as a new occurrence of
     * this NewSymbolHolder. As a consequence, the given "newUsageSymbolTable",
     * which is the SymbolTable on toTree, must see the future declaration of
     * this NewSymbolHolder,  This is to permit creation of a new symbol
     * based on another new symbol, with the SymbolHolder mechanism (cf adj10.f)
     */
    public static void copySymbolHolderAnnotation(
            Tree fromTree, Tree toTree, SymbolTable newUsageSymbolTable) {
        NewSymbolHolder symbolHolder =
                fromTree.getAnnotation("NewSymbolHolder");
        if (symbolHolder != null) {
            symbolHolder.registerNewRef(toTree, newUsageSymbolTable);
            symbolHolder.declarationLevelMustInclude(newUsageSymbolTable);
        }
    }

    public static VariableDecl getAttachedVariableDecl(Tree tree) {
        NewSymbolHolder symbolHolder =
                tree.getAnnotation("NewSymbolHolder");
        if (symbolHolder != null) {
            return symbolHolder.newVariableDecl;
        } else {
            return null;
        }
    }

    public static TapPair<NewSymbolHolder, Tree> buildCptrTree(Tree expr, SymbolTable usageSymbolTable,
                                                               boolean isNullPTR,
                                                               String cptrName, Unit targetUnit,
                                                               WrapperTypeSpec type, boolean initCptr) {
        NewSymbolHolder newSH;
        NewSymbolHolder symbolHolder = getNewSymbolHolder(usageSymbolTable, cptrName);
        if (symbolHolder == null) {
            newSH = new NewSymbolHolder(cptrName, targetUnit,
                    new WrapperTypeSpec(new NamedTypeSpec("C_PTR")), -1);
            Tree declCptr = ILUtils.build(ILLang.op_varDeclaration,
                    ILUtils.build(ILLang.op_modifiers),
                    ILUtils.build(ILLang.op_ident, "C_PTR"),
                    ILUtils.build(ILLang.op_declarators, newSH.makeNewRef(usageSymbolTable)));
            newSH.setInstruction(new Instruction(declCptr));
        } else {
            newSH = symbolHolder;
        }
        newSH.declarationLevelMustInclude(usageSymbolTable);
        Tree result;
        Tree cptrTree = newSH.makeNewRef(usageSymbolTable);
        Tree assignExpr = null;
        if (initCptr) {
            if (isNullPTR) {
                assignExpr = ILUtils.build(ILLang.op_assign, cptrTree, ILUtils.build(ILLang.op_ident, "C_NULL_PTR"));
            } else {
                assignExpr = ILUtils.build(ILLang.op_assign, cptrTree, ILUtils.buildCLoc(expr, "C_LOC"));
            }
        }
        Tree callCFPTR = ILUtils.buildCall(ILUtils.build(ILLang.op_ident, "c_f_pointer"),
                ILUtils.build(ILLang.op_expressions, newSH.makeNewRef(usageSymbolTable),
                        ILUtils.copy(expr)));
        boolean needShape = TypeSpec.isA(((PointerTypeSpec) type.wrappedType).destinationType, SymbolTableConstants.ARRAYTYPE);
        if (needShape) {
            String shape = "UNKNOWN_SHAPE_IN_" + targetUnit.origUnit.name();
            shape = shape.toUpperCase();
            symbolHolder = getNewSymbolHolder(usageSymbolTable, shape);
            NewSymbolHolder shapeHolder;
            if (symbolHolder == null) {
                shapeHolder = new NewSymbolHolder(shape, targetUnit,
                        new WrapperTypeSpec(new PrimitiveTypeSpec("integer")), -1);
                Tree declShape = ILUtils.build(ILLang.op_varDeclaration,
                        ILUtils.build(ILLang.op_modifiers),
                        ILUtils.build(ILLang.op_integer),
                        ILUtils.build(ILLang.op_declarators, shapeHolder.makeNewRef(usageSymbolTable)));
                shapeHolder.setInstruction(new Instruction(declShape));
            } else {
                shapeHolder = symbolHolder;
            }
            ILUtils.getArguments(callCFPTR).addChild(ILUtils.build(ILLang.op_arrayConstructor,
                    shapeHolder.makeNewRef(usageSymbolTable)), 3);
            shapeHolder.declarationLevelMustInclude(usageSymbolTable);
        }
        if (initCptr) {
            result = ILUtils.build(ILLang.op_blockStatement,
                    assignExpr,
                    callCFPTR);
        } else {
            result = ILUtils.build(ILLang.op_blockStatement,
                    callCFPTR);
        }
        targetUnit.usesISO_C_BINDING = true;
        return new TapPair<>(newSH, result);
    }

    private static NewSymbolHolder getNewSymbolHolder(SymbolTable usageSymbolTable, String cptrName) {
        TapList<NewSymbolHolder> inSymbolHolders = usageSymbolTable.newSymbolHolders.tail;
        NewSymbolHolder symbolHolder = null;
        NewSymbolHolder oneSymbolHolder;
        while (symbolHolder == null && inSymbolHolders != null) {
            oneSymbolHolder = inSymbolHolders.head;
            if (oneSymbolHolder.probableName() != null
                    && oneSymbolHolder.probableName().equals(cptrName)) {
                symbolHolder = oneSymbolHolder;
            }
            inSymbolHolders = inSymbolHolders.tail;
        }
        return symbolHolder;
    }

    /**
     * Say that references to this variable X must emulate PASS-BY-REFERENCE by using {@code *X} and {@code &X}.
     */
    public void setUsesRefTo() {
        usesRefTo = true;
    }

    public boolean usesRefTo() {
        return usesRefTo;
    }

    public boolean hasNewFunctionDecl() {
        return newFunctionDecl != null;
    }

    public TypeDecl newTypeDecl() {
        return newTypeDecl;
    }

    /**
     * Declares that this NewSymbolHolder represents a VARIABLE name,
     * of the given "typeSpec", and this "typeSpec" depends on the
     * previous declarations "dependsOn".
     */
    public void setAsVariable(WrapperTypeSpec typeSpec, TapList<SymbolDecl> dependsOn) {
        if (newVariableDecl == null) {
            newVariableDecl = new VariableDecl(makeNewRef(null), null);
        }
        newVariableDecl.setType(typeSpec);
        newVariableDecl.dependsOn = dependsOn;
    }

    public void setHintRootTree(Tree hintRootTree) {
        if (hintRootTree != null) {
            this.hintRootTree = hintRootTree;
        }
    }

    public void setHintArrayTreeForCallSize(Tree hintSize) {
        this.hintArrayTreeForCallSize = hintSize;
    }

    public void setDependsOn(TapList<SymbolDecl> dependsOn) {
        if (this.dependsOn == null) {
            this.dependsOn = dependsOn;
        } else {
            this.dependsOn = TapList.append(dependsOn, this.dependsOn);
        }
    }

    public TapList<SymbolDecl> dependsOn() {
        return dependsOn;
    }

    /**
     * Declares that this NewSymbolHolder represents a CONSTANT name,
     * of the given "typeSpec", and this "typeSpec" depends on the
     * previous declarations "dependsOn".
     */
    public void setAsConstantVariable(WrapperTypeSpec typeSpec, TapList<SymbolDecl> dependsOn,
                                      Tree value) {
        if (newVariableDecl == null) {
            newVariableDecl = new VariableDecl(makeNewRef(null), null);
        }
        newVariableDecl.setType(typeSpec);
        newVariableDecl.setConstant();
        newVariableDecl.setInitializationTree(value);
        newVariableDecl.dependsOn = dependsOn;
    }

    /**
     * Declares that this NewSymbolHolder represents a TYPE name.
     */
    public void setAsType(WrapperTypeSpec typeSpec, TapList<SymbolDecl> dependsOn) {
        Tree nameTree = ILUtils.build(ILLang.op_ident, UNSOLVED_MARK + probableName + UNSOLVED_MARK);
        newTypeDecl = new TypeDecl(nameTree, typeSpec);
        newTypeDecl.dependsOn = dependsOn;
    }

    public void setNewTypeDecl(TypeDecl typeDecl) {
        newTypeDecl = typeDecl;
    }

    /**
     * Builds a new Tree referencing the future symbol.
     * Register it so that it will receive the final name when chosen.
     * If symbolTable is not null, add it into the usage SymbolTable's.
     */
    public Tree makeNewRef(SymbolTable symbolTable) {
        Tree result;
        if (finalName == null) {
            // the final name is not chosen yet:
            result = ILUtils.build(ILLang.op_ident, UNSOLVED_MARK + probableName + UNSOLVED_MARK);
            registerNewRef(result, symbolTable);
        } else {
            result = ILUtils.build(ILLang.op_ident, finalName);
        }
        return result;
    }

    /**
     * Registers the "newUsage" Tree as a new usage of this NewSymbolHolder.
     * also registers "symbolTable" (if non null) as a SymbolTable that must
     * see the future declaration of this NewSymbolHolder.
     */
    private void registerNewRef(Tree newUsage, SymbolTable symbolTable) {
        boolean traceHere = false;
// traceHere = (derivationFrom!=null && ("xx".equals(derivationFrom.symbol) || "yy".equals(derivationFrom.symbol))) ;
// traceHere = fr.inria.tapenade.toplevel.Tapenade.flag && ("NBDirsMax".equals(probableName) || "nbdirsmax".equals(probableName)) ;
// if (traceHere) System.out.println("REGISTERING USAGE "+newUsage+" @"+Integer.toHexString(newUsage.hashCode())+" OF "+this+" FOR ST:"+symbolTable) ;
// if (traceHere) new Throwable().printStackTrace();
        newUsage.setAnnotation("NewSymbolHolder", this);
        usageTrees = new TapList<>(newUsage, usageTrees);
        solvingLevelMustInclude(symbolTable);
    }

    public void solvingLevelMustInclude(SymbolTable symbolTable) {
// if ("NBDirsMax".equals(probableName) || "nbdirsmax".equals(probableName)) {
// System.out.println("SOLVING LEVEL OF "+this+"\n   IS "+symbolSolvingRootSymbolTable+"\n   NOW MUST INCLUDE "+symbolTable) ;
// }
        //If finalName is chosen, it's too late to ask for solving!
        if (finalName == null && symbolTable != null
                && !TapList.contains(symbolUsageSymbolTables, symbolTable)) {
            symbolUsageSymbolTables =
                    new TapList<>(symbolTable, symbolUsageSymbolTables);
            if (symbolSolvingRootSymbolTable == null) {
                symbolSolvingRootSymbolTable = symbolTable;
                symbolSolvingRootSymbolTable.addNewSymbolHolder(this);
            } else if (!symbolTable.nestedIn(symbolSolvingRootSymbolTable)) {
                SymbolTable commonRootSymbolTable =
                        symbolSolvingRootSymbolTable.getCommonRoot(symbolTable);
                if (commonRootSymbolTable == null) {
                    TapEnv.toolError("(Creating new symbol 1) Internal error: Incoherent SymbolTables "
                                     + this + " in " + symbolTable.shortName());
new Throwable().printStackTrace() ;
                } else if (commonRootSymbolTable != symbolSolvingRootSymbolTable) {
                    symbolSolvingRootSymbolTable.delNewSymbolHolder(this);
                    symbolSolvingRootSymbolTable = commonRootSymbolTable;
                    symbolSolvingRootSymbolTable.addNewSymbolHolder(this);
                }
            }
        }
    }

    /**
     * @return true when the symbolSolvingRootSymbolTable resulting from merging with "symbolTable"
     * would be outside a C compilation unit, i.e. would be th C language root symboltable,
     * therefore causing conflicts when solving for the new name.
     */
    public boolean solvingRootIsOutsideFile(SymbolTable symbolTable) {
        if (symbolSolvingRootSymbolTable == null) {
            return false;
        }
        SymbolTable commonRootSymbolTable =
                symbolSolvingRootSymbolTable.getCommonRoot(symbolTable);
        return commonRootSymbolTable == symbolTable.getCallGraph().cRootSymbolTable();
    }

    /**
     * Says that the main SymbolDecl of this NewSymbolHolder must be
     * declared in a SymbolTable which must be seen by (i.e. must be a root symbol table of)
     * the given "symbolTable".
     */
    public void declarationLevelMustInclude(SymbolTable symbolTable) {
        if (symbolTable != null) {
            if (varDeclarationRootSymbolTable == null) {
                varDeclarationRootSymbolTable = symbolTable;
            } else if (!symbolTable.nestedIn(varDeclarationRootSymbolTable)) {
                SymbolTable commonRootSymbolTable =
                        varDeclarationRootSymbolTable.getCommonRoot(symbolTable);
                if (commonRootSymbolTable == null) {
                    TapEnv.toolError("(Creating new symbol 2) Internal error: Incoherent SymbolTables "
                                     + this + " in " + symbolTable.shortName());
                } else {
                    varDeclarationRootSymbolTable = commonRootSymbolTable;
                }
            }
        }
    }

    public void setOtherDefSymbolTable(SymbolTable symbolTable) {
        otherDefSymbolTable = symbolTable;
    }

    /**
     * Accumulates "symbolTable" into "newVariableDeclSymbolTables", so that
     * "newVariableDeclSymbolTables" is the "dominating" subset of the
     * accumulated symbolTable's. In other words, the resulting list contains no
     * pair of SymbolTable's where one is a root of the other.
     */
    private void addVariableDeclSymbolTable(SymbolTable symbolTable) {
        TapList<SymbolTable> toAlreadyHere = new TapList<>(null, newVariableDeclSymbolTables);
        TapList<SymbolTable> inAlreadyHere = toAlreadyHere;
        SymbolTable alreadySymbolTable;
        while (inAlreadyHere.tail != null && symbolTable != null) {
            alreadySymbolTable = inAlreadyHere.tail.head;
            if (alreadySymbolTable == symbolTable || symbolTable.nestedIn(alreadySymbolTable)) {
                symbolTable = null;
            } else if (alreadySymbolTable.nestedIn(symbolTable)) {
                inAlreadyHere.tail = inAlreadyHere.tail.tail;
            } else {
                inAlreadyHere = inAlreadyHere.tail;
            }
        }
        if (symbolTable == null) {
            newVariableDeclSymbolTables = toAlreadyHere.tail;
        } else {
            newVariableDeclSymbolTables = new TapList<>(symbolTable, toAlreadyHere.tail);
        }
    }

    public void setVarDeclTreeAlreadyPlacedFor(SymbolTable symbolTable) {
        varDeclTreeAlreadyPlacedFor =
                TapList.addUnlessPresent(varDeclTreeAlreadyPlacedFor, symbolTable);
    }

    public void setVarDeclTreeNotAlreadyPlaced() {
        varDeclTreeAlreadyPlacedFor = null;
    }

    public void addTypeDeclSymbolTable(SymbolTable symbolTable) {
        newTypeDeclSymbolTables =
                TapList.addUnlessPresent(newTypeDeclSymbolTables, symbolTable);
    }

    public void setTypeDeclTreeAlreadyPlacedFor(SymbolTable symbolTable) {
        typeDeclTreeAlreadyPlacedFor =
                TapList.addUnlessPresent(typeDeclTreeAlreadyPlacedFor, symbolTable);
    }

    private void addFunctionDeclSymbolTable(SymbolTable symbolTable) {
        newFunctionDeclSymbolTables =
                TapList.addUnlessPresent(newFunctionDeclSymbolTables, symbolTable);
    }

    private void addInterfaceDeclSymbolTable(SymbolTable symbolTable) {
        newInterfaceDeclSymbolTables =
                TapList.addUnlessPresent(newInterfaceDeclSymbolTables, symbolTable);
    }

    private boolean varDeclTreeAlreadyPlacedFor(SymbolTable symbolTable) {
        SymbolTable companionST = null;
        if (symbolTable.unit != null && symbolTable.unit.isFortran()) {
            if (symbolTable == symbolTable.unit.publicSymbolTable()) {
                companionST = symbolTable.unit.privateSymbolTable();
            } else if (symbolTable == symbolTable.unit.privateSymbolTable()) {
                companionST = symbolTable.unit.publicSymbolTable();
            }
        }
        return TapList.contains(varDeclTreeAlreadyPlacedFor, symbolTable)
                || (companionST != null
                && TapList.contains(varDeclTreeAlreadyPlacedFor, companionST));
    }

    /**
     * Prepares the Instruction that will contain the declaration of this NewSymbolHolder in targetUnit.
     */
    public void prepareDeclarationInstr(Unit targetUnit) {
        if (targetUnit != null && targetUnit.privateSymbolTable() != null) {
            Instruction newDecl = new Instruction();
            newDecl.block = targetUnit.privateSymbolTable().declarationsBlock;
            setInstruction(newDecl);
        }
    }

    public String probableName() {
        return probableName;
    }

    public String finalName() {
        return finalName;
    }

    public VariableDecl newVariableDecl() {
        return newVariableDecl;
    }

    public FunctionDecl newFunctionDecl() {
        return newFunctionDecl;
    }

    public WrapperTypeSpec typeSpec() {
        if (newVariableDecl != null && newVariableDecl.isA(SymbolTableConstants.VARIABLE)) {
            return newVariableDecl.type();
        } else if (newTypeDecl != null) {
            return newTypeDecl.typeSpec;
        } else {
            return null;
        }
    }

    public void declareUsageInstruction(Instruction instruction) {
        usageInstructions = new TapList<>(instruction, usageInstructions);
    }

    public void accumulateSymbolDecl(SymbolDecl newSymbolDecl, SymbolTable targetSymbolTable) {
        if (newSymbolDecl != null) {
            newSymbolDecl.symbol = UNSOLVED_MARK + probableName + UNSOLVED_MARK;
            solvingLevelMustInclude(targetSymbolTable);
            if (newSymbolDecl.isA(SymbolTableConstants.VARIABLE)) {
                newVariableDecl = (VariableDecl) newSymbolDecl;
                addVariableDeclSymbolTable(targetSymbolTable);
                extraInfo = newSymbolDecl.extraInfo();
            } else if (newSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                // It happens that an interfaceUnit remains instead of the full Unit that should have replaced it
                // this is probably a bug, but... In that case we can reach this place with the wrong newSymbolDecl
                // that points to the interfaceUnit. In that case, at least we should not erase a correct newFunctionDecl.
                if (!((((FunctionDecl) newSymbolDecl).unit() == null || ((FunctionDecl) newSymbolDecl).unit().isInterface())
                        && newFunctionDecl != null && newFunctionDecl.unit() != null && newFunctionDecl.unit().isStandard())) {
                    newFunctionDecl = (FunctionDecl) newSymbolDecl;
                }
                addFunctionDeclSymbolTable(targetSymbolTable);
            } else if (newSymbolDecl.isA(SymbolTableConstants.INTERFACE)) {
                newInterfaceDecl = (InterfaceDecl) newSymbolDecl;
                addInterfaceDeclSymbolTable(targetSymbolTable);
            } else if (newSymbolDecl.isA(SymbolTableConstants.TYPE)) {
                newTypeDecl = (TypeDecl) newSymbolDecl;
                addTypeDeclSymbolTable(targetSymbolTable);
            } else {
                TapEnv.toolError("Not yet implemented: accumulateSymbolDecl() for " + newSymbolDecl + " :" + probableName);
            }
        }
    }

    public void accumulateTargetForSymbolDecl(SymbolDecl modelSymbolDecl, SymbolTable targetSymbolTable) {
        solvingLevelMustInclude(targetSymbolTable);
        if (modelSymbolDecl.isA(SymbolTableConstants.VARIABLE) || modelSymbolDecl.isA(SymbolTableConstants.CONSTANT)) {
            addVariableDeclSymbolTable(targetSymbolTable);
        } else if (modelSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
            addFunctionDeclSymbolTable(targetSymbolTable);
        } else if (modelSymbolDecl.isA(SymbolTableConstants.INTERFACE)) {
            addInterfaceDeclSymbolTable(targetSymbolTable);
        } else if (modelSymbolDecl.isA(SymbolTableConstants.TYPE)) {
            addTypeDeclSymbolTable(targetSymbolTable);
        } else {
            TapEnv.toolError("Not yet implemented: accumulateTargetForSymbolDecl() for " + modelSymbolDecl + " :" + probableName);
        }
    }

    /**
     * Add available info from absorbedSymbolHolder into this.
     */
    public void absorb(NewSymbolHolder absorbedSymbolHolder) {
        if (absorbedSymbolHolder.newVariableDecl != null) {
            if (this.newVariableDecl == null) {
                this.newVariableDecl = absorbedSymbolHolder.newVariableDecl;
                this.newVariableDecl.replaceDiffSymbolHolder(absorbedSymbolHolder, this);
            } else if (absorbedSymbolHolder.newVariableDecl != this.newVariableDecl) {
                TapEnv.toolError("Not yet implemented: absorb " + absorbedSymbolHolder.newVariableDecl + " into " + this.newVariableDecl);
            }
        }
        if (absorbedSymbolHolder.newFunctionDecl != null) {
            if (this.newFunctionDecl == null) {
                this.newFunctionDecl = absorbedSymbolHolder.newFunctionDecl;
                this.newFunctionDecl.replaceDiffSymbolHolder(absorbedSymbolHolder, this);
            } else if (absorbedSymbolHolder.newFunctionDecl == this.newFunctionDecl
                    || absorbedSymbolHolder.newFunctionDecl.unit() == this.newFunctionDecl.unit()) {
                absorbedSymbolHolder.newFunctionDecl = null;
            } else {
                TapEnv.toolError("Not yet implemented: absorb " + absorbedSymbolHolder.newFunctionDecl + " into " + this.newFunctionDecl);
            }
        }
        if (absorbedSymbolHolder.newInterfaceDecl != null) {
            if (this.newInterfaceDecl == null) {
                this.newInterfaceDecl = absorbedSymbolHolder.newInterfaceDecl;
                this.newInterfaceDecl.replaceDiffSymbolHolder(absorbedSymbolHolder, this);
            } else if (absorbedSymbolHolder.newInterfaceDecl == this.newInterfaceDecl) {
                absorbedSymbolHolder.newInterfaceDecl = null;
            } else {
                TapEnv.toolError("Not yet implemented: absorb " + absorbedSymbolHolder.newInterfaceDecl + " into " + this.newInterfaceDecl);
            }
        }
        if (absorbedSymbolHolder.newTypeDecl != null) {
            if (this.newTypeDecl == null) {
                this.newTypeDecl = absorbedSymbolHolder.newTypeDecl;
                this.newTypeDecl.replaceDiffSymbolHolder(absorbedSymbolHolder, this);
            } else if (absorbedSymbolHolder.newTypeDecl != this.newTypeDecl) {
                TapEnv.toolError("Not yet implemented: absorb " + absorbedSymbolHolder.newTypeDecl + " into " + this.newTypeDecl);
            }
        }
        symbolUsageSymbolTables =
                TapList.union(symbolUsageSymbolTables, absorbedSymbolHolder.symbolUsageSymbolTables);
        if (this.symbolSolvingRootSymbolTable == null) {
            this.symbolSolvingRootSymbolTable = absorbedSymbolHolder.symbolSolvingRootSymbolTable;
        }
        usageInstructions =
                TapList.union(usageInstructions, absorbedSymbolHolder.usageInstructions);
        newVariableDeclSymbolTables =
                TapList.union(newVariableDeclSymbolTables, absorbedSymbolHolder.newVariableDeclSymbolTables);
        varDeclTreeAlreadyPlacedFor =
                TapList.union(varDeclTreeAlreadyPlacedFor, absorbedSymbolHolder.varDeclTreeAlreadyPlacedFor);
        newFunctionDeclSymbolTables =
                TapList.union(newFunctionDeclSymbolTables, absorbedSymbolHolder.newFunctionDeclSymbolTables);
        newInterfaceDeclSymbolTables =
                TapList.union(newInterfaceDeclSymbolTables, absorbedSymbolHolder.newInterfaceDeclSymbolTables);
        newTypeDeclSymbolTables =
                TapList.union(newTypeDeclSymbolTables, absorbedSymbolHolder.newTypeDeclSymbolTables);
        typeDeclTreeAlreadyPlacedFor =
                TapList.union(typeDeclTreeAlreadyPlacedFor, absorbedSymbolHolder.typeDeclTreeAlreadyPlacedFor);
        if (this.varDeclarationRootSymbolTable == null) {
            this.varDeclarationRootSymbolTable = absorbedSymbolHolder.varDeclarationRootSymbolTable;
        }
        dependsOn =
                TapList.union(dependsOn, absorbedSymbolHolder.dependsOn);
        // make every user of absorbedSymbolHolder now use this NewSymbolHolder instead:
        TapList<Tree> oldUsages = absorbedSymbolHolder.usageTrees;
        while (oldUsages != null) {
            oldUsages.head.setAnnotation("NewSymbolHolder", this);
            this.usageTrees = new TapList<>(oldUsages.head, this.usageTrees);
            oldUsages = oldUsages.tail;
        }
        // absorbedSymbolHolder must not be used nor solved any more:
        absorbedSymbolHolder.usageTrees = null;
    }

    /**
     * SOLVE a NewSymbolHolder.
     * When we reach the rootmost SymbolTable, solve this NewSymbolHolder
     * i.e. choose its final name and insert the new symbol SymbolDecl(s).
     */
    public void solve(SymbolTable symbolTable, boolean noCopySuffix) {
        boolean traceHere = false;
// traceHere = true ;
// traceHere = (derivationFrom!=null && ("myfunc".equals(derivationFrom.symbol) || "ii".equals(derivationFrom.symbol))) ;
// traceHere = ("NBDirsMax".equals(probableName) || "nbdirsmax".equals(probableName)) ;
// if (traceHere) System.out.println("SOLVING "+this+" FOR ST:"+symbolTable) ;
// if (traceHere) new Throwable().printStackTrace();
        if (this.usageTrees != null) {
            if (varDeclarationRootSymbolTable != null) {
                addVariableDeclSymbolTable(varDeclarationRootSymbolTable);
            }
            int i = -1;
            if (derivationFrom != null) {
                if (derivationFrom.symbol != null && derivationFrom.isA(SymbolTableConstants.VARIABLE)
                    && derivationFrom.getDiffSymbolHolder(diffSymbolSort, null, 0) == null
                ) {
                    // If the symbol from which this one is derived has finally received
                    //  its final name, then recompute this symbol's probable name.
                    // We detect this situation with the property that the SymbolDecl derivationFrom,
                    // when solved, has lost its link to its differentiated NewSymbolHolder.
                    // [llh 04/10/2013] TODO: this has strange effects like replacing tmp0b with tmpb0. ??
                    probableName = TapEnv.extendStringWithSuffix(derivationFrom.symbol, derivationSuffix);
                }
            }
            if (!symbolTable.isCaseDependent()) {
                probableName = probableName.toLowerCase();
            }

            // TODO: We have a problem with multi-definition of the same symbol
            // maybe we should resolve symbols after the differentiation and not during it.
            //if (finalName == null) {
            finalName = probableName;
            if (!isASameNameCopy(noCopySuffix)) {
                i = 0;
                // cas special d'une varDecl correspondant a une funcDecl C
                TapList<SymbolTable> insideUnitSymbolTables = null;
//                 boolean isReturnVar = false;
                if (newVariableDecl != null && newVariableDecl.isReturnVar && symbolTable.unit != null) {
                    insideUnitSymbolTables = symbolTable.unit.symbolTablesBottomUp();
//                     symbolUsageSymbolTables = TapList.append(symbolUsageSymbolTables,
//                                                      symbolTable.unit.symbolTablesBottomUp());
//                     isReturnVar = true;
                }
                while (!availableSymbol(finalName, symbolUsageSymbolTables, insideUnitSymbolTables)) {
// if (traceHere) System.out.println(" NAME ALREADY IN USE: "+finalName+" IN STs:"+symbolUsageSymbolTables+" AND INSIDEUNIT STs:"+insideUnitSymbolTables) ;
                    if (TapEnv.inputLanguage() != TapEnv.MIXED) {
                        finalName = probableName + i++;
                    } else {
                        if (newFunctionDecl != null && newFunctionDecl.unit() != null
                                && newFunctionDecl.unit().isCalledFromOtherLanguage()
                                && newFunctionDecl.unit().isC()) {
                            if (TapEnv.isC(symbolTable.language()) || symbolTable.basisSymbolTable() == null) {
                                String name = symbolTable.getCallGraph().getMixedLanguageFunctionName(probableName,
                                        newFunctionDecl.unit().language(), TapEnv.FORTRAN).first;
                                name = name + i++;
                                finalName = symbolTable.getCallGraph().getMixedLanguageFunctionName(name,
                                        TapEnv.FORTRAN, newFunctionDecl.unit().language()).first;
                            } else {
                                String name = symbolTable.getCallGraph().getMixedLanguageFunctionName(probableName,
                                        TapEnv.FORTRAN, newFunctionDecl.unit().language()).first;
                                finalName = symbolTable.getCallGraph().getMixedLanguageFunctionName(name,
                                        newFunctionDecl.unit().language(), TapEnv.FORTRAN).first;
                                finalName = finalName + i++;
                            }
                        } else {
                            finalName = probableName + i++;
                        }
                    }
                }
            }
// if (traceHere) System.out.println("SOLVED AS "+finalName) ;

            symbolUsageSymbolTables = null;
            Tree usageTree;
            String usageTreeStringValue;
            if (TapEnv.inputLanguage() != TapEnv.MIXED) {
                while (usageTrees != null) {
                    usageTree = usageTrees.head;
                    usageTree.setValue(finalName);
                    usageTree.removeAnnotation("NewSymbolHolder");
                    usageTrees = usageTrees.tail;
                }
            } else {
                if (newFunctionDecl != null && newFunctionDecl.unit() != null
                        && newFunctionDecl.unit().isCalledFromOtherLanguage()) {
                    if (newFunctionDecl.unit().isC()) {
                        finalName = finalName.toLowerCase();
                    }
                }
                while (usageTrees != null) {
                    usageTree = usageTrees.head;
                    usageTreeStringValue = usageTree.stringValue();
                    if (!usageTreeStringValue.isEmpty()) {
                        usageTreeStringValue = usageTreeStringValue.substring(1, usageTreeStringValue.length() - 1);
                        if (usageTreeStringValue.equals(probableName)) {
                            usageTree.setValue(finalName);
                        } else {
                            String finalNameC = usageTreeStringValue;
                            finalNameC = finalNameC.toLowerCase(); // mixed language avec bind(c) sans name = ...
                            if (i != 0) {
                                finalNameC = finalNameC + (i - 1);
                            }
                            usageTree.setValue(finalNameC);
                        }
                        usageTree.removeAnnotation("NewSymbolHolder");
                    }
                    usageTrees = usageTrees.tail;
                }
            }

            if (newVariableDecl != null) {
                newVariableDecl.symbol = finalName;
                newVariableDecl.eraseDiffSymbolHolder(diffSymbolSort);
                // pour les variables, on nettoie les public/private
                // si la declaration doit apparaitre ailleurs que dans un module Fortran.
                if (extraInfo != null && oneNotInModule(newVariableDeclSymbolTables)) {
                    extraInfo = TapList.cleanExtraInfoValue(extraInfo, "public");
                    extraInfo = TapList.cleanExtraInfoValue(extraInfo, "private");
                }
                // on nettoie aussi les intent in out inout et optional pour les variables
                // qui ne sont pas dans des publicSymbolTable
                if (TapList.containsOneOfStrings(extraInfo,
                        new String[]{"in", "out", "inout", "optional", "value"},
                        symbolTable.unit != null && !symbolTable.unit.isFortran())
                        && oneDeclarationInLocalVars() != null) {
                    extraInfo = TapList.cleanExtraInfoValue(extraInfo, "in");
                    extraInfo = TapList.cleanExtraInfoValue(extraInfo, "out");
                    extraInfo = TapList.cleanExtraInfoValue(extraInfo, "inout");
                    extraInfo = TapList.cleanExtraInfoValue(extraInfo, "value");
                    extraInfo = TapList.cleanExtraInfoValue(extraInfo, "optional");
                }
                newVariableDecl.setExtraInfo(extraInfo);
// if (traceHere) System.out.println(" DERIVFROM:"+derivationFrom+" ISAVAR:"+(derivationFrom != null && derivationFrom.isA(SymbolTableConstants.VARIABLE)));
// if (traceHere) System.out.println(" NEWVARIABLEDECL:"+newVariableDecl) ;
                if (derivationFrom != null
                        && derivationFrom.isA(SymbolTableConstants.VARIABLE)
                        && newVariableDecl.formalArgRank == SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK) {
                    newVariableDecl.formalArgRank = ((VariableDecl) derivationFrom).formalArgRank;
                }
                SymbolTable pbFortranSymbolTable;
// if (traceHere) System.out.println("instruction:"+instruction+" hasMainInstruction:"+newVariableDecl.hasMainInstruction()) ;
                if (instruction == null && !newVariableDecl.hasMainInstruction()) {
                    Tree typeTree;
                    if (newVariableDecl.type().wrappedType == null) {
                        TapEnv.toolWarning(-1, "(New symbol declaration) null wrapped type for " + finalName);
                        typeTree = ILUtils.build(ILLang.op_ident, "UnknownType");
                    } else {
                        typeTree = newVariableDecl.type().wrappedType.generateTree(null, null, null, true, null);
                    }
                    if (newVariableDecl.extraInfo() != null) {
                        TapList<String> extra = TapList.reverse(newVariableDecl.extraInfo());
                        TapList<Tree> modifiers = null;
                        while (extra != null) {
                            modifiers = new TapList<>(ILUtils.build(ILLang.op_ident, extra.head), modifiers);
                            extra = extra.tail;
                        }
                        typeTree = ILUtils.build(ILLang.op_modifiedType,
                                ILUtils.build(ILLang.op_modifiers, modifiers),
                                typeTree);
                    }
                    Tree newTree =
                            ILUtils.build(ILLang.op_varDeclaration,
                                    ILUtils.build(ILLang.op_modifiers),
                                    typeTree,
                                    ILUtils.build(ILLang.op_declarators,
                                            ILUtils.build(ILLang.op_ident, newVariableDecl.symbol)));
// if (traceHere) System.out.println("  NEWTREE:"+newTree+" FROM "+newVariableDecl) ;
                    // TODO + variable init
                    if (newVariableDecl.formalArgRank < 0) {
                        // When the new variable is not declared in the arguments list, create a declaration for it:
                        instruction = new Instruction(newTree);
                    }
                    newVariableDecl.setInstruction(instruction);
                }
                if (declare) {
// if (traceHere) System.out.println("DECLARE! "+instruction+" newVariableDecl:"+newVariableDecl) ;
                    if (instruction != null && instruction.tree != null && newVariableDecl.isA(SymbolTableConstants.VARIABLE)
                            && (pbFortranSymbolTable = oneSymbolTableIsF77Local(newVariableDeclSymbolTables)) != null) {
                        // In FORTRAN, one cannot declare a local array
                        // with a dimension which is unknown statically.
                        WrapperTypeSpec oldTypeSpec = newVariableDecl.type();
                        String hintNameInIdent = finalName;
                        String hintNameInText = finalName;
                        if (hintRootTree != null) {
                            hintNameInIdent = ILUtils.buildNameInText(hintRootTree);
                            hintNameInText = ILUtils.buildNameInText(hintRootTree);
                        }
                        newVariableDecl.setType(checkVariableDimension(newVariableDecl.type(),
                                hintNameInIdent, hintNameInText, hintArrayTreeForCallSize,
                                pbFortranSymbolTable.unit, pbFortranSymbolTable.origUnit(), symbolTable));
                        newVariableDecl.setType(
				newVariableDecl.type()
				.checkNoneDimensionsOfNewSH(hintNameInText, hintNameInIdent, hintArrayTreeForCallSize,
							    pbFortranSymbolTable, this));
                        if (oldTypeSpec != newVariableDecl.type()) {
                            Tree declarators = instruction.tree.down(3);
                            int rank = ILUtils.findRankOfDeclarator(declarators, newVariableDecl.symbol);
                            if (rank > 0) {
                                declarators.removeChild(rank);
                            }
                            newVariableDecl.setNullInstruction();
                            instruction = null;
                        }
                    }

                    if (!newVariableDecl.hasInstructionWithRootOperator(ILLang.op_varDeclaration, null)
                            && instruction != null) {
                        if (instruction.tree == null) {
                            instruction.tree =
                                    ILUtils.build(ILLang.op_varDeclaration,
                                            ILUtils.build(ILLang.op_modifiers),
                                            newVariableDecl.type().wrappedType.generateTree(null, null, null, true, null),
                                            ILUtils.build(ILLang.op_declarators,
                                                    ILUtils.build(ILLang.op_ident, finalName)));
                        }
                        newVariableDecl.setInstruction(instruction);
                    }
                    SymbolTable vdSymbolTable;

// if (traceHere) System.out.println("NEWVARIABLEDECLSYMBOLTABLES:"+newVariableDeclSymbolTables+" RECEIVE "+newVariableDecl+" OTHERDEF:"+otherDefSymbolTable) ;

                    while (newVariableDeclSymbolTables != null) {
                        vdSymbolTable = newVariableDeclSymbolTables.head;
// if (traceHere) System.out.println(" -- INTO "+vdSymbolTable+" DECL BLOCK:"+vdSymbolTable.declarationsBlock+" ADD INSTRUCTION:"+instruction+" ALREADY PLACED?:"+varDeclTreeAlreadyPlacedFor(vdSymbolTable)) ;
                        addSymbolDeclAndInstr(vdSymbolTable, newVariableDecl);
                        newVariableDeclSymbolTables = newVariableDeclSymbolTables.tail;
                    }
                    if (otherDefSymbolTable != null) {
// if (traceHere) System.out.println(" -- ALSO INTO otherDefSymbolTable:"+otherDefSymbolTable+" DECL BLOCK:"+otherDefSymbolTable.declarationsBlock) ;
                        // on copie newVariableDecl, elle ne doit pas etre partagee entre unit_fwd et unit_bwd
                        VariableDecl copyVariableDecl = (VariableDecl) newVariableDecl.copy(null, null);
                        addSymbolDeclAndInstr(otherDefSymbolTable, copyVariableDecl);
                    }
                }
                if (derivationFrom != null
                        && derivationFrom instanceof VariableDecl
                        && ((VariableDecl) derivationFrom).initializationTree() != null) {
                    Tree origInitTree = ((VariableDecl) derivationFrom).getInitializationTree();
                    Tree diffInitVal = newVariableDecl.type().wrappedType.buildConstantZero();
                    if (!derivationFrom.isA(SymbolTableConstants.CONSTANT)) {
                        diffInitVal = ILUtils.build(origInitTree.opCode(), null, diffInitVal);
                    }
                    newVariableDecl.setInitializationTree(diffInitVal);
                }
                newSolvedVariableDecl = newVariableDecl;
                if (!TapEnv.associationByAddress()) {
                    // TODO vmp pourquoi ce test sur AA???
                    // on utilise le NewSymbolHolder resolu pour obtenir
                    // le nom des parametres differenties d'une unit
                    newVariableDecl = null;
                }
            }

            if (newFunctionDecl != null) {
// if (traceHere) System.out.println("  NEWFUNCTIONDECL:"+newFunctionDecl+" FOR UNIT:"+newFunctionDecl.unit()) ;
// if (traceHere) System.out.println("  PUBST:"+newFunctionDecl.unit().publicSymbolTable());
                newFunctionDecl.symbol = finalName;
                newFunctionDecl.eraseDiffSymbolHolder(diffSymbolSort);
                newFunctionDecl.setExtraInfo(extraInfo);
                //Don't let the "USE_with_rename" renamed names corrupt
                // the true original name of the imported subroutine :
                if (derivationFrom.importedFrom == null
                        && newFunctionDecl.isA(SymbolTableConstants.FUNCTION)
                        && newFunctionDecl.unit() != null) {
                    Unit unit = newFunctionDecl.unit();
                    boolean unitIsFortran = unit.isFortran();
                    boolean symbolTableIsFortran = TapEnv.isFortran(symbolTable.language());
                    // Don't corrupt the name of a FORTRAN routine by solving its name seen from C:
                    // These tests below are heuristics: we still need a cleaner decision method!
                    if ((unitIsFortran && symbolTableIsFortran)
                            || (!unitIsFortran && !symbolTableIsFortran)
                            || (unit.name().startsWith("#") && symbolTable.language() == TapEnv.UNDEFINED)) {
                        unit.setName(finalName);
                    }
                }
                if (instruction != null) {
                    newFunctionDecl.setInstruction(instruction);
                }
                while (newFunctionDeclSymbolTables != null) {
                    newFunctionDeclSymbolTables.head.addSymbolDecl(newFunctionDecl);
                    newFunctionDeclSymbolTables = newFunctionDeclSymbolTables.tail;
                }

                // remove original return var declaration, only in a few carefully chosen (!) special cases:
                Unit newFunctionUnit = newFunctionDecl.unit() ;
                if (newFunctionUnit!=null && !newFunctionUnit.isModule()) {
                    SymbolTable newFunctionPublicST = newFunctionUnit.publicSymbolTable() ;
                    if (newFunctionPublicST!=null) {
                        Unit origUnit = newFunctionUnit.origUnit ;
                        if (TapEnv.associationByAddress() && origUnit!=null && origUnit.isFortran()
                            && origUnit.isAFunction() && newFunctionUnit.sortOfDiffUnit!=0/*DiffConstants.ORIGCOPY*/) {
                            newFunctionPublicST.removeDecl(origUnit.name(), SymbolTableConstants.VARIABLE, true) ;
                        }
                    }
                }

                newFunctionDecl = null;
            }

            if (newTypeDecl != null) {
                if (TypeSpec.isA(newTypeDecl.typeSpec, SymbolTableConstants.COMPOSITETYPE)
                        && !((CompositeTypeSpec) newTypeDecl.typeSpec.wrappedType).isEmpty()
                        && (derivationFrom == null
                        || !newTypeDecl.typeSpec.equalsCompilDep(((TypeDecl) derivationFrom).typeSpec))) {
                    newTypeDecl.symbol = finalName;
//                 String typeName =
//                     newTypeDecl.typeSpec.wrappedType.typeDeclName();
                    if (finalName.startsWith("struct ")) {
                        finalName = finalName.substring(7);
                    }
                    newTypeDecl.typeSpec.wrappedType.setOrAddTypeDeclName(finalName);
//                 ((CompositeTypeSpec)newTypeDecl.typeSpec.wrappedType).name = finalName;
                    while (newTypeDeclSymbolTables != null) {
                        newTypeDeclSymbolTables.head
                                .addSymbolDecl(newTypeDecl);
                        newTypeDeclSymbolTables = newTypeDeclSymbolTables.tail;
                    }
//             } else if (TypeSpec.isA(newTypeDecl.typeSpec, POINTERTYPE)) {
//                 WrapperTypeSpec destTypeSpec = ((PointerTypeSpec)
//                             newTypeDecl.typeSpec.actualTypeSpec).destinationType;
//                 if (TypeSpec.isA(destTypeSpec, COMPOSITETYPE)
//                         && !((CompositeTypeSpec) destTypeSpec.actualTypeSpec).isEmpty()) {
//                 }
                }
            }

            if (newInterfaceDecl != null) {
                if (derivationFrom.isA(SymbolTableConstants.INTERFACE)) {
                    if (((InterfaceDecl) derivationFrom).usefulName) {
                        newInterfaceDecl.symbol = finalName;
                    }
                } else {
                    newInterfaceDecl.symbol = finalName;
                }
                if (!ILUtils.isNullOrNone(newInterfaceDecl.nameTree)) {
                    newInterfaceDecl.nameTree = ILUtils.build(ILLang.op_ident, finalName);
                }
                newInterfaceDecl.eraseDiffSymbolHolder(diffSymbolSort);
                newInterfaceDecl.setExtraInfo(extraInfo);
                while (newInterfaceDeclSymbolTables != null) {
                    if (newInterfaceDecl.contents != null || newInterfaceDecl.functionDecls != null) {
                        newInterfaceDeclSymbolTables.head
                                .addSymbolDecl(newInterfaceDecl);
                    }
                    newInterfaceDeclSymbolTables = newInterfaceDeclSymbolTables.tail;
                }
                TapList<Unit> interfaceUnits = newInterfaceDecl.interfaceUnits();
                if (interfaceUnits != null && interfaceUnits.tail == null) {
                    interfaceUnits.head.setName(finalName);
                }
                if (instruction != null) {
                    newInterfaceDecl.setInstruction(instruction);
                }
                newInterfaceDecl = null;
            }
        }
    }

    /**
     * @return true when this NewSymbolHolder is of the "copy" sort (e.g. _CD or _CB),
     * but we are in the mode where copies must keep the name of their model,
     * i.e. we will call one FOO() and not FOO_CD()
     */
    private boolean isASameNameCopy(boolean noCopySuffix) {
        return isDerived && diffSymbolSort == 0/*DiffConstants.ORIGCOPY*/ && noCopySuffix;

    }

    private void addSymbolDeclAndInstr(SymbolTable vdSymbolTable, VariableDecl variableDecl) {
        if (instruction != null && instruction.tree != null && newFunctionDecl == null
                && !varDeclTreeAlreadyPlacedFor(vdSymbolTable)) {
            // Insert the declaration statement, at a location which is
            // immediately after the last declaration that is before any use:
            Block declBlock = null;
            // on ne rajoute pas d'instruction pour les newSymbolHolders
            // place's "trop haut" dans cRootSymbolTable:
            if (vdSymbolTable != vdSymbolTable.getCallGraph().cRootSymbolTable()) {
                declBlock = vdSymbolTable.declarationsBlock;
            }
            // en C, la publicSymbolTable d'une Unit a un declBlock null
            // [llh 9jan24] PATCH: a bug sometimes causes duplication of the same declaration "INTEGER iiN".
            // The following "containsInstructionTree" test just patches that:
            if (declBlock != null && !declBlock.containsInstructionTree(instruction.tree)) {
                Instruction cpInstruction = new Instruction(instruction.copyTreePlusComments());
                declBlock.addInstrDeclTlBeforeUse(cpInstruction, new TapList<>(variableDecl, null),
                        usageInstructions, vdSymbolTable, true);
            }
        }
        vdSymbolTable.addSymbolDecl(variableDecl);
        setVarDeclTreeAlreadyPlacedFor(vdSymbolTable) ;
    }

    /**
     * If the list of "symbolTables" contains one local vars SymbolTable
     * of a Unit which will be regenerated in a non-F9x language,
     * returns this local vars SymbolTable. @return null otherwise.
     */
    private SymbolTable oneSymbolTableIsF77Local(TapList<SymbolTable> symbolTables) {
        SymbolTable found = null;
        SymbolTable symbolTable;
        while (found == null && symbolTables != null) {
            symbolTable = symbolTables.head;
            if (!symbolTable.isFormalParamsLevel() &&
                    symbolTable.origUnit() != null &&
                    !TapEnv.isFortran9x(symbolTable.origUnit().language()))
            // if this is a local vars symbol table and the unit will not be
            // regenerated in F9x, then return this symbolTable:
            {
                found = symbolTable;
            }
            symbolTables = symbolTables.tail;
        }
        return found;
    }

    private boolean oneNotInModule(TapList<SymbolTable> symbolTables) {
        boolean found = false;
        SymbolTable symbolTable;
        Unit declUnit;
        while (!found && symbolTables != null) {
            symbolTable = symbolTables.head;
            declUnit = symbolTable == null ? null : symbolTable.origUnit();
            if (declUnit != null && !declUnit.isModule() && declUnit.isFortran()) {
                found = true;
            }
            symbolTables = symbolTables.tail;
        }
        return found;
    }

    private SymbolTable oneDeclarationInLocalVars() {
        SymbolTable found = null;
        SymbolTable symbolTable;
        TapList<SymbolTable> symbolTables = newVariableDeclSymbolTables;
        while (found == null && symbolTables != null) {
            symbolTable = symbolTables.head;
            if (symbolTable.origUnit() != null && !symbolTable.isFormalParamsLevel()) {
                found = symbolTable;
            }
            symbolTables = symbolTables.tail;
        }
        return found;
    }

    /**
     * @return true when tentative name "symbol" is available to name a new symbol.
     * This is true when the following 2 conditions hold:<br>
     * -- "symbol" is not visible from any of the future usage places "usageSymbolTables"<br>
     * -- the future location of the new "symbol" is not the public part of a module
     * which is USE'd at places that already see something named "symbol".
     */
    private boolean availableSymbol(String symbol, TapList<SymbolTable> usageSymbolTables,
                                    TapList<SymbolTable> insideUnitSymbolTables) {
        boolean available = true;
        while (available && usageSymbolTables != null) {
            SymbolDecl symbolDecl = usageSymbolTables.head.getSymbolDecl(symbol);
            if (symbolDecl != null) {
                // The test insideUnitSymbolTables!=null indicates that this symbol is the return value of a function.
                if (insideUnitSymbolTables != null && symbolDecl.isA(SymbolTableConstants.FUNCTION)
                        && ((FunctionDecl) symbolDecl).unit() != null //useless test to be removed!
                ) {
                    available = newVariableDecl == ((FunctionDecl) symbolDecl).unit().otherReturnVar();
                } else {
                    available = false;
                }
            }
            usageSymbolTables = usageSymbolTables.tail;
        }
        // Following loop only for the function's return name (cf new NewSymbolHolder(funcNameString+"") in FlowGraphBuilder)
        while (available && insideUnitSymbolTables != null) {
            SymbolDecl symbolDecl = insideUnitSymbolTables.head.getTopSymbolDecl(symbol);
            if (symbolDecl != null && symbolDecl != newVariableDecl) {
                available = false;
            }
            insideUnitSymbolTables = insideUnitSymbolTables.tail;
        }
        // cf nonRegrF90:lh41, lh59, lha47, v185 :
        if (available && definitionModule != null) {
            TapList<CallArrow> users = definitionModule.callers();
            while (available && users != null) {
                if (users.head.origin.externalSymbolTable().getSymbolDecl(symbol) != null) {
                    available = false;
                }
                users = users.tail;
            }
        }
        //[llh 17apr18] The instruction below *may be* subsumed by the instruction above. TODO: check and maybe simplify
        if (available &&
                (symbolUsedInUsersOf(symbol, newVariableDeclSymbolTables)
                        || symbolUsedInUsersOf(symbol, newFunctionDeclSymbolTables)
                        || symbolUsedInUsersOf(symbol, newInterfaceDeclSymbolTables)
                        || symbolUsedInUsersOf(symbol, newTypeDeclSymbolTables)
                )) {
            available = false;
        }
        return available;
    }

    private boolean symbolUsedInUsersOf(String symbol, TapList<SymbolTable> symbolTables) {
        boolean used = false;
        SymbolTable symbolTable;
        Unit declUnit;
        TapList<CallArrow> users;
        while (!used && symbolTables != null) {
            symbolTable = symbolTables.head;
            declUnit = symbolTable.origUnit();
            if (declUnit != null && declUnit.isModule() && symbolTable.isFormalParamsLevel()) {
                users = declUnit.callers();
                while (!used && users != null) {
                    if (users.head.origin.externalSymbolTable().getSymbolDecl(symbol) != null) {
                        used = true;
                    }
                    users = users.tail;
                }
            }
            symbolTables = symbolTables.tail;
        }
        return used;
    }

    public void isDerivationFrom(SymbolDecl baseDecl, String suffix) {
        if (baseDecl != null) {
            derivationFrom = baseDecl;
            derivationSuffix = suffix;
        }
    }

    /**
     * @return true if this NewSymbolHolder is actually used
     * and therefore must be declared.
     */
    public boolean isUsed() {
        return symbolUsageSymbolTables != null;
    }

    public void setExtraInfo(TapList<String> info) {
        extraInfo = info;
    }

    public TapList<String> extraInfo() {
        return extraInfo;
    }

    //[llh 23-avr-2009] useful only for differentiated types that have been
// created too early (in ADActivityAnalyzer!)
    public void migrateToCopySymbolTable(SymbolTable copyST) {
        varDeclarationRootSymbolTable = null;
        symbolUsageSymbolTables = new TapList<>(copyST, null);
        symbolSolvingRootSymbolTable = copyST;
        symbolSolvingRootSymbolTable.addNewSymbolHolder(this);
    }

    public void setInstruction(Instruction instr) {
        instruction = instr;
    }

    public Instruction instruction() {
        return instruction;
    }

    public Instruction instruction(SymbolTable symbolTable) {
        Instruction result = null;
        if (symbolTable == null || symbolTable.basisSymbolTable() == null) {
            result = instruction;
        } else {
            if (instruction != null
                    && instruction.block != null
                    && symbolTable.unit == instruction.block.unit()) {
                result = instruction;
            } else if (instruction != null && instruction.block == null) {
                result = instruction;
            }
        }
        if (result == null && symbolTable != null) {
            if (this.newVariableDecl != null) {
                result = instruction;
            }
        }
        return result;
    }

    /**
     * If this future new symbol is used in a block whose source correspondent is "srcOfUsageBlock"
     * and if the opening clause ($OMP PARALLEL) of the enclosing OpenMP region is between usageBlock
     * and its corresponding (future) declaration scope "srcDeclST", then this opening clause must be
     * extended to specify that this new symbol is "omp private".
     *
     * @param srcOfUsageBlock (the source block of) the block where this NewSymbolHolder will be used.
     * @param srcDeclST       source correspondent of the SymbolTable where the new symbol will be declared.
     * @param inFwd           when true, the private clause goes to the fwd differentiated pragma
     * @param diff            when true, the private clause must be about the variable's derivative
     */
    public void preparePrivateClause(Block srcOfUsageBlock, SymbolTable srcDeclST,
                                     boolean inFwd, boolean diff) {
        Instruction parallelRegionInstr = TapList.last(srcOfUsageBlock.parallelControls);
        if (parallelRegionInstr != null
                //"declarationSymbolTable must be above/around parallelRegionInstr":
                // The goal is to avoid declaring as private a variable that is
                // declared *inside* (strictly) the parallel region.
                && !srcDeclST.basisSymbolTable().nestedIn(parallelRegionInstr.block.symbolTable)
        ) {
            ILUtils.addVarIntoFuturePrivates(this, parallelRegionInstr.tree, inFwd, diff);
        }
    }

    /** Same as preparePrivateClause, but will create a "omp shared" clause */
    public void prepareSharedClause(Block srcOfUsageBlock, SymbolTable srcDeclST,
                                    boolean inFwd, boolean diff) {
        Instruction parallelRegionInstr = TapList.last(srcOfUsageBlock.parallelControls);
        if (parallelRegionInstr != null
                //"declarationSymbolTable must be above/around parallelRegionInstr":
                // The goal is to avoid declaring as private a variable that is
                // declared *inside* (strictly) the parallel region.
                && !srcDeclST.basisSymbolTable().nestedIn(parallelRegionInstr.block.symbolTable)
        ) {
            ILUtils.addVarIntoFutureShareds(this, parallelRegionInstr.tree, inFwd, diff);
        }
    }

    /**
     * Same as preparePrivateClause, but will cause a REDUCTION(+:...),
     * always in the BWD sweep, and on the diff
     */
    public void prepareSumReductionClause(Block usageBlock) {
        // The declaration scope of the future new symbol:
        SymbolTable declarationSymbolTable = usageBlock.symbolTable;
        TapList<Instruction> parallelControls = usageBlock.parallelControls;
        while (parallelControls != null
                && parallelControls.head.tree.opCode() != ILLang.op_parallelRegion) {
            parallelControls = parallelControls.tail;
        }
        if (parallelControls != null
                // Following test probably wrong. The goal is to avoid declaring a clause about
                // variables that are *declared* inside the parallel region.
                && parallelControls.head.block.symbolTable.nestedIn(declarationSymbolTable)) {
            ILUtils.addVarIntoFutureSumReductions(this, parallelControls.head.tree);
        }
    }

    @Override
    public String toString() {
        return "NewSymbolHolder"  //+"@"+Integer.toHexString(hashCode())
                + (finalName == null ? (" Probable:" + probableName) : (" Final:" + finalName))
                + "{V:" + newVariableDecl + " F:" + newFunctionDecl
                + " T:" + newTypeDecl + " I:" + newInterfaceDecl + "}";
    }
}
