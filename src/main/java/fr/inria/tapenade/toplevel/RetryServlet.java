/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.toplevel;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This servlet is called when a user wants to retry the program
 * with the same files,
 * using the "Retry with the same files" button.
 */
public final class RetryServlet extends ADServlet {

    private static final long serialVersionUID = 354065432054L;

    /**
     * Called by the Tapenade server.
     *
     * @param request  an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws IOException if an input or output error is detected when the servlet handles the request
     */
    @Override
    public final void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        HttpSession session = request.getSession(false);
        String servletDir;
        String url;
        if (session != null) {
            System.out.println("Retry Servlet "+session.getId());
            servletDir = (String) session.getAttribute("ServletDir");
            int choice = (Integer) session.getAttribute("choice");
            String tmpDirToClean = (String) session.getAttribute(
                    "tmpDirToClean");
            if (tmpDirToClean != null) {
                File dir = new File(tmpDirToClean);
                if (dir.exists()) {
                    cleanSubDirectory("tapenadeGenDir", session, tmpDirToClean);
                    cleanSubDirectory("tapenadeOutputDir", session, tmpDirToClean);
                }
            }
            final int INTTEXT = 2;
            if (choice == INTTEXT) {
                url = response.encodeRedirectURL(servletDir + "paste.jsp");
            } else {
                url = response.encodeRedirectURL(servletDir + "form.jsp");
            }
        } else {
            System.out.println("Retry Servlet ");
            servletDir = getServletDir();
            url = response.encodeRedirectURL(servletDir + "form.jsp");
        }
        response.sendRedirect(url);
    }

    /**
     * Called by the Tapenade web server.
     * This method is redefined because, to transmit the data, we use the POST method.
     *
     * @param request  an HttpServletRequest object that contains the request the client has made of the servlet
     * @param response an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws IOException if an input or output error is detected when the servlet handles the request
     */
    @Override
    public final void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        doGet(request, response);
    }
}
