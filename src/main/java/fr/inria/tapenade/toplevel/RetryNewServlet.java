/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.toplevel;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This servlet is called when a user wants to retry the program
 * with new files, using the "Retry with new files" button.
 */
public final class RetryNewServlet extends ADServlet {

    private static final long serialVersionUID = 354099994054L;

    /**
     * Called by the Tapenade web server.
     *
     * @param req an HttpServletRequest object that contains the request the client has made of the servlet
     * @param res an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws IOException if an input or output error is detected when the servlet handles the request
     */
    @Override
    public final void doPost(HttpServletRequest req, HttpServletResponse res)
            throws IOException {
        doGet(req, res);
    }

    /**
     * Called by the Tapenade server.
     *
     * @param req an HttpServletRequest object that contains the request the client has made of the servlet
     * @param res an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws IOException if an input or output error is detected when the servlet handles the request
     */
    @Override
    public final void doGet(HttpServletRequest req, HttpServletResponse res)
            throws IOException {
        HttpSession session = req.getSession(false);
        String servletDir = getServletDir();
        if (session != null) {
            System.out.println("RetryNServlet "+session.getId());
            String tmpDirToClean = (String) session.getAttribute(
                    "tmpDirToClean");
            if (tmpDirToClean != null) {
                File dir = new File(tmpDirToClean);
                if (dir.exists()) {
                    cleanSubDirectory("tapenadeGenDir", session, tmpDirToClean);
                    cleanSubDirectory("tapenadeOutputDir", session, tmpDirToClean);
                    String[] files = dir.list();
                    for (String fileName : Objects.requireNonNull(files)) {
                        File file = new File(tmpDirToClean + "/" + fileName);
                        if (!file.delete()) {
                            System.out.println("RetryServlet, delete "+tmpDirToClean+"/"+fileName);
                        }
                    }
                }
            }
            session.setAttribute("inputFileNames", null);
            session.setAttribute("includeFileNames", null);
            session.setAttribute("size", 0L);
            session.setAttribute("inputLanguage", null);
        } else {
            System.out.println("RetryNServlet");
        }
        String url = res.encodeRedirectURL(servletDir + "index.jsp");
        res.sendRedirect(url);
    }

}
