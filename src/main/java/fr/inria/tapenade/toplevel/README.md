Tapenade toplevel and web server
================================

Tapenade toplevel
-----------------

* Files:  `Tapenade`  [Javadoc](../../../../../../../build/fr/inria/tapenade/toplevel/package-index.html)

The top-level class of Tapenade is `Tapenade`.
It schedules the operations at the higher level.
It provides one main scheduler for command-line invocation.
Other schedulers are provided, for the GUI or for the web servlet.
Typical chain of operations is (See
[Figure 1](../../../../../../../docs/README-figure5.md)),
 in order:

   -  Parse all the source files (**Step [-1](../frontend/README-parser.md) &rArr; [0](../representation/README-inputast.md)**)  
 
      &larr; uses the input files  
 
      &rarr; results into a sequence of AST operators (IL) 
      [(**State 0**)](../frontend/README-ast.md)
   -  Build the Internal Representation (IR) (**Step 0 &rArr; 2**)
   
      &larr;  uses the stream of AST operators  
      
      &rarr; results into a Call-Graph of Flow-Graphs 
      [(**State 2**)](../representation/README-zone.md)
   -  Run Data-Flow analyses [(**Step 2 &rArr; 4**)](../analysis/README.md) 
    
      &larr;  uses the Internal Representation  
      
      &rarr; results into Data-Flow info attached to the Call-Graph and 
      Flow-Graphs [(**State 4**)](../analysis/README.md)
   -  Differentiate [(**Step 4 &rArr; 5**)](../differentiation/README.md)  
   
      &larr;  uses the Internal Representation augmented with Data-Flow info  
      
      &rarr; results into another Call-Graph of Flow-Graphs
      [(**State 5**)](../differentiation/README-diffCallGraph.md)
   -  Regenerate AST of differentiated code 
   [(**Step 5 &rArr; 6**)](../ir2tree/README.md) 
    
      &larr;  uses the differentiated Call-Graph of Flow-Graphs  
      
      &rarr; results into AST, one per future file 
      [(**State 6**)](../ir2tree/README.md)
   -  Create differentiated files [(**Step 6 &rArr; End**)](../prettyprint/README.md)  
   
      &larr;  uses the differentiated AST's  
      
      &rarr; results in files

Tapenade web-server
-------------------

* Files: 
`ADServlet RetryServlet RetryNewServlet`

Tapenade web server
[www-tapenade.inria.fr](http://www-tapenade.inria.fr:8080/)
is implemented in class ADServlet. Details will be given [here](docServlet.md)

Tapenade is also available as a docker image. You can find the Tapenade
docker images in the 
[Tapenade gitlab repository](https://gitlab.inria.fr/tapenade/tapenade/container_registry/643).

To get the latest Tapenade image, run:
`docker pull registry.gitlab.inria.fr/tapenade/tapenade:latest`

Add the `-html` option to generate the HTML results:
`docker run --rm registry.gitlab.inria.fr/tapenade/tapenade -html ... <files>`
Use your web browser to display the HTML Results contained in .../tapenadehtml/tapenade.html.

See 
[https://gitlab.inria.fr/tapenade/tapenade/-/blob/master/bin/tapenadocker](tapenadocker shell script)
to run the latest Tapenade docker image.


