/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.toplevel;

import fr.inria.tapenade.representation.PositionAndMessage;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.servletutils.DataRecovery;
import fr.inria.tapenade.utils.Chrono;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This class is the main servlet of the Tapenade AD tool.
 * It calls all the needed classes to get the user's data,
 * to launch the differentiation and to print the result.
 */
public class ADServlet extends HttpServlet {

    private static final long serialVersionUID = 9876299994054L;

    /**
     * Called by the Tapenade server.
     *
     * @param req an HttpServletRequest object that contains the request the client has made of the servlet
     * @param res an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws IOException if an input or output error is detected when the servlet handles the request
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws IOException {
    }

    /**
     * Called by the Tapenade web server.
     * This method is redefined because, to transmit the data, we use the POST method.
     *
     * @param req an HttpServletRequest object that contains the request the client has made of the servlet
     * @param res an HttpServletResponse object that contains the response the servlet sends to the client
     * @throws IOException if an input or output error is detected when the servlet handles the request
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws IOException {
        long maxSIZE = 1000000L;
        final int INTFILE = 1;
        final int INTTEXT = 2;
        final int INTINCLUDE = 3;
        final int INTREMOVE = 4;
        final int INTDIFF = 5;

        String clientDirPath;
        int inputLanguage;
        String servletDir;
        DataRecovery dataRecovery;
        long inputSize;
        HttpSession session = req.getSession(true);
        session.setMaxInactiveInterval(7200);
        LocalDateTime date = LocalDateTime.now();
        session.setAttribute("time", date.toString());
        System.out.println("");
        Chrono totalTapenadeTimer = new Chrono();
        String remoteHost = req.getRemoteHost();
        InetAddress addr;
        addr = InetAddress.getByName(remoteHost);
        String hostName = addr.getHostName();
        System.out.println("START "+hostName+" "+session.getId()+" "+date);
        String webappRoot = getServletConfig().getServletContext()
                .getRealPath("");
        System.out.println("WEBAPP_ROOT = "+webappRoot);
        System.out.println("JAVA_HOME = "+System.getProperty("java.home"));
        servletDir = getServletDir();
        if (req.getCookies() == null) {
            String url = res.encodeRedirectURL(servletDir + "cookies.jsp");
            res.sendRedirect(url);
        } else {
            Long maxSize = (Long) session.getAttribute("maxsize");
            if (maxSize != null) {
                maxSIZE = maxSize;
            }
            if (session.getAttribute("size") != null) {
                inputSize = (Long) session.getAttribute("size");
            } else {
                inputSize = 0L;
            }
            session.setAttribute("ServletDir", servletDir);
            String uploadedDirName = "UploadedFiles";
            String aliParser = webappRoot + File.separator + "WEB-INF" + File.separator + "lib" + File.separator;
            String clientDirName = "client" + session.getId();
            clientDirPath = webappRoot + File.separator + uploadedDirName + File.separator
                    + clientDirName + File.separator;
            String tapenadeLogFile = webappRoot + File.separator + uploadedDirName
                    + File.separator + "tapenade_log_" + clientDirName;
            /*puts in the session the useful paths*/
            session.setAttribute("clientDirPath", clientDirPath);
            session.setAttribute("UploadedDirName", uploadedDirName);
            session.setAttribute("clientDirName", clientDirName);
            /*creates a specific directory for each client, based on the session id*/
            File clientDir = new File(clientDirPath);
            if (!clientDir.mkdir()) {
                System.out.println("DoPost: mkdir "+clientDirPath);
            }
            session.setAttribute("clientDir", clientDir);
            session.setAttribute("tmpDirToClean", clientDirPath);
            /*gets the informations sent by the client like the fortran source file*/
            dataRecovery = new DataRecovery(req.getContentType(), req
                    .getInputStream(), res.getWriter());
            int buttonType;
            Vector<String> names;
            String filename = "";
            File file;
            String url = res.encodeRedirectURL(servletDir + "form.jsp");
            int fileSize;
            Tapenade aliServlet = null;
            try {
                dataRecovery.getData();
                inputLanguage = dataRecovery.getFileType();
                System.out.println("inputLanguage "+inputLanguage);
                session.setAttribute("inputLanguage", inputLanguage);
                buttonType = dataRecovery.getButtonType();
                dataRecovery.appendStringTapenadeLog(tapenadeLogFile,
                        System.lineSeparator() + "---- Session: " + session.getId() + ", time: "
                                + date + System.lineSeparator());
                dataRecovery.appendStringTapenadeLog(tapenadeLogFile, "Remote Host : " + req.getRemoteHost()
                        + " : " + hostName + System.lineSeparator());
                dataRecovery.appendTapenadeLog(tapenadeLogFile);
                dataRecovery.appendStringTapenadeLog(tapenadeLogFile, System.lineSeparator());
                switch (buttonType) {
                    case INTFILE:
                        fileSize = dataRecovery.saveFile(clientDirPath);
                        filename = dataRecovery.getFileName();
                        file = new File(clientDirPath + filename);
                        if (filename != null && !filename.isEmpty()
                                && file.length() != 0L) {
                            String extension = TapEnv.getExtension(filename);
                            names = (Vector<String>) session.getAttribute("inputFileNames");
                            if (names == null) {
                                names = new Vector<>();
                            }
                            if ("jar".equals(extension) || "zip".equals(extension)) {
                                String[] jarNames = Tapenade.extractFiles(clientDirPath,
                                        clientDirPath + filename);
                                for (String jarName : jarNames) {
                                    if (!names.contains(jarName)) {
                                        names.add(jarName);
                                    }
                                }
                                addSize(session, inputSize, fileSize);
                            } else if (!names.contains(filename)) {
                                names.add(filename);
                                addSize(session, inputSize, fileSize);
                            }
                            session.setAttribute("inputFileNames", names);
                        }
                        break;
                    case INTINCLUDE:
                        fileSize = dataRecovery.saveFile(clientDirPath);
                        filename = dataRecovery.getFileName();
                        file = new File(clientDirPath + filename);
                        if (filename != null && !filename.isEmpty()
                                && file.length() != 0L) {
                            String extension = TapEnv.getExtension(filename);
                            names = (Vector<String>) session.getAttribute("includeFileNames");
                            if (names == null) {
                                names = new Vector<>();
                            }
                            if ("jar".equals(extension) || "zip".equals(extension)) {
                                String[] jarNames = Tapenade.extractFiles(clientDirPath,
                                        clientDirPath + filename);
                                for (String jarName : jarNames) {
                                    if (!names.contains(jarName)) {
                                        names.add(jarName);
                                    }
                                }
                                addSize(session, inputSize, fileSize);
                            } else if (!names.contains(filename)) {
                                names.add(filename);
                                addSize(session, inputSize, fileSize);
                            }
                            session.setAttribute("includeFileNames", names);
                        }
                        break;
                    case INTREMOVE:
                        Vector<String> files = dataRecovery.getFileToRemove();
                        names = (Vector<String>) session.getAttribute("inputFileNames");
                        while (files != null && !files.isEmpty()) {
                            file = new File(clientDirPath + files.firstElement());
                            inputSize = subSize(session, inputSize, file.length());
                            if (!file.delete()) {
                                System.out.println("DoPost: delete file "+clientDirPath);
                            }
                            if (names != null) {
                                names.remove(files.firstElement());
                            }
                            files.remove(files.firstElement());
                        }
                        session.setAttribute("inputFileNames", names);
                        Vector<String> includes = dataRecovery.getIncludeToRemove();
                        names = (Vector<String>) session.getAttribute("includeFileNames");
                        while (includes != null && !includes.isEmpty()) {
                            file = new File(clientDirPath
                                    + includes.firstElement());
                            inputSize = subSize(session, inputSize, file.length());
                            if (!file.delete()) {
                                System.out.println("DoPost: delete include file "+clientDirPath);
                            }
                            if (names != null) {
                                names.remove(includes.firstElement());
                            }
                            includes.remove(includes.firstElement());
                        }
                        session.setAttribute("includeFileNames", names);
                        break;
                    case INTDIFF:
                        session.setAttribute("diffMode", Integer.toString(dataRecovery.getDiffMode()));
                        Object[] ifilesobj;
                        String[] ifiles;
                        names = (Vector<String>) session.getAttribute("inputFileNames");
                        int choice = dataRecovery.getChoice();
                        session.setAttribute("choice", choice);
                        boolean multiDirMode = dataRecovery.getMultiDirMode();
                        session.setAttribute("multiDirMode", Boolean.toString(multiDirMode));
                        int nbFiles;
                        if (names != null && (nbFiles = names.size()) > 0) {
                            ifilesobj = names.toArray();
                            ifiles = new String[nbFiles];
                            for (int k = 0; k < nbFiles; k++) {
                                ifiles[k] = (String) ifilesobj[k];
                            }
                            filename = ifiles[0];
                        } else {
                            if (choice == INTTEXT) {
                                // si fortran pas si c
                                filename = "default.f90";
                                inputSize = addSize(session, inputSize, dataRecovery.saveFile(clientDirPath));
                            } else {
                                fileSize = dataRecovery.saveFile(clientDirPath);
                                filename = dataRecovery.getFileName();
                                file = new File(clientDirPath + filename);
                                if (filename != null
                                        && !filename.isEmpty()
                                        && file.length() != 0L) {
                                    inputSize = addSize(session, inputSize, fileSize);
                                    java.util.List<String> vfilename = new Vector<>();
                                    vfilename.add(filename);
                                    session.setAttribute("inputFileNames",
                                            vfilename);
                                }
                            }
                            ifiles = new String[1];
                            ifiles[0] = filename;
                        }
                        session.setAttribute("filename", filename);
                        System.out.println("inputSize "+inputSize);
                        if (inputSize > maxSIZE) {
                            System.out.println("Errorsize: inputSize "+inputSize);
                            dataRecovery.appendStringTapenadeLog(tapenadeLogFile, "Errorsize: inputSize "
                                    + inputSize + System.lineSeparator());
                            url = res.encodeRedirectURL(servletDir
                                    + "errorsize.jsp");
                        } else {
                            TapEnv.get().mustTangent = false;
                            TapEnv.get().mustAdjoint = false;
                            aliServlet = new Tapenade(
                                    inputLanguage,
                                    aliParser,
                                    dataRecovery.getDiffMode(),
                                    webappRoot,
                                    clientDirPath,
                                    uploadedDirName + File.separator + clientDirName + File.separator,
                                    ifiles, webappRoot + File.separator + "WEB-INF" +
                                    File.separator + "lib" + File.separator,
                                    dataRecovery.getHeadFunction(), dataRecovery
                                    .getIndependentInputVariables(), dataRecovery
                                    .getDependentOutputVariables(),
                                    multiDirMode);
                            aliServlet.htmlMessageFileName = filename + "msg.html";
                            File genDir = new File(clientDirPath + Tapenade.TAPENADE_GEN_HTML_DIR);
                            if (!genDir.mkdir()) {
                                System.out.println("DoPost: mkdir "+clientDirPath+Tapenade.TAPENADE_GEN_HTML_DIR);
                            }
                            emptyDir(genDir);
                            session.setAttribute("tapenadeGenDir",
                                    Tapenade.TAPENADE_GEN_HTML_DIR);
                            File outputDir = new File(clientDirPath
                                    + Tapenade.TAPENADE_SERVLET_OUTPUT_DIR);
                            if (!outputDir.mkdir()) {
                                System.out.println("DoPost: mkdir "+clientDirPath+Tapenade.TAPENADE_SERVLET_OUTPUT_DIR);
                            }
                            emptyDir(outputDir);
                            session.setAttribute("tapenadeOutputDir",
                                    Tapenade.TAPENADE_SERVLET_OUTPUT_DIR);
                            // runs the application and creates the various files
                            //  such as the differentiated source file
                            System.out.println("Current mustTangent "+ TapEnv.get().mustTangent);
                            System.out.println("Current mustAdjoint "+ TapEnv.get().mustAdjoint);
                            boolean hasGlobalDecls = aliServlet.analyzeFiles();
                            int lang;
                            if (aliServlet.diffRoots != null) {
                                lang = aliServlet.diffRoots.head.unit().language();
                            } else {
                                lang = TapEnv.outputLanguage();
                            }
                            session.setAttribute("topLanguage", lang);

                            System.out.println("Saving error messages in "+clientDirPath+Tapenade.TAPENADE_GEN_HTML_DIR+File.separator+"msg.html");
                            TapList<PositionAndMessage> msgs = TapEnv.getAllMessages(
                                    aliServlet.origCallGraph(), aliServlet.diffCallGraph(),
                                    aliServlet.getTransformedSourceUnits(aliServlet.diffCallGraph() == null
                                            ? null : aliServlet.diffCallGraph().backAssociations),
                                    true);

                            /*deux appels a saveAllMessages a cause des paths
                             relatifs differents dans les liens
                             */
                            TapEnv.saveAllMessagesAsHTML(msgs, clientDirPath
                                            + Tapenade.TAPENADE_GEN_HTML_DIR
                                            + File.separator
                                            + aliServlet.htmlMessageFileName,
                                    aliServlet.cssDir(), "", null, hasGlobalDecls,
                                    TapEnv.get().preprocessFileSuffix);
                            TapEnv.saveAllMessagesAsHTML(msgs, clientDirPath
                                            + Tapenade.TAPENADE_GEN_HTML_DIR
                                            + File.separator + "msg.html", aliServlet.cssDir(),
                                    uploadedDirName + File.separator
                                            + clientDirName + File.separator
                                            + Tapenade.TAPENADE_GEN_HTML_DIR
                                            + File.separator, null, hasGlobalDecls,
                                    TapEnv.get().preprocessFileSuffix);
                            TapEnv.saveAllMessagesAsText(msgs,
                                    clientDirPath + Tapenade.TAPENADE_SERVLET_OUTPUT_DIR +
                                            File.separator + aliServlet.textMessageFileName);

                            if (!TapEnv.modeIsNoDiff() && aliServlet.diffCallGraph() != null) {
                                String outputDirectory = clientDirPath + Tapenade.TAPENADE_SERVLET_OUTPUT_DIR
                                        + File.separator;
                                File directory = new File(outputDirectory);
                                String[] genFiles = directory.list();
                                String zipFileName = outputDirectory + "TapenadeResults.zip";
                                if (genFiles != null) {
                                    createZipFile(TapEnv.get().outputDirectory, zipFileName, genFiles);
                                }
                            }

                            /*redirects to the frame page*/
                            url = res.encodeRedirectURL(servletDir
                                    + "result.html");
                            dataRecovery.appendStringTapenadeLog(tapenadeLogFile, "------------ Differentiation done " +
                                    java.time.LocalDateTime.now() + System.lineSeparator());
                        }
                        break;
                    default:
                        break;
                }
                res.sendRedirect(url);
                System.out.println("");
                System.out.println("STOP  "+hostName+" "+session.getId()+" "+date+" total time "+totalTapenadeTimer.elapsed()+" s");
                System.out.println("");
            } catch (Exception e) {
                if (aliServlet != null) {
                    TapList<PositionAndMessage> msgs = TapEnv.getAllMessages(
                            aliServlet.origCallGraph(), aliServlet.diffCallGraph(),
                            aliServlet.getTransformedSourceUnits(aliServlet.diffCallGraph() == null
                                    ? null : aliServlet.diffCallGraph().backAssociations), true);
                    TapEnv.saveAllMessagesAsText(msgs,
                            clientDirPath + Tapenade.TAPENADE_GEN_HTML_DIR
                                    + File.separator + aliServlet.textMessageFileName);
                }
                System.out.println("");
                System.out.println("---> Session num "+session.getId()+", heure: "+date);
                System.out.println("");
                System.out.println("ERREUR : "+e.getMessage());
                dataRecovery.appendStringTapenadeLog(tapenadeLogFile, "ERROR "
                        + e.getMessage() + System.lineSeparator());
                System.out.println(" Les valeurs utilisateurs sont :");
                System.out.println(" - Nom fichier : "+filename);
                System.out.println(" - Head : "+dataRecovery.getHeadFunction());
                String variables = dataRecovery.getIndependentInputVariables();
                System.out.println(" - Nom des independent input variables: "+variables);
                System.out.println("");
                variables = dataRecovery.getDependentOutputVariables();
                System.out.println(" - Nom des dependent output variables: "+variables);
                System.out.println("");
                System.out.println(" - Mode de differentiation : "+dataRecovery.getDiffMode());
                System.out.println("End   session "+session.getId()+" "+date+" total time: "+totalTapenadeTimer.elapsed()+" s");
                System.out.println("");
                try (PrintWriter errorPW = new PrintWriter(java.nio.file.Files.newOutputStream(
                        java.nio.file.Paths.get(clientDirPath + "error.html")))) {
                    errorPW.println(e);
                    errorPW.flush();
                } catch (IOException ignored) {

                }
                try (PrintWriter stackPW = new PrintWriter(java.nio.file.Files.newOutputStream(
                        java.nio.file.Paths.get(clientDirPath + "stack.html")))) {
                    stackPW.println("<pre>");
                    e.printStackTrace(stackPW);
                    stackPW.println("</pre>");
                    stackPW.flush();
                } catch (IOException ignored) {

                }
                if (aliServlet != null) {
                    TapEnv.closeOutputStream();
                }
                url = res.encodeRedirectURL(servletDir + "error.jsp");
                res.sendRedirect(url);
            }
        }
    }

    /**
     * @return the servlet working directory.
     */
    protected final String getServletDir() {
        File webDir = new File(getServletConfig().getServletContext()
                .getRealPath(""));
        return File.separator + webDir.getName() + File.separator;
    }

    private long addSize(HttpSession session, long inputSize, long size) {
        long result = inputSize + size;
        session.setAttribute("size", result);
        return result;
    }

    private long subSize(HttpSession session, long inputSize, long size) {
        long result = inputSize - size;
        session.setAttribute("size", result);
        return result;
    }

    private void emptyDir(File genDir) {
        File[] files = genDir.listFiles();
        for (File file : Objects.requireNonNull(files)) {
            if (!file.delete()) {
                System.out.println("EmptyDir: delete "+file);
            }
        }
    }

    private void createZipFile(String outputDirectory,
                               final String zipFileName, final String[] files) {
        final byte[] buf = new byte[1024];
        if (files.length > 0) {
            try (final ZipOutputStream out = new ZipOutputStream(java.nio.file.Files.newOutputStream(
                    java.nio.file.Paths.get(zipFileName)))) {
                for (final String file : files) {
                    try (final java.io.InputStream in = java.nio.file.Files.newInputStream(
                            java.nio.file.Paths.get(outputDirectory + file))) {
                        out.putNextEntry(new ZipEntry(file));
                        int len;
                        while ((len = in.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }
                        out.closeEntry();
                    } catch (final IOException ignored) {

                    }
                }
            } catch (final IOException ignored) {

            }
            System.out.println("Creating "+zipFileName);
        }
    }

    /**
     * Clean subDirectory, to avoid display of old results.
     *
     * @param subDirectory  tapenadeGenDir or tapenadeOutputDir
     * @param session       HttpSession
     * @param tmpDirToClean directory to clean
     */
    protected void cleanSubDirectory(String subDirectory, HttpSession session, String tmpDirToClean) {
        File dir;
        String[] files;
        String diffgenpath = tmpDirToClean + File.separator +
                session.getAttribute(subDirectory);
        dir = new File(diffgenpath);
        if (dir.exists()) {
            files = dir.list();
            for (int i = 0; i < Objects.requireNonNull(files).length; i++) {
                File f = new File(diffgenpath + File.separator + files[i]);
                if (!f.delete()) {
                    System.out.println("Clean directory, delete: "+files[i]);
                }
            }
        }
    }
}
