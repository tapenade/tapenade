/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 */

package fr.inria.tapenade.toplevel;

import fr.inria.tapenade.analysis.ReachAnalyzer;
import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ADTBRAnalyzer;
import fr.inria.tapenade.analysis.MultithreadAnalyzer;
import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.analysis.DepsAnalyzer;
import fr.inria.tapenade.analysis.DiffLivenessAnalyzer;
import fr.inria.tapenade.analysis.ReqExplicit;
import fr.inria.tapenade.differentiation.CallGraphDifferentiator;
import fr.inria.tapenade.frontend.GeneralLibReader;
import fr.inria.tapenade.frontend.Parser;
import fr.inria.tapenade.frontend.TreeProtocol;
import fr.inria.tapenade.gui.TapenadeFrame;
import fr.inria.tapenade.ir2tree.TreeGen;
import fr.inria.tapenade.prettyprint.CDecompiler;
import fr.inria.tapenade.prettyprint.CallGraphDisplayer;
import fr.inria.tapenade.prettyprint.Decompiler;
import fr.inria.tapenade.prettyprint.DotDecompiler;
import fr.inria.tapenade.prettyprint.FortranDecompiler;
import fr.inria.tapenade.prettyprint.JuliaDecompiler;
import fr.inria.tapenade.prettyprint.HtmlPrinter;
import fr.inria.tapenade.prettyprint.TextPrinter;
import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.CStuff;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.DiffPattern;
import fr.inria.tapenade.representation.DiffRoot;
import fr.inria.tapenade.representation.Directive;
import fr.inria.tapenade.representation.FlowGraphBuilder;
import fr.inria.tapenade.representation.FortranStuff;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.MPIcallInfo;
import fr.inria.tapenade.representation.MixedLanguageInfos;
import fr.inria.tapenade.representation.PositionAndMessage;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.utils.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Top class for Tapenade. Contains the "main" method.
 */
public final class Tapenade {

    protected static final String TAPENADE_SERVLET_OUTPUT_DIR = "tapenadeoutput";
    protected static final String TAPENADE_GEN_HTML_DIR = "tapenadehtml";
    private static final int FILE = 1;
    private static final int IL = 2;
    private static final int HTML = 3;
    private static final int VARIABLE = 0;
    private static final int PROCEDURE = 1;
    private static final int MODULE = 2;
    private static final int TYPE = 3;
    private static final int ORIGCOPY = 0;
    private static final int DIFFERENTIATED = 1;
    private static final int TANGENT = 1;
    private static final int ADJOINT = 2;
    private static final int ADJOINT_FWD = 3;
    private static final int ADJOINT_BWD = 4;
    private static final String DEFAULT_COPIED_UNIT_SUFFIX = "_nodiff";
    private static final String DEFAULT_MODULE_UNIT_SUFFIX = "_diff";
    private static final String DEFAULT_ADOLC_FUNC_SUFFIX = "_a";
    private static final String DEFAULT_ADOLC_VAR_SUFFIX = "a";
    private static final String DEFAULT_ASSOC_BY_ADDRESS_FUNC_SUFFIX = "aa";
    private static final int BEGINNING = 0;
    private static final int INIDENT = 1;
    private static final int INCASN = 2;
    private static final int INVARS = 3;
    private static final int ENDING = 10;
    private static final int NOTHING = 0;
    private static final int FUNC = 1;
    private static final int CASN = 2;
    private static final int VARS = 3;
    private static final int ONTK = 4;
    private static final int TOTK = 5;
    private static final int BEFORE_FUNC = 0;
    private static final int AFTER_FUNC = 1;
    private static final int AFTER_FUNC_CASN = 2;
    private static final int AFTER_ONTK = 3;
    private static final int AFTER_TOTK = 4;
    private static final int AFTER_VARS = 5;
    /**
     * The Java Runtime Environment directory.
     */
    private static final String JAVA_HOME = System.getProperty("java.home");
    /**
     * Operating system name.
     */
    private static final String OS_NAME = System.getProperty("os.name").toLowerCase(java.util.Locale.ENGLISH);

    /**
     * Linux OS.
     */
    private static final String LINUX = "linux";
    /**
     * Mac OS.
     */
    private static final String MAC = "mac";
    /**
     * Windows OS.
     */
    private static final String WINDOWS = "windows";

    /**
     * The path to our language parsers.
     */
    private static String pathToParsers;
    /**
     * The root directory of the servlet.
     */
    private static String tapenadeServletRoot;
    /**
     * The current parser directory (depending on operating system).
     */
    private static String parserDirectory;

    private final String directoryHtmlGen = Tapenade.TAPENADE_GEN_HTML_DIR + File.separator;

    /**
     * Default values of all suffixes used by differentiation.
     */
    private final String[][] suffixes =
            //       VAR , PROC   , MOD , TYPE
            {
                    {"", "_nodiff", "", ""},       //ORIGCOPY
                    {"d", "_d", DEFAULT_MODULE_UNIT_SUFFIX, DEFAULT_MODULE_UNIT_SUFFIX}, //TANGENT or DIFFERENTIATED
                    {"b", "_b", "", DEFAULT_MODULE_UNIT_SUFFIX},      //ADJOINT
                    {"b", "_fwd", "", DEFAULT_MODULE_UNIT_SUFFIX},    //ADJOINT_FWD
                    {"b", "_bwd", "", DEFAULT_MODULE_UNIT_SUFFIX}};   //ADJOINT_BWD

    protected String htmlMessageFileName = "msg.html";
    protected String textMessageFileName = "msg.txt";
    protected String dumpFileName = null ;
    /**
     * Html css directory for html output.
     */
    private String cssDir = "";
    /**
     * Hatted TapList of DiffRoot objects.
     * The hat itself is used as a pointer to the current
     * "diffRoot" of interest, so that isolated "-vars" or "-outvars"
     * arguments will affect this current "diffRoot" defined by a previous
     * "-root" option.
     */
    protected TapList<DiffRoot> diffRoots;
    private CallGraph diffCallGraph;
    /**
     * Html file used to display differentiated programs.
     * With -display option:
     * "tapenade.html" or "tapenadediff.html".
     */
    private String tapenadeHtmlDisplay = "tapenade.html";
    /**
     * "Kind" of values that can be differentiated.
     * Usually, this must be REALKIND.
     * Exceptionnally, when the code does dirty tricks such as
     * equivalencing REAL's and INTEGER's, one may set
     * diffKind to ALLKIND. But this will SLOW differentiation and
     * generate HEAVIER code!!!
     */
    private int diffKind = SymbolTableConstants.REALKIND;
    private String absDirectoryHtmlGen = TapEnv.get().outputDirectory + directoryHtmlGen;
    private String outputHtmlDirectory;
    private final TapList<String> hdIncludeDirs = new TapList<>(null, null);
    private TapList<String> tlIncludeDirs = hdIncludeDirs;
    private String inputFileName;
    private String[] inputFileNames;
    private TapList<TapPair<String, Integer>> inputFiles;
    private int defaultInputFormat = TapEnv.UNDEFINED;
    private TapIntList outputFormats = new TapIntList(Tapenade.FILE, null) ;
    private int defaultInputLanguage = TapEnv.UNDEFINED;
    private boolean parseOpenMP;
    private boolean nolib;
    private String stdGeneralLibraryDirectory;
    private String libraryFileNames = "";
    private boolean fortranLibraryLoaded;
    private OutputStream dumpOutputStream;
    private DiffPattern unfinishedInVars;
    private DiffPattern unfinishedOutVars;
    private TapList<TapList<String>> waitingInVars;
    private TapList<TapList<String>> waitingOutVars;
    private boolean debug;
    private boolean showMessagesInFile;
    private boolean viewCallGraph;
    /**
     * End-user given full name (possibly with path) of a unique differentiated file.
     */
    private String outputFileFullName = null ;
    /**
     * End-user given root (without path nor _d, _b, etc) of a unique differentiated file.
     */
    private String outputFileName = null ;
    /**
     * When true, generate one output file per top-level Unit.
     */
    private boolean splitOutputFiles = false ;
    private PrintWriter fileILNames;
    private TapList<String> splittedUnitNames;
    private TapList<String> jointedUnitNames;
    private TapList<String> specializedActivityUnitNames;

    private Tapenade() {
        tapenadeInit();
        TapEnv.setParserFileSeparator(File.separator);
        TapEnv.setMixedLanguageInfos(MixedLanguageInfos.defaultMixedLangInfos());
        diffRoots = null;
    }

    private Tapenade(final String[] args) {
        synchronized (this) {
            runTapenade(args);
        }
    }

    /**
     * Creation of one "Tapenade" for the servlet.
     */
    protected Tapenade(final int fileLang, final String aliParser, final int diffmode,
                       final String webAppRoot, final String absoluteDirectory,
                       final String clientDirectory, final String[] inputfilenames,
                       final String libDirectory, final String headFunction,
                       final String activevariables, final String usefulvariables,
                       final boolean multiDirDiffMode) {
        tapenadeInit();
        TapEnv.setMixedLanguageInfos(MixedLanguageInfos.defaultMixedLangInfos());
        diffRoots = null;
        TapEnv.setMsgLevel(TapEnv.MSG_TRACE);
        TapEnv.setTraceLevel(25);
        TapEnv.printlnOnTrace(Version.HEADER + " - Java " + System.getProperty("java.version"));
        TapEnv.printlnOnTrace("Revision: " + Version.GIT_LONG_HASH);
        if (!"".equals(Version.GIT_TAG)) TapEnv.printlnOnTrace("Tag:      " + Version.GIT_TAG);
        if (diffmode == 1) {
            TapEnv.setModeTangent();
            TapEnv.setRemoveDeadPrimal(false);
            TapEnv.setRemoveDeadControl(false);
        } else if (diffmode == -1) {
            TapEnv.setModeAdjoint();
        } else if (diffmode == 2) {
            TapEnv.setModeOverloading();
        } else {
            TapEnv.setModeNoDiff();
        }

        if (TapEnv.modeIsTangent()) {
            TapEnv.setMustTangent(true);
        }
        if (TapEnv.modeIsAdjoint()) {
            TapEnv.setMustAdjoint(true);
        }
        TapEnv.get().multiDirDiffMode = multiDirDiffMode;
        pathToParsers = aliParser;
        TapEnv.setIsServlet();
        tapenadeServletRoot = webAppRoot;
        TapEnv.setParserFileSeparator(File.separator);
        TapEnv.get().outputDirectory = absoluteDirectory + Tapenade.TAPENADE_SERVLET_OUTPUT_DIR + File.separator;
        absDirectoryHtmlGen = absoluteDirectory + directoryHtmlGen;
        outputHtmlDirectory = clientDirectory + directoryHtmlGen;
        cssDir = "../../../";
        defaultInputLanguage = fileLang;
        TapEnv.setInputLanguage(defaultInputLanguage);
        if (inputfilenames == null) {
            inputFileName = absoluteDirectory + "default.f90";
            inputFileNames = new String[1];
            inputFileNames[0] = inputFileName;
        } else {
            inputFileNames = new String[inputfilenames.length];
            for (int i = 0; i < inputfilenames.length; i++) {
                inputFileNames[i] = absoluteDirectory + inputfilenames[i];
            }
            inputFileName = absoluteDirectory + inputfilenames[0];
        }
        stdGeneralLibraryDirectory = libDirectory;
        TapEnv.get().stdLibraryDirectory = libDirectory;
        parseDiffRoots(headFunction);
        parseDiffVars(activevariables);
        parseDiffOutvars(usefulvariables);
        outputFormats = TapIntList.add(outputFormats, Tapenade.HTML);

        prepareAllSuffixes("_d", "d", "_b", "b",
                DEFAULT_MODULE_UNIT_SUFFIX, DEFAULT_MODULE_UNIT_SUFFIX, DEFAULT_MODULE_UNIT_SUFFIX,
                "_c", null);
    }

    /**
     * Test with a thread for each differentiation mode.
     * *
     *
     * @param args       Strings "-thread" + file name
     * @param modeString String "-p" "-d" "-b"
     * @param multiMode  boolean true if multiMode
     */
    private Tapenade(final String[] args, final String modeString, final boolean multiMode) {
        synchronized (this) {
            final String[] newArgs = new String[args.length + 3];
            newArgs[0] = modeString;
            System.arraycopy(args, 0, newArgs, 1, args.length);
            // if (multiMode) newArgs[args.length + 1] = "-multi";
            newArgs[newArgs.length - 2] = "-o";
            int random = (int) (Math.random() * 50 + 1);
            newArgs[newArgs.length - 1] = "result" + random;
            TapEnv.printlnOnTrace("RunTapenade " + modeString + " " + multiMode + " output " + newArgs[newArgs.length - 1]);
            runTapenade(newArgs);
        }
    }

    /**
     * Called by bin/tapenade shell script.
     *
     * @param args provided by user.
     */
    public static void main(final String[] args) {
      try {
        TapEnv.printlnOnTrace(Version.HEADER + " - Java "
                + System.getProperty("java.version") + " "
                + System.getProperty("os.name"));

        if (!TapEnv.isDistribVersion()) {
            TapEnv.printlnOnTrace("@@ TAPENADE_HOME=" + System.getProperty("tapenade_home"));
        }

        if (args.length > 0) {
            if (!TapEnv.isDistribVersion() && args[0].equals("-thread")) {
                // pour debugger les threads:
                TapEnv.printlnOnTrace("Test // PreProcess, Direct, DirectV, Reverse");
                new Thread() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            new Tapenade(args, "-p", false);
                        }
                    }
                }.start();
                new Thread() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            new Tapenade(args, "-d", false);
                        }
                    }
                }.start();
                new Thread() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            new Tapenade(args, "-d", true);
                        }
                    }
                }.start();
                new Thread() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            new Tapenade(args, "-b", false);
                        }
                    }
                }.start();
            } else {
                new Tapenade(args);
            }
        } else {
            Tapenade.gui();
        }
      } catch (Throwable ex) {
        System.err.println("Uncaught exception - " + ex.getMessage());
        ex.printStackTrace(System.err);
      }
    }

    /**
     * Initialize static fields TapEnv.isDistribVersion, Tapenade.parserDirectory,
     * pathToParsers.
     */
    private static void tapenadeInit() {
        final String tapenadeHome = System.getProperty("tapenade_home");
        if (tapenadeHome == null) {
            TapEnv.setDistribVersion(false);
        } else {
            final File path = new File(tapenadeHome
                    + File.separator + "src"
                    + File.separator + "main"
                    + File.separator + "java"
                    + File.separator + "fr" + File.separator);
            TapEnv.setDistribVersion(Tapenade.OS_NAME.startsWith(Tapenade.WINDOWS) || !path.exists());
        }
        if (Tapenade.OS_NAME.startsWith(Tapenade.MAC)) {
            parserDirectory = Tapenade.MAC;
        } else if (Tapenade.OS_NAME.startsWith(Tapenade.WINDOWS)) {
            parserDirectory = Tapenade.WINDOWS;
        } else {
            parserDirectory = Tapenade.OS_NAME;
        }
        if (TapEnv.tapenadeHome() == null) {
            // ./gradlew :run, shell variable TAPENADE_HOME undefined:
            TapEnv.setTapenadeHome(System.getProperty("user.dir"));
        }
        System.setProperty("file.encoding", "ISO8859_1");
        if (!TapEnv.isServlet()) {
            File libraryPath = new File(TapEnv.tapenadeHome() + File.separator + "bin");
            if (!libraryPath.exists()) {
                TapEnv.toolError("Parser not found: " + TapEnv.tapenadeHome() + File.separator + "bin");
            }
            pathToParsers = libraryPath.getPath() + File.separator;
        }
    }

    /**
     * When differentiating, holds the CallGraph of the original, non differentiated code.
     */
    protected CallGraph origCallGraph() {
        return TapEnv.get().origCallGraph();
    }

    protected CallGraph diffCallGraph() {
        return diffCallGraph;
    }

    private void setDiffCallGraph(CallGraph diffCallGraph) {
        this.diffCallGraph = diffCallGraph;
    }

    private static int getFortranDialect(final int type, int result) {
        if (type == TapEnv.FORTRAN
                && TapEnv.isFortran9x(result)) {
            result = TapEnv.FORTRAN90;
        } else if (TapEnv.isFortran(result)
                && TapEnv.isFortran9x(type)) {
            result = TapEnv.FORTRAN90;
        }
        return result;
    }

    /**
     * Parses all the source files into the given CallGraph.
     *
     * @param addDirectory    if true. localInputFileNames are in directory.
     * @param directory       directory.
     * @param inputFiles      TapList of pairs of full path of input files and their inputlanguages.
     * @param origCallGraph   current callGraph.
     * @param includeDirs     include directories.
     * @param parseOpenMP     OpenMP option for fortran parser.
     * @throws IOException if an input error is detected.
     */
    public static void parseFiles(final boolean addDirectory,
                                  final String directory, final TapList<TapPair<String, Integer>> inputFiles,
                                  final CallGraph origCallGraph,
                                  final TapList<String> includeDirs,
                                  final int defaultFormat, final boolean parseOpenMP)
            throws IOException {
        int position = 0;
        TapList<String> cPlusPlusFileNames = new TapList<>(null, null);
        TapList<String> cPlusPlusFileNamesTail = cPlusPlusFileNames;
        for (TapPair<String, Integer> filePair : inputFiles) {
            String fileName = filePair.first;
            int fileLanguage = filePair.second;

            final int index = fileName.lastIndexOf(File.separator);
            String currentDirectory = "";
            if (index > 0) {
                currentDirectory = fileName.substring(0, index + 1);
            }
            TapEnv.setCurrentDirectory(currentDirectory);
            if (addDirectory) fileName = directory+fileName ; // JLB: Doesn't fileName contain the path to input dir?
            int fileFormat = getFormatOfFile(fileName) ;
            if (fileFormat==-1) fileFormat = defaultFormat ; // JLB: Shouldn't we check for defaultInputFormat instead of hardcoded -1
            if (fileFormat==-1) fileFormat = Tapenade.FILE ;
            fileLanguage =  fileLanguage == TapEnv.UNDEFINED ? getLanguageOfFile(fileName) : fileLanguage;
            
            TapEnv.setRelatedLanguage(fileLanguage);
            if (fileFormat==Tapenade.IL) {
                TapEnv.printlnOnTrace(10, "@@ Reading from IL file " + fileName);
                final BufferedReader reader = new BufferedReader(new FileReader(fileName));
                String fileNameWithoutIL = fileName ;
                if ("il".equals(TapEnv.getExtension(fileNameWithoutIL)))
                    fileNameWithoutIL = fileNameWithoutIL.substring(0, fileNameWithoutIL.length()-3) ;
                // If IL file contains the original file names, use those as the fileName's:
                reader.mark(299);
                String line = reader.readLine();
                while (line != null) {
                    String givenInputFileName = fileNameWithoutIL ;
                    int givenInputFileLang = fileLanguage ;
                    if (line.startsWith("From file ")) {
                        givenInputFileName = line.substring(10);
                        TapEnv.printlnOnTrace(10, "@@  ...actually representing file " + givenInputFileName);
                        int givenLang = getLanguageOfFile(givenInputFileName);
                        if (givenLang!=TapEnv.UNDEFINED) {
                            givenInputFileLang = givenLang ;
                        }
                    } else {
                        reader.reset();
                    }
                    if (givenInputFileLang == TapEnv.UNDEFINED) {
                        // If we still haven't managed to figure out the file language, we'll just assume it's the same as the output's
                        givenInputFileLang = TapEnv.outputLanguage();
                    }
                    position = readILstream(reader, origCallGraph, position, givenInputFileName, givenInputFileLang);
                    reader.mark(299);
                    line = reader.readLine();
                    // If everything went ok, reader is either empty or next line is a "From file"
                    // => in case something went wrong, advance until reaching the same condition as above:
                    while (line != null && !line.startsWith("From file ")) {
                        reader.mark(299);
                        line = reader.readLine();
                    }
                }
            } else if (TapEnv.isFortran(fileLanguage)) {
                boolean ppRequired = false ;
                String ppTempFile = null ;
                String[] ppCommands = null ;
                // If a preprocessing pass was specified explicitly:
                if (TapEnv.get().fppCommand!=null && !"#none#".equals(TapEnv.get().fppCommand)) {
                    ppRequired = true ;
                    if (!Tapenade.commandFound(TapEnv.get().fppCommand)) {
                        TapEnv.commandWarning(TapEnv.MSG_ALWAYS, TapEnv.get().fppCommand + " not found ");
                        ppRequired = false ;
                    }
                }
                TapList<String> inIncludeDirs ;
                int ii ;
                if (ppRequired) {
                    ppTempFile = TapEnv.stripPath(fileName) ;
                    if (!("."+File.separator).equals(TapEnv.get().outputDirectory)) {
                        ppTempFile = TapEnv.get().outputDirectory+ppTempFile ;
                    }
                    ppTempFile = ppTempFile+"_preprocessed"+TapEnv.outputExtension(fileLanguage) ;
                    String[] ppOptions = null;
                    if (TapEnv.get().fppOptions != null) {
                        ppOptions = Tapenade.splitTokens(TapEnv.get().fppOptions);
                    }
                    int ppLength = 3  // minimal preprocess command length: "fpp" infile + outfile
                        + (false/*true if wanting option -P */ ? 1 : 0)
                        + (ppOptions==null ? 0 : ppOptions.length)
                        + TapList.length(includeDirs) ; //fpp wants no space after -I
                    ppCommands = new String[ppLength];
                    ii = 0 ;
                    ppCommands[ii++] = TapEnv.get().fppCommand;
                    if (false/*true if wanting option -P */) ppCommands[ii++] = "-P" ;
                    if (ppOptions != null) {
                        for (int i=0 ; i<ppOptions.length ; ++i)
                            ppCommands[ii++] = ppOptions[i] ;
                    }
                    inIncludeDirs = includeDirs ;
                    while (inIncludeDirs!=null) {
                        ppCommands[ii++] = "-I"+inIncludeDirs.head ; //fpp wants no space after -I
                        inIncludeDirs = inIncludeDirs.tail ;
                    }
                    ppCommands[ii++] = fileName ;
                    ppCommands[ii++] = ppTempFile ;
                }
                int length = 8 // minimal command length: "fortranParser -fileseparator sep -linesize ls ... "
                    + (parseOpenMP ? 1 : 0)
                    + (fileLanguage!=TapEnv.FORTRAN ? 1 : 0)
                    + 2*TapList.length(includeDirs);
                final String[] commands = new String[length];
                ii = 0 ;
                commands[ii++] = pathToParsers +
                    (TapEnv.isServlet() || OS_NAME.startsWith(Tapenade.WINDOWS)
                     ? parserDirectory + File.separator + "fortranParser"
                     : "fortranParserRun.sh") ;
                commands[ii++] = "-fileseparator";
                commands[ii++] = TapEnv.parserFileSeparator();
                commands[ii++] = "-linesize";
                commands[ii++] = TapEnv.get().fortran90LineLength;
                inIncludeDirs = includeDirs ;
                while (inIncludeDirs != null) {
                    commands[ii++] = "-includedir";
                    commands[ii++] = inIncludeDirs.head;
                    inIncludeDirs = inIncludeDirs.tail;
                }
                commands[ii++] = "-includetype";
                commands[ii++] = (TapEnv.isServlet() ? "servlet" : "tapenade") ;
                if (parseOpenMP) {
                    commands[ii++] = "-openmp";
                }
                if (fileLanguage != TapEnv.FORTRAN) {
                    commands[ii++] = "-freeformat";
                }
                commands[ii++] = (ppRequired ? ppTempFile : fileName) ;
                final BufferedReader reader = Parser.parse(ppCommands, commands, fileName);
                position = readILstream(reader, origCallGraph, position, fileName, fileLanguage);
                if (ppRequired && !TapEnv.get().traceParser && ppTempFile!=null) {
                    if (!new File(ppTempFile).delete()) {
                        TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "Cannot remove " + ppTempFile);
                    }
                }
            } else if (fileLanguage == TapEnv.CPLUSPLUS) {
                cPlusPlusFileNamesTail = cPlusPlusFileNamesTail.placdl(fileName);
            } else if (fileLanguage == TapEnv.C || fileLanguage == TapEnv.CUDA) {
                // Difference from Fortran: by default, we want to call a C preprocessor:
                if (TapEnv.get().cppCommand == null) {
                    if (Tapenade.MAC.equals(parserDirectory) && Tapenade.commandFound(TapEnv.MAC_CPP_COMMAND)) {
                        TapEnv.get().cppCommand = TapEnv.MAC_CPP_COMMAND;
                    } else {
                        TapEnv.get().cppCommand = TapEnv.STD_CPP_COMMAND;
                    }
                }
                boolean ppRequired = false ;
                String ppTempFile = null ;
                String[] ppCommands = null ;
                if (TapEnv.get().cppCommand!=null && !"#none#".equals(TapEnv.get().cppCommand)) {
                    ppRequired = true ;
                    if (!Tapenade.commandFound(TapEnv.get().cppCommand)) {
                        TapEnv.commandWarning(TapEnv.MSG_ALWAYS, TapEnv.get().cppCommand + " not found ");
                        ppRequired = false ;
                    }
                }
                TapList<String> inIncludeDirs ;
                int ii ;
                if (ppRequired) {
                    ppTempFile = TapEnv.stripPath(fileName) ;
                    if (!("."+File.separator).equals(TapEnv.get().outputDirectory)) {
                        ppTempFile = TapEnv.get().outputDirectory+ppTempFile ;
                    }
                    ppTempFile = ppTempFile+"_preprocessed"+TapEnv.outputExtension(fileLanguage) ;
                    String[] ppOptions = null;
                    if (TapEnv.get().cppOptions != null) {
                        ppOptions = Tapenade.splitTokens(TapEnv.get().cppOptions);
                    }
                    int ppLength = 5  // minimal preprocess command length: "cpp" + 2 options + infile + outfile
                        + (TapEnv.get().noComment ? 0 : 1)
                        + (ppOptions==null ? 0 : ppOptions.length)
                        + TapList.length(includeDirs) ; //cpp also works with no space after -I
                    ppCommands = new String[ppLength];
                    ii = 0 ;
                    ppCommands[ii++] = TapEnv.get().cppCommand;
                    ppCommands[ii++] = "-std=c99";
                    // for not having /usr/include/stdc-predef.h with gcc 4.8:
                    ppCommands[ii++] = "-ffreestanding";
                    if (!TapEnv.get().noComment) {
                        ppCommands[ii++] = "-C";
                    }
                    if (ppOptions != null) {
                        for (int i=0 ; i<ppOptions.length ; ++i)
                            ppCommands[ii++] = ppOptions[i] ;
                    }
                    inIncludeDirs = includeDirs ;
                    while (inIncludeDirs!=null) {
                        ppCommands[ii++] = "-I"+inIncludeDirs.head ; //cpp also works with no space after -I
                        inIncludeDirs = inIncludeDirs.tail ;
                    }
                    ppCommands[ii++] = fileName;
                    ppCommands[ii++] = ppTempFile ;
                }
                final String javaCommand = JAVA_HOME + File.separator + "bin" + File.separator + "java";
                final String jarName = (TapEnv.isServlet()
                                        ? (tapenadeServletRoot + File.separator
                                           + "WEB-INF" + File.separator + "lib"
                                           + File.separator + "frontc.jar")
                                        : (TapEnv.tapenadeHome() + File.separator + "build"
                                           + File.separator + "libs" + File.separator + "frontc.jar")) ;
                final String[] commands = {
                        javaCommand,
                        "-classpath",
                        jarName,
                        "fr/inria/frontc/C2IL",
                        (ppRequired ? ppTempFile : fileName)};
                final BufferedReader reader = Parser.parse(ppCommands, commands, fileName);
                position = readILstream(reader, origCallGraph, position, fileName, fileLanguage);
                if (ppRequired && !TapEnv.get().traceParser && ppTempFile!=null) {
                    if (!new File(ppTempFile).delete()) {
                        TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "Cannot remove " + ppTempFile);
                    }
                }
            } else if (fileLanguage == TapEnv.JULIA){
                TapList<String> inIncludeDirs ;
                String[] ppCommands = null ;
                int ii ;
                int length = 4; // julia juliaParser.jl inputfile outputfile 
                final String[] commands = new String[length];
                ii = 0 ;
                commands[ii++] = "julia";
                commands[ii++] = TapEnv.tapenadeHome() + File.separator + "src" + File.separator + "frontjulia" + File.separator + 
                    (TapEnv.isServlet() || OS_NAME.startsWith(Tapenade.WINDOWS)
                     ? parserDirectory + File.separator + "juliaParser.jl"
                     : "juliaParser.jl") ;
                commands[ii++] = fileName ;
                commands[ii++] = "/dev/null"; // TODO: Remove this once output file from parser removed
                final BufferedReader reader = Parser.parse(ppCommands, commands, fileName);
                position = readILstream(reader, origCallGraph, position, fileName, fileLanguage);
            } else if (fileLanguage == TapEnv.JAR) {
                Tapenade.parseFiles(true, directory,
                        Tapenade.generateListInputFiles(Tapenade.extractFiles(directory, fileName), null),
                        // Tapenade.extractFiles(directory, fileName),
                        origCallGraph, includeDirs, defaultFormat, parseOpenMP);
            } else if (fileLanguage == TapEnv.INCLUDE) {
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, fileName + ", include skipped in command line");
            } else {
                final String noParserFileName = TapEnv.isServlet() ? TapEnv.stripPath(fileName) : fileName;
                TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "No parser for " + noParserFileName + ". File skipped");
            }
        }
        // C++ files must be passed to clang all together:
        if (cPlusPlusFileNames.tail != null) {
            TapEnv.setRelatedLanguage(TapEnv.CPLUSPLUS);
            final TapList<String> allcPlusPlusFileNames = cPlusPlusFileNames.tail;
            cPlusPlusFileNames = cPlusPlusFileNames.tail;
            final String firstFileName = cPlusPlusFileNames.head;
            final int nbFiles = TapList.length(cPlusPlusFileNames);
            final String[] commands = new String[nbFiles + 1];
            commands[0] = pathToParsers + "cppParserRun.sh";
            int i = 1;
            while (cPlusPlusFileNames != null) {
                commands[i] = cPlusPlusFileNames.head;
                ++i;
                cPlusPlusFileNames = cPlusPlusFileNames.tail;
            }
            // All the coming json stuff is basically useless, done only to
            // prevent cppParser to emit complaint messages about it being absent!
            // [TODO] get rid of this json file, and consequently
            //        of Parser.modifyJsonFile() and Parser.cleanJsonFiles()
            // creation du fichier compile_commands.json dans le directory courant:
            final String[] preProcessCommands = new String[TapEnv.CPLUSPLUS_PARSER_NB_ARGS + nbFiles];
            preProcessCommands[0] = "clang++";
            preProcessCommands[1] = "-MJ";
            preProcessCommands[2] = "o.json";
            preProcessCommands[3] = "-std=c++11";
            preProcessCommands[4] = "-w";
            preProcessCommands[5] = "-c";
            if (nbFiles >= 0) {
                System.arraycopy(commands, 1, preProcessCommands, TapEnv.CPLUSPLUS_PARSER_NB_ARGS - 1 + 1, nbFiles);
            }

            final BufferedReader reader = Parser.parse(preProcessCommands, commands, firstFileName);
            TapEnv.setCurrentParsedFile(firstFileName);
            if (reader != null) {
                String line = reader.readLine();
                while (line != null) {
                    if (line.startsWith("From file ")) {
                        String fileNameGivenInIL = line.substring(10);
                        position = readILstream(reader, origCallGraph, position, fileNameGivenInIL, TapEnv.CPLUSPLUS);
                        line = reader.readLine();
                    } else {
                        throw new IOException("Parsing error");
                    }
                }
            }
            if (!TapEnv.get().traceParser) {
                Parser.cleanJsonFiles(allcPlusPlusFileNames);
            }
        }
    }

    /**
     * Reads, into the given callGraph, the code arriving through the given "reader", which is a stream
     * of IL protocol representing the contents of file "fileName", of language "language".
     *
     * @param callGraph the CallGraph that will receive the code
     * @param reader    a stream from which the serialized IL tree is going to arrive, following the IL protocol.
     * @param position  the arbitrary position of the next element in the "reader" stream,
     *                  from which the position of each following element will be determined.
     * @param fileName  the name of the file that originally contained this Tree.
     * @param language  the original language of this file.
     * @return the position of the last element read from "reader", plus one.
     */
    private static int readILstream(BufferedReader reader, CallGraph callGraph,
                                    int position, String fileName, int language) {
        Unit fileUnit = callGraph.createNewUnit(null, language);
        fileUnit.setTranslationUnit(fileName);
        if (fileName.endsWith("iso_c_bindingf.f90") || fileName.endsWith("mpif.f90")) {
            fileUnit.setPredefinedModuleOrTranslationUnit();
        }
        TapEnv.resetIncludeManager() ;
        TreeProtocol inputTreeStream = new TreeProtocol(reader);
        callGraph.setInputStream(inputTreeStream);
        inputTreeStream.setPosition(position);
        if (TapEnv.traceInputIL()) {
            // to get the full IL tree, for checking.
            inputTreeStream.pushTreeReader();
            inputTreeStream.setTraceOn(true);
        }
        try {
            FlowGraphBuilder.build(inputTreeStream, callGraph.languageRootSymbolTable(language),
                    callGraph, language, fileUnit, false);
        } catch (IOException ignored) {
        }
        if (TapEnv.traceInputIL()) {
            // Retrieve the full IL tree, just for checking.
            Tree fullTree = inputTreeStream.popTreeReader();
            TapEnv.printlnOnTrace("Checking IL syntax of input tree:" + (fullTree.checkSyntax(fileName) ? "ok" : "wrong"));
            String unmodifiedInputFile = "unmodifiedInput.c++";
            try {
                final TextPrinter textPrinter = new TextPrinter(unmodifiedInputFile);
                textPrinter.initFile(language);
                Decompiler languagePrinter = null;
                if (TapEnv.isFortran(language)) {
                    languagePrinter = new FortranDecompiler(textPrinter, null);
                } else if (TapEnv.isC(language)) {
                    languagePrinter = new CDecompiler(textPrinter, null);
                }
                if (languagePrinter != null) {
                    languagePrinter.decompileTree(fullTree, language, false, false);
                    languagePrinter.newLine(0);
                }
                textPrinter.newLine();
                textPrinter.closeFile();
            } catch (IOException e) {
                TapEnv.printlnOnTrace("Could not open file " + unmodifiedInputFile + " to write the unmodified input file");
            }
        }
        return inputTreeStream.position() + 1;
    }

    private static boolean commandFound(final String command) {
        final File commandFile = new File(command);
        return commandFile.exists();
    }

    private static String[] splitTokens(final String optionsLine) {
        final StringTokenizer token = new StringTokenizer(optionsLine);
        final String[] result = new String[token.countTokens()];
        int i = 0;
        while (token.hasMoreElements()) {
            result[i] = token.nextToken();
            i = i + 1;
        }
        return result;
    }

    private static int getFormatOfFile(final String filename) {
        final String extension = TapEnv.getExtension(filename);
        if ("il".equals(extension))
            return Tapenade.IL ;
        else
            return -1 ;
    }

    private static int getLanguageOfFile(String filename) {
        int type = TapEnv.UNDEFINED;
        String extension = TapEnv.getExtension(filename);
        if ("il".equals(extension)) {
            filename = filename.substring(0, filename.length()-3) ; // TODO: should be done better
            extension = TapEnv.getExtension(filename);
        }
        switch (extension) {
            case "f":
            case "for":
            case "fortran":
            case "F":
                type = TapEnv.FORTRAN;
                break;
            case "f90":
            case "f95":
                type = TapEnv.FORTRAN90;
                if (TapEnv.get().suffixF90.equals(".f90??")) { 
                    TapEnv.get().suffixF90 = "." + extension;
                }
                break;
            case "f03":
                type = TapEnv.FORTRAN2003;
                break;
            case "c":
                type = TapEnv.C;
                break;
            case "cu":
                type = TapEnv.CUDA;
                break;
            case "cpp":
            case "cxx":
            case "c++":
            case "cc":
                type = TapEnv.CPLUSPLUS;
                break;
            case "jl":
                type = TapEnv.JULIA;
                break;
            case "jar":
            case "zip":
                type = TapEnv.JAR;
                break;
            case "h":
            case "inc":
            case "include":
                type = TapEnv.INCLUDE;
                break;
            case "":
                type = TapEnv.UNDEFINED ;
                break;
            default:
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Unknown file extension " + extension);
                break;
        }
        return type;
    }

    /**
     * Extracts files contained in an archive file.
     *
     * @param directory directory containing archive file.
     * @param filename  archive file name (with .zip or .jar suffix).
     * @return file names contained in archive file.
     */
    protected static String[] extractFiles(final String directory, final String filename) {
        System.out.println("ExtractFiles of filename "+filename+" in directory "+directory);
        String[] exFileNames = null;
        try (final ZipFile zipFile = new ZipFile(filename)) {
            TapList<String> names = null;
            String name;
            for (final Enumeration<? extends ZipEntry> en = zipFile.entries(); en.hasMoreElements(); ) {
                final ZipEntry zipEntry = en.nextElement();
                name = zipEntry.getName();
                if (!name.equals("META-INF/")
                        && !name.equals("META-INF/MANIFEST.MF")) {
                    if (!zipEntry.isDirectory()) {
                        names = new TapList<>(name, names);
                        Tapenade.copyInputStream(zipFile.getInputStream(zipEntry),
                                new BufferedOutputStream(java.nio.file.Files.newOutputStream(
                                        java.nio.file.Paths.get(directory + name))));
                    } else {
                        System.out.println("Directory ignored in archive file");
                    }
                }
            }
            final int nb = TapList.length(names);
            int i = 0;
            exFileNames = new String[nb];
            while (names != null) {
                exFileNames[i] = names.head;
                i = i + 1;
                names = names.tail;
            }
        } catch (IOException ignored) {

        }
        return exFileNames;
    }

    private static void copyInputStream(final InputStream in, final OutputStream out)
            throws IOException {
        final byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) >= 0) {
            out.write(buffer, 0, len);
        }
        in.close();
        out.close();
    }

    private static String rmFilenameExtension(final String filename) {
        String newfilename = filename;
        if (filename.endsWith(".f") || filename.endsWith(".F")
                || filename.endsWith(".c")) {
            newfilename = filename.substring(0, filename.length() - 2);
	} else if (filename.endsWith(".cu")) {
	    newfilename = filename.substring(0, filename.length() - 3);
        } else if (filename.endsWith(".f90") || filename.endsWith(".F90")
                || filename.endsWith(".f95") || filename.endsWith(".F95")
                || filename.endsWith(".f03") || filename.endsWith(".F03")) {
            newfilename = filename.substring(0, filename.length() - 4);
        }
        return newfilename;
    }

    /**
     * Inequality test on 2 identifiers in their parsed form.
     * The 2 TapList must contain the same elements.
     */
    private static boolean neqIdentifiers(TapList<String> id1, TapList<String> id2) {
        boolean areEqual = true;
        while (areEqual && (id1 != null || id2 != null)) {
            if (id1 == null || id2 == null
                    || !id1.head.equals(id2.head)) {
                areEqual = false;
            } else {
                id1 = id1.tail;
                id2 = id2.tail;
            }
        }
        return !areEqual;
    }

    private static void addIdentInList(final TapList<String> ident, final TapList<TapList<String>> toList) {
        TapList<TapList<String>> inList = toList;
        while (inList.tail != null
                && neqIdentifiers(ident, inList.tail.head)) {
            inList = inList.tail;
        }
        if (inList.tail == null) {
            inList.tail = new TapList<>(ident, null);
        }
    }

    private static int parseLanguage(final String languageName) {
        int result = -1;
        switch (languageName) {
            case "FORTRAN":
                result = TapEnv.FORTRAN;
                break;
            case "FORTRAN90":
                result = TapEnv.FORTRAN90;
                break;
            case "FORTRAN2003":
                result = TapEnv.FORTRAN2003;
                break;
            case "C":
                result = TapEnv.C;
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * Prints a description of all commonly available differentiation options.
     */
    private static void help() {
        TapEnv.printlnOnTrace(" Builds a differentiated program.");
        TapEnv.printlnOnTrace(" Usage: tapenade [options]* filenames");
        TapEnv.printlnOnTrace("  options:");
        TapEnv.printlnOnTrace("   -head, -root <proc>     set the differentiation root procedure(s)");
        TapEnv.printlnOnTrace("                           See FAQ for refined invocation syntax, e.g.");
        TapEnv.printlnOnTrace("                           independent and dependent arguments, multiple heads...");
        TapEnv.printlnOnTrace("   -tangent, -d            differentiate in forward/tangent mode (default)");
        TapEnv.printlnOnTrace("   -reverse, -b            differentiate in reverse/adjoint mode");
        TapEnv.printlnOnTrace("   -vector, -multi         turn on \"vector\" mode (i.e. multi-directional)");
        TapEnv.printlnOnTrace("   -specializeactivity <unit_names or %all%>  Allow for several activity patterns per routine");
        TapEnv.printlnOnTrace("   -primal, -p             turn off differentiation. Show pointer destinations");
        TapEnv.printlnOnTrace("   -output, -o <file>      put all generated code into a single <file>");
        TapEnv.printlnOnTrace("   -splitoutputfiles       split generated code, one file per top unit");
        TapEnv.printlnOnTrace("   -outputdirectory, -O <directory>  put all generated files in <directory> (default: .)");
        TapEnv.printlnOnTrace("   -I <includePath>        add a new search path for include files");
        TapEnv.printlnOnTrace("   -tgtvarname <str>       set extension for tangent variables  (default %d)");
        TapEnv.printlnOnTrace("   -tgtfuncname <str>      set extension for tangent procedures (default %_d)");
        TapEnv.printlnOnTrace("   -tgtmodulename <str>    set extension for tangent modules and types (default %_diff)");
        TapEnv.printlnOnTrace("   -adjvarname <str>       set extension for adjoint variables  (default %b)");
        TapEnv.printlnOnTrace("   -adjfuncname <str>      set extension for adjoint procedures (default %_b)");
        TapEnv.printlnOnTrace("   -adjmodulename <str>    set extension for adjoint modules and types (default %_diff)");
        TapEnv.printlnOnTrace("   -modulename <str>       set extension for tangent&adjoint modules and types (default %_diff)");
        TapEnv.printlnOnTrace("   -inputlanguage <lang>   language of  input files (fortran, fortran90,");
        TapEnv.printlnOnTrace("                           fortran95, or C)");
        TapEnv.printlnOnTrace("   -outputlanguage <lang>  language of output files (fortran, fortran90,");
        TapEnv.printlnOnTrace("                           fortran95, or C)");
        TapEnv.printlnOnTrace("   -ext <file>             incorporate external library description <file>");
        TapEnv.printlnOnTrace("   -nolib                  don't load standard libraries descriptions");
        TapEnv.printlnOnTrace("   -i<n>                   count <n> bytes for an integer (default -i4)");
        TapEnv.printlnOnTrace("   -r<n>                   count <n> bytes for a real (default -r4)");
        TapEnv.printlnOnTrace("   -dr<n>                  count <n> bytes for a double real (default -dr8)");
        TapEnv.printlnOnTrace("   -p<n>                   count <n> bytes for a pointer (default -p8)");
        TapEnv.printlnOnTrace("   -fixinterface           don't use activity to filter user-given (in)dependent vars");
        TapEnv.printlnOnTrace("   -noinclude              inline include files");
        TapEnv.printlnOnTrace("   -debugTGT               insert instructions for debugging tangent mode");
        TapEnv.printlnOnTrace("   -debugADJ               insert instructions for debugging adjoint mode");
        TapEnv.printlnOnTrace("   -tracelevel <n>         set the level of detail of trace milestones");
        TapEnv.printlnOnTrace("   -msglevel <n>           set the level of detail of error messages");
        TapEnv.printlnOnTrace("   -msginfile              insert error messages in output files");
        TapEnv.printlnOnTrace("   -dump <file>            write a dump <file>");
        TapEnv.printlnOnTrace("   -html                   display results in a web browser");
        TapEnv.printlnOnTrace("   -nooptim <str>          turn off optimization <str> (in {activity, difftypes,");
        TapEnv.printlnOnTrace("                           diffarguments, stripprimalmodules, spareinit, splitdiff, ");
        TapEnv.printlnOnTrace("                           mergediff, saveonlyused, tbr, snapshot, diffliveness,");
        TapEnv.printlnOnTrace("                           deadcontrol, recomputeintermediates,");
        TapEnv.printlnOnTrace("                           everyoptim}");
        TapEnv.printlnOnTrace("   -version                display Tapenade version information");
        TapEnv.printlnOnTrace(" Report bugs to <tapenade@inria.fr>.");
    }

    /**
     * Prints hidden differentiation options.
     */
    private static void hiddenHelp() {
        TapEnv.printlnOnTrace("  Warning: some options implemented only partly");
        TapEnv.printlnOnTrace("   -optim {spareinit, diffliveness, statictape, stripprimalcode}");
        TapEnv.printlnOnTrace("   -nooptim {activity, usefulness, difftypes, stripprimalcode, stripprimalmodules,");
        TapEnv.printlnOnTrace("             spareinit, diffarguments, splitdiff, mergediff, saveonlyused, tbr, snapshot,");
        TapEnv.printlnOnTrace("             diffliveness, deadcontrol, recomputeintermediates, refineadmm, everyoptim}");
        TapEnv.printlnOnTrace("   -noisize");
        TapEnv.printlnOnTrace("   -noisize77");
        TapEnv.printlnOnTrace("   -standaloneDiff    diff code need not link with primal code");
        TapEnv.printlnOnTrace("   -profile    Adds memory and CPU profiling calls");
        TapEnv.printlnOnTrace("   -buildsaverestore");
        TapEnv.printlnOnTrace("   -debugActivity");
        TapEnv.printlnOnTrace("   -debugPassives");
        TapEnv.printlnOnTrace("   -differentiateintegers");
        TapEnv.printlnOnTrace("   -copyname <str>       set extension for copies (\"\" for no extension)");
        TapEnv.printlnOnTrace("   -context");
        TapEnv.printlnOnTrace("   -specializeActivity <unit_names or %all%>       Allow for several activity patterns per routine");
        TapEnv.printlnOnTrace("   -diffReplica <n>      similar to -multi, but on the other side and for a fixed n");
        TapEnv.printlnOnTrace("   -directValid          tangent mode computing validity interval");
        TapEnv.printlnOnTrace("   -distinctNewVars");
        TapEnv.printlnOnTrace("   -duplicable <unit_names>     Declare these user functions cheap and without side-effects");
        TapEnv.printlnOnTrace("   -useSharpInclude") ;
        TapEnv.printlnOnTrace("   -oldContext");
        TapEnv.printlnOnTrace("   =============== TRACE AND DUMP: ===============");
        TapEnv.printlnOnTrace("   -traceParser");
        TapEnv.printlnOnTrace("   -traceInputIL");
        TapEnv.printlnOnTrace("   -traceFlowGraphBuild <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceTypeCheck <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceInline <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -tracePointer <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceInOut <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceDeps <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceADDeps <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceActivity <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceReqExplicit <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceDiffLiveness <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceTBR <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceDifferentiation <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceTreeGen <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -traceBlock <block_rank>");
        TapEnv.printlnOnTrace("   -traceCallerUnit <unit_name>");
        TapEnv.printlnOnTrace("   -traceCalleeUnit <unit_name>");
        TapEnv.printlnOnTrace("   -dumpunit <unit_names or %all%>");
        TapEnv.printlnOnTrace("   -dumpCallGraph");
        TapEnv.printlnOnTrace("   -dumpSymbolTables");
        TapEnv.printlnOnTrace("   -dumpFlowGraphs");
        TapEnv.printlnOnTrace("   -dumpZones");
        TapEnv.printlnOnTrace("   -dumpDataFlow");
        TapEnv.printlnOnTrace("     -dumpInOut");
        TapEnv.printlnOnTrace("     -dumpPointers");
        TapEnv.printlnOnTrace("     -dumpDeps");
        TapEnv.printlnOnTrace("   -dumpDiffSymbolTables");
        TapEnv.printlnOnTrace("   -dumpDiffFlowGraphs");
        TapEnv.printlnOnTrace("   -viewCallGraph");
    }

    /**
     * Tapenade graphical user interface.
     */
    private static void gui() {
        String display = System.getenv("DISPLAY");
        System.out.println("DISPLAY="+display);
        System.out.println("os.name="+OS_NAME);
        if (display != null
                || OS_NAME.startsWith(Tapenade.MAC)
                || OS_NAME.startsWith(Tapenade.WINDOWS)) {
            TapenadeFrame.getInstance();
        } else {
            System.out.println("No X11 DISPLAY variable was set,");
            System.out.println("but this program performed an operation which requires it.");
        }
    }

    public static CallGraph preProcess(final String[] files) {
        final Tapenade tapenade = new Tapenade();
        TapEnv.setModeNoDiff();
        tapenade.analyzeParameters(files);
        try {
            tapenade.createCallGraph();
        } catch (final IOException e) {
            TapEnv.toolError(e.toString());
        }
        return tapenade.origCallGraph();
    }

    private void runTapenade(final String[] args) {
        final Chrono totalTapenadeTimer = new Chrono();
        boolean hasError = false;
        tapenadeInit();
        TapEnv.setMixedLanguageInfos(MixedLanguageInfos.defaultMixedLangInfos());
        diffRoots = null;
        analyzeParameters(args);
        if (!TapEnv.optionsString().isEmpty()) {
            TapEnv.printlnOnTrace("@@ Options: " + TapEnv.optionsString());
        }
        checkParserFileSeparator();
        // If user gave the output file full name without path, prefix it with the output path
        if (TapEnv.get().outputDirectory!=null && outputFileFullName!=null && outputFileFullName.equals(TapEnv.stripPath(outputFileFullName)))
            outputFileFullName =  TapEnv.get().outputDirectory+outputFileFullName ;
        textMessageFileName = outputFileName;
        if (textMessageFileName == null) {
            String referenceName = (outputFileFullName!=null ? outputFileFullName : inputFileName);
            textMessageFileName = TapEnv.stripLanguageExtension(TapEnv.stripPath(referenceName)) ;
        }
        textMessageFileName =
            TapEnv.extendStringWithSuffix(textMessageFileName, TapEnv.get().diffFileSuffix)
            + ".msg";
        boolean hasGlobalDeclarations = false;
        try {
            hasGlobalDeclarations = analyzeFiles();
        } catch (IOException e) {
            hasError = true;
            TapEnv.systemWarning(TapEnv.MSG_ALWAYS, e.toString());
            if (TapEnv.get().dumps && dumpOutputStream != null) {
                e.printStackTrace(new PrintStream(dumpOutputStream));
            }
            if (TapEnv.get().dumps || debug) {
                System.out.println("Error "+e);
            }
        }
        final TapList<Unit> transformedUnits =
                getTransformedSourceUnits(diffCallGraph() == null ? null : diffCallGraph().backAssociations);
        final boolean showMessagesOfAllUnits =
                TapEnv.modeIsNoDiff() || diffCallGraph() == null || transformedUnits == null || TapIntList.contains(outputFormats, Tapenade.HTML);
        final TapList<PositionAndMessage> allMessages =
                TapEnv.getAllMessages(origCallGraph(), diffCallGraph(), transformedUnits, showMessagesOfAllUnits);
        if (TapIntList.contains(outputFormats, Tapenade.HTML)) {
            TapEnv.saveAllMessagesAsHTML(allMessages, absDirectoryHtmlGen + htmlMessageFileName,
                    cssDir, "", outputFileName, hasGlobalDeclarations, TapEnv.get().preprocessFileSuffix);
        }
        TapEnv.saveAllMessagesAsText(allMessages, TapEnv.get().outputDirectory+textMessageFileName);
        TapEnv.printlnOnTrace(0, "@@ Created "+TapEnv.get().outputDirectory+textMessageFileName);
        if (TapIntList.contains(outputFormats, Tapenade.HTML)) {
            htmlOutput();
        }
        if (TapEnv.get().dumps && dumpOutputStream!=null) {
            TapEnv.printlnOnTrace(0, "@@ Dump was written into "+TapEnv.get().outputDirectory+dumpFileName) ;
            try {
                dumpOutputStream.close() ;
            } catch (final IOException e) {
                TapEnv.printlnOnTrace("OutputStream for dump broken");
            }
        }
        if (hasError) {
            TapEnv.printlnOnTrace("total time " + totalTapenadeTimer.elapsed() + " s");
            TapEnv.tapenadeExit(1);
        } else if (TapEnv.get().hasParsingError) {
            TapEnv.printlnOnTrace("job is completed (but doubts on parsing): total time " + totalTapenadeTimer.elapsed() + " s");
        } else if (debug) {
            TapEnv.printlnOnTrace("job is completed: total time " + totalTapenadeTimer.elapsed() + " s");
        }
    }

    /**
     * Collect the source CallGraph Units that have at least one differentiated counterpart.
     */
    protected TapList<Unit> getTransformedSourceUnits(TapList<TapPair<Unit, TapList<Unit>>> differentiationAssociations) {
        TapList<Unit> transformedUnits = null;
        while (differentiationAssociations != null) {
            transformedUnits =
                    new TapList<>(differentiationAssociations.head.first, transformedUnits);
            differentiationAssociations = differentiationAssociations.tail;
        }
        return transformedUnits;
    }

    protected boolean analyzeFiles() throws IOException {
        if (TapIntList.contains(outputFormats, Tapenade.HTML)) {
            final File tapHtmlDir = new File(absDirectoryHtmlGen);
            if (!tapHtmlDir.exists()) {
                if (!new File(absDirectoryHtmlGen).mkdir()) {
                    TapEnv.printlnOnTrace("Could not open output directory tapenadehtml, -html option ignored");
                    outputFormats = TapIntList.remove(outputFormats, Tapenade.HTML) ;
                }
            }
        }

        TapEnv.initializeSourceMessageCorrespondence();
        // Create the original file's CallGraph:
        createCallGraph();
        if (origCallGraph() != null) {
            // Run analysis for unreachable Blocks:
            ReachAnalyzer.runAnalysis(origCallGraph(), null) ;
            // Dump all Control-Flow graphs:
            if (TapEnv.get().dumps && TapEnv.get().dumpFlowGraphs && dumpOutputStream != null) {
                TapEnv.pushOutputStream(dumpOutputStream);
                try {
                    final boolean dumpAllUnitsOfCallGraph = TapEnv.get().dumpUnitNames == null ||
                            TapEnv.get().dumpUnitNames.head.equals("%all%");
                    final TapList<Unit> unitsToDump = collectUnitsToDump(origCallGraph(), dumpAllUnitsOfCallGraph);
                    TapEnv.println();
                    TapEnv.println(" *** *** *** FLOW GRAPHS : *** *** ***");
                    origCallGraph().dumpAllFlowGraphs(unitsToDump, 2);
                } catch (final IOException e) {
                    TapEnv.printlnOnTrace("OutputStream for dump broken");
                    dumpOutputStream = null;
                }
                TapEnv.popOutputStream();
            }

            // Dump SymbolTable's:
            if (TapEnv.get().dumps && dumpOutputStream != null && origCallGraph() != null) {
                TapEnv.pushOutputStream(dumpOutputStream);
                try {
                    final boolean dumpAllUnitsOfCallGraph =
                            TapEnv.get().dumpUnitNames == null || TapEnv.get().dumpUnitNames.head.equals("%all%");
                    TapList<Unit> unitsToDump = collectUnitsToDump(origCallGraph(), dumpAllUnitsOfCallGraph);
                    if (TapEnv.get().dumpSymbolTables) {
                        TapEnv.println();
                        TapEnv.println(" *** *** *** SYMBOL TABLES : *** *** ***");
                        TapEnv.resetSeenSymbolTables();
                        origCallGraph().dumpAllSymbolTables(unitsToDump, dumpAllUnitsOfCallGraph, 2);
                    }
                    TapEnv.println();
                } catch (final IOException e) {
                    TapEnv.printlnOnTrace("OutputStream for dump broken");
                    dumpOutputStream = null;
                }
                TapEnv.popOutputStream();
            }

            // Dump zones:
            dumpZones();

            findRootUnits();
            final TapList<Unit> rootUnits = DiffRoot.collectUnits(diffRoots);
            final TapList<Unit> analyzedUnits = (rootUnits==null
                                                 ? origCallGraph().sortedComputationalUnits()
                                                 : Unit.allCalleesMulti(rootUnits)) ;
            if (TapEnv.traceLevel>15) {
                TapList<Unit> allUnits = origCallGraph().sortedComputationalUnits() ;
                while (allUnits!=null) {
                    if (allUnits.head.isStandard()) allUnits.head.printSizeStatistics() ;
                    allUnits = allUnits.tail ;
                }
            }
            // Run analyses: localized-arrays, then pointer, then in-out :
            if (!TapEnv.get().createStub && TapEnv.inputLanguage() != TapEnv.CPLUSPLUS) {
                origCallGraph().prepareAnalyses(rootUnits);
            }

            // Find and annotate Units that the command-line options specify with alternative checkpointing:
            TapList<Unit> exceptUnits =
                (TapEnv.get().defaultCheckpointCalls
                 ? origCallGraph().getUnits(splittedUnitNames)
                 : origCallGraph().getUnits(jointedUnitNames)) ;
            if (exceptUnits!=null) {
                TapList<Unit> ckpCandidateUnits = analyzedUnits ;
                while (ckpCandidateUnits!=null) {
                    final Unit candidateUnit = ckpCandidateUnits.head ;
                    if (exceptUnits.head==null //case of %all%
                        || TapList.contains(exceptUnits, candidateUnit)) {
                        // if the command-line option gives alternative checkpointing to candidateUnit:
                        if (TapEnv.get().defaultCheckpointCalls) {
                            // candidateUnit must be split mode (not checkpointed)
                            candidateUnit.directives =
                                new TapList<>(new Directive(Directive.NOCHECKPOINT), candidateUnit.directives);
                            candidateUnit.maybeNoCheckpointed = true;
                        } else {
                            // candidateUnit must be joint mode (checkpointed)
                            candidateUnit.directives =
                                new TapList<>(new Directive(Directive.CHECKPOINT), candidateUnit.directives);
                            candidateUnit.maybeCheckpointed = true;
                        }
                    }
                    ckpCandidateUnits = ckpCandidateUnits.tail ;
                }
            }
            // Similarly, find and annotate Units for specializeActivity mode:
            TapList<Unit> specActUnits = origCallGraph().getUnits(specializedActivityUnitNames);
            if (specActUnits != null && specActUnits.head == null) {
                //Case of %all%
                specActUnits = analyzedUnits ;
            }
            while (specActUnits != null) {
                final Unit unitToSpecialize = specActUnits.head;
                if (unitToSpecialize != null && unitToSpecialize.isStandard()) {
                    unitToSpecialize.directives =
                            new TapList<>(new Directive(Directive.SPECIALIZEACTIVITY), unitToSpecialize.directives);
                }
                specActUnits = specActUnits.tail;
            }

            // Dump the Call-Graph:
            if (TapEnv.get().dumps && dumpOutputStream != null && origCallGraph() != null) {
                TapEnv.pushOutputStream(dumpOutputStream);
                try {
                    TapList<Unit> unitsToDump = collectUnitsToDump(origCallGraph(), true);
                    if (TapEnv.get().dumpCallGraph) {
                        TapEnv.println();
                        TapEnv.println(" *** *** *** CALL GRAPH " + origCallGraph().name() + " : *** *** ***");
                        CallGraph.dumpCompactCallGraph(unitsToDump);
                    }
                    TapEnv.println();
                } catch (final IOException e) {
                    TapEnv.printlnOnTrace("OutputStream for dump broken");
                    dumpOutputStream = null;
                }
                TapEnv.popOutputStream();
            }

            // View the Call-Graph in a separate graph window:
            if (viewCallGraph) {
                fr.inria.tapenade.graphicsutils.GraphViewer.open();
                fr.inria.tapenade.graphicsutils.GraphViewer.view(origCallGraph());
            }

// origCallGraph().bilanFC();

            if (TapEnv.modeIsNoDiff()) {
                dumpDataFlow();
            } else if (diffRoots == null) {
                TapEnv.fileWarning(TapEnv.NO_POSITION, null, "File: The code provided does not contain a top procedure");
            } else {
                // Run AD-specific static Data-Flow analyses:
                // Run activity analysis:
                ADActivityAnalyzer.runAnalysis(origCallGraph(), diffRoots, diffKind);
                // Run reqExplicit analysis:
                ReqExplicit.runAnalysis(origCallGraph(), diffRoots);

                // Print on trace the ActivityPattern's found:
                if (TapEnv.traceActivity() != null) {
                    TapList<Unit> allUnits = origCallGraph().sortedUnits();
                    // Only if one wants to see ActivityPattern(s) for all the Intrinsics! :
                    // allUnits = TapList.append(allUnits, origCallGraph().intrinsicUnits()) ;
                    Unit unit;
                    TapEnv.pushOutputStream(TapEnv.get().traceOutputStream);
                    try {
                        while (allUnits != null) {
                            unit = allUnits.head;
                            if (unit.isStandard() || unit.isExternal() || unit.isInterface() || unit.isIntrinsic() ) {
                                TapList<ActivityPattern> activityPatterns = unit.activityPatterns;
                                TapEnv.println("  Resulting ActivityPattern(s) for Unit " + unit
                                        + (activityPatterns != null ? ":" : ": none"));
                                while (activityPatterns != null) {
                                    activityPatterns.head.dump();
                                    TapEnv.println();
                                    activityPatterns = activityPatterns.tail;
                                }
                            }
                            allUnits = allUnits.tail;
                        }
                    } catch (final IOException e) {
                        TapEnv.printlnOnTrace("OutputStream for trace broken");
                    }
                    TapEnv.popOutputStream();
                }

                // Run plain dependencies analysis:
                DepsAnalyzer.runAnalysis(origCallGraph(), rootUnits);
                if (TapEnv.removeDeadPrimal()) {
                    // Run diff-liveness analysis:
                    DiffLivenessAnalyzer.runAnalysis(origCallGraph(), rootUnits);
                } else {
                    TapEnv.printlnOnTrace("Diff-liveness analysis turned off");
                }
                if (TapEnv.mustAdjoint()) {
                    // Run "To Be Recorded" analysis:
                    ADTBRAnalyzer.runAnalysis(origCallGraph(), rootUnits);
                    ADTBRAnalyzer.summarizeMustTBR(origCallGraph()) ;
                }
                // If source code uses multithread, run detection of status of variables:
                if ((TapEnv.doOpenMP() || origCallGraph().hasCuda) && !TapEnv.modeIsNoDiff()) {
                    MultithreadAnalyzer.runAnalysis(origCallGraph());
                }

                dumpDataFlow();

// origCallGraph().bilanFC();

                // MAIN CENTRAL PART: differentiate origCallGraph into diffCallGraph:
                setDiffCallGraph(CallGraphDifferentiator.run(origCallGraph(), rootUnits, suffixes));
                if (diffCallGraph() != null) {
                    diffCallGraph().numberSymbolTables();
                }
                // Dump the diff SymbolTables and all diff Control-Flow graphs:
                if (TapEnv.get().dumps && dumpOutputStream != null && diffCallGraph() != null) {
                    TapEnv.pushOutputStream(dumpOutputStream);
                    try {
                        final String modeName;
                        if (TapEnv.modeIsOverloading()) {
                            modeName = "ADOLC";
                        } else {
                            modeName = (TapEnv.mustTangent()
                                    ? (TapEnv.mustAdjoint() ? "TANGENT PLUS ADJOINT" : "TANGENT")
                                    : (TapEnv.mustAdjoint() ? "ADJOINT" : "unknown"));
                        }
                        TapEnv.println();
                        TapEnv.println("=============== " + modeName + " MODE DIFFERENTIATION ===============");
                        final boolean dumpAllUnitsOfCallGraph =
                                TapEnv.get().dumpUnitNames == null || TapEnv.get().dumpUnitNames.head.equals("%all%");
                        final TapList<Unit> unitsToDump = collectUnitsToDump(diffCallGraph(), dumpAllUnitsOfCallGraph);
                        if (TapEnv.get().dumpDiffSymbolTables) {
                            TapEnv.println();
                            TapEnv.println(" *** *** *** DIFF SYMBOL TABLES : *** *** ***");
                            TapEnv.resetSeenSymbolTables();
                            diffCallGraph().dumpAllSymbolTables(unitsToDump, dumpAllUnitsOfCallGraph, 2);
                        }
                        if (TapEnv.get().dumpDiffFlowGraphs) {
                            TapEnv.println();
                            TapEnv.println(" *** *** *** DIFF FLOW GRAPHS : *** *** ***");
                            diffCallGraph().dumpAllFlowGraphs(unitsToDump, 2);
                        }
                        TapEnv.println();
                    } catch (final IOException e) {
                        TapEnv.printlnOnTrace("OutputStream for dump broken");
                        dumpOutputStream = null;
                    }
                    TapEnv.popOutputStream();
                }

            }

            if (TapIntList.contains(outputFormats, Tapenade.IL)) {
                fileILNames = new PrintWriter(java.nio.file.Files.newOutputStream(
                        java.nio.file.Paths.get(TapEnv.get().outputDirectory + "generatedIL.unitNames")));
            }

            TapEnv.get().srcUnitsToTraceInTreeGen = origCallGraph().getUnits(TapEnv.get().traceTreeGenUnitNames);

            final Unit topUnit = origCallGraph().sortedUnit(0);
            int targetLanguageCallDirection = 1;
            if (topUnit == null || topUnit.isC() || TapEnv.inputLanguage() == TapEnv.MIXED) {
                targetLanguageCallDirection = -1;
            }

            final ToBool existGlobalDeclarations = new ToBool(false);

            if (TapEnv.modeIsNoDiff() || TapIntList.contains(outputFormats, Tapenade.HTML) || showMessagesInFile) {
                final Chrono regenTimer = new Chrono();
                outputCallGraph(origCallGraph(),
                        TapEnv.get().outputDirectory, absDirectoryHtmlGen, "",
                        existGlobalDeclarations, targetLanguageCallDirection, true, true);
                TapEnv.printlnOnTrace(10, "@@ Source Regeneration total time " + regenTimer.elapsed() + " s");
            }

            if (!TapEnv.modeIsNoDiff() && diffCallGraph() != null) {
                final Chrono regenTimer = new Chrono();
                // Regenerate differentiated CallGraph's and Tree's :
                outputCallGraph(diffCallGraph(),
                        TapEnv.get().outputDirectory, absDirectoryHtmlGen, TapEnv.get().diffFileSuffix,
                        existGlobalDeclarations, targetLanguageCallDirection, false, false);
                TapEnv.printlnOnTrace(10, "@@ Diff Regeneration total time " + regenTimer.elapsed() + " s");
            }

            if (TapIntList.contains(outputFormats, Tapenade.HTML)) {
                new CallGraphDisplayer(origCallGraph(), absDirectoryHtmlGen + "callgraph.html",
                        existGlobalDeclarations.get(), outputFileName,
                        outputHtmlDirectory == null ? "" : outputHtmlDirectory,
                        TapEnv.get().preprocessFileSuffix, "Original call graph", "origFile")
                        .display();
                new CallGraphDisplayer(diffCallGraph(), absDirectoryHtmlGen + "callgraphdiff.html",
                        existGlobalDeclarations.get(), outputFileName,
                        outputHtmlDirectory == null ? "" : outputHtmlDirectory,
                        TapEnv.get().diffFileSuffix, "Differentiated call graph", "diffFile")
                        .display();
            }

            if (TapIntList.contains(outputFormats, Tapenade.IL)) {
                fileILNames.flush();
                fileILNames.close();
            }
            return existGlobalDeclarations.get();
        } else {
            return false;
        }
    }

    /**
     * Dump the zones of each SymbolTable for dumpUnitNames
     */
    public void dumpZones() {
        if (TapEnv.get().dumps && dumpOutputStream != null && TapEnv.get().dumpZones) {
            TapEnv.pushOutputStream(dumpOutputStream);
            try {
                TapEnv.println();
                TapEnv.println(" *** *** *** ZONES: *** *** ***");
                TapEnv.println();
                final boolean dumpAllUnits = (TapEnv.get().dumpUnitNames==null
                                              || TapEnv.get().dumpUnitNames.head.equals("%all%"));
                TapList<Unit> unitsToDump = collectUnitsToDump(origCallGraph(), dumpAllUnits);
                TapList<Unit> externalUnitsToDump = null;
                Unit oneUnit;
                TapList<SymbolTable> unitSymbolTables;
                SymbolTable oneSymbolTable;
                TapList<TapPair<SymbolTable, TapIntList>> toSTChainRks = new TapList<>(null, null);
                while (unitsToDump != null) {
                    oneUnit = unitsToDump.head;
                    unitSymbolTables = oneUnit.symbolTablesBottomUp();
                    if (unitSymbolTables == null) { // e.g. for external Units
                        externalUnitsToDump = new TapList<>(oneUnit, externalUnitsToDump);
                    } else {
                        while (unitSymbolTables != null) {
                            oneSymbolTable = unitSymbolTables.head;
                            addIntoSTChainRks(oneSymbolTable, toSTChainRks, null);
                            unitSymbolTables = unitSymbolTables.tail;
                        }
                    }
                    unitsToDump = unitsToDump.tail;
                }
                toSTChainRks = TapList.nreverse(toSTChainRks.tail);
                TapPair<SymbolTable, TapIntList> stInfo;
                String stRanks;
                SymbolTable thisSymbolTable;
                Unit thisUnit;
                while (toSTChainRks != null) {
                    stInfo = toSTChainRks.head;
                    thisSymbolTable = stInfo.first;
                    stRanks = stRanksBuildStr(new TapIntList(thisSymbolTable.rank(), stInfo.second));
                    thisUnit = thisSymbolTable.unit;
                    TapEnv.pushRelatedUnit(thisUnit);
                    if (thisUnit != null &&
                            thisUnit.publicSymbolTable() == thisSymbolTable) {
                        thisUnit.dumpExternalZones();
                    }
                    thisSymbolTable.dumpDeclaredZones(stRanks);
                    TapEnv.popRelatedUnit();
                    toSTChainRks = toSTChainRks.tail;
                }
                while (externalUnitsToDump != null) {
                    thisUnit = externalUnitsToDump.head;
                    TapEnv.pushRelatedUnit(thisUnit);
                    thisUnit.dumpExternalZones();
                    TapEnv.popRelatedUnit();
                    externalUnitsToDump = externalUnitsToDump.tail;
                }
                TapEnv.println();
            } catch (final IOException e) {
                TapEnv.printlnOnTrace("OutputStream for dump broken");
                dumpOutputStream = null;
            }
            TapEnv.popOutputStream();
        }
    }

    /**
     * Dump the static data-flow info on each Unit for dumpUnitNames
     */
    public void dumpDataFlow() {
        if (TapEnv.get().dumps && dumpOutputStream != null && TapEnv.get().dumpDataFlow) {
            TapEnv.pushOutputStream(dumpOutputStream);
            try {
                final boolean dumpAllUnits = TapEnv.get().dumpUnitNames == null
                        || TapEnv.get().dumpUnitNames.head.equals("%all%");
                final TapList<Unit> unitsToDump = collectUnitsToDump(origCallGraph(), dumpAllUnits);
                TapEnv.println();
                TapEnv.println(" *** *** *** DATA FLOW INFO: *** *** ***");
                TapEnv.println();
                origCallGraph().dumpAllDataFlows(unitsToDump);
                TapEnv.println();
            } catch (final IOException e) {
                TapEnv.printlnOnTrace("OutputStream for dump broken");
                dumpOutputStream = null;
            }
            TapEnv.popOutputStream();
        }
    }

    /**
     * Adds symbolTable and all its roots into toSTChainRks.
     * Doesn't add twice a SymbolTable which is already in toSTChainRks.
     * Adds the rank of symbolTable and possibly of its roots into
     * the lists that contain, for each SymbolTable,
     * the ranks of the SymbolTables who inherit from it.
     */
    private void addIntoSTChainRks(final SymbolTable symbolTable,
                                   TapList<TapPair<SymbolTable, TapIntList>> toSTChainRks,
                                   TapIntList ranks) {
        while (toSTChainRks.tail != null &&
                !symbolTable.nestedIn(toSTChainRks.tail.head.first)) {
            toSTChainRks = toSTChainRks.tail;
        }
        if (toSTChainRks.tail == null || symbolTable != toSTChainRks.tail.head.first) {
            toSTChainRks = toSTChainRks.placdl(new TapPair<>(symbolTable, ranks));
            ranks = new TapIntList(symbolTable.rank(), ranks);
        } else {
            toSTChainRks.tail.head.second =
                    TapIntList.quickUnion(toSTChainRks.tail.head.second, ranks);
        }
        if (symbolTable.basisSymbolTable() != null) {
            addIntoSTChainRks(symbolTable.basisSymbolTable(), toSTChainRks, ranks);
        }
    }

    private String stRanksBuildStr(TapIntList ranks) {
        StringBuilder result = new StringBuilder();
        while (ranks != null) {
            result.append('d').append(ranks.head);
            ranks = ranks.tail;
        }
        return result.toString();
    }

    private TapList<Unit> collectUnitsToDump(final CallGraph callGraph, final boolean dumpAllUnits) {
        if (dumpAllUnits) {
            TapList<Unit> allUnitsList = callGraph.sortedUnits();
            if (allUnitsList == null) {
                allUnitsList = callGraph.units();
            }
            allUnitsList = TapList.append(callGraph.dummyUnits, allUnitsList) ;
            final TapList<Unit> hdToDump = new TapList<>(null, null);
            TapList<Unit> tlToDump = hdToDump;
            Unit unit;
            while (allUnitsList != null) {
                unit = allUnitsList.head;
                if (!MPIcallInfo.isMessagePassingFunction(unit.name(), unit.language())
                    && !unit.inStdCIncludeFile()
                    && (unit.isTranslationUnit() || unit.isStandard() || unit.isExternal() || unit.isModule() || unit.isInterface() || unit.isClass() || unit.isDummy())
                ) {
                    tlToDump = tlToDump.placdl(unit);
                }
                allUnitsList = allUnitsList.tail;
            }
            return hdToDump.tail;
        } else {
            TapList<Unit> selectedOrigUnits = origCallGraph().getUnits(TapEnv.get().dumpUnitNames);
            if (callGraph == origCallGraph()) {
                return selectedOrigUnits;
            } else {
                // When dumping the diff callGraph, dump all counterparts
                // of the given selected orig units.
                TapList<Unit> thisUnitDiffs;
                TapList<Unit> diffUnitsToDump = null;
                while (selectedOrigUnits != null) {
                    thisUnitDiffs =
                            Unit.removePhantomPackages(
                                    TapList.cassq(selectedOrigUnits.head,
                                            diffCallGraph().backAssociations));
                    while (thisUnitDiffs != null) {
                        diffUnitsToDump = new TapList<>(thisUnitDiffs.head, diffUnitsToDump);
                        thisUnitDiffs = thisUnitDiffs.tail;
                    }
                    selectedOrigUnits = selectedOrigUnits.tail;
                }
                return diffUnitsToDump;
            }
        }
    }

    /**
     * Fills the origCallGraph with the AD libraries and then with all the given files.
     */
    private void createCallGraph() throws IOException {
        final Chrono cgBuildTimer = new Chrono();
        TapEnv.get().setOrigCallGraph(new CallGraph("Input CallGraph"));
        TapEnv.setRelatedUnit(null);
        
        final int lang;
        final int[] languages;
        // TODO: Review language handling
        if (Tapenade.LINUX.equals(parserDirectory)
                || Tapenade.MAC.equals(parserDirectory)
                || Tapenade.WINDOWS.equals(parserDirectory)) {
            
            // JLB: TODO: This whole block of things should be transformed and handled differently.
            if (defaultInputLanguage != TapEnv.UNDEFINED) {
                lang = defaultInputLanguage;
                languages = new int[2];
                if (lang == TapEnv.C) {
                    languages[TapEnv.C_FAMILY] = lang;
                    TapEnv.setLanguages(TapEnv.C_FAMILY, TapEnv.C);
                } else if (lang == TapEnv.CPLUSPLUS) {
                    languages[TapEnv.C_FAMILY] = lang;
                    TapEnv.setLanguages(TapEnv.C_FAMILY, TapEnv.CPLUSPLUS);
                } else {
                    languages[TapEnv.FORTRAN_FAMILY] = lang;
                    TapEnv.setLanguages(TapEnv.FORTRAN_FAMILY, lang);
                }
            } else {
                languages = getInputLanguageOfFiles(false, TapEnv.get().outputDirectory, inputFiles);
                if (languages[TapEnv.FORTRAN_FAMILY] == TapEnv.UNDEFINED) {
                    lang = languages[TapEnv.C_FAMILY];
                    TapEnv.setLanguages(TapEnv.C_FAMILY, lang);
                } else if (languages[TapEnv.C_FAMILY] == TapEnv.UNDEFINED) {
                    lang = languages[TapEnv.FORTRAN_FAMILY];
                    TapEnv.setLanguages(TapEnv.FORTRAN_FAMILY, lang);
                } else {
                    lang = TapEnv.MIXED;
                    TapEnv.setLanguages(TapEnv.FORTRAN_FAMILY, languages[TapEnv.FORTRAN_FAMILY]);
                    TapEnv.setLanguages(TapEnv.C_FAMILY, languages[TapEnv.C_FAMILY]);
                }
            }

            if (lang == TapEnv.C) {
                TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(DD99) Warning: Some features of C are not yet supported");
            } else if (lang == TapEnv.CPLUSPLUS) {
                TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(DD99) Warning: Many features of C++ are not yet supported");
            } else if (lang == TapEnv.MIXED) {
                TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(DD999) Warning: mixed language differentiation not yet fully implemented");
            }


            TapEnv.setInputLanguage(lang);
            origCallGraph().createRootSymbolTables();
            FortranStuff.initFortranSymbolTable(origCallGraph().fortranRootSymbolTable());
            CStuff.initCSymbolTable(origCallGraph().cRootSymbolTable());
            CStuff.initCSymbolTable(origCallGraph().cPlusPlusRootSymbolTable());

            if (!TapEnv.get().stripPrimalModules && lang != TapEnv.C) {
                TapEnv.addOptionsString(" no!StripPrimalModules");
            }        
            
            if (stdGeneralLibraryDirectory!=null) { // i.e. unless option -nolib
                if (lang == TapEnv.MIXED) {
                    TapEnv.setInputLanguage(languages[TapEnv.FORTRAN_FAMILY]);
                    // This duplicate reading of the same file is awful: TODO clean this up.
                    new GeneralLibReader(stdGeneralLibraryDirectory,
                        "GeneralLib", origCallGraph(), languages[TapEnv.FORTRAN_FAMILY]);
                    TapEnv.setInputLanguage(languages[TapEnv.FORTRAN_FAMILY]);
                    new GeneralLibReader(stdGeneralLibraryDirectory,
                        "GeneralLib", origCallGraph(), languages[TapEnv.C_FAMILY]);
                } else {
                    TapEnv.setInputLanguage(lang);
                    new GeneralLibReader(stdGeneralLibraryDirectory,
                        "GeneralLib", origCallGraph(), lang);
                }
                loadGeneralLibraries();
            }

            if (lang == TapEnv.MIXED) {
                // This duplicate reading of the same file is awful: TODO clean this up.
                TapEnv.setInputLanguage(languages[TapEnv.FORTRAN_FAMILY]);
                loadExternalLibraries(languages[TapEnv.FORTRAN_FAMILY]);
                TapEnv.setInputLanguage(languages[TapEnv.FORTRAN_FAMILY]);
                loadExternalLibraries(languages[TapEnv.C_FAMILY]);
            } else {
                TapEnv.setInputLanguage(lang);
                loadExternalLibraries(lang);
            }

            // Hopefully redundant? Maybe for servlet ?
            TapEnv.setIncludeDirs(hdIncludeDirs.tail);

            TapEnv.setInputLanguage(lang); // JLB: Isn't this the same as above?
            Tapenade.parseFiles(false, TapEnv.get().outputDirectory,
                    inputFiles, origCallGraph(), TapEnv.includeDirs(), defaultInputFormat, parseOpenMP);
            
            /* JLB: End of old and ugly language handling */
        } else {
            TapEnv.toolError("No parser for " + OS_NAME);
        }
        TapEnv.printlnOnTrace(10, "@@ Call Graph building total time " + cgBuildTimer.elapsed() + " s");
        final Chrono typeCheckTimer = new Chrono();
        origCallGraph().fixRemainingInterfaces() ;
        origCallGraph().terminateTypesAndZones();
        origCallGraph().numberSymbolTables();
        TapEnv.printlnOnTrace(10, "@@ Type Checking total time " + typeCheckTimer.elapsed() + " s");
    }

    private void loadGeneralLibraries() {
        boolean[] neededLibrairies = new boolean[TapEnv.JULIA+1]; // This is not nice. JULIA is simply the nb of languages handled
        for (final TapPair<String, Integer> filePair : inputFiles) {
            int aLang = filePair.second;
            neededLibrairies[aLang] = true;
        }
        if (neededLibrairies[TapEnv.C] || neededLibrairies[TapEnv.CPLUSPLUS] || neededLibrairies[TapEnv.CUDA])
            new GeneralLibReader(stdGeneralLibraryDirectory, "CGeneralLib", origCallGraph(), TapEnv.C);
        if (neededLibrairies[TapEnv.CUDA])
            new GeneralLibReader(stdGeneralLibraryDirectory, "CudaGeneralLib", origCallGraph(), TapEnv.CUDA);
        if (neededLibrairies[TapEnv.FORTRAN] || neededLibrairies[TapEnv.FORTRAN90] || neededLibrairies[TapEnv.FORTRAN2003])
            new GeneralLibReader(stdGeneralLibraryDirectory, "F77GeneralLib", origCallGraph(), TapEnv.FORTRAN);
        if ( neededLibrairies[TapEnv.FORTRAN90] || neededLibrairies[TapEnv.FORTRAN2003])
            new GeneralLibReader(stdGeneralLibraryDirectory, "F95GeneralLib", origCallGraph(), TapEnv.FORTRAN90);
        // Add JULIA library once written.
    }

    private void loadExternalLibraries(int lang){

        final StringTokenizer stLib = new StringTokenizer(libraryFileNames);
        String libraryFileName;
        while (stLib.hasMoreTokens()) {
            libraryFileName = stLib.nextToken();
            new GeneralLibReader("", libraryFileName, origCallGraph(), lang);
        }
    }

    /**
     * @return if mixed language [FortranDialect, C]
     * else [lang].
     */
    private int[] getInputLanguageOfFiles(final boolean addDirectory, final String directory,
                                          final TapList<TapPair<String, Integer>> localInputFileNames) {
        
        // In its current form this function does not allow for mix between C and C++ for instance
        // Nor does it allow for mix among ForTran dialects.
        String localInputFileName;
        int type;
        int[] types;
        int result = TapEnv.UNDEFINED;
        int[] results = new int[2]; // TODO: Once languages have been reviewed this can be updated.
        results[TapEnv.FORTRAN_FAMILY] = TapEnv.UNDEFINED;
        results[TapEnv.C_FAMILY] = TapEnv.UNDEFINED;
        for (final TapPair<String, Integer> filePair : localInputFileNames) {
            String fileName = filePair.first;
            int defLanguage = filePair.second;
            final int index = fileName.lastIndexOf(File.separator);
            String currentDirectory = "";
            if (index > 0) {
                currentDirectory = fileName.substring(0, index + 1);
            }
            TapEnv.setCurrentDirectory(currentDirectory);
            if (addDirectory) {
                localInputFileName = directory + fileName;
            } else {
                localInputFileName = fileName;
            }
            type = (defLanguage == TapEnv.UNDEFINED ? getLanguageOfFile(localInputFileName) : defLanguage);
            // JLB: 'type' may include CUDA and CPP
            if (type == TapEnv.JAR) {
                types = getInputLanguageOfFiles(true, directory, 
                    Tapenade.generateListInputFiles(Tapenade.extractFiles(directory, localInputFileName), null)
                );
                results = types;
            }
            if (result == TapEnv.UNDEFINED) {
                result = type;
                if (type == TapEnv.C || type == TapEnv.CPLUSPLUS || type == TapEnv.CUDA) {
                    results[TapEnv.C_FAMILY] = result;
                } else {
                    results[TapEnv.FORTRAN_FAMILY] = result;
                }
            } else if (result != type && type != TapEnv.INCLUDE) {
                if (type != TapEnv.C && result != TapEnv.C) {
                    result = Tapenade.getFortranDialect(type, result);
                    results[TapEnv.FORTRAN_FAMILY] = result;
                } else if (type == TapEnv.C) {
                    result = TapEnv.C;
                    results[TapEnv.C_FAMILY] = result;
                } else {
                    // JLB: Assuming 'type' was C++ and result was null, we are setting C++ into ForTran family. 
                    result = type;
                    results[TapEnv.FORTRAN_FAMILY] = result;
                }
            }
        }
        return results;
    }

    /**
     * sous cygwin, avec la ligne de commande complete, le filename contient des /
     * il faut changer le parserFileSeparator pour mettre / sinon on ne trouve
     * pas les include, cf F77:v53.
     */
    private void checkParserFileSeparator() {
        if (TapEnv.parserFileSeparator() == null) {
            if (File.separator.equals("\\")) {
                // getenv("PWD") renvoie /... sous cygwin windows
                // System.getProperty("user.dir") renvoie \...
                final String path = System.getenv("PWD");
                if (path != null && path.startsWith("/")) {
                    TapEnv.setParserFileSeparator("/");
                } else {
                    TapEnv.setParserFileSeparator(File.separator);
                }
            } else {
                TapEnv.setParserFileSeparator(File.separator);
            }
        }
    }

    /**
     * Translate the "differentiation roots" information (diffRoots)
     * that comes from the command line, into a form that is directly
     * usable by the CallGraph of the given program.
     * <br>Inputs are the global
     * CallGraph (which is fully type-checked and zones are built) and
     * the global "diffRoots", list of differentiation roots that contains
     * only the paths to diff Units and diff variables.
     * <br>Output is the same
     * "diffRoots", but completed with the diff Units themselves and the TapIntList
     * of public zones of their dependent and independent variables.
     */
    private void findRootUnits() {
        diffRoots = new TapList<>(null, diffRoots);
        TapList<DiffRoot> inDiffRoots = diffRoots;
        DiffRoot diffRoot;
        Unit unit;
        while (inDiffRoots.tail != null) {
            diffRoot = inDiffRoots.tail.head;
            unit = origCallGraph().getUnit(diffRoot.unitPathName());
            if (unit == null) {
                TapEnv.commandWarning(10, "unit " + DiffRoot.rebuildIdentifier(diffRoot.unitPathName()) + ": not found");
            } else if (!unit.isStandard() && !unit.isInterface()) {
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Can't differentiate " + unit.name() + ": is not a standard procedure");
                unit = null;
            }
            if (unit != null) {
                diffRoot.setUnit(unit);
                inDiffRoots = inDiffRoots.tail;
            } else {
                inDiffRoots.tail = inDiffRoots.tail.tail;
            }
        }
        if (diffRoots.tail == null && origCallGraph().nbUnits() > 0) {
            Unit firstUnit = null;
            Unit firstModule = null;
            Unit mainUnit = null;
            Unit uniti;
            for (int i = 0; i < origCallGraph().nbUnits() && firstUnit == null; ++i) {
                uniti = origCallGraph().sortedUnit(i);
                if (uniti != null) {
                    if (uniti.isStandard()) {
                        if (uniti == origCallGraph().getMainUnit()) {
                            mainUnit = uniti;
                        } else {
                            firstUnit = uniti;
                        }
                    } else if (uniti.isModule()) {
                        firstModule = uniti;
                    }
                }
            }
            if (firstUnit == null) {
                firstUnit = mainUnit;
            }
            if (!TapEnv.modeIsNoDiff()) {
                if (firstUnit == null) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "No root unit to differentiate");
                } else {
                    String unitStyle = "procedure";
                    if (firstUnit.isFortran()) {
                        unitStyle = "subroutine";
                    }
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Took " + unitStyle + " " + firstUnit.name() + " as default differentiation root");
                }
            } else if (firstUnit == null) {
                firstUnit = firstModule;
            }
            if (firstUnit != null) {
                final DiffRoot defaultRoot = new DiffRoot(firstUnit, null, null);
                final DiffPattern defaultPattern = new DiffPattern(null, null, null);
                defaultRoot.appendDiffPattern(defaultPattern);
                if (waitingInVars != null) {
                    defaultPattern.setIndepsPathNames(waitingInVars.tail);
                    waitingInVars = null;
                }
                if (waitingOutVars != null) {
                    defaultPattern.setDepsPathNames(waitingOutVars.tail);
                    waitingOutVars = null;
                }
                diffRoots.placdl(defaultRoot);
            }
        }
        diffRoots = diffRoots.tail;
    }

    // [FEWER NONREGRESSION] ONLY FOR FEWER DIFFS IN NONREGRESSION. TODO: REMOVE AND ACCEPT AD:
    private void forceSortedOrderInBlocks(TapList<Instruction> instructions) {
        // Bubble sort!
        TapList<Instruction> rest;
        Instruction i1;
        Instruction i2;
        Instruction iLight;
        Unit u1;
        Unit u2;
        Unit uLight;
        while (instructions != null) {
            i1 = instructions.head;
            u1 = i1.isUnitDefinitionStub();
            if (u1 != null) {
                iLight = i1;
                uLight = u1;
                rest = instructions.tail;
                while (rest != null) {
                    i2 = rest.head;
                    u2 = i2.isUnitDefinitionStub();
                    if (u2 != null) {
                        if (uLight.rank() > u2.rank()) {
                            iLight = i2;
                            uLight = u2;
                        }
                    }
                    rest = rest.tail;
                }
                if (iLight != i1) {
                    final Tree tmpTree = i1.tree;
                    i1.tree = iLight.tree;
                    iLight.tree = tmpTree;
                }
            }
            instructions = instructions.tail;
        }
    }

    /**
     * Output the given "callGraph" into files.
     *
     * @param callGraph       the CallGraph that must be output into files.
     * @param isOrigCallGraph true when this callGraph is the original CallGraph,
     *                        false when it is the differentiated CallGraph.
     */
    private void outputCallGraph(final CallGraph callGraph,
                                 final String fileDirectory, final String htmlDirectory,
                                 final String diffFileSuffix, final ToBool existsGlobalDeclarationsFile,
                                 final int callDirection, final boolean keepAllUnits,
                                 final boolean isOrigCallGraph) throws IOException {

        // Reorder CallGraph for the final TreeGen: (Is this useful now? [FEWER NONREGRESSION])
        callGraph.sortUnits(keepAllUnits, callDirection, -1, 1);

        if (isOrigCallGraph) { // if reordering, recompute units to trace, as traceTreeGenUnitNames may include ranks!
            TapEnv.get().srcUnitsToTraceInTreeGen =
                    callGraph.getUnits(TapEnv.get().traceTreeGenUnitNames);
        }

        // [FEWER NONREGRESSION] ONLY FOR FEWER DIFFS IN NONREGRESSION. TODO: REMOVE AND ACCEPT AD:
        for (TapList<Unit> topUs = callGraph.topUnits(); topUs != null; topUs = topUs.tail) {
            if (!topUs.head.isC()) {
                for (TapList<Block> allBs = topUs.head.allBlocks(); allBs != null; allBs = allBs.tail) {
                    forceSortedOrderInBlocks(allBs.head.instructions);
                }
            }
        }
        // Now that final code order is fixed, decide for forward declarations:
        callGraph.decideForwardDeclarations();

        final TapList<Tapenade.FutureFile> toFutureFiles = new TapList<>(null, null);
        TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes = new TapList<>(null, null);

        TapEnv.setRelatedUnit(null);
        int lang = TapEnv.inputLanguage();
        Unit topUnit = null;
        if (callGraph.topUnits() != null) {
            if (lang == TapEnv.FORTRAN) {
                topUnit = callGraph.topUnits().head;
            } else {
                topUnit = TapList.last(callGraph.topUnits());
            }
        }
        if (TapEnv.outputLanguage() != TapEnv.UNDEFINED) {
            lang = TapEnv.outputLanguage();
            if (TapEnv.outputLanguage() == TapEnv.MIXED && outputFileName != null) {
                if (topUnit != null) {
                    lang = topUnit.language();
                    TapEnv.setOutputLanguage(lang);
                } else {
                    lang = TapEnv.inputLanguage();
                }
            }
        } else if (topUnit != null) {
            lang = topUnit.language();
        } else {
            lang = TapEnv.UNDEFINED;
        }
        Tree mainServletFileTree = null;
        Tree otherServletFileTree = null;
        int otherServletFileLanguage = TapEnv.UNDEFINED;
        if (TapEnv.isServlet()) {
            mainServletFileTree = ILUtils.build(ILLang.op_blockStatement);
            TreeGen.addHeader(mainServletFileTree, lang, Version.HEADER);
            // TODO a changer avec CPP
            otherServletFileTree = ILUtils.build(ILLang.op_blockStatement);
            if (lang == TapEnv.C || lang == TapEnv.MIXED) {
                otherServletFileLanguage = TapEnv.languages(0);
                lang = TapEnv.languages(1);
            } else {
                otherServletFileLanguage = TapEnv.C;
            }
        }

        TreeGen.cleanBeforeGenerate(callGraph);

        // If command line uses option -o or -outputfullname and this is forbidden due to
        // symbols that are local to "translation units":
        if (outputFileName != null || outputFileFullName != null) {
            @SuppressWarnings("unchecked") final TapPair<Integer, TapList<String>>[] conflictingLocals =
                    new TapPair[]{new TapPair<>(0, null), new TapPair<>(0, null)};
            collectConflictingLocals(origCallGraph(), conflictingLocals);
            if (conflictingLocals[0].first > 1 && conflictingLocals[0].second != null) {
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Single-file output forbidden because Fortran symbols:" + conflictingLocals[0].second + " are local to their translation unit");
            }
            if (conflictingLocals[1].first > 1 && conflictingLocals[1].second != null) {
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Single-file output forbidden because C symbols:" + conflictingLocals[1].second + " are local to their translation unit");
            }
            if (TapEnv.associationByAddress()) {
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Single-file output forbidden with association byaddress");
            }
        }

        // When non-null, globalDeclsIncludeName is the name of an include file to be included at top of each file:
        String globalDeclsIncludeName = null;
        final boolean moreThanOneFile = TapList.length(callGraph.getTranslationUnitSymbolTables()) > 1;
        if (moreThanOneFile && outputFileName == null && outputFileFullName==null) {
            final Tree globalFileTree = ILUtils.build(ILLang.op_blockStatement);
            final boolean needsGlobalDecls =
                    TreeGen.generateGlobalDeclarations(callGraph, toFutureIncludes, null, globalFileTree, lang, false);
            if (needsGlobalDecls) {
                TreeGen.addHeader(globalFileTree, lang, Version.HEADER);
                existsGlobalDeclarationsFile.set(true);
                int globalLang = lang;
                if (lang == TapEnv.MIXED) {
                    // il n'y a pas de globales en fortran
                    globalLang = TapEnv.C; // TODO CPLUSPLUS
                }
                globalDeclsIncludeName = "GlobalDeclarations";
                if (!isOrigCallGraph) {
                    globalDeclsIncludeName = TapEnv.extendStringWithSuffix(globalDeclsIncludeName, diffFileSuffix);
                }
                if (TapEnv.modeIsNoDiff()) {
                    globalDeclsIncludeName = TapEnv.extendStringWithSuffix(globalDeclsIncludeName, TapEnv.get().preprocessFileSuffix);
                }
                toFutureFiles.placdl(new Tapenade.FutureFile(globalFileTree, globalLang, fileDirectory + globalDeclsIncludeName,
                        globalLang, null, false));
                if (TapEnv.isServlet()) {
                    final int nbNewSons = globalFileTree.length();
                    for (int i = 1; i <= nbNewSons; ++i) {
                        assert mainServletFileTree != null;
                        mainServletFileTree.addChild(ILUtils.copy(globalFileTree.down(i)), -1);
                    }
                }
            }
        }

        Tree treeForServlet;
        // New approach now that every file creates a translation Unit in topUnits:
        TapList<Unit> fileUnits = ((outputFileName != null || outputFileFullName!=null)
                                   ? callGraph.orderedFiles() : callGraph.topUnits()) ;
        while (fileUnits != null) {
            final Unit fileUnit = fileUnits.head;
            if (fileUnit.isTranslationUnit() && !fileUnit.isPredefinedModuleOrTranslationUnit()) {
                TapEnv.setRelatedUnit(fileUnit);
                if (TapEnv.outputLanguage() != TapEnv.UNDEFINED && TapEnv.outputLanguage() != TapEnv.MIXED) {
                    fileUnit.setLanguage(TapEnv.outputLanguage());
                }
                treeForServlet = TapEnv.isServlet() ?
                        fileUnit.sameLanguage(lang) ? mainServletFileTree : otherServletFileTree : null;
                final SymbolTable tuSymbolTable = fileUnit.publicSymbolTable();
                String futureFileFullName = null ;
                if (outputFileFullName!=null) {
                    futureFileFullName = outputFileFullName ;
                } else {
                    futureFileFullName = outputFileName ;
                    if (futureFileFullName==null)
                        futureFileFullName =
                            TapEnv.stripLanguageExtension(TapEnv.stripPath(fileUnit.name())) ;
                    String differentiationSuffix =
                        (isOrigCallGraph ?
                         (TapEnv.get().complexStep ? "_complexStep" : TapEnv.get().preprocessFileSuffix)
                         : TapEnv.get().diffFileSuffix);
                    futureFileFullName =
                        fileDirectory
                        + TapEnv.extendStringWithSuffix(futureFileFullName, differentiationSuffix)
                        + TapEnv.outputExtension(fileUnit.language()) ;
                }
                prepareFutureFile(futureFileFullName, fileUnit, tuSymbolTable,
                        globalDeclsIncludeName, toFutureIncludes, treeForServlet,
                        toFutureFiles, fileDirectory, callGraph, splitOutputFiles);
                if (splitOutputFiles) {
                    TapList<Unit> subUnits = fileUnit.lowerLevelUnits;
                    Unit subUnit;
                    while (subUnits != null) {
                        subUnit = subUnits.head;
                        TapEnv.setRelatedUnit(subUnit);
                        prepareFutureFile(fileDirectory+subUnit.name()+TapEnv.outputExtension(subUnit.language()),
                                subUnit, null, globalDeclsIncludeName, toFutureIncludes,
                                treeForServlet, toFutureFiles, fileDirectory, callGraph, false);
                        subUnits = subUnits.tail;
                    }
                }
            }
            fileUnits = fileUnits.tail;
        }

        TapEnv.setRelatedUnit(null);
        if (toFutureFiles.tail != null) {
            final Tree firstFileTree = toFutureFiles.tail.head.tree;
            if (firstFileTree != null) {
                ILUtils.adoptOrAppendComments(callGraph.topPreComments, "preComments", firstFileTree);
                ILUtils.adoptOrAppendComments(callGraph.topPostComments, "postComments", firstFileTree);
                ILUtils.adoptOrAppendComments(callGraph.topPreCommentsBlock, "preCommentsBlock", firstFileTree);
                ILUtils.adoptOrAppendComments(callGraph.topPostCommentsBlock, "postCommentsBlock", firstFileTree);
            }
        }
        if (outputFileName == null && TapEnv.isServlet()) {
            final String oFileName = TapEnv.extendStringWithSuffix(Tapenade.rmFilenameExtension(inputFileName), TapEnv.get().diffFileSuffix) + "-all";
            String strippedFileName = TapEnv.stripPath(oFileName) ;
            strippedFileName = Tapenade.rmFilenameExtension(strippedFileName) ;
            if (TapEnv.modeIsNoDiff()) {
                strippedFileName =
                    TapEnv.extendStringWithSuffix(strippedFileName,
                          (TapEnv.get().complexStep ? "_complexStep" : TapEnv.get().preprocessFileSuffix));
            }
            if (!TapEnv.isDistribVersion()) {
                TapEnv.printlnOnTrace(0, "@@ Creating " + fileDirectory + strippedFileName + TapEnv.outputExtension(lang));

            }
            toFutureFiles.placdl(
                    new Tapenade.FutureFile(mainServletFileTree, lang, fileDirectory + strippedFileName,
                            lang, null, false));
            if (!TapEnv.isDistribVersion()) {
                TapEnv.printlnOnTrace(0, "@@ Creating " + fileDirectory + strippedFileName + TapEnv.outputExtension(otherServletFileLanguage));
            }
            toFutureFiles.placdl(
                    new Tapenade.FutureFile(otherServletFileTree, otherServletFileLanguage, fileDirectory + strippedFileName,
                            lang, null, false));
        }

        // Now that all FutureFiles are ready, sweep through required include files and add them too into FutureFiles:
        toFutureIncludes.tail =
                TreeGen.generateInclude(toFutureIncludes, Version.HEADER);
        toFutureIncludes = toFutureIncludes.tail;
        while (toFutureIncludes != null) {
            final TapTriplet<Tree, Tree, Integer> include = toFutureIncludes.head;
            String includeName = include.first.stringValue();
            final int includeLang = include.third;
            if (TapEnv.isDiffIncludeFileName(includeName) && (!isOrigCallGraph || TapEnv.modeIsNoDiff())) {
//                 if (includeLang == TapEnv.C || includeLang == TapEnv.MIXED) {
                    if (includeName.startsWith("\"")) {
                        includeName = includeName.substring(1);
                    }
                    if (includeName.endsWith("\"")) {
                        includeName = includeName.substring(0, includeName.length() - 1);
                    }
//                 }
                final int index = includeName.indexOf(File.separator);
                String includeDirectory = fileDirectory;
                if (index >= 0) {
                    includeName = TapEnv.stripPath(includeName);
                    includeDirectory = fileDirectory;
                }
                if (!(TapIntList.contains(outputFormats, Tapenade.HTML) && isOrigCallGraph)) {
                    TapEnv.printlnOnTrace(0, "@@ Creating " + includeDirectory + includeName);
                }
                toFutureFiles.placdl(new Tapenade.FutureFile(include.second, includeLang, includeDirectory + includeName,
                        includeLang, null, true));
            }
            toFutureIncludes = toFutureIncludes.tail;
        }

        // Now output into files all the files in futureFiles:
        TapList<Tapenade.FutureFile> futureFiles = toFutureFiles.tail;
        String createdFile;
        final StringBuilder allCreatedFiles = new StringBuilder();
        while (futureFiles != null) {
            final Tapenade.FutureFile futureFile = futureFiles.head;
            if (futureFile.tree != null && futureFile.tree.length() > 0) {

                if (TapIntList.contains(outputFormats, Tapenade.HTML) && !futureFile.isIncludeFile) {
                    createdFile = htmlDirectory + TapEnv.stripPath(futureFile.fileFullName) + ".html";
                    allCreatedFiles.insert(0, createdFile + " ");
                    try {
                        String oppositeHtmlFileFullName = null;
                        if (futureFile.oppositeHtmlFileName != null) {
                            oppositeHtmlFileFullName =
                                    futureFile.oppositeHtmlFileName + TapEnv.outputExtension(futureFile.oppositeHtmlLanguage) + ".html";
                        }
                        final HtmlPrinter htmlPrinter =
                                new HtmlPrinter(createdFile, oppositeHtmlFileFullName,
                                        isOrigCallGraph ? "diffFile" : "origFile", cssDir);
                        htmlPrinter.initFile(futureFile.language);
                        Decompiler languagePrinter = null;
                        if (TapEnv.isFortran(futureFile.language)) {
                            languagePrinter = new FortranDecompiler(htmlPrinter, htmlMessageFileName);
                        } else if (TapEnv.isC(futureFile.language)) {
                            languagePrinter = new CDecompiler(htmlPrinter, htmlMessageFileName);
                        }
                        if (languagePrinter != null) {
                            languagePrinter.decompileTree(futureFile.tree, futureFile.language,
                                    false, showMessagesInFile);
                            languagePrinter.newLine(0);
                        }
                        htmlPrinter.closeFile();
                    } catch (final FileNotFoundException e) {
                        TapEnv.printlnOnTrace("Could not open output file " + createdFile + " for html output");
                    }
                }

                if (TapIntList.contains(outputFormats, Tapenade.FILE) && (!isOrigCallGraph || TapEnv.modeIsNoDiff() || showMessagesInFile)) {
                    createdFile = futureFile.fileFullName;
                    allCreatedFiles.insert(0, createdFile + " ");
                    try {
                        final TextPrinter textPrinter = new TextPrinter(createdFile);
                        textPrinter.initFile(futureFile.language);
                        Decompiler languagePrinter = null;
                        if (TapEnv.isFortran(futureFile.language)) {
                            languagePrinter = new FortranDecompiler(textPrinter, null);
                        } else if (TapEnv.isC(futureFile.language)) {
                            languagePrinter = new CDecompiler(textPrinter, null);
                        } else if (TapEnv.isJulia(futureFile.language)) {
                            languagePrinter = new JuliaDecompiler(textPrinter, null);
                        }
                        if (languagePrinter != null) {
                            if (TapEnv.isC(futureFile.language) && futureFile.isIncludeFile) {
                                String includeShortName = TapEnv.stripPath(futureFile.fileFullName);
                                if (includeShortName.contains(".")) {
                                    includeShortName = includeShortName.substring(0, includeShortName.indexOf('.'));
                                }
                                includeShortName = includeShortName + "_loaded";
                                includeShortName = includeShortName.toUpperCase();
                                textPrinter.printText("#ifndef " + includeShortName, "plain");
                                languagePrinter.newLine(0);
                                textPrinter.printText("#define " + includeShortName, "plain");
                                languagePrinter.newLine(0);
                            }
                            languagePrinter.decompileTree(futureFile.tree, futureFile.language,
                                    futureFile.isIncludeFile, showMessagesInFile);
                            if (TapEnv.isC(futureFile.language) && futureFile.isIncludeFile) {
                                languagePrinter.newLine(0);
                                textPrinter.printText("#endif", "plain");
                            }
                            languagePrinter.newLine(0);

                            if (TapEnv.isFortran(futureFile.language)) {
                                languagePrinter.newLine(0);
                            }

                        }
                        textPrinter.closeFile();
                    } catch (final FileNotFoundException e) {
                        TapEnv.printlnOnTrace("Could not open output file " + createdFile + " for file output");
                    }
                }

                if (TapIntList.contains(outputFormats, Tapenade.IL)) {
                    createdFile = futureFile.fileFullName + ".il";
                    allCreatedFiles.insert(0, createdFile + " ");
                    try {
                        final TextPrinter textPrinter = new TextPrinter(createdFile);
                        textPrinter.initFile(futureFile.language);
                        new DotDecompiler(textPrinter, null)
                                .decompileTree(futureFile.tree, futureFile.language, futureFile.isIncludeFile, showMessagesInFile);
                        textPrinter.newLine();
                        textPrinter.closeFile();
                    } catch (final FileNotFoundException e) {
                        TapEnv.printlnOnTrace("Could not open output file " + createdFile + " for IL output");
                    }
                }

            }
            futureFiles = futureFiles.tail;
        }

        if (allCreatedFiles.length() > 0) {
            TapEnv.printlnOnTrace(0, "@@ Created " + allCreatedFiles);
        }

    }

    private void prepareFutureFile(String fileFullName, final Unit unit, final SymbolTable fileSymbolTable,
                                   final String globalDeclsIncludeName,
                                   final TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes, final Tree treeForServlet,
                                   final TapList<Tapenade.FutureFile> toFutureFiles, final String fileDirectory,
                                   final CallGraph callGraph, final boolean skipSubUnits) {
        final int language = unit.language();
        int otherHtmlLanguage = unit.language();
        TapList<Tree> fileUserHelpStrings = null;
        boolean addUseDiffSizesHere = false;
        if (TapEnv.isC(language)) {
            addUseDiffSizesHere = true;
            fileUserHelpStrings = new TapList<>(null, null);
        }
        // Generate the Tree for the given unit:
        final Tree fileTree;
        if (unit.rank() == -1) {
            TapEnv.toolWarning(10, "Regenerated unit seems external (rank==-1): " + unit);
        }
        fileTree = TreeGen.generate(unit, false, false, skipSubUnits, toFutureIncludes, fileUserHelpStrings);

        // Search for an already existing FutureFile with the same file name (possible if -o filename):
        Tapenade.FutureFile futureFile = null;
        TapList<Tapenade.FutureFile> inFutureFiles = toFutureFiles.tail;
        while (futureFile == null && inFutureFiles != null) {
            if (inFutureFiles.head.fileFullName.equals(fileFullName)) {
                futureFile = inFutureFiles.head;
            }
            inFutureFiles = inFutureFiles.tail;
        }

        if (futureFile == null) {
            // Do this only when fileTree will be the first element in the futureFile:
            if (!TapEnv.isDistribVersion() && (!TapIntList.contains(outputFormats, Tapenade.HTML) || callGraph != TapEnv.get().origCallGraph())) {
                TapEnv.printlnOnTrace(15, "@@ Creating " + fileFullName);
            }

            TreeGen.addHeader(fileTree, language, Version.HEADER);
            String oppositeHtmlFileName = null;
            if (TapIntList.contains(outputFormats, Tapenade.HTML)) {
                String fileNameForHtml = TapEnv.stripLanguageExtension(TapEnv.stripPath(fileFullName)) ;
                if (callGraph == TapEnv.get().origCallGraph()) {
                    TapList<Unit> diffUnits = null;
                    if (diffCallGraph() != null) {
                        diffUnits = Unit.removePhantomPackages(
                                TapList.cassq(unit,
                                        diffCallGraph().backAssociations));
                    }
                    if (diffUnits != null) {
                        oppositeHtmlFileName =
                                fileNameForHtml.substring(0, fileNameForHtml.length() - TapEnv.get().preprocessFileSuffix.length())
                                        + TapEnv.get().diffFileSuffix;
                        otherHtmlLanguage = diffUnits.head.language();
                    }
                } else {
                    oppositeHtmlFileName =
                            fileNameForHtml.substring(0, fileNameForHtml.length() - TapEnv.get().diffFileSuffix.length())
                                    + TapEnv.get().preprocessFileSuffix;
                    if (unit.otherName() != null) {
                        otherHtmlLanguage = unit.otherName().language();
                    }
                }
            }

            if (TapEnv.modeIsNoDiff() && callGraph != TapEnv.get().origCallGraph()) {
                fileFullName = 
                    TapEnv.extendStringWithSuffix(
                        TapEnv.stripLanguageExtension(fileFullName),
                        (TapEnv.get().complexStep ? "_complexStep" : TapEnv.get().preprocessFileSuffix))
                    + TapEnv.outputExtension(language) ;
            }

            // If fileSymbolTable is provided (SymbolTable of the current "Translation Unit" i.e. file),
            // fill the fileTree with the global declarations:
            if (fileSymbolTable != null && fileSymbolTable.mustBeCreated()) {
                if (globalDeclsIncludeName != null) {
                    fileTree.addChild(
                            ILUtils.build(ILLang.op_include, "\"" + globalDeclsIncludeName + TapEnv.outputExtension(TapEnv.C) + "\""),
                            1);
                } else if (TapEnv.isC(language)) {
                    //[llh] This seems to add only the includes of adBuffer, adDebug, etc... rewrite?
                    TreeGen.generateGlobalDeclarations(callGraph, toFutureIncludes, fileUserHelpStrings,
                            fileTree, language, false);
                }
            }

            // When necessary, add the include/use of "DIFFSIZE.?", plus user help hint comments:
            if (addUseDiffSizesHere) {
                TreeGen.addUseDiffSizesAndUserHelp(fileTree, fileUserHelpStrings.tail, language);
            }

            futureFile = new Tapenade.FutureFile(fileTree, language, fileFullName,
                    otherHtmlLanguage, oppositeHtmlFileName, false);
            toFutureFiles.placdl(futureFile);

        } else {
            final Tree[] newPieces = fileTree.children();
            for (final Tree newPiece : newPieces) {
                futureFile.tree.addChild(newPiece, -1);
            }
        }

        if (TapEnv.get().dumps && dumpOutputStream != null && TapEnv.get().dumpDiffFlowGraphs && fileTree != null) {
            TapEnv.printOnDump("Checking syntax of unit " + unit.name() + ':'
                    + (fileTree.checkSyntax(unit.name()) ? "ok" : "wrong") + System.lineSeparator());
        }

        if (fileTree != null && treeForServlet != null) {
            treeForServlet.addChild(ILUtils.copy(fileTree), -1);
        }

        if (TapEnv.get().srcUnitsToTraceInTreeGen != null) {
            TapEnv.printlnOnTrace("Contents for future file " + fileFullName + ':');
            try {
                TapEnv.indent(2);
                ILUtils.dump(futureFile.tree, 2);
                TapEnv.println();
            } catch (final IOException e) {
                TapEnv.printlnOnTrace("IO error while dumping generated tree");
            }
        }
    }

    /**
     * For each family of languages (i.e. C or Fortran?...), collects the (variable or procedure) symbols
     * that are local to their translation unit (i.e. file), and counts the number of files of this language.
     * This is used to detect cases where the "-o" option would cause conflicts
     * because previously local variables would be fused into a single file.
     */
    private void collectConflictingLocals(final CallGraph callGraph, final TapPair<Integer, TapList<String>>[] conflictingLocals) {
        Unit topUnit;
        int langFamily;
        SymbolTable tust;
        SymbolDecl symbol;
        for (TapList<Unit> topUnits = callGraph.topUnits(); topUnits != null; topUnits = topUnits.tail) {
            topUnit = topUnits.head;
            if (topUnit.isTranslationUnit()) {
                langFamily = topUnit.isC() ? TapEnv.C_FAMILY : TapEnv.FORTRAN_FAMILY;
                conflictingLocals[langFamily].first = conflictingLocals[langFamily].first + 1;
                if (langFamily == TapEnv.C_FAMILY) {
                    // So far, we see no possible conflict in Fortran because there are no global vars and all procedures are global!!
                    tust = topUnit.translationUnitSymbolTable();
                    for (TapList<SymbolDecl> symbols = tust.getAllTopVariableDecls(); symbols != null; symbols = symbols.tail) {
                        symbol = symbols.head;
                        if (!symbol.isShared()) {
                            conflictingLocals[TapEnv.C_FAMILY].second =
                                    new TapList<>(symbol.symbol, conflictingLocals[langFamily].second);
                        }
                    }
                    for (TapList<SymbolDecl> symbols = tust.getAllTopFunctionDecls(); symbols != null; symbols = symbols.tail) {
                        symbol = symbols.head;
                        if (!symbol.isShared()) {
                            conflictingLocals[langFamily].second =
                                    new TapList<>(symbol.symbol, conflictingLocals[langFamily].second);
                        }
                    }
                }
            }
        }
    }

    /**
     * Extract all information from the command-line arguments "argv".
     */
    private void analyzeParameters(final String[] argv) {
        TapList<TapPair<String, Integer>> listFileNames = null;
        String dumpPathFile = null;
        String tgtFuncSuffix = null;
        String tgtVarSuffix = null;
        String adjFuncSuffix = null;
        String adjVarSuffix = null;
        String tgtModuleSuffix = null;
        String adjModuleSuffix = null;
        String moduleSuffix = null;
        String diffFuncSuffix = null;
        String diffVarSuffix = null;
        String copiedUnitSuffix = null;
        String assocByAddressSuffix = null;
        boolean seenExplicitDiffLivenessOption = false;
        int i = 0;
        String argviLower;
        while (i < argv.length) {
            argviLower = argv[i].toLowerCase();
            //STANDARD DIFFERENTIATION OPTIONS:
            if (argviLower.equals("-inputformat")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                if (argv[i + 1] != null && !argv[i + 1].isEmpty()) {
                    final String inputForm = argv[i + 1].toLowerCase();
                    switch (inputForm) {
                        case "protocol": //obsolete
                        case "dot":      //obsolete
                        case "il":
                            defaultInputFormat = Tapenade.IL;
                            break ;
                        case "file":
                            defaultInputFormat = Tapenade.FILE;
                            break ;
                        default:
                            break ;
                    }
                }
                i++;
            } else if (argviLower.equals("-outputformat")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                if (argv[i + 1] != null && !argv[i + 1].isEmpty()) {
                    final String outputForm = argv[i + 1].toLowerCase();
                    switch (outputForm) {
                        case "protocol": //obsolete
                        case "dot":      //obsolete
                        case "il":
                            outputFormats = TapIntList.add(outputFormats, Tapenade.IL) ;
                            break ;
                        case "html":
                            outputFormats = TapIntList.add(outputFormats, Tapenade.HTML) ;
                            break ;
                        case "file":
                            outputFormats = TapIntList.add(outputFormats, Tapenade.FILE) ;
                            break ;
                        case "nolanguage":
                        case "nofile":
                            outputFormats = TapIntList.remove(outputFormats, Tapenade.FILE) ;
                            break ;
                        default:
                            break ;
                    }
                }
                i++;
            } else if (argviLower.equals("-inputlanguage")) {
                if (i >= argv.length - 2) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i] + "\n\t2 required: forced language and filename");
                    TapEnv.tapenadeExit(1);
                }
                if (argv[i+1] != null && !argv[i+1].isEmpty()) {
                    if (argv[i+2].startsWith("-")) {
                        TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Second argument after " + argv[i] + " must be a filename. " + argv[i+2] + " given. ");
                    }
                    final String inputLang = argv[i+1].toLowerCase();
                    int forcedLanguage = TapEnv.UNDEFINED;
                    switch (inputLang) {
                        case "fortran":
                            forcedLanguage = TapEnv.FORTRAN;
                            break;
                        case "fortran90":
                        case "fortran95":
                            forcedLanguage = TapEnv.FORTRAN90;
                            break;
                        case "fortran2003":
                        case "fortran03":
                            forcedLanguage = TapEnv.FORTRAN2003;
                            break;
                        case "c":
                            forcedLanguage = TapEnv.C;
                            break;
                        case "cpp":
                        case "c++":
                            forcedLanguage = TapEnv.CPLUSPLUS;
                            break;
                        default:
                            break;
                    }
                    listFileNames = addFileToBeDifferentiated(argv[i+2], forcedLanguage, listFileNames);
                    TapEnv.setInputLanguage(forcedLanguage);
                }
                i += 2;
            } else if (argviLower.equals("-outputlanguage")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                String outputLang = argv[i + 1];
                if (outputLang != null) {
                    outputLang = outputLang.toLowerCase();
                    switch (outputLang) {
                        case "fortran":
                            TapEnv.setOutputLanguage(TapEnv.FORTRAN);
                            break;
                        case "fortran90":
                            TapEnv.setOutputLanguage(TapEnv.FORTRAN90);
                            TapEnv.get().suffixF90 = ".f90";
                            break;
                        case "fortran95":
                            TapEnv.setOutputLanguage(TapEnv.FORTRAN90);
                            TapEnv.get().suffixF90 = ".f95";
                            break;
                        case "fortran2003":
                            TapEnv.setOutputLanguage(TapEnv.FORTRAN2003);
                            TapEnv.get().suffixF90 = ".f03";
                            break;
                        case "c":
                            TapEnv.setOutputLanguage(TapEnv.C);
                            break;
                        case "cuda":
                            TapEnv.setOutputLanguage(TapEnv.CUDA);
                            break;
                        case "cpp":
                        case "c++":
                            TapEnv.setOutputLanguage(TapEnv.CPLUSPLUS);
                            break;
                        default:
                            break;
                    }
                }
                i++;
            } else if (argviLower.equals("-openmp")) {
                this.parseOpenMP = true;
                TapEnv.setDoOpenMP(true);
                TapEnv.setDoOpenMPZ3(true);
                TapEnv.addOptionsString(" OpenMP");
            } else if (argviLower.equals("-openmp-noz3")) {
                this.parseOpenMP = true;
                TapEnv.setDoOpenMP(true);
                TapEnv.addOptionsString(" OpenMP-NOZ3");
                TapEnv.setDoOpenMPZ3(false);
            // Need to check what this is and why it is here. Is it still relevant? 
            } else if (argviLower.equals("-freeformat") || argv[i].equals("-f95")) {
                defaultInputLanguage = TapEnv.FORTRAN90;
            } else if (argviLower.equals("-linelength")) {
                TapEnv.get().fortran90LineLength = argv[i + 1];
                TapEnv.get().fortran90OutputLineLength = Integer.parseInt(TapEnv.get().fortran90LineLength);
                i++;
            } else if (argviLower.equals("-tangent") || argviLower.equals("-forward")
                    || argv[i].equals("-d") || argv[i].equals("-tl")) {
                TapEnv.setMustTangent(true);
                TapEnv.setModeTangent();
            } else if (argviLower.equals("-adolc")) {
                TapEnv.setModeOverloading();
            } else if (argviLower.equals("-backward") || argviLower.equals("-reverse") || argv[i].equals("-b")
                    || argv[i].equals("-cl")) {
                if (TapEnv.get().complexStep) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Complex-Step method forbids \""+argv[i]+"\"") ;
                } else {
                    TapEnv.setMustAdjoint(true);
                    TapEnv.setModeAdjoint();
                }
            } else if (argviLower.equals("-directvalid")) {
                TapEnv.setModeTangent();
                TapEnv.setValid(true);
                TapEnv.addOptionsString(" directValid");
            } else if (argviLower.equals("-distinctnewvars")) {
                TapEnv.get().newSHdisambiguator = 0;
                TapEnv.addOptionsString(" distinctNewVars");
            } else if (argviLower.equals("-context")) {
                TapEnv.setMustContext(true);
                TapEnv.addOptionsString(" context");
                if (TapEnv.get().diffReplica>1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Context mode forces diffReplica==1") ;
                    TapEnv.get().diffReplica = 1 ;
                }
            } else if (argviLower.equals("-oldcontext")) {
                // Old context system, before inventing the "-context"
                // This mode adds memory allocation and pointer computations of the adjoint variables
                // into the copied "_CD" and "_CB" procedures.
                TapEnv.get().oldContext = true;
                TapEnv.addOptionsString(" oldContext");
            } else if (argviLower.equals("-usesharpinclude")) {
                TapEnv.setUseSharpInclude(true) ;
                TapEnv.addOptionsString(" useSharpInclude");
            } else if (argviLower.equals("-tgtfuncname")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                tgtFuncSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-tgtvarname")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                tgtVarSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-tgtmodulename")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                tgtModuleSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-adjfuncname")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                adjFuncSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-adjvarname")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                adjVarSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-adjmodulename")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                adjModuleSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-modulename")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                moduleSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-copyname")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                copiedUnitSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-assocbyaddresssuffix")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                assocByAddressSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-difffuncname")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                } else {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Command-line option -difffuncname is deprecated: please use -tgtfuncname or -adjfuncname");
                }
                diffFuncSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-diffvarname")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                } else {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Command-line option -diffvarname is deprecated: please use -tgtvarname or -adjvarname");
                }
                diffVarSuffix = argv[i + 1];
                i++;
            } else if (argviLower.equals("-primal") || argv[i].equals("-p") || argviLower.equals("-preprocess")) {
                // TODO: deprecate -preprocess (ambiguous with -preprocessor). Promote -primal!
                // TODO: fix combination with -complexStep
                TapEnv.setModeNoDiff();
            } else if (argviLower.equals("-createstub")) {
                TapEnv.setModeNoDiff();
                TapEnv.get().createStub = true;
            } else if (argv[i].equals("-O")
                    || argviLower.equals("-outputdirectory")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                String directory = argv[i + 1];
                final File directoryFile = new File(directory);
                if (directoryFile.exists() && directoryFile.isDirectory()) {
                    if (!directory.endsWith(File.separator)) {
                        directory = directory+File.separator;
                    }
                    TapEnv.get().outputDirectory = directory;
                } else {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Output directory not found: " + directory);
                    TapEnv.tapenadeExit(1);
                }
                i++;
            } else if (argv[i].equals("-o") || argviLower.equals("-output")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                final String outputFilePathName = argv[i];
                if (outputFilePathName == null || outputFilePathName.isEmpty()) {
                    outputFileName = null;
                } else {
                    outputFileName = TapEnv.stripPath(outputFilePathName);
                    if (outputFileName != null
                            && !outputFileName.equals(outputFilePathName)) {
                        TapEnv.commandWarning(14, "Parent directory ignored in " + outputFilePathName);
                    }
                }
            } else if (argviLower.equals("-outputfullname")) {
                // (for Onera) exact name of the single output file
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                outputFileFullName = argv[i];
                if (outputFileFullName != null && outputFileFullName.isEmpty()) {
                    outputFileFullName = null;
                }
            } else if (!TapIntList.contains(outputFormats, Tapenade.HTML) && argviLower.equals("-splitoutputfiles")) {
                splitOutputFiles = true;
            } else if (argviLower.equals("-diffreplica")) {
                // Mode with several replicate diff variables. Similar to multi. Usage e.g. "tapenade -diffReplica N"
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                try {
                    int diffReplica = Integer.parseInt(argv[i+1]) ;
                    if (diffReplica==1 || (TapEnv.debugAdMode()==TapEnv.NO_DEBUG && !TapEnv.mustContext())) {
                        TapEnv.get().diffReplica = diffReplica ;
                        TapEnv.addOptionsString(" diffReplica:"+diffReplica);
                    } else {
                        TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Debug or Context modes force diffReplica==1") ;
                    }
                } catch (final NumberFormatException e) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Incorrect replica number "+argv[i+1]);
                }
                i++;
            } else if (argviLower.equals("-multi") || argviLower.equals("-vector")) {
                TapEnv.get().multiDirDiffMode = true;
                TapEnv.addOptionsString(" multiDirectional");
            } else if (argviLower.equals("-nbdirs")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                final String fixedNbdirsString = argv[i + 1];
                if (fixedNbdirsString != null && !fixedNbdirsString.isEmpty()) {
                    TapEnv.get().fixedNbdirsString = fixedNbdirsString;
                    TapEnv.get().fixedNbdirsmaxString = fixedNbdirsString;
                    TapEnv.addOptionsString(" nbdirs:"+fixedNbdirsString);
                }
                i++;
            } else if (argviLower.equals("-nbdirsmax")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                final String fixedNbdirsmaxString = argv[i + 1];
                if (fixedNbdirsmaxString != null && !fixedNbdirsmaxString.isEmpty()) {
                    TapEnv.get().fixedNbdirsmaxString = fixedNbdirsmaxString;
                    TapEnv.addOptionsString(" NBDirsMax:"+fixedNbdirsmaxString);
                }
                i++;
            } else if (argviLower.equals("-parserfileseparator")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                final String parserFileSep = argv[i + 1];
                if (parserFileSep != null && !parserFileSep.isEmpty()) {
                    TapEnv.get().parserFileSeparator = parserFileSep;
                }
                i++;
            } else if (argv[i].equals("-I") || argviLower.equals("-include")) {
                // There may be successive -I includeDir. Keep them all, in order:
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                String includeDir = argv[i + 1];
                if (!includeDir.endsWith(File.separator)) {
                    includeDir = includeDir + File.separator;
                }
                // pb "./" specifique unix:
                if (includeDir.startsWith("./")) {
                    includeDir = includeDir.substring(2);
                }
                tlIncludeDirs = tlIncludeDirs.placdl(includeDir);
                i++;
            } else if (argv[i].startsWith("-I")) {
                // Same as above, but with the directory stuck to the "-I"
                String includeDir = argv[i].substring(2);
                if (!includeDir.endsWith(File.separator)) {
                    includeDir = includeDir + File.separator;
                }
                // pb "./" specifique unix:
                if (includeDir.startsWith("./")) {
                    includeDir = includeDir.substring(2);
                }
                tlIncludeDirs = tlIncludeDirs.placdl(includeDir);
            } else if (argviLower.equals("-noinclude")) {
                TapEnv.get().expandAllIncludeFile = true;
                TapEnv.addOptionsString(" noinclude");
            } else if (argviLower.equals("-cpp") || argviLower.equals("-cpreprocessor")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                TapEnv.get().cppCommand = ("".equals(argv[i + 1]) ? "#none#" : argv[i + 1]);
                i++;
            } else if (argviLower.equals("-cppoptions") || argviLower.equals("-cpreprocessoroptions")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                TapEnv.get().cppOptions = argv[i + 1];
                i++;
            } else if (argviLower.equals("-fpp") || argviLower.equals("-fpreprocessor")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                TapEnv.get().fppCommand = ("".equals(argv[i + 1]) ? "#none#" : argv[i + 1]);
                i++;
            } else if (argviLower.equals("-fppoptions") || argviLower.equals("-fpreprocessoroptions")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                TapEnv.get().fppOptions = argv[i + 1];
                i++;
            } else if (argviLower.equals("-nocomment")) {
                TapEnv.get().noComment = true;
                TapEnv.addOptionsString(" nocomment");
            } else if (argviLower.equals("-root") || argviLower.equals("-head")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                parseDiffRoots(argv[i + 1]);
                i++;
            } else if (argviLower.equals("-vars") || argviLower.equals("-variables")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                if (TapEnv.isDistribVersion()) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Command-line option " + argv[i] + " is deprecated: please use -head instead");
                }
                parseDiffVars(argv[i + 1]);
                i++;
            } else if (argviLower.equals("-outvars")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                if (TapEnv.isDistribVersion()) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Command-line option " + argv[i] + " is deprecated: please use -head instead");
                }
                parseDiffOutvars(argv[i + 1]);
                i++;
            } else if (argviLower.equals("-nolib")) {
                nolib = true;
            } else if (argv[i].equals("-ext")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                libraryFileNames = libraryFileNames + ' ' + argv[i + 1];
                i++;
            } else if (argviLower.equals("-extad")) {
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Sorry, option -extAD is now obsolete. Please merge your ADLib into the corresponding GeneralLib and use only option -ext");
                TapEnv.tapenadeExit(1);
            } else if (argv[i].equals("-i2")) {
                TapEnv.get().integerSize = 2;
                TapEnv.addOptionsString(" i2");
            } else if (argv[i].equals("-i4")) {
                TapEnv.get().integerSize = 4;
                TapEnv.addOptionsString(" i4");
            } else if (argv[i].equals("-i8")) {
                TapEnv.get().integerSize = 8;
                TapEnv.addOptionsString(" i8");
            } else if (argv[i].equals("-r4")) {
                TapEnv.get().realSize = 4;
                TapEnv.addOptionsString(" r4");
            } else if (argv[i].equals("-r8")) {
                TapEnv.get().realSize = 8;
                TapEnv.addOptionsString(" r8");
            } else if (argv[i].equals("-dr4")) {
                TapEnv.get().doubleRealSize = 4;
                TapEnv.addOptionsString(" dr4");
            } else if (argv[i].equals("-dr8")) {
                TapEnv.get().doubleRealSize = 8;
                TapEnv.addOptionsString(" dr8");
            } else if (argv[i].equals("-dr16")) {
                TapEnv.get().doubleRealSize = 16;
                TapEnv.addOptionsString(" dr16");
            } else if (argv[i].equals("-p4")) {
                TapEnv.get().pointerSize = 4;
                TapEnv.addOptionsString(" p4");
            } else if (argv[i].equals("-p8")) {
                TapEnv.get().pointerSize = 8;
                TapEnv.addOptionsString(" p8");
            } else if (argviLower.equals("-specializeactivity")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                specializedActivityUnitNames = collectUnitNamesFromString(argv[i]);
                TapEnv.addOptionsString(" specializeActivity(" + argv[i] + ')');
            } else if (argviLower.equals("-defaultnocheckpoint")) {
                TapEnv.get().defaultCheckpointCalls = false ;
                TapEnv.addOptionsString(" defaultnocheckpoint") ;
                if (TapEnv.get().staticTape) {
                    TapEnv.get().staticTape = false;
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Command-line option -defaultnocheckpoint deactivates option statictape");
                }
            } else if (argviLower.equals("-split")
                    || argviLower.equals("-nocheckpoint")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                splittedUnitNames = collectUnitNamesFromString(argv[i]);
                TapEnv.addOptionsString(" split(" + argv[i] + ')');
                if (TapEnv.get().staticTape) {
                    TapEnv.get().staticTape = false;
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Command-line option -nocheckpoint deactivates option statictape");
                }
            } else if (argviLower.equals("-joint")
                    || argviLower.equals("-checkpoint")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                jointedUnitNames = collectUnitNamesFromString(argv[i]);
                TapEnv.addOptionsString(" joint(" + argv[i] + ')');
            } else if (argviLower.equals("-duplicable")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setDuplicableUnitNames(collectUnitNamesFromString(argv[i]));
                TapEnv.addOptionsString(" duplicable(" + argv[i] + ')');
            } else if (argviLower.equals("-numberpushpops")) {
                TapEnv.get().numberPushPops = true;
                TapEnv.addOptionsString(" numberpushpops");
            } else if (argviLower.equals("-fixinterface")) {
                TapEnv.get().fixInterface = true;
                TapEnv.addOptionsString(" fixinterface");
            } else if (argviLower.equals("-differentiateintegers")) {
                diffKind = SymbolTableConstants.ALLKIND;
                TapEnv.setDiffKind(diffKind);
                TapEnv.addOptionsString(" diffIntegers");
            } else if (argviLower.equals("-debugactivity")) {
                TapEnv.setDebugActivity(true);
                TapEnv.addOptionsString(" debugActivity");
            } else if (argviLower.equals("-dump") || argviLower.equals("-dumpfile")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                debug = true;
                i++;
                dumpPathFile = argv[i];
                TapEnv.get().dumps = dumpPathFile != null;
            } else if (argviLower.equals("-dumpunit")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.get().dumpUnitNames = collectUnitNamesFromString(argv[i]);
            } else if (argviLower.equals("-dumpcallgraph")) {
                TapEnv.get().dumpCallGraph = true;
            } else if (argviLower.equals("-dumpsymboltables")) {
                TapEnv.get().dumpSymbolTables = true;
            } else if (argviLower.equals("-dumpflowgraphs")) {
                TapEnv.get().dumpFlowGraphs = true;
            } else if (argviLower.equals("-dumpzones")) {
                TapEnv.get().dumpZones = true;
            } else if (argviLower.equals("-dumpdataflow")) {
                TapEnv.get().dumpDataFlow = true;
            } else if (argviLower.equals("-dumpdiffsymboltables")) {
                TapEnv.get().dumpDiffSymbolTables = true;
            } else if (argviLower.equals("-dumpdiffflowgraphs")) {
                TapEnv.get().dumpDiffFlowGraphs = true;
            } else if (argviLower.equals("-dumpinout")) {
                TapEnv.get().dumpInOut = true;
            } else if (argviLower.equals("-dumppointers")) {
                TapEnv.get().dumpPointers = true;
            } else if (argviLower.equals("-dumpdeps")) {
                TapEnv.get().dumpDeps = true;
            } else if (argviLower.equals("-profile")) {
                TapEnv.get().profile = true;
                TapEnv.addOptionsString(" profile");
            } else if (argviLower.equals("-tracelevel") || argviLower.equals("-debug")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                try {
                    TapEnv.setTraceLevel(Integer.parseInt(argv[i + 1]));
                } catch (final NumberFormatException e) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Incorrect trace level value");
                }
                debug = true;
                i++;
            } else if (argviLower.equals("-msglevel")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                try {
                    TapEnv.setMsgLevel(Integer.parseInt(argv[i + 1]));
                } catch (final NumberFormatException e) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Incorrect message level value");
                }
                debug = true;
                i++;
            } else if (argviLower.equals("-noisize")) {
                TapEnv.get().noisize = true;
                TapEnv.addOptionsString(" noISIZE");
            } else if (argviLower.equals("-noisize77")) {
                TapEnv.get().noisize77 = true;
                TapEnv.get().noisize = true;
                TapEnv.addOptionsString(" noISIZE77");
            } else if (argviLower.equals("-standalonediff")) {
                TapEnv.get().standaloneDiff = true;
                TapEnv.addOptionsString(" standaloneDiff");
            } else if (argviLower.equals("-viewcallgraph")) {
                viewCallGraph = true;
            } else if (argviLower.equals("-associationbyaddress")
                       || argviLower.equals("-assocbyaddress")
                       || argviLower.equals("-byaddress")
                       || argv[i].equals("-aa")) {
                TapEnv.setAssociationByAddress(true);
                TapEnv.addOptionsString(" associationByAddress");
            } else if (argviLower.equals("-mixedlanguage")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                parseMixedLanguage(argv[i + 1]);
                i++;
            } else if (argviLower.equals("-optim")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                seenExplicitDiffLivenessOption = analyzeOptimOptions(argv[i], seenExplicitDiffLivenessOption);
            } else if (argviLower.equals("-nooptim")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                seenExplicitDiffLivenessOption = analyzeNoOptimOptions(argv[i], seenExplicitDiffLivenessOption);
            } else if (argviLower.equals("-stripprimalevenifimportsactive")) {
                // Very special option (not recommended) to force stripping primal code even if it imports active module:
                TapEnv.get().stripPrimalEvenIfImportsActive = true;
                TapEnv.addOptionsString(" stripPrimalEvenIfImportsActive");
            } else if (argviLower.equals("-msginfile")) {
                showMessagesInFile = true;
                TapEnv.addOptionsString(" messagesInFile");
            } else if (argviLower.equals("-complexstep")) {
                TapEnv.setAssociationByAddress(true);
                TapEnv.get().complexStep = true ;
                TapEnv.addOptionsString(" complexStep (implies -d)");
                TapEnv.setMustTangent(true);
                TapEnv.setModeTangent();
            } else if (argviLower.equals("-debugtgt")) {
                TapEnv.get().mergeDiffInstructions = false;
                TapEnv.setDebugAdMode(TapEnv.TGT_DEBUG);
                TapEnv.addOptionsString(" debugTangent");
                if (TapEnv.get().diffReplica>1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Debug modes force diffReplica==1") ;
                    TapEnv.get().diffReplica = 1 ;
                }
                // debugTangent => noSpareInit :
                TapEnv.setSpareDiffReinitializations(false);
                TapEnv.addOptionsString(" no!SpareInits");
                // debugTangent => fixinterface
                TapEnv.get().fixInterface = true;
                TapEnv.addOptionsString(" fixinterface");
            } else if (argviLower.equals("-debugadj")) {
                TapEnv.get().mergeDiffInstructions = false;
                TapEnv.setDebugAdMode(TapEnv.ADJ_DEBUG);
                TapEnv.addOptionsString(" debugAdjoint");
                if (TapEnv.get().diffReplica>1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Debug modes force diffReplica==1") ;
                    TapEnv.get().diffReplica = 1 ;
                }
                // debugAdjoint => noSpareInit :
                TapEnv.setSpareDiffReinitializations(false);
                TapEnv.addOptionsString(" no!SpareInits");
                // debugAdjoint => fixinterface
                TapEnv.get().fixInterface = true;
                TapEnv.addOptionsString(" fixinterface");
            } else if (argviLower.equals("-debugpassives")) {
                TapEnv.get().debugPassives = true;
                TapEnv.addOptionsString(" debugPassives");
                // debugPassives =>  noUsefulness
                TapEnv.setDoUsefulness(false);
                TapEnv.addOptionsString(" no!Usefulness");
            } else if (argv[i].equals("-html")) {
                outputFormats = TapIntList.add(outputFormats, Tapenade.HTML);
                splitOutputFiles = false;
            } else if (argviLower.equals("-display")) {
                outputFormats = TapIntList.add(outputFormats, Tapenade.HTML);
                splitOutputFiles = false;
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    tapenadeHtmlDisplay = "tapenadediff.html";
                } else {
                    i++;
                    TapEnv.printlnOnTrace("-display " + argv[i]);
                    tapenadeHtmlDisplay = argv[i];
                }
            } else if (argviLower.equals("-version") || argviLower.equals("--version")) {
                TapEnv.printlnOnTrace("@@ JAVA_HOME=" + JAVA_HOME);
                TapEnv.printlnOnTrace("Revision: " + Version.GIT_LONG_HASH);
                if (!"".equals(Version.GIT_TAG)) TapEnv.printlnOnTrace("Tag:      " + Version.GIT_TAG);
                if (!TapEnv.optionsString().isEmpty()) {
                    TapEnv.printlnOnTrace(" Options: " + TapEnv.optionsString());
                }
                TapEnv.tapenadeExit(0);
            } else if (argviLower.equals("-versiontapenade")) {
                TapEnv.tapenadeExit(0);
            } else if (argviLower.equals("-java")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.printlnOnTrace("-java " + argv[i]);
            } else if (argv[i].equals("-h") || argv[i].equals("-help")
                    || argv[i].equals("-?")) {
                Tapenade.help();
                TapEnv.tapenadeExit(0);
            } else if (argv[i].equals("-H") || argv[i].equals("-HELP")) {
                Tapenade.hiddenHelp();
                TapEnv.tapenadeExit(0);
            } else if (argviLower.equals("-traceparser")) {
                TapEnv.get().traceParser = true;
            } else if (argviLower.equals("-traceinputil")) {
                TapEnv.setTraceInputIL(true);
            } else if (argviLower.equals("-traceflowgraphbuild")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceFlowGraphBuild(collectUnitNamesFromString(argv[i]));
            } else if (argviLower.equals("-tracetypecheck")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.get().traceTypeCheck = collectUnitNamesFromString(argv[i]);
            } else if (argviLower.equals("-traceinline")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.get().traceInline = collectUnitNamesFromString(argv[i]);
            } else if (argviLower.equals("-tracepointer")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTracePointer(collectUnitNamesFromString(argv[i]));
            } else if (argviLower.equals("-traceinout")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceInOut(collectUnitNamesFromString(argv[i]));
            } else if (argviLower.equals("-tracedeps")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceDeps(collectUnitNamesFromString(argv[i]));
            } else if (argviLower.equals("-traceaddeps")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceADDeps(collectUnitNamesFromString(argv[i]));
            } else if (argviLower.equals("-traceactivity")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceActivity(collectUnitNamesFromString(argv[i]));
                TapEnv.setActiveMark("@");
            } else if (argviLower.equals("-tracereqexplicit") || argviLower.equals("-tracereqx")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceReqExplicit(collectUnitNamesFromString(argv[i]));
                TapEnv.setPointerActiveMark("#");
            } else if (argviLower.equals("-tracediffliveness")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceDiffLiveness(collectUnitNamesFromString(argv[i]));
            } else if (argviLower.equals("-tracetbr")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceTBR(collectUnitNamesFromString(argv[i]));
            } else if (argviLower.equals("-tracemultithread")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.setTraceMultithread(collectUnitNamesFromString(argv[i]));
            } else if (argviLower.equals("-tracedifferentiation")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.get().traceDifferentiationUnitNames = collectUnitNamesFromString(argv[i]);
            } else if (argviLower.equals("-tracetreegen")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.get().traceTreeGenUnitNames = collectUnitNamesFromString(argv[i]);
            } else if (argviLower.equals("-traceblock")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.get().traceBlockRk = Integer.parseInt(argv[i]);
            } else if (argviLower.equals("-tracecalleeunit") || argviLower.equals("-tracecalledunit") || argviLower.equals("-tracetounit")) {
                if (i >= argv.length - 1) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Missing command-line argument after: " + argv[i]);
                    TapEnv.tapenadeExit(1);
                }
                i++;
                TapEnv.get().traceCalledUnitName = argv[i];
            } else {
                // SOURCE FILE NAMES:
                final String fileName = argv[i];
                if (!new File(fileName).exists()) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "File not found: " + fileName);
                } else if (getLanguageOfFile(fileName) == TapEnv.INCLUDE) {
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, fileName + ", include skipped in command line");
                } else {
                    listFileNames = addFileToBeDifferentiated(fileName, TapEnv.UNDEFINED, listFileNames);
                }
            }
            i++;
        }

        if (TapEnv.modeIsTangent()) {
            TapEnv.setMustTangent(true);
        }

        // Temporary until we decide that both tangent
        // and adjoint have diffLiveness analysis and slicing.
        if (TapEnv.modeIsTangent() && !TapEnv.mustAdjoint() && !seenExplicitDiffLivenessOption) {
            TapEnv.setRemoveDeadPrimal(false);
            TapEnv.setRemoveDeadControl(false);
        }

        // les options de dump peuvent apparaitre dans le desordre,
        // donc on les termine maintenant qu'on les a toutes lues:
        if (TapEnv.get().dumps) {
            dumpFileName = TapEnv.stripPath(dumpPathFile);
            if (dumpFileName != null && !dumpFileName.equals(dumpPathFile)) {
                TapEnv.commandWarning(14, "Parent directory ignored in " + dumpPathFile);
            }
            try {
                dumpOutputStream = java.nio.file.Files.newOutputStream(
                        java.nio.file.Paths.get(TapEnv.get().outputDirectory+dumpFileName));
            } catch (IOException e) {
                TapEnv.systemWarning(TapEnv.MSG_ALWAYS, e.toString());
                dumpOutputStream = null;
            }
            TapEnv.get().dumpOutputStream = dumpOutputStream;
            // Default dump mode (i.e. when the command line only says "-dump <fileName>" ) :
            if (!TapEnv.get().dumpCallGraph && !TapEnv.get().dumpZones && !TapEnv.get().dumpDataFlow &&
                    !TapEnv.get().dumpSymbolTables && !TapEnv.get().dumpDiffSymbolTables &&
                    !TapEnv.get().dumpFlowGraphs && !TapEnv.get().dumpDiffFlowGraphs && TapEnv.get().dumpUnitNames == null) {
                //TapEnv.get().dumpCallGraph = true ;
                TapEnv.get().dumpZones = true;
                TapEnv.get().dumpDataFlow = true;
                TapEnv.get().dumpSymbolTables = true;
                TapEnv.get().dumpFlowGraphs = true;
                TapEnv.get().dumpDiffSymbolTables = true;
                TapEnv.get().dumpDiffFlowGraphs = true;
            }
        }

        // copy the collected filenames into arrayFileNames:
        if (listFileNames == null) {
            TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "No input files");
            TapEnv.tapenadeExit(1);
        }

        inputFiles = TapList.nreverse(listFileNames);
        for (TapPair<String, Integer> inputFile : inputFiles) {
            if (inputFile.second == TapEnv.UNDEFINED) {
                inputFile.second = getLanguageOfFile(inputFile.first);
            }
        }

        inputFileName = inputFiles.head.first;

        TapEnv.setIncludeDirs(hdIncludeDirs.tail);

        final String stdLibraryDirectory = "lib" + File.separator;
        if (nolib) {
            stdGeneralLibraryDirectory = null;
        } else {
            stdGeneralLibraryDirectory = stdLibraryDirectory;
        }
        TapEnv.get().stdLibraryDirectory = stdLibraryDirectory;
        if (tgtFuncSuffix == null) {
            tgtFuncSuffix = diffFuncSuffix;
        }
        if (tgtVarSuffix == null) {
            tgtVarSuffix = diffVarSuffix;
        }
        if (adjFuncSuffix == null) {
            adjFuncSuffix = diffFuncSuffix;
        }
        if (adjVarSuffix == null) {
            adjVarSuffix = diffVarSuffix;
        }
        if (tgtModuleSuffix == null) {
            tgtModuleSuffix = moduleSuffix;
        }
        if (adjModuleSuffix == null) {
            adjModuleSuffix = moduleSuffix;
        }
        prepareAllSuffixes(tgtFuncSuffix, tgtVarSuffix, adjFuncSuffix, adjVarSuffix,
                tgtModuleSuffix, adjModuleSuffix, moduleSuffix, copiedUnitSuffix, assocByAddressSuffix);
        absDirectoryHtmlGen = TapEnv.get().outputDirectory+directoryHtmlGen;
    }

    private TapList<TapPair<String, Integer>> addFileToBeDifferentiated(String filename, int fileLanguage, TapList<TapPair<String, Integer>> listFilenames){
        
        TapList<TapPair<String, Integer>> list = listFilenames; 
        TapList<TapPair<String, Integer>> previous = null; 
        boolean found = false;
        while ((list != null) && (!found)) {
            found = list.head.first.equalsIgnoreCase(filename);
            previous = list; 
            list = list.tail;
        }
        if (found) {
            int currentLanguage = previous.head.second;
            if (currentLanguage == fileLanguage) {
                // TapEnv.commandWarning(TapEnv.MSG_TRACE, "File " + filename + " appearing twice with consistant forced languages");
            } else if (fileLanguage == TapEnv.UNDEFINED) {
                // Inconsistent languages. Choice: keep incoming language unless it is undefined then keep previous. 
                // TapEnv.commandWarning(TapEnv.MSG_TRACE, "File " + filename + " already has a forced language. Trying to overwrite it with UNDEFINED is aborted. ");
            } else {
                if (currentLanguage != TapEnv.UNDEFINED){
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "File " + filename + " already exists. Overwriting previous language (" + currentLanguage + ") with newly specified one (" + fileLanguage + ")");
                }
                previous.head.second = fileLanguage;
            }
        } else {
            // This file is being added for the first time. 
            listFilenames = new TapList<TapPair<String, Integer>>(new TapPair<String, Integer>(filename, fileLanguage), listFilenames);
        }
        return listFilenames;
        
    }

    /**
     * Analyze -optim options.
     *
     * @param str                            option.
     * @param seenExplicitDiffLivenessOption cf -nooptim diffliveness
     * @return seenExplicitDiffLivenessOption
     */
    private boolean analyzeOptimOptions(String str, boolean seenExplicitDiffLivenessOption) {
        String oneOptim;
        String oneoptimLower;
        final StringTokenizer token = new StringTokenizer(str, " ,");
        while (token.hasMoreElements()) {
            oneOptim = token.nextToken();
            oneoptimLower = oneOptim.toLowerCase();
            switch (oneoptimLower) {
                case "diffliveness":
                    seenExplicitDiffLivenessOption = true;
                    TapEnv.setRemoveDeadPrimal(true);
                    TapEnv.setRemoveDeadControl(true);
                    TapEnv.addOptionsString(" with!SliceDeadControl");
                    TapEnv.addOptionsString(" with!SliceDeadInstrs");
                    break;
                case "stripprimalcode":
                    TapEnv.get().stripPrimalCode = true;
                    TapEnv.addOptionsString(" with!StripPrimalCode");
                    break;
                case "spareinit":
                    TapEnv.setSpareDiffReinitializations(true);
                    TapEnv.addOptionsString(" with!SpareInits");
                    break;
                case "statictape":
                    TapEnv.get().staticTape = true;
                    if (splittedUnitNames != null || !TapEnv.get().defaultCheckpointCalls) {
                        TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Option statictape deactivated on units with nocheckpoint option");
                    }
                    TapEnv.addOptionsString(" with!StaticTaping");
                    break;
                default:
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Unknown command-line option: -optim " + oneOptim);
                    break;
            }
        }
        return seenExplicitDiffLivenessOption;
    }

    /**
     * Analyze -nooptim options.
     *
     * @param str                            option.
     * @param seenExplicitDiffLivenessOption cf -optim diffliveness
     * @return seenExplicitDiffLivenessOption
     */
    private boolean analyzeNoOptimOptions(String str, boolean seenExplicitDiffLivenessOption) {
        String oneNooptim;
        String onenooptimLower;
        final StringTokenizer token = new StringTokenizer(str, " ,");
        while (token.hasMoreElements()) {
            oneNooptim = token.nextToken();
            onenooptimLower = oneNooptim.toLowerCase();
            switch (onenooptimLower) {
                case "activity":
                    TapEnv.setDoActivity(false);
                    TapEnv.addOptionsString(" no!Activity");
                    // no activity => all modules get differentiated
                    TapEnv.get().stripPrimalModules = false;
                    // no activity => Don't condense diff instructions:
                    TapEnv.get().mergeDiffInstructions = false;
                    TapEnv.addOptionsString(" =>no!MergeDiffInstrs");
                    break;
                case "usefulness":
                    TapEnv.setDoUsefulness(false);
                    TapEnv.addOptionsString(" no!Usefulness");
                    break;
                case "diffarguments":
                    TapEnv.get().spareNoDiffArgs = false;
                    TapEnv.addOptionsString(" no!SpareDiffArgs");
                    break;
                case "spareinit":
                    TapEnv.setSpareDiffReinitializations(false);
                    TapEnv.addOptionsString(" no!SpareInits");
                    break;
                case "splitdiff":
                    TapEnv.get().splitDiffExpressions = false;
                    TapEnv.addOptionsString(" no!SplitDiffExprs");
                    break;
                case "mergediff":
                    TapEnv.get().mergeDiffInstructions = false;
                    TapEnv.addOptionsString(" no!MergeDiffInstrs");
                    break;
                case "saveonlyused":
                    TapEnv.get().dontSaveUnused = false;
                    TapEnv.addOptionsString(" no!SaveOnlyUsed");
                    TapEnv.setDoTBR(false);
                    TapEnv.addOptionsString(" no!SaveOnlyTBR");
                    break;
                case "tbr":
                    TapEnv.setDoTBR(false);
                    TapEnv.addOptionsString(" no!SaveOnlyTBR");
                    break;
                case "snapshot":
                    //TapEnv.get().optSnapshots = false;
                    TapEnv.addOptionsString(" no!ReducedSnapshots");
                    break;
                case "adjointliveness":
                case "diffliveness":
                    seenExplicitDiffLivenessOption = true;
                    TapEnv.setRemoveDeadPrimal(false);
                    TapEnv.setRemoveDeadControl(false);
                    TapEnv.setDoRecompute(false);
                    TapEnv.addOptionsString(" no!SliceDeadControl");
                    TapEnv.addOptionsString(" no!SliceDeadInstrs");
                    TapEnv.addOptionsString(" no!tryRecomputations");
                    break;
                case "deadcontrol":
                    seenExplicitDiffLivenessOption = true;
                    TapEnv.setRemoveDeadControl(false);
                    TapEnv.addOptionsString(" no!SliceDeadControl");
                    break;
                case "dc":
                    seenExplicitDiffLivenessOption = true;
                    TapEnv.setRemoveDeadControl(false);
                    TapEnv.addOptionsString(" no!SliceDeadControl");
                    TapEnv.setRemoveDeadPrimal(false);
                    TapEnv.addOptionsString(" no!SliceDeadInstrs");
                    break;
                case "recomputeintermediates":
                    TapEnv.setDoRecompute(false);
                    TapEnv.addOptionsString(" no!tryRecomputations");
                    break;
                case "difftypes":
                    TapEnv.get().stripDiffTypes = false;
                    TapEnv.addOptionsString(" no!StripDiffTypes");
                    break;
                case "stripprimalcode":
                    TapEnv.get().stripPrimalCode = false;
                    TapEnv.addOptionsString(" no!StripPrimalCode");
                    break;
                case "diffmodules":
                case "stripprimalmodules":
                    TapEnv.get().stripPrimalModules = false;
                    break;
                case "refineadmm":
                    TapEnv.get().refineADMM = false;
                    TapEnv.addOptionsString(" no!refineADMM");
                    break;
                case "dataflow":
                    TapEnv.setDoActivity(false);
                    TapEnv.addOptionsString(" no!Activity");
                    TapEnv.get().stripPrimalModules = false;
                    TapEnv.get().mergeDiffInstructions = false;
                    TapEnv.addOptionsString(" =>no!MergeDiffInstrs");
                    TapEnv.setSpareDiffReinitializations(false);
                    TapEnv.addOptionsString(" no!SpareInits");
                    TapEnv.setRemoveDeadPrimal(false);
                    TapEnv.setRemoveDeadControl(false);
                    TapEnv.setDoRecompute(false);
                    TapEnv.addOptionsString(" no!SliceDeadControl");
                    TapEnv.addOptionsString(" no!SliceDeadInstrs");
                    TapEnv.addOptionsString(" no!tryRecomputations");
                    seenExplicitDiffLivenessOption = true;
                    TapEnv.setRemoveDeadControl(false);
                    TapEnv.addOptionsString(" no!SliceDeadControl");
                    TapEnv.setDoRecompute(false);
                    TapEnv.addOptionsString(" no!tryRecomputations");
                    TapEnv.setDoTBR(false);
                    TapEnv.addOptionsString(" no!SaveOnlyTBR");
                    TapEnv.get().dontSaveUnused = false;
                    TapEnv.addOptionsString(" no!SaveOnlyUsed");
                    TapEnv.setDoTBR(false);
                    TapEnv.addOptionsString(" no!SaveOnlyTBR");
                    TapEnv.addOptionsString(" no!ReducedSnapshots");
                    break;
                case "everyoptim":
                    TapEnv.setDoActivity(false);
                    TapEnv.addOptionsString(" no!Activity");
                    TapEnv.get().stripPrimalModules = false;
                    TapEnv.get().spareNoDiffArgs = false;
                    TapEnv.addOptionsString(" no!SpareDiffArgs");
                    TapEnv.setSpareDiffReinitializations(false);
                    TapEnv.addOptionsString(" no!SpareInits");
                    TapEnv.get().splitDiffExpressions = false;
                    TapEnv.addOptionsString(" no!SplitDiffExprs");
                    TapEnv.get().mergeDiffInstructions = false;
                    TapEnv.addOptionsString(" no!MergeDiffInstrs");
                    TapEnv.get().dontSaveUnused = false;
                    TapEnv.addOptionsString(" no!SaveOnlyUsed");
                    TapEnv.setDoTBR(false);
                    TapEnv.addOptionsString(" no!SaveOnlyTBR");
                    TapEnv.addOptionsString(" no!ReducedSnapshots");
                    seenExplicitDiffLivenessOption = true;
                    TapEnv.setRemoveDeadControl(false);
                    TapEnv.addOptionsString(" no!SliceDeadControl");
                    TapEnv.setRemoveDeadPrimal(false);
                    TapEnv.addOptionsString(" no!SliceDeadInstrs");
                    TapEnv.setDoRecompute(false);
                    TapEnv.addOptionsString(" no!tryRecomputations");
                    TapEnv.get().stripDiffTypes = false;
                    TapEnv.addOptionsString(" no!StripDiffTypes");
                    break;
                default:
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Unknown command-line option: -nooptim " + oneNooptim);
                    break;
            }
        }
        return seenExplicitDiffLivenessOption;
    }

    /**
     * The input Strings are the USER-GIVEN suffixes. If any of them is null or
     * empty, it will be replaced by the TAPENADE-GIVEN default which is located now
     * (i.e. upon entry into this method prepareAllSuffixes()) in the suffixes[][] array.
     * See the initialization of suffixes[][] upon its declaration.
     * Upon exit from this prepareAllSuffixes(), suffixes[][] contains its
     * final values that incorporate the USER-GIVEN values.
     */
    private void prepareAllSuffixes(String tgtFuncSuffix, String tgtVarSuffix,
                                    String adjFuncSuffix, String adjVarSuffix,
                                    final String tgtModuleSuffix, final String adjModuleSuffix,
                                    String moduleSuffix,
                                    String copiedUnitSuffix, String assocByAddressSuffix) {
        if (TapEnv.associationByAddress()) {
            if (assocByAddressSuffix == null) {
                assocByAddressSuffix = Tapenade.DEFAULT_ASSOC_BY_ADDRESS_FUNC_SUFFIX;
            }
        }
        if (TapEnv.modeIsNoDiff()) {
            tgtVarSuffix = "";
            tgtFuncSuffix = "";
        } else if (TapEnv.modeIsOverloading()) {
            if (tgtVarSuffix == null || tgtVarSuffix.isEmpty()) {
                tgtVarSuffix = Tapenade.DEFAULT_ADOLC_VAR_SUFFIX;
            }
            if (tgtFuncSuffix == null || tgtFuncSuffix.isEmpty()) {
                tgtFuncSuffix = Tapenade.DEFAULT_ADOLC_FUNC_SUFFIX;
            }
        } else {
            if (tgtVarSuffix == null || tgtVarSuffix.isEmpty()) {
                tgtVarSuffix = suffixes[Tapenade.TANGENT][Tapenade.VARIABLE];
            }
            if (tgtFuncSuffix == null || tgtFuncSuffix.isEmpty()) {
                tgtFuncSuffix = suffixes[Tapenade.TANGENT][Tapenade.PROCEDURE];
            }
            if (adjVarSuffix == null || adjVarSuffix.isEmpty()) {
                adjVarSuffix = suffixes[Tapenade.ADJOINT][Tapenade.VARIABLE];
            }
            if (adjFuncSuffix == null || adjFuncSuffix.isEmpty()) {
                adjFuncSuffix = suffixes[Tapenade.ADJOINT][Tapenade.PROCEDURE];
            }
            if (TapEnv.associationByAddress() && !TapEnv.get().complexStep) {
                if (!assocByAddressSuffix.isEmpty()) {
                    //Warning: this looses the user-given [tgt/adj]FuncSuffix'es :
                    tgtFuncSuffix = "_" + assocByAddressSuffix + tgtVarSuffix;
                    adjFuncSuffix = "_" + assocByAddressSuffix + adjVarSuffix;
                    copiedUnitSuffix = "_c" + assocByAddressSuffix + (TapEnv.mustTangent() ? "d" : "b");
                }
            }
        }
        if (TapEnv.get().multiDirDiffMode) {
            tgtFuncSuffix =
                    TapEnv.extendStringWithSuffix(tgtFuncSuffix, TapEnv.get().multiFuncSuffix);
            adjFuncSuffix =
                    TapEnv.extendStringWithSuffix(adjFuncSuffix, TapEnv.get().multiFuncSuffix);
        }
        if (copiedUnitSuffix == null) {
            copiedUnitSuffix = Tapenade.DEFAULT_COPIED_UNIT_SUFFIX;
        }
        if (moduleSuffix == null) {
            moduleSuffix = Tapenade.DEFAULT_MODULE_UNIT_SUFFIX;
        }

        suffixes[Tapenade.ORIGCOPY][Tapenade.PROCEDURE] = copiedUnitSuffix;
        suffixes[Tapenade.TANGENT][Tapenade.VARIABLE] = tgtVarSuffix;
        suffixes[Tapenade.TANGENT][Tapenade.PROCEDURE] = tgtFuncSuffix;
        suffixes[Tapenade.TANGENT][Tapenade.MODULE] = tgtModuleSuffix == null ? Tapenade.DEFAULT_MODULE_UNIT_SUFFIX : tgtModuleSuffix;
        suffixes[Tapenade.TANGENT][Tapenade.TYPE] = moduleSuffix;
        suffixes[Tapenade.ADJOINT][Tapenade.VARIABLE] = adjVarSuffix;
        suffixes[Tapenade.ADJOINT][Tapenade.PROCEDURE] = adjFuncSuffix;
        suffixes[Tapenade.ADJOINT][Tapenade.MODULE] = adjModuleSuffix == null ? Tapenade.DEFAULT_MODULE_UNIT_SUFFIX : adjModuleSuffix;
        suffixes[Tapenade.ADJOINT][Tapenade.TYPE] = moduleSuffix;
        suffixes[Tapenade.ADJOINT_FWD][Tapenade.VARIABLE] = adjVarSuffix;
        suffixes[Tapenade.ADJOINT_FWD][Tapenade.MODULE] = adjModuleSuffix == null ? Tapenade.DEFAULT_MODULE_UNIT_SUFFIX : adjModuleSuffix;
        suffixes[Tapenade.ADJOINT_FWD][Tapenade.TYPE] = moduleSuffix;
        suffixes[Tapenade.ADJOINT_BWD][Tapenade.VARIABLE] = adjVarSuffix;
        suffixes[Tapenade.ADJOINT_BWD][Tapenade.MODULE] = adjModuleSuffix == null ? Tapenade.DEFAULT_MODULE_UNIT_SUFFIX : adjModuleSuffix;
        suffixes[Tapenade.ADJOINT_BWD][Tapenade.TYPE] = moduleSuffix;
        //Because a null copiedUnitSuffix will mean "override original procedure names" :
        if (suffixes[Tapenade.ORIGCOPY][Tapenade.PROCEDURE].isEmpty()) {
            suffixes[Tapenade.ORIGCOPY][Tapenade.PROCEDURE] = ""; // TODO a modifier???
        }

        TapEnv.get().diffFileSuffix =
            (TapEnv.mustTangent()
             ? (TapEnv.mustAdjoint()
                ? (TapEnv.get().multiDirDiffMode
                   ? TapEnv.extendStringWithSuffix("_db", TapEnv.get().multiFuncSuffix)
                   : "_db")
                : tgtFuncSuffix)
             : (TapEnv.mustAdjoint()
                ? adjFuncSuffix
                : (copiedUnitSuffix.isEmpty()
                   ? Tapenade.DEFAULT_COPIED_UNIT_SUFFIX
                   : copiedUnitSuffix))) ;

        //THE FOLLOWING OVERRIDES THE BETTER CHOICES ABOVE, ONLY TO REDUCE THE NUMBER OF DIFFS IN runAD !!:
        //Still in doubt about the correct way to name differentiated modules:
        if (!TapEnv.mustTangent() && !TapEnv.mustAdjoint()) {
            if (TapEnv.modeIsOverloading()) {
                TapEnv.get().diffFileSuffix = tgtFuncSuffix;
            } else {
                TapEnv.get().diffFileSuffix = TapEnv.get().preprocessFileSuffix;
            }
        }

        suffixes[Tapenade.DIFFERENTIATED][Tapenade.MODULE] = TapEnv.mustTangent()
                ? TapEnv.mustAdjoint() ? moduleSuffix : suffixes[Tapenade.TANGENT][Tapenade.MODULE]
                : TapEnv.mustAdjoint() ? suffixes[Tapenade.ADJOINT][Tapenade.MODULE] : moduleSuffix;

        if (tgtModuleSuffix != null && !tgtModuleSuffix.equals(suffixes[Tapenade.ADJOINT][Tapenade.MODULE])
                || adjModuleSuffix != null && !adjModuleSuffix.equals(suffixes[Tapenade.TANGENT][Tapenade.MODULE])) {
            // old _d et _b pour validations:
            suffixes[Tapenade.ORIGCOPY][Tapenade.PROCEDURE] = "_c" + (TapEnv.mustTangent() ? "d" : "b");
        }
        if (TapEnv.get().multiDirDiffMode && (TapEnv.mustTangent() || TapEnv.mustAdjoint())
                && !(Tapenade.DEFAULT_COPIED_UNIT_SUFFIX.equals(suffixes[Tapenade.DIFFERENTIATED][Tapenade.MODULE])
                || Tapenade.DEFAULT_COPIED_UNIT_SUFFIX.equals(suffixes[Tapenade.ADJOINT][Tapenade.MODULE]))) {
            suffixes[Tapenade.TANGENT][Tapenade.MODULE] =
                    TapEnv.extendStringWithSuffix(suffixes[Tapenade.TANGENT][Tapenade.MODULE],
                            TapEnv.get().multiFuncSuffix);
            suffixes[Tapenade.ADJOINT][Tapenade.MODULE] =
                    TapEnv.extendStringWithSuffix(suffixes[Tapenade.ADJOINT][Tapenade.MODULE],
                            TapEnv.get().multiFuncSuffix);
        }
        suffixes[Tapenade.TANGENT][Tapenade.TYPE] = suffixes[Tapenade.TANGENT][Tapenade.MODULE];
        suffixes[Tapenade.ADJOINT][Tapenade.TYPE] = suffixes[Tapenade.ADJOINT][Tapenade.MODULE];
        suffixes[Tapenade.ADJOINT_BWD][Tapenade.TYPE] = suffixes[Tapenade.ADJOINT_BWD][Tapenade.MODULE];

        // Special adaptation of suffixes for the complex-step method:
        if (TapEnv.get().complexStep) {
//             tgtFuncSuffix = "_" + tgtVarSuffix;
            suffixes[Tapenade.ORIGCOPY][Tapenade.PROCEDURE] = copiedUnitSuffix ;
            TapEnv.get().diffFileSuffix = tgtFuncSuffix ;
            suffixes[Tapenade.TANGENT][Tapenade.PROCEDURE] = tgtFuncSuffix;
            TapEnv.get().assocAddressValueSuffix = "re" ;
            TapEnv.get().assocAddressDiffSuffix = "im" ;
        }
    }

    private TapList<String> collectUnitNamesFromString(final String str) {
        final StringTokenizer tokenizer = new StringTokenizer(str, " ,");
        TapList<String> result = null;
        String name;
        while (tokenizer.hasMoreElements()) {
            name = tokenizer.nextToken();
            result = new TapList<>(name, result);
        }
        return result;
    }

    /**
     * Parses the argument of the option "-root".
     * This argument can be a simple function name,
     * or in general a nonempty list (=&gt; between (double-)quotes) of
     * elements of the form Identifier(vars1)[/\](vars2).
     */
    private void parseDiffRoots(final String cmdStr) {
        String diffRootFunc = null;
        String diffRootCasN = null;
        String diffRootIndeps = null;
        String diffRootDeps = null;
        final StrIndex strIndex = new StrIndex(cmdStr);
        int state = Tapenade.BEFORE_FUNC;
        final StrIndex toPiece = new StrIndex("");
        int pieceKind = getNextDiffRootPiece(strIndex, toPiece);
        // parser finite state automaton :
        while (pieceKind != Tapenade.NOTHING) {
            switch (state) {
                case Tapenade.BEFORE_FUNC:
                    if (pieceKind == Tapenade.FUNC) {
                        diffRootFunc = toPiece.str;
                        state = Tapenade.AFTER_FUNC;
                    } else {
                        TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Wrong argument syntax. Ignored: " + toPiece.str);
                    }
                    break;
                case Tapenade.AFTER_FUNC:
                    switch (pieceKind) {
                        case Tapenade.FUNC:
                            flushDiffRoot(diffRootFunc, null, null, null);
                            diffRootFunc = toPiece.str;
                            break;
                        case Tapenade.CASN:
                            diffRootCasN = toPiece.str;
                            state = Tapenade.AFTER_FUNC_CASN;
                            break;
                        case Tapenade.VARS:
                            diffRootDeps = toPiece.str;
                            state = Tapenade.AFTER_VARS;
                            break;
                        case Tapenade.ONTK:
                            state = Tapenade.AFTER_ONTK;
                            break;
                        case Tapenade.TOTK:
                            state = Tapenade.AFTER_TOTK;
                            break;
                        default:
                            TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Wrong argument syntax. Ignored: " + toPiece.str);
                    }
                    break;
                case Tapenade.AFTER_FUNC_CASN:
                    switch (pieceKind) {
                        case Tapenade.FUNC:
                            flushDiffRoot(diffRootFunc, diffRootCasN, null, null);
                            diffRootCasN = null;
                            diffRootFunc = toPiece.str;
                            break;
                        case Tapenade.VARS:
                            diffRootDeps = toPiece.str;
                            state = Tapenade.AFTER_VARS;
                            break;
                        case Tapenade.ONTK:
                            state = Tapenade.AFTER_ONTK;
                            break;
                        case Tapenade.TOTK:
                            state = Tapenade.AFTER_TOTK;
                            break;
                        default:
                            TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Wrong argument syntax. Ignored: " + toPiece.str);
                    }
                    break;
                case Tapenade.AFTER_ONTK:
                    switch (pieceKind) {
                        case Tapenade.FUNC:
                            flushDiffRoot(diffRootFunc, diffRootCasN, null, diffRootDeps);
                            diffRootIndeps = null;
                            diffRootDeps = null;
                            diffRootCasN = null;
                            diffRootFunc = toPiece.str;
                            state = Tapenade.AFTER_FUNC;
                            break;
                        case Tapenade.VARS:
                            diffRootIndeps = toPiece.str;
                            flushDiffRoot(diffRootFunc, diffRootCasN, diffRootIndeps, diffRootDeps);
                            diffRootFunc = null;
                            diffRootIndeps = null;
                            diffRootDeps = null;
                            diffRootCasN = null;
                            state = Tapenade.BEFORE_FUNC;
                            break;
                        default:
                            TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Wrong argument syntax. Ignored: " + toPiece.str);
                    }
                    break;
                case Tapenade.AFTER_TOTK:
                    switch (pieceKind) {
                        case Tapenade.FUNC:
                            flushDiffRoot(diffRootFunc, diffRootCasN, diffRootIndeps, null);
                            diffRootIndeps = null;
                            diffRootDeps = null;
                            diffRootCasN = null;
                            diffRootFunc = toPiece.str;
                            state = Tapenade.AFTER_FUNC;
                            break;
                        case Tapenade.VARS:
                            flushDiffRoot(diffRootFunc, diffRootCasN, diffRootIndeps, toPiece.str);
                            diffRootFunc = null;
                            diffRootIndeps = null;
                            diffRootDeps = null;
                            diffRootCasN = null;
                            state = Tapenade.BEFORE_FUNC;
                            break;
                        default:
                            TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Wrong argument syntax. Ignored: " + toPiece.str);
                    }
                    break;
                case Tapenade.AFTER_VARS:
                    switch (pieceKind) {
                        case Tapenade.FUNC:
                            flushDiffRoot(diffRootFunc, diffRootCasN, null, diffRootDeps);
                            diffRootIndeps = null;
                            diffRootDeps = null;
                            diffRootCasN = null;
                            diffRootFunc = toPiece.str;
                            state = Tapenade.AFTER_FUNC;
                            break;
                        case Tapenade.VARS:
                            diffRootIndeps = diffRootDeps;
                            diffRootDeps = toPiece.str;
                            flushDiffRoot(diffRootFunc, diffRootCasN, diffRootIndeps, diffRootDeps);
                            diffRootFunc = null;
                            diffRootIndeps = null;
                            diffRootDeps = null;
                            diffRootCasN = null;
                            state = Tapenade.BEFORE_FUNC;
                            break;
                        case Tapenade.ONTK:
                            state = Tapenade.AFTER_ONTK;
                            break;
                        case Tapenade.TOTK:
                            diffRootIndeps = diffRootDeps;
                            diffRootDeps = null;
                            state = Tapenade.AFTER_TOTK;
                            break;
                        default:
                            TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Wrong argument syntax. Ignored: " + toPiece.str);
                    }
                    break;
                default:
                    TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Wrong argument syntax. Ignored: " + toPiece.str
                            + " " + strIndex.str.substring(strIndex.index));
                    strIndex.str = "";
                    strIndex.index = 0;
            }
            pieceKind = getNextDiffRootPiece(strIndex, toPiece);
        }
        if (diffRootFunc != null) {
            flushDiffRoot(diffRootFunc, diffRootCasN, diffRootIndeps, diffRootDeps);
        }
    }

    /**
     * Reads into "toPiece" the next "piece" string in a diffRoot element.
     * The diffRoot element is given in strIndex, which is a String and a
     * read position index in it. The position advances as the reading goes.
     *
     * @return the integer representing the kind of the piece read.
     * A piece may be a
     * <br>
     * -- FUNC : e.g. "mod.func"
     * <br>
     * -- CASN : e.g. "case1" (the square brackets around are stripped)
     * <br>
     * -- VARS : e.g. "x y z.t z%q"   (the parentheses around are stripped)
     * <br>
     * -- ONTK : e.g. "/"
     * <br>
     * -- TOTK : e.g. "\"
     * <br>
     * -- NOTHING : "" when nothing remains to be read in the diffRoot Element.
     */
    private int getNextDiffRootPiece(final StrIndex strIndex, final StrIndex toPiece) {
        final String str = strIndex.str;
        int found = Tapenade.NOTHING;
        if (str != null) {
            final int strlen = str.length();
            int i = strIndex.index;
            int ifirst = i;
            int ilast = -1;
            int inext = i;
            char chari;
            int state = Tapenade.BEGINNING;
            while (i < strlen && state != Tapenade.ENDING) {
                chari = str.charAt(i);
                switch (state) {
                    case Tapenade.BEGINNING:
                        if (chari == ' ') {
                        } else if (chari == '\\' || chari == '>') {
                            found = Tapenade.TOTK;
                            state = Tapenade.ENDING;
                        } else if (chari == '/') {
                            found = Tapenade.ONTK;
                            state = Tapenade.ENDING;
                        } else if (chari == '(') {
                            ifirst = ilast = i + 1;
                            found = Tapenade.VARS;
                            state = Tapenade.INVARS;
                        } else if (chari == '[') {
                            ifirst = ilast = i + 1;
                            found = Tapenade.CASN;
                            state = Tapenade.INCASN;
                        } else {
                            ifirst = i;
                            ilast = i + 1;
                            found = Tapenade.FUNC;
                            state = Tapenade.INIDENT;
                        }
                        inext = i + 1;
                        break;
                    case Tapenade.INVARS:
                        if (chari == ')') {
                            ilast = i;
                            state = Tapenade.ENDING;
                        } else {
                            ilast = i + 1;
                        }
                        inext = i + 1;
                        break;
                    case Tapenade.INCASN:
                        if (chari == ']') {
                            ilast = i;
                            state = Tapenade.ENDING;
                        } else {
                            ilast = i + 1;
                        }
                        inext = i + 1;
                        break;
                    case Tapenade.INIDENT:
                        if (chari == ' ' || chari == '\\' || chari == '>'
                                || chari == '/' || chari == '(' || chari == '[') {
                            ilast = inext = i;
                            state = Tapenade.ENDING;
                        } else {
                            ilast = inext = i + 1;
                        }
                        break;
                    default:
                        break;
                }
                i++;
            }
            if (found == Tapenade.FUNC || found == Tapenade.VARS) {
                toPiece.str = str.substring(ifirst, ilast);
            } else if (found == Tapenade.CASN) {
                toPiece.str = stripWhitesInCasN(str.substring(ifirst, ilast));
            } else {
                toPiece.str = null;
            }
            strIndex.index = inext;
        }
        return found;
    }

    private String stripWhitesInCasN(final String str) {
        final StringBuilder buffer = new StringBuilder();
        boolean placeSeparator = false;
        for (int i = 0; i < str.length(); ++i) {
            if (str.charAt(i) == ' ') {
                placeSeparator = true;
            } else {
                if (placeSeparator && buffer.length() != 0) {
                    buffer.append('_');
                }
                buffer.append(str.charAt(i));
                placeSeparator = false;
            }
        }
        return buffer.toString();
    }

    /**
     * Parses the argument of the option "-vars", which is a list
     * of identifiers of variables. This list is accumulated to the
     * list of independent vars in the current diffRoot, which is the
     * last defined diffRoot. If no diffRoot is current yet,
     * these vars will accumulate to the next coming current diffRoot.
     */
    private void parseDiffVars(final String cmdStr) {
        flushDiffRoot(null, null, cmdStr, null);
    }

    /**
     * Parses the argument of the option "-outvars", which is a list
     * of identifiers of variables. This list is accumulated to the
     * list of dependent vars in the current diffRoot, which is the
     * last defined diffRoot. If no diffRoot is current yet,
     * these vars will accumulate to the nextcoming current diffRoot.
     */
    private void parseDiffOutvars(final String cmdStr) {
        flushDiffRoot(null, null, null, cmdStr);
    }

    /**
     * Adds a new component into the accumulated TapList of
     * differentiation roots "diffRoots". Each element of this
     * "diffRoots" list is of type DiffRoot.
     */
    private void flushDiffRoot(final String func, final String casN, final String indeps, final String deps) {
        TapList<String> parsedIdent;
        StringTokenizer varsTokenizer;
        // Accumulate the independents:
        final TapList<TapList<String>> inCumulVars = new TapList<>(null, null);
        if (indeps != null) {
            varsTokenizer = new StringTokenizer(indeps, " ,");
            while (varsTokenizer.hasMoreElements()) {
                parsedIdent = TapList.parseIdentifier(varsTokenizer.nextToken());
                Tapenade.addIdentInList(parsedIdent, inCumulVars);
            }
            if (func == null) {
                if (unfinishedInVars == null) {
                    waitingInVars = inCumulVars;
                } else {
                    unfinishedInVars.setIndepsPathNames(inCumulVars.tail);
                    unfinishedInVars = null;
                }
            }
        }
        // Accumulate the dependents:
        final TapList<TapList<String>> outCumulVars = new TapList<>(null, null);
        if (deps != null) {
            varsTokenizer = new StringTokenizer(deps, " ,");
            while (varsTokenizer.hasMoreElements()) {
                parsedIdent = TapList.parseIdentifier(varsTokenizer.nextToken());
                Tapenade.addIdentInList(parsedIdent, outCumulVars);
            }
            if (func == null) {
                if (unfinishedOutVars == null) {
                    waitingOutVars = outCumulVars;
                } else {
                    unfinishedOutVars.setDepsPathNames(outCumulVars.tail);
                    unfinishedOutVars = null;
                }
            }
        }

        if (func != null) {
            parsedIdent = TapList.parseIdentifier(func);
            TapList<DiffRoot> inDiffRoots = diffRoots;

            while (inDiffRoots != null
                    && neqIdentifiers(inDiffRoots.head.unitPathName(),
                    parsedIdent)) {
                inDiffRoots = inDiffRoots.tail;
            }
            final DiffRoot thisDiffRoot;
            if (inDiffRoots == null) {
                diffRoots = new TapList<>(new DiffRoot(null, parsedIdent, null), diffRoots);
                thisDiffRoot = diffRoots.head;
            } else {
                thisDiffRoot = inDiffRoots.head;
            }
            final DiffPattern thisDiffPattern = new DiffPattern(casN, null, null);
            // Accumulate the independents:
            if (indeps == null) { // if no new indeps are given, try to use waiting indeps
                if (waitingInVars != null) {
                    thisDiffPattern.setIndepsPathNames(waitingInVars.tail);
                    waitingInVars = null;
                } else { // if no indeps are waiting, let this diffPattern wait for indeps
                    unfinishedInVars = thisDiffPattern;
                }
            } else {
                thisDiffPattern.setIndepsPathNames(inCumulVars.tail);
            }
            // Accumulate the dependents:
            if (deps == null) {
                // if no deps are given, try to use waiting deps
                if (waitingOutVars != null) {
                    thisDiffPattern.setDepsPathNames(waitingOutVars.tail);
                    waitingOutVars = null;
                } else {
                    // if no deps are waiting, let this diffPattern wait for deps
                    unfinishedOutVars = thisDiffPattern;
                }
            } else {
                thisDiffPattern.setDepsPathNames(outCumulVars.tail);
            }
            thisDiffRoot.appendDiffPattern(thisDiffPattern);
        }
    }

    public static TapList<TapPair<String, Integer>> generateListInputFiles(String[] fnames, int[] flanguages) {
        boolean langAvailable = flanguages != null; 
        
        TapList<TapPair<String, Integer>> output = null;
        for(int i = 0; i < fnames.length; i++){
            TapList.addUnlessPresent(output, new TapPair<String, Integer>(fnames[i], langAvailable ? flanguages[i] : TapEnv.UNDEFINED));
        }

        return output;

    }

    /**
     * -mixedLanguage "Lang1 Lang2 byReferenceOrbyResult bind_suffix_list".
     *
     * @param cmdStr "Lang1 Lang2 byReferenceOrbyResult bind_suffix_list".
     */
    private void parseMixedLanguage(final String cmdStr) {
        if (cmdStr != null) {
            final String[] cmdArray = cmdStr.split("\\s+", -1);
            boolean error = cmdArray.length < 5;
            final int origLang;
            final int destLang;
            origLang = Tapenade.parseLanguage(cmdArray[0]);
            destLang = Tapenade.parseLanguage(cmdArray[1]);
            if (origLang == TapEnv.UNDEFINED || destLang == TapEnv.UNDEFINED) {
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Undefined language in: " + cmdStr);
                error = true;
            }
            if (error) {
                TapEnv.commandWarning(TapEnv.MSG_ALWAYS, "Mixed language info ignored: " + cmdStr);
            } else {
                TapEnv.addMixedLanguageInfos(new MixedLanguageInfos(origLang, destLang,
                        cmdArray[2], cmdArray[3], cmdArray[4]));
            }
        }
    }

    /**
     * tapenade -html displays results in a browser
     * defined in BROWSER shell variable.
     */
    private void htmlOutput() {
        File sourceDirFile = new File(TapEnv.tapenadeHome() + File.separator
                + "build" + File.separator + "libs" + File.separator);
        final String sourceDir = sourceDirFile.getPath() + File.separator;
        final String tapenadedir = File.separator + Tapenade.TAPENADE_GEN_HTML_DIR + File.separator;
        final String outputdir = TapEnv.get().outputDirectory+tapenadedir;
        Tapenade.extractFiles(outputdir, sourceDir + "tapenadehtml.jar");
        String browser = System.getProperty("browser");
        if (browser == null || browser.isEmpty()) {
            if (OS_NAME.startsWith(Tapenade.WINDOWS)) {
                // windows default browser
                browser = "iexplore";
            } else {
                // default browser
                browser = "firefox";
            }
        }
        final String[] cmd;
        cmd = new String[2];
        String path = TapEnv.get().outputDirectory + tapenadedir + tapenadeHtmlDisplay;
        final File file = new File(path);
        path = file.getAbsolutePath();
        cmd[0] = browser;
        cmd[1] = path;
        if (commandFound(browser)) {
            System.out.println("Viewing result: "+cmd[0]+" "+cmd[1]);
            new Thread() {
                private String[] cmd;

                private void start(final String[] c) {
                    cmd = c.clone();
                    start();
                }

                @Override
                public void run() {
                    try {
                        final Process p = Runtime.getRuntime().exec(cmd);
                        final InputStream err = p.getErrorStream();
                        loadStream(err);
                    } catch (SecurityException | IOException | NullPointerException | IndexOutOfBoundsException e) {
                        TapEnv.toolError(e.toString());
                    }
                }

                /**
                 * read an input-stream into a String.
                 */
                private void loadStream(InputStream in) throws IOException {
                    int ptr;
                    in = new BufferedInputStream(in);
                    final StringBuilder buffer = new StringBuilder();
                    while ((ptr = in.read()) != -1) {
                        buffer.append((char) ptr);
                    }
                }
            }.start(cmd);
        } else {
            System.out.println("HTML Results in "+cmd[1]);
        }
    }

    /**
     * @return Html css directory for html output.
     */
    protected String cssDir() {
        return cssDir;
    }

    private static final class StrIndex {
        private String str;
        private int index;

        private StrIndex(final String strg) {
            str = strg;
        }
    }

    private static final class FutureFile {
        private final Tree tree;
        private final int language;
        private final int oppositeHtmlLanguage;
        private final String fileFullName;
        private final String oppositeHtmlFileName;
        private final boolean isIncludeFile;

        private FutureFile(final Tree tr, final int lang, final String fullname,
                           final int oppositelanguage, final String oppositehtmlfilename, final boolean isincludefile) {
            tree = tr;
            language = lang;
            fileFullName = fullname;
            oppositeHtmlLanguage = oppositelanguage;
            oppositeHtmlFileName = oppositehtmlfilename;
            isIncludeFile = isincludefile;
        }

        @Override
        public String toString() {
            return "< FutureFile " //+"@"+Integer.toHexString(this.hashCode())+" "
                    + fileFullName + " lang:" + language + " [HTML opposite:" + oppositeHtmlFileName + "] tree:" + (tree == null ? "empty" : tree.length() + "children") + ">";
        }
    }
}
