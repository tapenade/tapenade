<% pageContext.include("header.jsp"); %>

<script src="checkPaste.js" type="text/JavaScript"></script>

<%@ page language="java" %>
    <%
       Integer inputLanguage = (Integer) session.getAttribute("inputLanguage");
       int inputFT = 0;
       String chk77 = "";
       String chk95 = "";
       String chkc = "";
       if (inputLanguage != null) inputFT = inputLanguage.intValue();
       if (inputFT == 1) {chk77 = "checked";} 
       else if (inputFT == 2) {chk95 = "checked";}
       else if (inputFT == 3) {chkc = "checked";};
    %>

<form name="aliad" method="post" enctype="multipart/form-data" action="/tropics/servlet/fr.inria.tapenade.toplevel.ADServlet">

    <div class="form-group row">
      <label class="col-3 col-form-label" for="Diff">Select the input language</label>
      <div class="col">
          <label class="radio-inline col-form-label">
                <input type="radio"
                       NAME="filetype" VALUE="fortran77" autocomplete="off" <%= chk77 %>>
             Fortran 77
          </label>
          <label class="radio-inline col-form-label">
                <input type="radio"
                       NAME="filetype" VALUE="fortran95" checked autocomplete="off" <%= chk95 %>>
             Fortran 95
          </label>
          <label class="radio-inline col-form-label">
                <input type="radio"
                       NAME="filetype" VALUE="c" autocomplete="off" <%= chkc %>>
             C
          </label>
      </div>
    </div>

  <div class="form-group">
          <label for="Paste">
               Copy and paste your source program in the following text area
               or <a href="index.jsp">upload your files</a>
          </label>
          <div class="col">
              <textarea class="form-control" name="source" cols="70" rows="20"></textarea>
          </div>
  </div>

      <input type="hidden" NAME="choice" VALUE="text">


<% pageContext.include("fields.jsp"); %>

    </form>

<% pageContext.include("footer.jsp"); %>
