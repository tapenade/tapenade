<!doctype html>
<html>
  <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

          <!-- Bootstrap CSS -->
          <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
                    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

          <link type="text/CSS" rel="stylesheet" href="tapenadebootstrap.css">

          <title>TAPENADE Automatic Differentiation Engine</title>
  </head>

    <SCRIPT SRC="checkForm.js" TYPE="text/JavaScript"></SCRIPT>

    <%@ page language="java" %>
    <%
       String servletDir = (String)session.getAttribute("ServletDir");
       if (servletDir == null) servletDir = "/tropics/";
       String sessionId = session.getId() + "";
       String time = (String)session.getAttribute("time");
       String clientDirPath = (String)session.getAttribute("clientDirPath");;
       Object uploadedDir = session.getAttribute("UploadedDirName");
       String uploadedDirName = (String)uploadedDir;
       String clientDirName = (String)session.getAttribute("clientDirName");
       String filename = (String)session.getAttribute("filename");
       String errorPath = uploadedDirName + "/" + clientDirName + "/" + "error.html";
       String tapenadeGenDir = (String)session.getAttribute("tapenadeGenDir");
       String msgPath = uploadedDirName + "/" + clientDirName + 
                "/" + tapenadeGenDir + "/" + filename + "msg.html";
    %>

  <body class="bg-light">
  <div class="container">
    <p>
      Something went wrong during differentiation.
      <br />
<pre>
<% if (uploadedDir != null) {
      if (filename != null) { 
          java.io.File fileMsg = new java.io.File(clientDirPath + "/" +
               tapenadeGenDir + "/" + filename + "msg.html");
          if (fileMsg.exists()) {
              pageContext.include(msgPath);
          }
      }
      pageContext.include(errorPath); 
   } %>
</pre>
      For more information, display <a href="<%=uploadedDirName + "/" + clientDirName + "/" + "stack.html" %>"> the stack trace</a>.
      There can be several causes to this failure:
    <ul>
      <li>The syntax of your source may not be recognized,
<a href="http://www-sop.inria.fr/ecuador/tapenade/userdoc/build/html/tapenade/faq.html#SE1"> refer to the FAQ</a>.
      <li>The name of the head procedure doesn't exist in the program.
      <li>The given input and output variables are unknown to the head
procedure, or are not differentiable,
<a href="http://www-sop.inria.fr/ecuador/tapenade/userdoc/build/html/tapenade/faq.html#diffvarsinit"> refer to the FAQ</a>.
      <li>JavaScript is not enabled in your browser.
      <li>Your browser does not accept cookies.
      <li>Your session has been invalidated after 3600 seconds.
    </ul>

     Please check the above points and : 
<table cols=2>
<tr>
<td>
     <form name="retrys" action="<%=servletDir + "servlet/fr.inria.tapenade.toplevel.RetryNewServlet"%>"
	 target="_top" align=left>
	<input class="btn btn-outline-primary" name="buttonretrynew" type="submit"
	  value="Retry with new files">
     </form>
</td>
</tr>
</table>

If everything seems correct, it is probably an internal bug.
<font color=red><b>In that case,
please <a href="mailto:tapenade@inria.fr?subject=Erreur de la servlet&body=Session:<%=sessionId%>, Time:<%=time%>">send us</a> this bug report</b></font>, without modifying the contents of the message. We will examine it promptly.
    </p>
    <hr>
  </div>
  </body>
</html>
