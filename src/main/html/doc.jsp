Given 
<ul src="Images/puce.gif">
<%@ page language="java" %>
    <%
       Integer inputLanguage = (Integer)session.getAttribute("inputLanguage");
       int inputFT = 0;
       String chk77 = "";
       String chk95 = "";
       String chkundef = "";
       String chkc = "";
       if (inputLanguage != null) inputFT = inputLanguage.intValue();
       if (inputFT == 1) {chk77 = "checked";} 
       else if (inputFT == 2) {chk95 = "checked";}
       else if (inputFT == 3) {chkc = "checked";}
       else {chkundef = "checked";} ;
    %>

<li> a source program,
<li> the name of the top procedure to be differentiated,
<li> the dependent output variables whose derivatives are
required,
<li> the independent input variables with respect to which it must
differentiate,
</ul>

          this tool returns the forward (tangent) or reverse
(adjoint) differentiated program.
<br>

<p>

<div class="form-group row">
  <label class="col-3 col-form-label" for="Diff">Select the input language</label>
  <div class="col">
      <label class="radio-inline col-form-label">
            <input type="radio"
                   NAME="filetype" VALUE="undefined" autocomplete="off" checked <%= chkundef %>>
         from the files extensions
      </label>
      <label class="radio-inline col-form-label">
            <input type="radio"
                   NAME="filetype" VALUE="fortran77" autocomplete="off" <%= chk77 %>>
         Fortran 77
      </label>
      <label class="radio-inline col-form-label">
            <input type="radio"
                   NAME="filetype" VALUE="fortran95" autocomplete="off" <%= chk95 %>>
         Fortran 95
      </label>
      <label class="radio-inline col-form-label">
            <input type="radio"
                   NAME="filetype" VALUE="c" autocomplete="off" <%= chkc %>>
         C
      </label>
  </div>
</div>
