<html>
  <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
                integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link type="text/CSS" rel="stylesheet" href="tapenadebootstrap.css">
    <title>TAPENADE Error messages</title>
  </head>

  <body>

    <%@ page language="java"%>
    
    <%
	String clientDirName = (String)session.getAttribute("clientDirName");
	String uploadedDirName = (String)session.getAttribute("UploadedDirName");
	String filename = (String)session.getAttribute("filename");
        String msgFile= uploadedDirName + "/" + clientDirName + "/" + 
             (String)session.getAttribute("tapenadeGenDir")
             + "/" + filename + "msg.html";
    %>

<% pageContext.include(uploadedDirName + "/" + clientDirName + "/" +
        (String)session.getAttribute("tapenadeGenDir")
        + "/" + "msg.html"); %>

  </body>
</html>
