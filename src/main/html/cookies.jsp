<!doctype html>
<html>
  <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

          <!-- Bootstrap CSS -->
          <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
                    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

          <link type="text/CSS" rel="stylesheet" href="tapenadebootstrap.css">

          <title>TAPENADE Automatic Differentiation Engine</title>
  </head>

    <%@ page language="java" %>
    <%
       String servletDir = (String)session.getAttribute("ServletDir");
       if (servletDir == null) servletDir = "/tropics/";
       String sessionId = session.getId() + "";
       String time = (String)session.getAttribute("time");
    %>

  <body class="bg-light">
  <div class="container">
    <p>
      An error occurs during the execution of the application, probably
      because your browser does not accept cookies. 
	Please modify your preferences in order to accept them.

<table cols=3>
<tr>
<td>
     <form name="retrys" action="<%=servletDir + "servlet/fr.inria.tapenade.toplevel.RetryServlet"%>"
	 target="_top" align=left>
	<input class="btn btn-outline-primary" name="buttonretrysame" type="submit"
	  value="Retry with the same files">
     </form>
</td>
</tr>
</table>

If everything seems correct, it is probably an internal bug.
<font color=red><b>In that case,
please <a href="mailto:tapenade@inria.fr?subject=www-tapenade error&body=Session:<%=sessionId%>, Time:<%=time%>">send us</a> this bug report</b></font>, without modifying the contents of the message. We will examine it promptly.
    </p>
    <hr>

  </body>
  </div>
</html>
