<!-- Includes the HTML code of the differentiate callgraph representation -->

<html>
  <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
              integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link type="text/CSS" rel="stylesheet" href="tapenadebootstrap.css">
    <title>Differentiated code</title>
  </head>

  <body>

    <%@ page language="java"%>
    <%
    String servletDir = (String)session.getAttribute("ServletDir");
            if (servletDir == null) servletDir = "/tropics/";
	String clientDirName = (String)session.getAttribute("clientDirName");
	String uploadedDirName = (String)session.getAttribute("UploadedDirName");
	String filename = (String)session.getAttribute("filename");
    %>

<% pageContext.include(uploadedDirName + "/" + clientDirName + "/" +
        (String)session.getAttribute("tapenadeGenDir")
        + "/" + "callgraphdiff.html"); %>

  </body>
</html>
