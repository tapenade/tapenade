Tapenade HTML output and web server utilities
============================================= 
`-html` option creates Html results in `tapenadehtml/tapenade.html` in the
output directory.  

Set the `BROWSER` shell variable to your favorite web browser to automatically 
display the result files in your browser.
