<!doctype html>
<html lang="en">
  <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
                  integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <link type="text/CSS" rel="stylesheet" href="tapenadebootstrap.css">

        <title>TAPENADE Automatic Differentiation Engine</title>
  </head>

  <body class="bg-light">
  <div class="container">

          <div class="row bg-white">
              <div class="col-2">
                  <a href="http://www.inria.fr">
                      <img class="img-fluid" src="Images/logo-inria.jpg" alt="Inria"></a>
              </div>

              <div class="col-4">
                  <h1>TAPENADE</h1><br>
                  <h4>Automatic Differentiation Engine</h4>
              </div>

              <div class="col-1">
                  <img class="img-fluid"
                       src="tapenadelogo.gif" alt="Tapenade">
              </div>

              <div class="col-5">
                  <p class="text-right">
                <a href="https://team.inria.fr/ecuador/en/tapenade/" target="_new">Documentation</a><br>
                <a href="mailto:sympa@inria.fr?subject=subscribe%20tapenade-users">Subscribe</a>
                to the <a href="mailto:tapenade-users@inria.fr">tapenade-users mailing list.</a>
                  </p>
              </div>
          </div>

<p>

