<form>
  <div class="form-group row">
          <label class="col-3 col-form-label" for="Top">Name of the top procedure</label>
          <div class="col">
             <input type="text" class="form-control" name="head" id="Top">
          </div>
  </div>

  <div class="form-group row">
          <label class="col-3 col-form-label" for="Dep">Dependent output variables</label>
          <div class="col">
             <input type="text" class="form-control" name="depoutputvars" id="Dep">
             <small id="help" class="form-text text-muted">
                              separator: white space, default: all variables
             </small>
          </div>
  </div>

  <div class="form-group row">
          <label class="col-3 col-form-label" for="Indep">Independent input variables</label>
          <div class="col">
              <input type="text" class="form-control" name="indepinputvars" id="Indep">
              <small id="help" class="form-text text-muted">
                               separator: white space, default: all variables
              </small>
          </div>
  </div>

  <div class="form-group row">
          <label class="col-3 col-form-label" for="App">For our records</label>
          <div class="col">
              <textarea class="form-control" name="Tapenade-Log" rows="1"></textarea>
              <small id="help" class="form-text text-muted">
                   Could you please give us your name and the application you have in mind,
                   it can very well be only "test"
              </small>
          </div>
  </div>

  <div class="form-group row">
            <label class="col-3 col-form-label" for="Diff">Differentiate in</label>
            <div class="col" role="group" aria-label="Button">
  	          <input class="btn btn-outline-primary"
        	         name="buttondiffforward" type="submit"
                     value="Tangent Mode"
                     onClick="return checkFiles()"/>
  	          <input class="btn btn-outline-primary"
  	                 name="buttondiffforwardvectorial" type="submit"
                     value="Multidirectional Tangent Mode"
                     onClick="return checkFiles()"/>
  	          <input class="btn btn-outline-primary"
  	                 name="buttondiffbackward" type="submit"
                     value="Adjoint Mode"
                     onClick="return checkFiles()"/>
  	          <input class="btn btn-outline-primary"
  	                 name="buttondiffbackwardvectorial" type="submit"
                     value="Multiobjective Adjoint Mode"
                     onClick="return checkFiles()"/>
            </div>
    </div>
</form>
