/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

function servletName() {
    sname = document.location.pathname;
    index = sname.lastIndexOf('/');
    sname = sname.slice(0,index+1);
    return sname;
}

function checkFileName() {
    if (document.aliad.uploaded_file_or_include.value == null ||
       document.aliad.uploaded_file_or_include.value.length == 0) {
       alert("Please, enter the path of the file");
       return false
    } else {
       return true
    }
}

function checkFiles() {
    if ((document.aliad.files != null && document.aliad.files.length > 0) || 
        (document.aliad.uploaded_file_or_include.value != null &&
         document.aliad.uploaded_file_or_include.value.length != 0)) {
        return true
    } else {
         alert("Please, enter the path of the file");
         return false
    }
}

function retrynew() {
    window.location.href=servletName() + "servlet/fr.inria.tapenade.toplevel.RetryNewServlet"
    return true
}
