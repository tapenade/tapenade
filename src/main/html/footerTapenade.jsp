<div class="bg-white">
If you want to be kept informed about new developments and releases
of TAPENADE,
<a href="mailto:sympa@inria.fr?subject=subscribe tapenade-users">
subscribe</a>
to the
<a href="mailto:tapenade-users@inria.fr">
tapenade-users mailing list.
</a>
<br>
REVISION   - <a href="mailto:tapenade@inria.fr">tapenade@inria.fr</a>
</div>

    <div class="alert text-center ml-auto" id="tapenadecookie" role="alert">
        www-tapenade.inria.fr uses cookies.
        <input value="Deny" type="button" class="btn btn-light btn-sm" onClick="return denyCookie()"/>
        <input value="Accept" type="button" class="btn btn-primary btn-sm" onClick="return acceptCookieJsp()"/>
    </div>

</div>

  </body>

<script src="cookiealert.js" type="text/JavaScript"></script>

</html>
