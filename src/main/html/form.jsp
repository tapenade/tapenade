<% pageContext.include("header.jsp"); %>

<script src="checkForm.js" type="text/JavaScript"></script>

    <%@ page language="java" %>
    <%
       String servletDir = (String)session.getAttribute("ServletDir");
       if (servletDir == null) servletDir = "/tropics/";
       String sessionId = session.getId() + "";
       String inputFileNames = "";
       String includeFileNames = "";
       String filename = "";
       if (session != null ) {
       String time = (String)session.getAttribute("time");
       String uploadedDirName = (String)session.getAttribute("UploadedDirName");
       String clientDirName = (String)session.getAttribute("clientDirName");
       java.util.Vector inputFiles = (java.util.Vector)session.getAttribute("inputFileNames");
       java.util.Vector includeFiles = (java.util.Vector)session.getAttribute("includeFileNames");
       int i = 0;
       if (inputFiles != null) i = inputFiles.size();
       if (i > 0) {
          Object[] arrayNames = inputFiles.toArray();
          for (int ii = 0; ii < arrayNames.length; ii++) {
             inputFileNames = "<option>" + arrayNames[ii] + 
                           inputFileNames;
          }
       } else {
          i = 1;
       }
       inputFileNames = "<select class=\"form-control\" size=" + i
                        + " name=\"files\" multiple>" +
                        inputFileNames + "</select>";
       int j = 0;
       if (includeFiles != null) j = includeFiles.size();
       if (j > 0) {
          Object[] arrayNames = includeFiles.toArray();
          for (int ii = 0; ii < arrayNames.length; ii++) {
             includeFileNames = "<option>" + arrayNames[ii] +
                             includeFileNames;
          }
       } else {
          j = 1;
       }
       includeFileNames = "<select class=\"form-control\" size=" + j
                           + " name=\"includes\" multiple>" +
                           includeFileNames + "</select>";

       }
    %>

    <form name="aliad" method="post" enctype="multipart/form-data"
         action="<%=servletDir + "servlet/fr.inria.tapenade.toplevel.ADServlet"%>" >
      <input type="hidden" NAME="choice" VALUE="file">
<% pageContext.include("doc.jsp"); %>


<div class="form-group row">
       <label class="col-3 col-form-label">Upload source and include files, repeatedly</label>
       <div class="col">
           <input type="file" class="form-control-file" name="uploaded_file_or_include" accept="text/fortran">
       </div>
</div>

<div class="form-group row">
     <label class="col-3 col-form-label">Upload it</label>
     <div class="col">
       <div class="row">
         <div class="col-6">
          <input class="btn btn-outline-primary" name="buttonfile" type="submit"
              value="as a source"
              onClick="return checkFileName()">
         </div>
         <div class="col-6">
          <input class="btn btn-outline-primary" name="buttoninclude" type="submit"
              value="as an include"
              onClick="return checkFileName()">
         </div>
       </div>
       <div class="row">
         <div class="col-6">
          <%= inputFileNames %>
         </div>
         <div class="col-6">
          <%= includeFileNames %>
         </div>
       </div>
       <div class="row">
          <div class="col">
          	  <input class="btn btn-outline-primary"
                	         name="buttonremove" type="submit"
                	         value="Remove selected files">
          	  <input class="btn btn-outline-primary"
          	             type="button"
          	             value="Retry with new files"
          	             onClick="return retrynew()">
          </div>
       </div>
     </div>
</div>

<p>

<% pageContext.include("fields.jsp"); %>

    </form>

<% pageContext.include("footer.jsp"); %>
