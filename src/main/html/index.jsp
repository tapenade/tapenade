<% pageContext.include("header.jsp"); %>

<script src="checkForm.js" type="text/JavaScript"></script>

    <%@ page language="java" %>
    <%
         session.setAttribute("maxsize",new Long(150000));
         session.setAttribute("inputFileNames", null);
         session.setAttribute("includeFileNames", null);
    %>

<form name="aliad" method="post" enctype="multipart/form-data"
            action="/tropics/servlet/fr.inria.tapenade.toplevel.ADServlet"
            onSubmit="return checkFileName()">
           <input type="hidden" NAME="choice" VALUE="file">

<% pageContext.include("doc.jsp"); %>


     <div class="form-group row">
       <label class="col-3 col-form-label">Upload source and include files, repeatedly</label>
       <div class="col">
           <input type="file" class="form-control-file" name="uploaded_file_or_include">
           or <a href="paste.jsp">copy paste your Fortran or C source program</a>
       </div>
     </div>

     <div class="form-group row">
       <label class="col-3 col-form-label">Upload it</label>

       <div class="col-6" role="group" aria-label="Button">
          <input class="btn btn-outline-primary" name="buttonfile" type="submit"
              value="as a source"
              onClick="return checkFileName()">
          <input class="btn btn-outline-primary" name="buttoninclude" type="submit"
              value="as an include"
              onClick="return checkFileName()">
       </div>
     </div>
<p>

<% pageContext.include("fields.jsp"); %>
    </form>

<% pageContext.include("footer.jsp"); %>
