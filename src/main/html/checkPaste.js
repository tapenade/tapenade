/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

function servletName() {
    sname = document.location.pathname;
    index = sname.lastIndexOf('/');
    sname = sname.slice(0,index+1);
    return sname;
}

function checkFiles() {
    if (document.aliad.source.value.length == 0) {
       alert("Please, copy-paste your source code in the text area");
       return false;
    }
    return true;
}

function retrynew() {
    window.location.href=servletName() + "servlet/fr.inria.tapenade.toplevel.RetryNewServlet"
    return true
}
