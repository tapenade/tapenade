<html>
  <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

          <!-- Bootstrap CSS -->
          <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
                    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

          <link type="text/CSS" rel="stylesheet" href="tapenadebootstrap.css">

          <title>TAPENADE Automatic Differentiation Engine</title>
  </head>

    <SCRIPT SRC="checkForm.js" TYPE="text/JavaScript"></SCRIPT>

    <%@ page language="java" %>
    <%
       String servletDir = (String)session.getAttribute("ServletDir");
       if (servletDir == null) servletDir = "/tropics/";
       String sessionId = session.getId() + "";
       String time = (String)session.getAttribute("time");
       String uploadedDirName = (String)session.getAttribute("UploadedDirName");
       String clientDirName = (String)session.getAttribute("clientDirName");
       String filename = (String)session.getAttribute("filename");
       String errorPath = uploadedDirName + "/" + clientDirName + "/" + "error.html";
       String msgPath = uploadedDirName + "/" + clientDirName + "/" +
             (String)session.getAttribute("tapenadeGenDir")
             + "/" + filename + "msg.html";
       Long maxServletSize = (Long)session.getAttribute("maxsize");
    %>

  <body class="bg-light">
  <div class="container">
The total size of the uploaded files is limited to <%= maxServletSize %> characters.
<p>
Please try again with smaller files:

<table cols=3>
<tr>
<td>
     <form name="retrys" action="<%=servletDir + "servlet/fr.inria.tapenade.toplevel.RetryServlet"%>"
	 target="_top" align=left>
	<input class="btn btn-outline-primary" name="buttonretrysame" type="submit"
	  value="Retry with the same files" />
     </form>
</td>
<td>
     <form name="retryn" action="<%=servletDir + "servlet/fr.inria.tapenade.toplevel.RetryNewServlet"%>"
	target="_top" align=left>
	<input class="btn btn-outline-primary" name="buttonretrynew" type="submit"
	  value="Retry with new files" />
     </form>
</td>
</tr>
</table>

or 
<a href="http://www-sop.inria.fr/ecuador/tapenade/userdoc/build/html/download.html">download Tapenade</a>
and 
<a href="http://www-sop.inria.fr/ecuador/tapenade/userdoc/build/html/tapenade/tutorial.html#install">
install Tapenade locally on your system</a>.

  </div>
  </body>
</html>
