<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link type="text/CSS" rel="stylesheet" href="tapenadebootstrap.css">
    <title>Tapenade buttons</title>
</head>
<body>

    <%@ page language="java" %>
    <%
    String servletDir = (String) session.getAttribute("ServletDir");
            if (servletDir == null) servletDir = "/tropics/";
	String clientDirName = (String) session.getAttribute("clientDirName");
	String uploadedDirName = (String) session.getAttribute("UploadedDirName");
	String filename = (String) session.getAttribute("filename");
	java.util.Vector filenames = (java.util.Vector) session.getAttribute("inputFileNames");
	String mode = (String) session.getAttribute("diffMode");
	int diffMode = 0;
        boolean multi = false;
        String multiDirMode = (String) session.getAttribute("multiDirMode");
        multi = multiDirMode.equals("true");
	if (mode != null && mode.length() != 0)
	    diffMode = Integer.parseInt(mode);
        Integer language = (Integer) session.getAttribute("topLanguage");
        int topLanguage = language.intValue();
        String retryButton = "Retry with the same files";
        Integer choice = (Integer) session.getAttribute("choice");
        if (choice == 2) retryButton = "Retry copy/paste";
        String str="";
        if (diffMode == -1 ) str = "<form action=\"ADFirstAidKit.tar\"> <input class=\"btn btn-outline-primary btn-block\" name=\"DownloadC\" type=\"submit\" value=\"Download PUSH/POP\" /></form>";
    %>

<div class="btn-group-sm" role="group" aria-label="Button">
        <form name="retrys" action="<%=servletDir + "servlet/fr.inria.tapenade.toplevel.RetryServlet"%>"
              target="_top" align=left>
           <input class="btn btn-outline-primary btn-block"
                name="buttonretrysame" type="submit"
                value="<%=retryButton%>" />
        </form>

        <form name="retrys" action="<%=servletDir + "servlet/fr.inria.tapenade.toplevel.RetryNewServlet"%>"
              target="_top" align=left>
           <input class="btn btn-outline-primary btn-block"
                name="buttonretrynew" type="submit"
                value="Retry with new files" />
        </form>

        <form name="download"
                 action="<%=uploadedDirName + "/" + clientDirName + "/"
        	        + "tapenadeoutput/TapenadeResults.zip"%>"
        	 target="_top" align=left>
        	<input class="btn btn-outline-primary btn-block"
        	     name="Download" type="submit"
        	     value="Download differentiated file" />
        </form>

        <%=str%>
</div>

</body>
</html>
