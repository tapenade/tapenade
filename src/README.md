Overall operations chain of Tapenade
====================================

![Overall operations chain of Tapenade](../docs/archiTapenade5v2.jpg)

The structure of the source reflects our view of Tapenade as a chain of
successive steps, that go from a set of source files to a set of
differentiated files. Each step operates on one representation of the code
and either enriches the given representation, or transforms it into another
representation. Three sorts of representation of a program are used:
* **Files**, i.e. textual files in some programming language, exist obviously 
at both ends of the chain. In addition to files as "text", there are options 
to produce HTML files able to display output files in a GUI.
* **Abstract Syntax Trees** (ASTs) exist mostly as the output of initial parsing 
and as the input of final pretty-printing. ASTs are also used in the central 
Internal Representation to hold individual code statements. Tapenade ASTs are 
(almost) independent from the files language (Fortran,C...). They are expressed 
in a special language that we call "IL" for "Imperative Language", a sort of 
union of the constructs that one may find in actual languages. Code for ASTs 
is (mostly) in packages `utils` 
[README.md](main/java/fr/inria/tapenade/utils/README.md) and 
`representation` [README.md](main/java/fr/inria/tapenade/representation/README.md).
* **Internal Representation** (IR) is a graph-based representation of the 
complete given set of files. It is a call graph of flow graphs, together 
with a tree of Symbol Tables. Code for the IR is in package `representation`.

The successive steps are:
* **Parsing**, that translates the set of input files into their ASTs. Code 
for this step is in packages `front`, `frontend`, `frontf`, `frontc`, 
`frontcpp` etc.
* **IR construction**, that translates the incoming parsed ASTs into the 
Internal Representation. Code for this step is in package `representation`.
* **Analysis**, that runs useful data-flow analysis on the complete IR, thus 
enriching the IR. Code for this step is in package `analysis`,
see [README.md](main/java/fr/inria/tapenade/analysis/README.md).
* **Differentiation**, that builds a new, differentiated IR, from the given IR. 
Code for this step is in package `differentiation`.
* **Tree regeneration**, that translates the differentiated IR into new, 
differentiated ASTs. Code for this step is in package `ir2tree`.
* **Pretty-Print**, that translates the differentiated ASTs into output files. 
Code for this step is in package `prettyprint`.

Two other packages exist on the side:
* Package `utils` holds very basic objects used everywhere, such as trees, 
lists, bitsets, etc. 
See [README.md](main/java/fr/inria/tapenade/utils/README.md).
* Package `gui` defines a simple gui for Tapenade, and a few other graphical 
utilities (for debug...)

Refer to each package for more details on its implementation.

Note that the Parsing packages are not located here in fr/inria/tapenade, 
but in a different place. This is in part because parsers are complex, and 
may be defined with different tools and languages. In fact parsers are 
considered external to Tapenade itself, and the "official" input of Tapenade 
is just ASTs using formalism IL. Of course Tapenade distribution does provide 
parsers for Fortran, C, for convenience, but we like to think that 
one may define their own parser for another imperative language, make it spit 
out a set of IL ASTs, and connect it to the input of Tapenade. We provide the 
definition of the IL formalism for that purpose.
