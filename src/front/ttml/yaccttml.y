%{
#include <stdio.h>
#include <ctype.h>
%}
%token KW_POUV
%token KW_EQULSUPE
%token KW_PFER
%token KW_MINUSUPE
%token KW_PVIR
%token KW_DPINDPIN
%token KW_DPINEQUL
%token KW_CHAP
%token KW_BOUV
%token KW_DPIN
%token KW_BFER
%token KW_COUV
%token KW_CFER
%token KW_ETOI
%token KW_XCLAXCLA
%token KW_VIRG
%token KW_1
%token IDENT
%token VARIABLE
%token STRING
%{
#include "lexttml.c"
%}
%start NT_file
%%
NT_file : NT_anyIdent  KW_POUV  NT_anyIdent  KW_EQULSUPE  NT_anyIdent  KW_PFER  NT_rules  
   { fputs("1\n", yyout);}
   ;
NT_rule : NT_anyPattern  KW_MINUSUPE  NT_anyPattern  KW_PVIR  
   { fputs("2\n", yyout);}
   ;
NT_anyPattern : NT_ident  KW_DPINDPIN  NT_anyPattern  
   { fputs("3\n", yyout);}
   ;
NT_anyPattern : NT_variable  KW_DPINEQUL  NT_anyPattern  
   { fputs("4\n", yyout);}
   ;
NT_anyPattern : NT_variable  
    { }
   ;
NT_anyPattern : NT_userFunctionCall  
    { }
   ;
NT_anyPattern : NT_realTreePattern  
    { }
   ;
NT_anyPattern : NT_realListPattern  
    { }
   ;
NT_treePattern : NT_ident  KW_DPINDPIN  NT_treePattern  
   { fputs("9\n", yyout);}
   ;
NT_treePattern : NT_variable  KW_DPINEQUL  NT_treePattern  
   { fputs("10\n", yyout);}
   ;
NT_treePattern : NT_variable  
    { }
   ;
NT_treePattern : NT_userFunctionCall  
    { }
   ;
NT_treePattern : NT_realTreePattern  
    { }
   ;
NT_realTreePattern : NT_ident  NT_listPattern  
   { fputs("14\n", yyout);}
   ;
NT_realTreePattern : NT_realTreePattern  KW_CHAP  KW_BOUV  NT_ident  KW_DPIN  NT_treePattern  KW_BFER  
   { fputs("15\n", yyout);}
   ;
NT_realTreePattern : NT_variable  KW_CHAP  KW_BOUV  NT_ident  KW_DPIN  NT_treePattern  KW_BFER  
   { fputs("16\n", yyout);}
   ;
NT_realTreePattern : NT_ident  KW_DPIN  NT_value  
   { fputs("17\n", yyout);}
   ;
NT_realTreePattern : NT_ident  KW_DPIN  NT_variable  
   { fputs("18\n", yyout);}
   ;
NT_realTreePattern : NT_ident  KW_DPIN  NT_userFunctionCall  
   { fputs("19\n", yyout);}
   ;
NT_listPattern : NT_ident  KW_DPINDPIN  NT_listPattern  
   { fputs("20\n", yyout);}
   ;
NT_listPattern : NT_variable  KW_DPINEQUL  NT_listPattern  
   { fputs("21\n", yyout);}
   ;
NT_listPattern : NT_variable  
    { }
   ;
NT_listPattern : NT_userFunctionCall  
    { }
   ;
NT_listPattern : NT_realListPattern  
    { }
   ;
NT_realListPattern : KW_COUV  NT_listPatterns  KW_CFER  
   { fputs("25\n", yyout);}
   ;
NT_realListPattern : KW_COUV  KW_CFER  
   { fputs("26\n", yyout);}
   ;
NT_realListPattern : KW_POUV  NT_treePatterns  KW_PFER  
   { fputs("27\n", yyout);}
   ;
NT_realListPattern : KW_POUV  KW_PFER  
   { fputs("28\n", yyout);}
   ;
NT_realListPattern : KW_POUV  NT_treePatterns  KW_PFER  KW_ETOI  
   { fputs("29\n", yyout);}
   ;
NT_userFunctionCall : KW_XCLAXCLA  NT_ident  KW_POUV  NT_argSigs  KW_PFER  KW_POUV  NT_argPatterns  KW_PFER  
   { fputs("30\n", yyout);}
   ;
NT_userFunctionCall : KW_XCLAXCLA  NT_ident  KW_POUV  NT_argSigs  KW_PFER  KW_POUV  KW_PFER  
   { fputs("31\n", yyout);}
   ;
NT_rules : NT_rules  NT_rule  
   { fputs("32\n", yyout);}
   ;
NT_rules : 
   { fputs("33\n", yyout);}
   ;
NT_listPatterns : NT_listPatterns  KW_VIRG  NT_listPattern  
   { fputs("34\n", yyout);}
   ;
NT_listPatterns : NT_listPattern  
   { fputs("35\n", yyout);}
   ;
NT_treePatterns : NT_treePatterns  KW_VIRG  NT_treePattern  
   { fputs("36\n", yyout);}
   ;
NT_treePatterns : NT_treePattern  
   { fputs("37\n", yyout);}
   ;
NT_argPatterns : NT_argPatterns  KW_VIRG  NT_treePattern  
   { fputs("38\n", yyout);}
   ;
NT_argPatterns : NT_argPatterns  KW_VIRG  NT_value  
   { fputs("39\n", yyout);}
   ;
NT_argPatterns : NT_treePattern  
   { fputs("40\n", yyout);}
   ;
NT_argPatterns : NT_value  
   { fputs("41\n", yyout);}
   ;
NT_argSigs : NT_argSigs  KW_VIRG  NT_argSig  
   { fputs("42\n", yyout);}
   ;
NT_argSigs : NT_argSig  
   { fputs("43\n", yyout);}
   ;
NT_argSig : KW_1  
   { fputs("44\n", yyout);}
   ;
NT_argSig : KW_ETOI  
   { fputs("45\n", yyout);}
   ;
NT_argSig : KW_DPIN  
   { fputs("46\n", yyout);}
   ;
NT_ident : IDENT  
   { fputs("47\n", yyout);fputs(yytext,yyout); putc('\n',yyout) ; }
   ;
NT_anyIdent : NT_ident  
    { }
   ;
NT_anyIdent : NT_variable  
    { }
   ;
NT_variable : VARIABLE  
   { fputs("50\n", yyout);fputs(yytext,yyout); putc('\n',yyout) ; }
   ;
NT_value : STRING  
   { fputs("51\n", yyout);fputs(yytext,yyout); putc('\n',yyout) ; }
   ;
%%
