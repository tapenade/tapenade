#include <stdio.h>
#define WAITDATA_DONE -1515
#include "yaccttml.c"
main()
{
  int code;
  yyin = stdin; yyout = stdout;	/* for flex */
  for (;;) {
    fflush(yyout);
    BEGIN waitdata;
    switch ((code = yylex())) {
    case WAITDATA_DONE:
      BEGIN entrypoint;yyparse();
      fputs("-7\n", yyout);
      if (yyin != stdin) {
	fclose(yyin);
	yyin = stdin;
#ifdef FLEX_SCANNER
	YY_NEW_FILE;
#else
	/* The following is needed in case of a syntax error */
	yyprevious = '\n';	/* Starting a line */
	yysptr = yysbuf;	/* Empties the lex buffer */
#endif
      }
      break;
    case 0: exit (0);		/* EOF */
    default:
      fprintf(stderr, "Unexpected return from yylex%d\n", code);
      exit (1);
    }
  }
}
