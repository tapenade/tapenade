1i\
%{\
/*\
This file is a sed script to be applied to the file defining the non\
generic tokens.  It should define all the generic ones whose definition\
can't be specified in METAL.  It could also define some macros and sizes\
of LEX that may be needed.\
*/\
%}\
%S waitdata entrypoint sentences comment\
%%\
<comment>.*            {fprintf(yyout,"-1\\n%d\\n%s\\n",yyleng,yytext); }\
<sentences,comment>\\n {BEGIN sentences; fputs("-2\\n",yyout); }\
<sentences,comment>[ \\t] ;\

$a\
<sentences>IDENT		return(IDENT);\
<sentences>VARIABLE		return(VARIABLE);\
<sentences>STRING		return(STRING);\
<sentences>.	{fputs("-3\\n",yyout); fputs(yytext,yyout); putc('\\n',yyout); }
