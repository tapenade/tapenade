(defvar #:sys-package:colon 'parser)
(defvar #:parserttml:nbfunc 51)
(defvar #:parserttml:pattern (when (boundp '#:parserttml:pattern) #:
parserttml:pattern))
(de #:parserttml:func:f1 () (:init 4) (:pd 4) (:pd 3) (:pd 2) (:pd 1) (:push (
:arbre (loadeval (operdenom 'translator (langdenom 'ttml))) (:pop) (:pop) (:
pop) (:pop))))
(de #:parserttml:func:f2 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (loadeval
(operdenom 'rule (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f3 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (loadeval
(operdenom 'context (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f4 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (loadeval
(operdenom 'assign (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f9 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (loadeval
(operdenom 'context (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f10 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (
loadeval (operdenom 'assign (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f14 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (
loadeval (operdenom 'tree (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f15 () (:init 3) (:pd 3) (:pd 2) (:pd 1) (:push (:arbre 
(loadeval (operdenom 'annot (langdenom 'ttml))) (:pop) (:pop) (:pop))))
(de #:parserttml:func:f16 () (:init 3) (:pd 3) (:pd 2) (:pd 1) (:push (:arbre 
(loadeval (operdenom 'annot (langdenom 'ttml))) (:pop) (:pop) (:pop))))
(de #:parserttml:func:f17 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (
loadeval (operdenom 'atom (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f18 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (
loadeval (operdenom 'atom (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f19 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (
loadeval (operdenom 'atom (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f20 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (
loadeval (operdenom 'context (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f21 () (:init 2) (:pd 2) (:pd 1) (:push (:arbre (
loadeval (operdenom 'assign (langdenom 'ttml))) (:pop) (:pop))))
(de #:parserttml:func:f25 () (if (#:tree:filtrer (:pop) (vref #:parserttml:
pattern 0) ':putmetaval) (progn (:push (:getmetaval 'X)) (:renamelist (:pop) (
loadeval (operdenom 'concat (langdenom 'ttml))))) (:cerr 1)))
(de #:parserttml:func:f26 () (:push (:arbre (loadeval (operdenom 'concat (
langdenom 'ttml))))))
(de #:parserttml:func:f27 () (if (#:tree:filtrer (:pop) (vref #:parserttml:
pattern 1) ':putmetaval) (progn (:push (:getmetaval 'X)) (:renamelist (:pop) (
loadeval (operdenom 'sons (langdenom 'ttml))))) (:cerr 1)))
(de #:parserttml:func:f28 () (:push (:arbre (loadeval (operdenom 'sons (
langdenom 'ttml))))))
(de #:parserttml:func:f29 () (if (#:tree:filtrer (:pop) (vref #:parserttml:
pattern 2) ':putmetaval) (progn (:push (:getmetaval 'X)) (:renamelist (:pop) (
loadeval (operdenom 'star (langdenom 'ttml))))) (:cerr 1)))
(de #:parserttml:func:f30 () (:init 3) (:pd 3) (:pd 2) (:pd 1) (:push (:arbre 
(loadeval (operdenom 'userCall (langdenom 'ttml))) (:pop) (:pop) (:pop))))
(de #:parserttml:func:f31 () (:init 2) (:push (:arbre (loadeval (operdenom '
args (langdenom 'ttml))))) (:pd 2) (:pd 1) (:push (:arbre (loadeval (operdenom
'userCall (langdenom 'ttml))) (:pop) (:pop) (:pop))))
(de #:parserttml:func:f32 () (:init 2) (:pd 2) (:pd 1) (:push (:post (:pop) (:
pop))))
(de #:parserttml:func:f33 () (:push (:arbre (loadeval (operdenom 'rules (
langdenom 'ttml))))))
(de #:parserttml:func:f34 () (:init 2) (:pd 2) (:pd 1) (:push (:post (:pop) (:
pop))))
(de #:parserttml:func:f35 () (:push (:arbre (loadeval (operdenom 'patterns (
langdenom 'ttml))) (:pop))))
(de #:parserttml:func:f36 () (:init 2) (:pd 2) (:pd 1) (:push (:post (:pop) (:
pop))))
(de #:parserttml:func:f37 () (:push (:arbre (loadeval (operdenom 'patterns (
langdenom 'ttml))) (:pop))))
(de #:parserttml:func:f38 () (:init 2) (:pd 2) (:pd 1) (:push (:post (:pop) (:
pop))))
(de #:parserttml:func:f39 () (:init 2) (:pd 2) (:pd 1) (:push (:post (:pop) (:
pop))))
(de #:parserttml:func:f40 () (:push (:arbre (loadeval (operdenom 'args (
langdenom 'ttml))) (:pop))))
(de #:parserttml:func:f41 () (:push (:arbre (loadeval (operdenom 'args (
langdenom 'ttml))) (:pop))))
(de #:parserttml:func:f42 () (:init 2) (:pd 2) (:pd 1) (:push (:post (:pop) (:
pop))))
(de #:parserttml:func:f43 () (:push (:arbre (loadeval (operdenom 'argSigs (
langdenom 'ttml))) (:pop))))
(de #:parserttml:func:f44 () (:push (:makeatom (loadeval (operdenom 'treeArg (
langdenom 'ttml))) ())))
(de #:parserttml:func:f45 () (:push (:makeatom (loadeval (operdenom 'listArg (
langdenom 'ttml))) ())))
(de #:parserttml:func:f46 () (:push (:makeatom (loadeval (operdenom 'valueArg 
(langdenom 'ttml))) ())))
(de #:parserttml:func:f47 () (:push (:makeatom (loadeval (operdenom 'ident (
langdenom 'ttml))) (:getatom))))
(de #:parserttml:func:f50 () (:push (:makeatom (loadeval (operdenom 'variable 
(langdenom 'ttml))) (:getatom))))
(de #:parserttml:func:f51 () (:push (:makeatom (loadeval (operdenom 'value (
langdenom 'ttml))) (:getatom))))
