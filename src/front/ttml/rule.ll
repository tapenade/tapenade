; $Id: rule.ll 2320 2008-03-06 10:37:51Z llh $

(defvar *target-channel* (when (boundp '*target-channel*) *target-channel*))
(defvar *translator-name* (when (boundp '*translator-name*) *translator-name*))
(defvar *source-lang* (when (boundp '*source-lang*) *source-lang*))
(defvar *target-lang* (when (boundp '*target-lang*) *target-lang*))
(defvar *source-lang-filename* (when (boundp '*source-lang-filename*) *source-lang-filename*))
(defvar *target-lang-filename* (when (boundp '*target-lang-filename*) *target-lang-filename*))
(defvar *contexts* (when (boundp '*contexts*) *contexts*))
(defvar *catchalls* (when (boundp '*catchalls*) *catchalls*))
(defvar *matchStarTests* (when (boundp '*matchStarTests*) *matchStarTests*))
(defvar *matchStarTestsNum* (when (boundp '*matchStarTestsNum*) *matchStarTestsNum*))
(defvar *userCalls* (when (boundp '*userCalls*) *userCalls*))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(de #:ttmlrule:create (rule)
    (let ((ttmlrule (#:ttmlrule:make))
      )
      (#:ttmlrule:tree ttmlrule rule)
      (#:ttmlrule:catchallRank ttmlrule ())
      (#:ttmlrule:variables ttmlrule ())
      (#:ttmlrule:userCalls ttmlrule ())
      (#:ttmlrule:userCallsRank ttmlrule ())
      (#:ttmlrule:hasStar ttmlrule ())
      (#:ttmlrule:cutters ttmlrule ())
      (#:ttmlrule:curCutter ttmlrule ())
      (#:ttmlrule:annotCutters ttmlrule ())
      (#:ttmlrule:sourceContext ttmlrule 0)
      (#:ttmlrule:sourceKind ttmlrule ())
      (#:ttmlrule:resultKind ttmlrule ())
      (classify-lhs (tgo rule 1) ttmlrule)
      (classify-rhs (tgo rule 2) ttmlrule)
      (check-arities rule ())
      (collect-rhs-variables (tgo rule 2) ttmlrule
                 (#:ttmlrule:resultKind ttmlrule) ())
      (find-lhs-variables (tgo rule 1) ttmlrule () () ()
              (if (eq (#:ttmlrule:sourceKind ttmlrule) -1)
                 '(1 . 0)
                 (#:ttmlrule:sourceKind ttmlrule))
              ())
      (check-undefined-variables (#:ttmlrule:variables ttmlrule))
      ttmlrule
))

(de classify-lhs (lhs ttmlrule)
   (selectq (tgo lhs oper)
      (assign             (classify-lhs (tgo lhs 2) ttmlrule))
      (context            (when (eq (#:ttmlrule:sourceContext ttmlrule) 0)
                 (#:ttmlrule:sourceContext ttmlrule
                (check-context (concat (tgo lhs 1 atval)))))
              (classify-lhs (tgo lhs 2) ttmlrule))
      (annot              (#:ttmlrule:sourceKind ttmlrule 1)
              (classify-lhs (tgo lhs 1) ttmlrule))
      ((tree atom)        (#:ttmlrule:sourceKind ttmlrule 1)
                          (#:ttmlrule:opername ttmlrule (concat (tgo lhs 1 atval))))
      ((concat sons star) (#:ttmlrule:sourceKind ttmlrule -1))
))

(de classify-rhs (rhs ttmlrule)
   (selectq (tgo rhs oper)
      ((context assign)   (classify-rhs (tgo rhs 2) ttmlrule))
      ((tree atom annot)  (#:ttmlrule:resultKind ttmlrule 1))
      ((concat sons star) (#:ttmlrule:resultKind ttmlrule -1))
      (userCall           (let ((argSigs (reverse (flatten-sigs (tgo rhs 2) ()))))
                 (#:ttmlrule:resultKind ttmlrule (car argSigs))))
))

(de collect-rhs-variables (pattern ttmlrule resultKind inUserCall)
   (selectq (tgo pattern oper)
      (assign  ; no assign supported on the rhs !!!
          (print "###> TTML file error: no assignment supported on the right part of a rule")
          (collect-rhs-variables (tgo pattern 2) ttmlrule resultKind inUserCall))
      (userCall
         (when inUserCall
       (print "###> TTML file error: calls to user-functions cannot be nested"))
         (let* ((name (concat (tgo pattern 1 atval)))
        (sons (tgo pattern 3 sons))
        (place (assq name *userCalls*))
        sigs
        )
         (unless place
            (setq place (cons name
                  (#:userCall:create name (tgo pattern 2))))
        (newl *userCalls* place))
         (#:ttmlrule:userCallsRank ttmlrule
        (ifn (#:ttmlrule:userCallsRank ttmlrule) 1
             (add1 (#:ttmlrule:userCallsRank ttmlrule))))
         (#:userCall:rank (cdr place) (#:ttmlrule:userCallsRank ttmlrule))
         (when (neq (#:userCall:resultKind (cdr place)) resultKind)
           (print "###> TTML file error: user-function " name " returns a "
              (cassq (#:userCall:resultKind (cdr place))
             '((1 . "tree") (2 . "value") (-1 . "list of trees")))
              " while rule expects a "
              (cassq resultKind
             '((1 . "tree") (2 . "value") (-1 . "list of trees"))))
           (#:userCall:resultKind (cdr place) resultKind))
         (#:ttmlrule:userCalls ttmlrule
           (append1 (#:ttmlrule:userCalls ttmlrule) (cdr place)))
         (setq sigs (#:userCall:argSigs (cdr place)))
         (while sons
           (collect-rhs-variables (nextl sons) ttmlrule (nextl sigs) t))
      ))
      (context
          (check-context (concat (tgo pattern 1 atval)))
          (collect-rhs-variables (tgo pattern 2) ttmlrule resultKind inUserCall))
      (annot
          (collect-rhs-variables (tgo pattern 1) ttmlrule 1 inUserCall)
      (collect-rhs-variables (tgo pattern 3) ttmlrule 1 inUserCall))
      (tree
          (collect-rhs-variables (tgo pattern 2) ttmlrule -1 inUserCall))
      (atom
          (collect-rhs-variables (tgo pattern 2) ttmlrule 2 inUserCall))
      (star
          (#:ttmlrule:hasStar ttmlrule t)
          (let ((sons (tgo pattern sons))
        )
        (while sons
          (collect-rhs-variables (nextl sons) ttmlrule 1 inUserCall))))
      ((sons args)
          (let ((sons (tgo pattern sons))
        )
        (while sons
          (collect-rhs-variables (nextl sons) ttmlrule 1 inUserCall))))
      (concat
          (let ((sons (tgo pattern sons))
        )
        (while sons
          (collect-rhs-variables (nextl sons) ttmlrule -1 inUserCall))))
      (variable
          (let ((varname (concat (tgo pattern atval)))
        place
        )
         (setq place (assq varname (#:ttmlrule:variables ttmlrule)))
         (unless place
            (setq place
              (cons varname (#:ttmlvar:create varname resultKind)))
        (#:ttmlrule:variables ttmlrule
           (cons place (#:ttmlrule:variables ttmlrule))))
      ))
      (value)
      (t
          (print "###> TTML compiler error: collect-rhs-variables unexpected operator " (tgo pattern oper)))
))

(de #:ttmlvar:create (name resultKind)
    (let ((ttmlvar (#:ttmlvar:make))
     )
      (#:ttmlvar:name ttmlvar name)
      (#:ttmlvar:resultKind ttmlvar resultKind)
      (#:ttmlvar:starpath ttmlvar ())
      (#:ttmlvar:starlength ttmlvar ())
      (#:ttmlvar:annotpath ttmlvar ())
      (#:ttmlvar:cutter ttmlvar ())
      (#:ttmlvar:annotCutter ttmlvar ())
      (#:ttmlvar:defined ttmlvar ())
      ttmlvar
))


(de #:userCall:create (name argSigs)
    (let ((userCall (#:userCall:make))
      (rsigs (flatten-sigs argSigs ()))
      sigs sig rank
     )
      (setq sigs (reverse rsigs))
      (#:userCall:name userCall name)
      (#:userCall:rank userCall ())
      (#:userCall:resultKind userCall (car sigs))
      (#:userCall:argSigs userCall (cdr sigs))
      (#:userCall:treeRanks userCall ())
      (#:userCall:listRanks userCall ())
      (setq rank 0)
      (while (cdr rsigs) ;;Don't look at last sig, which is the type of the return value!!
     (nextl rsigs sig)
     (incr rank)
     (if (eq sig 1)
         (#:userCall:treeRanks userCall 
        (cons rank (#:userCall:treeRanks userCall)))
         (when (eq sig -1)
         (#:userCall:listRanks userCall 
            (cons rank (#:userCall:listRanks userCall)))))
      )
      userCall
))
)
(de flatten-sigs (argSigs list)
   (selectq (tgo argSigs oper)
      (argSigs    (let ((sons (tgo argSigs sons)))
            (while sons
              (setq list (flatten-sigs (nextl sons) list))))
          list)
      (treeArg    (cons 1 list))
      (listArg    (cons -1 list))
      (valueArg    (cons 2 list))
      (t          (print "###> TTML compiler error: unexpected operator in list of sigs" (tgo argSigs oper)))
))

(de #:userCall:generate (userCall translatorName)
    (selectq (#:userCall:resultKind userCall)
         (-1  (prin "ListTree *"))
         (2   (prin "AtomValue "))
         (t   (prin "Tree *")))
    (prin translatorName "U" (#:userCall:name userCall) "(")
    (let ((argSigs (#:userCall:argSigs userCall))
      argSig
     )
       (while (nextl argSigs argSig)
      (selectq argSig
           (1   (prin "Tree*"))
           (-1  (prin "ListTree*"))
           (2   (prin "AtomValue")))
      (when argSigs (prin ", "))
    ))
    (print ") ;")
)

(de find-lhs-variables (pattern ttmlrule path starpath starlength sublist annotpath)
   (selectq (tgo pattern oper)
      (context
          (check-context (concat (tgo pattern 1 atval)))
          (find-lhs-variables (tgo pattern 2) ttmlrule
                  path starpath starlength sublist annotpath)
      )
      (annot
          (when starpath (print "###> TTML file error: no annotation allowed inside a star: "
                (tgo pattern 2 atval)))
          (find-lhs-variables (tgo pattern 1) ttmlrule
                  path starpath starlength sublist annotpath)
      (setq annotpath (cons (tgo pattern 2 atval) path))
      (let ((ttmlcutter (#:ttmlAnnotCutter:create annotpath))
        )
        (#:ttmlAnnotCutter:rank ttmlcutter
           (if (null (#:ttmlrule:annotCutters ttmlrule)) 0
          (add1 (#:ttmlAnnotCutter:rank
             (cdr (car (#:ttmlrule:annotCutters ttmlrule)))))))
        (#:ttmlrule:annotCutters ttmlrule
        (cons (cons annotpath ttmlcutter) (#:ttmlrule:annotCutters ttmlrule)))
      )
      (find-lhs-variables (tgo pattern 3) ttmlrule
                  (list (cons 'annot (tgo pattern 2 atval)))
                  starpath starlength sublist annotpath)
      )
      (tree
          (find-lhs-variables (tgo pattern 2) ttmlrule
                  path starpath starlength '(1 . 0) annotpath)
      )
      (atom
          (find-lhs-variables (tgo pattern 2) ttmlrule
                  (cons 'val path) starpath starlength 2 annotpath)
      )
      (sons
          (let ((sons (tgo pattern sons))
        (rk (car sublist))
        son
        )
         (while (nextl sons son)
        (find-lhs-variables son ttmlrule (cons rk path) starpath starlength 1 annotpath)
            (incr rk))
      ))
      (concat
          (let ((elems (tgo pattern sons))
        offsets offset elem
        )
         (setq offsets (check-offsets elems sublist))
         (while (nextl elems elem)
            (nextl offsets offset)
        ;;TODO: here refine to introduce the "cut", declare it, and use it in the path's
        (find-lhs-variables elem ttmlrule path starpath starlength offset annotpath))
      ))
      (star
          (let ((sons (tgo pattern sons))
        (rk 1)
        period son
        )
         (setq period (length sons))
         (when (details-in-star pattern)
           (incr *matchStarTestsNum*)
           (newl *matchStarTests*
             (#:matchStarTest:create
              *matchStarTestsNum* pattern period sublist))
         )
         (while (nextl sons son)
            (find-lhs-variables son ttmlrule
                    (list rk) (cons sublist path) period 1 annotpath)
        (incr rk))
      ))
      (assign
          (find-lhs-variables (tgo pattern 1) ttmlrule path starpath starlength sublist annotpath)
          (find-lhs-variables (tgo pattern 2) ttmlrule path starpath starlength sublist annotpath)
      )
      (variable
          (let ((varname (concat (tgo pattern atval)))
        ttmlvar cutPath
        )
         (setq ttmlvar (cassq varname (#:ttmlrule:variables ttmlrule)))
         (when ttmlvar
            (#:ttmlvar:defined ttmlvar t)
            (#:ttmlvar:sourceKind ttmlvar (if (consp sublist) -1 sublist))
        (#:ttmlvar:path ttmlvar
                (if (consp sublist) (cons sublist path) path))
        (#:ttmlvar:starpath ttmlvar starpath)
        (#:ttmlvar:starlength ttmlvar starlength)
        (when (setq cutPath (needsCutter ttmlvar))
           (setq ttmlcutter (cassq cutPath (#:ttmlrule:cutters ttmlrule)))
           (unless ttmlcutter
             (setq ttmlcutter (#:ttmlcutter:create cutPath))
             (#:ttmlcutter:rank ttmlcutter
            (if (null (#:ttmlrule:cutters ttmlrule)) 0
              (add1 (#:ttmlcutter:rank
                 (cdr (car (#:ttmlrule:cutters ttmlrule)))))))
             (#:ttmlrule:cutters ttmlrule
                     (cons (cons cutPath ttmlcutter)
                           (#:ttmlrule:cutters ttmlrule)))
           )
           (#:ttmlvar:cutter ttmlvar ttmlcutter)
        )
        (#:ttmlvar:annotpath ttmlvar annotpath)
        (when annotpath
          (setq ttmlcutter (cassq annotpath (#:ttmlrule:annotCutters ttmlrule)))
          (#:ttmlvar:annotCutter ttmlvar ttmlcutter)
        )
         )
      ))
      (value
      )
      (userCall
          (print "###> TTML file error: no call to user-function allowed on the left hand side of a rule.")
      )
      (t  (print "###> TTML compiler error: find-lhs-variables unexpected operator "
         (tgo pattern oper))
      )
))

(de check-undefined-variables (variables)
    (let (variable ttmlvar)
      (while (nextl variables variable)
     (setq ttmlvar (cdr variable))
     (when (not (#:ttmlvar:defined ttmlvar))
        (print "###> TTML file error: undefined variable on the right-hand side: "
           (car variable)
)))))

(de #:ttmlcutter:create (cutPath)
    (let ((result (#:ttmlcutter:make))
     )
      (#:ttmlcutter:cutPath result cutPath)
      (#:ttmlcutter:on result ())
      result
))

(de needsCutter (ttmlvar)
    (let ((path (#:ttmlvar:path ttmlvar))
      )
      (when (and (consp (car path)) (neq (caar path) 'annot) (neq (cdar path) 0))
    path
)))

(de #:ttmlAnnotCutter:create (annotpath)
    (let ((result (#:ttmlAnnotCutter:make))
     )
      (#:ttmlAnnotCutter:annotPath result annotpath)
      (#:ttmlAnnotCutter:on result ())
      result
))

(de check-context (context-name)
   (let ((place (assq context-name *contexts*))
    )
      (unless place
     (setq place (cons context-name (add1 (cdr (car *contexts*)))))
     (setq *contexts* (cons place *contexts*))
      )
      (cdr place)
))

;;;Returns a list of pairs of ints, one pair for each pattern in "patterns",
;;; and each pair containing first the rank of the 1st element matching the
;;; corresponding pattern and second the rank of the last matching element.
(de check-offsets (patterns offset)
    (let ((in-patterns patterns)
      (top-lengths (list t))
      tail-lengths pattern
      (begin (if (consp offset) (or (car offset) 1) 1))
      (end   (if (consp offset) (or (cdr offset) 0) 0))
      next-begin
     )
       (setq tail-lengths top-lengths)
       (while (nextl in-patterns pattern)
      (setq tail-lengths (placdl tail-lengths (pattern-length pattern -1))))
       (setq tail-lengths (cdr top-lengths))
       (while (and tail-lengths (car tail-lengths))
      (setq next-begin (+ begin (car tail-lengths)))
      (rplaca tail-lengths (cons begin (+ begin (sub1 (car tail-lengths)))))
      (setq begin next-begin)
      (setq tail-lengths (cdr tail-lengths))
       )
       (when tail-lengths
      (setq end (terminate-tail-lengths (cdr tail-lengths) end))
      (rplaca tail-lengths (cons begin end)))
       (cdr top-lengths)
))
(de terminate-tail-lengths (lengths end)
   (ifn lengths
       end
       (setq end (terminate-tail-lengths (cdr lengths) end))
       (rplaca lengths (cons (- end (sub1 (car lengths))) end))
       (sub1 (car (car lengths)))
))
(de pattern-length (pattern kind)
   (selectq (tgo pattern oper)
      ((tree atom)   1)
      (concat
          (let ((elems (tgo pattern sons))
        (length 0)
        elem elem-length
        )
         (while (nextl elems elem)
           (setq elem-length (pattern-length elem -1))
           (setq length (when (and length elem-length) (+ length elem-length))))
         length
      ))
      (star          ())
      (sons          (length (tgo pattern sons)))
      (assign        (pattern-length (tgo pattern 2) kind))
      (variable      (if (eq kind 1) 1 ()))
      (t  ; no userCall or context supported on the lhs !!!
          (print "###> TTML compiler error: pattern-length unexpected operator " (tgo pattern oper))
      ())
))

(de #:ttmlrule:display (ttmlrule)
    (print "Rule on " (#:ttmlrule:opername ttmlrule)
      " [" (#:ttmlrule:sourceKind ttmlrule) ">" (#:ttmlrule:resultKind ttmlrule) "] ")
    (let ((ttmlvars (#:ttmlrule:variables ttmlrule))
      ttmlvar
      )
       (while ttmlvars
      (setq ttmlvar (cdr (nextl ttmlvars)))
      (prin "  -- ")
      (#:ttmlvar:display ttmlvar)
      (print)
    ))
)

(de #:ttmlvar:display (ttmlvar)
    (prin (#:ttmlvar:name ttmlvar) " ==> ["
       (#:ttmlvar:sourceKind ttmlvar) ">" (#:ttmlvar:resultKind ttmlvar) "] ")
    (when (#:ttmlvar:starlength ttmlvar)
    (prin "Period " (#:ttmlvar:starlength ttmlvar) " in " (#:ttmlvar:starpath ttmlvar)))
    (prin (#:ttmlvar:path ttmlvar))
)

(de #:matchStarTest:create (rank pattern period sublist)
    (let ((result (#:matchStarTest:make))
      )
      (#:matchStarTest:rank result rank)
      (#:matchStarTest:pattern result pattern)
      (#:matchStarTest:period result period)
      (#:matchStarTest:sublist result sublist)
      result
))

(de #:matchStarTest:generate (matchStarTest)
    (tg-print "int matchStar" (#:matchStarTest:rank matchStarTest)
          "(ListTree *sons) {")
    (tg-print "    int matches = 0 ;")
    (tg-print "    int length = listTreeLength(sons) ;")
    (tg-print "    while ((matches) && (length>" 
          (sub1 (sub (#:matchStarTest:period matchStarTest)
             (cdr (#:matchStarTest:sublist matchStarTest))))
          ")) {")
    (let ((sons (tgo (#:matchStarTest:pattern matchStarTest) sons))
      (init-path "sons")
      son)
      (while (nextl sons son)
    (while (memq (tgo son oper) '(context assign))
         (setq son (tgo son 2)))
    (unless (eq (tgo son oper) 'variable)
      (tg-prin  "      if (matches")
      (generate-lhs-match son "sons->tree" () ())
      (tg-print ")")
      (tg-print "        matches = 1 ;")
    )
    (tg-print  "      sons = sons->next; length-- ;")
      )
    )
    (tg-print "    }")
    (tg-print "    return matches ;")
    (tg-print "}")
)

(de details-in-star (pattern)
    (let ((sons (tgo pattern sons))
      (has-details ())
      son
      )
       (while (nextl sons son)
      (while (memq (tgo son oper) '(context assign))
         (setq son (tgo son 2)))
      (unless (eq (tgo son oper) 'variable)
         (setq has-details t))
       )
       has-details
))

;;;Checks the arities used in the patterns wrt the actual arities
;;; of the actual language operators.
;;;For each pattern, returns a pair of the minimal tree-length
;;; of the pattern, cons'ed with a boolean () when fixed length,
;;; and 't for variable length (but >= the minimal length).
(de check-arities (pattern side)
    (selectq (tgo pattern oper)
        (rule
        (check-arities (tgo pattern 1) 'source)
        (check-arities (tgo pattern 2) 'target)
        ()
        )
        (assign
            (check-arities (tgo pattern 2) side)
        )
        (userCall
            (check-arities (tgo pattern 3) 'target)
        (cons 0 t)
        )
        (context
            (check-arities (tgo pattern 2) 'source)
            (cons 0 t)
        )
        (annot
            (check-arities (tgo pattern 1) side)
            (check-arities (tgo pattern 3) side)
            (cons 1 ())
        )
        ((tree atom)
         (let ( (oper (if (eq side 'source)
                    (sourceOper (tgo pattern 1))
                    (targetOper (tgo pattern 1))))
                refArity usedArity
              )
              (setq refArity (#:oper:arity oper))
              (setq usedArity (check-arities (tgo pattern 2) side))
              (when (aritiesConflict refArity usedArity)
                    (print "###> TTML file error: bad arity for " side " operator """ (#:oper:name oper)
              """ (" (car usedArity) " vs expected " refArity ")"))
         )
         (cons 1 ())
        )
        (star
         (let   ((sons (tgo pattern sons)))
                (while sons
                    (check-arities (nextl sons) side)))
         (cons 0 t)
        )
        (sons
             (let ((sons (tgo pattern sons))
               len
            )
            (setq len (length sons))
            (while sons
              (check-arities (nextl sons) side))
            (cons len ()))
        )
        (concat
             (let ((sons (tgo pattern sons))
               (len 0)
               (more ())
               usedArity
               )
            (while sons
              (setq usedArity (check-arities (nextl sons) side))
              (setq len (add len (car usedArity)))
              (setq more (or more (cdr usedArity))))
            (cons len more)
         )
       )
       (args
            (let ((sons (tgo pattern sons))
            )
            (while sons
              (check-arities (nextl sons) side)))
            (cons 0 ())
       )
       (t
         (cons 0 ())
       )
    )
)

(de aritiesConflict (refArity usedArity)
    (and (ge refArity 0)
     (if (cdr usedArity)
        (lt refArity (car usedArity))
        (not (equal refArity (car usedArity))))))
