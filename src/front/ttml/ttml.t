*VERSION   2

*LANGUAGE           ttml  0

*PHYLA              17        24
EVERY                                        25
24  23  22  21  20  19  18  17  16  15  14  13  12  11  10  9  8  7  6  5  4  
3  2  1  -1  
COMMENT_S                                    2
24  22  
COMMENT                                      2
23  22  
ValuePattern                                 4
21  12  20  22  
ArgSig                                       4
15  16  17  22  
ArgSigs                                      2
14  22  
Arg                                          9
10  11  6  4  5  12  20  21  22  
Args                                         2
13  22  
Variable                                     2
20  22  
GeneralPattern                               5
4  5  12  20  22  
TreePattern                                  8
10  11  6  4  5  12  20  22  
ListPattern                                  8
7  8  9  4  5  12  20  22  
Pattern                                      19
4  5  12  20  7  8  9  4  5  12  20  10  11  6  4  5  12  20  22  
Rule                                         2
3  22  
Rules                                        2
2  22  
AnyIdent                                     3
19  20  22  
Ident                                        2
19  22  

*OPERS              24
translator                                   4    16   16   16   15   
rules                                        14   14   
rule                                         2    13   13   
context                                      2    17   13   
assign                                       2    9   13   
annot                                        3    13   17   13   
concat                                       14   12   
sons                                         14   11   
star                                         14   11   
tree                                         2    17   12   
atom                                         2    17   4   
userCall                                     3    17   6   8   
args                                         14   7   
argSigs                                      14   5   
treeArg                                      0    3                  
listArg                                      0    3                  
valueArg                                     0    3                  
patterns                                     14   13   
ident                                        0    1                  
variable                                     0    1                  
value                                        0    1                  
meta                                         0    0                  
comment                                      0    1                  
comment_s                                    14   3   

*PROPERTI           1         24
LISTNODE            8
2  7  8  9  13  14  18  24  

*LIMITS
22  23  24  

*KEYWORDS

*EXTERNES

*END
