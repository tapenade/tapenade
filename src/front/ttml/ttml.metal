definition of ttml is
   rules
      <file> ::= <anyIdent> "(" <anyIdent> "=>" <anyIdent> ")" <rules> ;
         translator (<anyIdent>.1, <anyIdent>.2, <anyIdent>.3, <rules>)
      <rule> ::= <anyPattern> "->" <anyPattern> ";" ;
         rule (<anyPattern>.1, <anyPattern>.2)
      <anyPattern> ::= <ident> "::" <anyPattern> ;
         context (<ident>, <anyPattern>)
      <anyPattern> ::= <variable> ":=" <anyPattern> ;
         assign (<variable>, <anyPattern>)
      <anyPattern> ::= <variable> ;
         <variable>
      <anyPattern> ::= <userFunctionCall> ;
         <userFunctionCall>
      <anyPattern> ::= <realTreePattern> ;
         <realTreePattern>
      <anyPattern> ::= <realListPattern> ;
         <realListPattern>
      <treePattern> ::= <ident> "::" <treePattern> ;
         context (<ident>, <treePattern>)
      <treePattern> ::= <variable> ":=" <treePattern> ;
         assign (<variable>, <treePattern>)
      <treePattern> ::= <variable> ;
         <variable>
      <treePattern> ::= <userFunctionCall> ;
         <userFunctionCall>
      <treePattern> ::= <realTreePattern> ;
         <realTreePattern>
      <realTreePattern> ::= <ident> <listPattern> ;
         tree (<ident>, <listPattern>)
      <realTreePattern> ::=
         <realTreePattern> "^" "{" <ident> ":" <treePattern> "}" ;
         annot (<realTreePattern>, <ident>, <treePattern>)
      <realTreePattern> ::= <variable> "^" "{" <ident> ":" <treePattern> "}" ;
         annot (<variable>, <ident>, <treePattern>)
      <realTreePattern> ::= <ident> ":" <value> ;
         atom (<ident>, <value>)
      <realTreePattern> ::= <ident> ":" <variable> ;
         atom (<ident>, <variable>)
      <realTreePattern> ::= <ident> ":" <userFunctionCall> ;
         atom (<ident>, <userFunctionCall>)
      <listPattern> ::= <ident> "::" <listPattern> ;
         context (<ident>, <listPattern>)
      <listPattern> ::= <variable> ":=" <listPattern> ;
         assign (<variable>, <listPattern>)
      <listPattern> ::= <variable> ;
         <variable>
      <listPattern> ::= <userFunctionCall> ;
         <userFunctionCall>
      <listPattern> ::= <realListPattern> ;
         <realListPattern>
      <realListPattern> ::= "[" <listPatterns> "]" ;
         let patterns-list (X) = <listPatterns> in
            concat-list (X)
      <realListPattern> ::= "[" "]" ;
         concat-list (())
      <realListPattern> ::= "(" <treePatterns> ")" ;
         let patterns-list (X) = <treePatterns> in
            sons-list (X)
      <realListPattern> ::= "(" ")" ;
         sons-list (())
      <realListPattern> ::= "(" <treePatterns> ")" "*" ;
         let patterns-list (X) = <treePatterns> in
            star-list (X)
      <userFunctionCall> ::=
         "!!" <ident> "(" <argSigs> ")" "(" <argPatterns> ")" ;
         userCall (<ident>, <argSigs>, <argPatterns>)
      <userFunctionCall> ::= "!!" <ident> "(" <argSigs> ")" "(" ")" ;
         userCall (<ident>, <argSigs>, args-list (()))
      <rules> ::= <rules> <rule> ;
         rules-post (<rules>, <rule>)
      <rules> ::= ;
         rules-list (())
      <listPatterns> ::= <listPatterns> "," <listPattern> ;
         patterns-post (<listPatterns>, <listPattern>)
      <listPatterns> ::= <listPattern> ;
         patterns-list ((<listPattern>))
      <treePatterns> ::= <treePatterns> "," <treePattern> ;
         patterns-post (<treePatterns>, <treePattern>)
      <treePatterns> ::= <treePattern> ;
         patterns-list ((<treePattern>))
      <argPatterns> ::= <argPatterns> "," <treePattern> ;
         args-post (<argPatterns>, <treePattern>)
      <argPatterns> ::= <argPatterns> "," <value> ;
         args-post (<argPatterns>, <value>)
      <argPatterns> ::= <treePattern> ;
         args-list ((<treePattern>))
      <argPatterns> ::= <value> ;
         args-list ((<value>))
      <argSigs> ::= <argSigs> "," <argSig> ;
         argSigs-post (<argSigs>, <argSig>)
      <argSigs> ::= <argSig> ;
         argSigs-list ((<argSig>))
      <argSig> ::= "1" ;
         treeArg ()
      <argSig> ::= "*" ;
         listArg ()
      <argSig> ::= ":" ;
         valueArg ()
      <ident> ::= %IDENT ;
         ident-atom(%IDENT)
      <anyIdent> ::= <ident> ;
         <ident>
      <anyIdent> ::= <variable> ;
         <variable>
      <variable> ::= %VARIABLE ;
         variable-atom(%VARIABLE)
      <value> ::= %STRING ;
         value-atom(%STRING)

   abstract syntax
      translator  -> AnyIdent AnyIdent AnyIdent Rules ;
      Ident  ::= ident ;
      AnyIdent  ::= ident variable ;
      Rules  ::= rules ;
      rules  -> Rule * ... ;
      Rule  ::= rule ;
      rule  -> Pattern Pattern ;
      Pattern  ::= GeneralPattern ListPattern TreePattern ;
      ListPattern  ::= concat sons star GeneralPattern ;
      TreePattern  ::= tree atom annot GeneralPattern ;
      GeneralPattern  ::= context assign userCall Variable ;
      Variable  ::= variable ;
      context  -> Ident Pattern ;
      assign  -> Variable Pattern ;
      annot  -> Pattern Ident Pattern ;
      concat  -> ListPattern * ... ;
      sons  -> TreePattern * ... ;
      star  -> TreePattern * ... ;
      tree  -> Ident ListPattern ;
      atom  -> Ident ValuePattern ;
      userCall  -> Ident ArgSigs Args ;
      Args  ::= args ;
      args  -> Arg * ... ;
      Arg  ::= TreePattern value ;
      ArgSigs  ::= argSigs ;
      argSigs  -> ArgSig * ... ;
      ArgSig  ::= treeArg listArg valueArg ;
      treeArg  -> implemented as SINGLETON ;
      listArg  -> implemented as SINGLETON ;
      valueArg  -> implemented as SINGLETON ;
      ValuePattern  ::= value userCall Variable ;
      patterns  -> Pattern * ... ;
      ident  -> implemented as STRING ;
      variable  -> implemented as STRING ;
      value  -> implemented as STRING ;
end definition
