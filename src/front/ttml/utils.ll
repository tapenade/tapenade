; $Id: utils.ll 1015 2006-04-06 08:29:48Z cmassol $
(eval-when (load eval compile)

(dmd #:ttml:oper-name (tree) `(#:operator:name (#:tree:operator ,tree)))

(dmd tgo (tree . args)
  (let ((res tree)
        arg
       )
     (while (setq arg (nextl args))
         (setq res (mcons (selectq arg
                                (atval  '#:tree:atom_value)
                                (sons   '#:tree:sons)
                                (oper   '#:ttml:oper-name)
                                (up     '#:tree:father)
                                (t      (if (numberp arg) '#:tree:down 'identity))
                          )
                          res
                          (when (numberp arg) (list arg))
     )))
     res
))

)

(de sourceOper (identTree)
    (#:language:name-oper *source-lang* (tgo identTree atval)))

(de targetOper (identTree)
    (#:language:name-oper *target-lang* (tgo identTree atval)))

(de tg-print str
    (with ((outchan *target-channel*)
	   (rmargin 150))
        (apply 'print str)))

(de tg-prin str
    (with ((outchan *target-channel*)
	   (rmargin 150))
        (apply 'prinflush str)))

(de readnextline (channel)
    (let ((str
	   (tag eof
		(with ((inchan channel))
		      (readstring)))))
       (unless (eq str channel) str)))
	   

   


