; $Id: defstructs.ll 1015 2006-04-06 08:29:48Z cmassol $
(eval-when (eval load compile)
  (defstruct #:language
    name         ;name of the language
    oper-list    ;Temporary: list of all operators
    operators    ;Vector of all operators
    nbOpers      ;Number of operators
))

(eval-when (eval load compile)
  (defstruct #:oper
    name       ;name of the operator
    rank       ;rank of the operator in the C representation
    arity
))

(eval-when (eval load compile)
   (defstruct #:ttmlrule
      tree
      sourceContext
      sourceKind
      resultKind
      opername
      catchallRank
      variables
      userCalls
      userCallsRank
      hasStar
      cutters
      curCutter
      annotCutters
))

(eval-when (eval load compile)
   (defstruct #:ttmlvar
      name
      sourceKind
      resultKind
      path
      starpath
      starlength
      annotpath
      cutter
      annotCutter
      defined
))

(eval-when (eval load compile)
   (defstruct #:ttmlcutter
     rank
     cutPath
     on
))

(eval-when (eval load compile)
   (defstruct #:ttmlAnnotCutter
     rank
     annotPath
     on
))

(eval-when (eval load compile)
   (defstruct #:userCall
     name
     rank
     argSigs
     resultKind
     inAtom
     treeRanks
     listRanks
))

(eval-when (eval load compile)
   (defstruct #:matchStarTest
     rank
     pattern
     period
     sublist
))
