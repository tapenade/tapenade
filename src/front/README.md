Compilation avec centaur sur rataxes2
=====================================
Création du parser ttml
-----------------------
```
cd ... tapenade
./gradlew

cd ..
ln -s /proj/tropics/home/centaur/dist20exp .CENTAUR_SRC

ssh rataxes2

lire: /proj/croap/README.txt

cd tapenade/src/front/ttml
./build-ttml.sh
```

Création de f2il.c
------------------

```
cd ../src/frontf
make cleans
make -f Makefilecentaur
```

