static char yaccbib_id[]="$Id$";
#ifdef NOSELECT
#define _rcsid(msg) "msg $"
static char noselec[]=_rcsid($Id: yaccbib.c: no select);
#endif
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>

#include <sys/stat.h>

#ifndef NOSELECT
#ifndef FD_SET
#include <sys/select.h>
#endif
#ifndef S_ISSOCK
#define	S_ISSOCK(m)	(((m)&S_IFMT) == S_IFSOCK)
#endif
#endif /* NOSELECT */

extern int yyleng;

extern char yytext[];

/*
 * yytext may be a (char *) or a (char []) when using flex or lex
 * With some new version of flex (at least 2.5) its type may be 
 * forced by an %array or %pointer declaration in the lex source file.
 * To be the most portable, don't compile separately this file, but 
 * include it the end of the lexL.c file, then don't declare yytext 
 * here.
 */
extern FILE *yyin, *yyout;

#ifdef DEBUG
#define show(val,fmt) fprintf(stderr, "val = fmt\n\r", val)
#else
#define show(val,fmt) 
#endif

#ifndef NOSELECT
void_flushfilbuf(p) FILE *p;
{
  /* This function replaces _filbuf in order to achieve two goals:
     
     - flushing yyout when on the point of beeing blocked reading
     
     - terminating as soon as the process eventually receiving on yyout
     terminates. This is the case in the communication:
     emacs --> parser --> centaur
     
     This only permits to redefine getc with the "define _filbuf" that exists 
     in the <lang>.c file.

     One assume that yyout is constant when using the static flag out_fd

     One should perhaps not exit directly when EOF is noticed on yyout, but
     pass the control to somebody else.

     */

#ifndef NOSELECT
  fd_set readfds;
  static int out_fd = -2;	/* -2 means uninitialized, -1 not valid */
#endif

  fflush(yyout);

#ifndef NOSELECT
#ifndef FD_SETSIZE
/* This is wrong, but what the heck, system that don't define FD_SETSIZE
 * don't deserve any better.
 */
#define SELECT_MAXFDS getdtablesize()
#else
/* FD_SETSIZE can be (much) bigger than the value returned by getdtablesize,
 * which is a performance penalty.  However, getdtablesize can return a value
 * bigger then FD_SETSIZE and this program breaks.
 */
#define SELECT_MAXFDS FD_SETSIZE
#endif

  show(fileno(yyin), %d);
  FD_ZERO(&readfds); FD_SET(fileno(yyin), &readfds);

  switch (out_fd) {
  case -2:
    {
      struct stat buf;

      out_fd = fileno(yyout);
      show(out_fd,%d);
      if (fstat(out_fd, &buf) == 0) {
	show(buf.st_mode,%x);
	if (isatty(0) ||
	    (
	     (buf.st_mode != 0) && /* On Ultrix */
	     (! S_ISSOCK(buf.st_mode)))) {
	  out_fd = -1;
	  show(out_fd,invalide %d);
	}
      } else {
	out_fd = -1;
      }
    }
  case -1:
    break;
    default:
    FD_SET(out_fd, &readfds);
  }

  switch (select(SELECT_MAXFDS, &readfds, NULL, NULL, NULL)) {
  case 0:
    break;
  case -1:
    perror("select");
    break;
    default:
    show(readfds, %x);
    if((out_fd > 0) && 
       /* In case yyin == yyout, skip the test on yyout.  This is needed to
	  prevent reading a character on yyin with th efolowing read, but is 
	  also necessary to prevent a read error in a sucha case: when 
	  both yyin an yyout reference the same socket. Why?
	  */
       (! FD_ISSET(fileno(yyin), &readfds)) && 
       FD_ISSET(out_fd, &readfds)) {
      char c;

      if(read(&c, out_fd, 1) <= 0){
	show(out_fd, %d);
#ifdef DEBUG
	perror("read");
#endif
	show(0, eof on out_fd);
	exit(0);		/* Refine? */
      }
    }
  }
#endif

  return(_filbuf(p));
}
#endif /* NOSELECT */

int redirect_input(str, from, to)
     char *str;
     int from, to;
{
  char *ptr;
  FILE *F;

  for(ptr = str+to-1; ptr > str+from; ptr--){
    if ((*ptr != '\n') && (*ptr != ' ') && (*ptr != '\t')) {
      break;
    }
  }
  *(ptr+1) = '\0';
  if ((F = fopen(str+from, "r")) == NULL) {
    fputs("-8\n", yyout); fputs(str+from, yyout); putc('\n', yyout);
    return(0);
  } 
  yyin = F;
  return(1);
}

int yaccupperc( lower)
int lower;
{
	if ( lower >= 'a' && lower <= 'z')
				return ( lower + 'A' -'a' ) ;
       else
		return (lower);
}


int uppercases ()
{ int i;
 	for (i=0 ;  i<yyleng ;  i++) yytext[i] = yaccupperc(yytext [i]);
}

/*
 * From ytypolbib.c
 */
int yacclowerc(upper)
 int upper;
{ if (upper >= 'A' && upper <= 'Z')  return(upper-'A'+'a');
  else return(upper);
}

int lowercases()
{ int i;
  for (i=0 ; i<yyleng ; i++) yytext[i] = yacclowerc(yytext[i]);
}

int adjuststring()
/* Special case for strings: cancel the starting and ending quote, and also
   double quotes are replaced by a simple one */
{int i,j;
 j=0;
 for  (i=1 ; i<yyleng - 1 ; i++)
	{if (yytext[i] == '\'' ) i++ ;
	 yytext[j] = yytext[i] ;
	 j++;
	};
 yyleng = j;
 yytext[yyleng] = '\0';
}

int delete_rc ()
{ int i,j;
        j = 0;
 	for (i=0 ;  i<=yyleng ;  i++) 
            if (yytext[i] != '\n') {yytext[j] = yytext [i] ; j++ ; } ;
        yyleng = j;
}

int shiftleft()
{ int i;
	for (i=0 ;  i<yyleng ;  i++) yytext[i] = yytext [i+1];
        yytext[yyleng -1] = '\0';
        yyleng = yyleng -1 ;
}

int multi_shiftleft(int n);

int rm_leading_spaces()
{
  char *s;
  for (s = yytext; isspace(*s); s++);
  multi_shiftleft(s - (char *) yytext);
}

int multi_shiftleft(int n) { 
  int i;
  if (n > 0) {
    yyleng -= n;
    for (i=0 ;  i<yyleng ;  i++) yytext[i] = yytext [i+n];
    yytext[yyleng] = '\0';
  }
}

int suplast()
{
yytext[yyleng-1] = '\0';
yyleng = yyleng -1 ;
}

/* This function will not be used by comp_metal after comp_metal:1.34 */
int action(n)
int n;
{
fprintf(yyout,"%d\n",n);
}

/* For flex 2.3 */
#ifndef yywrap

int yywrap()
{
return(1);
}
#endif

int yyerror(  )
{
stbSyntaxError() ;
/* fprintf(yyout,"-4\n"); */
}

/* other prototypes needed by yaccfortran.c: */
void NeedOffset() ;
void NeedKeyword(int) ;
void LexReclassify() ;
void NeedInImplicit(int) ;
void NeedLetter(int) ;
void NeedColoninGould(int) ;
void CommonFlag(int) ;
void OptComma(int) ;
void NeedOpKeyword(int) ;
void InDecl(int) ;
void DoWhile(int) ;
void OnlyInt(int) ;
void DoLabel() ;
void InIO(int) ;
void NeedTypeKeyword(int) ;
void SizeComma(int) ;
void setScannerOpenMP(int) ;
void InitLexer() ;
#include "cvtp.h"
void showTreeAsDot(Tree* tree, FILE* out);
