/* $Id: pointers.h,v 1.1 10/.0/.1 .1:.0:.1 vmp Exp $ */
#ifndef CVTP_LOADED
#define CVTP_LOADED

#include "cvtp.h"

/******************************* Public Forward Declarations: */

void tagPointers(Tree* tree);

#endif
