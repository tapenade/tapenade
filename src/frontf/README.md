Fortran parser (fortranParser)
==============================

Fortran parser is defined in `src/frontf`.

- Dependencies

    - gcc [https://gcc.gnu.org/](https://gcc.gnu.org/)
    - i686-w64-mingw32-gcc cross compiler for Windows
    - Docker [https://www.docker.com/](https://www.docker.com/)

- Compilation

To build a fortranParser executable from tapenade directory:

`./gradlew frontf`

`fortranParser` is created in a subdirectory of `tapenade/bin/`
according the operating system name: linux or mac.

`./gradlew frontfdocker`

`fortranParser` is created in `tapenade/bin/linux`. 
A Docker image of fortranParser is pushed in
`registry.gitlab.inria.fr/tapenade/tapenade/fortranparser`.
