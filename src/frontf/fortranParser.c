static char fPid[]="GITVERSION";

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#ifdef YYDEBUG
extern  int     yydebug;
#endif

int (*yylex)();	/* the scanner function assigned in InitLexer */
#ifndef MAXTOKLEN
#define MAXTOKLEN 500			/* max size of a token */
#endif

#ifndef True
#define True 1
#define False 0
#endif /* True */

#ifndef SCANNERVARIABLES
#define SCANNERVARIABLES 1
/* ---------------------------------------------------------------------*/
/* Top Layer (fortranprotocol.c) to set the protocol for communication  */
/* with the Parser => Parser Server					*/
/* There is three types of Parser :					*/
/*	- Header Parser : which parses only the Header & the Main Unit	*/
/*	- Body Parser	:   "     "     "   the wanted Unit		*/
/*	- Fortran Parser:   "     "    a whole file			*/
/* ---------------------------------------------------------------------*/

/* Body Parser not implemented : Perhaps needs more than only scanning to */
/* find unit bodies							  */


extern int Header_Parser;	/* say whether we are in mode Header_Parser */
extern int Body_Parser;	/* say whether we are in mode Body_Parser   */
				/* default is Fortran Parser		    */

extern int Tags_Header; /*Indicate whether we are on a Unit Header : Viland */
                                    /* we are a priori not in a Unit Header  */
extern int Func_Header; /* Indicate that we read a Basic Type, Consequently */
                                         /* the next token may be a function */
                                    /* we are a priori not with a Basic Type */
extern int Sub_Unit;   /* Indicate whether we are in a SubUnit : Viland     */
                                    /* we are a priori not in a SubUnit      */
#endif /* SCANNERVARIABLES */

extern void setScannerDialect();
extern void setScannerLineSize();
extern void setScannerFreeFormat();

FILE *yyin = (FILE *) 0, *yyout = (FILE *) 0;

char *INPUTFILENAME;

extern int showtokens;

#include "type.h"

extern parserOption parserOptions[MAX_PARSER_OPTIONS];

#include "yaccfortran.c"
#include "tablesfortran.c"
#include "../../build/generated-src/c/main/front/tablesil.c"
#include "../front/stackTreeBuilder.h"
#include "../front/cvtp.h"
#include "f2il.h"

char *directory = "./";
char *fileseparator = "/";
/* par defaut, pas de servlet, on garde les pathnames des includes en entier */
char *includetype = "tapenade";
char *includedirlist[10];
int includedirNb = 0;

/* Global var to force printing of debugging messages during parsing: */
int debugFortranParser = 0;

static FILE *outputFile;

/* dans le cas windows sous cygwin, java.delimitor est "\"
   il faut remettre "/" */
void checkSeparator() {
  char *pwd;
  if (fileseparator[0] == '\\') {
    pwd = getenv("PWD");
    if (pwd != NULL) {
      if (strlen(pwd) > 0) {
       if (pwd[0] == '/') {
          fileseparator[0] = pwd[0];
       }
      }
    }
  }
}

char *getDirectory(char *fileName) {
  char sep = fileseparator[0];
  char *dir;
  int index = -1;
  int i;
  for (i = strlen(fileName)-1; (i >= 0 && index == -1); i--) {
    if (fileName[i] == sep)
      index = i;
  }
  dir = (char *)malloc(sizeof(char)*(index+2));
  for (i=0;i<=index;i++) dir[i]=fileName[i];
  dir[index+1]='\0';
  return dir;
}

char *getFileName(char *fileName) {
  char sep = fileseparator[0];
  int index = -1;
  int i;
  for (i = strlen(fileName)-1; (i >= 0 && index == -1); i--) {
    if (fileName[i] == sep)
      index = i;
  }
  /*return &fileName[index+1];*/
  return fileName+index+1;
}

Tree *parseF(char *inputFilePathAndName, char *phylum) {
  FILE *inputFile;
  char *tmp;
#ifdef YYDEBUG
  if (debugFortranParser) yydebug = 1;
#endif
#ifdef SHOWTOKENS
  if (debugFortranParser) showtokens = 1;
#endif
  INPUTFILENAME = getFileName(inputFilePathAndName);
  inputFile=fopen(inputFilePathAndName,"r");
  if (inputFile == NULL) {
    fprintf(stdout,"%d\n", ilParsingErrorCode); 
    fprintf(stdout,"Fortran Parser: Interrupted analysis\n");
    fprintf(stderr,"Fortran Parser: %s: No such file or directory\n", inputFilePathAndName);
    return NULL;
  }
  
  Tree *parsedTree = NULL;
  parserOption *curOption = parserOptions;
  /* Force not to pad with white spaces at the end of multi-line strings: */
  curOption->name = "padToEol";
  curOption->val = "()";
  curOption++;
  if (phylum) {
    curOption->name = "phylum";
    curOption->val = phylum;
    curOption++;
  }
  yyin = inputFile;
  stbInit();
  stbLanguage(fortranOperators,
              fortranNullTreeCode,
              fortranLabelTreeCode,
              fortranLabelsTreeCode,
              fortranLabstatTreeCode,
              fortranCommentTreeCode,
              fortranCommentsTreeCode,
              fortranIntCstTreeCode,
              fortranPppLineTreeCode,
              fortranStringTreeCode,
              fortranIdentTreeCode);
  InitLexer();
  yyparse();
  if (stbEmptyStack()) {
    fprintf(stdout,"%d\n", ilParsingErrorCode); 
    fprintf(stdout,"Fortran Parser: Interrupted analysis\n");
    fprintf(stderr,"Fortran Parser: failed on: %s\n", inputFilePathAndName);
    return NULL;
  }
  
  parsedTree = stbPop();
  /* place the last waiting comment: */
  stbPlaceLabelAndComment();
  if (yyin != stdin) {
    fclose(yyin);
    yyin = stdin;
  }
  /*tagPointers(parsedTree);*/
  if (debugFortranParser) {
    char* dotfile;
    FILE* dot;
    fprintf(stdout,"==FORTRAN===FORTRAN===FORTRAN===\n");
    showTree(parsedTree, 0);
    dotfile = (char*)malloc(strlen(inputFilePathAndName) + 7);
    sprintf(dotfile, "%s.f.dot", inputFilePathAndName);
    dot = fopen(dotfile, "w");
    free(dotfile);
    showTreeAsDot(parsedTree, dot);
    fclose(dot);
  }
  return parsedTree;
}

/*, int code, struct sigcontext *sc) !Sun compiler does not like that!*/
int exit_handler(int sig) {
  fprintf(stdout,"%d\n", ilParsingErrorCode); 
  fprintf(stdout,"Fortran Parser: Interrupted analysis\n");
  fprintf(stderr, "Fortran Parser: Caught signal %i\n", sig);
  if (outputFile != stdout) fclose(outputFile);
  exit(sig);
}

int main(int argc, char *argv[]) {
  char *inputFilePathAndName = NULL;
  char *phylum = NULL;
  char *outputFileName = NULL;
  Tree *parsedTree;

#ifdef  linux
  yyout = stdout;
#endif
  {
    /* ANALYSIS OF COMMAND LINE ARGUMENTS: */
    int i = 1;
    char *arg;
    int waitphylum = 0;
    int waitoutputfile = 0;
    int waitfileseparator = 0;
    int waitincludetype = 0;
    int waitincludedir = 0;
    int waitdialect = 0;
    int waitlinesize = 0;
    if (argc == 1) {
      fprintf(stderr,"Fortran Parser: No input files\n");
      exit(0);
    }
    while (i<argc) {
      arg = argv[i];
      if (waitphylum) {
          phylum = arg;
          waitphylum = 0;
      } else if (waitoutputfile) {
          outputFileName = arg;
          waitoutputfile = 0;
      } else if (waitfileseparator) {
          fileseparator = arg;
          waitfileseparator = 0;
      } else if (waitincludetype) {
          includetype = arg;
          waitincludetype = 0;
            } else if (waitincludedir) {
                includedirlist[includedirNb] = arg;
                includedirNb = includedirNb + 1;
          waitincludedir = 0;
      } else if (waitdialect) {
          setScannerDialect(arg);
          waitdialect = 0;
      } else if (waitlinesize) {
          setScannerLineSize(arg);
          waitlinesize = 0;
      } else if (0 == strcmp(arg, "-debug"))
          debugFortranParser = 1;
      else if (0 == strcmp(arg, "-phylum"))
          waitphylum = 1;
      else if (0 == strcmp(arg, "-output"))
          waitoutputfile = 1;
      else if (0 == strcmp(arg, "-fileseparator"))
          waitfileseparator = 1;
      else if (0 == strcmp(arg, "-includetype"))
          waitincludetype = 1;
      else if (0 == strcmp(arg, "-includedir"))
          waitincludedir = 1;
      else if (0 == strcmp(arg, "-dialect"))
          waitdialect = 1;
      else if (0 == strcmp(arg, "-linesize"))
          waitlinesize = 1;
      else if (0 == strcmp(arg, "-freeformat"))
          setScannerFreeFormat(1);
      else if (0 == strcmp(arg, "-openmp"))
          setScannerOpenMP(1);
      else if (0 == strcmp(arg, "-version")
               || 0 == strcmp(arg, "--version")) {
        printf("Tapenade fortranParser version %s\n", fPid);
        exit(0);
      }
      else if (0 == strcmp(arg, "-help")
               || 0 == strcmp(arg, "--help")) {
        printf("Tapenade fortranParser version %s\n", fPid);
        printf("Options: -debug\n");
        printf("         -phylum <phylum>\n");
        printf("         -output <file>\n");
        printf("         -fileseparator <fileseparator>\n");
        printf("         -includetype <includetype>\n");
        printf("         -includedir <directory>\n");
        printf("         -dialect <dialect>\n");
        printf("         -linesize <num>\n");
        printf("         -freeformat\n");
        printf("         -openmp\n");
        exit(0);
      } else
        inputFilePathAndName = arg;
      i++;
    }
  }
  while (includedirNb<10) {
    includedirlist[includedirNb] = NULL;
    ++includedirNb;
  }
  directory = getDirectory(inputFilePathAndName);
  outputFile = (outputFileName)?fopen(outputFileName,"w"):stdout;
  if (parsedTree = parseF(inputFilePathAndName, phylum)) {
    if (debugFortranParser) {
      char* dotfile;
      FILE* dot;
      fprintf(stdout, "---IL------IL------IL------IL---\n");
      // Warning: this debugging will cause a 2nd parsing sweep on the include files:
      Tree *ilTree = f2ilTree(parsedTree);
      showTree(ilTree, 0);
      dotfile = (char*)malloc(strlen(inputFilePathAndName) + 8);
      sprintf(dotfile, "%s.il.dot", inputFilePathAndName);
      dot = fopen(dotfile, "w");
      free(dotfile);
      showTreeAsDot(ilTree, dot);
      fclose(dot);
      freeTree(ilTree);
    }
    f2ilProtocol(parsedTree, outputFile);
  }
  if (outputFile != stdout) fclose(outputFile);
  free(directory);
}
