; $Id: decmetal.ll,v 1.1 10/.0/.0 .1:.1:.2 vmp Exp $
;==============================================================================
;==============================================================================
;
;                              DECMETAL
;
;------------------------------------------------------------------------------

; Genereration des fichiers (pour un langage l)
;     yaccl.y input Yacc
;     l.tks table des mots-cles
;     l.c lanceur pour l'analyseur
;     l.lex

(defvar #:sys-package:colon 'compilo-Metal)

(de {metal}:yacc (tree #:compilo-Metal:*language* . flags)
    (let ((#:compilo-Metal:*name* ({langage}:name #:compilo-Metal:*language*))
          (nocompact (memq 'non-compacte flags))
          tokentable)

      ;;[llh] Added for for tree building in C:
      (:prepareCLanguageTables #:compilo-Metal:*language* #:compilo-Metal:*name*)
      ;;

      (:gettokens (bas tree 2))

      (:create_yaccfile tree tokentable #:compilo-Metal:*name*)

      ;;[llh] Commented for for tree building in C:
      ;;(:create_tokensfile tokentable #:compilo-Metal:*name*)
      ;;

      ;;[llh] Commented for for tree building in C:
      ;;(:create_lexfile tokentable #:compilo-Metal:*name*)
      ;;

      ;;[llh] Commented for for tree building in C:
      ;;(:create_mainfile #:compilo-Metal:*name*)
      ;;

      ;;[llh] Commented for for tree building in C:
      ;;#:compilo-Metal:*language*
      ;;
))

(de :gettokens (tree)
    (selectq ({operateur}:nom (operdearbre tree))
      (terminal
        (:addtokens (atomeval tree) 'T))
      (genericatom
        (:addtokens (atomeval tree) 'G))
      ((production chapt)
        (:gettokens (bas tree 2)))
      (rule
        (:gettokens (bas tree 1)))
      ((non_terminal abs_syn))
      ((zone_s rule_s elem_s) 
       ({arbre}:for-all-sons tree
          (lambda (son) (:gettokens son))))))

(de :addtokens (elem type)
    (unless (assq elem tokentable)
            (newr tokentable (cons elem type))))

(de :create_yaccfile (tree tokentable #:compilo-Metal:*name*)
    (let ((firstnonter t) (rulenb 0) (nbtoken 0)
          isageneric isaxiome
	  (file (:absolute-pathname #:compilo-Metal:*name* (catenate "yacc" #:compilo-Metal:*name* ".y"))))
         (withoutput
	  file
	  ;; Since v15.24 an argument of prin may be truncated at the end of a 
	  ;; line.  Disabling rmargin prevents that, but may confuse yacc
	  ;; if too long lines are generated.  Let's see!
	  (with ((rmargin (1+ (slen (outbuf)))))
		(print "%{")
		(print "#include <stdio.h>")
		(print "#include <ctype.h>")
		;;[llh] Added for for tree building in C:
		(print "#include ""stackTreeBuilder.h""")
		(print "#include ""cvtp.h""")
		(print "%}")
		(:printtokens tokentable)
		;; Now that the tokens are defined, include the lex
		;; It works for lex and flex. Yeah!
		(print "%{")
		(if (and (boundp '*fortranSpecialParser*) *fortranSpecialParser*)
		    (progn
		      (print "#include ""scanner.c""")
		      (print "#include ""yaccbib.c""")
		      )
		  (print "#include """
		       (catenate "lex" #:compilo-Metal:*name* ".c") """")
		)
		(print "%}")
		(:rule tree ':decprod)
		(print "%%")
		))
	 (printscreen file " created")))

(de :rule (tree function)
    (selectq ({operator}:nom (operdearbre tree))
      (rule (funcall function tree))
      (abs_syn)
      (t ({tree}:for-all-sons tree
           (lambda (son) (:rule son function))))))
  
(de :decprod (tree)
    (let ((prod (bas tree 1))
          (semantic (bas tree 2))
          leftsons rigth)
         (setq leftsons (:non_terminal (atomeval (bas prod 1))))
         (setq rigth (bas prod 2))
         (when firstnonter
               (setq #:compilo-Metal:*axiome* leftsons)
               (print "%start " leftsons)
               (print "%%")
               (setq firstnonter nil))
         (prin leftsons)
         (setq isaxiome (eq leftsons #:compilo-Metal:*axiome*))
         (prin " : ")
         ({arbre}:for-all-sons rigth
            (lambda (son)
                  (let ((#:compilo-Metal:*name* (atomeval son)))
                       (selectq ({operateur}:nom (operdearbre son))
                        (ucid 
                          (prin #:compilo-Metal:*name* "  "))
                        (non_terminal
                          (prin (:non_terminal #:compilo-Metal:*name*) "  "))
			(genericatom 
                          (setq isageneric t)
                          (prin (:generic_name #:compilo-Metal:*name*) "  "))
                        (terminal
                          (prin (:token_name #:compilo-Metal:*name*) "  "))))))
         (terpri)
         (incr rulenb)
         (if (and (eq ({operateur}:nom (operdearbre semantic))
	              'non_terminal)
                  (:no_nterm (bas prod 2)))
             (print) ;was: (print "    { }")
	   ;;Alternative mode: directly calls for tree building in C:
	   (:printCProduction prod semantic)
	   ;;Normal Centaur mode: puts the number of the rule onto yyout:
	   ;;(:printLispProduction rulenb isageneric)
	   )
         (print "   ;")))

(de :printLispProduction (rulenb isageneric)
    (print "   { fputs(""" rulenb "\n"", yyout);"
	   (ifn isageneric ""
		(setq isageneric nil)
		"fputs(yytext,yyout); putc('\n',yyout) ; ")
	   "}"))

(de :no_nterm (tree)
    (let ((nterm 0))
      (tag nterm
        ({arbre}:for-all-sons tree
          (lambda(son)
             (when (and (eq ({operateur}:nom (operdearbre son)) 'non_terminal)
                        (> (incr nterm) 1))
                   (exit nterm ()))))
        t)))

(de :printtokens (tokentable)
    (mapc (lambda (elem)
           (print (catenate "%token " 
                          (selectq (cdr elem)
                           (T (:token_name (car elem)))
                           (G (:generic_name (car elem)))))))
          tokentable))

(de :create_tokensfile (tokentable #:compilo-Metal:*name*)
    (let ((file (:absolute-pathname #:compilo-Metal:*name* (catenate #:compilo-Metal:*name* ".tks"))))
	 (withoutput
	  file
	  ;; See comment in :create_yaccfile
	  (with ((rmargin (1+ (slen (outbuf))))) 

		(mapc 'print
		      '(
			"<waitdata>^""[BEGINDATA]""\n return(WAITDATA_DONE);"
			"<waitdata>^""[PARSEFILE]"".*\n {"
			"    if (redirect_input(yytext, 11, yyleng)) { "
			"	return(WAITDATA_DONE);"
			"    } else {"
			"	fflush(yyout);"
			"    }};"
			"<waitdata>^""[IDENT]"".*\n { fprintf(yyout, ""-6\n%s"",yytext+7); fflush(yyout);}"
			"<waitdata>^""[CLIENTIDENT]"".*\n { fputs(yytext+13,yyout); fflush(yyout);}"
			"<waitdata>(.|\n) ;"
			"<waitdata>^""[ENDDATA]""\n ;"
			"^""[ENDDATA]""\n return(0);"
			))
		(mapc (lambda (elem)
			(when (eq (cdr elem) 'T)
			      (if (:entrypoint (:dpname (car elem)))
				  (progn
				    (print "<entrypoint>"""
					   (:dpname (car elem)) 
					   "\n""  {BEGIN sentences; return("
					   (:token_name (car elem)) ");};")
				    )
				  (print "<sentences>""" (:dpname (car elem))
					 """ return("
					 (:token_name (car elem)) ");")
				  )))
		      tokentable)
		(print "<entrypoint>. {BEGIN sentences; unput(yytext[0]);};")
		(print "<entrypoint>\n {BEGIN sentences; unput(yytext[0]);};")
		))
	 (printscreen file " created")))

(de :entrypoint (tokenname)
   (eqstring "EN_" (substring (:token_name tokenname) 0 3))
)
     
(de :create_mainfile (#:compilo-Metal:*name*)
    (let ((file (:absolute-pathname #:compilo-Metal:*name* (catenate #:compilo-Metal:*name* ".c"))))
	 (withoutput
	  file
	  (mapc 'print
		`(
		  "#include <stdio.h>"
		  "#define WAITDATA_DONE -1515"
		  ,(catenate "#include ""yacc" #:compilo-Metal:*name* ".c""")
		  "main()"
		  "{"
		  "  int code;"
		  "  yyin = stdin; yyout = stdout;	/* for flex */"
		  "  for (;;) {"
		  "    fflush(yyout);"
		  "    BEGIN waitdata;"
		  "    switch ((code = yylex())) {"
		  "    case WAITDATA_DONE:"
		  "      BEGIN entrypoint;yyparse();"
		  "      fputs(""-7\n"", yyout);"
		  "      if (yyin != stdin) {"
		  "	fclose(yyin);"
		  "	yyin = stdin;"
		  "#ifdef FLEX_SCANNER"
		  "	YY_NEW_FILE;"
		  "#else"
		  "	/* The following is needed in case of a syntax error */"
		  "	yyprevious = '\n';	/* Starting a line */"
		  "	yysptr = yysbuf;	/* Empties the lex buffer */"
		  "#endif"
		  "      }"
		  "      break;"
		  "    case 0: exit (0);		/* EOF */"
		  "    default:"
		  "      fprintf(stderr, ""Unexpected return from yylex%d\n"", code);"
		  "      exit (1);"
		  "    }"
		  "  }"
		  "}")))
	  (printscreen file " created")))


(de :create_lexfile (tokentable #:compilo-Metal:*name*)
    (let ((file (:absolute-pathname #:compilo-Metal:*name* (catenate #:compilo-Metal:*name* ".tokens.x"))))
	 (withoutput
	  file
	  (mapc (lambda (line) (print line "\"))
           '(
"1i"
"%{"
"/*"
"This file is a sed script to be applied to the file defining the non"
"generic tokens.  It should define all the generic ones whose definition"
"can't be specified in METAL.  It could also define some macros and sizes"
"of LEX that may be needed."
"*/"
"%}"
"%S waitdata entrypoint sentences comment"
"%%"
"<comment>.*            {fprintf(yyout,""-1\\n%d\\n%s\\n"",yyleng,yytext); }"
"<sentences,comment>\\n {BEGIN sentences; fputs(""-2\\n"",yyout); }"
"<sentences,comment>[ \\t] ;"
           ))
	 (print)

	 (print "$a\")
         (mapc (lambda (elem)
	      (when (eq (cdr elem) 'G)
		    (let ((str (:generic_name (car elem))))
			 (print "<sentences>" str
                                "		return(" str ");\"))))
	    tokentable)
	 (print "<sentences>.	{fputs(""-3\\n"",yyout); fputs(yytext,yyout); putc('\\n',yyout); }")
	 )
	  (printscreen file " created")))

(de :non_terminal (#:compilo-Metal:*name*)
    (catenate "NT_" #:compilo-Metal:*name*))

(de :generic_name (#:compilo-Metal:*name*)
    (catenate (cdr (pname #:compilo-Metal:*name*))))

(de :token_name (#:compilo-Metal:*name*)
    (setq #:compilo-Metal:*name* (pname #:compilo-Metal:*name*))
    (cond
	((and (eq (car #:compilo-Metal:*name*) #/[)
	      (gt (length #:compilo-Metal:*name*) 2)
	      (eq (car (last #:compilo-Metal:*name*)) #/]))
	 (catenate "EN_"  (reverse (cdr (reverse (cdr #:compilo-Metal:*name*))))))
	(t (catenate "KW_" (mapcan ':translate #:compilo-Metal:*name*)))))

(de :translate (char)
    (if (or (and (ge char #/A) (le char #/Z))
            (and (ge char #/a) (le char #/z))
	    (and (ge char #/0) (le char #/9))
	    )
	(list char)
      (pname (cassq char :special_chars))))

(de :dpname (string)
;----------
; convert "" to \"
; and     \ to \\
; for lex

    (lets ((str (duplstring 1 string))
           (len (slen str))
           (i 0))
          (while (< i len)
                 (cond
                     ((= (sref str i) #/\)
                       (setq str
                             (catenate (substring str 0 i)
                                       "\"
                                       (substring str i)
			     )
		       )
                       (incr i 1)
                       (incr len))
                     ((and (= (sref str i) #/")
                           (= (sref str (1+ i)) #/"))
                       (sset str i #/\)
                       (incr i))
		 )
              (incr i))
          str))

(defvar :special_chars '(
(#/! . XCLA) (#/" . DQUO) (#/# . DIES) (#/$ . DOLL)
(#/% . PCEN) (#/& . ETPE) (#/' . QUOT) (#/( . POUV)
(#/) . PFER) (#/* . ETOI) (#/+ . PLUS) (#/, . VIRG)
(#/- . MINU) (#/. . POIN) (#// . SLAH) (#/: . DPIN)
(#/; . PVIR) (#/< . INFE) (#/= . EQUL) (#/> . SUPE)
(#/? . INTE) (#/@ . ARRO) (#/[ . COUV)
(#/] . CFER) (#/^ . CHAP) (#/_ . |_|)  (#/` . BQUO)
(#/{ . BOUV) (#/} . BFER) (#/~ . TILD) (#/\ . BSLA)
(#/| . VERT)))
