; $Id: metatbls.ll,v 1.1 10/.0/.0 .1:.1:.2 vmp Exp $
;============================================================================
;-----------------------------------------------------------------------------
(defun {metal}:abstract (tree . flags)
    (tag semantic_error
        ({formalism}:save
            ({metal}:formalism tree (car (memq 'complete flags)))
            (car (memq 'commente flags)))))


;-----------------------------------------------------------------------------
;============================================================================
;
;                                METATABLES
;
;----------------------------------------------------------------------------
; Creation d'une structure de type langage a partir de l'arbre Metal 
; (defrecord langage #:compilo-Metal:*name* (version 0) opers phyla limits codeopers codephyla
;    frames tables parser vides)
(defvar #:sys-package:colon 'compilo-Metal)

(defun {metal}:formalism (tree . args)
    (let ((complete (memq 'complete args))
          (#:compilo-Metal:*langname* (atomeval (bas tree 1)))
          (#:compilo-Metal:*language* ())
          (phyltoextend ())
          (op-list ())
          (phyl-list ()))
         (ifn complete
             (setq #:compilo-Metal:*language*
                 ({langage}:creer #:compilo-Metal:*langname*))
             (tag file-system
                 (setq #:compilo-Metal:*language*
                     (langdenom #:compilo-Metal:*langname*)))
             (unless #:compilo-Metal:*language*
                     (setq #:compilo-Metal:*language*
                         ({langage}:creer #:compilo-Metal:*langname*))))
         (unless complete
                 ({langage}:maxcodop #:compilo-Metal:*language* -2)
                 ({operator}:make
                     '_null_tree #:compilo-Metal:*language* 0 {singleton})
                 ({operator}:skipcode #:compilo-Metal:*language*))
         (:addphyl-and-op tree)
         (:defphyl-and-op tree)
         ; Extension des phyla apparaissant comme composant d'autres phyla.
         (:extendall phyltoextend)
         ; Creation des operators meta comment et comment_s 
         ; si ils n'existent pas (egalement creation du phylum COMMENT)
         (noexit duplication ({operator}:make 'meta #:compilo-Metal:*language*))
         ({operator}:structure
             ({name}:operator 'meta #:compilo-Metal:*language*)
             (list 0 {metavariable}))
         (noexit
             duplication
             ({operator}:make 'comment #:compilo-Metal:*language* 0 {string}))
         (noexit
             duplication
             ({operator}:make
                 'comment_s
                 #:compilo-Metal:*language*
                 '*
                 (or (noexit
                         not_found
                         ({name}:phylum 'COMMENT #:compilo-Metal:*language*))
                     ({phylum}:make
                         'COMMENT
                         #:compilo-Metal:*language*
                         ({name}:operator 'comment #:compilo-Metal:*language*)))))
         (or (noexit
                 not_found ({name}:phylum 'COMMENT_S #:compilo-Metal:*language*))
             ({phylum}:make
                 'COMMENT_S
                 #:compilo-Metal:*language*
                 ({name}:operator 'comment_s #:compilo-Metal:*language*)))
         ({langage}:limits
             #:compilo-Metal:*language*
             (list
                 ({name}:operator 'meta #:compilo-Metal:*language*)
                 ({name}:operator 'comment #:compilo-Metal:*language*)
                 ({name}:operator 'comment_s #:compilo-Metal:*language*)))
         (addop_meta #:compilo-Metal:*language*)
         ({langage}:complete #:compilo-Metal:*language*)
         #:compilo-Metal:*language*))


;----------------------- creation des phyla ----------------------------------
(defun :addphyl (tree)
    (let ((#:compilo-Metal:*name* (:upper-diese (atomeval (bas tree 1))))
          (phyl ()))
         (lock
             (lambda (tag val) (when (and tag (not complete))
                                     ({metal}:post
                                         'already_defined_phylum
                                         (list (list #:compilo-Metal:*name* ())))))
             (setq phyl
                 ({phylum}:make
                     #:compilo-Metal:*name* #:compilo-Metal:*language*))
             (when complete
                   (newl phyl-list phyl)))))


(defun :defphyl (tree)
    (tag :defphyl
        (let ((#:compilo-Metal:*name* (:upper-diese (atomeval (bas tree 1))))
              (extend ())
              (phyl ()))
             (setq phyl
                 ({name}:phylum
                     #:compilo-Metal:*name* #:compilo-Metal:*language*))
             (when (and complete (not (memq phyl phyl-list)))
                   (exit :defphyl))
             ({arbre}:for-all-sons
                 (bas tree 2)
                 (lambda (son)
                     (let ((opname ({operateur}:nom (operdearbre son)))
                           (op ())
                           (sonname (:lower-diese (atomeval son))))
                          (setq op
                              (if (eq opname 'lcid)
                                  (lock
                                      (lambda (tag val)
                                          (ifn tag
                                              val
                                              ({metal}:post
                                                  'undefined_operator
                                                  (list
                                                      (list sonname ())
                                                      (list
                                                          ({phylum}:nom phyl) ())))))
                                      ({name}:operator
                                          sonname #:compilo-Metal:*language*))
                                  (progn
                                     (setq extend t)
                                     (:upper-diese (atomeval son)))))
                          (when op
                                ({phylum}:insert phyl op)))))
             (if extend
                 (newl phyltoextend phyl)))))


; Berk ! Revoir le code des fonctions extend un de ces jours
(defun :extendall (phyltoextend)
    (while phyltoextend
           (let* ((phylum (nextl phyltoextend))
                  (path (list ({phylum}:nom phylum))))
                 (:extend (reverse ({phylum}:contents phylum)) phylum ()))))


(defun :extend (l phyl l1)
    (let ((ph ()) (nameph ()))
         (cond ((null l) ;              ({phylum}:insert phyl l1)
                         ({phylum}:contents phyl l1)
                         ({phylum}:size phyl (length l1))
                         l1)
               (;; \\ sources/vtp/formalism should provide a boolean function
                ;; \\ to know if something is an operator or not!
                (typep (car l) 'operator)
                   (:extend (cdr l) phyl (cons (car l) l1)))
               ((atomp (car l))
                   (:extend
                       (cdr l)
                       phyl
                       (append
                           (lock
                               (lambda (tag val) (if (neq tag 'not_found)
                                                     (cond
                                                         ((memq
                                                              (setq nameph
                                                                  ({phylum}:nom
                                                                      ph)) path)
                                                             (newl path nameph)
                                                             ({metal}:post
                                                                 'circular_phylum_definition
                                                                 (list
                                                                     (list
                                                                         (apply
                                                                             'catenate
                                                                             (mapcar (lambda
                                                                                         (x)
                                                                                         (catenate
                                                                                             (string
                                                                                                 x)
                                                                                             " "))
                                                                                 (reverse
                                                                                     path)))
                                                                         ())))
                                                             (setq path
                                                                 (list
                                                                     ({phylum}:
                                                                      nom
                                                                         phylum)))
                                                             ())
                                                         (t
                                                             (newl path nameph)
                                                             (:extend
                                                                 (reverse
                                                                     ({phylum}:
                                                                      contents
                                                                         ph))
                                                                 ph
                                                                 ())))
                                                     ({metal}:post
                                                         'undefined_phylum1
                                                         (list
                                                             (list (car l) ())
                                                             (list
                                                                 ({phylum}:nom
                                                                     phyl) ())))))
                               (setq ph
                                   ({name}:phylum
                                       (car l) #:compilo-Metal:*language*)))
                           l1)))
               (t (error ':extend "unexpected operator or atom list" l)))))


;-------------------------- creation des operateurs -------------------------
(defun :addop (tree)
    (let ((#:compilo-Metal:*name* (:lower-diese (atomeval (bas tree 1))))
          (op ()))
         (lock
             (lambda (tag val) (when (and tag (not complete))
                                     ({metal}:post
                                         'already_defined_operator
                                         (list (list #:compilo-Metal:*name* ())))))
             (setq op
                 ({operator}:make
                     #:compilo-Metal:*name* #:compilo-Metal:*language*))
             (when complete
                   (newl op-list op)))))


(defun :defop (tree)
    (tag :defop
        (let ((#:compilo-Metal:*name* (:lower-diese (atomeval (bas tree 1))))
              (treesons (bas tree 2)))
             (let ((optreesons ({operateur}:nom (operdearbre treesons)))
                   (op ({name}:operator
                           #:compilo-Metal:*name* #:compilo-Metal:*language*))
                   (sons ()))
                  (when (and complete (not (memq op op-list)))
                        (exit :defop))
                  (if (memq optreesons '(at_impl_constraint at_impl))
                      ; at_impl est uniquement conserve pour compatibilite
                      ; avec les precedentes versions de Metal.
                      (let ((type (atomeval (bas treesons 1))))
                           (setq sons
                               (list
                                   0
                                   (selectq type
                                       (IDENTIFIER {name})
                                       (STRING {string})
                                       (SINGLETON {singleton})
                                       (INTEGER {number})
                                       (CHAR {character})
                                       (TREE {tree})
                                       (BIGNUM {integer})
                                       (t (lowercatom type))))))
                      (if (eq ({arbre}:nbsons treesons) 0)
                          ; cas des operateurs nullaires dont 
                          ; l'implementation n'a pas ete definie par
                          ; l'utilisateur. On met une valeur par defaut
                          ; qui est celle des identificateurs.
                          (setq sons (list 0 {name}))
                          (progn
                             ({arbre}:for-all-sons
                                 treesons
                                 (lambda (son)
                                     (let ((son1 (:upper-diese (atomeval son))))
                                          (lock
                                              (lambda (tag val) (if tag
                                                                    ({metal}:
                                                                     post
                                                                        'undefined_phylum
                                                                        (list
                                                                            (list
                                                                                son1
                                                                                ())
                                                                            (list
                                                                                ({operator}:
                                                                                 nom
                                                                                    op)
                                                                                ())))
                                                                    (newl
                                                                        sons val)))
                                              ({name}:phylum
                                                  son1
                                                  #:compilo-Metal:*language*)))))
                             (setq sons
                                 (cons
                                     (selectq optreesons
                                         (plus_arbitrary '+)
                                         (star_arbitrary '*)
                                         (t (length sons)))
                                     (nreverse sons))))))
                  ({operateur}:structure op sons)))))


;---------------------------------------------------------------------------
; provisoire: a inclure dans la RI
(defun :conselems (l)
    (if (null l)
        ()
        (cons (cons (car l) (cadr l)) (:conselems (cddr l)))))


(defun :contraintecreer (type . constrs)
    (cons type (:conselems constrs)))


;-------------------------------------------------------------------------
(defun :addphyl-and-op (tree)
    (selectq ({operator}:nom (operdearbre tree))
        (sort (:addphyl tree))
        (operator (:addop tree))
        (rule_s )
        (t ({tree}:for-all-sons tree (lambda (son) (:addphyl-and-op son))))))


(defun :defphyl-and-op (tree)
    (selectq ({operator}:nom (operdearbre tree))
        (sort (:defphyl tree))
        (operator (:defop tree))
        (rule_s )
        (t ({tree}:for-all-sons tree (lambda (son) (:defphyl-and-op son))))))


(defun :upper-diese (exp)
    exp)


(defun :lower-diese (exp)
    exp)


