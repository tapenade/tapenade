; $Id: cProduct.ll,v 1.1 10/.0/.0 .1:.1:.2 vmp Exp $

(defvar #:sys-package:colon 'compilo-Metal)

;;[llh] Added for for tree building in C:
(defvar :globalLanguageTable ())
;;

(de :prepareCLanguageTables (formalism #:compilo-Metal:*name*)
    ;;(setq gf formalism);;debug
    (let ((file (:absolute-pathname #:compilo-Metal:*name* (catenate "tables" #:compilo-Metal:*name* ".c")))
	  (nullOp ())
	  (labelOp ())
	  (labelsOp ())
	  (labstatOp ())
	  (commentOp ())
	  (commentsOp ())
	  (intCstOp ())
	  (pppLineOp ())
	  (stringOp ())
	  (identOp ())
	  opers oper opobj opcode i maxi
	  )
      (setq opers (car (#:formalism:opers formalism))) ;;The opers seem to be all in the car !!
      (setq :globalLanguageTable (makevector (length opers) ()))
      (while (nextl opers oper)
	(setq opcode (#:operator:code oper))
	(when (eq opcode -1) (setq opcode 0))  ;; Strange case for null_tree...
	(setq opobj (list (#:operator:name oper) (#:operator:arity oper) opcode))
	(when (vref :globalLanguageTable opcode)
	  (print "ERROR: two opers with same code: " opcode "!!!"))
	(vset :globalLanguageTable opcode opobj)
	)
      ;;The C file tablesLang.h contains a list of Operator, (for each operator in the language):
      ;; where an Operator is a record of
      ;;   -- its name
      ;;   -- its arity
      ;;   -- its code, which is its rank in this list
      (withoutput file
	(print "static Operator " #:compilo-Metal:*name* "Operators[] = {")
	(setq i -1)
	(setq maxi (sub (vlength :globalLanguageTable) 1))
	(while (le (incr i) maxi)
	  (setq opobj (vref :globalLanguageTable i))
	  (print "   {""" (car opobj) """, " 
		  (if (memq (cadr opobj) '(+ *)) "-1" (cadr opobj))
		  ", " (caddr opobj)
		  (if (eq i maxi) "}" "},"))
	  (when (equal (string (car opobj)) "none") (setq nullOp (caddr opobj)))
	  (when (equal (string (car opobj)) "label") (setq labelOp (caddr opobj)))
	  (when (equal (string (car opobj)) "labels") (setq labelsOp (caddr opobj)))
	  (when (equal (string (car opobj)) "labstat") (setq labstatOp (caddr opobj)))
	  (when (equal (string (car opobj)) "comment") (setq commentOp (caddr opobj)))
	  (when (equal (string (car opobj)) "comment_s") (setq commentsOp (caddr opobj)))
	  (when (equal (string (car opobj)) "intCst") (setq intCstOp (caddr opobj)))
	  (when (equal (string (car opobj)) "pppLine") (setq pppLineOp (caddr opobj)))
	  (when (equal (string (car opobj)) "string") (setq stringOp (caddr opobj)))
	  (when (equal (string (car opobj)) "ident") (setq identOp (caddr opobj)))
	  )
	(print "} ;")
	(when nullOp
	  (print "static short " #:compilo-Metal:*name* "NullTreeCode = " nullOp " ;"))
	(when labelOp
	  (print "static short " #:compilo-Metal:*name* "LabelTreeCode = " labelOp " ;"))
	(when labelsOp
	  (print "static short " #:compilo-Metal:*name* "LabelsTreeCode = " labelsOp " ;"))
	(when labstatOp
	  (print "static short " #:compilo-Metal:*name* "LabstatTreeCode = " labstatOp " ;"))
	(when commentOp
	  (print "static short " #:compilo-Metal:*name* "CommentTreeCode = " commentOp " ;"))
	(when commentsOp
	  (print "static short " #:compilo-Metal:*name* "CommentsTreeCode = " commentsOp " ;"))
	(when intCstOp
	  (print "static short " #:compilo-Metal:*name* "IntCstTreeCode = " intCstOp " ;"))
	(when pppLineOp
	  (print "static short " #:compilo-Metal:*name* "PppLineTreeCode = " pppLineOp " ;"))
	(when stringOp
	  (print "static short " #:compilo-Metal:*name* "StringTreeCode = " stringOp " ;"))
	(when identOp
	  (print "static short " #:compilo-Metal:*name* "IdentTreeCode = " identOp " ;"))
	)
      (print "Info: " file " created.")
))

(de :printCProduction (prod semantic)
    (let (NTList template vars
	  )
      (setq NTList (nreverse (:getNTList (bas prod 2))))
      (setq template (:getCaseTemplate semantic NTList ()))
      (:putProduction NTList template)
))

(de :getNTList (rhs)
    (let ((sons (#:tree:sons rhs))
	  (NTList ())
	  son
	  )
       (while (nextl sons son)
	  (when (eq (#:operator:nom (operdearbre son)) 'non_terminal)
	     (newl NTList (atomeval son))))
       NTList
))

;;;----------------------------------------------------------
;;; Building the tree construction template:
;;;----------------------------------------------------------

(de :getCaseTemplate (semantic NTList letVars)
    (selectq (#:operator:nom (operdearbre semantic))
	     (letop
	      (let ((newLetVars (list ()))
		    patterns
		    )
		(setq patterns
		      (list (:getPattern (bas (bas semantic 1) 1) newLetVars)))
		(list 'Case
		      (:getVarTemplate (bas (bas semantic 1) 2) NTList letVars 'tree)
		      (list patterns
			    (cdr newLetVars)
			    (:getCaseTemplate (bas semantic 2) NTList
					  (append letVars (cdr newLetVars)))))
	      ))
	     (caseop
	      (mcons 'Case
		     (:getVarTemplate (bas semantic 1) NTList letVars 'tree)
		     (:getCaseTemplate (bas semantic 2) NTList letVars)
	      ))
	     (alternative_s
	      (let ((sons (#:tree:sons semantic))
		    (res (list ()))
		    tl-res son
		    )
		 (setq tl-res res)
		 (while (nextl sons son)
		    (setq tl-res
			  (placdl tl-res
				  (:getCaseTemplate son NTList letVars))))
		 (cdr res)
	      ))
	     (alternative
	      (let ((newLetVars (list ()))
		    patterns
		    )
		(setq patterns (:getPattern (bas semantic 1) newLetVars))
		(list patterns
		      (cdr newLetVars)
		      (:getCaseTemplate (bas semantic 2) NTList
				    (append letVars (cdr newLetVars))))
	      ))
	     (t
	      (:getTemplate semantic NTList letVars 'tree)
	      )
))	     

(de :getTemplate (semantic NTList letVars type)
    (selectq (#:operator:nom (operdearbre semantic))
	     (node
	      (mcons 'Fix
		     (:getOpIndex (atomeval (bas semantic 1)))
		     (:getTemplate (bas semantic 2) NTList letVars 'tree))
	      )
	     (named_listop
	      (let ((opIndex (:getOpIndex (atomeval (bas semantic 1))))
		    (listTree (bas semantic 2))
		    )
		 (selectq (#:operator:nom (operdearbre listTree))
			  (postop
			   (list 'Post opIndex
				 (:getTemplate (bas listTree 1) NTList letVars 'tree)
				 (:getTemplate (bas listTree 2) NTList letVars 'tree)
			   ))
			  (preop
			   (list 'Pre opIndex
				 (:getTemplate (bas listTree 1) NTList letVars 'tree)
				 (:getTemplate (bas listTree 2) NTList letVars 'tree)
			   ))
			  (listop
			   (setq listTree (bas listTree 1))
			   (if (eq (#:operator:nom (operdearbre listTree))
				   'factor_s)
			       (mcons 'List opIndex
				      (:getTemplate listTree NTList letVars 'tree))
			     (list 'List2 opIndex
				   (:getTemplate listTree NTList letVars 'list))
			   ))
	      )))
	     (named_atomop
	      (list 'Atom
		    (:getOpIndex (atomeval (bas semantic 1)))
		    (:getTemplate (bas (bas semantic 2) 1) NTList letVars 'atom))
	      )
	     (genericatom
	      ()
	      )
	     (string
	      (:adjust_string (atomeval semantic))
	      )
	     (factor_s
	      (let ((sons (#:tree:sons semantic))
		    (res (list ()))
		    tl-res son
		    )
		 (setq tl-res res)
		 (while (nextl sons son)
		    (setq tl-res
			  (placdl tl-res
				  (:getTemplate son NTList letVars 'tree))))
		 (cdr res)
	      ))
	     ((non_terminal dot_non_terminal ucid)
	      (:getVarTemplate semantic NTList letVars type)
	      )
))

(defun :adjust_string (string)
    (catenate (nreverse (cdr (nreverse (cdr (pname string)))))))


(de :getVarTemplate (semantic NTList letVars type)
    (selectq (#:operator:nom (operdearbre semantic))
	     (non_terminal
	      (let ((index 1)
		    (inNTList NTList)
		    (NTname (atomeval semantic))
		    )
		 (until (or (null inNTList) (equal NTname (car inNTList)))
		    (incr index)
		    (nextl inNTList))
		 index
	      ))
	     (dot_non_terminal
	      (let ((index 1)
		    (inNTList NTList)
		    (NTname (atomeval (bas semantic 1)))
		    (rank (:number (atomeval (bas semantic 2))))
		    )
		 (until (or (null inNTList)
			    (and (equal NTname (car inNTList))
				 (eq rank 1)))
		    (incr index)
		    (when (equal NTname (car inNTList)) (decr rank))
		    (nextl inNTList))
		 index
	      ))
	     (ucid
	      (let ((inLetVars letVars)
		    (name (atomeval semantic))
		    )
		(until (or (null inLetVars) (equal name (cadr (car inLetVars))))
		       (nextl inLetVars))
		(unless (null inLetVars)
		  (rplaca (cddr (car inLetVars)) 'used)
		  (rplaca (cdddr (car inLetVars)) type))
		(car inLetVars)
	      ))
))

(de :getPattern (factor newLetVars)
    (selectq (#:operator:nom (operdearbre factor))
	     (othersop
	      t
	      )
	     (choice_s
	      (let ((sons (#:tree:sons factor))
		    (res (list ()))
		    tl-res son
		    )
		 (setq tl-res res)
		 (while (nextl sons son)
		    (setq tl-res (placdl tl-res (:getPattern son newLetVars))))
		 (cdr res)
	      ))
	     (named_listop
	      (let ((opIndex (:getOpIndex (atomeval (bas factor 1))))
		    (listTree (bas factor 2))
		    )
		 (selectq (#:operator:nom (operdearbre listTree))
			  (postop
			   (list 'Post opIndex
				 (:getPattern (bas listTree 1) newLetVars)
				 (:getPattern (bas listTree 2) newLetVars)
			   ))
			  (preop
			   (list 'Pre opIndex
				 (:getPattern (bas listTree 1) newLetVars)
				 (:getPattern (bas listTree 2) newLetVars)
			   ))
			  (listop
			   (setq listTree (bas listTree 1))
			   (if (eq (#:operator:nom (operdearbre listTree))
				   'factor_s)
			       (mcons 'List opIndex
				      (:getPattern listTree newLetVars))
			     (list 'List2 opIndex
				   (:getPattern listTree newLetVars))
			   ))
	      )))
	     (named_atomop
	      (list 'Atom
		    (:getOpIndex (atomeval (bas factor 1)))
		    (:getPattern (bas (bas factor 2) 1) newLetVars)
	      ))
	     (node
	      (mcons 'Fix
		     (:getOpIndex (atomeval (bas factor 1)))
		     (:getPattern (bas factor 2) newLetVars))
	      )
	     (string
	      (atomeval factor)
	      )
	     (ucid
	      (let ((name (atomeval factor))
		    (place newLetVars)
		    )
		 (until (or (null (cdr place)) (equal (cadr (cadr place)) name))
			(setq place (cdr place)))
		 (when (null (cdr place))
		   (rplacd place (list (list 'Var name () ()))))
		 (cadr place)
	      ))
	     (factor_s
	      (let ((sons (#:tree:sons factor))
		    (res (list ()))
		    tl-res son
		    )
		 (setq tl-res res)
		 (while (nextl sons son)
		    (setq tl-res (placdl tl-res (:getPattern son newLetVars))))
		 (cdr res)
	      ))
))

(de :getOpIndex (opName)
    (let ((i 0)
	  (maxi (vlength :globalLanguageTable))
	 )
       (until (or (ge i maxi) (equal (car (vref :globalLanguageTable i)) opName))
	      (incr i)
	      )
       (if (lt i maxi)
	 (caddr (vref :globalLanguageTable i))
	 (concat "?" opName "?"))
))

;;;----------------------------------------------------------
;;; Analysis and printing of the tree construction template:
;;;----------------------------------------------------------

(de :putProduction (NTList template)
  (let ((NTNb (length NTList))
	)
    (prin "{")
    (cond 
     ;; <a> ::= <b1> ... <b*> ... ; -> <b1>
     ((:equalTemplate template '1)
      (:putLostPops (sub NTNb 1))
      )
     ((:equalTemplate template '(Post () 1 2))
      (:putLostPops (sub NTNb 2))
      (prin :predefPostFuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Fix ()))
      (:putLostPops NTNb)
      (prin :predefArity0FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Fix () 1))
      (:putLostPops (sub NTNb 1))
      (prin :predefArity1FuncName "(" (cadr template) ") ;")
      )
     ;; <a> ::= <b1> ... <b2> ... <b*> ... ; -> op(<b1>,<b2>)
     ((:equalTemplate template '(Fix () 1 2))
      (:putLostPops (sub NTNb 2))
      (prin :predefArity2FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(List () 1))
      (:putLostPops (sub NTNb 1))
      (prin :predefList1FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(List ()))
      (:putLostPops NTNb)
      (prin :predefList0FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Atom () ()))
      (:putLostPops NTNb)
      (prin :pushFuncName "(" :atomicTreeFuncName "(" :rankOperFuncName "(" (cadr template) "), "
	                     "(" :atomValueType ")(strdup(yytext)))) ;")
      )
     ((:equalTemplate template '(Fix () 1 2 3))
      (:putLostPops (sub NTNb 3))
      (prin :predefArity3FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Fix () 1 2 3 4))
      (:putLostPops (sub NTNb 4))
      (prin :predefArity4FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(List () 1 2))
      (:putLostPops (sub NTNb 2))
      (prin :predefList2FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(List () 1 2 3))
      (:putLostPops (sub NTNb 3))
      (prin :predefList3FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(List () 1 2 3 4))
      (:putLostPops (sub NTNb 4))
      (prin :predefList4FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Pre () 1 2))
      (:putLostPops (sub NTNb 2))
      (prin :predefPreFuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Post () 2 1))
      (:putLostPops (sub NTNb 2))
      (prin :predefInversePostFuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Pre () 2 1))
      (:putLostPops (sub NTNb 2))
      (prin :predefInversePreFuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Fix () 2 1))
      (:putLostPops (sub NTNb 2))
      (prin :predefInverseArity2FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(Fix () 3 2 1))
      (:putLostPops (sub NTNb 3))
      (prin :predefInverseArity3FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(List () 2 1))
      (:putLostPops (sub NTNb 2))
      (prin :predefInverseList2FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template '(List () 3 2 1))
      (:putLostPops (sub NTNb 3))
      (prin :predefInverseList3FuncName "(" (cadr template) ") ;")
      )
     ;; Very special cases of dummy trees with "none()" subtrees:
     ((:equalTemplate template `(Fix () (Fix ,(:getOpIndex 'none)) (Fix ,(:getOpIndex 'none))))
      (:putLostPops NTNb)
      (prin :predefDumArity2FuncName "(" (cadr template) ") ;")
      )
     ((:equalTemplate template `(Fix () (Fix ,(:getOpIndex 'none))))
      (:putLostPops NTNb)
      (prin :predefDumArity1FuncName "(" (cadr template) ") ;")
      )
     ;; Other templates for which there is no predefined action function: 
     (t
      (:putPops NTNb)
      (:putCaseTemplate template)
      )
     )
    (print "}")
))

(de :equalTemplate (template model)
    (if (and (listp template) (listp model))
	(and (eq (car template) (car model))
	     (eq (length template) (length model))
	     (or (null (cadr model)) (eq (cadr template) (cadr model)))
	     (:equalListTemplates (cddr template) (cddr model)))
        (eq template model)
))

(de :equalListTemplates (listTemplate listModel)
    (or (and (null listTemplate) (null listModel))
	(and (:equalTemplate (car listTemplate) (car listModel))
	     (:equalListTemplates (cdr listTemplate) (cdr listModel)))
))

;;;----------------------------------------------------------
;;; Actual printing of the tree-building instructions:
;;;----------------------------------------------------------

(de :putCaseTemplate (template)
    (if (and (consp template) (eq (car template) 'Case))
	(let ((caseVar (cadr template))
	      (cases (cddr template))
	      case caseVarString
	      )
	  (setq caseVarString
		(if (numberp caseVar)
		    (:numberedTreeVariable caseVar)
					;Else the caseVar is a metavar : ('Var <name> <used> ...)
		  (:metaVariable caseVar)))
	  (:putMetaVariableDecls cases)
	  (while (nextl cases case)
	    (when (neq 't (caar case))
	      (prin "if (")
	      (:putOneCase caseVarString (car case))
	      (print ") "))
	    (print "{")
	    (:putCaseTemplate (caddr case))
	    (if (car cases)
		(prin "} else ")
	      (prin "}"))
	))
      (prin :pushFuncName "(")
      (:putTemplate template)
      (prin ") ;")
))

(de :putOneCase (caseVarString choices)
    (ifn (cdr choices)
	(:putOneChoice caseVarString (car choices) (list t))
	(prin "(")
	(let (choice)
	  (while (nextl choices choice)
	    (:putOneChoice caseVarString choice (list t))
	    (when choices (prin " || "))
	))
	(prin ")")
))

(de :putOneChoice (caseVarString template toIsFirst)
    (cond
     ((null template)
      )
     ((stringp template)
      (if (car toIsFirst) (rplaca toIsFirst ()) (prin " && "))
      (prin "(" caseVarString " == " template ")")
      )
     ((consp template)
      (selectq (car template)
	       (Fix
		(if (car toIsFirst) (rplaca toIsFirst ()) (prin " && "))
		(prin "(" :treeOpCodeFuncName "(" caseVarString ") == " (cadr template) ")")
		(:putSonsChoice caseVarString (cddr template) toIsFirst)
		)
	       (Atom
		(if (car toIsFirst) (rplaca toIsFirst ()) (prin " && "))
		(prin "(" :treeOpCodeFuncName "(" caseVarString ") == " (cadr template) ")")
		(:putOneChoice (concat :treeAtomValueFuncName "(" caseVarString ")")
			       (caddr template) toIsFirst)
		)
	       (List
		(if (car toIsFirst) (rplaca toIsFirst ()) (prin " && "))
		(prin "(" :treeOpCodeFuncName "(" caseVarString ") == " (cadr template) ")")
		(prin " && (" :treeNbSonsFuncName "(" caseVarString
		      ") == " (length (cddr template)) ")")
		(:putSonsChoice caseVarString (cddr template) toIsFirst)
		)
	       (Pre
		;???
		)
	       (Post
		;???
		)
	       (List2
		(if (car toIsFirst) (rplaca toIsFirst ()) (prin " && "))
		(prin "(" :treeOpCodeFuncName "(" caseVarString ") == " (cadr template) ")")
		(:putOneChoice (concat :treeListOfSonsFuncName "(" caseVarString ")")
			       (caddr template) toIsFirst)
		)
	       (Var
		(when (caddr template) ;When template is 'used:
		  (if (car toIsFirst) (rplaca toIsFirst ()) (prin " && "))
		  (prin "(" (:metaVariable template) "=" caseVarString ")")
		))
      ))
))

(de :putSonsChoice (caseVarString templates toIsFirst)
    (let ((sonRank 0))
      (while templates
	(incr sonRank)
	(:putOneChoice (concat :treeDownFuncName "(" caseVarString ", " sonRank ")")
		       (nextl templates) toIsFirst)
)))

(de :putTemplate (template)
    (cond
     ((null template)
      (prin "yytext")
      )
     ((numberp template)
      (prin (:numberedTreeVariable template))
      )
     ((stringp template)
      (prin """" template """")
      )
     ((consp template)
      (selectq (car template)
	       (Fix
		(prin :makeFixTreeFuncName "(" :rankOperFuncName "(" (cadr template) "), ")
		(:putListTemplates (cddr template))
		(prin ")")
		)
	       (Atom
		(prin :atomicTreeFuncName  "(" :rankOperFuncName "(" (cadr template) "), ")
		(:putTemplate (caddr template))
		(prin ")")
	       )
	       (List
		(prin :makeListTreeFuncName "(" :rankOperFuncName "(" (cadr template) "), ")
		(:putListTemplates (cddr template))
		(prin ")")
		)
	       (Pre
		(prin :preListFuncName "(")
		(:putTemplate (caddr template))
		(prin ", ")
		(:putTemplate (cadddr template))
		(prin ")")
		)
	       (Post
		(prin :postListFuncName "(")
		(:putTemplate (caddr template))
		(prin ", ")
		(:putTemplate (cadddr template))
		(prin ")")
		)
	       (List2
		(prin :listFromSonsFuncName "(" :rankOperFuncName "(" (cadr template) "), ")
		(:putTemplate (caddr template))
		(prin ")")
		)
	       (Var
		(prin (:metaVariable template))
		)
      ))
     (t
      (prin :nullTreeFuncName "()")
      )
))

(de :putListTemplates (listTemplate)
    (ifn listTemplate
      (prin "(" :listTreeType "*)NULL")
      (prin :consSonFuncName "(")
      (:putTemplate (car listTemplate))
      (prin ",")
      (:putListTemplates (cdr listTemplate))
      (prin ")")
))

(de :putLostPops (number)
    (repeat number (prin :popFuncName "(); "))
    (when (gt number 0) (print))
)

(de :putPops (popNumber)
    (while (gt popNumber 0)
       (print :treeType " *" (:numberedTreeVariable popNumber) " = " :popFuncName "(); ")
       (decr popNumber)
))

(de :putMetaVariableDecls (cases)
   (let ((tnames ())
	 (lnames ())
	 (anames ())
	 case metavars metavar name
	)
      (while (nextl cases case)
	 (setq metavars (cadr case))
	 (while (nextl metavars metavar)
	   (when (caddr metavar) ;When metavar is 'used:
	    (setq name (:metaVariable metavar))
	    (selectq (cadddr metavar)
		     ('tree (unless (member name tnames)(newl tnames name))
		      )
		     ('list (unless (member name lnames)(newl lnames name))
		      )
		     ('atom (unless (member name anames)(newl anames name))
		      )
      ))))
      (when tnames
	(prin :treeType " ")
	(while (nextl tnames name)
	   (prin "*" name)
	   (when tnames (prin ", ")))
	(print " ;"))
      (when lnames
	(prin :listTreeType " ")
	(while (nextl lnames name)
	   (prin "*" name)
	   (when lnames (prin ", ")))
	(print " ;"))
      (when anames
	(prin :atomValueType " ")
	(while (nextl anames name)
	   (prin name)
	   (when anames (prin ", ")))
	(print " ;"))
))

;;;----------------------------------------------------------
;;; Strings of predefined C functions and variables on trees:
;;;----------------------------------------------------------

(defvar :predefArity5FuncName "prTt5")
(defvar :predefArity4FuncName "prTt4")
(defvar :predefArity3FuncName "prTt3")
(defvar :predefArity2FuncName "prTt2")
(defvar :predefArity1FuncName "prTt1")
(defvar :predefArity0FuncName "prTt0")
(defvar :predefList5FuncName "prTt5")
(defvar :predefList4FuncName "prTt4")
(defvar :predefList3FuncName "prTt3")
(defvar :predefList2FuncName "prTt2")
(defvar :predefList1FuncName "prTt1")
(defvar :predefList0FuncName "prTt0")
(defvar :predefInverseArity3FuncName "prTtr3")
(defvar :predefInverseArity2FuncName "prTtr2")
(defvar :predefInverseList3FuncName "prTtr3")
(defvar :predefInverseList2FuncName "prTtr2")
(defvar :predefPostFuncName "prTpost")
(defvar :predefPreFuncName "prTpre")
(defvar :predefInversePostFuncName "prTrpost")
(defvar :predefInversePreFuncName "prTrpre")
(defvar :predefDumArity2FuncName "prTdf2")
(defvar :predefDumArity1FuncName "prTdf1")

(defvar :treeType "Tree")

(defvar :listTreeType "ListTree")

(defvar :atomValueType "AtomValue")

(de :numberedTreeVariable (number) (concat "t" number))

(de :metaVariable (x) (concat "meta" (cadr x)))

(defvar :makeFixTreeFuncName "mkTree")

(defvar :makeListTreeFuncName "mkTree")

(defvar :rankOperFuncName "stbRkOper")

(defvar :consSonFuncName "mkSons")

(defvar :postListFuncName "mkPost")

(defvar :preListFuncName "mkPre")

(defvar :listFromSonsFuncName "mkTree")

(defvar :atomicTreeFuncName "mkAtom")

(defvar :nullTreeFuncName "stbMkNull")

(defvar :treeOpCodeFuncName "treeOpCode")

(defvar :treeNbSonsFuncName "treeNbSons")

(defvar :treeAtomValueFuncName "treeAtomValue")

(defvar :treeListOfSonsFuncName "treeSons")

(defvar :treeDownFuncName "treeDown")

(defvar :popFuncName "stbPop")

(defvar :pushFuncName "stbPush")
