; $Id: metalcheck.ll,v 1.1 10/.0/.0 .1:.1:.2 vmp Exp $
(setq #:sys-package:colon 'metal)

; utilitaries
(dmd :make-lcid-tree (op-name)
    `({tree}:make ({name}:operator 'lcid METAL) ,op-name))


(de #:metal:tc:post (tool message-name message-class message-pos values)
    (#:generic-tool:post tool 'typechecker-messages
			 message-name message-class
			 message-pos ()
			 values))




; 
; 0] The toplevel of the checker
;
(de :check (tool tree)
    (let ((:*tool* tool)
	  (GLOBAL-OPERATOR-LIST ())
	  (GLOBAL-PHYLUM-LIST ())
	  (GLOBAL-NB-OPERATOR -1)
	  (GLOBAL-NB-PHYLUM -1)
	  (GLOBAL-USED-PHYLUM-LIST ())
	  (GLOBAL-RULE-LIST ())
	  (GLOBAL-NON-TERMINAL-LIST ())
	  (GLOBAL-NON-TERMINAL-USED-LIST ())
	  (GLOBAL-USED-OPERATOR-LIST ())
	  (METAL ({name}:formalism 'METAL))
	  (EVERY-PHYLUM ()))
	 (let ((language-name ({tree}:atom_value ({tree}:down tree 1)))
	       (description ({tree}:down tree 2)))
	      ;; with the traversal, we verify the multiple declarations
	      ({zone_s}:check-description description) ;description is a zone_s
	      ;; verifications concerning the PHYLA
	      (:check-phylum-declaration 
	       GLOBAL-USED-PHYLUM-LIST GLOBAL-PHYLUM-LIST)
	      (:check-phylum-use
	       GLOBAL-USED-PHYLUM-LIST GLOBAL-PHYLUM-LIST)
	      (let (((phylum-dependance-graph . operator-dependance-graph)
		     (:build-dependance-graphs
		      GLOBAL-PHYLUM-LIST (1+ GLOBAL-NB-PHYLUM)
		      GLOBAL-OPERATOR-LIST (1+ GLOBAL-NB-OPERATOR))))
		   (:check-non-circularity phylum-dependance-graph
					   (1+ GLOBAL-NB-PHYLUM))
		   ;; verifications concerning the OPERATORS
		   (:check-operator-in-phylum operator-dependance-graph
					      (1+ GLOBAL-NB-OPERATOR))
		   ;; verifications in the rule parts
		   (:check-operator-arity)
		   ;; verification of correspondances between semantics and rhs
		   (:check-non-terminals)
		   ;; check of the PHYLUM EVERY
		   (:check-phylum-EVERY GLOBAL-OPERATOR-LIST tree)
		   t))))

;
; 1] The traversal of the metal description construct several global list
; and make a few elementary verifications 
;

(de {zone_s}:check-description (zone_s)
    (mapc '{zone}:check-description ({tree}:sons zone_s))
    zone_s)

(de {zone}:check-description (zone)
    (selectq ({tree}:operator-name zone)
      (chapt ({zone_s}:check-description
	        ({tree}:down zone 2)))
      (rule_s (mapc '{rule}:check-rule ({tree}:sons zone)))
      (abs_syn ({abs_syn}:check-abstract-syntax zone))
      (t (exit OU-SUIS-JE1 zone))))

;
; Traversal of the rules
;

(de {rule}:check-rule (rule)
    (newl GLOBAL-RULE-LIST rule)
    (let ((production ({tree}:down rule 1)))
	 ({absyn-construct}:check  
	  ({tree}:down rule 2)
	  ({tree}:down production 2)
	  ({consyn-construct}:check production))
	 ))
	  
;
; Traversal of consyn part of the rules
;

(de {consyn-construct}:check (production)
    (lets ((atom({tree}:atom_value ({tree}:down production 1)))
	   (rhs ({tree}:down production 2))
	   (#:compilo-Metal:*name*+rhs (assq atom GLOBAL-NON-TERMINAL-LIST)))
	  (when (and #:compilo-Metal:*name*+rhs 
		     ({tree}:equal rhs (cdr #:compilo-Metal:*name*+rhs)))
	    (let ((position 
		   ;;Show position of both rules!
		   ({path}:merge
		    ({tree}:path ({tree}:down production 1))
		    ({tree}:path ({tree}:down 
				  ({tree}:up (cdr #:compilo-Metal:*name*+rhs) 1)
				  1))
		    )))
		 (#:metal:tc:post :*tool* 
		  'two_equal_rules 'Error ()
		  (list (list atom position)))
		 ))
	  (newl GLOBAL-NON-TERMINAL-LIST (cons atom rhs))
	  (let ((rhs-elem-list ()))
	       (mapc (lambda (elem)
		       (selectq ({tree}:operator-name elem)
				(non_terminal
				 (let* ((#:compilo-Metal:*name* ({tree}:atom_value elem))
					(elem+count (assq #:compilo-Metal:*name* 
							  rhs-elem-list)))
				   (if elem+count
				       (rplacd elem+count
					       (1+ (cdr elem+count)))
				       (newl rhs-elem-list
					     (cons #:compilo-Metal:*name* 1))
				       (newl GLOBAL-NON-TERMINAL-USED-LIST
					     (cons #:compilo-Metal:*name* elem)))))
				(genericatom
				 (newl rhs-elem-list 
				       (cons ({tree}:atom_value elem) 1)))))
		     ({tree}:sons rhs))
	       rhs-elem-list)))
    
;
; Traversal of absyn part of the rules
;

(de {absyn-construct}:check (semantic rhs-tree rhs-elem-list)
    (selectq ({tree}:operator-name semantic)
	     ((metavar ucid terminal string othersop) ())
	     (genericatom
	      (unless (assq ({tree}:atom_value semantic) rhs-elem-list)
		(#:metal:tc:post :*tool* 
		 'bad_generic_right 'Error ()
		 (list (list ({tree}:atom_value semantic) ({tree}:path semantic))
		       (cons rhs-tree (cons ({tree}:path rhs-tree) 'private))))
		))
	     (dot_non_terminal
	      (let ((non-terminal ({tree}:atom_value ({tree}:down semantic 1)))
		    (count ({tree}:atom_value ({tree}:down semantic 2))))
		   (let ((non-terminal-xref
			  (cassq non-terminal rhs-elem-list)))
			(ifn non-terminal-xref
			     (#:metal:tc:post :*tool*
			      'non_corresp_nt 'Error ()
			      (list (list non-terminal 
					  ({tree}:path ({tree}:down semantic 1)))
				    (cons rhs-tree
					  (cons ({tree}:path rhs-tree) 'private))
				    ))
			     (when (> (implode (explode count))	; count is symbol
				      non-terminal-xref)
			       (#:metal:tc:post :*tool*
				'bad_nt_count 'Error ()
				(list 
				 (list count ())
				 (list semantic ({tree}:path semantic))))
			       )))))
	     (non_terminal
	      (let* ((non-terminal ({tree}:atom_value semantic))
		     (non-terminal-xref
		      (cassq non-terminal rhs-elem-list)))
		(unless non-terminal-xref
		  (#:metal:tc:post :*tool*
		   'non_corresp_nt 'Error ()
		   (list (list non-terminal ({tree}:path semantic))
			 (cons rhs-tree (cons ({tree}:path rhs-tree) 'private))))
		  )))
	     ((named_listop named_atomop node)
	      (newl GLOBAL-USED-OPERATOR-LIST 
		    (cons ({tree}:atom_value ({tree}:down semantic 1)) semantic))
	      (mapc (lambda (x)
		      ({absyn-construct}:check x rhs-tree rhs-elem-list))
		    ({tree}:sons ({tree}:down semantic 2))))
	     (factor_s (mapc (lambda (x)
			       ({absyn-construct}:check x rhs-tree rhs-elem-list))
			     ({tree}:sons semantic)))
	     (letop ({absyn-construct}:check 
		     ({tree}:down ({tree}:down semantic 1) 1) 
		     rhs-tree
		     rhs-elem-list)
		    ({absyn-construct}:check ({tree}:down semantic 2) 
					     rhs-tree
					     rhs-elem-list))

	     (caseop
	      ({absyn-construct}:check ({tree}:down semantic 1)
				       rhs-tree
				       rhs-elem-list)
	      (mapc (lambda (alternative)
		      (mapc (lambda (x)
			      ({absyn-construct}:check
			       x rhs-tree rhs-elem-list))
			    ({tree}:sons ({tree}:down alternative 1)))
		      ({absyn-construct}:check 
		       ({tree}:down alternative 2) 
		       rhs-tree
		       rhs-elem-list))
		    ({tree}:sons ({tree}:down semantic 2))))
	     (t (exit OU-SUIS-JE2 ({tree}:operator-name semantic)))))

;
; Traversal of the abstract syntax part
;

(de {abs_syn}:check-abstract-syntax (abs_syn)
    (mapc (lambda (op_sort)
		  (selectq ({tree}:operator-name op_sort)
		      (operator ({operator}:check-abstract-syntax op_sort))
		      (sort ({sort}:check-abstract-syntax op_sort))
		      (t (exit OU-SUIS-JE3 op_sort))))
	  ({tree}:sons abs_syn))
    abs_syn)

(de {sort}:check-abstract-syntax (sort)
    (let ((phylum-name ({tree}:atom_value ({tree}:down sort 1))))
         (when (eq phylum-name 'COMMENT)
	   (#:metal:tc:post :*tool* 
	    'redef_predef_phylum 'Warning ()
	    (list (list phylum-name ({tree}:path ({tree}:down sort 1))))))
	 (cond ((assq phylum-name GLOBAL-PHYLUM-LIST)
		(let ((decl-tree (:find-occurence phylum-name)))
		     (#:metal:tc:post :*tool* 
		      'double_decl_phylum 'Error ()
		      (list (cons "declaration" (cons ({tree}:path decl-tree) 'private))
			    (list phylum-name ({tree}:path ({tree}:down sort 1))))))
		;;This seems silly. Consider the first declaration the right one!
		;;(newl GLOBAL-PHYLUM-LIST
		;;(mcons phylum-name GLOBAL-NB-PHYLUM
		;;({sort}:check-subsort ({tree}:down sort 2))))
		)
	       ((eq 'EVERY phylum-name)
		(if EVERY-PHYLUM
		    (let ((decl-tree (:find-occurence phylum-name)))
			 (#:metal:tc:post :*tool* 
			  'double_decl_phylum 'Error ()
			  (list (cons "declaration" 
				      (cons ({tree}:path decl-tree) 'private))
				(list phylum-name ({tree}:path ({tree}:down sort 1))))
			  ))
		    (setq EVERY-PHYLUM ({tree}:down sort 2))))
	       (t (newl GLOBAL-PHYLUM-LIST
			(mcons phylum-name (incr GLOBAL-NB-PHYLUM)
			    ({sort}:check-subsort ({tree}:down sort 2))))))))

(de {sort}:check-subsort (oplist)
    (mapc (lambda (uclcid)
		  (when (eq ({tree}:operator-name uclcid) 'ucid) ;subsort case
			(:add-in-list ({tree}:atom_value uclcid) uclcid
			 GLOBAL-USED-PHYLUM-LIST)))
	  ({tree}:sons oplist))
    oplist)

(de {operator}:check-abstract-syntax (op)
    (let ((lcid ({tree}:atom_value ({tree}:down op 1))))
	 (when (memq lcid '(meta comment comment_s))
	   (#:metal:tc:post :*tool* 
	    'redef_predef_op 'Warning ()
	    (list (list lcid ({tree}:path ({tree}:down op 1))))))
	 (if (assq lcid GLOBAL-OPERATOR-LIST)
	     (progn 
	       (let ((decl-tree (:find-op-occurence lcid)))
		    (#:metal:tc:post :*tool* 
		     'double_decl_op 'Error ()
		     (list (cons "declaration" (cons ({tree}:path decl-tree) 'private))
			   (list lcid ({tree}:path ({tree}:down op 1))))))
	       ({sons_description}:check-abstract-syntax ({tree}:down op 2)))
	     ({sons_description}:check-abstract-syntax ({tree}:down op 2))
	     (newl GLOBAL-OPERATOR-LIST
		   (mcons
			 lcid (incr GLOBAL-NB-OPERATOR) op)))))

(de {sons_description}:check-abstract-syntax (sons_description)
    (selectq ({tree}:operator-name sons_description)
	(ucid_s ({ucid_s}:check-abstract-syntax sons_description))
	((at_impl_constraint at_impl) ())
	((star_arbitrary plus_arbitrary)
	  (:add-in-list ({tree}:atom_value ({tree}:down sons_description 1))
	      ({tree}:down sons_description 1)
	      GLOBAL-USED-PHYLUM-LIST))
	(t (exit OU-SUIS-JE4 sons_description)))
    sons_description)

(de {ucid_s}:check-abstract-syntax (ucid_s)
    (mapc (lambda (ucid)
	     (:add-in-list ({tree}:atom_value ucid) ucid
		 GLOBAL-USED-PHYLUM-LIST))
	  ({tree}:sons ucid_s))
    ucid_s)

; :add-in-list does a side effect on the list given as second argument
(dmd :add-in-list (elt tree list)
    `(let ((elt ,elt))
	  (unless (cassq elt ,list)(newl ,list (cons elt ,tree)))))

;
; 2] The first verifications concerning PHYLA
;

(de :check-phylum-declaration (used-name-list declared-assoc-list)
    (mapc
     (lambda ((#:compilo-Metal:*name* . tree))
       (unless (assq #:compilo-Metal:*name* declared-assoc-list)
	 (#:metal:tc:post :*tool* 
	  'undefined_phylum 'Error ()
	  (list (list #:compilo-Metal:*name* ({tree}:path tree))))
	 ))
     used-name-list))

(de :check-phylum-use (used-name-list declared-assoc-list)
    (mapc
	 (lambda ((#:compilo-Metal:*name* . ops))
		 (unless (assq #:compilo-Metal:*name* used-name-list)
		   (#:metal:tc:post :*tool* 
		    'unused_phylum 'Warning ()
		    (list (list #:compilo-Metal:*name* 
				({tree}:path ({tree}:left (cdr ops) 1)))))
		   ))
	 declared-assoc-list))

;
; 3] We construct for the sequel the phyla dependency graph
;

(de :build-dependance-graphs (phylum-list nb-phylum operator-list nb-operator)
    (let ((phylum-graph (makevector nb-phylum ()))
	  (operator-graph (makevector nb-operator ())))
	 (mapc
	    (lambda ((phylum-name phylum-ref . uclcid_s))
		    (:dependances uclcid_s
			phylum-name phylum-ref
			phylum-graph operator-graph
			phylum-list operator-list))
	    phylum-list)
	 (cons phylum-graph operator-graph)))

(de :dependances (uclcid_s phylum-name phylum-ref
		  phylum-graph operator-graph phylum-list operator-list)
    (let ((dependances ()))
	 (mapc
	    (lambda (uclcid)
		    (selectq ({tree}:operator-name uclcid)
			(lcid
			     (let ((operator-xref
				       (cassq ({tree}:atom_value uclcid)
					      operator-list)))
				  (if operator-xref
				      (vset operator-graph (car operator-xref)
					      (cons phylum-ref
						(vref operator-graph
						      (car operator-xref))))
				      (#:metal:tc:post 
				       :*tool* 
				       'undefined_op 'Error ()
				       (list (list ({tree}:atom_value uclcid)
						   ({tree}:path uclcid))))
				      )))
			(ucid
			     (let ((phylum-xref
				       (cassq ({tree}:atom_value uclcid)
					      phylum-list)))
				  (if phylum-xref
				      (newl dependances (car phylum-xref))
				      (#:metal:tc:post
				       :*tool*
				       'undefined_phylum 'Error ()
				       (list (list ({tree}:atom_value uclcid)
						   ({tree}:path uclcid))))
				      )))
			))
	    ({tree}:sons uclcid_s))
	 (vset phylum-graph phylum-ref (cons phylum-name dependances))))

;
; And now, we can check this graph for circularity
; (mark can be replaced by an invisible bit on a vertex)
;

(de :check-non-circularity (graph nb-vertex)
    (let ((GLOBAL-STACK ())
	  (mark (makevector nb-vertex ())))
	 (for (vertex 0 1 (1- nb-vertex))
	      (unless (vref mark vertex)
		      (:visit vertex mark graph)))))

(de :visit (vertex mark graph)
    (if (memq vertex GLOBAL-STACK)
	(let ((cycle (list vertex)))
	     (until (= vertex (car GLOBAL-STACK))
		    (newl cycle (nextl GLOBAL-STACK)))
	     (newl cycle vertex)
	     (mapc (lambda (x)
		     (let ((phylum-name (car (vref graph x)))
			   (phylum-strings "")
			   )
			  ;;There is a variable number of arguments
			  ;;possible for the phyla. So the message text
			  ;;cannot be static. 
			  (mapc (lambda (x) 
				  (let ((p-name (car (vref graph x))))
				       (setq phylum-strings
					     (catenate phylum-strings 
						       " " (string p-name)))))
				cycle) ;phylum-name
			  (#:metal:tc:post :*tool*
			   'circ_phylum_def 'Error ()
			   (list (list phylum-strings
				       ({tree}:path (:find-occurence phylum-name))
				       )))
			  ))
		   (cdr cycle)))
	(unless (vref mark vertex)
	  (newl GLOBAL-STACK vertex)
	  (vset mark vertex t)
	  (mapc (lambda (vertex)
		  (:visit vertex mark graph))
		(cdr (vref graph vertex)))
	  (setq GLOBAL-STACK (cdr GLOBAL-STACK)))))

(dmd :find-occurence (#:compilo-Metal:*name*)
    `({tree}:left (cdr (cassq ,#:compilo-Metal:*name* GLOBAL-PHYLUM-LIST)) 1))

(dmd :find-op-occurence (#:compilo-Metal:*name*)
    `(cdr (cassq ,#:compilo-Metal:*name* GLOBAL-OPERATOR-LIST)))

;
; 4] Verification concerning the operators
;


;
; We verify the definition of operators
;

(de :check-operator-in-phylum (graph nb-vertex)
    (for (vertex 0 1 (1- nb-vertex))
	 (unless (vref graph vertex)
            (let (((op-name . def)
		   (any (lambda ((operator-name operator-ref . def))
				(when (= operator-ref vertex)
				      (cons operator-name def)))
			GLOBAL-OPERATOR-LIST)))
		 (#:metal:tc:post :*tool* 
		  'op_not_in_phylum 'Warning ()
		  (list (list op-name ({tree}:path ({tree}:down def 1)))))
		 ))))


;
; We verify the use of operators
;

(de :check-operator-arity ()
    (mapc '{absyn-part}:check-operator-arity GLOBAL-USED-OPERATOR-LIST))

(de {absyn-part}:check-operator-arity ((op-name . absyn-part))
    (let ((op-def (cassq op-name GLOBAL-OPERATOR-LIST))
	  (op-use ({tree}:down absyn-part 1)))
	 (ifn op-def
	      (if (eq op-name 'meta)
		  (unless (eq ({tree}:operator-name absyn-part) 'named_atomop)
		    (#:metal:tc:post :*tool*
		     'bad_arity_op 'Error ()
		     (list (list op-name ({tree}:path op-use))
			   (list 'singleton ())))
		    )
		  (#:metal:tc:post :*tool* 
		   'undefined_op 'Error ()
		   (list (list op-name ({tree}:path op-use))))
		  )
	      (lets ((description ({tree}:down (cdr op-def) 2))
		     (decl-arity (length ({tree}:sons description)))
		     (used-arity (length ({tree}:sons ({tree}:down absyn-part 2))))
		     )
		    ;;The absyn-part is the tree construction part of a rule.
		    (selectq ({tree}:operator-name description)
			     ((star_arbitrary plus_arbitrary)
			      ;;Right hand side is a list declaration.
			      (:check-list-op-arity op-name absyn-part description))
			     (ucid_s
			      ;;Right hand side of operator declaration
			      ;;is a list of sorts.
			      (selectq ({tree}:operator-name absyn-part)
				       (node
					;;Used as fixed also.
					(unless (eqn decl-arity used-arity)
					  (#:metal:tc:post
					   :*tool*
					   'bad_arity_op 'Error ()
					   (list (list op-name ({tree}:path op-use))
						 (cons decl-arity 
						       (cons
							({tree}:path description)
							'private))))
					  ))
				       (named_listop
					;;Used as list
					(#:metal:tc:post 
					 :*tool*
					 'bad_arity_op 'Error ()
					 (list (list op-name ({tree}:path op-use))
					       (cons decl-arity
						     (cons
						      ({tree}:path description)
						      'private))))
					)
				       (named_atomop
					(when (neqn decl-arity 0)
					  (#:metal:tc:post 
					   :*tool*
					   'bad_arity_op 'Error ()
					   (list (list op-name ({tree}:path op-use))
						 (cons decl-arity
						       (cons
							({tree}:path description)
							'private))))
					  ))
				       ))
			     ((at_impl at_impl_constraint)
			      ;;Right hand side of operator declaration
			      ;;is "implemented as ..."
			      ;;Thus arity = 0.
			      ;;Check against use.
			      (let ((impl-type ({tree}:atom_value
						({tree}:down description 1))))
				   (cond 
				    ((eqn used-arity 0)
				     (unless (eq impl-type 'SINGLETON)
				       (#:metal:tc:post
					:*tool*
					'bad_arity_op 'Error ()
					(list (list op-name ({tree}:path op-use))
					      (cons 'atomic
						    (cons
						     ({tree}:path description)
						     'private)))))
				     )
				    ((eqn used-arity 1)
				     ;;If = 1, check that
				     ;;operator not defined as singleton.
				     ;;OR, used-arity = 1 may be either the construction
				     ;;of an atomic operator, or the construction
				     ;;of an arity 1 operator, which is wrong.
				     (when (or (eq impl-type 'SINGLETON)
					     (eq ({tree}:operator-name absyn-part) 'node))
					 (#:metal:tc:post
					  :*tool*
					  'bad_arity_op 'Error ()
					  (list (list op-name ({tree}:path op-use))
						(cons 'atomic
						      (cons
						       ({tree}:path description)
						       'private)))))
					 )
				     (t 
				      (#:metal:tc:post 
				      :*tool*
				      'bad_arity_op 'Error ()
				      (list (list op-name 
						  ({tree}:path op-use))
					    (cons 'atomic
						  (cons
						   ({tree}:path description)
						   'private))))
				     ))))
			     (t (exit OU-SUIS-JE5 absyn-part))
			     )
		    ))))

(de :check-list-op-arity (op-name absyn-part operator)
    (ifn (eq 'named_listop ({tree}:operator-name absyn-part))
	 (#:metal:tc:post :*tool* 
	  'bad_arity_op 'Error ()
	  (list (list op-name ({tree}:path ({tree}:down absyn-part 1)))
		(cons 'list
		      (cons ({tree}:path operator)
			    'private))))
	 (when (and
		   (eq 'plus_arbitrary ({tree}:operator-name operator))
		   (eq 'listop
		       ({tree}:operator-name ({tree}:down absyn-part 2)))
		   (null ({tree}:sons
			  ({tree}:down ({tree}:down absyn-part 2) 1))))
	   (#:metal:tc:post :*tool* 
	    'plus_op_with_null 'Error ()
	    (list (list op-name ({tree}:path ({tree}:down absyn-part 1)))))
	   )))
					;
; 5] A minimum verification of rhs of concrete productions
;

(de :check-non-terminals ()
    (mapc (lambda ((non-terminal . tree))
		  (unless (assq non-terminal GLOBAL-NON-TERMINAL-LIST)
		    (#:metal:tc:post :*tool* 
		     'undefined_nt 'Error ()
		     (list (list non-terminal ({tree}:path tree))))
		    ))
	  GLOBAL-NON-TERMINAL-USED-LIST))


;
; 6] We check  the phylum EVERY if it exists
;

(de :check-phylum-EVERY (operator-list metal)
	 ; EVERY-PHYLUM contains the uclcid_s_op list of ops
	 (when EVERY-PHYLUM
	      (:check-EVERY operator-list EVERY-PHYLUM)))

(de :check-EVERY (operator-list uclcid_s)
    (let (warning)
	 (mapc (lambda ((op-name . rest))
		       (unless 
			  (any (lambda (uclcid)
					 (eq op-name 
					      ({tree}:atom_value uclcid)))
				 ({tree}:sons uclcid_s))
			  (when ({tree}:sons uclcid_s)
				(setq warning t)
				({tree}:follow ({tree}:down uclcid_s 1) 
				       (:make-lcid-tree op-name)))))
	       operator-list)
	 (when warning
	   (#:metal:tc:post :*tool*  
	    'phylum_every_modified 'Warning
	    EVERY-PHYLUM ())
	   )))

