; $Id: deccons.ll,v 1.1 10/.0/.0 .1:.1:.2 vmp Exp $
;==============================================================================
;==============================================================================
;
;                              DECODECONS
;
;------------------------------------------------------------------------------

; Impression des tables de construction d'arbres l_code.ll et l_sch.

(setq #:sys-package:colon 'compilo-Metal)

(de {generator}:save (#:compilo-Metal:*language* . flags)
    (let ((comment (memq 'commente flags)))
         (:printfunctions #:compilo-Metal:*language* comment)
         (:printpatterns #:compilo-Metal:*language* comment)
	 (when (memq 'module flags)
	       (:printmodule #:compilo-Metal:*language*)
	       )
         #:compilo-Metal:*language*))

(de :absolute-pathname (#:compilo-Metal:*langname* entryname)
;;; ------------------
;;; Returns the absolute pathname of entryname to use when writing an
;;; object file.
;;; Uses the syntaxManager, and if in 'compat mode returns
;;; <entryname>, meaning write in the current directory

;;[llh7jan11] simplification: always write in the current directory!!
;     (let ((location (#:remoteSyntax:absolute-location #:compilo-Metal:*langname*
; 						      ()
; 						       )))
; 	 (if location
; 	     (catenate location "/" entryname)
; 	     entryname))
    entryname
)
    
;------------------------------- module --------------------------------------

(de :printmodule (#:compilo-Metal:*language*)
;;; ------------
;;;
    (let* ((module-name (:module-name #:compilo-Metal:*language*))
	   (source-file (catenate module-name
				  "/" ({formalism}:name #:compilo-Metal:*language*) "_code"
				  )
			)
	   (module-file (:module-file module-name))
	   )
      (when module-file
	    (progn
	      (with ((printlength 0)(printlevel 0)(printline 0))
		    (withoutput module-file
				(:print-module module-name
					       (list source-file)
					       (:exported-functions #:compilo-Metal:*language*)
					       )
			  )
		    (printscreen module-file " created")
		    )
	      )
	    )
      )
    )


(de :module-name (#:compilo-Metal:*language*)
;;;  -----------
;;;
    (#:remoteSyntax:module-name ({formalism}:name #:compilo-Metal:*language*)
				()
				)
    )


(de :module-file (module-name)
;;; ------------
;;;
    (let ((file-name (catenate
		      (search-in-user-path
		       module-name
		       )
		      "/module.LM"
		      )
		     )
	  )
      (when (probefile file-name)
	    (let ((def (readdefmodule module-name)))
	      (unless (assq 'metal-version def)
		      (error '{generator}:save
			     "cannot overwrite user module"
			     file-name
			     )
		      )
	      )
	    )
      file-name
      )
    )


(de :exported-functions (#:compilo-Metal:*language*)
;;; -------------------
;;;
    (let ((func_table 
	   (symeval (symbol (concat 'parser ({formalism}:name #:compilo-Metal:*language*))
			    'vector
			    )
		    )
	   )
	  exported-functions
	  )
      (mapvector (lambda (entry)
		   (when entry (newl exported-functions entry))
		   )
		 func_table
		 )
      exported-functions
      )
    )

(de :print-module (#:compilo-Metal:*name* files export)
;;; -------------
;;; <#:compilo-Metal:*name*> is the module #:compilo-Metal:*name* (a symbol),
;;; files is the list of file-names (strings) of the module.
;;;
    (let ((#:system:print-for-read t))
      (print 'defmodule #:compilo-Metal:*name*)
      (print)
      (print 'Metal-version
	     (#:module:loaded-revision 'centaur/sources/comp_metal))
      (print)
      (print 'files files)
      (print)
      (print 'import  '(centaur/sources/vtp/cx
			centaur/sources/vtp/util
			centaur/sources/vtp/formalism
			centaur/sources/vtp/tree
			centaur/sources/vtp/meta
			centaur/sources/parser
			)
	     )
      (print)
      (print 'export export)
      )
    )


;------------------------------ functions ------------------------------------

(de :printfunctions (#:compilo-Metal:*language* comment)
    (let ((#:compilo-Metal:*langname* ({langage}:nom #:compilo-Metal:*language*))
          (filefunc) (func_table) (nbfunc)
          (#:sys-package:colon 'parser)
          (#:system:print-for-read t)
          (#:system:print-package-flag 0))
         (setq filefunc (:absolute-pathname #:compilo-Metal:*langname*
					    (catenate #:compilo-Metal:*langname* "_code.ll")))
         (setq func_table 
	       (symeval (symbol (concat 'parser #:compilo-Metal:*langname*) 'vector)))
         (setq nbfunc (sub1(vlength func_table)))
         (with ((printlength 0)(printlevel 0)(printline 0))
	  (withoutput filefunc
           (print `(defvar #:sys-package:colon  ',#:sys-package:colon))
           (print `(defvar ,(symbol (concat 'parser #:compilo-Metal:*langname*) 'nbfunc)
                           ,nbfunc))
	   (let ((pattern-var (symbol (concat 'parser #:compilo-Metal:*langname*) 'pattern)))
	     (print `(defvar ,pattern-var
		       (when (boundp ',pattern-var)
			     ,pattern-var
			     )
		       )
		    )
	     )
           (for (i 1 1 nbfunc)
                (let ((func (vref func_table i)))
                     (when func
                           (setq func (getdef func))
                           (when func
                                 (when comment (print))
                                 (funcall (if comment 'pprint 'print) func))))
	   )))
         (setq #:system:print-for-read ())
         (printscreen filefunc " created")))

;------------------------------ schemas --------------------------------------

(de :printpatterns (#:compilo-Metal:*language* comment)
    (let ((#:compilo-Metal:*langname* ({langage}:nom #:compilo-Metal:*language*))
          (filepatt) (table_name) (pattern_table) (nbpattern))
         (setq filepatt (:absolute-pathname #:compilo-Metal:*langname*
					    (catenate #:compilo-Metal:*langname* "_sch")))
         (setq table_name (symbol (concat 'parser #:compilo-Metal:*langname*) 'pattern))
         (setq pattern_table (symeval table_name))
         (setq nbpattern (vlength pattern_table))
         (with ((printlength 0)(printlevel 0)(printline 0))
	  (withoutput filepatt
		      (print nbpattern)
		      (unless (zerop nbpattern)
			      (rmargin (setq polish-write-linesize 80))
			      (setallunused)
			      (pwrbegin)
			      (for (i 0 1 (sub1 nbpattern))
				   (polishwrite {tree} (vref pattern_table i)))
			      (terpri)
			      (when comment
				    (printerror ':printpatterns
						"obsolete flag comment"
						"ignored")
				    ;; What was suppose to do the following?
				    ;; The ecrire function doesn't seem to
				    ;; exist, even in vtp:1.3 (89/01/26) [fm]
				    ;;(for (i 0 1 (sub1 nbpattern))
				    ;;  (ecrire {tree} (vref pattern_table i))
				    ;;  (terpri))
				    ))))
         (printscreen filepatt " created")))
