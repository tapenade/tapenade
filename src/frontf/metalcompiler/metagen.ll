;$Id: metagen.ll,v 1.1 10/.0/.0 .1:.1:.2 vmp Exp $
;=============================================================================
;-----------------------------------------------------------------------------
;                           Toplevel of the Metal Compiler
;----------------------------------------------------------------------------
(defun {metal}:compile (tree . flags)
    (let ((complete (car (memq 'complete flags)))
          (commente (car (memq 'commente flags)))
          (gen-module (car (memq 'module flags)))
          (n-compacte (car (memq 'non-compacte flags)))
	  langage)
         (tag semantic_error
	      (setq langage ({metal}:formalism tree complete))
	      ({metal}:yacc tree langage n-compacte)
)))

(defun {metal}:treebuilding (tree #:compilo-Metal:*language* . flags)
    ({generator}:save
        ({metal}:generator
            tree
            #:compilo-Metal:*language*
            (car (memq 'non-compacte flags))
            (car (memq 'module flags)))
        (car (memq 'commente flags))
        (car (memq 'module flags))))


(defun {metal}:post (Name Args)
    (ctmess-post
        (#:message:create
            (#:MessageModules:create-module
                (#:rmo:Formalism:formalismObject 'METAL) 'compiler-messages)
            Name
            'Error
            ()
            ()
            Args)))


;-----------------------------------------------------------------------------
;============================================================================;
;                              METAGENERATEUR
;
;----------------------------------------------------------------------------
(defvar #:sys-package:colon 'compilo-Metal)

(defun {metal}:generator (tree #:compilo-Metal:*language* . flags)
    (let ((rulenb 0)
          (nocompact (memq 'non-compacte flags))
          (nbpattern -1)
          (pattern_table ())
          (v_pattern_table ())
          (#:compilo-Metal:*langname* ({langage}:nom #:compilo-Metal:*language*))
          (packpattern ())
          (packfunc ())
          (packvector ())
          (package ()))
         (setq package (concat 'parser #:compilo-Metal:*langname*))
         (setq packfunc (symbol package 'func))
         (setq packvector (symbol package 'vector))
         (setq packpattern (symbol package 'pattern))
         (mapc 'remob (mapcan 'oblist (list packfunc package)))
         (:rule tree ':generaterule)
         (set packvector (:create_func_table #:compilo-Metal:*language* rulenb))
         (setq v_pattern_table (makevector (add1 nbpattern) ()))
         (setq pattern_table (nreverse pattern_table))
         (for (i 0 1 nbpattern) (vset v_pattern_table i (nextl pattern_table)))
         (set packpattern v_pattern_table)
         #:compilo-Metal:*language*))


;-------------------------- creation du code ---------------------------------
(defun :generaterule (tree)
    (let ((syntactic (bas tree 1))
          (semantic (bas tree 2))
          (nbnterm ())
          (body ())
          (pd1 0))
         (setq nbnterm (:nbofnonter syntactic))
         (incr rulenb)
         (setq body
             (append
                 (unless (eq nbnterm 0)
                         `((#:parser:init ,nbnterm)))
                 (:createcode semantic)))
         (setq body (:optimize body pd1))
         (when (car body)
               (:make_defun (symbol packfunc (concat 'f rulenb)) '() body))))


(defun :createcode (tree)
    (selectq ({operateur}:nom (operdearbre tree))
        ;----
        (non_terminal ;------------
                      (let ((n (:rankinsyntax (atomeval tree) 1 syntactic)))
                           (when (eq n 1)
                                 (incr pd1))
                           `((#:parser:pd ,n))))
        (dot_non_terminal ;----------------
                          (let ((n (:rankinsyntax
                                       (atomeval (bas tree 1))
                                       (:number (atomeval (bas tree 2)))
                                       syntactic)))
                               (when (eq n 1)
                                     (incr pd1))
                               `((#:parser:pd ,n))))
        (lcid )
        (ucid ;----
              `((#:parser:push (#:parser:getmetaval ',(atomeval tree)))))
        (node ;----
              (let ((s1 (bas tree 1))
                    (s2 (bas tree 2))
                    (ops1 ())
                    (nameops1 ())
                    (arits1 ())
                    (code ())
                    (lpop ()))
                   (setq ops1
                       (:tag_operdenom
                           (atomeval s1) #:compilo-Metal:*language* tree))
                   (setq nameops1 ({operateur}:nom ops1))
                   (setq arits1 (aritedeoper ops1))
                   (if (memq ops1 '(voidbid VOIDBID))
                       `((#:parser:push
                          (#:parser:makeatom
                           (loadeval
                            (operdenom
                             '_null_tree
                             (langdenom ',#:compilo-Metal:*langname*)))
                           ())))
                       (if (eq arits1 0)
                           `((#:parser:push
                              (#:parser:makeatom
                               (loadeval
                                (operdenom
                                 ',nameops1
                                 (langdenom ',#:compilo-Metal:*langname*)))
                               ())))
                           (append
                               (for (i ({arbre}:nbfils s2) -1 1 code)
                                    (setq code
                                        (append code (:createcode (bas s2 i)))))
                               `(,(cons
                                   '#:parser:push
                                   `((#:parser:arbre
                                      (loadeval
                                       (operdenom
                                        ',nameops1
                                        (langdenom ',#:compilo-Metal:*langname*)))
                                      ,@(for (i 1 1 arits1 lpop)
                                            (newl lpop `(#:parser:pop))))))))))))
        (named_atomop ;------------
                      (let ((s1 (bas tree 1))
                            (s2 (bas tree 2))
                            (ops1 ())
                            (nameops1 ()))
                           (setq ops1
                               (:tag_operdenom
                                   (atomeval s1) #:compilo-Metal:*language* tree))
                           (setq nameops1 ({operateur}:nom ops1))
                           (if (neq (aritedeoper ops1) 0)
                               ({metal}:post
                                   'gen_bad_arity
                                   (list
                                       (list (string rulenb) ())
                                       (list nameops1 ())))
                               (let ((s2s1 (bas s2 1)))
                                    (selectq ({operateur}:nom
                                                 (operdearbre s2s1))
                                        (genericatom ;-----------
                                                     `((#:parser:push
                                                        (#:parser:makeatom
                                                         (loadeval
                                                          (operdenom
                                                           ',nameops1
                                                           (langdenom
                                                            ',#:compilo-Metal:
                                                              *langname*)))
                                                         (#:parser:getatom)))))
                                        (string ;------
                                                `((#:parser:push
                                                   (#:parser:makeatom
                                                    (loadeval
                                                     (operdenom
                                                      ',nameops1
                                                      (langdenom
                                                       ',#:compilo-Metal:
                                                         *langname*)))
                                                    ,(:adjust_string
                                                      (atomeval s2s1))))))
                                        (ucid ;----
                                              `((let ((tree
                                                       (#:parser:getmetaval
                                                        ',(atomeval s2s1))))
                                                    (#:parser:push
                                                     (convertir
                                                      tree
                                                      (loadeval
                                                       (operdenom
                                                        ',nameops1
                                                        (langdenom
                                                         ',#:compilo-Metal:
                                                           *langname*))))))))
                                        (t
                                            ({metal}:post
                                                'gen_bad_atom
                                                (list (list (string rulenb) ())))))))))
        (named_listop ;------------
                      (let ((s1 (bas tree 1))
                            (s2 (bas tree 2))
                            (ops1 ())
                            (nameops1 ())
                            (code ())
                            (lpop ()))
                           (setq ops1
                               (:tag_operdenom
                                   (atomeval s1) #:compilo-Metal:*language* tree))
                           (setq nameops1 ({operateur}:nom ops1))
                           (selectq ({operateur}:nom (operdearbre s2))
                               (listop ;------
                                       (let ((s2s1 (bas s2 1)))
                                            (cond
                                                ((eq
                                                     ({operateur}:nom
                                                         (operdearbre s2s1))
                                                     'factor_s)
                                                    (if
                                                      (eq
                                                          ({arbre}:nbsons s2s1)
                                                          0)
                                                      `((#:parser:push
                                                         (#:parser:arbre
                                                          (loadeval
                                                           (operdenom
                                                            ',nameops1
                                                            (langdenom
                                                             ',#:compilo-Metal:
                                                               *langname*))))))
                                                      (append
                                                          (for (i
                                                                   ({arbre}:
                                                                    nbsons s2s1)
                                                                   -1
                                                                   1
                                                                   code)
                                                               (setq code
                                                                   (append
                                                                       code
                                                                       (:createcode
                                                                           (bas
                                                                               s2s1
                                                                               i)))))
                                                          `(,(cons
                                                              '#:parser:push
                                                              `((#:parser:arbre
                                                                 (loadeval
                                                                  (operdenom
                                                                   ',nameops1
                                                                   (langdenom
                                                                    ',#:compilo-Metal:
                                                                      *langname*)))
                                                                 ,@(for (i
                                                                         1
                                                                         1
                                                                         ({arbre}:
                                                                          nbsons
                                                                          s2s1)
                                                                         lpop)
                                                                       (newl
                                                                        lpop
                                                                        `(#:parser:
                                                                          pop))))))))))
                                                (t
                                                    (append
                                                        (:createcode s2s1)
                                                        `((#:parser:renamelist
                                                           (#:parser:pop)
                                                           (loadeval
                                                            (operdenom
                                                             ',nameops1
                                                             (langdenom
                                                              ',#:compilo-Metal:
                                                                *langname*))))))))))
                               (postop ;------
                                       (append
                                           (:createcode (bas s2 2))
                                           (:createcode (bas s2 1))
                                           `((#:parser:push
                                              (#:parser:post
                                               (#:parser:pop)
                                               (#:parser:pop))))))
                               (preop ;-----
                                      (append
                                          (:createcode (bas s2 2))
                                          (:createcode (bas s2 1))
                                          `((#:parser:push
                                             (#:parser:pre
                                              (#:parser:pop)
                                              (#:parser:pop)))))))))
        (caseop ;------
                (let ((s1 (bas tree 1)) (s2 (bas tree 2)) (code ()) (code1 ()))
                     (append
                         (:createcode s1)
                         `((let ((pop (#:parser:pop)))
                               ,(cons
                                 'cond
                                 (for (i 1 1 ({arbre}:nbsons s2) code)
                                     (let
                                      ((son (bas s2 i)) (sons1) (sons2))
                                      (setq sons1
                                          (bas son 1)
                                          sons2
                                          (bas son 2)
                                          code1
                                          ())
                                      (newr
                                       code
                                       (tag others
                                           (cons
                                            (cons
                                             'or
                                             (for (j
                                                   1
                                                   1
                                                   ({arbre}:nbsons sons1)
                                                   code1)
                                                 (let ((son (bas sons1 j)))
                                                     (if (eq
                                                          ({operateur}:nom
                                                           (operdearbre son))
                                                          'othersop)
                                                         (exit
                                                          others
                                                          `(t
                                                            ,@(:createcode sons2)))
                                                         (:generateschema son)
                                                         (newr
                                                          code1
                                                          `({arbre}:filtrer
                                                            pop
                                                            (vref
                                                             ,packpattern
                                                             ,nbpattern)
                                                            '#:parser:putmetaval))))))
                                            (:createcode sons2))))))))))))
        (letop ;-----
               (let ((s1 (bas tree 1)) (s2 (bas tree 2)))
                    (:generateschema (bas s1 1))
                    (append
                        (:createcode (bas s1 2))
                        `((if ({arbre}:filtrer
                               (#:parser:pop)
                               (vref ,packpattern ,nbpattern)
                               '#:parser:putmetaval)
                              ,(cons 'progn (:createcode s2)) (#:parser:cerr 1))))))))


(defun :optimize (body pd1)
    (cond
        ((and (equal (car body) '(#:parser:init 1))
              (equal (cadr body) '(#:parser:pd 1))
              (le pd1 1)) (cddr body))
        (t body)))


(defun :tag_operdenom (#:compilo-Metal:*name* #:compilo-Metal:*language* tree)
    (lock
        (lambda (tag val) (if (eq tag 'not_found)
                              (if
                                (memq #:compilo-Metal:*name* '(voidbid VOIDBID))
                                #:compilo-Metal:*name*
                                ({metal}:post
                                    'gen_undefined_op
                                    (list
                                        (list (string rulenb) ())
                                        (list #:compilo-Metal:*name* ()))))
                              val))
        ({name}:operator
            (:lower-diese #:compilo-Metal:*name*) #:compilo-Metal:*language*)))


(defun :create_func_table (#:compilo-Metal:*language* n)
    (let ((i 0)
          (package (concat 'parser ({langage}:nom #:compilo-Metal:*language*)))
          (func_table ())
          (func_name ()))
         (setq func_table (makevector (add1 n) ()))
         (repeat n
             (setq func_name (symbol packfunc (concat 'f (incr i))))
             (when (typefn func_name)
                   (vset func_table i func_name)))
         func_table))


(defun :nbofnonter (tree)
    (let ((nonternb 0))
         ({arbre}:for-all-sons
             (bas tree 2)
             (lambda (son)
                 (if (eq ({operateur}:nom (operdearbre son)) 'non_terminal)
                     (incr nonternb))))
         nonternb))


(defun :rankinsyntax (#:compilo-Metal:*name* occ tree)
    (let ((pos 0) (x 0))
         (tag found
             ({arbre}:for-all-sons
                 (bas tree 2)
                 (lambda (son)
                     (when
                       (eq ({operateur}:nom (operdearbre son)) 'non_terminal)
                       (incr pos)
                       (if (eq (atomeval son) #:compilo-Metal:*name*)
                           (if (= (incr x) occ)
                               (exit found pos))))))
             ({metal}:post
                 'gen_undefined_nt
                 (list
                     (list (string rulenb) ()) (list #:compilo-Metal:*name* ())))
             0)))


(defun :make_defun (#:compilo-Metal:*name* args body)
    (setfn #:compilo-Metal:*name* 'expr (cons args body)))


(defun :adjust_string (string)
    (catenate (nreverse (cdr (nreverse (cdr (pname string)))))))


(defun :number (x)
    (- (cascii x) 48))


;------------------------- creation des schemas -------------------------------
(defun :generateschema (tree)
    (incr nbpattern)
    (if (eq ({operateur}:nom (operdearbre tree)) 'othersop)
        (newl pattern_table (metavar 'X* #:compilo-Metal:*language*))
        (newl pattern_table (:createschema tree))))


(defun :createschema (tree)
    (selectq ({operateur}:nom (operdearbre tree))
        (ucid ;----
              (metavar (atomeval tree) #:compilo-Metal:*language*))
        (lcid ;----
              (:tag_operdenom
                  #:compilo-Metal:*name* #:compilo-Metal:*language* tree))
        (node ;----
              (let ((s1 (bas tree 1)) (s2 (bas tree 2)) (ops1 ()) (sons ()))
                   (setq ops1
                       (:tag_operdenom
                           (atomeval s1) #:compilo-Metal:*language* tree))
                   (arbre-s
                       (if (memq ({operateur}:nom ops1) '(voidbid VOIDBID))
                           (operdenom '_null_tree #:compilo-Metal:*language*)
                           ops1)
                       (for (i 1 1 ({arbre}:nbsons s2) sons)
                            (newr sons (:createschema (bas s2 i)))))))
        (named_atomop ;------------
                      (let ((s1 (bas tree 1)) (s2 (bas tree 2)) (ops1 ()))
                           (setq ops1
                               (:tag_operdenom
                                   (atomeval s1) #:compilo-Metal:*language* tree))
                           (let ((s2s1 (bas s2 1)))
                                (selectq ({operateur}:nom (operdearbre s2s1))
                                    (string ;------
                                            (arbre
                                                ops1
                                                (:make-actual-value
                                                    (atomeval s2s1))))
                                    (ucid ;----
                                          (metavar
                                              (atomeval s2s1)
                                              #:compilo-Metal:*language*
                                              0
                                              {tree}
                                              ({valeurtypee}:creer
                                                  ops1 {operator})))
                                    (t
                                        ({metal}:post
                                            'gen_bad_atom
                                            (list (list (string rulenb) ()))))))))
        (named_listop ;------------
                      (let ((s1 (bas tree 1))
                            (s2 (bas tree 2))
                            (ops1 ())
                            (names2 ())
                            (sons ()))
                           (setq ops1
                               (:tag_operdenom
                                   (atomeval s1) #:compilo-Metal:*language* tree))
                           (setq names2 ({operateur}:nom (operdearbre s2)))
                           (if (eq names2 'listop)
                               (let ((s2s1 (bas s2 1)))
                                    (selectq ({operateur}:nom
                                                 (operdearbre s2s1))
                                        (factor_s ;--------
                                                  (arbre-s
                                                      ops1
                                                      (for (i
                                                               1
                                                               1
                                                               ({arbre}:nbsons
                                                                   s2s1)
                                                               sons)
                                                           (newr
                                                               sons
                                                               (:createschema
                                                                   (bas s2s1 i))))))
                                        (ucid ;----
                                              (arbre
                                                  ops1
                                                  (metavar
                                                      (atomeval s2s1)
                                                      #:compilo-Metal:*language*
                                                      0
                                                      {sublist})))))
                               (let ((slist ()) (selem ()) (metavar ()))
                                    (if (eq names2 'postop)
                                        (setq slist (bas s2 1)
                                              selem (bas s2 2))
                                        (setq selem (bas s2 1)
                                              slist (bas s2 2)))
                                    (selectq ({operateur}:nom
                                                 (operdearbre slist))
                                        (ucid ;----
                                              (setq metavar
                                                  (metavar
                                                      (atomeval slist)
                                                      #:compilo-Metal:*language*
                                                      0
                                                      {sublist}))
                                              (arbre-s
                                                  ops1
                                                  (if (eq names2 'postop)
                                                      (list
                                                          metavar
                                                          (:createschema selem))
                                                      (list
                                                          (:createschema selem)
                                                          metavar))))
                                        (named_listop ; -----------
                                                      (if
                                                        ({operateur}:egale
                                                            ops1
                                                            (:tag_operdenom
                                                                (atomeval
                                                                    (bas
                                                                        slist 1))
                                                                #:compilo-Metal:
                                                                *language*
                                                                tree))
                                                        (arbre
                                                            ops1
                                                            (:createschema
                                                                (bas s2 1))
                                                            (:createschema
                                                                (bas s2 2)))
                                                        ({metal}:post
                                                            'gen_bad_pre-post
                                                            (list
                                                                (list
                                                                    (string
                                                                        rulenb)
                                                                    ())))))
                                        (t
                                            ({metal}:post
                                                'gen_bad_pre-post1
                                                (list (list (string rulenb) ())))))))))))


(defun :make-actual-value (str)
    (let* ((l (slength str))
           (interstr (makestring l #/a))
           (char (chrnth 1 str))
           (i 1)
           (offset 0))
          (while (lt (add1 i) l)
                 (when (eqn char #/')
                       (setq offset (add1 offset))
                       (setq i (add1 i)))
                 (chrset (sub i (add1 offset)) interstr char)
                 (setq i (add1 i))
                 (setq char (chrnth i str)))
          (substring interstr 0 (sub i (add1 offset)))))


