#!/usr/bin/env bash

if [ "$(uname)" = "Linux" ]; then
  docker run --rm -v "$PWD":/usr/tapenade/src/frontf -v "$PWD"/../../:/usr/tapenade -u $(stat -c "%u:%g" ./) -w /usr/tapenade/src/frontf gcc:latest make
else
  docker run --rm -v "$PWD":/usr/tapenade/src/frontf -v "$PWD"/../../:/usr/tapenade -w /usr/tapenade/src/frontf gcc:latest make
fi

cp ../../bin/linux/fortranParser .

docker build -t registry.gitlab.inria.fr/tapenade/tapenade/fortranparser:latest -t registry.gitlab.inria.fr/tapenade/tapenade/fortranparser:$(cat ../../build/resources/main/gittag) .

/bin/rm fortranParser

if [ "$(uname)" = "Linux" ]; then
  docker push registry.gitlab.inria.fr/tapenade/tapenade/fortranparser:latest
  docker push registry.gitlab.inria.fr/tapenade/tapenade/fortranparser:$(cat ../../build/resources/main/gittag)
else
  echo "docker push Time out on macos"
fi


