/* $Id$ */

#include <stdlib.h>
#include <ctype.h>
#include "f2iluserFunctions.h"

AtomValue f2ilUlcident(AtomValue oldVal) {
/*   char *lcvalue = (char *)strdup((char*)oldVal) ; */
  char *lcvalue = (char *)malloc(sizeof(char)*(strlen((char *)oldVal)+1)) ;
  sprintf(lcvalue,"%s\0",(char *)oldVal) ;
  int i;
  for (i=strlen(lcvalue)-1 ;  i>=0 ;  i--) lcvalue[i] = tolower(lcvalue[i]);
  return((AtomValue)lcvalue);
}

AtomValue f2ilUconcatU(AtomValue realVal, AtomValue extension) {
  char *result = (char *)malloc(sizeof(char)*
         (strlen((char *)realVal) + strlen((char *)extension) + 2));
  sprintf(result,"%s_%s\0",(char *)realVal, (char *)extension);
  return ((AtomValue)result);
}

AtomValue f2ilUblockdata(AtomValue oldVal) {
  char *newVal = (char *)malloc(sizeof(char)*
         (strlen((char *)oldVal) + 12));
  sprintf(newVal,"_blockdata_%s\0", (char *)oldVal);
  return((AtomValue)newVal);
}


Tree *f2ilTree(Tree*) ;
Tree *parseF(char*, char*) ;
char *getDirectory(char *fileName) ;
char *getFileName(char *fileName) ;

char* checkFileExisting(char *rootDirectory, char *includeFilePathAndName, int *fileFound) {
  FILE *inputFile ;
  char *pathTried = (char*)malloc((strlen(rootDirectory)+strlen(includeFilePathAndName)+1)*sizeof(char)) ;
  sprintf(pathTried, "%s%s\0", rootDirectory, includeFilePathAndName) ;
  inputFile = fopen(pathTried,"r") ;
  *fileFound = (inputFile!=NULL) ;
  if (*fileFound) fclose(inputFile) ;
  return pathTried ;
}

ListTree *f2ilUincludeDirective(AtomValue includeFilePathAndName) {
  extern char* includetype;
  extern char* includedirlist[];
  extern char* directory;
  char *absPath;
  Tree *parsedTree;
  ListTree *ilTree = NULL;
  int fileFound = 0 ;
  int i ;

  /* When servlet, disable local directory: */
  if (strcmp(includetype,"tapenade")!=0) includeFilePathAndName = getFileName((char*)includeFilePathAndName) ;

  /* In any case, first look in the present local directory: */
  absPath = checkFileExisting(directory, includeFilePathAndName, &fileFound) ;

  /* Then if file not found, look in the includeDirs: */
  i = 0 ;
  while (!fileFound && i<10 && includedirlist[i]!=NULL) {
    free(absPath) ;
    absPath = checkFileExisting(includedirlist[i], includeFilePathAndName, &fileFound) ;
    ++i ;
  }

  /* If everything else fails, set absolute path to present local directory: */
  if (!fileFound) {
    free(absPath) ;
    absPath = checkFileExisting(directory, includeFilePathAndName, &fileFound) ;
  }

  parsedTree = parseF(absPath, "IncludeFile");
  if (parsedTree != NULL) {
    char *oldDirectory = directory ;
    directory = getDirectory(absPath) ;
    Tree *ilIncludeTreeTop = f2ilTree(parsedTree) ;
    ilTree = ilIncludeTreeTop->contents.sons ;
    freeTreeNode(ilIncludeTreeTop) ;
    free(directory) ;
    directory = oldDirectory ;
    //freeTree(parsedTree) ; //For unknown reason, this loops and segfaults! missing initialization in parsedTree?
  }
  free(absPath) ;
  return ilTree;
}
