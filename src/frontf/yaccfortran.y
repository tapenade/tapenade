%{
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "stackTreeBuilder.h"
#include "cvtp.h"
%}
%token KW_END_OF_FILE
%token KW_NEEDOFFSET
%token KW_OPTIONS
%token KW_EOL
%token KW_CHECK
%token KW_EQUL
%token KW_NEEDKEYWORD
%token KW_PROGRAM
%token KW_BLOCKDATA
%token KW_MODULE
%token KW_EXTRINSIC
%token KW_POUV
%token KW_SUBROUTINE
%token KW_FUNCTION
%token KW_ETOI
%token KW_PFER
%token KW_RECURSIVE
%token KW_PURE
%token KW_ELEMENTAL
%token KW_RESULT
%token KW_CONTAINS
%token KW_INTERNAL
%token KW_RECLASSIFY
%token KW_ACCEPTLABELS
%token KW_END
%token KW_ENDPROGRAM
%token KW_ENDBLOCKDATA
%token KW_ENDSUBROUTINE
%token KW_ENDFUNCTION
%token KW_ENDMODULE
%token KW_ENDINTERNAL
%token KW_PARAMETER
%token KW_IMPLICIT
%token KW_NEEDIMPLICIT
%token KW_NONE
%token KW_UNDEFINED
%token KW_A
%token KW_MINU
%token KW_Z
%token KW_NEEDLETTER
%token KW_NOLETTER
%token KW_INIMPLICIT
%token KW_CHARACTER
%token KW_NEEDCOLON
%token KW_NEEDNOCOLON
%token KW_STATIC
%token KW_AUTOMATICDQUODQUOPOUV
%token KW_DIMENSION
%token KW_EXTERNAL
%token KW_INTRINSIC
%token KW_SAVE
%token KW_SLAH
%token KW_COMMONFLAG
%token KW_EQUIVALENCE
%token KW_COMMON
%token KW_opt_comma
%token KW_SLAHSLAH
%token KW_no_opt_comma
%token KW_INTENT
%token KW_IN
%token KW_OUT
%token KW_INOUT
%token KW_TYPE
%token KW_ENDTYPE
%token KW_VIRG
%token KW_SEQUENCE
%token KW_PRIVATE
%token KW_INTERFACE
%token KW_ABSTRACTINTERFACE
%token KW_INTERFACEOP
%token KW_INTERFACEASS
%token KW_MODULEPROCEDURE
%token KW_ENDINTERFACE
%token KW_ENDOPERATOR
%token KW_ENDASSIGNMENT
%token KW_USE
%token KW_NEEDOPKEYWORD
%token KW_DPIN
%token KW_ONLY
%token KW_PTRASS
%token KW_IMPORT
%token KW_ASSIGNMENT
%token KW_OPERATOR
%token KW_OP_USER
%token KW_PLUS
%token KW_ETOIETOI
%token KW_EQ
%token KW_NE
%token KW_GE
%token KW_LE
%token KW_GT
%token KW_LT
%token KW_EQV
%token KW_NEQV
%token KW_AND
%token KW_OR
%token KW_XOR
%token KW_NOT
%token KW_ALLOCATABLE
%token KW_OPTIONAL
%token KW_POINTER
%token KW_TARGET
%token KW_AUTOMATIC
%token KW_VOLATILE
%token KW_CONTIGUOUS
%token KW_STRUCTURE
%token KW_ENDSTRUCTURE
%token KW_UNION
%token KW_ENDUNION
%token KW_MAP
%token KW_ENDMAP
%token KW_RECORD
%token KW_DICTIONARY
%token KW_EXTENDEDBLOCK
%token KW_EXTENDEDBASE
%token KW_EXTENDEDDUMMY
%token KW_FORMAT
%token FORMATELEM
%token KW_ENTRY
%token DIRNAME
%token FLCDIRECTIVE
%token KW_NAMELIST
%token KW_DATA
%token KW_INCLUDE
%token KW_SHARP_INC
%token KW_IN_DECL
%token KW_ARRAY
%token KW_VALUE
%token KW_PASS
%token KW_NOPASS
%token KW_DEFERRED
%token KW_ABSTRACT
%token KW_PUBLIC
%token KW_BIND
%token KW_EXTENDS
%token KW_HPF_SPEC
%token KW_PROCESSORS
%token KW_ALIGN
%token KW_WITH
%token KW_DISTRIBUTEONTO
%token KW_ONTO
%token KW_DISTRIBUTE
%token KW_DYNAMIC
%token KW_INHERIT
%token KW_TEMPLATE
%token KW_NOSEQUENCE
%token KW_BLOCK
%token KW_CYCLIC
%token KW_DISTSTAR
%token KW_ALIGNWITH
%token KW_COMBDISTRIBUTEONTO
%token KW_LOGIF
%token KW_ASS
%token KW_ASSIGN
%token KW_TO
%token KW_CONTINUE
%token KW_CALL
%token KW_ARITHIF
%token KW_RETURN
%token KW_STOP
%token KW_PAUSE
%token KW_EXIT
%token KW_ENDDO
%token KW_CYCLE
%token KW_ALLOCATE
%token KW_DEALLOCATE
%token KW_NULLIFY
%token KW_WHERE
%token KW_FORALL
%token KW_INLINE
%token KW_ENDI
%token KW_LEAVE
%token KW_HPF_STAT
%token KW_GOTO
%token KW_ASSGOTO
%token KW_COMPGOTO
%token KW_INDEPENDENT
%token KW_NEW
%token KW_REALIGN
%token KW_REALIGNWITH
%token KW_REDISTRIBUTEONTO
%token KW_REDISTRIBUTE
%token KW_EMPTYREDISTRIBUTEONTO
%token KW_THEN
%token KW_ENDIF
%token KW_ELSE
%token KW_ELSEIF
%token KW_STRUCTWHERE
%token KW_ENDWHERE
%token KW_ELSEWHERE
%token KW_STRUCTFORALL
%token KW_ENDFORALL
%token KW_SELECTCASE
%token KW_ENDSELECT
%token KW_GOULDSELECTCASE
%token KW_SELECTTYPE
%token KW_CASE
%token KW_GOULDCASE
%token KW_TYPEIS
%token KW_CLASSIS
%token KW_CLASSDEFAULT
%token KW_DEFAULT
%token KW_DO
%token KW_DOWHILEON
%token KW_INTONLYON
%token KW_DOLABEL
%token KW_INTONLYOFF
%token KW_DOWHILEOFF
%token KW_ENDOFDO
%token KW_WHILE
%token KW_BEGIN
%token KW_UNTIL
%token KW_TIMES
%token KW_PARALLELDO
%token KW_EOLOMPENDPARALLELDO
%token KW_EOLOMPENDDO
%token KW_PARALLEL
%token KW_OMPENDPARALLEL
%token OMPDIRECTIVE
%token KW_NUM_THREADS
%token KW_SCHEDULE
%token KW_SHARED
%token KW_FIRSTPRIVATE
%token KW_LASTPRIVATE
%token KW_REDUCTION
%token KW_OPEN
%token KW_in_io
%token KW_CLOSE
%token KW_FLUSH
%token KW_INQUIRE
%token KW_BACKSPACELOG
%token KW_BACKSPACE
%token KW_ENDFILELOG
%token KW_ENDFILE
%token KW_REWINDLOG
%token KW_REWIND
%token KW_PRINT
%token KW_READ
%token KW_WRITE
%token KW_ACCEPT
%token KW_ENCODE
%token KW_DECODE
%token KW_DELETE
%token KW_FIND
%token KW_REWRITE
%token KW_UNLOCK
%token KW_BACKFILELOG
%token KW_BACKFILE
%token KW_SKIPFILELOG
%token KW_SKIPFILE
%token KW_DEFINEFILE
%token KW_notin_io
%token KW_QUOT
%token KW_LBRACKET
%token KW_NEEDTYPEKEYWORD
%token KW_RBRACKET
%token KW_PCEN
%token KW_POIN
%token OP_USER
%token KW_UNDERSCORE
%token KW_CLASS
%token KW_PROCEDURE
%token KW_INTEGER
%token KW_REAL
%token KW_DOUBLE
%token KW_COMPLEX
%token KW_DOUBLECOMPLEX
%token KW_LOGICAL
%token KW_BYTE
%token KW_BIT
%token KW_VOID
%token KW_size_comma
%token NAME
%token EQNAME
%token INTCST
%token BITCST
%token REALCST
%token DOUBLECST
%token KW_TRUE
%token KW_FALSE
%token LETTER
%token OFFSET
%token HOLLERITH
%token ASTRING
%token KW_NOG_FLOATING
%token KW_G_FLOATING
%token KW_NOI4
%token KW_I4
%token KW_NOF77
%token KW_F77
%token KW_NOCHECK
%token KW_NOEXTEND_SOURCE
%token KW_EXTEND_SOURCE
%token KW_ALL
%token KW_NOOVERFLOW
%token KW_OVERFLOW
%token KW_NOBOUNDS
%token KW_BOUNDS
%token KW_NOUNDERFLOW
%token KW_UNDERFLOW
%token KW_ACCEPTCOMMENTS
%token KW_ACCEPTCOMMENTSPOST
%token KW_ACCEPTCOMMENTSM0
%token KW_ACCEPTCOMMENTSM1
%token KW_ACCEPTCOMMENTSM2
%token KW_ACCEPTCOMMENTSXM2
%token EN_Unit
%token EN_OptHeader
%token EN_OptTrailer
%token EN_Decls
%token EN_Decl
%token EN_Stats
%token EN_Stat
%token EN_Exp
%token EN_Type
%token EN_IncludeFile
%{
#include "yaccbib.c"
%}
%start NT_axiom
%%
NT_axiom : NT_units  NT_acceptCommentsPost  
{stbPop(); 
}
   ;
NT_axiom : KW_END_OF_FILE  
{prTt0(293) ;}
   ;
NT_unit : NT_topoptions  NT_decl  NT_acceptCommentsAndEOL  NT_decls  NT_topStats  NT_acceptCommentsPost  NT_locals  NT_trailer  KW_NEEDOFFSET  NT_opt_offset  
{Tree *t9 = stbPop(); 
Tree *t8 = stbPop(); 
Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(2), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(mkPre(t2, t4),mkSons(t5,mkSons(t7,mkSons(t8,mkSons(t9,(ListTree*)NULL))))))))) ;}
   ;
NT_unit : NT_topoptions  NT_realStat  NT_acceptCommentsAndEOL  NT_stats  NT_acceptCommentsPost  NT_locals  NT_trailer  KW_NEEDOFFSET  NT_opt_offset  
{Tree *t8 = stbPop(); 
Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(2), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(mkTree(stbRkOper(297), (ListTree*)NULL),mkSons(mkPre(t2, t4),mkSons(t6,mkSons(t7,mkSons(t8,(ListTree*)NULL))))))))) ;}
   ;
NT_unit : NT_topoptions  NT_header  NT_acceptCommentsAndEOL  NT_decls  NT_topStats  NT_acceptCommentsPost  NT_locals  NT_trailer  KW_NEEDOFFSET  NT_opt_offset  
{Tree *t9 = stbPop(); 
Tree *t8 = stbPop(); 
Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(2), mkSons(t1,mkSons(t2,mkSons(t4,mkSons(t5,mkSons(t7,mkSons(t8,mkSons(t9,(ListTree*)NULL))))))))) ;}
   ;
NT_topoptions : KW_OPTIONS  NT_options  KW_EOL  

   ;
NT_topoptions : 
{prTt0(294) ;}
   ;
NT_option : NT_simpleOption  

   ;
NT_option : KW_CHECK  KW_EQUL  KW_NEEDKEYWORD  NT_checkOption  

   ;
NT_header : KW_PROGRAM  NT_opt_ident  
{prTt1(15) ;}
   ;
NT_header : KW_BLOCKDATA  NT_opt_ident  
{prTt1(16) ;}
   ;
NT_header : KW_MODULE  NT_ident  
{prTt1(17) ;}
   ;
NT_header : KW_EXTRINSIC  KW_POUV  NT_ident  NT_closeParenthRecla  NT_type_header  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(18), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_header : KW_SUBROUTINE  NT_ident  NT_paramlist  KW_NEEDKEYWORD  NT_opt_bind  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(19), mkSons(mkTree(stbRkOper(296), (ListTree*)NULL),mkSons(t1,mkSons(t2,mkSons(t3,(ListTree*)NULL)))))) ;}
   ;
NT_header : NT_prefixs  KW_SUBROUTINE  NT_ident  NT_paramlist  KW_NEEDKEYWORD  NT_opt_bind  
{prTt4(19) ;}
   ;
NT_header : KW_FUNCTION  NT_ident  NT_paramlist  KW_NEEDKEYWORD  NT_opt_result  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(20), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,mkSons(t2,mkSons(mkTree(stbRkOper(296), (ListTree*)NULL),mkSons(t3,(ListTree*)NULL))))))) ;}
   ;
NT_header : NT_prefixs  KW_FUNCTION  NT_ident  NT_paramlist  KW_NEEDKEYWORD  NT_opt_result  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(20), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t2,mkSons(t3,mkSons(t1,mkSons(t4,(ListTree*)NULL))))))) ;}
   ;
NT_header : NT_prefixs  NT_type  KW_FUNCTION  NT_ident  NT_opt_returnSize  NT_paramlist  KW_NEEDKEYWORD  NT_opt_result  
{Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
if ((treeOpCode(t2) == 276)) 
{
stbPush(mkTree(stbRkOper(20), mkSons(t2,mkSons(t3,mkSons(t5,mkSons(t1,mkSons(t6,(ListTree*)NULL))))))) ;} else {
stbPush(mkTree(stbRkOper(20), mkSons(mkTree(stbRkOper(276), mkSons(t2,mkSons(t4,(ListTree*)NULL))),mkSons(t3,mkSons(t5,mkSons(t1,mkSons(t6,(ListTree*)NULL))))))) ;}}
   ;
NT_header : NT_type  NT_prefixs  KW_FUNCTION  NT_ident  NT_opt_returnSize  NT_paramlist  KW_NEEDKEYWORD  NT_opt_result  
{Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
if ((treeOpCode(t1) == 276)) 
{
stbPush(mkTree(stbRkOper(20), mkSons(t1,mkSons(t3,mkSons(t5,mkSons(t2,mkSons(t6,(ListTree*)NULL))))))) ;} else {
stbPush(mkTree(stbRkOper(20), mkSons(mkTree(stbRkOper(276), mkSons(t1,mkSons(t4,(ListTree*)NULL))),mkSons(t3,mkSons(t5,mkSons(t2,mkSons(t6,(ListTree*)NULL))))))) ;}}
   ;
NT_header : NT_type  KW_FUNCTION  NT_ident  NT_opt_returnSize  NT_paramlist  KW_NEEDKEYWORD  NT_opt_result  
{Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
if ((treeOpCode(t1) == 276)) 
{
stbPush(mkTree(stbRkOper(20), mkSons(t1,mkSons(t2,mkSons(t4,mkSons(mkTree(stbRkOper(296), (ListTree*)NULL),mkSons(t5,(ListTree*)NULL))))))) ;} else {
stbPush(mkTree(stbRkOper(20), mkSons(mkTree(stbRkOper(276), mkSons(t1,mkSons(t3,(ListTree*)NULL))),mkSons(t2,mkSons(t4,mkSons(mkTree(stbRkOper(296), (ListTree*)NULL),mkSons(t5,(ListTree*)NULL))))))) ;}}
   ;
NT_type_header : KW_PROGRAM  NT_opt_ident  
{prTt1(15) ;}
   ;
NT_type_header : KW_BLOCKDATA  NT_opt_ident  
{prTt1(16) ;}
   ;
NT_type_header : KW_MODULE  NT_ident  
{prTt1(17) ;}
   ;
NT_returnSize : KW_ETOI  NT_intCst  

   ;
NT_paramlist : 
{prTt0(295) ;}
   ;
NT_paramlist : KW_POUV  KW_PFER  
{prTt0(295) ;}
   ;
NT_paramlist : KW_POUV  NT_params  KW_PFER  

   ;
NT_param : NT_ident  

   ;
NT_param : NT_star  

   ;
NT_prefix : KW_RECURSIVE  
{prTt0(21) ;}
   ;
NT_prefix : KW_PURE  
{prTt0(22) ;}
   ;
NT_prefix : KW_ELEMENTAL  
{prTt0(23) ;}
   ;
NT_opt_result : KW_RESULT  KW_POUV  NT_ident  KW_PFER  

   ;
NT_opt_result : NT_bind  

   ;
NT_opt_result : 
{prTt0(292) ;}
   ;
NT_opt_bind : NT_bind  

   ;
NT_opt_bind : 
{prTt0(318) ;}
   ;
NT_locals : KW_CONTAINS  KW_EOL  NT_localUnits1  

   ;
NT_locals : NT_localsB  

   ;
NT_locals : 
{prTt0(293) ;}
   ;
NT_localsB : KW_INTERNAL  KW_RECLASSIFY  NT_localUnit  
{prTt1(293) ;}
   ;
NT_localsB : NT_localsB  KW_INTERNAL  KW_RECLASSIFY  NT_localUnit  
{prTpost(293) ;}
   ;
NT_localUnit : NT_header  NT_acceptCommentsAndEOL  NT_decls  NT_topStats  NT_acceptCommentsPost  NT_locals  NT_trailer  
{Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(2), mkSons(mkTree(stbRkOper(294), (ListTree*)NULL),mkSons(t1,mkSons(t3,mkSons(t4,mkSons(t6,mkSons(t7,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL))))))))) ;}
   ;
NT_trailer : KW_ACCEPTLABELS  NT_pureTrailer  NT_acceptCommentsAndEOL  
{stbPop(); 
}
   ;
NT_pureTrailer : KW_END  
{prTt0(24) ;}
   ;
NT_pureTrailer : KW_ENDPROGRAM  NT_opt_ident  
{prTt1(25) ;}
   ;
NT_pureTrailer : KW_ENDBLOCKDATA  NT_opt_ident  
{prTt1(26) ;}
   ;
NT_pureTrailer : KW_ENDSUBROUTINE  NT_opt_ident  
{prTt1(27) ;}
   ;
NT_pureTrailer : KW_ENDFUNCTION  NT_opt_ident  
{prTt1(28) ;}
   ;
NT_pureTrailer : KW_ENDMODULE  NT_opt_ident  
{prTt1(29) ;}
   ;
NT_pureTrailer : KW_ENDINTERNAL  
{prTt0(24) ;}
   ;
NT_offset : NT_oneOffset  NT_oneOffset  
{prTt2(30) ;}
   ;
NT_decl : NT_parameterDecl  

   ;
NT_decl : NT_implicitDecl  

   ;
NT_decl : NT_dimensionDecl  

   ;
NT_decl : NT_externalDecl  

   ;
NT_decl : NT_intrinsicDecl  

   ;
NT_decl : NT_saveDecl  

   ;
NT_decl : NT_equivalenceDecl  

   ;
NT_decl : NT_commonDecl  

   ;
NT_decl : NT_intentDecl  

   ;
NT_decl : NT_derivedDecl  

   ;
NT_decl : NT_interfaceDecl  

   ;
NT_decl : NT_useDecl  

   ;
NT_decl : NT_importDecl  

   ;
NT_decl : NT_accessDecl  

   ;
NT_decl : NT_allocatableDecl  

   ;
NT_decl : NT_optionalDecl  

   ;
NT_decl : NT_pointerDecl  

   ;
NT_decl : NT_pointerPairDecl  

   ;
NT_decl : NT_targetDecl  

   ;
NT_decl : NT_automaticDecl  

   ;
NT_decl : NT_staticDecl  

   ;
NT_decl : NT_volatileDecl  

   ;
NT_decl : NT_contiguousDecl  

   ;
NT_decl : NT_structureDecl  

   ;
NT_decl : NT_recordDecl  

   ;
NT_decl : NT_dictionaryDecl  

   ;
NT_decl : NT_extBlockDecl  

   ;
NT_decl : NT_extBaseDecl  

   ;
NT_decl : NT_extDummyDecl  

   ;
NT_decl : NT_inst  

   ;
NT_inst : NT_formatDecl  

   ;
NT_inst : NT_entryDecl  

   ;
NT_inst : NT_directiveDecl  

   ;
NT_inst : NT_flcdirectiveDecl  

   ;
NT_inst : NT_namelistDecl  

   ;
NT_inst : NT_dataDecl  

   ;
NT_inst : NT_includeDecl  

   ;
NT_inst : NT_varDecl  

   ;
NT_inst : NT_hpfDecl  

   ;
NT_parameterDecl : KW_PARAMETER  KW_POUV  NT_parameters  KW_PFER  

   ;
NT_parameter : NT_ident  KW_EQUL  NT_exp  
{prTt2(32) ;}
   ;
NT_implicitDecl : KW_IMPLICIT  KW_NEEDIMPLICIT  NT_in_implicit  

   ;
NT_in_implicit : KW_NONE  
{prTt0(33) ;}
   ;
NT_in_implicit : KW_UNDEFINED  KW_POUV  KW_NEEDKEYWORD  KW_A  KW_MINU  KW_NEEDKEYWORD  KW_Z  KW_PFER  
{prTt0(33) ;}
   ;
NT_in_implicit : NT_implicits  

   ;
NT_implicit : NT_type_non77like  KW_POUV  KW_NEEDLETTER  NT_letterss  KW_NOLETTER  KW_PFER  
{prTt2(34) ;}
   ;
NT_implicit : NT_non_char_type  KW_INIMPLICIT  KW_POUV  NT_generalExp  KW_PFER  KW_POUV  KW_NEEDLETTER  NT_letterss  KW_NOLETTER  KW_PFER  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(34), mkSons(mkTree(stbRkOper(276), mkSons(t1,mkSons(t2,(ListTree*)NULL))),mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_implicit : KW_CHARACTER  KW_INIMPLICIT  KW_POUV  KW_NEEDCOLON  NT_generalExps  KW_NEEDNOCOLON  KW_PFER  KW_POUV  KW_NEEDLETTER  NT_letterss  KW_NOLETTER  KW_PFER  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(34), mkSons(mkTree(stbRkOper(276), mkSons(mkTree(stbRkOper(281), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL))),mkSons(t2,(ListTree*)NULL)))) ;}
   ;
NT_implicit : KW_STATIC  KW_POUV  KW_NEEDLETTER  NT_letterss  KW_NOLETTER  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(34), mkSons(mkTree(stbRkOper(35), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_implicit : KW_AUTOMATICDQUODQUOPOUV  KW_NEEDLETTER  NT_letterss  KW_NOLETTER  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(34), mkSons(mkTree(stbRkOper(36), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_letters : NT_letter  

   ;
NT_letters : NT_letter  KW_MINU  NT_letter  
{prTt2(37) ;}
   ;
NT_dimensionDecl : KW_DIMENSION  NT_opt_dblcolon  NT_fullDecls  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(39), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_externalDecl : KW_EXTERNAL  NT_opt_dblcolon  NT_idents  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(47), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_intrinsicDecl : KW_INTRINSIC  NT_opt_dblcolon  NT_idents  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(48), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_saveDecl : KW_SAVE  
{stbPush(mkTree(stbRkOper(49), mkSons(mkTree(stbRkOper(50), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_saveDecl : KW_SAVE  NT_opt_dblcolon  NT_blockNames  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(49), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_blockName : NT_ident  

   ;
NT_blockName : KW_SLAH  KW_COMMONFLAG  NT_ident  KW_SLAH  
{Tree *t1 = stbPop(); 
AtomValue metaIDENT ;
if ((treeOpCode(t1) == 244) && (metaIDENT=treeAtomValue(t1))) 
{
stbPush(mkAtom(stbRkOper(245), metaIDENT)) ;}}
   ;
NT_equivalenceDecl : KW_EQUIVALENCE  NT_equivalences  

   ;
NT_equivalence : KW_POUV  NT_variableRefs  KW_PFER  

   ;
NT_commonDecl : KW_COMMON  KW_opt_comma  NT_commons1  NT_no_opt_comma  
{stbPop(); 
}
   ;
NT_common : KW_SLAHSLAH  NT_basicDecls  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(53), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_common : KW_SLAH  KW_COMMONFLAG  NT_ident  KW_SLAH  NT_basicDecls  
{prTt2(53) ;}
   ;
NT_common : NT_basicDecls  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(53), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_no_opt_comma : KW_no_opt_comma  
{prTt0(292) ;}
   ;
NT_intentDecl : KW_INTENT  KW_POUV  KW_NEEDKEYWORD  NT_intentValue  KW_PFER  NT_opt_dblcolon  NT_idents  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(54), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_intentValue : KW_IN  
{prTt0(55) ;}
   ;
NT_intentValue : KW_OUT  
{prTt0(56) ;}
   ;
NT_intentValue : KW_INOUT  
{prTt0(57) ;}
   ;
NT_derivedDecl : KW_TYPE  NT_derived_access  NT_ident  NT_acceptCommentsM1  KW_EOL  NT_privateSequences  NT_components  KW_ENDTYPE  NT_opt_ident  
{Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(58), mkSons(t2,mkSons(t1,mkSons(t4,mkSons(t5,mkSons(mkTree(stbRkOper(297), (ListTree*)NULL),mkSons(t6,(ListTree*)NULL)))))))) ;}
   ;
NT_derivedDecl : KW_TYPE  NT_derived_access  NT_ident  NT_acceptCommentsM1  KW_EOL  NT_privateSequences  NT_components  KW_CONTAINS  KW_EOL  NT_boundProcs1  KW_ENDTYPE  NT_opt_ident  
{Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(58), mkSons(t2,mkSons(t1,mkSons(t4,mkSons(t5,mkSons(t6,mkSons(t7,(ListTree*)NULL)))))))) ;}
   ;
NT_derived_access : KW_VIRG  KW_NEEDKEYWORD  NT_access  NT_dblcolon  
{stbPop(); 
}
   ;
NT_derived_access : NT_opt_dblcolon  
{stbPop(); 
prTt0(292) ;}
   ;
NT_privateSequence : KW_SEQUENCE  KW_EOL  
{prTt0(62) ;}
   ;
NT_privateSequence : KW_PRIVATE  KW_EOL  
{prTt0(60) ;}
   ;
NT_component : NT_varDecl  KW_EOL  

   ;
NT_boundProcs1 : NT_component  
{prTt1(297) ;}
   ;
NT_boundProcs1 : NT_boundProcs1  NT_component  
{prTpost(297) ;}
   ;
NT_interfaceDecl : NT_interface_header  NT_acceptCommentsAndEOL  NT_interface_body  NT_interface_end  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(66), mkSons(t1,mkSons(t3,mkSons(t4,(ListTree*)NULL))))) ;}
   ;
NT_interface_header : KW_INTERFACE  
{prTt0(292) ;}
   ;
NT_interface_header : KW_ABSTRACTINTERFACE  
{prTt0(291) ;}
   ;
NT_interface_header : KW_INTERFACE  NT_ident  

   ;
NT_interface_header : KW_INTERFACEOP  KW_POUV  NT_op_spec  KW_PFER  

   ;
NT_interface_header : KW_INTERFACEASS  KW_POUV  KW_EQUL  KW_PFER  
{prTdf2(165) ;}
   ;
NT_interface_body : 
{prTt0(306) ;}
   ;
NT_interface_body : NT_header  NT_acceptCommentsAndEOL  NT_decls  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(306), mkSons(mkTree(stbRkOper(67), mkSons(t1,mkSons(t3,mkSons(mkTree(stbRkOper(24), (ListTree*)NULL),(ListTree*)NULL)))),(ListTree*)NULL))) ;}
   ;
NT_interface_body : NT_interfaces1  

   ;
NT_interface : KW_MODULEPROCEDURE  NT_idents  
{prTt1(68) ;}
   ;
NT_interface : NT_directiveDecl  

   ;
NT_interface : NT_header  NT_acceptCommentsAndEOL  NT_decls  NT_pureTrailer  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(67), mkSons(t1,mkSons(t3,mkSons(t4,(ListTree*)NULL))))) ;}
   ;
NT_interface_end : KW_ENDINTERFACE  
{prTt0(292) ;}
   ;
NT_interface_end : KW_ENDINTERFACE  NT_ident  

   ;
NT_interface_end : KW_ENDOPERATOR  KW_POUV  NT_op_spec  KW_PFER  

   ;
NT_interface_end : KW_ENDASSIGNMENT  KW_POUV  KW_EQUL  KW_PFER  
{prTdf2(165) ;}
   ;
NT_useDecl : KW_USE  NT_opt_intrinsic  NT_ident  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(69), mkSons(t2,mkSons(mkTree(stbRkOper(72), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_useDecl : KW_USE  NT_opt_intrinsic  NT_ident  KW_VIRG  KW_NEEDOPKEYWORD  NT_useList  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(69), mkSons(t2,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_opt_intrinsic : 
{prTt0(292) ;}
   ;
NT_opt_intrinsic : KW_VIRG  KW_NEEDKEYWORD  KW_INTRINSIC  KW_DPIN  KW_DPIN  
{prTt0(292) ;}
   ;
NT_useList : KW_ONLY  KW_DPIN  KW_NEEDOPKEYWORD  NT_onlyVisibles  

   ;
NT_useList : NT_renamedVisibles  

   ;
NT_visible : NT_genericName  

   ;
NT_visible : NT_renamed  

   ;
NT_renamed : NT_genericName  KW_PTRASS  KW_NEEDOPKEYWORD  NT_genericName  
{prTt2(73) ;}
   ;
NT_importDecl : KW_IMPORT  NT_opt_dblcolon  NT_idents  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(70), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_accessDecl : NT_access  NT_opt_dblcolon  KW_NEEDOPKEYWORD  NT_genericNames  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(74), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_genericNames : 
{prTt0(50) ;}
   ;
NT_genericNames : NT_genericNames1  

   ;
NT_genericName : NT_blockName  

   ;
NT_genericName : KW_ASSIGNMENT  KW_POUV  KW_EQUL  KW_PFER  
{prTdf2(165) ;}
   ;
NT_genericName : KW_OPERATOR  KW_POUV  NT_op_spec  KW_PFER  

   ;
NT_op_spec : KW_OP_USER  NT_definedOp  

   ;
NT_op_spec : KW_PLUS  
{prTdf2(247) ;}
   ;
NT_op_spec : KW_MINU  
{prTdf2(248) ;}
   ;
NT_op_spec : KW_ETOI  
{prTdf2(249) ;}
   ;
NT_op_spec : KW_SLAH  
{prTdf2(250) ;}
   ;
NT_op_spec : KW_ETOIETOI  
{prTdf2(251) ;}
   ;
NT_op_spec : KW_EQ  
{prTdf2(261) ;}
   ;
NT_op_spec : KW_NE  
{prTdf2(266) ;}
   ;
NT_op_spec : KW_GE  
{prTdf2(262) ;}
   ;
NT_op_spec : KW_LE  
{prTdf2(264) ;}
   ;
NT_op_spec : KW_GT  
{prTdf2(263) ;}
   ;
NT_op_spec : KW_LT  
{prTdf2(265) ;}
   ;
NT_op_spec : KW_EQV  
{prTdf2(259) ;}
   ;
NT_op_spec : KW_NEQV  
{prTdf2(260) ;}
   ;
NT_op_spec : KW_AND  
{prTdf2(256) ;}
   ;
NT_op_spec : KW_OR  
{prTdf2(257) ;}
   ;
NT_op_spec : KW_XOR  
{prTdf2(258) ;}
   ;
NT_op_spec : KW_NOT  
{prTdf1(255) ;}
   ;
NT_op_spec : KW_SLAHSLAH  
{prTdf2(254) ;}
   ;
NT_allocatableDecl : KW_ALLOCATABLE  NT_opt_dblcolon  NT_basicDecls  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(77), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_optionalDecl : KW_OPTIONAL  NT_opt_dblcolon  NT_idents  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(78), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_pointerDecl : KW_POINTER  NT_opt_dblcolon  NT_basicDecls  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(79), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_pointerPairDecl : KW_POINTER  NT_pointerPairs  

   ;
NT_pointerPair : KW_POUV  NT_ident  KW_VIRG  NT_basicDecl  KW_PFER  
{prTt2(81) ;}
   ;
NT_targetDecl : KW_TARGET  NT_opt_dblcolon  NT_basicDecls  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(82), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_automaticDecl : KW_AUTOMATIC  NT_idents  
{prTt1(83) ;}
   ;
NT_staticDecl : KW_STATIC  NT_idents  
{prTt1(84) ;}
   ;
NT_volatileDecl : KW_VOLATILE  NT_opt_dblcolon  NT_blockNames  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(85), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_contiguousDecl : KW_CONTIGUOUS  NT_opt_dblcolon  NT_idents  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(86), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_structureDecl : KW_STRUCTURE  KW_SLAH  KW_COMMONFLAG  NT_ident  KW_SLAH  NT_acceptCommentsM0  KW_EOL  NT_fieldDecls  KW_ENDSTRUCTURE  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(87), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_fieldDecls : 
{prTt0(304) ;}
   ;
NT_fieldDecls : NT_fieldDecls1  

   ;
NT_fieldDecl : NT_recordDecl  

   ;
NT_fieldDecl : NT_varDecl  

   ;
NT_fieldDecl : NT_parameterDecl  

   ;
NT_fieldDecl : KW_STRUCTURE  KW_SLAH  KW_COMMONFLAG  NT_ident  KW_SLAH  NT_basicDecls  NT_acceptCommentsM1  KW_EOL  NT_fieldDecls1  NT_acceptCommentsPost  KW_ENDSTRUCTURE  
{Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(63), mkSons(t1,mkSons(t2,mkSons(t4,(ListTree*)NULL))))) ;}
   ;
NT_fieldDecl : KW_STRUCTURE  NT_basicDecls  NT_acceptCommentsM0  KW_EOL  NT_fieldDecls1  NT_acceptCommentsPost  KW_ENDSTRUCTURE  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(63), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,mkSons(t3,(ListTree*)NULL))))) ;}
   ;
NT_fieldDecl : KW_UNION  KW_EOL  NT_mapDecls1  KW_ENDUNION  
{prTt1(64) ;}
   ;
NT_mapDecl : KW_MAP  KW_EOL  NT_fieldDecls1  KW_ENDMAP  
{prTt1(65) ;}
   ;
NT_recordDecl : KW_RECORD  KW_opt_comma  NT_records1  NT_no_opt_comma  
{stbPop(); 
}
   ;
NT_record : KW_SLAH  KW_COMMONFLAG  NT_ident  KW_SLAH  NT_basicDecls  
{prTt2(89) ;}
   ;
NT_dictionaryDecl : KW_DICTIONARY  NT_gstring  KW_EOL  
{prTt1(90) ;}
   ;
NT_extBlockDecl : KW_EXTENDEDBLOCK  KW_opt_comma  NT_commons1  NT_no_opt_comma  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
ListTree *metaX ;
if ((treeOpCode(t1) == 52) && (metaX=treeSons(t1))) 
{
stbPush(mkTree(stbRkOper(91), metaX)) ;}}
   ;
NT_extBaseDecl : KW_EXTENDEDBASE  KW_opt_comma  NT_extBases1  NT_no_opt_comma  
{stbPop(); 
}
   ;
NT_extBase : KW_SLAH  KW_COMMONFLAG  NT_ident  KW_SLAH  NT_ext_size  
{prTt2(93) ;}
   ;
NT_ext_size : NT_intCst  

   ;
NT_ext_size : NT_star  

   ;
NT_extDummyDecl : KW_EXTENDEDDUMMY  NT_idents  
{prTt1(94) ;}
   ;
NT_formatDecl : KW_ACCEPTLABELS  KW_FORMAT  NT_formatElems  
{prTt1(95) ;}
   ;
NT_formatElem : FORMATELEM  
{stbPush(mkAtom(stbRkOper(96), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_formatElem : NT_gstring  

   ;
NT_entryDecl : KW_ENTRY  NT_ident  NT_paramlist  
{prTt2(97) ;}
   ;
NT_directiveDecl : NT_directive_name  NT_directive_arguments  
{prTt2(98) ;}
   ;
NT_directive_name : DIRNAME  
{stbPush(mkAtom(stbRkOper(244), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_flcdirectiveDecl : NT_flcdirname  NT_directive_arguments  
{prTt2(99) ;}
   ;
NT_flcdirname : FLCDIRECTIVE  
{stbPush(mkAtom(stbRkOper(244), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_directive_arguments : 
{prTt0(308) ;}
   ;
NT_directive_arguments : NT_dirArgs  

   ;
NT_namelistDecl : KW_NAMELIST  KW_opt_comma  NT_namelists1  NT_no_opt_comma  
{stbPop(); 
}
   ;
NT_namelist : KW_SLAH  NT_ident  KW_SLAH  NT_idents  
{prTt2(115) ;}
   ;
NT_dataDecl : KW_DATA  NT_datas  

   ;
NT_datas : NT_data  
{prTt1(116) ;}
   ;
NT_datas : NT_datas  KW_VIRG  NT_data  
{prTpost(116) ;}
   ;
NT_datas : NT_datas  NT_data  
{prTpost(116) ;}
   ;
NT_data : NT_datavars  KW_SLAH  NT_values  KW_SLAH  
{prTt2(117) ;}
   ;
NT_datavar : NT_variableRef  

   ;
NT_datavar : KW_POUV  NT_datavars  KW_VIRG  NT_doRange  KW_PFER  
{prTt2(118) ;}
   ;
NT_value : NT_simple_exp  KW_ETOI  NT_simple_value  
{prTt2(119) ;}
   ;
NT_value : NT_simple_value  

   ;
NT_simple_value : NT_variableRef  

   ;
NT_simple_value : NT_simpleCst  

   ;
NT_simple_value : NT_complexConstructor  

   ;
NT_simple_value : KW_PLUS  NT_simple_exp  
{prTt1(252) ;}
   ;
NT_simple_value : KW_MINU  NT_simple_exp  
{prTt1(253) ;}
   ;
NT_includeDecl : KW_INCLUDE  NT_gstring  
{prTt1(120) ;}
   ;
NT_includeDecl : KW_SHARP_INC  NT_gstring  
{prTt1(120) ;}
   ;
NT_varDecl : NT_type  NT_fullDecls  
{prTt2(122) ;}
   ;
NT_varDecl : NT_type  NT_dblcolon  NT_fullDecls  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(122), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_varDecl : NT_type  KW_VIRG  NT_typeSpecModifiers  NT_dblcolon  NT_fullDecls  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(122), mkSons(mkTree(stbRkOper(277), mkSons(t1,mkSons(t2,(ListTree*)NULL))),mkSons(t4,(ListTree*)NULL)))) ;}
   ;
NT_fullDecl : NT_basicDecl  

   ;
NT_fullDecl : NT_basicDecl  NT_initializer  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
Tree *metaVAR ;
if ((treeOpCode(t1) == 40) && (metaVAR=treeDown(t1, 1))) 
{
stbPush(mkTree(stbRkOper(40), mkSons(metaVAR,mkSons(t2,(ListTree*)NULL)))) ;}}
   ;
NT_basicDecl : NT_ident_in_decl  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(40), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_basicDecl : NT_ident_in_decl  NT_typesize  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(40), mkSons(mkTree(stbRkOper(44), mkSons(t1,mkSons(t2,(ListTree*)NULL))),mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_basicDecl : NT_ident_in_decl  KW_POUV  NT_dims  KW_PFER  KW_NEEDNOCOLON  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(40), mkSons(mkTree(stbRkOper(43), mkSons(t1,mkSons(t2,(ListTree*)NULL))),mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_basicDecl : NT_ident_in_decl  NT_typesize  KW_POUV  NT_dims  KW_PFER  KW_NEEDNOCOLON  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(40), mkSons(mkTree(stbRkOper(44), mkSons(mkTree(stbRkOper(43), mkSons(t1,mkSons(t3,(ListTree*)NULL))),mkSons(t2,(ListTree*)NULL))),mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_basicDecl : NT_ident_in_decl  KW_POUV  NT_dims  KW_PFER  NT_typesize  KW_NEEDNOCOLON  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(40), mkSons(mkTree(stbRkOper(44), mkSons(mkTree(stbRkOper(43), mkSons(t1,mkSons(t2,(ListTree*)NULL))),mkSons(t3,(ListTree*)NULL))),mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_ident_in_decl : NT_ident  KW_IN_DECL  

   ;
NT_dim : KW_DPIN  
{prTdf2(45) ;}
   ;
NT_dim : NT_uexpOrStar  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(45), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_dim : NT_uexp  KW_DPIN  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(45), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_dim : NT_uexp  KW_DPIN  NT_uexpOrStar  
{prTt2(45) ;}
   ;
NT_uexpOrStar : NT_star  

   ;
NT_uexpOrStar : NT_uexp  

   ;
NT_initializer : KW_SLAH  NT_values  KW_SLAH  

   ;
NT_initializer : KW_EQUL  NT_exp  
{prTt1(41) ;}
   ;
NT_initializer : KW_PTRASS  NT_exp  
{prTt1(42) ;}
   ;
NT_typeSpecModifier : KW_ARRAY  KW_POUV  NT_dims  KW_PFER  
{prTt1(123) ;}
   ;
NT_typeSpecModifier : KW_PARAMETER  
{prTt0(124) ;}
   ;
NT_typeSpecModifier : KW_SAVE  
{prTt0(125) ;}
   ;
NT_typeSpecModifier : KW_DATA  
{prTt0(126) ;}
   ;
NT_typeSpecModifier : KW_INTENT  KW_POUV  KW_NEEDKEYWORD  NT_intentValue  KW_PFER  
{prTt1(127) ;}
   ;
NT_typeSpecModifier : KW_ALLOCATABLE  
{prTt0(128) ;}
   ;
NT_typeSpecModifier : KW_DIMENSION  KW_POUV  NT_dims  KW_PFER  
{prTt1(123) ;}
   ;
NT_typeSpecModifier : KW_EXTERNAL  
{prTt0(129) ;}
   ;
NT_typeSpecModifier : KW_INTRINSIC  
{prTt0(130) ;}
   ;
NT_typeSpecModifier : KW_OPTIONAL  
{prTt0(131) ;}
   ;
NT_typeSpecModifier : KW_POINTER  
{prTt0(132) ;}
   ;
NT_typeSpecModifier : KW_TARGET  
{prTt0(133) ;}
   ;
NT_typeSpecModifier : KW_VALUE  
{prTt0(134) ;}
   ;
NT_typeSpecModifier : NT_access  

   ;
NT_typeSpecModifier : KW_VOLATILE  
{prTt0(135) ;}
   ;
NT_typeSpecModifier : KW_CONTIGUOUS  
{prTt0(136) ;}
   ;
NT_typeSpecModifier : KW_PASS  
{prTt0(137) ;}
   ;
NT_typeSpecModifier : KW_NOPASS  
{prTt0(138) ;}
   ;
NT_typeSpecModifier : KW_DEFERRED  
{prTt0(139) ;}
   ;
NT_access : KW_ABSTRACT  
{prTt0(291) ;}
   ;
NT_access : KW_PRIVATE  
{prTt0(60) ;}
   ;
NT_access : KW_PUBLIC  
{prTt0(59) ;}
   ;
NT_access : NT_bind  

   ;
NT_access : NT_extends  

   ;
NT_bind : KW_BIND  KW_POUV  NT_ident  KW_VIRG  NT_keywordArg  KW_PFER  
{prTt2(75) ;}
   ;
NT_bind : KW_BIND  KW_POUV  NT_ident  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(75), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_extends : KW_EXTENDS  KW_POUV  NT_ident  KW_VIRG  NT_keywordArg  KW_PFER  
{prTt2(76) ;}
   ;
NT_extends : KW_EXTENDS  KW_POUV  NT_ident  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(76), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_hpfDecl : KW_HPF_SPEC  KW_RECLASSIFY  NT_hpfDirective  

   ;
NT_hpfDirective : KW_PROCESSORS  NT_basicDecls  
{prTt1(141) ;}
   ;
NT_hpfDirective : KW_ALIGN  NT_ident  KW_POUV  NT_dims  NT_closeParenthRecla  KW_WITH  NT_withClause  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(142), mkSons(t1,mkSons(t2,mkSons(t4,(ListTree*)NULL))))) ;}
   ;
NT_hpfDirective : KW_DISTRIBUTEONTO  NT_ident  NT_formatClause  KW_RECLASSIFY  KW_ONTO  NT_ontoClause  
{prTt3(143) ;}
   ;
NT_hpfDirective : KW_DISTRIBUTE  NT_ident  NT_formatClause  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(143), mkSons(t1,mkSons(t2,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL))))) ;}
   ;
NT_hpfDirective : KW_DYNAMIC  NT_idents  
{prTt1(144) ;}
   ;
NT_hpfDirective : KW_INHERIT  NT_idents  
{prTt1(145) ;}
   ;
NT_hpfDirective : KW_TEMPLATE  NT_basicDecls  
{prTt1(146) ;}
   ;
NT_hpfDirective : NT_hpfAttributes  NT_dblcolon  NT_fullDecls  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(147), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_hpfDirective : KW_SEQUENCE  
{prTdf1(148) ;}
   ;
NT_hpfDirective : KW_SEQUENCE  NT_opt_dblcolon  NT_blockNames  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(148), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_hpfDirective : KW_NOSEQUENCE  
{prTdf1(149) ;}
   ;
NT_hpfDirective : KW_NOSEQUENCE  NT_opt_dblcolon  NT_blockNames  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(149), mkSons(t2,(ListTree*)NULL))) ;}
   ;
NT_withClause : NT_variableRef  
{prTt1(150) ;}
   ;
NT_withClause : KW_ETOI  NT_variableRef  
{prTt1(151) ;}
   ;
NT_formatClause : KW_POUV  NT_distFormats  KW_PFER  
{prTt1(152) ;}
   ;
NT_formatClause : KW_ETOI  
{prTdf1(153) ;}
   ;
NT_formatClause : KW_ETOI  KW_POUV  NT_distFormats  KW_PFER  
{prTt1(153) ;}
   ;
NT_ontoClause : NT_ident  
{prTt1(150) ;}
   ;
NT_ontoClause : KW_ETOI  NT_opt_ident  
{prTt1(151) ;}
   ;
NT_distFormat : KW_BLOCK  
{prTdf1(154) ;}
   ;
NT_distFormat : KW_BLOCK  KW_POUV  NT_exp  KW_PFER  
{prTt1(154) ;}
   ;
NT_distFormat : KW_CYCLIC  
{prTdf1(155) ;}
   ;
NT_distFormat : KW_CYCLIC  KW_POUV  NT_exp  KW_PFER  
{prTt1(155) ;}
   ;
NT_distFormat : KW_DISTSTAR  
{prTt0(156) ;}
   ;
NT_hpfAttribute : KW_PROCESSORS  
{prTt0(157) ;}
   ;
NT_hpfAttribute : KW_ALIGNWITH  NT_withClause  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(158), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_hpfAttribute : KW_ALIGN  KW_POUV  NT_closeParenthRecla  KW_WITH  NT_withClause  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(158), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t2,(ListTree*)NULL)))) ;}
   ;
NT_hpfAttribute : KW_ALIGN  KW_POUV  NT_dims  NT_closeParenthRecla  KW_WITH  NT_withClause  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(158), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_hpfAttribute : KW_COMBDISTRIBUTEONTO  NT_ontoClause  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(159), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_hpfAttribute : KW_DISTRIBUTEONTO  NT_formatClause  KW_RECLASSIFY  KW_ONTO  NT_ontoClause  
{prTt2(159) ;}
   ;
NT_hpfAttribute : KW_DISTRIBUTE  NT_formatClause  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(159), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_hpfAttribute : KW_DYNAMIC  
{prTt0(160) ;}
   ;
NT_hpfAttribute : KW_INHERIT  
{prTt0(161) ;}
   ;
NT_hpfAttribute : KW_TEMPLATE  
{prTt0(162) ;}
   ;
NT_hpfAttribute : KW_DIMENSION  KW_POUV  NT_dims  KW_PFER  
{prTt1(163) ;}
   ;
NT_topStats : 
{prTt0(315) ;}
   ;
NT_topStats : NT_realStat  NT_acceptCommentsAndEOL  NT_stats  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkPre(t1, t3)) ;}
   ;
NT_stat : NT_inst  

   ;
NT_stat : NT_realStat  

   ;
NT_realStat : KW_ACCEPTLABELS  NT_pureStat  

   ;
NT_pureStat : NT_opt_ident  KW_LOGIF  KW_POUV  NT_exp  NT_closeParenthRecla  NT_simpleStat  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(187), mkSons(t2,mkSons(t4,(ListTree*)NULL)))) ;}
   ;
NT_pureStat : NT_simpleStat  

   ;
NT_pureStat : NT_structStat  

   ;
NT_simpleStat : KW_ASS  NT_variableRef  KW_EQUL  NT_assign_right  
{prTt2(165) ;}
   ;
NT_simpleStat : KW_ASSIGN  NT_label  KW_NEEDKEYWORD  KW_TO  NT_ident  
{prTt2(167) ;}
   ;
NT_simpleStat : KW_CONTINUE  
{prTt0(168) ;}
   ;
NT_simpleStat : KW_CALL  NT_variableRef  
{Tree *t1 = stbPop(); 
Tree *metaEXPS, *metaVARREF ;
if ((treeOpCode(t1) == 243) && (metaVARREF=treeDown(t1, 1)) && (metaEXPS=treeDown(t1, 2))) 
{
stbPush(mkTree(stbRkOper(169), mkSons(metaVARREF,mkSons(metaEXPS,(ListTree*)NULL)))) ;} else {
stbPush(mkTree(stbRkOper(169), mkSons(t1,mkSons(mkTree(stbRkOper(318), (ListTree*)NULL),(ListTree*)NULL)))) ;}}
   ;
NT_simpleStat : NT_goto  

   ;
NT_simpleStat : NT_io_stat  

   ;
NT_simpleStat : KW_ARITHIF  KW_POUV  NT_exp  KW_PFER  NT_opt_label  KW_VIRG  NT_opt_label  KW_VIRG  NT_opt_label  
{prTt4(170) ;}
   ;
NT_simpleStat : KW_RETURN  NT_opt_exp  
{prTt1(171) ;}
   ;
NT_simpleStat : KW_STOP  NT_opt_exp  
{prTt1(172) ;}
   ;
NT_simpleStat : KW_PAUSE  NT_opt_exp  
{prTt1(173) ;}
   ;
NT_simpleStat : KW_EXIT  NT_opt_ident  
{prTt1(174) ;}
   ;
NT_simpleStat : KW_ENDDO  NT_opt_ident  
{prTt1(175) ;}
   ;
NT_simpleStat : KW_CYCLE  NT_opt_ident  
{prTt1(176) ;}
   ;
NT_simpleStat : KW_ALLOCATE  KW_POUV  NT_variableRefs  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(177), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_simpleStat : KW_ALLOCATE  KW_POUV  NT_variableRefs  KW_VIRG  NT_keywordArg  KW_PFER  
{prTt2(177) ;}
   ;
NT_simpleStat : KW_DEALLOCATE  KW_POUV  NT_variableRefs  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(178), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_simpleStat : KW_DEALLOCATE  KW_POUV  NT_variableRefs  KW_VIRG  NT_keywordArg  KW_PFER  
{prTt2(178) ;}
   ;
NT_simpleStat : KW_NULLIFY  KW_POUV  NT_variableRefs  KW_PFER  
{prTt1(179) ;}
   ;
NT_simpleStat : KW_ASS  NT_variableRef  KW_PTRASS  NT_exp  
{prTt2(166) ;}
   ;
NT_simpleStat : KW_WHERE  KW_POUV  NT_exp  NT_closeParenthRecla  NT_simpleStat  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(180), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_simpleStat : KW_FORALL  KW_POUV  NT_forallRanges  NT_opt_mask  NT_closeParenthRecla  NT_simpleStat  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(181), mkSons(t1,mkSons(t2,mkSons(t4,(ListTree*)NULL))))) ;}
   ;
NT_simpleStat : KW_INLINE  KW_EOL  KW_ENDI  
{prTt0(182) ;}
   ;
NT_simpleStat : KW_LEAVE  NT_opt_label  
{prTt1(183) ;}
   ;
NT_simpleStat : KW_HPF_STAT  KW_RECLASSIFY  NT_hpfStat  

   ;
NT_assign_right : NT_variableRef  KW_EQUL  NT_assign_right  
{prTt2(165) ;}
   ;
NT_assign_right : NT_exp  

   ;
NT_goto : KW_GOTO  NT_label  
{prTt1(184) ;}
   ;
NT_goto : KW_ASSGOTO  NT_ident  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(185), mkSons(t1,mkSons(mkTree(stbRkOper(321), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_goto : KW_ASSGOTO  NT_ident  NT_opt_comma  KW_POUV  NT_labels  KW_PFER  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(185), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_goto : KW_COMPGOTO  KW_POUV  NT_labels  KW_PFER  NT_opt_comma  NT_exp  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(186), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_forallRange : NT_ident  KW_EQUL  NT_rangeColon  
{prTt2(220) ;}
   ;
NT_opt_mask : 
{prTt0(292) ;}
   ;
NT_opt_mask : KW_VIRG  NT_exp  

   ;
NT_hpfStat : KW_INDEPENDENT  
{prTdf1(188) ;}
   ;
NT_hpfStat : KW_INDEPENDENT  KW_VIRG  KW_RECLASSIFY  KW_NEW  KW_POUV  NT_variableRefs  KW_PFER  
{prTt1(188) ;}
   ;
NT_hpfStat : KW_REALIGN  NT_ident  KW_POUV  NT_dims  NT_closeParenthRecla  KW_WITH  NT_variableRef  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(189), mkSons(t1,mkSons(t2,mkSons(t4,(ListTree*)NULL))))) ;}
   ;
NT_hpfStat : KW_REALIGNWITH  NT_variableRef  NT_dblcolon  NT_idents  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(189), mkSons(t3,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL))))) ;}
   ;
NT_hpfStat : KW_REALIGN  KW_POUV  NT_dims  NT_closeParenthRecla  KW_WITH  NT_variableRef  NT_dblcolon  NT_idents  
{Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(189), mkSons(t5,mkSons(t1,mkSons(t3,(ListTree*)NULL))))) ;}
   ;
NT_hpfStat : KW_REDISTRIBUTEONTO  NT_ident  KW_POUV  NT_distFormats  NT_closeParenthRecla  KW_ONTO  NT_ident  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(190), mkSons(t1,mkSons(t2,mkSons(t4,(ListTree*)NULL))))) ;}
   ;
NT_hpfStat : KW_REDISTRIBUTE  NT_ident  KW_POUV  NT_distFormats  KW_PFER  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(190), mkSons(t1,mkSons(t2,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL))))) ;}
   ;
NT_hpfStat : KW_REDISTRIBUTE  KW_POUV  NT_distFormats  KW_PFER  NT_dblcolon  NT_idents  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(190), mkSons(t3,mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL))))) ;}
   ;
NT_hpfStat : KW_EMPTYREDISTRIBUTEONTO  NT_ident  NT_dblcolon  NT_idents  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(190), mkSons(t3,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL))))) ;}
   ;
NT_hpfStat : KW_REDISTRIBUTEONTO  KW_POUV  NT_distFormats  NT_closeParenthRecla  KW_ONTO  NT_ident  NT_dblcolon  NT_idents  
{Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(190), mkSons(t5,mkSons(t1,mkSons(t3,(ListTree*)NULL))))) ;}
   ;
NT_structStat : NT_opt_ident  KW_LOGIF  KW_POUV  NT_exp  NT_closeParenthRecla  KW_THEN  NT_acceptCommentsM2  KW_EOL  NT_stats  NT_acceptCommentsPost  NT_else_end  
{Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(214), mkSons(t1,mkSons(t2,mkSons(t5,mkSons(t7,(ListTree*)NULL)))))) ;}
   ;
NT_else_end : KW_ENDIF  NT_opt_ident  
{stbPop(); 
prTt0(315) ;}
   ;
NT_else_end : KW_ELSE  NT_opt_ident  KW_EOL  NT_stats  NT_acceptCommentsPost  KW_ENDIF  NT_opt_ident  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(t2) ;}
   ;
NT_else_end : KW_ELSEIF  KW_POUV  NT_exp  NT_closeParenthRecla  KW_THEN  NT_opt_ident  KW_EOL  NT_stats  NT_acceptCommentsPost  NT_else_end  
{Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(214), mkSons(t3,mkSons(t1,mkSons(t4,mkSons(t6,(ListTree*)NULL)))))) ;}
   ;
NT_structStat : NT_opt_ident  KW_STRUCTWHERE  KW_POUV  NT_exp  KW_PFER  NT_acceptCommentsM1  KW_EOL  NT_stats  NT_acceptCommentsPost  NT_elsewhere_end  
{Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(215), mkSons(t1,mkSons(t2,mkSons(t4,mkSons(t6,(ListTree*)NULL)))))) ;}
   ;
NT_elsewhere_end : KW_ENDWHERE  NT_opt_ident  
{stbPop(); 
prTt0(315) ;}
   ;
NT_elsewhere_end : KW_ELSEWHERE  NT_opt_ident  KW_EOL  NT_stats  NT_acceptCommentsPost  KW_ENDWHERE  NT_opt_ident  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(t2) ;}
   ;
NT_elsewhere_end : KW_ELSEWHERE  KW_POUV  NT_exp  KW_PFER  NT_opt_ident  KW_EOL  NT_stats  NT_acceptCommentsPost  NT_elsewhere_end  
{Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(215), mkSons(t2,mkSons(t1,mkSons(t3,mkSons(t5,(ListTree*)NULL)))))) ;}
   ;
NT_structStat : NT_opt_ident  KW_STRUCTFORALL  KW_POUV  NT_forallRanges  NT_opt_mask  KW_PFER  NT_acceptCommentsM2  KW_EOL  NT_stats  NT_acceptCommentsPost  KW_ENDFORALL  NT_opt_ident  
{Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(216), mkSons(t1,mkSons(t2,mkSons(t3,mkSons(t5,(ListTree*)NULL)))))) ;}
   ;
NT_structStat : NT_opt_ident  KW_SELECTCASE  KW_POUV  NT_exp  KW_PFER  NT_acceptCommentsM1  KW_EOL  NT_cases  KW_ENDSELECT  NT_opt_ident  
{Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(217), mkSons(t1,mkSons(t2,mkSons(t4,mkSons(t5,(ListTree*)NULL)))))) ;}
   ;
NT_structStat : KW_GOULDSELECTCASE  NT_exp  NT_acceptCommentsM0  KW_EOL  NT_cases  KW_ENDSELECT  NT_opt_ident  
{Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(217), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,mkSons(t3,mkSons(t4,(ListTree*)NULL)))))) ;}
   ;
NT_structStat : NT_opt_ident  KW_SELECTTYPE  KW_POUV  NT_expOrPtrAssign  KW_PFER  NT_acceptCommentsM1  KW_EOL  NT_cases  KW_ENDSELECT  NT_opt_ident  
{Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(218), mkSons(t1,mkSons(t2,mkSons(t4,mkSons(t5,(ListTree*)NULL)))))) ;}
   ;
NT_expOrPtrAssign : NT_exp  

   ;
NT_expOrPtrAssign : NT_ident  KW_PTRASS  NT_exp  
{prTt2(166) ;}
   ;
NT_case : KW_CASE  KW_NEEDKEYWORD  NT_case_selector  NT_opt_ident  KW_EOL  NT_stats  NT_acceptCommentsPost  
{stbPop(); 
prTt3(222) ;}
   ;
NT_case : KW_GOULDCASE  KW_NEEDKEYWORD  NT_gould_case_selector  KW_EOL  NT_stats  NT_acceptCommentsPost  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(222), mkSons(t1,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t2,(ListTree*)NULL))))) ;}
   ;
NT_case : KW_ELSE  KW_EOL  NT_stats  NT_acceptCommentsPost  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(222), mkSons(mkTree(stbRkOper(223), (ListTree*)NULL),mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL))))) ;}
   ;
NT_case : KW_TYPEIS  KW_POUV  NT_ident  KW_PFER  NT_opt_ident  KW_EOL  NT_stats  NT_acceptCommentsPost  
{stbPop(); 
prTt3(224) ;}
   ;
NT_case : KW_CLASSIS  KW_POUV  NT_ident  KW_PFER  NT_opt_ident  KW_EOL  NT_stats  NT_acceptCommentsPost  
{stbPop(); 
prTt3(225) ;}
   ;
NT_case : KW_CLASSDEFAULT  NT_opt_ident  KW_EOL  NT_stats  NT_acceptCommentsPost  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(225), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(t1,mkSons(t2,(ListTree*)NULL))))) ;}
   ;
NT_case_selector : KW_POUV  NT_caseRanges  KW_PFER  

   ;
NT_case_selector : KW_DEFAULT  
{prTt0(223) ;}
   ;
NT_gould_case_selector : KW_NEEDCOLON  NT_gould_caseRanges  KW_NEEDNOCOLON  

   ;
NT_gould_case_selector : KW_DEFAULT  
{prTt0(223) ;}
   ;
NT_caseRange : NT_generalExp  

   ;
NT_gould_caseRange : NT_uexp  

   ;
NT_gould_caseRange : NT_rangeColon  

   ;
NT_gould_caseRange : KW_POUV  NT_rangeColon  
{prTt1(273) ;}
   ;
NT_gould_caseRange : NT_rangeColon  KW_PFER  
{prTt1(274) ;}
   ;
NT_gould_caseRange : KW_POUV  NT_rangeColon  KW_PFER  
{prTt1(275) ;}
   ;
NT_structStat : NT_loopStructStat  

   ;
NT_loopStructStat : NT_opt_ident  KW_DO  KW_DOWHILEON  KW_INTONLYON  NT_opt_label  KW_DOLABEL  KW_INTONLYOFF  NT_do_control1  KW_DOWHILEOFF  NT_acceptCommentsM2  KW_EOL  NT_stats  NT_acceptCommentsPost  KW_ENDOFDO  
{Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(219), mkSons(t1,mkSons(t2,mkSons(t3,mkSons(t5,(ListTree*)NULL)))))) ;}
   ;
NT_do_control1 : 
{prTt0(292) ;}
   ;
NT_do_control1 : KW_VIRG  
{prTt0(292) ;}
   ;
NT_do_control1 : KW_VIRG  KW_VIRG  
{prTt0(292) ;}
   ;
NT_do_control1 : KW_VIRG  NT_do_control  

   ;
NT_do_control1 : NT_do_control  

   ;
NT_do_control : KW_WHILE  KW_POUV  NT_exp  KW_PFER  
{prTt1(226) ;}
   ;
NT_do_control : KW_BEGIN  
{prTt0(292) ;}
   ;
NT_do_control : KW_UNTIL  KW_POUV  NT_exp  KW_PFER  
{prTt1(227) ;}
   ;
NT_do_control : KW_POUV  KW_DOWHILEOFF  NT_exp  KW_PFER  KW_NEEDKEYWORD  KW_TIMES  
{prTt1(228) ;}
   ;
NT_do_control : KW_DOWHILEOFF  NT_doRange  

   ;
NT_structStat : NT_ompSentinel_needkw  KW_PARALLELDO  KW_NEEDKEYWORD  KW_opt_comma  NT_omp_directive_arguments  NT_no_opt_comma  NT_acceptCommentsXM2AndEOL  NT_loopStructStat  NT_acceptComments  NT_opt_eol_omp_end_parallel_do  
{Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(100), mkSons(t2,mkSons(t5,(ListTree*)NULL)))) ;}
   ;
NT_opt_eol_omp_end_parallel_do : 
{prTt0(292) ;}
   ;
NT_opt_eol_omp_end_parallel_do : KW_EOLOMPENDPARALLELDO  
{prTt0(292) ;}
   ;
NT_structStat : NT_ompSentinel_needkw  KW_DO  KW_NEEDKEYWORD  KW_opt_comma  NT_omp_directive_arguments  NT_no_opt_comma  NT_acceptCommentsXM2AndEOL  NT_loopStructStat  NT_acceptComments  NT_opt_eol_omp_end_do  
{Tree *t7 = stbPop(); 
Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(101), mkSons(t2,mkSons(t5,(ListTree*)NULL)))) ;}
   ;
NT_opt_eol_omp_end_do : 
{prTt0(292) ;}
   ;
NT_opt_eol_omp_end_do : KW_EOLOMPENDDO  
{prTt0(292) ;}
   ;
NT_structStat : NT_ompSentinel_needkw  KW_PARALLEL  KW_NEEDKEYWORD  KW_opt_comma  NT_omp_directive_arguments  NT_no_opt_comma  NT_acceptCommentsXM2AndEOL  NT_stats  NT_acceptCommentsPost  KW_OMPENDPARALLEL  
{Tree *t6 = stbPop(); 
Tree *t5 = stbPop(); 
Tree *t4 = stbPop(); 
Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(102), mkSons(t2,mkSons(t5,(ListTree*)NULL)))) ;}
   ;
NT_ompSentinel_needkw : NT_ompSentinel  KW_NEEDKEYWORD  

   ;
NT_ompSentinel : OMPDIRECTIVE  
{stbPush(mkAtom(stbRkOper(244), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_omp_directive_arguments : 
{prTt0(103) ;}
   ;
NT_omp_directive_arguments : NT_omp_clause  KW_NEEDKEYWORD  NT_opt_comma_needkw  NT_omp_directive_arguments  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkPre(t1, t3)) ;}
   ;
NT_omp_clause : KW_NUM_THREADS  KW_POUV  NT_exp  KW_PFER  
{prTt1(104) ;}
   ;
NT_omp_clause : KW_SCHEDULE  KW_POUV  NT_exps  KW_PFER  
{prTt1(105) ;}
   ;
NT_omp_clause : KW_SHARED  KW_POUV  NT_omp_clause_arguments  KW_PFER  
{prTt1(106) ;}
   ;
NT_omp_clause : KW_PRIVATE  KW_POUV  NT_omp_clause_arguments  KW_PFER  
{prTt1(107) ;}
   ;
NT_omp_clause : KW_FIRSTPRIVATE  KW_POUV  NT_omp_clause_arguments  KW_PFER  
{prTt1(108) ;}
   ;
NT_omp_clause : KW_LASTPRIVATE  KW_POUV  NT_omp_clause_arguments  KW_PFER  
{prTt1(109) ;}
   ;
NT_omp_clause : KW_REDUCTION  KW_POUV  KW_PLUS  KW_DPIN  NT_omp_clause_arguments  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(110), mkSons(mkAtom(stbRkOper(244), "+"),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_omp_clause : KW_REDUCTION  KW_POUV  KW_ETOI  KW_DPIN  NT_omp_clause_arguments  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(110), mkSons(mkAtom(stbRkOper(244), "*"),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_omp_clause : KW_REDUCTION  KW_POUV  NT_ident  KW_DPIN  NT_omp_clause_arguments  KW_PFER  
{prTt2(110) ;}
   ;
NT_omp_clause : KW_DEFAULT  KW_POUV  NT_ident  KW_PFER  
{prTt1(111) ;}
   ;
NT_omp_clause_arguments : NT_omp_clause_arg  
{prTt1(112) ;}
   ;
NT_omp_clause_arguments : NT_omp_clause_arguments  KW_VIRG  NT_omp_clause_arg  
{prTpost(112) ;}
   ;
NT_omp_clause_arg : NT_ident  

   ;
NT_io_stat : KW_OPEN  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(191) ;}
   ;
NT_io_stat : KW_CLOSE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(192) ;}
   ;
NT_io_stat : KW_FLUSH  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(193) ;}
   ;
NT_io_stat : KW_INQUIRE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  NT_generalExps  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(194), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_BACKSPACELOG  NT_logical_unit  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(195), mkSons(mkTree(stbRkOper(320), mkSons(t1,(ListTree*)NULL)),(ListTree*)NULL))) ;}
   ;
NT_io_stat : KW_BACKSPACE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(195) ;}
   ;
NT_io_stat : KW_ENDFILELOG  NT_logical_unit  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(196), mkSons(mkTree(stbRkOper(320), mkSons(t1,(ListTree*)NULL)),(ListTree*)NULL))) ;}
   ;
NT_io_stat : KW_ENDFILE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(196) ;}
   ;
NT_io_stat : KW_REWINDLOG  NT_logical_unit  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(197), mkSons(mkTree(stbRkOper(320), mkSons(t1,(ListTree*)NULL)),(ListTree*)NULL))) ;}
   ;
NT_io_stat : KW_REWIND  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(197) ;}
   ;
NT_io_stat : KW_PRINT  NT_format_id  KW_VIRG  NT_generalExps  
{prTt2(198) ;}
   ;
NT_io_stat : KW_PRINT  NT_format_id  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(198), mkSons(t1,mkSons(mkTree(stbRkOper(318), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_PRINT  
{stbPush(mkTree(stbRkOper(198), mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),mkSons(mkTree(stbRkOper(318), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_READ  KW_in_io  KW_POUV  NT_vax_io_specs  KW_PFER  NT_notin_io  NT_generalExps  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(199), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_READ  NT_format_id  KW_VIRG  NT_generalExps  
{prTt2(199) ;}
   ;
NT_io_stat : KW_READ  NT_format_id  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(199), mkSons(t1,mkSons(mkTree(stbRkOper(318), (ListTree*)NULL),(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_WRITE  KW_in_io  KW_POUV  NT_vax_io_specs  KW_PFER  NT_notin_io  NT_generalExps  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(200), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_ACCEPT  NT_format_id  NT_generalExps  
{prTt2(201) ;}
   ;
NT_io_stat : KW_ENCODE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  NT_generalExps  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(202), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_DECODE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  NT_generalExps  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(203), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_DELETE  KW_in_io  KW_POUV  NT_vax_io_specs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(204) ;}
   ;
NT_io_stat : KW_FIND  KW_in_io  KW_POUV  NT_vax_io_specs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(205) ;}
   ;
NT_io_stat : KW_REWRITE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  NT_generalExps  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(206), mkSons(t1,mkSons(t3,(ListTree*)NULL)))) ;}
   ;
NT_io_stat : KW_UNLOCK  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(207) ;}
   ;
NT_io_stat : KW_UNLOCK  NT_numspec  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(207), mkSons(mkTree(stbRkOper(320), mkSons(t1,(ListTree*)NULL)),(ListTree*)NULL))) ;}
   ;
NT_io_stat : KW_BACKFILELOG  NT_logical_unit  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(208), mkSons(mkTree(stbRkOper(320), mkSons(t1,(ListTree*)NULL)),(ListTree*)NULL))) ;}
   ;
NT_io_stat : KW_BACKFILE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(208) ;}
   ;
NT_io_stat : KW_SKIPFILELOG  NT_logical_unit  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(209), mkSons(mkTree(stbRkOper(320), mkSons(t1,(ListTree*)NULL)),(ListTree*)NULL))) ;}
   ;
NT_io_stat : KW_SKIPFILE  KW_in_io  KW_POUV  NT_ioSpecs  KW_PFER  NT_notin_io  
{stbPop(); 
prTt1(209) ;}
   ;
NT_io_stat : KW_DEFINEFILE  NT_defineSpecs  
{prTt1(210) ;}
   ;
NT_notin_io : KW_notin_io  
{prTt0(292) ;}
   ;
NT_defineSpec : NT_intCst  NT_defineparams  
{prTt2(211) ;}
   ;
NT_defineSpec : NT_bitCst  NT_defineparams  
{prTt2(211) ;}
   ;
NT_defineSpec : NT_ident  NT_defineparams  
{prTt2(211) ;}
   ;
NT_defineparams : KW_POUV  NT_numspec  KW_VIRG  NT_numspec  KW_VIRG  NT_opt_ident  KW_VIRG  NT_ident  KW_PFER  
{prTt4(212) ;}
   ;
NT_vax_io_specs : NT_unit_record  KW_VIRG  NT_ioSpecs  
{prTpre(320) ;}
   ;
NT_vax_io_specs : NT_unit_record  
{prTt1(320) ;}
   ;
NT_vax_io_specs : NT_ioSpecs  

   ;
NT_unit_record : NT_numspec  KW_QUOT  NT_exp  
{prTt2(213) ;}
   ;
NT_numspec : NT_variableRef  KW_NEEDNOCOLON  

   ;
NT_numspec : NT_intCst  

   ;
NT_numspec : NT_bitCst  

   ;
NT_ioSpec : NT_eqname_exp  

   ;
NT_ioSpec : NT_eqname  NT_eqname_exp  
{prTt2(113) ;}
   ;
NT_eqname_exp : NT_star  

   ;
NT_eqname_exp : NT_exp  

   ;
NT_eqname_exp : KW_POUV  NT_ioRanges  KW_PFER  

   ;
NT_ioRange : NT_opt_exp  KW_DPIN  NT_opt_exp  
{prTt2(246) ;}
   ;
NT_ioRange : NT_opt_exp  KW_DPIN  NT_opt_exp  KW_DPIN  NT_opt_exp  
{prTt3(246) ;}
   ;
NT_ioRange : NT_opt_exp  KW_DPIN  NT_opt_exp  KW_DPIN  NT_opt_exp  KW_DPIN  NT_opt_exp  
{prTt4(246) ;}
   ;
NT_format_id : NT_star  

   ;
NT_format_id : NT_label  

   ;
NT_format_id : NT_variableRef  

   ;
NT_format_id : NT_string_exp  

   ;
NT_logical_unit : NT_star  

   ;
NT_logical_unit : NT_exp  

   ;
NT_simple_exp : NT_ident  

   ;
NT_simple_exp : NT_simpleCst  

   ;
NT_generalExp : NT_exp  

   ;
NT_generalExp : KW_ETOI  NT_label  

   ;
NT_generalExp : NT_rangeColon  

   ;
NT_generalExp : NT_keywordArg  

   ;
NT_rangeColon : NT_opt_exp  KW_DPIN  NT_opt_exp  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(221), mkSons(t1,mkSons(t2,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL))))) ;}
   ;
NT_rangeColon : NT_opt_exp  KW_DPIN  NT_opt_exp  KW_DPIN  NT_exp  
{prTt3(221) ;}
   ;
NT_keywordArg : NT_ident  KW_EQUL  NT_exp  
{prTt2(113) ;}
   ;
NT_keywordArg : NT_ident  KW_EQUL  NT_star  
{prTt2(113) ;}
   ;
NT_keywordArg : NT_ident  KW_EQUL  NT_rangeColon  
{prTt2(113) ;}
   ;
NT_keywordArg : NT_star  

   ;
NT_exp : NT_uexp  

   ;
NT_exp : NT_compositeExp  

   ;
NT_compositeExp : NT_complexConstructor  

   ;
NT_compositeExp : NT_implDo  

   ;
NT_complexConstructor : KW_POUV  NT_uexp  KW_VIRG  NT_uexp  KW_PFER  
{prTt2(232) ;}
   ;
NT_implDo : KW_POUV  NT_uexp  KW_VIRG  NT_doRange  KW_PFER  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(118), mkSons(mkTree(stbRkOper(318), mkSons(t1,(ListTree*)NULL)),mkSons(t2,(ListTree*)NULL)))) ;}
   ;
NT_implDo : KW_POUV  NT_compositeExp  KW_VIRG  NT_doRange  KW_PFER  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(118), mkSons(mkTree(stbRkOper(318), mkSons(t1,(ListTree*)NULL)),mkSons(t2,(ListTree*)NULL)))) ;}
   ;
NT_implDo : KW_POUV  NT_exps2more  KW_VIRG  NT_doRange  KW_PFER  
{prTt2(118) ;}
   ;
NT_implDo : KW_POUV  KW_POUV  NT_exps3more  KW_PFER  KW_VIRG  NT_doRange  KW_PFER  
{prTt2(118) ;}
   ;
NT_doRange : NT_ident  KW_EQUL  NT_rangeComma  
{prTt2(220) ;}
   ;
NT_rangeComma : NT_exp  KW_VIRG  NT_exp  KW_VIRG  NT_exp  
{prTt3(221) ;}
   ;
NT_rangeComma : NT_exp  KW_VIRG  NT_exp  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(221), mkSons(t1,mkSons(t2,mkSons(mkTree(stbRkOper(292), (ListTree*)NULL),(ListTree*)NULL))))) ;}
   ;
NT_exps2more : NT_uexp  KW_VIRG  NT_exp  
{prTt2(318) ;}
   ;
NT_exps2more : NT_compositeExp  KW_VIRG  NT_exp  
{prTt2(318) ;}
   ;
NT_exps2more : NT_exps2more  KW_VIRG  NT_exp  
{prTpost(318) ;}
   ;
NT_exps3more : NT_exps2more  KW_VIRG  NT_uexp  
{prTpost(318) ;}
   ;
NT_exps3more : NT_exps2more  KW_VIRG  NT_implDo  
{prTpost(318) ;}
   ;
NT_uexp : NT_intCst  KW_LBRACKET  KW_NEEDTYPEKEYWORD  NT_constructor  KW_RBRACKET  
{prTt2(271) ;}
   ;
NT_uexp : KW_LBRACKET  KW_NEEDTYPEKEYWORD  NT_constructor  KW_RBRACKET  

   ;
NT_constructor : NT_exps  
{prTt1(272) ;}
   ;
NT_constructor : NT_rangeColon  
{prTt1(272) ;}
   ;
NT_constructor : NT_type  NT_dblcolon  NT_exps  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(309), mkSons(t1,mkSons(mkTree(stbRkOper(272), mkSons(t3,(ListTree*)NULL)),(ListTree*)NULL)))) ;}
   ;
NT_uexp : NT_variableRef  

   ;
NT_variableRef : NT_variableRef  KW_POUV  KW_NEEDCOLON  NT_generalExps  KW_NEEDNOCOLON  KW_PFER  
{prTt2(243) ;}
   ;
NT_variableRef : NT_variableRef  KW_PCEN  NT_ident  
{prTt2(242) ;}
   ;
NT_variableRef : NT_variableRef  KW_POIN  NT_ident  
{prTt2(242) ;}
   ;
NT_variableRef : NT_ident  

   ;
NT_generalExps : 
{prTt0(318) ;}
   ;
NT_generalExps : NT_generalExp  
{prTt1(318) ;}
   ;
NT_generalExps : NT_generalExps1  KW_VIRG  NT_opt_generalExp  
{prTpost(318) ;}
   ;
NT_generalExps1 : NT_opt_generalExp  
{prTt1(318) ;}
   ;
NT_generalExps1 : NT_generalExps1  KW_VIRG  NT_opt_generalExp  
{prTpost(318) ;}
   ;
NT_uexp : KW_POUV  NT_exp  KW_PFER  
{prTt1(270) ;}
   ;
NT_uexp : NT_simpleCst  

   ;
NT_uexp : NT_exp  KW_PLUS  NT_exp  
{prTt2(247) ;}
   ;
NT_uexp : NT_exp  KW_MINU  NT_exp  
{prTt2(248) ;}
   ;
NT_uexp : NT_exp  KW_ETOI  NT_exp  
{prTt2(249) ;}
   ;
NT_uexp : NT_exp  KW_SLAH  NT_exp  
{prTt2(250) ;}
   ;
NT_uexp : NT_exp  KW_ETOIETOI  NT_exp  
{prTt2(251) ;}
   ;
NT_uexp : KW_PLUS  NT_exp  
{prTt1(252) ;}
   ;
NT_uexp : KW_MINU  NT_exp  
{prTt1(253) ;}
   ;
NT_uexp : NT_exp  KW_EQ  NT_exp  
{prTt2(261) ;}
   ;
NT_uexp : NT_exp  KW_NE  NT_exp  
{prTt2(266) ;}
   ;
NT_uexp : NT_exp  KW_GE  NT_exp  
{prTt2(262) ;}
   ;
NT_uexp : NT_exp  KW_LE  NT_exp  
{prTt2(264) ;}
   ;
NT_uexp : NT_exp  KW_GT  NT_exp  
{prTt2(263) ;}
   ;
NT_uexp : NT_exp  KW_LT  NT_exp  
{prTt2(265) ;}
   ;
NT_uexp : NT_exp  KW_EQV  NT_exp  
{prTt2(259) ;}
   ;
NT_uexp : NT_exp  KW_NEQV  NT_exp  
{prTt2(260) ;}
   ;
NT_uexp : NT_exp  KW_AND  NT_exp  
{prTt2(256) ;}
   ;
NT_uexp : NT_exp  KW_OR  NT_exp  
{prTt2(257) ;}
   ;
NT_uexp : NT_exp  KW_XOR  NT_exp  
{prTt2(258) ;}
   ;
NT_uexp : KW_NOT  NT_exp  
{prTt1(255) ;}
   ;
NT_uexp : NT_exp  KW_SLAHSLAH  NT_exp  
{prTt2(254) ;}
   ;
NT_string_exp : NT_gstring  

   ;
NT_string_exp : NT_string_exp  KW_SLAHSLAH  NT_gstring  
{prTt2(254) ;}
   ;
NT_definedOp : OP_USER  
{stbPush(mkAtom(stbRkOper(269), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_uexp : KW_OP_USER  NT_definedOp  NT_exp  
{prTt2(267) ;}
   ;
NT_uexp : NT_exp  KW_OP_USER  NT_definedOp  NT_exp  
{Tree *t3 = stbPop(); 
Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(268), mkSons(t2,mkSons(t1,mkSons(t3,(ListTree*)NULL))))) ;}
   ;
NT_simpleCst : NT_nnCst  

   ;
NT_simpleCst : NT_gstring  

   ;
NT_simpleCst : NT_kindCst  

   ;
NT_nnCst : NT_intCst  

   ;
NT_nnCst : NT_realCst  

   ;
NT_nnCst : NT_doubleCst  

   ;
NT_nnCst : NT_bitCst  

   ;
NT_nnCst : NT_logicCst  

   ;
NT_kindCst : NT_nnCst  KW_UNDERSCORE  NT_ident  
{prTt2(238) ;}
   ;
NT_kindCst : NT_nnCst  KW_UNDERSCORE  NT_intCst  
{prTt2(238) ;}
   ;
NT_kindCst : NT_ident  KW_UNDERSCORE  NT_gstring  
{prTtr2(238) ;}
   ;
NT_type : NT_f77_like_type  

   ;
NT_type : NT_type_non77like  

   ;
NT_type_non77like : NT_groundType  

   ;
NT_type_non77like : NT_groundType  NT_typesize  
{prTt2(276) ;}
   ;
NT_type_non77like : KW_TYPE  KW_POUV  NT_ident  KW_PFER  
{prTt1(278) ;}
   ;
NT_type_non77like : KW_CLASS  KW_POUV  NT_ident  KW_PFER  
{prTt1(279) ;}
   ;
NT_type_non77like : KW_CLASS  KW_POUV  NT_star  KW_PFER  
{prTt1(279) ;}
   ;
NT_type_non77like : KW_PROCEDURE  KW_POUV  NT_opt_ident  KW_PFER  
{prTt1(280) ;}
   ;
NT_type_non77like : KW_PROCEDURE  
{prTdf1(280) ;}
   ;
NT_groundType : KW_CHARACTER  
{prTt0(281) ;}
   ;
NT_groundType : NT_non_char_type  

   ;
NT_non_char_type : KW_INTEGER  
{prTt0(282) ;}
   ;
NT_non_char_type : KW_REAL  
{prTt0(283) ;}
   ;
NT_non_char_type : KW_DOUBLE  
{prTt0(284) ;}
   ;
NT_non_char_type : KW_COMPLEX  
{prTt0(285) ;}
   ;
NT_non_char_type : KW_DOUBLECOMPLEX  
{prTt0(286) ;}
   ;
NT_non_char_type : KW_LOGICAL  
{prTt0(287) ;}
   ;
NT_non_char_type : KW_BYTE  
{prTt0(288) ;}
   ;
NT_non_char_type : KW_BIT  
{prTt0(289) ;}
   ;
NT_non_char_type : KW_VOID  
{prTt0(290) ;}
   ;
NT_typesize : KW_size_comma  KW_ETOI  KW_INTONLYON  NT_typel_1  KW_INTONLYOFF  

   ;
NT_typel_1 : NT_intCst  

   ;
NT_typel_1 : KW_POUV  NT_exp  KW_PFER  

   ;
NT_typel_1 : KW_POUV  NT_star  KW_PFER  

   ;
NT_f77_like_type : NT_non_char_type  KW_POUV  NT_generalExp  KW_PFER  
{Tree *t2 = stbPop(); 
Tree *t1 = stbPop(); 
if ((treeOpCode(t1) == 285)) 
{
if ((treeOpCode(t2) == 234) && (treeAtomValue(t2) == '4')) 
{
stbPush(mkTree(stbRkOper(276), mkSons(t1,mkSons(mkAtom(stbRkOper(234), "8"),(ListTree*)NULL)))) ;} else if ((treeOpCode(t2) == 234) && (treeAtomValue(t2) == '8')) 
{
stbPush(mkTree(stbRkOper(276), mkSons(t1,mkSons(mkAtom(stbRkOper(234), "16"),(ListTree*)NULL)))) ;} else {
stbPush(mkTree(stbRkOper(276), mkSons(t1,mkSons(t2,(ListTree*)NULL)))) ;}} else {
stbPush(mkTree(stbRkOper(276), mkSons(t1,mkSons(t2,(ListTree*)NULL)))) ;}}
   ;
NT_f77_like_type : KW_CHARACTER  KW_POUV  KW_NEEDCOLON  NT_generalExps  KW_NEEDNOCOLON  KW_PFER  
{Tree *t1 = stbPop(); 
stbPush(mkTree(stbRkOper(276), mkSons(mkTree(stbRkOper(281), (ListTree*)NULL),mkSons(t1,(ListTree*)NULL)))) ;}
   ;
NT_ident : NAME  
{stbPush(mkAtom(stbRkOper(244), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_star : KW_ETOI  
{prTt0(231) ;}
   ;
NT_dblcolon : KW_DPIN  KW_DPIN  
{prTt0(292) ;}
   ;
NT_eqname : EQNAME  
{stbPush(mkAtom(stbRkOper(244), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_label : INTCST  
{stbPush(mkAtom(stbRkOper(229), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_intCst : INTCST  
{stbPush(mkAtom(stbRkOper(234), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_bitCst : BITCST  
{stbPush(mkAtom(stbRkOper(237), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_realCst : REALCST  
{stbPush(mkAtom(stbRkOper(235), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_doubleCst : DOUBLECST  
{stbPush(mkAtom(stbRkOper(236), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_logicCst : KW_TRUE  
{stbPush(mkAtom(stbRkOper(233), "true")) ;}
   ;
NT_logicCst : KW_FALSE  
{stbPush(mkAtom(stbRkOper(233), "false")) ;}
   ;
NT_letter : LETTER  
{stbPush(mkAtom(stbRkOper(38), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_oneOffset : OFFSET  
{stbPush(mkAtom(stbRkOper(234), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_gstring : HOLLERITH  
{stbPush(mkAtom(stbRkOper(239), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_gstring : NT_strings1  

   ;
NT_string : ASTRING  
{stbPush(mkAtom(stbRkOper(241), (AtomValue)(strdup(yytext)))) ;}
   ;
NT_simpleOption : KW_NOG_FLOATING  
{stbPush(mkTree(stbRkOper(3), mkSons(mkTree(stbRkOper(14), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_simpleOption : KW_G_FLOATING  
{stbPush(mkTree(stbRkOper(3), mkSons(mkTree(stbRkOper(13), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_simpleOption : KW_NOI4  
{stbPush(mkTree(stbRkOper(4), mkSons(mkTree(stbRkOper(14), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_simpleOption : KW_I4  
{stbPush(mkTree(stbRkOper(4), mkSons(mkTree(stbRkOper(13), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_simpleOption : KW_NOF77  
{stbPush(mkTree(stbRkOper(5), mkSons(mkTree(stbRkOper(14), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_simpleOption : KW_F77  
{stbPush(mkTree(stbRkOper(5), mkSons(mkTree(stbRkOper(13), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_simpleOption : KW_NOCHECK  
{stbPush(mkTree(stbRkOper(7), mkSons(mkTree(stbRkOper(14), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_simpleOption : KW_NOEXTEND_SOURCE  
{stbPush(mkTree(stbRkOper(6), mkSons(mkTree(stbRkOper(14), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_simpleOption : KW_EXTEND_SOURCE  
{stbPush(mkTree(stbRkOper(6), mkSons(mkTree(stbRkOper(13), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_checkOption : KW_ALL  
{prTt0(11) ;}
   ;
NT_checkOption : KW_NOOVERFLOW  
{stbPush(mkTree(stbRkOper(8), mkSons(mkTree(stbRkOper(14), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_checkOption : KW_OVERFLOW  
{stbPush(mkTree(stbRkOper(8), mkSons(mkTree(stbRkOper(13), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_checkOption : KW_NOBOUNDS  
{stbPush(mkTree(stbRkOper(9), mkSons(mkTree(stbRkOper(14), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_checkOption : KW_BOUNDS  
{stbPush(mkTree(stbRkOper(9), mkSons(mkTree(stbRkOper(13), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_checkOption : KW_NOUNDERFLOW  
{stbPush(mkTree(stbRkOper(10), mkSons(mkTree(stbRkOper(14), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_checkOption : KW_UNDERFLOW  
{stbPush(mkTree(stbRkOper(10), mkSons(mkTree(stbRkOper(13), (ListTree*)NULL),(ListTree*)NULL))) ;}
   ;
NT_checkOption : KW_NONE  
{prTt0(12) ;}
   ;
NT_closeParenthRecla : KW_PFER  KW_RECLASSIFY  
{prTt0(292) ;}
   ;
NT_acceptCommentsAndEOL : NT_acceptComments  KW_EOL  
{stbPop(); 
prTt0(292) ;}
   ;
NT_acceptCommentsXM2AndEOL : NT_acceptCommentsXM2  KW_EOL  
{stbPop(); 
prTt0(292) ;}
   ;
NT_acceptComments : KW_ACCEPTCOMMENTS  
{prTt0(292) ;}
   ;
NT_acceptCommentsPost : KW_ACCEPTCOMMENTSPOST  
{prTt0(292) ;}
   ;
NT_acceptCommentsM0 : KW_ACCEPTCOMMENTSM0  
{prTt0(292) ;}
   ;
NT_acceptCommentsM1 : KW_ACCEPTCOMMENTSM1  
{prTt0(292) ;}
   ;
NT_acceptCommentsM2 : KW_ACCEPTCOMMENTSM2  
{prTt0(292) ;}
   ;
NT_acceptCommentsXM2 : KW_ACCEPTCOMMENTSXM2  
{prTt0(292) ;}
   ;
NT_units : 
{prTt0(293) ;}
   ;
NT_units : NT_units  NT_unit  
{prTpost(293) ;}
   ;
NT_decls : 
{prTt0(297) ;}
   ;
NT_decls : NT_not_empty_decls  

   ;
NT_not_empty_decls : NT_decls  NT_decl  NT_acceptCommentsAndEOL  
{stbPop(); 
prTpost(297) ;}
   ;
NT_cases : 
{prTt0(317) ;}
   ;
NT_cases : NT_cases  NT_case  
{prTpost(317) ;}
   ;
NT_privateSequences : 
{prTt0(303) ;}
   ;
NT_privateSequences : NT_privateSequences  NT_privateSequence  
{prTpost(303) ;}
   ;
NT_formatElems : 
{prTt0(307) ;}
   ;
NT_formatElems : NT_formatElems  NT_formatElem  
{prTpost(307) ;}
   ;
NT_components : 
{prTt0(304) ;}
   ;
NT_components : NT_components  NT_component  
{prTpost(304) ;}
   ;
NT_stats : 
{prTt0(315) ;}
   ;
NT_stats : NT_not_empty_stats  

   ;
NT_not_empty_stats : NT_stats  NT_stat  NT_acceptCommentsAndEOL  
{stbPop(); 
prTpost(315) ;}
   ;
NT_commons1 : NT_common  
{prTt1(52) ;}
   ;
NT_commons1 : NT_commons1  NT_common  
{prTpost(52) ;}
   ;
NT_records1 : NT_record  
{prTt1(88) ;}
   ;
NT_records1 : NT_records1  NT_record  
{prTpost(88) ;}
   ;
NT_extBases1 : NT_extBase  
{prTt1(92) ;}
   ;
NT_extBases1 : NT_extBases1  NT_extBase  
{prTpost(92) ;}
   ;
NT_namelists1 : NT_namelist  
{prTt1(114) ;}
   ;
NT_namelists1 : NT_namelists1  NT_namelist  
{prTpost(114) ;}
   ;
NT_localUnits1 : NT_localUnit  
{prTt1(293) ;}
   ;
NT_localUnits1 : NT_localUnits1  NT_localUnit  
{prTpost(293) ;}
   ;
NT_interfaces1 : NT_interface  NT_acceptCommentsAndEOL  
{stbPop(); 
prTt1(306) ;}
   ;
NT_interfaces1 : NT_interfaces1  NT_interface  NT_acceptCommentsAndEOL  
{stbPop(); 
prTpost(306) ;}
   ;
NT_strings1 : NT_string  
{prTt1(240) ;}
   ;
NT_strings1 : NT_strings1  NT_string  
{prTpost(240) ;}
   ;
NT_parameters : NT_parameter  
{prTt1(31) ;}
   ;
NT_parameters : NT_parameters  KW_VIRG  NT_parameter  
{prTpost(31) ;}
   ;
NT_dirArgs : NT_ioSpec  
{prTt1(308) ;}
   ;
NT_dirArgs : NT_dirArgs  KW_VIRG  NT_ioSpec  
{prTpost(308) ;}
   ;
NT_letterss : NT_letters  
{prTt1(298) ;}
   ;
NT_letterss : NT_letterss  KW_VIRG  NT_letters  
{prTpost(298) ;}
   ;
NT_equivalences : NT_equivalence  
{prTt1(51) ;}
   ;
NT_equivalences : NT_equivalences  KW_VIRG  NT_equivalence  
{prTpost(51) ;}
   ;
NT_variableRefs : NT_variableRef  
{prTt1(301) ;}
   ;
NT_variableRefs : NT_variableRefs  KW_VIRG  NT_variableRef  
{prTpost(301) ;}
   ;
NT_idents : NT_ident  
{prTt1(50) ;}
   ;
NT_idents : NT_idents  KW_VIRG  NT_ident  
{prTpost(50) ;}
   ;
NT_basicDecls : NT_basicDecl  
{prTt1(302) ;}
   ;
NT_basicDecls : NT_basicDecls  KW_VIRG  NT_basicDecl  
{prTpost(302) ;}
   ;
NT_fullDecls : NT_fullDecl  
{prTt1(299) ;}
   ;
NT_fullDecls : NT_fullDecls  KW_VIRG  NT_fullDecl  
{prTpost(299) ;}
   ;
NT_labels : NT_label  
{prTt1(321) ;}
   ;
NT_labels : NT_labels  KW_VIRG  NT_label  
{prTpost(321) ;}
   ;
NT_params : NT_param  
{prTt1(295) ;}
   ;
NT_params : NT_params  KW_VIRG  NT_param  
{prTpost(295) ;}
   ;
NT_forallRanges : NT_forallRange  
{prTt1(316) ;}
   ;
NT_forallRanges : NT_forallRanges  KW_VIRG  NT_forallRange  
{prTpost(316) ;}
   ;
NT_caseRanges : NT_caseRange  
{prTt1(318) ;}
   ;
NT_caseRanges : NT_caseRanges  KW_VIRG  NT_caseRange  
{prTpost(318) ;}
   ;
NT_gould_caseRanges : NT_gould_caseRange  
{prTt1(318) ;}
   ;
NT_gould_caseRanges : NT_gould_caseRanges  KW_VIRG  NT_gould_caseRange  
{prTpost(318) ;}
   ;
NT_ioRanges : NT_ioRange  
{prTt1(318) ;}
   ;
NT_ioRanges : NT_ioRanges  KW_VIRG  NT_ioRange  
{prTpost(318) ;}
   ;
NT_renamedVisibles : NT_renamed  
{prTt1(72) ;}
   ;
NT_renamedVisibles : NT_renamedVisibles  KW_VIRG  NT_renamed  
{prTpost(72) ;}
   ;
NT_pointerPairs : NT_pointerPair  
{prTt1(80) ;}
   ;
NT_pointerPairs : NT_pointerPairs  KW_VIRG  NT_pointerPair  
{prTpost(80) ;}
   ;
NT_datavars : NT_datavar  
{prTt1(310) ;}
   ;
NT_datavars : NT_datavars  KW_VIRG  NT_datavar  
{prTpost(310) ;}
   ;
NT_values : NT_value  
{prTt1(311) ;}
   ;
NT_values : NT_values  KW_VIRG  NT_value  
{prTpost(311) ;}
   ;
NT_blockNames : NT_blockName  
{prTt1(50) ;}
   ;
NT_blockNames : NT_blockNames  KW_VIRG  NT_blockName  
{prTpost(50) ;}
   ;
NT_ioSpecs : NT_ioSpec  
{prTt1(320) ;}
   ;
NT_ioSpecs : NT_ioSpecs  KW_VIRG  NT_ioSpec  
{prTpost(320) ;}
   ;
NT_exps : NT_exp  
{prTt1(318) ;}
   ;
NT_exps : NT_exps  KW_VIRG  NT_exp  
{prTpost(318) ;}
   ;
NT_defineSpecs : NT_defineSpec  
{prTt1(319) ;}
   ;
NT_defineSpecs : NT_defineSpecs  KW_VIRG  NT_defineSpec  
{prTpost(319) ;}
   ;
NT_options : KW_SLAH  KW_NEEDKEYWORD  NT_option  
{prTt1(294) ;}
   ;
NT_options : NT_options  KW_SLAH  KW_NEEDKEYWORD  NT_option  
{prTpost(294) ;}
   ;
NT_prefixs : NT_prefix  KW_NEEDKEYWORD  
{prTt1(296) ;}
   ;
NT_prefixs : NT_prefixs  NT_prefix  KW_NEEDKEYWORD  
{prTpost(296) ;}
   ;
NT_implicits : NT_implicit  
{prTt1(33) ;}
   ;
NT_implicits : NT_implicits  KW_VIRG  KW_NEEDIMPLICIT  NT_implicit  
{prTpost(33) ;}
   ;
NT_onlyVisibles : 
{prTt0(71) ;}
   ;
NT_onlyVisibles : NT_visible  
{prTt1(71) ;}
   ;
NT_onlyVisibles : NT_onlyVisibles  KW_VIRG  KW_NEEDOPKEYWORD  NT_visible  
{prTpost(71) ;}
   ;
NT_genericNames1 : NT_genericName  
{prTt1(50) ;}
   ;
NT_genericNames1 : NT_genericNames1  KW_VIRG  KW_NEEDOPKEYWORD  NT_genericName  
{prTpost(50) ;}
   ;
NT_fieldDecls1 : NT_fieldDecl  KW_EOL  
{prTt1(304) ;}
   ;
NT_fieldDecls1 : NT_fieldDecls1  NT_fieldDecl  KW_EOL  
{prTpost(304) ;}
   ;
NT_mapDecls1 : NT_mapDecl  KW_EOL  
{prTt1(305) ;}
   ;
NT_mapDecls1 : NT_mapDecls1  NT_mapDecl  KW_EOL  
{prTpost(305) ;}
   ;
NT_typeSpecModifiers : KW_NEEDKEYWORD  NT_typeSpecModifier  
{prTt1(312) ;}
   ;
NT_typeSpecModifiers : NT_typeSpecModifiers  KW_VIRG  KW_NEEDKEYWORD  NT_typeSpecModifier  
{prTpost(312) ;}
   ;
NT_dims : KW_NEEDCOLON  NT_dim  
{prTt1(300) ;}
   ;
NT_dims : NT_dims  KW_NEEDCOLON  KW_VIRG  NT_dim  
{prTpost(300) ;}
   ;
NT_distFormats : KW_RECLASSIFY  NT_distFormat  
{prTt1(313) ;}
   ;
NT_distFormats : NT_distFormats  KW_VIRG  KW_RECLASSIFY  NT_distFormat  
{prTpost(313) ;}
   ;
NT_hpfAttributes : NT_hpfAttribute  
{prTt1(314) ;}
   ;
NT_hpfAttributes : NT_hpfAttributes  KW_VIRG  KW_RECLASSIFY  NT_hpfAttribute  
{prTpost(314) ;}
   ;
NT_opt_offset : NT_offset  

   ;
NT_opt_offset : 
{prTt0(292) ;}
   ;
NT_opt_exp : NT_exp  

   ;
NT_opt_exp : 
{prTt0(292) ;}
   ;
NT_opt_generalExp : NT_generalExp  

   ;
NT_opt_generalExp : 
{prTt0(292) ;}
   ;
NT_opt_ident : NT_ident  

   ;
NT_opt_ident : 
{prTt0(292) ;}
   ;
NT_opt_label : NT_label  

   ;
NT_opt_label : 
{prTt0(292) ;}
   ;
NT_opt_dblcolon : NT_dblcolon  
{stbPop(); 
prTt0(292) ;}
   ;
NT_opt_dblcolon : 
{prTt0(292) ;}
   ;
NT_opt_returnSize : NT_returnSize  

   ;
NT_opt_returnSize : 
{prTt0(292) ;}
   ;
NT_opt_comma_needkw : KW_VIRG  KW_NEEDKEYWORD  
{prTt0(292) ;}
   ;
NT_opt_comma_needkw : 
{prTt0(292) ;}
   ;
NT_opt_comma : KW_VIRG  
{prTt0(292) ;}
   ;
NT_opt_comma : 
{prTt0(292) ;}
   ;
NT_axiom : EN_Unit  NT_unit  

   ;
NT_axiom : EN_OptHeader  NT_header  

   ;
NT_axiom : EN_OptHeader  
{prTt0(292) ;}
   ;
NT_axiom : EN_OptTrailer  NT_trailer  

   ;
NT_axiom : EN_OptTrailer  
{prTt0(292) ;}
   ;
NT_axiom : EN_Decls  NT_decls  

   ;
NT_axiom : EN_Decl  NT_decl  

   ;
NT_axiom : EN_Stats  NT_stats  

   ;
NT_axiom : EN_Stat  NT_stat  

   ;
NT_axiom : EN_Exp  NT_generalExp  

   ;
NT_axiom : EN_Type  NT_type  

   ;
NT_includefile : NT_decls  NT_topStats  NT_acceptCommentsPost  
{stbPop(); 
prTt2(1) ;}
   ;
NT_axiom : EN_IncludeFile  NT_includefile  

   ;
%%
