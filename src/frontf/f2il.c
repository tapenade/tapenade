
/****** GENERATED FILE : DO NOT EDIT !!!! ******/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../front/cvtp.h"
#include "../front/treeBuilder.h"
#include "tablesfortran.c"
#include "../../build/generated-src/c/main/front/tablesil.c"

#define new(TYPE) (TYPE*)malloc(sizeof(TYPE))

/* TYPES: */

/* Type for "ttml function for translation of trees with a fixed root operator". */
typedef void (TTMLOperatorTreeTranslator)(Tree*, int, short) ;

/* Used to cut a ListTree in two ListTree's */
typedef struct _Cutter{
  ListTree **cut ;
  ListTree *left, *right ;
}Cutter ;

/* GLOBAL VARIABLES: */
static TreeBuilder *targetBuilder = NULL ;
typedef struct TargetBuilderStack_ { 
    TreeBuilder* top; 
    struct TargetBuilderStack_* next;
} TargetBuilderStack;
static TargetBuilderStack* stack = NULL ;
static FILE *protocolOutputStream = NULL ;
static const char *(contextNames[]) = {"TOP", "fuse", "blockdata", "impldecl", "pointer", "reverse", "beginInclude", "inInclude", "endInclude", "nameOnly", "dimsOnly", "forallR", "inSwitch"} ;

void sendListTreeProtocol(ListTree *listTree, FILE *protocolOutputStream) ;

void sendTreeProtocol(Tree *tree, FILE *protocolOutputStream) {
  if ((tree == NULL) || (tree->oper == NULL)) {
    fprintf(stdout, "incomplete tree\n") ;
  } else {
    if (tree->annotations != NULL) {
      KeyListTree *tlAnnotations = tree->annotations ;
      while (tlAnnotations != NULL) {
	fprintf(protocolOutputStream, "%d\n", ilAnnotationCode) ;
	fprintf(protocolOutputStream, "%s\n", tlAnnotations->key) ;
	sendTreeProtocol(tlAnnotations->tree, protocolOutputStream) ;
	tlAnnotations = tlAnnotations->next ;
      }
    }
    fprintf(protocolOutputStream, "%d\n", tree->oper->rank) ;
    if (tree->oper->arity == 0) {
        if (tree->contents.value)
	           fprintf(protocolOutputStream, "%s\n", tree->contents.value) ;
    } else {
      sendListTreeProtocol(tree->contents.sons, protocolOutputStream) ;
      if (tree->oper->arity == -1)
        fprintf(protocolOutputStream, "%d\n", ilEndOfListCode) ;
    }
  }
}

void sendListTreeProtocol(ListTree *listTree, FILE *protocolOutputStream) {
    while (listTree != NULL) {
      sendTreeProtocol(listTree->tree, protocolOutputStream) ;
      listTree = listTree->next ;
    }
}

void sendOp(int operRk) {
  if (stack->top)
    startTree(stack->top, operRk) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%d\n", operRk) ;
  else
    printf("%d\n", operRk) ;
}

void sendValue(const char *value) {
  if (stack->top)
    putValue(stack->top, strdup(value)) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%s\n", value) ;
  else
    printf("%s\n", value) ;
}

void endList() {
  if (stack->top)
    terminateListTree(stack->top) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%d\n", ilEndOfListCode) ;
  else
    printf("%d\n", ilEndOfListCode) ;
}

void sendAnnotation(char *name) {
    if (stack->top) {
      startAnnotation(stack->top, name) ;
  } else if (protocolOutputStream) {
    fprintf(protocolOutputStream, "%d\n", ilAnnotationCode) ;
      fprintf(protocolOutputStream, "%s\n", name) ;
  } else {
      printf("%d\n", ilAnnotationCode) ;
      printf("%s\n", name) ;
  }
}

void acceptAnnotations() {
    if (stack->top) stack->top->acceptAnnotations = 1 ;
}

void sendTree(Tree *tree) {
  if (stack->top)
    putTree(stack->top, tree) ;
  else
    sendTreeProtocol(tree,
		     (protocolOutputStream)?protocolOutputStream:stdout) ;
}

void sendListTree(ListTree *listTree) {
  if (stack->top)
    putListTree(stack->top, listTree) ;
  else
    sendListTreeProtocol(listTree,
	                        (protocolOutputStream)?protocolOutputStream:stdout) ;
}

Tree *sendDummyOp(int arity) {
    if (stack->top)
	return startDummyTree(stack->top, arity) ;
    else {
	printf("Cannot start a dummy tree with no targetBuilder\n") ;
	return NULL ;
    }
}

void cutAt(Cutter *cutter, ListTree *listTree, int cutPos) {
  /* only implemented case: cutPos < 0 */
  int i = listTreeLength(listTree) + cutPos ;
  if (i==0) {
    cutter->left = NULL ;
    cutter->right = listTree ;
    cutter->cut = NULL ;
  } else {
    cutter->left = listTree ;
    while ((--i)>0) listTree = listTree->next ;
    cutter->right = listTree->next ;
    listTree->next = NULL ;
    cutter->cut = &(listTree->next) ;
  }
}

void undoCut(Cutter *cutter) {
  if (cutter->cut != NULL) *(cutter->cut)=cutter->right ;
}

void redoCut(Cutter *cutter) {
  if (cutter->cut != NULL) *(cutter->cut)=NULL ;
}

/*Forward declarations: */
void f2ilTTList(ListTree *listTree, int context, short resultKind) ;
void f2ilTT(Tree *fTree, int context, short resultKind) ;

void ttmlNoRule(Tree *fTree, int context, short resultKind) {
      char *message = (char*)malloc(200) ;
      sprintf(message, "Unexpected source operator: %s (%s)",
		   fTree->oper->name, contextNames[context]) ;
      sendOp(ilParsingErrorCode) ;
      sendValue(message) ;
}

void action5(Tree *fTree, short resultKind) {
    /* RULE dimsOnly::X -> 
              none()  */
    sendOp(143) ;
}

void action4(Tree *fTree, short resultKind) {
    /* RULE nameOnly::X -> 
              X  */
    f2ilTT(fTree, 0, resultKind) ;
}

void action3(Tree *fTree, short resultKind) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree, "line") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("line") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree, 0, 1) ;
    annotCutter0->key = annotKey0 ;
}

void action2(Tree *fTree, short resultKind) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree, "postComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("postComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree, 0, 1) ;
    annotCutter0->key = annotKey0 ;
}

void action1(Tree *fTree, short resultKind) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree, "preComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree, 0, 1) ;
    annotCutter0->key = annotKey0 ;
}

#include "f2iluserFunctions.c"

void f2ilTT1(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE includeFile(Decls,Stats) -> 
              blockStatement[fuse::Decls,fuse::Stats]  */
    sendOp(27) ;
    f2ilTT(fTree->contents.sons->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->tree, 1, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT2(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree->contents.sons->next->tree, "preComments"))) {
    /* RULE X:=unit(Options,Header^{preComments:C},Decls,Stats,Units,Trail,Offset) -> 
              X^{preComments:C}  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree->contents.sons->next->tree, "preComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree, 0, 1) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 292)) {
    /* RULE unit(Options,none(),Decls,Stats,Units,Trail,Offset) -> 
              program(modifiers[Options],none(),varDeclarations(),blockStatement[fuse::Decls,fuse::Stats,fuse::Units,(Trail)])  */
    sendOp(164) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->tree, 0, -1) ;
    endList() ;
    sendOp(143) ;
    sendOp(207) ;
    endList() ;
    sendOp(27) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->next->tree, 0, 1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 15)) {
    /* RULE unit(Options,program(Ident),Decls,Stats,Units,Trail,Offset) -> 
              program(modifiers[Options],Ident,varDeclarations(),blockStatement[fuse::Decls,fuse::Stats,fuse::Units,(Trail)])  */
    sendOp(164) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->tree, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
    sendOp(207) ;
    endList() ;
    sendOp(27) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->next->tree, 0, 1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 17)) {
    /* RULE unit(Options,module(Ident),Decls,Stats,Units,Trail,Offset) -> 
              module(Ident,declarations[fuse::Decls,fuse::Stats,fuse::Units,(Trail)])  */
    sendOp(135) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
    sendOp(54) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->next->tree, 0, 1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 19)) {
    /* RULE unit(Options,subroutine(Prefixs,Ident,Params,OptBind),Decls,Stats,Units,Trail,Offset) -> 
              function(modifiers[Options,Prefixs],void(),none(),Ident^{bind:OptBind},Params,blockStatement[fuse::Decls,fuse::Stats,fuse::Units,(Trail)
])  */
    sendOp(91) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->tree, 0, -1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, -1) ;
    endList() ;
    sendOp(211) ;
    sendOp(143) ;
    sendAnnotation("bind") ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->next->next->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(27) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->next->tree, 0, 1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 20)) {
    /* RULE unit(Options,function(Type,Ident,Params,Prefix,OptIdent),Decls,Stats,Units,Trail,Offset) -> 
              function(modifiers[Options,Prefix],Type,none(),Ident^{explicitReturnVar:OptIdent},Params,blockStatement[fuse::Decls,fuse::Stats,fuse::
Units,(Trail)])  */
    sendOp(91) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->tree, 0, -1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->next->next->tree, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
    sendOp(143) ;
    sendAnnotation("explicitReturnVar") ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->next->next->next->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(27) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->next->tree, 0, 1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 16)) {
    /* RULE unit(Options,blockdata(OptIdent),Decls,Stats,Units,Trail,Offset) -> 
              module(blockdata::OptIdent,Decls)  */
    sendOp(135) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 2, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT20(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE function(Type,Ident,Params,Prefix,OptIdent) -> 
              function(modifiers[Prefix],Type,none(),Ident^{explicitReturnVar:OptIdent},Params,none())  */
    sendOp(91) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(143) ;
    sendAnnotation("explicitReturnVar") ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT21(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE recursive() -> 
              ident:"recursive"  */
    sendOp(98) ;
    sendValue("recursive") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT22(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE pure() -> 
              ident:"pure"  */
    sendOp(98) ;
    sendValue("pure") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT23(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE elemental() -> 
              ident:"elemental"  */
    sendOp(98) ;
    sendValue("elemental") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT24(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE end() -> 
              none()  */
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT25(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endprogram(X) -> 
              none()  */
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endprogram(OptIdent) -> 
              none()  */
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT26(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endblockdata(X) -> 
              none()  */
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT27(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endsubroutine(X) -> 
              none()  */
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endsubroutine(OptIdent) -> 
              none()  */
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT28(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endfunction(X) -> 
              none()  */
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endfunction(OptIdent) -> 
              none()  */
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT29(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endmodule(X) -> 
              none()  */
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE endmodule(OptIdent) -> 
              none()  */
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT31(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE parameters(Param)* -> 
              constDeclaration(none(),declarators(Param)*)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(46) ;
    sendOp(143) ;
    sendOp(55) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT32(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context &&
      (fTree->contents.sons->next->tree->oper->rank == 292)) {
    /* RULE parameterDecl(Ident,none()) -> 
              Ident  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE parameterDecl(Ident,Exp) -> 
              assign(Ident,Exp)  */
    sendOp(14) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT34(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE typedLetters(TypeOrMore,Letterss) -> 
              implicit(typedLetters(TypeOrMore,Letterss))  */
    sendOp(102) ;
    sendOp(200) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT37(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE letterRange(Letter1,Letter2) -> 
              letterRange(Letter1,Letter2)  */
    sendOp(123) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT38(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE letter N -> 
              letter N  */
    sendOp(122) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT39(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 299)) {
    /* RULE dimensionDecl(fullDecls(FullDecl)*) -> 
              varDimDeclaration(FullDecl)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(209) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT40(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context &&
      (fTree->contents.sons->next->tree->oper->rank == 292)) {
    /* RULE fullDecl(Decl,none()) -> 
              Decl  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 42)) {
    /* RULE fullDecl(Decl,pointer_initializer(Init)) -> 
              pointerAssign(Decl,Init)  */
    sendOp(157) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE fullDecl(Decl,Init) -> 
              assign(Decl,Init)  */
    sendOp(14) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT41(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE cm_initializer(Exp) -> 
              Exp  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT42(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE pointer_initializer(Init) -> 
              Init  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT43(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE arrayDecl(Ident,Dims) -> 
              arrayDeclarator(Ident,reverse::Dims)  */
    sendOp(11) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 5, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT44(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE sizedDecl(Decl,Size) -> 
              sizedDeclarator(Decl,Size)  */
    sendOp(180) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT45(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 292) &&
      (fTree->contents.sons->next->tree->oper->rank == 292)) {
    /* RULE dim(none(),none()) -> 
              dimColon(none(),none())  */
    sendOp(61) ;
    sendOp(143) ;
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 292) &&
      (fTree->contents.sons->next->tree->oper->rank == 231)) {
    /* RULE dim(none(),star()) -> 
              dimColon(intCst:"1",star())  */
    sendOp(61) ;
    sendOp(106) ;
    sendValue("1") ;
    sendOp(182) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 292)) {
    /* RULE dim(none(),Exp2) -> 
              dimColon(intCst:"1",Exp2)  */
    sendOp(61) ;
    sendOp(106) ;
    sendValue("1") ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 231)) {
    /* RULE dim(Exp1,star()) -> 
              dimColon(Exp1,star())  */
    sendOp(61) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(182) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE dim(Exp1,Exp2) -> 
              dimColon(Exp1,Exp2)  */
    sendOp(61) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT47(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 50)) {
    /* RULE externalDecl(idents(Ident)*) -> 
              external(Ident)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(76) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT48(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 50)) {
    /* RULE intrinsicDecl(idents(Ident)*) -> 
              intrinsic(Ident)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(111) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT49(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 50)) {
    /* RULE saveDecl(idents(Ident)*) -> 
              save(Ident)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(176) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT50(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE idents(Ident)* -> 
              expressions(Ident)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(73) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT51(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE equivalences(VarRefs)* -> 
              equivalence(VarRefs)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(71) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT53(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE common(Ident,Decls) -> 
              common(Ident,Decls)  */
    sendOp(40) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT54(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 55)) {
    /* RULE intentDecl(in(),Idents) -> 
              accessDecl(ident:"in",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("in") ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 56)) {
    /* RULE intentDecl(out(),Idents) -> 
              accessDecl(ident:"out",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("out") ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 57)) {
    /* RULE intentDecl(inout(),Idents) -> 
              accessDecl(ident:"inout",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("inout") ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT58(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 292) &&
      (fTree->contents.sons->next->next->next->tree->oper->rank == 304) &&
      (fTree->contents.sons->next->next->next->next->tree->oper->rank == 297) &&
      (listTreeLength(fTree->contents.sons->next->next->next->next->tree->contents.sons) == 0)) {
    /* RULE derivedDecl(Ident,none(),Pp,fieldDecls(Decl)*,decls(),Oo) -> 
              typeDeclaration(Ident,recordType(none(),modifiers[Pp],declarations(Decl)*))  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(199) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(166) ;
    sendOp(143) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, -1) ;
    endList() ;
    sendOp(54) ;
    listStars = fTree->contents.sons->next->next->next->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 291) &&
      (fTree->contents.sons->next->next->next->tree->oper->rank == 304) &&
      (fTree->contents.sons->next->next->next->next->tree->oper->rank == 297) &&
      (listTreeLength(fTree->contents.sons->next->next->next->next->tree->contents.sons) == 0)) {
    /* RULE derivedDecl(Ident,isAbstract(),Pp,fieldDecls(Decl)*,decls(),Oo) -> 
              typeDeclaration(Ident,recordType(none(),modifiers[(ident:"abstract"),Pp],declarations(Decl)*))  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(199) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(166) ;
    sendOp(143) ;
    sendOp(134) ;
    sendOp(98) ;
    sendValue("abstract") ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, -1) ;
    endList() ;
    sendOp(54) ;
    listStars = fTree->contents.sons->next->next->next->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 76) &&
      (fTree->contents.sons->next->next->next->tree->oper->rank == 304) &&
      (fTree->contents.sons->next->next->next->next->tree->oper->rank == 297) &&
      (listTreeLength(fTree->contents.sons->next->next->next->next->tree->contents.sons) == 0)) {
    /* RULE derivedDecl(Ident,X:=extends(BaseType,Exps),Pp,fieldDecls(Decl)*,decls(),Oo) -> 
              typeDeclaration(Ident,recordType(none(),modifiers(X),declarations(Decl)*))  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(199) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(166) ;
    sendOp(143) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    endList() ;
    sendOp(54) ;
    listStars = fTree->contents.sons->next->next->next->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->next->next->tree->oper->rank == 304) &&
      (fTree->contents.sons->next->next->next->next->tree->oper->rank == 297) &&
      (listTreeLength(fTree->contents.sons->next->next->next->next->tree->contents.sons) == 0)) {
    /* RULE derivedDecl(Ident,Aa,Pp,fieldDecls(Decl)*,decls(),Oo) -> 
              accessDecl(Aa,typeDeclaration(Ident,recordType(none(),modifiers[Pp],declarations(Decl)*)))  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(2) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(199) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(166) ;
    sendOp(143) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, -1) ;
    endList() ;
    sendOp(54) ;
    listStars = fTree->contents.sons->next->next->next->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 292)) {
    /* RULE derivedDecl(Ident,none(),Pp,FieldDecls,BoundProcDecls,Oo) -> 
              class(modifiers[Pp],Ident,parentClasses(),declarations[fuse::FieldDecls,fuse::BoundProcDecls])  */
    sendOp(36) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(153) ;
    endList() ;
    sendOp(54) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 291)) {
    /* RULE derivedDecl(Ident,isAbstract(),Pp,FieldDecls,BoundProcDecls,Oo) -> 
              class(modifiers[(ident:"abstract"),Pp],Ident,parentClasses(),declarations[fuse::FieldDecls,fuse::BoundProcDecls])  */
    sendOp(36) ;
    sendOp(134) ;
    sendOp(98) ;
    sendValue("abstract") ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(153) ;
    endList() ;
    sendOp(54) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 76)) {
    /* RULE derivedDecl(Ident,X:=extends(BaseType,Exps),Pp,FieldDecls,BoundProcDecls,Oo) -> 
              class(modifiers[Pp],Ident,parentClasses(parentClass(modifiers(),BaseType)),declarations[fuse::FieldDecls,fuse::BoundProcDecls])  */
    sendOp(36) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(153) ;
    sendOp(152) ;
    sendOp(134) ;
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
    endList() ;
    sendOp(54) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE derivedDecl(Ident,Aa,Pp,FieldDecls,BoundProcDecls,Oo) -> 
              class(modifiers[Pp],Ident,parentClasses(),declarations[fuse::FieldDecls,fuse::BoundProcDecls])  */
    sendOp(36) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(153) ;
    endList() ;
    sendOp(54) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 1, -1) ;
    f2ilTT(fTree->contents.sons->next->next->next->next->tree, 1, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT59(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isPublic() -> 
              ident:"public"  */
    sendOp(98) ;
    sendValue("public") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT60(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isPrivate() -> 
              ident:"private"  */
    sendOp(98) ;
    sendValue("private") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT62(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isSequence() -> 
              ident:"sequence"  */
    sendOp(98) ;
    sendValue("sequence") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT66(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 269)) {
    /* RULE interfaceDecl(definedOp N,Interfaces,OptExpression2) -> 
              interfaceDecl(binary(none(),ident!!lcident(:,:)(N),none()),Interfaces,OptExpression2)  */
    AtomValue userResValue1 ;
    sendOp(109) ;
    sendOp(17) ;
    sendOp(143) ;
    sendOp(98) ;
    userResValue1 = f2ilUlcident((AtomValue)(char *)fTree->contents.sons->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 291)) {
    /* RULE interfaceDecl(isAbstract(),Interfaces,OptExpression2) -> 
              interfaceDecl(abstract(),Interfaces,OptExpression2)  */
    sendOp(109) ;
    sendOp(1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE interfaceDecl(OptExpression1,Interfaces,OptExpression2) -> 
              interfaceDecl(OptExpression1,Interfaces,OptExpression2)  */
    sendOp(109) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT67(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 20)) {
    /* RULE interface(function(Type,Ident,Params,Prefix,OptIdent),Declarations,Trailer) -> 
              interface(function(modifiers[Prefix],Type,none(),Ident^{explicitReturnVar:OptIdent},Params,Declarations))  */
    sendOp(108) ;
    sendOp(91) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->next->next->tree, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    sendOp(143) ;
    sendAnnotation("explicitReturnVar") ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->next->next->next->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 19)) {
    /* RULE interface(subroutine(Prefixs,Ident,Params,OptBind),Declarations,Trailer) -> 
              interface(function(modifiers[Prefixs],void(),none(),Ident^{bind:OptBind},Params,Declarations))  */
    sendOp(108) ;
    sendOp(91) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, -1) ;
    endList() ;
    sendOp(211) ;
    sendOp(143) ;
    sendAnnotation("bind") ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->next->next->tree, 0, 1) ;
    acceptAnnotations() ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT68(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 50)) {
    /* RULE module_procedure(idents(Ident)*) -> 
              moduleProcedure(Ident)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(136) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT69(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE useDecl(Ident,Visibles) -> 
              useDecl(Ident,Visibles)  */
    sendOp(204) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT70(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE importDecl(Idents) -> 
              importDecl(Idents)  */
    sendOp(103) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT71(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE onlyVisibles(Visible)* -> 
              onlyVisibles(Visible)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(147) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT72(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE renamedVisibles(Visible)* -> 
              renamedVisibles(Visible)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(171) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT73(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE renamed(Ident1,Ident2) -> 
              renamed(Ident1,Ident2)  */
    sendOp(170) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT74(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE accessDecl(Access,Idents) -> 
              accessDecl(Access,Idents)  */
    sendOp(2) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT75(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 292)) {
    /* RULE bind(Bind,none()) -> 
              accessDecl(ident:"bind",expressions(Bind))  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("bind") ;
    sendOp(73) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE bind(Bind,Exps) -> 
              accessDecl(ident:"bind",expressions(Bind,Exps))  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("bind") ;
    sendOp(73) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT76(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE extends(Extended,OptExps) -> 
              extends(Extended,OptExps)  */
    sendOp(74) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT77(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE allocatableDecl(Idents) -> 
              accessDecl(ident:"allocatable",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("allocatable") ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT78(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE optionalDecl(Idents) -> 
              accessDecl(ident:"optional",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("optional") ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT79(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE pointerDecl(Idents) -> 
              accessDecl(ident:"pointer",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("pointer") ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT81(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == -1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 40) &&
      (fTree->contents.sons->next->tree->contents.sons->tree->oper->rank == 43) &&
      (fTree->contents.sons->next->tree->contents.sons->next->tree->oper->rank == 292)) {
    /* RULE pointerPair(Ident1,fullDecl(arrayDecl(Ident2,Dims),none())) -> 
              (varDimDeclaration(arrayDeclarator(Ident2,reverse::Dims)),varDeclaration(modifiers(),none(),declarators(pointerDeclarator(Ident1))),
assign(Ident1,address(Ident2)))  */
    sendOp(209) ;
    sendOp(11) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree->contents.sons->next->tree, 5, 1) ;
    endList() ;
    sendOp(206) ;
    sendOp(134) ;
    endList() ;
    sendOp(143) ;
    sendOp(55) ;
    sendOp(158) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    endList() ;
    sendOp(14) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(4) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree->contents.sons->tree, 0, 1) ;
  } else  if ((resultKind == -1) && !context) {
    /* RULE pointerPair(Ident,BasicDecl) -> 
              (varDeclaration(modifiers(),none(),declarators(pointerDeclarator(Ident))),assign(Ident,address(BasicDecl)))  */
    sendOp(206) ;
    sendOp(134) ;
    endList() ;
    sendOp(143) ;
    sendOp(55) ;
    sendOp(158) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    endList() ;
    sendOp(14) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(4) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT82(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE targetDecl(Idents) -> 
              accessDecl(ident:"target",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("target") ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT85(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE volatileDecl(Idents) -> 
              accessDecl(ident:"volatile",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("volatile") ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT86(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE contiguousDecl(Idents) -> 
              accessDecl(ident:"contiguous",Idents)  */
    sendOp(2) ;
    sendOp(98) ;
    sendValue("contiguous") ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT96(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE formatElem N -> 
              formatElem N  */
    sendOp(87) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT97(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE entryDecl(Ident,Params) -> 
              parsingError:"parsing error, entry ignored"  */
    sendOp(154) ;
    sendValue("parsing error, entry ignored") ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT98(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 308)) {
    /* RULE directiveDecl(Ident,dirArgs(DirArg)*) -> 
              directive(Ident,expressions(DirArg)*)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(63) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(73) ;
    listStars = fTree->contents.sons->next->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT100(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 103) &&
      (getAnnotationTree(fTree->contents.sons->tree, "preComments"))) {
    /* RULE ompParallelDo(ompClauses(DirArg)*^{preComments:C},Loop) -> 
              parallelRegion(ident:"OMP",expressions(DirArg)*,blockStatement(parallelLoop(ident:"OMP",expressions(),Loop)))^{preComments:C}  */
    ListTree *listStars ;
    int listStarsLength ;
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree->contents.sons->tree, "preComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    sendOp(150) ;
    sendOp(98) ;
    sendValue("OMP") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(27) ;
    sendOp(149) ;
    sendOp(98) ;
    sendValue("OMP") ;
    sendOp(73) ;
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    endList() ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 103)) {
    /* RULE ompParallelDo(ompClauses(DirArg)*,Loop) -> 
              parallelRegion(ident:"OMP",expressions(DirArg)*,blockStatement(parallelLoop(ident:"OMP",expressions(),Loop)))  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(150) ;
    sendOp(98) ;
    sendValue("OMP") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(27) ;
    sendOp(149) ;
    sendOp(98) ;
    sendValue("OMP") ;
    sendOp(73) ;
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT101(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 103) &&
      (getAnnotationTree(fTree->contents.sons->tree, "preComments"))) {
    /* RULE ompDo(ompClauses(DirArg)*^{preComments:C},Loop) -> 
              parallelLoop(ident:"OMP",expressions(DirArg)*,Loop)^{preComments:C}  */
    ListTree *listStars ;
    int listStarsLength ;
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree->contents.sons->tree, "preComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    sendOp(149) ;
    sendOp(98) ;
    sendValue("OMP") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 103)) {
    /* RULE ompDo(ompClauses(DirArg)*,Loop) -> 
              parallelLoop(ident:"OMP",expressions(DirArg)*,Loop)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(149) ;
    sendOp(98) ;
    sendValue("OMP") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT102(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 103) &&
      (getAnnotationTree(fTree->contents.sons->tree, "preComments"))) {
    /* RULE ompParallel(ompClauses(DirArg)*^{preComments:C},Stats) -> 
              parallelRegion(ident:"OMP",expressions(DirArg)*,Stats)^{preComments:C}  */
    ListTree *listStars ;
    int listStarsLength ;
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree->contents.sons->tree, "preComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    sendOp(150) ;
    sendOp(98) ;
    sendValue("OMP") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 103)) {
    /* RULE ompParallel(ompClauses(DirArg)*,Stats) -> 
              parallelRegion(ident:"OMP",expressions(DirArg)*,Stats)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(150) ;
    sendOp(98) ;
    sendValue("OMP") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT104(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompNumThreads(Exp) -> 
              numThreads(Exp)  */
    sendOp(146) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT105(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompSchedule(Exps) -> 
              schedule(Exps)  */
    sendOp(177) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT106(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompShared(Vars) -> 
              sharedVars(Vars)  */
    sendOp(179) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT107(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompPrivate(Vars) -> 
              privateVars(Vars)  */
    sendOp(162) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT108(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompFirstPrivate(Vars) -> 
              firstPrivateVars(Vars)  */
    sendOp(79) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT109(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompLastPrivate(Vars) -> 
              lastPrivateVars(Vars)  */
    sendOp(118) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT110(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompReduction(Op,Vars) -> 
              reductionVars(Op,Vars)  */
    sendOp(167) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT111(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompDefault(Attr) -> 
              defaultSharingAttribute(Attr)  */
    sendOp(57) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT112(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ompClauseArgs(ClauseArg)* -> 
              idents(ClauseArg)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(99) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT113(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE eqName(Ident,Exp) -> 
              nameEq(Ident,Exp)  */
    sendOp(139) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT115(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE namelist(Ident,Idents) -> 
              nameList(Ident,Idents)  */
    sendOp(140) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT116(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 1) &&
      (fTree->contents.sons->tree->oper->rank == 117)) {
    /* RULE datas(data(Datavars,Values)) -> 
              data(Datavars,Values)  */
    sendOp(52) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT117(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE data(Datavars,Values) -> 
              data(Datavars,Values)  */
    sendOp(52) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT118(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE impldo(Datavars,NameRange) -> 
              iterativeVariableRef(Datavars,NameRange)  */
    sendOp(113) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT119(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE repeat_factor(Exp1,Exp2) -> 
              repeatedValue(Exp1,Exp2)  */
    sendOp(172) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT121(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE pppLine(Arg)* -> 
              pppLine(Arg)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(161) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT122(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 299)) {
    /* RULE varDecl(Type,fullDecls(FullDecl)*) -> 
              varDeclaration(modifiers(),Type,declarators(FullDecl)*)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(206) ;
    sendOp(134) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(55) ;
    listStars = fTree->contents.sons->next->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT123(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE isDimension(Dims) -> 
              Dims  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT124(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isParameter() -> 
              ident:"constant"  */
    sendOp(98) ;
    sendValue("constant") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT125(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isSave() -> 
              ident:"save"  */
    sendOp(98) ;
    sendValue("save") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT126(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isData() -> 
              ident:"data"  */
    sendOp(98) ;
    sendValue("data") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT127(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 55)) {
    /* RULE isIntent(in()) -> 
              ident:"in"  */
    sendOp(98) ;
    sendValue("in") ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 56)) {
    /* RULE isIntent(out()) -> 
              ident:"out"  */
    sendOp(98) ;
    sendValue("out") ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 57)) {
    /* RULE isIntent(inout()) -> 
              ident:"inout"  */
    sendOp(98) ;
    sendValue("inout") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT128(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isAllocatable() -> 
              ident:"allocatable"  */
    sendOp(98) ;
    sendValue("allocatable") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT129(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isExternal() -> 
              ident:"external"  */
    sendOp(98) ;
    sendValue("external") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT130(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isIntrinsic() -> 
              ident:"intrinsic"  */
    sendOp(98) ;
    sendValue("intrinsic") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT131(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isOptional() -> 
              ident:"optional"  */
    sendOp(98) ;
    sendValue("optional") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT132(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isPointer() -> 
              ident:"pointer"  */
    sendOp(98) ;
    sendValue("pointer") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT133(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isTarget() -> 
              ident:"target"  */
    sendOp(98) ;
    sendValue("target") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT134(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isValue() -> 
              ident:"value"  */
    sendOp(98) ;
    sendValue("value") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT135(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isVolatile() -> 
              ident:"volatile"  */
    sendOp(98) ;
    sendValue("volatile") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT136(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isContiguous() -> 
              ident:"contiguous"  */
    sendOp(98) ;
    sendValue("contiguous") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT137(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isPass() -> 
              ident:"pass"  */
    sendOp(98) ;
    sendValue("pass") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT138(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isNopass() -> 
              ident:"nopass"  */
    sendOp(98) ;
    sendValue("nopass") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT139(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE isDeferred() -> 
              ident:"deferred"  */
    sendOp(98) ;
    sendValue("deferred") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT164(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 321) &&
      (listTreeLength(fTree->contents.sons->tree->contents.sons) >= 1) &&
      (fTree->contents.sons->next->tree->oper->rank == 95)) {
    /* RULE labstat(labels[(Label),Labels],formatDecl(FormatElems)) -> 
              format(Label,FormatElems)  */
    sendOp(86) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE labstat(Labels,Stat) -> 
              labelStatement(Labels,Stat)  */
    sendOp(115) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT165(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE assign(VarRef,Exp) -> 
              assign(VarRef,Exp)  */
    sendOp(14) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT166(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE pointer_assign(Ident,Exp) -> 
              pointerAssign(Ident,Exp)  */
    sendOp(157) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT167(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE assignto(Label,Ident) -> 
              assignLabelVar(Label,Ident)  */
    sendOp(15) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT168(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE continue() -> 
              continue()  */
    sendOp(50) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT169(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 242)) {
    /* RULE call(fieldRef(VarRef,Ident),Exps) -> 
              call(VarRef,Ident,Exps)  */
    sendOp(31) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE call(FuncExp,Exps) -> 
              call(none(),FuncExp,Exps)  */
    sendOp(31) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT170(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE arith_if(Exp,Label1,Label2,Label3) -> 
              if(lt(Exp,intCst:"0"),goto(Label1),if(eq(Exp,intCst:"0"),goto(Label2),goto(Label3)))  */
    sendOp(100) ;
    sendOp(126) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(106) ;
    sendValue("0") ;
    sendOp(95) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(100) ;
    sendOp(70) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(106) ;
    sendValue("0") ;
    sendOp(95) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(95) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT171(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE return(Exp) -> 
              return(none(),Exp)  */
    sendOp(173) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT172(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE stop(Exp) -> 
              stop()  */
    sendOp(184) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT173(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE pause(Exp1) -> 
              ioCall(ident:"pause",expressions(Exp1),expressions())  */
    sendOp(112) ;
    sendOp(98) ;
    sendValue("pause") ;
    sendOp(73) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT174(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE exit(OptIdent) -> 
              exit(OptIdent)  */
    sendOp(72) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT175(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE enddo(OptIdent) -> 
              none()  */
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT176(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE cycle(OptIdent) -> 
              cycle(OptIdent)  */
    sendOp(51) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT177(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 301) &&
      (listTreeLength(fTree->contents.sons->tree->contents.sons) == 1)) {
    /* RULE allocate(variableRefs(VarDecl),KeywordArg) -> 
              pointerAssign(nameOnly::VarDecl,allocate(none(),dimsOnly::VarDecl,none(),KeywordArg,expressions()))  */
    sendOp(157) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->tree, 9, 1) ;
    sendOp(5) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->tree, 10, 1) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(73) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 301)) {
    /* RULE allocate(variableRefs(VarDecl)*,KeywordArg) -> 
              blockStatement[(pointerAssign(nameOnly::VarDecl,allocate(none(),dimsOnly::VarDecl,none(),KeywordArg,expressions())))*]  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(27) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    sendOp(157) ;
    f2ilTT(listStars->tree, 9, 1) ;
    sendOp(5) ;
    sendOp(143) ;
    f2ilTT(listStars->tree, 10, 1) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(73) ;
    endList() ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT178(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 301) &&
      (listTreeLength(fTree->contents.sons->tree->contents.sons) == 1)) {
    /* RULE deallocate(variableRefs(Ident),KeywordArg) -> 
              deallocate(Ident,KeywordArg)  */
    sendOp(53) ;
    f2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 301)) {
    /* RULE deallocate(variableRefs(Ident)*,KeywordArg) -> 
              blockStatement[(deallocate(Ident,KeywordArg))*]  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(27) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    sendOp(53) ;
    f2ilTT(listStars->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT179(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 301)) {
    /* RULE nullify(variableRefs(Exp)*) -> 
              nullify(expressions(Exp)*)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(145) ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT180(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE where(Exp,Stat) -> 
              where(Exp,Stat,none(),none())  */
    sendOp(212) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(143) ;
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT181(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE forall(NameRanges,OptExp,Stat) -> 
              loop(none(),none(),forall(NameRanges,OptExp),Stat)  */
    sendOp(125) ;
    sendOp(143) ;
    sendOp(143) ;
    sendOp(82) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT184(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE goto(Label) -> 
              goto(Label)  */
    sendOp(95) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT185(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE assigned_goto(Ident,Labels) -> 
              gotoLabelVar(Ident,Labels)  */
    sendOp(96) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT186(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE comp_goto(Labels,Exp) -> 
              compGoto(Labels,Exp)  */
    sendOp(41) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT187(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE log_if(Test,Then) -> 
              if(Test,Then,none())  */
    sendOp(100) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT191(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE open(ioSpecs(Exp1)*) -> 
              ioCall(ident:"open",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("open") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT192(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE close(ioSpecs(Exp1)*) -> 
              ioCall(ident:"close",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("close") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT193(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE flush(ioSpecs(Exp1)*) -> 
              ioCall(ident:"flush",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("flush") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT194(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE inquire(ioSpecs(Exp1)*,Exp2) -> 
              ioCall(ident:"inquire",expressions(Exp1)*,Exp2)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("inquire") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT195(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE backspace(ioSpecs(Exp1)*) -> 
              ioCall(ident:"backspace",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("backspace") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT196(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE endFile(ioSpecs(Exp1)*) -> 
              ioCall(ident:"endfile",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("endfile") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT197(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE rewind(ioSpecs(Exp1)*) -> 
              ioCall(ident:"rewind",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("rewind") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT198(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE print(Format,Exps) -> 
              ioCall(ident:"print",expressions(none(),Format),Exps)  */
    sendOp(112) ;
    sendOp(98) ;
    sendValue("print") ;
    sendOp(73) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT199(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE read(ioSpecs(Exp1)*,Exp2) -> 
              ioCall(ident:"read",expressions(Exp1)*,Exp2)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("read") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE read(Format,Exp2) -> 
              ioCall(ident:"read",expressions(none(),Format),Exp2)  */
    sendOp(112) ;
    sendOp(98) ;
    sendValue("read") ;
    sendOp(73) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT200(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE write(ioSpecs(Exp1)*,Exp2) -> 
              ioCall(ident:"write",expressions(Exp1)*,Exp2)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("write") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT202(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE encode(ioSpecs(Exp1)*,Exp2) -> 
              ioCall(ident:"encode",expressions(Exp1)*,Exp2)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("encode") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT203(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE decode(ioSpecs(Exp1)*,Exp2) -> 
              ioCall(ident:"decode",expressions(Exp1)*,Exp2)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("decode") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT204(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE delete(ioSpecs(Exp1)*) -> 
              ioCall(ident:"delete",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("delete") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT205(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE find(ioSpecs(Exp1)*) -> 
              ioCall(ident:"find",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("find") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT206(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE rewrite(ioSpecs(Exp1)*,Exp2) -> 
              ioCall(ident:"rewrite",expressions(Exp1)*,Exp2)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("rewrite") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT207(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE unlock(ioSpecs(Exp1)*) -> 
              ioCall(ident:"unlock",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("unlock") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT208(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE backFile(ioSpecs(Exp1)*) -> 
              ioCall(ident:"backfile",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("backfile") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT209(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 320)) {
    /* RULE skipFile(ioSpecs(Exp1)*) -> 
              ioCall(ident:"skipfile",expressions(Exp1)*,expressions())  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(112) ;
    sendOp(98) ;
    sendValue("skipfile") ;
    sendOp(73) ;
    listStars = fTree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT214(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE struct_if(Ident,Test,Then,Else) -> 
              if(Test,Then,Else)  */
    sendOp(100) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT215(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE struct_where(Ident,Test,Then,Else) -> 
              where(Test,Then,Else,Ident)  */
    sendOp(212) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT216(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE struct_forall(OptIdent,NameRanges,OptExp,Stats) -> 
              loop(OptIdent,none(),forall(NameRanges,OptExp),Stats)  */
    sendOp(125) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(143) ;
    sendOp(82) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT217(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE select_case(Ident1,Exp,Cases,Ident2) -> 
              switch(Exp,Cases)  */
    sendOp(189) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT218(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE select_type(Ident1,Exp,Cases,Ident2) -> 
              switchType(Exp,Cases)  */
    sendOp(192) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT219(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE do(Ident,Label,Control,Stats) -> 
              loop(Ident,Label,Control,Stats)  */
    sendOp(125) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT220(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 11) &&
      (fTree->contents.sons->next->tree->oper->rank == 221)) {
    /* RULE forallR::nameRange(Ident,range(Exp1,Exp2,Exp3)) -> 
              forallRangeTriplet(Ident,Exp1,Exp2,Exp3)  */
    sendOp(84) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 221)) {
    /* RULE nameRange(Ident,range(Exp1,Exp2,Exp3)) -> 
              do(Ident,Exp1,Exp2,Exp3)  */
    sendOp(66) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT221(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE range(Exp1,Exp2,Exp3) -> 
              arrayTriplet(Exp1,Exp2,Exp3)  */
    sendOp(12) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT222(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE case(Exps,Ident,Stat) -> 
              switchCase(Exps,inSwitch::Stat)  */
    sendOp(190) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 12, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT223(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE default() -> 
              expressions()  */
    sendOp(73) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT224(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE typeCase(Type,Ident,Stat) -> 
              typeCase(Type,inSwitch::Stat)  */
    sendOp(198) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 12, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT225(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 292)) {
    /* RULE classCase(none(),Ident,Stat) -> 
              typeCase(none(),inSwitch::Stat)  */
    sendOp(198) ;
    sendOp(143) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 12, 1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE classCase(Type,Ident,Stat) -> 
              typeCase(classType(Type),inSwitch::Stat)  */
    sendOp(198) ;
    sendOp(37) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 12, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT226(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE while(Exp) -> 
              while(Exp)  */
    sendOp(213) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT227(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE until(Exp) -> 
              until(Exp)  */
    sendOp(203) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT228(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE times(Exp) -> 
              times(Exp)  */
    sendOp(195) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT229(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE label X -> 
              label X  */
    sendOp(114) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT231(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE star() -> 
              star()  */
    sendOp(182) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT232(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE complexConstructor(Exp1,Exp2) -> 
              complexConstructor(Exp1,Exp2)  */
    sendOp(43) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT233(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE logicCst N -> 
              boolCst N  */
    sendOp(28) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT234(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE intCst N -> 
              intCst N  */
    sendOp(106) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT235(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE realCst N -> 
              realCst N  */
    sendOp(165) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT236(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE doubleCst N -> 
              realCst N  */
    sendOp(165) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT237(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE bitCst N -> 
              bitCst N  */
    sendOp(20) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT238(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 235) &&
      (fTree->contents.sons->next->tree->oper->rank == 244)) {
    /* RULE kindCst(realCst N,ident Ext) -> 
              realCst!!concatU(:,:,:)(N,Ext)  */
    AtomValue userResValue1 ;
    sendOp(165) ;
    userResValue1 = f2ilUconcatU((AtomValue)(char *)fTree->contents.sons->tree->contents.value,
      (AtomValue)(char *)fTree->contents.sons->next->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 235) &&
      (fTree->contents.sons->next->tree->oper->rank == 234)) {
    /* RULE kindCst(realCst N,intCst Ext) -> 
              realCst!!concatU(:,:,:)(N,Ext)  */
    AtomValue userResValue1 ;
    sendOp(165) ;
    userResValue1 = f2ilUconcatU((AtomValue)(char *)fTree->contents.sons->tree->contents.value,
      (AtomValue)(char *)fTree->contents.sons->next->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 236) &&
      (fTree->contents.sons->next->tree->oper->rank == 244)) {
    /* RULE kindCst(doubleCst N,ident Ext) -> 
              realCst!!concatU(:,:,:)(N,Ext)  */
    AtomValue userResValue1 ;
    sendOp(165) ;
    userResValue1 = f2ilUconcatU((AtomValue)(char *)fTree->contents.sons->tree->contents.value,
      (AtomValue)(char *)fTree->contents.sons->next->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 236) &&
      (fTree->contents.sons->next->tree->oper->rank == 234)) {
    /* RULE kindCst(doubleCst N,intCst Ext) -> 
              realCst!!concatU(:,:,:)(N,Ext)  */
    AtomValue userResValue1 ;
    sendOp(165) ;
    userResValue1 = f2ilUconcatU((AtomValue)(char *)fTree->contents.sons->tree->contents.value,
      (AtomValue)(char *)fTree->contents.sons->next->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 234) &&
      (fTree->contents.sons->next->tree->oper->rank == 244)) {
    /* RULE kindCst(intCst N,ident Ext) -> 
              realCst!!concatU(:,:,:)(N,Ext)  */
    AtomValue userResValue1 ;
    sendOp(165) ;
    userResValue1 = f2ilUconcatU((AtomValue)(char *)fTree->contents.sons->tree->contents.value,
      (AtomValue)(char *)fTree->contents.sons->next->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 234) &&
      (fTree->contents.sons->next->tree->oper->rank == 234)) {
    /* RULE kindCst(intCst N,intCst Ext) -> 
              realCst!!concatU(:,:,:)(N,Ext)  */
    AtomValue userResValue1 ;
    sendOp(165) ;
    userResValue1 = f2ilUconcatU((AtomValue)(char *)fTree->contents.sons->tree->contents.value,
      (AtomValue)(char *)fTree->contents.sons->next->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT239(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE hollerith N -> 
              formatElem N  */
    sendOp(87) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT240(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context &&
      (listTreeLength(fTree->contents.sons) == 1)) {
    /* RULE strings(Str) -> 
              Str  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE strings(Str)* -> 
              strings(Str)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(186) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT241(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE string N -> 
              stringCst N  */
    sendOp(185) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT242(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE fieldRef(VarRef,Ident) -> 
              fieldAccess(VarRef,Ident)  */
    sendOp(77) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT243(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 244) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"selected_int_kind") == 0)) {
    /* RULE vardim(ident:"selected_int_kind",Exps) -> 
              call(none(),ident:"selected_int_kind",Exps)  */
    sendOp(31) ;
    sendOp(143) ;
    sendOp(98) ;
    sendValue("selected_int_kind") ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 244) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"selected_real_kind") == 0)) {
    /* RULE vardim(ident:"selected_real_kind",Exps) -> 
              call(none(),ident:"selected_real_kind",Exps)  */
    sendOp(31) ;
    sendOp(143) ;
    sendOp(98) ;
    sendValue("selected_real_kind") ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 318)) {
    /* RULE vardim(Ident,exps Exps) -> 
              arrayAccess(Ident,expressions reverse::Exps)  */
    sendOp(9) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(73) ;
    f2ilTTList(fTree->contents.sons->next->tree->contents.sons, 5, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::vardim(V,D) -> 
              V  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((context == 10)) {
    /* RULE dimsOnly::vardim(V,D) -> 
              reverse::D  */
    f2ilTT(fTree->contents.sons->next->tree, 5, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT244(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 2)) {
    /* RULE blockdata::ident Ident -> 
              ident!!blockdata(:,:)(Ident)  */
    AtomValue userResValue1 ;
    sendOp(98) ;
    userResValue1 = f2ilUblockdata((AtomValue)(char *)fTree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ident X -> 
              ident!!lcident(:,:)(X)  */
    AtomValue userResValue1 ;
    sendOp(98) ;
    userResValue1 = f2ilUlcident((AtomValue)(char *)fTree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT245(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE blockName X -> 
              ident!!lcident(:,:)(X)  */
    AtomValue userResValue1 ;
    sendOp(98) ;
    userResValue1 = f2ilUlcident((AtomValue)(char *)fTree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT247(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE add(Exp1,Exp2) -> 
              add(Exp1,Exp2)  */
    sendOp(3) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT248(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE sub(Exp1,Exp2) -> 
              sub(Exp1,Exp2)  */
    sendOp(187) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT249(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE mul(Exp1,Exp2) -> 
              mul(Exp1,Exp2)  */
    sendOp(137) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT250(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE div(Exp1,Exp2) -> 
              div(Exp1,Exp2)  */
    sendOp(64) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT251(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE power(Exp1,Exp2) -> 
              power(Exp1,Exp2)  */
    sendOp(160) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT252(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE plus(Exp1) -> 
              Exp1  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT253(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE minus(Exp1) -> 
              minus(Exp1)  */
    sendOp(128) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT254(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE concat(Exp1,Exp2) -> 
              concat(Exp1,Exp2)  */
    sendOp(44) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT255(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE not(Exp1) -> 
              not(Exp1)  */
    sendOp(144) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT256(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE and(Exp1,Exp2) -> 
              and(Exp1,Exp2)  */
    sendOp(6) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT257(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE or(Exp1,Exp2) -> 
              or(Exp1,Exp2)  */
    sendOp(148) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT258(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE xor(Exp1,Exp2) -> 
              xor(Exp1,Exp2)  */
    sendOp(214) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT259(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE eqv(Exp1,Exp2) -> 
              eq(Exp1,Exp2)  */
    sendOp(70) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT260(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE neqv(Exp1,Exp2) -> 
              neq(Exp1,Exp2)  */
    sendOp(142) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT261(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE eq(Exp1,Exp2) -> 
              eq(Exp1,Exp2)  */
    sendOp(70) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT262(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ge(Exp1,Exp2) -> 
              ge(Exp1,Exp2)  */
    sendOp(94) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT263(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE gt(Exp1,Exp2) -> 
              gt(Exp1,Exp2)  */
    sendOp(97) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT264(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE le(Exp1,Exp2) -> 
              le(Exp1,Exp2)  */
    sendOp(119) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT265(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE lt(Exp1,Exp2) -> 
              lt(Exp1,Exp2)  */
    sendOp(126) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT266(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ne(Exp1,Exp2) -> 
              neq(Exp1,Exp2)  */
    sendOp(142) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT267(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 269)) {
    /* RULE unaryDefOp(definedOp N,Exp) -> 
              unary(ident!!lcident(:,:)(N),Exp)  */
    AtomValue userResValue1 ;
    sendOp(201) ;
    sendOp(98) ;
    userResValue1 = f2ilUlcident((AtomValue)(char *)fTree->contents.sons->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT268(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 269)) {
    /* RULE binaryDefOp(definedOp N,Exp1,Exp2) -> 
              binary(Exp1,ident!!lcident(:,:)(N),Exp2)  */
    AtomValue userResValue1 ;
    sendOp(17) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(98) ;
    userResValue1 = f2ilUlcident((AtomValue)(char *)fTree->contents.sons->tree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
    f2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT269(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE definedOp N -> 
              binary(none(),ident!!lcident(:,:)(N),none())  */
    AtomValue userResValue1 ;
    sendOp(17) ;
    sendOp(143) ;
    sendOp(98) ;
    userResValue1 = f2ilUlcident((AtomValue)(char *)fTree->contents.value) ;
    sendValue(userResValue1) ;
    if (stack == NULL) free(userResValue1) ;
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT270(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE parenth(Exp1) -> 
              Exp1  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT272(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 310)) {
    /* RULE constructor(datavars Datavars) -> 
              arrayConstructor Datavars  */
    sendOp(10) ;
    f2ilTTList(fTree->contents.sons->tree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 318)) {
    /* RULE constructor(exps Exps) -> 
              arrayConstructor Exps  */
    sendOp(10) ;
    f2ilTTList(fTree->contents.sons->tree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT273(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE left_range(Range) -> 
              Range  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT274(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE right_range(Range) -> 
              Range  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT275(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE left_right_range(Range) -> 
              Range  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT276(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 281) &&
      (fTree->contents.sons->next->tree->oper->rank == 231)) {
    /* RULE sizedType(character(),star()) -> 
              arrayType(character(),dimColons(dimColon(intCst:"1",star())))  */
    sendOp(13) ;
    sendOp(35) ;
    sendOp(62) ;
    sendOp(61) ;
    sendOp(106) ;
    sendValue("1") ;
    sendOp(182) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 281) &&
      (fTree->contents.sons->next->tree->oper->rank == 318) &&
      (listTreeLength(fTree->contents.sons->next->tree->contents.sons) == 1)) {
    /* RULE sizedType(character(),exps(Size)) -> 
              arrayType(character(),dimColons(dimColon(intCst:"1",Size)))  */
    sendOp(13) ;
    sendOp(35) ;
    sendOp(62) ;
    sendOp(61) ;
    sendOp(106) ;
    sendValue("1") ;
    f2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 281)) {
    /* RULE sizedType(character(),Size) -> 
              arrayType(character(),dimColons(dimColon(intCst:"1",Size)))  */
    sendOp(13) ;
    sendOp(35) ;
    sendOp(62) ;
    sendOp(61) ;
    sendOp(106) ;
    sendValue("1") ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    endList() ;
  } else  if (!context &&
      (fTree->contents.sons->next->tree->oper->rank == 292)) {
    /* RULE sizedType(GroundType,none()) -> 
              GroundType  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 318)) {
    /* RULE sizedType(GroundType,exps Exps) -> 
              modifiedType(modifiers Exps,GroundType)  */
    sendOp(133) ;
    sendOp(134) ;
    f2ilTTList(fTree->contents.sons->next->tree->contents.sons, 0, -1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE sizedType(GroundType,Size) -> 
              modifiedType(modifiers(Size),GroundType)  */
    sendOp(133) ;
    sendOp(134) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    endList() ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT277(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE modifiedType(Type,TypeSpecModifiers) -> 
              modifiedType(TypeSpecModifiers,Type)  */
    sendOp(133) ;
    f2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT278(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE derived(TypeName) -> 
              TypeName  */
    f2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT279(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE class(ClassName) -> 
              classType(ClassName)  */
    sendOp(37) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT280(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE procedure(ProcedureInterfaceName) -> 
              procedureInterfaceName(ProcedureInterfaceName)  */
    sendOp(163) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT281(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE character() -> 
              character()  */
    sendOp(35) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT282(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE integer() -> 
              integer()  */
    sendOp(107) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT283(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE real() -> 
              float()  */
    sendOp(80) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT284(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE double() -> 
              modifiedType(modifiers(ident:"double"),float())  */
    sendOp(133) ;
    sendOp(134) ;
    sendOp(98) ;
    sendValue("double") ;
    endList() ;
    sendOp(80) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT285(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE complex() -> 
              complex()  */
    sendOp(42) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT286(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE dblcomplex() -> 
              modifiedType(modifiers(ident:"double"),complex())  */
    sendOp(133) ;
    sendOp(134) ;
    sendOp(98) ;
    sendValue("double") ;
    endList() ;
    sendOp(42) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT287(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE logical() -> 
              boolean()  */
    sendOp(29) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT288(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE byte() -> 
              ident:"byte"  */
    sendOp(98) ;
    sendValue("byte") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT289(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE bit() -> 
              ident:"FFbit"  */
    sendOp(98) ;
    sendValue("FFbit") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT290(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE void() -> 
              void()  */
    sendOp(211) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT292(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 2)) {
    /* RULE blockdata::none() -> 
              ident:"_blockdata_"  */
    sendOp(98) ;
    sendValue("_blockdata_") ;
  } else  if ((resultKind == -1) && (context == 1)) {
    /* RULE fuse::none() -> 
              []  */
  } else  if ((resultKind == 1) && !context) {
    /* RULE none() -> 
              none()  */
    sendOp(143) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT293(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == -1) && (context == 1) &&
      (listTreeLength(fTree->contents.sons) == 0) &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE fuse::units()^{postComments:C} -> 
              [(none()^{preComments:C})]  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree, "postComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    sendOp(143) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((context == 1)) {
    /* RULE fuse::units Units -> 
              Units  */
    f2ilTTList(fTree->contents.sons, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE units(Unit)* -> 
              file(Unit)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(78) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((resultKind == -1) && (context == 1)) {
    /* RULE fuse::units(Unit)* -> 
              (Unit)*  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT294(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == -1) && !context) {
    /* RULE options X -> 
              []  */
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT295(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 0)) {
    /* RULE params() -> 
              varDeclarations()  */
    sendOp(207) ;
    endList() ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE params(Param)* -> 
              varDeclarations(varDeclaration(modifiers(),none(),declarators(Param)*))  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(207) ;
    sendOp(206) ;
    sendOp(134) ;
    endList() ;
    sendOp(143) ;
    sendOp(55) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT296(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == -1) && !context) {
    /* RULE prefixs(Prefix)* -> 
              (Prefix)*  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT297(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == -1) && (context == 1) &&
      (listTreeLength(fTree->contents.sons) == 0) &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE fuse::decls()^{postComments:C} -> 
              [(none()^{preComments:C})]  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree, "postComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    sendOp(143) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((context == 1)) {
    /* RULE fuse::decls Decls -> 
              Decls  */
    f2ilTTList(fTree->contents.sons, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE decls Decls -> 
              blockStatement Decls  */
    sendOp(27) ;
    f2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE decls(Decl)* -> 
              blockStatement(Decl)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(27) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT298(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE letterss(Letters)* -> 
              letterss(Letters)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(124) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT299(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE fullDecls(Declarator)* -> 
              declarators(Declarator)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(55) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT300(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE dims LDims -> 
              dimColons reverse::LDims  */
    sendOp(62) ;
    f2ilTTList(fTree->contents.sons, 5, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && (context == 5)) {
    /* RULE reverse::dims LDims -> 
              dimColons reverse::LDims  */
    sendOp(62) ;
    f2ilTTList(fTree->contents.sons, 5, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT301(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE variableRefs(VarRef)* -> 
              expressions(VarRef)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(73) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT302(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE basicDecls(Declarator)* -> 
              varDeclarators(Declarator)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(208) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT303(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE privateSequences P -> 
              P  */
    f2ilTTList(fTree->contents.sons, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT304(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == -1) && (context == 1) &&
      (listTreeLength(fTree->contents.sons) == 0) &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE fuse::fieldDecls()^{postComments:C} -> 
              [(none()^{preComments:C})]  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree, "postComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    sendOp(143) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((context == 1)) {
    /* RULE fuse::fieldDecls Decls -> 
              Decls  */
    f2ilTTList(fTree->contents.sons, 0, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT306(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE interfaces(Interface)* -> 
              interfaces(Interface)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(110) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT307(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE formatElems Exps -> 
              expressions Exps  */
    sendOp(73) ;
    f2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT309(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 272) &&
      (fTree->contents.sons->next->tree->contents.sons->tree->oper->rank == 318)) {
    /* RULE multiCast(Type,constructor(exps Exps)) -> 
              multiCast(Type,arrayConstructor Exps)  */
    sendOp(138) ;
    f2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(10) ;
    f2ilTTList(fTree->contents.sons->next->tree->contents.sons->tree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT310(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE datavars(Datavar)* -> 
              expressions(Datavar)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(73) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT311(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE values(Value)* -> 
              expressions(Value)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(73) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT312(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 0)) {
    /* RULE typeSpecModifiers[] -> 
              none()  */
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE typeSpecModifiers(TypeSpecModifier)* -> 
              modifiers(TypeSpecModifier)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(134) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT315(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == -1) && (context == 1) &&
      (listTreeLength(fTree->contents.sons) == 0) &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE fuse::stats()^{postComments:C} -> 
              [(none()^{preComments:C})]  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree, "postComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    sendOp(143) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((context == 1)) {
    /* RULE fuse::stats Stats -> 
              Stats  */
    f2ilTTList(fTree->contents.sons, 0, resultKind) ;
  } else  if ((resultKind == 1) && (context == 12) &&
      (listTreeLength(fTree->contents.sons) == 0) &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE inSwitch::stats()^{postComments:C} -> 
              blockStatement[(none()^{preComments:C},break())]  */
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(fTree, "postComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    sendOp(27) ;
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    sendOp(143) ;
    sendOp(30) ;
    endList() ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == 1) && (context == 12)) {
    /* RULE inSwitch::stats Stats -> 
              blockStatement[Stats,(break())]  */
    sendOp(27) ;
    f2ilTTList(fTree->contents.sons, 0, -1) ;
    sendOp(30) ;
    endList() ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE stats Stats -> 
              blockStatement Stats  */
    sendOp(27) ;
    f2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE stats(Stat)* -> 
              blockStatement(Stat)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(27) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT316(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE nameRanges(NameRange)* -> 
              forallRanges(forallR::NameRange)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(85) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 11, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT317(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE cases(Case)* -> 
              switchCases(Case)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(191) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT318(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 5)) {
    /* RULE reverse::exps LDims -> 
              expressions reverse::LDims  */
    sendOp(73) ;
    f2ilTTList(fTree->contents.sons, 5, -1) ;
    endList() ;
  } else  if ((context == 1)) {
    /* RULE fuse::exps Exps -> 
              Exps  */
    f2ilTTList(fTree->contents.sons, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE exps Exps -> 
              expressions Exps  */
    sendOp(73) ;
    f2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT320(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE ioSpecs Exps -> 
              expressions Exps  */
    sendOp(73) ;
    f2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT321(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE labels(Label)* -> 
              labels(Label)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(116) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT323(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE comment T -> 
              stringCst T  */
    sendOp(185) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTT324(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE comment_s(Comment)* -> 
              comments(Comment)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(38) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTTXTree(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "preComments"))) {
    /* RULE T^{preComments:C} -> 
              T^{preComments:C}  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "postComments"))) {
    /* RULE T^{postComments:C} -> 
              T^{postComments:C}  */
    action2(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (getAnnotationTree(fTree, "line"))) {
    /* RULE T^{line:C} -> 
              T^{line:C}  */
    action3(fTree, resultKind) ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    action4(fTree, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    action5(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void f2ilTTList(ListTree *listTree, int context, short resultKind) {
  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 52)) {
    /* RULE [(commons(Common)*),Decls] -> 
              [(Common)*,Decls]  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = listTree->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 114)) {
    /* RULE [(namelists(Namelist)*),Decls] -> 
              [(Namelist)*,Decls]  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = listTree->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 33)) {
    /* RULE [(implicits Implicits),Decls] -> 
              [impldecl::Implicits,Decls]  */
    f2ilTTList(listTree->tree->contents.sons, 3, -1) ;
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 116)) {
    /* RULE [(datas(Data)*),Decls] -> 
              [(Data)*,Decls]  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = listTree->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 80)) {
    /* RULE [(pointerPairs PointerPairs),Decls] -> 
              [pointer::PointerPairs,Decls]  */
    f2ilTTList(listTree->tree->contents.sons, 4, -1) ;
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && (context == 4) &&
      (listTreeLength(listTree) >= 1)) {
    /* RULE pointer::[(X),PointerPairs] -> 
              [X,pointer::PointerPairs]  */
    f2ilTT(listTree->tree, 0, -1) ;
    f2ilTTList(listTree->next, 4, -1) ;
  } else  if ((resultKind == -1) && (context == 4) &&
      (listTreeLength(listTree) == 0)) {
    /* RULE pointer::[] -> 
              []  */
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 120) &&
      (listTree->tree->contents.sons->tree->oper->rank == 240)) {
    /* RULE [(includeDecl(strings Strings)),Decls] -> 
              [beginInclude::Strings,inInclude::Strings,endInclude::Strings,Decls]  */
    f2ilTTList(listTree->tree->contents.sons->tree->contents.sons, 6, -1) ;
    f2ilTTList(listTree->tree->contents.sons->tree->contents.sons, 7, -1) ;
    f2ilTTList(listTree->tree->contents.sons->tree->contents.sons, 8, -1) ;
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 240)) {
    /* RULE [(strings(String)*),Exps] -> 
              [(String)*,Exps]  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = listTree->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 177) &&
      (listTree->tree->contents.sons->tree->oper->rank == 301) &&
      (getAnnotationTree(listTree->tree, "preComments"))) {
    /* RULE [(allocate(variableRefs(VarDecl)*,KeywordArg)^{preComments:C}),Exps] -> 
              [(pointerAssign(nameOnly::VarDecl,allocate(none(),dimsOnly::VarDecl,none(),KeywordArg,expressions()))^{preComments:C})*,Exps]  */
    ListTree *listStars ;
    int listStarsLength ;
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(listTree->tree, "preComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    listStars = listTree->tree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    if (!annotOnce0) {
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    annotOnce0 = 1 ;
    }
    sendOp(157) ;
    f2ilTT(listStars->tree, 9, 1) ;
    sendOp(5) ;
    sendOp(143) ;
    f2ilTT(listStars->tree, 10, 1) ;
    sendOp(143) ;
    f2ilTT(listTree->tree->contents.sons->next->tree, 0, 1) ;
    sendOp(73) ;
    endList() ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 177) &&
      (listTree->tree->contents.sons->tree->oper->rank == 301) &&
      (getAnnotationTree(listTree->tree, "postComments"))) {
    /* RULE [(allocate(variableRefs(VarDecl)*,KeywordArg)^{postComments:C}),Exps] -> 
              [(pointerAssign(nameOnly::VarDecl,allocate(none(),dimsOnly::VarDecl,none(),KeywordArg,expressions()))^{postComments:C})*,Exps]  */
    ListTree *listStars ;
    int listStarsLength ;
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(listTree->tree, "postComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    listStars = listTree->tree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    if (!annotOnce0) {
    sendAnnotation("postComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    annotOnce0 = 1 ;
    }
    sendOp(157) ;
    f2ilTT(listStars->tree, 9, 1) ;
    sendOp(5) ;
    sendOp(143) ;
    f2ilTT(listStars->tree, 10, 1) ;
    sendOp(143) ;
    f2ilTT(listTree->tree->contents.sons->next->tree, 0, 1) ;
    sendOp(73) ;
    endList() ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 177) &&
      (listTree->tree->contents.sons->tree->oper->rank == 301)) {
    /* RULE [(allocate(variableRefs(VarDecl)*,KeywordArg)),Exps] -> 
              [(pointerAssign(nameOnly::VarDecl,allocate(none(),dimsOnly::VarDecl,none(),KeywordArg,expressions())))*,Exps]  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = listTree->tree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    sendOp(157) ;
    f2ilTT(listStars->tree, 9, 1) ;
    sendOp(5) ;
    sendOp(143) ;
    f2ilTT(listStars->tree, 10, 1) ;
    sendOp(143) ;
    f2ilTT(listTree->tree->contents.sons->next->tree, 0, 1) ;
    sendOp(73) ;
    endList() ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 178) &&
      (listTree->tree->contents.sons->tree->oper->rank == 301) &&
      (getAnnotationTree(listTree->tree, "preComments"))) {
    /* RULE [(deallocate(variableRefs(Ident)*,KeywordArg)^{preComments:C}),Exps] -> 
              [(deallocate(Ident,KeywordArg)^{preComments:C})*,Exps]  */
    ListTree *listStars ;
    int listStarsLength ;
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(listTree->tree, "preComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    listStars = listTree->tree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    if (!annotOnce0) {
    sendAnnotation("preComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    annotOnce0 = 1 ;
    }
    sendOp(53) ;
    f2ilTT(listStars->tree, 0, 1) ;
    f2ilTT(listTree->tree->contents.sons->next->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 178) &&
      (listTree->tree->contents.sons->tree->oper->rank == 301) &&
      (getAnnotationTree(listTree->tree, "postComments"))) {
    /* RULE [(deallocate(variableRefs(Ident)*,KeywordArg)^{postComments:C}),Exps] -> 
              [(deallocate(Ident,KeywordArg)^{postComments:C})*,Exps]  */
    ListTree *listStars ;
    int listStarsLength ;
    KeyListTree *annotCutter0 ;
    char *annotKey0 ;
    int annotOnce0 = 0 ;
    annotCutter0 = getAnnotationPlace(listTree->tree, "postComments") ;
    annotKey0 = annotCutter0->key ;
    annotCutter0->key = "" ;
    listStars = listTree->tree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    if (!annotOnce0) {
    sendAnnotation("postComments") ;
    f2ilTT(annotCutter0->tree, 0, 1) ;
    acceptAnnotations() ;
    annotOnce0 = 1 ;
    }
    sendOp(53) ;
    f2ilTT(listStars->tree, 0, 1) ;
    f2ilTT(listTree->tree->contents.sons->next->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
    annotCutter0->key = annotKey0 ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 178) &&
      (listTree->tree->contents.sons->tree->oper->rank == 301)) {
    /* RULE [(deallocate(variableRefs(Ident)*,KeywordArg)),Exps] -> 
              [(deallocate(Ident,KeywordArg))*,Exps]  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = listTree->tree->contents.sons->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    sendOp(53) ;
    f2ilTT(listStars->tree, 0, 1) ;
    f2ilTT(listTree->tree->contents.sons->next->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1)) {
    /* RULE [(Elem),List] -> 
              [(Elem),List]  */
    f2ilTT(listTree->tree, 0, 1) ;
    f2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) == 0)) {
    /* RULE [] -> 
              []  */
  } else  if ((resultKind == -1) && (context == 5) &&
      (listTreeLength(listTree) >= 1)) {
    /* RULE reverse::[(Dim1),Dims] -> 
              [reverse::Dims,(Dim1)]  */
    f2ilTTList(listTree->next, 5, -1) ;
    f2ilTT(listTree->tree, 0, 1) ;
  } else  if ((resultKind == -1) && (context == 5) &&
      (listTreeLength(listTree) == 0)) {
    /* RULE reverse::[] -> 
              []  */
  } else  if ((resultKind == -1) && (context == 3) &&
      (listTreeLength(listTree) == 0)) {
    /* RULE impldecl::[] -> 
              (implicit())  */
    sendOp(102) ;
    endList() ;
  } else  if ((resultKind == -1) && (context == 3)) {
    /* RULE impldecl::(Implicit)* -> 
              (Implicit)*  */
    ListTree *listStars ;
    int listStarsLength ;
    listStars = listTree ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    f2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
  } else  if ((resultKind == -1) && (context == 7) &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 241)) {
    /* RULE inInclude::[(string String),Strings] -> 
              [!!includeDirective(*,:)(String),inInclude::Strings]  */
    ListTree *userResListTree1 ;
    userResListTree1 = f2ilUincludeDirective((AtomValue)(char *)listTree->tree->contents.value) ;
    sendListTree(userResListTree1) ;
    if (stack == NULL) freeListTree(userResListTree1) ;
    f2ilTTList(listTree->next, 7, -1) ;
  } else  if ((resultKind == -1) && (context == 7) &&
      (listTreeLength(listTree) == 0)) {
    /* RULE inInclude::[] -> 
              []  */
  } else  if ((resultKind == -1) && (context == 6) &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 241)) {
    /* RULE beginInclude::[(string String),Strings] -> 
              (include String)  */
    sendOp(104) ;
    sendValue((char *)listTree->tree->contents.value) ;
  } else  if ((resultKind == -1) && (context == 8) &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 241)) {
    /* RULE endInclude::[(string String),Strings] -> 
              (include:"tapenade end #include")  */
    sendOp(104) ;
    sendValue("tapenade end #include") ;
  } else  if ((context == 9)) {
    /* RULE nameOnly::X -> 
              X  */
    f2ilTTList(listTree, 0, resultKind) ;
  } else  if ((resultKind == 1) && (context == 10)) {
    /* RULE dimsOnly::X -> 
              none()  */
    sendOp(143) ;
  } else {
    char *message = (char*)malloc(1000) ;
    int len = 0 ;
    message[0] = '\0' ;
    message = strcat(message, "Unexpected source list: (") ;
    while (listTree != NULL) {
      message = strcat(message, listTree->tree->oper->name) ;
      listTree = listTree->next ;
      if (listTree) {
        if (len>5) {
          message = strcat(message, " ...") ;
          listTree=NULL ;
        } else message = strcat(message, " ") ;
        len++ ;
      }
    }
    message = strcat(message, ") (") ;
    message = strcat(message, contextNames[context]) ;
    message = strcat(message, ")") ;
    sendOp(ilParsingErrorCode) ;
    sendValue(message) ;
  }
}


static TTMLOperatorTreeTranslator *(f2ilOperTreeTrans[]) = {
  /*   0...*/ &f2ilTTXTree,&f2ilTT1,&f2ilTT2,&f2ilTTXTree,&f2ilTTXTree,
  /*   5...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /*  10...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /*  15...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /*  20...*/ &f2ilTT20,&f2ilTT21,&f2ilTT22,&f2ilTT23,&f2ilTT24,
  /*  25...*/ &f2ilTT25,&f2ilTT26,&f2ilTT27,&f2ilTT28,&f2ilTT29,
  /*  30...*/ &f2ilTTXTree,&f2ilTT31,&f2ilTT32,&f2ilTTXTree,&f2ilTT34,
  /*  35...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTT37,&f2ilTT38,&f2ilTT39,
  /*  40...*/ &f2ilTT40,&f2ilTT41,&f2ilTT42,&f2ilTT43,&f2ilTT44,
  /*  45...*/ &f2ilTT45,&f2ilTTXTree,&f2ilTT47,&f2ilTT48,&f2ilTT49,
  /*  50...*/ &f2ilTT50,&f2ilTT51,&f2ilTTXTree,&f2ilTT53,&f2ilTT54,
  /*  55...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTT58,&f2ilTT59,
  /*  60...*/ &f2ilTT60,&f2ilTTXTree,&f2ilTT62,&f2ilTTXTree,&f2ilTTXTree,
  /*  65...*/ &f2ilTTXTree,&f2ilTT66,&f2ilTT67,&f2ilTT68,&f2ilTT69,
  /*  70...*/ &f2ilTT70,&f2ilTT71,&f2ilTT72,&f2ilTT73,&f2ilTT74,
  /*  75...*/ &f2ilTT75,&f2ilTT76,&f2ilTT77,&f2ilTT78,&f2ilTT79,
  /*  80...*/ &f2ilTTXTree,&f2ilTT81,&f2ilTT82,&f2ilTTXTree,&f2ilTTXTree,
  /*  85...*/ &f2ilTT85,&f2ilTT86,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /*  90...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /*  95...*/ &f2ilTTXTree,&f2ilTT96,&f2ilTT97,&f2ilTT98,&f2ilTTXTree,
  /* 100...*/ &f2ilTT100,&f2ilTT101,&f2ilTT102,&f2ilTTXTree,&f2ilTT104,
  /* 105...*/ &f2ilTT105,&f2ilTT106,&f2ilTT107,&f2ilTT108,&f2ilTT109,
  /* 110...*/ &f2ilTT110,&f2ilTT111,&f2ilTT112,&f2ilTT113,&f2ilTTXTree,
  /* 115...*/ &f2ilTT115,&f2ilTT116,&f2ilTT117,&f2ilTT118,&f2ilTT119,
  /* 120...*/ &f2ilTTXTree,&f2ilTT121,&f2ilTT122,&f2ilTT123,&f2ilTT124,
  /* 125...*/ &f2ilTT125,&f2ilTT126,&f2ilTT127,&f2ilTT128,&f2ilTT129,
  /* 130...*/ &f2ilTT130,&f2ilTT131,&f2ilTT132,&f2ilTT133,&f2ilTT134,
  /* 135...*/ &f2ilTT135,&f2ilTT136,&f2ilTT137,&f2ilTT138,&f2ilTT139,
  /* 140...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /* 145...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /* 150...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /* 155...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,
  /* 160...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTT164,
  /* 165...*/ &f2ilTT165,&f2ilTT166,&f2ilTT167,&f2ilTT168,&f2ilTT169,
  /* 170...*/ &f2ilTT170,&f2ilTT171,&f2ilTT172,&f2ilTT173,&f2ilTT174,
  /* 175...*/ &f2ilTT175,&f2ilTT176,&f2ilTT177,&f2ilTT178,&f2ilTT179,
  /* 180...*/ &f2ilTT180,&f2ilTT181,&f2ilTTXTree,&f2ilTTXTree,&f2ilTT184,
  /* 185...*/ &f2ilTT185,&f2ilTT186,&f2ilTT187,&f2ilTTXTree,&f2ilTTXTree,
  /* 190...*/ &f2ilTTXTree,&f2ilTT191,&f2ilTT192,&f2ilTT193,&f2ilTT194,
  /* 195...*/ &f2ilTT195,&f2ilTT196,&f2ilTT197,&f2ilTT198,&f2ilTT199,
  /* 200...*/ &f2ilTT200,&f2ilTTXTree,&f2ilTT202,&f2ilTT203,&f2ilTT204,
  /* 205...*/ &f2ilTT205,&f2ilTT206,&f2ilTT207,&f2ilTT208,&f2ilTT209,
  /* 210...*/ &f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTTXTree,&f2ilTT214,
  /* 215...*/ &f2ilTT215,&f2ilTT216,&f2ilTT217,&f2ilTT218,&f2ilTT219,
  /* 220...*/ &f2ilTT220,&f2ilTT221,&f2ilTT222,&f2ilTT223,&f2ilTT224,
  /* 225...*/ &f2ilTT225,&f2ilTT226,&f2ilTT227,&f2ilTT228,&f2ilTT229,
  /* 230...*/ &f2ilTTXTree,&f2ilTT231,&f2ilTT232,&f2ilTT233,&f2ilTT234,
  /* 235...*/ &f2ilTT235,&f2ilTT236,&f2ilTT237,&f2ilTT238,&f2ilTT239,
  /* 240...*/ &f2ilTT240,&f2ilTT241,&f2ilTT242,&f2ilTT243,&f2ilTT244,
  /* 245...*/ &f2ilTT245,&f2ilTTXTree,&f2ilTT247,&f2ilTT248,&f2ilTT249,
  /* 250...*/ &f2ilTT250,&f2ilTT251,&f2ilTT252,&f2ilTT253,&f2ilTT254,
  /* 255...*/ &f2ilTT255,&f2ilTT256,&f2ilTT257,&f2ilTT258,&f2ilTT259,
  /* 260...*/ &f2ilTT260,&f2ilTT261,&f2ilTT262,&f2ilTT263,&f2ilTT264,
  /* 265...*/ &f2ilTT265,&f2ilTT266,&f2ilTT267,&f2ilTT268,&f2ilTT269,
  /* 270...*/ &f2ilTT270,&f2ilTTXTree,&f2ilTT272,&f2ilTT273,&f2ilTT274,
  /* 275...*/ &f2ilTT275,&f2ilTT276,&f2ilTT277,&f2ilTT278,&f2ilTT279,
  /* 280...*/ &f2ilTT280,&f2ilTT281,&f2ilTT282,&f2ilTT283,&f2ilTT284,
  /* 285...*/ &f2ilTT285,&f2ilTT286,&f2ilTT287,&f2ilTT288,&f2ilTT289,
  /* 290...*/ &f2ilTT290,&f2ilTTXTree,&f2ilTT292,&f2ilTT293,&f2ilTT294,
  /* 295...*/ &f2ilTT295,&f2ilTT296,&f2ilTT297,&f2ilTT298,&f2ilTT299,
  /* 300...*/ &f2ilTT300,&f2ilTT301,&f2ilTT302,&f2ilTT303,&f2ilTT304,
  /* 305...*/ &f2ilTTXTree,&f2ilTT306,&f2ilTT307,&f2ilTTXTree,&f2ilTT309,
  /* 310...*/ &f2ilTT310,&f2ilTT311,&f2ilTT312,&f2ilTTXTree,&f2ilTTXTree,
  /* 315...*/ &f2ilTT315,&f2ilTT316,&f2ilTT317,&f2ilTT318,&f2ilTTXTree,
  /* 320...*/ &f2ilTT320,&f2ilTT321,&f2ilTTXTree,&f2ilTT323,&f2ilTT324,
  /* end...*/ NULL
} ;

void f2ilTT(Tree *fTree, int context, short resultKind) {
  (*(f2ilOperTreeTrans[fTree->oper->rank]))(fTree, context, resultKind) ;
}

void f2ilProtocol(Tree *fTree, FILE *outputStream) {
  TreeBuilder *previousTargetBuilder = targetBuilder ;
  FILE *previousProtocolOutputStream = protocolOutputStream ;
  TargetBuilderStack *newStack = new(TargetBuilderStack);
  newStack->top = NULL ;
  newStack->next = stack;
  stack = newStack;
  protocolOutputStream = outputStream ;
  f2ilTT(fTree, 0, 1) ;
  stack = stack->next;
  free(newStack);
  protocolOutputStream = previousProtocolOutputStream ;
  targetBuilder = previousTargetBuilder ;
}

Tree *f2ilTree(Tree *fTree) {
  Tree *returnTree ;
  TreeBuilder *previousTargetBuilder = targetBuilder ;
  FILE *previousProtocolOutputStream = protocolOutputStream ;
  TargetBuilderStack *newStack = new(TargetBuilderStack);
  newStack->top = newTreeBuilder(ilOperators) ;
  newStack->next = stack;
  stack = newStack;
  protocolOutputStream = NULL ;
  f2ilTT(fTree, 0, 1) ;
  returnTree = getTreeBuilt(stack->top) ;
  stack = stack->next;
  deleteTreeBuilder(newStack->top);
  free(newStack);
  protocolOutputStream = previousProtocolOutputStream ;
  targetBuilder = previousTargetBuilder ;
  return returnTree ;
}
