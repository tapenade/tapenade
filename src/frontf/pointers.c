static char fPid[]="$Id$";

#include "cvtp.h"
#include "pointers.h"

#include "tablesfortran.c"
#include <string.h>
#include <stdlib.h>

static char* key = "pointer";

typedef struct _IdentSet {
  char* ident;
  int isPointer;
  struct _IdentSet* next;
} IdentSet;

typedef struct _ContextStack {
  IdentSet* top;
  struct _ContextStack* prev;
} ContextStack;

static void internalTagPointers(Tree* tree, ContextStack* ctx);
static void clearContext(ContextStack* ctx);
static int isTaggedInContext(ContextStack* ctx, char* id);
static void enrichContext(Tree* tree, ContextStack* ctx, int* ptr);

void tagPointers(Tree* tree)
{
  internalTagPointers(tree, NULL);
}

static void internalTagPointers(Tree* tree, ContextStack* ctx)
{
 if(strcmp(tree->oper->name, "varDecl") == 0)
    {
      int ptr;
      ptr = 0;
      enrichContext(tree, ctx, &ptr);
    }
 else if(strcmp(tree->oper->name, "subroutine") == 0)
   {
   }
 else
   {
     ContextStack local;
     local.top = NULL;
     local.prev = ctx;
     if((strcmp(tree->oper->name, "unit") == 0)
	|| (strcmp(tree->oper->name, "includeFile") == 0))
       ctx = &local;
     else if(strcmp(tree->oper->name, "ident") == 0)
       {
	 if(isTaggedInContext(ctx, tree->contents.value) != 0)
	   *getSetToAnnotationTree(tree, key)
	     = mkAtom(&fortranOperators[260], NULL);
       }

     switch(tree->oper->arity)
       {
       case 0:
	 break;
       case -1:
       default:
	 {
	   ListTree* lt;
	   for(lt = tree->contents.sons;lt != NULL;lt = lt->next)
	     internalTagPointers(lt->tree, ctx);
	 }
	 break;
       }

     clearContext(&local);
   }
}

static void clearContext(ContextStack* ctx)
{
  while(ctx->top != NULL)
    {
      IdentSet* f = ctx->top;
      ctx->top = f->next;
      free(f);
    }
}

static int isTaggedInContext(ContextStack* ctx, char* id)
{
  while(ctx != NULL)
    {
      IdentSet* cur;
      for(cur = ctx->top;cur != NULL;cur = cur->next)
	{
	  if(strcmp(cur->ident, id) == 0)
	    return cur->isPointer;
	}
      ctx = ctx->prev;
    }

  return 0;
}

static void enrichContext(Tree* tree, ContextStack* ctx, int* ptr)
{
  if(strcmp(tree->oper->name, "ident") == 0)
    {
      IdentSet* is;
      is = malloc(sizeof(IdentSet));
      is->ident = tree->contents.value;
      is->isPointer = *ptr;
      is->next = ctx->top;
      ctx->top = is;
    }

  if(strcmp(tree->oper->name, "isPointer") == 0)
    *ptr = 1;

  switch(tree->oper->arity)
    {
    case 0:
      break;
    case -1:
    default:
	{
	  ListTree* lt;
	  for(lt = tree->contents.sons;lt != NULL;lt = lt->next)
	    enrichContext(lt->tree, ctx, ptr);
	}
	break;
    }
}
