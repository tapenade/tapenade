static char rcsid[]="$Id$";

#include <stdio.h>
#include <stdlib.h>
#include "stackTreeBuilder.h"

extern  int     yydebug;
extern FILE *yyin, *yyout;
extern int (*yylex)();

static FILE *debugfile ;

#ifndef MAXTOKLEN
#define MAXTOKLEN 500			/* max size of a token */
#endif

#ifndef True
#define True 1
#define False 0
#endif /* True */

#define Boolean int

#define nil 0

/* for debug */
int showtokens ;

/* Forward declarations */
typedef struct _keylist Keylist;

void yyerror() ;
int GetKey(Keylist *table, Keylist *endtable, int *len) ;
int ScanStartFreeStatement(int, int) ;
int Scan1to6(int) ;
void Newstring() ;
void Endstring(char) ;
void GetComment(char) ;
Boolean match_begin(char **bufptr, int blen, char *str) ;
int GetCppLine() ;
void LexReclassify() ;
int is_ONLY () ;
int is_FUNCTION (char *sav,char *next_ch,char *last_ch) ;
int is_PREFIX (char *sav, char *next_ch, char *last_ch, char *prefix, int prefixlen) ; 

#define FUZZYNESS 0
#ifndef SCANNERVARIABLES
#define SCANNERVARIABLES 1
/* ---------------------------------------------------------------------*/
/* Top Layer (FORTRANprotocol.c) to set the protocol for communication  */
/* with the Parser => Parser Server					*/
/* There is three types of Parser :					*/
/*	- Header Parser : which parses only the Header & the Main Unit	*/
/*	- Body Parser	:   "     "     "   the wanted Unit		*/
/*	- Fortran Parser:   "     "    a whole file			*/
/* ---------------------------------------------------------------------*/

/* Body Parser not implemented : Perhaps needs more than only scanning to */
/* find unit bodies							  */

int Header_Parser = False;	/* say whether we are in mode Header_Parser */
int Body_Parser = False;	/* say whether we are in mode Body_Parser   */
				/* default is Fortran Parser		    */

int Tags_Header = False; /*Indicate whether we are on a Unit Header : Viland */
                                    /* we are a priori not in a Unit Header  */
int Func_Header = False; /* Indicate that we read a Basic Type, Consequently */
                                         /* the next token may be a function */
                                    /* we are a priori not with a Basic Type */
int Sub_Unit = False;   /* Indicate whether we are in a SubUnit : Viland     */
                                    /* we are a priori not in a SubUnit      */
#endif /* SCANNERVARIABLES */


#define PROTOCOL_ENDDATA  "enddata"


extern int Fyylex();

/* ---------------------------------------------------------------------*/
/*			End Protocol Layer				*/
/* ---------------------------------------------------------------------*/

#include <ctype.h>
#include <string.h>


#ifdef YYDEBUG
/*extern	int	yydebug;*/
#endif

/* Variable defining the nb of column in a line (72 or 132) */
int Max_Col_Nb = 72;		/* Default */
int Keep_Line_Trailer = 0;	/* if true keep columns > 72 as comments */

/* Language Type */
#define GOULD_DIALECT "GOULD"
#define SUN_DIALECT "SUN"
#define VAX_DIALECT "VAX"
#define FLC90_DIALECT "FLC90"
Boolean padToEol = True;        /* padd Holl. (and string) with ' ' --> 72 */
Boolean GouldFortran = False;	/* true to have FORTRAN Gould Extension  */
Boolean NeedGouldColon = False;	/* to accept ':' in Gould Identifier */
int Flc90             = 0;	/* if true, recognize flc90 directives */
Boolean SkipComments = False;

/* special Gould Kwd which are skipped within the Scanner */
#define KW_NULL 29995
#define KW_OPTION 29996
#define KW_PAGE   KW_NULL
#define KW_SPACE  KW_NULL
#define KW_USER   KW_NULL
#define KW_PVIR 29999

/* Format Type */
Boolean FreeFormat = False;
/* Also parse OpenMP pragmas (instead of considering them as comments) */
Boolean parseOpenMP = False ;

Boolean StructVax = True;
Boolean SendKWDir = False;
char *NeedOpUser = (char *)0;
int CurTypeDir = 0;  /* specifie the current type directive */
                     /* initialized at the begining of FreeFormatGetFullLine */
int NextLineTypeDir = 0;      /* specify the type directive of the next line */
                              /* if necessary positionned in the function    */
                              /* ScanStartFreeLine */
int DIR_TYPE_HPF = 2;
int DIR_TYPE_OMP = 3;

/* -- data for current input file */

#define	MAXCONTL 2000		/* nb of continuation lines or comments */
#define FILENMLEN 200		/* filenames length */
#define MAXBUF	MAXCONTL*80	/* size of statement buffer */
#define MAXSTR	30000		/* max nb of strings or comments */
#define MAXDOLAB 100		/* nb of nested loops */
#define MAXTOK	400		/* size of token unput buffer */

/* Main states of automaton in yylex */
#define ST_BEGIN 0
#define ST_KEY 1
#define ST_OTHER 2

/* pseudo-characters for directives strings hollerith and comments */
/* contained in the read Inbuf string                              */
#define SPEC_STRING 31
#define SPEC_COMM 30
#define SPEC_DIR 29
#define SPEC_HOLL 28

/* H Character for Hollerith */
#define HOLLERITH_CHAR 'H'

/* -------------------- lexer variables ------------------------------ */
char	Inbuf[MAXBUF+1];		/* input buffer */
char	*Maxbuf = Inbuf+MAXBUF;
char	*lastch,*nextch;

/* To get the NAME real values (written with the right case) */
char	Realbuf[MAXBUF+1];
char	*Reallastch, *Realnextch; /* used in HAVE */
int	RealCc;  		/* last real read char (with the good case) */



#define MAXUNGET 128
char	Ungetbuf[MAXUNGET];
int	Ungetnb;

/* Table of buffer which contains all the token having a long string value */
/* such as : FORTRAN strings, Comments, FORMAT elem.			   */
/* Rules :								   */
/*		strlen(StrPtr[i]) == StrLen[i]				   */
/* 		StrCnt <= MAXSTR					   */

int ErrorFlag = False;			/* stop the parser when syntax error */

char	*StrPtr[MAXSTR];		/* table of buffer */
int	StrLen[MAXSTR];			/* table of buffer length */
char	StrBuf[MAXBUF+1];		/* buffer */
int	StrCnt;				/* top of the table */
char	*StrIns;			/* current read char  */
char	*StrEnd = StrBuf+MAXBUF;	/* end of current buffer */


char	LowerC[128];			/* translation table */
int     LexDataInitialized = False ;    /* True after InitLexData called */

/* Warning: must probably be removed in SOLARIS... */
char	yytext[MAXTOKLEN];
int	yyleng;

int	LastToken;			/* remember last token */

/* The TokStack contains stacked tokens (DO LOOPS, FORMAT)		   */
/* token_struct = tok 	      the token code,	 			   */
/*		  value_index the token index value referenced in the      */
/*		              StrPtr table.				   */
/* If value_ptr == -1 the token has no value				   */
struct  token_struct {
    short tok;
    int   value_index;
};
struct token_struct TokStack[MAXTOK];		/* token unput buffer */
int	TokSP;
int     TokBP;

char *nlStack[MAXCONTL]; /* address in Inbuf of each char. just after a '\n' */
int  nlSP;		 /* top of the '\n' stack			     */

short	CommentStack[MAXSTR];		/* Stack of comments to be sent at next EOL */
int	CommentSP;

/* for DO loops end detection */
int	Dolabels[MAXDOLAB];		/* labels of stacked loops */
int	Docounts[MAXDOLAB];		/* nb of loops with same label */
int	DoDepth;			/* number of stacked loops */
int	ClosingCount;		        /* number of loops closed by the label */

/* ----- data for stack of files (recursive input by INCLUDE directive) ---- */

typedef struct _fstack {
	FILE	*fp;
	char	name[FILENMLEN];
	int	lineno;
} FSTACK;

#define MAXFILE 8

FILE	*Infile;			/* file itself */
char	*Infname;			/* file name */
int	curInputLineNo; 		/* line number */
int	readLines;			/* nb of lines read since beginning
					   of statement */
long    offset,EndOffset;	        /* current and last offset to scan */
long    StatementOffset = 0;		/* offset of line start */
long    UnitOffset = 0;		        /* offset of the current Unit */

int NoEOLToken;				/* DO not want to return EOL at end
					   of line 			    */

/* char	Cc; Does not work with EOF which is -1*/
int	Cc;  			/* last read char */
int	Ccol;			/* current column */
int     TabFormat;		/* says if the line is in tab format */

/* flag set to true when Scanning an entry ([ENTRY] ended with [ENDDATA]) */
int InEntryPt = False;			   /* a priori not in an Entry Point*/
int LastEntryPt = False;		   /* True if the last pass is an 
					      analysis of an Entry Point */

FSTACK	FileStack[MAXFILE+1];
int	FStackP;
 
/* -- Saved information for parser */
/* the Hints table contains the information concerning the current statement */
/* each time a ';' is found, a new statement level is stacked in the table   */
struct StatementInfo{
int	GotEqual;	/* We have a '=' sign in stat not within parenthesis */
int	GotDblColon;	/* We have a '::' sign in stat */
int	GotComma;	/* We have a comma in stat, not within parenthesis */
int	CurLabel;	/* label of current line (none if <= 0)*/
};
struct StatementInfo STable[32]; /* stack of statement separated with ';' */
int pSTable;		         /* pointer on current stat. in STable */

int	ParLevel;	/* Parenthesis level */
int	BrackLevel;	/* Bracket level */
int     GotInclBracket;	/* include <xxx.H> style, search in include path */

int	NextLabel;	/* label of next line (none if <= 0) */
int	EntryPt;	/* entry point code (none if < 0) */
int	LxStatus;	/* state of lexer */
int	MainKey;	/* main keyword of statement */

/* flags set by parser actions */
int	Need_Kwd;	/* forces idents to be seen as keywords */
int	Opt_Need_Kwd;	/* forces idents to be seen as keywords or else as a name */
int	Need_Letter;	/* forces idents to be seen as letters if length 1 */
enum _need_offset { NO, SEND_START, SEND_END }	Need_Offset;	/* ask for a OFFSET pseudo token */
int	Need_Op_Kwd;	/* forces OPERATOR and ASSIGNMENT to be seen as kwd */
int     Need_Type_Kwd;  /* forces type names to be seen as kwd */
int	InIoctl;	/* in IO control (for "NAME = ...")*/
int	OptCommactl;	/* For optional comma */
int	SizeCommactl;	/* For optional comma */
int	InDeclctl;	/* For optional comma */
int	IntOnly;	/* numbers are integers (don't eat "e" or "d") */
int 	InImplicit;	/* set by an action in metal for FORTRAN implicit */
int 	Common_Flag;	/* True when processing a COMMON name */
int     Do_While_Kwd;   /* True when While Keyword needed */
int	InOperatorSpec;	/* To recognize OPERATOR(\) */
int     InString_or_Holl;    /* when procesing multi line holl. or string */

/* -------------------- Macros ---------------------------------------- */

#define GET()	{ if(Ungetnb > 0) Cc = Ungetbuf[--Ungetnb]; \
		      else { Cc = getc(Infile); offset++; if (Cc=='\n' || Cc=='\r') readLines++; }}
#define UNGET(c) { Ungetbuf[Ungetnb++] = (c); }
#define STORE(c) { if(StrIns < StrEnd) *StrIns++ = (c); }
#define INIT_NEXTCH() { nextch = lastch = Inbuf; \
                        Realnextch = Reallastch = Realbuf;}
#define HAVE(c) if(lastch < Maxbuf)  { *lastch++ = (c);\
					   *Reallastch++ = RealCc ;}
#define TAKE(c) { yytext[yyleng] = (c); if(yyleng < MAXTOKLEN) yyleng++; }

#define LEXLINE() stbLexLine()
/* #define LEXLINE() attachComments() */ /* llh 18/5/01 */
/* #define LEXLINE() fprintf(yyout,"-2\n") */
#define COMMENT(len,str) stbWaitingComment((len), (str))
/*#define COMMENT(len,str) fprintf(yyout,"-1\n%d\n%s\n",(len)-1,(str)+1);*/
#define LABEL(intlabel) stbWaitingLabel(intlabel)

#define RETTOKEN(t) { LastToken = t ; return(t); }

/* Get an integer coded on two bytes */
#define GetStrCnt(s) ((unsigned char)*(s) * 256 + (unsigned char)*(s+1))

/* Determine if the token is a Unit header : Viland */
#define IS_A_HEADER(token) ((token) == KW_SUBROUTINE ||\
			    (token) == KW_PROGRAM ||\
			    (token) == KW_FUNCTION ||\
			    (token) == KW_MODULE ||\
			    (token) == KW_BLOCKDATA )

/* Determine if the token is a Basic Type : Viland */
#define IS_A_BASIC_TYPE(token) ((token) == KW_INTEGER ||\
				(token) == KW_REAL ||\
				(token) == KW_COMPLEX ||\
				(token) == KW_DOUBLE ||\
				(token) == KW_DOUBLECOMPLEX ||\
				(token) == KW_LOGICAL ||\
				(token) == KW_CHARACTER ||\
				(token) == KW_VOID ||\
				(token) == KW_BYTE ||\
				(token) == KW_BIT )

#define IS_A_NUMERIC_CST(token) ((token) == INTCST ||\
				 (token) == REALCST ||\
				 (token) == DOUBLECST ||\
				 (token) == BITCST )

#define IS_A_HPF_STAT(token) ((token) == KW_INDEPENDENT ||\
			      (token) == KW_REALIGN ||\
			      (token) == KW_REALIGNWITH ||\
			      (token) == KW_REDISTRIBUTE ||\
			      (token) == KW_REDISTRIBUTEONTO ||\
			      (token) == KW_EMPTYREDISTRIBUTEONTO )

/* Determine if the token is an end of Unit */
#define IS_AN_END_UNIT(token) ((token) == KW_END ||\
			       (token) == KW_ENDPROGRAM ||\
			       (token) == KW_ENDBLOCKDATA ||\
			       (token) == KW_ENDSUBROUTINE ||\
			       (token) == KW_ENDMODULE ||\
			       (token) == KW_ENDFUNCTION ||\
                               (token) == KW_END_OF_FILE)

/* Determine if the token is an generic operator : OPERATOR or ASSIGNMENT */
#define IS_OP_KEYWORD(tken) ((tken) == KW_OPERATOR || (tken) == KW_ASSIGNMENT)

/* It is not a symbolic operator ==, /=, >=, <= */
#define IS_NOT_AN_OPERATOR(c) (c != '/' && c != '>' && c != '<' && c != '=')

#define STREQ(s1,s2) ( 0 == strcmp((s1),(s2)) )


/* -------------------- Keywords -------------------------------------- */
typedef struct _keylist {
	char	*name;
	short	code,len;
} Keylist;

/**** ----------------------------------------------------------------- ***/
/**** BEWARE : if "type" is before "typename"  for example, the keyword ***/
/****          "type" will always be found even if we are looking for   ***/
/**** 	       "typename".                                              ***/
/**** ----------------------------------------------------------------- ***/
#include "token.h"

Keylist  Keywords[ ] =
{
  	{ "#include",		KW_SHARP_INC, 0},	/* SUN-VAX */
        { "abstract interface", KW_ABSTRACTINTERFACE, 0}, /* F2003 */
        { "abstractinterface",  KW_ABSTRACTINTERFACE, 0},  /* F2003 */
        { "abstract",           KW_ABSTRACT, 0},        /* F2003 */
        { "accept", 		KW_ACCEPT, 0},		/* SUN-VAX */
        { "allocatable",	KW_ALLOCATABLE, 0}, 	/* F90 */
        { "allocate",		KW_ALLOCATE, 0}, 	/* F90 */	
	{ "all", 		KW_ALL, 0},             /* SUN-VAX */
	{ "assignment",		KW_ASSIGNMENT, 0},      /* F90 */
	{ "assign",		KW_ASSIGN, 0},
	{ "automatic",		KW_AUTOMATIC, 0},	/* SUN-VAX */
	{ "array",		KW_ARRAY, 0},		/* CMF */
	{ "a",			KW_A, 0},          	/* SUN */
	{ "backspace",		KW_BACKSPACE, 0},
	{ "backfile",		KW_BACKFILE, 0},
        { "begin",              KW_BEGIN, 0},		/* GOULD */
	{ "block data",		KW_BLOCKDATA, 0},       /* Free Format */
	{ "blockdata",		KW_BLOCKDATA, 0},
	{ "byte", 		KW_BYTE, 0},		/* SUN-VAX */
	{ "bind",		KW_BIND, 0},		/* F90 */
	{ "bit", 		KW_BIT, 0},		/* GOULD */
	{ "bounds", 		KW_BOUNDS, 0},          /* SUN-VAX */
	{ "call",		KW_CALL, 0},
	{ "case",		KW_CASE, 0},		/* CMF */
	{ "character",		KW_CHARACTER, 0},
	{ "check",		KW_CHECK, 0},
	{ "class default",	KW_CLASSDEFAULT, 0},	/* F2003 */
	{ "classdefault",	KW_CLASSDEFAULT, 0},	/* F2003 */
	{ "class is",		KW_CLASSIS, 0},		/* F2003 */
	{ "classis",		KW_CLASSIS, 0},		/* F2003 */
	{ "class",		KW_CLASS, 0},		/* F2003 */
	{ "close",		KW_CLOSE, 0},
	{ "common",		KW_COMMON, 0},
	{ "complex",		KW_COMPLEX, 0},
	{ "contains",		KW_CONTAINS, 0},	/* F90 */
	{ "contiguous",		KW_CONTIGUOUS, 0},      /* F2008 */
	{ "continue",		KW_CONTINUE, 0},
	{ "cycle",		KW_CYCLE, 0},		/* CMF */
	{ "data",		KW_DATA, 0},
	{ "deallocate",		KW_DEALLOCATE, 0}, 	/* F90 */
	{ "decode",		KW_DECODE, 0},		/* SUN-VAX Fortran66 */
	{ "default",		KW_DEFAULT, 0},		/* CMF */
        { "deferred",           KW_DEFERRED, 0},        /* F2003 */
	{ "definefile",		KW_DEFINEFILE, 0},	/* VAX/VMS */
	{ "delete",		KW_DELETE, 0},		/* VAX/VMS */
	{ "dictionary",		KW_DICTIONARY, 0},	/* VAX/VMS */
	{ "dimension",		KW_DIMENSION, 0},
	{ "double precision",	KW_DOUBLE, 0},          /* Free Format */
	{ "doubleprecision",	KW_DOUBLE, 0},
	{ "double complex", 	KW_DOUBLECOMPLEX, 0},
	{ "doublecomplex", 	KW_DOUBLECOMPLEX, 0},
	{ "do", 		KW_DO, 0},
	{ "else if",		KW_ELSEIF, 0},          /* Free Format */
	{ "elseif",		KW_ELSEIF, 0},
	{ "elsewhere",		KW_ELSEWHERE, 0},	/* CMF */
	{ "else",		KW_ELSE, 0},
	{ "elemental",		KW_ELEMENTAL, 0},       /* F95 */
	{ "end if",		KW_ENDIF, 0},           /* Free Format */
	{ "endif",		KW_ENDIF, 0},
	{ "end do",		KW_ENDDO, 0},           /* Free Format */
	{ "enddo", 		KW_ENDDO, 0},
	{ "entry",		KW_ENTRY, 0},
	{ "equivalence",	KW_EQUIVALENCE, 0},
	{ "external",		KW_EXTERNAL, 0},
	{ "extends",		KW_EXTENDS, 0},         /* F2003 */
	{ "encode",		KW_ENCODE, 0}, 		/* SUN-VAX Fortran66 */
	{ "end block data", 	KW_ENDBLOCKDATA, 0},    /* Free Format */
	{ "end blockdata", 	KW_ENDBLOCKDATA, 0},    /* Free Format */
	{ "endblock data", 	KW_ENDBLOCKDATA, 0},    /* Free Format */
	{ "endblockdata", 	KW_ENDBLOCKDATA, 0},	/* CMF */
	{ "end file",		KW_ENDFILE, 0},         /* Free Format */
	{ "endfile",		KW_ENDFILE, 0},
 	{ "end forall",		KW_ENDFORALL, 0},       /* Free Format */
 	{ "endforall",		KW_ENDFORALL, 0},       /* Free Format */
	{ "end function", 	KW_ENDFUNCTION, 0},     /* Free Format */
	{ "endfunction", 	KW_ENDFUNCTION, 0},	/* CMF */
	{ "end interface assignment", KW_ENDASSIGNMENT, 0},/* Free Format */
	{ "endinterface assignment", KW_ENDASSIGNMENT, 0}, /* Free Format */
	{ "endinterfaceassignment",   KW_ENDASSIGNMENT, 0},/* F90 */
	{ "end interface operator", KW_ENDOPERATOR, 0},    /* Free Format */
	{ "endinterface operator",   KW_ENDOPERATOR, 0},    /* F90 */
	{ "endinterfaceoperator",   KW_ENDOPERATOR, 0},    /* F90 */
	{ "end interface",	KW_ENDINTERFACE, 0},    /* Free Format */
	{ "endinterface",	KW_ENDINTERFACE, 0},	/* CMF */
	{ "endinternal",	KW_ENDINTERNAL, 0},	/* GOULD */
	{ "endmap",		KW_ENDMAP, 0},		/* SUN-VAX */
	{ "end module", 	KW_ENDMODULE, 0},       /* Free Format */
	{ "endmodule", 		KW_ENDMODULE, 0},	/* F90 */
        { "end parallel do",    KW_EOLOMPENDPARALLELDO, 0},     /* OMP */
        { "endparalleldo",      KW_EOLOMPENDPARALLELDO, 0},     /* OMP */
        { "end parallel",       KW_OMPENDPARALLEL, 0},     /* OMP */
        { "endparallel",        KW_OMPENDPARALLEL, 0},     /* OMP */
	{ "end program", 	KW_ENDPROGRAM, 0},      /* Free Format */
	{ "endprogram", 	KW_ENDPROGRAM, 0},	/* CMF */
	{ "end select", 	KW_ENDSELECT, 0},	/* Free Format */
	{ "endselect", 		KW_ENDSELECT, 0},	/* F90 */
	{ "endstructure",	KW_ENDSTRUCTURE, 0},	/* SUN-VAX */
	{ "end subroutine", 	KW_ENDSUBROUTINE, 0},	/* Free Format */
	{ "endsubroutine", 	KW_ENDSUBROUTINE, 0},	/* CMF */
	{ "end type", 		KW_ENDTYPE, 0},		/* Free Format */
	{ "endtype", 		KW_ENDTYPE, 0},		/* F90 */
	{ "endunion",		KW_ENDUNION, 0},	/* SUN-VAX */
	{ "end where",		KW_ENDWHERE, 0},	/* Free Format */
	{ "endwhere",		KW_ENDWHERE, 0},	/* CMF */
	{ "endi", 		KW_ENDI, 0},		/* GOULD */
	{ "end",		KW_END, 0},
	{ "exit",		KW_EXIT, 0},		/* F90 */
	{ "extend_source", 	KW_EXTEND_SOURCE, 0},   /*SUN-VAX */
	{ "extendedblock", 	KW_EXTENDEDBLOCK, 0},   /* GOULD */
	{ "extendedbase", 	KW_EXTENDEDBASE, 0},    /* GOULD */
	{ "extendeddummy", 	KW_EXTENDEDDUMMY, 0},   /* GOULD */
	{ "extrinsic",		KW_EXTRINSIC, 0},       /* HPF */
	{ "find",		KW_FIND, 0},		/* VAX/VMS */
	{ "firstprivate",	KW_FIRSTPRIVATE, 0},	/* OMP */
	{ "flush",		KW_FLUSH, 0},
	{ "forall",		KW_FORALL, 0},		/* CMF */
	{ "format",		KW_FORMAT, 0},
	{ "f77", 		KW_F77, 0},             /*SUN-VAX */
	{ "function",		KW_FUNCTION, 0},
	{ "go to",		KW_GOTO, 0},            /* Free Format */
	{ "goto",		KW_GOTO, 0},
	{ "g_floating",		KW_G_FLOATING, 0},      /*SUN-VAX */
	{ "i4", 		KW_I4, 0},              /*SUN-VAX */
	{ "integer",		KW_INTEGER, 0},
	{ "implicit",		KW_IMPLICIT, 0},
        { "import",             KW_IMPORT, 0},          /* F2003 */
	{ "include",		KW_INCLUDE, 0},		/* SUN-VAX */
	{ "interface assignment", KW_INTERFACEASS, 0},  /* Free Format */
	{ "interfaceassignment",   KW_INTERFACEASS, 0}, /* F90 */
	{ "interface operator", KW_INTERFACEOP, 0},     /* Free Format */
	{ "interfaceoperator",   KW_INTERFACEOP, 0},    /* F90 */
	{ "interface",		KW_INTERFACE, 0},	/* F90 */
	{ "in out",		KW_INOUT, 0},		/* Free Format */
	{ "inout",		KW_INOUT, 0},		/* CMF */
	{ "inquire",		KW_INQUIRE, 0},
	{ "intent",		KW_INTENT, 0},		/* CMF */
	{ "intrinsic",		KW_INTRINSIC, 0},
	{ "inline",		KW_INLINE, 0},		/* GOULD */
	{ "internal",		KW_INTERNAL, 0},	/* GOULD */
	{ "in",	        	KW_IN, 0},		/* CMF */
	{ "lastprivate",	KW_LASTPRIVATE, 0},	/* OMP */
	{ "leave",		KW_LEAVE, 0},		/* GOULD */
	{ "logical",		KW_LOGICAL, 0},
	{ "module procedure", 	KW_MODULEPROCEDURE, 0},   /*F90 */
	{ "moduleprocedure", 	KW_MODULEPROCEDURE, 0},   /*F90 */
	{ "module", 		KW_MODULE, 0},            /*F90 */
	{ "map", 		KW_MAP, 0},            /*SUN-VAX */
	{ "namelist", 		KW_NAMELIST, 0},	/* SUN-VAX */
	{ "nobounds", 		KW_NOBOUNDS, 0},        /*SUN-VAX */
	{ "nocheck", 		KW_NOCHECK, 0},          /*SUN-VAX */
	{ "noextend_source", 	KW_NOEXTEND_SOURCE, 0},  /*SUN-VAX */
	{ "nof77", 		KW_NOF77, 0},          /*SUN-VAX */
	{ "nog_floating",	KW_NOG_FLOATING, 0},   /*SUN-VAX */
	{ "noi4", 		KW_NOI4, 0},           /*SUN-VAX */
	{ "nooverflow", 	KW_NOOVERFLOW, 0},     /*SUN-VAX */
	{ "nounderflow",	KW_NOUNDERFLOW, 0},    /*SUN-VAX */
	{ "none", 		KW_NONE, 0},		/* SUN-VAX */
        { "nopass",             KW_NOPASS, 0},          /* F2003 */
	{ "nullify",		KW_NULLIFY, 0},		/* F90 */
	{ "num_threads",	KW_NUM_THREADS, 0},	/* OMP */
	{ "only",		KW_ONLY, 0},		/* F90 */
	{ "open",		KW_OPEN, 0},
	{ "operator",		KW_OPERATOR, 0},	/* F90 */
	{ "options",		KW_OPTIONS, 0},		/* SUN-VAX */
	{ "optional",		KW_OPTIONAL, 0},	/* F90 */
	{ "option",		KW_OPTION, 0},	  /* GOULD (set in comment) */
	{ "out",		KW_OUT, 0},		/* CMF */
	{ "overflow", 		KW_OVERFLOW, 0},	/*SUN-VAX */
        { "pass",               KW_PASS, 0},            /* F2003 */
        { "paralleldo",         KW_PARALLELDO, 0},        /* OMP */
        { "parallel do",        KW_PARALLELDO, 0},        /* OMP F90*/
        { "parallel",           KW_PARALLEL, 0},        /* OMP */
	{ "parameter",		KW_PARAMETER, 0},
	{ "pause",		KW_PAUSE, 0},
	{ "pointer",		KW_POINTER, 0},		 /* SUN_VAX */
	{ "print",		KW_PRINT, 0},
	{ "private",		KW_PRIVATE, 0},		/* F90 */
	{ "program",		KW_PROGRAM, 0},
	{ "public",		KW_PUBLIC, 0},		/* F90 */
	{ "pure",		KW_PURE, 0},		/* HPF */
	{ "punch",		KW_PRINT, 0},		/* GOULD */
	{ "page",		KW_NULL, 0},		/* GOULD (ignored) */
	{ "read",		KW_READ, 0},
	{ "real",		KW_REAL, 0},
	{ "record",		KW_RECORD, 0},		 /* SUN-VAX */
	{ "return",		KW_RETURN, 0},
	{ "recursive",		KW_RECURSIVE, 0},	/* F90 */
	{ "result",		KW_RESULT, 0},		/* F90 */
        { "reduction",          KW_REDUCTION, 0},       /* OMP */
	{ "rewind",		KW_REWIND, 0},
	{ "rewrite",            KW_REWRITE, 0},		/* VAX/VMS */
	{ "save",		KW_SAVE, 0},
        { "schedule",           KW_SCHEDULE, 0},        /* OMP */
	{ "select case",	KW_SELECTCASE, 0},	/* Free Format */
	{ "selectcase",		KW_SELECTCASE, 0},	/* F90 */
	{ "select type",	KW_SELECTTYPE, 0},	/* Free Format */
	{ "selecttype",		KW_SELECTTYPE, 0},	/* F2003 */
	{ "sequence",		KW_SEQUENCE, 0},	/* F90 */
        { "shared",             KW_SHARED, 0},          /* OMP */
	{ "static",		KW_STATIC, 0},		/* SUN-VAX */
	{ "stop",		KW_STOP, 0},
	{ "structure",		KW_STRUCTURE, 0},	 /* SUN-VAX */
	{ "subroutine",		KW_SUBROUTINE, 0},
	{ "skipfile",		KW_SKIPFILE, 0},	/* GOULD */
	{ "space",		KW_NULL, 0},		/* GOULD (ignored) */
        { "target",		KW_TARGET, 0},		/* F90 */
        { "value",		KW_VALUE, 0},		/* F90 */
	{ "then",		KW_THEN, 0},
	{ "times",		KW_TIMES, 0},		/* CMF */
	{ "to",			KW_TO, 0},		/* CMF */
	{ "type is",		KW_TYPEIS, 0},		/* F2003 */
	{ "typeis",		KW_TYPEIS, 0},		/* F2003 */
	{ "type",		KW_TYPE, 0},		/* F90-VAX */
	{ "procedure",		KW_PROCEDURE, 0},	/* F2003 */
	{ "undefined",		KW_UNDEFINED, 0},     /*SUN */
	{ "underflow",		KW_UNDERFLOW, 0},     /*SUN-VAX */
	{ "until",		KW_UNTIL, 0},     	/* GOULD */
	{ "user",		KW_NULL, 0},		/* GOULD (ignored) */
	{ "use",		KW_USE, 0},     	/* F90 */
	{ "union",		KW_UNION, 0},		/*  SUN-VAX */
	{ "unlock",		KW_UNLOCK, 0},		/* VAX/VMS */
	{ "virtual",		KW_DIMENSION, 0},    /* SUN-VAX, dimension */
	{ "void",		KW_VOID, 0},
	{ "volatile",		KW_VOLATILE, 0},	/* SUN-VAX */
	{ "where", 		KW_WHERE, 0},		/* CMF */
	{ "while", 		KW_WHILE, 0},		/* SUN-VAX */
	{ "write",		KW_WRITE, 0},
	{ "z", 			KW_Z, 0},           /* SUN-VAX */
	{ 0, 0, 0}
};
Keylist  HPF_Keywords[ ] =
{
  	{ "*",		        KW_DISTSTAR, 0},
  	{ "align with",		KW_ALIGNWITH, 0},		
  	{ "align",		KW_ALIGN, 0},			
  	{ "block",		KW_BLOCK, 0},
  	{ "cyclic",		KW_CYCLIC, 0},		
  	{ "dimension",		KW_DIMENSION, 0},
  	{ "distribute onto",	KW_COMBDISTRIBUTEONTO, 0},	
  	{ "distribute",		KW_DISTRIBUTE, 0},
  	{ "dynamic",		KW_DYNAMIC, 0},			
  	{ "independent",	KW_INDEPENDENT, 0},	
  	{ "inherit",		KW_INHERIT, 0},
  	{ "new",		KW_NEW, 0},	       
  	{ "no sequence",	KW_NOSEQUENCE, 0},	
  	{ "onto",		KW_ONTO, 0},	
  	{ "processors",		KW_PROCESSORS, 0},	
  	{ "sequence",		KW_SEQUENCE, 0},			
  	{ "template",		KW_TEMPLATE, 0},	
  	{ "with",		KW_WITH, 0},	
  	{ "realign with",	KW_REALIGNWITH, 0},
  	{ "realign",		KW_REALIGN, 0},		
  	{ "redistribute onto",	KW_EMPTYREDISTRIBUTEONTO, 0},	
  	{ "redistribute",	KW_REDISTRIBUTE, 0},	 
	{ 0, 0, 0}
};



/**** ----------------------------------------------------------------- ***/
/**** BEWARE : if "type" is before "typename"  for example, the keyword ***/
/****          "type" will always be found even if we are looking for   ***/
/**** 	       "typename".                                              ***/
/**** ----------------------------------------------------------------- ***/
Keylist	Dots[] =
{
	{ "and.",	KW_AND, 0},
	{ "or.",	KW_OR, 0},
	{ "not.",	KW_NOT, 0},
	{ "true.",	KW_TRUE, 0},
	{ "false.",	KW_FALSE, 0},
	{ "eq.",	KW_EQ, 0},
	{ "ne.",	KW_NE, 0},
	{ "lt.",	KW_LT, 0},
	{ "le.",	KW_LE, 0},
	{ "gt.",	KW_GT, 0},
	{ "ge.",	KW_GE, 0},
	{ "neqv.",	KW_NEQV, 0},
	{ "eqv.",	KW_EQV, 0},
	{ "xor.",	KW_XOR, 0},
	{ "==", 	KW_EQ, 0},
	{ "/=", 	KW_NE, 0},
	{ "=>", 	KW_PTRASS, 0},
	{ "=", 	        KW_EQUL, 0},
	{ "<=", 	KW_LE, 0},
	{ "<>", 	KW_NE, 0},
	{ "<", 	        KW_LT, 0},
	{ ">=", 	KW_GE, 0},
	{ ">", 	        KW_GT, 0},
	{ 0, 0, 0}

};




/**** ----------------------------------------------------------------- ***/
/**** BEWARE : if "type" is before "typename"  for example, the keyword ***/
/****          "type" will always be found even if we are looking for   ***/
/**** 	       "typename".                                              ***/
/**** ----------------------------------------------------------------- ***/
Keylist  EntryPoints[ ] =
{
  {"unit",          EN_Unit, 0},
  {"opt_header",    EN_OptHeader, 0},
  {"opt_trailer",   EN_OptTrailer, 0},
  {"decls",         EN_Decls, 0},
  {"decl",          EN_Decl, 0},
  {"stats",         EN_Stats, 0},
  {"stat",          EN_Stat, 0},
  {"exp",           EN_Exp, 0},
  {"type",          EN_Type, 0},
  {"includefile",          EN_IncludeFile, 0},
  {0, 0, 0}
};





Keylist	*KeyStart[26],*KeyStop[26];	/* arrays for fast access */
Keylist *BopStart;

/* ---------------------------------------------------------------------- */
/* |	Error sent by Scanner						| */
/* |	text : format for the printf ("... %s ...%s")			| */
/* |	p1,p2,p3,p4 are strings matching with format			| */
/* ---------------------------------------------------------------------- */
int LexError(error,text,p1,p2,p3,p4)
     Boolean error ;
     char	*text,*p1,*p2,*p3,*p4;
{
    /* for now nothing in Tree protocol to read string after Lexical Error 
       -3 code. It generates an error when buf is bigger (2000) than
       the 1024 buffer on the Lisp side. 
       Old code was :
	char	buf[2000];
	fprintf(yyout,"-3\n%s\n",buf);
	sprintf(buf,text,p1,p2,p3,p4);
	*/
    char *buf = (char*)malloc(200) ;
    int i ;
    for (i=0 ; i<200 ; i++) buf[i]='\0' ;
    sprintf(buf,text,p1,p2,p3,p4) ;
    if (error)
      stbLexicalError(buf) ; /* llh 18/5/01 */
    else
      stbLexicalWarning(buf) ;
    /*    fprintf(yyout,"-3\n"); */ /* llh 18/5/01 */
#ifdef YYDEBUG
    if(yydebug) {
	fprintf(stderr,"|%s|\n",Inbuf);
    }
#endif
}

/* ---------------------------------------------------------------------- */
/* |	Send -9 to Tree Protocol:					| */
/* |    -9\nannotation-name\nmsg-string					| */
/* |    This protocol signifies the Tree Builder to set the		| *
/* |    annotation-name string annotation on the current tree (in the	| */
/* |    same way it is done for comments)				| */
/* |    In our case the annotation is always 'scanner-warning'  	| */
/* ---------------------------------------------------------------------- */
// void ScannerWarning(msg) 
// char *msg;
// {
//     fprintf(yyout,"-9\nscanner-warning\n%s\n",msg);
// }

/* ---------------------------------------------------------------------- */
/* |	Initializes data						| */
/* |	Initialize LowerC[] : tables to translate char in lowercase	| */
/* |	Initialize KeyStart, KeyStop tables for fast acces in the	| */
/* |	Keywords table (KeyStart[i] = 1st keyword starting with ith 	| */
/* |	letter in the alphabet, KeyStop[i] =  last keyword starting with| */
/* |	ith letter in the alphabet).					| */
/* |	Initialize Dots tablefor .op. operator				| */
/* |	Initialize EntryPoints table					| */
/* ---------------------------------------------------------------------- */
int InitLexData()
{
	int	i;
	Keylist	*k;

	for(i = 0; i < 128; i++) {
	  if(i >= 'A' && i <= 'Z')
	    LowerC[i] = i + 32;
	  else LowerC[i] = i;
	}

	for(k = Keywords; k->name != nil; ++k) {
		i = k->name[0] - 'a';
		if(KeyStart[i] == nil)
			KeyStart[i] = k;
		KeyStop[i] = k;
		k->len = strlen(k->name);
	}
	for(k = Dots; k->name != nil; ++k) {
		k->len = strlen(k->name);
		if (BopStart == nil && k->name[0] == '=')
			BopStart = k;
	}
	for(k = EntryPoints; k->name != nil; ++k)
		k->len = strlen(k->name);
	for(k = HPF_Keywords; k->name != nil; ++k)
		k->len = strlen(k->name);
	LexDataInitialized = True;
}

/* ---------------------------------------------------------------------- */
/* Put a token in the token stack					  */
/* Its Value (when it has one) is  stored in the StrPtr buffer 		  */
/* ---------------------------------------------------------------------- */
void PutTokStack(code,value_index)
short code;					/* token code */
int value_index;{				/* token value */

/* printf("PUTTOKSTACK %i %s\n", code, StrPtr[value_index]) ; */
    if (TokSP < MAXTOK) {
	TokStack[TokSP].value_index = value_index;
	TokStack[TokSP].tok = code;
    } else LexError(True, "Internal table overflow");
    TokSP++;
}

/* ---------------------------------------------------------------------- */
/* |	Send the comments to the yacc engine    			| */
/* |    with the format "-1\n%d\n%s\n",len,str" and -2 at each end of 	| */
/* |	line.								| */
/* |    The comments are stored in the comment stack			| */
/* |"									| */
/* ---------------------------------------------------------------------- */
int FlushComments()
{
	int i,len;
	
	if ( SkipComments == False && (EndOffset == 0 || offset < EndOffset)) {
	    for(i=0; i<CommentSP; i++) {
		len = CommentStack[i];
		if (--readLines >= 0) {LEXLINE();}
		/* Filter the token sending to yacc : Header Parser*/
		if (Sub_Unit==False || Tags_Header==True) {
		  COMMENT(StrLen[len],StrPtr[len]);
                }
	    }
	}
	CommentSP = 0;
	return i;
}

/* ---------------------------------------------------------------------- */
/* |	Immediately Send the comments to the yacc engine   		| */
/* |	the comments are stored in the current line, we just saw a	| */
/* |	SPEC_COMM							| */
/* |    next_ch(in) : the current line just after a SPEC_COMM		| */
/* |	see the format in the FlushComments function			| */
/* |    before(in) : decides wether the LEXLINE must be sent before or  | */
/* |                 after COMMENT					| */
/* ---------------------------------------------------------------------- */
int ImmediateFlushComments(next_ch,lexline,before)
char *next_ch;
int  lexline,before;
{
	int i = GetStrCnt(next_ch);
	if ( SkipComments == False && (EndOffset == 0 || offset < EndOffset)) {
	    /* Filter the token sending to yacc : Header Parser*/
            if (lexline && before && --readLines >= 0) {++curInputLineNo; LEXLINE();}
	    if (Sub_Unit==False || Tags_Header==True) {
		COMMENT(StrLen[i],StrPtr[i]);
            }
	    if (lexline && !before && --readLines >= 0) {++curInputLineNo; LEXLINE();}
	}
}

/* ----------------------------------------------------------------------- */
/* |	Process Fix Format comment found during line parsing (GetToken)  | */
/* |    at the next_ch(in) current char which is equal to SPEC_COMM	 | */
/* |    In general it corresponds to '!' comments or 'C' comments in     | */
/* |    between line continuation.					 | */
/* |    '!' or 'C' comments are identifed by the first comment char      | */
/* |    stored in the StrPtr table. The COMMENT macro does not send the  | */
/* |    first comment char, it is set by ppml when unparsing		 | */
/* |    '!' comments are sent immediately to be placed in postfix in tree| */
/* |    except when tokstack(in) is True, in this case they are put in   | */
/* |    TokStack to be sent at next GetToken.
/* |    'C' comments are stored in CommentStack to be sent later.        | */
/* ----------------------------------------------------------------------- */

int ProcessFixComment(next_ch,tokstack)
char *next_ch;
int  tokstack;
{
    int index = GetStrCnt(next_ch+1);
    if (*(StrPtr[index]) == '!') { /* Immediate comment */
	if (tokstack) {
	    PutTokStack(SPEC_COMM,index);
	} else {
	    ImmediateFlushComments(next_ch+1,1,0);
	}
    } else {	           /* store the comments */
	CommentStack[CommentSP++] = index;
    }		    
}
/* ---------------------------------------------------------------------- */
/* |	Set the Dialect used by the lexer				| */
/* ---------------------------------------------------------------------- */
void setScannerDialect(dialect)
     char *dialect;
{
    if (dialect) {
	if (0 == strcmp(dialect,GOULD_DIALECT)) {
	    GouldFortran = True;
	    NeedGouldColon = True;
	} else
	  if (0 == strcmp(dialect,SUN_DIALECT)) padToEol = True;
	  else
	    if (0 == strcmp(dialect,FLC90_DIALECT)) Flc90 = True;
	    else
	      if (0 == strcmp(dialect,VAX_DIALECT)) {
		  padToEol = False; 
	      }
    }
}

void setScannerFreeFormat(int val) {
  if (val) {
    FreeFormat = True;
    if (Max_Col_Nb == 72)
      Max_Col_Nb = 132;
  } else
    FreeFormat = False;
}

void setScannerOpenMP(int val) {
  parseOpenMP = (val ? True : False) ;
}

void setScannerLineSize(char *strsize) {
    if (strsize) {
	Max_Col_Nb = atoi(strsize);
	if (Max_Col_Nb == 0) {
	    if (FreeFormat)
		Max_Col_Nb = 132; /* default */
	    else
		Max_Col_Nb = 72; /* default */
	}
    } else { Max_Col_Nb = 72; }
}

/* ---------------------------------------------------------------------- */
/* |	Set the Phylum used by the lexer				| */
/* ---------------------------------------------------------------------- */
static void SetPhylum(phylum)
     char* phylum;
{
    char *val;
    int len;
    INIT_NEXTCH();
    /* pretend we have read phylum in nextch */
    for (val = phylum; *val ; val++) { HAVE(LowerC[*val]); }
    EntryPt = GetKey(EntryPoints,nil,&len);
    if (EntryPt != -1) {
	InEntryPt = True;
	/* PutTokStack(EntryPt,-1); */ /* not handled this way, see line 3006 */
    }
}
extern char *getenv();

#include "type.h"
parserOption parserOptions[MAX_PARSER_OPTIONS] = { NULL , NULL };

/* ---------------------------------------------------------------------- */
/* |    Print the list of the scanning options                          | */
/* ---------------------------------------------------------------------- */
/* 
void
PrintOptions(options)
parserOption *options;
{
    parserOption *iter_option = options;
    while (iter_option && iter_option->name!=NULL) 
      {
        printf("Nom de l'option : %s , Valeur : %s \n", 
               iter_option->name, 
               iter_option->val);
        iter_option ++ ;
      }
} */

/* ---------------------------------------------------------------------- */
/* |	SetScannerOptions(optionsVect)                                  | */
/* | Set options used by the lexer from options parsed in protocol	| */
/* | SIDE EFFECTS: Sets the following globals:				| */
/* |  padToEol, GouldFortran             				| */
/* |  Max_Col_Nb, EntryPt, InEntryPt, offset, EndOffset 		| */
/* | Options are reset to default or protocol values before each parse. | */
/* ---------------------------------------------------------------------- */
static void
SetScannerOptions()
{
    long startOffset;
    parserOption *option = parserOptions;
    /* PrintOptions(option); */
    /* Set options default values */

    /* reset dialect default values */
    NeedGouldColon = False;
    padToEol = True;
    
    /* !!! remove the getenv when done with options */
    /*
    setScannerDialect(getenv("FORTRANDIALECT"));
    setScannerLineSize(getenv("FORTRANLINESIZE"));
    */
    offset = -1; EndOffset=0;
    SkipComments = False;
	
    InEntryPt = False;		/* a priori not in an Entry Point */
    LastEntryPt = False;
    EntryPt = -1;
    /* Set scanner options from parser protocol optionsVect */
    while (option && option->name!=NULL) {
	if(STREQ(option->name, "FORTRANDIALECT")) {
	    setScannerDialect(option->val);
	} else if (STREQ(option->name, "free_format")) {
	  setScannerFreeFormat((STREQ(option->val,"()"))?0:1) ;
#ifdef SHOWTOKENS
	  if (showtokens)
	    fprintf(stderr,"Parsing in free format with %d cols\n",Max_Col_Nb);
#endif
	} else if (STREQ(option->name, "struct_vax")
		   && STREQ (option->val,"()")) {
	    StructVax = False;
	} else if (STREQ(option->name, "FORTRANLINESIZE")) {
	    setScannerLineSize(option->val);
	} else if (STREQ(option->name, "startoffset")) {
	    long startOffset;
	    int status;
	    startOffset = atol(option->val);
	    if (startOffset>0 && yyin!=stdin) {
		status = fseek(yyin,startOffset,0);
		if (status==0)  {
		  offset     = startOffset - 1; /* offset OK */
		  UnitOffset = startOffset ;
		}
	    }
	} else if (STREQ(option->name, "endoffset")) {
	    EndOffset = atol(option->val);
	    if(EndOffset<=0) EndOffset = 0;
	} else if (STREQ(option->name, "phylum")) {
	    SetPhylum(option->val);
	} else if (   STREQ(option->name, "skipComments")
		   && ! STREQ (option->val,"()")) {
	    SkipComments = True;
	} else if (STREQ(option->name, "padToEol")) {
	    if (STREQ (option->val,"()")) {
		padToEol = False;
	    } else {
		padToEol = True;
	    }
	}else {
          //fprintf(stderr,"unknown option:%s=%s\n",option->name,option->val);
	}
	option++ ;
    }
}

/* Init the global Hints structure, side-effect on globals*/
void InitSTable(level)
   int level; 
{
   pSTable = level;
   STable[level].GotEqual = False;
   STable[level].GotDblColon = False;
   STable[level].GotComma = False;
   STable[level].CurLabel = -1;
}

/* ---------------------------------------------------------------------- */
/* |	Start Up the Lexer 						| */
/* ---------------------------------------------------------------------- */
int InitLexer()
{
  /*debugfile = fopen("./lexertrace", "a") ;*/

        if (!LexDataInitialized) InitLexData();
	
	offset = -1;
	StatementOffset = 0;
        UnitOffset = 0;
	StructVax = False;
	CurTypeDir = 0;
	/* [llh] already set when analyzing fortranParser's command line argument:
	FreeFormat = False;
	*/
	SetScannerOptions();
	Infile = yyin;

	/* Initialise the yylex function according to the wanted parsing */
	/* if (Header_Parser == True)	
	      yylex = H_Pyylex;
	   else if (Body_Parser == True)
	          yylex = B_Pyylex; 
	   else */
	yylex = Fyylex;


/*	Infname = "stdin"; */
        ErrorFlag = 0;
	readLines = 0;
        curInputLineNo = 1;

	StrCnt = 0;
	StrIns = StrBuf;
        INIT_NEXTCH();
	Ungetnb = 0;

	MainKey = KW_NULL; /* no main key at the beginning */
	LxStatus = ST_BEGIN;
	NextLabel = -1;
	LastToken = -1;
	TokSP = 0;
	TokBP = 0;
	CommentSP = 0;
	DoDepth = 0;
	ClosingCount = 0;

	IntOnly = False;
	InIoctl = False;
	SizeCommactl = False;
	OptCommactl = False;
	InDeclctl = False;
	Need_Kwd = False;
	Opt_Need_Kwd = False;
	Need_Op_Kwd = False;
	Need_Offset = NO;
        InImplicit = False;
        Need_Type_Kwd = False;
	Common_Flag = False;
	Do_While_Kwd = False;
	NoEOLToken = False;
	InitSTable(0);
        InString_or_Holl = False;
	/* Scan the label part of first line */
	GET();
	if (InEntryPt) {
	    switch(EntryPt) {
                case EN_Unit:		/* Parsing starts with a Scan1to6 */
                case EN_OptHeader:		
		case EN_Decls:
		case EN_Decl:
                case EN_Stats:
                case EN_Stat:
	        case EN_IncludeFile:
                        break;
                default:
                        /* Stat. directly read with GetFullLine */
                        /* On the first char of the line */
			return True;
		    }
	}
	if (FreeFormat)	  {
	    if (!ScanStartFreeStatement(False, 1)) {
		LexError(True, "Illegal (free) continuation line");
		ErrorFlag = True; 
	    }
	} else { /* Fixed Format */
	    if(!Scan1to6(False)) {
		LexError(True, "Illegal (fixed) continuation line");
		ErrorFlag = True; 
	    }
	}
	/* Usually, all comments encountered while reading a line are        */
	/* stored in the commentstack and will be sent after the currentline,*/
	/* in order to be attached as prefix to the next line. However, this */
	/* does not work for the first comments of a file which must be      */
	/* attached to the first line                                        */
	/* GetToken(); */
	/* FlushComments(); */

	return True;
}

/* ---------------------------------------------------------------------- */
/* |	Open a file as input	(OBSOLETE)				| */
/* ---------------------------------------------------------------------- */
int	NewInput(fname)
char	*fname;
{
	FSTACK	*fs;
	FILE	*newfp;

	newfp = fopen(fname,"r");
	if(newfp == nil) {
		LexError(True, "Cannot open file %s",fname);
		return False;
	}
	if(Infile != nil) {
		/* save current file context */
		if(FStackP >= MAXFILE) {
			LexError(True, "Includes nested too deep. Cannot open %s",fname);
			return False;
		}
		++FStackP;
		fs = FileStack + FStackP;
		fs->fp = Infile;
		fs->lineno = curInputLineNo;
		strcpy(fs->name,Infname);
	}
	/* create new file context */
	Infile = newfp;
	Infname = fname;
	curInputLineNo = 1;
	GET();
	return True;
}


/* ---------------------------------------------------------------------- */
/* |	Close an input file: returns True if it was the top level file	| */
/* ---------------------------------------------------------------------- */
int	PopInput()
{
	FSTACK	*fs;

	if(Infile != nil)
		fclose(Infile);

	if(FStackP < 0)
		return True;

	fs = FileStack + FStackP;
	Infile = fs->fp;
	curInputLineNo = fs->lineno;
	Infname = fs->name;
	FStackP--;
	return False;
}

/* ---------------------------------------------------------------------- */
/* |	CompactStrEq(compact,str,len) --> real length			| */
/* |	<compact> is a compact buffer with whitespaces removed		| */
/* |	and embedded SPEC_COMM and SPEC_HOLL followed by 2 bytes	| */
/* |	<str> is an ordinary string, 					| */
/* |	<len> is the number of char to compare				| */
/* ---------------------------------------------------------------------- */

#define SKIPCOMSPEC(p) while (*(p)==SPEC_COMM) (p)+=3 

#define ISSPECCHAR(p) (   (*(p)==SPEC_COMM)  \
		       && (*((p)-2)!=SPEC_COMM) \
		       && (*((p)-2)!=SPEC_HOLL))

#define ISSPECCOM(p) (   (*(p)==SPEC_COMM)  \
		       && (*((p)-2)!=SPEC_COMM) \
		       && (*((p)-2)!=SPEC_STRING) \
		       && (*((p)-2)!=SPEC_HOLL))

#define ISSPECC(p) (   ((*(p)==SPEC_HOLL) || (*(p)==SPEC_STRING))  \
		       && (*((p)-2)!=SPEC_COMM) \
		       && (*((p)-2)!=SPEC_HOLL))

int CompactStrEq(compact, str, len)
     char *compact;
     char *str;
     int len;
{
    char *bufp = compact;

    while (len ) {
	len-- ;
	SKIPCOMSPEC(bufp);
        if (FreeFormat && 
	    *str == ' ' && *bufp == ' ') {/* in free format " SPEC_COMM " is equivalent to " " */
	  str++;
	  while (*bufp == ' ' || *bufp == SPEC_COMM) {
	    if (*bufp == ' ') { bufp++; }
	    else if (*bufp == SPEC_COMM) { bufp += 3; }
	  }
	} else {
	  if (*bufp++ != *str++)
	    return False;
	}
    }
    return (bufp - compact);
}

/* ---------------------------------------------------------------------- */
/* Recreate a hollerith slice from something which was read as a string   */
/* recreated as  yyleng'H'yytext (by the way yyleng modified)             */
/* The slice rebuilt is given with start, endh : for instance		  */
/* starting with Hollerith 10H1234567890       				  */
/* reprint_holl(0,1) returns yyleng = 2 and yytext = "10"		  */
/* reprint_holl(0,-1) means reprint the whole hollerith			  */
/* ---------------------------------------------------------------------- */
void reprint_holl(begin,endh)
int begin,endh;
{
    int i;
    int l;
    char number[MAXTOKLEN];

    sprintf(number,"%d",yyleng);
    l = strlen(number) + 1;  /* room for yyleng and H */

    for (i = yyleng - 1; i > -1 ;i--) yytext[i+l] = yytext[i];
    for (i = 0; i<l-1; i++) yytext[i] = number[i];
    yytext[i] = HOLLERITH_CHAR;
    /* now get the slice */
    if (begin != 0) 
	strcpy(yytext, yytext+begin);
    if (endh != -1) {
	yyleng = endh - begin + 1;
    } else {
	yyleng += l-begin;
    }
    yytext[yyleng] = '\0';
}

/* ---------------------------------------------------------------------- */
/* Put the value of a token (stored in StrPtr) in yytext, yyleng	  */
/* If concat == True, we concat the value to yytext                       */
/* ---------------------------------------------------------------------- */
void GetTokenValue(index, concat)
    int index, concat;
{
  if (concat) {
    yyleng = StrLen[index] + yyleng;
    strncat(yytext,StrPtr[index],StrLen[index]);
    yytext[yyleng] = '\0';
  } else {
    yyleng = StrLen[index];
    strncpy(yytext,StrPtr[index],yyleng);
    yytext[yyleng] = '\0';
  }
}

/* ---------------------------------------------------------------------- */
/* | Finish a current Token contained in StrPtr and put in Tok Stack 	| */
/* ---------------------------------------------------------------------- */
void FinishToken(short Token) {
    StrLen[StrCnt] = StrIns - StrPtr[StrCnt];
    STORE(0);
    PutTokStack(Token,StrCnt);
    StrCnt++;
}

/* ---------------------------------------------------------------------- */
/* | Build a token of type String in the StrPtr stack		        | */
/* |  starting with the string "s" and its  "nb" chars     		| */
/* | The value of the token is put in the StrPtr table at index StrCnt  | */
/* | the length of the token is put in the StrLen table " "     "    "  | */
/* | if "realchar" we consider that we can look in the Realbuf to get   | */
/* | the real chars instead of "s" chars. It also does not rebuild      | */
/* | string with their starting \" -> this only when this function	| */
/* | is called with "s" == nextch					| */
/* | nb is always > 0							| */
/* | Token is the token type rebuilt                                    | */
/* ---------------------------------------------------------------------- */
void ReBuildTokString(char *s, int nb, int realchar, short Token) {
	char *str,*deb;
	int strpos;
	int i,len;
	short currentToken = Token; /* the current processed token */
	Boolean islastSPEC = False; /* Is last char == (SPEC_HOLL or STRING) */

	if (Token == FORMATELEM && 
	    (*(s-1) == SPEC_HOLL || *(s-2) == SPEC_HOLL)) {
	    /* we are in the middle of an Holl. before H */
	    /* was reduced at the previous ReBuildTokString */
	    if (*(s-1) == SPEC_HOLL) {
		s += 2; nb -= 2;
	    } else {
		s ++; nb--;
	    }
	    if (nb == 0) return; /* finish */
	}
	    
	if (Token != FORMATELEM) Newstring();
	
	deb = s;

	while (nb) {
	    switch (*s) {
	    case SPEC_COMM:
		if (FreeFormat) { /* store the comments in tok stack */
		    PutTokStack(SPEC_COMM,GetStrCnt(s+1));
		} else { /* Analyse comment type, '!' are sent in tok stack */
		    ProcessFixComment(s,1);
		}		    
		s += 2;			 
		nb -=2;
		break;

            case SPEC_STRING : /* rebuild the string */
		if (Token == FORMATELEM) { /* Hollerith in FORMAT */
		    if (deb != s) { /* not the begining, finish current */
			FinishToken(currentToken);
		    }
		    islastSPEC = True;
		    /* rebuild the string */
		    Newstring();
		    strpos = GetStrCnt(s+1);
		    for (str = StrPtr[strpos] ; *str ; str++) STORE(*str);
		    currentToken = ASTRING;
		} else { /* not in FORMAT */
		    /* rebuild the string */
		    strpos = GetStrCnt(s+1);
		    if (!realchar) STORE('"');
		    for (str = StrPtr[strpos] ; *str ; str++) {
			if ( *str == '\"')  STORE('\"');
			STORE(*str);
		    }
		    if (!realchar)STORE('"');
		}
		s += 2; nb -= 2;
		break;

	    case SPEC_HOLL : /* rebuild the Hollerith */
		if (Token == FORMATELEM) {/* Hollerith in FORMAT */
		    if (deb != s) { /* not the begining, finish current */
			FinishToken(currentToken);
		    }
		    islastSPEC = True;
		    /* and rebuild a HOLLERTIH */
		    strpos = GetStrCnt(s+1);
		    GetTokenValue(strpos, False);
		    reprint_holl(0,-1);	/* put the holl. in yytext - yyleng */
		    if (nb <= 2) { /* Hollerith slice before H!! */
			nb = 1;    /* finish */
		    } else {
			s += 2; nb -= 2;
		    }
		    Newstring();
		    for (i = 0 ; i<yyleng ; i++) STORE(yytext[i]);
		    currentToken = HOLLERITH;
		} else { /* not in FORMAT */
		    /* rebuild the Hollerith */
		    strpos = GetStrCnt(s+1);
		    GetTokenValue(strpos, False);
		    reprint_holl(0,-1);	/* put the holl. in yytext - yyleng */
		    s += 2; nb -= 2;
		    for (i = 0 ; i<yyleng ; i++) STORE(yytext[i]);
		}
		break;

	    default : /* take the char ! */
	        if (realchar) {
		    STORE(Realnextch[s - nextch]);
	        } else { /* FORMATELEM */
		    if (islastSPEC == True) {
			FinishToken(currentToken);
		    }
		    if (deb == s || islastSPEC == True) {
			Newstring();
			currentToken = Token;
		    }
		    islastSPEC = False;
		    STORE(*s);
		}
		break;
	    }
	    nb--;
	    s++;
        }
	FinishToken(currentToken);
    }

/* ---------------------------------------------------------------------- */
/* Get a token from the token stack					  */
/*  Its Value (when it has one) was  stored in the StrPtr buffer and is	  */
/*  put in yytext - yyleng 						  */
/*  When the token is SPEC_COMM, send comments before returning the token.*/
/* return the token code						  */
/* ---------------------------------------------------------------------- */
int GetTokStack() {
    int index,token;

    token = TokStack[TokBP].tok;
    index = TokStack[TokBP].value_index;
    if (token == SPEC_COMM) { /* comment token, flush the comments */
	if ( SkipComments == False && (EndOffset == 0 || offset < EndOffset)) {
	    COMMENT(StrLen[index],StrPtr[index]);
	    if (--readLines >= 0) {++curInputLineNo; LEXLINE();}
	}
	TokBP++;
	if (TokSP > TokBP)  /* still something in the Token Stack */
	    token = GetTokStack();
        else token = -1; /* means no token only COMM */
    } else { /* other token */
	if (index >= 0)
	    GetTokenValue(index, False);
	TokBP++;
    }
    return token;
}

/* ---------------------------------------------------------------------- */
/* | We are Building a SPEC_STRING a Newstring is open, and the current | */
/* | char is '\n'							| */
/* | stringchar is the delimiter character used (" or ')		| */
/* |  Padd with ' ' multi-line hollerith in SUN (default) .	 	| */
/* |  Cut just after '\n' in VAX.					| */
/* | return False when lince continuation				| */
/* ---------------------------------------------------------------------- */
int GetMultiLineString(char stringchar) {
    int stop, i;

    if (padToEol) {
	/* padd with ' ' */
	for (i = Max_Col_Nb-Ccol+1 ; i > 0 ; i--) 
	    STORE(' ');
    } /* Vax cut on '\n' */
    Endstring(SPEC_STRING);
    InString_or_Holl = True;
    stop = Scan1to6(False);
    InString_or_Holl = False;
    if (stop) {/* A line continuation is expected */
	ErrorFlag = 1;
	yyerror();
    } else  {
	Cc = stringchar; /* start a new astring */
	nlStack[nlSP++] = lastch;
    }	   /* memorize the continuation line */
    return stop;
}

/* ---------------------------------------------------------------------- */
/* | The version of GetMultiLineString for the free format.             | */
/* | if a character context is to be continued, the '&' shall be the    | */
/* | last nonblank character on the line and shall not be followed by	| */
/* | commentary. An '&' shall be the first nonblank character on the 	| */
/* | next line that is not a comment line and the statment continues	| */
/* | with the next character following the '&'.                 	| */
/* | Return False if the string is malformed.                       	| */
/* ---------------------------------------------------------------------- */
int FreeFormatGetMultiLineString(char stringchar) {
    int stop, i, cpt;

    /* we test if there are nothing after the continuation character */
    if (Ccol > Max_Col_Nb) { /* The string is too long */
	Endstring(SPEC_STRING);
	ErrorFlag = 1;
	yyerror();
	return False;
    }
    cpt=1; Ccol++; GET();
    while (Ccol <= Max_Col_Nb &&
	   (Cc == ' ' || Cc == '\t')) {
	GET(); cpt++; Ccol++;
      }
    if (Cc=='\n' || Cc=='\r' || Ccol > Max_Col_Nb) {
	Endstring(SPEC_STRING);
    }
    if (Ccol > Max_Col_Nb) {
	if( Keep_Line_Trailer ) { /* Get as comm. */
	    UNGET(Cc);
	    GetComment('!');
	} else { /* skip to end of line (or file) */
	    while (Cc!='\n' && Cc!='\r' && Cc!=EOF) { GET(); }
	}
    }
    if (Cc == EOF) return False;
    if (Cc=='\n' || Cc=='\r' || Ccol > Max_Col_Nb) { /* we look at the next string */
      stop = ScanStartFreeStatement(True, 1);
	if (stop) {/* A line continuation is expected */
	  ErrorFlag = 1;
	  yyerror();
	} else  {
	  Cc = stringchar; /* start a new astring */
	  nlStack[nlSP++] = lastch;
	}	   /* memorize the continuation line */
      } else {
	UNGET(Cc);
	for (i = 1; i<cpt; i++) UNGET(' ');
	Ccol = Ccol - cpt;
	Cc = '&';
	stop = False;
      }
    return stop;
}

/* ------------------------------------------------------------------------ */
/* | Read an Hollerith from input and store it in the nextch buffer       | */
/* | the header part is already read (i.e the part before H (included))   | */
/* |  count corresponds to the number of character contained in the hol.  | */
/* |  Padd with ' ' multi-line hollerith in SUN (default) .		  | */
/* |  Cut just after '\n' in VAX.					  | */
/* ------------------------------------------------------------------------ */
void GetFullHollerith(int count) {
  int i,stop;

  while( count > 0) {
      GET();
      Ccol++;
      if (Ccol>Max_Col_Nb && Cc != '\n') continue; /* skip after column 72 */
      if (Cc != '\n' && Cc != -1) {
	  STORE(Cc); count--;
      } else 
	  if (Cc == '\n') {
	      if (! padToEol || Ccol > Max_Col_Nb) {
		  /* save string table state (in case of comments in Scan1to6) */
 		  int savecnt = StrCnt; char *saveins = StrIns; StrCnt++;
                  InString_or_Holl = True;
		  stop=Scan1to6(False);
                  InString_or_Holl = False;
		  /* restore prev. values, ignore comments in hollerith */
		  StrCnt = savecnt; StrIns = saveins;
		  if (stop == True) { 
		      /* no line continuation, stop reading  */
		      if (count) { /* bad count => Error */
			  ErrorFlag = 1;
			  yyerror();
		      }
		  } else {
		      nlStack[nlSP++] = lastch;
		  }
	      } else { /* default : padToEol */
		  if ((Max_Col_Nb-Ccol) < count) { 
		      /* fill with blank until '\n' */
		      for (i = Max_Col_Nb-Ccol+1 ; i > 0 ; i--) {
			  STORE(' '); count--;
		      }
		      /* continue holl. after '\n'  and continuation line*/
                      InString_or_Holl = True;
		      if (True == Scan1to6(False)) { 
			  /* no line continuation, stop reading  */
			  if (count) { /* bad count => Error */
			      ErrorFlag = 1;
			      yyerror();
			  }
		      } else {
			  nlStack[nlSP++] = lastch;
		      }
                      InString_or_Holl = False;
		  } else {		/* fill with blank */
		      for( ; count > 0; count--) STORE(' ');
		      UNGET(Cc);
		  }
	      }
	  } else {			/* Cc = -1 */
	      /* EOF terminates an  */
	      /* hollerith constant */
	      UNGET(Cc);
	  }
  }
}

/* ---------------------------------------------------------------------- */
/* | ProcessGouldInclude : for special Gould Fortran Include		| */
/* |  Take all the stuff after the INCLUDE Kwd as a long string		| */
/* | nextch is on the 'i' of INCLUDE					| */
/* | return the INCLUDE token						| */
/* ---------------------------------------------------------------------- */
int ProcessGouldInclude() {
    if (lastch - nextch > 0) {
	/* Verify we just have a SPEC_STRING */
	if (*nextch != SPEC_STRING || (nextch+3 < lastch)) {
	    /* bad Gould Include : parse the whole line, make string with it */
	    nextch += 7;
	    Realnextch += 7;
	    ReBuildTokString(nextch,lastch-nextch,True,ASTRING);
	    nextch = lastch; /* finish the line */
	}
    }
    return KW_INCLUDE;
}

/* ---------------------------------------------------------------------- */
/* |	VerifyGouldInline : for Gould Fortran INLINE Kwd 		| */
/* |     All the following line are considered as comments until "ENDI"	| */
/* |    Cc is on the first char of the line 				| */
/* |    return FALSE when continuing within INLINE			| */
/* |    return TRUE when end of INLINE (ENDI found)			| */
/* ---------------------------------------------------------------------- */
int VerifyGouldInline() {
    char startline[128];
    char realline[128];
    int col = 1;
    int charstop = 0;
    int endline = 0;
    int i;

    if(Cc == EOF) {
	UNGET((char)Cc);
	return True;
    }
    if(Cc <= 0) LexError(True, "Negative char %d\n",Cc);

    while (col < Max_Col_Nb && charstop != 4 && endline == 0) {
	realline[col-1] = (char)Cc;
	switch ((char)Cc) {
	  case '\n':
	    /* blank line continue within INLINE */
	    endline = 1;
	    break;
	  case ' ':
	  case '\t' :
	    break ;	/* skipspace */
	  default:
	    startline[charstop] = (char)Cc;
	    charstop++;
	    
	}
	col++;
	GET();
    }
    /* more than Max_Col_Nb chars, end of inline ... */
    if (col == Max_Col_Nb) {
	return True;
    }
    /* now process the startline */
    if (charstop == 4) {
	/* we read 4 significant chars, is it endi ? */
	UNGET((char)Cc);
	while (col-1) { UNGET(realline[col-2]) ; col--;}
	if (0 == strncasecmp(startline,"endi",4)) {
	    /* end of inline */
	    return True;
	}
	/* It is Inline elem */
	GetComment('C');
	return VerifyGouldInline();
    } else {
	if (endline) {
	    if (charstop) {
		/* small Inline elem with significative characters */
		UNGET((char)Cc);
		while (col-1) { UNGET(realline[col-2]) ; col--;}
		GetComment('C');
	    }
	    return VerifyGouldInline(); /* try again */
	}
    }
}

/* ---------------------------------------------------------------------- */
/* |	Create a new string						| */
/* ---------------------------------------------------------------------- */
void Newstring() {
	if(StrCnt >= MAXSTR) {
		LexError(True, "Internal table overflow");
	} else {
        	StrPtr[StrCnt] = StrIns;
	}
}

/* ---------------------------------------------------------------------- */
/* |	Terminate a string						| */
/* ---------------------------------------------------------------------- */
void Endstring(char code) {

	StrLen[StrCnt] = StrIns - StrPtr[StrCnt];
	HAVE(code);
	HAVE(StrCnt/256); /* StrCnt coded on two bytes must be < MAXSTR */
	HAVE(StrCnt%256);
	STORE(0);	  /* add null character at the end of string */
	StrCnt++;
}

/* ---------------------------------------------------------------------- */
/* |	read a comment till end of line					| */
/* |    store comment_char as the first char of the comment		| */
/* ---------------------------------------------------------------------- */
void GetComment(char comment_char) {
	GET();
	Newstring();
	STORE(comment_char);
	for( ; Cc != '\n' && Cc != EOF; ) {
		STORE(Cc);
		GET();
	}
	if(Cc == '\n') GET();
	Endstring(SPEC_COMM);
}

/* Look whether  contains the PROTOCOL_ENDDATA to end the data transmission */
Boolean   end_of_data() {
    char    *end_data;         /* look for [ENDDATA] sequence : protocol */
    int len;

    GET();			/* get the '[' */
    end_data = lastch;
    for( ; Cc != ']' && Cc != EOF ; ) {
	HAVE(LowerC[Cc]);
	GET();
    }
    /* Look for "enddata" delimiter (end of data transmission) */
     if (strncmp(end_data, PROTOCOL_ENDDATA, 7) == 0) {
	GET();
	if (Cc == '\n' || Cc == EOF) {
            lastch = end_data ; /* restore state */
	    Cc = EOF;		/* Terminate the Scanning once the last */
	    return True;	/* line is processed                    */
	}
	UNGET(Cc);
    }
    /* restore state */
    while (lastch>end_data) { UNGET(*lastch); lastch--; }
    return False;
}

/** Returns true iff the next chars form a number. Should not modify Cc nor future text to come. */
int followedByNumber() {
  int found = 0 ;
  char storedCc = Cc ;
  char    *end_data = lastch;
  GET() ;
  while (Cc==' ' || Cc=='\t') {
    HAVE(Cc) ; GET() ;
  }
  if (Cc>='0' && Cc<='9') {
    found = 1 ;
    HAVE(Cc) ; GET() ;
    while (Cc>='0' && Cc<='9') {
      HAVE(Cc) ; GET() ;
    }
    if (Cc==' ' || Cc=='\t' || Cc=='\n' || Cc=='\r') {
      found = 1 ;
    } else {
      found = 0 ;
    }
  }
  UNGET(Cc) ;
  /* restore state */
  while (lastch>end_data) { UNGET(*(--lastch)); }
  Cc = storedCc ;
  return found ;
}

/** Returns true iff the next chars in "text" are "end", case independent,
 * and even if there are white spaces in the middle */
int followedByEND(char *line, int* toj) {
  while (isspace(Cc)) {GET() ; line[(*toj)++] = Cc;}
  if (Cc!='e' && Cc!='E') return False ; else {GET() ; line[(*toj)++] = Cc;} ;
  while (isspace(Cc)) {GET() ; line[(*toj)++] = Cc;}
  if (Cc!='n' && Cc!='N') return False ; else {GET() ; line[(*toj)++] = Cc;} ;
  while (isspace(Cc)) {GET() ; line[(*toj)++] = Cc;}
  if (Cc!='d' && Cc!='D') return False ; else return True ;
}

/* ---------------------------------------------------------------------- */
/* |	 Directive (DirC) --> int					| */
/* |     DirC is the first char after the comment char identifying the  | */
/* |     directive (/ or % or $)                                        | */
/* |     Returns the sub-sort of OMP Directive found, which implies     | */
/* |     decision whether GetFullLine() should stop accumulating the    | */
/* |     coming text as the "next line" (i.e. into Inbuf)               | */
/* |     Return codes:                                                  | */
/* |      If finds an  "OMP   END ", returns 2  (=> don't stop)         | */
/* |      If finds an  "OMP+"      , returns -1 (=> don't stop)         | */
/* |      If finds an  "OMP "      , returns 1  (=> stop)               | */
/* |      else (=> comment coming!), returns 0  (=> don't stop)         | */
/* ---------------------------------------------------------------------- */
int Directive(char DirC) {
    int j = 0;
    int len=0;
    int result;
#define MAX_FLCDIRLEN 11
#define MAX_DIRLEN 17
    char line[MAX_DIRLEN];
    switch (DirC) {
      case '%' :
      case '/' : /* FLC Directive */
	GET() ; 
	while (isalpha(Cc) && j<MAX_FLCDIRLEN) {
	    line[j++] = Cc;     /* verify it is a directive */
	    GET();		/* list of alpha followed with '/' */
	}
	len = j;
	/* check that we have a FLC directive */
	if( ( len==3 && (   0==strncasecmp("cre",line,3)
			 || 0==strncasecmp("rel",line,3)
			 || 0==strncasecmp("uti",line,3)))
	   || len==9 &&     0==strncasecmp("parameter",line,9)
	   || len==7 &&     0==strncasecmp("realloc",line,7)) {
	    result = 1;
	    Cc = SPEC_DIR;
	    UNGET('/');
	    Ccol = 1;		/*  we have only GET() 1 char yet */
	} else {
	    result = 0;
            UNGET(Cc);
	}
	break;
      case '$' : /* "Standard" Directive */
        do {     /* get the directive keyword */
            GET() ; 
	    line[j++] = Cc;
	} while ((isalpha(Cc) || Cc=='_') && j<MAX_DIRLEN);
	len = j;
        if ( parseOpenMP && len-1==3 && 0==strncasecmp("omp",line,3)) {
          if (isspace(Cc)) {
            /* continue lookahead to detect if this $OMP is followed by "END" */
            if (followedByEND(line, &len)) {
              result = 2;
            } else
              result = 1;
          } else
            result = -1 ;
          Cc = SPEC_DIR;
          Ccol = 2;
        } else if (   ( (len-1)==4 && ( 0==strncasecmp("pred",line,4)
				 || 0==strncasecmp("fsys",line,4)))
/* [llh] the following would be a better handling of AD directives: */
/*             || ( (len-1)==2 && ( 0==strncasecmp("ad",line,2))) */
            || ( (len-1)==5 && ( 0==strncasecmp("avail",line,5)))
            || ( (len-1)==7 && ( 0==strncasecmp("purefun",line,7)))
            || ( (len-1)==8 && ( 0==strncasecmp("deadlist",line,8)
				 || 0==strncasecmp("deadvars",line,8)))
            || ( (len-1)==9 && ( 0==strncasecmp("injective",line,9)))
            || ( (len-1)==11 && ( 0==strncasecmp("global_pred",line,11)))
            || ( (len-1)==12 && ( 0==strncasecmp("nosideeffect",line,12)))
            || ( (len-1)==16 && ( 0==strncasecmp("global_injective",line,16)))) {
	    result = 1;
	    Cc = SPEC_DIR;
	    Ccol = 2;
	} else {
	    result = 0;
	}
	break;
      default :
        break;
    }
    /* push back all directive, will be eaten in GetFullLine, except that */
    /* in "C$OMP+blabla", we want the pointer to remain on blabla */
    if (result!=-1) {
      for (j=len-1 ; j>=0 ; j--) UNGET(line[j]);
    }

    if (result == 0) Cc = (DirC=='%' ? '/' : DirC) ;

    return result ;
}

/* ---------------------------------------------------------------------- */
/* | Input : We are on col. 6 of a FORTRAN statement, Cc (current char) | */
/* |         is a white char (' ' or '\t'), Ccol = 6.                   | */
/* | Role  : look for the first non white char (' ' or '\t') to look    | */
/* |         for ! cooments (eol comments).                             | */
/* | Output: return True if eol comments found, and false else          | */
/* | Side Effect : update the Ccol variable                             | */
/* ---------------------------------------------------------------------- */
int SearchForEolComments() {
    do {
	GET();
	Ccol++;
	if (Cc == '!' || Cc == '{') { /* '{' hp extension for eol comments */
	    Ccol = 6 ;
	    return True;
	} else if (!isspace(Cc)) {
	    return False;
	}
    } while (Cc != '\n');
    return False;
}

/* ---------------------------------------------------------------------- */
/* |	Test if we treat a HPF Directive                  		| */
/* ---------------------------------------------------------------------- */
int is_HPF_DIR () {
  int SaveCc = Cc;
  int SaveCc1, SaveCc2, SaveCc3;

  GET();
  if ( Cc == 'H' || Cc == 'h' ) 
    {
      SaveCc1 = Cc;
      GET();
      if ( Cc == 'P' || Cc == 'p' ) 
	{
	  SaveCc2 = Cc;
	  GET();
	  if ( Cc == 'F' || Cc == 'f' ) 
	    {
	      SaveCc3 = Cc;
	      GET();
	      if ( Cc == '$' ) 
		return True;
	      else
		{
		  UNGET(Cc);
		  UNGET(SaveCc3);
		  UNGET(SaveCc2);
		  UNGET(SaveCc1);
		}
	    }
	  else
	    {
	      UNGET(Cc);
	      UNGET(SaveCc2);
	      UNGET(SaveCc1);
	    }
	}
      else
	{
	  UNGET(Cc);
	  UNGET(SaveCc1);
	}
    }
  else
    UNGET(Cc);

  Cc = SaveCc;
  return False;
}

/** Similar test, for OMP Directive header.
 * Returns the sub-sort of OMP Directive found, which implies
 * decision whether accumulating coming line into Inbuf should stop.
 * Following convention of fixed-format equivalent "Directive()"      */
int is_OMP_DIR () {
  int SaveCc = Cc;
  int SaveCc0, SaveCc1, SaveCc2, SaveCc3, SaveCc4, SaveCc5, SaveCc6;
  GET();
  if ( Cc == '$' ) {
      SaveCc0 = Cc;
      GET();
      if ( Cc == 'O' || Cc == 'o' ) {
	  SaveCc1 = Cc;
	  GET();
	  if ( Cc == 'M' || Cc == 'm' ) {
	      SaveCc2 = Cc;
	      GET();
	      if ( Cc == 'P' || Cc == 'p' ) {
                SaveCc3 = Cc;
                int nbWhite = 0 ;
                GET() ;
                while ( Cc == ' ' ) {
                  ++nbWhite ;
                  GET() ;
                }
                if ( Cc == 'E' || Cc == 'e' ) {
                  SaveCc4 = Cc;
                  GET();
                  if ( Cc == 'N' || Cc == 'n' ) {
                    SaveCc5 = Cc;
                    GET();
                    if ( Cc == 'D' || Cc == 'd' ) {
                      UNGET(Cc);
                      UNGET(SaveCc5);
                      UNGET(SaveCc4);
                      while (nbWhite) {
                        UNGET(' ') ;
                        --nbWhite ;
                      }
                      UNGET(SaveCc3);
                      UNGET(SaveCc2);
                      UNGET(SaveCc1);
                      Cc = SaveCc;
                      return 2 ;
                    } else if (nbWhite) {
                      UNGET(Cc);
                      UNGET(SaveCc5);
                      UNGET(SaveCc4);
                      while (nbWhite) {
                        UNGET(' ') ;
                        --nbWhite ;
                      }
                      UNGET(SaveCc3);
                      UNGET(SaveCc2);
                      UNGET(SaveCc1);
                      Cc = SaveCc;
                      return 1 ;
                    } else {
                      UNGET(Cc);
                      UNGET(SaveCc5);
                      Cc = SaveCc;
                      return -1 ;
                    }
                  } else {
                    if (nbWhite) {
                      UNGET(Cc);
                      UNGET(SaveCc4);
                      while (nbWhite) {
                        UNGET(' ') ;
                        --nbWhite ;
                      }
                      UNGET(SaveCc3);
                      UNGET(SaveCc2);
                      UNGET(SaveCc1);
                      Cc = SaveCc;
                      return 1 ;
                    } else {
                      UNGET(Cc);
                      Cc = SaveCc;
                      return -1 ;
                    }
                  }
                } else {
                  if (nbWhite) {
                    UNGET(Cc);
                    while (nbWhite) {
                      UNGET(' ') ;
                      --nbWhite ;
                    }
                    UNGET(SaveCc3);
                    UNGET(SaveCc2);
                    UNGET(SaveCc1);
                    Cc = SaveCc;
                    return 1 ;
                  } else {
                    Cc = SaveCc;
                    return -1 ;
                  }
                }
              } else {
                UNGET(Cc);
                UNGET(SaveCc2);
                UNGET(SaveCc1);
                UNGET(SaveCc0);
              }
          } else {
            UNGET(Cc);
            UNGET(SaveCc1);
            UNGET(SaveCc0);
          }
      } else {
        UNGET(Cc);
        UNGET(SaveCc0);
      }
  } else
    UNGET(Cc);

  Cc = SaveCc;
  return 0;
}

/* ---------------------------------------------------------------------- */
/* |	Scan the begin of a statement in the free format       		| */
/* |	if we are not in a continued line (ContinuedLine == False)      | */
/* |	   - returns True -> we are at beginning of a new statement or	| */
/* |         directive;							| */
/* |	     After this, NextLabel is set and Cc is on first char of  	| */
/* |	     the next statement                                 	| */
/* |	if we are in a continued line (ContinuedLine == True)           | */
/* |	   if it is the continuation of a statment  (CurTypeDir == 0)   | */
/* |	      - returns True -> if the first non blank character of the | */
/* |            next line is a '&'    [VMP]: or a '\n' 			| */
/* |	      - returns False -> else                           	| */
/* |	   if it is the continuation of a directive  (CurTypeDir != 0)  | */
/* |	      - returns True -> if the next line is a continuation for  | */
/* |            directive                        			| */
/* |	      - returns False -> else                           	| */
/* |    In continued line we treat HPF directives as comment            | */
/* ---------------------------------------------------------------------- */

int ScanStartFreeStatement(ContinuedLine, StartCol)
     int ContinuedLine, StartCol;
{        
  int     stop, lab, i, ContinuedDir, InDir;
  int ompDetected = 0 ;

  ContinuedDir = CurTypeDir && ContinuedLine;
  InDir = False;
  NextLineTypeDir = 0;
  stop = False;
  StatementOffset = offset;
  Ccol = StartCol;
retry:
  switch (Cc) {
    case EOF:             /* End of File */
	return True;
	break;
    case 0x0b:            /* ^K Vertical TAB */
    case 0x0c:            /* ^L New Page  */
    case '\n':            /* empty line */
    case '\r':
	GET();
	Ccol = 1;
	goto retry;          
    case  '&':            /* continuation line in free format */
	if (ContinuedDir) {
	    GET();
	    Ccol++;
	    goto retry; 
	  } else {
            stop = !ContinuedLine ;
            break; 
	  }
    case '#':             /* CPP directive  */
    case '%':             /* CPP directive on HP */
    case '!': {              /* Comment or Directive ? */
      int isCPP = (Cc=='#'||Cc=='%') ;
      if (isCPP && !followedByNumber()) {  /* CPP directive, using '%' on HP */
        return True;
      } else {
      /* Directive is disabled. Unchecked these comments to enable the analysis
       of directives. */

/* for (int j=Ungetnb-1; j>=0 ; --j) fprintf(stderr, "   UNGETBUF[%i]=%c\n", j, Ungetbuf[j]) ; */
/* for (int j=0; j<3 ; ++j) {Cc=getc(Infile) ; fprintf(stderr, "      NEXTCH %i:%c\n", j, Cc);} ; */
	if (ContinuedLine && !ContinuedDir) {
	    /* It was a comment , fall through next case */
            GetComment(isCPP ? '#' : '!');
	    Ccol = 1;
	    goto retry; 
	} else if (parseOpenMP && (ompDetected=is_OMP_DIR())) {
          Cc = (ompDetected==-1||ompDetected==2?' ':SPEC_DIR) ;
          stop = (ompDetected==1);
          NextLineTypeDir = DIR_TYPE_OMP ;
          InDir = True;
	} else {
          GET();
          UNGET(Cc); Cc = (isCPP ? '#' : '!');
          /* Comment on next line, will be processed by next line */
          stop = True;
	}
      }
      break;
    }
    case '\t':            /* VAX Tab format */
	GET(); Ccol++;
	goto retry;
    case ';':       
	if (StartCol==1)
	  goto badchar; 
	else {
	    GET(); Ccol++;
	    goto retry; 
	}
    case '[':             /* check for entry point or enddata */
	if (end_of_data()) return True; 
	else goto badchar;
    case ' ':             /* Regular FORTRAN, at last */
	GET(); Ccol++;
	goto retry;
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
	if (!ContinuedLine && !InDir) {
	    lab = 0;
	    /* Scan label field */
	    while(isdigit(Cc)) {
		lab = 10 * lab + (Cc - '0') ;
		GET(); Ccol++;
	    }
	    if(Cc == '!') {
		GetComment('!');
		Ccol = 1;
		goto retry;
	    } 
	    if (Cc != ' ' && Cc != '\t') 
	      if (lab)
		goto badlabel;
	      else
		goto badchar;
	    
	    if (lab>0) NextLabel = lab;      

	    while (Cc == ' ' || Cc == '\t') { GET(); Ccol++; }
	    /* Now Cc is the char after the label */ 
          
	    if (Cc == '\n' && lab <= 0) {
		GET();      /* empty line */
		Ccol = 1;
		goto retry;      
	    }
	    stop = True;
	    break;
	}
    default:
      stop = (!ContinuedDir || NextLineTypeDir!=CurTypeDir) ;
      break;
    } /* end switch(Cc) on first char of line */

  return stop;

badlabel:
        LexError(True, "Need a Blank character after a label");
        ErrorFlag = True;
        return stop;
badchar:
        LexError(True, "Bad character in the beginning of the line");
        ErrorFlag = True;
        return stop;
}

/* ---------------------------------------------------------------------- */
/* |	In fixed format look for a label on current input		  */
/* | and update the current label global : STable[pSTable].CurLabel	  */
/* ---------------------------------------------------------------------- */
void GetNextLabel() {
    int labval = 0;
    int inLabel = True;
    do {
	GET();
	switch (Cc) {
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
	    labval = 10 * labval + (Cc - '0') ;
	    Ccol++;
	    break;
	case '\t': case ' ':
	    Ccol++;
	    break;
	default :
	    inLabel = False;
	    break;
	}
    } while(inLabel);
    UNGET(Cc); /* unget last */
    if (labval) STable[pSTable].CurLabel = labval;
}

/* ---------------------------------------------------------------------- */
/* |	Scan columns 1 to 6 of a line :					| */
/* |	returns True if we are at beginning of a new statement or	| */
/* |    directive;							| */
/* |	returns False if we are on a comment or continuation line;	| */
/* |	After this, NextLabel is set and Cc is on first char of the	| */
/* |	next statement							| */
/* |    When Inline == True means we are within an Inline statement :   | */
/* |     all line which starts at column 1 with non blank must be con-  | */
/* |     as continuation line (and Ccol == 1).				| */
/* ---------------------------------------------------------------------- */
int Scan1to6(GouldInline)
     int GouldInline;
{
	int	stop,i,lab,len,j;
	int     lastCc;		/* previous Cc value */

	stop = False;	
	TabFormat = False;
	if (GouldInline) {
	    return VerifyGouldInline();
	}
	StatementOffset = offset;
retry:
	Ccol = 6;
	switch (Cc) {
	  case EOF:		/* End of File */
	    return True;
	    break;
	  case 0x0b:		/* ^K Vertical TAB */
	  case 0x0c:		/* ^L New Page  */
          case '\n':            /* empty line taken as comment */
              if (!InString_or_Holl && !InEntryPt) {
                  UNGET(Cc);
                  GetComment('C');
              } else { /* continuation line in Hollerith, continue */
                  GET();
              }
              goto retry;
	  case '\r':
	      GET();
	      goto retry;
	  case  '&':		/* continuation line in TabFormat */
	    {
		GET();
		TabFormat = True;
		stop = False;
	    }
	    break;
	  case '#':		/* CPP directive  */
          case '%':             /* CPP directive on HP */
	  case 'c':		/* Comment or Directive ? */
	  case 'C':
	  case '*':
	  case '$':
	  case '!':		/* Plain comment take as 'C' comment */
	  case '{': {		/* hp extension for eol comments */
            int isCPP = (Cc=='#'||Cc=='%') ;
            if (isCPP && !followedByNumber()) {  /* CPP directive, using '%' on HP */
              return True;
            } else {
	      /* Directive are disabled. Untoggle comments to re enable. */
                GET();
                if ( Cc == '$' ) {
                  int comingLineSort = Directive('$') ;
                  if (comingLineSort==1)
                    return True;
                  else if (comingLineSort==2 || comingLineSort==-1) {
                    return False ;
                  }
                }
                UNGET(Cc);
		/* It was a comment , fall through next case */
		GetComment(isCPP ? '#' : 'C');
                goto retry;
	    }
          }
	  case 'd':		/* Various preprocessing comments: */
	  case 'D':		/* [d|D] for VAX FORTRAN debug line */
	  case 'x':		/* Gould preprocessing */
	  case 'X':		/* => transformed in Comment, CX or CY */
	  case 'y':
	  case 'Y':
	  case '?':
	    {
		UNGET(Cc);	/* include the 1st char in command line */
		GetComment('C');
		goto retry;
	    }
	    break;
	  case '\t':		/* VAX Tab format */
	    {
		TabFormat = True;
		GET();
		if(Cc >= '1' && Cc <= '9') { /* Tab format continuation */
		    stop = False;
		} else {
		    goto statement_field; /* statement start ? */
		}
	    }
	    break;
	  case '[':		/* check for entry point or enddata */
	    if (end_of_data()) return True; 
	    else goto badchar;
	    break;
	  case ' ':		/* Regular FORTRAN, at last */
	  case '0': case '1': case '2': case '3': case '4':
	  case '5': case '6': case '7': case '8': case '9':
	    {			
		lab = 0;
		/* Scan label field  (colums 1 - 5) */
		for(i = 0; i < 5; i++) {
		    if(isdigit(Cc))
		      lab = 10 * lab + (Cc - '0') ;
		    else if(Cc == '\n' || Cc == '\t') /* Vax tab */
		      break;
		    else if (Cc == '!' || Cc == '{') {
			GetComment('!');
			goto retry;
		    } else if (!isspace(Cc)) goto badchar;
		    GET();
		}
		/* Now Cc is the char of the sixth column (continuation ?) */
                /* if something in 6th col. assumed to be cont. line       */
		if (lab>0) NextLabel = lab;
		if(Cc == ' ' || Cc == '\t') {
		    if (Cc == '\t') TabFormat = True;
		    goto statement_field;
		} else if (Cc == '\n' && lab <= 0) {
		    GET();	/* empty line */
		    goto retry;
		} else if (Cc == '0') {
		    GET();
		    Ccol = 7;
		    return True;
		}
	    }
	    break;
	  default:
	    goto badchar;
	} /* end switch(Cc) on first char of line */

	return stop;
/*
 * When this state is reached:
 * Cccol == 6, CC is the non continuation char (6th column, or after tab)
 * Check for ! comment and empty line
 */
statement_field:
	/* make sure that there is no ! (eol) comments on the line */
	if (   (Cc == ' ' || Cc == '\t') && (SearchForEolComments())
	    || (TabFormat && (Cc == '!' || Cc == '{')) ) {
	    GetComment('!');	/* comment after label field (or vax tab) */
	    if (NextLabel<0) goto retry; /* empty line, just an eol comment */
	    UNGET(Cc);		/* pretend line was empty after label */
	    Cc ='\n';
	    return True;
	}
	if (Cc=='\n') {
	    GET();
	    goto retry; /* only whitespaces on line */
	} else {
	    return True;
	}

badchar:
	LexError(True, "Unknown character '%c' in column 1 to 5", (char)Cc);
	ErrorFlag = True;

	return stop;

}



/*=============================================================================*\
| AllowVaxRecordQuote(compacted_start) --> Boolean     	       	       	       	|
| return true if the simple quote ' is not the start of a string but		|
| the unit - record separator in the VMS shorthand for direct access IO		|
| read(unit'record) <=> read(UNIT=unit, REC=record)				|
| We check:  1) that this is the first string we have sean so far		|
|            2) the first word of compacted_start match a direct acces IO stmt	|
|            3) that after the opening '(' we have an int or a name		|
\*=============================================================================*/

Boolean AllowVaxRecordQuote(compact_start)
     char *compact_start;
{
    int blen;

    /* check that this is the first string we have sean so far */
    /* if (StrCnt > 0) return False; */
    /* check that the first word of compact_start match a direct acces IO stmt */
    blen = lastch - compact_start ;
    if (   (compact_start[0]=='r' && match_begin(&compact_start, blen, "read("))
	|| (compact_start[0]=='f' && match_begin(&compact_start, blen, "find("))
	|| (compact_start[0]=='d' && match_begin(&compact_start, blen, "delete("))
	|| (compact_start[0]=='w' && match_begin(&compact_start, blen, "write("))) {
	char *p=compact_start;
	int parcnt = 0;
	SKIPCOMSPEC(p);
	/* check that after the opening '(' we have an int or a name or array ref */
	while ( *p && p<lastch &&
	       ( isalnum(*p) || *p=='(' || *p==')'
		|| ( parcnt>0 && *p==',') ) ) {
	    if (*p=='(') parcnt++;
	    if (*p==')') parcnt--;
	    p++;
	    SKIPCOMSPEC(p);
	}
	return (   ( p != compact_start) /* <=> <unit> found */ 
		&& ( p == lastch)
		&& ( parcnt == 0) );
    } else {
	return False;
    }
}
/* utility function match_begin(bufptr,lastch, str)
   if str match the begining of *bufptr, advance *bufptr to after str
   and return true */
Boolean match_begin(bufptr, blen, str)
     char **bufptr;
     int blen;
     char *str;
{
    int slen, buflen, real_len;

    slen = strlen(str);
    if (blen >= slen) {
	real_len = CompactStrEq(*bufptr, str, slen);
	if (real_len) {
	    *bufptr = *bufptr + real_len;
	    return True;
	}
    }
    return False;    
}
   
/* ---------------------------------------------------------------------- */
/* |	This procedure treat the hollerith constants			| */
/* |	Return True if it is really a hollerith constant                | */
/* ---------------------------------------------------------------------- */
int TreatHollerith(hollcnt)
int hollcnt;
{
  char	*cbefore;
  char  *first_digit;
  char	*oldlastch;

  /* lets find char placed before the digits */
  /* as the digits have already been read and*/
  /* put aside in the processed line         */
  cbefore = lastch-1;
  for ( ; ; ) { 
    /* go back stepping over comms and holl... */
    if (ISSPECCHAR(cbefore-2)) {
      cbefore = cbefore - 3 ;
    } else if (ISSPECC(cbefore-2)) {
      cbefore -= 2;
      break;
    } else if (isdigit(*cbefore)) {
      first_digit = cbefore;
      --cbefore;
    } else break;
  }
  /* In the free format, one blank can be before the constant */
  if (FreeFormat && *cbefore == ' ')
    --cbefore; 
  /* additional tests for hollerith constant : */
  /* must be preceded by a punctuation mark    */
  /* or a '*' in a data statement              */
  if(*cbefore == '/' || *cbefore == '(' ||
     *cbefore == ',' || *cbefore == '=' ||
     *cbefore == '.' || *cbefore == '+' ||
     *cbefore == SPEC_STRING || 
     *cbefore == SPEC_HOLL   ||
     *cbefore == '-' || 
     (*cbefore == '*' && Inbuf[0] == 'd')) 
    {
      Newstring();
      oldlastch = lastch;
      lastch = first_digit;
      Reallastch += (lastch - oldlastch);
      GetFullHollerith(hollcnt);
      hollcnt =  0;
      Endstring(SPEC_HOLL);
      return True; /* Get OUT ! */
    }
  return False;
}

/* ---------------------------------------------------------------------- */
/* |	This procedure treat the double quoted string constants		| */
/* ---------------------------------------------------------------------- */
void
TreatDoubleQuotedString(state, stop)
int *state, *stop; {
  switch(Cc) {
    case -1:
	*stop = True;
	LexError(True, "Malformed string");
	break;
    case '\n':
    case '\r':
	if (FreeFormat) {
	    *stop = True;
	    LexError(True, "Malformed string");
        } else {
	    *state = 0;
	    *stop = GetMultiLineString('\"');
        }
	break;
    case '\"':
	GET();
	Ccol++;
	if(Cc == '\"') {
	    STORE(Cc);
	    GET();
	    Ccol++;
        } else {
	    *state = 0;
	    Endstring(SPEC_STRING);
        }
	break;
    case '\\':
	GET();
	Ccol++;
	STORE(Cc);
	GET();
	Ccol++;
	break;
    case '&':
	if (FreeFormat) {
	    *stop = FreeFormatGetMultiLineString('\"');
	    if (!(*stop) && Cc == '&') {
		STORE(Cc);
		GET();
		Ccol++;
            } else
                *state = 0;
            if (*stop)
	      LexError(True, "Malformed string");
	    break;
        }
    default:
	if (FreeFormat && Ccol > Max_Col_Nb) {
            /* skip to end of line (or file) */
            do { GET(); } while(Cc!='\n' && Cc!='\r' && Cc!=EOF);
            *state = 0;
            LexError(True, "Malformed string");
          } else 
              if (!FreeFormat && (TabFormat == False && Ccol > Max_Col_Nb)) {
                  if( Keep_Line_Trailer ) { /* Get as comm. */
                      UNGET(Cc);
                      GetComment('!');
                  } else { /* skip to end of line (or file) */
                      do { GET(); } while(Cc!='\n' && Cc!='\r' && Cc!=EOF);
                  }
                  if (Cc=='\n' || Cc=='\r') {
                      *state = 0;
                      *stop = GetMultiLineString('\"');
                  }
              } else {
                  STORE(Cc);
                  GET();
                  Ccol++;
              }
	break;
  }
}
/* ---------------------------------------------------------------------- */
/* |	This procedure treat the single quoted string constants		| */
/* ---------------------------------------------------------------------- */
void
TreatSingleQuotedString(state, stop)
int *state, *stop;{
    switch(Cc) {
    case -1:
	*stop = True;
	LexError(True, "Malformed string");
	break;
    case '\n':
    case '\r':
        if (FreeFormat) {
            *stop = True;
            LexError(True, "Malformed string");
	} else {
            *state = 0;
            *stop = GetMultiLineString('\'');
	}
        break;
    case '\'':
	GET();
	Ccol++;
	if(Cc == '\'') {
	    STORE(Cc);
	    GET();
	    Ccol++;
	} else {
	    *state = 0;
	    Endstring(SPEC_STRING);
	}
	break;
    case '\\':
	GET();
	Ccol++;
	STORE(Cc);
	GET();
	Ccol++;
	break;
    case '&':
        if (FreeFormat) {
            *stop = FreeFormatGetMultiLineString('\'');
            if (!(*stop) && Cc == '&') {
                STORE(Cc);
                GET();
                Ccol++;
	    } else
              *state = 0;
            
            if (*stop)
              LexError(True, "Malformed string");
            break;
	}
    default:
        if (FreeFormat && Ccol > Max_Col_Nb) {
            /* skip to end of line (or file) */
            do { GET(); } while(Cc!='\n' && Cc!='\r' && Cc!=EOF);
            *state = 0;
            LexError(True, "Malformed string");
          } else
	      if (!FreeFormat && (TabFormat == False && Ccol > Max_Col_Nb)) {
		  if( Keep_Line_Trailer ) { /* Get as comm. */
		      UNGET(Cc);
		      GetComment('!');
		  } else { /* skip to end of line (or file) */
		      do { GET(); } while(Cc!='\n' && Cc!='\r' && Cc!=EOF);
		  }
		  if (Cc=='\n' || Cc=='\r') {
		      *state = 0;
		*stop = GetMultiLineString('\'');
		  }
	      } else {
		  STORE(Cc);
		  GET();
		  Ccol++;
	      }
	break;
    }
}

/* ---------------------------------------------------------------------- */
/* |	Get a full line:						| */
/* |	This procedure processes a line removing blanks, eating		| */
/* |	continuation lines, until a real end-of-line is reached.	| */
/* |	It stores the label number in CurLabel (-1 if none)		| */
/* |	It saves strings/Hollerith and comments into a separate buffer	| */
/* |	(respectively) replacing them by by the special chars		| */
/* |	SPEC_STRING SPEC_HOLL and SPEC_COMM followed by 2 chars which	| */
/* |	code the address of the string in the StrPtr stack		| */
/* |	- Returns False if EOF reached					| */
/* ---------------------------------------------------------------------- */
int	GetFullLine(skipspaces)
int	skipspaces;
{
	int	state;
	int	hollcnt;
	int	stop;
	char	*oldlastch;
	Boolean emptyStatement = False;
        Boolean alreadySaidTooLong = False ;

	/* Error ?? */
        if (ErrorFlag) return False;
	/* add on */
	if (nextch < lastch) return True;
	if (   EndOffset
	    && offset >= EndOffset
	    && (IS_AN_END_UNIT(MainKey) || offset >= EndOffset+FUZZYNESS))
	    return False;

	state = 0;
	GotInclBracket = False;
	StrIns = StrBuf;
	StrCnt = 0;
	TokSP = 0;
	TokBP = 0;
	nlSP = 0;
	hollcnt = 0;
	ParLevel = 0;
	BrackLevel = 0;
	nextch = lastch = Inbuf;
	Realnextch = Reallastch = Realbuf;

	NextLabel = -1;
        InImplicit = False;
        Need_Type_Kwd = False;
	Need_Op_Kwd = False;

	if(Cc == EOF)
		return False;

        //[llh] written like in FreeFormatGetFullLine(), but unsure this is necessary?
        if (Cc =='#' || Cc=='%') {
          if (followedByNumber())
            Cc='#' ; // for post [cf]pp line comments.
          else
            return GetCppLine();
        }

	stop = False;
	for( ; !stop ; ) {
	        if (Cc != ' ' && Cc != ';' && Cc != '\t' && Cc != '\r' && Cc != '\n' && Cc != '!' && Cc != '{')
		  emptyStatement = False;
		switch(state) {
			/* 0: normal state */
		case 0:
		    	RealCc = Cc;		/* Keep the real input char */
			Cc = LowerC[Cc];
			if(isdigit(Cc) && 
			   (TabFormat != False || Ccol <= Max_Col_Nb))
				hollcnt = 10 * hollcnt + Cc - '0';
			else if(Cc != 'h' && Cc != '\n' && Cc != '\r')
				hollcnt = 0;
			if(Cc < 0) LexError(True, "Negative char %d\n",Cc);
			switch(Cc) {
			case '\n':
			        if (emptyStatement) { lastch--; Reallastch--; }
			        if (GouldFortran && 
				    CompactStrEq(nextch,"inline",6) &&
				    (lastch-nextch) == 6) {
				    /* special case of inline */
				    GET();
				    stop = Scan1to6(True);
				} else {
				    GET();
				    stop = Scan1to6(False);
				}
				if (!stop) { /* memorize the line cont. */
				  /* cont. line and Label => ERROR !!! */
				  if (NextLabel != -1) {
				    yyerror();
				    ErrorFlag = 1;
				  }
				  nlStack[nlSP++] = lastch;
				}
				break;
			case ';':
			    if (!emptyStatement) {
			      InitSTable(pSTable+1); /* new stat level */
			      GetNextLabel(); /* is there any label ? */
			      Cc = ';'; /* GetNextLabel changes Cc */ 
			      HAVE(Cc);
			    }
			    emptyStatement = True;
			    break;
			case '#':
			case '!':
			case '{': /* hp extension for comments */
			        if (emptyStatement) { lastch--; Reallastch--; }
				GetComment('!'/*failed attempt:Cc=='!' ? '!' : '#'*/) ;
				stop = Scan1to6(False);
				if (!stop) { /* memorize the line cont. */
				  /* cont. line and Label => ERROR !!! */
				  if (NextLabel != -1) {
				    yyerror();
				    ErrorFlag = 1;
				  }
				  nlStack[nlSP++] = lastch;
				}
				break;
			case '\"':
				Newstring();
				state = 1;
				break;
			case '\'':
				/* check if it's a find(u'r) */
				if (AllowVaxRecordQuote(nextch)) {
				    HAVE(Cc);
				} else {
				    Newstring();
				    state = 2;
				}
				break;
			case '\r': break;  /* for DOS format file */
			case ' ':
			case '\t' :
				if(skipspaces) break;
				else skipspaces = True;
				/* leave only the first space so the  */
				/* directive name will be recognised. */
				/* ELSE GO ON DOWN THERE ! */
			default:
                                if (Cc==SPEC_DIR) TabFormat = True; /* dir. got in Tab Format*/
				if (TabFormat == False && Ccol > Max_Col_Nb) {
                                  /* we warn the user when trailer */
                                  if (!alreadySaidTooLong) {
                                    LexError(False, "Characters found after column %d",Max_Col_Nb);
                                    alreadySaidTooLong = True ;
                                  }
				  if( Keep_Line_Trailer ) {
				      UNGET(Cc);
				      GetComment('!'); /* Get the comment   */
				      stop = Scan1to6(False); /* until end of line */
				      if (!stop) { /* memorize the line cont. */
					  /* cont. line and Label => ERROR !!! */
					  if(NextLabel != -1) {
					    yyerror();
					    ErrorFlag = 1;
					  }
					  nlStack[nlSP++] = lastch;
				      }
                                  }
				  break;
			        }
				if(Cc == 'h' && hollcnt > 0) {
				  if (TreatHollerith(hollcnt)) {
				      hollcnt = 0;
				      break;
				  }
				}
				if(Cc == '(') {
					HAVE(Cc); GET();
					if (Cc != '/') {
						ParLevel++;
						UNGET(Cc);
					} else {
						BrackLevel++;
						Ccol++;
						HAVE(Cc);
					}
					break;
				} else if (Cc == '[') {
					BrackLevel++;
				} else if(Cc == ')') {
					ParLevel--;
				} else if (Cc == ']') {
					BrackLevel--;
				} else if (Cc == '/') {
					HAVE(Cc); GET();
					if(Cc == ')') {
						BrackLevel--;
						Ccol++;
					} else {
						UNGET(Cc);
						break;
					}
				} else if(ParLevel == 0) {
					if ((Cc == '=' && 
					     !(STable[pSTable].GotDblColon))
					    &&
                                            IS_NOT_AN_OPERATOR((*(lastch-1)))){
                                            /* avoid the case of == */
                                            int oldCc = Cc;
                                            GET();
                                            if (Cc != '=') 
						STable[pSTable].GotEqual = True;
                                            UNGET(Cc);
                                            Cc = oldCc;
                                        }
					else if(Cc == ',' && BrackLevel == 0)
					    STable[pSTable].GotComma = True;
					if (Cc == ':' && *(lastch-1) == ':')
					    STable[pSTable].GotDblColon = True;
				}
				HAVE(Cc);
				break;
			}
			if(!stop) {
				GET();
				Ccol++;
			}
			break;

			/* 1: within a double quote string */
		case 1:
		  {
		    /* printf("double\n"); */
		    TreatDoubleQuotedString(&state, &stop);
		    break;
		  }
			/* 2: within a single quote string */
		case 2:
		  {
		    /* printf("single\n"); */
		    TreatSingleQuotedString(&state, &stop);
		    break;
		  }
		    }
		if ( Cc == -1) stop = True;
	}
	*lastch = '\000';
	*Reallastch = '\000';

	return True;
}

/* ---------------------------------------------------------------------- */
/* |	Process the End of line continuation, readjusting the global	| */
/* |    variables lastch and Reallastch removing the unused '&' cont.	| */
/* |    char. Preserve the comments found after the '&'.		| */
/* |	p_line_cont(in) is on the '&' char in the current line		| */
/* ---------------------------------------------------------------------- */
void AdjustEndOfLine(p_line_cont)
char *p_line_cont; {
    int delta;
    char *pcurr = p_line_cont;
    /* find the first comment or lastch */
    while (pcurr < lastch && *pcurr != SPEC_COMM) pcurr++;
    delta = pcurr - p_line_cont;
    while (pcurr < lastch) { /* preserve the comments found after '&' */
	int i;
	if (*pcurr == SPEC_COMM)
	    for (i = 0 ; i < 3 ; i++) *(p_line_cont++) = *(pcurr++);
    }
    /* put back lastch and Reallastch to ignore the '&' */
     lastch -= delta ; 
     Reallastch -=delta; 
    /* put back lastch and Reallastch when the last character is ' ' */
     if (*(lastch-1) == ' ' && (lastch-1<=nextch+1 || *(lastch-3)!=SPEC_COMM)) { lastch--; }
     if (*(Reallastch-1) == ' ' && (Reallastch-1<=nextch+1 || *(Reallastch-3)!=SPEC_COMM)) {  Reallastch--; }
}


/* ---------------------------------------------------------------------- */
/* |	Process the End of line continuation  for Free Format		| */
/* |    We saw a \n we are looking for an eventual &			| */
/* |    stop(out) : True if no continuation line found			| */
/* | Warning side-effects on global nextch, lastch, Reallastch		| */
/* ---------------------------------------------------------------------- */
void FreeFormatEndLineCont(stop)
int *stop; {
    char *precch;

    if (lastch != nextch) {
	precch = lastch - 1;
	/* get back till an '&' (skip whitespace and SPEC_COMM) */
        /* Attention: tester l'egalite a ' ' risque de confondre le compteur apres
         * un SPEC_COMM avec un blanc quand ce compteur arrive a la valeur 32(==' '). */
        if(precch != nextch && *precch== ' ' && (precch<=nextch+1 || *(precch-2)!=SPEC_COMM))
	    precch = precch - 1;
	if(precch > nextch + 1 && *(precch-2) == SPEC_COMM)
	    precch = precch - 3;
	if(precch != nextch && *precch== ' ' && (precch<=nextch+1 || *(precch-2)!=SPEC_COMM))
	    precch = precch - 1;
	if(*precch == '&') {
	    /* there is an end of line continuation */
            *stop = ScanStartFreeStatement(True, 1) ;
	    /* adjust the end of line and preserve the comments found after 
	       line continuation */
	    AdjustEndOfLine(precch);
	    if (CurTypeDir) {
              if (!(*stop)) {
		    RealCc = ' '; HAVE(' ');
		    UNGET(Cc);
		}
	    } else {
		if (*stop) {
		    RealCc = ' '; HAVE(' ');
		    UNGET(Cc);
		}
		*stop = False;
	    }
	} else
          *stop = ScanStartFreeStatement(False, 1) ;
    } else
      *stop = ScanStartFreeStatement(False, 1) ;
}

/* ---------------------------------------------------------------------- */
/* |	The version for the free format, treat :      			| */
/* |	- the significant blanks                        		| */
/* |	- the continuation '&'   					| */
/* |	                           					| */
/* |	- Returns False if EOF reached					| */
/* ---------------------------------------------------------------------- */
int	FreeFormatGetFullLine()
{
	int	state;
	int	hollcnt;
	int	stop;
	Boolean emptyStatement = False;
        Boolean alreadySaidTooLong = False ;

	/* Error ?? */
        if (ErrorFlag) return False;
	/* add on */
	if (nextch < lastch) return True;
	if (   EndOffset
	    && offset >= EndOffset
	    && (IS_AN_END_UNIT(MainKey) || offset >= EndOffset+FUZZYNESS))
	    return False;

	/* tests */
	/* Max_Col_Nb =10; Keep_Line_Trailer=True; */
	
	CurTypeDir = NextLineTypeDir ;
	SendKWDir = False;

	state = 0;
	GotInclBracket = False;
	StrIns = StrBuf;
	StrCnt = 0;
	TokSP = 0;
	TokBP = 0;
	nlSP = 0;
	hollcnt = 0;
	ParLevel = 0;
	BrackLevel = 0;
	nextch = lastch = Inbuf;
	Realnextch = Reallastch = Realbuf;

	NextLabel = -1;
        InImplicit = False;
        Need_Type_Kwd = False;
	Need_Op_Kwd = False;

	if(Cc == EOF)
		return False;

        if (Cc =='#' || Cc=='%') {
          if (followedByNumber())
            Cc='#' ; // for post [cf]pp line comments.
          else
            return GetCppLine();
        }

	stop = False;
	for( ; !stop ; ) {
	        if (Cc != ' ' && Cc != ';' && Cc != '\t' && Cc != '\r' && Cc != '\n' && Cc != '!')
		  emptyStatement = False;
		switch(state) { 
		case 0: /* 0: normal state */
		    	RealCc = Cc;		/* Keep the real input char */
			Cc = LowerC[Cc];

			/* Treat Hollerith for the free format ??? */
			if (isdigit(Cc) && Ccol <= Max_Col_Nb)
			  hollcnt = 10 * hollcnt + Cc - '0';
			else if(Cc != 'h' && Cc != '\n' && Cc != '\r')
			  hollcnt = 0;

			if(Cc < 0) LexError(True, "Negative char %d\n",Cc);
			switch(Cc) {
			case '\n': 
			    if (emptyStatement) { lastch--; Reallastch--; }
			    GET();
			    /* look for a end of line continuation */
			    FreeFormatEndLineCont(&stop);
			    if (!stop) { /* memorize the line cont. */
			      /* cont. line and Label => ERROR !!! */
			      if (NextLabel != -1) {
				yyerror();
				ErrorFlag = 1;
			      }
			      nlStack[nlSP++] = lastch;
			    }
			    break;
			case ';':
			    if (!emptyStatement) {
			      InitSTable(pSTable+1); /* new stat level */
			      GetNextLabel(); /* is there any label ? */
			      Cc = ';'; /* GetNextLabel changes Cc */ 
			      HAVE(Cc);
			    }
			    emptyStatement = True;
			    break;
			case '#':
			case '!':
			    if (emptyStatement) { lastch--; Reallastch--; }
			    GetComment(Cc=='!' ? '!' : '#');
			    /* look for a end of line continuation */
			    FreeFormatEndLineCont(&stop) ;
			    if (!stop) { /* memorize the line cont. */
			      /* cont. line and Label => ERROR !!! */
			      if (NextLabel != -1) {
				yyerror();
				ErrorFlag = 1;
			      }
			      nlStack[nlSP++] = lastch;
			    }
			    break;
			case '\"':
				Newstring();
				state = 1;
				break;
			case '\'':
				/* check if it's a find(u'r) */
				if (AllowVaxRecordQuote(nextch)) {
				    HAVE(Cc);
				} else {
				    Newstring();
				    state = 2;
				}
				break;
			case '\r': break;  /* for DOS format file */
			case ' ':
			case '\t' :
			  {
			    int bufCc;
			    /* keep only the first space */
			    /* skip the space until the next keyword
			       or the end of the line */
			    bufCc = Cc;
			    GET();
			    while ((Cc == ' ' || Cc == '\t') &&
				   (Ccol <= Max_Col_Nb)) 
			      { GET(); Ccol++; } 
			    UNGET(Cc);
			    Cc = bufCc;
			  }
			default:
			  {
			    if (Ccol > Max_Col_Nb) {
                              /* we warn the user when trailer */
                              if (!alreadySaidTooLong) {
                                LexError(False, "Characters found after column %d",Max_Col_Nb);
                                alreadySaidTooLong = True ;
                              }
                              if( Keep_Line_Trailer ) {
				  UNGET(Cc);
				  /* Get the comment until end of line */
				  GetComment('!');
				  /* look for a end of line continuation */
				  FreeFormatEndLineCont(&stop);
				  if (!stop) { /* memorize the line cont. */
				    /* cont. line and Label => ERROR !!! */
				    if(NextLabel != -1) {
				      yyerror();
				      ErrorFlag = 1;
				    }
				    nlStack[nlSP++] = lastch;
				  }
                              }
                              break;
                            }
			  }
			  /* Treat Hollerith for the free format ??? */
			  if(Cc == 'h' && hollcnt > 0)
			    if (TreatHollerith(hollcnt)) {
				hollcnt = 0;
				break;
			    }
			  if(Cc == '(') {
			    HAVE(Cc); GET();
			    if (Cc != '/') {
			      ParLevel++;
			      UNGET(Cc);
			    } else {
			      BrackLevel++;
			      Ccol++;
			      HAVE(Cc);
			    }
			    break;
			  } else if (Cc == '[') {
			    BrackLevel++;
			  } else if(Cc == ')') {
			    ParLevel--;
			  } else if (Cc == ']') {
			    BrackLevel--;
			  } else if (Cc == '/') {
			    HAVE(Cc); GET();
			    if(Cc == ')') {
			      BrackLevel--;
			      Ccol++;
			    } else {
			      UNGET(Cc);
			      break;
			    }
			  } else if(ParLevel == 0) {
			    if ((Cc == '=' && !(STable[pSTable].GotDblColon)) &&
                                IS_NOT_AN_OPERATOR((*(lastch-1)))) {
                                /* avoid the case of == */
                                int oldCc = Cc;
                                GET();
                                if (Cc != '=') 
				    STable[pSTable].GotEqual = True;
                                UNGET(Cc);
                                Cc = oldCc;
                            }
			    else if(Cc == ',' && BrackLevel == 0)
			      STable[pSTable].GotComma = True;
			    if (Cc == ':' && *(lastch-1) == ':')
			      STable[pSTable].GotDblColon = True;
			  }
			  if (Cc == '\t')
			    { RealCc = ' '; HAVE(' '); }
			  else
			    { HAVE(Cc); }
			  break;
			}
			if(!stop) {
			  GET();
			  Ccol++;
			}
			break;
		
		case 1: /* 1: within a double quote string */
		    TreatDoubleQuotedString(&state, &stop);
		    break;

		case 2: /* 2: within a single quote string */
		    TreatSingleQuotedString(&state, &stop);
		    break;
		} /* end switch(state) */
		if ( Cc == -1) stop = True;
	} /* end for (; !stop ;) */
	*lastch = '\000';
	*Reallastch = '\000';
	return True;
}

/*---------------------------------------------------------------------------*/
/* GetCppLine() --> int :                                                    */
/* Processes a directive aimed at [cf]pp, e.g. #include.                     */
/* This is not to process post-cpp line comments e.g. # 10 "foo.inc"         */
/* The current Cc char is '#' standard first char in cpp directives or '%'   */
/* used on hp, when '%' we process it as a '#'                               */
/* does the equivalent of getfullline for a CPP preprocessor line,           */
/* with the following differences:                                           */
/*   1- does not ignore spaces, except after '#'                             */
/*   2- treat <xxx> as string in #include lines                              */
/*   3- do not convert to lower case                                         */
/* when \n found -> syntax error					     */
/*---------------------------------------------------------------------------*/
int GetCppLine()
{
    int	state=0;
    int stop = False;
    int delim; 

    /* skip initial space chars */
    HAVE('#'); GET();
    while(Cc==' ' || Cc=='\t') GET();
    /* get rest of line, without skipping spaces */
    while( !stop ) {
	if(Cc == EOF) {
	    if (state != 0){ 
		LexError(True, "Malformed string");
	    }
	    return False;
	}
	switch(state) {
	    /* 0: normal state */
	  case 0:
	    RealCc = Cc;		/*  real input char */
	    switch(Cc) {
	      case '\n':
		stop = True;
		break;
	      case '\'':
	      case '\"':
		delim = Cc;
		Newstring();
		state = 1;
		break;
	      case '<':
		if(CompactStrEq(nextch,"#include",8)) {
		    GotInclBracket = True;
		    Newstring();
		    state = 2;
		} else { HAVE(Cc); }
		break;
	      default:
		HAVE(Cc);
		break;
	    }
	    break;
	  case 1: /* state 1: within "string" */
	    if ( Cc == delim ) {
		state = 0;
		Endstring(SPEC_STRING);
	    } else if ( Cc == '\n') {
		yyerror();
		ErrorFlag = 1;
	    } else
		STORE(Cc);
	    break;
	  case 2: /* state 2: within <string> */
	    if ( Cc == '>' ) {
		state = 0;
		Endstring(SPEC_STRING);
	    } else if ( Cc == '\n') {
		yyerror();
		ErrorFlag = 1;
	    } else
		STORE(Cc);
	}
	if(!stop) {
	    GET();
	    Ccol++;
	}
    } /* end while(!stop)  */
    *lastch = '\000';
    *Reallastch = '\000';
    return True;
} /* end GetCppLine() */

		
    

/* ---------------------------------------------------------------------- */
/* |	Make the line empty after a syntax error			| */
/* ---------------------------------------------------------------------- */
void Flushline() {
	nextch = lastch;
}

/* ------------------------------------------------------------------------- */
/* |	Enter or Quit the "In IO" state	: called in the parser as an action| */
/* ------------------------------------------------------------------------- */
void InIO(int status) {
	InIoctl = status;
}

/* ----------------------------------------------------------------------- */
/* |	Enter or Quit the "Optional Comma " state : called in the parser | */
/* |    as an action 							 | */
/* ----------------------------------------------------------------------- */
void OptComma(int status) {
	OptCommactl = status;
}

void SizeComma(int status) {
	SizeCommactl = status;
}

void InDecl(int status) {
	InDeclctl = status;
}

/* ---------------------------------------------------------------------- */
/* |	Enter or Quit the "Only Integers" state				| */
/* ---------------------------------------------------------------------- */
void OnlyInt(int status) {
	IntOnly = status;
}

/* ---------------------------------------------------------------------- */
/* |	Need colon for Gould						| */
/* ---------------------------------------------------------------------- */
void NeedColoninGould(int status) {
	if (GouldFortran) NeedGouldColon = status;
}

/* ---------------------------------------------------------------------- */
/* |	Need keyword							| */
/* ---------------------------------------------------------------------- */
void NeedKeyword(int status) {
	Need_Kwd = status;
}

/* ---------------------------------------------------------------------- */
/* |	Optional Need keyword							| */
/* ---------------------------------------------------------------------- */
void OptNeedKeyword(int status) {
	Opt_Need_Kwd = status;
}

/* ---------------------------------------------------------------------- */
/* |	Need letter (for IMPLICIT ...)					| */
/* ---------------------------------------------------------------------- */
void NeedLetter(int status) {
	Need_Letter = status;
}

/* ---------------------------------------------------------------------- */
/* |	Need to put in token stack the INIMPLICIT Kwd			| */
/* ---------------------------------------------------------------------- */
void NeedInImplicit(int status) {
    Need_Kwd = status;
    InImplicit = status;
}

/* ---------------------------------------------------------------------- */
/* |  forces CHARACTER, INTEGER, REAL, DOUBLE ... to be seen as kwd	  */
/* ---------------------------------------------------------------------- */
void NeedTypeKeyword(int status) {
    Need_Kwd = status;
    Need_Type_Kwd = status;
}


/* ---------------------------------------------------------------------- */
/* |	Need to forces OPERATOR and ASSIGNMENT to be seen as kwd	  */
/* ---------------------------------------------------------------------- */
void NeedOpKeyword(int status) {
    Need_Kwd = status;
    Need_Op_Kwd = status;
}

/* ---------------------------------------------------------------------- */
/* |	Need Offset Token               				| */
/* |	We need this because we must not have offsets for inner END s   | */
/* |	like interface block ends or internal routine ends              | */
/* ---------------------------------------------------------------------- */
void NeedOffset() {
	Need_Offset = SEND_START;
}



/* ---------------------------------------------------------------------- */
/* |	CommonFlag  (for Common ...)					| */
/* ---------------------------------------------------------------------- */
void CommonFlag(int status) {
	Common_Flag = status;
}

/* ---------------------------------------------------------------------- */
/* | Disambiguate do While loops					| */
/* ---------------------------------------------------------------------- */
void DoWhile(int status) {
    Do_While_Kwd = status;
}

/* ---------------------------------------------------------------------- */
/* |	Stack on a DO label						| */
/* ---------------------------------------------------------------------- */
void DoLabel() {
    int	lab;

    if(LastToken == INTCST)
      if(sscanf(yytext,"%d",&lab) == 1) {
          if(DoDepth > 0 && Dolabels[DoDepth-1] == lab)
            Docounts[DoDepth-1] += 1;
          else 
            if(DoDepth < MAXDOLAB) {
                Dolabels[DoDepth] = lab;
                Docounts[DoDepth] = 1;
                DoDepth++;
            } else {
                LexError(True, "Too many nested loops");
            }
      }
}

/* ---------------------------------------------------------------------- */
/* |	Searches a keyword at current position				| */
/* ---------------------------------------------------------------------- */
int GetKey(Keylist *table, Keylist *endtable, int *len) {
	register int	L,tok,i;
	register Keylist	*pk;
	char *bufp;

	if(table != nil)
		for(pk = table ; endtable == nil || pk <= endtable ; ++pk ) {
			L = pk->len;
			if(L <= 0)
				break;
			if(nextch + L > lastch)
				continue;
			L = CompactStrEq(nextch,pk->name,L);
			/* do not return KW_NULL if not GOULD */
			if (!GouldFortran && KW_NULL==pk->code)
			  continue; 
			if (L) {
			  /* save comments */
                            bufp = nextch;
			    for (i=0; i<L; i++) {
			      if (*bufp++==SPEC_COMM)
				PutTokStack(SPEC_COMM,GetStrCnt(bufp));
			    }
			    *len = L;
			    return pk->code;
			}
		}
	*len = 0;
	return -1;
}

/* ---------------------------------------------------------------------- */
/* |	Disambiguate the IMPLICIT FORTRAN 90 statement                  | */
/* |    The previous kwd was a FORTRAN 77 basic type (integer,...)      | */
/* |    Add the KW_INIMPLICIT when the following tokens are             | */
/* |            (lexp)(letters)                                         | */
/* |    next_ch(in) is on the current token                     	| */
/* |	last_ch(in) is the end of the current line			| */
/* ---------------------------------------------------------------------- */
void Search_for_Implicit90 (next_ch,last_ch)
char *next_ch, *last_ch;
{
    char *pos = next_ch;
    SKIPCOMSPEC(pos);
    while (pos <= last_ch && (*pos) == ' ') pos++;
    if (pos > last_ch) return;
    if ((*pos) == '(') { /* found the opening bracket */
	int par_level = 0;
	while (pos <= last_ch) {
	    pos++;
	    SKIPCOMSPEC(pos);
	    switch (*pos) {
	    case '(' : par_level++;
		break;
	    case ')': par_level--;
		break;
	    }
	    if (par_level == -1) { /* found the closing bracket */
		pos++;
		while (pos <= last_ch && (*pos) == ' ') pos++;
		SKIPCOMSPEC(pos);
		if (pos > last_ch) return;
		/* next char must be another opening bracket */
		if ((*pos) == '(') PutTokStack(KW_INIMPLICIT,-1);
		return;
	    }
	}
    }
}

/* ---------------------------------------------------------------------- */
/* |	Verify a number in a radix					| */
/* ---------------------------------------------------------------------- */
void VerifyNumber(int radix) {
}

/* ------------------------------------------------------------------------ */
/* |	Verify that current char of the line (*nextch) is an alphanumeric | */
/* |    char and that a SPEC_COMM is not inserted in the current line.    | */
/* |    When a SPEC_COMM is found -> a comment is inserted within a 	  | */
/* |    token, process the comment and verify nextch isalnum		  | */
/* ------------------------------------------------------------------------ */
int  Isalnum() {
    while (*nextch == SPEC_COMM && !FreeFormat) { /* process the comment */
	ProcessFixComment(nextch,1);
	nextch += 3;			 /* take back StrCnt */
	if (nextch == lastch) return False;
    }
    return isalnum(*nextch);
}

/* ---------------------------------------------------------------------- */
/* Recreate a bit cst from something which was read as a string           */
/* bitcst is recreated as  radix'yytext' (by the way yyleng = yyleng + 3) */
/* ---------------------------------------------------------------------- */
void reprint_bitcst(radix)
char radix;
{
    int i;

    for (i = yyleng - 1; i > -1 ; i--) yytext[i+2] = yytext[i];
    yytext[0] = radix ;
    yyleng++;
    yytext[1] = '\'';
    yyleng++;
    TAKE('\''); /* yytext[yyleng] = '\'';   yyleng++; */
    yytext[yyleng] = '\0';
}


/* -----------------------------------------------------------------------  */
/* | Get a bit_cst token : bit, octal, hexa, or ASTRING according to the  | */
/* | radix value.							  | */
/* | input :nextch is on a SPEC_STRING character			  | */
/* | output yytext,yyleng contains the token				  | */
/* |        and nextch is placed after the 'SPEC_STRING StrCnt' token     | */
/* |       return the token value					  | */
/* | Some verification might be done in VerifyNumber			  | */
/* ------------------------------------------------------------------------ */
int GetBit_cst(radix)
    char radix;{
	int strpos;
	int tok,nradix;

	strpos =  GetStrCnt(nextch+1); /* take back StrCnt */
	GetTokenValue(strpos, False);
	nextch += 3;
	switch(radix) {
	case 'x' :
	case 'X' :
	case 'z' : /* VMS EXTENSION */
	case 'Z' :
	    nradix = 16;
	    break;
	case 'o' :
	case 'O' :
	    nradix = 8;
	    break;
	case 'b' :
	case 'B' :
	    nradix = 2;
	    break;
	default:
	    nradix = 0;
	    break;
	}
	if(nradix == 0)
	    tok = ASTRING;
	else {
	    tok = BITCST;
	    /* We take all the parts of the bit_cst */
	    while ( *nextch == SPEC_STRING ) {
	        strpos =  GetStrCnt(nextch+1);
		GetTokenValue(strpos, True);
		nextch += 3;
	    }
	    reprint_bitcst(radix);
	    /* VerifyNumber(nradix); */
	}
	return tok;
    }

/* ---------------------------------------------------------------------- */
/* |   Parse a line just to look for its comments 			| */
/* ---------------------------------------------------------------------- */
void ParseCommentinLine()
{
    char *s;
    for (s = nextch ; s<lastch ; s++) {
	if(*s == SPEC_COMM) {
	    ProcessFixComment(s,1);	/* process the comments */
	    s += 2;			/* take back StrCnt */
	}
    }
}

/* ---------------------------------------------------------------------- */
/* |   Set the current line as a comment line				| */
/* |   UNGET all Real chars in line and GetComment 			| */
/* ---------------------------------------------------------------------- */
void SetLineinComments()
{
    char *s,*s2;
    int index,len;
    /* UNGET advanced read characters */
    UNGET(Cc); UNGET('\n');
    /* UNGET the whole line */
    for (s = lastch - 1 ; s>=nextch ; s--) { 
	if (*s) {
	    switch (*s) {
	      case SPEC_HOLL :
	      case SPEC_STRING : 
		index = GetStrCnt(s+1);
		UNGET('"');
		for (len = StrLen[index] - 1, s2 = StrPtr[index] ; 
		     len >= 0 ;
		     len--)
		  UNGET(*(s2+len));
		UNGET('"');
		break;
	      case SPEC_COMM : 
		break;
	        default :
		if (isprint(*s) && *(s-2) != SPEC_COMM && 
		    *(s-2) != SPEC_STRING && *(s-2) != SPEC_HOLL) {
		    UNGET(Realnextch[s - nextch]);
		} else {
		    if (*(s-2) == SPEC_COMM || *(s-2) == SPEC_STRING ||
			*(s-2) == SPEC_HOLL)
		      s--;
		}
	    }
	}
    }
    /* Then Get the whole line in comment */
    GetComment('C');
}
		  
char* is_OMPEND(char *curChar) {
  if (FreeFormat) while (*curChar==' ') ++curChar ;
  if (curChar+6<=lastch && CompactStrEq(curChar,"ompend",6))
    return curChar+6 ;
  else if (FreeFormat && curChar+7<=lastch && CompactStrEq(curChar,"omp end",7))
    return curChar+7 ;
  else
    return NULL ;
}

char* is_OMPENDPARALLELDO(char *curChar) {
  if (FreeFormat) while (*curChar==' ') ++curChar ;
  if (curChar+10<=lastch && CompactStrEq(curChar,"paralleldo",10))
    return curChar+10 ;
  else if (FreeFormat && curChar+11<=lastch && CompactStrEq(curChar,"parallel do",11))
    return curChar+11 ;
  else
    return NULL ;
}

char* is_OMPENDPARALLEL(char *curChar) {
  if (FreeFormat) while (*curChar==' ') ++curChar ;
  if (curChar+8<=lastch && CompactStrEq(curChar,"parallel",8))
    return curChar+8 ;
  else
    return NULL ;
}

char* is_OMPENDDO(char *curChar) {
  if (FreeFormat) while (*curChar==' ') ++curChar ;
  if (curChar+2<=lastch && CompactStrEq(curChar,"do",2))
    return curChar+2 ;
  else
    return NULL ;
}

/* ---------------------------------------------------------------------- */
/* |	Gets a token 							| */
/* ---------------------------------------------------------------------- */
int	GetToken() {
	int	i,tok,radix,len,havedot,eqlen;
	char	*sav;

	if (NeedOpUser) {
	    char *buff;
	    /* take the '.' in the name */
	    TAKE('.');
	    for (buff = nextch; buff < NeedOpUser; buff++)
	      TAKE(Realbuf[buff-Inbuf]);
	    TAKE('.');
	    nextch = NeedOpUser+1;
	    NeedOpUser = (char *)0;
	    yytext[yyleng] = '\0';
	    RETTOKEN(OP_USER);
	}
retry:
        /* [llh] CAUTION: it seems the "Token Stack" is not a Stack. It is FIFO! */
	if(TokSP > TokBP) { /* Something in the Token Stack */
		tok = GetTokStack();
		if (tok != -1) { /* we found only unsignificant token */
		    if (tok == KW_PVIR) { /* multi statement with ';' */ 
			LexReclassify();
			pSTable++;        /* next stat level */
		    }
		    RETTOKEN(tok);
		} /* we continue looking for a significant token */
	}

/* printf("GETTOKEN |%s|\n", strndup(nextch, 20)) ; */
        /* "are there ending DO loops" mixed with "is there an OMP END something" */
        char *afterOmpEnd = NULL ;
        if (afterOmpEnd = is_OMPEND(nextch)) {
          char *afterDirective = NULL ;
          if (afterDirective = is_OMPENDPARALLELDO(afterOmpEnd)) {
	    if (ClosingCount > 0) {
              PutTokStack(KW_ENDOFDO,-1);
              for (ClosingCount--; ClosingCount > 0; ClosingCount--) {
                PutTokStack(KW_EOL,-1);
                PutTokStack(KW_ENDOFDO, -1);
              }
	    }
            PutTokStack(KW_EOLOMPENDPARALLELDO, -1) ;
            nextch = afterDirective;
	    RETTOKEN(KW_EOL);
          } else if (afterDirective = is_OMPENDPARALLEL(afterOmpEnd)) {
            for ( ; ClosingCount > 0; ClosingCount--) {
                PutTokStack(KW_ENDOFDO, -1);
                PutTokStack(KW_EOL,-1);
              }
            PutTokStack(KW_OMPENDPARALLEL, -1) ;
            nextch = afterDirective;
            RETTOKEN(KW_EOL);
          } else if (afterDirective = is_OMPENDDO(afterOmpEnd)) {
	    if (ClosingCount > 0) {
              PutTokStack(KW_ENDOFDO,-1);
              for (ClosingCount--; ClosingCount > 0; ClosingCount--) {
                PutTokStack(KW_EOL,-1);
                PutTokStack(KW_ENDOFDO, -1);
              }
	    }
            PutTokStack(KW_EOLOMPENDDO, -1) ;
            nextch = afterDirective;
	    RETTOKEN(KW_EOL);
          }
        } else if (nextch >= lastch) {
	    if (ClosingCount > 0) {
		PutTokStack(KW_ENDOFDO,-1);
		for(ClosingCount--; ClosingCount > 0; ClosingCount--) {
		    PutTokStack(KW_EOL,-1);
		    PutTokStack(KW_ENDOFDO, -1);
		}
	    }
	    RETTOKEN(KW_EOL);
	}

	havedot = False;
	yyleng = 0;
        /*fprintf(stderr,"nextch=%d\n", *nextch); */
	switch(*nextch) {
	case SPEC_HOLL : 
		len = GetStrCnt(nextch+1); /* take back StrCnt */
		GetTokenValue(len, False);	   /* Put the value in yytext */
		nextch += 3;
		reprint_holl(0,-1);
		tok = HOLLERITH;
		break;

	case SPEC_STRING:
		/* 'nnn'radix bit_cst or string */
		tok = GetBit_cst(*(nextch+3));
		if (tok != ASTRING) nextch++; /* skip the radix */
		/* fprintf(stderr,"str=%s\n", yytext);*/
		break;

	case SPEC_COMM:
	        if (FreeFormat) { /* immediately send the comments */
		    ImmediateFlushComments(nextch+1,1,1);
		} else {
		    ProcessFixComment(nextch,0);
		}
		nextch += 3;
		goto retry;

	case ' ':			/* not necessary in principle */
		nextch++;
		goto retry;		/* but used for directives */

	case '.':
		/* fprintf(stderr,"1--> %s\n", nextch); */
		nextch++;
		if(nextch >= lastch)
			goto badchar;
		if(isdigit(*nextch)) {
			TAKE('.');
			havedot = True;
			goto number;
		}
		tok = GetKey(Dots,nil,&len);
		if(!StructVax && tok <= 0) { /* user operator ?? */
		    char *nextdot = strchr(nextch, '.');
		    if (nextdot) {  
			int isident = 1;
			char *buff;

			buff = nextch;
			while (isident && buff < nextdot) {
			    isident = isalnum(*buff); buff++;
			}
			if (isident) {
			    tok = KW_OP_USER;
			    NeedOpUser = nextdot;
			} else {
			  tok = KW_DOT;
			}
		    } else {
		      tok = KW_DOT;
		    }
		  } else {
		      if(tok <= 0) tok = KW_DOT;
		  }
		nextch += len;
		break;

	case '(':
		nextch++;
		if(nextch >= lastch 
		   || *nextch != '/'
		   || (*nextch == '/' && InOperatorSpec == True)) {
		  tok = KW_LPAR;
		} else {
		  nextch++;
		  tok = KW_LBRACKET;
		}
		break;
	case ')':
		nextch++;
		tok = KW_RPAR;
		break;
	case '[':
		nextch++;
		tok = KW_LBRACKET;
		break;
	case ']':
		nextch++;
		tok = KW_RBRACKET;
		break;
	case ',':
		nextch++;
		/* Optional Comma in common and namelist before a name group */
		if(OptCommactl && *nextch == '/') {
		    nextch++;
                    tok = KW_SLASH;
		} else {
		    /* send comment first if it exists to have them in a better
		       place when building the tree */
		    if (*nextch == SPEC_COMM || 
			(*nextch == ' ' && *(nextch+1) == SPEC_COMM)) {
			if (*nextch == ' ') nextch++;
			ImmediateFlushComments(nextch+1,1,0);
			nextch += 3;
		    }
		    tok = KW_COMMA;
		}
		break;
	case '+':
		nextch++;
		tok = KW_PLUS;
		break;
	case '-':
		nextch++;
		tok = KW_MINU;
		break;
	case '&':
		nextch++;
		tok = KW_STAR;
		break;
	case '\'':
		nextch++;
		tok = KW_QUOT;
		break;
	case ':':
		nextch++;
		tok = KW_COLON;
		break;
	case '*':
		nextch++;
		if(nextch >= lastch || *nextch != '*') tok = KW_STAR;
		else {
			nextch++;
			tok = KW_STARSTAR;
		}
		break;
	case '/':
		nextch++;
		if(nextch >= lastch || 
		   (*nextch != '/' 
		    && (*nextch != ')' || (*nextch == ')' && InOperatorSpec == True))
		    && *nextch != '=')) {
		        tok = KW_SLASH;
		} else if (*nextch == '/') {
			nextch++;
			tok = KW_SLASHSLASH;
		} else if (*nextch == ')') {
			nextch++;
			tok = KW_RBRACKET;
		} else if (*nextch == '=') {
			nextch++;
			tok = KW_NE;
		}
		break;

	case '=':
	case '<':
	case '>':
		tok = GetKey(BopStart,nil,&len);
		if(tok <= 0)
			goto badchar;
		nextch += len;
		break;
	case '%':		 /* extension %LOC,... %fill */
		TAKE('%'); nextch++; yyleng = 1;
                if (LastToken == NAME || LastToken == KW_RPAR) {
		    RETTOKEN(KW_PCEN);	/* f90 field ref. */
		}
		if (!isalpha(*nextch)) goto badchar;
		while((*nextch > 0 && Isalnum(*nextch))) {
		    /* TAKE the real char stored in the real buf */
		    TAKE(Realbuf[nextch-Inbuf]);
		    nextch++;
		}
		if(yyleng == 0) goto badchar;
		tok = NAME;
		break;
        case ';':      /* multiple statements on a line */    
		    nextch++;
		    tok = KW_EOL;
		    /* Are there ending DO loops */
		    if(ClosingCount > 0) {
		      PutTokStack(KW_ENDOFDO,-1);
		      for(ClosingCount--; ClosingCount > 0; ClosingCount--) {
			PutTokStack(KW_EOL,-1);
			PutTokStack(KW_ENDOFDO, -1);
		      }
		      PutTokStack(KW_EOL,-1);
		    }
		    PutTokStack(KW_PVIR,-1);
		    break;
	default:
		if(!isdigit(*nextch))
			goto other;
number:
		if (havedot) tok = REALCST;
		else tok = INTCST;
		for( ; nextch < lastch; ) {
			if(isdigit(*nextch)) {
				TAKE(*nextch++);
			}
			else if(*nextch == '.')
				if(havedot) {
					tok = REALCST;
					break;
				}
				else if(nextch+2 < lastch &&
				    isalpha(nextch[1]) && isalpha(nextch[2]))
					break;
				else {
					havedot = True;
					tok = REALCST;
					TAKE(*nextch++);
				}
			else if (GouldFortran && LastToken == KW_COLON &&
				 (Isalnum(*nextch) || *nextch == '$' || 
				  *nextch == '_' || *nextch == ':')) {
			    while((*nextch > 0 && Isalnum(*nextch)) ||
				  *nextch == '$' || *nextch == '_'  ||
				  *nextch == ':') {
				      /* TAKE the real char in the real buf */
				      TAKE(Realbuf[nextch-Inbuf]);
				      nextch++;
				  }
			    tok = NAME;
			    break;
			} else if(!IntOnly && (   *nextch == 'd'
					     || *nextch == 'e'
					     || *nextch == 'q')) {
				if(*nextch == 'e') {
				  tok = REALCST;
				} else { tok = DOUBLECST; }
				sav = nextch;
				TAKE(*nextch++);
				if(nextch < lastch &&
				   (*nextch == '+'  || *nextch == '-')) {
					TAKE(*nextch++);
				}
				if(nextch >= lastch || !isdigit(*nextch)) {
					yyleng -= (nextch - sav);
					nextch = sav;
					tok = INTCST;
					break;
				}
				for( ; nextch < lastch && isdigit(*nextch) ; ){
					TAKE(*nextch);
					nextch++;
				}
				break;
			} else if(!IntOnly && (*nextch == 'b')) {
			    nextch++;
			    tok = BITCST;
			    reprint_bitcst('O');
			    break;			    
			} else break;
		}
		/* optional comma in type size specification,
		   right after the typename, not in the decl itself */
		/* Must distinguish between f77 and CMF declarations */
		if (SizeCommactl && (InDeclctl == False)) {
		    if ((*nextch == ',')  &&
			((STable[pSTable].GotDblColon == False) ||
			 ((*(nextch+1) == ':') && (*(nextch+2) == ':'))))
                       nextch++;
		    SizeCommactl = False;
	    	}
		break;

other:
		sav = nextch;
		if(Need_Kwd || Opt_Need_Kwd) {
		    if (CurTypeDir == DIR_TYPE_HPF) {
			if (!SendKWDir) {
			    tok = GetKey(HPF_Keywords,nil,&len);
			    if (IS_A_HPF_STAT(tok))
                                tok = KW_HPF_STAT;
			    else
                                tok = KW_HPF_SPEC;
			    SendKWDir = True;
			    len = 0;
                        } else
                            tok = GetKey(HPF_Keywords,nil,&len);
                    } else {
			if ((len = *nextch - 'a') < 0) /* KWD starting with # */
                            tok = GetKey(Keywords,KeyStart[0],&len);
			else
                            tok = GetKey(KeyStart[len],KeyStop[len],&len);
                    }
		    if ( tok == KW_ONLY && is_ONLY() == 0) { tok = -1; }
		    if ( tok == KW_OPERATOR ) { InOperatorSpec = True; }
		    if ( tok > 0  && 
			 (!Need_Op_Kwd 
			  || (Need_Op_Kwd && IS_OP_KEYWORD(tok))
			  || tok == KW_ONLY))
                        nextch += len;
		    if (InImplicit && IS_A_BASIC_TYPE(tok)) {
			/* look for FORTRAN 90 implicit type */
			Search_for_Implicit90(nextch,lastch);
			InImplicit = False;
		    }
		    NeedKeyword(False);
		    /* we assume Opt_Need_Kwd, Need_Op_Kwd, Need_Type_Kwd are exclusive */
		    if (Opt_Need_Kwd) {
		      /* If Opt_Need_Kwd and tok < 0 we do not break */
		      OptNeedKeyword(False);
		      if (tok > 0) {
			break;
                      }
                    } else if (Need_Type_Kwd) {
                      Need_Type_Kwd = False;
                      if (IS_A_BASIC_TYPE(tok)) {
                        /* Keep tok as a keyword: */
                        break ;
		      } else {
                        /* prepare for turning back tok into a NAME: */
                        nextch = sav ;
                      }
		    } else {
		      /* When Need_Op_Kwd and tok is an OP kwd, keep the tok
			 as a kwd. Else we continue (i.e we do not break) and
			 the tok will surely become a NAME */
		      if (!Need_Op_Kwd 
			  || (Need_Op_Kwd && IS_OP_KEYWORD(tok))
			  || tok == KW_ONLY) {
			break;
		      }
		    }
                }
		if (IS_A_NUMERIC_CST(LastToken)) {
		    if (*nextch == '_') {
			tok = KW_UNDERSCORE;
			nextch++;
			break;
                    }
                }
		if(nextch >= lastch) {
			tok = KW_EOL;
			break;
		}
		/* fprintf(stderr,"--> %s\n", nextch); */
		if (Common_Flag && *nextch > 0 && Isalnum(*nextch)) TAKE('/');
		while(   (*nextch > 0 && Isalnum(*nextch)) 
		      || *nextch == '$' || *nextch == '_'  || *nextch == '@'
		      || (NeedGouldColon && *nextch == ':')) {
		    /* TAKE the real char stored in the real buf */
		    TAKE(Realbuf[nextch-Inbuf]);
		    nextch++;
		    if (*nextch == '_' && *(nextch+1) == SPEC_STRING) {
		        PutTokStack(KW_UNDERSCORE,-1);
			nextch++;
		        break;
		    }
		}
		if (Common_Flag) {TAKE('/') ; CommonFlag(False);};
/*                 printf("Inbuf : |%s| \n yytext : %s \n", Inbuf, yytext); */
		if(yyleng == 0)
			goto badchar;
		tok = NAME;
		while(*nextch == ' ' && nextch <= lastch)
			nextch++;
		if(InIoctl && *nextch == '=') {
			tok = EQNAME;
			nextch++;
		}
		else if(sav+5 <= lastch &&
			Do_While_Kwd &&
			!(STable[pSTable].GotEqual)  &&
			(eqlen = CompactStrEq(sav,"while",5))) {
		    tok = KW_WHILE;
		    Do_While_Kwd = 0;
		    nextch = sav + eqlen;
		} /* For Gould as DO WHILE => DO UNTIL */
		else if(sav+5 <= lastch &&
			Do_While_Kwd &&
			!(STable[pSTable].GotEqual)  &&
			(eqlen = CompactStrEq(sav,"until",5))) {
		    tok = KW_UNTIL;
		    Do_While_Kwd = 0;
		    nextch = sav + eqlen;
		} /* For Gould as DO WHILE => DO BEGIN */
                else if(GouldFortran &&
                        sav+5 <= lastch &&
                        Do_While_Kwd &&
                        !(STable[pSTable].GotEqual)  &&
                        (eqlen = CompactStrEq(sav,"begin",5))) {
                    tok = KW_BEGIN;
                    Do_While_Kwd = 0;
                    nextch = sav + eqlen;
                }
		else if(eqlen = is_FUNCTION(sav,nextch,lastch)) {
                  tok = KW_FUNCTION;
			nextch = sav + eqlen;
		}
		else if (FreeFormat && 
			 is_PREFIX(sav,nextch,lastch,"recursive",9)) {
                    /* look for recursive KWD in freeFormat */
		    tok = KW_RECURSIVE;
		}
		else if (FreeFormat && 
			 is_PREFIX(sav,nextch,lastch,"pure",4)) {
                    /* look for pure KWD in freeFormat */
		    tok = KW_PURE;
		}
		else if (FreeFormat && 
			 is_PREFIX(sav,nextch,lastch,"elemental",9)) {
                    /* look for pure KWD in freeFormat */
		    tok = KW_ELEMENTAL;
		}
		else if(yyleng == 1 && *nextch == SPEC_STRING) {
		    /* bit_cst of the form : radix'nnnn' */
		    tok = GetBit_cst(yytext[0]);
		    if (tok == ASTRING)
			/*string impossible here*/
			LexError(True, "bad bit identifier");
		    /* else 	VerifyNumber(radix); */
		    /* fprintf(stderr,"str=%s\n", yytext);*/
		}
		else if(yyleng == 1 && Need_Letter)
		    tok = LETTER;
		break;
	    }
	yytext[yyleng] = '\0';
	NeedKeyword(False);
	RETTOKEN(tok);

badchar:
	LexError(True, "bad char %d '%c'",*nextch,*nextch);
	return -1;
}

/* true if <c> may start a format spec for F66 TYPE statement */
Boolean maystartFMT(char *c) {
    while (*c == ' ' || *c == SPEC_COMM) {
      if (*c == ' ') { c++; }
      else if (*c == SPEC_COMM) { c += 3; }
    }
    return ( isdigit(*c) || *c=='*' || *c== SPEC_STRING || *c== SPEC_HOLL);
}

/* ---------------------------------------------------------------------- */
/* |	Search for a CM-fortran tag preeceding the statement:		| */
/* |	such as :      loop1: do 100 i=...	                        | */
/* |    In GouldFortran no tag !!					| */
/* ---------------------------------------------------------------------- */
int Search_for_tag() {
    char	*cc=nextch;

    if (!GouldFortran) {
	for(; cc <= lastch && (   (*cc >= 'a' && *cc <= 'z')
			       || (*cc >= '0' && *cc <= '9')
			       || *cc == '_') ; cc++);
        if (FreeFormat && *cc == ' ') cc++; /* skip ' ' sep for free format */
	if (cc <= lastch && *cc == ':' && *(cc+1) != ':') {
	    for(yyleng = 0 ; *nextch != ':' ; ) {
		if (*nextch != ' ') /* skip ' ' sep for free *format */
		    TAKE(Realbuf[nextch-Inbuf]);
		nextch++;
	    } /* yytext[yyleng++] = *nextch++; */
	    nextch++; /* to skip the colon */
	    yytext[yyleng] = '\0';
	    MainKey = NAME;
	    return True;
	} else  return False;
    } else return False;
}

/* ---------------------------------------------------------------------- */
/* |	Skip parenthesis in order to discriminate between different	| */
/* |	statements                      	                        | */
/* ---------------------------------------------------------------------- */
char * SkipPar(int Tlen) {
	int	parlev;
	char	*curc,cc;

	parlev = 1;
	/* skip parentheses */
	for(curc = nextch + Tlen ; curc < lastch; curc++) {
		cc = *curc;
		if(cc == SPEC_COMM || cc == SPEC_STRING || cc == SPEC_HOLL)
			curc += 2;
		else if(cc == '(')
			parlev++;
		else if(cc == ')') {
			parlev--;
			if(parlev <= 0) {
				curc++;
				break;
			}
		}
	}
	return curc;
}

/* ---------------------------------------------------------------------- */
/* |	Test if the key word is a IF                                    | */
/* ---------------------------------------------------------------------- */
int is_IF () {
  int test = 0;

  if (FreeFormat)
    {  
      if (nextch+3 <= lastch)
	test = CompactStrEq(nextch,"if(",3);
      if(!test && nextch+4 <= lastch)
	test = CompactStrEq(nextch,"if (",4);
    }
  else
    {
      if (nextch+3 <= lastch)
	test = CompactStrEq(nextch,"if(",3);
    }
  return test;
}

/* ---------------------------------------------------------------------- */
/* |	Test if the key word is a WHERE                                 | */
/* ---------------------------------------------------------------------- */
int is_WHERE () {
  int test = 0;

  if (FreeFormat)
    {  
      if (nextch+6 <= lastch)
	test = CompactStrEq(nextch,"where(",6);
      if (!test && nextch+7 <= lastch)
	test = CompactStrEq(nextch,"where (",7);
    }
  else
    {
      if (nextch+6 <= lastch)
	test = CompactStrEq(nextch,"where(",6);
    }
  return test;
}

/* ---------------------------------------------------------------------- */
/* |	Test if the key word is a ONLY :                                | */
/* ---------------------------------------------------------------------- */
int is_ONLY () {
  int test = 0;

  if (FreeFormat)
    {  
      if (nextch+5 <= lastch)
	test = CompactStrEq(nextch,"only:",5);
      if (!test && nextch+6 <= lastch)
	test = CompactStrEq(nextch,"only :",6);
    }
  else
    {
      if (nextch+5 <= lastch)
	test = CompactStrEq(nextch,"only:",5);
    }
  return test;
}

/* ---------------------------------------------------------------------- */
/* |	Test if the key word is a ELSEWHERE                                 | */
/* ---------------------------------------------------------------------- */
int is_ELSEWHERE () {
  int test = 0;

  test = CompactStrEq(nextch,"elsewhere",9);
  return test;
}

/* ---------------------------------------------------------------------- */
/* |	Test if the key word is a FORALL                                | */
/* ---------------------------------------------------------------------- */
int is_FORALL () {
  int test = 0;

  if (FreeFormat)
    {  
      if (nextch+7 <= lastch)
	test = CompactStrEq(nextch,"forall(",7);
      if (!test && nextch+8 <= lastch)
	test = CompactStrEq(nextch,"forall (",8);
    }
  else
    {
      if (nextch+7 <= lastch)
	test = CompactStrEq(nextch,"forall(",7);
    }
  return test;
}

/* ---------------------------------------------------------------------- */
/* |	Test if the key word is a computed GOTO                         | */
/* ---------------------------------------------------------------------- */
int is_COMPGOTO () {
  int i;

  i = 0;
  while (nextch[i] == ' ' || nextch[i] == SPEC_COMM) {
    if (nextch[i] == ' ') { i++; }
    else if (nextch[i] == SPEC_COMM) { i += 3; }
  }
  if ( nextch[i] == 'g' && nextch[i+1] == 'o' ) {
    i += 2;
    while (nextch[i] == ' ' || nextch[i] == SPEC_COMM) {
      if (nextch[i] == ' ') { i++; }
      else if (nextch[i] == SPEC_COMM) { i += 3; }
    }
    if ( nextch[i] == 't' && nextch[i+1] == 'o' ) {
      i += 2;
      while (nextch[i] == ' ' || nextch[i] == SPEC_COMM) {
	if (nextch[i] == ' ') { i++; }
	else if (nextch[i] == SPEC_COMM) { i += 3; }
      }
      if ( nextch[i] == '(' ) { return True; }
      else { return False; }
    } else { return False; }
  } else { return False; }
}

/* ---------------------------------------------------------------------- */
/* |	Test if the key word is a assigned GOTO                         | */
/* ---------------------------------------------------------------------- */
int is_ASSGOTO () {
  int i;

  i = 0;
  while (nextch[i] == ' ' || nextch[i] == SPEC_COMM) {
    if (nextch[i] == ' ') { i++; }
    else if (nextch[i] == SPEC_COMM) { i += 3; }
  }
  if ( nextch[i] == 'g' && nextch[i+1] == 'o' ) {
    i += 2;
    while (nextch[i] == ' ' || nextch[i] == SPEC_COMM) {
      if (nextch[i] == ' ') { i++; }
      else if (nextch[i] == SPEC_COMM) { i += 3; }
    }
    if ( nextch[i] == 't' && nextch[i+1] == 'o' ) {
      i += 2;
      while (nextch[i] == ' ' || nextch[i] == SPEC_COMM) {
	if (nextch[i] == ' ') { i++; }
	else if (nextch[i] == SPEC_COMM) { i += 3; }
      }
      if ( isalpha(nextch[i]) ) { return True; }
      else { return False; }
    } else { return False; }
  } else { return False; }
}
/* ---------------------------------------------------------------------- */
/* |	Test if sav is on a function kwd                                | */
/* |    Look for the "function <name> (.*)" pattern			| */
/* |    sav (in) may be the begining of the function kwd		| */
/* |    next_ch(in) is on the token immediately folowing sav		| */
/* |	last_ch(in) is the end of the current line			| */
/* ---------------------------------------------------------------------- */
int is_FUNCTION (char *sav,char *next_ch,char *last_ch) {
    char *firstpar;
    int  test, i, one_blank;

    if (sav+9 <= last_ch) { /* line must be longer than 9 */
        test = CompactStrEq(sav,"function",8); /* look for function */
        if (test) {
            if (FreeFormat) { /* in free ' ' are significant ! */
	      /* Test if the next non blank and non SPEC_COMM character isalpha */
	      i = 8; one_blank = 0;
	      while (sav[i] == ' ' || sav[i] == SPEC_COMM) {
		if (sav[i] == ' ') { i++; one_blank = 1; }
		else if (sav[i] == SPEC_COMM) { i += 3; }
	      }
	      if (isalpha(sav[i]) && one_blank) { /* next token isalpha */
                /* search for a '(' */
		firstpar = next_ch;
		while ((firstpar <= last_ch) && (*firstpar != '(')) { firstpar++; }
		if (firstpar != NULL && firstpar <= last_ch) {
		    return test;
		}
	      }
            } else { /* fixed format */
	      /* Test if the next non SPEC_COMM character isalpha */
	      i = 8;
	      while (sav[i] == SPEC_COMM) { i += 3; }
	      if (isalpha(sav[i])) { /* next token isalpha */
                /* search for a '(' */
		firstpar = next_ch;
		while ((firstpar <= last_ch) && (*firstpar != '(')) { firstpar++; }
		if (firstpar != NULL && firstpar <= last_ch) {
		    return test;
		}
	      }
            }
        }
    }
    /* Does not match the heuristics */
    return False;
}

/* ---------------------------------------------------------------------- */
/* |	Test if sav is on a free format function prefix                 | */
/* |    We are in free format :                                         | */
/* |    Look for the "<prefix>+ function <name> (.*)" pattern		| */
/* |    sav (in) may be the begining of the <prefix> kwd		| */
/* |    next_ch(in) is on the token immediately folowing sav		| */
/* |	last_ch(in) is the end of the current line			| */
/* |    prefix(in)  is the name of the prefix (pure or recursive)	| */
/* |    prefixlen(in) is strlen(prefix)					| */
/* ---------------------------------------------------------------------- */
int is_PREFIX (char *sav, char *next_ch, char *last_ch, char *prefix, int prefixlen) { 
    if (sav+prefixlen+2 <= last_ch) { /* avoid simple decl like "real pure" */
	if (CompactStrEq(sav,prefix,prefixlen)) { /* look for prefix */	
	    char *tok_after_func;
	    char *func = strstr(sav,"function");
	    if (func != NULL) { /* now look for function somewhere */
		tok_after_func = strchr(func,' ');
		if (tok_after_func != NULL) {
		    tok_after_func++;
		    return is_FUNCTION(func,tok_after_func,last_ch);
		}
	    }
	}
    }
    return False; /* heuristic is False */
}
/* ---------------------------------------------------------------------- */
/* |	Test if the key word is a DISTRIBUTE                               | */
/* ---------------------------------------------------------------------- */
int is_DISTRIBUTE () {
  int test = 0;

  if (FreeFormat) {  
      if (nextch+10 <= lastch)
	test = CompactStrEq(nextch,"distribute",10);
  }
  if (test) {
    char *test2 ;
    test2 = strchr(nextch, '(' ); //strchr was casted as (int)
      if (test2!=NULL)
	test = (int)(test2-nextch) + 1; //nextch was casted as (int)
  }
  return test;
}

/* ---------------------------------------------------------------------- */
/* |	Some heuristics to classify the statement:			| */
/* |									| */
/* |	* "elsewhere"::        -> KW_ELSEWHERE                         | */
/* |	* "if("  ::	- skip well balanced expression,		| */
/* |			- if eoline -> KW_LOGIF				| */
/* |			  else if GotEqual -> KW_ASS			| */
/* |			  else if digit -> KW_ARITHIF			| */
/* |			  else -> KW_LOGIF				| */
/* |	* GotEqual ::	- GotComma && "do"  -> KW_DO			| */
/* |			  GotComma && "use" -> KW_USE	       		| */
/* |			  else KW_ASS					| */
/* |	    GotEqual will be set only for equal not inside parenthesis  | */
/* |	    and not following a double colon sign outside a parenthesis | */
/* |	* "do" && a "while" somewhere :: -> KW_DO			| */
/* |	* else search a keyword :					| */
/* |		- "goto(" -> KW_COMPGOTO				| */
/* |		- "goto" + alpha -> KW_ASSGOTO				| */
/* |		- "format" -> gather text into a single string		| */
/* |    * For GOULD : KWD USER, SPACE, PAGE return KWD -1 => they are   | */
/* |	  ignored							| */
/* ---------------------------------------------------------------------- */
int Classify() {
	int	K,Tlen,tok,i;
	char	*curc,*debc,*endformat,cc, eqlen;
/* fprintf(stderr, "CLASSIFYING |%s|\n", strndup(nextch,10)) ; */
	Tlen = 0;
	while (*nextch == ' ')
	  nextch++;
	if(eqlen=is_IF()) {
		curc = SkipPar(eqlen);
		cc = *curc;
		while (cc == ' ') {
		    if (cc == ' ') /* skip the blank */
		      curc++; 
		    cc = *curc;
		}
		if(curc >= lastch || cc == SPEC_COMM)
			tok = KW_LOGIF;
		else if(cc == '=')
			tok = KW_ASS;
		else if(isdigit(cc) || cc== ',')
			tok = KW_ARITHIF;
		else tok = KW_LOGIF;
		if(tok != KW_ASS)
			Tlen = 2;
	} else if(eqlen=is_WHERE()) {
		curc = SkipPar(eqlen);
		cc = *curc;
/* if only SPEC_COMM or blank after the where(...) it is a STRUCTWHERE
   else it is a WHERE statement */
		while (cc == ' ' || cc == SPEC_COMM) {
		  if (cc == ' ') { curc++; }
		  else if (cc == SPEC_COMM) { curc += 3; }
		  cc = *curc;
		}
		if(cc == '\0')
			tok = KW_STRUCTWHERE;
		else if(*curc == '=')
			tok = KW_ASS;
		else tok = KW_WHERE;
		if(tok != KW_ASS)
			Tlen = 5;
	} else if(eqlen=is_FORALL()) {
		curc = SkipPar(eqlen);
		cc = *curc;
/* if only SPEC_COMM or blank after the forall(...) it is a STRUCTFORALL
   else it is a FORALL statement */
		while (cc == ' ' || cc == SPEC_COMM) {
		  if (cc == ' ') { curc++; }
		  else if (cc == SPEC_COMM) { curc += 3; }
		  cc = *curc;
		}
		if(cc == '\0')
			tok = KW_STRUCTFORALL;
		else if(*curc == '=')
			tok = KW_ASS;
		else tok = KW_FORALL;
		if(tok != KW_ASS)
			Tlen = 6;
	} else if(eqlen=is_ELSEWHERE()) {
		curc = nextch + 9;
		cc = *curc;
		while (cc == ' ') {
		    curc++; 
		    cc = *curc;
		}
		if(*curc == '=')
			tok = KW_ASS;
		else {tok = KW_ELSEWHERE; Tlen = 9;}
	} else if(*nextch == SPEC_DIR) {
	    Boolean isFlcDir = Flc90 && ('/' == nextch[1]);
	    if (isFlcDir) { /* flc90 directive */
		nextch++;
		tok = FLCDIRECTIVE;
            } else if (CompactStrEq(nextch+1,"omp",3)) { /* OpenMP directives */
                tok = OMPDIRECTIVE;
	    } else {		/* regular C$dir style directive */
		tok = DIRNAME;
	    }
	    for(nextch++, yyleng = 0 ;
		nextch <= lastch && ( (*nextch >= 'a' && *nextch <= 'z') 
				     || *nextch == '_') ; ) {
		TAKE(Realbuf[nextch-Inbuf]);
		nextch++;
	    }
	    if (isFlcDir) nextch++;
	    yytext[yyleng] = '\0';
	    InIO(True);		/* to recognize EQNAME tokens */
	}  else if(nextch+8 <= lastch && strncmp("options/",nextch,8) == 0) {
		curc = SkipPar(8);
		if(curc >= lastch)
			tok = KW_OPTIONS;
		else if(*curc == '=')
			tok = KW_ASS;
		else tok = KW_OPTIONS;
		if(tok != KW_ASS) {
		    Tlen = 7;
		}
	} else if (STable[pSTable].GotEqual) {
		if (STable[pSTable].GotComma && nextch+2 <= lastch &&
		    CompactStrEq(nextch,"do",2)) {
			tok = KW_DO;
			Tlen = 2;
		} else if (STable[pSTable].GotComma && nextch+3 <= lastch &&
		    CompactStrEq(nextch,"use",3)) {
			tok = KW_USE;
			Tlen = 3;
		}
		else tok = KW_ASS;
	}
	/* traitement du while ?? */
	else {
	        if (CurTypeDir == DIR_TYPE_HPF) {
		    if (!SendKWDir) {
			tok = GetKey(HPF_Keywords,nil,&Tlen);
			if (IS_A_HPF_STAT(tok))
			  tok = KW_HPF_STAT;
			else
			  tok = KW_HPF_SPEC;
			SendKWDir = True;
			Tlen = 0;
		    } else
			tok = GetKey(HPF_Keywords,nil,&Tlen);
		    if (tok == KW_DISTRIBUTE) {
                        char* eqlen2 ;
			eqlen2 = strchr(nextch, '(' ); //strchr was casted as (int)
			if (eqlen2!=NULL)
			  eqlen = (int)(eqlen2-nextch) + 1; //nextch was casted as (int)
			curc = SkipPar(eqlen);
			cc = *curc;
			while (cc == ' ') {
			    if (cc == ' ') /* skip the blank */
				curc++; 
			    cc = *curc;
			}
			if(curc >= lastch || cc == SPEC_COMM || cc == ':')
			  tok = KW_DISTRIBUTE;
			else tok = KW_DISTRIBUTEONTO;
			Tlen = 10;
		    }
		    if (tok == KW_REDISTRIBUTE) {
                        char* eqlen2 ;
                        eqlen2 = strchr(nextch, '(' ); //strchr was casted as (int)
			if (eqlen2!=NULL)
			  eqlen = (int)(eqlen2-nextch) + 1; //nextch was casted as (int)
			curc = SkipPar(eqlen);
			cc = *curc;
			while (cc == ' ') {
			    if (cc == ' ') /* skip the blank */
			      curc++; 
			    cc = *curc;
			}
			if(curc >= lastch || cc == SPEC_COMM || cc == ':')
			  tok = KW_REDISTRIBUTE;
			else tok = KW_REDISTRIBUTEONTO;
			Tlen = 12;
		    }
		} else {
                    if ((K = *nextch - 'a') < 0) { /* KWD starting with # */
                      tok = GetKey(Keywords,KeyStart[0],&Tlen);
		    } else if ( K<26 ) {
		      tok = GetKey(KeyStart[K],KeyStop[K],&Tlen);
		    } else tok = -1;
		}
		if (tok == KW_INTERFACEOP || tok == KW_ENDOPERATOR) {
		  InOperatorSpec = True;
		}
		if(tok <= 0) {
			LexError(True, "Unknown keyword : %s ", nextch);
			ErrorFlag = True;
		} else if(tok == KW_GOTO) {
                        if (*nextch == ' ') nextch++;
			if(is_COMPGOTO())
				tok = KW_COMPGOTO;
			else if(is_ASSGOTO())
				tok = KW_ASSGOTO;
				else;
		} else if (GouldFortran &&
			   (tok == KW_NULL || tok == KW_OPTION)) {
		    /* special GOULD token processing (ignored statement) */
		    if (tok == KW_OPTION) { /* set in comments */
			SetLineinComments();
		    }
		    tok = -1; 	      /* ignored lines with no EOL  */
		    NoEOLToken = True;    /* token at the end	    */
		    /* Look for comments to keep in zapped line */
		    ParseCommentinLine();
		} else if (GouldFortran &&
			   (tok == KW_CASE || tok == KW_SELECTCASE)) {
		    /* special GOULD CASE ... SELECT CASE */
		    if (tok == KW_CASE)     	    tok = KW_GOULDCASE;
		    else /* tok = KW_SELECTCASE */  tok = KW_GOULDSELECTCASE;
		} else if (GouldFortran &&
			   (tok == KW_INCLUDE)) {
		    /* special GOULD INCLUDE .... */
		  tok = ProcessGouldInclude();
		} else if(tok == KW_FORMAT) {
			nextch+= 6;
                        if (*nextch == ' ') nextch++;
			endformat = lastch;
			/* Scan the list of format elems, put one token */
			if(*nextch == '(') nextch++;
			else {
			  yyerror();
			  ErrorFlag = 1;
			}
			/* Isolate the FORMAT elems from the comments that */
			/* may end the stat. */
			while(endformat > nextch  &&
			 (endformat[-1] != ')' || endformat[-3] == SPEC_COMM ))
			    endformat--;
			if(endformat > nextch && endformat[-1] == ')')
			    endformat--;
			else {
			  yyerror();
			  ErrorFlag = 1;
			}

			/* Read the FORMATELEM according to the   nlStack */
			/* with one "        " for each \n in the "  "    */
			debc = nextch; i = 0;
			do
			{ /* there is always at least one FORMATELEM */
			    while((i < nlSP) && (nlStack[i] <= nextch))
			      i++; /* \n within FORMAT Keyword, ignore it */
			    if (i == nlSP || (nlStack[i]>=endformat))
                                /* no more \n , or \n after endformat */
			        curc = endformat;
			    else 		/* there was \n */
				curc = nlStack[i];
			    i++;
			    K = curc - debc; /* nb of char in the FORMATELEM */
			    if (K>0) {
				ReBuildTokString(debc,K,False,FORMATELEM);
			    }
			    debc = curc;
			} while (i <= nlSP);
			nextch = endformat+1; /* the format is fully scanned */
			Tlen = 0;
		/* detect Fortran 66 TYPE, synonym for print */
		} else if (tok == KW_TYPE && maystartFMT(nextch+Tlen)) {
		    tok = KW_PRINT;
                    LexError(False, "Non standard statement TYPE (replaced with PRINT)");
		/* Disambiguate BACKSPACE, ENDFILE, REWIND, BACKFILE, SKIPFILE */
		} else if (tok == KW_BACKSPACE ||
		           tok == KW_ENDFILE ||
		           tok == KW_REWIND ||
		           tok == KW_BACKFILE ||
		           tok == KW_SKIPFILE) {
		    curc = nextch + Tlen;
		    cc = *curc;
		    while (cc == ' ' || cc == SPEC_COMM) {
		      if (cc == ' ') { curc++; } /* skip the blank */
	              else if (cc == SPEC_COMM) { curc += 3; } /* skip SPEC_COMM */ 
			    cc = *curc;
		    }
		    if (cc != '(') {
		      switch (tok) {
		      case KW_BACKSPACE :
			tok = KW_BACKSPACELOG;
		        break;
		      case KW_ENDFILE :
			tok = KW_ENDFILELOG;
		        break;
		      case KW_REWIND :
			tok = KW_REWINDLOG;
		        break;
		      case KW_BACKFILE :
			tok = KW_BACKFILELOG;
		        break;
		      case KW_SKIPFILE :
			tok = KW_SKIPFILELOG;
		        break;
		      }
		    }
		}
	    }
	nextch += Tlen;
	if(tok <= 0) {
		/* tok = 1;			unknown */
		nextch = lastch;		/* skip line */
	}
	MainKey = tok;
	return tok;
}

/* ---------------------------------------------------------------------- */
/* |	Functions called by Yacc :					| */
/* |       - Fyylex()	: is the basic FORTRAN Scanner			| */
/* |	   - H_Pyylex()	: is the Header Parser Scanner			| */
/* |	   - B_Pyylex()	: is the Body Parser Scanner			| */
/* |	These functions returns the code of the current token		| */
/* ---------------------------------------------------------------------- */
int	Fyylex()
{
  int	tok=0,i,len;
retry:
	yytext[0] = 0;
	switch(LxStatus) {
		/* at beginning of line */
	case ST_BEGIN :
		NeedKeyword(False);NeedOpKeyword(False);
		InIO(False);
		OptCommactl = False;
		InDeclctl = False;
		SizeCommactl = False;
		InOperatorSpec = False;
		/* at end of unit, send offsets */
		if (   Need_Offset!=NO
		       && IS_AN_END_UNIT(MainKey) && (InEntryPt == False)) {
		    if (Cc == EOF) StatementOffset = -1;
		    if (Need_Offset==SEND_START) {
			sprintf(yytext, "%d", UnitOffset);
			Need_Offset = SEND_END;
		    } else {                    
			sprintf(yytext, "%d", StatementOffset);
			Need_Offset = NO;
			UnitOffset = StatementOffset;
		    }
		    RETTOKEN(OFFSET);
		}
		curInputLineNo += readLines;		/* update line counter */
		/* in free format we send the comments into the token flow */
		/* see GetToken */
		if (!FreeFormat) { FlushComments(); }
		for( ; --readLines >= 0 ; ) 	/* tell it to Centaur */
		    LEXLINE();
		readLines = 0;
                /* If LastEntryPt the STable is OK for the current line */
		if (tok != KW_PVIR && !LastEntryPt) {
		  InitSTable(0);
		  STable[0].CurLabel = NextLabel;
		} else {
		  LastEntryPt = False;
		}
		if(FreeFormat) {
		    if(!FreeFormatGetFullLine()) {
			if (!IS_AN_END_UNIT(MainKey) && (InEntryPt == False) &&
			    (ErrorFlag == False)) {
			    /* incomplete file (include ..) and not Entry Point */
			    UNGET(EOF);
			    MainKey = KW_END_OF_FILE;
			    RETTOKEN(KW_END_OF_FILE); /* send first pseudo end */
                        }
			RETTOKEN(0);			 /* eof */
                    }
                } else { /* Fixed Format */
		    if(!GetFullLine(Cc != SPEC_DIR)) {
			/* do not skip spaces if  it is a directive ! */
			if (!IS_AN_END_UNIT(MainKey) && (InEntryPt == False) &&
			    (ErrorFlag == False)) {
			    /* incomplete file (include ..) and not Entry Point */
			    UNGET(EOF);
			    MainKey = KW_END_OF_FILE;
			    RETTOKEN(KW_END_OF_FILE); /* send first pseudo end */
                        }
			RETTOKEN(0);			 /* eof */
                    }
                }
/* printf( "FULLLINE:|%s| nextchindex:%i, length:%i\n", strndup(Inbuf, lastch-Inbuf), nextch-Inbuf, lastch-Inbuf) ; */
		/* in free format we send the comments into the token flow */
		/* see GetToken */
		if (!FreeFormat) { FlushComments(); }
		if(EntryPt >= 0) {
			int ept;
			ept = EntryPt;
			EntryPt = -1;
			LastEntryPt = True;
			/* fprintf(stderr,"entrypoint=%d\n",ept); */
			RETTOKEN(ept);
		}
		LxStatus = ST_KEY;
		if(tok != KW_PVIR)
		  pSTable = 0; /* reset stat level before anylzing the line */

		/* CAREFUL! we go on to send the label 
		   and find the main  keyword */

	case ST_KEY:
                if(STable[pSTable].CurLabel > 0) {
                  for(i = DoDepth-1; i >= 0; i--) /* end of do loops ? */
                    if(Dolabels[i] == STable[pSTable].CurLabel) {
                      if(i != DoDepth-1) {
                        yyerror();
                        ErrorFlag = 1;
                      }
                      ClosingCount = Docounts[i]; /* close all do  */
                      DoDepth = i;		  /* with same label */
                      break;
                    }
                  /* Now labels are passed separately, like comments. see below:
                     sprintf(yytext,"%d",STable[pSTable].CurLabel);
                     STable[pSTable].CurLabel = -1;
                     RETTOKEN(INTCST);
                  */
                  LABEL(STable[pSTable].CurLabel) ;
                  STable[pSTable].CurLabel = -1; /* reset it */
                }
                /* Skip the blanks before the SPEC_COMM */
                while(*nextch == ' ' && nextch <= lastch) {
                  nextch++;
                }
                while(*nextch == SPEC_COMM && nextch<lastch) {
                  if (FreeFormat) { /* immediately send the comments */
                    ImmediateFlushComments(nextch+1,1,1);
                  } else {          /* proces the comment */
                    ProcessFixComment(nextch,0);
                  }
                  nextch += 3;
                  while(*nextch == ' ' && nextch <= lastch)
                    nextch++;
                }
		if(nextch >= lastch) {
		    LxStatus = ST_BEGIN;
		    goto retry;	/* empty line ignored */
		}
		if (!Search_for_tag()) {
		  /* find main keyword */
                  Classify();
                  if (ErrorFlag) RETTOKEN(0);
                  LxStatus = ST_OTHER;
                  if(MainKey < 0) goto retry;
		}
		if (ClosingCount == 0 && MainKey == KW_ENDDO)
                  ClosingCount = 1;
#ifdef SHOWTOKENS
	  if (showtokens)
		fprintf(stderr,"MainKey=%d  val=%s\n",MainKey,yytext);
#endif
		RETTOKEN(MainKey);
		break;

	case ST_OTHER:
		tok = GetToken();
		if(tok == KW_PVIR) { 
		    LxStatus = ST_BEGIN;
		    goto retry;
		}
		if(tok == KW_EOL && (TokSP == TokBP)) {  /* TokStack empty */
		    LxStatus = ST_BEGIN;
		    if (NoEOLToken) {
			readLines--; /* really ignore the line */
			NoEOLToken = False;
			goto retry;
		    }
		}

#ifdef SHOWTOKENS
	  if (showtokens)
		fprintf(stderr,"tok=%d  val=%s\n",tok,yytext);
#endif
          /*fprintf(debugfile,"tok=%d  val=%s\n",tok,yytext);*/
		RETTOKEN(tok);
		break;
	    }
}

/* ---------------------------------------------------------------------- */
/* |	Put back yylex in state ST_KEY : used after a logical if	| */
/* ---------------------------------------------------------------------- */
void LexReclassify() {
	LxStatus = ST_KEY;
}

void LexState(int st) {
	switch(st) {
	case 1:
		LxStatus = ST_KEY;
		break;
	case 2:
		LxStatus = ST_OTHER;
		break;
	}
}


/*  for debug only */
#ifdef SHOWTOKENS
int pnextch()
{ char *s=nextch;
  while (s<lastch) {
      char c=*s;
      int cnt;
      switch (c) {
	case SPEC_STRING:
	  cnt = GetStrCnt(s+1);
	  fprintf(stderr,"@S%d",cnt);
	  s+=2;
	  break;
	case SPEC_COMM:
	  cnt = GetStrCnt(s+1);
	  fprintf(stderr,"@C%d",cnt);
	  s+=2;
	  break;
	case SPEC_DIR:
	  cnt = GetStrCnt(s+1);
	  fprintf(stderr,"@D");
	  break;
	case SPEC_HOLL:
	  cnt = GetStrCnt(s+1);
	  fprintf(stderr,"@H%d",cnt);
	  s+=2;
	  break;
	default:
	  fprintf(stderr,"%c",c);
	  break;
      }
      s++;
  }
  fprintf(stderr,"\n");
  return (lastch-nextch);
}
/*  useful GDB debugger commands
define state
printf " Cc = %d(%c) Ccol=%d MainKey=%d TabFormat=%d %d\n", Cc, Cc, Ccol, MainKey, TabFormat, pnextch()
end
*/

#endif

