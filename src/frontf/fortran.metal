-- $Id: fortran.metal,v 1.2 10/.0/.0 .1:.5:.0 vmp Exp $
definition of fortran is
   -- TOP
   rules
      <axiom> ::= <units> <acceptCommentsPost> ;
         <units>
      <axiom> ::= "END_OF_FILE" ;
         units-list (())
      <unit> ::=
         <topoptions> <decl> <acceptCommentsAndEOL> <decls> <topStats> <acceptCommentsPost> <locals>
            <trailer> "NEEDOFFSET" <opt_offset> ;
         unit
            (<topoptions>, none (), decls-pre (<decl>, <decls>), <topStats>,
                <locals>, <trailer>, <opt_offset>)
      <unit> ::=
         <topoptions> <realStat> <acceptCommentsAndEOL> <stats> <acceptCommentsPost> <locals>
            <trailer> "NEEDOFFSET" <opt_offset> ;
         unit
            (<topoptions>, none (), decls-list (()),
                stats-pre (<realStat>, <stats>), <locals>, <trailer>,
                <opt_offset>)
      <unit> ::=
         <topoptions> <header> <acceptCommentsAndEOL> <decls> <topStats> <acceptCommentsPost>
            <locals> <trailer> "NEEDOFFSET" <opt_offset> ;
         unit
            (<topoptions>, <header>, <decls>, <topStats>, <locals>, <trailer>,
                <opt_offset>)
      <topoptions> ::= "OPTIONS" <options> "EOL" ;
         <options>
      <topoptions> ::= ;
         options-list (())
      <option> ::= <simpleOption> ;
         <simpleOption>
      <option> ::= "CHECK" "=" "NEEDKEYWORD" <checkOption> ;
         <checkOption>-- UNIT HEADER
         
      <header> ::= "PROGRAM" <opt_ident> ;
         program (<opt_ident>)
      <header> ::= "BLOCKDATA" <opt_ident> ;
         blockdata (<opt_ident>)
      <header> ::= "MODULE" <ident> ;
         module (<ident>)
      <header> ::= "EXTRINSIC" "(" <ident> <closeParenthRecla> <type_header> ;
         hpfHeader (<ident>, <type_header>)
      <header> ::= "SUBROUTINE" <ident> <paramlist> "NEEDKEYWORD" <opt_bind> ;
         subroutine (prefixs-list (()), <ident>, <paramlist>, <opt_bind>)
      <header> ::= <prefixs> "SUBROUTINE" <ident> <paramlist> "NEEDKEYWORD" <opt_bind>;
         subroutine (<prefixs>, <ident>, <paramlist>, <opt_bind>)
      -- [llh] TODO: merge all these "FUNCTION" rules. Why merging implies bad shift/reduce conflicts ?
      <header> ::= "FUNCTION" <ident> <paramlist> "NEEDKEYWORD" <opt_result> ;
         function
            (none (), <ident>, <paramlist>, prefixs-list (()), <opt_result>)
      <header> ::=
         <prefixs> "FUNCTION" <ident> <paramlist> "NEEDKEYWORD" <opt_result> ;
         function (none (), <ident>, <paramlist>, <prefixs>, <opt_result>)
      <header> ::=
         <prefixs> <type> "FUNCTION" <ident> <opt_returnSize> <paramlist>
            "NEEDKEYWORD" <opt_result> ;
         case <type>
            when sizedType (IDENT, SIZE)
               =>
                  function
                     (<type>, <ident>, <paramlist>, <prefixs>, <opt_result>)
            when others
               => -- A NE FAIRE QUE SI <opt_returnSize> EST NON NULL !!!
                  function
                     (sizedType (<type>, <opt_returnSize>), <ident>,
                         <paramlist>, <prefixs>, <opt_result>)
         end case
      <header> ::=
         <type> <prefixs> "FUNCTION" <ident> <opt_returnSize> <paramlist>
            "NEEDKEYWORD" <opt_result> ;
         case <type>
            when sizedType (IDENT, SIZE)
               =>
                  function
                     (<type>, <ident>, <paramlist>, <prefixs>, <opt_result>)
            when others
               => -- A NE FAIRE QUE SI <opt_returnSize> EST NON NULL !!!
                  function
                     (sizedType (<type>, <opt_returnSize>), <ident>,
                         <paramlist>, <prefixs>, <opt_result>)
         end case
      <header> ::=
         <type> "FUNCTION" <ident> <opt_returnSize> <paramlist> "NEEDKEYWORD"
            <opt_result> ;
         case <type>
            when sizedType (IDENT, SIZE)
               =>
                  function
                     (<type>, <ident>, <paramlist>, prefixs-list (()),
                         <opt_result>)
            when others
               => -- A NE FAIRE QUE SI <opt_returnSize> EST NON NULL !!!
                  function
                     (sizedType (<type>, <opt_returnSize>), <ident>,
                         <paramlist>, prefixs-list (()), <opt_result>)
         end case
      <type_header> ::= "PROGRAM" <opt_ident> ;
         program (<opt_ident>)
      <type_header> ::= "BLOCKDATA" <opt_ident> ;
         blockdata (<opt_ident>)
      <type_header> ::= "MODULE" <ident> ;
         module (<ident>)
      <returnSize> ::= "*" <intCst> ;
         <intCst>
      <paramlist> ::= ;
         params-list (())
      <paramlist> ::= "(" ")" ;
         params-list (())
      <paramlist> ::= "(" <params> ")" ;
         <params>
      <param> ::= <ident> ;
         <ident>
      <param> ::= <star> ;
         <star>
      <prefix> ::= "RECURSIVE" ;
         recursive ()
      <prefix> ::= "PURE" ;
         pure ()
      <prefix> ::= "ELEMENTAL" ;
         elemental ()
      <opt_result> ::= "RESULT" "(" <ident> ")" ;
         <ident>
      <opt_result> ::= <bind> ;
         <bind>
      <opt_result> ::= ;
         none ()
      <opt_bind> ::= <bind> ;
         <bind>
      <opt_bind> ::=  ;
         exps ()
      -- UNIT LOCAL SUB-UNITS
      <locals> ::= "CONTAINS" "EOL" <localUnits1> ;
         <localUnits1>
      <locals> ::= <localsB> ;
         <localsB>
      <locals> ::= ;
         units-list (())
      <localsB> ::= "INTERNAL" "RECLASSIFY" <localUnit> ;
         units-list ((<localUnit>))
      <localsB> ::= <localsB> "INTERNAL" "RECLASSIFY" <localUnit> ;
         units-post (<localsB>, <localUnit>)
      <localUnit> ::=
         <header> <acceptCommentsAndEOL> <decls> <topStats> <acceptCommentsPost> <locals> <trailer> ;
         unit
            (options-list (()), <header>, <decls>, <topStats>, <locals>,
                <trailer>, none ())
      -- UNIT TRAILER
      <trailer> ::= "ACCEPTLABELS" <pureTrailer> <acceptCommentsAndEOL> ;
         <pureTrailer>
      <pureTrailer> ::= "END" ;
         end ()
      <pureTrailer> ::= "ENDPROGRAM" <opt_ident> ;
         endprogram (<opt_ident>)
      <pureTrailer> ::= "ENDBLOCKDATA" <opt_ident> ;
         endblockdata (<opt_ident>)
      <pureTrailer> ::= "ENDSUBROUTINE" <opt_ident> ;
         endsubroutine (<opt_ident>)
      <pureTrailer> ::= "ENDFUNCTION" <opt_ident> ;
         endfunction (<opt_ident>)
      <pureTrailer> ::= "ENDMODULE" <opt_ident> ;
         endmodule (<opt_ident>)
      <pureTrailer> ::= "ENDINTERNAL" ;
         end ()
      -- UNIT OPTIONAL OFFSET
      <offset> ::= <oneOffset> <oneOffset> ;
         offset (<oneOffset>.1, <oneOffset>.2)
      -- DECLARATIONS
      <decl> ::= <parameterDecl> ;
         <parameterDecl>
      <decl> ::= <implicitDecl> ;
         <implicitDecl>
      <decl> ::= <dimensionDecl> ;
         <dimensionDecl>
      <decl> ::= <externalDecl> ;
         <externalDecl>
      <decl> ::= <intrinsicDecl> ;
         <intrinsicDecl>
      <decl> ::= <saveDecl> ;
         <saveDecl>
      <decl> ::= <equivalenceDecl> ;
         <equivalenceDecl>
      <decl> ::= <commonDecl> ;
         <commonDecl>
      <decl> ::= <intentDecl> ;
         <intentDecl>
      <decl> ::= <derivedDecl> ;
         <derivedDecl>
      <decl> ::= <interfaceDecl> ;
         <interfaceDecl>
      <decl> ::= <useDecl> ;
         <useDecl>
      <decl> ::= <importDecl> ;
         <importDecl>
      <decl> ::= <accessDecl> ;
         <accessDecl>
      <decl> ::= <allocatableDecl> ;
         <allocatableDecl>
      <decl> ::= <optionalDecl> ;
         <optionalDecl>
      <decl> ::= <pointerDecl> ;
         <pointerDecl>
      <decl> ::= <pointerPairDecl> ;
         <pointerPairDecl>
      <decl> ::= <targetDecl> ;
         <targetDecl>
      <decl> ::= <automaticDecl> ;
         <automaticDecl>
      <decl> ::= <staticDecl> ;
         <staticDecl>
      <decl> ::= <volatileDecl> ;
         <volatileDecl>
      <decl> ::= <contiguousDecl> ;
         <contiguousDecl>
      <decl> ::= <structureDecl> ;
         <structureDecl>
      <decl> ::= <recordDecl> ;
         <recordDecl>
      <decl> ::= <dictionaryDecl> ;
         <dictionaryDecl>
      <decl> ::= <extBlockDecl> ;
         <extBlockDecl>
      <decl> ::= <extBaseDecl> ;
         <extBaseDecl>
      <decl> ::= <extDummyDecl> ;
         <extDummyDecl>
            -- <inst> are the declarations that can be found in the statements.
         
      <decl> ::= <inst> ;
         <inst>
      <inst> ::= <formatDecl> ;
         <formatDecl>
      <inst> ::= <entryDecl> ;
         <entryDecl>
      <inst> ::= <directiveDecl> ;
         <directiveDecl>
      <inst> ::= <flcdirectiveDecl> ;
         <flcdirectiveDecl>
      <inst> ::= <namelistDecl> ;
         <namelistDecl>
      <inst> ::= <dataDecl> ;
         <dataDecl>
      <inst> ::= <includeDecl> ;
         <includeDecl>
      <inst> ::= <varDecl> ;
         <varDecl>
      <inst> ::= <hpfDecl> ;
         <hpfDecl>-- INTERNALS OF DECLARATIONS
                  -- parameter declaration:
         
      <parameterDecl> ::= "PARAMETER" "(" <parameters> ")" ;
         <parameters>
      <parameter> ::= <ident> "=" <exp> ;
         parameterDecl (<ident>, <exp>)
      -- implicit types declaration:
      <implicitDecl> ::= "IMPLICIT" "NEEDIMPLICIT" <in_implicit> ;
         <in_implicit>
      <in_implicit> ::= "NONE" ;
         implicits-list (())
      <in_implicit> ::=
         "UNDEFINED" "(" "NEEDKEYWORD" "A" "-" "NEEDKEYWORD" "Z" ")" ;
         implicits-list (())
      <in_implicit> ::= <implicits> ;
         <implicits>
            -- [llh] TODO: try to merge all these <implicit> cases. Check the "hack"!
         
      <implicit> ::= <type_non77like> "(" "NEEDLETTER" <letterss> "NOLETTER" ")" ;
         typedLetters (<type_non77like>, <letterss>)
      -- to resolve very ambigous case like
      -- implicit real(d)(z) we need this hack
      -- the Scanner does a special look forward with the NEEDIMPLICIT action
      -- send in the upper rules
      -- and return the SPECIAL pseudo INIMPLICIT keyword to disambiguate the rule
      <implicit> ::=
         <non_char_type> "INIMPLICIT" "(" <generalExp> ")" "(" "NEEDLETTER"
            <letterss> "NOLETTER" ")" ;
         typedLetters (sizedType (<non_char_type>, <generalExp>), <letterss>)
      <implicit> ::=
         "CHARACTER" "INIMPLICIT" "(" "NEEDCOLON" <generalExps> "NEEDNOCOLON"
            ")" "(" "NEEDLETTER" <letterss> "NOLETTER" ")" ;
         typedLetters (sizedType (character (), <generalExps>), <letterss>)
      <implicit> ::= "STATIC" "(" "NEEDLETTER" <letterss> "NOLETTER" ")" ;
         -- SUN
         typedLetters (isStatic (), <letterss>)
      <implicit> ::= "AUTOMATIC""(" "NEEDLETTER" <letterss> "NOLETTER" ")" ;
         -- SUN
         typedLetters (isAutomatic (), <letterss>)
      <letters> ::= <letter> ;
         <letter>
      <letters> ::= <letter> "-" <letter> ;
         letterRange (<letter>.1, <letter>.2)
      -- dimension declaration:
      <dimensionDecl> ::= "DIMENSION" <opt_dblcolon> <fullDecls> ;
         dimensionDecl (<fullDecls>)
      -- external routine declaration:
      <externalDecl> ::= "EXTERNAL" <opt_dblcolon> <idents> ;
         externalDecl (<idents>)
      -- intrinsic routine declaration:
      <intrinsicDecl> ::= "INTRINSIC" <opt_dblcolon> <idents> ;
         intrinsicDecl (<idents>)
      -- remanent variables declaration:
      <saveDecl> ::= "SAVE" ;
         saveDecl (idents-list (()))
      <saveDecl> ::= "SAVE" <opt_dblcolon> <blockNames> ;
         saveDecl (<blockNames>)
      <blockName> ::= <ident> ;
         <ident>
      <blockName> ::= "/" "COMMONFLAG" <ident> "/" ;
         let ident-atom(IDENT) = <ident> in
            blockName-atom(IDENT)
      -- equivalence declaration:
      <equivalenceDecl> ::= "EQUIVALENCE" <equivalences> ;
         <equivalences>
      <equivalence> ::= "(" <variableRefs> ")" ;
         <variableRefs>-- common declaration:
                       -- [llh] TODO: voir si on peut inliner ce <no_opt_comma> en "no_opt_comma":
         
      <commonDecl> ::= "COMMON" "opt_comma" <commons1> <no_opt_comma> ;
         <commons1>
      <common> ::= "//" <basicDecls> ;
         common (none (), <basicDecls>)
      <common> ::= "/" "COMMONFLAG" <ident> "/" <basicDecls> ;
         common (<ident>, <basicDecls>)
      <common> ::= <basicDecls> ;
         common (none (), <basicDecls>)
      <no_opt_comma> ::= "no_opt_comma" ;
         none ()
      -- intent declaration:
      <intentDecl> ::=
         "INTENT" "(" "NEEDKEYWORD" <intentValue> ")" <opt_dblcolon> <idents> ;
         intentDecl (<intentValue>, <idents>)
      <intentValue> ::= "IN" ;
         in ()
      <intentValue> ::= "OUT" ;
         out ()
      <intentValue> ::= "INOUT" ;
         inout ()
      -- structured type("derived type") declaration:
      <derivedDecl> ::=
         "TYPE" <derived_access> <ident> <acceptCommentsM1> "EOL"
            <privateSequences> <components> "ENDTYPE" <opt_ident> ;
         derivedDecl
            (<ident>, <derived_access>, <privateSequences>, <components>,
                decls-list (()), <opt_ident>)
      <derivedDecl> ::=
         "TYPE" <derived_access> <ident> <acceptCommentsM1> "EOL"
            <privateSequences> <components> "CONTAINS" "EOL" <boundProcs1> "ENDTYPE" <opt_ident> ;
         derivedDecl
            (<ident>, <derived_access>, <privateSequences>, <components>,
                <boundProcs1>, <opt_ident>)

      <derived_access> ::= "," "NEEDKEYWORD" <access> <dblcolon> ;
         <access>
      <derived_access> ::= <opt_dblcolon> ;
         none ()
      <privateSequence> ::= "SEQUENCE" "EOL" ;
         isSequence ()
      <privateSequence> ::= "PRIVATE" "EOL" ;
         isPrivate ()
      <component> ::= <varDecl> "EOL" ;
         <varDecl>
      <boundProcs1> ::= <component> ;
         decls-list ((<component>))
      <boundProcs1> ::= <boundProcs1> <component> ;
         decls-post (<boundProcs1>, <component>)
-- interface declaration:
      <interfaceDecl> ::=
         <interface_header> <acceptCommentsAndEOL> <interface_body>
            <interface_end> ;
         interfaceDecl (<interface_header>, <interface_body>, <interface_end>)
      <interface_header> ::= "INTERFACE" ;
         none ()
      <interface_header> ::= "ABSTRACTINTERFACE" ;
         isAbstract ()
      <interface_header> ::= "INTERFACE" <ident> ;
         <ident>
      <interface_header> ::= "INTERFACEOP" "(" <op_spec> ")" ;
         <op_spec>
      <interface_header> ::= "INTERFACEASS" "(" "=" ")" ;
         assign (none (), none ())
      <interface_body> ::= ;
         interfaces-list (())
      <interface_body> ::= <header> <acceptCommentsAndEOL> <decls> ;
         -- for cmf, one interface, no end
         interfaces-list ((interface (<header>, <decls>, end ())))
      <interface_body> ::= <interfaces1> ;
         <interfaces1>
      <interface> ::= "MODULEPROCEDURE" <idents> ;
         module_procedure (<idents>)
      <interface> ::= <directiveDecl> ;
         <directiveDecl>
      <interface> ::= <header> <acceptCommentsAndEOL> <decls> <pureTrailer> ;
         interface (<header>, <decls>, <pureTrailer>)
      <interface_end> ::= "ENDINTERFACE" ;
         none ()
      <interface_end> ::= "ENDINTERFACE" <ident> ;
         <ident>
      <interface_end> ::= "ENDOPERATOR" "(" <op_spec> ")" ;
         <op_spec>
      <interface_end> ::= "ENDASSIGNMENT" "(" "=" ")" ;
         assign (none (), none ())
      -- use declaration:
      <useDecl> ::= "USE" <opt_intrinsic> <ident> ;
         useDecl (<ident>, renamedVisibles-list (()))
      <useDecl> ::= "USE" <opt_intrinsic> <ident> "," "NEEDOPKEYWORD" <useList> ;
         useDecl (<ident>, <useList>)
      <opt_intrinsic> ::= ;
        none()
      <opt_intrinsic> ::= "," "NEEDKEYWORD" "INTRINSIC" ":" ":" ;
        none()
      <useList> ::= "ONLY" ":" "NEEDOPKEYWORD" <onlyVisibles> ;
         <onlyVisibles>
      <useList> ::= <renamedVisibles> ;
         <renamedVisibles>
      <visible> ::= <genericName> ;
         <genericName>
      <visible> ::= <renamed> ;
         <renamed>
      <renamed> ::= <genericName> "PTRASS" "NEEDOPKEYWORD" <genericName> ;
         renamed (<genericName>.1, <genericName>.2)
      -- import declaration:
      <importDecl> ::= "IMPORT" <opt_dblcolon> <idents> ;
         importDecl (<idents>)
      -- access declaration:
      -- the NEEDOPKEYWORD action is needed to process case like
      -- public :: type, operator (*)
      -- we need a kwd to find the "operator" token, but we must not return a kwd
      -- for "type" which must be considered as a name.
      <accessDecl> ::= <access> <opt_dblcolon> "NEEDOPKEYWORD" <genericNames> ;
         accessDecl (<access>, <genericNames>)
      <genericNames> ::= ;
         idents-list (())
      <genericNames> ::= <genericNames1> ;
         <genericNames1>
      <genericName> ::= <blockName> ;
         <blockName>
      <genericName> ::= "ASSIGNMENT" "(" "=" ")" ;
         assign (none (), none ())
      <genericName> ::= "OPERATOR" "(" <op_spec> ")" ;
         <op_spec>
      <op_spec> ::= "OP_USER" <definedOp> ;
         <definedOp>
      <op_spec> ::= "+" ;
         add (none (), none ())
      <op_spec> ::= "-" ;
         sub (none (), none ())
      <op_spec> ::= "*" ;
         mul (none (), none ())
      <op_spec> ::= "/" ;
         div (none (), none ())
      <op_spec> ::= "**" ;
         power (none (), none ())
      <op_spec> ::= "EQ" ;
         eq (none (), none ())
      <op_spec> ::= "NE" ;
         ne (none (), none ())
      <op_spec> ::= "GE" ;
         ge (none (), none ())
      <op_spec> ::= "LE" ;
         le (none (), none ())
      <op_spec> ::= "GT" ;
         gt (none (), none ())
      <op_spec> ::= "LT" ;
         lt (none (), none ())
      <op_spec> ::= "EQV" ;
         eqv (none (), none ())
      <op_spec> ::= "NEQV" ;
         neqv (none (), none ())
      <op_spec> ::= "AND" ;
         and (none (), none ())
      <op_spec> ::= "OR" ;
         or (none (), none ())
      <op_spec> ::= "XOR" ;
         xor (none (), none ())
      <op_spec> ::= "NOT" ;
         not (none ())
      <op_spec> ::= "//" ;
         concat (none (), none ())
      -- allocatable declaration:
      <allocatableDecl> ::= "ALLOCATABLE" <opt_dblcolon> <basicDecls> ;
         allocatableDecl (<basicDecls>)
      -- optional declaration:
      <optionalDecl> ::= "OPTIONAL" <opt_dblcolon> <idents> ;
         optionalDecl (<idents>)
      -- pointer declarations (F90 and Cray styles):
      <pointerDecl> ::= "POINTER" <opt_dblcolon> <basicDecls> ;
         pointerDecl (<basicDecls>)
      <pointerPairDecl> ::= "POINTER" <pointerPairs> ;
         <pointerPairs>
      <pointerPair> ::= "(" <ident> "," <basicDecl> ")" ;
         pointerPair (<ident>, <basicDecl>)
      -- target declaration:
      <targetDecl> ::= "TARGET" <opt_dblcolon> <basicDecls> ;
         targetDecl (<basicDecls>)
      -- automatic declaration:
      <automaticDecl> ::= "AUTOMATIC" <idents> ;
         automaticDecl (<idents>)
      -- static declaration:
      <staticDecl> ::= "STATIC" <idents> ;
         staticDecl (<idents>)
      -- volatile declaration:
      <volatileDecl> ::= "VOLATILE" <opt_dblcolon> <blockNames> ;
         volatileDecl (<blockNames>)
      -- contiguous declaration:
      <contiguousDecl> ::= "CONTIGUOUS" <opt_dblcolon> <idents> ;
         contiguousDecl (<idents>)
      -- structure declaration:
      <structureDecl> ::=
         "STRUCTURE" "/" "COMMONFLAG" <ident> "/" <acceptCommentsM0> "EOL"
            <fieldDecls> "ENDSTRUCTURE" ;
         structureDecl (<ident>, <fieldDecls>)
      <fieldDecls> ::= ;
         fieldDecls-list (())
      <fieldDecls> ::= <fieldDecls1> ;
         <fieldDecls1>
      <fieldDecl> ::= <recordDecl> ;
         <recordDecl>
      <fieldDecl> ::= <varDecl> ;
         <varDecl>
      <fieldDecl> ::= <parameterDecl> ;
         <parameterDecl>
      <fieldDecl> ::=
         "STRUCTURE" "/" "COMMONFLAG" <ident> "/" <basicDecls>
            <acceptCommentsM1> "EOL" <fieldDecls1> <acceptCommentsPost>
            "ENDSTRUCTURE" ;
         substructure (<ident>, <basicDecls>, <fieldDecls1>)
      <fieldDecl> ::=
         "STRUCTURE" <basicDecls> <acceptCommentsM0> "EOL" <fieldDecls1>
            <acceptCommentsPost> "ENDSTRUCTURE" ;
         substructure (none (), <basicDecls>, <fieldDecls1>)
      <fieldDecl> ::= "UNION" "EOL" <mapDecls1> "ENDUNION" ;
         union_decl (<mapDecls1>)
      <mapDecl> ::= "MAP" "EOL" <fieldDecls1> "ENDMAP" ;
         mapDecl (<fieldDecls1>)
      -- record declaration:
      <recordDecl> ::= "RECORD" "opt_comma" <records1> <no_opt_comma> ;
         <records1>
      <record> ::= "/" "COMMONFLAG" <ident> "/" <basicDecls> ;
         record (<ident>, <basicDecls>)
      -- dictionary declaration:
      <dictionaryDecl> ::= "DICTIONARY" <gstring> "EOL" ;
         dictionaryDecl (<gstring>)
      -- extended block declaration:
      <extBlockDecl> ::= "EXTENDEDBLOCK" "opt_comma" <commons1> <no_opt_comma> ;
         -- GOULD
         let commons-list (X) = <commons1> in
            extBlocks-list (X)
      -- extended base declaration:
      <extBaseDecl> ::= "EXTENDEDBASE" "opt_comma" <extBases1> <no_opt_comma> ;
         -- GOULD
         <extBases1>
      <extBase> ::= "/" "COMMONFLAG" <ident> "/" <ext_size> ;
         -- GOULD
         extBase (<ident>, <ext_size>)
      <ext_size> ::= <intCst> ;
         <intCst>
      <ext_size> ::= <star> ;
         <star>-- extended dummy declaration:
         
      <extDummyDecl> ::= "EXTENDEDDUMMY" <idents> ;
         -- GOULD
         extDummyDecl (<idents>)
      -- format declaration:
      <formatDecl> ::= "ACCEPTLABELS" "FORMAT" <formatElems> ;
         formatDecl (<formatElems>)
      -- this generates a shift/reduce conflict which must be ignored:
      <formatElem> ::= %FORMATELEM ;
         formatElem-atom(%FORMATELEM)
      <formatElem> ::= <gstring> ;
         <gstring>-- entry declaration:
         
      <entryDecl> ::= "ENTRY" <ident> <paramlist> ;
         entryDecl (<ident>, <paramlist>)
      -- directive declaration:
      <directiveDecl> ::= <directive_name> <directive_arguments> ;
         directiveDecl (<directive_name>, <directive_arguments>)
      <directive_name> ::= %DIRNAME ;
         ident-atom(%DIRNAME)
      <flcdirectiveDecl> ::= <flcdirname> <directive_arguments> ;
         flcdirectiveDecl (<flcdirname>, <directive_arguments>)
      <flcdirname> ::= %FLCDIRECTIVE ;
         ident-atom(%FLCDIRECTIVE)
      <directive_arguments> ::= ;
         dirArgs-list (())
      <directive_arguments> ::= <dirArgs> ;
         <dirArgs>

      -- namelist declaration:
      <namelistDecl> ::= "NAMELIST" "opt_comma" <namelists1> <no_opt_comma> ;
         <namelists1>
      -- The optional "," induces a bad conflict with names
      --         <namelists> ::= <namelists> "," <namelist> ;
      --            namelist-post(<namelists>, <namelist>)
      -- [llh] TODO: try to solve that ?
         
      <namelist> ::= "/" <ident> "/" <idents> ;
         namelist (<ident>, <idents>)
      -- data declaration:
      <dataDecl> ::= "DATA" <datas> ;
         <datas>
      <datas> ::= <data> ;
         datas-list ((<data>))
      <datas> ::= <datas> "," <data> ;
         datas-post (<datas>, <data>)
      -- [llh] TODO: try to use <opt_comma>:
      <datas> ::= <datas> <data> ;
         datas-post (<datas>, <data>)
      <data> ::= <datavars> "/" <values> "/" ;
         data (<datavars>, <values>)
      <datavar> ::= <variableRef> ;
         <variableRef>
      <datavar> ::= "(" <datavars> "," <doRange> ")" ;
         impldo (<datavars>, <doRange>)
      <value> ::= <simple_exp> "*" <simple_value> ;
         repeat_factor (<simple_exp>, <simple_value>)
      <value> ::= <simple_value> ;
         <simple_value>
      <simple_value> ::= <variableRef> ;
         <variableRef>
      <simple_value> ::= <simpleCst> ;
         <simpleCst>
      <simple_value> ::= <complexConstructor> ;
         <complexConstructor>
      <simple_value> ::= "+" <simple_exp> ;
         plus (<simple_exp>)
      <simple_value> ::= "-" <simple_exp> ;
         minus (<simple_exp>)
      -- include declaration:
      <includeDecl> ::= "INCLUDE" <gstring> ;
         includeDecl (<gstring>)
      <includeDecl> ::= "SHARP_INC" <gstring> ;
         includeDecl (<gstring>)
      -- variable declaration:
      <varDecl> ::= <type> <fullDecls> ;
         varDecl (<type>, <fullDecls>)
      -- Known pb with the optional comma(See the typesize production)
      --(patched with "size_comma" in the scanner)
      -- <varDecl> ::= "CHARACTER" <typesize> "," <fullDecls> ;
      --     varDecl(sizedType(character(), <typesize>), <fullDecls>)

      -- variable declaration for CM-Fortran:
      <varDecl> ::= <type> <dblcolon> <fullDecls> ;
         varDecl (<type>, <fullDecls>)
      -- Here the comma is not optional
      <varDecl> ::= <type> "," <typeSpecModifiers> <dblcolon> <fullDecls> ;
         varDecl (modifiedType (<type>, <typeSpecModifiers>), <fullDecls>)
      <fullDecl> ::= <basicDecl> ;
         <basicDecl>
      <fullDecl> ::= <basicDecl> <initializer> ;
         let fullDecl (VAR, INIT) = <basicDecl> in
            fullDecl (VAR, <initializer>)
      <basicDecl> ::= <ident_in_decl> ;
         fullDecl (<ident_in_decl>, none ())
      <basicDecl> ::= <ident_in_decl> <typesize> ;
         fullDecl (sizedDecl (<ident_in_decl>, <typesize>), none ())
      <basicDecl> ::= <ident_in_decl> "(" <dims> ")" "NEEDNOCOLON" ;
         fullDecl (arrayDecl (<ident_in_decl>, <dims>), none ())
      <basicDecl> ::= <ident_in_decl> <typesize> "(" <dims> ")" "NEEDNOCOLON" ;
         fullDecl
            (sizedDecl (arrayDecl (<ident_in_decl>, <dims>), <typesize>),
                none ())
      <basicDecl> ::= <ident_in_decl> "(" <dims> ")" <typesize> "NEEDNOCOLON" ;
         fullDecl
            (sizedDecl (arrayDecl (<ident_in_decl>, <dims>), <typesize>),
                none ())
      -- [llh] TODO: inline this <ident_in_decl> ?
      <ident_in_decl> ::= <ident> "IN_DECL" ;
         <ident>
      <dim> ::= ":" ;
         -- CM Fortran
         dim (none (), none ())
      <dim> ::= <uexpOrStar> ;
         dim (none (), <uexpOrStar>)
      <dim> ::= <uexp> ":" ;
         -- CM Fortran
         dim (<uexp>, none ())
      <dim> ::= <uexp> ":" <uexpOrStar> ;
         dim (<uexp>, <uexpOrStar>)
      <uexpOrStar> ::= <star> ;
         <star>
      <uexpOrStar> ::= <uexp> ;
         <uexp>
      <initializer> ::= "/" <values> "/" ;
         -- VAX,SUN
         <values>
      <initializer> ::= "=" <exp> ;
         -- CM Fortran
         cm_initializer (<exp>)
      <initializer> ::= "PTRASS" <exp> ;
         -- F95
         pointer_initializer (<exp>)
      <typeSpecModifier> ::= "ARRAY" "(" <dims> ")" ;
         isDimension (<dims>)
      <typeSpecModifier> ::= "PARAMETER" ;
         isParameter ()
      <typeSpecModifier> ::= "SAVE" ;
         isSave ()
      <typeSpecModifier> ::= "DATA" ;
         isData ()
      <typeSpecModifier> ::= "INTENT" "(" "NEEDKEYWORD" <intentValue> ")" ;
         isIntent (<intentValue>)
      <typeSpecModifier> ::= "ALLOCATABLE" ;
         isAllocatable ()
      <typeSpecModifier> ::= "DIMENSION" "(" <dims> ")" ;
         isDimension (<dims>)
      <typeSpecModifier> ::= "EXTERNAL" ;
         isExternal ()
      <typeSpecModifier> ::= "INTRINSIC" ;
         isIntrinsic ()
      <typeSpecModifier> ::= "OPTIONAL" ;
         isOptional ()
      <typeSpecModifier> ::= "POINTER" ;
         isPointer ()
      <typeSpecModifier> ::= "TARGET" ;
         isTarget ()
      <typeSpecModifier> ::= "VALUE" ;
         isValue()
      <typeSpecModifier> ::= <access> ;
         <access>
      <typeSpecModifier> ::= "VOLATILE" ;
         isVolatile()
      <typeSpecModifier> ::= "CONTIGUOUS" ;
         isContiguous()
      <typeSpecModifier> ::= "PASS" ;
         isPass()
      <typeSpecModifier> ::= "NOPASS" ;
         isNopass()
      <typeSpecModifier> ::= "DEFERRED" ;
         isDeferred()
      <access> ::= "ABSTRACT" ;
         isAbstract ()
      <access> ::= "PRIVATE" ;
         isPrivate ()
      <access> ::= "PUBLIC" ;
         isPublic ()
      -- F2003
      <access> ::= <bind> ;
         <bind>
      <access> ::= <extends> ;
         <extends>
      <bind> ::= "BIND" "(" <ident> "," <keywordArg> ")" ;
         bind (<ident>, <keywordArg>)
      <bind> ::= "BIND" "(" <ident> ")" ;
         bind (<ident>, none())
      <extends> ::= "EXTENDS" "(" <ident> "," <keywordArg> ")" ;
         extends (<ident>, <keywordArg>)
      <extends> ::= "EXTENDS" "(" <ident> ")" ;
         extends (<ident>, none())
      -- HPF directives:
      <hpfDecl> ::= "HPF_SPEC" "RECLASSIFY" <hpfDirective> ;
         <hpfDirective>
      <hpfDirective> ::= "PROCESSORS" <basicDecls> ;
         hpfProcessors (<basicDecls>)
      <hpfDirective> ::=
         "ALIGN" <ident> "(" <dims> <closeParenthRecla> "WITH" <withClause> ;
         hpfAlign (<ident>, <dims>, <withClause>)
      <hpfDirective> ::=
         "DISTRIBUTEONTO" <ident> <formatClause> "RECLASSIFY" "ONTO"
            <ontoClause> ;
         hpfDistribute (<ident>, <formatClause>, <ontoClause>)
      <hpfDirective> ::= "DISTRIBUTE" <ident> <formatClause> ;
         hpfDistribute (<ident>, <formatClause>, none ())
      <hpfDirective> ::= "DYNAMIC" <idents> ;
         hpfDynamic (<idents>)
      <hpfDirective> ::= "INHERIT" <idents> ;
         hpfInherit (<idents>)
      <hpfDirective> ::= "TEMPLATE" <basicDecls> ;
         hpfTemplate (<basicDecls>)
      <hpfDirective> ::= <hpfAttributes> <dblcolon> <fullDecls> ;
         hpfCombined (<hpfAttributes>, <fullDecls>)
      <hpfDirective> ::= "SEQUENCE" ;
         hpfSequence (none ())
      <hpfDirective> ::= "SEQUENCE" <opt_dblcolon> <blockNames> ;
         hpfSequence (<blockNames>)
      <hpfDirective> ::= "NOSEQUENCE" ;
         hpfNoSequence (none ())
      <hpfDirective> ::= "NOSEQUENCE" <opt_dblcolon> <blockNames> ;
         hpfNoSequence (<blockNames>)
      <withClause> ::= <variableRef> ;
         refClause (<variableRef>)
      <withClause> ::= "*" <variableRef> ;
         refStarClause (<variableRef>)
      <formatClause> ::= "(" <distFormats> ")" ;
         formatClause (<distFormats>)
      <formatClause> ::= "*" ;
         formatStarClause (none ())
      <formatClause> ::= "*" "(" <distFormats> ")" ;
         formatStarClause (<distFormats>)
      <ontoClause> ::= <ident> ;
         refClause (<ident>)
      <ontoClause> ::= "*" <opt_ident> ;
         refStarClause (<opt_ident>)
      <distFormat> ::= "BLOCK" ;
         blockFormat (none ())
      <distFormat> ::= "BLOCK" "(" <exp> ")" ;
         blockFormat (<exp>)
      <distFormat> ::= "CYCLIC" ;
         cyclicFormat (none ())
      <distFormat> ::= "CYCLIC" "(" <exp> ")" ;
         cyclicFormat (<exp>)
      <distFormat> ::= "DISTSTAR" ;
         starFormat ()
      <hpfAttribute> ::= "PROCESSORS" ;
         hpfProcessorsAttribute ()
      <hpfAttribute> ::= "ALIGNWITH" <withClause> ;
         hpfAlignAttribute (none (), <withClause>)
      <hpfAttribute> ::= "ALIGN" "(" <closeParenthRecla> "WITH" <withClause> ;
         hpfAlignAttribute (none (), <withClause>)
      <hpfAttribute> ::=
         "ALIGN" "(" <dims> <closeParenthRecla> "WITH" <withClause> ;
         hpfAlignAttribute (<dims>, <withClause>)
      <hpfAttribute> ::= "COMBDISTRIBUTEONTO" <ontoClause> ;
         hpfDistributeAttribute (none (), <ontoClause>)
      <hpfAttribute> ::=
         "DISTRIBUTEONTO" <formatClause> "RECLASSIFY" "ONTO" <ontoClause> ;
         hpfDistributeAttribute (<formatClause>, <ontoClause>)
      <hpfAttribute> ::= "DISTRIBUTE" <formatClause> ;
         hpfDistributeAttribute (<formatClause>, none ())
      <hpfAttribute> ::= "DYNAMIC" ;
         hpfDynamicAttribute ()
      <hpfAttribute> ::= "INHERIT" ;
         hpfInheritAttribute ()
      <hpfAttribute> ::= "TEMPLATE" ;
         hpfTemplateAttribute ()
      <hpfAttribute> ::= "DIMENSION" "(" <dims> ")" ;
         hpfDimensionAttribute (<dims>)
      -- STATEMENTS
      <topStats> ::= ;
         stats-list (())
      <topStats> ::= <realStat> <acceptCommentsAndEOL> <stats> ;
         stats-pre (<realStat>, <stats>)
      <stat> ::= <inst> ;
         <inst>
      <stat> ::= <realStat> ;
         <realStat>
      <realStat> ::= "ACCEPTLABELS" <pureStat> ;
         <pureStat>
      <pureStat> ::=
         <opt_ident> "LOGIF" "(" <exp> <closeParenthRecla> <simpleStat> ;
         log_if (<exp>, <simpleStat>)
      <pureStat> ::= <simpleStat> ;
         <simpleStat>
      <pureStat> ::= <structStat> ;
         <structStat>
-- INTERNALS OF SIMPLE STATEMENTS
      <simpleStat> ::= "ASS" <variableRef> "=" <assign_right> ;
         assign (<variableRef>, <assign_right>)
      <simpleStat> ::= "ASSIGN" <label> "NEEDKEYWORD" "TO" <ident> ;
         assignto (<label>, <ident>)
      <simpleStat> ::= "CONTINUE" ;
         continue ()
      <simpleStat> ::= "CALL" <variableRef> ;
         case <variableRef>
            when vardim(VARREF, EXPS)
               => call(VARREF, EXPS)
            when others
               => call(<variableRef>, exps-list (()))
         end case

      <simpleStat> ::= <goto> ;
         <goto>
      <simpleStat> ::= <io_stat> ;
         <io_stat>
      <simpleStat> ::=
         "ARITHIF" "(" <exp> ")" <opt_label> "," <opt_label> "," <opt_label> ;
         arith_if (<exp>, <opt_label>.1, <opt_label>.2, <opt_label>.3)
      <simpleStat> ::= "RETURN" <opt_exp> ;
         return (<opt_exp>)
      <simpleStat> ::= "STOP" <opt_exp> ;
         stop (<opt_exp>)
      <simpleStat> ::= "PAUSE" <opt_exp> ;
         pause (<opt_exp>)
      <simpleStat> ::= "EXIT" <opt_ident> ;
         -- CM Fortran
         exit (<opt_ident>)
      <simpleStat> ::= "ENDDO" <opt_ident> ;
         -- CM Fortran
         enddo (<opt_ident>)
      <simpleStat> ::= "CYCLE" <opt_ident> ;
         -- CM Fortran
         cycle (<opt_ident>)
      <simpleStat> ::= "ALLOCATE" "(" <variableRefs> ")" ;
         -- CM Fortran
         allocate (<variableRefs>, none ())
      <simpleStat> ::= "ALLOCATE" "(" <variableRefs> "," <keywordArg> ")" ;
         -- CM Fortran
         allocate (<variableRefs>, <keywordArg>)
      <simpleStat> ::= "DEALLOCATE" "(" <variableRefs> ")" ;
         -- CM Fortran
         deallocate (<variableRefs>, none ())
      <simpleStat> ::= "DEALLOCATE" "(" <variableRefs> "," <keywordArg> ")" ;
         -- CM Fortran
         deallocate (<variableRefs>, <keywordArg>)
      <simpleStat> ::= "NULLIFY" "(" <variableRefs> ")" ;
         -- CM Fortran
         nullify (<variableRefs>)
      <simpleStat> ::= "ASS" <variableRef> "PTRASS" <exp> ;
         -- CM Fortran, F95
         pointer_assign (<variableRef>, <exp>)
      <simpleStat> ::= "WHERE" "(" <exp> <closeParenthRecla> <simpleStat> ;
         where (<exp>, <simpleStat>)
      <simpleStat> ::=
         "FORALL" "(" <forallRanges> <opt_mask> <closeParenthRecla> <simpleStat> ;
         forall (<forallRanges>, <opt_mask>, <simpleStat>)
      <simpleStat> ::= "INLINE" "EOL" "ENDI" ;
         -- GOULD
         inline ()
      <simpleStat> ::= "LEAVE" <opt_label> ;
         -- GOULD
         leave (<opt_label>)
      <simpleStat> ::= "HPF_STAT" "RECLASSIFY" <hpfStat> ;
         -- HPF
         <hpfStat>
      <assign_right> ::= <variableRef> "=" <assign_right> ;
         -- GOULD
         assign (<variableRef>, <assign_right>)
      <assign_right> ::= <exp> ;
         <exp>
      <goto> ::= "GOTO" <label> ;
         goto (<label>)
      <goto> ::= "ASSGOTO" <ident> ;
         assigned_goto (<ident>, labels-list (()))
      <goto> ::= "ASSGOTO" <ident> <opt_comma> "(" <labels> ")" ;
         assigned_goto (<ident>, <labels>)
      <goto> ::= "COMPGOTO" "(" <labels> ")" <opt_comma> <exp> ;
         comp_goto (<labels>, <exp>)
      <forallRange> ::= <ident> "=" <rangeColon> ;
         nameRange (<ident>, <rangeColon>)
      <opt_mask> ::= ;
         none ()
      <opt_mask> ::= "," <exp> ;
         <exp>-- HPF statements:
         
      <hpfStat> ::= "INDEPENDENT" ;
         hpfIndependent (none ())
      <hpfStat> ::= "INDEPENDENT" "," "RECLASSIFY" "NEW" "(" <variableRefs> ")" ;
         hpfIndependent (<variableRefs>)
      <hpfStat> ::=
         "REALIGN" <ident> "(" <dims> <closeParenthRecla> "WITH" <variableRef> ;
         hpfRealign (<ident>, <dims>, <variableRef>)
      <hpfStat> ::= "REALIGNWITH" <variableRef> <dblcolon> <idents> ;
         hpfRealign (<idents>, none (), <variableRef>)
      <hpfStat> ::=
         "REALIGN" "(" <dims> <closeParenthRecla> "WITH" <variableRef>
            <dblcolon> <idents> ;
         hpfRealign (<idents>, <dims>, <variableRef>)
      <hpfStat> ::=
         "REDISTRIBUTEONTO" <ident> "(" <distFormats> <closeParenthRecla> "ONTO"
            <ident> ;
         hpfRedistribute (<ident>.1, <distFormats>, <ident>.2)
      <hpfStat> ::= "REDISTRIBUTE" <ident> "(" <distFormats> ")" ;
         hpfRedistribute (<ident>, <distFormats>, none ())
      <hpfStat> ::= "REDISTRIBUTE" "(" <distFormats> ")" <dblcolon> <idents> ;
         hpfRedistribute (<idents>, <distFormats>, none ())
      <hpfStat> ::= "EMPTYREDISTRIBUTEONTO" <ident> <dblcolon> <idents> ;
         hpfRedistribute (<idents>, none (), <ident>)
      <hpfStat> ::=
         "REDISTRIBUTEONTO" "(" <distFormats> <closeParenthRecla> "ONTO" <ident>
            <dblcolon> <idents> ;
         hpfRedistribute (<idents>, <distFormats>, <ident>)
      -- INTERNALS OF STRUCTURED STATEMENTS
      -- if-then-else-elseif-endif statement:
      <structStat> ::=
         <opt_ident> "LOGIF" "(" <exp> <closeParenthRecla> "THEN"
            <acceptCommentsM2> "EOL" <stats> <acceptCommentsPost> <else_end> ;
         struct_if (<opt_ident>, <exp>, <stats>, <else_end>)
      <else_end> ::= "ENDIF" <opt_ident> ;
         stats-list (())
      <else_end> ::=
         "ELSE" <opt_ident> "EOL" <stats> <acceptCommentsPost> "ENDIF"
            <opt_ident> ;
         <stats>
      <else_end> ::=
         "ELSEIF" "(" <exp> <closeParenthRecla> "THEN" <opt_ident> "EOL" <stats>
            <acceptCommentsPost> <else_end> ;
         struct_if (<opt_ident>, <exp>, <stats>, <else_end>)
      -- structured where statement:
      <structStat> ::=
         <opt_ident> "STRUCTWHERE" "(" <exp> ")" <acceptCommentsM1> "EOL"
            <stats> <acceptCommentsPost> <elsewhere_end> ;
         struct_where (<opt_ident>, <exp>, <stats>, <elsewhere_end>)
      <elsewhere_end> ::= "ENDWHERE" <opt_ident> ;
         stats-list (())
      <elsewhere_end> ::=
         "ELSEWHERE" <opt_ident> "EOL" <stats> <acceptCommentsPost> "ENDWHERE"
            <opt_ident> ;
         <stats>
      <elsewhere_end> ::=
         "ELSEWHERE" "(" <exp> ")" <opt_ident> "EOL" <stats>
            <acceptCommentsPost> <elsewhere_end> ;
         struct_where (<opt_ident>, <exp>, <stats>, <elsewhere_end>)
      -- structured forall statement:
      <structStat> ::=
         <opt_ident> "STRUCTFORALL" "(" <forallRanges> <opt_mask> ")"
            <acceptCommentsM2> "EOL" <stats> <acceptCommentsPost> "ENDFORALL"
            <opt_ident> ;
         -- CM Fortran
         struct_forall (<opt_ident>.1, <forallRanges>, <opt_mask>, <stats>)
      -- select-case statement:
      <structStat> ::=
         <opt_ident> "SELECTCASE" "(" <exp> ")" <acceptCommentsM1> "EOL" <cases>
            "ENDSELECT" <opt_ident> ;
         select_case (<opt_ident>.1, <exp>, <cases>, <opt_ident>.2)
      <structStat> ::=
         "GOULDSELECTCASE" <exp> <acceptCommentsM0> "EOL" <cases> "ENDSELECT"
            <opt_ident> ;
         -- GOULD
         select_case (none (), <exp>, <cases>, <opt_ident>)
      <structStat> ::= 
         <opt_ident> "SELECTTYPE" "(" <expOrPtrAssign> ")" <acceptCommentsM1> "EOL" <cases>
            "ENDSELECT" <opt_ident> ;
         -- F2003
         select_type (<opt_ident>.1, <expOrPtrAssign>, <cases>, <opt_ident>.2)
      <expOrPtrAssign> ::= <exp> ;
         <exp>
      <expOrPtrAssign> ::= <ident> "PTRASS" <exp> ;
         pointer_assign (<ident>, <exp>)
      <case> ::= "CASE" "NEEDKEYWORD" <case_selector> <opt_ident> "EOL" <stats> <acceptCommentsPost> ;
         case (<case_selector>, <opt_ident>, <stats>)
      <case> ::= "GOULDCASE" "NEEDKEYWORD" <gould_case_selector> "EOL" <stats> <acceptCommentsPost> ;
         -- GOULD
         case (<gould_case_selector>, none (), <stats>)
      <case> ::= "ELSE" "EOL" <stats> <acceptCommentsPost> ;
         -- GOULD
         case (default (), none (), <stats>)
      <case> ::= "TYPEIS" "(" <ident> ")" <opt_ident> "EOL" <stats> <acceptCommentsPost> ;
         -- F2003
         typeCase(<ident>, <opt_ident>, <stats>)
      <case> ::= "CLASSIS" "(" <ident> ")" <opt_ident> "EOL" <stats> <acceptCommentsPost> ;
         -- F2003
         classCase(<ident>, <opt_ident>, <stats>)
      <case> ::= "CLASSDEFAULT" <opt_ident> "EOL" <stats> <acceptCommentsPost> ;
         -- F2003
         classCase(none(), <opt_ident>, <stats>)
      <case_selector> ::= "(" <caseRanges> ")" ;
         <caseRanges>
      <case_selector> ::= "DEFAULT" ;
         default ()
      <gould_case_selector> ::= "NEEDCOLON" <gould_caseRanges> "NEEDNOCOLON" ;
         -- GOULD
         <gould_caseRanges>
      <gould_case_selector> ::= "DEFAULT" ;
         -- GOULD
         default ()
      <caseRange> ::= <generalExp> ;
         <generalExp>
      <gould_caseRange> ::= <uexp> ;
         -- GOULD
         <uexp>
      <gould_caseRange> ::= <rangeColon> ;
         -- GOULD
         <rangeColon>
      <gould_caseRange> ::= "(" <rangeColon> ;
         -- GOULD
         left_range (<rangeColon>)
      <gould_caseRange> ::= <rangeColon> ")" ;
         -- GOULD
         right_range (<rangeColon>)
      <gould_caseRange> ::= "(" <rangeColon> ")" ;
         -- GOULD
         left_right_range (<rangeColon>)
      -- loop statement:
      <structStat> ::= <loopStructStat> ;
        <loopStructStat>
      <loopStructStat> ::=
         <opt_ident> "DO" "DOWHILEON" "INTONLYON" <opt_label> "DOLABEL"
            "INTONLYOFF" <do_control1> "DOWHILEOFF" <acceptCommentsM2> "EOL"
            <stats> <acceptCommentsPost> "ENDOFDO" ;
         do (<opt_ident>, <opt_label>, <do_control1>, <stats>)
      <do_control1> ::= ;
         none ()
      <do_control1> ::= "," ;
         -- VAX SUN GOULD
         none ()
      <do_control1> ::= "," "," ;
         -- VAX SUN GOULD
         none ()
      <do_control1> ::= "," <do_control> ;
         <do_control>
      <do_control1> ::= <do_control> ;
         <do_control>
      <do_control> ::= "WHILE" "(" <exp> ")" ;
         while (<exp>)
      <do_control> ::= "BEGIN" ;
         none ()
      <do_control> ::= "UNTIL" "(" <exp> ")" ;
         until (<exp>)
      <do_control> ::= "(" "DOWHILEOFF" <exp> ")" "NEEDKEYWORD" "TIMES" ;
         times (<exp>)
      <do_control> ::= "DOWHILEOFF" <doRange> ;
         <doRange>

      -- OMP STATEMENTS:
      -- [llh] A good magic combination for optional commas seems to be: ... "opt_comma" <listNTwithOptCommas> <no_opt_comma> ...
      --   where <listNTwithOptCommas> uses NT <opt_comma>, but sometimes with extra "NEEDKEYWORD", like here.
      <structStat> ::= <ompSentinel_needkw> "PARALLELDO" "NEEDKEYWORD" "opt_comma" <omp_directive_arguments> <no_opt_comma> <acceptCommentsXM2AndEOL> <loopStructStat> <acceptComments> <opt_eol_omp_end_parallel_do> ;
        ompParallelDo(<omp_directive_arguments>, <loopStructStat>)
      <opt_eol_omp_end_parallel_do> ::= ;
        none()
      <opt_eol_omp_end_parallel_do> ::= "EOLOMPENDPARALLELDO" ;
        none()

      <structStat> ::= <ompSentinel_needkw> "DO" "NEEDKEYWORD" "opt_comma" <omp_directive_arguments> <no_opt_comma> <acceptCommentsXM2AndEOL> <loopStructStat> <acceptComments> <opt_eol_omp_end_do> ;
        ompDo(<omp_directive_arguments>, <loopStructStat>)
      <opt_eol_omp_end_do> ::= ;
        none()
      <opt_eol_omp_end_do> ::= "EOLOMPENDDO" ;
        none()

      <structStat> ::= <ompSentinel_needkw> "PARALLEL" "NEEDKEYWORD" "opt_comma" <omp_directive_arguments> <no_opt_comma> <acceptCommentsXM2AndEOL> <stats> <acceptCommentsPost> "OMPENDPARALLEL" ;
        ompParallel(<omp_directive_arguments>, <stats>)

      <ompSentinel_needkw> ::= <ompSentinel> "NEEDKEYWORD" ;
        <ompSentinel>
      <ompSentinel> ::= %OMPDIRECTIVE ;
         ident-atom(%OMPDIRECTIVE)

      <omp_directive_arguments> ::= ;
        ompClauses-list(())

      <omp_directive_arguments> ::= <omp_clause> "NEEDKEYWORD" <opt_comma_needkw> <omp_directive_arguments> ;
        ompClauses-pre(<omp_clause>, <omp_directive_arguments>)

      <omp_clause> ::= "NUM_THREADS" "(" <exp> ")" ;
        ompNumThreads(<exp>)

      <omp_clause> ::= "SCHEDULE" "(" <exps> ")" ;
        ompSchedule(<exps>)

      <omp_clause> ::= "SHARED" "(" <omp_clause_arguments> ")" ;
        ompShared(<omp_clause_arguments>)

      <omp_clause> ::= "PRIVATE" "(" <omp_clause_arguments> ")" ;
        ompPrivate(<omp_clause_arguments>)

      <omp_clause> ::= "FIRSTPRIVATE" "(" <omp_clause_arguments> ")" ;
        ompFirstPrivate(<omp_clause_arguments>)

      <omp_clause> ::= "LASTPRIVATE" "(" <omp_clause_arguments> ")" ;
        ompLastPrivate(<omp_clause_arguments>)

      <omp_clause> ::= "REDUCTION" "(" "+" ":" <omp_clause_arguments> ")" ;
        ompReduction(ident-atom('+'), <omp_clause_arguments>)

      <omp_clause> ::= "REDUCTION" "(" "*" ":" <omp_clause_arguments> ")" ;
        ompReduction(ident-atom('*'), <omp_clause_arguments>)

      <omp_clause> ::= "REDUCTION" "(" <ident> ":" <omp_clause_arguments> ")" ;
        ompReduction(<ident>, <omp_clause_arguments>)

      <omp_clause> ::= "DEFAULT" "(" <ident> ")" ;
        ompDefault(<ident>)

      <omp_clause_arguments> ::= <omp_clause_arg> ;
        ompClauseArgs-list((<omp_clause_arg>))

      <omp_clause_arguments> ::= <omp_clause_arguments> "," <omp_clause_arg> ;
        ompClauseArgs-post(<omp_clause_arguments>, <omp_clause_arg>)

      <omp_clause_arg> ::= <ident> ;
        <ident>

      -- INTERNALS OF I-O STATEMENTS
      <io_stat> ::= "OPEN" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         open (<ioSpecs>)
      <io_stat> ::= "CLOSE" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         close (<ioSpecs>)
      <io_stat> ::= "FLUSH" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         flush (<ioSpecs>)
      <io_stat> ::= "INQUIRE" "in_io" "(" <ioSpecs> ")" <notin_io> <generalExps> ;
         inquire (<ioSpecs>, <generalExps>)
      <io_stat> ::= "BACKSPACELOG" <logical_unit> ;
         backspace (ioSpecs-list ((<logical_unit>)))
      <io_stat> ::= "BACKSPACE" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         backspace (<ioSpecs>)
      <io_stat> ::= "ENDFILELOG" <logical_unit> ;
         endFile (ioSpecs-list ((<logical_unit>)))
      <io_stat> ::= "ENDFILE" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         endFile (<ioSpecs>)
      <io_stat> ::= "REWINDLOG" <logical_unit> ;
         rewind (ioSpecs-list ((<logical_unit>)))
      <io_stat> ::= "REWIND" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         rewind (<ioSpecs>)
      <io_stat> ::= "PRINT" <format_id> "," <generalExps> ;
         print (<format_id>, <generalExps>)
      <io_stat> ::= "PRINT" <format_id>  ;
         print (<format_id>, exps-list (()))
      <io_stat> ::= "PRINT" ;
         print (none (), exps-list (()))
      <io_stat> ::=
         "READ" "in_io" "(" <vax_io_specs> ")" <notin_io> <generalExps> ;
         read (<vax_io_specs>, <generalExps>)
      <io_stat> ::= "READ" <format_id> "," <generalExps> ;
         read (<format_id>, <generalExps>)
      <io_stat> ::= "READ" <format_id> ;
         read (<format_id>, exps-list (()))
      <io_stat> ::=
         "WRITE" "in_io" "(" <vax_io_specs> ")" <notin_io> <generalExps> ;
         write (<vax_io_specs>, <generalExps>)
      <io_stat> ::= "ACCEPT" <format_id> <generalExps> ;
         -- VAX SUN
         accept (<format_id>, <generalExps>)
      <io_stat> ::= "ENCODE" "in_io" "(" <ioSpecs> ")" <notin_io> <generalExps> ;
         -- VAX SUN
         encode (<ioSpecs>, <generalExps>)
      <io_stat> ::= "DECODE" "in_io" "(" <ioSpecs> ")" <notin_io> <generalExps> ;
         -- VAX SUN
         decode (<ioSpecs>, <generalExps>)
      <io_stat> ::= "DELETE" "in_io" "(" <vax_io_specs> ")" <notin_io> ;
         -- VAX SUN
         delete (<vax_io_specs>)
      <io_stat> ::= "FIND" "in_io" "(" <vax_io_specs> ")" <notin_io> ;
         -- VAX SUN
         find (<vax_io_specs>)
      <io_stat> ::= "REWRITE" "in_io" "(" <ioSpecs> ")" <notin_io> <generalExps> ;
         -- VAX SUN
         rewrite (<ioSpecs>, <generalExps>)
      <io_stat> ::= "UNLOCK" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         -- VAX SUN
         unlock (<ioSpecs>)
      <io_stat> ::= "UNLOCK" <numspec> ;
         -- VAX SUN
         unlock (ioSpecs-list ((<numspec>)))
      <io_stat> ::= "BACKFILELOG" <logical_unit> ;
         -- GOULD
         backFile (ioSpecs-list ((<logical_unit>)))
      <io_stat> ::= "BACKFILE" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         -- GOULD
         backFile (<ioSpecs>)
      <io_stat> ::= "SKIPFILELOG" <logical_unit> ;
         -- GOULD
         skipFile (ioSpecs-list ((<logical_unit>)))
      <io_stat> ::= "SKIPFILE" "in_io" "(" <ioSpecs> ")" <notin_io> ;
         -- GOULD
         skipFile (<ioSpecs>)
      <io_stat> ::= "DEFINEFILE" <defineSpecs> ;
         defineFile (<defineSpecs>)
      <notin_io> ::= "notin_io" ;
         none ()
      <defineSpec> ::= <intCst> <defineparams> ;
         -- GOULD
         defineSpec (<intCst>, <defineparams>)
      <defineSpec> ::= <bitCst> <defineparams> ;
         -- GOULD
         defineSpec (<bitCst>, <defineparams>)
      <defineSpec> ::= <ident> <defineparams> ;
         -- GOULD
         defineSpec (<ident>, <defineparams>)
      <defineparams> ::=
         "(" <numspec> "," <numspec> "," <opt_ident> "," <ident> ")" ;
         -- GOULD
         defineParams (<numspec>.1, <numspec>.2, <opt_ident>, <ident>)
      <vax_io_specs> ::= <unit_record> "," <ioSpecs> ;
         -- VAX
         ioSpecs-pre (<unit_record>, <ioSpecs>)
      <vax_io_specs> ::= <unit_record> ;
         -- VAX
         ioSpecs-list ((<unit_record>))
      <vax_io_specs> ::= <ioSpecs> ;
         -- VAX
         <ioSpecs>
      <unit_record> ::= <numspec> "'" <exp> ;
         -- VAX
         unit_record (<numspec>, <exp>)
      -- because <variableRef> use the "NEEDCOLON" actions
      <numspec> ::= <variableRef> "NEEDNOCOLON" ;
         -- VAX
         <variableRef>
      <numspec> ::= <intCst> ;
         -- VAX
         <intCst>
      <numspec> ::= <bitCst> ;
         -- VAX
         <bitCst>
      <ioSpec> ::= <eqname_exp> ;
         <eqname_exp>
      <ioSpec> ::= <eqname> <eqname_exp> ;
         eqName (<eqname>, <eqname_exp>)
      <eqname_exp> ::= <star> ;
         <star>
      <eqname_exp> ::= <exp> ;
         <exp>
      <eqname_exp> ::= "(" <ioRanges> ")" ;
         -- VMS
         <ioRanges>
      <ioRange> ::= <opt_exp> ":" <opt_exp> ;
         -- VMS
         keySpecs-list ((<opt_exp>.1, <opt_exp>.2))
      <ioRange> ::= <opt_exp> ":" <opt_exp> ":" <opt_exp> ;
         -- VMS
         keySpecs-list ((<opt_exp>.1, <opt_exp>.2, <opt_exp>.3))
      <ioRange> ::= <opt_exp> ":" <opt_exp> ":" <opt_exp> ":" <opt_exp> ;
         -- VMS
         keySpecs-list ((<opt_exp>.1, <opt_exp>.2, <opt_exp>.3, <opt_exp>.4))
      <format_id> ::= <star> ;
         <star>
      <format_id> ::= <label> ;
         <label>
      <format_id> ::= <variableRef> ;
         <variableRef>
      <format_id> ::= <string_exp> ;
         <string_exp>
      <logical_unit> ::= <star> ;
         <star>
      <logical_unit> ::= <exp> ;
         <exp>-- EXPRESSIONS         
         
      <simple_exp> ::= <ident> ;
         <ident>
      <simple_exp> ::= <simpleCst> ;
         <simpleCst>
            -- <generalExp> is all possible expressions, including those which
            --   appead only in special places, such as io expressions.
            --   For example, <generalExp> can be <rangeColon>, *<label>, <keywordArg>,
            --   but also normal <exp>.
            -- <exp> is all normal expressions that can appear in computations, i.e.
            --   that return a value, maybe composite, such as IO implicit DO <implDo>
            --   or a <complexConstructor>. It may also be a normal, scalar expression <uexp>.
            -- <uexp> is all scalar normal expressions,
            --   such as constants, variables, operations and function calls.
            --   It can also be an array constructor...
            --   TODO: Maybe array constructors could be moved up into <exp>'s ??
         
      <generalExp> ::= <exp> ;
         <exp>
      <generalExp> ::= "*" <label> ;
         <label>
      <generalExp> ::= <rangeColon> ;
         <rangeColon>
      <generalExp> ::= <keywordArg> ;
         -- CM Fortran
         <keywordArg>
      <rangeColon> ::= <opt_exp> ":" <opt_exp> ;
         range (<opt_exp>.1, <opt_exp>.2, none ())
      <rangeColon> ::= <opt_exp> ":" <opt_exp> ":" <exp> ;
         range (<opt_exp>.1, <opt_exp>.2, <exp>)
      -- CM Fortran
      <keywordArg> ::= <ident> "=" <exp> ;
         eqName (<ident>, <exp>)
      -- CM Fortran
      <keywordArg> ::= <ident> "=" <star> ;
         eqName (<ident>, <star>)
      -- F2003
      <keywordArg> ::= <ident> "=" <rangeColon> ;
         eqName (<ident>, <rangeColon>)
      <keywordArg> ::= <star> ;
         -- CM Fortran
         <star>
      <exp> ::= <uexp> ;
         <uexp>
      <exp> ::= <compositeExp> ;
         <compositeExp>
      <compositeExp> ::= <complexConstructor> ;
         <complexConstructor>
      <compositeExp> ::= <implDo> ;
         <implDo>
      <complexConstructor> ::= "(" <uexp> "," <uexp> ")" ;
         complexConstructor (<uexp>.1, <uexp>.2)
      <implDo> ::= "(" <uexp> "," <doRange> ")" ;
         impldo (exps-list ((<uexp>)), <doRange>)
      <implDo> ::= "(" <compositeExp> "," <doRange> ")" ;
         impldo (exps-list ((<compositeExp>)), <doRange>)
      <implDo> ::= "(" <exps2more> "," <doRange> ")" ;
         impldo (<exps2more>, <doRange>)
      -- Reject 2 <exp>s in parenths, 'cause conflict with <complexConstructor>:
      <implDo> ::= "(" "(" <exps3more> ")" "," <doRange> ")" ;
         impldo (<exps3more>, <doRange>)
      <doRange> ::= <ident> "=" <rangeComma> ;
         nameRange (<ident>, <rangeComma>)
      <rangeComma> ::= <exp> "," <exp> "," <exp> ;
         range (<exp>.1, <exp>.2, <exp>.3)
      <rangeComma> ::= <exp> "," <exp> ;
         range (<exp>.1, <exp>.2, none ())
      <exps2more> ::= <uexp> "," <exp> ;
         exps-list ((<uexp>, <exp>))
      <exps2more> ::= <compositeExp> "," <exp> ;
         exps-list ((<compositeExp>, <exp>))
      <exps2more> ::= <exps2more> "," <exp> ;
         exps-post (<exps2more>, <exp>)
      <exps3more> ::= <exps2more> "," <uexp> ;
         exps-post (<exps2more>, <uexp>)
      <exps3more> ::= <exps2more> "," <implDo> ;
         exps-post (<exps2more>, <implDo>)
      <uexp> ::= <intCst> "LBRACKET" "NEEDTYPEKEYWORD" <constructor> "RBRACKET" ;
         nvalues (<intCst>, <constructor>)
      <uexp> ::= "LBRACKET" "NEEDTYPEKEYWORD" <constructor> "RBRACKET" ;
         <constructor>
      <constructor> ::= <exps> ;
         -- R431
         constructor (<exps>)
      <constructor> ::= <rangeColon> ;
         -- R431
         constructor (<rangeColon>)
      <constructor> ::= <type> <dblcolon> <exps> ;
         -- special gfortran cast on string length
         multiCast(<type>, constructor (<exps>))
      <uexp> ::= <variableRef> ;
         <variableRef>-- NEEDCOLON is to avoid Gould Ident containing ':'
         
      <variableRef> ::=
         <variableRef> "(" "NEEDCOLON" <generalExps> "NEEDNOCOLON" ")" ;
         vardim (<variableRef>, <generalExps>)
      <variableRef> ::= <variableRef> "%" <ident> ;
         -- F90
         fieldRef (<variableRef>, <ident>)
      <variableRef> ::= <variableRef> "." <ident> ;
         -- VAX SUN
         fieldRef (<variableRef>, <ident>)
      <variableRef> ::= <ident> ;
         <ident>
      <generalExps> ::= ;
         exps-list (())
      <generalExps> ::= <generalExp> ;
         exps-list ((<generalExp>))
      ---- Note: we allow null expressions for vax VMS
      <generalExps> ::= <generalExps1> "," <opt_generalExp> ;
         exps-post (<generalExps1>, <opt_generalExp>)
      <generalExps1> ::= <opt_generalExp> ;
         exps-list ((<opt_generalExp>))
      <generalExps1> ::= <generalExps1> "," <opt_generalExp> ;
         exps-post (<generalExps1>, <opt_generalExp>)
      <uexp> ::= "(" <exp> ")" ;
         parenth (<exp>)
      <uexp> ::= <simpleCst> ;
         <simpleCst>
      <uexp> ::= <exp> "+" <exp> ;
         add (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "-" <exp> ;
         sub (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "*" <exp> ;
         mul (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "/" <exp> ;
         div (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "**" <exp> ;
         power (<exp>.1, <exp>.2)
      <uexp> ::= "+" <exp> ;
         plus (<exp>)
      <uexp> ::= "-" <exp> ;
         minus (<exp>)
      <uexp> ::= <exp> "EQ" <exp> ;
         eq (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "NE" <exp> ;
         ne (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "GE" <exp> ;
         ge (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "LE" <exp> ;
         le (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "GT" <exp> ;
         gt (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "LT" <exp> ;
         lt (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "EQV" <exp> ;
         eqv (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "NEQV" <exp> ;
         neqv (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "AND" <exp> ;
         and (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "OR" <exp> ;
         or (<exp>.1, <exp>.2)
      <uexp> ::= <exp> "XOR" <exp> ;
         xor (<exp>.1, <exp>.2)
      <uexp> ::= "NOT" <exp> ;
         not (<exp>)
      <uexp> ::= <exp> "//" <exp> ;
         concat (<exp>.1, <exp>.2)
      -- string exp needed for format_id
      <string_exp> ::= <gstring> ;
         <gstring>
      <string_exp> ::= <string_exp> "//" <gstring> ;
         concat (<string_exp>, <gstring>)
      -- pb : shift/reduce conflict
      <definedOp> ::= %OP_USER ;
         definedOp-atom(%OP_USER)
      <uexp> ::= "OP_USER" <definedOp> <exp> ;
         unaryDefOp (<definedOp>, <exp>)
      <uexp> ::= <exp> "OP_USER" <definedOp> <exp> ;
         binaryDefOp (<definedOp>, <exp>.1, <exp>.2)
      <simpleCst> ::= <nnCst> ;
         <nnCst>
      <simpleCst> ::= <gstring> ;
         <gstring>
      <simpleCst> ::= <kindCst> ;
         <kindCst>
      <nnCst> ::= <intCst> ;
         <intCst>
      <nnCst> ::= <realCst> ;
         <realCst>
      <nnCst> ::= <doubleCst> ;
         <doubleCst>
      <nnCst> ::= <bitCst> ;
         <bitCst>
      <nnCst> ::= <logicCst> ;
         <logicCst>
      <kindCst> ::= <nnCst> "UNDERSCORE" <ident> ;
         kindCst (<nnCst>, <ident>)
      <kindCst> ::= <nnCst> "UNDERSCORE" <intCst> ;
         kindCst (<nnCst>, <intCst>)
      <kindCst> ::= <ident> "UNDERSCORE" <gstring> ;
         kindCst (<gstring>, <ident>)
      <type> ::= <f77_like_type> ;
         <f77_like_type>
      <type> ::= <type_non77like> ;
         <type_non77like>
      <type_non77like> ::= <groundType> ;
         <groundType>
      <type_non77like> ::= <groundType> <typesize> ;
         sizedType (<groundType>, <typesize>)
      <type_non77like> ::= "TYPE" "(" <ident> ")" ;
         derived (<ident>)
      <type_non77like> ::= "CLASS" "(" <ident> ")" ;
         class (<ident>)
      <type_non77like> ::= "CLASS" "(" <star> ")" ;
         class (<star>)
      <type_non77like> ::= "PROCEDURE" "(" <opt_ident> ")" ;
         procedure (<opt_ident>)
      <type_non77like> ::= "PROCEDURE" ;
         procedure (none())

      <groundType> ::= "CHARACTER" ;
         character ()
      <groundType> ::= <non_char_type> ;
         <non_char_type>
      <non_char_type> ::= "INTEGER" ;
         integer ()
      <non_char_type> ::= "REAL" ;
         real ()
      <non_char_type> ::= "DOUBLE" ;
         double ()
      <non_char_type> ::= "COMPLEX" ;
         complex ()
      <non_char_type> ::= "DOUBLECOMPLEX" ;
         dblcomplex ()
      <non_char_type> ::= "LOGICAL" ;
         logical ()
      <non_char_type> ::= "BYTE" ;
         byte ()
      <non_char_type> ::= "BIT" ;
         bit ()
      <non_char_type> ::= "VOID" ;
         void ()
      <typesize> ::= "size_comma" "*" "INTONLYON" <typel_1> "INTONLYOFF" ;
         <typel_1>
      <typel_1> ::= <intCst> ;
         <intCst>
      <typel_1> ::= "(" <exp> ")" ;
         <exp>
      <typel_1> ::= "(" <star> ")" ;
         <star>
      <f77_like_type> ::= <non_char_type> "(" <generalExp> ")" ;
         case <non_char_type>
            when complex() =>
               case <generalExp>
                  when intCst-atom('4') =>
                     sizedType (<non_char_type>, intCst-atom('8'))
                  when intCst-atom('8') =>
                     sizedType (<non_char_type>, intCst-atom('16'))
                  when others =>
                     sizedType (<non_char_type>, <generalExp>)
               end case
            when others =>
               sizedType (<non_char_type>, <generalExp>)
         end case
      <f77_like_type> ::=
         "CHARACTER" "(" "NEEDCOLON" <generalExps> "NEEDNOCOLON" ")" ;
         sizedType (character (), <generalExps>)
      -- ATOMS
      <ident> ::= %NAME ;
         ident-atom(%NAME)
      <star> ::= "*" ;
         star ()
      <dblcolon> ::= ":" ":" ;
         none ()
      <eqname> ::= %EQNAME ;
         ident-atom(%EQNAME)
      <label> ::= %INTCST ;
         label-atom(%INTCST)
      <intCst> ::= %INTCST ;
         intCst-atom(%INTCST)
      <bitCst> ::= %BITCST ;
         bitCst-atom(%BITCST)
      <realCst> ::= %REALCST ;
         realCst-atom(%REALCST)
      <doubleCst> ::= %DOUBLECST ;
         doubleCst-atom(%DOUBLECST)
      <logicCst> ::= "TRUE" ;
         logicCst-atom('true')
      <logicCst> ::= "FALSE" ;
         logicCst-atom('false')
      <letter> ::= %LETTER ;
         letter-atom(%LETTER)
      <oneOffset> ::= %OFFSET ;
         intCst-atom(%OFFSET)
      <gstring> ::= %HOLLERITH ;
         hollerith-atom(%HOLLERITH)
      <gstring> ::= <strings1> ;
         <strings1>
      <string> ::= %ASTRING ;
         string-atom(%ASTRING)
      <simpleOption> ::= "NOG_FLOATING" ;
         gFloating (no ())
      <simpleOption> ::= "G_FLOATING" ;
         gFloating (yes ())
      <simpleOption> ::= "NOI4" ;
         i4 (no ())
      <simpleOption> ::= "I4" ;
         i4 (yes ())
      <simpleOption> ::= "NOF77" ;
         f77 (no ())
      <simpleOption> ::= "F77" ;
         f77 (yes ())
      <simpleOption> ::= "NOCHECK" ;
         check (no ())
      <simpleOption> ::= "NOEXTEND_SOURCE" ;
         extendSource (no ())
      <simpleOption> ::= "EXTEND_SOURCE" ;
         extendSource (yes ())
      <checkOption> ::= "ALL" ;
         checkAll ()
      <checkOption> ::= "NOOVERFLOW" ;
         checkOverflow (no ())
      <checkOption> ::= "OVERFLOW" ;
         checkOverflow (yes ())
      <checkOption> ::= "NOBOUNDS" ;
         checkBounds (no ())
      <checkOption> ::= "BOUNDS" ;
         checkBounds (yes ())
      <checkOption> ::= "NOUNDERFLOW" ;
         checkUnderflow (no ())
      <checkOption> ::= "UNDERFLOW" ;
         checkUnderflow (yes ())
      <checkOption> ::= "NONE" ;
         checkNone ()
      -- UNFORGIVEABLE HACK !!!
      -- This is to force the "RECLASSIFY" action to be done
      -- BEFORE the following token is read from the scanner !!!
      <closeParenthRecla> ::= ")" "RECLASSIFY" ;
         none ()
      -- Acceptation of comments:
      <acceptCommentsAndEOL> ::= <acceptComments> "EOL" ;
         none ()
      <acceptCommentsXM2AndEOL> ::= <acceptCommentsXM2> "EOL" ;
         none ()
      <acceptComments> ::= "ACCEPTCOMMENTS" ;
         none ()
      <acceptCommentsPost> ::= "ACCEPTCOMMENTSPOST" ;
         none ()
        -- accept comments on top tree in built-trees stack, or on 1st child if list:
      <acceptCommentsM0> ::= "ACCEPTCOMMENTSM0" ;
         none ()
        -- accept comments on tree at depth 1 in built-trees stack, or on 1st child if list:
      <acceptCommentsM1> ::= "ACCEPTCOMMENTSM1" ;
         none ()
        -- accept comments on tree at depth 2 in built-trees stack, or on 1st child if list:
      <acceptCommentsM2> ::= "ACCEPTCOMMENTSM2" ;
         none ()
        -- accept comments on eXactly tree at depth 2 in built-trees stack:
      <acceptCommentsXM2> ::= "ACCEPTCOMMENTSXM2" ;
         none ()
      -- [llh] TODO: normalize the "s" and "s1" terminations.
      -- "LIST" NON-TERMINALS (SYSTEMATIC)
      <units> ::= ;
         units-list (())
      <units> ::= <units> <unit> ;
         units-post (<units>, <unit>)
      <decls> ::= ;
         decls-list (())
      <decls> ::= <not_empty_decls> ;
         <not_empty_decls>
      <not_empty_decls> ::= <decls> <decl> <acceptCommentsAndEOL> ;
         decls-post (<decls>, <decl>)
      <cases> ::= ;
         cases-list (())
      <cases> ::= <cases> <case> ;
         cases-post (<cases>, <case>)
      <privateSequences> ::= ;
         privateSequences-list (())
      <privateSequences> ::= <privateSequences> <privateSequence> ;
         privateSequences-post (<privateSequences>, <privateSequence>)
      <formatElems> ::= ;
         formatElems-list (())
      <formatElems> ::= <formatElems> <formatElem> ;
         formatElems-post (<formatElems>, <formatElem>)
      <components> ::= ;
         fieldDecls-list (())
      <components> ::= <components> <component> ;
         fieldDecls-post (<components>, <component>)
      <stats> ::= ;
         stats-list (())
      <stats> ::= <not_empty_stats> ;
         <not_empty_stats>
      <not_empty_stats> ::= <stats> <stat> <acceptCommentsAndEOL> ;
         stats-post (<stats>, <stat>)
      -- "NON-EMPTY LIST" NON-TERMINALS (SYSTEMATIC)
      <commons1> ::= <common> ;
         commons-list ((<common>))
      <commons1> ::= <commons1> <common> ;
         commons-post (<commons1>, <common>)
      <records1> ::= <record> ;
         records-list ((<record>))
      <records1> ::= <records1> <record> ;
         records-post (<records1>, <record>)
      <extBases1> ::= <extBase> ;
         extBases-list ((<extBase>))
      <extBases1> ::= <extBases1> <extBase> ;
         extBases-post (<extBases1>, <extBase>)
      <namelists1> ::= <namelist> ;
         namelists-list ((<namelist>))
      <namelists1> ::= <namelists1> <namelist> ;
         namelists-post (<namelists1>, <namelist>)
      <localUnits1> ::= <localUnit> ;
         units-list ((<localUnit>))
      <localUnits1> ::= <localUnits1> <localUnit> ;
         units-post (<localUnits1>, <localUnit>)
      <interfaces1> ::= <interface> <acceptCommentsAndEOL> ;
         interfaces-list ((<interface>))
      <interfaces1> ::= <interfaces1> <interface> <acceptCommentsAndEOL> ;
         interfaces-post (<interfaces1>, <interface>)
      <strings1> ::= <string> ;
         strings-list ((<string>))
      <strings1> ::= <strings1> <string> ;
         strings-post (<strings1>, <string>)
      -- COMMA-SEPARATED "NON-EMPTY LIST" NON-TERMINALS (SYSTEMATIC)
      <parameters> ::= <parameter> ;
         parameters-list ((<parameter>))
      <parameters> ::= <parameters> "," <parameter> ;
         parameters-post (<parameters>, <parameter>)
      -- [llh] TODO: pourquoi ce <ioSpec> ??
      <dirArgs> ::= <ioSpec> ;
         dirArgs-list ((<ioSpec>))
      <dirArgs> ::= <dirArgs> "," <ioSpec> ;
         dirArgs-post (<dirArgs>, <ioSpec>)
      <letterss> ::= <letters> ;
         letterss-list ((<letters>))
      <letterss> ::= <letterss> "," <letters> ;
         letterss-post (<letterss>, <letters>)
      <equivalences> ::= <equivalence> ;
         equivalences-list ((<equivalence>))
      <equivalences> ::= <equivalences> "," <equivalence> ;
         equivalences-post (<equivalences>, <equivalence>)
      <variableRefs> ::= <variableRef> ;
         variableRefs-list ((<variableRef>))
      <variableRefs> ::= <variableRefs> "," <variableRef> ;
         variableRefs-post (<variableRefs>, <variableRef>)
      <idents> ::= <ident> ;
         idents-list ((<ident>))
      <idents> ::= <idents> "," <ident> ;
         idents-post (<idents>, <ident>)
      <basicDecls> ::= <basicDecl> ;
         basicDecls-list ((<basicDecl>))
      <basicDecls> ::= <basicDecls> "," <basicDecl> ;
         basicDecls-post (<basicDecls>, <basicDecl>)
      <fullDecls> ::= <fullDecl> ;
         fullDecls-list ((<fullDecl>))
      <fullDecls> ::= <fullDecls> "," <fullDecl> ;
         fullDecls-post (<fullDecls>, <fullDecl>)
      <labels> ::= <label> ;
         labels-list ((<label>))
      <labels> ::= <labels> "," <label> ;
         labels-post (<labels>, <label>)
      <params> ::= <param> ;
         params-list ((<param>))
      <params> ::= <params> "," <param> ;
         params-post (<params>, <param>)
      <forallRanges> ::= <forallRange> ;
         nameRanges-list ((<forallRange>))
      <forallRanges> ::= <forallRanges> "," <forallRange> ;
         nameRanges-post (<forallRanges>, <forallRange>)
      <caseRanges> ::= <caseRange> ;
         exps-list ((<caseRange>))
      <caseRanges> ::= <caseRanges> "," <caseRange> ;
         exps-post (<caseRanges>, <caseRange>)
      <gould_caseRanges> ::= <gould_caseRange> ;
         exps-list ((<gould_caseRange>))
      <gould_caseRanges> ::= <gould_caseRanges> "," <gould_caseRange> ;
         exps-post (<gould_caseRanges>, <gould_caseRange>)
      <ioRanges> ::= <ioRange> ;
         exps-list ((<ioRange>))
      <ioRanges> ::= <ioRanges> "," <ioRange> ;
         exps-post (<ioRanges>, <ioRange>)
      <renamedVisibles> ::= <renamed> ;
         renamedVisibles-list ((<renamed>))
      <renamedVisibles> ::= <renamedVisibles> "," <renamed> ;
         renamedVisibles-post (<renamedVisibles>, <renamed>)
      <pointerPairs> ::= <pointerPair> ;
         pointerPairs-list ((<pointerPair>))
      <pointerPairs> ::= <pointerPairs> "," <pointerPair> ;
         pointerPairs-post (<pointerPairs>, <pointerPair>)
      <datavars> ::= <datavar> ;
         datavars-list ((<datavar>))
      <datavars> ::= <datavars> "," <datavar> ;
         datavars-post (<datavars>, <datavar>)
      <values> ::= <value> ;
         values-list ((<value>))
      <values> ::= <values> "," <value> ;
         values-post (<values>, <value>)
      <blockNames> ::= <blockName> ;
         idents-list ((<blockName>))
      <blockNames> ::= <blockNames> "," <blockName> ;
         idents-post (<blockNames>, <blockName>)
      <ioSpecs> ::= <ioSpec> ;
         ioSpecs-list ((<ioSpec>))
      <ioSpecs> ::= <ioSpecs> "," <ioSpec> ;
         ioSpecs-post (<ioSpecs>, <ioSpec>)
      <exps> ::= <exp> ;
         exps-list ((<exp>))
      <exps> ::= <exps> "," <exp> ;
         exps-post (<exps>, <exp>)
      <defineSpecs> ::= <defineSpec> ;
         defineSpecs-list ((<defineSpec>))
      <defineSpecs> ::= <defineSpecs> "," <defineSpec> ;
         defineSpecs-post (<defineSpecs>, <defineSpec>)
      -- OTHER LIST NON-TERMINALS, WITH DIFFERENT SEPARATORS (SYSTEMATIC)
      -- [llh] TODO: push the non-standard separator into the elementary non-terminal...
      <options> ::= "/" "NEEDKEYWORD" <option> ;
         options-list ((<option>))
      <options> ::= <options> "/" "NEEDKEYWORD" <option> ;
         options-post (<options>, <option>)
      <prefixs> ::= <prefix> "NEEDKEYWORD" ;
         prefixs-list ((<prefix>))
      <prefixs> ::= <prefixs> <prefix> "NEEDKEYWORD" ;
         prefixs-post (<prefixs>, <prefix>)
      <implicits> ::= <implicit> ;
         implicits-list ((<implicit>))
      <implicits> ::= <implicits> "," "NEEDIMPLICIT" <implicit> ;
         implicits-post (<implicits>, <implicit>)
      <onlyVisibles> ::= ;
         onlyVisibles-list (())
      <onlyVisibles> ::= <visible> ;
         onlyVisibles-list ((<visible>))
      <onlyVisibles> ::= <onlyVisibles> "," "NEEDOPKEYWORD" <visible> ;
         onlyVisibles-post (<onlyVisibles>, <visible>)
      <genericNames1> ::= <genericName> ;
         idents-list ((<genericName>))
      <genericNames1> ::= <genericNames1> "," "NEEDOPKEYWORD" <genericName> ;
         idents-post (<genericNames1>, <genericName>)
      <fieldDecls1> ::= <fieldDecl> "EOL" ;
         fieldDecls-list ((<fieldDecl>))
      <fieldDecls1> ::= <fieldDecls1> <fieldDecl> "EOL" ;
         fieldDecls-post (<fieldDecls1>, <fieldDecl>)
      <mapDecls1> ::= <mapDecl> "EOL" ;
         mapDecls-list ((<mapDecl>))
      <mapDecls1> ::= <mapDecls1> <mapDecl> "EOL" ;
         mapDecls-post (<mapDecls1>, <mapDecl>)
      <typeSpecModifiers> ::= "NEEDKEYWORD" <typeSpecModifier> ;
         typeSpecModifiers-list ((<typeSpecModifier>))
      <typeSpecModifiers> ::=
         <typeSpecModifiers> "," "NEEDKEYWORD" <typeSpecModifier> ;
         typeSpecModifiers-post (<typeSpecModifiers>, <typeSpecModifier>)
      <dims> ::= "NEEDCOLON" <dim> ;
         dims-list ((<dim>))
      <dims> ::= <dims> "NEEDCOLON" "," <dim> ;
         dims-post (<dims>, <dim>)
      <distFormats> ::= "RECLASSIFY" <distFormat> ;
         distFormats-list ((<distFormat>))
      <distFormats> ::= <distFormats> "," "RECLASSIFY" <distFormat> ;
         distFormats-post (<distFormats>, <distFormat>)
      <hpfAttributes> ::= <hpfAttribute> ;
         hpfAttributes-list ((<hpfAttribute>))
      <hpfAttributes> ::= <hpfAttributes> "," "RECLASSIFY" <hpfAttribute> ;
         hpfAttributes-post (<hpfAttributes>, <hpfAttribute>)
      -- "OPTIONAL" NON-TERMINALS (SYSTEMATIC)
      <opt_offset> ::= <offset> ;
         <offset>
      <opt_offset> ::= ;
         none ()
      <opt_exp> ::= <exp> ;
         <exp>
      <opt_exp> ::= ;
         none ()
      <opt_generalExp> ::= <generalExp> ;
         <generalExp>
      <opt_generalExp> ::= ;
         none ()
      <opt_ident> ::= <ident> ;
         <ident>
      <opt_ident> ::= ;
         none ()
      <opt_label> ::= <label> ;
         <label>
      <opt_label> ::= ;
         none ()
      <opt_dblcolon> ::= <dblcolon> ;
         none ()
      <opt_dblcolon> ::= ;
         none ()
      <opt_returnSize> ::= <returnSize> ;
         <returnSize>
      <opt_returnSize> ::= ;
         none ()
      <opt_comma_needkw> ::= "," "NEEDKEYWORD" ;
         none ()
      <opt_comma_needkw> ::= ;
         none ()
      <opt_comma> ::= "," ;
         none ()
      <opt_comma> ::= ;
         none ()
      -- ENTRY POINTS (SYSTEMATIC)
      <axiom> ::= "[Unit]" <unit> ;
         <unit>
      <axiom> ::= "[OptHeader]" <header> ;
         <header>
      <axiom> ::= "[OptHeader]" ;
         none ()
      <axiom> ::= "[OptTrailer]" <trailer> ;
         <trailer>
      <axiom> ::= "[OptTrailer]" ;
         none ()
      <axiom> ::= "[Decls]" <decls> ;
         <decls>
      <axiom> ::= "[Decl]" <decl> ;
         <decl>
      <axiom> ::= "[Stats]" <stats> ;
         <stats>
      <axiom> ::= "[Stat]" <stat> ;
         <stat>
      <axiom> ::= "[Exp]" <generalExp> ;
         <generalExp>
      <axiom> ::= "[Type]" <type> ;
         <type>
      <includefile> ::= <decls> <topStats> <acceptCommentsPost> ;
         includeFile (<decls>, <topStats>)
      <axiom> ::= "[IncludeFile]" <includefile> ;
         <includefile>

   abstract syntax
      Unit  ::= unit ;
      IncludeFile  ::= includeFile ;
      includeFile  -> Decls Stats ;
      unit  -> Options OptHeader Decls Stats Units OptTrailer OptOffset ;
      Option  ::=
         gFloating i4 f77 extendSource check checkAll checkNone checkOverflow
         checkBounds checkUnderflow ;
      gFloating  -> YesNo ;
      i4  -> YesNo ;
      f77  -> YesNo ;
      extendSource  -> YesNo ;
      check  -> YesNo ;
      checkOverflow  -> YesNo ;
      checkBounds  -> YesNo ;
      checkUnderflow  -> YesNo ;
      checkAll  -> ;
      checkNone  -> ;
      YesNo  ::= yes no ;
      yes  -> ;
      no  -> ;
      Header  ::= program blockdata module subroutine function hpfHeader ;
      program  -> OptIdent ;
      blockdata  -> OptIdent ;
      module  -> Ident ;
      hpfHeader  -> Ident TypeHeader ;
      subroutine  -> Prefixs Ident Params OptBind;
      function  -> OptType Ident Params Prefixs OptIdent;
      TypeHeader  ::= program blockdata module ;
      Param  ::= ident star ;
      Prefix  ::= recursive pure elemental Type intCst ;
      recursive  -> ;
      pure  -> ;
      elemental  -> ;
      Trailer  ::=
         labstat end endprogram endblockdata endsubroutine endfunction endmodule ;
      end  -> ;
      endprogram  -> OptIdent ;
      endblockdata  -> OptIdent ;
      endsubroutine  -> OptIdent ;
      endfunction  -> OptIdent ;
      endmodule  -> OptIdent ;
      OptOffset  ::= Offset none ;
      Offset  ::= offset ;
      offset  -> IntCst IntCst ;
      Decl  ::=
         parameters implicits dimensionDecl externalDecl intrinsicDecl saveDecl
         equivalences commons intentDecl derivedDecl interfaceDecl useDecl importDecl
         accessDecl allocatableDecl optionalDecl pointerDecl pointerPairs
         targetDecl automaticDecl staticDecl volatileDecl contiguousDecl
         structureDecl records
         dictionaryDecl extBlocks extBases extDummyDecl directiveDecl Inst
         hpfProcessors hpfAlign hpfDistribute hpfDynamic hpfInherit hpfTemplate
         hpfCombined hpfSequence hpfNoSequence ;
      Inst  ::=
         formatDecl entryDecl includeDecl pppLine namelists datas flcdirectiveDecl
         varDecl stmtFunc ;
      parameters  -> Parameter * ... ;
      Parameter  ::= parameterDecl ;
      parameterDecl  -> Ident Exp ;
      implicits  -> Implicit * ... ;
      Implicit  ::= typedLetters ;
      typedLetters  -> TypeOrMore Letterss ;
      TypeOrMore  ::= Type isStatic isAutomatic ;
      isStatic  -> ;
      isAutomatic  -> ;
      Letters  ::= Letter letterRange ;
      letterRange  -> Letter Letter ;
      Letter  ::= letter ;
      letter  -> implemented as STRING ;
      dimensionDecl  -> FullDecls ;
      FullDecl  ::= fullDecl ;
      fullDecl  -> BasicDecl OptInitialVal ;
      InitialVal  ::= Values cm_initializer pointer_initializer ;
      cm_initializer  -> Exp ;
      pointer_initializer  -> Exp ;
      BasicDecl  ::= Ident arrayDecl sizedDecl pseudoField ;
      arrayDecl  -> Ident Dims ;
      sizedDecl  -> BasicDecl Exp ;
      Dim  ::= dim ;
      dim  -> Exp Exp ;
      pseudoField  -> ;
      externalDecl  -> Idents ;
      intrinsicDecl  -> Idents ;
      saveDecl  -> Idents ;
      Idents  ::= idents ;
      idents  -> IdentObject * ... ;
      IdentObject  ::=
         ident blockName definedOp assign add sub mul div div power eq ne ge le
         gt lt eqv neqv and or xor not concat ;
      equivalences  -> VariableRefs * ... ;
      commons  -> Common * ... ;
      Common  ::= common ;
      common  -> OptIdent BasicDecls ;
      intentDecl  -> IntentValue Idents ;
      IntentValue  ::= in out inout ;
      in  -> ;
      out  -> ;
      inout  -> ;
      derivedDecl  -> Ident OptAccess PrivateSequences FieldDecls Decls OptIdent ;
      Access  ::= isPublic isPrivate isAbstract bind extends accessDecl;
      isPublic  -> ;
      isPrivate  -> ;
      isBind  -> ;
      PrivateSequence  ::= isPrivate isSequence ;
      isSequence  -> ;
      FieldDecl  ::= substructure union_decl records parameters varDecl ;
      substructure  -> OptIdent BasicDecls FieldDecls ;
      union_decl  -> MapDecls ;
      MapDecl  ::= mapDecl ;
      mapDecl  -> FieldDecls ;
      interfaceDecl  -> OptIdentObject Interfaces OptIdentObject ;
      Interface  ::= interface module_procedure directiveDecl ;
      interface  -> Header Decls Trailer ;
      module_procedure  -> Idents ;
      useDecl  -> Ident Visibles ;
      importDecl -> Idents ;
      Visibles  ::= onlyVisibles renamedVisibles ;
      onlyVisibles  -> Visible * ... ;
      renamedVisibles  -> Visible * ... ;
      Visible  ::= IdentObject renamed ;
      renamed  -> Ident Ident ;
      accessDecl  -> Access Idents ;
      bind  -> Access Exps ;
      extends  -> Access Exps ;
      allocatableDecl  -> BasicDecls ;
      optionalDecl  -> Idents ;
      pointerDecl  -> BasicDecls ;
      pointerPairs  -> PointerPair * ... ;
      PointerPair  ::= pointerPair ;
      pointerPair  -> Ident BasicDecl ;
      targetDecl  -> BasicDecls ;
      automaticDecl  -> Idents ;
      staticDecl  -> Idents ;
      volatileDecl  -> Idents ;
      contiguousDecl  -> Idents ;
      structureDecl  -> Ident FieldDecls ;
      records  -> Record * ... ;
      Record  ::= record ;
      record  -> Ident BasicDecls ;
      dictionaryDecl  -> GString ;
      extBlocks  -> Common * ... ;
      extBases  -> ExtBase * ... ;
      ExtBase  ::= extBase ;
      extBase  -> Ident Exp ;
      extDummyDecl  -> Idents ;
      formatDecl  -> FormatElems ;
      Label  ::= label ;
      FormatElem  ::= formatElem GString ;
      formatElem  -> implemented as STRING ;
      entryDecl  -> Ident Params ;
      directiveDecl  -> Ident DirArgs ;
      flcdirectiveDecl  -> Ident DirArgs ;

      DoStat ::= do ;
      ompParallelDo -> OmpClauses DoStat ;
      ompDo -> OmpClauses DoStat ;
      ompParallel -> OmpClauses Stats;

      OmpClauses ::= ompClauses ;
      ompClauses -> OmpClause + ... ;
      OmpClause ::= ompShared ompPrivate ompReduction ;
      ompNumThreads -> Exp ;
      ompSchedule -> Exps ;
      ompShared -> OmpClauseArgs ;
      ompPrivate -> OmpClauseArgs ;
      ompFirstPrivate -> OmpClauseArgs ;
      ompLastPrivate -> OmpClauseArgs ;
      ompReduction -> Ident OmpClauseArgs ;
      ompDefault -> Ident ;
      OmpClauseArgs ::= ompClauseArgs ;
      ompClauseArgs -> Ident + ... ;

      DirArg  ::= eqName EqNameExp ;
      eqName  -> Ident EqNameExp ;
      EqNameExp  ::= Exp exps star ;
      namelists  -> Namelist * ... ;
      Namelist  ::= namelist ;
      namelist  -> Ident Idents ;
      datas  -> Data * ... ;
      Data  ::= data ;
      data  -> Datavars Values ;
      Datavar  ::= VariableRef impldo ;
      impldo  -> Datavars NameRange ;
      Value  ::= Exp repeat_factor ;
      repeat_factor  -> SimpleExp Exp ;
      SimpleExp  ::= Ident SimpleCst ;
      includeDecl  -> GString ;
      pppLine -> Exp * ... ;
      varDecl  -> Type FullDecls ;
      TypeSpecModifier  ::=
         isDimension isParameter isSave isData isIntent isAllocatable isExternal
         isIntrinsic isOptional isPointer isTarget isPublic isPrivate isValue 
         isVolatile isContiguous isPass isNopass isDeferred ;
      isDimension  -> Dims ;
      isParameter  -> ;
      isSave  -> ;
      isData  -> ;
      isIntent  -> IntentValue ;
      isAllocatable  -> ;
      isExternal  -> ;
      isIntrinsic  -> ;
      isOptional  -> ;
      isPointer  -> ;
      isTarget  -> ;
      isValue -> ;
      isVolatile -> ;
      isContiguous -> ;
      isPass -> ;
      isNopass -> ;
      isDeferred -> ;
      stmtFunc  -> VariableRef Exp ;
      hpfProcessors  -> BasicDecls ;
      hpfAlign  -> Ident Dims RefClause ;
      hpfDistribute  -> Ident FormatClause RefClause ;
      hpfDynamic  -> Idents ;
      hpfInherit  -> Idents ;
      hpfTemplate  -> BasicDecls ;
      hpfCombined  -> HPFAttributes FullDecls ;
      hpfSequence  -> Idents ;
      hpfNoSequence  -> Idents ;
      RefClause  ::= refClause refStarClause ;
      refClause  -> VariableRef ;
      refStarClause  -> VariableRef ;
      FormatClause  ::= formatClause formatStarClause ;
      formatClause  -> DistFormats ;
      formatStarClause  -> OptDistFormats ;
      DistFormat  ::= blockFormat cyclicFormat starFormat ;
      blockFormat  -> OptExp ;
      cyclicFormat  -> OptExp ;
      starFormat  -> ;
      HPFAttribute  ::=
         hpfProcessorsAttribute hpfAlignAttribute hpfDistributeAttribute
         hpfDynamicAttribute hpfInheritAttribute hpfTemplateAttribute
         hpfDimensionAttribute ;
      hpfProcessorsAttribute  -> ;
      hpfAlignAttribute  -> OptDims RefClause ;
      hpfDistributeAttribute  -> OptFormatClause OptRefClause ;
      hpfDynamicAttribute  -> ;
      hpfInheritAttribute  -> ;
      hpfTemplateAttribute  -> ;
      hpfDimensionAttribute  -> Dims ;
      Stat  ::= labstat Inst SimpleStat StructStat Trailer ;
      labstat  -> Label StatInLabel ;
      StatInLabel  ::= SimpleStat StructStat Trailer formatDecl ;
      SimpleStat  ::=
         assign assignto continue call arith_if return stop pause exit enddo
         cycle allocate deallocate nullify pointer_assign where forall inline
         leave goto assigned_goto comp_goto log_if hpfIndependent hpfRealign
         hpfRedistribute open close flush inquire backspace endFile rewind
         print read
         write accept encode decode delete find rewrite unlock backFile skipFile
         defineFile ;
      StructStat  ::= struct_if struct_where struct_forall select_case select_type do ompParallelDo ompDo ompParallel ;
      assign  -> VariableRef ExpOrAssign ;
      pointer_assign  -> VariableRef ExpOrAssign ;
      ExpOrAssign  ::= Exp assign pointer_assign;
      assignto  -> Label Ident ;
      continue  -> ;
      call  -> VariableRef Exps ;
      arith_if  -> Exp OptLabel OptLabel OptLabel ;
      return  -> OptExp ;
      stop  -> OptExp ;
      pause  -> OptExp ;
      exit  -> OptIdent ;
      enddo  -> OptIdent ;
      cycle  -> OptIdent ;
      allocate  -> VariableRefs KeywordArg ;
      deallocate  -> VariableRefs KeywordArg ;
      KeywordArg  ::= eqName star none ;
      nullify  -> VariableRefs ;
      where  -> Exp SimpleStat ;
      forall  -> NameRanges OptExp SimpleStat ;
      inline  -> ;
      leave  -> OptLabel ;
      goto  -> Label ;
      assigned_goto  -> Label Labels ;
      comp_goto  -> Labels Exp ;
      log_if  -> Exp SimpleStat ;
      hpfIndependent  -> OptVariableRefs ;
      hpfRealign  -> IdentOrIdents OptDims VariableRef ;
      hpfRedistribute  -> IdentOrIdents OptDistFormats OptIdent ;
      IdentOrIdents  ::= Ident Idents ;
      open  -> IOSpecs ;
      close  -> IOSpecs ;
      flush  -> IOSpecs ;
      inquire  -> IOSpecs Exps ;
      backspace  -> IOSpecs ;
      endFile  -> IOSpecs ;
      rewind  -> IOSpecs ;
      print  -> OptFormatId Exps ;
      FormatId  ::= Exp Label ;
      read  -> ReadSpec Exps ;
      ReadSpec  ::= IOSpecs FormatId ;
      write  -> IOSpecs Exps ;
      accept  -> FormatId Exps ;
      encode  -> IOSpecs Exps ;
      decode  -> IOSpecs Exps ;
      delete  -> IOSpecs ;
      find  -> IOSpecs ;
      rewrite  -> IOSpecs Exps ;
      unlock  -> IOSpecs ;
      backFile  -> IOSpecs ;
      skipFile  -> IOSpecs ;
      defineFile  -> DefineSpecs ;
      DefineSpec  ::= defineSpec ;
      defineSpec  -> IdentOrCst DefineParams ;
      IdentOrCst  ::= Ident SimpleCst ;
      DefineParams  ::= defineParams ;
      defineParams  -> Exp Exp OptExp VariableRef ;
      IOSpec  ::= Exp eqName unit_record ;
      unit_record  -> IdentOrCst Exp ;
      struct_if  -> OptIdent Exp Stats StatsOrElseif ;
      struct_where  -> OptIdent Exp Stats StatsOrElsewhere ;
      struct_forall  -> OptIdent NameRanges Exp Stats ;
      select_case  -> OptIdent Exp Cases OptIdent ;
      select_type  -> OptIdent ExpOrAssign Cases OptIdent ;
      do  -> OptIdent OptLabel DoControl Stats ;
      NameRange  ::= nameRange ;
      nameRange  -> Ident Range ;
      Range  ::= range ;
      range  -> OptExp OptExp OptExp ;
      StatsOrElseif  ::= Stats struct_if labstat ;
      StatsOrElsewhere  ::= Stats struct_where labstat ;
      Case  ::= case typeCase classCase ;
      case  -> Selector OptIdent Stats ;
      Selector  ::= exps default ;
      default  -> ;
      typeCase -> TypeSelector OptIdent Stats ;
      classCase -> TypeSelector OptIdent Stats ;
      TypeSelector ::= Type default ;
      DoControl  ::= nameRange while until times none ;
      while  -> Exp ;
      until  -> Exp ;
      times  -> Exp ;
      Exp  ::=
         label eqName star complexConstructor SimpleCst VariableRef keySpecs
         Oper nvalues multiCast constructor range left_range right_range left_right_range
         labelArg none ;
      SimpleCst  ::= NkCst kindCst ;
      NkCst  ::= logicCst intCst realCst doubleCst bitCst StringCst ;
      StringCst  ::= hollerith strings ;
      Oper  ::=
         add sub mul div power plus minus concat not and or xor eqv neqv eq ge
         gt le lt ne binaryDefOp unaryDefOp parenth ;
      VariableRef  ::= Ident vardim fieldRef ;
      label  -> implemented as STRING ;
      labelArg  -> Label ;
      star  -> ;
      complexConstructor  -> Exp Exp ;
      logicCst  -> implemented as STRING ;
      intCst  -> implemented as STRING ;
      realCst  -> implemented as STRING ;
      doubleCst  -> implemented as STRING ;
      bitCst  -> implemented as STRING ;
      kindCst  -> NkCst TypeRef ;
      TypeRef  ::= ident intCst ;
      hollerith  -> implemented as STRING ;
      GString  ::= hollerith strings ;
      strings  -> String * ... ;
      String  ::= string ;
      string  -> implemented as STRING ;
      fieldRef  -> VariableRef Ident ;
      vardim  -> Ident Exps ;
      Ident  ::= ident ;
      ident  -> implemented as IDENTIFIER ;
      blockName  -> implemented as IDENTIFIER ;
      keySpecs  -> Exp * ... ;
      add  -> Exp Exp ;
      sub  -> Exp Exp ;
      mul  -> Exp Exp ;
      div  -> Exp Exp ;
      power  -> Exp Exp ;
      plus  -> Exp ;
      minus  -> Exp ;
      concat  -> Exp Exp ;
      not  -> Exp ;
      and  -> Exp Exp ;
      or  -> Exp Exp ;
      xor  -> Exp Exp ;
      eqv  -> Exp Exp ;
      neqv  -> Exp Exp ;
      eq  -> Exp Exp ;
      ge  -> Exp Exp ;
      gt  -> Exp Exp ;
      le  -> Exp Exp ;
      lt  -> Exp Exp ;
      ne  -> Exp Exp ;
      unaryDefOp  -> DefinedOp Exp ;
      binaryDefOp  -> DefinedOp Exp Exp ;
      DefinedOp  ::= definedOp ;
      definedOp  -> implemented as IDENTIFIER ;
      parenth  -> Exp ;
      nvalues  -> IntCst Exp ;
      IntCst  ::= intCst ;
      constructor  -> Datavars ;
      left_range  -> Exp ;
      right_range  -> Exp ;
      left_right_range  -> Exp ;
      Type  ::= sizedType modifiedType derived class procedure GroundType ;
      sizedType  -> GroundType Exp ;
      modifiedType  -> Type TypeSpecModifiers ;
      GroundType  ::=
         character integer real double complex dblcomplex logical byte bit void ;
      derived  -> Ident ;
      class  -> Ident ;
      procedure  -> OptIdent ;
      character  -> ;
      integer  -> ;
      real  -> ;
      double  -> ;
      complex  -> ;
      dblcomplex  -> ;
      logical  -> ;
      byte  -> ;
      bit  -> ;
      void  -> ;
      isAbstract -> ;
      none  -> ;
      OptExp  ::= Exp ;
      -- "LIST" PHYLA AND OPERATORS (SYSTEMATIC)
      Units  ::= units ;
      units  -> Unit * ... ;
      Options  ::= options ;
      options  -> Option * ... ;
      Params  ::= params ;
      params  -> Param * ... ;
      Prefixs  ::= prefixs ;
      prefixs  -> Prefix * ... ;
      Decls  ::= decls ;
      decls  -> Decl * ... ;
      Letterss  ::= letterss ;
      letterss  -> Letters * ... ;
      FullDecls  ::= fullDecls ;
      fullDecls  -> FullDecl * ... ;
      Dims  ::= dims ;
      dims  -> Dim * ... ;
      VariableRefs  ::= variableRefs ;
      variableRefs  -> VariableRef * ... ;
      BasicDecls  ::= basicDecls ;
      basicDecls  -> BasicDecl * ... ;
      PrivateSequences  ::= privateSequences ;
      privateSequences  -> PrivateSequence * ... ;
      FieldDecls  ::= fieldDecls ;
      fieldDecls  -> FieldDecl * ... ;
      MapDecls  ::= mapDecls ;
      mapDecls  -> MapDecl * ... ;
      Interfaces  ::= interfaces ;
      interfaces  -> Interface * ... ;
      FormatElems  ::= formatElems ;
      formatElems  -> FormatElem * ... ;
      DirArgs  ::= dirArgs ;
      dirArgs  -> DirArg * ... ;
      Datavars  ::= datavars ;
      multiCast -> Type Constructor ;
      Constructor ::= constructor ;
      datavars  -> Datavar * ... ;
      Values  ::= values ;
      values  -> Value * ... ;
      TypeSpecModifiers  ::= typeSpecModifiers ;
      typeSpecModifiers  -> TypeSpecModifier * ... ;
      DistFormats  ::= distFormats ;
      distFormats  -> DistFormat * ... ;
      HPFAttributes  ::= hpfAttributes ;
      hpfAttributes  -> HPFAttribute * ... ;
      Stats  ::= stats ;
      stats  -> Stat * ... ;
      NameRanges  ::= nameRanges ;
      nameRanges  -> NameRange * ... ;
      Cases  ::= cases ;
      cases  -> Case * ... ;
      Exps  ::= exps ;
      exps  -> Exp * ... ;
      DefineSpecs  ::= defineSpecs ;
      defineSpecs  -> DefineSpec * ... ;
      IOSpecs  ::= ioSpecs ;
      ioSpecs  -> IOSpec * ... ;
      Labels  ::= labels ;
      labels  -> Label * ... ;
      -- "OPTIONAL" PHYLA (SYSTEMATIC)
      OptHeader  ::= Header none ;
      OptTrailer  ::= Trailer none ;
      OptIdent  ::= Ident none ;
      OptBind  ::= bind none ;
      OptAccess  ::= Access none ;
      OptIdentObject  ::= IdentObject none isAbstract ;
      OptLabel  ::= Label none ;
      OptFormatId  ::= FormatId none ;
      OptInitialVal  ::= InitialVal none ;
      OptDims  ::= Dims none ;
      OptFormatClause  ::= FormatClause none ;
      OptRefClause  ::= RefClause none ;
      OptVariableRefs  ::= VariableRefs none ;
      OptDistFormats  ::= DistFormats none ;
      OptType  ::= Type none ;
end definition
