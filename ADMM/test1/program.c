#include <stdlib.h>
#include <stdio.h>

void head(double x, double* y) {
  int n = 10 ;
  int offset = 3 ;
  int i ;
  double *A, *p ;
  double z ;
  A = malloc(n*8) ;
  for(i=0 ; i<10 ; ++i) A[i] = i*x ;
  p = A+2 ;
  z = (*p)*x ;
  p = p+offset ;
  z += (*p)*x ;
  free(A) ;
  *y = z ;
}

int main(int argc, char** argv) {
  double x,y ;
  x = 3.5 ;
  printf("input x is set to [3.500000==] %f \n", x);
  head(x,&y);
  printf("result y gets val [85.75000==] %f \n", y);
  return 0;
}
