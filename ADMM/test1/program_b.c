#include <stdlib.h>
#include <stdio.h>

#include "admm/admm.h"
#include "ADFirstAidKit/adBuffer.h"

void head_b(double x, double* xb, double* y, double* yb) {
  int n = 10 ;
  int offset = 3 ;
  int i ;
  double *A, *p ;
  double *Ab, *pb ;
  double z ;
  double zb ;
  A = FW_ADMM_Malloc(n*8) ;
  for(i=0 ; i<10 ; ++i) A[i] = i*x ;
  p = A+2 ;
  z = (*p)*x ;
  pushpointer8(p) ;
  p = p+offset ;
  z += (*p)*x ;
  FW_ADMM_Free(A,1) ;
  *y = z ;
  /* ===== turn point ===== */
  ADMM_Reoffset(&p,&pb) ;
  zb = *yb ;
  *yb = 0.0 ;
  BW_ADMM_Free(&A,&Ab,1) ;
  for(i=0 ; i<10 ; ++i) Ab[i] = 0.0 ;
  (*xb) += (*p)*zb ;
  (*pb) += x*zb ;
  poppointer8(&p) ;
  ADMM_Reoffset(&p,&pb) ;
  (*xb) += (*p)*zb ;
  (*pb) += x*zb ;
  for(i=9 ; i>=0 ; --i) {
    (*xb) += i*Ab[i] ;
    Ab[i] = 0.0 ;
  }
  BW_ADMM_Malloc(A,Ab) ;
}

int main(int argc, char** argv) {
  double x,y ;
  double xb,yb ;
  x = 3.5 ;
  yb = 1.0 ;
  xb = 0.0 ;
  printf("input x is set to [3.500000==] %f \n", x);
  printf("diff yb is set to [1.000000==] %f \n", yb);
  head_b(x,&xb,&y,&yb);
  printf("result y gets val [85.75000==] %f \n", y);
  printf("diff xb gets val  [49.00000==] %f \n", xb);
  return 0;
}
