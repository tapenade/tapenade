/** Re-base waiting pointers *pp and *ppb
 * from their old base from the forward sweep
 * to their new base in the backward sweep.
 * When new base not available yet, schedules
 * this to be done when new base is allocated. */
void ADMM_Reoffset(void **pp, void **ppb);

/** Forward sweep variant of standard malloc().
 * Keeps track of base and size of the allocated chunk */
void* FW_ADMM_Malloc(int size);

/** Backward sweep variant of standard malloc().
 * Frees the chunk based at newbase, as well as its adjoint if present. */
void BW_ADMM_Malloc(void *newbase, void *newadjbase);

/** Forward sweep variant of standard free().
 * Pushes values from chunk before it is freed, if TBR.
 * Pushes base and size. */ 
void FW_ADMM_Free(void *base, int tbr);

/** Backward sweep variant of standard free().
 * Pops old base and size.
 * Re-allocates a chunk, and an adjoint chunk if present.
 * Pops values into chunk, if TBR.
 * Remembers correspondence from old base to
 * new base and new adjoint base.
 * Re-base waiting pointers. */
void BW_ADMM_Free(void **chunk, void **chunkb, int tbr);
