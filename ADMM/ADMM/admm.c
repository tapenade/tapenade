#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "admm.h"
#include "ADFirstAidKit/adBuffer.h"
#include "ADFirstAidKit/adStack.h"

/** Cell of a chained list of void* */
typedef struct _ADMM_Cell {
  void* head ;
  struct _ADMM_Cell* tail ;
} ADMM_Cell ;

typedef struct {
  void* base ;
  int   size ;
  void* newbase ;
  void* newadjbase ;
} ADMM_ChunkInfo ;

typedef struct {
  void** pp ;
  void** ppb ;
} ADMM_PairOfToPointers ;

/** The first cell of the list of ChunkInfo's. It is just a hat */
ADMM_Cell firstChunkInfoCell = {NULL, NULL} ;
/** The current list of ChunkInfo's. */
ADMM_Cell* chunkInfoList = &firstChunkInfoCell ;

/** The first cell of the list of waiting pointers. It is just a hat */
ADMM_Cell firstWaitingRebaseCell = {NULL, NULL} ;
/** The current list of pointers waiting to be rebased */
ADMM_Cell* waitingRebaseList = &firstWaitingRebaseCell ;

/** TEMPORARY: FOR DEBUGGING. */
void dumpWaiting() ;

/** Registers into the chunkInfoList the data about a new chunk */
void ADMM_RegisterNewChunkInfo(void *base, int size, void* newbase, void* newadjbase) {
  ADMM_Cell* inChunkInfos = chunkInfoList ;
  ADMM_Cell* newCell = (ADMM_Cell*)malloc(sizeof(ADMM_Cell)) ;
  ADMM_ChunkInfo* newChunkInfo = (ADMM_ChunkInfo*)malloc(sizeof(ADMM_ChunkInfo)) ;
  newChunkInfo->base = base ;
  newChunkInfo->size = size ;
  newChunkInfo->newbase = newbase ;
  newChunkInfo->newadjbase = newadjbase ;
  newCell->head = (void*)newChunkInfo ;
  // Find the location to insert to, to keep chunkInfoList ordered by increasing base's:
  while (inChunkInfos->tail && ((ADMM_ChunkInfo*)inChunkInfos->tail->head)->base<base)
    inChunkInfos = inChunkInfos->tail ;
  newCell->tail = inChunkInfos->tail ;
  inChunkInfos->tail = newCell ;
}

/** Frees the next Cell and its attached ChunkInfo */
void ADMM_FreeNextChunkInfo(ADMM_Cell *inChunkInfos) {
  free(inChunkInfos->tail->head) ;
  ADMM_Cell *newTail = inChunkInfos->tail->tail ;
  free(inChunkInfos->tail) ;
  inChunkInfos->tail = newTail ;
}

/** Registers the given pointer,adjoint-pointer pair as waiting for rebase. */
void ADMM_RegisterNewWaitingRebase(void** pp, void** ppb) {
  ADMM_Cell* inWaitingRebases = waitingRebaseList ;
  ADMM_Cell* newCell = (ADMM_Cell*)malloc(sizeof(ADMM_Cell)) ;
  ADMM_PairOfToPointers* newPair = (ADMM_PairOfToPointers*)malloc(sizeof(ADMM_PairOfToPointers));
  newPair->pp = pp ;
  newPair->ppb = ppb ;
  newCell->head = (void*)newPair ;
  // Find the location to insert to, to keep waitingRebaseList ordered by increasing pp's:
  while (inWaitingRebases->tail && ((ADMM_PairOfToPointers*)inWaitingRebases->tail->head)->pp<pp)
    inWaitingRebases = inWaitingRebases->tail ;
  newCell->tail = inWaitingRebases->tail ;
  inWaitingRebases->tail = newCell ;
  printf("    After ADMM_RegisterNewWaitingRebase: ") ;
  dumpWaiting() ;
}

/** Frees the next Cell and its attached pair of pointers. */
void ADMM_FreeNextWaitingRebase(ADMM_Cell *inWaitingRebases) {
  free(inWaitingRebases->tail->head) ;
  ADMM_Cell *newTail = inWaitingRebases->tail->tail ;
  free(inWaitingRebases->tail) ;
  inWaitingRebases->tail = newTail ;
  printf("    After ADMM_FreeNextWaitingRebase: ") ;
  dumpWaiting();
}

/** Rebase the given pointer (and adjoint-pointer if present) coherently
 * with the base changed to newbase and its adjoint to newadjbase. */
void ADMM_RebaseOne(void **pp, void **ppb,
                    void* base, void* newbase, void* newadjbase) {
  printf("    now rebasing %x(%x) observing %x,%x,%x",*pp,*ppb,base,newbase,newadjbase);
  if (newbase) {
    *pp += (newbase - base) ;
    if (ppb && newadjbase)
      *ppb = *pp + (newadjbase - newbase) ;
  }
  printf(" gives %x(%x) \n",*pp,*ppb) ;
}





/** Re-base waiting pointers *pp and *ppb
 * from their old base from the forward sweep
 * to their new base in the backward sweep.
 * When new base not available yet, schedules
 * this to be done when new base is allocated. */
void ADMM_Reoffset(void **pp, void **ppb) {
  // [Pb:] possible bug: the same pointer might be rebased twice if aliased!
  // Possible solution is to keep track of pointers already rebased.
  ADMM_ChunkInfo* chunkInfo ;
  ADMM_Cell* inChunkInfos = chunkInfoList->tail ;
  while (inChunkInfos) {
    chunkInfo = (ADMM_ChunkInfo*)inChunkInfos->head ;
    if (*pp < (chunkInfo->base+chunkInfo->size)) {
      if (*pp >= chunkInfo->base) {
        ADMM_RebaseOne(pp, ppb,
                       chunkInfo->base, chunkInfo->newbase, chunkInfo->newadjbase) ;
        pp = NULL ;
      }
      break ;
    }
    inChunkInfos = inChunkInfos->tail ;
  }
  // if the containing chunk is not found, it may appear later:
  if (pp) ADMM_RegisterNewWaitingRebase(pp, ppb) ;
}

/** Forward sweep variant of standard malloc().
 * Keeps track of base and size of the allocated chunk */
void* FW_ADMM_Malloc(int size) {
  void *chunk = malloc(size) ;
  ADMM_RegisterNewChunkInfo(chunk, size, NULL, NULL) ;
  printf("Registered allocated address %x size %i\n",chunk,size) ;
  return chunk ;
}

/** Backward sweep variant of standard malloc().
 * Frees the chunk based at newbase, as well as its adjoint if present. */
void BW_ADMM_Malloc(void *newbase, void *newadjbase) {
  ADMM_ChunkInfo* chunkInfo ;
  ADMM_Cell* inChunkInfos = chunkInfoList ;
  int found = 0 ;
  while (!found && inChunkInfos->tail) {
    chunkInfo = (ADMM_ChunkInfo*)inChunkInfos->tail->head ;
    found = ((newbase && newbase==chunkInfo->newbase) ||
             (newadjbase && newadjbase==chunkInfo->newadjbase)) ;
    if (!found) inChunkInfos = inChunkInfos->tail ;
  }
  if (!found) {printf("Chunk not found in BW_ADMM_Malloc!\n"); exit(0);}
  printf("de-Register again allocated addresses %x(%x) size %i\n",newbase,newadjbase,chunkInfo->size) ;
  ADMM_FreeNextChunkInfo(inChunkInfos) ;
  if (newbase) free(newbase) ;
  if (newadjbase) free(newadjbase) ;
}

/** Forward sweep variant of standard free().
 * Pushes values from chunk before it is freed, if TBR.
 * Pushes base and size. */ 
void FW_ADMM_Free(void *base, int tbr) {
  ADMM_ChunkInfo* chunkInfo ;
  ADMM_Cell* inChunkInfos = chunkInfoList ;
  while (inChunkInfos->tail) {
    chunkInfo = (ADMM_ChunkInfo*)inChunkInfos->tail->head ;
    if (chunkInfo->base >= base) {
      if (chunkInfo->base!=base) inChunkInfos = NULL ;
      break ;
    }
    inChunkInfos = inChunkInfos->tail ;
  }
  if (!inChunkInfos) {printf("Chunk not found in FW_ADMM_Free!\n"); exit(0);}
  if (tbr) pushNarray(base, (unsigned int)chunkInfo->size) ;
  printf("de-Register and push allocated address %x size %i\n",chunkInfo->base,chunkInfo->size) ;
  pushinteger4(chunkInfo->size) ;
  pushpointer8((char*)base) ;
  free(base) ;
  ADMM_FreeNextChunkInfo(inChunkInfos) ;
}

/** Backward sweep variant of standard free().
 * Pops old base and size.
 * Re-allocates a chunk, and an adjoint chunk if present.
 * Pops values into chunk, if TBR.
 * Remembers correspondence from old base to
 * new base and new adjoint base.
 * Re-base waiting pointers. */
void BW_ADMM_Free(void **chunk, void **chunkb, int tbr) {
  void* oldBase ;
  int size ;
  poppointer8((char**)&oldBase) ;
  popinteger4(&size) ;
  if (chunkb) {
    *chunkb = malloc(size) ;
    // [Pb:] here we should initialize to zero the derivative *chunkb, but how?
    // int i ;
    // for (i=0 ; i<size ; ++i) (*chunkb)(i) = 0.0 ;
  }
  if (chunk) {
    *chunk = malloc(size) ;
    if (tbr) popNarray(*chunk, (unsigned int)size) ;
    // [Pb:] here if the popped *chunk contains addresses, they should be rebased too!
  }
  // [Pb:] bizarre that the behavior seems the same if chunkb==NULL and if *chunk==NULL ?
  ADMM_RegisterNewChunkInfo(oldBase, size, (chunk?*chunk:0), (chunkb?*chunkb:0)) ;
  printf("Re-Registered allocated addresses %x(%x) for old base %x size %i\n",(chunk?*chunk:0),(chunkb?*chunkb:0),oldBase,size) ;
  // now try to re-base waiting pointers:
  ADMM_PairOfToPointers* waitingRebase ;
  ADMM_Cell* inWaitingRebases = waitingRebaseList ;
  while (inWaitingRebases->tail) {
    waitingRebase = (ADMM_PairOfToPointers*)inWaitingRebases->tail->head ;
    if (*(waitingRebase->pp)>=(oldBase+size))
      break ; //because inWaitingRebases is sorted, no more rebasing is possible.
    else if (*(waitingRebase->pp)>=oldBase) {
      // rebase and then remove from waitingRebaseList:
      ADMM_RebaseOne(waitingRebase->pp, waitingRebase->ppb,
                     oldBase, (chunk?*chunk:0), (chunkb?*chunkb:0)) ;
      ADMM_FreeNextWaitingRebase(inWaitingRebases) ;
    } else {
      // keep in waitingRebaseList for later rebase:
      inWaitingRebases = inWaitingRebases->tail ;
    }
  }
}





/** TEMPORARY: FOR DEBUGGING. */
void dumpWaiting() {
  ADMM_PairOfToPointers* waitingRebase ;
  ADMM_Cell* inWaitingRebases = waitingRebaseList ;
  printf(" waiting :") ;
  while (inWaitingRebases) {
    waitingRebase = (ADMM_PairOfToPointers*)inWaitingRebases->head ;
    if (waitingRebase)
      printf("(%x;%x)->",
             (waitingRebase->pp?*(waitingRebase->pp):0),
             (waitingRebase->ppb?*(waitingRebase->ppb):0)) ;
    else
      printf("(Null2)->") ;
    inWaitingRebases = inWaitingRebases->tail ;
  }
  printf("null\n") ;
}
