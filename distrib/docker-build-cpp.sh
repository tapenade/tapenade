#!/usr/bin/env bash

if [ "$(uname)" = "Linux" ]; then
  TAPENADE_HOME="$(dirname -- "$(readlink -f -- "$0")")"/..
else
  REFDIR=$(cd "$(dirname "$0")" && pwd)
  TAPENADE_HOME=$REFDIR/..
fi

version="$(
  cd ../
  ./gradlew -q version
)"
tapenadeversion=_"$version"
installDir=$PWD/distrib/tapenade"$tapenadeversion"

./copy2distrib.sh

cd "$TAPENADE_HOME"/
./gradlew frontcppdocker

cp "$TAPENADE_HOME"/bin/linux/cppParser "$installDir"/bin/linux/

cd "$TAPENADE_HOME"/distrib
docker build -f Dockerfile-cpp -t registry.gitlab.inria.fr/tapenade/tapenade:latest -t registry.gitlab.inria.fr/tapenade/tapenade:cpp-$(cat ../build/resources/main/gittag) .

docker container run --rm -v ${PWD}:/tapdir registry.gitlab.inria.fr/tapenade/tapenade -version

if [ "$(uname)" = "Linux" ]; then
  docker push registry.gitlab.inria.fr/tapenade/tapenade
else
  echo "docker push Time out on macos"
fi
