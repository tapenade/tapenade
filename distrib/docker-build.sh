#!/usr/bin/env bash

./copy2distrib.sh

docker build -t registry.gitlab.inria.fr/tapenade/tapenade:latest -t registry.gitlab.inria.fr/tapenade/tapenade:$(cat ../build/resources/main/gittag) .

docker container run --rm -v ${PWD}:/tapdir registry.gitlab.inria.fr/tapenade/tapenade -version

if [ "$(uname)" = "Linux" ]; then
  docker push registry.gitlab.inria.fr/tapenade/tapenade:latest
  docker push registry.gitlab.inria.fr/tapenade/tapenade:$(cat ../build/resources/main/gittag)
else
  echo "docker push Time out on macos"
fi
