How to build Tapenade
=====================

Creating a new tapenade sources directory
-----------------------------------------

`git clone git@gitlab.inria.fr:tapenade/tapenade.git`

Building Tapenade from sources in tapenade directory
----------------------------------------------------

`cd tapenade`

`./gradlew`

Building a tar distribution
---------------------------

See executable and upload jobs in `.gitlab-ci.yml`

Building a docker image
-----------------------

See dockerimage job in `.gitlab-ci.yml`

