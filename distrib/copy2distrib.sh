#!/usr/bin/env bash

if [ "$(uname)" = "Linux" ]; then
  TAPENADE_HOME="$(dirname -- "$(readlink -f -- "$0")")"/..
else
  REFDIR=$(cd "$(dirname "$0")" && pwd)
  TAPENADE_HOME=$REFDIR/..
fi

if [ -n "$1" ]; then
  destination=$1 ### absolute pathname !!!
else
  destination=$PWD/distrib
fi

version="$(
  cd ../
  ./gradlew -q version
)"

tapenadeversion=_"$version"

installDir="$destination"/tapenade"$tapenadeversion"

# linux mac et windows
SYST1=linux
SYST2=mac
SYST3=windows

mkdir -p "$installDir"
mkdir -p "$installDir"/bin
mkdir -p "$installDir"/bin/$SYST1
mkdir -p "$installDir"/bin/$SYST2
mkdir -p "$installDir"/bin/$SYST3
mkdir -p "$installDir"/build/libs
mkdir -p "$installDir"/resources/lib

cd "$TAPENADE_HOME"/ || exit

./gradlew -q clean
./gradlew -q cleanfront
./gradlew -q >/tmp/taptrace 2>&1
grep warnings /tmp/taptrace
/bin/rm /tmp/taptrace

cp "$TAPENADE_HOME"/build/libs/tapenade-"$version".jar "$installDir"/build/libs/tapenade-"$version".jar
# cp "$TAPENADE_HOME"/build/libs/log4j*.jar "$installDir"/build/libs
cp "$TAPENADE_HOME"/build/libs/frontc.jar "$installDir"/build/libs
cp "$TAPENADE_HOME"/resources/lib/*ib "$installDir"/resources/lib/
cp "$TAPENADE_HOME"/resources/lib/*.f90 "$installDir"/resources/lib/

cp "$TAPENADE_HOME"/build/libs/tapenadehtml.jar "$installDir"/build/libs
cp "$TAPENADE_HOME"/bin/tapenade "$installDir"/bin/
cp "$TAPENADE_HOME"/bin/tapenadocker "$installDir"/bin/

cp "$TAPENADE_HOME"/bin/tapenade.bat "$installDir"/bin/
chmod +x "$installDir"/bin/tapenade.bat

cp "$TAPENADE_HOME"/bin/*ParserRun.sh "$installDir"/bin

if [ -x "$(command -v docker)" ]; then
  ./gradlew -q cleanfront
  ./gradlew -q frontfdocker
  cp "$TAPENADE_HOME"/bin/linux/fortranParser "$installDir"/bin/linux/fortranParser
else
  cp "$TAPENADE_HOME"/bin/linux/fortranParser "$installDir"/bin/$SYST1/fortranParser
  cp "$TAPENADE_HOME"/bin/$SYST2/fortranParser "$installDir"/bin/$SYST2/fortranParser
  ./gradlew -q cleanfront
  ./gradlew -q -Psystem=windows >/tmp/taptrace 2>&1
  cp "$TAPENADE_HOME"/bin/$SYST3/fortranParser.exe "$installDir"/bin/$SYST3/fortranParser.exe

fi

cp -R "$TAPENADE_HOME"/ADFirstAidKit/ "$installDir"/ADFirstAidKit

pandoc "$TAPENADE_HOME"/distrib/README-install.md -o /tmp/README.html
sed -f "$TAPENADE_HOME"/distrib/script-README /tmp/README.html >"$installDir"/README.html

pandoc "$TAPENADE_HOME"/LICENSE.md -o "$installDir"/LICENSE.html

cp "$TAPENADE_HOME"/bin/tap* "$installDir"/bin/

echo ""

"$destination"/tapenade"$tapenadeversion"/bin/tapenade -version
