How to install Tapenade
=======================

### Linux or Mac OS X

Before installing Tapenade, you must check that an up-to-date Java
Runtime Environment is installed. Tapenade will not run with older Java
Runtime Environment.

-   Read [the Tapenade license](LICENSE.html)
-   Download [tapenade_3.16.tar](tapenade_3.16.tar) into your chosen
    installation directory *\"install\_dir\"*
-   Go to your chosen installation directory *\"install\_dir\"*, and
    extract Tapenade from the tar file :\
    `tar xvfz tapenade_3.16.tar`
-   On Linux, depending on your distribution, Tapenade may require you
    to set the shell variable JAVA\_HOME to your java installation
    directory. It is often `JAVA_HOME=/usr/java/default`.

For more information on the `tapenade` command and its arguments, type
:\
`tapenade -?`

### Windows Platform

Before installing Tapenade, you must check that an up-to-date Java
Runtime Environment is installed.
Tapenade will not run with older Java Runtime Environment.  
The Fortran parser of Tapenade uses [cygwin](https://www.cygwin.com).

-   Read [the Tapenade license](LICENSE.html)
-   Download [tapenade_3.16.zip](tapenade_3.16.zip) into your chosen
    installation directory *\"install\_dir\"*
-   Go to your chosen installation directory *\"install\_dir\"*, and
    extract Tapenade from the zip file.
-   Save a copy of the
    *\"install\_dir\"*`\tapenade_3.16\bin\tapenade.bat` file and modify
    *\"install\_dir\"*`\tapenade_3.16\bin\tapenade.bat` according to
    your installation parameters:  
    replace `TAPENADE_HOME=..` by
    `TAPENADE_HOME="install_dir"\tapenade_3.16`  
    replace `JAVA_HOME="C:\Progra~1\Java\jdkXXXX"` by your current
    java directory  
    replace
    `BROWSER="C:\Program Files\Internet Explorer\iexplore.exe"`
    by your current browser.



### Running Tapenade from a docker container

`docker container run --rm -v ${PWD}:/tapdir registry.gitlab.inria.fr/tapenade/tapenade -version`

`docker container run --rm -v ${PWD}:/tapdir -v /tmp:/tmp registry.gitlab.inria.fr/tapenade/tapenade -O /tmp program.c f.f90`

On linux, add -u option:
`-u $(stat -c "%u:%g" ./)`

To download the latest tapenade image:  
`docker pull registry.gitlab.inria.fr/tapenade/tapenade`

### Tapenade User Documentation

If you want to be kept informed about new developments and releases of
Tapenade,
[subscribe](mailto:sympa@inria.fr?subject=subscribe%20tapenade-users) to
the [tapenade-users mailing list.](mailto:tapenade-users@inria.fr)  
[Tapenade User Documentation](https://tapenade.gitlabpages.inria.fr/userdoc)
is available on [our web site](https://team.inria.fr/ecuador/en/tapenade).  
[The Tapenade Automatic Differentiation tool: principles, model, and
specification](https://hal.archives-ouvertes.fr/hal-00913983) is the
Tapenade reference article.
