! Un exemple pour tester la differentiation
! dans le cas d'arguments optionnels.
subroutine top(a,b,c,res)
  real a,b,c,res
  real, dimension(99) :: x1,x2,z1,z2,y1,y2
  x1 = a*b
  x2 = a*c
  z1 = 5.0
  z2 = a*c
  y1 = 0.0
  y2 = b*c
  call foo(x1=x1, z1=z1, y1=y1)
  call foo(x2=x2, z2=z2, y2=y2)
  res = SUM(x1)+SUM(x2)+SUM(z1)+SUM(z2)+SUM(y1)+SUM(y2)

contains


subroutine foo(x1,x2,z1,z2,y1,y2)
  real,dimension(:),intent(in),optional :: x1
  real,dimension(:),intent(in),optional :: x2
  real,dimension(:),intent(inout),optional :: z1
  real,dimension(:),intent(inout),optional :: z2
  real,dimension(:),intent(out),optional :: y1
  real,dimension(:),intent(out),optional :: y2

  real,dimension(99) :: rx1,rx2,rz1,rz2,ry1,ry2
  real,dimension(99) :: tmp1,tmp2

  rx1 = 0.0
  if (PRESENT(x1)) rx1 = x1
  rx2 = 1.0
  if (PRESENT(x2)) rx2 = x2
  rz1 = 2.0
  if (PRESENT(z1)) rz1 = z1
  rz2 = 3.0
  if (PRESENT(z2)) rz2 = z2
  tmp1 = rz1
  rz1 = rx1*rx1
  if (PRESENT(z1)) z1 = rz1
  tmp2 = rz2
  if (PRESENT(z2)) z2 = rz2
  rz2 = rz1*rx2
  ry1 = rz2*tmp1
  if (PRESENT(y1)) y1 = ry1
  ry2 = ry1*tmp2
  if (PRESENT(y2)) y2 = ry2
END subroutine foo


end subroutine top  
