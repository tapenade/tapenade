!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 15:32
!
MODULE G1_DIFF
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), POINTER :: z, cf1
  REAL(kind=8), DIMENSION(:), POINTER :: zb, cf1b
END MODULE G1_DIFF

!  Differentiation of top as a context to call adjoint code (with options context):
PROGRAM TOP_B
  USE G1_DIFF
  IMPLICIT NONE
  INTERFACE 
      SUBROUTINE FOO_NODIFF(cf2)
        REAL(kind=8), DIMENSION(:), POINTER :: cf2
      END SUBROUTINE FOO_NODIFF
  END INTERFACE

  INTERFACE 
      SUBROUTINE FOO_B(cf2, cf2b)
        REAL(kind=8), DIMENSION(:), POINTER :: cf2
        REAL(kind=8), DIMENSION(:), POINTER :: cf2b
      END SUBROUTINE FOO_B
  END INTERFACE

  ALLOCATE(cf1b(99))
  cf1b = 0.0_8
  ALLOCATE(cf1(99))
  ALLOCATE(zb(88))
  zb = 0.0_8
  ALLOCATE(z(88))
  z = 1.d0
  CALL FOO_B(cf1, cf1b)
  IF (ASSOCIATED(cf1b)) THEN
    DEALLOCATE(cf1b)
  END IF
  DEALLOCATE(cf1)
END PROGRAM TOP_B

SUBROUTINE TOP_NODIFF()
  USE G1_DIFF
  IMPLICIT NONE
  INTERFACE 
      SUBROUTINE FOO_NODIFF(cf2)
        REAL(kind=8), DIMENSION(:), POINTER :: cf2
      END SUBROUTINE FOO_NODIFF
  END INTERFACE

  ALLOCATE(cf1(99))
  ALLOCATE(z(88))
  z = 1.d0
  CALL FOO_NODIFF(cf1)
  DEALLOCATE(cf1)
END SUBROUTINE TOP_NODIFF

!  Differentiation of foo as a context to call adjoint code (with options context):
!   Plus diff mem management of: z[from module g1]:in cf2:in
SUBROUTINE FOO_B(cf2, cf2b)
  USE DIFFSIZES
!  Hint: ISIZE1OFDrfcf2 should be the size of dimension 1 of array *cf2
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), POINTER :: cf2
  REAL(kind=8), DIMENSION(:), POINTER :: cf2b
  REAL(kind=8) :: x
  REAL(kind=8) :: xb
  INTERFACE 
      SUBROUTINE BAR_NODIFF(cf3, x)
        REAL(kind=8), DIMENSION(:) :: cf3
!     REAL(KIND=8), dimension(:), pointer :: cf3
        REAL(kind=8) :: x
      END SUBROUTINE BAR_NODIFF
  END INTERFACE

  INTERFACE 
      SUBROUTINE BAR_B(cf3, cf3b, x, xb)
        REAL(kind=8), DIMENSION(:) :: cf3
        REAL(kind=8), DIMENSION(:) :: cf3b
!     REAL(KIND=8), dimension(:), pointer :: cf3
        REAL(kind=8) :: x
        REAL(kind=8) :: xb
      END SUBROUTINE BAR_B
  END INTERFACE

  x = 5
  CALL ADCONTEXTADJ_INIT(0.87_8)
  IF (ASSOCIATED(cf2)) CALL ADCONTEXTADJ_INITREAL8ARRAY('cf2'//CHAR(0), &
&                                                 cf2, cf2b, &
&                                                 ISIZE1OFDrfcf2)
  CALL ADCONTEXTADJ_INITREAL8('x'//CHAR(0), x, xb)
  CALL BAR_B(cf2, cf2b, x, xb)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  IF (ASSOCIATED(cf2)) CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('cf2'//CHAR(&
&                                                     0), cf2, cf2b, &
&                                                     ISIZE1OFDrfcf2)
  CALL ADCONTEXTADJ_CONCLUDEREAL8('x'//CHAR(0), x, xb)
  CALL ADCONTEXTADJ_CONCLUDE()
END SUBROUTINE FOO_B

SUBROUTINE FOO_NODIFF(cf2)
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), POINTER :: cf2
  REAL(kind=8) :: x
  INTERFACE 
      SUBROUTINE BAR_NODIFF(cf3, x)
        REAL(kind=8), DIMENSION(:) :: cf3
!     REAL(KIND=8), dimension(:), pointer :: cf3
        REAL(kind=8) :: x
      END SUBROUTINE BAR_NODIFF
  END INTERFACE

  x = 5
  CALL BAR_NODIFF(cf2, x)
END SUBROUTINE FOO_NODIFF

!  Differentiation of bar in reverse (adjoint) mode (with options context):
!   gradient     of useful results: [alloc*z in top] *z x cf3
!   with respect to varying inputs: [alloc*z in top] *z x cf3
!   RW status of diff variables: [alloc*z in top]:in-out *z:in-out
!                x:in-out cf3:incr
!   Plus diff mem management of: z:in
SUBROUTINE BAR_B(cf3, cf3b, x, xb)
  USE G1_DIFF
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:) :: cf3
  REAL(kind=8), DIMENSION(:) :: cf3b
!  REAL(KIND=8), dimension(:), pointer :: cf3
  REAL(kind=8) :: x
  REAL(kind=8) :: xb
  CALL PUSHREAL8(x)
  x = x*cf3(5)
  xb = xb + SUM(z*zb)
  zb = x*zb
  CALL POPREAL8(x)
  cf3b(5) = cf3b(5) + x*xb
  xb = cf3(5)*xb
END SUBROUTINE BAR_B

SUBROUTINE BAR_NODIFF(cf3, x)
  USE G1_DIFF
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:) :: cf3
!  REAL(KIND=8), dimension(:), pointer :: cf3
  REAL(kind=8) :: x
  x = x*cf3(5)
  z = z*x
END SUBROUTINE BAR_NODIFF

