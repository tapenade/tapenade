program twodwave

  implicit none
  integer,parameter :: dp = kind(1.0d0)

  type u_type
     real(kind=dp) :: old
     real(kind=dp) :: new
     real(kind=dp) :: current
  end type u_type

  type(u_type),allocatable,dimension(:,:) :: u

  real(kind=dp) :: x,y,r
  real(kind=dp) :: dt,dx,dy,c
  integer :: Nx = 1024
  integer :: Ny = 1024
  integer :: ix,iy,nstep,istep
  integer :: ierr
  integer :: ts,tc,rate

  call system_clock(ts,rate)

  dx = 10.0_dp/real(Nx-1,kind=dp)
  dy = 10.0_dp/real(Ny-1,kind=dp)
  dt = 0.001_dp
  c  = 5.0_dp
  nstep = 100

  allocate(u(1:Nx,1:Ny),stat=ierr)

  call system_clock(tc,rate)

  y  = 0.0_dp
  x  = 0.0_dp
  do iy=1,Ny
	do ix=1,Nx
    		x = x + dx
	end do
	y = y + dy
	x  = 0.0_dp
  end do
  deallocate(u,stat=ierr)

end program twodwave
