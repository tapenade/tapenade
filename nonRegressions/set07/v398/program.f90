! from badPush.f90 Jean 08/05/2012

module types
  implicit none

  type inner 
     real,pointer, dimension(:,:) :: r 
  end type inner

  type middle
     type(inner) ::anInner
  end type middle

  type outer
     type(middle) :: aMiddle
  end type outer

  type outerP
     type(outer),pointer :: p
  end type outerP

contains 
  subroutine foo(s)
    type(outerP), dimension(:) :: s
    integer i
    do i=1,ubound(s,1)
       s(i)%p%aMiddle%anInner%r=0
    end do
  end subroutine foo

end module types


subroutine head(x,y,outerPArray,n)
  use types
  implicit none
  real :: x,y
  integer ::  n
  integer :: i,j
  type(outerP), dimension(n) :: outerPArray
  do i=1,n
     do j=1,3
        outerPArray(i)%p%aMiddle%anInner%r(j,j)=x
     end do
  end do
  do i=1,n
     do j=1,3
        outerPArray(i)%p%aMiddle%anInner%r(j,j)=outerPArray(i)%p%aMiddle%anInner%r(j,j)*5
     end do
  end do
  y=1
  do i=1,n
     do j=1,3
        y=y*outerPArray(i)%p%aMiddle%anInner%r(j,j)*sin(outerPArray(i)%p%aMiddle%anInner%r(j,j))
     end do
  end do
  call foo(outerPArray)
end subroutine head

program p 
  use types
  implicit none
  real :: x,y
  integer, parameter ::  n=10
  integer :: i,j
  type(outerP), dimension(n) :: outerPArray
  do i=1,n
     allocate(outerPArray(i)%p)
     allocate(outerPArray(i)%p%aMiddle%anInner%r(3,3))
  end do
  x=0.5
  call head(x,y,outerPArray,n) 
  print*,y
end program p

