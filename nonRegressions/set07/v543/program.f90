module global_mod
   REAL, PUBLIC, DIMENSION(2,3) ::  sshb
   INTEGER, PUBLIC, DIMENSION(2,3) ::  ibonitl

end module global_mod

! from F77 lha78
module functodiff
      use global_mod

contains
      SUBROUTINE top(a,b)
      REAL a,b
      
      a = a*a
      b=b*a*b
      call bar(a)
!$AD BINOMIAL-CKP 11 3 1
      DO i=1,10
         call foo(a,b)
      ENDDO
      a = 2*a*a
      END

      SUBROUTINE foo(a2,b2)
      REAL a2,b2
      b2 = b2+1.0
      a2 = a2*a2 + b2
      call bar(a2)
      END

      subroutine bar(a)
      real a
      integer i,j
        do i = 1,2
         do j = 1,3 
           sshb(i,j) = sshb(i,j) * a
         end do
        end do
        a = sshb(2,3)
      end

end module functodiff
