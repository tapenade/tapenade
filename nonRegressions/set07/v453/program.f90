module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

module M1_I
real :: compute
external compute
end module M1_I

function compute(x,y)
  use M
  implicit none
  real, dimension(n) :: x,y
  real :: compute
  y = 2 * x
  compute = y(1) * y(2)
  global = global + compute
end function compute

module M2_I
real :: ftest
external  ftest
end module M2_I

function ftest(r,s,g)
  use M
  use M1_I
  implicit none
  real, dimension(n) :: r,s
  real ftest
  real :: g
  external g
  ftest = g(r,s)
end function ftest

program prog
use M
use M1_I
use M2_I
implicit none
real, dimension(n) :: r,s
r(1) = 3.0
s(1) = 0.0
r(2) = 2.0
s(2) = 0.0
print*,ftest(r,s,compute)
!! print*,compute(r,s) avec ca change les interfaces differentiees
end program prog
