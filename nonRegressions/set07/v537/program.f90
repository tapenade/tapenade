! from v522 avec test associated dans compute
module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

module M1_I
   interface
      subroutine compute (x,y)
       use M
       implicit none
       real, dimension(:), pointer :: x,y
      end subroutine compute
   end interface

   interface
      subroutine alloc (x,y)
       implicit none
       real, dimension(:), pointer :: x,y
      end subroutine alloc
   end interface

   interface
      subroutine dealloc (x,y)
       implicit none
       real, dimension(:), pointer :: x,y
      end subroutine dealloc
   end interface

end module M1_I

subroutine alloc(x,y)
  real, dimension(:), pointer :: x,y
  allocate(x(5))
  allocate(y(5))
end subroutine alloc

subroutine dealloc(x,y)
  real, dimension(:), pointer :: x,y
  deallocate(x,y)
end subroutine dealloc

subroutine compute(x,y)
  use M
  implicit none
  real, dimension(:), pointer :: x,y
  real :: computex
  if ( .not. associated(y)) then 
     allocate(y(5))
  end if
  y = 2 * x
  computex = y(1) * y(2)
  global = global + computex
end subroutine compute

subroutine top()
  use M
  use M1_I
  implicit none
  real, dimension(:), pointer :: r,s
  call alloc(r,s)
  r(1) = 3.0
  r(2) = 2.0
  call compute(r,s)
  print*,s(1)
  call dealloc(r,s)
end subroutine top
