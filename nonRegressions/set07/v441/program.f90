       RECURSIVE FUNCTION FACTORIAL(N)
       IMPLICIT NONE
       double precision, INTENT(IN)      :: N
       double precision               :: FACTORIAL
               FACTORIAL = 1
               FACTORIAL = int(N) * FACTORIAL(N-1)

       END FUNCTION FACTORIAL
