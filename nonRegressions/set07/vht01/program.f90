MODULE M
  
  PUBLIC foo 

CONTAINS
  
  SUBROUTINE foo
    
    !! * Local declarations
    REAL ::   toto = 1      ! temporary real
    
    toto = toto * 2
    
    PRINT*,'TOTO = ',toto
    
  END SUBROUTINE foo
  
END MODULE M


PROGRAM main
  
  USE M
  
  call foo
  call foo
  
END PROGRAM main
