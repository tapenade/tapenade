MODULE MMA
  REAL,POINTER :: vv1,aa1
  REAL,ALLOCATABLE :: vv2,aa2
  REAL,TARGET:: dvv1,daa1
END MODULE MMA

SUBROUTINE GLOBSUBA(xx)
  USE MMA
  REAL xx
  aa2 = aa2*aa2
  vv1 = vv1*xx
  vv2 = 2.0*vv2
  xx = xx*vv1*vv2
END SUBROUTINE GLOBSUBA

SUBROUTINE TOP(uu,tt)
  USE MMA
  REAL uu,tt
! $AD NOCHECKPOINT
  call GLOBSUBA(uu)
  tt = uu*tt
END SUBROUTINE TOP
