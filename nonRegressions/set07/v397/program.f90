! from Jean 27/04/20
! java.lang.NullPointerException
! at ali.symbolTable.InterfaceDecl.findFunctionDeclEqualsOrElementType

module m1
 contains 
  subroutine foo_a(x)
    real :: x 
    x=x*2
    print *, "foo ", x
  end subroutine 
  subroutine bar_p(y)
    double precision :: y 
    print *, "bar ", y
  end subroutine 
end module 

module m2
  use m1 
  interface i_a 
   module procedure foo_a
  end interface 
  interface i_p
   module procedure bar_p
  end interface
end module 

subroutine head(x,y)
  use m2
  real :: x
  double precision :: t,y
  call i_a(x)
  t=x
  call i_p(t)
  y=t
end subroutine 

program p 
  real ::  x 
  double precision :: y 
  x=1.0
  call head(x,y)
  print *,y
end program

