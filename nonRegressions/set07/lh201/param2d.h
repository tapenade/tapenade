      implicit real (a-h,o-z)
      parameter (nn  = 60000,     ! 15000 in the original code
     &           nnt = 2*nn,
     &           nnsg= 3*nn,  
     &           nnfr= 2500 ,      ! 1500 in the original code 
     &           maxp=20,maxt=20)  !  20 in the original code
      parameter(nvar=6)
      parameter (irkmax= 4)
      common/comge0/ns,nt,nseg,nfr
      common/comge1/coor(2,nn)
      common/comge2/airs(nn),airt(nnt)
      common/comge3/nubo(2,nnsg)
      common/comge4/logfr(nn),nufr(nnfr),nu(3,nnt)
      common/comge5/vnocl(3,nnsg)
      common/comge6/vnox(nn),vnoy(nn),vno(nn)
      common/comge9/dthaut(nn)
      common/comso1/un(nvar,nn)
      common/comso2/ua(nvar,nn),pres(nn)
      common/comdt0/iloc,isave
      common/comdt1/cfl,dtl(nn)
      common/comkt/t,kt,tmax,ktmax,resf,ifre
      common/comrkk/irk,alpha(irkmax)
      common/comfl1/ce(nvar,nn)
      common/comfl2/iflux,nordre
      common/comgr0/beta,beta1
      common/comgr1/dx(4,nn),dy(4,nn)
      common/cominf/roin,uxin,uyin,pin,roout,uxout,uyout,pout,
     &              ein,ruxin,ruyin,eout,ruxout,ruyout
      common/comcnt/ncont
      common/comgam/gam,gam1,gam4
      common/comvi1/ivis
      common/comvi2/tbrd2,pr,reynolds,tinf,tinfd,xmach,iecc,froud
      common/frontier/nslog2,log2(nnfr),nslog3,log3(nnfr),
     1                nslog4,log4(nnfr),nslog5,log5(nnfr),
     1                nslog6,log6(nnfr)
      common/comaxi/iaxi,rrs(nn),airsa(nn),airta(nnt)
      common/comvi3/reylam(nn),dist(nn),reyturb(nn),reyt2(nn)
      common/turb1/iturb,delta,ilaw,ncont1,prt,xkin,xein 
      common/turb2/numc(nn),nbt(nnfr),jtb(maxt,nnfr),nbp(nnfr),
     1             ipb(maxp,nnfr),nswall(nn)
      common/turb3/xtmin,xtmax,ytmin,ytmax
      common/turb4/fluro(nnsg),flurob(nn)
