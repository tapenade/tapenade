C Bug found in papaale: "temp3" was not declared !
      subroutine fluosh
c     bijan mohammadi, INRIA
      include 'param2d.h'

      real dpm(4),dpor(4),dpex(4),aux1(4),aux2(4),
     1     gradi(4),gradj(4)

      e2       =1.e-16
      beta2 = beta
      beta3 = 0.5*(1.0 - 2.0*beta)
      gg1      = gam/gam1
      GAMO     = (GAM-1.)*.5
      USG0     = 1./GAMO
      POW      = 1./(2.*GAM)
      COEFF    = GAM1/(GAM+1.)

      do 500 nsg=1,nseg
         xnn       =- vnocl(1,nsg)
         ynn       =- vnocl(2,nsg)
         rnn       =  vnocl(3,nsg) * 0.5
         nubo1     = nubo(1,nsg)
         nubo2     = nubo(2,nsg)
         aix       = coor(1,nubo2)-coor(1,nubo1)
         aiy       = coor(2,nubo2)-coor(2,nubo1) 
         uas11     = ua(1,nubo1)
         uas12     = ua(1,nubo2)
         uas21     = ua(2,nubo1)
         uas22     = ua(2,nubo2)
         uas31     = ua(3,nubo1)
         uas32     = ua(3,nubo2)
         uas41     = pres(nubo1)
         uas42     = pres(nubo2)

         PROD1 = xnn
         C1    = SQRT(GAM*UAS41/UAS11)
         PROD2 = ynn
         C2    = SQRT(GAM*UAS42/UAS12)

         FM4 = (GG1*UAS41+.5*UAS11*
     &        (UAS21*UAS21+UAS31*UAS31))*PROD1+
     &        (GG1*UAS42+.5*UAS12*
     &        (UAS22*UAS22+UAS32*UAS32))*PROD2

         AUX        = (C1*USG0-PROD1)/C1
         UVNL11     = AUX*((COEFF*ABS(AUX))**USG0)*UAS11
         UVNL14     = AUX*((UVNL11/UAS11)**GAM)*UAS41
         UVNL12     = xnn
         UVNL13     = ynn

         PROVL1     = xnn
         FLUVN14    = (gg1*UVNL14+.5*(UVNL12*UVNL12+
     &        UVNL13*UVNL13)*UVNL11)*PROVL1

         PROVL2     = xnn
         FLUVN24    = ynn

         SI1        = xnn
         SI4        = ynn

         FM4=FM4-((gg1*UAS42+.5*(UAS22*UAS22+UAS32*UAS32)*
     &        UAS12)*PROD2-FLUVN24)*SI1
     &       +((gg1*UAS41+.5*(UAS21*UAS21+UAS31*UAS31)*
     &        UAS11)*PROD1-FLUVN14)*SI4


         CE(4,NUBO1)=CE(4,NUBO1)-FM4*rnn

 500  continue
c
      return
      end
