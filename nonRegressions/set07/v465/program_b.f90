!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
! bug de Jean Utke du 31/05/2011
! pb avec le type de result
MODULE M_DIFF
  IMPLICIT NONE
  INTEGER, PUBLIC, PARAMETER :: rk=8
  TYPE T
      REAL(rk) :: rho0
      REAL(rk) :: n
  END TYPE T
  TYPE(T) :: params
  REAL(rk) :: rho
  REAL(rk) :: rhob

CONTAINS
!  Differentiation of s in reverse (adjoint) mode:
!   gradient     of useful results: rho yy
!   with respect to varying inputs: rho yy
!   RW status of diff variables: rho:incr yy:in-zero
  SUBROUTINE S_B(xx, yy, yyb)
    IMPLICIT NONE
    REAL*8 :: xx, yy, lat
    REAL*8 :: yyb
    INTRINSIC REAL
    INTRINSIC SIGN
    REAL(rk) :: result1
    result1 = REAL(90.0, kind=rk)
    lat = SIGN(result1, params%n)
    rhob = rhob + lat*yyb
    yyb = 0.0_8
  END SUBROUTINE S_B

  SUBROUTINE S(xx, yy)
    IMPLICIT NONE
    REAL*8 :: xx, yy, lat
    INTRINSIC REAL
    INTRINSIC SIGN
    REAL(rk) :: result1
    result1 = REAL(90.0, kind=rk)
    lat = SIGN(result1, params%n)
    yy = rho*lat
  END SUBROUTINE S

END MODULE M_DIFF

