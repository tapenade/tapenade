! bug de Jean Utke du 31/05/2011
! pb avec le type de result

module m
  INTEGER, PUBLIC, PARAMETER :: rk=8
  type t
     real(rk) :: rho0 
     real(rk) :: n 
  end type t
  type(t) :: params
   real(rk) :: rho

contains

subroutine s(xx,yy)
  real*8 :: xx, yy, lat
  lat = SIGN( REAL(90.0, kind=rk), params%n) 
  yy= rho * lat
end subroutine s

end module m
