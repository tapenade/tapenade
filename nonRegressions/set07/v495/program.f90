! from Pablo Saide from v313
      subroutine test1(x,y,z,crefin)

        IMPLICIT NONE

      real x,y,z
      real refr,refi,aux, aux1, aux2
      complex crefin

      refr=real(crefin)
      refi=-imag(crefin)

      aux=crefin*conjg(crefin)
      aux1= conjg(crefin)
      aux2= conjg(crefin)
      aux = aux + aux1 + aux2

      x = y + z*z + refr*refi +aux*aux
      end 
