MODULE DIFFSIZES
  IMPLICIT NONE
  INTEGER :: ISIZE1OFDrfind_tet, ISIZE2OFDrfind_tet
  INTEGER :: ISIZE1OFind_tet, ISIZE2OFind_tet, ISIZE1OFind_nf, ISIZE2OFind_nf
  INTEGER :: ISIZE1OFDrfstockm
  INTEGER :: ISIZE1OFinda, ISIZE1OFsel_dg, ISIZE2OFsel_dg, ISIZE1OFstockm
  INTEGER :: ISIZE2OFDrfstockm
  INTEGER :: ISIZE2OFstockm, ISIZE1OFaa, ISIZE1OFab_sorp, ISIZE2OFab_sorp
  INTEGER :: ISIZE3OFab_sorp, ISIZE1OFalpha_perm, ISIZE1OFalpha_sat
  INTEGER :: ISIZE1OFan_sorp, ISIZE2OFan_sorp, ISIZE3OFan_sorp, ISIZE1OFa_perm
  INTEGER :: ISIZE1OFDrfck_n
  INTEGER :: ISIZE1OFb_perm, ISIZE1OFckold, ISIZE1OFck_n2, ISIZE1OFck_n
  INTEGER :: ISIZE1OFck_save, ISIZE1OFck, ISIZE2OFck, ISIZE1OFc_perm
  INTEGER :: ISIZE1OFiaa, ISIZE1OFc_sat, ISIZE1OFfac_trans, ISIZE2OFfac_trans
  INTEGER :: ISIZE1OFDrffk_n
  INTEGER :: ISIZE3OFfac_trans, ISIZE1OFfc_prod, ISIZE2OFfc_prod, ISIZE1OFfk_n
  INTEGER :: ISIZE1OFDrffk_prod, ISIZE2OFDrffk_prod
  INTEGER :: ISIZE1OFDrffk, ISIZE2OFDrffk
  INTEGER :: ISIZE1OFfk_prod, ISIZE2OFfk_prod, ISIZE1OFfk, ISIZE2OFfk
  INTEGER :: ISIZE1OFDrffprprod
  INTEGER :: ISIZE1OFflucomp, ISIZE1OFfprprod, ISIZE1OFfps_trans, ISIZE1OFkd_sorp
  INTEGER :: ISIZE1OFDrlibere_fk
  INTEGER :: ISIZE1OFDrflocn, ISIZE2OFDrflocn, ISIZE1OFDrflibere_fk
  INTEGER :: ISIZE2OFkd_sorp, ISIZE1OFlocn, ISIZE2OFlocn, ISIZE1OFlibere_fk
  INTEGER :: ISIZE1OFlambda_perm, ISIZE1OFlambda_sat, ISIZE1OFmass_vol
  INTEGER :: ISIZE1OFm_perm, ISIZE1OFmqconv, ISIZE2OFmqconv, ISIZE1OFmqd
  INTEGER :: ISIZE2OFmqd, ISIZE1OFm_sat, ISIZE1OFmtc1, ISIZE2OFmtc1
  INTEGER :: ISIZE1OFmtc_n, ISIZE2OFmtc_n, ISIZE1OFmtcold, ISIZE2OFmtcold
  INTEGER :: ISIZE1OFmtc, ISIZE1OFn_perm, ISIZE1OFn_sat, ISIZE1OFpkp1
  INTEGER :: ISIZE1OFpk, ISIZE1OFporos, ISIZE1OFpsia_sat, ISIZE1OFresidual_sat
  INTEGER :: ISIZE1OFretard_actu, ISIZE1OFretard_av, ISIZE1OFrho_s, ISIZE1OFrzs
  INTEGER :: ISIZE1OFtcold, ISIZE2OFtcold, ISIZE1OFtensor, ISIZE2OFtensor
  INTEGER :: ISIZE1OFtpold, ISIZE1OFtpp1, ISIZE1OFtpc, ISIZE1OFtp
  INTEGER :: ISIZE1OFmcfact, ISIZE1OFcmk, ISIZE1OFfps, ISIZE1OFdel_pre
  INTEGER :: ISIZE2OFdel_pre, ISIZE1OFddrt, ISIZE1OFddr, ISIZE1OFddz
  INTEGER :: ISIZE1OFDrfddc
  INTEGER :: ISIZE1OFddc, ISIZE1OFdddia1, ISIZE1OFdddia, ISIZE1OFddd
  INTEGER :: ISIZE1OFdia1, ISIZE1OFdia, ISIZE1OFdc, ISIZE1OFdzs, ISIZE1OFd
  INTEGER :: ISIZE1OFv, ISIZE1OFr, ISIZE1OFy, ISIZE1OFz, ISIZE1OFx, ISIZE1OFf

  INTEGER :: ISIZE1OFfacnor, ISIZE2OFfacnor, ISIZE3OFfacnor, ISIZE1OFxyz, ISIZE2OFxyz
  INTEGER :: ISIZE1OFDrperm_t
  INTEGER :: ISIZE1OFporos_t, ISIZE2OFporos_t, ISIZE3OFporos_t, ISIZE1OFperm_t
  INTEGER :: ISIZE2OFperm_t, ISIZE3OFperm_t, ISIZE1OFviscos_t, ISIZE2OFviscos_t
  INTEGER :: ISIZE3OFviscos_t, ISIZE1OFmass_vol_t, ISIZE2OFmass_vol_t, ISIZE3OFmass_vol_t
  INTEGER :: ISIZE1OFmcfact_t, ISIZE2OFmcfact_t, ISIZE3OFmcfact_t, ISIZE1OFviscos
  INTEGER :: ISIZE1OFDrfperm
  INTEGER :: ISIZE1OFperm, ISIZE2OFperm, ISIZE1OFtime_jalons, ISIZE1OFwrite_times
  INTEGER :: ISIZE1OFflow_velocity, ISIZE2OFflow_velocity, ISIZE1OFmq, ISIZE2OFmq
  INTEGER :: ISIZE1OFporosity, ISIZE1OFtcm1, ISIZE2OFtcm1, ISIZE2OFmtc, ISIZE3OFmtc
  INTEGER :: ISIZE1OFckm1, ISIZE2OFckm1, ISIZE1OFconc_old, ISIZE2OFconc_old, ISIZE1OFmtc_old
  INTEGER :: ISIZE2OFmtc_old, ISIZE3OFmtc_old, ISIZE1OFretard_t, ISIZE2OFretard_t
  INTEGER :: ISIZE3OFretard_t, ISIZE4OFretard_t, ISIZE1OFlambda_i_splt, ISIZE1OFsigma_ij 
  INTEGER :: ISIZE2OFsigma_ij, ISIZE1OFtheta_lambda, ISIZE1OFlambda_i
  INTEGER :: ISIZE1OFomega_i, ISIZE2OFomega_i, ISIZE1OFc_sat_e, ISIZE2OFc_sat_e
  INTEGER :: ISIZE1OFfk_save, ISIZE1OFfkold, ISIZE1OFfps_hydro, ISIZE1OFval_src
  INTEGER :: ISIZE1OFDrfsolid_sp_zone
  INTEGER :: ISIZE1OFval_src_t, ISIZE2OFval_src_t, ISIZE3OFval_src_t, ISIZE1OFsolid_sp_zone
  INTEGER :: ISIZE2OFDrfsolid_sp_zone, ISIZE1OFDrfwater_zone_init
  INTEGER :: ISIZE2OFsolid_sp_zone, ISIZE1OFwater_zone_init, ISIZE1OFconc_bound_zone
  INTEGER :: ISIZE2OFconc_bound_zone, ISIZE3OFconc_bound_zone, ISIZE1OFwater_zone
  INTEGER :: ISIZE1OFsink_sp_zone, ISIZE2OFsink_sp_zone, ISIZE1OFconc_sp_zone_init
  INTEGER :: ISIZE2OFconc_sp_zone_init, ISIZE1OFad_conc_sp_zone, ISIZE2OFad_conc_sp_zone
  INTEGER :: ISIZE1OFDrfflux_dc_surf
  INTEGER :: ISIZE1OFconc_sp_zone, ISIZE2OFconc_sp_zone, ISIZE1OFflux_dc_surf
  INTEGER :: ISIZE2OFDrfflux_dc_surf, ISIZE3OFDrfflux_dc_surf, ISIZE1OFDrfsink_wat_zone
  INTEGER :: ISIZE2OFflux_dc_surf, ISIZE3OFflux_dc_surf, ISIZE1OFsink_wat_zone
  INTEGER :: ISIZE1OFDrfsink_sld_sp_zone, ISIZE2OFDrfsink_sld_sp_zone
  INTEGER :: ISIZE1OFDrfwater_bound_zone
  INTEGER :: ISIZE1OFwater_bound_zone, ISIZE1OFsink_sld_sp_zone, ISIZE2OFsink_sld_sp_zone
  INTEGER :: ISIZE1OFval_cal_t, ISIZE2OFval_cal_t, ISIZE3OFval_cal_t, ISIZE1OFval_cal
  INTEGER :: ISIZE2OFval_cal, ISIZE1OFche_src, ISIZE2OFche_src
  INTEGER :: ISIZE1OFck_temp, ISIZE1OFom_del_pre
  INTEGER :: ISIZE1OFtpm1, ISIZE1OFpkm1, ISIZE1OFpk_save

  INTEGER :: ISIZE1OFalpha_th, ISIZE1OFalpha_t_th, ISIZE2OFalpha_t_th
  INTEGER :: ISIZE3OFalpha_t_th, ISIZE4OFalpha_t_th, ISIZE1OFalpha_tv
  INTEGER :: ISIZE1OFalpha_t_tv, ISIZE2OFalpha_t_tv, ISIZE3OFalpha_t_tv, ISIZE4OFalpha_t_tv
  INTEGER :: ISIZE1OFalpha_l, ISIZE1OFdiff_c, ISIZE1OFdiff, ISIZE1OFdiff_t
  INTEGER :: ISIZE2OFdiff_t, ISIZE3OFdiff_t, ISIZE4OFdiff_t
  INTEGER :: ISIZE1OFalpha_l_t, ISIZE2OFalpha_l_t, ISIZE3OFalpha_l_t
  INTEGER :: ISIZE4OFalpha_l_t, ISIZE1OFstockmff

  INTEGER :: ISIZE1OFlc, ISIZE1OFvalcalhydro, ISIZE2OFvalcalhydro
  INTEGER :: ISIZE1OFvalsrchydro, ISIZE1OFwhichcalhydro, ISIZE1OFwhich_cal
  INTEGER :: ISIZE1OFfac_limit, ISIZE2OFfac_limit

  INTEGER :: ISIZE1OFdiff_t_bin, ISIZE2OFdiff_t_bin, ISIZE3OFdiff_t_bin, ISIZE4OFdiff_t_bin
  INTEGER :: ISIZE1OFporos_t_bin, ISIZE2OFporos_t_bin, ISIZE3OFporos_t_bin
  INTEGER :: ISIZE1OFvalsrcthydro, ISIZE2OFvalsrcthydro, ISIZE3OFvalsrcthydro
  INTEGER :: ISIZE1OFvalcalthydro, ISIZE2OFvalcalthydro, ISIZE3OFvalcalthydro
  INTEGER :: isize1ofvstr, isize1ofvsttr, isize2ofvsttr, isize3ofvsttr
  INTEGER :: isize1ofvctr, isize2ofvctr, isize1ofvcttr, isize2ofvcttr, isize3ofvcttr
  INTEGER :: isize1ofmqhdy, isize2ofmqhdy, ISIZE1OFaamesh
  INTEGER :: isize1oftpoldhydro, isize1offvarcomm, isize1oflctr
  integer :: isize1ofdrfconc_sp_zone
integer :: isize2ofdrfconc_sp_zone
integer :: isize1ofdrfad_conc_sp_zone
integer :: isize2ofdrfad_conc_sp_zone
integer :: isize1ofdrfconc_sp_zone_init
integer :: isize2ofdrfconc_sp_zone_init
integer :: isize1ofdrfsink_sp_zone
integer :: isize2ofdrfsink_sp_zone
integer :: isize1ofdrfwater_zone
integer :: isize1ofdrfconc_bound_zone
integer :: isize3ofdrfconc_bound_zone
integer :: isize2ofdrfconc_bound_zone
integer :: isize1ofdrfck_n2
integer :: isize1ofdrfom_del_pre
integer :: isize1ofdrfdddia1
integer :: isize1ofdrfdddia
integer :: isize1ofdrfddrt
integer :: isize1ofdrftpc
integer :: isize1ofdrfddz
integer :: isize1ofdrfdel_pre
integer :: isize2ofdrfdel_pre
integer :: isize1ofdrff
integer :: isize1ofdrfddd
integer :: isize1ofdrfck
integer :: isize2ofdrfck
integer :: isize1ofdrftcold
integer :: isize2ofdrftcold
integer :: isize1ofdrfmqd
integer :: isize2ofdrfmqd
integer :: isize1ofdrfmqconv
integer :: isize2ofdrfmqconv
integer :: isize1ofdrfmtc
integer :: isize3ofdrfmtc
integer :: isize2ofdrfmtc
integer :: isize1ofdrfckold
integer :: isize1ofdrfmtcold
integer :: isize2ofdrfmtcold
integer :: isize1ofdrftcm1
integer :: isize2ofdrftcm1
integer :: isize1ofdrfsel_dg
integer :: isize2ofdrfsel_dg
integer :: isize1ofdrfmtc1
integer :: isize2ofdrfmtc1
integer :: isize1ofdrffc_prod
integer :: isize2ofdrffc_prod
integer :: isize1ofdrfvalcalhydro
integer :: isize2ofdrfvalcalhydro
integer :: isize1ofdrffac_limit
integer :: isize2ofdrffac_limit
integer :: isize1ofdrfwhichcalhydro
integer :: isize1ofdrfvalsrchydro
integer :: isize2ofdrfperm
integer :: isize1ofdrfviscos
integer :: isize1ofdrfporos
integer :: isize1ofdrfmcfact
integer :: isize1ofdrfmass_vol
integer :: isize1ofdrfdiff
integer :: isize1ofdrfalpha_l
integer :: isize1ofdrfalpha_tv
integer :: isize1ofdrftensor
integer :: isize2ofdrftensor
integer :: isize1ofdrfalpha_th
integer :: isize1ofdrfval_cal
integer :: isize2ofdrfval_cal
integer :: isize1ofdrffac_trans
integer :: isize3ofdrffac_trans
integer :: isize2ofdrffac_trans
integer :: isize1ofdrflc
integer :: isize1ofdrfwhich_cal
integer :: isize1ofdrfval_src
integer :: isize1ofdrfretard_av
integer :: isize1ofdrfretard_actu
integer :: isize1ofdrfind_nf
integer :: isize2ofdrfind_nf
integer :: isize1ofdrfinda
integer :: isize1ofdrfiaa
integer :: isize1ofdrfvctr
integer :: isize2ofdrfvctr
integer :: isize1ofdrffps_trans
integer :: isize1ofdrflctr
integer :: isize2ofdrfmtc_n
integer :: isize1ofdrfmtc_n
integer :: isize1ofdrfck_save
integer :: isize1ofdrfddr
integer :: isize1ofdrfc_sat_e
integer :: isize1ofdrfomega_i

integer :: isize1ofdrfk
integer :: size1ofdrfkt_src
integer :: isize1ofdrfiso_elt
integer :: isize1ofdrfkt_src 

integer :: ISIZE1OFtemp
integer :: ISIZE1OFdrflambda_i
integer :: isize1ofdrfpk
integer :: isize1ofdrfwrite_times
integer :: isize1ofdrftime_jalons
integer :: isize1ofdrfsorption

integer :: isize1ofdrfind_f_cum
integer :: isize1ofdrffps_hydro
integer :: isize1ofdrfaa
integer :: isize1ofdrffk_save

CONTAINS

  SUBROUTINE INIT_DIFFSIZES(dim, nb_elts, nb_mass_vol, nb_mcf, nb_perm, &
 & nb_poros, nb_specie, nb_surf, nb_time_cal, nb_time_src, nb_type_cal, &
 & nb_type_src, nb_viscos, nb_zone, nb_zone_bil, nc_f, nc_n, ndeltat, nf, &
 & nf_n, nm, nn, nt_write, nb_times)

    IMPLICIT NONE
    INTEGER dim, nb_elts, nb_mass_vol, nb_mcf, nb_perm, nb_poros, nb_specie, &
 & nb_surf, nb_time_cal, nb_time_src, nb_type_cal, nb_type_src, nb_viscos, &
 & nb_zone, nb_zone_bil, nc_f, nc_n, ndeltat, nf, nf_n, nm, nn, nt_write, &
 & nb_times

    ISIZE1OFind_tet = nc_f
    ISIZE1OFdrfind_tet = nc_f
    ISIZE1OFind_tet = nc_f
    ISIZE1OFdrfind_tet = nc_f
    ISIZE2OFind_tet = 4
    ISIZE2OFdrfind_tet = 4
    ISIZE1OFind_nf = nc_n
    ISIZE1OFdrfind_nf = nc_n
    ISIZE2OFind_nf = dim
    ISIZE2OFdrfind_nf = dim
    ISIZE1OFinda = (nf+1)
    ISIZE1OFdrfinda = (nf+1)
    ISIZE1OFsel_dg = nm
    ISIZE1OFdrfsel_dg = nm
    ISIZE2OFsel_dg = nb_specie
    ISIZE2OFdrfsel_dg = nb_specie
    ISIZE1OFstockm = nm
    ISIZE1OFdrfstockm = nm
    ISIZE2OFstockm = (nc_f+2)
    ISIZE2OFdrfstockm = (nc_f+2)
    ISIZE1OFaa = 2*nc_f*nf
    ISIZE1OFab_sorp = 2
    ISIZE2OFab_sorp = nb_zone
    ISIZE3OFab_sorp = nb_specie
    ISIZE1OFalpha_perm = nb_zone
    ISIZE1OFalpha_sat = nb_zone
    ISIZE1OFan_sorp = 2
    ISIZE2OFan_sorp = nb_zone
    ISIZE3OFan_sorp = nb_specie
    ISIZE1OFa_perm = nb_zone
    ISIZE1OFb_perm = nb_zone
    ISIZE1OFckold = nm
    ISIZE1OFdrfckold = nm
    ISIZE1OFck_n2 = nm
    ISIZE1OFdrfck_n2 = nm
    ISIZE1OFck_n = nm
    ISIZE1OFdrfck_n = nm
    ISIZE1OFck_save = nm
    ISIZE1OFdrfck_save = nm
    ISIZE1OFck = nm
    ISIZE1OFdrfck = nm
    ISIZE2OFck = nb_specie
    ISIZE2OFdrfck = nb_specie
    ISIZE1OFc_perm = nb_zone
    ISIZE1OFiaa = (2*nc_f*nf)
    ISIZE1OFdrfiaa = (2*nc_f*nf)
    ISIZE1OFc_sat = nb_zone
    ISIZE1OFfac_trans = 2
    ISIZE1OFdrffac_trans = 2
    ISIZE2OFfac_trans = nf
    ISIZE2OFdrffac_trans = nf
    ISIZE3OFfac_trans = nb_specie
    ISIZE3OFdrffac_trans = nb_specie
    ISIZE1OFfc_prod = nm
    ISIZE1OFdrffc_prod = nm
    ISIZE2OFfc_prod = nb_specie
    ISIZE2OFdrffc_prod = nb_specie
    ISIZE1OFfk_n = nm
    ISIZE1OFdrffk_n = nm
    ISIZE1OFDrffk_prod = nm
    ISIZE1OFfk_prod = nm
    ISIZE1OFdrffk_prod = nm
    ISIZE2OFfk_prod = nb_specie
    ISIZE2OFdrffk_prod = nb_specie
    ISIZE1OFfk = nm
    ISIZE1OFdrffk = nm
    ISIZE2OFfk = nb_specie
    ISIZE2OFdrffk = nb_specie
    ISIZE1OFflucomp = nb_zone
    ISIZE1OFfprprod = nm
    ISIZE1OFdrffprprod = nm
    ISIZE1OFfps_trans = nm
    ISIZE1OFdrffps_trans = nm
    ISIZE1OFkd_sorp = nb_zone
    ISIZE2OFkd_sorp = nb_specie
    ISIZE1OFlocn = nc_f
    ISIZE1OFdrflocn = nc_f
    ISIZE2OFlocn = nf_n
    ISIZE2OFdrflocn = nf_n
    ISIZE1OFDrflibere_fk = nm
    ISIZE1OFlibere_fk = nm
    ISIZE1OFdrflibere_fk = nm
    ISIZE1OFlambda_perm = nb_zone
    ISIZE1OFlambda_sat = nb_zone
    ISIZE1OFmass_vol = nb_zone
    ISIZE1OFdrfmass_vol = nb_zone
    ISIZE1OFm_perm = nb_zone
    ISIZE1OFmqconv = nc_f
    ISIZE1OFdrfmqconv = nc_f
    ISIZE2OFmqconv = nm
    ISIZE2OFdrfmqconv = nm
    ISIZE1OFmqd = nc_f
    ISIZE1OFdrfmqd = nc_f
    ISIZE2OFmqd = nm
    ISIZE2OFdrfmqd = nm
    ISIZE1OFm_sat = nb_zone
    ISIZE1OFmtc1 = nc_n
    ISIZE1OFdrfmtc1 = nc_n
    ISIZE2OFmtc1 = nm
    ISIZE2OFdrfmtc1 = nm
    ISIZE1OFmtc_n = nc_n
    ISIZE1OFdrfmtc_n = nc_n
    ISIZE2OFmtc_n = nm
    ISIZE2OFdrfmtc_n = nm
    ISIZE1OFmtcold = nc_n
    ISIZE1OFdrfmtcold = nc_n
    ISIZE2OFmtcold = nm
    ISIZE2OFdrfmtcold = nm
    ISIZE1OFn_perm = nb_zone
    ISIZE1OFn_sat = nb_zone
    ISIZE1OFpkp1 = nm
    ISIZE1OFpk = nm
    ISIZE1OFporos = nb_zone
    ISIZE1OFdrfporos = nb_zone
    ISIZE1OFpsia_sat = nb_zone
    ISIZE1OFresidual_sat = nb_zone 
    ISIZE1OFretard_actu = nb_zone
    ISIZE1OFdrfretard_actu = nb_zone
    ISIZE1OFretard_av = nb_zone
    ISIZE1OFdrfretard_av = nb_zone
    ISIZE1OFrho_s = nb_zone
!    ISIZE1OFrzs = nk
    ISIZE1OFtcold = nf
    ISIZE1OFdrftcold = nf
    ISIZE2OFtcold = nb_specie
    ISIZE2OFdrftcold = nb_specie
    if (dim.eq.2) then
       ISIZE1OFdrftensor = 3
    else
       ISIZE1OFdrftensor = 6
    endif
    ISIZE2OFdrftensor = nm
    ISIZE1OFtpold = nf
    ISIZE1OFtpp1 = nf
    ISIZE1OFtpc = nf
    ISIZE1OFdrftpc = nf
    ISIZE1OFtp = nf
    ISIZE1OFmcfact = nb_zone
    ISIZE1OFdrfmcfact = nb_zone
    ISIZE1OFcmk = nm
    ISIZE1OFfps = nm
    ISIZE1OFdel_pre = 2
    ISIZE1OFdrfdel_pre = 2
    ISIZE2OFdel_pre = nm
    ISIZE2OFdrfdel_pre = nm
    ISIZE1OFddrt = nf
    ISIZE1OFdrfddrt = nf
    ISIZE1OFddr = nf
    ISIZE1OFdrfddr = nf
    ISIZE1OFddz = nf
    ISIZE1OFdrfddz = nf
    ISIZE1OFddc = nf
    ISIZE1OFdrfddc = nf
    ISIZE1OFdddia1 = nf
    ISIZE1OFdrfdddia1 = nf
    ISIZE1OFdddia = nf
    ISIZE1OFdrfdddia = nf
    ISIZE1OFddd = nf
    ISIZE1OFdrfddd = nf
    ISIZE1OFdia1 = nf
    ISIZE1OFdia = nf
    ISIZE1OFdc = nf
!    ISIZE1OFdzs = nk
    ISIZE1OFd = nf
    ISIZE1OFv = nf
    ISIZE1OFr = nf
    ISIZE1OFy = nf
    ISIZE1OFz = nf
!    ISIZE1OFx = n
    ISIZE1OFf = nf
    ISIZE1OFdrff = nf

    ISIZE2OFfacnor = nc_f
    ISIZE3OFfacnor = nm
    ISIZE1OFxyz = dim
    ISIZE2OFxyz = nn
    ISIZE1OFporos_t = 2
    ISIZE2OFporos_t = nb_poros
    ISIZE3OFporos_t = nb_zone
    if (dim.eq.2) then
       ISIZE1OFdrfperm = 3
       ISIZE1OFperm_t = 4
    else
       ISIZE1OFdrfperm = 6
       ISIZE1OFperm_t = 7
    endif
    ISIZE3OFperm_t = nb_zone
    ISIZE1OFviscos_t = 2
    ISIZE2OFviscos_t = nb_viscos
    ISIZE3OFviscos_t = nb_zone
    ISIZE1OFmass_vol_t = 2
    ISIZE2OFmass_vol_t = nb_mass_vol
    ISIZE3OFmass_vol_t = nb_zone
    ISIZE1OFmcfact_t = 2
    ISIZE2OFmcfact_t = nb_mcf
    ISIZE3OFmcfact_t = nb_zone
    ISIZE1OFviscos = nb_zone
    ISIZE1OFdrfviscos = nb_zone
    ISIZE2OFperm = nb_zone
    ISIZE2OFdrfperm = nb_zone
    ISIZE1OFtime_jalons = ndeltat+1
    ISIZE1OFwrite_times = nt_write+1
    ISIZE1OFflow_velocity = dim
    ISIZE2OFflow_velocity = nm
    ISIZE1OFmq = nc_f
    ISIZE2OFmq = nm
    ISIZE1OFporosity = nm
    ISIZE1OFtcm1 = nf
    ISIZE1OFdrftcm1 = nf
    ISIZE2OFtcm1 = nb_specie
    ISIZE2OFdrftcm1 = nb_specie
    ISIZE1OFmtc = nc_n
    ISIZE1OFdrfmtc = nc_n
    ISIZE2OFmtc = nm
    ISIZE2OFdrfmtc = nm
    ISIZE3OFmtc = nb_specie
    ISIZE3OFdrfmtc = nb_specie
    ISIZE1OFckm1 = 1
    ISIZE2OFckm1 = 1
    ISIZE1OFconc_old = nm
    ISIZE2OFconc_old = nb_specie
    ISIZE1OFmtc_old = nc_n
    ISIZE2OFmtc_old = nm
    ISIZE3OFmtc_old = nb_specie
    ISIZE1OFretard_t = 2
    ISIZE2OFretard_t = 1 !pb retimes(1) !!!
    ISIZE3OFretard_t = nb_zone
    ISIZE4OFretard_t = nb_specie
    ISIZE1OFlambda_i_splt = nb_specie
    ISIZE1OFsigma_ij = nb_specie
    ISIZE2OFsigma_ij = nb_specie
    ISIZE1OFtheta_lambda = nb_specie
    ISIZE1OFlambda_i = nb_specie
    ISIZE1OFomega_i = nb_specie
    ISIZE2OFomega_i = nb_zone
    ISIZE1OFc_sat_e = nb_elts
    ISIZE2OFc_sat_e = nb_zone
    ISIZE1OFfk_save = nm
    ISIZE1OFfkold = nm
    ISIZE1OFfps_hydro = nm
    ISIZE1OFval_src = nb_type_src
    ISIZE1OFdrfval_src = nb_type_src
    ISIZE1OFval_src_t = 2
    ISIZE2OFval_src_t = nb_time_src
    ISIZE3OFval_src_t = nb_type_src
    ISIZE1OFsolid_sp_zone = nb_zone_bil
    ISIZE1OFdrfsolid_sp_zone = nb_zone_bil
    ISIZE2OFsolid_sp_zone = nb_specie
    ISIZE2OFdrfsolid_sp_zone = nb_specie
    ISIZE1OFwater_zone_init = nb_zone_bil
    ISIZE1OFdrfwater_zone_init = nb_zone_bil
    ISIZE1OFconc_bound_zone = 2
    ISIZE1OFdrfconc_bound_zone = 2
    ISIZE2OFconc_bound_zone = nb_zone_bil
    ISIZE2OFdrfconc_bound_zone = nb_zone_bil
    ISIZE3OFconc_bound_zone = nb_specie
    ISIZE3OFdrfconc_bound_zone = nb_specie
    ISIZE1OFwater_zone = nb_zone_bil
    ISIZE1OFdrfwater_zone = nb_zone_bil
    ISIZE1OFsink_sp_zone = nb_zone_bil
    ISIZE1OFdrfsink_sp_zone = nb_zone_bil
    ISIZE2OFsink_sp_zone = nb_specie
    ISIZE2OFdrfsink_sp_zone = nb_specie
    ISIZE1OFconc_sp_zone_init = nb_zone_bil
    ISIZE1OFdrfconc_sp_zone_init = nb_zone_bil
    ISIZE2OFconc_sp_zone_init = nb_specie
    ISIZE2OFdrfconc_sp_zone_init = nb_specie
    ISIZE1OFad_conc_sp_zone = nb_zone_bil
    ISIZE1OFdrfad_conc_sp_zone = nb_zone_bil
    ISIZE2OFad_conc_sp_zone = nb_specie
    ISIZE2OFdrfad_conc_sp_zone = nb_specie
    ISIZE1OFconc_sp_zone = nb_zone_bil
    ISIZE1OFdrfconc_sp_zone = nb_zone_bil
    ISIZE2OFconc_sp_zone = nb_specie
    ISIZE2OFdrfconc_sp_zone = nb_specie
    ISIZE1OFflux_dc_surf = nb_surf
    ISIZE1OFdrfflux_dc_surf = nb_surf
    ISIZE2OFflux_dc_surf = 2
    ISIZE2OFdrfflux_dc_surf = 2
    ISIZE3OFflux_dc_surf = nb_specie+1
    ISIZE3OFdrfflux_dc_surf = nb_specie+1
    ISIZE1OFsink_wat_zone = nb_zone_bil
    ISIZE1OFdrfsink_wat_zone = nb_zone_bil
    ISIZE1OFwater_bound_zone = nb_zone_bil
    ISIZE1OFdrfwater_bound_zone = nb_zone_bil
    ISIZE1OFsink_sld_sp_zone = nb_zone_bil
    ISIZE1OFdrfsink_sld_sp_zone = nb_zone_bil
    ISIZE2OFsink_sld_sp_zone = nb_specie
    ISIZE2OFdrfsink_sld_sp_zone = nb_specie
    ISIZE1OFval_cal_t = 4
    ISIZE2OFval_cal_t = nb_time_cal
    ISIZE3OFval_cal_t = nb_type_cal
    ISIZE1OFval_cal = 3
    ISIZE1OFdrfval_cal = 3
    ISIZE2OFval_cal = nb_type_cal
    ISIZE2OFdrfval_cal = nb_type_cal
    ISIZE1OFche_src = nm
    ISIZE2OFche_src = nb_specie
    ISIZE1OFck_temp = nn
    ISIZE1OFom_del_pre = nm
    ISIZE1OFdrfom_del_pre = nm
    ISIZE1OFtpm1 = nf
    ISIZE1OFpkm1 = nm
    ISIZE1OFpk_save = nm

    ISIZE1OFalpha_t_th = 2
    ISIZE2OFalpha_t_th = nb_times
    ISIZE3OFalpha_t_th = nb_zone
    ISIZE4OFalpha_t_th = nb_specie
    ISIZE1OFalpha_tv =  nb_zone
    ISIZE1OFalpha_t_tv = 2
    ISIZE2OFalpha_t_tv = nb_times
    ISIZE3OFalpha_t_tv = nb_zone
    ISIZE4OFalpha_t_tv = nb_specie
    ISIZE1OFalpha_l = nb_zone
    ISIZE1OFdrfalpha_l = nb_zone
    ISIZE1OFdiff_c = nm
    ISIZE1OFdiff = nb_zone
    ISIZE1OFdrfdiff = nb_zone
    ISIZE1OFdiff_t = 2
    ISIZE2OFdiff_t = nb_times
    ISIZE3OFdiff_t = nb_zone
    ISIZE4OFdiff_t = nb_specie
    ISIZE1OFalpha_l_t = 2
    ISIZE2OFalpha_l_t = nb_times
    ISIZE3OFalpha_l_t = nb_zone
    ISIZE4OFalpha_l_t = nb_specie

    ISIZE1OFdrflc = nf
    ISIZE1OFvalcalhydro = 3
    ISIZE1OFdrfvalcalhydro = 3
    ISIZE2OFvalcalhydro = nb_type_cal
    ISIZE2OFdrfvalcalhydro = nb_type_cal
    ISIZE1OFvalsrchydro = nb_type_src
    ISIZE1OFdrfvalsrchydro = nb_type_src
    ISIZE1OFwhichcalhydro = nb_type_cal
    ISIZE1OFdrfwhichcalhydro = nb_type_cal
    ISIZE1OFwhich_cal = nb_type_cal
    ISIZE1OFdrfwhich_cal = nb_type_cal
    ISIZE1OFfac_limit = 2
    ISIZE1OFdrffac_limit = 2
    ISIZE2OFfac_limit = nf
    ISIZE2OFdrffac_limit = nf



    ISIZE2OFdiff_t_bin = 1
    ISIZE3OFdiff_t_bin = 2
    ISIZE4OFdiff_t_bin = 1
    ISIZE1OFporos_t_bin = 2
    ISIZE2OFporos_t_bin = 1
    ISIZE3OFporos_t_bin = 2
    ISIZE1OFvalsrcthydro = 2
    ISIZE2OFvalsrcthydro = NB_TIME_SRC
    ISIZE3OFvalsrcthydro = NB_TYPE_SRC
    ISIZE1OFvalcalthydro = 4
    ISIZE2OFvalcalthydro = NB_TIME_CAL
    ISIZE3OFvalcalthydro = NB_TYPE_CAL
    isize1ofvstr = nb_type_src
    isize1ofvsttr = 2
    isize2ofvsttr = nb_time_src
    isize3ofvsttr = nb_type_src
    isize1ofvctr = 3
    isize2ofvctr = nb_type_cal
    isize1ofvcttr = 4
    isize2ofvcttr = nb_time_cal
    isize3ofvcttr = nb_type_cal
    isize1ofmqhdy = nc_f
    isize2ofmqhdy = nf
    ISIZE1OFaamesh = 2*nc_f*nf
    isize1oftpoldhydro = nf
    isize1offvarcomm = nf
    isize1oflctr = nf
    isize1ofdrflctr = nf
    isize1ofdrfc_sat_e = nb_elts
    isize1ofdrfomega_i = nb_specie

    ISIZE1OFtemp =  nb_specie
    isize1ofdrflambda_i =  nb_specie
    isize1ofdrfpk=  nb_specie
    isize1ofdrfwrite_times=  nb_specie
    isize1ofdrftime_jalons=  nb_specie
    isize1ofdrfsorption =  nb_specie

    isize1ofdrfind_f_cum = nm
    isize1ofdrffps_hydro = nm
    isize1ofdrfaa = 2*nc_f*nf
    isize1ofdrffk_save = nm

  END SUBROUTINE INIT_DIFFSIZES
END MODULE DIFFSIZES
