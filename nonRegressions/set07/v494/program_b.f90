!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 28 Jun 2019 16:12
!
!  Differentiation of det in reverse (adjoint) mode (with options no!Activity =>no!MergeDiffInstrs no!StripPrimalModules):
!   gradient     of useful results: res
!   with respect to varying inputs: a
!   RW status of diff variables: res:in-zero a:incr
! from lh01
RECURSIVE SUBROUTINE DET_B(n, a, ab, k, m, res, resb)
  IMPLICIT NONE
  DOUBLE PRECISION :: res
  DOUBLE PRECISION :: resb
  INTEGER, INTENT(IN) :: k, m, n
  DOUBLE PRECISION, DIMENSION(n, n), INTENT(IN) :: a
  DOUBLE PRECISION, DIMENSION(n, n) :: ab
  DOUBLE PRECISION, DIMENSION(n) :: pt
  DOUBLE PRECISION, DIMENSION(n) :: ptb
  DOUBLE PRECISION :: t, resdet
  DOUBLE PRECISION :: tb, resdetb
  DOUBLE PRECISION, PARAMETER :: zero=0.
  DOUBLE PRECISION :: zerob=0.D0
  INTEGER :: p, s, p1, i
  INTRINSIC MOD
  INTEGER :: arg1
  INTEGER :: arg2
  INTEGER*4 :: branch
  IF (m .EQ. 0) THEN
    tb = 0.D0
    resdetb = 0.D0
    ptb = 0.D0
    resb = 0.D0
  ELSE
    DO i=1,n
      pt(i) = a(k, i)
    END DO
    p = 1
    IF (MOD(k, 2) .NE. 0) THEN
      s = 1
    ELSE
      s = -1
    END IF
    DO i=1,n
      p1 = 2*p
      IF (MOD(m, p1) .GE. p) THEN
        IF (m .EQ. 0) THEN
          IF (s .GT. 0) THEN
            CALL PUSHCONTROL3B(0)
          ELSE
            CALL PUSHCONTROL3B(1)
          END IF
        ELSE
          arg1 = k - 1
          CALL PUSHINTEGER4(arg2)
          arg2 = m - p
          CALL PUSHREAL8(resdet)
          CALL DET(n, a, arg1, arg2, resdet)
          IF (s .GT. 0) THEN
            CALL PUSHCONTROL3B(2)
          ELSE
            CALL PUSHCONTROL3B(3)
          END IF
        END IF
        s = -s
      ELSE
        CALL PUSHCONTROL3B(4)
      END IF
      p = p1
    END DO
    tb = 0.D0
    resdetb = 0.D0
    ptb = 0.D0
    tb = tb + resb
    resb = 0.D0
    DO 100 i=n,1,-1
      CALL POPCONTROL3B(branch)
      IF (branch .LT. 2) THEN
        IF (branch .EQ. 0) THEN
          ptb(i) = ptb(i) + tb
        ELSE
          ptb(i) = ptb(i) - tb
        END IF
      ELSE
        IF (branch .EQ. 2) THEN
          ptb(i) = ptb(i) + resdet*tb
          resdetb = resdetb + pt(i)*tb
        ELSE IF (branch .EQ. 3) THEN
          ptb(i) = ptb(i) - resdet*tb
          resdetb = resdetb - pt(i)*tb
        ELSE
          GOTO 100
        END IF
        arg1 = k - 1
        CALL POPREAL8(resdet)
        CALL DET_B(n, a, ab, arg1, arg2, resdet, resdetb)
        CALL POPINTEGER4(arg2)
      END IF
 100 CONTINUE
    zerob = tb
    tb = 0.D0
    DO i=n,1,-1
      ab(k, i) = ab(k, i) + ptb(i)
      ptb(i) = 0.D0
    END DO
  END IF
END SUBROUTINE DET_B

