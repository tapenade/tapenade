! from lh01
RECURSIVE SUBROUTINE det(n,A,k,m,res)
  DOUBLE PRECISION, INTENT(out) ::  res
  INTEGER, INTENT(in) ::  k,m,n
  DOUBLE PRECISION, DIMENSION(n,n), INTENT(in) :: A
  DOUBLE PRECISION, DIMENSION(n) :: pt
  DOUBLE PRECISION t, resdet
  DOUBLE PRECISION, PARAMETER :: zero = 0.
  INTEGER p,s,p1,i
  IF (m.EQ.0) THEN 
     res = 1.
  ELSE
     DO i = 1, n
        pt(i) = A(k,i)
     END DO
     t = zero
     p = 1
     IF (MOD(k,2).NE.0) THEN
        s = 1
     ELSE 
        s = -1
     END IF
     DO i = 1, n 
        p1 = 2*p
        IF (MOD(m,p1).GE.p) THEN
           IF (m.EQ.0) THEN 
              IF (s.GT.0) THEN
                 t = t + pt(i)
              ELSE
                 t = t - pt(i)
              END IF
           ELSE
              CALL det(n,A,k-1,m-p,resdet);
              IF (s.GT.0) THEN
                 t = t + pt(i)*resdet
              ELSE
                 t = t - pt(i)*resdet
              END IF
           END IF
           s = -s
        END IF
        p = p1
     END DO
     res = t
  END IF
END SUBROUTINE det

