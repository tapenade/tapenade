! from C lh21
subroutine suba(x,y)
  implicit none
  real, dimension(4) :: x
  real, dimension(5) :: y
  real, dimension(:), pointer :: lpp
  integer :: i
  allocate(lpp(4))
  do i = 1,4 
    lpp(i) = x(i)*(i-1)*y(i+1)
  end do
  do i = 1,4 
    y(i) = y(i+1)-lpp(i)
  end do
  deallocate(lpp)
end subroutine suba

! $AD NOCHECKPOINT
subroutine subb1(y,z,gpp)
  implicit none
  real, dimension(5) :: y
  real, dimension(:) :: z
  real, dimension(:), pointer :: gpp
  integer :: i
  real :: tmp
  allocate(gpp(10))
  do i = 1,10
    tmp = 1 + ((i-1)/2)
    gpp(i) = (i-1)*y(int(tmp))
  end do
  gpp(7) = gpp(7) * z(1)
end subroutine subb1

! $AD NOCHECKPOINT
real function subb2(z,gpp)
  implicit none
  real, dimension(:) :: z
  real, dimension(:), pointer :: gpp
  real :: res
  integer :: j
  res = 1.0
  do j = 1,4
    res = res * gpp(j) * z(6-j)
  end do
  do j = 1,7
    z(j) = 0.0
  end do
  deallocate(gpp)
  subb2 = res
end function subb2

subroutine testallocs(x,y)
  real, dimension(4) :: x
  real, dimension(5) :: y
  real, dimension(:), pointer :: z
  real, dimension(:), pointer :: pp
  integer :: i,j

  interface

    subroutine subb1(y,z,gpp)
      real, dimension(5) :: y
      real, dimension(:) :: z
      real, dimension(:), pointer :: gpp
    end subroutine subb1

    real function subb2(z,gpp)
      real, dimension(:) :: z
      real, dimension(:), pointer :: gpp
    end function subb2

  end interface

  x(3) = x(4) * y(2)
  allocate(z(7))
  do i = 1,7
    z(i) = x(3)*(i-1)
  end do
  call suba(x,y)
  do j = 2,3
    x(j) = x(j-1) * z(j)
  end do
  call subb1(y,z,pp)
  x(4) = pp(4) * pp(3)
  x(1) = x(1) * x(4)
  x(3) = subb2(z,pp)
end subroutine testallocs
