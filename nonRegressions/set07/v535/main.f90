program prog
  implicit none
  real, dimension(4) :: x
  real, dimension(5) :: y
  real :: summ = 0.0;
  integer :: i
  do i = 1,4
    x(i) = 1.0/i
  end do
  do i = 1,5
    y(i) = 2.0/(i+1.0)
  end do
  call testallocs(x,y)
  do i = 1,4
    summ = summ + x(i)
  end do
  do i = 1,5
    summ = summ + y(i)
  end do
  print*, 'Summ ', summ
end program prog
