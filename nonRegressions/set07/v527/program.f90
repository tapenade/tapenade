subroutine blammo(alpha, v, flattened_normals, nx, rhs)

implicit none

real(kind=8), intent(in) :: alpha, v, flattened_normals(nx, 3)
integer, intent(in) :: nx

real(kind=8), intent(out) :: rhs(nx)

integer :: ind
real(kind=8) :: a1, cosa, sina, v_inf(3), arr(3)

a1 = alpha * 3.14159265 / 180.
arr(1) = cos(a1)
arr(3) = sin(a1)
v_inf = v * arr

do ind=1,nx
  rhs(ind) = sum(-flattened_normals * v_inf)
end do

end subroutine
