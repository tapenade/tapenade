!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 20 Jun 2019 16:47
!
!  Differentiation of blammo in forward (tangent) mode:
!   variations   of useful results: rhs
!   with respect to varying inputs: v alpha flattened_normals rhs
!   RW status of diff variables: v:in alpha:in flattened_normals:in
!                rhs:in-out
SUBROUTINE BLAMMO_D(alpha, alphad, v, vd, flattened_normals, &
& flattened_normalsd, nx, rhs, rhsd)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nx
  REAL(kind=8), INTENT(IN) :: alpha, v, flattened_normals(nx, 3)
  REAL(kind=8), INTENT(IN) :: alphad, vd, flattened_normalsd(nx, 3)
  REAL(kind=8), INTENT(OUT) :: rhs(nx)
  REAL(kind=8), INTENT(OUT) :: rhsd(nx)
  INTEGER :: ind
  REAL(kind=8) :: a1, cosa, sina, v_inf(3), arr(3)
  REAL(kind=8) :: a1d, v_infd(3), arrd(3)
  INTRINSIC COS
  INTRINSIC SIN
  INTRINSIC SUM
  a1d = 3.14159265*alphad/180.
  a1 = alpha*3.14159265/180.
  arrd = 0.0_8
  arrd(1) = -(SIN(a1)*a1d)
  arr(1) = COS(a1)
  arrd(3) = COS(a1)*a1d
  arr(3) = SIN(a1)
  v_infd = arr*vd + v*arrd
  v_inf = v*arr
  DO ind=1,nx
    rhsd(ind) = SUM(-(v_inf*flattened_normalsd+flattened_normals*v_infd)&
&     )
    rhs(ind) = SUM(-(flattened_normals*v_inf))
  END DO
END SUBROUTINE BLAMMO_D

