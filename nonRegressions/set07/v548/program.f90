module test
 private
 real, public :: foo = 2.0
end module test

subroutine top(x)
  use test
  x = x * foo
end subroutine top
