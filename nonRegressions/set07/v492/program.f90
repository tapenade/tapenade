module m1
 real, dimension(:), pointer :: global
end module m1

module m_t
  interface traiter
    module procedure traiter
    module procedure traiter1
    module procedure traiter2
  end interface traiter
contains

subroutine traiter(erreur)
use m1
real :: erreur
print*, 'call traiter ', erreur
allocate(global(1))
global(1) = erreur * 10
erreur = erreur * global(1)
end subroutine traiter

subroutine traiter1(erreur, a)
real, dimension(1) :: erreur
integer:: a
print*, 'call traiter1 ', erreur
end subroutine traiter1

subroutine traiter2(erreur, a, b)
real, dimension(1,1) :: erreur
integer:: a,b
end subroutine traiter2

subroutine top(x)
use m1
real :: x
x = x * x * global(1)
end subroutine top

subroutine context(x)
use m1
real :: x
call traiter(x)
call traiter(global, 1)
call top(x)
x = x * global(1)
end subroutine context

end module m_t

program main
use m_t
real :: x
x = 2.0
call context(x)
end program main
