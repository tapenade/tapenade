!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 15:32
!
MODULE M1_DIFF
  IMPLICIT NONE
  REAL, DIMENSION(:), POINTER :: global
  REAL, DIMENSION(:), POINTER :: globald
END MODULE M1_DIFF

MODULE M_T_DIFF
  IMPLICIT NONE
  INTERFACE TRAITER
      MODULE PROCEDURE TRAITER
      MODULE PROCEDURE TRAITER1
      MODULE PROCEDURE TRAITER2
  END INTERFACE TRAITER

  INTERFACE TRAITER_D
      MODULE PROCEDURE TRAITER_D
  END INTERFACE


CONTAINS
!  Differentiation of traiter as a context to call tangent code (with options context):
!   Plus diff mem management of: global:out
  SUBROUTINE TRAITER_D(erreur)
    USE M1_DIFF
    IMPLICIT NONE
    REAL :: erreur
    PRINT*, 'call traiter ', erreur
    ALLOCATE(globald(1))
    globald = 0.0
    ALLOCATE(global(1))
    global(1) = erreur*10
    erreur = erreur*global(1)
  END SUBROUTINE TRAITER_D

  SUBROUTINE TRAITER(erreur)
    USE M1_DIFF
    IMPLICIT NONE
    REAL :: erreur
    PRINT*, 'call traiter ', erreur
    ALLOCATE(global(1))
    global(1) = erreur*10
    erreur = erreur*global(1)
  END SUBROUTINE TRAITER

  SUBROUTINE TRAITER1(erreur, a)
    IMPLICIT NONE
    REAL, DIMENSION(1) :: erreur
    INTEGER :: a
    PRINT*, 'call traiter1 ', erreur
  END SUBROUTINE TRAITER1

  SUBROUTINE TRAITER2(erreur, a, b)
    IMPLICIT NONE
    REAL, DIMENSION(1, 1) :: erreur
    INTEGER :: a, b
  END SUBROUTINE TRAITER2

!  Differentiation of top in forward (tangent) mode (with options context):
!   variations   of useful results: x
!   with respect to varying inputs: [alloc*global in traiter] *global
!                x
!   RW status of diff variables: [alloc*global in traiter]:in *global:in
!                x:in-out
!   Plus diff mem management of: global:in
  SUBROUTINE TOP_D(x, xd)
    USE M1_DIFF
    IMPLICIT NONE
    REAL :: x
    REAL :: xd
    xd = global(1)*2*x*xd + x**2*globald(1)
    x = x*x*global(1)
  END SUBROUTINE TOP_D

  SUBROUTINE TOP(x)
    USE M1_DIFF
    IMPLICIT NONE
    REAL :: x
    x = x*x*global(1)
  END SUBROUTINE TOP

!  Differentiation of context as a context to call tangent code (with options context):
  SUBROUTINE CONTEXT_D(x)
    USE M1_DIFF
    USE DIFFSIZES
!  Hint: ISIZE1OFDrfglobal should be the size of dimension 1 of array *global
    IMPLICIT NONE
    REAL :: x
    REAL :: xd
    CALL TRAITER_D(x)
    CALL TRAITER(global, 1)
    CALL ADCONTEXTTGT_INIT(1.e-4_8, 0.87_8)
    IF (ASSOCIATED(global)) CALL ADCONTEXTTGT_INITREAL4ARRAY('global'//&
&                                                      CHAR(0), global, &
&                                                      globald, &
&                                                      ISIZE1OFDrfglobal&
&                                                     )
    CALL ADCONTEXTTGT_INITREAL4('x'//CHAR(0), x, xd)
    CALL TOP_D(x, xd)
    CALL ADCONTEXTTGT_STARTCONCLUDE()
    IF (ASSOCIATED(global)) CALL ADCONTEXTTGT_CONCLUDEREAL4ARRAY(&
&                                                          'global'//&
&                                                          CHAR(0), &
&                                                          global, &
&                                                          globald, &
&                                                      ISIZE1OFDrfglobal&
&                                                         )
    CALL ADCONTEXTTGT_CONCLUDEREAL4('x'//CHAR(0), x, xd)
    CALL ADCONTEXTTGT_CONCLUDE()
    x = x*global(1)
  END SUBROUTINE CONTEXT_D

  SUBROUTINE CONTEXT(x)
    USE M1_DIFF
    IMPLICIT NONE
    REAL :: x
    CALL TRAITER(x)
    CALL TRAITER(global, 1)
    CALL TOP(x)
    x = x*global(1)
  END SUBROUTINE CONTEXT

END MODULE M_T_DIFF

!  Differentiation of main as a context to call tangent code (with options context):
PROGRAM MAIN_D
  USE M_T_DIFF
  IMPLICIT NONE
  REAL :: x
  x = 2.0
  CALL CONTEXT_D(x)
END PROGRAM MAIN_D

SUBROUTINE MAIN_NODIFF()
  USE M_T_DIFF
  IMPLICIT NONE
  REAL :: x
  x = 2.0
  CALL CONTEXT(x)
END SUBROUTINE MAIN_NODIFF

