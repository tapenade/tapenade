module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

module M1_I
   ! interface avec des noms de variables differents de la fonction
   ! program_b.f90 ne compile pas, mais message dans program_b.msg
   interface
      function compute (xi,y,z)
       use M
       implicit none
       real, dimension(n) :: xi,y
       real :: compute, z
      end function compute
   end interface
end module M1_I

function compute(x,y,z)
  use M
  implicit none
  real, dimension(n) :: x,y
  real :: compute, z
  y = 2 * x
  compute = y(1) * y(2)
  global = global + compute
  z = global
end function compute

program prog
use M
use M1_I
implicit none
real, dimension(n) :: r,s
r(1) = 3.0
s(1) = 0.0
r(2) = 2.0
s(2) = 0.0
print*,compute(r,s,r(1))
end program prog
