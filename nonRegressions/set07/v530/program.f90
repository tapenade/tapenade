! pour tester intent(inout) dans interface sur parametre differentie' local

module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M


program prog
use M
implicit none
real, dimension(n) :: r,s
real, dimension(:), pointer :: z => null()
r(1) = 3.0
s(1) = 0.0
r(2) = 2.0
s(2) = 0.0
  allocate(z(1))
print*,toplevel()
  deallocate(z)

contains
function toplevel()
  use M
  implicit none
  real :: toplevel

  call rez(r,s)
  toplevel = s(1) * s(2) 
  global = global + toplevel
  z(1) = global

end function toplevel

end program prog

subroutine rez(r,s)
use M
real, dimension(n) :: r,s
s = r * s
end subroutine rez
