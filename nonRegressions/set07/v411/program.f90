! Burgers 1D PDE
! Written by Michael Herty
! Translated to Fortran by Johannes Willkomm

module burgers
contains

function limiter(a, b) result(z)
  implicit none
  real(8) :: a, b, z, s
  dimension :: a(:), b(:), s(size(a))
  dimension :: z(size(a))
  s = sign(1d0, a)
  z = s*max(0d0, min(abs(a), s*b))
end function limiter

subroutine dxsp(unew,vnew,a,dxu,dxv)
  implicit none
  real(8) :: unew, vnew, a, wp, wm, wpi, wmi, dxu, dxv
  real(8) :: sigmap, sigmam, ui, vi
  integer :: Nx

  dimension :: unew(:), vnew(:), wp(size(unew)), wm(size(unew))
  dimension :: wpi(ubound(unew,1)+1), wmi(ubound(unew,1)+1)
  dimension :: sigmap(ubound(unew,1)+1), sigmam(ubound(unew,1)+1)
  dimension :: ui(ubound(unew,1)+1), vi(ubound(unew,1)+1)
  dimension :: dxu(size(unew)), dxv(size(unew))

  Nx = ubound(unew,1)

  ! compute difference for the transported variables wpm = v+- a u  
  wp  =   vnew + a * unew 
  wm  =   vnew - a * unew

  ! BEGIN. First-order
  ! fluxes wpi-1/2
  wpi = [ wp(Nx), wp ]
  wmi = [ wm,  wm(1) ]
  ! END


  ! BEGIN. Second-order
  sigmap      =   limiter( [wp, wp(1)] - [wp(Nx), wp], [wp(Nx), wp] - [wp(Nx-1), wp(Nx), wp(1:Nx-1)] )
  wpi         =   wpi + 0.5* sigmap   

  wmi         =   wmi - 0.5 * sigmam   
  ! END

  ! recover discr for u and v: i-1/2
  vi           =   ( wpi + wmi) / 2.e00 
  ui           =   ( wpi - wmi ) / a / 2.e00

  ! difference quotients
  dxv    =  vi(2:Nx+1) - vi(1:Nx)
  dxu    =  ui(2:Nx+1) - ui(1:Nx)
end subroutine dxsp

end module burgers


