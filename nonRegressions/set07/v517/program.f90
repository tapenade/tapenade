! from v294, test interface et intent differents entre interface et definition
module real_precision

	IMPLICIT NONE

	INTEGER, PARAMETER :: sp = SELECTED_REAL_KIND(6, 37)  !< single precision (real 4)
	INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(12,307) !< double precision (real 8)
	INTEGER, PARAMETER :: wp = dp                         !< working precision

end module real_precision


MODULE type_assimilation

USE real_precision

IMPLICIT NONE

TYPE ctlvec                                
    INTEGER :: nsize 
    REAL :: xdata    
END TYPE ctlvec

END MODULE type_assimilation


MODULE assim_params

USE real_precision
USE type_assimilation

IMPLICIT NONE

TYPE(ctlvec), SAVE :: x0ref 

INTERFACE
	SUBROUTINE interface_simulator_observations_tap(xctlvec)
		USE real_precision
		USE type_assimilation
    	TYPE(ctlvec), intent(inout) :: xctlvec  !< vecteur d'entree
	END SUBROUTINE interface_simulator_observations_tap
END INTERFACE

private intf_private

interface intf_private
    subroutine test(x)
       USE real_precision
       USE type_assimilation
       real x
    end subroutine test
end interface

interface avec_deux_fonctions
   subroutine f1(x)
     USE real_precision
     USE type_assimilation
     real x
   end subroutine f1

   subroutine f2(x)
     real, dimension(2) ::  x
   end subroutine f2
end interface

REAL :: J_cost !< fonction cout totale

END MODULE assim_params


SUBROUTINE interface_simulator_observations_tap(xctlvec)
	
    use real_precision
    USE assim_params, only: avec_deux_fonctions, ctlvec

    IMPLICIT NONE
        
    ! Arguments
    TYPE(ctlvec), intent(out) :: xctlvec   !< vecteur d'entree
    call avec_deux_fonctions(xctlvec%xdata)

END SUBROUTINE interface_simulator_observations_tap

SUBROUTINE cost_winnie_tapenade

	use real_precision
	USE assim_params

	IMPLICIT NONE

    CALL interface_simulator_observations_tap(x0ref)
    J_cost = 0.
	
END SUBROUTINE cost_winnie_tapenade



subroutine f1(x)
USE real_precision
USE type_assimilation
real x
 x = 2 * x
end subroutine f1

subroutine f2(x)
real x
 x = x * x
end subroutine f2
