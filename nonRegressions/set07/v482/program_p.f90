!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.10 (r5435M) - 18 Nov 2014 09:32
!
SUBROUTINE TEST(x, y)
  IMPLICIT NONE
  REAL :: x, y
  EXTERNAL SARAP
  REAL :: etatz
  REAL :: erreur
! Donnees/Resultats
! Cote de la surface libre
! Erreur
  CALL SARAP(etatz, erreur)
END SUBROUTINE TEST

MODULE M
  IMPLICIT NONE
  CHARACTER(len=*), PARAMETER :: err_4='&
&("Erreur a l''ouverture du fichier : ",A,/,                   &
& "en ecriture.",/,                                            &
& "Verifier l''existence du chemin specifie.")'
END MODULE M

