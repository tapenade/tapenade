subroutine TEST    ( &

     X        , &
     Y          &
                      )
  real                 :: X , Y
  external SARAP

         call SARAP       ( &

        ! Donnees/Resultats
         EtatZ            , & ! Cote de la surface libre
         Erreur                & ! Erreur
              )

end subroutine TEST


module M
character (*), parameter ::                                             &
err_4  = '("Erreur a l''ouverture du fichier : ",A,/,                   &
         & "en ecriture.",/,                                            &
         & "Verifier l''existence du chemin specifie.")'
end module M
