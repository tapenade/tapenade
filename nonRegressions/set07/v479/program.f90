module MMA

TYPE ETAT_T
  real :: v
END TYPE ETAT_T

  TYPE(ETAT_T), POINTER :: avv1,aa1
  TYPE(ETAT_T), ALLOCATABLE :: avv2,aa2
  TYPE(ETAT_T), TARGET :: davv1,daa1

contains
  SUBROUTINE AINIT()
    avv1=>davv1
    aa1=>daa1
    ALLOCATE(avv2)
    ALLOCATE(aa2)
  END SUBROUTINE AINIT

  SUBROUTINE INIT(x)
    TYPE(ETAT_T) :: x
    avv1=>davv1
    aa1=>daa1
    x%v = 0.0
  END SUBROUTINE INIT

end module MMA


MODULE MMB
  use MMA
  TYPE ETAT_M
     real :: w
     type(ETAT_T),dimension(:), pointer :: Liaisons => null()
  END TYPE ETAT_M
  TYPE(ETAT_M),POINTER :: vv1,bb1
  TYPE(ETAT_M),POINTER :: vv2,bb2
  TYPE(ETAT_M),TARGET :: dvv1,dbb1
CONTAINS
  SUBROUTINE BINIT(x)
    TYPE(ETAT_M) :: x
    vv1=>dvv1
    bb1=>dbb1
    ALLOCATE(vv2)
    ALLOCATE(bb2)
    call INIT(x%liaisons(1))
    call INIT(bb2%liaisons(1))
  END SUBROUTINE BINIT
END MODULE MMB


SUBROUTINE GLOBSUBA(xx)
  USE MMA
  REAL xx
  aa2%v = aa2%v*aa2%v
  avv1%v = avv1%v*xx
  avv2%v = 2.0*avv2%v
  xx = xx*avv1%v*avv2%v
END SUBROUTINE GLOBSUBA

SUBROUTINE GLOBSUBB(xx)
  USE MMB
  REAL xx
  call BINIT(vv2)
  bb2%w = bb2%w*bb2%w
  vv1%w = vv1%w*xx
  vv2%w = 2.0*vv2%w
  xx = xx*vv1%w*vv2%w
END SUBROUTINE GLOBSUBB

SUBROUTINE TOP(uu,tt)
  USE MMA, ONLY:avv1,avv2,aa1,aa2
  USE MMB, ONLY:vv1,vv2,bb1,bb2
  REAL uu,tt
  call GLOBSUBA(uu)
  call GLOBSUBB(tt)
  tt = uu*tt
END SUBROUTINE TOP
