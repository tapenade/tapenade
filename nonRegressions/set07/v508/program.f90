! from huneeus validation, pb -nooptim saveonlyused
subroutine nl_lidrad_mie_orig(r_0,M_conc,lid_sig,rad,io)
implicit none
double precision, dimension(3) :: M_conc
double precision, dimension(4) :: rad
double precision, dimension(3, 5) :: lid_sig
double precision :: PP(0:1,1:4)
integer :: lev, iter_wl, io
double precision :: alpha_ext(1:4)
double precision :: od, aux, aer, r_0, ext
real :: pi, nu
external nl_model_mie_orig

pi=ATAN(1.)*4.
nu=cos(50.*pi/180.)

call nl_model_mie_orig(r_0,alpha_ext,PP)
do iter_wl=1, 5
   od=0.
   do lev=3,1,-1
      ext=M_conc(lev)*alpha_ext(iter_wl)
      od=od+ext
      aux=-2.*od
      aer=1.*PP(1,iter_wl)*ext
!      aer=1.*phf180*ext
      lid_sig(lev,iter_wl)=aer*exp(aux)
   enddo
enddo

do iter_wl=1, 4
   od=0.
   do lev=1, 3
      ext=M_conc(lev)*alpha_ext(5+iter_wl)
      od=od+ext
   enddo
!   rad(iter_wl)=cte_rad*1.*PP(0,5+iter_wl)*od*f/(4.*pi*nu)
enddo
return
end
