module my_subs

contains

function sin (x)
real :: sin
real, intent (in) :: x

sin = x**2

return
end function sin

end module my_subs


function other(x)
real :: x, other
other = sin(x)
write (*,*) x, other
end function other

program test_sin

use my_subs

implicit none
real :: other

write (*, *) sin (2.0)

write (*, *) other(2.0)

stop

end program test_sin

