SUBROUTINE p(rho, ec1, ec2)
    IMPLICIT NONE
    REAL :: rho, ec1,ec2
    optional ec1,ec2
    IF (PRESENT(ec1) .and. PRESENT(ec2)) rho = rho * ec1 * ec2
END SUBROUTINE p
