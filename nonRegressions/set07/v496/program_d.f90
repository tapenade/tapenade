!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 14 Jun 2021 12:55
!
MODULE M_DIFF
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=2
  REAL :: global
  REAL :: globald
END MODULE M_DIFF

FUNCTION TOP_NODIFF(r, s) RESULT (top)
  USE M_DIFF
  IMPLICIT NONE
  REAL, DIMENSION(n) :: r, s
  REAL :: top
  INTERFACE 
      FUNCTION COMPUTE_NODIFF(x, y) RESULT (compute)
        USE M_DIFF
        IMPLICIT NONE
        REAL, DIMENSION(n) :: x, y
        REAL :: compute
      END FUNCTION COMPUTE_NODIFF
  END INTERFACE

  REAL, EXTERNAL :: FTEST
  top = FTEST(r, s, COMPUTE_NODIFF)
END FUNCTION TOP_NODIFF

!  Differentiation of top in forward (tangent) mode (with options no!Activity =>no!MergeDiffInstrs no!StripPrimalModules):
!   variations   of useful results: r s top
!   with respect to varying inputs: r s
!   RW status of diff variables: r:in-out s:in-out top:out
REAL FUNCTION TOP_D(r, rd, s, sd, top)
  USE M_DIFF
  IMPLICIT NONE
  REAL, DIMENSION(n) :: r, s
  REAL, DIMENSION(n) :: rd, sd
  REAL :: top
  INTERFACE 
      FUNCTION COMPUTE_NODIFF(x, y) RESULT (compute)
        USE M_DIFF
        IMPLICIT NONE
        REAL, DIMENSION(n) :: x, y
        REAL :: compute
      END FUNCTION COMPUTE_NODIFF
  END INTERFACE

  REAL, EXTERNAL :: FTEST
  REAL, EXTERNAL :: FTEST_D
  EXTERNAL COMPUTE_D
  top_d = FTEST_D(r, rd, s, sd, COMPUTE_NODIFF, COMPUTE_D, top)
END FUNCTION TOP_D

