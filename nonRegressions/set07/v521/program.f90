module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

module M1_I
   interface
      function compute (x,y)
       use M
       implicit none
       real, dimension(:), pointer :: x,y
       real :: compute
      end function compute
   end interface
end module M1_I

function compute(x,y)
  use M
  implicit none
  real, dimension(:), pointer :: x,y
  real :: compute
  y = 2 * x
  compute = y(1) * y(2)
  global = global + compute
end function compute

subroutine top(x,y)
  use M
  use M1_I
  implicit none
  real, dimension(:), pointer :: x,y
  allocate(y(5))
  y(1) = compute(x,y)
  print*,y(1)
end subroutine top

program prog
use M
use M1_I
implicit none
real, dimension(:), pointer :: r,s
   interface
      subroutine top (x,y)
       use M
       use M1_I
       implicit none
       real, dimension(:), pointer :: x,y
      end subroutine top
   end interface

allocate(r(5))
allocate(s(5))
r(1) = 3.0
s(1) = 0.0
r(2) = 2.0
s(2) = 0.0
call top(r,s)
end program prog
