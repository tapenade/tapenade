!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4799M) -  7 Mar 2013 10:35
!
MODULE CONST
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=2
END MODULE CONST

MODULE TEST
  IMPLICIT NONE
  INTERFACE 
      SUBROUTINE SUB(x)
        USE CONST
        REAL, DIMENSION(n) :: x
      END SUBROUTINE SUB
  END INTERFACE

END MODULE TEST

