module test
  implicit none
  interface
   subroutine sub(x)
     use const
     real, dimension(n) :: x
   end subroutine sub
  end interface
end module test

module const
  integer, parameter :: n = 2
end module const
