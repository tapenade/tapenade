subroutine ABug(x,nx,c)
implicit none
integer,intent(in) :: nx
real(8),target,intent(in) :: x(nx)
real(8),intent(out) :: c

real(8),pointer :: y(:),z
y => x(1:3)
z => x(nx)
c=z+y(3)
end subroutine ABug
