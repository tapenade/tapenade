subroutine test(x, values, numDims)
     real                                 :: x
     real, dimension(:, :, :, :, :, :, :) :: values
     integer                              :: numDims
     numDims                 = size(shape(values))
     values(:, :, :, :, :, :, :) = x
end subroutine test
