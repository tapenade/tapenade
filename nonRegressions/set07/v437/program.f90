module M_PRECISION
   implicit none
   integer, parameter :: DOUBLE = SELECTED_REAL_KIND(15,100)
end module M_PRECISION

module M_FCONJ_I
   interface

   function FCONJ( &
           TIRANT  & ! Tirant d'eau
                  )

   use M_PRECISION

   implicit none

   real(DOUBLE)                                  :: FCONJ
   real(DOUBLE),                   intent(in)    :: TIRANT

   integer      :: ALPHA
   real(DOUBLE) :: A1,A2,A3
   common /COEFS/ ALPHA
   common /COEFH/ A1,A2,A3

   end function FCONJ

   end interface

end module M_FCONJ_I


   function FCONJ( &
           TIRANT  & ! Tirant d'eau
                  )

   use M_PRECISION

   implicit none

   real(DOUBLE)                                  :: FCONJ, local
   real(DOUBLE),                   intent(in)    :: TIRANT

   integer      :: ALPHA
   real(DOUBLE) :: A1,A2,A3
   common /COEFS/ ALPHA
   common /COEFH/ A1,A2,A3

   local = 2
   FCONJ = local * TIRANT

   end function FCONJ
