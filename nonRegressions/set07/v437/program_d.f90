!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 28 Apr 2021 16:32
!
MODULE M_PRECISION_DIFF
  IMPLICIT NONE
  INTEGER, PARAMETER :: double=SELECTED_REAL_KIND(15, 100)
END MODULE M_PRECISION_DIFF

MODULE M_FCONJ_I_DIFF
  IMPLICIT NONE
  INTERFACE 
      FUNCTION FCONJ_NODIFF(tirant) RESULT (fconj)
        USE M_PRECISION_DIFF
        IMPLICIT NONE
        DOUBLE PRECISION :: fconj
        DOUBLE PRECISION, INTENT(IN) :: tirant
        INTEGER :: alpha
        DOUBLE PRECISION :: a1, a2, a3
        COMMON /coefs/ alpha
        COMMON /coefh/ a1, a2, a3
      END FUNCTION FCONJ_NODIFF
  END INTERFACE

  INTERFACE 
      DOUBLE PRECISION FUNCTION FCONJ_D(tirant, tirantd, fconj)
        USE M_PRECISION_DIFF
        IMPLICIT NONE
        DOUBLE PRECISION :: fconj
        DOUBLE PRECISION, INTENT(IN) :: tirant
        DOUBLE PRECISION, INTENT(IN) :: tirantd
        INTEGER :: alpha
        DOUBLE PRECISION :: a1, a2, a3
        COMMON /coefs/ alpha
        COMMON /coefh/ a1, a2, a3
      END FUNCTION FCONJ_D
  END INTERFACE

END MODULE M_FCONJ_I_DIFF

!  Differentiation of fconj in forward (tangent) mode (with options no!StripPrimalModules):
!   variations   of useful results: fconj
!   with respect to varying inputs: tirant
!   RW status of diff variables: fconj:out tirant:in
! Tirant d'eau
DOUBLE PRECISION FUNCTION FCONJ_D(tirant, tirantd, fconj)
  USE M_PRECISION_DIFF
  IMPLICIT NONE
  DOUBLE PRECISION :: fconj, local
  DOUBLE PRECISION, INTENT(IN) :: tirant
  DOUBLE PRECISION, INTENT(IN) :: tirantd
  INTEGER :: alpha
  DOUBLE PRECISION :: a1, a2, a3
  COMMON /coefs/ alpha
  COMMON /coefh/ a1, a2, a3
  local = 2
  fconj_d = local*tirantd
  fconj = local*tirant
END FUNCTION FCONJ_D

! Tirant d'eau
FUNCTION FCONJ_NODIFF(tirant) RESULT (fconj)
  USE M_PRECISION_DIFF
  IMPLICIT NONE
  DOUBLE PRECISION :: fconj, local
  DOUBLE PRECISION, INTENT(IN) :: tirant
  INTEGER :: alpha
  DOUBLE PRECISION :: a1, a2, a3
  COMMON /coefs/ alpha
  COMMON /coefh/ a1, a2, a3
  local = 2
  fconj = local*tirant
END FUNCTION FCONJ_NODIFF

