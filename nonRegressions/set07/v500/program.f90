module residual

  implicit none

  integer, parameter :: dim = 3
  
contains

subroutine foo(n,b,J)
  implicit none
  integer,                   intent(in)  :: n
  real(8), dimension(dim,n), intent(in)  :: b
  real(8),                   intent(out) :: J

  call bar(n,b(1,:),J)

end subroutine foo


subroutine bar(n,b,J)
  implicit none
  integer,               intent(in)  :: n
  real(8), dimension(n), intent(in)  :: b
  real(8),               intent(out) :: J
  ! Local variables
  integer :: i

  J = 0.d0
  do i=1, n
    J =  J + b(i)
  end do
 
end subroutine bar


end module residual
