module g1
  REAL(KIND=8), dimension(:), pointer :: z, cf1
end module g1

program top
  use g1
  real(KIND=8) :: x
  interface 
    function foo(cf2)
     REAL(KIND=8), dimension(:), pointer :: cf2
     REAL(KIND=8) :: foo
    end function foo
  end interface
  allocate(cf1(99))
  allocate(z(88))
  z = 1.d0
  x = foo(cf1)
  deallocate(cf1)
end program top

function foo(cf2)
  REAL(KIND=8), dimension(:), pointer :: cf2
  real(KIND=8) :: x, foo
  interface 
    subroutine bar(cf3, x)
     REAL(KIND=8), dimension(:) :: cf3
!     REAL(KIND=8), dimension(:), pointer :: cf3
     real(KIND=8) :: x
    end subroutine bar
  end interface
  x = 5
  call bar(cf2, x)
  foo = x
end function foo

subroutine bar(cf3, x)
  use g1
  REAL(KIND=8), dimension(:) :: cf3
!  REAL(KIND=8), dimension(:), pointer :: cf3
  real(KIND=8) :: x
  x = x * cf3(5)
  z = z * x
end subroutine bar

