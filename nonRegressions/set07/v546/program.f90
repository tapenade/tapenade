module sing
  TYPE SINGULARITE_T
      REAL, DIMENSION(:), POINTER :: ptz => NULL()
  END TYPE SINGULARITE_T
  real glob
end module sing


module m
use sing
contains
subroutine top(x,y,singularite)
use sing
type(singularite_t) :: singularite
real, DIMENSION(:) ::  x,y
external toto
call foo (x*glob,y,singularite%ptz)
end subroutine top

subroutine foo(x,y,z)
real, DIMENSION(:) :: x,y
real, DIMENSION(:), intent(in) :: z
call bar(x,y)
call bar(z,y)
end

subroutine bar(a,b)
real, DIMENSION(:) :: a,b
b = a*b
end 

end module m
