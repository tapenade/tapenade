MODULE real_precision 
 IMPLICIT NONE
  INTEGER, PARAMETER :: RP = SELECTED_REAL_KIND(8) 
END MODULE real_precision


subroutine f(a,b,c,phalf,p_surface,dim_z,gridX,gridY,x,y)
 use real_precision
 INTEGER dim_z
 REAL(RP) Phalf(0:DIM_Z)
 REAL(RP) A(DIM_Z + 1)
 REAL(RP) B(DIM_Z + 1)
 REAL(RP) C(DIM_Z + 1)
 REAL(RP) P_Surface(gridX,gridY)
 REAL(RP) x
 REAL(RP) y
 INTEGER gridX,gridY
 DO i = 1, gridX
    DO j = 1, gridY
     Phalf(:) =  A(:) + x * B(:) * P_Surface(i,j) * C(:)
    ENDDO
 ENDDO
end

subroutine g(a,b,c,phalf,p_surface,dim_z,gridX,gridY,x,y)
 use real_precision
 INTEGER dim_z
 REAL(RP) Phalf(0:DIM_Z)
 REAL(RP) A(DIM_Z + 1)
 REAL(RP) B(DIM_Z + 1)
 REAL(RP) C(DIM_Z + 1)
 REAL(RP) P_Surface(gridX,gridY)
 REAL(RP) x
 REAL(RP) y
 INTEGER gridX,gridY
 INTEGER i,j
 Phalf(i) =  (A(i) + x * B(i) * P_Surface(i,j) * C(i)) / 2._RP
end
