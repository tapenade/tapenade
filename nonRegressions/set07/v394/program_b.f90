!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) -  1 Aug 2023 09:28
!
!  Differentiation of f in reverse (adjoint) mode:
!   gradient     of useful results: x p_surface phalf a b c
!   with respect to varying inputs: x p_surface phalf a b c
!   RW status of diff variables: x:incr p_surface:incr phalf:in-out
!                a:incr b:incr c:incr
SUBROUTINE F_B(a, ab, b, bb, c, cb, phalf, phalfb, p_surface, p_surfaceb&
& , dim_z, gridx, gridy, x, xb, y)
  USE REAL_PRECISION
  IMPLICIT NONE
  INTEGER :: dim_z
  REAL(rp) :: phalf(0:dim_z)
  REAL(rp) :: phalfb(0:dim_z)
  REAL(rp) :: a(dim_z+1)
  REAL(rp) :: ab(dim_z+1)
  REAL(rp) :: b(dim_z+1)
  REAL(rp) :: bb(dim_z+1)
  REAL(rp) :: c(dim_z+1)
  REAL(rp) :: cb(dim_z+1)
  INTEGER :: gridx, gridy
  REAL(rp) :: p_surface(gridx, gridy)
  REAL(rp) :: p_surfaceb(gridx, gridy)
  REAL(rp) :: x
  REAL(rp) :: xb
  REAL(rp) :: y
  INTEGER :: i
  INTEGER :: j
  REAL(rp), DIMENSION(dim_z+1) :: tempb
  REAL(rp), DIMENSION(dim_z+1) :: tempb0
  DO i=gridx,1,-1
    DO j=gridy,1,-1
      ab = ab + phalfb
      tempb = p_surface(i, j)*c*phalfb
      tempb0 = x*b*phalfb
      p_surfaceb(i, j) = p_surfaceb(i, j) + SUM(c*tempb0)
      cb = cb + p_surface(i, j)*tempb0
      xb = xb + SUM(b*tempb)
      bb = bb + x*tempb
      phalfb = 0.0
    END DO
  END DO
END SUBROUTINE F_B

!  Differentiation of g in reverse (adjoint) mode:
!   gradient     of useful results: x p_surface phalf a b c
!   with respect to varying inputs: x p_surface phalf a b c
!   RW status of diff variables: x:incr p_surface:incr phalf:in-out
!                a:incr b:incr c:incr
SUBROUTINE G_B(a, ab, b, bb, c, cb, phalf, phalfb, p_surface, p_surfaceb&
& , dim_z, gridx, gridy, x, xb, y)
  USE REAL_PRECISION
  IMPLICIT NONE
  INTEGER :: dim_z
  REAL(rp) :: phalf(0:dim_z)
  REAL(rp) :: phalfb(0:dim_z)
  REAL(rp) :: a(dim_z+1)
  REAL(rp) :: ab(dim_z+1)
  REAL(rp) :: b(dim_z+1)
  REAL(rp) :: bb(dim_z+1)
  REAL(rp) :: c(dim_z+1)
  REAL(rp) :: cb(dim_z+1)
  INTEGER :: gridx, gridy
  REAL(rp) :: p_surface(gridx, gridy)
  REAL(rp) :: p_surfaceb(gridx, gridy)
  REAL(rp) :: x
  REAL(rp) :: xb
  REAL(rp) :: y
  INTEGER :: i, j
  REAL(rp) :: tempb
  REAL(rp) :: tempb0
  REAL(rp) :: tempb1
  tempb = phalfb(i)/2._RP
  phalfb(i) = 0.0
  ab(i) = ab(i) + tempb
  tempb0 = p_surface(i, j)*c(i)*tempb
  tempb1 = x*b(i)*tempb
  p_surfaceb(i, j) = p_surfaceb(i, j) + c(i)*tempb1
  cb(i) = cb(i) + p_surface(i, j)*tempb1
  xb = xb + b(i)*tempb0
  bb(i) = bb(i) + x*tempb0
END SUBROUTINE G_B

