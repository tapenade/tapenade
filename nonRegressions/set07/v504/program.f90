module real_precision
	IMPLICIT NONE
	INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(12,307) 
end module real_precision

MODULE m_type
USE real_precision
IMPLICIT NONE
TYPE ctlvec                                
    REAL(KIND=wp), POINTER, DIMENSION(:) :: xdata    
END TYPE ctlvec
END MODULE m_type

MODULE mod_a
USE real_precision
USE m_type
IMPLICIT NONE

TYPE(ctlvec), SAVE :: x0ref 

INTERFACE
	SUBROUTINE unit_a(xctlvec)
		USE real_precision
		USE m_type
    	TYPE(ctlvec) :: xctlvec  
	END SUBROUTINE unit_a
END INTERFACE

REAL(KIND=wp) :: J_cost 
END MODULE mod_a


SUBROUTINE unit_a(xctlvec)
    use real_precision
    USE mod_a
    IMPLICIT NONE
    TYPE(ctlvec) :: xctlvec   
END SUBROUTINE unit_a

SUBROUTINE unit_b
    use real_precision
    USE mod_a
    IMPLICIT NONE
    CALL unit_a(x0ref)
    J_cost = 0.
END SUBROUTINE unit_b
