!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 18 Jan 2024 10:24
!
!  Differentiation of top as a context to call adjoint code (with options context):
! Un exemple pour tester la differentiation
! dans le cas d'arguments optionnels.
SUBROUTINE TOP_B(a, b, c, res)
  IMPLICIT NONE
  REAL :: a, b, c, res
  REAL :: ab
  REAL, DIMENSION(99) :: x1, x2, z1, z2, y1, y2
  REAL, DIMENSION(99) :: x1b, x2b, z1b, z2b, y1b, y2b
  REAL :: x3
  REAL :: x3b
  INTRINSIC SUM
  REAL :: tmp
  REAL :: tmpb
  REAL :: tmp0
  REAL :: tmpb0
  x1 = a*b
  x2 = a*c
  z1 = 5.0
  z2 = a*c
  y1 = 0.0
  y2 = b*c
  CALL ADCONTEXTADJ_INIT(0.87_8)
  CALL ADCONTEXTADJ_INITREAL4ARRAY('y1'//CHAR(0), y1, y1b, 99)
  CALL ADCONTEXTADJ_INITREAL4ARRAY('z1'//CHAR(0), z1, z1b, 99)
  CALL ADCONTEXTADJ_INITREAL4ARRAY('x1'//CHAR(0), x1, x1b, 99)
  tmpb = SUM(x1b)
  x1b = 0.0
  CALL FOO_B(x1=x1, x1b=x1b, z1=z1, z1b=z1b, y1=y1, y1b=y1b, foob=tmpb)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  CALL ADCONTEXTADJ_CONCLUDEREAL4ARRAY('y1'//CHAR(0), y1, y1b, 99)
  CALL ADCONTEXTADJ_CONCLUDEREAL4ARRAY('z1'//CHAR(0), z1, z1b, 99)
  CALL ADCONTEXTADJ_CONCLUDEREAL4ARRAY('x1'//CHAR(0), x1, x1b, 99)
  CALL ADCONTEXTADJ_CONCLUDE()
  CALL ADCONTEXTADJ_INIT(0.87_8)
  CALL ADCONTEXTADJ_INITREAL4ARRAY('y2'//CHAR(0), y2, y2b, 99)
  CALL ADCONTEXTADJ_INITREAL4ARRAY('z2'//CHAR(0), z2, z2b, 99)
  CALL ADCONTEXTADJ_INITREAL4ARRAY('x2'//CHAR(0), x2, x2b, 99)
  tmpb0 = SUM(x2b)
  x2b = 0.0
  CALL FOO_B(x2=x2, x2b=x2b, z2=z2, z2b=z2b, y2=y2, y2b=y2b, foob=tmpb0)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  CALL ADCONTEXTADJ_CONCLUDEREAL4ARRAY('y2'//CHAR(0), y2, y2b, 99)
  CALL ADCONTEXTADJ_CONCLUDEREAL4ARRAY('z2'//CHAR(0), z2, z2b, 99)
  CALL ADCONTEXTADJ_CONCLUDEREAL4ARRAY('x2'//CHAR(0), x2, x2b, 99)
  CALL ADCONTEXTADJ_CONCLUDE()
  CALL ADCONTEXTADJ_INIT(0.87_8)
  CALL ADCONTEXTADJ_INITREAL4('a'//CHAR(0), a, ab)
  CALL ADCONTEXTADJ_INITREAL4ARRAY('x1'//CHAR(0), x1, x1b, 99)
  CALL ADCONTEXTADJ_INITREAL4('x3'//CHAR(0), x3, x3b)
  CALL FOO_B(x1, x1b, z1=a, z1b=ab, foob=x3b)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  CALL ADCONTEXTADJ_CONCLUDEREAL4('a'//CHAR(0), a, ab)
  CALL ADCONTEXTADJ_CONCLUDEREAL4ARRAY('x1'//CHAR(0), x1, x1b, 99)
  CALL ADCONTEXTADJ_CONCLUDE()
  res = SUM(x1) + SUM(x2) + SUM(z1) + SUM(z2) + SUM(y1) + SUM(y2)

CONTAINS
!  Differentiation of foo in reverse (adjoint) mode (with options context):
!   gradient     of useful results: y1 y2 z1 z2 x1 x2 foo
!   with respect to varying inputs: y1 y2 z1 z2 x1 x2
!   RW status of diff variables: y1:in-out y2:in-out z1:in-out
!                z2:in-out x1:incr x2:incr foo:in-killed
  SUBROUTINE FOO_B(x1, x1b, x2, x2b, z1, z1b, z2, z2b, y1, y1b, y2, y2b&
&   , foob)
    IMPLICIT NONE
    REAL, DIMENSION(:), INTENT(IN), OPTIONAL :: x1
    REAL, DIMENSION(:), OPTIONAL :: x1b
    REAL, DIMENSION(:), INTENT(IN), OPTIONAL :: x2
    REAL, DIMENSION(:), OPTIONAL :: x2b
    REAL, DIMENSION(:), INTENT(INOUT), OPTIONAL :: z1
    REAL, DIMENSION(:), INTENT(INOUT), OPTIONAL :: z1b
    REAL, DIMENSION(:), INTENT(INOUT), OPTIONAL :: z2
    REAL, DIMENSION(:), INTENT(INOUT), OPTIONAL :: z2b
    REAL, DIMENSION(:), OPTIONAL :: y1
    REAL, DIMENSION(:), OPTIONAL :: y1b
    REAL, DIMENSION(:), OPTIONAL :: y2
    REAL, DIMENSION(:), OPTIONAL :: y2b
    REAL, DIMENSION(99) :: rx1, rx2, rz1, rz2, ry1, ry2
    REAL, DIMENSION(99) :: rx1b, rx2b, rz1b, rz2b, ry1b, ry2b
    REAL, DIMENSION(99) :: tmp1, tmp2
    REAL, DIMENSION(99) :: tmp1b, tmp2b
    REAL :: foo
    REAL :: foob
    INTRINSIC PRESENT
    INTEGER*4 :: branch
    rx1 = 0.0
    IF (PRESENT(x1)) THEN
      rx1 = x1
      CALL PUSHCONTROL1B(0)
    ELSE
      CALL PUSHCONTROL1B(1)
    END IF
    rx2 = 1.0
    IF (PRESENT(x2)) THEN
      rx2 = x2
      CALL PUSHCONTROL1B(0)
    ELSE
      CALL PUSHCONTROL1B(1)
    END IF
    rz1 = 2.0
    IF (PRESENT(z1)) THEN
      rz1 = z1
      CALL PUSHCONTROL1B(0)
    ELSE
      CALL PUSHCONTROL1B(1)
    END IF
    rz2 = 3.0
    IF (PRESENT(z2)) THEN
      rz2 = z2
      CALL PUSHCONTROL1B(0)
    ELSE
      CALL PUSHCONTROL1B(1)
    END IF
    tmp1 = rz1
    rz1 = rx1*rx1
    IF (PRESENT(z1)) THEN
      CALL PUSHCONTROL1B(0)
    ELSE
      CALL PUSHCONTROL1B(1)
    END IF
    tmp2 = rz2
    IF (PRESENT(z2)) THEN
      CALL PUSHCONTROL1B(0)
    ELSE
      CALL PUSHCONTROL1B(1)
    END IF
    rz2 = rz1*rx2
    ry1 = rz2*tmp1
    IF (PRESENT(y1)) THEN
      CALL PUSHCONTROL1B(0)
    ELSE
      CALL PUSHCONTROL1B(1)
    END IF
    IF (PRESENT(y2)) THEN
      CALL PUSHCONTROL1B(0)
    ELSE
      CALL PUSHCONTROL1B(1)
    END IF
    rz2b = 0.0
    rz2b(1) = rz2b(1) + foob
    CALL POPCONTROL1B(branch)
    IF (branch .EQ. 0) THEN
      ry2b = 0.0
      ry2b = y2b
      y2b = 0.0
    ELSE
      ry2b = 0.0
    END IF
    tmp2b = 0.0
    ry1b = 0.0
    ry1b = tmp2*ry2b
    tmp2b = ry1*ry2b
    CALL POPCONTROL1B(branch)
    IF (branch .EQ. 0) THEN
      ry1b = ry1b + y1b
      y1b = 0.0
    END IF
    tmp1b = 0.0
    rz2b = rz2b + tmp1*ry1b
    tmp1b = rz2*ry1b
    rx2b = 0.0
    rz1b = 0.0
    rz1b = rx2*rz2b
    rx2b = rz1*rz2b
    CALL POPCONTROL1B(branch)
    IF (branch .EQ. 0) THEN
      rz2b = 0.0
      rz2b = z2b
      z2b = 0.0
    ELSE
      rz2b = 0.0
    END IF
    rz2b = rz2b + tmp2b
    CALL POPCONTROL1B(branch)
    IF (branch .EQ. 0) THEN
      rz1b = rz1b + z1b
      z1b = 0.0
    END IF
    rx1b = 0.0
    rx1b = 2*rx1*rz1b
    rz1b = 0.0
    rz1b = tmp1b
    CALL POPCONTROL1B(branch)
    IF (branch .EQ. 0) z2b = z2b + rz2b
    CALL POPCONTROL1B(branch)
    IF (branch .EQ. 0) z1b = z1b + rz1b
    CALL POPCONTROL1B(branch)
    IF (branch .EQ. 0) x2b = x2b + rx2b
    CALL POPCONTROL1B(branch)
    IF (branch .EQ. 0) x1b = x1b + rx1b
  END SUBROUTINE FOO_B

  FUNCTION FOO(x1, x2, z1, z2, y1, y2)
    IMPLICIT NONE
    REAL, DIMENSION(:), INTENT(IN), OPTIONAL :: x1
    REAL, DIMENSION(:), INTENT(IN), OPTIONAL :: x2
    REAL, DIMENSION(:), INTENT(INOUT), OPTIONAL :: z1
    REAL, DIMENSION(:), INTENT(INOUT), OPTIONAL :: z2
    REAL, DIMENSION(:), INTENT(OUT), OPTIONAL :: y1
    REAL, DIMENSION(:), INTENT(OUT), OPTIONAL :: y2
    REAL, DIMENSION(99) :: rx1, rx2, rz1, rz2, ry1, ry2
    REAL, DIMENSION(99) :: tmp1, tmp2
    REAL :: foo
    INTRINSIC PRESENT
    rx1 = 0.0
    IF (PRESENT(x1)) rx1 = x1
    rx2 = 1.0
    IF (PRESENT(x2)) rx2 = x2
    rz1 = 2.0
    IF (PRESENT(z1)) rz1 = z1
    rz2 = 3.0
    IF (PRESENT(z2)) rz2 = z2
    tmp1 = rz1
    rz1 = rx1*rx1
    IF (PRESENT(z1)) z1 = rz1
    tmp2 = rz2
    IF (PRESENT(z2)) z2 = rz2
    rz2 = rz1*rx2
    ry1 = rz2*tmp1
    IF (PRESENT(y1)) y1 = ry1
    ry2 = ry1*tmp2
    IF (PRESENT(y2)) y2 = ry2
    foo = rz2(1)
  END FUNCTION FOO

END SUBROUTINE TOP_B

