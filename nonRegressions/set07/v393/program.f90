
SUBROUTINE TEST(a, b, z, code, rang, etiquette, nb)
  IMPLICIT NONE
  INCLUDE 'mpif.h'
  REAL :: a, b, z, x, y
  INTEGER :: rang, code, code1, code2, code3, code4 
  INTEGER :: etiquette, etiquette1, etiquette2
  INTEGER, DIMENSION(mpi_status_size) :: statut1, statut2
  INTEGER :: ierr, nb

  etiquette1 = etiquette + 100
  etiquette2 = etiquette + 103

  IF (rang .EQ. 0) THEN
    b = 40.0
! channel 1 ou 2
    if (nb .EQ. 20) then
      etiquette = etiquette1
    else
      etiquette = etiquette2
    end if
    print *,'etiquette ', etiquette
    print *,'Proc send a ',  rang, a, etiquette
! channel 1 ou 2
    CALL MPI_SEND(a, 1, mpi_real, 1, etiquette, mpi_comm_world, code)
    if (nb .EQ. 20) then
      print *,'Proc send b ',  rang, b, etiquette2
! $AD CHANNEL channel3
      CALL MPI_SEND(b, 1, mpi_real, 1, etiquette2, mpi_comm_world, code2)
    end if
  ELSE
    z = 0
! channel 1
    if (nb .EQ. 20) then
! $AD CHANNEL channel1
      CALL MPI_RECV(z, 1, mpi_real, 0, etiquette1, mpi_comm_world, code, &
&            statut1)
      print *,'Proc recv z ',  rang, z, etiquette1
    end if
! channel 2 ou 3
    CALL MPI_RECV(y, 1, mpi_real, 0, etiquette2, mpi_comm_world, code4, &
&            statut2)
    print *,'Proc recv y ',  rang, y, etiquette2
  END IF
END SUBROUTINE TEST

program testk
 implicit none
 include 'mpif.h'
 integer :: etiquette
 integer :: nb_procs,rang, code, nb
 real :: x,y,z
 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 x = 22.0
 y = 4.0
 z = 2.0
 nb = 202
 etiquette = 100

 call test(x, y, z, code, rang, etiquette, nb)

 call MPI_FINALIZE (code)
end program testk
