SUBROUTINE FOO(a, b, c)

  IMPLICIT NONE
  REAL :: a
  LOGICAL :: b, c
  IF (b .EQV. c) THEN
    a = SIN(a)
  ELSE
    a = COS(a)
  END IF
END SUBROUTINE FOO
