!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 28 May 2019 17:51
!
!  Differentiation of foo in forward (tangent) mode:
!   variations   of useful results: a
!   with respect to varying inputs: a
!   RW status of diff variables: a:in-out
SUBROUTINE FOO_D(a, ad, b, c)
  IMPLICIT NONE
  REAL :: a
  REAL :: ad
  LOGICAL :: b, c
  INTRINSIC SIN
  INTRINSIC COS
  IF (b .EQV. c) THEN
    ad = COS(a)*ad
    a = SIN(a)
  ELSE
    ad = -(SIN(a)*ad)
    a = COS(a)
  END IF
END SUBROUTINE FOO_D

