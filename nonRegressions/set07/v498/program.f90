module moduleB
   TYPE, PUBLIC :: test
    PRIVATE
    LOGICAL :: init   = .FALSE.
  END TYPE test
  
  PUBLIC :: OPERATOR(==)
  INTERFACE OPERATOR(==)
    MODULE PROCEDURE test_eq
  end interface

  public :: Operator(.not.)
  INTERFACE OPERATOR(.not.)
    MODULE PROCEDURE my_not
  end interface

  PUBLIC :: OPERATOR(.banana.)
  INTERFACE OPERATOR(.banana.)
    MODULE PROCEDURE banana
  end interface 
 contains 
 LOGICAL FUNCTION test_eq (day1, day2)
    TYPE (test), INTENT(in) :: day1, day2
    test_eq =  .false. 
  END FUNCTION test_eq 

 LOGICAL FUNCTION banana (day1, day2)
    TYPE (test), INTENT(in) :: day1, day2
    banana =  .false. 
  END FUNCTION banana

 logical function my_not(i)
    type(test) , INTENT(in) :: i
    my_not = .false.
 end function my_not

end module


module moduleA
    use moduleB, only: test, operator(==), operator(.banana.)
contains    
    subroutine foobar(a,b)
       implicit none
       type(test) :: a,b
       if (a==b) write(*,*) "FOO"
    end subroutine

end module


   subroutine foo(x,n,fc)
      use moduleA
      use moduleB, operator(.nurke.) => operator(.banana.)
     implicit none
     integer :: n
     double precision :: x(n),fc
     double precision, dimension(n) :: ff
     double precision, dimension(n,2) ::ff2
     type(test) :: a,b
     ff=x
     call foobar(a,b)
     ff2= spread(ff,ncopies=2,dim=2)
     fc=sum(ff2)
   end subroutine
