! tapenade -O /tmp -o p -vars v2 -outvars f program.f90
! ou bien
! tapenade -O /tmp -o p -vars "v2" -outvars "v3" pr2.f90

program testk
 implicit none
 include 'mpif.h'
 integer, dimension( MPI_STATUS_SIZE ) :: statut
 integer, parameter :: etiquette=100
 integer :: nb_procs,rang, code
 real :: x,y,z,b,f
 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 print *,'Proc ' , rang

 x = 0.0
 y = 4.0
 z = 2.0
 b = 7.0
 f = 99.0

 call test(x, y, z, b, rang, code,  statut)

 call MPI_FINALIZE (code)
end program testk


subroutine test(v1,v2,v3,v4, rang, code, statut)
include 'mpif.h'

real :: v1,v2,v3,v4
integer :: rang, code
integer, dimension( MPI_STATUS_SIZE ) :: statut


 if (rang == 0) then
      call f1(v1,v2, code)
 else
      call f2(v3,v4, statut,code)
      !z = b * y
 end if

 print *,'z ', z , ', proc ' , rang

 call MPI_REDUCE(v3,f,1,MPI_REAL,MPI_SUM,0,MPI_COMM_WORLD,ierr)

 print *,'Resultat ', f , ', proc ' , rang


end subroutine

subroutine f1(a,b, code)
include 'mpif.h'
  real x
  integer :: code, err
  INTEGER, DIMENSION(mpi_status_size) :: statut

!  a = x                                  
!  send(a)  ! a ne dépend d'aucun in de f1    
   x = 7.0
   a = x * b
!   send(a, k) ! a dépend de b

   call MPI_SEND (a,1, MPI_REAL , 1, 100, &
         MPI_COMM_WORLD ,code)


end

subroutine f2(c,d, statut,code)
include 'mpif.h'
  integer :: code,  err
integer, dimension( MPI_STATUS_SIZE ) :: statut

  c = 0.0
!  recv(c, k)  ! c dépend du b de f1

  call MPI_RECV (c,1, MPI_REAL , 0, 100, &
      MPI_COMM_WORLD ,code,statut)
  
end

!! si b active, c active

