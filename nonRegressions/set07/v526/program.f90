module M1
 type pair
   real, dimension(:)  , pointer ::f1
   integer f2
 end type pair
end module M1

subroutine foo(x,y,p1)
use M1
real x,y
type(pair), dimension(10) :: p1
call gee(p1(5)%f2, x, y)
p1(3)%f1 = x
x = x + size(p1)
y = y + p1(3)%f1(2)
end subroutine foo

subroutine gee(n,x,y)
real x,y
integer n
y = y+ n*x*x
end subroutine gee
