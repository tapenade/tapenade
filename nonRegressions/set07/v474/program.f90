! bug mauvais flowgraph regenere (connecting a Flow Arrow twice)
! quand boucle naturelle adjoint-dead + index de boucle tbr

subroutine top(x,y,a,b,n,i)
 real x,y
 integer n,i
 real a,b
 x = x * y * i
 a = 7.0
 b = 3.0
 if ( n > 0) then
    do i = 1,10
       a = a * b *i
    enddo
    do i = 1, 100
       b = b * a
       call foo(a,i)
       if ( n  > 5) then
          return
       end if
       a = a * a
    enddo
 end if
end subroutine top
