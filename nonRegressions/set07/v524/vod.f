      subroutine vod(in,out)
      implicit none
      include 'inclconstants'
      include 'inclpar.vod'
      real in,out
      real ctilde

      integer i

      a_vod = a_vod*(1.+in)
      b_vod = b_vod*(1.+in)

      ctilde = atan(-b_vod*c_vod)
      out = 0
      do i=1,nm
         out = out + a_vod + ctilde*d_vod
      enddo

      end

      subroutine init_vod()
      implicit none
      include 'inclpar.vod'
      a_vod = 17275.
      b_vod = 9.07
      c_vod = 0.87
      d_vod = 1045.
      end
