! Tapenade differentiation with:
! tapenade -msglevel 40 -msginfile -I . -tgtvarname _fw -tgtfuncname _fw  -o dvfoo -head "foo(y)/(x)" -outputlanguage fortran90 foo.f90 vod.f mo_fix2free.f
subroutine foo(x,y)
  use fix2free
  implicit none

  ! arguments
  real, intent(in) :: x
  real, intent(out) :: y

  call init_vod()

  print*, 'looping over nm=',nm
  call vod(x, y)

  
end subroutine foo
