! pour tester addSizeAndTypeInAllocate 
! et split declaration avec save
      subroutine test(x,y,i,j,rr)
      
        real, dimension(:), allocatable :: x
        real, dimension(:), allocatable :: y
        integer :: i, j
        integer :: target_sig, pos, size_of_in, status=0, fileorig
        real :: rr

        rr = rr*rr
        allocate(x(5 + max(i,j)))
        deallocate(x)
        allocate(y(5))
        deallocate(y)

      end  subroutine test
