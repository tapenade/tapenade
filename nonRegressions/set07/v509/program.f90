program main
implicit none
real :: x, y
x = 2.0
print*, 'x ', x
call foo(x,y)
print*, 'x ', x, ' y ', y

contains
subroutine foo(x, y)
implicit none
real :: x, y
call top(x, y)

end subroutine

subroutine top(a, b)
implicit none
real, value :: a
real :: b

print*, 'a ', a
a = 2 * a
b = a * a
print*, 'a ', a, ' b ', b

end subroutine

end program
