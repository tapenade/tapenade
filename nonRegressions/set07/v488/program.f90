module g1
  REAL(KIND=8), dimension(:), pointer :: z, cf1
end module g1

program top
  use g1
  interface 
    subroutine foo1(cf2)
     REAL(KIND=8), dimension(:), pointer :: cf2
    end subroutine foo1
  end interface
  allocate(cf1(99))
  allocate(z(88))
  z = 1.d0
  call foo1(cf1)
  deallocate(cf1)
end program top

subroutine foo1(cf2)
  REAL(KIND=8), dimension(:), pointer :: cf2
  interface 
    subroutine foo(cf2)
     REAL(KIND=8), dimension(:) :: cf2
    end subroutine foo
  end interface
  call foo(cf2)
end subroutine foo1

subroutine foo(cf2)
  REAL(KIND=8), dimension(:) :: cf2
  real(KIND=8) :: x
  interface 
    subroutine bar(cf3, x)
     REAL(KIND=8), dimension(:) :: cf3
!     REAL(KIND=8), dimension(:), pointer :: cf3
     real(KIND=8) :: x
    end subroutine bar
  end interface
  x = 5
  call bar(cf2, x)
end subroutine foo

subroutine bar(cf3, x)
  use g1
  REAL(KIND=8), dimension(:) :: cf3
!  REAL(KIND=8), dimension(:), pointer :: cf3
  real(KIND=8) :: x
  x = x * cf3(5)
  z = z * x
end subroutine bar

