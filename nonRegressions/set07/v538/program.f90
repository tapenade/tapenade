! from v522 avec test associated dans compute
module M
   implicit none
   integer, parameter :: n = 2
   real :: global
   type:: struc
    real, dimension(:),pointer::vol => null()
   end type

end module M

module M1_I
   interface
      subroutine compute (x,y)
       use M
       implicit none
       type(struc) :: x,y
      end subroutine compute
   end interface

   interface
      subroutine alloc (x,y)
       implicit none
       real, dimension(:), pointer :: x,y
      end subroutine alloc
   end interface

   interface
      subroutine dealloc (x,y)
       implicit none
       real, dimension(:), pointer :: x,y
      end subroutine dealloc
   end interface

end module M1_I

subroutine alloc(x,y)
  real, dimension(:), pointer :: x,y
  allocate(x(5))
  allocate(y(5))
end subroutine alloc

subroutine dealloc(x,y)
  real, dimension(:), pointer :: x,y
  deallocate(x,y)
end subroutine dealloc

subroutine compute(x,y)
  use M
  implicit none
  type(struc) :: x,y
  real :: computex
  if ( .not. associated(y%vol)) then 
     allocate(y%vol(5))
  end if
  y%vol = 2 * x%vol
  computex = y%vol(1) * y%vol(2)
  global = global + computex
end subroutine compute

subroutine top()
  use M
  use M1_I
  implicit none
  type(struc) :: r,s
  call alloc(r%vol,s%vol)
  r%vol(1) = 3.0
  r%vol(2) = 2.0
  call compute(r,s)
  print*,s%vol(1)
  call dealloc(r%vol,s%vol)
end subroutine top
