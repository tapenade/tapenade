!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 15:32
!
! from v522 avec test associated dans compute
MODULE M_DIFF
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=2
  REAL :: global
  REAL :: globalb
  TYPE STRUC
      REAL, DIMENSION(:), POINTER :: vol => NULL()
  END TYPE STRUC
END MODULE M_DIFF

MODULE M1_I_DIFF
  IMPLICIT NONE
  INTERFACE 
      SUBROUTINE COMPUTE_NODIFF(x, y)
        USE M_DIFF
        IMPLICIT NONE
        TYPE(STRUC) :: x, y
      END SUBROUTINE COMPUTE_NODIFF
  END INTERFACE

  INTERFACE 
      SUBROUTINE COMPUTE_B(x, xb, y, yb)
        USE M_DIFF
        IMPLICIT NONE
        TYPE(STRUC) :: x, y
        TYPE(STRUC) :: xb, yb
      END SUBROUTINE COMPUTE_B
  END INTERFACE

  INTERFACE 
      SUBROUTINE ALLOC(x, y)
        IMPLICIT NONE
        REAL, DIMENSION(:), POINTER :: x, y
      END SUBROUTINE ALLOC
  END INTERFACE

  INTERFACE 
      SUBROUTINE ALLOC_FWD(x, xb, y, yb)
        IMPLICIT NONE
        REAL, DIMENSION(:), POINTER :: x, y
        REAL, DIMENSION(:), POINTER :: xb, yb
      END SUBROUTINE ALLOC_FWD
  END INTERFACE

  INTERFACE 
      SUBROUTINE ALLOC_BWD(x, xb, y, yb)
        IMPLICIT NONE
        REAL, DIMENSION(:), POINTER :: x, y
        REAL, DIMENSION(:), POINTER :: xb, yb
      END SUBROUTINE ALLOC_BWD
  END INTERFACE

  INTERFACE 
      SUBROUTINE DEALLOC(x, y)
        IMPLICIT NONE
        REAL, DIMENSION(:), POINTER :: x, y
      END SUBROUTINE DEALLOC
  END INTERFACE

  INTERFACE 
      SUBROUTINE DEALLOC_B(x, xb, y, yb)
        IMPLICIT NONE
        REAL, DIMENSION(:), POINTER :: x, y
        REAL, DIMENSION(:), POINTER :: xb, yb
      END SUBROUTINE DEALLOC_B
  END INTERFACE

END MODULE M1_I_DIFF

!  Differentiation of top in reverse (adjoint) mode (with options context noISIZE split(alloc)):
!   gradient     of useful results: [alloc*(y.vol) in compute]
!                [alloc*y in alloc] [alloc*x in alloc] global
!   with respect to varying inputs: [alloc*(y.vol) in compute]
!                [alloc*y in alloc] [alloc*x in alloc] global
!   RW status of diff variables: [alloc*(y.vol) in compute]:in-out
!                [alloc*y in alloc]:in-out [alloc*x in alloc]:in-out
!                global:in-out
SUBROUTINE TOP_B()
  USE M_DIFF
  USE M1_I_DIFF
  IMPLICIT NONE
  TYPE(STRUC) :: r, s
  TYPE(STRUC) :: rb, sb
  REAL, DIMENSION(:), POINTER :: dummyzerodiffb
  REAL, DIMENSION(:), POINTER :: dummyzerodiffb0
  CALL ALLOC_FWD(r%vol, rb%vol, s%vol, sb%vol)
  r%vol(1) = 3.0
  r%vol(2) = 2.0
  CALL DEALLOC_B(r%vol, rb%vol, s%vol, sb%vol)
  CALL COMPUTE_B(r, rb, s, sb)
  rb%vol(2) = 0.0
  rb%vol(1) = 0.0
  dummyzerodiffb => NULL()
  dummyzerodiffb0 => NULL()
  CALL ALLOC_BWD(r%vol, dummyzerodiffb, s%vol, dummyzerodiffb0)
END SUBROUTINE TOP_B

!  Differentiation of alloc in reverse (adjoint) mode, forward sweep (with options context noISIZE split(alloc)):
!   Plus diff mem management of: x:out y:out
SUBROUTINE ALLOC_FWD(x, xb, y, yb)
  IMPLICIT NONE
  REAL, DIMENSION(:), POINTER :: x, y
  REAL, DIMENSION(:), POINTER :: xb
  REAL, DIMENSION(:), POINTER :: yb
  ALLOCATE(xb(5))
  xb = 0.0
  ALLOCATE(x(5))
  ALLOCATE(yb(5))
  yb = 0.0
  ALLOCATE(y(5))
END SUBROUTINE ALLOC_FWD

!  Differentiation of alloc in reverse (adjoint) mode, backward sweep (with options context noISIZE split(alloc)):
!   Plus diff mem management of: x:out y:out
SUBROUTINE ALLOC_BWD(x, xb, y, yb)
  IMPLICIT NONE
  REAL, DIMENSION(:), POINTER :: x, y
  REAL, DIMENSION(:), POINTER :: xb, yb
  DEALLOCATE(y)
  DEALLOCATE(yb)
  DEALLOCATE(x)
  DEALLOCATE(xb)
END SUBROUTINE ALLOC_BWD

!  Differentiation of dealloc in reverse (adjoint) mode (with options context noISIZE split(alloc)):
!   Plus diff mem management of: x:out y:out
SUBROUTINE DEALLOC_B(x, xb, y, yb)
  IMPLICIT NONE
  REAL, DIMENSION(:), POINTER :: x, y
  REAL, DIMENSION(:), POINTER :: xb, yb
END SUBROUTINE DEALLOC_B

!  Differentiation of compute in reverse (adjoint) mode (with options context noISIZE split(alloc)):
!   gradient     of useful results: [alloc*(y.vol) in compute]
!                [alloc*y in alloc] [alloc*x in alloc] global *(x.vol)
!                *(y.vol)
!   with respect to varying inputs: [alloc*(y.vol) in compute]
!                [alloc*y in alloc] [alloc*x in alloc] global *(x.vol)
!                *(y.vol)
!   Plus diff mem management of: x.vol:in y.vol:in-out
SUBROUTINE COMPUTE_B(x, xb, y, yb)
  USE M_DIFF
  USE ISO_C_BINDING
  USE ADMM_TAPENADE_INTERFACE
  IMPLICIT NONE
  TYPE(STRUC) :: x, y
  TYPE(STRUC) :: xb, yb
  REAL :: computex
  REAL :: computexb
  INTRINSIC ASSOCIATED
  TYPE(C_PTR) :: cptr
  INTEGER :: unknown_shape_in_compute
  INTEGER*4 :: branch
  IF (.NOT.ASSOCIATED(y%vol)) THEN
    CALL PUSHPOINTER8(C_LOC(yb%vol))
    ALLOCATE(yb%vol(5))
    yb%vol = 0.0
    CALL PUSHPOINTER8(C_LOC(y%vol))
    ALLOCATE(y%vol(5))
    CALL PUSHCONTROL1B(0)
  ELSE
    CALL PUSHCONTROL1B(1)
  END IF
  y%vol = 2*x%vol
  computexb = globalb
  yb%vol(1) = yb%vol(1) + y%vol(2)*computexb
  yb%vol(2) = yb%vol(2) + y%vol(1)*computexb
  xb%vol = xb%vol + 2*yb%vol
  yb%vol = 0.0
  CALL POPCONTROL1B(branch)
  IF (branch .EQ. 0) THEN
    DEALLOCATE(y%vol)
    CALL POPPOINTER8(cptr)
    CALL C_F_POINTER(cptr, y%vol, (/unknown_shape_in_compute/))
    DEALLOCATE(yb%vol)
    CALL POPPOINTER8(cptr)
    CALL C_F_POINTER(cptr, yb%vol, (/unknown_shape_in_compute/))
  END IF
END SUBROUTINE COMPUTE_B

SUBROUTINE TOP_NODIFF()
  USE M_DIFF
  USE M1_I_DIFF
  IMPLICIT NONE
  TYPE(STRUC) :: r, s
  CALL ALLOC(r%vol, s%vol)
  r%vol(1) = 3.0
  r%vol(2) = 2.0
  CALL COMPUTE_NODIFF(r, s)
  PRINT*, s%vol(1)
  CALL DEALLOC(r%vol, s%vol)
END SUBROUTINE TOP_NODIFF

SUBROUTINE COMPUTE_NODIFF(x, y)
  USE M_DIFF
  IMPLICIT NONE
  TYPE(STRUC) :: x, y
  REAL :: computex
  INTRINSIC ASSOCIATED
  IF (.NOT.ASSOCIATED(y%vol)) THEN
    ALLOCATE(y%vol(5))
  END IF
  y%vol = 2*x%vol
  computex = y%vol(1)*y%vol(2)
  global = global + computex
END SUBROUTINE COMPUTE_NODIFF

!  Differentiation of prog as a context to call adjoint code (with options context noISIZE split(alloc)):
PROGRAM PROG_B
  USE M_DIFF
  USE M1_I_DIFF
  IMPLICIT NONE
  INTERFACE 
      SUBROUTINE TOP_NODIFF()
        USE M_DIFF
        USE M1_I_DIFF
        IMPLICIT NONE
      END SUBROUTINE TOP_NODIFF
  END INTERFACE

  INTERFACE 
      SUBROUTINE TOP_B()
        USE M_DIFF
        USE M1_I_DIFF
        IMPLICIT NONE
      END SUBROUTINE TOP_B
  END INTERFACE

  CALL ADCONTEXTADJ_INIT(0.87_8)
  CALL ADCONTEXTADJ_INITREAL4('global'//CHAR(0), global, globalb)
  CALL TOP_B()
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  CALL ADCONTEXTADJ_CONCLUDEREAL4('global'//CHAR(0), global, globalb)
  CALL ADCONTEXTADJ_CONCLUDE()
END PROGRAM PROG_B

SUBROUTINE PROG_NODIFF()
  USE M_DIFF
  USE M1_I_DIFF
  IMPLICIT NONE
  INTERFACE 
      SUBROUTINE TOP_NODIFF()
        USE M_DIFF
        USE M1_I_DIFF
        IMPLICIT NONE
      END SUBROUTINE TOP_NODIFF
  END INTERFACE

  CALL TOP_NODIFF()
END SUBROUTINE PROG_NODIFF

