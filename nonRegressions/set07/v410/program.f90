function limiter(a, b) result(z)
  implicit none
  real(8) :: a, b, z, s
  dimension :: a(:), b(:), s(size(a))
  dimension :: z(size(a))
  s = sign(1d0, a)
  z = s*max(0d0, min(abs(a), s*b))
end function limiter
