module precision
    integer, parameter :: intType = 4
end module precision

subroutine test (x,y,z)
    use precision
    implicit none
    integer(kind=intType), parameter :: firstOrder  = 1_intType
    integer(intType), parameter :: secondOrder  = 2_intType
    real(intType), parameter :: thirdOrder  = 3_intType
    real, parameter :: zero = 0.0
    real*8, parameter :: zerod = 0.0_8
    integer:: x,y,z
end subroutine test


