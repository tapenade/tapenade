module g1
  REAL(KIND=8), dimension(:), pointer :: cf1
  REAL(KIND=8), dimension(:), pointer :: z
end module g1

module m_i
  interface 
    subroutine foo1(cf1, z)
        REAL(KIND=8), dimension(:), pointer :: cf1
        REAL(KIND=8), dimension(:), pointer :: z
    end subroutine foo1
  end interface
  interface 
    subroutine foo2(cf1, z)
        REAL(KIND=8), dimension(:), pointer :: cf1
        REAL(KIND=8), dimension(:), pointer :: z
    end subroutine foo2
  end interface
  interface 
    subroutine calcul(cf1, z)
        REAL(KIND=8), dimension(:), pointer :: cf1
        REAL(KIND=8), dimension(:), pointer :: z
    end subroutine calcul
  end interface
end module m_i

program main
  use g1
  use m_i
  implicit none
  call foo1(cf1, z)
  cf1 = 2
  z = 10
  call toplevel(cf1, z)

contains

subroutine toplevel(cf1, z)
  use m_i
  implicit none
  REAL(KIND=8), dimension(:), pointer :: cf1
  REAL(KIND=8), dimension(:), pointer :: z
  call foo1(cf1, z)
  call calcul(cf1, z)
  print*, 'calcul cf1 ', cf1, ' -> ', z
  call foo2(cf1,z)

end subroutine toplevel

end program main

subroutine foo1(cf1, z)
  implicit none
  REAL(KIND=8), dimension(:), pointer :: cf1
  REAL(KIND=8), dimension(:), pointer :: z
  if (.not. associated(cf1)) then
    allocate(cf1(2))
  end if
  if (.not. associated(z)) then
    allocate(z(2))
  end if
end subroutine foo1

subroutine calcul(cf1, z)
  implicit none
  REAL(KIND=8), dimension(:), pointer :: cf1
  REAL(KIND=8), dimension(:), pointer :: z
  z = z * cf1
end subroutine calcul

subroutine foo2(cf1, z)
  implicit none
  REAL(KIND=8), dimension(:), pointer :: cf1
  REAL(KIND=8), dimension(:), pointer :: z
  deallocate(cf1)
  deallocate(z)
end subroutine foo2
