module zz
   real zn,zd
end module
    
      subroutine head(i1,i2,o)
      use zz
      real i1,i2,o
      call sub0(i1,i2)
      o = zn / zd
      return

      contains

      subroutine sub0(u,v)
      use zz
      real u,v,z1,z2
      call sub1(u,z1)
      call sub2(v,z2)
      zn = z1 - z2
      zd = 1 + z1 + z2
      return
      end subroutine


      end subroutine
