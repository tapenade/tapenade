MODULE M
  IMPLICIT NONE
  real :: pi = 3.14
  real, dimension(2) :: tab = (/1.0,2.0/)
END MODULE M

module n
  use m
  real :: global = sin(3.14)
  real :: gl = 1.0
end module n

subroutine subr(y)
 use m
 use n
 real y
 y = y * global * gl
end subroutine subr

program test
use n

print*, global
print*, gl
call subr(global)

end program
