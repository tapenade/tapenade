!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.11 (r6038) - 21 Apr 2016 16:31
!
!  Differentiation of f in reverse (adjoint) mode:
!   gradient     of useful results: l
!   with respect to varying inputs: l x
!   RW status of diff variables: l:in-zero x:out
SUBROUTINE F_B(l, lb, x, xb)
  IMPLICIT NONE
  REAL*8, INTENT(INOUT) :: l
  REAL*8, INTENT(INOUT) :: lb
  REAL*8, DIMENSION(2), INTENT(IN) :: x
  REAL*8, DIMENSION(2) :: xb
  INTRINSIC SUM
  xb = 0.0_8
  xb(1) = xb(1) + SUM(x)*lb
  xb = xb + x(1)*lb
  lb = 0.0_8
END SUBROUTINE F_B

