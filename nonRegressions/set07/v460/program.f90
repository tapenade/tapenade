module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

function top(r,s)
use M
implicit none
real, dimension(n) :: r,s
real top
  interface compute
      function compute1 (x,y)
       use M
       implicit none
       real :: x
       real, dimension(n) :: y
       real :: compute1
      end function compute1

      function compute2 (x,y)
       use M
       implicit none
       real, dimension(n) :: x,y
       real :: compute2
      end function compute2

  end interface

real, external :: ftest
real :: c
c = compute(r,s)
top = ftest(r,s,c)
end function top

