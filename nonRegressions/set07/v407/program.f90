  REAL(KIND=8) function clock (timdmy)
  REAL(KIND=8) :: timdmy
    INTEGER ntime(3)
! call itime(ntime)
! clock=ntime(3)+ntime(2)*60+ntime(1)*3600
    call gtime(timdmy)
    clock=timdmy
    return
  end function clock

subroutine test(dummy)
 REAL(KIND=8) :: dummy, to1
 to1=clock(dummy)
end subroutine test
