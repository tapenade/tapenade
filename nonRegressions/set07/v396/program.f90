! a partir de lha07  pour test use only avec allocate(variable_differentiee)

module test
  real, dimension(:), allocatable :: global

contains

  subroutine top(a,x)
    real :: a,b,x
    b = 5.0
    call initgp()
    call foo(b, x)
    call initga(a)
    call foo(a, x)
  end subroutine top

  subroutine initga(a)
    real :: a
    global(0) = a
  end subroutine initga

  subroutine foo(v, x)
    real :: v,x
    integer :: i
    do i=1,5
       v = v + global(0)
    enddo
    x = x*x
  end subroutine foo

end module test

  subroutine initgp()
    use test, only : global
    ALLOCATE(global(3))
    global(0) = 1.0
  end subroutine initgp

