Module Operators
  Implicit none

  Interface assignment(=)
    Module procedure Chars_to_Integer
  End interface

Contains

  subroutine Chars_to_Integer(int, int_as_chars)
    Character(len=5), intent(in)  :: int_as_chars
    Integer, intent(OUT)          :: int
    Read (int_as_chars, FMT = '(I5)') int
  End subroutine Chars_to_Integer

End module Operators

Program Try_Operators

Use Operators, only: assignment(=)

Implicit none

Character(len=5)  :: string5 = '-1234'   ! Char. to integer

Integer           :: i_value = 99999  

  i_value = string5     ! Convert from a string to an integer.

  Print *, string5, i_value

End program Try_Operators

