!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 15:32
!
MODULE G1
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), POINTER :: cf1
  REAL(kind=8), DIMENSION(:), POINTER :: z
END MODULE G1

MODULE M_I
  IMPLICIT NONE
  INTERFACE 
      SUBROUTINE FOO1(cf1, z)
        REAL(kind=8), DIMENSION(:), POINTER :: cf1
        REAL(kind=8), DIMENSION(:), POINTER :: z
      END SUBROUTINE FOO1
  END INTERFACE

  INTERFACE 
      SUBROUTINE FOO2(cf1, z)
        REAL(kind=8), DIMENSION(:), POINTER :: cf1
        REAL(kind=8), DIMENSION(:), POINTER :: z
      END SUBROUTINE FOO2
  END INTERFACE

  INTERFACE 
      SUBROUTINE CALCUL()

      END SUBROUTINE CALCUL
  END INTERFACE

END MODULE M_I

PROGRAM MAIN
  USE G1
  USE M_I
  IMPLICIT NONE
  CALL FOO1(cf1, z)
!     z points to [alloc*z in foo1] or *z
!     cf1 points to [alloc*cf1 in foo1] or *cf1
  cf1 = 2
  z = 10
  CALL TOPLEVEL(cf1, z)
!     z points to Undef
!     cf1 points to Undef

CONTAINS
  SUBROUTINE TOPLEVEL(cf1, z)
!     z points to [alloc*z in foo1] or *z
!     cf1 points to [alloc*cf1 in foo1] or *cf1
!     z points to [alloc*z in foo1] or *z
!     cf1 points to [alloc*cf1 in foo1] or *cf1
    USE M_I
    IMPLICIT NONE
    REAL(kind=8), DIMENSION(:), POINTER :: cf1
    REAL(kind=8), DIMENSION(:), POINTER :: z
    CALL FOO1(cf1, z)
    CALL CALCUL()
    PRINT*, 'calcul cf1 ', cf1, ' -> ', z
    CALL FOO2(cf1, z)
!     z points to Undef
!     cf1 points to Undef
  END SUBROUTINE TOPLEVEL

END PROGRAM MAIN

SUBROUTINE FOO1(cf1, z)
  IMPLICIT NONE
!     z points to [alloc*z in foo1] or *z
!     cf1 points to [alloc*cf1 in foo1] or *cf1
!     z points to [alloc*z in foo1] or *z
!     cf1 points to [alloc*cf1 in foo1] or *cf1
  REAL(kind=8), DIMENSION(:), POINTER :: cf1
  REAL(kind=8), DIMENSION(:), POINTER :: z
  INTRINSIC ASSOCIATED
  IF (.NOT.ASSOCIATED(cf1)) THEN
    ALLOCATE(cf1(2))
!     cf1 points to [alloc*cf1 in foo1]
  END IF
!     cf1 points to [alloc*cf1 in foo1] or *cf1
  IF (.NOT.ASSOCIATED(z)) THEN
    ALLOCATE(z(2))
!     z points to [alloc*z in foo1]
  END IF
END SUBROUTINE FOO1
!     z points to [alloc*z in foo1] or *z

SUBROUTINE CALCUL()
!     z points to [alloc*z in foo1] or *z
!     cf1 points to [alloc*cf1 in foo1] or *cf1
  USE G1
  IMPLICIT NONE
  z = z*cf1
END SUBROUTINE CALCUL

SUBROUTINE FOO2(cf1, z)
  IMPLICIT NONE
!     z points to [alloc*z in foo1] or *z
!     cf1 points to [alloc*cf1 in foo1] or *cf1
!     z points to [alloc*z in foo1] or *z
!     cf1 points to [alloc*cf1 in foo1] or *cf1
  REAL(kind=8), DIMENSION(:), POINTER :: cf1
  REAL(kind=8), DIMENSION(:), POINTER :: z
  DEALLOCATE(cf1)
!     cf1 points to Undef
  DEALLOCATE(z)
!     z points to Undef
END SUBROUTINE FOO2

