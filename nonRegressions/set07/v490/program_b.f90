!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 15:32
!
MODULE G1_DIFF
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), POINTER :: cf1
  REAL(kind=8), DIMENSION(:), POINTER :: cf1b
  REAL(kind=8), DIMENSION(:), POINTER :: z
  REAL(kind=8), DIMENSION(:), POINTER :: zb
END MODULE G1_DIFF

MODULE M_I_DIFF
  IMPLICIT NONE
  INTERFACE 
      SUBROUTINE FOO1(cf1, z)
        REAL(kind=8), DIMENSION(:), POINTER :: cf1
        REAL(kind=8), DIMENSION(:), POINTER :: z
      END SUBROUTINE FOO1
  END INTERFACE

  INTERFACE 
      SUBROUTINE FOO1_B0(cf1, cf1b, z, zb)
        REAL(kind=8), DIMENSION(:), POINTER :: cf1
        REAL(kind=8), DIMENSION(:), POINTER :: cf1b
        REAL(kind=8), DIMENSION(:), POINTER :: z
        REAL(kind=8), DIMENSION(:), POINTER :: zb
      END SUBROUTINE FOO1_B0
      SUBROUTINE FOO1_B(cf1, cf1b, z, zb)
        REAL(kind=8), DIMENSION(:), POINTER :: cf1
        REAL(kind=8), DIMENSION(:), POINTER :: cf1b
        REAL(kind=8), DIMENSION(:), POINTER :: z
        REAL(kind=8), DIMENSION(:), POINTER :: zb
      END SUBROUTINE FOO1_B
  END INTERFACE

  INTERFACE 
      SUBROUTINE FOO2(cf1, z)
        REAL(kind=8), DIMENSION(:), POINTER :: cf1
        REAL(kind=8), DIMENSION(:), POINTER :: z
      END SUBROUTINE FOO2
  END INTERFACE

  INTERFACE 
      SUBROUTINE FOO2_B(cf1, cf1b, z, zb)
        REAL(kind=8), DIMENSION(:), POINTER :: cf1
        REAL(kind=8), DIMENSION(:), POINTER :: cf1b
        REAL(kind=8), DIMENSION(:), POINTER :: z
        REAL(kind=8), DIMENSION(:), POINTER :: zb
      END SUBROUTINE FOO2_B
  END INTERFACE

  INTERFACE 
      SUBROUTINE CALCUL_NODIFF()

      END SUBROUTINE CALCUL_NODIFF
  END INTERFACE

  INTERFACE 
      SUBROUTINE CALCUL_B()

      END SUBROUTINE CALCUL_B
  END INTERFACE

END MODULE M_I_DIFF

SUBROUTINE MAIN_NODIFF()
  USE G1_DIFF
  USE M_I_DIFF
  IMPLICIT NONE
  CALL FOO1(cf1, z)
  cf1 = 2
  z = 10
  CALL TOPLEVEL(cf1, z)

CONTAINS
  SUBROUTINE TOPLEVEL(cf1, z)
    USE M_I_DIFF
    IMPLICIT NONE
    REAL(kind=8), DIMENSION(:), POINTER :: cf1
    REAL(kind=8), DIMENSION(:), POINTER :: z
    CALL FOO1(cf1, z)
    CALL CALCUL_NODIFF()
    PRINT*, 'calcul cf1 ', cf1, ' -> ', z
    CALL FOO2(cf1, z)
  END SUBROUTINE TOPLEVEL

END SUBROUTINE MAIN_NODIFF

!  Differentiation of main as a context to call adjoint code (with options context):
!   Plus diff mem management of: z:in-out cf1:in-out
PROGRAM MAIN_B
  USE G1_DIFF
  USE M_I_DIFF
  USE DIFFSIZES
!  Hint: ISIZE1OFDrfcf1 should be the size of dimension 1 of array *cf1
!  Hint: ISIZE1OFDrfz should be the size of dimension 1 of array *z
  IMPLICIT NONE
  CALL FOO1_B0(cf1, cf1b, z, zb)
  cf1 = 2
  z = 10
  CALL ADCONTEXTADJ_INIT(0.87_8)
  IF (ASSOCIATED(z)) CALL ADCONTEXTADJ_INITREAL8ARRAY('z'//CHAR(0), z, &
&                                               zb, ISIZE1OFDrfz)
  IF (ASSOCIATED(cf1)) CALL ADCONTEXTADJ_INITREAL8ARRAY('cf1'//CHAR(0), &
&                                                 cf1, cf1b, &
&                                                 ISIZE1OFDrfcf1)
  CALL TOPLEVEL_B(cf1, cf1b, z, zb)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  IF (ASSOCIATED(z)) CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('z'//CHAR(0), &
&                                                   z, zb, ISIZE1OFDrfz)
  IF (ASSOCIATED(cf1)) CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('cf1'//CHAR(&
&                                                     0), cf1, cf1b, &
&                                                     ISIZE1OFDrfcf1)
  CALL ADCONTEXTADJ_CONCLUDE()

CONTAINS
!  Differentiation of toplevel in reverse (adjoint) mode (with options context):
!   gradient     of useful results: [alloc*z in foo1] [alloc*cf1 in foo1]
!                *z[from module g1] *cf1[from module g1] *z *cf1
!   with respect to varying inputs: [alloc*z in foo1] [alloc*cf1 in foo1]
!                *z[from module g1] *cf1[from module g1] *z *cf1
!   RW status of diff variables: [alloc*z in foo1]:in-out [alloc*cf1 in foo1]:in-out
!                *z[from module g1]:in-out *cf1[from module g1]:in-out
!                z:(loc) *z:in-out cf1:(loc) *cf1:in-out
!   Plus diff mem management of: z[from module g1]:in cf1[from module g1]:in
!                z:out cf1:out
  SUBROUTINE TOPLEVEL_B(cf1, cf1b, z, zb)
    USE M_I_DIFF
    IMPLICIT NONE
    REAL(kind=8), DIMENSION(:), POINTER :: cf1
    REAL(kind=8), DIMENSION(:), POINTER :: cf1b
    REAL(kind=8), DIMENSION(:), POINTER :: z
    REAL(kind=8), DIMENSION(:), POINTER :: zb
    CALL FOO2_B(cf1, cf1b, z, zb)
    CALL CALCUL_B()
    CALL FOO1_B(cf1, cf1b, z, zb)
  END SUBROUTINE TOPLEVEL_B

  SUBROUTINE TOPLEVEL(cf1, z)
    USE M_I_DIFF
    IMPLICIT NONE
    REAL(kind=8), DIMENSION(:), POINTER :: cf1
    REAL(kind=8), DIMENSION(:), POINTER :: z
    CALL FOO1(cf1, z)
    CALL CALCUL_NODIFF()
    PRINT*, 'calcul cf1 ', cf1, ' -> ', z
    CALL FOO2(cf1, z)
  END SUBROUTINE TOPLEVEL

END PROGRAM MAIN_B

!  Differentiation of foo1 in reverse (adjoint) mode (with options context):
!   Plus diff mem management of: z:out cf1:out
SUBROUTINE FOO1_B(cf1, cf1b, z, zb)
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), POINTER :: cf1
  REAL(kind=8), DIMENSION(:), POINTER :: cf1b
  REAL(kind=8), DIMENSION(:), POINTER :: z
  REAL(kind=8), DIMENSION(:), POINTER :: zb
  INTRINSIC ASSOCIATED
  INTEGER*4 :: branch
  IF (.NOT.ASSOCIATED(cf1)) THEN
    ALLOCATE(cf1b(2))
    cf1b = 0.0_8
    ALLOCATE(cf1(2))
    CALL PUSHCONTROL1B(0)
  ELSE
    CALL PUSHCONTROL1B(1)
  END IF
  IF (.NOT.ASSOCIATED(z)) THEN
    ALLOCATE(zb(2))
    zb = 0.0_8
    ALLOCATE(z(2))
    DEALLOCATE(z)
    DEALLOCATE(zb)
  END IF
  CALL POPCONTROL1B(branch)
  IF (branch .EQ. 0) THEN
    DEALLOCATE(cf1)
    DEALLOCATE(cf1b)
  END IF
END SUBROUTINE FOO1_B

!  Differentiation of foo1 as a context to call adjoint code (with options context):
!   Plus diff mem management of: z:in-out cf1:in-out
SUBROUTINE FOO1_B0(cf1, cf1b, z, zb)
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), POINTER :: cf1
  REAL(kind=8), DIMENSION(:), POINTER :: cf1b
  REAL(kind=8), DIMENSION(:), POINTER :: z
  REAL(kind=8), DIMENSION(:), POINTER :: zb
  INTRINSIC ASSOCIATED
  IF (.NOT.ASSOCIATED(cf1)) THEN
    ALLOCATE(cf1b(2))
    cf1b = 0.0_8
    ALLOCATE(cf1(2))
  END IF
  IF (.NOT.ASSOCIATED(z)) THEN
    ALLOCATE(zb(2))
    zb = 0.0_8
    ALLOCATE(z(2))
  END IF
END SUBROUTINE FOO1_B0

!  Differentiation of calcul in reverse (adjoint) mode (with options context):
!   gradient     of useful results: [alloc*z in foo1] [alloc*cf1 in foo1]
!                *z *cf1
!   with respect to varying inputs: [alloc*z in foo1] [alloc*cf1 in foo1]
!                *z *cf1
!   Plus diff mem management of: z:in cf1:in
SUBROUTINE CALCUL_B()
  USE G1_DIFF
  IMPLICIT NONE
  cf1b = cf1b + z*zb
  zb = cf1*zb
END SUBROUTINE CALCUL_B

SUBROUTINE CALCUL_NODIFF()
  USE G1_DIFF
  IMPLICIT NONE
  z = z*cf1
END SUBROUTINE CALCUL_NODIFF

!  Differentiation of foo2 in reverse (adjoint) mode (with options context):
!   Plus diff mem management of: z:out cf1:out
SUBROUTINE FOO2_B(cf1, cf1b, z, zb)
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), POINTER :: cf1
  REAL(kind=8), DIMENSION(:), POINTER :: cf1b
  REAL(kind=8), DIMENSION(:), POINTER :: z
  REAL(kind=8), DIMENSION(:), POINTER :: zb
END SUBROUTINE FOO2_B

