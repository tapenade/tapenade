module residual

  implicit none

  integer, parameter :: dim = 3
  
contains

subroutine foo(n,i2j,x,b,res)
  implicit none
  integer,               intent(in)    :: n
  integer, dimension(n), intent(in)    :: i2j
  real(8), dimension(n), intent(in)    :: x
  real(8), dimension(n), intent(inout) :: b
  real(8), dimension(n), intent(out)   :: res
  ! Local variables
  integer :: i

  res = 0.d0

  call bar(n,i2j,x,b)

  res = res + x*b
 
end subroutine foo


subroutine bar(n,i2j,x,b)
  implicit none
  integer,               intent(in)    :: n
  integer, dimension(n), intent(in)    :: i2j
  real(8), dimension(n), intent(in)    :: x
  real(8), dimension(n), intent(inout) :: b
  ! Local vars
  integer :: i,j
  
  do i=1, n
    j = i2j(i)
    call barbar(n,x(j),b(i))
  end do
 
end subroutine bar


subroutine barbar(n,x,b)
  implicit none
  integer, intent(in)    :: n
  real(8), intent(inout) :: b
  real(8), intent(in)    :: x
  
    b = b + x*x
 
end subroutine barbar

end module residual
