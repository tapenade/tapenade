program test_exit
    real :: x
    x = 1.9
    call foo(x)
end program test_exit

subroutine foo(x)
    integer :: STATUS = 99
    real :: x
    do
     if (x .ge. 0.0) then
      print *, 'This program is going to exit. ', x
      x = 0.0
      call EXIT(STATUS)
     else 
      x = x * x
     endif
    enddo
    print *, 'End of foo'
end subroutine foo
