module g1
  type ext_t
     REAL(KIND=8), dimension(:), pointer :: cf1
  end type ext_t
  REAL(KIND=8), dimension(:), pointer :: z
  type(ext_t) :: str
end module g1

program top
  use g1
  interface 
    subroutine foo(str2)
     use g1
     type(ext_t) :: str2
    end subroutine foo
  end interface
  allocate(str%cf1(99))
  allocate(z(88))
  z = 1.d0
  call foo(str)
  deallocate(str%cf1)
end program top

subroutine foo(str2)
  use g1
  type(ext_t) :: str2
  real(KIND=8) :: x
  interface 
    subroutine bar(cf3, x)
     REAL(KIND=8), dimension(:) :: cf3
!     REAL(KIND=8), dimension(:), pointer :: cf3
     real(KIND=8) :: x
    end subroutine bar
  end interface
  x = 5
  call bar(str2%cf1, x)
end subroutine foo

subroutine bar(cf3, x)
  use g1
  REAL(KIND=8), dimension(:) :: cf3
!  REAL(KIND=8), dimension(:), pointer :: cf3
  real(KIND=8) :: x
  x = x * cf3(5)
  z = z * x
end subroutine bar

