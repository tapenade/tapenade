!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_bugFixes) -  1 Dec 2020 19:47
!
MODULE M_DIFF
  IMPLICIT NONE

CONTAINS
!  Differentiation of top in forward (tangent) mode:
!   variations   of useful results: y
!   with respect to varying inputs: x
!   RW status of diff variables: x:in y:out
  SUBROUTINE TOP_D(x, xd, y, yd, singularite)
    USE SING
    IMPLICIT NONE
    TYPE(SINGULARITE_T) :: singularite
    REAL, DIMENSION(:) :: x, y
    REAL, DIMENSION(:) :: xd, yd
    CALL FOO_D(x, xd, y, yd, singularite%ptz)
  END SUBROUTINE TOP_D

  SUBROUTINE TOP(x, y, singularite)
    USE SING
    IMPLICIT NONE
    TYPE(SINGULARITE_T) :: singularite
    REAL, DIMENSION(:) :: x, y
    CALL FOO(x, y, singularite%ptz)
  END SUBROUTINE TOP

!  Differentiation of foo in forward (tangent) mode:
!   variations   of useful results: y
!   with respect to varying inputs: x
  SUBROUTINE FOO_D(x, xd, y, yd, z)
    USE DIFFSIZES
!  Hint: ISIZE1OFz should be the size of dimension 1 of array z
    IMPLICIT NONE
    REAL, DIMENSION(:) :: x, y
    REAL, DIMENSION(:) :: xd, yd
    REAL, DIMENSION(:), INTENT(IN) :: z
    REAL, DIMENSION(ISIZE1OFz) :: zd
    yd = 0.0
    CALL BAR_D(x, xd, y, yd)
    zd = 0.0
    CALL BAR_D(z, zd, y, yd)
  END SUBROUTINE FOO_D

  SUBROUTINE FOO(x, y, z)
    IMPLICIT NONE
    REAL, DIMENSION(:) :: x, y
    REAL, DIMENSION(:), INTENT(IN) :: z
    CALL BAR(x, y)
    CALL BAR(z, y)
  END SUBROUTINE FOO

!  Differentiation of bar in forward (tangent) mode:
!   variations   of useful results: b
!   with respect to varying inputs: a b
  SUBROUTINE BAR_D(a, ad, b, bd)
    IMPLICIT NONE
    REAL, DIMENSION(:) :: a, b
    REAL, DIMENSION(:) :: ad, bd
    bd = b*ad + a*bd
    b = a*b
  END SUBROUTINE BAR_D

  SUBROUTINE BAR(a, b)
    IMPLICIT NONE
    REAL, DIMENSION(:) :: a, b
    b = a*b
  END SUBROUTINE BAR

END MODULE M_DIFF

