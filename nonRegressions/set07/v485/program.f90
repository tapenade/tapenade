module FoX_dom_types

  type NodeList
    private
    character, pointer :: nodeName(:) => null()
  end type NodeList

end module FoX_dom_types
