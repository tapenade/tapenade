! from v185/program_p.f90
! il faut un msg DD25 pour call toto dans titi et test
!  
MODULE MODULE1
  IMPLICIT NONE
  PUBLIC toto

CONTAINS
  SUBROUTINE TOTO(a, b, c)
    IMPLICIT NONE
    REAL :: a, b, c
    c = a + b
  END SUBROUTINE TOTO
END MODULE MODULE1

MODULE MODULE2
  USE MODULE1
  IMPLICIT NONE
  PUBLIC tata
! tres important ce private 
! pour cacher toto pour les 
! modules qui importent module2
  PRIVATE 

CONTAINS
  SUBROUTINE TATA(x, y, z)
    IMPLICIT NONE
    REAL :: x, y, z
    CALL TOTO(x, y, z)
  END SUBROUTINE TATA
END MODULE MODULE2

!! une autre subroutine TOTO  
SUBROUTINE TOTO(a, b, c)
    IMPLICIT NONE
    REAL :: a, b, c
    c = a - b
END SUBROUTINE TOTO

MODULE MODULE3
  USE MODULE2
  IMPLICIT NONE

CONTAINS
  SUBROUTINE TITI(a, b, c, x, y, z)
    IMPLICIT NONE
    REAL :: a, b, c, x, y, z
    EXTERNAL TOTO
    CALL TOTO(a, b, c)
    CALL TATA(x, y, z)
  END SUBROUTINE TITI
END MODULE MODULE3

PROGRAM MAIN
  IMPLICIT NONE
  REAL :: a, b, c, x, y, z
  a = 1.0
  b = 2.0
  c = 3.0
  x = 4.0
  y = 5.0
  z = 6.0
  CALL TEST(a, b, c, x, y, z)
  WRITE(*, *) a, b
  WRITE(*, *) c, x
  WRITE(*, *) y, z
END PROGRAM MAIN

SUBROUTINE TEST(a, b, c, x, y, z)
  USE MODULE2
  IMPLICIT NONE
  REAL :: a, b, c, x, y, z
  EXTERNAL TOTO
  CALL TOTO(a, b, c)
  CALL TATA(x, y, z)
END SUBROUTINE TEST

