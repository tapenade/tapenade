MODULE MOD_PCG
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), ALLOCATABLE :: ddd, ddc, ddr, ddz,  dddia, &
& dddia1, ddrt
    REAL(kind=8) :: alphat

CONTAINS

  SUBROUTINE PCG (AA,IAA,IND,B,X,N,NZ,ITER,TOL)
      INTEGER:: N,NZ,IND(N+1),IAA(NZ)
      REAL(KIND=8)::B(N),X(N)
      REAL(KIND=8):: AA(NZ)
      REAL(KIND=8) ALPHA,ALPHAT,TOL,TOL2,TAU,BETHA,DIV,NORMB,GAMMA
      INTEGER:: K,I,ITER
      GAMMA=0.0D0
      K=0
      TOL2=TOL**2
      DDR(:)=0.0D0
      CALL CALDIA(GAMMA,DDDIA,AA,N,IAA,IND,DDR)
      CALL SMULTIP(AA,N,IAA,IND,X,DDR)
      DDR(1:N)=B(1:N)-DDR(1:N)
      ALPHA=0.0D0
      DO I=1,N
         ALPHA=ALPHA+DDR(I)**2
      END DO
      IF (ALPHA.LT.TOL2) GOTO 100
      CALL SSLOWER(DDDIA,AA,N,IAA,IND,DDR)
      ALPHA=0.0D0
      NORMB=0.0D0
      DO I=1,N
         NORMB=NORMB+B(I)*B(I)
         DDC(I)=DDDIA(I)*DDR(I)
         DDD(I)=DDC(I)
         DDDIA1(I)=2.0D0*DDDIA(I)-AA(IND(I))
         ALPHA=ALPHA+DDR(I)*DDC(I)
      END DO
      IF ((NORMB.LT.1.0D0).AND.(NORMB.GT.1.0D0-10)) TOL2=TOL2*NORMB

      DO K=1,ITER
          IF (ALPHA.LT.TOL2) THEN
             CALL SMULTIP(AA,N,IAA,IND,X,DDRT)
             ALPHAT=0.0D0
             DO I=1,N
                ALPHAT=ALPHAT+(B(I)-DDRT(I))**2
             END DO
             IF(ALPHAT.LT.TOL2*10.0D0.OR.ALPHA.LT.TOL2**2*10.0D0) GOTO 100
          END IF
          CALL SSUPPER(DDDIA,AA,N,IAA,IND,DDC,DDD,DDZ,DDDIA1)
          CALL SSLOWER(DDDIA,AA,N,IAA,IND,DDZ)
          DIV=0.0D0
          DO I=1,N
             DDZ(I)=DDD(I)+DDZ(I)
             DIV=DIV+DDC(I)*DDZ(I)
          END DO
          TAU=ALPHA/DIV
          X(1:N)=X(1:N)+TAU*DDD(1:N)
          DDR(1:N)=DDR(1:N)-TAU*DDZ(1:N)
          BETHA=1.0D0/ALPHA
          ALPHA=0.0D0
          DO I=1,N
             DDZ(I)=DDDIA(I)*DDR(I)
             ALPHA=ALPHA+DDR(I)*DDZ(I)
          END DO
          BETHA=BETHA*ALPHA
          DDC(1:N)=DDZ(1:N)+BETHA*DDC(1:N)
          DDD(1:N)=DDC(1:N)
      END DO
  100 CONTINUE
      END SUBROUTINE
END MODULE
