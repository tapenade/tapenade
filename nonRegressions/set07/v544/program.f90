module test
    INTEGER (KIND=4), PARAMETER :: C_INTPTR_T = 4
    TYPE :: C_PTR
        PRIVATE
        INTEGER(C_INTPTR_T) :: ptr
    END TYPE C_PTR
    TYPE(C_PTR), PARAMETER :: C_NULL_PTR = C_PTR(0)
end module test
