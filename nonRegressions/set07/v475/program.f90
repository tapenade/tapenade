module tNode
  type Node 
   real :: x
   type(Node), pointer :: next => null()
  end type Node
end module tNode

module test
 use tNode
 type(Node), pointer :: ff => null()

contains

  subroutine s1(y)
    real :: y
    ff => f1(ff%next,y)
  end subroutine s1

  function f1(fb,y) result(n)
    type(Node), pointer :: fb
    type(Node), pointer :: n
    real :: y
    n => fb
    n%x = y * 2
  end function f1

  subroutine s2(n)
    interface 
       function f2(g,h) 
         use tNode
         type(Node), pointer :: g
         integer :: h
         type(Node), pointer :: f2
       end function f2
    end interface
    integer :: n
    ff => f2(ff%next,n)
  end subroutine s2

  subroutine s3(n)
    interface 
       function f3(g,h) 
         use tNode
         type(Node), pointer :: g
         integer :: h
         type(Node), pointer :: f3
       end function f3
    end interface

    interface
       function foo(g)
         use tNode
         type(Node), pointer :: g
         type(Node), pointer :: foo
       end function foo
    end interface

    integer :: n
    ff => f3(foo(ff%next),n)
  end subroutine s3

end module test
