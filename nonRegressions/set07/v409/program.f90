function limiter(a, b)
  implicit none
  real(8) :: a, b, limiter, s
  dimension :: a(:), b(:), s(size(a))

  dimension :: limiter(size(a))
  s = sign(1d0, a)
  limiter = s*max(0d0, min(abs(a), s*b))
end function limiter
