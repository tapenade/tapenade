module M1
 type pair
   real, dimension(:)  , pointer ::f1
   integer, dimension(:)  , pointer ::i1
   integer f2
 end type pair
end module M1

subroutine foo(x,y,p1)
use M1
real x,y
type(pair), dimension(10) :: p1
call gee(p1(5)%f2, x, y, p1(4)%i1)
p1(3)%f1 = x
x = x + size(p1) + size(p1(2)%i1) + bar(p1(2)%i1)
y = y + p1(3)%f1(2)
end subroutine foo

subroutine gee(n,x,y,i)
real x,y
integer n
integer, dimension(10) :: i
y = y+ n*x*x
end subroutine gee
