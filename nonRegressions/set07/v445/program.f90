module const_m
implicit none

double precision,parameter::one=1
double precision,parameter::ten=10
double precision,parameter::small=ten**(-20)
double precision,parameter::large=one/small

contains

function foo(a) result(b)
  double precision a,b

  b = sin(a)
end function

end module
