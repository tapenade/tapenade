MODULE BO
  INTEGER, PARAMETER :: wp=4
  PRIVATE
  REAL(wp) :: avt_c=5.e-4_wp, rho_c=0.01_wp
contains

      subroutine head(i1,i2,o)
      double precision i1,i2,o,zn,zd
      call sub0(i1,i2)
      o = avt_c /  o
      return
      end subroutine

      subroutine sub0(u,v)
      double precision u,v
      u = u * v
      return
      end subroutine

END MODULE BO
