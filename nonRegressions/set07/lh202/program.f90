! From Calculix. In the declaration of a function,
! one should not declare the dimension of the return value
      subroutine BUG(AINV, DET)
      IMPLICIT NONE
      DOUBLE PRECISION, DIMENSION(3,3), INTENT(OUT) :: AINV
      DOUBLE PRECISION :: DET
      DOUBLE PRECISION, DIMENSION(3,3) :: COFACTOR

      COFACTOR(:,:) = DET
      AINV = TRANSPOSE(COFACTOR) / DET
      END

      function TRANSPOSE(X)
      IMPLICIT NONE
      DOUBLE PRECISION X(3,3),TRANSPOSE(3,3)
      TRANSPOSE(:,:) = X(2,3)
      end function

