! Same code as lh67, but the difference is that we will differentiate it
! with options -diffvarname g_% -difffuncname h_%_h -copyname cp%cp
module param_m
  integer,parameter::wp=8
  real(wp),parameter::g=-9.81_wp
end module

module struc_m
  use param_m,only: wp, g
  type::geom_t
    real(wp),dimension(:),allocatable::vol
    real(wp),dimension(:),allocatable::area
  end type
  type::prop_t
    real(wp),dimension(:),allocatable::den
    real(wp),dimension(:),allocatable::vis
  end type
  type::obj_t
    real(wp)::force
    real(wp)::speed
    real(wp)::acc
  end type
contains
  subroutine init_geom(geom,n)
  type(geom_t)::geom
  allocate(geom%vol(n))
  allocate(geom%area(n))
  end subroutine
  subroutine init_prop(prop,n)
  type(prop_t)::prop
  allocate(prop%den(n))
  allocate(prop%vis(n))
  end subroutine
  subroutine init_obj(obj,n)
  type(obj_t)::obj
  obj%force=0
  obj%speed=0
  obj%acc=0
  end subroutine
end module

module calc_m
contains
  subroutine calc_force(geom,prop,obj,acc)
  use param_m, only: wp, g
  use struc_m
  type(geom_t)::geom
  type(prop_t)::prop
  type(obj_t)::obj
  real(wp),dimension(:)::acc
  real(wp)::vol2, mass
  real(wp),dimension(size(geom%vol))::vol
  n = size(geom%vol)
  do i=1,n
    ilb=1; if(i==1)ilb=0
    iub=1; if(i==n)iub=0
    vol(i) = (geom%vol(i-ilb)+geom%vol(i+iub))/2
  end do
!$AD II-LOOP
  do i=1,n
    vol2 = geom%vol(i)
    mass = prop%den(i) * vol2 * obj%acc
    acc(i) = obj%force/mass
  end do
  obj%force = mass * (g + obj%acc + sum(acc))
  obj%acc = 0
  end subroutine
end module
