module tNode
  type Node 
   integer :: a
   real :: x
   type(Node), pointer :: next => null()
  end type Node

  type NodeT
   integer :: b
   integer :: c
   real :: x
   type(NodeT), pointer :: next => null()
  end type NodeT

end module tNode

module test
 use tNode
 type(Node), pointer :: ff => null()

contains

  subroutine s1(y)
    real :: y
    ff => f1(ff%next,y)
  end subroutine s1

  function f1(fb,y) result(n)
    type(Node), pointer :: fb
    type(Node), pointer :: n
    real :: y
    n => fb
    n%x = y * 2
  end function f1

  subroutine s2(n)
    interface 
       function f2(g,h) 
         use tNode
         type(Node), pointer :: g
         integer :: h
         type(Node), pointer :: f2
       end function f2
    end interface
    integer :: n
    ff => f2(ff%next,n)
  end subroutine s2

  subroutine s3(n)
    interface 
       function f3(g,h) 
         use tNode
         type(Node), pointer :: g
         integer :: h
         type(Node), pointer :: f3
       end function f3
    end interface

    interface foo
      module procedure foo2, foo1
    end interface

    integer :: n
    ff => f3(foo(ff%next),n)
  end subroutine s3

  
      function foo1(g)
         use tNode
         type(Node), pointer :: g
         type(Node), pointer :: foo1
         foo1 => g
         foo1%x = g%x * 2
      end function foo1

      function foo2(g)
         use tNode
         type(NodeT), pointer :: g
         type(Node), pointer :: foo2
         foo2 => ff
         foo2%x = g%x * 3
      end function foo2

end module test

      function f3(g,h)
         use tNode
         type(Node), pointer :: g
         integer :: h
         type(Node), pointer :: f3
         f3%x = g%x * 4
      end function f3
