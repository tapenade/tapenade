! pour tester intent(inout) dans interface sur parametre differentie' local

module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

module M1_I
   interface
      function compute (x,y,z)
       use M
       implicit none
       real, dimension(n), intent(inout) :: x,y
       real  :: compute
       real, intent(inout), dimension(:), allocatable :: z
      end function compute
   end interface
end module M1_I

function compute(x,y,z)
  use M
  implicit none
  real, dimension(n), intent(inout) :: x,y
  real :: compute
  real, intent(inout), dimension(:), allocatable :: z
  allocate(z(1))
  y = 2 * x
  compute = y(1) * y(2)
  global = global + compute
  z(1) = global
  deallocate(z)
end function compute

program prog
use M
use M1_I
implicit none
real, dimension(n) :: r,s
real, dimension(:), allocatable :: z
r(1) = 3.0
s(1) = 0.0
r(2) = 2.0
s(2) = 0.0
print*,compute(r,s,z)
end program prog
