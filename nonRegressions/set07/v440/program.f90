
! library sacada_ctm; do horizontal advection (semi lagrange scheme)
!
SUBROUTINE sl_fwd()
!-------------------------------------------------------------------------------
!
! Description:
!  Horizontal advection (semi lagrange scheme) is performed for the
!  field 'vmr'
!
! Method:
!(added from prog_s_02 GME 1.22:)
!     Compute the semi-Lagrangian trajectories to the departure point
!     for the 10 diamonds based on the horizontal wind components
!     (u, v) at the time level "t" (nt3c)
!
!(original routine:)
!  Volume mixing ratios at timelevel 'tlev_current' are interpolated to the 
!  departure points of semi lagrange trajectories yielding the vmr values
!  for time level 'tlev_next'. There are three different interpolation
!  schemes available: linear                      (la_method = la_linear)
!                     quadratic, positiv definite (la_method = la_qposdef)
!                     quadratic, monotonic        (la_method = la_qmonot)
!
!
! History:
!  Version      Date          Comment
!  --------     ----          -------
!  2.0          11.05.2007    Initial release: R.Botchorisvili, FhG SCAI
!                                              J. Schwinger, RIU
!  2.4			17.04.2013	  added derivation of departure points from GME
!												F. Baier, DFD
! Modules used:
USE mod_sac_nconst,   ONLY: MP
USE mod_sac_org,      ONLY: la_method,la_linear,la_qposdef,la_qmonot,          &
                            tlev_next,tlev_current,n_spec,i3cs,i3ce
!USE mod_gme_routines, ONLY: interp2ls_sacada, interp2qs_sacada
!USE mod_sac_fields,   ONLY: vmr
USE mod_gme_grid,     ONLY: eta,chi,cpsi,spsi,grd,ispoke,ispokes,i1mrp,        &
							i2mrp,bary
USE mod_gme_fields,   ONLY: ba_dep,bb_dep,it_dep, u, v
USE gme_data_parameters, ONLY : ireals, iintegers4
!-------------------------------------------------------------------------------

IMPLICIT NONE

INCLUDE "gme_param.h"
!INCLUDE "gme_comorg.h"
!INCLUDE "gme_horgrid.h"
!INCLUDE "gme_commlf.h"

!     Input: (from routine prog_s_02.f90) 
!INTEGER  ki1sc  , & ! first  dimension of calculation, start index
!         ki1ec  , & ! first  dimension of calculation, end   index
!         ki2sc  , & ! second dimension of calculation, start index
!         ki2ec      ! second dimension of calculation, end   index
!

!Global variable from mod_sac_fields
REAL(KIND=MP) :: vmr(1:n_spec,ig1s:ig1e,ig2s:ig2e,i3cs:i3ce)
! Local variables
REAL(KIND=MP) :: vmr_i(1:n_spec,ig1s:ig1e,ig2s:ig2e,i3cs:i3ce)
INTEGER       :: jd, ierr, mierr
LOGICAL       :: lmono, lposdef, ldebug

!INTEGER  jd        ! Loop variable
!INTEGER  mierr     ! error flag, set to 0 if no error occured

!temporary field variables for exchange with sltraj
REAL(KIND=ireals) :: pui     (ig1sm2:ig1ep2, ig2sm2:ig2ep2, i3s:i3e, nd)
!                    u-component of wind field (time level "nt3c")
REAL(KIND=ireals) :: pvi     (ig1sm2:ig1ep2, ig2sm2:ig2ep2, i3s:i3e, nd)
!                    v-component of wind field (time level "nt3c")
REAL(KIND=ireals) :: petai   (ig1sm1:ig1ep1, ig2sm1:ig2ep1, 7)
!                    eta-coordinates of the 6 (5) surrounding gridpoints  
REAL(KIND=ireals) :: pchii   (ig1sm1:ig1ep1, ig2sm1:ig2ep1, 7)
!                    chi-coordinates of the 6 (5) surrounding gridpoints  
REAL(KIND=ireals) :: pcpsii  (ig1sm1:ig1ep1, ig2sm1:ig2ep1, 6)
!                    cosine of the rotation angle for the wind rotation
REAL(KIND=ireals) :: pspsii  (ig1sm1:ig1ep1, ig2sm1:ig2ep1, 6)
!                    sine   of the rotation angle for the wind rotation
REAL(KIND=ireals) :: pbaryi  (ig1s  :ig1e  , ig2s  :ig2e  , 6)
!                    factor needed for the barycentric coordinates
!
REAL(KIND=ireals) :: pba_depo(ig1s  :ig1e  , ig2s  :ig2e  , i3s:i3e, nd),   &
!                    first barycentric coordinate of the departure point of
!                    the SL-trajectory
                     pbb_depo(ig1s  :ig1e  , ig2s  :ig2e  , i3s:i3e, nd)
!                    second barycentric coordinate of the departure point of
!                    the SL-trajectory
! 
ldebug=.FALSE.

!=======================================================================
!
!     Compute the semi-Lagrangian trajectories to the departure point 
!     for the 10 diamonds based on the horizontal wind components
!     (u, v) at the time level "t" (nt3c=1)

! note that sltraj uses different variable declarations for real & integer
! thus in/out variables have to be reformatted  !fb260413
!
      DO jd = 1,nd    ! Loop over all diamonds
!
! reformat input fields
ui=u; vi=v; etai=eta; chii=chi; cpsii=cpsi; spsii=spsi; baryi=bary

        CALL sltraj (ui      (ig1sm2, ig2sm2, i3cs   , jd),  &
     &               vi      (ig1sm2, ig2sm2, i3cs   , jd),  &
     &               etai   , chii   , cpsii  , spsii  ,             &
     &               baryi  , ispoke,                             &
     &               ig1sm2, ig1ep2, ig2sm2, ig2ep2,             &
     &               ig1sm1, ig1ep1, ig2sm1, ig2ep1,             &
     &               ig1s  , ig1e  , ig2s  , ig2e  ,             &
     &               i3cs  , i3ce  , jd    ,                     &
     &               ig1s , ig1e , ig2s , ig2e ,                 &
     &               ba_depo (ig1s  , ig2s  , i3cs   , jd),        &
     &               bb_depo (ig1s  , ig2s  , i3cs   , jd),        &
     &               it_dep  (ig1s  , ig2s  , i3cs   , jd), mierr)
!
        IF (mierr .NE. 0) THEN
          PRINT *, '  Error in *prog_s_02* calling *sltraj*, STOP!'
          CALL gmabort
        ENDIF
!
! reformat REAL output fields
ba_dep(ig1s:ig1e,ig2s:ig2e,i3cs:i3ce,jd)=real(ba_depo(ig1s:ig1e,ig2s:ig2e,i3cs:i3ce,jd))
bb_dep(ig1s:ig1e,ig2s:ig2e,i3cs:i3ce,jd)=real(bb_depo(ig1s:ig1e,ig2s:ig2e,i3cs:i3ce,jd))

      ENDDO  ! End of the loop over the diamonds
!
!=======================================================================
!

!SELECT CASE(la_method)
!CASE(la_linear)
  ! Loop over all diamonds
  DO jd=1,nd
    CALL interp2ls_sacada(vmr(1,ig1sm2,ig2sm2,i3cs,tlev_current,jd),           &
                          1      , n_spec  ,                                   &
                          ig1sm2 , ig1ep2  , ig2sm2 , ig2ep2 ,                 &
                          i3cs   , i3ce    ,                                   &
                          ba_dep(ig1s,ig2s,i3cs,jd),                           &
                          bb_dep(ig1s,ig2s,i3cs,jd),                           &
                          it_dep(ig1s,ig2s,i3cs,jd),                           &
                          ig1s   , ig1e    , ig2s   , ig2e   ,                 &
                          ispoke ,                                             &
                          ig1s   , ig1e    , ig2s   , ig2e    , jd ,           &
                          ldebug ,                                             &
                          vmr_i(1,ig1s,ig2s,i3cs),                             &
                          ig1s   , ig1e    , ig2s   , ig2e    , ierr)

    vmr(1:n_spec,ig1s:ig1e,ig2s:ig2e,i3cs:i3ce,tlev_next,jd) = vmr_i
  END DO

!CASE(la_qposdef,la_qmonot)
!non-linear mode currently not active fb030513
!  IF(la_method == la_qposdef) THEN
!    lposdef = .TRUE.
!    lmono   = .FALSE.
!  ELSE
!    lposdef = .FALSE.
!    lmono   = .TRUE.
!  END IF
!!  ! Loop over all diamonds
!    CALL interp2qs_sacada(vmr(1,ig1sm2,ig2sm2,i3cs,tlev_current,jd),           &
!                          1     , n_spec,                                      &
!                          ig1sm2, ig1ep2 , ig2sm2, ig2ep2,                     &
!                          i3cs  , i3ce   ,                                     &
!                         ba_dep(ig1s,ig2s,i3cs,jd),                           &
!                         bb_dep(ig1s,ig2s,i3cs,jd),                           &
!                          it_dep(ig1s,ig2s,i3cs,jd),                           &
!                          ig1s  , ig1e   , ig2s  , ig2e  ,                     &
!                          eta   , chi    , cpsi  , spsi  , grd,                &
!                          ig1sm1, ig1ep1 , ig2sm1, ig2ep1,                     &
!                          ispoke, ispokes, i1mrp , i2mrp ,                     &
!                         ig1s  , ig1e   , ig2s  , ig2e  , jd ,                &
!                          lmono , lposdef , ldebug ,                           &
!                          vmr_i(1,ig1s,ig2s,i3cs),                             &
!                          ig1s  , ig1e  , ig2s , ig2e , ierr)
!
!    vmr(1:n_spec,ig1s:ig1e,ig2s:ig2e,i3cs:i3ce,tlev_next,jd) = vmr_i
!  END DO

!END SELECT

!-------------------------------------------------------------------------------
END SUBROUTINE fw_fwd





