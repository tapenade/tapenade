
!
!@(#) Library gmtri: Global gridpoint model, triangular grid
!@(#) Module gme_param.h, V1.22 from 2/12/03, extracted: 3/20/03
!
!     gme_param.h
!
!=======================================================================
!
!     Global dimensions of arrays including domain decomposition
!     Length of INTEGER variables for GRIB routines
!
!=======================================================================
!
! Current Code Owner: DWD, D. Majewski
!    phone: +49-69-8062-2728, fax: +49-69-8062-3721
!    email: detlev.majewski@dwd.de
!
!=======================================================================
!
! History:
! Version    Date       Name
! ---------- ---------- ----
! 1.1        2000/01/28 D. Majewski
!  Initial release
! 1.14       2002/01/16 A. Mueller 
!  remove intgrib
!
! Code Description:
! Language: Fortran 90.
! Software Standards: "European Standards for Writing and
! Documenting Exchangeable Fortran 90 Code".
!
!=======================================================================
!
!     A domain decomposition for the efficient use of MPP systems is
!     introduced by subdividing each of the 10 diamonds into the same
!     number of subdomains. A full diamond contains (0:ni) x (1:ni+1)
!     gridpoints, a subdomain (ig1s:ig1e) x (ig2s:ig2e) ones.
!     Each processing element PE holds data of all 10 diamonds.
!     The subdivision is given by "nproc1" and "nproc2" in header file
!     "gme_commpi.h". nproc1 is the number of PEs in the j1 direction,
!     "nproc2" is the number of PEs in the j2 direction.
!
!     A "halo" of one or two grid rows/columns (extended array) is de-
!     fined to ease communication between neighbouring subdomains.
!     With "halo", a subdomain contains for an extension by 1 row/column
!     (ig1sm1:ig1ep1) x (ig2sm1:ig2ep1) gridpoints
!     and for an extension by 2 rows/columns
!     (ig1sm2:ig1ep2) x (ig2sm2:ig2ep2) gridpoints.
!     The communication is performed in subroutine *xd_p*.
!
! Declarations:
!
!     ni:    number of intervals on a main triangle side; 
!            ni=3**ni3*2**ni2 with ni3 0 or 1 and ni2 > 1
!
!     ig1s:  first dimension of arrays, start index  (ig1s >= 0)
!     ig1sm1 = ig1s - 1
!     ig1sm2 = ig1s - 2
!
!     ig1e:  first dimension of arrays, end index    (ig1e <= ni)
!     ig1ep1 = ig1e + 1
!     ig1ep2 = ig1e + 2
!
!     ig2s:  second dimension of arrays, start index (ig2s >= 1)
!     ig2sm1 = ig2s - 1
!     ig2sm2 = ig2s - 2
!
!     ig2e:  second dimension of arrays, end index   (ig2e <= ni+1)
!     ig2ep1 = ig2e + 1
!     ig2ep2 = ig2e + 2
!
!     i3s:   third dimension (vertical), start index (i3s  = 1)
! 
!     i3e:   third dimension (vertical), end index 
!           (i3e = number of layers)
! 
!     nt2:   two time levels   (nt2 = 2)
!     nt3:   three time levels (nt3 = 3)
!
!     ids:   start index of diamonds (ids =  1)
!     ide:   end index of diamonds   (ide = 10)
!     nd:    number of diamonds      (nd  = ide-ids+1 = 10)
!
!     nsi:   number of 2-d Helmholtz equations in SI scheme
!           (split semi-implicit treatment)
!
!     The variables ni, i3e, nsi, nproc1 and nproc2 are defined via 
!     NAMELIST input in subroutine *readnlst*
!
!=======================================================================
!
      INTEGER   ni, ig1s, ig1sm1 , ig1sm2, ig1e , ig1ep1, ig1ep2,  &
     &              ig2s, ig2sm1 , ig2sm2, ig2e , ig2ep1, ig2ep2,  &
     &              i3s , i3e    , nt2   , nt3  ,                  &
     &              ids , ide    , nd    , ni2  , ni3   , nsi
!
      PARAMETER ( i3s = 1 , nt2 = 2 , nt3 = 3 ,                    &
     &            ids = 1 , ide = 10, nd  = ide-ids+1 )
!
      COMMON /gme_param/ ni    , ni2   , ni3   , i3e   , nsi   ,         &
     &                   ig1s  , ig1sm1, ig1sm2, ig1e  , ig1ep1, ig1ep2, &
     &                   ig2s  , ig2sm1, ig2sm2, ig2e  , ig2ep1, ig2ep2

