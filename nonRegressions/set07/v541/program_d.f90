!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 15:32
!
!  Differentiation of prog as a context to call tangent code (with options context):
PROGRAM PROG_D
  IMPLICIT NONE
  REAL, DIMENSION(4) :: x
  REAL, DIMENSION(4) :: xd
  REAL, DIMENSION(5) :: y
  REAL, DIMENSION(5) :: yd
  REAL, SAVE :: summ=0.0
  INTEGER :: i
  DO i=1,4
    x(i) = 1.0/i
  END DO
  DO i=1,5
    y(i) = 2.0/(i+1.0)
  END DO
  CALL ADCONTEXTTGT_INIT(1.e-4_8, 0.87_8)
  CALL ADCONTEXTTGT_INITREAL4ARRAY('x'//CHAR(0), x, xd, 4)
  CALL ADCONTEXTTGT_INITREAL4ARRAY('y'//CHAR(0), y, yd, 5)
  CALL TESTALLOCS_D(x, xd, y, yd)
  CALL ADCONTEXTTGT_STARTCONCLUDE()
  CALL ADCONTEXTTGT_CONCLUDEREAL4ARRAY('x'//CHAR(0), x, xd, 4)
  CALL ADCONTEXTTGT_CONCLUDEREAL4ARRAY('y'//CHAR(0), y, yd, 5)
  CALL ADCONTEXTTGT_CONCLUDE()
  DO i=1,4
    summ = summ + x(i)
  END DO
  DO i=1,5
    summ = summ + y(i)
  END DO
  PRINT*, 'Summ ', summ
END PROGRAM PROG_D

!  Differentiation of testallocs in forward (tangent) mode (with options context):
!   variations   of useful results: [alloc*z in testallocs] [alloc*gpp in subb1]
!                [alloc*lpp in suba] x y
!   with respect to varying inputs: [alloc*z in testallocs] [alloc*gpp in subb1]
!                [alloc*lpp in suba] x y
!   RW status of diff variables: [alloc*z in testallocs]:in-out
!                [alloc*gpp in subb1]:in-out [alloc*lpp in suba]:in-out
!                x:in-out y:in-out
SUBROUTINE TESTALLOCS_D(x, xd, y, yd)
  IMPLICIT NONE
  REAL, DIMENSION(4) :: x
  REAL, DIMENSION(4) :: xd
  REAL, DIMENSION(5) :: y
  REAL, DIMENSION(5) :: yd
  REAL, DIMENSION(:), ALLOCATABLE, TARGET :: z
  REAL, DIMENSION(:), ALLOCATABLE, TARGET :: zd
  REAL, DIMENSION(:), ALLOCATABLE :: pp
  REAL, DIMENSION(:), ALLOCATABLE :: ppd
  INTEGER :: i, j
  INTERFACE 
      SUBROUTINE SUBB1(y, z, gpp)
        REAL, DIMENSION(5) :: y
        REAL, DIMENSION(:) :: z
        REAL, DIMENSION(:), ALLOCATABLE, TARGET :: gpp
      END SUBROUTINE SUBB1
      REAL FUNCTION SUBB2(z, gpp)
        REAL, DIMENSION(:) :: z
        REAL, DIMENSION(:), ALLOCATABLE :: gpp
      END FUNCTION SUBB2
  END INTERFACE

  INTERFACE 
      SUBROUTINE SUBB1_D(y, yd, z, zd, gpp, gppd)
        REAL, DIMENSION(5) :: y
        REAL, DIMENSION(5) :: yd
        REAL, DIMENSION(:) :: z
        REAL, DIMENSION(:) :: zd
        REAL, DIMENSION(:), ALLOCATABLE, TARGET :: gpp
        REAL, DIMENSION(:), ALLOCATABLE, TARGET :: gppd
      END SUBROUTINE SUBB1_D
      REAL FUNCTION SUBB2_D(z, zd, gpp, gppd, subb2)
        REAL, DIMENSION(:) :: z
        REAL, DIMENSION(:) :: zd
        REAL, DIMENSION(:), ALLOCATABLE :: gpp
        REAL, DIMENSION(:), ALLOCATABLE :: gppd
        REAL :: subb2
      END FUNCTION SUBB2_D
  END INTERFACE

  xd(3) = y(2)*xd(4) + x(4)*yd(2)
  x(3) = x(4)*y(2)
  ALLOCATE(zd(7))
  zd = 0.0
  ALLOCATE(z(7))
  DO i=1,7
    zd(i) = (i-1)*xd(3)
    z(i) = x(3)*(i-1)
  END DO
  CALL SUBA_D(x, xd, y, yd)
  DO j=2,3
    xd(j) = z(j)*xd(j-1) + x(j-1)*zd(j)
    x(j) = x(j-1)*z(j)
  END DO
  CALL SUBB1_D(y, yd, z, zd, pp, ppd)
  xd(4) = pp(3)*ppd(4) + pp(4)*ppd(3)
  x(4) = pp(4)*pp(3)
  xd(1) = x(4)*xd(1) + x(1)*xd(4)
  x(1) = x(1)*x(4)
  xd(3) = SUBB2_D(z, zd, pp, ppd, x(3))
END SUBROUTINE TESTALLOCS_D

!  Differentiation of suba in forward (tangent) mode (with options context):
!   variations   of useful results: [alloc*lpp in suba] y
!   with respect to varying inputs: [alloc*lpp in suba] x y
! from C lh21 v534
SUBROUTINE SUBA_D(x, xd, y, yd)
  IMPLICIT NONE
  REAL, DIMENSION(4) :: x
  REAL, DIMENSION(4) :: xd
  REAL, DIMENSION(5) :: y
  REAL, DIMENSION(5) :: yd
  REAL, DIMENSION(:), ALLOCATABLE :: lpp
  REAL, DIMENSION(:), ALLOCATABLE :: lppd
  INTEGER :: i
  ALLOCATE(lppd(4))
  lppd = 0.0
  ALLOCATE(lpp(4))
  DO i=1,4
    lppd(i) = (i-1)*(y(i+1)*xd(i)+x(i)*yd(i+1))
    lpp(i) = x(i)*(i-1)*y(i+1)
  END DO
  DO i=1,4
    yd(i) = yd(i+1) - lppd(i)
    y(i) = y(i+1) - lpp(i)
  END DO
  IF (ALLOCATED(lppd)) THEN
    DEALLOCATE(lppd)
  END IF
  DEALLOCATE(lpp)
! a tester avec ou sans deallocate
END SUBROUTINE SUBA_D

!  Differentiation of subb1 in forward (tangent) mode (with options context):
!   variations   of useful results: [alloc*gpp in subb1]
!   with respect to varying inputs: [alloc*gpp in subb1] y z
!   Plus diff mem management of: gpp:out
SUBROUTINE SUBB1_D(y, yd, z, zd, gpp, gppd)
  IMPLICIT NONE
  REAL, DIMENSION(5) :: y
  REAL, DIMENSION(5) :: yd
  REAL, DIMENSION(:) :: z
  REAL, DIMENSION(:) :: zd
  REAL, DIMENSION(:), ALLOCATABLE, TARGET :: gpp
  REAL, DIMENSION(:), ALLOCATABLE, TARGET :: gppd
  INTEGER :: i
  REAL :: tmp
  INTRINSIC INT
  ALLOCATE(gppd(10))
  gppd = 0.0
  ALLOCATE(gpp(10))
  DO i=1,10
    tmp = 1 + (i-1)/2
    gppd(i) = (i-1)*yd(INT(tmp))
    gpp(i) = (i-1)*y(INT(tmp))
  END DO
  gppd(7) = z(1)*gppd(7) + gpp(7)*zd(1)
  gpp(7) = gpp(7)*z(1)
END SUBROUTINE SUBB1_D

!  Differentiation of subb2 in forward (tangent) mode (with options context):
!   variations   of useful results: z subb2
!   with respect to varying inputs: [alloc*gpp in subb1] z *gpp
!   Plus diff mem management of: gpp:in-out
REAL FUNCTION SUBB2_D(z, zd, gpp, gppd, subb2)
  IMPLICIT NONE
  REAL, DIMENSION(:) :: z
  REAL, DIMENSION(:) :: zd
  REAL, DIMENSION(:), ALLOCATABLE :: gpp
  REAL, DIMENSION(:), ALLOCATABLE :: gppd
  REAL :: res
  REAL :: resd
  INTEGER :: j
  REAL :: temp
  REAL :: subb2
  res = 1.0
  resd = 0.0
  DO j=1,4
    temp = res*gpp(j)
    resd = z(6-j)*(gpp(j)*resd+res*gppd(j)) + temp*zd(6-j)
    res = temp*z(6-j)
  END DO
  DO j=1,7
    zd(j) = 0.0
    z(j) = 0.0
  END DO
  IF (ALLOCATED(gppd)) THEN
    DEALLOCATE(gppd)
  END IF
  DEALLOCATE(gpp)
  subb2_d = resd
  subb2 = res
END FUNCTION SUBB2_D

