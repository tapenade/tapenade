module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

function compute(x,y)
  use M
  implicit none
  real, dimension(n) :: x,y
  real :: compute
  y = 2 * x
  compute = y(1) * y(2)
  global = global + compute
end function compute

function ftest(r,s,compute)
  use M
  implicit none
  real, dimension(n) :: r,s
  real ftest
  real, external :: compute
  ftest = compute(r,s)
end function ftest

function top(r,s)
use M
implicit none
real, dimension(n) :: r,s
real top
real, external :: compute
real, external :: ftest
top = ftest(r,s,compute)
end function top

program prog
use M
implicit none
real, dimension(n) :: r,s
real top
r(1) = 3.0
s(1) = 0.0
r(2) = 2.0
s(2) = 0.0
print*,top(r,s)
end program prog
