#ifndef _SONICS_TMO_CUDA_UTILITIES_H_
#define _SONICS_TMO_CUDA_UTILITIES_H_

#ifdef ENABLE_CUDA
// ------------------------------------------------------------------
// External include
#include <memory>
#include <vector>
#include <cassert>
#include <thrust/system_error.h>
#include <thrust/system/cuda/error.h>
// ------------------------------------------------------------------

// ------------------------------------------------------------------
// Internal include
// ------------------------------------------------------------------

// ===========================================================================
// Interface for mdbuffer
// ===========================================================================
__host__ void* _cudaMalloc(size_t n);

// ===========================================================================
__host__ void* _cudaMallocManaged(size_t n);

// ===========================================================================
__host__ void _cudaCopyHostToDevice(const size_t& size, void* a_h, void* a_d);

// ===========================================================================
__host__ void _cudaCopyDeviceToHost(const size_t& size, void* a_h, void* a_d);

// ===========================================================================
__host__ void _cudaFree(void* p);

// ===========================================================================
// Typed interface
// ===========================================================================
__host__ void copyHostToDevice(const int& size, double* a, double* d_a);

// ===========================================================================
__host__ void copyHostToDeviceInt(const int& size, int* a, int* d_a);

// ===========================================================================
__host__ double* allocateCuda(const int& n_vtx);

// ===========================================================================
__host__ int* allocateCudaInt(const int& n_vtx);

// ===========================================================================
__host__ void freeCuda(double* a);

// ===========================================================================
__host__ void freeCudaInt(int* a);

// ===========================================================================
// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
// #if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
// #endif
  return result;
}

// ===========================================================================
constexpr int n_thread_per_block = 64;

struct gpu_kernel_config {
  //! Work groups (CUDA blocks) counts
  std::size_t grid_size[3] = { 1, 1, 1 };
  //! Per work group (CUDA block) thread counts
  std::size_t block_size[3] = { 1, 1, 1 };
  //! Shared memory size in bytes
  std::size_t shared_memory_size = 0;
};


#else

// ===========================================================================
// Interface for mdbuffer
// ===========================================================================
void* _cudaMalloc(size_t n);

// ===========================================================================
void* _cudaMallocManaged(size_t n);

// ===========================================================================
void _cudaCopyHostToDevice(const size_t& size, void* a_h, void* a_d);

// ===========================================================================
void _cudaCopyDeviceToHost(const size_t& size, void* a_d, void* a_h);

// ===========================================================================
void _cudaFree(void* p);

#endif


#endif /* _SONICS_TMO_CUDA_UTILITIES_H_ */
