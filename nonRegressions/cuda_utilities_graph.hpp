#ifndef _SONICS_TMO_CUDA_UTILITIES_GRAPH_H_
#define _SONICS_TMO_CUDA_UTILITIES_GRAPH_H_
#ifdef ENABLE_CUDA

// ------------------------------------------------------------------
// External include
#include <memory>
#include <vector>
#include <utility>
// ------------------------------------------------------------------

// ------------------------------------------------------------------
// Internal include
#include "sonics/tmo/cuda_utilities.hpp"
// ------------------------------------------------------------------

// ===========================================================================
template<typename F, typename... Args>
constexpr
void
prepare_kernel_graph_node(cudaGraph_t&                  graph,
                          cudaGraphNode_t&              kernel_node,
                          std::vector<cudaGraphNode_t>& node_dependencies,
                          dim3                          thr_topo,
                          dim3                          blk_topo,
                          F&&                           f,
                          Args&&...                     args)
{
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  cudaKernelNodeParams kernel_node_params = {0};
  kernel_node_params.func           = (void *) f;
  kernel_node_params.gridDim        = thr_topo;
  kernel_node_params.blockDim       = blk_topo;
  kernel_node_params.sharedMemBytes = 0;

  int args_size = sizeof...(Args);
  // void* kernel_args[args_size] = { static_cast<void*>(&std::forward<Args>(args))... , }; // Marche paps si un type est const
  const void* kernel_args[args_size] = { static_cast<const void*>(&std::forward<Args>(args))... , };

  kernel_node_params.kernelParams = (void **) kernel_args;
  kernel_node_params.extra        = NULL;

  // printf(" node_dependencies.size()::%d\n", node_dependencies.size());
  checkCuda( cudaGraphAddKernelNode(&kernel_node, graph, node_dependencies.data(),
                                     node_dependencies.size(), &kernel_node_params));

}

// ===========================================================================
template<typename F, typename... Args>
constexpr
void
prepare_kernel_graph_node(cudaGraph_t&     graph,
                          cudaGraphNode_t& kernel_node,
                          dim3             thr_topo,
                          dim3             blk_topo,
                          F&&              f,
                          Args&&...        args)
{
  std::vector<cudaGraphNode_t> node_dependencies_init;
  prepare_kernel_graph_node(graph, kernel_node,
                            node_dependencies_init,
                            thr_topo, blk_topo,
                            f,
                            std::forward<Args>(args)...);
}

#endif /* ENABLE_CUDA */
#endif /* _SONICS_TMO_CUDA_UTILITIES_GRAPH_H_ */
