!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
MODULE MODVAR
  IMPLICIT NONE
  REAL, DIMENSION(100, 100), ALLOCATABLE :: var1
  REAL, DIMENSION(:), ALLOCATABLE :: var2
  REAL, DIMENSION(:, :), ALLOCATABLE :: facnor
  INTEGER, SAVE :: allocdone=0
  INTEGER :: dim1, dim2

CONTAINS
  SUBROUTINE MKALLOC()
    IMPLICIT NONE
    INTRINSIC ALLOCATED
    IF (allocdone .EQ. 0) THEN
      ALLOCATE(var1)
!     var1 points to [alloc*var1 in mkalloc]
      var1 = 0.0
      ALLOCATE(var2(100))
!     var2 points to [alloc*var2 in mkalloc]
      var2 = 0.0
      allocdone = 1
    END IF
!     var1 points to [alloc*var1 in mkalloc] or *var1
!     var2 points to [alloc*var2 in mkalloc] or *var2
    IF (.NOT.ALLOCATED(facnor)) THEN
      ALLOCATE(facnor(dim1, dim2))
!     facnor points to [alloc*facnor in mkalloc]
    END IF
  END SUBROUTINE MKALLOC
!     facnor points to [alloc*facnor in mkalloc] or *facnor

END MODULE MODVAR

SUBROUTINE TEST()
  USE MODVAR
  IMPLICIT NONE
  REAL :: x, y
  EXTERNAL UNKNF1
  CALL UNKNF1(var2, y)
  x = var2(7)*var2(10)
  var1(2, :) = var2(:)*var1(3, :)
  CALL FF1(x, y)
  var2(3) = var2(4)*y
END SUBROUTINE TEST

SUBROUTINE FF1(a, b)
  USE MODVAR
  IMPLICIT NONE
  REAL :: a, b
  EXTERNAL UNKNF2
  CALL UNKNF2(var2, a)
  a = var2(3)*var1(10, 20) + a
  b = a*a
END SUBROUTINE FF1

