module modvar
  REAL,DIMENSION(1:100,1:100),  ALLOCATABLE :: var1
  REAL,DIMENSION(:),  ALLOCATABLE :: var2
  REAL, DIMENSION(:,:), ALLOCATABLE :: FACNOR
  INTEGER :: allocdone = 0
  INTEGER :: dim1,dim2
CONTAINS
  subroutine mkalloc
    if (allocdone.eq.0) THEN
       ALLOCATE(var1)
       var1 = 0.0
       ALLOCATE(var2(100))
       var2 = 0.0
       allocdone = 1
    endif
    IF(.NOT.ALLOCATED(FACNOR))THEN
       ALLOCATE(FACNOR(dim1,dim2))
    ENDIF
  end subroutine mkalloc
end module modvar

subroutine test
  USE modvar
  real x,y
  call unknf1(var2,y)
  x = var2(7)*var2(10)
  var1(2,:) = var2(:)*var1(3,:)
  call FF1(x,y)
  var2(3) = var2(4)*y
end subroutine test

subroutine FF1(a,b)
  USE modvar
  real a,b
  call unknf2(var2,a)
  a= var2(3)*var1(10,20)+a
  b = a*a
end subroutine FF1
