module m
type toto
   real, allocatable :: value
end type toto

contains

subroutine program(toto)
      type(toto) :: toto

      allocate(toto%value)
      deallocate(toto%value)
    end subroutine program
  end module m

