subroutine top(a, b, r, p)
  real :: a, b, r
  real, pointer :: p, p1

  a = 1.0
  r = 3.5
  allocate(p)
  if(a == 2.0) then
     p => a
     a = 3.5
  else if(a > 2.0) then
        p => b
     else
        allocate(p, p1, STAT=1)
  endif
  a = 2.0
  r = 2 * p + a

end subroutine top
