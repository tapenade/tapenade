MODULE prog

CONTAINS
SUBROUTINE TOTO(a,b,c,d,e,f,g,h,i)
        INTEGER, INTENT(IN) :: a ! f_e
        INTEGER, OPTIONAL, INTENT(OUT) :: b, c, d, e ! nb_dims,nb_vars,nb_atts,id_unlm
        INTEGER,DIMENSION(:),OPTIONAL,INTENT(OUT) :: f, g, h ! nn_idm,nn_ldm,nn_aid
        CHARACTER(LEN=*),DIMENSION(:),OPTIONAL,INTENT(OUT) :: i ! cc_ndm
END SUBROUTINE

SUBROUTINE TITI
        INTEGER a
        INTEGER i, j
        INTEGER, PARAMETER :: jpi = 10, jpj = 10
        INTEGER, DIMENSION(jpi) :: bb, cc, dd, ee
        INTEGER, DIMENSION(jpi,jpk) :: ff, gg, hh
        i = 1
        j = 1
        CALL TOTO(a, b=bb(i), e=ee(i), d=dd(i), g=gg(:,j), h=hh(:,j), f=ff(:,j))

END SUBROUTINE


END MODULE
