MODULE ERR
LOGICAL :: ioipsl_debug
CONTAINS

SUBROUTINE ipsldbg (new_status,old_status)
!!--------------------------------------------------------------------
!! The "ipsldbg" routine
!! allows to activate or deactivate the debug,
!! and to know the current status of the debug.
!!
!! SUBROUTINE ipsldbg (new_status,old_status)
!!
!! Optional INPUT argument
!!
!! (L) new_status : new status of the debug
!!
!! Optional OUTPUT argument
!!
!! (L) old_status : current status of the debug
!!--------------------------------------------------------------------
  IMPLICIT NONE
!-
  LOGICAL,OPTIONAL,INTENT(IN)  :: new_status
  LOGICAL,OPTIONAL,INTENT(OUT) :: old_status
!---------------------------------------------------------------------
  IF (PRESENT(old_status)) THEN
    old_status = ioipsl_debug
  ENDIF
  IF (PRESENT(new_status)) THEN
    ioipsl_debug = new_status
  ENDIF
!---------------------
END SUBROUTINE ipsldbg
END MODULE

SUBROUTINE test
	USE ERR
	CALL IPSLDBG(old_status=.true.)
END SUBROUTINE
