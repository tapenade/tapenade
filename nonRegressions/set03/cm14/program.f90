! Test d un traitement d un appel de fonction
! Avec pointeur.
! Verifie la mecanique de traduction des elements elementaire
! pour une destination initiale de pointeur.

module top
  use struct

contains
  subroutine topsub(p1, p2, p3)
    real, pointer :: p1
    type(tr1), pointer :: p2
    type(tr2), pointer :: p3

    call equal(p2, p3)
  end subroutine topsub

  subroutine equal(p1, p2)
    type(tr1), pointer :: p1
    type(tr2), pointer :: p2

    p2%x = p1%x
  end subroutine equal
end module top

module struct
  type tr1
     real :: x, y
  end type tr1

  type tr2
     real :: x
     type(tr2), pointer :: n
  end type tr2
end module struct
