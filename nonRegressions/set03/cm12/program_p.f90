!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
! Test construction arguments elementaires
! + construction des arbres initialTargets et pointedElemArgs pour
! chaque pointer
MODULE TOP
  IMPLICIT NONE
  TYPE TR1
      REAL, POINTER :: x
      REAL :: y
  END TYPE TR1
  TYPE TR2
      REAL :: x
      TYPE(TR2), POINTER :: n
  END TYPE TR2

CONTAINS
  SUBROUTINE TOPSUB(p1, p2, p3, object)
    IMPLICIT NONE
    REAL, POINTER :: p1
    TYPE(TR1), POINTER :: p2
    TYPE(TR2), POINTER :: p3
    TYPE(TR2), TARGET :: object
    p2%x => p1
!     *p2.x points to *p1 or *(*p2.x)
    p3%n => object
!     *p3.n points to object or *p3
  END SUBROUTINE TOPSUB

  SUBROUTINE TOP(p1, p2, p3, object)
    IMPLICIT NONE
    REAL, POINTER :: p1
    TYPE(TR1), POINTER :: p2
    TYPE(TR2), POINTER :: p3
    TYPE(TR2) :: object
    CALL TOPSUB(p1, p2, p3, object)
!     *p2.x points to *p1 or *(*p2.x)
!     *p3.n points to object or *p3
  END SUBROUTINE TOP

END MODULE TOP

