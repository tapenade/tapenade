! Test construction arguments elementaires
! + construction des arbres initialTargets et pointedElemArgs pour
! chaque pointer

module top
  type tr1
     real, pointer :: x
     real y
  end type tr1

  type tr2
     real :: x
     type(tr2), pointer :: n
  end type tr2

contains
  subroutine topsub(p1, p2, p3, object)
    real, pointer :: p1
    type(tr1), pointer :: p2
    type(tr2), pointer :: p3
    type(tr2), target :: object

    p2%x => p1;
    p3%n => object
  end subroutine topsub

  subroutine top(p1, p2, p3, object)
    real, pointer :: p1
    type(tr1), pointer :: p2
    type(tr2), pointer :: p3
    type(tr2) :: object

    call topsub(p1, p2, p3, object)
  end subroutine top
end module top
