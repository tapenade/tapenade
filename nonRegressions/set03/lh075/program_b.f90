!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) - 10 Nov 2023 17:10
!
!  Differentiation of bug_lidar in reverse (adjoint) mode:
!   gradient     of useful results: aa bb
!   with respect to varying inputs: aa bb
!   RW status of diff variables: aa:in-out bb:in-out
SUBROUTINE BUG_LIDAR_B(aa, aab, bb, bbb)
  IMPLICIT NONE
  DOUBLE PRECISION :: aa(100), bb(100)
  DOUBLE PRECISION :: aab(100), bbb(100)
  INTEGER :: ii, jj, kk
  INTRINSIC SIN
  DOUBLE PRECISION :: tmp
  DOUBLE PRECISION :: tmpb
  DOUBLE PRECISION :: tmp0
  DOUBLE PRECISION :: tmpb0
  DOUBLE PRECISION :: tmp1
  DOUBLE PRECISION :: tmpb1
  INTEGER*4 :: branch
  INTEGER :: ad_count
  INTEGER :: i
  CALL PUSHINTEGER4(ii)
  CALL PUSHCONTROL1B(0)
  DO ii=1,50
    CALL PUSHREAL8(aa(ii))
    aa(ii) = aa(ii)*aa(ii)
    CALL PUSHINTEGER4(jj)
    ad_count = 1
    DO jj=1,60
      tmp = bb(ii)*bb(jj)
      CALL PUSHREAL8(bb(ii))
      bb(ii) = tmp
      IF (bb(jj) .LE. 8.) THEN
        CALL PUSHREAL8(bb(jj))
        bb(jj) = aa(ii)
        CALL PUSHCONTROL1B(0)
      ELSE IF (bb(jj) .LE. 10.) THEN
        CALL PUSHREAL8(bb(jj))
        bb(jj) = 3*aa(ii)
        CALL PUSHCONTROL1B(1)
      ELSE
        GOTO 120
      END IF
      CALL PUSHREAL8(aa(jj))
      aa(jj) = 2*bb(ii)
      DO kk=1,5
        tmp0 = 5*aa(jj)
        CALL PUSHREAL8(aa(kk))
        aa(kk) = tmp0
      END DO
      CALL PUSHINTEGER4(jj)
      ad_count = ad_count + 1
    END DO
    CALL PUSHCONTROL1B(0)
    CALL PUSHINTEGER4(ad_count)
    CALL PUSHREAL8(bb(ii))
    bb(ii) = SIN(bb(ii))
    DO kk=1,6
      tmp1 = 5*bb(ii)
      CALL PUSHREAL8(bb(kk))
      bb(kk) = tmp1
    END DO
    CALL PUSHINTEGER4(ii)
    CALL PUSHCONTROL1B(1)
  END DO
 100 CALL POPCONTROL1B(branch)
  IF (branch .NE. 0) THEN
    CALL POPINTEGER4(ii)
    DO kk=6,1,-1
      CALL POPREAL8(bb(kk))
      tmpb1 = bbb(kk)
      bbb(kk) = 0.D0
      bbb(ii) = bbb(ii) + 5*tmpb1
    END DO
    CALL POPREAL8(bb(ii))
    bbb(ii) = COS(bb(ii))*bbb(ii)
    CALL POPINTEGER4(ad_count)
    DO i=1,ad_count
      IF (i .EQ. 1) THEN
        CALL POPCONTROL1B(branch)
        IF (branch .EQ. 0) GOTO 110
      ELSE
        DO kk=5,1,-1
          CALL POPREAL8(aa(kk))
          tmpb0 = aab(kk)
          aab(kk) = 0.D0
          aab(jj) = aab(jj) + 5*tmpb0
        END DO
        CALL POPREAL8(aa(jj))
        bbb(ii) = bbb(ii) + 2*aab(jj)
        aab(jj) = 0.D0
        CALL POPCONTROL1B(branch)
        IF (branch .EQ. 0) THEN
          CALL POPREAL8(bb(jj))
          aab(ii) = aab(ii) + bbb(jj)
          bbb(jj) = 0.D0
        ELSE
          CALL POPREAL8(bb(jj))
          aab(ii) = aab(ii) + 3*bbb(jj)
          bbb(jj) = 0.D0
        END IF
      END IF
      CALL POPREAL8(bb(ii))
      tmpb = bbb(ii)
      bbb(ii) = bb(jj)*tmpb
      bbb(jj) = bbb(jj) + bb(ii)*tmpb
 110  CALL POPINTEGER4(jj)
    END DO
    CALL POPREAL8(aa(ii))
    aab(ii) = 2*aa(ii)*aab(ii)
    GOTO 100
  END IF
  CALL POPINTEGER4(ii)
  GOTO 130
 120 CALL PUSHCONTROL1B(1)
  CALL PUSHINTEGER4(ad_count)
  STOP
 130 CONTINUE
END SUBROUTINE BUG_LIDAR_B

