SUBROUTINE bug_lidar(AA,BB)
IMPLICIT NONE

DOUBLE PRECISION AA(100),BB(100)
INTEGER ii,jj,kk
INTRINSIC SIN

DO ii=1,50
   AA(ii) = AA(ii)*AA(ii)
   DO jj=1,60
      BB(ii) = BB(ii)*BB(jj)
 
      IF (BB(jj).LE.8.) THEN
         BB(jj) = AA(ii)
      ELSE IF (BB(jj).LE.10.) THEN
         BB(jj) = 3*AA(ii)
      ELSE 
         STOP
      ENDIF

      AA(jj) = 2*BB(ii)
      DO kk=1,5
         AA(kk)=5*AA(jj)
      ENDDO
   ENDDO   !---jj
   BB(ii) = sin(BB(ii))
   DO kk=1,6
      BB(kk)=5*BB(ii)
   ENDDO
ENDDO  !--ii

END SUBROUTINE bug_lidar
