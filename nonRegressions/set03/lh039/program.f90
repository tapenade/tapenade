subroutine testintrinsics(A,B,I,CH)
REAL A(*),B
INTEGER I
CHARACTER(*) CH
REAL(KIND=8) :: tim, tim2, dummy

I = I + LENGTH(TRIM(CH))

I = I + COUNT(A(:).gt.0.0)

if (ALL(A(:).gt.0.0)) I = I+10

A(2) = A(2) + EPSILON(A(2))

call gtime(tim)

A(4) = A(5)*B

B=2.0*B + PRODUCT(A(3:20))

B=2.0*B + MAXVAL(A(3:20))

B = B*A(4)

I = I + IARGC()

CALL GETARG(2,CH)

tim2 = TIMER(dummy)

B = B*(tim+tim2)

A(6:10) = DABS(A(1:5))

end subroutine testintrinsics
