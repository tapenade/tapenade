module main_m
contains
  function cross_prod(v1,v2) result(v)
    real,dimension(3),intent(in)::v1,v2
    real,dimension(3)::v
    integer,dimension(3),parameter::i1=(/2,3,1/)
    integer,dimension(3),parameter::i2=(/3,1,2/)
    v=v1(i1)*v2(i2)-v2(i1)*v1(i2)
  end function cross_prod
end module main_m

subroutine top(u)
  use main_m
  real,dimension(3)::u,z1

  z1=(/11.,12.,13./)
  u=cross_prod(u,z1)

end subroutine top
