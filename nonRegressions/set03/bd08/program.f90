MODULE toto

PUBLIC :: histbeg

INTERFACE histbeg
  MODULE PROCEDURE histbeg1, histbeg2
END INTERFACE
      

CONTAINS

SUBROUTINE histbeg1                           &
 & (pfilename, pim, plon, pjm, plat,          &
 &  par_orix, par_szx, par_oriy, par_szy,     &
 &  pitau0, pdate0, pdeltat, phoriid, pfileid, domain_id)
  IMPLICIT NONE
      
  CHARACTER(LEN=*) :: pfilename
  INTEGER,INTENT(IN) :: pim,pjm
  REAL,DIMENSION(pim),INTENT(INOUT) :: plon
  REAL,DIMENSION(pjm),INTENT(IN) :: plat
  INTEGER,INTENT(IN):: par_orix,par_szx,par_oriy,par_szy
  INTEGER,INTENT(IN) :: pitau0
  REAL,INTENT(IN) :: pdate0,pdeltat
  INTEGER :: pfileid,phoriid
  INTEGER,INTENT(IN),OPTIONAL :: domain_id
  plon(1) = plon(1) * 2
END SUBROUTINE
      
SUBROUTINE histbeg2                          &
 & (pfilename, pim, plon, pjm, plat,         &
 &  par_orix, par_szx, par_oriy, par_szy,    &
 &  pitau0, pdate0, pdeltat, phoriid, pfileid, &
 &  opt_rectilinear, domain_id)
  IMPLICIT NONE
  CHARACTER(LEN=*) :: pfilename
  INTEGER,INTENT(IN) :: pim,pjm
  REAL,DIMENSION(pim,pjm),INTENT(INOUT) :: plon,plat
  INTEGER,INTENT(IN):: par_orix, par_szx, par_oriy, par_szy
  INTEGER,INTENT(IN) :: pitau0
  REAL,INTENT(IN) :: pdate0, pdeltat
  INTEGER :: pfileid, phoriid
  LOGICAL,INTENT(IN),OPTIONAL :: opt_rectilinear
  INTEGER,INTENT(IN),OPTIONAL :: domain_id
!-
  plon(1,1) = plon(1,1) * 2

END SUBROUTINE
END MODULE toto

SUBROUTINE HEAD(titi,tata)
  USE TOTO
  REAL, DIMENSION(10) :: titi, tata
  CALL histbeg("coin", 10, titi, 10, tata, 1, 2, 3, 4, 5, 0.1, 0.2, 6, 7, domain_id=12)
END SUBROUTINE HEAD
