MODULE MM1
  REAL :: vv1,vv2,vv3
END MODULE MM1

SUBROUTINE GLOBSUB1(xx)
  USE MM1
  REAL xx
  vv1 = vv1*xx
  vv2 = 2.0*vv2
  xx = xx*vv1*vv2
END SUBROUTINE GLOBSUB1

SUBROUTINE GLOBSUB2(xx)
  REAL xx
  REAL gg1,gg2
  COMMON /COMGG/gg1,gg2
  gg1 = gg1*xx
  xx = xx*gg1*gg2
END SUBROUTINE GLOBSUB2

MODULE MM3
  REAL :: ww1,ww2
END MODULE MM3

SUBROUTINE GLOBSUB3(xx)
  USE MM3
  REAL xx
  ww1 = ww1*xx
  xx = xx*ww1*ww2
END SUBROUTINE GLOBSUB3

SUBROUTINE TOP(aa,bb)
  USE MM1, ONLY : vv3, renamevv => vv2
  REAL aa,bb
  call GLOBSUB1(aa)
  call GLOBSUB2(bb)
  call GLOBSUB3(aa)
  bb = aa*bb
END SUBROUTINE TOP
