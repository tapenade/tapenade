! BUG 3 proposed by Dominic Jones.
!  3) formal params declared with size (:) may cause a
!     problem for the size of their derivative,
!     when the derivative is local.
module struc_m
  type::geom_t
     real(4),dimension(:),allocatable::vol
     real(4),dimension(:),allocatable::area
  end type geom_t
  type::prop_t
     real(4),dimension(:),allocatable::den
     real(4),dimension(:),allocatable::vis
  end type prop_t
  type::obj_t
     real(4)::force
     real(4), private ::speed
     real(4)::acc
  end type obj_t
end module struc_m

module calc_m
  real(4), allocatable, dimension(:) :: nt1
contains
  subroutine calc_force(geom,prop,obj,acc,nt2,nt3,nt4,nt5)
    use struc_m, only: geom_t, prop_t, obj_t
    type(geom_t)::geom
    type(prop_t)::prop
    type(obj_t)::obj
    real(4),dimension(:)::nt2
    real(4),dimension(:)::acc,nt3
    real(4) :: nt4(3,:)
    type(obj_t)::nt5(:)
    nt2(5) = acc(5)
    nt1(3) = acc(8)
    nt3(2) = acc(10)
    nt4(2,1) = acc(4)
    nt5(2)%force = acc(5)
    acc(6) = nt1(3)*nt2(5) + nt3(2)*nt4(2,1)+nt5(2)%force
    obj%force = acc(6)*acc(7)
  end subroutine calc_force
end module calc_m
