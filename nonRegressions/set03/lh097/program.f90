module MM

  type typea
     real :: y
     integer :: j
  end type typea

  type typeb
     real :: x
     integer :: i
  end type typeb

  type typetop
     real :: truc
     type (typea) :: chose
     type (typeb) :: machin
     integer :: iii
  end type typetop

contains

  subroutine top(vv)
    real :: vv
    type(typetop), DIMENSION(5) :: A,B
    B%chose%y = 2.0
    B%machin%x = vv
    A = B
    vv = vv + A(2)%machin%x + A(3)%chose%y
  end subroutine top

end module MM
