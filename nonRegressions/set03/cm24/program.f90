module m
type toto
   real, pointer :: x
end type toto
contains 

  subroutine top(a, r, object)
    real :: a,r
    type(toto) :: object

    allocate(object%x)
    object%x = a
    r = object%x * a
    deallocate(object%x)
  end subroutine top
end module m
