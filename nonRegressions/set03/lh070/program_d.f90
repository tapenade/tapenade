!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 19 Jan 2024 17:05
!
!  Differentiation of rosenbrock in forward (tangent) mode:
!   variations   of useful results: rosenbrock
!   with respect to varying inputs: x
!   RW status of diff variables: x:in rosenbrock:out
REAL FUNCTION ROSENBROCK_D(x, xd, n, rosenbrock)
  IMPLICIT NONE
  INTEGER :: n
  REAL, DIMENSION(n) :: x
  REAL, DIMENSION(n) :: xd
  INTRINSIC SUM
  REAL, DIMENSION(n-1) :: arg1
  REAL, DIMENSION(n-1) :: arg1d
  REAL, DIMENSION(n-1) :: temp
  REAL :: rosenbrock
  temp = x(2:n) - x(1:n-1)*x(1:n-1)
  arg1d = 100.0*2*temp*(xd(2:n)-2*x(1:n-1)*xd(1:n-1)) - 2*(1-x(1:n-1))*&
&   xd(1:n-1)
  arg1 = 100.0*(temp*temp) + (1-x(1:n-1))*(1-x(1:n-1))
  rosenbrock_d = SUM(arg1d)
  rosenbrock = SUM(arg1)
END FUNCTION ROSENBROCK_D

