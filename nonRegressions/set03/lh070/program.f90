real function rosenbrock(X, n)
  integer n
  real, dimension(n) :: X
  rosenbrock = SUM(100.0*(X(2:n)-X(1:n-1)**2)**2  &
       &           + (1-X(1:n-1))**2)
end function rosenbrock
