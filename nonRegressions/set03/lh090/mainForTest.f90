PROGRAM MAIN
  call TESTDD(5.5d0)
  call TESTDD(1.2d0)
  call TESTDD(0.6d0)
  call TESTDD(-2.7d0)
END PROGRAM MAIN

SUBROUTINE TESTDD(origvvv)
  USE TEST_D
  IMPLICIT NONE
  TYPE(TTTT) :: xxx1
  TYPE(TTTT_D) :: xxx1d
  DOUBLE PRECISION ::  origvvv, vvv, keepvvv
  DOUBLE PRECISION :: vvvd, keepvvvd
  aaa1d = 0.0
  aaa2d = 0.0
  aaa3d = 0.0
  aaa4d = 0.0
  aaa1 = 0.1
  aaa2 = 0.2
  aaa3 = 0.3
  aaa4 = 0.4
  ALLOCATE(xxx1d%fff1(11, 11))
  ALLOCATE(xxx1%fff1(11, 11))
  ALLOCATE(xxx1d%fff2(11, 11))
  ALLOCATE(xxx1%fff2(11, 11))
  xxx1d%fff3 => aaa1d
  xxx1%fff3 => aaa1
  xxx1d%fff4 => aaa1d
  xxx1%fff4 => aaa1
  xxx1d%fff1 = 0.0
  xxx1d%fff2 = 0.0
  xxx1d%fff3 = 0.0
  xxx1d%fff4 = 0.0
  xxx1%fff1 = 1.1
  xxx1%fff2 = 1.2
  xxx1%fff3 = 1.3
  xxx1%fff4 = 1.4
  ALLOCATE(xxx2d%fff1(8, 8))
  ALLOCATE(xxx2%fff1(8, 8))
  ALLOCATE(xxx2d%fff2(8, 8))
  ALLOCATE(xxx2%fff2(8, 8))
  xxx2d%fff3 => aaa1d
  xxx2%fff3 => aaa1
  xxx2d%fff4 => aaa1d
  xxx2%fff4 => aaa1
  xxx2d%fff1 = 0.0
  xxx2d%fff2 = 0.0
  xxx2d%fff3 = 0.0
  xxx2d%fff4 = 0.0
  xxx2%fff1 = 3.1
  xxx2%fff2 = 3.2
  xxx2%fff3 = 3.3
  xxx2%fff4 = 3.4

  vvvd = 0.0
  vvv = origvvv
  vvv = vvv + 3.e-7
  CALL TOP_D(xxx1, xxx1d, vvv, vvvd)
  keepvvv = vvv

  aaa1 = 0.1
  aaa2 = 0.2
  aaa3 = 0.3
  aaa4 = 0.4
  xxx1d%fff3 => aaa1d
  xxx1%fff3 => aaa1
  xxx1d%fff4 => aaa1d
  xxx1%fff4 => aaa1
  xxx1%fff1 = 1.1
  xxx1%fff2 = 1.2
  xxx1%fff3 = 1.3
  xxx1%fff4 = 1.4
  xxx2d%fff3 => aaa1d
  xxx2%fff3 => aaa1
  xxx2d%fff4 => aaa1d
  xxx2%fff4 => aaa1
  xxx2%fff1 = 3.1
  xxx2%fff2 = 3.2
  xxx2%fff3 = 3.3
  xxx2%fff4 = 3.4

  vvvd = 1.0
  vvv = origvvv
  CALL TOP_D(xxx1, xxx1d, vvv, vvvd)
  print *,' DD:',(keepvvv-vvv)/3.e-7
  print *,'TGT:',vvvd
  print *,'----------'

  DEALLOCATE(xxx1%fff1)
  DEALLOCATE(xxx1%fff2)
  DEALLOCATE(xxx2%fff1)
  DEALLOCATE(xxx2%fff2)
  DEALLOCATE(xxx1d%fff1)
  DEALLOCATE(xxx1d%fff2)
  DEALLOCATE(xxx2d%fff1)
  DEALLOCATE(xxx2d%fff2)
END SUBROUTINE TESTDD
