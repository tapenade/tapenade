subroutine testpushregions(gcr,r1,d1,d2,ind)
! To test the indices used for push and pop of array regions
integer i,j,d1,d2,ind(d2)
REAL(16) :: gcr(d1,d2),r1
!
gcr(:,:) = gcr(:,:)*r1
call foo(gcr(:,ind(:)), ind)
gcr(5,5) = gcr(3,3)*gcr(2,2)
end
