MODULE SIMTEST1
  IMPLICIT NONE

  TYPE CRTYPE
      REAL*8, DIMENSION(:), ALLOCATABLE :: isodat
  END TYPE CRTYPE

CONTAINS

  FUNCTION qCalc(this, Cmob)
    IMPLICIT NONE
    TYPE(CrType) :: this
    REAL*8       :: Cmob,qCalc
    INTEGER Ncomp

    Ncomp = SIZE(this%isodat, dim=1)
    qCalc = Cmob*this%isodat(Ncomp/2)
  END FUNCTION qCalc

END MODULE SIMTEST1
