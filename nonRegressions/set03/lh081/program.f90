subroutine testwhere(XX,TT,UU,VV)
  REAL*8, DIMENSION(20) :: XX,TT,UU,VV,MM
  INTEGER i
  do i=1,20
     MM(i) = 12.0*i
  enddo
  VV(:) = 1.5*XX(:)
  where (MM.GT.123.4)
     UU(:) = 3.0 * XX(:)
  end where
  TT(:) = 2.5*XX(:)
end subroutine testwhere
