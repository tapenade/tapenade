!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.2 (r3057M) - 07/03/2009 16:51
!  
MODULE DATA_TYPES
  IMPLICIT NONE
  INTEGER, PARAMETER :: mcell=100
  TYPE GRIDDATA
      SEQUENCE 
      REAL(kind=8), DIMENSION(mcell) :: x
      REAL(kind=8), DIMENSION(mcell) :: y
  END TYPE GRIDDATA
  TYPE SOLUTIONDATA
      SEQUENCE 
      INTEGER :: n
      REAL(kind=8), DIMENSION(mcell) :: a
      REAL(kind=8), DIMENSION(mcell) :: b
      REAL(kind=8), DIMENSION(mcell) :: c
  END TYPE SOLUTIONDATA
END MODULE DATA_TYPES

PROGRAM MAIN
  USE DATA_TYPES
  IMPLICIT NONE
  TYPE(GRIDDATA) :: grddat
  TYPE(SOLUTIONDATA), DIMENSION(2) :: soldat
  CALL FUNCTION(grddat, soldat)
END PROGRAM MAIN

SUBROUTINE FUNCTION(grddat, soldat)
  USE DATA_TYPES
  IMPLICIT NONE
  TYPE(GRIDDATA) :: grddat
  TYPE(SOLUTIONDATA), DIMENSION(2) :: soldat
  soldat(1)%a = soldat(2)%b*grddat%x + soldat(2)%c + grddat%y
END SUBROUTINE FUNCTION

