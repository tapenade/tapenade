!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.12 (r6321M) -  3 Mar 2017 12:43
!
MODULE M
  IMPLICIT NONE
  TYPE T1
      INTEGER :: a
      REAL :: b(20)
  END TYPE T1
END MODULE M

SUBROUTINE TESTTYPES(n, arg, x)
  USE M
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: n
  TYPE(T1), DIMENSION(2:n), TARGET, INTENT(IN) :: arg
  INTRINSIC SIZE
  REAL, DIMENSION(SIZE(arg%b)), TARGET :: loc
  REAL*8, INTENT(INOUT) :: x
  loc(2) = x
  x = x*loc(2)
END SUBROUTINE TESTTYPES

