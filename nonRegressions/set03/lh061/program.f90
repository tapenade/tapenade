MODULE M
  TYPE :: T1
     INTEGER :: a
     REAL :: b(20)
  END TYPE T1
END MODULE M

SUBROUTINE testTypes(n,arg,x)
  use M
  INTEGER, INTENT(IN) :: n
  TYPE(T1), DIMENSION(2:n), TARGET, INTENT(IN) :: arg
  REAL, dimension(size(arg%b)), target :: loc
  REAL*8, INTENT(INOUT) :: x

  loc(2) = x
  x = x*loc(2)
END
