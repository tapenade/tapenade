! Cas delicat si un test risque d'etre note adjoint dead
! parce qu'il ne contient que du code adjoint dead
! mais pourtant il contient des affectations de pointeur
! qui sont required explicit
! => le test ne doit pas etre adjoint dead !!
subroutine nonadjdeadtest(x,y)
  real :: x,y
  real, pointer :: p,q
  integer :: k
  k = 1
  p => x
  if (x.gt.0.0) then
     k = k+1
     p => y
  endif
  y = x*x + p + k
end subroutine nonadjdeadtest
