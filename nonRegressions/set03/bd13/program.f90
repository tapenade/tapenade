module test1

interface toto
 function toto_one (a, b, c)
	integer, intent(in) :: a, b
        integer, intent(out) :: c
	integer toto_one
 end function
 function toto_dims (a, b, c)
	integer, intent(in) :: a, b(:)
        integer, intent(out) :: c
	integer toto_dims
 end function

end interface
contains

subroutine test(b)
 integer a, b(10), c, d
 d = toto(a,b,c)
end subroutine

end module
