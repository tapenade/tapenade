subroutine TOP()
  real, dimension(:) :: x
  real, dimension(:), allocatable :: y

  allocate(x)
  allocate(y)
  deallocate(x)
  deallocate(y)

end subroutine TOP

subroutine TOP2(x2, y2)
  real, dimension(:) :: x2
  real, dimension(:), allocatable :: y2

  allocate(x2)
  allocate(y2)
  deallocate(x2)
  deallocate(y2)

end subroutine TOP2
