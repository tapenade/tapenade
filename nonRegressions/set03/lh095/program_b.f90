!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
MODULE MM_DIFF
  IMPLICIT NONE
  TYPE TTT1
      REAL :: truc
      REAL :: chose
      INTEGER :: iii
  END TYPE TTT1
  TYPE TTT1_DIFF
      REAL :: truc
      REAL :: chose
  END TYPE TTT1_DIFF

CONTAINS
!  Differentiation of top in reverse (adjoint) mode:
!   gradient     of useful results: a.truc a.chose b.truc b.chose
!   with respect to varying inputs: a.truc a.chose b.truc b.chose
!   RW status of diff variables: a.truc:in-zero a.chose:in-zero
!                b.truc:incr b.chose:incr
  SUBROUTINE TOP_B(a, ab, b, bb)
    IMPLICIT NONE
    TYPE(TTT1) :: a, b
    TYPE(TTT1_DIFF) :: ab, bb
    bb%truc = bb%truc + ab%truc
    bb%chose = bb%chose + ab%chose
    ab%truc = 0.0
    ab%chose = 0.0
  END SUBROUTINE TOP_B

  SUBROUTINE TOP(a, b)
    IMPLICIT NONE
    TYPE(TTT1) :: a, b
    a = b
  END SUBROUTINE TOP

END MODULE MM_DIFF

