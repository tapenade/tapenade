subroutine testarrays(dof, res)
! To test the array notation bug that reappeared in 2.1.3
integer i
REAL :: dof(10,100), res(10,100), resc(100)

do i = 1,10
   resc     = dof(i,:)
   res(i,:) = 2d0*resc
enddo

end
