PROGRAM MAIN
  call TESTDD(-2.7d0)
  call TESTDD(1.2d0)
  call TESTDD(2.2d0)
  call TESTDD(3.2d0)
  call TESTDD(4.2d0)
  call TESTDD(5.5d0)
  call TESTDD(6.5d0)
  call TESTDD(7.1d0)
  call TESTDD(10.6d0)
  call TESTDD(12.6d0)
  call TESTDD(16.2d0)
END PROGRAM MAIN

SUBROUTINE TESTDD(origvvv)
  USE TEST_DIFF
  IMPLICIT NONE
  TYPE(TTTT) :: xxx1
  TYPE(TTTT_DIFF) :: xxx1d
  DOUBLE PRECISION ::  origvvv, vvv, keepvvv
  DOUBLE PRECISION :: vvvd, keepvvvd
  double precision, dimension(9,9), target :: passv,passvd
  passv = 1.1d0
  passvd = 0.d0
  aaa2d = 0.0d0
  aaa4d = 0.0d0
  aaa2 = 0.2d0
  aaa4 = 0.4d0
  ALLOCATE(xxx1d%fff1(11, 11))
  ALLOCATE(xxx1%fff1(11, 11))
  ALLOCATE(xxx1d%fff2(11, 11))
  ALLOCATE(xxx1%fff2(11, 11))
  xxx1d%fff3 => passvd
  xxx1%fff3 => passv
  xxx1d%fff4 => passvd
  xxx1%fff4 => passv
  xxx1d%fff1 = 0.0d0
  xxx1d%fff2 = 0.0d0
  xxx1d%fff3 = 0.0d0
  xxx1d%fff4 = 0.0d0
  xxx1%fff1 = 1.1d0
  xxx1%fff2 = 1.2d0
  xxx1%fff3 = 1.3d0
  xxx1%fff4 = 1.4d0
  ALLOCATE(xxx2d%fff1(8, 8))
  ALLOCATE(xxx2%fff1(8, 8))
  ALLOCATE(xxx2d%fff2(8, 8))
  ALLOCATE(xxx2%fff2(8, 8))
  xxx2d%fff3 => passvd
  xxx2%fff3 => passv
  xxx2d%fff4 => passvd
  xxx2%fff4 => passv
  xxx2d%fff1 = 0.0d0
  xxx2d%fff2 = 0.0d0
  xxx2d%fff3 = 0.0d0
  xxx2d%fff4 = 0.0d0
  xxx2%fff1 = 3.1d0
  xxx2%fff2 = 3.2d0
  xxx2%fff3 = 3.3d0
  xxx2%fff4 = 3.4d0

  vvvd = 0.0d0
  vvv = origvvv
  vvv = vvv + 3.d-7
  CALL TOP_D(xxx1, xxx1d, vvv, vvvd)
  keepvvv = vvv

  aaa2 = 0.2d0
  aaa4 = 0.4d0
  xxx1d%fff3 => passvd
  xxx1%fff3 => passv
  xxx1d%fff4 => passvd
  xxx1%fff4 => passv
  xxx1%fff1 = 1.1d0
  xxx1%fff2 = 1.2d0
  xxx1%fff3 = 1.3d0
  xxx1%fff4 = 1.4d0
  xxx2d%fff3 => passvd
  xxx2%fff3 => passv
  xxx2d%fff4 => passvd
  xxx2%fff4 => passv
  xxx2%fff1 = 3.1d0
  xxx2%fff2 = 3.2d0
  xxx2%fff3 = 3.3d0
  xxx2%fff4 = 3.4d0

  vvvd = 1.0d0
  vvv = origvvv
  CALL TOP_D(xxx1, xxx1d, vvv, vvvd)
  print *,' DD:',(keepvvv-vvv)/3.d-7
  print *,'TGT:',vvvd
  print *,'----------'

  DEALLOCATE(xxx1%fff1)
  DEALLOCATE(xxx1%fff2)
  DEALLOCATE(xxx2%fff1)
  DEALLOCATE(xxx2%fff2)
  DEALLOCATE(xxx1d%fff1)
  DEALLOCATE(xxx1d%fff2)
  DEALLOCATE(xxx2d%fff1)
  DEALLOCATE(xxx2d%fff2)
END SUBROUTINE TESTDD
