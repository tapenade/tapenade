! Reproduction du bug trouve par Dominic Jones en Jan 2011.
! Provient d'un code approximatif pour addDiffReInit()
! qui remet a zero TOUS les champs d'un struct differentie
! alors que seuls certains champs sont concernes !
MODULE TEST
  IMPLICIT NONE

  TYPE TTTT
     integer nnn
     double precision, dimension(:, :), allocatable :: fff1
     double precision, dimension(:, :), allocatable :: fff2
     double precision, dimension(:, :), pointer :: fff3
     double precision, dimension(:, :), pointer :: fff4
  END TYPE TTTT

  TYPE(TTTT) :: xxx2

  double precision, dimension(9,9), target :: aaa2,aaa4

CONTAINS

  SUBROUTINE TOP(xxx1,vvv)
    IMPLICIT NONE
    TYPE(TTTT) :: xxx1
    double precision, dimension(7,7) :: bbb1,bbb2
    double precision :: vvv
    double precision :: vvvtest

    vvvtest = vvv
    if (vvvtest.ge.8.0) then
       print*,"SWITCH 1"
       vvvtest = vvvtest-8.0
       xxx1%fff3 => aaa2
       xxx1%fff4 => aaa4
    endif
    aaa2 = aaa2*vvv
    if (vvvtest.ge.4.0) then
       print*,"SWITCH 2"
       vvvtest = vvvtest-4.0
       xxx2%fff3 => aaa2
       xxx2%fff4 => aaa4
    endif
    aaa4 = aaa4*vvv
    xxx1%fff2 = vvv*aaa2(2,2)
    xxx2%fff1 = vvv

    aaa2 = 0.0
    xxx1%fff1 = 0.0
    xxx2%fff2 = 0.0

    if (vvvtest.gt.2.0) then
       print*,"SWITCH 3"
       vvvtest = vvvtest-2.0
       ! before call 1: RESET DIFF x2%2 a2
       !  DON'T RESET DIFF x1%2 x1%3 x2%1 x2%3 x2%4 a1 a4
       call CHANGES(xxx1%fff3,vvv)
       ! after  call 1: RESET DIFF x1%3
    endif

    vvv = vvv+SUM(aaa4)+SUM(xxx2%fff2)+SUM(xxx2%fff4)

    ! Force full activity of subroutine CHANGE
    if (vvvtest.gt.1.0) then
       print*,"SWITCH 4"
       xxx1%fff1 = vvv
       xxx1%fff2 = vvv
       xxx1%fff3 = vvv
       xxx1%fff4 = vvv
       xxx2%fff1 = vvv
       xxx2%fff2 = vvv
       xxx2%fff3 = vvv
       xxx2%fff4 = vvv
       ! before call 2: RESET DIFF a2
       !  DON'T RESET DIFF x1%1 x1%2 x1%3 x1%4 x2%1 x2%2 x2%3 x2%4 a1 a4
       call CHANGES(xxx1%fff1,vvv)
       ! after  call 2: RESET DIFF x1%1
    endif

    vvv = vvv+SUM(aaa2)+SUM(aaa4)
    vvv = vvv+SUM(xxx1%fff1)+SUM(xxx1%fff2)
    vvv = vvv+SUM(xxx2%fff1)+SUM(xxx2%fff2)
  END SUBROUTINE TOP

  SUBROUTINE CHANGES(zzz1,vvv)
    IMPLICIT NONE
    double precision, dimension(:,:) :: zzz1
    double precision :: vvv

    vvv = vvv+zzz1(1,2)
    vvv = vvv+xxx2%fff1(1,2)+xxx2%fff2(1,3)
    vvv = vvv+xxx2%fff3(1,2)+xxx2%fff4(1,3)
    zzz1 = 0.0
    xxx2%fff2 = 0.0
    xxx2%fff4 = 0.0
    vvv = 2.1*vvv
  END SUBROUTINE changes
END MODULE TEST
