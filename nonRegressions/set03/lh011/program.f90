MODULE INTERVAL_ARITHMETICS
       TYPE INTERVAL
              REAL LOWER, UPPER
       END TYPE INTERVAL

       TYPE BOX
          TYPE(INTERVAL) :: INTVX, INTVY
       END TYPE BOX

       TYPE (INTERVAL) :: X,Y

       TYPE(BOX) :: B1

END MODULE INTERVAL_ARITHMETICS

subroutine top(b1,b2,r3)
  use INTERVAL_ARITHMETICS
  type(box) :: b1, b2
  real r3
  r3 = 8.0 * b2%intvx%lower
  b2 = surface(b1)
end subroutine top
  

function surface(b)
  use INTERVAL_ARITHMETICS
  type(box) :: b, surface
  b%intvx%lower = (b%intvx%upper - b%intvx%lower) * (b%intvy%upper - b%intvy%lower)
  b%intvx%upper = (b%intvx%upper - b%intvx%lower) * (b%intvy%upper - b%intvy%lower)
  surface = b
end function surface

