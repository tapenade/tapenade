! It is a Bug
! stackOverFlowError in PointerAnalysis
! Bug: Pointeur bouclant passe en parametre d une fonction

MODULE pointersandrecords

  TYPE ch1
     real*8 f1
     integer f2
     TYPE(ch2), pointer :: inside
  END TYPE ch1

  TYPE ch2
     integer f2
     TYPE(ch1), pointer :: inside
  END TYPE ch2

CONTAINS

  SUBROUTINE top5()
    TYPE(ch1) :: xx1

    call create(xx1)
    DEALLOCATE(xx1%inside)
  END SUBROUTINE top5

  SUBROUTINE create(xx1)
    TYPE(ch1) :: xx1

    ALLOCATE(xx1%inside)
  END SUBROUTINE create

END MODULE pointersandrecords
