REAL(KIND=8) FUNCTION  BADSTOPS(A,B,I)
  INTEGER I
  REAL(KIND=8) :: A(10),B(20), X,Y
  SELECT CASE  (I)
  CASE (1)
     X = 0.d0
  CASE (2)
     X = A(2)*B(3)
  CASE (3)
     IF (A(1).GT.0.0) THEN
        PRINT*,'stop 1'
        STOP
     ELSE
        Y = B(4)*B(5)
        X = Y*A(6)
     ENDIF
  CASE (4)
     BADSTOPS = 1.d0
  CASE (5)
     PRINT*,'stop 2'
     STOP
  END SELECT
  BADSTOPS = X*A(8)
END FUNCTION BADSTOPS
