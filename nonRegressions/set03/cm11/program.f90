module m
type s
   real, pointer :: p
end type s
end module m


subroutine top(r1, r2, r3, result)
  use m
      type(s), target :: s1, s2
      real, target :: r1,r2,r3
      real :: result
      type(s), pointer :: ps

      r1 = 2.0
      r2 = 3.0
      r3 = 4.0
      s1%p => r1
      s2%p => r3
      ps => s1

      do I = 0,N
         ps%p => r2
         ps => s2
      end do
      result = 2*s1%p + s1%p*s2%p
end subroutine top

