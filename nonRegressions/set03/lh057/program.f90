       RECURSIVE PURE FUNCTION RFACTORIAL(N) RESULT (FAC_RESULT)
       IMPLICIT NONE
       double precision, INTENT(IN)      :: N
       double precision               :: FAC_RESULT
       IF ( N <= 1.0 ) THEN
               FAC_RESULT = 1.0
       ELSE
               FAC_RESULT = N * RFACTORIAL(N-1.0)
       END IF
       END FUNCTION RFACTORIAL
