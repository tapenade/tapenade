      program program
      
        real, dimension(:), allocatable :: x
        real, dimension(:), allocatable, save :: y

        allocate(x(5))
        deallocate(x)
        allocate(y(5))
        deallocate(y)

      end program program
