MODULE M
  IMPLICIT NONE
  PUBLIC fofo
  REAL, DIMENSION(10), PRIVATE :: e1e2_i
  logical :: lk_mpp = .true.
  integer, PUBLIC :: nitend

CONTAINS
  SUBROUTINE fofo(zarea, a_fwb, kt)
    IMPLICIT NONE
    real zarea, a_fwb
    INTEGER :: inum=1
    INTEGER :: kt
    INTEGER :: ikty=5

    e1e2_i =  10.
    CALL FUFU(zarea)
    IF (kt .EQ. nitend) THEN
       zarea = zarea**2   
    END IF
    
    IF (kt .EQ. nitend) THEN
       OPEN(inum, file='EMPave.dat') 
       WRITE(inum, '(24X,I8,2ES24.16)')'coucou'
    END IF
    
  END SUBROUTINE fofo

  SUBROUTINE fufu(zz)
    IMPLICIT NONE
    real zz
    integer :: inum = 2

    zz = zz**2
    if (zz.lt.10.0) goto 100
    if (zz.lt.20.0) then
       zz = 2*zz**2
    end if
    if (zz.lt.30.0) then
 100   OPEN(inum, file='truc') 
       WRITE(inum,zz)
    end if
  end SUBROUTINE fufu

END MODULE M
