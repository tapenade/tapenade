!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
!
! problem is: the 2nd arg of next_d must be differentiated !
MODULE M_D
  IMPLICIT NONE
  TYPE TOTO
      TYPE(TOTO), POINTER :: next
      REAL, POINTER :: value
  END TYPE TOTO

CONTAINS
!  Differentiation of next in forward (tangent) mode:
!   Plus diff mem management of: a.next:in-out
  SUBROUTINE NEXT_D(a, ad, b, bd)
    IMPLICIT NONE
    TYPE(TOTO) :: a
    TYPE(TOTO) :: ad
    TYPE(TOTO), TARGET :: b
    TYPE(TOTO), TARGET :: bd
    ad%next => bd
    a%next => b
  END SUBROUTINE NEXT_D

  SUBROUTINE NEXT(a, b)
    IMPLICIT NONE
    TYPE(TOTO) :: a
    TYPE(TOTO), TARGET :: b
    a%next => b
  END SUBROUTINE NEXT

END MODULE M_D

!  Differentiation of top in forward (tangent) mode:
!   variations   of useful results: r
!   with respect to varying inputs: *(object2.value)
!   RW status of diff variables: r:out object2.next:(loc) object2.value:(loc)
!                *(object2.value):in
!   Plus diff mem management of: object2.value:in
SUBROUTINE TOP_D(object2, object2d, r, rd)
  USE M_D
  IMPLICIT NONE
  TYPE(TOTO), POINTER :: nextobject
  TYPE(TOTO), POINTER :: nextobjectd
  REAL, TARGET :: v2, v3
  TYPE(TOTO) :: object1
  TYPE(TOTO) :: object1d
  TYPE(TOTO) :: object2
  TYPE(TOTO) :: object2d
  REAL :: r
  REAL :: rd
  CALL NEXT_D(object1, object1d, object2, object2d)
  nextobjectd => object1d%next
  nextobject => object1%next
  rd = 2*nextobjectd%value
  r = 2*nextobject%value
END SUBROUTINE TOP_D

SUBROUTINE TOP_CD(object2, r)
  USE M_D
  IMPLICIT NONE
  TYPE(TOTO), POINTER :: nextobject
  REAL, TARGET :: v2, v3
  TYPE(TOTO) :: object1
  TYPE(TOTO) :: object2
  REAL :: r
  CALL NEXT(object1, object2)
  nextobject => object1%next
  r = 2*nextobject%value
END SUBROUTINE TOP_CD

