! problem is: the 2nd arg of next_d must be differentiated !
module m
type toto
   type(toto), pointer :: next
   real, pointer :: value
end type toto

contains

subroutine next(a, b)
      type(toto) :: a
      type(toto), target :: b

      a%next => b
    end subroutine next

end module m


subroutine top(object2, r)
  use m
      type(toto), pointer :: nextObject
      real, target :: v2, v3
      type(toto) :: object1
      type(toto) :: object2
      real :: r

      call next(object1, object2)
      nextObject => object1%next
      r = 2 * nextObject%value
    end subroutine top

