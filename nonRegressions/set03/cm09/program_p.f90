!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
! problem is: the 2nd arg of next_d must be differentiated !
MODULE M
  IMPLICIT NONE
  TYPE TOTO
      TYPE(TOTO), POINTER :: next
      REAL, POINTER :: value
  END TYPE TOTO

CONTAINS
  SUBROUTINE NEXT(a, b)
    IMPLICIT NONE
!     a.next points to a or Undef
!     a.value points to *(a.value) or Undef
    TYPE(TOTO) :: a
    TYPE(TOTO), TARGET :: b
    a%next => b
!     a.next points to a or b or Undef
  END SUBROUTINE NEXT

END MODULE M

SUBROUTINE TOP(object2, r)
!     object1.next points to Undef
!     object1.value points to Undef
!     nextobject points to Undef
!     *nextobject.next points to Undef
!     *nextobject.value points to Undef
  USE M
  IMPLICIT NONE
  TYPE(TOTO), POINTER :: nextobject
  REAL, TARGET :: v2, v3
  TYPE(TOTO) :: object1
  TYPE(TOTO) :: object2
  REAL :: r
  CALL NEXT(object1, object2)
!     object1.next points to object2 or Undef
  nextobject => object1%next
!     nextobject points to object2 or Undef
  r = 2*nextobject%value
END SUBROUTINE TOP

