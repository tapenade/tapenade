module MM

  type typea
     real :: y
     integer :: j
  end type typea

  type typeb
     real, dimension(10) :: x
     integer :: i
  end type typeb

  type typetop
     real :: truc
     type (typea) :: chose
     type (typeb), DIMENSION(49,:,:) :: tab
     integer :: iii
  end type typetop

contains

  subroutine top(A,B)
    type(typetop), DIMENSION(5) :: A,B

    A = B
  end subroutine top

end module MM

program main
use mm
implicit none
type (typetop) a,b
b%truc = 1.0
b%chose%y = 2.0
call top(a,b)
write (*,*) a%truc, a%chose%y
end
