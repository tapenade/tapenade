!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 14:31
!
MODULE GLOBALS_DIFF
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:), ALLOCATABLE :: aa, bb, cc
  REAL(kind=8), DIMENSION(:), ALLOCATABLE :: aab, bbb

CONTAINS
!  Differentiation of init_globals as a context to call adjoint code (with options context):
!   Plus diff mem management of: aa:out bb:out
  SUBROUTINE INIT_GLOBALS_B()
    IMPLICIT NONE
    ALLOCATE(aab(100))
    aab = 0.0_8
    ALLOCATE(aa(100))
    ALLOCATE(bbb(100))
    bbb = 0.0_8
    ALLOCATE(bb(100))
    ALLOCATE(cc(100))
  END SUBROUTINE INIT_GLOBALS_B

  SUBROUTINE INIT_GLOBALS()
    IMPLICIT NONE
    ALLOCATE(aa(100))
    ALLOCATE(bb(100))
    ALLOCATE(cc(100))
  END SUBROUTINE INIT_GLOBALS

END MODULE GLOBALS_DIFF

!  Differentiation of pp as a context to call adjoint code (with options context):
PROGRAM PP_B
  USE GLOBALS_DIFF
  USE DIFFSIZES
!  Hint: ISIZE1OFDrfbb should be the size of dimension 1 of array *bb
!  Hint: ISIZE1OFDrfaa should be the size of dimension 1 of array *aa
  IMPLICIT NONE
  REAL :: x
  REAL :: xb
  CALL INIT_GLOBALS_B()
  CALL ADCONTEXTADJ_INIT(0.87_8)
  IF (ALLOCATED(aa)) CALL ADCONTEXTADJ_INITREAL8ARRAY('aa'//CHAR(0), aa&
&                                               , aab, ISIZE1OFDrfaa)
  IF (ALLOCATED(bb)) CALL ADCONTEXTADJ_INITREAL8ARRAY('bb'//CHAR(0), bb&
&                                               , bbb, ISIZE1OFDrfbb)
  CALL ADCONTEXTADJ_INITREAL4('x'//CHAR(0), x, xb)
  CALL COMP_B(x, xb)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  IF (ALLOCATED(aa)) CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('aa'//CHAR(0)&
&                                                   , aa, aab, &
&                                                   ISIZE1OFDrfaa)
  IF (ALLOCATED(bb)) CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('bb'//CHAR(0)&
&                                                   , bb, bbb, &
&                                                   ISIZE1OFDrfbb)
  CALL ADCONTEXTADJ_CONCLUDEREAL4('x'//CHAR(0), x, xb)
  CALL ADCONTEXTADJ_CONCLUDE()
END PROGRAM PP_B

!  Differentiation of comp in reverse (adjoint) mode (with options context):
!   gradient     of useful results: [alloc*bb in init_globals]
!                [alloc*aa in init_globals] *aa *bb x
!   with respect to varying inputs: [alloc*bb in init_globals]
!                [alloc*aa in init_globals] *aa *bb x
!   RW status of diff variables: [alloc*bb in init_globals]:in-out
!                [alloc*aa in init_globals]:in-out *aa:in-out *bb:in-out
!                x:in-out
!   Plus diff mem management of: aa:in-out bb:in-out
SUBROUTINE COMP_B(x, xb)
  USE GLOBALS_DIFF
  USE ISO_C_BINDING
  USE ADMM_TAPENADE_INTERFACE
  USE DIFFSIZES
!  Hint: ISIZE1OFDrfbb should be the size of dimension 1 of array *bb
!  Hint: ISIZE1OFDrfaa should be the size of dimension 1 of array *aa
  IMPLICIT NONE
  REAL :: x
  REAL :: xb
  EXTERNAL PCG
  EXTERNAL PCG_B
  INTEGER*4 :: branch
  CALL PUSHREAL8(bb(2))
  bb(2) = bb(3)*bb(4)
  IF (ALLOCATED(bb)) THEN
    CALL PUSHREAL8ARRAY(bb, ISIZE1OFDrfbb)
    CALL PUSHCONTROL1B(1)
  ELSE
    CALL PUSHCONTROL1B(0)
  END IF
  CALL PUSHPOINTER8(bb)
  CALL PUSHREAL4(x)
  IF (ALLOCATED(aa)) THEN
    CALL PUSHREAL8ARRAY(aa, ISIZE1OFDrfaa)
    CALL PUSHCONTROL1B(1)
  ELSE
    CALL PUSHCONTROL1B(0)
  END IF
  CALL PUSHPOINTER8(aa)
  CALL PCG(aa, bb, x)
  bbb(1) = bbb(1) + aa(2)*aab(1)
  aab(2) = aab(2) + bb(1)*aab(1)
  aab(1) = 0.0_8
  CALL POPPOINTER8(aa)
  CALL POPCONTROL1B(branch)
  IF (branch .EQ. 1) CALL POPREAL8ARRAY(aa, ISIZE1OFDrfaa)
  CALL POPREAL4(x)
  CALL ADSTACK_STARTREPEAT()
  CALL POPPOINTER8(bb)
  CALL POPCONTROL1B(branch)
  IF (branch .EQ. 1) CALL POPREAL8ARRAY(bb, ISIZE1OFDrfbb)
  CALL ADSTACK_RESETREPEAT()
  CALL ADSTACK_ENDREPEAT()
  CALL PCG_B(aa, aab, bb, bbb, x, xb)
  CALL POPPOINTER8(bb)
  CALL POPCONTROL1B(branch)
  IF (branch .EQ. 1) CALL POPREAL8ARRAY(bb, ISIZE1OFDrfbb)
  CALL POPREAL8(bb(2))
  bbb(3) = bbb(3) + bb(4)*bbb(2)
  bbb(4) = bbb(4) + bb(3)*bbb(2)
  bbb(2) = 0.0_8
END SUBROUTINE COMP_B

SUBROUTINE PP_NODIFF()
  USE GLOBALS_DIFF
  IMPLICIT NONE
  REAL :: x
  CALL INIT_GLOBALS()
  CALL COMP_NODIFF(x)
END SUBROUTINE PP_NODIFF

SUBROUTINE COMP_NODIFF(x)
  USE GLOBALS_DIFF
  IMPLICIT NONE
  REAL :: x
  EXTERNAL PCG
  bb(2) = bb(3)*bb(4)
  CALL PCG(aa, bb, x)
  aa(1) = bb(1)*aa(2)
END SUBROUTINE COMP_NODIFF

