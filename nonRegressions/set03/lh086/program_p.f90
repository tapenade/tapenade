!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
MODULE HEAP
  IMPLICIT NONE
  TYPE WK_T
      REAL(KIND(1.d0)), DIMENSION(:), POINTER :: d
  END TYPE WK_T
  TYPE(WK_T), DIMENSION(:), ALLOCATABLE :: wk
  INTERFACE GET
      MODULE PROCEDURE GET_DV
      MODULE PROCEDURE GET_DS
  END INTERFACE


CONTAINS
  FUNCTION GET_DV(d, i) RESULT (p)
    IMPLICIT NONE
    INTRINSIC KIND
!     d points to *d or Undef
    REAL(KIND(1.d0)), DIMENSION(:), POINTER :: d, p
    INTEGER, INTENT(IN) :: i
!   nullify(d)
    p => wk(i)%d
!     p points to *(*wk[_:_].d)
  END FUNCTION GET_DV

  FUNCTION GET_DS(d, i, j) RESULT (p)
    IMPLICIT NONE
    INTRINSIC KIND
!     d points to *d or Undef
    REAL(KIND(1.d0)), POINTER :: d, p
    INTEGER, INTENT(IN) :: i, j
!   nullify(d)
    p => wk(i)%d(j)
!     p points to *(*wk[_:_].d)[_:_]
  END FUNCTION GET_DS

END MODULE HEAP

SUBROUTINE TEST_FUNCTION()
!     objective points to Undef
!     state points to Undef
  USE HEAP
  IMPLICIT NONE
  INTRINSIC KIND
  REAL(KIND(1.d0)), DIMENSION(:), POINTER :: state
  REAL(KIND(1.d0)), POINTER :: objective
  INTRINSIC SUM
  state => GET(state, 1)
!     state points to *(*wk[_:_].d)
  objective => GET(objective, 2, 1)
!     objective points to *(*wk[_:_].d)[_:_]
  objective = SUM(state**2)
END SUBROUTINE TEST_FUNCTION

