module heap

type wk_t
  real(kind(1.d0)),dimension(:),pointer::d
end type
type(wk_t),dimension(:),allocatable::wk

interface get
  module procedure get_dv
  module procedure get_ds
end interface

contains

function get_dv(d,i) result(p)
  real(kind(1.d0)),dimension(:),pointer::d,p
  integer,intent(in)::i
!   nullify(d)
  p=>wk(i)%d
end function

function get_ds(d,i,j) result(p)
  real(kind(1.d0)),pointer::d,p
  integer,intent(in)::i,j
!   nullify(d)
  p=>wk(i)%d(j)
end function

end module


subroutine test_function()
  use heap
  real(kind(1.d0)),dimension(:),pointer::state
  real(kind(1.d0)),pointer::objective

  state=>get(state,1)
  objective=>get(objective,2,1)

  objective=sum(state**2)
end subroutine
