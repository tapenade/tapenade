module test1
  implicit none
  public
  real :: a(10)
end module

module test2
   use test1
end module 

subroutine test3(b)
	use test2, c => a
	real b(10)
	c(1) = b(1)
end subroutine
	
subroutine test4(b)
	use test2, only: a
	real b(10)
	a(1) = b(1)
end subroutine
