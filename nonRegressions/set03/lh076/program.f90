! Source faux a cause de la non-declaration de la
! taille de sa(:) => cause un adjoint logiquement faux
! qui est donc acceptable. Verifier juste qu'on a
! les bons messages d'erreur a la differentiation.
SUBROUTINE myfunc1(sa,ta,ua) 
  dimension ua(:),ta(:),ua(:)
  sa(:) = sa(:) * ta(:)*ua(:)*(ta(:)+ua(:))
  sa(:) = sa(:) * FUNC(ta(:)*ua(:)*(ta(:)+ua(:)))
END SUBROUTINE myfunc1
