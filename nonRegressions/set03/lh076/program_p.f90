!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4912M) - 30 Jul 2013 16:45
!
! Source faux a cause de la non-declaration de la
! taille de sa(:) => cause un adjoint logiquement faux
! qui est donc acceptable. Verifier juste qu'on a
! les bons messages d'erreur a la differentiation.
SUBROUTINE MYFUNC1(sa, ta, ua)
  IMPLICIT NONE
  REAL :: ua
  REAL :: ta
  DIMENSION ua(:), ta(:), ua(:)
  EXTERNAL FUNC
  REAL :: FUNC
  REAL :: sa
  sa(:) = sa(:)*ta(:)*ua(:)*(ta(:)+ua(:))
  sa(:) = sa(:)*FUNC(ta(:)*ua(:)*(ta(:)+ua(:)))
END SUBROUTINE MYFUNC1

