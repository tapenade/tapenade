module mod
  interface fff
     module procedure fff8, fff4
  end interface
  contains 
   function fff8(ncid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid
     integer (kind = 8), dimension(:, :), intent( in) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
    ff8 = 0
  end function fff8


  function fff4(ncid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid
     real (kind = 4), dimension(:),   intent( in) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
    fff4 = 0
  end function fff4
 end module mod

subroutine test(x)
  use mod
    IMPLICIT NONE
    INTEGER :: corner(4), edges(4), iret,  ncid
    REAL, DIMENSION(:), SAVE, ALLOCATABLE :: buff_tmp2_histcom
    REAL :: x
  
   iret = fff(ncid, buff_tmp2_histcom, start=corner(1:4), count=edges(1:4))
   x = iret*x
end subroutine test
