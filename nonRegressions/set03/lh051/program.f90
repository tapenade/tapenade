subroutine TOP(X,Y,Z)
  REAL, TARGET :: X,Y,Z
  REAL, POINTER :: P
  interface
     REAL function SUB(P0,V0)
       REAL, TARGET :: V0
       REAL, POINTER :: P0
     end function SUB
  end interface
  P => X
  Z = sub(P,Y)
  Z = X*Z
end subroutine TOP

function SUB(P0,V0)
  REAL, TARGET :: V, V0
  REAL :: SUB
  REAL, POINTER :: P0, P
  P =>P0
  V = V0
  P = 3.1*P**2
  P =>V
  P = 4.2*P**2
  SUB = V*V
end function SUB
