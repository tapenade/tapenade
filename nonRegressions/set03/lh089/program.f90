program testk
  implicit none
  include 'ampi/ampif.h'
  integer, dimension( MPI_STATUS_SIZE ) :: statut
  integer, parameter :: etiquette=100
  integer :: nb_procs,rang, code
  real :: x,y,z,f
  call AMPI_INIT_NT (code)
  call AMPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
  call AMPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)
  x = 0.0
  y = 4.0
  z = 2.0
  f = 99.0
  call test(x, y, z, f, rang, code, etiquette, statut)
  call AMPI_FINALIZE_NT (code)
end program testk


subroutine test(v1, v2, v3, f, rang, code, etiquette, statut)
  include 'ampi/ampif.h'
  real :: v1,v2,v3,f
  integer :: rang, code, statut, etiquette
  if (rang == 0) then
     if (rang.NE.4) then
        ! this call effectively sends an active value:
        call AMPI_SEND(v1, 1, MPI_REAL, 1, etiquette, AMPI_TO_RECV, MPI_COMM_WORLD, code)
     else
        call AMPI_SEND(v2, 1, MPI_REAL, 1, etiquette, AMPI_TO_RECV, MPI_COMM_WORLD, code)
     endif
  else
     if (rang.NE.1) then
        ! this call effectively receives an active value:
        call AMPI_RECV(v3, 1, MPI_REAL, 0, etiquette, AMPI_FROM_SEND, MPI_COMM_WORLD, statut, code)
     else
        call AMPI_RECV(v2, 1, MPI_REAL, 0, etiquette, AMPI_FROM_SEND, MPI_COMM_WORLD, statut, code)
     endif
  end if
  call AMPI_REDUCE(v3, f, 1, MPI_REAL, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
  v3 = v3+f
end subroutine test
