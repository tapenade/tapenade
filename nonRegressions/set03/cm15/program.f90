! TypeCheck Test

module top
type struct
   real :: x
   real, pointer :: y
   type(struct), pointer :: struct
end type struct

contains
  subroutine topsub(p1, p2, p3)
    real, pointer :: p1, p2, p3
    type(struct) :: t
    type(struct), pointer :: t1
    real, target :: r1
    real, dimension(:), allocatable :: x1

    ! simple pointer expression
    p1 = p2
    p1 = p2 + p3
    p1 = p2 * p3
    p1 = p2 - p3

    ! field access in assign expression
    t%x = p1
    p1 = t%x
    t%y = p1
    p1 = t%y
    t%struct = t1
    t%struct => t1

    ! pointer in field access assign expression
    t1%x = p1
    t1%y = p1

    ! call functions
    call call(p1)
    call call(t1%y)

    ! pointer Assign
    p1 => p2
    p1 => t%y
    p1 => t1%y
    p1 => r1
    p1 => t%x

  end subroutine topsub

  subroutine call(p)
    real, pointer :: p
  end subroutine call

end module top
