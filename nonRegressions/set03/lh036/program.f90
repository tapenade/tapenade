
MODULE MOD_RETARD
CONTAINS

  REAL(KIND=8) FUNCTION RETARD(Z,CK,I_ZONE,SP,TEMPS,IFIM1)
    INTEGER:: I_ZONE,SP
    INTEGER, OPTIONAL ::IFIM1
    REAL(KIND=8):: CK,Z
    REAL(KIND=8), OPTIONAL ::TEMPS

    retard = ck*ck
    retard = retard*retard
    Z = 2*Z
  end FUNCTION RETARD

end MODULE MOD_RETARD

subroutine top(ck,rwk,i,j,i1,j1,z)
  use MOD_RETARD
  INTEGER :: i,j,i1,j1,k
  REAL(KIND=8) :: ck(i,j),z
  DOUBLE PRECISION rwk

  RWK=RETARD(z,CK(i1,j1),3,4)
  RWK=RWK+RETARD(z,35.d0,3,4)

end subroutine top
