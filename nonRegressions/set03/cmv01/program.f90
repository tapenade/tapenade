module m

type toto
   type(toto), pointer :: next
   real, pointer :: value
   integer :: inutile
end type toto

end module m
  subroutine top(v2, v3, r, i)
    use m
    integer i
    type(toto) :: v2,v3
    v2%next = v3%next
    v2%inutile = i
    r = v2%value * v3%next%value
    v2%value = r
    end subroutine top
