subroutine top(A,B)
  real, dimension(10) :: A,B
  type ttt0
     real, DIMENSION(10) :: ff,gg
  end type ttt0
  type(ttt0) :: C
  real, dimension(10) :: D

  call crunch(A,B)
  C%ff = B
  call crunch(A,C%ff)
  C%gg = 0.0
  call crunch(A,C%gg)
  D = 0.0
  call crunch(A,D)
end subroutine top

subroutine crunch(X,Y)
  real, dimension(10) :: X,Y
  X(2) = X(3)*X(2)
  X(4) = X(4) + Y(5)*Y(1)
end subroutine crunch
