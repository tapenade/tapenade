!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 19 Jan 2024 14:16
!
!  Differentiation of eos_insitu_pot in forward (tangent) mode:
!   variations   of useful results: zws
!   with respect to varying inputs: psal
!   RW status of diff variables: psal:in zws:out
SUBROUTINE EOS_INSITU_POT_D(zws, zwsd, psal, psald)
  IMPLICIT NONE
  INTEGER :: jpi
  INTEGER :: jpj
  INTEGER :: jpk
! Bug vu dans ifremer-opa:
!  cree un tableau temporaire abs1 de taille (:) !!!
!  De plus les intents sont faux en mode inverse.
  REAL, DIMENSION(jpi, jpj, jpk), INTENT(IN) :: psal
  REAL, DIMENSION(jpi, jpj, jpk), INTENT(IN) :: psald
  REAL, DIMENSION(jpi, jpj, jpk), INTENT(OUT) :: zws
  REAL, DIMENSION(jpi, jpj, jpk), INTENT(OUT) :: zwsd
  INTRINSIC ABS
  EXTERNAL ABS_D
  INTRINSIC SQRT
  REAL, DIMENSION(jpi, jpj, jpk) :: result1
  REAL, DIMENSION(jpi, jpj, jpk) :: result1d
  REAL, DIMENSION(jpi, jpj, jpk) :: temp
  CALL ABS_D(psal(:, :, :), psald(:, :, :), result1, result1d)
  temp = SQRT(result1)
  WHERE (result1 .EQ. 0.0) 
    zwsd(:, :, :) = 0.0
  ELSEWHERE
    zwsd(:, :, :) = result1d/(2.0*temp)
  END WHERE
  zws(:, :, :) = temp
END SUBROUTINE EOS_INSITU_POT_D

