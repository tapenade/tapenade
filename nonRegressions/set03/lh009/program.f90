SUBROUTINE eos_insitu_pot (zws, psal)
! Bug vu dans ifremer-opa:
!  cree un tableau temporaire abs1 de taille (:) !!!
!  De plus les intents sont faux en mode inverse.
  REAL, DIMENSION(jpi,jpj,jpk), INTENT( in ) :: psal
  REAL, DIMENSION(jpi,jpj,jpk), INTENT( out ) :: zws
  zws(:,:,:) = SQRT( ABS( psal(:,:,:) ) )
END SUBROUTINE eos_insitu_pot
