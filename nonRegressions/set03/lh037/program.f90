subroutine testinits(A,B,C)
REAL A,B,C,X(6),Y(6),J(2,2)

X(1:6) = A
Y(1:6) = B
IF (A>0.0) THEN
   J(1,:) = (/X(2)-X(1),Y(2)-Y(1)/)
   J(2,:) = (/X(3)-X(4),Y(5)-Y(6)/)
   Y(5) = SUM(J)
ENDIF
C = SUM(Y)
end subroutine testinits
