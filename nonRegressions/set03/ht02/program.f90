        MODULE module1
        public toto
	CONTAINS
	SUBROUTINE toto(a, b, c)
	real a, b, c
	c = a+b
	END SUBROUTINE 
        END MODULE

	
	MODULE module2
        PUBLIC tata
	PRIVATE toto
	CONTAINS
	
	SUBROUTINE tata(x, y, z)
	real x, y, z
		call toto(x, y, z) 
	END SUBROUTINE 
	
	SUBROUTINE toto(x, y, z)
	real x, y, z
	z = x*y
	END SUBROUTINE 
	
        END MODULE


	
	
	MODULE module3
        USE module1
        USE module2
        PUBLIC titi
        CONTAINS
        SUBROUTINE titi(a, b, c, x, y, z)
	real a, b, c, x, y, z
               CALL toto(a, b, c)
	       CALL tata(x, y, z)
        END SUBROUTINE
        END MODULE
   