!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 18 Apr 2019 18:10
!
!  Differentiation of testeqtype in forward (tangent) mode:
!   variations   of useful results: x4 x8
!   with respect to varying inputs: x4 x8
!   RW status of diff variables: x4:in-out x8:in-out
SUBROUTINE TESTEQTYPE_D(x4, x4d, x8, x8d)
  IMPLICIT NONE
  INTEGER, PARAMETER :: i8=8, i4=4
  REAL(kind=i8) :: x8
  REAL(kind=i8) :: x8d
  REAL(kind=i4) :: x4
  REAL(kind=i4) :: x4d
  x4d = 2*x4*x4d
  x4 = x4*x4
  x8d = 2*x8*x8d
  x8 = x8*x8
END SUBROUTINE TESTEQTYPE_D

