
REAL FUNCTION SUB1(px0, y0, pz0)
  REAL, POINTER :: px0, pz0
  REAL :: y0
  REAL, POINTER :: px, pz
  REAL :: y
  px => px0
  y = y0
  pz => pz0
  px = 3.7*y ;
  pz = pz + 2*y ;
  SUB1 = y*pz ;
END FUNCTION SUB1


SUBROUTINE TOP(a, b)
  REAL :: a
  REAL, TARGET :: x, y, z
  REAL, POINTER :: px, pz
  REAL, POINTER :: b
  INTERFACE
     REAL FUNCTION SUB1(px, y, pz)
       REAL, POINTER :: px,pz
       REAL :: y
     END FUNCTION SUB1
  END INTERFACE
  IF (a.gt.0.0) THEN
     y = 2.0*a
     z = a-7.0
     px => x
     pz => z
     y = y*SUB1(px,y,pz)
     b = x*y*z
  END IF
  a = -2.9
END SUBROUTINE TOP
