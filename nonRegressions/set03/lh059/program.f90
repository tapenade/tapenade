MODULE MMA
  REAL,POINTER :: vv1,aa1
  REAL,ALLOCATABLE :: vv2,aa2
  REAL :: dvv1,daa1
CONTAINS
  SUBROUTINE INIT()
    vv1=>dvv1
    aa1=>daa1
    ALLOCATE(vv2)
    ALLOCATE(aa2)
  END SUBROUTINE INIT
END MODULE MMA


MODULE MMB
  REAL,POINTER :: vv1,bb1
  REAL,ALLOCATABLE :: vv2,bb2
  REAL :: dvv1,dbb1
CONTAINS
  SUBROUTINE INIT()
    vv1=>dvv1
    bb1=>dbb1
    ALLOCATE(vv2)
    ALLOCATE(bb2)
  END SUBROUTINE INIT
END MODULE MMB


SUBROUTINE GLOBSUBA(xx)
  USE MMA
  REAL xx
  aa2 = aa2*aa2
  vv1 = vv1*xx
  vv2 = 2.0*vv2
  xx = xx*vv1*vv2
END SUBROUTINE GLOBSUBA

SUBROUTINE GLOBSUBB(xx)
  USE MMB
  REAL xx
  bb2 = bb2*bb2
  vv1 = vv1*xx
  vv2 = 2.0*vv2
  xx = xx*vv1*vv2
END SUBROUTINE GLOBSUBB

SUBROUTINE TOP(uu,tt)
  USE MMA, ONLY:mmavv1=>vv1,vv2,aa1,aa2
  USE MMB, ONLY:vv1,mmbvv2=>vv2,bb1,bb2
  REAL uu,tt
  call GLOBSUBA(uu)
  call GLOBSUBB(tt)
  tt = uu*tt
END SUBROUTINE TOP
