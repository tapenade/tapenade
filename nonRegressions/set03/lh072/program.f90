! Cas delicat pour le mode inverse avec
! combinaison de non-active et adjoint-dead

subroutine nonadjdeadtest(x,y,z,t)
  real :: x,y,z
  integer :: k
  t = t*z
  if (x.gt.0.) then
     if (x.gt.1.) then
        if (x.gt.2.) then
           y = log(y)
        else
           y = exp(y)
        endif
        z = z+1
        y = y+1
     else
        y = 3*y+1
     endif
     x=2*x
  else
     y = sin(y)
  endif
  y = 5*y
  x = y*y
  z = exp(z)
end subroutine nonadjdeadtest
