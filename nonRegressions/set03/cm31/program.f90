module m
type toto
   real, pointer :: x
   type(toto), pointer :: next
end type toto
contains 

  subroutine top(a, r, object)
    real :: a,r
    type(toto) :: object
    type(toto), pointer :: nextObject

    allocate(object%x)
    allocate(object%next)
    nextObject => object%next
    allocate(nextObject%x)
    deallocate(object%x)
    deallocate(nextObject%x)
  end subroutine top
end module m
