        MODULE test1
        REAL, PUBLIC, DIMENSION(10, 10) :: sa, ta
        END MODULE

        MODULE test3
        USE test1
        PUBLIC myfunc2
        CONTAINS
        SUBROUTINE myfunc2() 
                sa(:) = 2 * ta(:)
        END SUBROUTINE
        END MODULE

        MODULE test2
        USE test1
        USE test3
        PUBLIC myfunc1
        CONTAINS
        SUBROUTINE myfunc1() 
                sa(:) = 2 * ta(:)
                CALL myfunc2()
        END SUBROUTINE
        END MODULE

        SUBROUTINE diffme()
                USE test2
! $AD NOCHECKPOINT
                CALL myfunc1()
        END SUBROUTINE
