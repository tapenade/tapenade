module top
real qt
contains
       subroutine topsub()
        qt = 2.0 * qt
       end subroutine
end module

subroutine use_of_top()
        use top
        call topsub()
        call topsub()
end subroutine
        
