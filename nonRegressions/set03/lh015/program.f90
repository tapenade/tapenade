! Bug apparu apres modif ReqExplicit svn:r1796
! disparitions des ad_from et ad_to !!!
subroutine bugbounds(rr,nn1,nn2,nn3,nn4)
  integer nn1,nn2,nn3,nn4
  real rr(100)
  integer ii,jj,kk
  do ii = 1,20
     kk = ii+5
     do jj = 2+kk, nn1*3+nn4, 3
        rr(jj) = rr(jj)*rr(ii)
     enddo
     nn2 = ii+2
     nn3 = ii+3
     do jj = nn2-8,nn3-1
        rr(jj+2) = rr(jj+2)*rr(jj)
     enddo
  enddo
end subroutine bugbounds
