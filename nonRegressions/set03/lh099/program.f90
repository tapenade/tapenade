SUBROUTINE XDOT(xv,fv)
! Bug trouve par Dan Moerder, NASA:
!  graphe data-dep insuffisant autour du call qsc_xx_d,
!  qui cause "fvd(5)=" declenche trop tot !
! This version is SCQ3E
  IMPLICIT NONE
  real(8),intent(in) :: xv(20)
  real(8),intent(inout) :: fv(20)
  real(8) :: qsc
  integer k

  xv(k) = 2*fv(k)
  fv(10)=fv(10)+1
  fv(2)=fv(3)+2
  call qsc_xx(xv(12),qsc)
  fv(5)=2*(qsc-xv(12))

CONTAINS

  SUBROUTINE QSC_XX(x, qs)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x(3)
    REAL*8, INTENT(OUT) :: qs
    qs = x(2)*x(3)
  END SUBROUTINE QSC_XX

END SUBROUTINE XDOT
