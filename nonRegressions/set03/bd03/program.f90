        MODULE test
        REAL, PUBLIC :: coin
        PUBLIC test1
        CONTAINS
! $AD NOCHECKPOINT
        SUBROUTINE test1()
                coin = 2 * coin
                CALL test2()
        END SUBROUTINE test1

        SUBROUTINE test2()
                coin = 2 * coin
        END SUBROUTINE test2

        END MODULE test
