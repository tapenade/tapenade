

module m
  contains
      subroutine foo(p5, p6, p7, r)
      real, pointer :: p5, p6, p7
      real, target :: v8
      real :: r
       
      p5 => v8
      p7 => p6
    end subroutine foo


subroutine program(v4, r)
      real, pointer :: p1, p2, p3
      real, target :: v4
      p2 => v4
      call foo(p1, p2, p3, p2+4.0)
      r = 2 * p3 + p1
end subroutine program

end module m

