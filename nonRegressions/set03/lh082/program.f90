subroutine testwhere(XX,YY,MM)
  REAL*8, DIMENSION(20) :: XX,YY,MM
  where (MM.GT.10.0)
     XX(:) = SUM(YY(:),mask=(MM(:).LE.5.0))
  end where
  YY(:) = YY(:)*XX(:)
end subroutine testwhere
