!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 19 Jan 2024 17:05
!
!  Differentiation of testwhere in reverse (adjoint) mode:
!   gradient     of useful results: xx yy
!   with respect to varying inputs: xx yy
!   RW status of diff variables: xx:in-out yy:in-out
SUBROUTINE TESTWHERE_B(xx, xxb, yy, yyb, mm)
  IMPLICIT NONE
  REAL*8, DIMENSION(20) :: xx, yy, mm
  REAL*8, DIMENSION(20) :: xxb, yyb
  INTRINSIC SUM
  LOGICAL, DIMENSION(20) :: mask
  LOGICAL, DIMENSION(20) :: arg1
  mask = mm .GT. 10.0
  arg1 = mm(:) .LE. 5.0
  WHERE (mask) xx(:) = SUM(yy(:), mask=arg1)
  xxb = xxb + yy*yyb
  yyb = xx*yyb
  WHERE (arg1) yyb = yyb + SUM(xxb, MASK=mask)
  WHERE (mask) xxb = 0.0_8
END SUBROUTINE TESTWHERE_B

