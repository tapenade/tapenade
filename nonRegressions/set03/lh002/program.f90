module top
  use mid
contains
  subroutine topsub(a,b)
    real a,b
    call midsub(a,b)
  end subroutine topsub
end module top

module mid
  use bot
contains
  subroutine midsub(a,b)
    real a,b
    call botsub(a,b)
  end subroutine midsub
end module mid

module bot
  use varx
contains
  subroutine botsub(a,b)
    real a,b
    a = a*b
    call writeinx(a)
    b = 10.5
    call readfromx(b)
    call readfromx(a)
  end subroutine botsub
end module bot

module varx
  real, private :: x
  real ,public :: y
contains
  subroutine writeinx(v)
    real v
    x = v
  end subroutine writeinx
  subroutine readfromx(v)
    real v
    v = x
  end subroutine readfromx
end module varx
