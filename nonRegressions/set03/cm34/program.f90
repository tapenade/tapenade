module m
type toto
   real, dimension(:), allocatable :: value
   type(toto), pointer :: next
end type toto

contains

subroutine allocateToto(toto1)
      type(toto) :: toto1

      allocate(toto1%value(1))
end subroutine allocateToto

end module m


program program
  use m
      type(toto) :: toto1
      type(toto) :: toto2
      type(toto), pointer :: toto3

      call allocateToto(toto1)
      allocate(toto2%value(1))
      allocate(toto1%next)
      allocate(toto3)
      allocate(toto3%value(1))

end program program

