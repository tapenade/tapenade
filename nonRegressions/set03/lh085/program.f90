subroutine test(x,y)
   implicit none
   real x(4)
   real y(4)
   y = x + 1;

assign_1:   where (x > 1)
               y = x + 2;     
               y = x + 3;
            elsewhere
assign_2:      where (x > 4)
                  y = x + 5;
               elsewhere
                  y = x + 6;
               end where assign_2
            end where assign_1
end subroutine test
