!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 18 Apr 2019 18:10
!
!  Differentiation of badstop2 in forward (tangent) mode:
!   variations   of useful results: a
!   with respect to varying inputs: a
!   RW status of diff variables: a:in-out
SUBROUTINE BADSTOP2_D(a, ad)
  IMPLICIT NONE
  REAL :: a(10)
  REAL :: ad(10)
  INTEGER :: i, j, k
  IF (a(1) .GT. 1.0) THEN
    STOP
  ELSE
    DO i=1,3
      DO j=1,4
        ad(j) = a(j)*ad(i) + a(i)*ad(j)
        a(j) = a(i)*a(j)
      END DO
      IF (a(2) .LE. 0.0) THEN
        GOTO 100
      ELSE
        ad(4) = a(6)*ad(5) + a(5)*ad(6)
        a(4) = a(5)*a(6)
      END IF
    END DO
    ad(7) = a(9)*ad(8) + a(8)*ad(9)
    a(7) = a(8)*a(9)
    GOTO 110
 100 DO k=1,5
      PRINT*, ' A ', a(k)
    END DO
    STOP
  END IF
 110 CONTINUE
END SUBROUTINE BADSTOP2_D

