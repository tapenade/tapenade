module m
type toto
   real, pointer :: x
   type(toto), pointer :: next
end type toto
contains 

  subroutine top(toto1, p)
    type(toto) :: toto1
    type(toto), pointer :: p2
    real, pointer :: p
    real, target :: a
    a = 1.0
    p => a
    toto1%x => a
    p2 => toto1%next
    p2%x => a
  end subroutine top
end module m
