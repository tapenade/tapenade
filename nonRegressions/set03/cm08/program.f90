module m

type toto
   type(toto), pointer :: next
   real, pointer :: value
end type toto

end module m


program program
  use m
      real, pointer :: p1
      real, target :: v2, v3
      type(toto) :: object1
      type(toto), target :: object2
      type(toto), target :: object3
      type(toto), target :: object4

      object1%next => object2
      object1%next%value => v2
      object1%next%next => object3
      object1%next%next%next => object4
      object1%next%next%next%value => v2
      object4%next => object1%next%next%next
      v2 = object1%value
      object2%value = v3
      p1 => v2
      object1%value => p1

end program program

