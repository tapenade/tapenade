subroutine testpushregions(gcr,res,d1,d2,T,TX)
! To test the indices used for push and pop of array regions
INTEGER :: d1,d2,T(d2)
REAL :: res,TX(100)
REAL, DIMENSION(d1,d2) :: gcr
integer i,j
!
res = PROC(TX((27*i+j/i)*2*i*j+(j+2)/(i*3)), gcr(2:d1,T(2:d2)), T)
d2=d2+1
res = res*res*d2
gcr(5,5) = gcr(3,3)*gcr(2,2)
gcr(5,5) = gcr(1,1)*gcr(5,5)
end
