MODULE PAR_KIND
  IMPLICIT NONE
  INTEGER, PUBLIC, PARAMETER ::        &  !: Floating point section
       sp = SELECTED_REAL_KIND( 6, 37),  &  !: single precision (real 4)
       dp = SELECTED_REAL_KIND(12,307),  &  !: double precision (real 8)
       wp = 8!dp                              !: working precision
END MODULE PAR_KIND

MODULE M
  USE PAR_KIND
  IMPLICIT NONE
CONTAINS
  SUBROUTINE toto(a, b, c)
    IMPLICIT NONE
    REAL(wp), dimension(10) :: a
    REAL(wp) :: b

    a = a*a
    a = a*a

    b = b*b
    b = b*b
  END SUBROUTINE toto
END MODULE M

