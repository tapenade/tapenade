! Complicated bug of name conflicts:
! Differentiated variable diff(xxx)
! of mod1.xxx cannot be named mod1.xxxd,
! not because any use of diff(xxx)
! conflicts with a visible xxxd,
! but because some place (i.e. aux()) sees
! both mod1_d and a module (i.e. mod2) that
! defines another xxxd

module mod1
  real xxx(10)
contains
  subroutine subr1
    xxx(:) = 2*xxx(:)
  end subroutine subr1
end module mod1

module mod2
  real xxxd(10)
end module mod2

subroutine top(u,v)
  use mod1
  use mod2
  real u,v
  call subr1()
  call aux(u,v)
end subroutine top

subroutine aux(rv1,rv2)
  use mod1
  use mod2
  real rv1,rv2,loc
  rv1 = rv1*rv2
  loc = xxxd(3) + 1.0
end subroutine aux
