!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 28 May 2019 17:51
!
!  Differentiation of toto in forward (tangent) mode:
!   variations   of useful results: z
!   with respect to varying inputs: x
!   RW status of diff variables: x:in z:out
SUBROUTINE TOTO_D(x, xd, y, z, zd)
  IMPLICIT NONE
  REAL :: x, y, z
  REAL :: xd, zd
  INTRINSIC SIGN
  zd = SIGN(1.d0, x*y)*xd
  z = SIGN(x, y)
END SUBROUTINE TOTO_D

