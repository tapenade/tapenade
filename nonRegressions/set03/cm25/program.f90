module m
type toto
   real, pointer :: x
end type toto
contains 

  subroutine top(a, r, object)
    real :: a,r
    type(toto) :: object

    call allocateX(object%x)
    object%x = a
    r = object%x * a
    call deallocateX(object%x)
  end subroutine top

  subroutine allocateX(x)
    real, pointer :: x

    allocate(x)
  end subroutine allocateX
  subroutine deallocateX(x)
    real, pointer :: x

    deallocate(x)
  end subroutine deallocateX
end module m
