! Pour tester RefDescriptor et IterDescriptor,
! par exemple pour les reinitialisations.
! => verifier reinitialisations d'un type derive d'un
! type structure (comme type(MIXT) :: structarg)
subroutine test1(A,B,C,x,y,structarg)
  real, dimension(30) :: A,B,C
  real x,y,z
  integer i,j,k
  TYPE mixt
     real :: ff1
     integer :: ff2
  end TYPE mixt
  type(mixt), dimension(2) :: structarg

  x = A(1)*B(2)
  CALL GGG(x,y)
  READ*, (B(i), i=2,19,5), structarg
  B(4) = B(4)*x
  x = 2.3
  y = 3.4
end
