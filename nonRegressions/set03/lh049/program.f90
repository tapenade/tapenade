module glob
  real(KIND=8), DIMENSION(:), ALLOCATABLE :: gg1,gg2,gg3,gg4
contains
  subroutine initall
    allocate(gg1(100))
    allocate(gg2(100))
    allocate(gg3(100))
    allocate(gg4(100))
  end subroutine initall
end module glob

subroutine insid(pp1,pp2,pp3,pp4)
  use glob
  real*8 pp1,pp2,pp3,pp4
  pp1 = gg1(11)*gg1(12)
  gg2(20) = gg2(20)*pp2
  gg3(30) = pp3*pp3
  pp3 = pp3*gg3(31)
  pp4 = 2.0*pp4 + 1.0
end subroutine insid

subroutine top(tt1,tt2,tt3,tt4)
  real*8 tt1,tt2,tt3,tt4
  call insid(tt1,tt2,tt3,tt4)
end subroutine top
