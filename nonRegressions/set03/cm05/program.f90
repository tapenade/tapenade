

module m
  contains
      function foo(p5, p6)
        real, pointer :: foo
      real, pointer :: p5, p6

      deallocate(p6)
      allocate(p5)
      p5 = 4
      foo => p5
      end function foo
end module m


program program
  use m
      real, pointer :: p1, p2, p3
      real, target :: v3, v4

      v3 = 1
      p1 => v3
      v3 = p1
      p3 => v4
      p1 => foo(p2, p3) 
end program program

