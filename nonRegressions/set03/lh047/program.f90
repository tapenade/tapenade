

module m
contains
  function foo(p5, p6, p7, r)
    real, pointer :: foo
    real, pointer :: p5, p6, p7
    real, target  :: v8
    real, pointer :: r

    p5 => v8
    p7 => p6
    foo => r
  end subroutine foo

  subroutine program(v4, r)
    real, pointer :: p1, p2, p3, p4
    real, target :: v4
    p2 => v4
    p4 => foo(p1, p2, p3, p2)
    r = 2 * p3 + p1 + p4
  end subroutine program

end module m

