subroutine icethick(ddx,ddd,adv,testv)
REAL(KIND=wp),dimension(nx) :: ddx,ddd,adv,testv

where (testv.gt.0.)
   ddx = ddx+ddd
   where (testv.gt.5)
      ddd = 2*ddd
   elsewhere
      ddd = 3*ddd
   end where
elsewhere
   ddx = ddx-ddd
end where

ddx(2) = ddx(2)*ddd(3)

end subroutine icethick
