      subroutine sub1(t1, t2, t3, x,a,d)
      double precision x, d
      double precision, dimension (4) :: a, t1, t2, t3
t3(:) = sum(a(:)) * d
a(:) = t1(:) * d + sum(t2(:)) * d
x = 3 * d
       return
      end
