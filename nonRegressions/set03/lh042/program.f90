module allocs
  REAL(KIND=8), DIMENSION(:,:),ALLOCATABLE :: WKARR
contains
  subroutine mkalloc(n2)
    integer n2
    ALLOCATE(WKARR(33,n2))
    WKARR(:,:) = 2.0
  end subroutine mkalloc
end module allocs

module work
  use allocs
contains
  subroutine main()
    call mkalloc(44)
    call foo(2.5_8,5.2_8)
  end subroutine main

  subroutine foo(X,Y)
    REAL(KIND=8) :: X,Y
    wkarr(3,7) = X*X
    Y = wkarr(3,7)*X
  end subroutine foo
end module work
