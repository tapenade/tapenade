!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 14:31
!
MODULE ALLOCS_DIFF
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(:, :), ALLOCATABLE :: wkarr
  REAL(kind=8), DIMENSION(:, :), ALLOCATABLE :: wkarrd

CONTAINS
!  Differentiation of mkalloc as a context to call tangent code (with options context):
!   Plus diff mem management of: wkarr:out
  SUBROUTINE MKALLOC_D(n2)
    IMPLICIT NONE
    INTEGER :: n2
    ALLOCATE(wkarrd(33, n2))
    wkarrd = 0.0_8
    ALLOCATE(wkarr(33, n2))
    wkarr(:, :) = 2.0
  END SUBROUTINE MKALLOC_D

  SUBROUTINE MKALLOC(n2)
    IMPLICIT NONE
    INTEGER :: n2
    ALLOCATE(wkarr(33, n2))
    wkarr(:, :) = 2.0
  END SUBROUTINE MKALLOC

END MODULE ALLOCS_DIFF

MODULE WORK_DIFF
  USE ALLOCS_DIFF
  IMPLICIT NONE

CONTAINS
!  Differentiation of main as a context to call tangent code (with options context):
  SUBROUTINE MAIN_D()
    USE DIFFSIZES
!  Hint: ISIZE2OFDrfwkarr should be the size of dimension 2 of array *wkarr
!  Hint: ISIZE1OFDrfwkarr should be the size of dimension 1 of array *wkarr
    IMPLICIT NONE
    CALL MKALLOC_D(44)
    CALL ADCONTEXTTGT_INIT(1.e-8_8, 0.87_8)
    IF (ALLOCATED(wkarr)) CALL ADCONTEXTTGT_INITREAL8ARRAY('wkarr'//CHAR&
&                                                    (0), wkarr, wkarrd&
&                                                    , ISIZE1OFDrfwkarr*&
&                                                    ISIZE2OFDrfwkarr)
    CALL FOO_D(2.5_8, 0.0_8, 5.2_8, 0.0_8)
    CALL ADCONTEXTTGT_STARTCONCLUDE()
    IF (ALLOCATED(wkarr)) CALL ADCONTEXTTGT_CONCLUDEREAL8ARRAY('wkarr'//&
&                                                        CHAR(0), wkarr&
&                                                        , wkarrd, &
&                                                       ISIZE1OFDrfwkarr&
&                                                        *&
&                                                       ISIZE2OFDrfwkarr&
&                                                       )
    CALL ADCONTEXTTGT_CONCLUDE()
  END SUBROUTINE MAIN_D

  SUBROUTINE MAIN()
    IMPLICIT NONE
    CALL MKALLOC(44)
    CALL FOO(2.5_8, 5.2_8)
  END SUBROUTINE MAIN

!  Differentiation of foo in forward (tangent) mode (with options context):
!   variations   of useful results: [alloc*wkarr in mkalloc] *wkarr
!                y
!   with respect to varying inputs: [alloc*wkarr in mkalloc] *wkarr
!                x
!   RW status of diff variables: [alloc*wkarr in mkalloc]:in-out
!                *wkarr:in-out x:in y:out
!   Plus diff mem management of: wkarr:in
  SUBROUTINE FOO_D(x, xd, y, yd)
    IMPLICIT NONE
    REAL(kind=8) :: x, y
    REAL(kind=8) :: xd, yd
    wkarrd(3, 7) = 2*x*xd
    wkarr(3, 7) = x*x
    yd = x*wkarrd(3, 7) + wkarr(3, 7)*xd
    y = wkarr(3, 7)*x
  END SUBROUTINE FOO_D

  SUBROUTINE FOO(x, y)
    IMPLICIT NONE
    REAL(kind=8) :: x, y
    wkarr(3, 7) = x*x
    y = wkarr(3, 7)*x
  END SUBROUTINE FOO

END MODULE WORK_DIFF

