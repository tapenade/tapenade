PROGRAM MAIN
  integer :: i
  do i=0,15
     call TESTDD( i+0.5d0)
     call TESTREV(i+0.5d0)
     print *,'----------'
  enddo
END PROGRAM MAIN

SUBROUTINE TESTDD(origvvv)
  USE TEST_D
  IMPLICIT NONE
  TYPE(TTTT) :: xxx1
  TYPE(TTTT_D) :: xxx1d
  DOUBLE PRECISION ::  origvvv, vvv, keepvvv
  DOUBLE PRECISION :: vvvd, keepvvvd
  double precision, dimension(9,9), target :: nnn1, nnn2, nnn3, nnn4
  double precision, dimension(9,9), target :: nnn1d, nnn2d, nnn3d, nnn4d
  nnn1d = 0.0d0
  nnn2d = 0.0d0
  nnn3d = 0.0d0
  nnn4d = 0.0d0
  nnn1 = 0.1d0
  nnn2 = 0.2d0
  nnn3 = 0.3d0
  nnn4 = 0.4d0
  aaa1d = 0.0d0
  aaa2d = 0.0d0
  aaa3d = 0.0d0
  aaa4d = 0.0d0
  aaa1 = 0.1d0
  aaa2 = 0.2d0
  aaa3 = 0.3d0
  aaa4 = 0.4d0
  ALLOCATE(xxx1d%fff1(11, 11))
  ALLOCATE(xxx1%fff1(11, 11))
  ALLOCATE(xxx1d%fff2(11, 11))
  ALLOCATE(xxx1%fff2(11, 11))
  xxx1d%fff3 => nnn1d
  xxx1%fff3 => nnn1
  xxx1d%fff4 => nnn2d
  xxx1%fff4 => nnn2
  xxx1d%fff1 = 0.0d0
  xxx1d%fff2 = 0.0d0
  xxx1d%fff3 = 0.0d0
  xxx1d%fff4 = 0.0d0
  xxx1%fff1 = 1.1d0
  xxx1%fff2 = 1.2d0
  xxx1%fff3 = 1.3d0
  xxx1%fff4 = 1.4d0
  ALLOCATE(xxx2d%fff1(8, 8))
  ALLOCATE(xxx2%fff1(8, 8))
  ALLOCATE(xxx2d%fff2(8, 8))
  ALLOCATE(xxx2%fff2(8, 8))
  xxx2d%fff3 => nnn3d
  xxx2%fff3 => nnn3
  xxx2d%fff4 => nnn4d
  xxx2%fff4 => nnn4
  xxx2d%fff1 = 0.0d0
  xxx2d%fff2 = 0.0d0
  xxx2d%fff3 = 0.0d0
  xxx2d%fff4 = 0.0d0
  xxx2%fff1 = 3.1d0
  xxx2%fff2 = 3.2d0
  xxx2%fff3 = 3.3d0
  xxx2%fff4 = 3.4d0

  vvvd = 0.0d0
  vvv = origvvv
  vvv = vvv + 3.d-7
  CALL TOP_D(xxx1, xxx1d, vvv, vvvd)
  keepvvv = vvv

  aaa1 = 0.1d0
  aaa2 = 0.2d0
  aaa3 = 0.3d0
  aaa4 = 0.4d0
  xxx1d%fff3 => nnn1d
  xxx1%fff3 => nnn1
  xxx1d%fff4 => nnn2d
  xxx1%fff4 => nnn2
  xxx1%fff1 = 1.1d0
  xxx1%fff2 = 1.2d0
  xxx1%fff3 = 1.3d0
  xxx1%fff4 = 1.4d0
  xxx2d%fff3 => nnn3d
  xxx2%fff3 => nnn3
  xxx2d%fff4 => nnn4d
  xxx2%fff4 => nnn4
  xxx2%fff1 = 3.1d0
  xxx2%fff2 = 3.2d0
  xxx2%fff3 = 3.3d0
  xxx2%fff4 = 3.4d0

  vvvd = 1.0d0
  vvv = origvvv
  CALL TOP_D(xxx1, xxx1d, vvv, vvvd)
  print *,' DD:',(keepvvv-vvv)/3.d-7
  print *,'TGT:',vvvd

  DEALLOCATE(xxx1%fff1)
  DEALLOCATE(xxx1%fff2)
  DEALLOCATE(xxx2%fff1)
  DEALLOCATE(xxx2%fff2)
  DEALLOCATE(xxx1d%fff1)
  DEALLOCATE(xxx1d%fff2)
  DEALLOCATE(xxx2d%fff1)
  DEALLOCATE(xxx2d%fff2)
END SUBROUTINE TESTDD

SUBROUTINE TESTREV(origvvv)
  USE TEST_B
  IMPLICIT NONE
  TYPE(TTTT) :: xxx1
  TYPE(TTTT_B) :: xxx1b
  DOUBLE PRECISION ::  origvvv, vvv
  DOUBLE PRECISION :: vvvb
  double precision, dimension(9,9), target :: nnn1, nnn2, nnn3, nnn4
  double precision, dimension(9,9), target :: nnn1b, nnn2b, nnn3b, nnn4b
  nnn1b = 0.0d0
  nnn2b = 0.0d0
  nnn3b = 0.0d0
  nnn4b = 0.0d0
  nnn1 = 0.1d0
  nnn2 = 0.2d0
  nnn3 = 0.3d0
  nnn4 = 0.4d0
  aaa1b = 0.0d0
  aaa2b = 0.0d0
  aaa3b = 0.0d0
  aaa4b = 0.0d0
  aaa1 = 0.1d0
  aaa2 = 0.2d0
  aaa3 = 0.3d0
  aaa4 = 0.4d0
  ALLOCATE(xxx1b%fff1(11, 11))
  ALLOCATE(xxx1%fff1(11, 11))
  ALLOCATE(xxx1b%fff2(11, 11))
  ALLOCATE(xxx1%fff2(11, 11))
  xxx1b%fff3 => nnn1b
  xxx1%fff3 => nnn1
  xxx1b%fff4 => nnn2b
  xxx1%fff4 => nnn2
  xxx1b%fff1 = 0.0d0
  xxx1b%fff2 = 0.0d0
  xxx1b%fff3 = 0.0d0
  xxx1b%fff4 = 0.0d0
  xxx1%fff1 = 1.1d0
  xxx1%fff2 = 1.2d0
  xxx1%fff3 = 1.3d0
  xxx1%fff4 = 1.4d0
  ALLOCATE(xxx2b%fff1(8, 8))
  ALLOCATE(xxx2%fff1(8, 8))
  ALLOCATE(xxx2b%fff2(8, 8))
  ALLOCATE(xxx2%fff2(8, 8))
  xxx2b%fff3 => nnn3b
  xxx2%fff3 => nnn3
  xxx2b%fff4 => nnn4b
  xxx2%fff4 => nnn4
  xxx2b%fff1 = 0.0d0
  xxx2b%fff2 = 0.0d0
  xxx2b%fff3 = 0.0d0
  xxx2b%fff4 = 0.0d0
  xxx2%fff1 = 3.1d0
  xxx2%fff2 = 3.2d0
  xxx2%fff3 = 3.3d0
  xxx2%fff4 = 3.4d0

  vvvb = 1.0d0
  vvv = origvvv
  CALL TOP_B(xxx1, xxx1b, vvv, vvvb)
  print *,'REV:',vvvb

  DEALLOCATE(xxx1%fff1)
  DEALLOCATE(xxx1%fff2)
  DEALLOCATE(xxx2%fff1)
  DEALLOCATE(xxx2%fff2)
  DEALLOCATE(xxx1b%fff1)
  DEALLOCATE(xxx1b%fff2)
  DEALLOCATE(xxx2b%fff1)
  DEALLOCATE(xxx2b%fff2)
END SUBROUTINE TESTREV
