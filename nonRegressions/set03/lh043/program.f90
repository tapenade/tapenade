module allocs
  REAL(KIND=8), DIMENSION(:),ALLOCATABLE :: stockm
contains
  subroutine mkalloc()
    ALLOCATE(stockm(33))
    stockm(:) = 2.0
  end subroutine mkalloc
end module allocs

module modutil
contains
  
  subroutine sub1(X, Y, stockm2)
    use allocs
    REAL(KIND=8) :: X,Y
    REAL(KIND=8), DIMENSION(:) :: stockm2
    stockm(:) = X*X
    Y = Y*X
  end subroutine sub1

  subroutine sub2(X, Y, stockm)
    REAL(KIND=8) :: X,Y
    REAL(KIND=8), DIMENSION(:) :: stockm
    Y = Y + stockm(2)*stockm(3)*X
  end subroutine sub2

end module modutil

subroutine foo(X,Y)
  use allocs
  use modutil
  REAL(KIND=8) :: X,Y
  call mkalloc()
  Y = X*X
  call sub1(X,Y,stockm)
  call sub2(X,Y,stockm)
end subroutine foo
