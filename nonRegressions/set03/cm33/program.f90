module m
type toto
   real, allocatable :: x
   type(toto), pointer :: next
end type toto
contains 
  subroutine allocateFunc(Object1)
    type(toto) :: object1

    allocate(object1%x)
  end subroutine allocateFunc
  subroutine deallocateFunc(Object2)
    type(toto) :: object2

    deallocate(object2%x)
  end subroutine deallocateFunc



  subroutine top(a, r, object3)
    real :: a,r
    type(toto) :: object3
    type(toto), pointer :: nextObject

    call allocateFunc(object3)
    allocate(object3%next)
    nextObject => object3%next
    call deallocateFunc(object3)
  end subroutine top
end module m
