module m
  contains
      subroutine allocateX(x)
      real, allocatable :: x
      allocate(x)
      x = 2.0
      end

      subroutine deallocateX(x)
      real, allocatable :: x
      deallocate(x)
      end
end module m


subroutine top(a1, a2, r)
  use m
      real, allocatable :: a1, a2
      real :: r

      call allocateX(a1)
      call allocateX(a2)
      r = a1 * a2
      call deallocateX(a1)
      call deallocateX(a2)
end subroutine top

