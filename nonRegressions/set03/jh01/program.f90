module somemod
contains
subroutine A(w,x,y,z)
  implicit none
  real(kind=8) :: x, z
  double precision :: w(10), y(10)
  !------------

  ! y,z are output, w,x are inputs only
  w(1:3) = z*y(4)*x*w(5)
  if(y .le. z) then
    y = sin(w(3)*x) ! overwrite, depending on both inputs
  else
    z = w(3)**5     ! overwrite, depending on one input
  end if
  call C(w(3),x,y(4),z)
end subroutine

subroutine B(w,x,y,z)
  implicit none
  real(kind=8) :: w, x, y, z
  !------------

  ! w is inout, z,y are input, x is output
  w = cos(w*z) ! overwrite, depending on an input and itself
  x = 2*y ! overwrite, depending on one input
  call C(w, x, y, z)
end subroutine

subroutine C(w,x,y,z)
  implicit none
  real(kind=8) :: w, x, y, z
  !------------
  
  ! w,y: inout
  ! x: out
  ! z: in
  w = sin(z)*w ! w is inout, but influences only itself, and is influenced by z
  x = y ! y is used as input here, then overwritten
  y = E(z*w)
  z = 0.3*z ! z influences itself only
  call D(w,x,y,z,1)
end subroutine

recursive subroutine D(w,x,y,z,i)
  implicit none
  real(kind=8) :: w, x, y, z
  integer :: i
  !------------
  
  ! x is inout, only dependent on itself.
  ! z is inout and depends on w
  if(i .eq. 0) then
    x = x*2
    z = sin(z)*w
  else 
    call D(w,x,y,z,0)
  end if
end subroutine

function E(x)
  implicit none
  real(kind=8) :: E, x
  E = x*x+x
end function

end module
program main
  use somemod
  implicit none
  real(kind=8) :: x, z
  double precision :: w(10), y(10)
  !------------

  w = 1
  x = 2
  y = 3
  z = 4
  
  call A(w,x,y,z)
  call B(w,x,y,z)

  write(*,*) w,x,y,z
end program
