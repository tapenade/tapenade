module m
type toto
   type(tata), pointer :: tata
end type toto

type tata
   REAL, pointer :: value 
   REAL :: r
end type tata

contains

subroutine allocateTata(object)
      type(toto) :: object

      allocate(object%tata)

end subroutine allocateTata

subroutine deallocateTata(object)
      type(toto) :: object

      deallocate(object%tata)

end subroutine deallocateTata

end module m


program program
  use m
      type(toto) :: object1

      call allocateTata(object1)    
      call deallocateTata(object1)    
end program program

