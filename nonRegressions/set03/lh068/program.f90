! Bug found by D.Jones: initializations of fields of
! a differentiated record variable when there is no
! such field in the differentiated record type.
module mod1
  TYPE GRID_T
     real :: a,b,c,f
  END TYPE GRID_T
  
contains

  subroutine top(v)
    real v
    TYPE(GRID_T) :: rec
    rec%a = v
    rec%b = 3.1*v
    rec%c = 5*v*v
    call test(rec, v)
    v = v*v
  end subroutine top
  
  subroutine test(X,Y)
    TYPE(GRID_T) :: X
    real Y
    y = x%a + x%b
    x%a = 1.0
    x%b = 1.0
    x%c = 1.0
  end subroutine test

end module mod1
