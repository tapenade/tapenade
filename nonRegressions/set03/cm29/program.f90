module m
type toto
   real, dimension(:), allocatable :: value
end type toto

contains

subroutine allocateToto(object)
      type(toto) :: object

      allocate(object%value)
    end subroutine allocateToto
subroutine deallocateToto(object)
      type(toto) :: object

      deallocate(object%value)
    end subroutine deallocateToto
subroutine program(object)
      type(toto) :: object

      call allocateToto(object)
      call deallocateToto(object)
    end subroutine program
  end module m

