!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
MODULE M
  IMPLICIT NONE
  TYPE TOTO
      REAL, DIMENSION(:), ALLOCATABLE :: value
  END TYPE TOTO

CONTAINS
  SUBROUTINE ALLOCATETOTO(object)
    IMPLICIT NONE
    TYPE(TOTO) :: object
    ALLOCATE(object%value)
!     object.value points to [alloc*(object.value) in allocatetoto]
  END SUBROUTINE ALLOCATETOTO

  SUBROUTINE DEALLOCATETOTO(object)
    IMPLICIT NONE
!     object.value points to [alloc*(object.value) in allocatetoto] or *(object.value)
    TYPE(TOTO) :: object
    DEALLOCATE(object%value)
!     object.value points to Undef
  END SUBROUTINE DEALLOCATETOTO

  SUBROUTINE PROGRAM(object)
    IMPLICIT NONE
    TYPE(TOTO) :: object
    CALL ALLOCATETOTO(object)
!     object.value points to [alloc*(object.value) in allocatetoto]
    CALL DEALLOCATETOTO(object)
!     object.value points to Undef
  END SUBROUTINE PROGRAM

END MODULE M

