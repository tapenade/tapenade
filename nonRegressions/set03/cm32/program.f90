module m
type toto
   real, allocatable :: x
   type(toto), pointer :: next
end type toto
contains 

  subroutine top(a, r, object)
    real :: a,r
    type(toto) :: object
    type(toto), pointer :: nextObject

    allocate(object%x)
    deallocate(object%x)
  end subroutine top
end module m
