!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 18 Apr 2019 18:10
!
!  Differentiation of inlinewhere in forward (tangent) mode:
!   variations   of useful results: a b
!   with respect to varying inputs: a b
!   RW status of diff variables: a:in-out b:in-out
SUBROUTINE INLINEWHERE_D(a, ad, b, bd)
  IMPLICIT NONE
! => Intermediate arrays abs1 and abs1d had size (:) !
! => There was a buggy "GOTO 100" added in the reverse diff
! CAN STILL BE IMPROVED (2 pushes of A is too much!
!   and B should be detected not useful upon entry)
  REAL, DIMENSION(100) :: a, b
  REAL, DIMENSION(100) :: ad, bd
  INTRINSIC ABS
  REAL, DIMENSION(100) :: abs0
  REAL, DIMENSION(100) :: abs0d
  ad(5) = b(10)*ad(10) + a(10)*bd(10)
  a(5) = a(10)*b(10)
  WHERE (b .GE. 0.0) 
    ad = bd
    a = b
  ELSEWHERE
    ad = -bd
    a = -b
  END WHERE
  bd(2) = b(20)*ad(15) + a(15)*bd(20)
  b(2) = a(15)*b(20)
  abs0d = 0.0
  WHERE (b .GE. 0.0) 
    abs0d = bd
    abs0 = b
  ELSEWHERE
    abs0d = -bd
    abs0 = -b
  END WHERE
  bd = abs0*ad(1) + a(1)*abs0d
  b = a(1)*abs0
END SUBROUTINE INLINEWHERE_D

