subroutine inlinewhere(A,B)
! => Intermediate arrays abs1 and abs1d had size (:) !
! => There was a buggy "GOTO 100" added in the reverse diff
! CAN STILL BE IMPROVED (2 pushes of A is too much!
!   and B should be detected not useful upon entry)
real, dimension (100) :: A,B
A(5) = A(10)*B(10)
A = ABS(B)
B(2) = A(15)*B(20)
B = A(1)*ABS(B)
end subroutine

