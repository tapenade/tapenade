MODULE MOD_UNSATURATIONLAWS
  IMPLICIT NONE

CONTAINS

  REAL(KIND=8) FUNCTION DPSIDH(PK,I_ZONE)
    IMPLICIT NONE
    REAL(KIND=8):: PK
    INTEGER :: I_ZONE
    DPSIDH = PK*PK
    RETURN
  END FUNCTION DPSIDH

  REAL(KIND=8) FUNCTION STORAGE(PK,I_ZONE,TEMPS,IFIM1)
    IMPLICIT NONE
    INTEGER:: I_ZONE
    INTEGER, OPTIONAL ::IFIM1
    REAL(KIND=8):: PK, DPSI
    REAL(KIND=8), OPTIONAL ::TEMPS

    TEMPS = TEMPS*2.0
    PK = 0.0
    DPSI=DPSIDH(PK,I_ZONE)
    STORAGE = 15.0
    RETURN
  END FUNCTION STORAGE

END MODULE MOD_UNSATURATIONLAWS


program test
  use mod_unsaturationlaws
  REAL(KIND=8) :: pk, pkd, temps, tempsd, stg
  INTEGER :: i_zone, ifim1
  i_zone = 1
  ifim1 = 1
  pk = 2.0
  pkd = 1.0
  temps = 3.5
  tempsd = 1.0
  stg = storage(pk, i_zone, temps, ifim1)
end program test
