!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
!
MODULE M_D
  IMPLICIT NONE
  TYPE TOTO
      TYPE(TOTO), DIMENSION(10), POINTER :: next
      REAL, POINTER :: value
      INTEGER :: inutile
  END TYPE TOTO
  TYPE TOTO_D
      TYPE(TOTO_D), DIMENSION(10), POINTER :: next
      REAL, POINTER :: value
  END TYPE TOTO_D
END MODULE M_D

!  Differentiation of top in forward (tangent) mode:
!   variations   of useful results: *(v2.value) r
!   with respect to varying inputs: *(v2.value) *(v3.value)
!   RW status of diff variables: v2.next:(loc) v2.value:(loc) *(v2.value):in-out
!                v3.next:(loc) v3.value:(loc) *(v3.value):in r:out
!   Plus diff mem management of: v2.value:in v3.next:in v3.value:in
SUBROUTINE TOP_D(v2, v2d, v3, v3d, r, rd, i)
  USE M_D
  IMPLICIT NONE
  INTEGER :: i
  TYPE(TOTO) :: v2, v3
  TYPE(TOTO_D) :: v2d, v3d
  REAL :: r
  REAL :: rd
  v2%inutile = i
  rd = v3%next(1)%value*v2d%value + v2%value*v3d%next(1)%value
  r = v2%value*v3%next(1)%value
  v2d%value = rd
  v2%value = r
END SUBROUTINE TOP_D

SUBROUTINE TOP_CD(v2, v3, r, i)
  USE M_D
  IMPLICIT NONE
  INTEGER :: i
  TYPE(TOTO) :: v2, v3
  REAL :: r
  v2%inutile = i
  r = v2%value*v3%next(1)%value
  v2%value = r
END SUBROUTINE TOP_CD

