MODULE PUSHPOPPIECES
  TYPE TT4
     REAL, POINTER :: vv1,vv2
     REAL :: vv3,vv4
  END TYPE TT4

  TYPE TTP
     REAL :: xx1
     REAL, POINTER :: ppr
     TYPE(TT4) :: xxt
     TYPE(TT4), POINTER :: ppt
  END TYPE TTP

CONTAINS

  SUBROUTINE TOP(arg1,arg2,arg3)
    TYPE(TT4) :: arg1
    TYPE(TTP),POINTER :: arg2
    REAL :: arg3, v4

    arg1%vv4 = arg1%vv2 * arg1%vv4
    v4 = SUB(arg1, arg2, arg3)
    arg3 = v4*arg3
  END SUBROUTINE TOP

  REAL FUNCTION SUB(a1,a2,a3)
    TYPE(TT4) :: a1
    TYPE(TTP),POINTER :: a2
    REAL, TARGET :: a3

    a1%vv4 = a1%vv1*a1%vv4
    a2%ppt%vv1 = 1.3 * a2%ppt%vv1
    a1%vv2 = a1%vv4*a1%vv4 + a2%ppt%vv1*a2%ppt%vv4
    a2%ppt%vv2 = 0.0
    a2%ppt%vv1 => a3
    sub = a1%vv2*a3
  END FUNCTION SUB
END MODULE PUSHPOPPIECES
