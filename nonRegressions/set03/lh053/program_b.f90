!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
!
MODULE PUSHPOPPIECES_DIFF
  IMPLICIT NONE
  TYPE TT4
      REAL, POINTER :: vv1, vv2
      REAL :: vv3, vv4
  END TYPE TT4
  TYPE TT4_DIFF
      REAL, POINTER :: vv1
      REAL, POINTER :: vv2
      REAL :: vv4
  END TYPE TT4_DIFF
  TYPE TTP
      REAL :: xx1
      REAL, POINTER :: ppr
      TYPE(TT4) :: xxt
      TYPE(TT4), POINTER :: ppt
  END TYPE TTP
  TYPE TTP_DIFF
      TYPE(TT4_DIFF), POINTER :: ppt
  END TYPE TTP_DIFF

CONTAINS
!  Differentiation of top in reverse (adjoint) mode:
!   gradient     of useful results: *(arg1.vv1) *(arg1.vv2) arg1.vv4
!                *(*(*arg2.ppt).vv1) *(*arg2.ppt).vv4 arg3
!   with respect to varying inputs: *(arg1.vv1) *(arg1.vv2) arg1.vv4
!                *(*(*arg2.ppt).vv1) *(*(*arg2.ppt).vv2) *(*arg2.ppt).vv4
!                arg3
!   RW status of diff variables: arg1.vv1:(loc) *(arg1.vv1):incr
!                arg1.vv2:(loc) *(arg1.vv2):in-out arg1.vv3:(loc)
!                arg1.vv4:in-out arg2:(loc) *arg2.ppt:(loc) *(*arg2.ppt).vv1:(loc)
!                *(*(*arg2.ppt).vv1):in-out *(*arg2.ppt).vv2:(loc)
!                *(*(*arg2.ppt).vv2):zero *(*arg2.ppt).vv4:incr
!                arg3:in-out
!   Plus diff mem management of: arg1.vv1:in arg1.vv2:in arg2:in
!                *arg2.ppt:in *(*arg2.ppt).vv1:in-out
  SUBROUTINE TOP_B(arg1, arg1b, arg2, arg2b, arg3, arg3b)
    USE ISO_C_BINDING
    USE ADMM_TAPENADE_INTERFACE
    IMPLICIT NONE
    TYPE(TT4) :: arg1
    TYPE(TT4_DIFF) :: arg1b
    TYPE(TTP), POINTER :: arg2
    TYPE(TTP_DIFF), POINTER :: arg2b
    REAL :: arg3, v4
    REAL :: arg3b, v4b
    TYPE(C_PTR) :: cptr
    CALL PUSHREAL4(arg1%vv4)
    arg1%vv4 = arg1%vv2*arg1%vv4
    CALL PUSHREAL4(arg2%ppt%vv1)
    CALL PUSHPOINTER8(C_LOC(arg2%ppt%vv1))
    CALL PUSHREAL4(arg1%vv2)
    CALL PUSHREAL4(arg1%vv4)
    v4 = SUB(arg1, arg2, arg3)
    v4b = arg3*arg3b
    arg3b = v4*arg3b
    CALL POPREAL4(arg1%vv4)
    CALL POPREAL4(arg1%vv2)
    CALL POPPOINTER8(cptr)
    CALL C_F_POINTER(cptr, arg2%ppt%vv1)
    CALL POPREAL4(arg2%ppt%vv1)
    CALL SUB_B(arg1, arg1b, arg2, arg2b, arg3, arg3b, v4b)
    CALL POPREAL4(arg1%vv4)
    arg1b%vv2 = arg1%vv4*arg1b%vv4
    arg1b%vv4 = arg1%vv2*arg1b%vv4
    arg2b%ppt%vv2 = 0.0
  END SUBROUTINE TOP_B

  SUBROUTINE TOP(arg1, arg2, arg3)
    IMPLICIT NONE
    TYPE(TT4) :: arg1
    TYPE(TTP), POINTER :: arg2
    REAL :: arg3, v4
    arg1%vv4 = arg1%vv2*arg1%vv4
    v4 = SUB(arg1, arg2, arg3)
    arg3 = v4*arg3
  END SUBROUTINE TOP

!  Differentiation of sub in reverse (adjoint) mode:
!   gradient     of useful results: sub *(a1.vv1) *(a1.vv2) a1.vv4
!                *(*(*a2.ppt).vv1) *(*a2.ppt).vv4 a3
!   with respect to varying inputs: *(a1.vv1) a1.vv4 *(*(*a2.ppt).vv1)
!                *(*a2.ppt).vv4 a3
!   Plus diff mem management of: a1.vv1:in a1.vv2:in a2:in *a2.ppt:in
!                *(*a2.ppt).vv1:in-out
  SUBROUTINE SUB_B(a1, a1b, a2, a2b, a3, a3b, subb)
    IMPLICIT NONE
    TYPE(TT4) :: a1
    TYPE(TT4_DIFF) :: a1b
    TYPE(TTP), POINTER :: a2
    TYPE(TTP_DIFF), POINTER :: a2b
    REAL, TARGET :: a3
    REAL, TARGET :: a3b
    REAL :: subb
    REAL :: sub
    CALL PUSHREAL4(a1%vv4)
    a1%vv4 = a1%vv1*a1%vv4
    a2%ppt%vv1 = 1.3*a2%ppt%vv1
    CALL PUSHREAL4(a1%vv2)
    a1%vv2 = a1%vv4*a1%vv4 + a2%ppt%vv1*a2%ppt%vv4
    a1b%vv2 = a1b%vv2 + a3*subb
    a3b = a3b + a1%vv2*subb
    CALL POPREAL4(a1%vv2)
    a1b%vv4 = a1b%vv4 + 2*a1%vv4*a1b%vv2
    a2b%ppt%vv1 = a2b%ppt%vv1 + a2%ppt%vv4*a1b%vv2
    a2b%ppt%vv4 = a2b%ppt%vv4 + a2%ppt%vv1*a1b%vv2
    a2b%ppt%vv1 = 1.3*a2b%ppt%vv1
    CALL POPREAL4(a1%vv4)
    a1b%vv1 = a1b%vv1 + a1%vv4*a1b%vv4
    a1b%vv4 = a1%vv1*a1b%vv4
  END SUBROUTINE SUB_B

  REAL FUNCTION SUB(a1, a2, a3)
    IMPLICIT NONE
    TYPE(TT4) :: a1
    TYPE(TTP), POINTER :: a2
    REAL, TARGET :: a3
    a1%vv4 = a1%vv1*a1%vv4
    a2%ppt%vv1 = 1.3*a2%ppt%vv1
    a1%vv2 = a1%vv4*a1%vv4 + a2%ppt%vv1*a2%ppt%vv4
    a2%ppt%vv2 = 0.0
    a2%ppt%vv1 => a3
    sub = a1%vv2*a3
  END FUNCTION SUB

END MODULE PUSHPOPPIECES_DIFF

