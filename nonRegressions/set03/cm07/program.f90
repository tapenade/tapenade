module m

type toto
   type(toto), pointer :: next
   real, pointer :: value
end type toto

end module m
  subroutine top(v2, v3, r)
    use m

      real, pointer :: p1
      type(toto), pointer :: next
      real, target :: v2, v3
      real :: r
      type(toto) :: object1
      type(toto), target :: object2
      type(toto) :: object3

      object1%next => object2
      object1%next = object3
      next => object1%next
      next%value => v3
      next%value = v3

      v2 = object1%value
      object2%value = v2

      p1 => v2
      object1%value => p1
      r= next%value * 2 + v2
    end subroutine top
