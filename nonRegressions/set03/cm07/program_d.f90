!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (develop) -  8 Nov 2019 16:26
!
MODULE M_D
  IMPLICIT NONE
  TYPE TOTO
      TYPE(TOTO), POINTER :: next
      REAL, POINTER :: value
  END TYPE TOTO
END MODULE M_D

!  Differentiation of top in forward (tangent) mode:
!   variations   of useful results: v2 v3 r
!   with respect to varying inputs: v3
!   RW status of diff variables: v2:zero v3:in-out r:out
SUBROUTINE TOP_D(v2, v2d, v3, v3d, r, rd)
  USE M_D
  IMPLICIT NONE
  REAL, POINTER :: p1
  TYPE(TOTO), POINTER :: next
  TYPE(TOTO), POINTER :: nextd
  REAL, TARGET :: v2, v3
  REAL, TARGET :: v2d, v3d
  REAL :: r
  REAL :: rd
  TYPE(TOTO) :: object1
  TYPE(TOTO) :: object1d
  TYPE(TOTO), TARGET :: object2
  TYPE(TOTO), TARGET :: object2d
  TYPE(TOTO) :: object3
  TYPE(TOTO) :: object3d
  object1d%next => object2d
  object1%next => object2
  object1d%next = object3d
  object1%next = object3
  nextd => object1d%next
  next => object1%next
  nextd%value => v3d
  next%value => v3
  nextd%value = v3d
  next%value = v3
  v2 = object1%value
  object2d%value = 0.0
  object2%value = v2
  p1 => v2
  object1%value => p1
  rd = 2*nextd%value
  r = next%value*2 + v2
  v2d = 0.0
END SUBROUTINE TOP_D

SUBROUTINE TOP_CD(v2, v3, r)
  USE M_D
  IMPLICIT NONE
  REAL, POINTER :: p1
  TYPE(TOTO), POINTER :: next
  REAL, TARGET :: v2, v3
  REAL :: r
  TYPE(TOTO) :: object1
  TYPE(TOTO), TARGET :: object2
  TYPE(TOTO) :: object3
  object1%next => object2
  object1%next = object3
  next => object1%next
  next%value => v3
  next%value = v3
  v2 = object1%value
  object2%value = v2
  p1 => v2
  object1%value => p1
  r = next%value*2 + v2
END SUBROUTINE TOP_CD

