module m

type toto

   real, pointer :: value

end type toto

end module m


program program
  use m
      real, pointer :: p1
      real, target :: v2, v3
      type(toto) :: object
      type(toto) :: object2

      v2 = 1.0
      p1 => v2
      object%value => p1
      object2%value => v3
      object%value => object2%value
      object2 = object

end program program

