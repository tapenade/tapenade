!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_cuda) - 16 Nov 2022 19:31
!
MODULE COIN_B
  IMPLICIT NONE

CONTAINS
!PUBLIC head
!  Differentiation of coine in reverse (adjoint) mode, forward sweep:
!   gradient     of useful results: a b
!   with respect to varying inputs: a b
  SUBROUTINE COINE_FWD(a, b)
    IMPLICIT NONE
    REAL :: a
    REAL :: b
    CALL HEAD_FWD(a, b)
  END SUBROUTINE COINE_FWD

!  Differentiation of coine in reverse (adjoint) mode, backward sweep:
!   gradient     of useful results: a b
!   with respect to varying inputs: a b
!   RW status of diff variables: a:in-zero b:incr
  SUBROUTINE COINE_BWD(a, ab, b, bb)
    IMPLICIT NONE
    REAL :: a
    REAL :: ab
    REAL :: b
    REAL :: bb
    CALL HEAD_BWD(a, ab, b, bb)
    ab = 0.0
  END SUBROUTINE COINE_BWD

  SUBROUTINE COINE(a, b)
    IMPLICIT NONE
    REAL :: a
    REAL :: b
    CALL HEAD(a, b)
  END SUBROUTINE COINE

END MODULE COIN_B

!  Differentiation of head in reverse (adjoint) mode, forward sweep:
!   gradient     of useful results: a b
!   with respect to varying inputs: b
SUBROUTINE HEAD_FWD(a, b)
  IMPLICIT NONE
  REAL :: a, b
END SUBROUTINE HEAD_FWD

!  Differentiation of head in reverse (adjoint) mode, backward sweep:
!   gradient     of useful results: a b
!   with respect to varying inputs: b
SUBROUTINE HEAD_BWD(a, ab, b, bb)
  IMPLICIT NONE
  REAL :: a, b
  REAL :: ab, bb
  bb = bb + ab
END SUBROUTINE HEAD_BWD

