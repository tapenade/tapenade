!Bug found by Mikko Auvinen: useless PUSH/POP of loop indices before they are initialized.

module VARIABLES
  INTEGER :: i,j,idf,id1,id2
end module VARIABLES

SUBROUTINE SCALAR_NORMAL_FLUX(q, qb, nxy, resq)
  USE VARIABLES
  IMPLICIT NONE
  REAL(kind=rp), DIMENSION(99), INTENT(IN) :: q
  REAL(kind=rp), DIMENSION(99), INTENT(IN) :: qb
  REAL(kind=rp), DIMENSION(99), INTENT(INOUT) :: resq
  REAL(kind=rp), DIMENSION(99), INTENT(IN) :: nxy
  REAL(kind=rp) :: qf, flux
  REAL(kind=rp) :: fw1,fw2
  REAL(kind=rp), DIMENSION(99) :: w1,sf
  INTEGER, DIMENSION(999) :: iface_id,facecell_id,bdryface_id

! This is a loop over all the internal faces
!$AD II-LOOP
  DO i=1,51
    idf = iface_id(i)
    j = 2*idf
    id1 = facecell_id(j-1)
    id2 = facecell_id(j)

    fw1 = w1(idf)
    fw2 = 1.0 - fw1

    qf = fw1*q(id1) + fw2*q(id2)
    flux = qf*nxy(idf)*sf(idf)
    resq(id1) = resq(id1) - flux
    resq(id2) = resq(id2) + flux
  END DO


! This is a loop over all boundary faces
!$AD II-LOOP
  DO i=1,52
    idf = bdryface_id(i)
    id1 = facecell_id(2*idf-1)
    flux = qb(i)*nxy(idf)*sf(idf)
    resq(id1) = resq(id1) - flux
  END DO

END SUBROUTINE SCALAR_NORMAL_FLUX
