module m

type toto
   type(toto) :: next
   real :: value
end type toto

end module m
  subroutine top(v2, v3, r,next)
    use m

      type(toto) :: next
      real :: v2, v3
      real :: r


      next%value = v3
      r = next%value * v2

    end subroutine top
