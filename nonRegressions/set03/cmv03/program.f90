module m

type tree
   type(listtree), pointer :: sons
   integer :: operator
   real :: tpos
end type tree

type listtree
   type(tree), pointer :: tree
   type(listtree), pointer :: next
   real :: lpos
end type listtree

end module m

  subroutine top(t1, t2, pos)
    use m
    real pos
    type(tree) :: t1, t2
    pos = t1%tpos * t2%sons%lpos * pos
    t1%tpos = t1%tpos * pos
    t1%sons%tree = t2%sons%tree
  end subroutine top
