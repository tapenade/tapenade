!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) -  1 Aug 2023 09:28
!
!  Differentiation of sub1 in reverse (adjoint) mode:
!   gradient     of useful results: a b c
!   with respect to varying inputs: a b c
!   RW status of diff variables: a:in-out b:in-zero c:incr
SUBROUTINE SUB1_B(a, ab, b, bb, c, cb)
  IMPLICIT NONE
! Un test de plus pour des cas de SUM
  DOUBLE PRECISION, DIMENSION(10) :: a, b, c
  DOUBLE PRECISION, DIMENSION(10) :: ab, bb, cb
  INTRINSIC SUM
  DOUBLE PRECISION :: tempb
  cb = cb + bb + SUM(bb)
  tempb = SUM(ab)
  ab = (SUM(a+2.0)+SUM(a))*tempb
  bb = 0.D0
END SUBROUTINE SUB1_B

