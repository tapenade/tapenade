  SUBROUTINE FLOTTAB_1D(s,h,b,h_rho,h_buoy,bsoc,flotte,rho_i,sealevel,rho_sea)
    IMPLICIT NONE
    REAL*8 :: h_rho(99),rho_i,bsoc(99),sealevel,rho_sea,b(99),h_buoy(99),h(99),s(99)
    INTEGER :: flotte(99)

    h_rho(:) = h(:)*rho_i
    WHERE (bsoc(:) .LE. sealevel - h_rho(:)/rho_sea) 
      b(:) = sealevel - h_rho(:)/rho_sea
      flotte(:) = 1
    ELSEWHERE
      b(:) = bsoc(:)
      flotte(:) = 0
      h_buoy(:) = (sealevel-bsoc(:))*rho_sea/rho_i
    END WHERE
    h_buoy(:) = (sealevel-bsoc(:))*rho_sea/rho_i
    h_buoy(:) = h(:) - h_buoy(:)
    s(:) = h(:) + b(:)
  END SUBROUTINE FLOTTAB_1D
