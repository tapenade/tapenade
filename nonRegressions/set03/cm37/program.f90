subroutine top(a, b, r, p)
  real :: a, b, r
  real, pointer :: p

  r = 3.5
  a = 1.0
  if(a >= 2.0) then
     p => a
  else
     p => b
  endif
  a = 2.0
  r = 2 * p + a
end subroutine top
