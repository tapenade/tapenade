!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 2.2.5 (r2426M) - 06/12/2008 11:23
!  
SUBROUTINE TOP(a, b, r, p)
  IMPLICIT NONE
  REAL :: a, b, r
  REAL, POINTER :: p
  r = 3.5
  a = 1.0
  IF (a .GE. 2.0) THEN
    p => a
!     p points to a
  ELSE
    p => b
!     p points to b
  END IF
!     p points to a or b
  a = 2.0
  r = 2*p + a
END SUBROUTINE TOP

