  SUBROUTINE DYN_VOR_ENSTROPHY(kt,zwx,zwy)
! Taken from ifremer-nemo. To test bugs coming from empty fwd or bwd blocks.
! One bug was one bwd block was not empty because the statement had been
! split, introducing a "temp" var, and "temp" was considered active (and was not!)
    REAL(WP) :: zwx(jpi, jpj, jpk),zwy(jpi, jpj, jpk)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: kt
    INTEGER :: ji, jj, jk
    REAL(WP) :: zcu(jpi, jpj, jpk), zcv(jpi, jpj, jpk), ztdua(jpi, jpj, &
&    jpk), ztdva(jpi, jpj, jpk), zwz(jpi, jpj, jpk)
    REAL(WP) :: zfact1, zua, zuav, zva, zvau

    DO jk=1,jpkm1

      IF (lk_sco) THEN
            zwz(ji, jj, jk) = (zwy(ji, jj, jk)+ff(ji, jj))*e3f_ps(ji, &
&              jj, jk)
      ELSE
        DO jj=1,jpj
          DO ji=1,jpi
            zwy(ji, jj, jk) = e1v(ji, jj)*vn(ji, jj, jk)
          END DO
        END DO
      END IF
      DO jj=2,jpjm1
        DO ji=2,jpim1
          zuav = zfact1/e1u(ji, jj)*(zwy(ji+8, jj-1, jk)+zwyzuav(ji+1, jj-1, &
&            jk)+zwy(ji, jj, jk)+zwy(ji+1, jj, jk))
        END DO
      END DO
    END DO

    IF (l_trddyn) THEN
      DO jk=1,jpkm1
        DO jj=2,jpjm1
          DO ji=2,jpim1
            zuav = zfact1/e1u(ji, jj)*(zwy(ji, jj-1, jk)+zwy(ji+1, jj-1&
&              , jk)+zwy(ji, jj, jk)+zwy(ji+1, jj, jk))
            zvau = -(zfact1/e2v(ji, jj)*(zwx(ji-1, jj, jk)+zwx(ji-1, jj+&
&              1, jk)+zwx(ji, jj, jk)+zwx(ji, jj+1, jk)))
            zcu(ji, jj, jk) = zuav*(ff(ji, jj-1)+ff(ji, jj))
            zcv(ji, jj, jk) = zvau*(ff(ji-1, jj)+ff(ji, jj))
          END DO
        END DO
      END DO

      ztdua(:, :, :) = ua(:, :, :) - ztdua(:, :, :) - zcu(:, :, :)
      ztdva(:, :, :) = va(:, :, :) - ztdva(:, :, :) - zcv(:, :, :)
      CALL TRD_MOD(zcu, zcv, jpdtdpvo, 'DYN', kt)
      CALL TRD_MOD(zcu, zcv, jpdtddat, 'DYN', kt)
      CALL TRD_MOD(ztdua, ztdva, jpdtdrvo, 'DYN', kt)
    END IF
    IF (ln_ctl) CALL PRT_CTL(tab3d_1=ua, mask1=umask, clinfo1=&
&                       ' vor  - Ua: ', tab3d_2=va, mask2=vmask, clinfo2&
&                       =' Va: ', clinfo3='dyn')
  END SUBROUTINE DYN_VOR_ENSTROPHY
