! tapenade -O /tmp -o p -vars v2 -outvars f program.f90
! ou bien
! tapenade -O /tmp -o p -vars "v2" -outvars "v3" pr2.f90

program testk
  implicit none
  include 'ampi/ampif.h'
  integer, dimension( MPI_STATUS_SIZE ) :: statut
  integer, parameter :: etiquette=100
  integer :: nb_procs,rang, code
  real :: x,y,z,b,f
  call AMPI_INIT_NT (code)
  call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
  call AMPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)
  x = 0.0
  y = 4.0
  z = 2.0
  b = 7.0
  f = 99.0
  call test(x, y, z, b, f, rang, code, etiquette, statut)
  call AMPI_FINALIZE_NT (code)
end program testk

subroutine test(v1,v2,v3,v4, f,rang, code, etiquette, statut)
  include 'ampi/ampif.h'
  real :: v1,v2,v3,v4,f
  integer :: rang, code, statut, etiquette
  if (rang == 0) then
     if (rang.NE.4) then
        ! this call effectively sends an active value:
        call f1(v1,v2, etiquette, code)
     else
        call f1(v2,v1, etiquette, code)
     endif
  else
     if (rang.NE.1) then
        ! this call effectively receives an active value:
        call f2(v3, etiquette, statut,code)
     else
        call f2(v4, etiquette, statut,code)
     endif
  end if
  call MPI_REDUCE(v3,f,1,MPI_REAL,MPI_SUM,0,MPI_COMM_WORLD,ierr)
  v3 = v3+f
end subroutine test

subroutine f1(a,b, etiquette, code)
  include 'ampi/ampif.h'
  real x,a,b
  integer :: code, etiquette
  x = 7.0
  a = x * b
  call AMPI_SEND(a,1, MPI_REAL, 1, etiquette, &
&                 AMPI_TO_RECV, MPI_COMM_WORLD,code)
end subroutine f1

subroutine f2(c,etiquette, statut,code)
  include 'ampi/ampif.h'
  integer :: code, statut, etiquette
  real :: c
  c = 0.0
  call AMPI_RECV(c,1, MPI_REAL ,0,etiquette, &
&                 AMPI_FROM_SEND,MPI_COMM_WORLD,statut,code)
end subroutine f2
