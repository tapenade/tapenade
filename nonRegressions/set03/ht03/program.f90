!!! a compiler avec l'option -r8

MODULE MOD0
  IMPLICIT NONE
  INTEGER, PUBLIC, PARAMETER :: dp=SELECTED_REAL_KIND(12, 307)
  INTEGER, PUBLIC, PARAMETER :: wp=dp
END MODULE MOD0


module mod1
use mod0
interface toto
 module procedure sub_toto
end interface toto
PUBLIC toto
PRIVATE sub_toto
contains
subroutine sub_toto (a)
	real a
a = a * a
end subroutine sub_toto
end module mod1



module mod2
PUBLIC titi
CONTAINS
SUBROUTINE titi(a)
use mod1
 real(wp) a
 CALL toto(a)
END SUBROUTINE
END MODULE mod2


