module m4
  public
  integer :: n4
end module m4

module flxmod
  IMPLICIT NONE
  private
  public flx
  integer :: ndayflx

CONTAINS
  
  subroutine flx(a)
    use m4
    real a
    a = ndayflx*a
    ndayflx = ndayflx+1
  end subroutine flx
end module flxmod

module step
  use flxmod
  public stp
  
CONTAINS

  subroutine stp(b)
    real b
    b = b*b
    call flx(b)
    b = b*b
  end subroutine stp
end module step
