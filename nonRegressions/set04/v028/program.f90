module m1
  real, save :: x
  real y,x2
  dimension y(2)
  equivalence (x2, y(1))
  real, private, save :: z
  real, private :: t
  real c1,c2
  save c1,x2
  common /cc/c1,c2
  contains 
    subroutine h(t)
      x2 = x2 + t
    end subroutine
end module m1

module m2
  use m1
  use m3
  contains
    real function fm2(t)
      real t
      fm2 = t*t + x
      x = fm2
      return
    end function
end module m2

subroutine f(u,v)
  use m1
  real u,v
  real, save :: w
  v = y(1)
  call h(u)
  x = x + u
end subroutine 

subroutine g(u,v)
real u,v
real :: w = 5.0
real :: x = 10.0
v = 3.0
call f(u,v)
call f(u,v)
end subroutine

