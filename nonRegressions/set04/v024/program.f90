  FUNCTION LENSTR(label)
    IMPLICIT NONE
      integer, parameter ::  int_kind  = kind(1)
    CHARACTER*(*) :: label
    CHARACTER (KIND=1, LEN=1), PARAMETER :: C_NULL_CHAR = ACHAR(0)
    INTEGER :: lenstr
    INTEGER(KIND=INT_KIND) :: length, n
    INTRINSIC LEN
    length = LEN(label)
  END FUNCTION LENSTR
