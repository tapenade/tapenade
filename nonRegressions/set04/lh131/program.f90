! This illustrates influence of the choice of the (in)dependent sets
! on the in-out status of adjoint variables:
! here dists[1,2]%point[1,2]%[x,y] are only in the independent,
! therefore their adjoint dists[1,2]b are output only, i.e. their
! value upon entry into ENERGY_B is immediately set to zero.
! This is possibly surprising but normal. To avoid this resetting,
! dists[1,2]%point[1,2]%[x,y] must be added to the dependents as well.
MODULE TriProblem

  TYPE TT
     REAL*8 :: x
     REAL*8 :: y
  END TYPE TT

  TYPE DD1
     TYPE(TT),POINTER :: point1
     TYPE(TT),POINTER :: point2
     REAL*8 :: requested
  END TYPE DD1

  TYPE DD2
     TYPE(TT),POINTER :: point1
     TYPE(TT),POINTER :: point2
     REAL*8 :: requested
  END TYPE DD2

CONTAINS

  FUNCTION Energy(dists1,dists2)
    REAL*8 :: Energy
    TYPE(DD1),  DIMENSION(*) :: dists1
    TYPE(DD2),  DIMENSION(*) :: dists2
    REAL*8 :: dx,dy,delta

    !$AD II-LOOP
    DO i=1,10
       dx = dists1(i)%point2%x - dists1(i)%point1%x
       dy = dists1(i)%point2%y - dists1(i)%point1%y
       delta = SQRT(dx*dx + dy*dy) - dists1(i)%requested
       Energy = Energy + delta*delta
    END DO

    !$AD II-LOOP
    DO i=1,20
       dx = dists2(i)%point2%x - dists2(i)%point1%x
       dy = dists2(i)%point2%y - dists2(i)%point1%y
       delta = SQRT(dx*dx + dy*dy) - dists2(i)%requested
       Energy = Energy + delta*delta
    END DO

  END FUNCTION Energy

END MODULE TriProblem
