module M

      private x,y
      private privatetype


      type privatetype
        real lower, upper
      end type privatetype

      type(privatetype) :: x,y

      contains
      real function f(t)
      real t
      f = t * t + x%lower * t + y%upper * t
      write (*,*) x%lower , y%upper
      return
      end function

      subroutine init(xv,yv)
       x%lower = xv
       x%upper = xv
       y%lower = yv
       y%upper = yv
      end subroutine

end module


program test
use M
real x,y
call init(10.0,20.0)
x = 3.4
y = f(x)
WRITE (*,*) x, y
end


