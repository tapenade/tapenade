! Pour tester bug trouve par jean: encore un split
! d'argument inutile et nuisible:
! arg1(len=ISIZE1OFarg1) pour TRIM(ADJUSTL(line(2:jj-1)))
  SUBROUTINE BLK(tt1,jj,line)
    REAL :: tt1
    integer jj
    character(len=30), dimension(30) :: line

    CALL INSERTSECTION(TRIM(ADJUSTL(line(2:jj-1))), tt1)
    tt1 = sin(tt1)
  END SUBROUTINE BLK

  subroutine INSERTSECTION(strs, zzz)
    character(len=*), dimension(*) :: strs
    REAL :: zzz
    zzz = zzz*zzz + SUM(length(strs))
  end subroutine INSERTSECTION
