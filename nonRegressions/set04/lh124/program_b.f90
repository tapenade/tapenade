!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 20 Jun 2019 11:04
!
!  Differentiation of blk in reverse (adjoint) mode:
!   gradient     of useful results: tt1
!   with respect to varying inputs: tt1
!   RW status of diff variables: tt1:in-out
! Pour tester bug trouve par jean: encore un split
! d'argument inutile et nuisible:
! arg1(len=ISIZE1OFarg1) pour TRIM(ADJUSTL(line(2:jj-1)))
SUBROUTINE BLK_B(tt1, tt1b, jj, line)
  IMPLICIT NONE
  REAL :: tt1
  REAL :: tt1b
  INTEGER :: jj
  CHARACTER(len=30), DIMENSION(30) :: line
  INTRINSIC ADJUSTL
  INTRINSIC TRIM
  INTRINSIC SIN
  CALL PUSHREAL4(tt1)
  CALL INSERTSECTION(TRIM(ADJUSTL(line(2:jj-1))), tt1)
  tt1b = COS(tt1)*tt1b
  CALL POPREAL4(tt1)
  CALL INSERTSECTION_B(TRIM(ADJUSTL(line(2:jj-1))), tt1, tt1b)
END SUBROUTINE BLK_B

!  Differentiation of insertsection in reverse (adjoint) mode:
!   gradient     of useful results: zzz
!   with respect to varying inputs: zzz
SUBROUTINE INSERTSECTION_B(strs, zzz, zzzb)
  IMPLICIT NONE
  CHARACTER(len=*), DIMENSION(*) :: strs
  REAL :: zzz
  REAL :: zzzb
  INTRINSIC LENGTH
  INTRINSIC SUM
  zzzb = 2*zzz*zzzb
END SUBROUTINE INSERTSECTION_B

