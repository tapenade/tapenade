! Bug trouve par Hubert
! on dirait que adjoint-liveness ne propage pas
! les variables utilisees par un allocate.
subroutine bugalloc(X,Y,nn)
  real :: X,Y
  integer :: nn
  real, dimension(:), allocatable :: T
  integer :: size

  size = 2*nn
  X = X*Y
  ALLOCATE(T(size))
  T(:) = X*X
  Y = T(3)*T(4)
  DEALLOCATE(T)
  Y = Y*Y
end subroutine bugalloc
