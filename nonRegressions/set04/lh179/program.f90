MODULE MM1
  REAL :: vv1,vv2,vv3
END MODULE MM1

MODULE MM2
  USE MM1
  REAL :: bb1
END MODULE MM2

! Bug on opa-nemo after rev:5730 cleaning up visible zones:
!  vv2 was wrongly classified as side-effect because of
!  the USE-ONLY, whereas it is visible through the other USE.
SUBROUTINE TEST(xx)
  USE MM2
  USE MM1, ONLY:vv1
  REAL xx
  vv1 = vv1*xx
  CALL FOO()
  vv2 = vv2*xx
  xx = xx*vv1*vv2
END SUBROUTINE TEST

SUBROUTINE FOO()
  USE MM1
  vv2 = vv2*vv2
END SUBROUTINE FOO
