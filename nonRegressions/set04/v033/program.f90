!@+leo
!@+node:0::@file burg.f
!@+body
!@@language fortran
	subroutine Fburgers(F, um1, dim, dt, dx, visco)

	Real, dimension(0:dim-1), intent(inout)::u
	Real, dimension(0:dim-1), intent(in)::um1
	Integer, intent(in)::dim
	Real, intent(in)::dt, dx, visco
	
	Integer::i
	
		Do i=1,dim-2
			F(i)=-1./(4.*dx)*(um1(i+1)**2-um1(i-1)**2) &
     &    +visco/(dx**2)*(um1(i+1)-2.*um1(i)+um1(i-1))
		End Do
		
	Return
	End



!@-body
!@-node:0::@file burg.f
!@-leo
