!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 18 Apr 2019 18:10
!
!  Differentiation of ptr1 in forward (tangent) mode:
!   variations   of useful results: a
!   with respect to varying inputs: b c
!   RW status of diff variables: a:out b:in c:in
SUBROUTINE PTR1_D(a, ad, b, bd, c, cd)
  IMPLICIT NONE
  REAL*8 :: a
  REAL*8 :: ad
  REAL*8, TARGET :: b, c
  REAL*8, TARGET :: bd, cd
  REAL*8, POINTER :: p
  REAL*8, POINTER :: pd
  IF (a .GT. 0.0) THEN
    pd => bd
    p => b
  ELSE
    pd => cd
    p => c
  END IF
  ad = 2*p*pd
  a = p*p
END SUBROUTINE PTR1_D

