subroutine ptr1(a,b,c)
 REAL*8 :: a
 REAL*8,target :: b,c
 REAL*8,pointer :: p

 if (a.gt.0.0) then
    p => b
 else
    p => c
 endif
 a = p*p
end subroutine ptr1
