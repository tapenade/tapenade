      subroutine state_residuals(psi,gamma,w, n_control, n_state)
      implicit none
      integer, intent(out) :: n_control, n_state
      real(8), allocatable, intent(inout) :: gamma(:) , w(:)
      real(8), allocatable, intent(inout) :: psi(:)

      n_state = 2
      n_control = 2

      if (.not. allocated(gamma)) allocate(gamma(n_control))
      if (.not. allocated(w)) allocate(w(n_state))
      if (.not. allocated(psi)) allocate(psi(n_state))

      psi(1) = gamma(2) - gamma(1) - w(1) + w(2)**2 + (gamma(1)-gamma(2))**2*w(1)
      psi(2) = 2.0*w(1) - w(2)**2

      end
