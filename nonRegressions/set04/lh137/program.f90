!Encore un exemple pour les questions de contexte d'appel
! du code differentie, les _D et les _CD, pour les
! problemes poses par Jean. Simplifie de lha36 mais
! plus vicieux du cote des enchainements de calls.
module MMA
  REAL, ALLOCATABLE, DIMENSION(:) :: TT0
  REAL, ALLOCATABLE, DIMENSION(:) :: TT1
  REAL, ALLOCATABLE, DIMENSION(:) :: TT2
  REAL, ALLOCATABLE, DIMENSION(:) :: TT3
  REAL, ALLOCATABLE, DIMENSION(:) :: TT4

contains

  subroutine initSome(someTT)
    REAL, ALLOCATABLE, DIMENSION(:) :: someTT
    ALLOCATE(someTT(20))
  end subroutine initSome

  subroutine deallocSome(someTT)
    REAL, ALLOCATABLE, DIMENSION(:) :: someTT
    DEALLOCATE(someTT)
  end subroutine deallocSome
end module MMA

module MMX
contains
  subroutine initSomeLev3(ST)
    REAL, ALLOCATABLE, DIMENSION(:) :: ST
    call initSomeLev2(ST)
  end subroutine initSomeLev3

  subroutine initSomeLev2(ST)
    use MMA
    REAL, ALLOCATABLE, DIMENSION(:) :: ST
    call initSome(ST)
  end subroutine initSomeLev2

  subroutine deallocSomeLev3(ST)
    REAL, ALLOCATABLE, DIMENSION(:) :: ST
    call deallocSomeLev2(ST)
  end subroutine deallocSomeLev3

  subroutine deallocSomeLev2(ST)
    use MMA
    REAL, ALLOCATABLE, DIMENSION(:) :: ST
    call deallocSome(ST)
  end subroutine deallocSomeLev2
end module MMX

subroutine ROOT(XX,ZZ)
  use MMA
  REAL, ALLOCATABLE, DIMENSION(:) :: XX
  REAL :: ZZ

  ZZ = TT1(1)*TT2(2) + TT3(3)*TT4(4)
  ZZ = ZZ * SUM(XX(:))
end subroutine ROOT

program MAIN
  use MMA
  use MMX
  INTERFACE
     subroutine ROOT(XX,ZZ)
       REAL, ALLOCATABLE, DIMENSION(:) :: XX
       REAL :: ZZ
     end subroutine ROOT
  END INTERFACE
  REAL, ALLOCATABLE, DIMENSION(:) :: X
  REAL :: Z
  INTEGER i
  ALLOCATE(X(30))
  call initSome(TT0)
  call initSome(TT1)
  call initSome(TT2)
  call initSomeLev2(TT3)
  call initSomeLev3(TT4)
  do i = 1,30
     X(i) = 3.3*i
  enddo
  TT0 = 1.0
  TT1 = 1.1
  TT2 = 1.2
  TT3 = 1.3
  TT4 = 1.4
  call ROOT(X,Z)
  print*,'Z=',Z,'(should be 4818.330)'
  DEALLOCATE(X)
  call deallocSome(TT0)
  call deallocSome(TT1)
  call deallocSome(TT2)
  call deallocSomeLev2(TT3)
  call deallocSomeLev3(TT4)
end program MAIN
     
  
  
  

  
