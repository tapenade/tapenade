! Bug Schreier:
! Tool: (Differentiation of a Flow Graph) connecting a Flow Arrow twice!
SUBROUTINE FOO (verbose, axs)
  INTEGER :: verbose
  REAL :: axs(5)
  REAL :: timeInit=0.0
  INTRINSIC CPU_TIME

  IF (verbose>1) THEN
!$AD DO-NOT-DIFF
     CONTINUE
     axs(3) = axs(3)*axs(3)
!$AD END-DO-NOT-DIFF
     CONTINUE
  ENDIF
  axs(2) = axs(2)*axs(2)
END SUBROUTINE FOO
