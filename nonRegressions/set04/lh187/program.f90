! From the bug report "inviscidCentralFlux" from G. Kenway
! about wrong AD of multiply nested II-loops 
! Without the explicit Checkpoint:

SUBROUTINE INVISCIDCENTRALFLUX(w,res)
  implicit none
  real*8 w(30,30)
  integer(kind=intType) :: i, k
!  real(kind=realType) :: vnp,qsp,res
  real*8 :: vnp,qsp,res

  !DIR$ IVDEP
  !$AD II-LOOP labk22
  do k=2,22
     !DIR$ IVDEP
     !$AD II-LOOP labi22.1
     do i=2,22
        vnp = w(i+1,k)*w(i,k) + w(i+1,k)*w(i,k) + w(i+1,k)*w(i,k)
        qsp = SIN(vnp)
        res = res + qsp*qsp
     enddo
  enddo

  !$AD II-LOOP labk11
!  DIR$ IVDEP
  do k=1,11
     !$AD II-LOOP labi22.2
     !DIR$ IVDEP
     do i=2,22
        vnp = w(i+2,k)*w(i,k) + w(i+2,k)*w(i,k) + w(i+2,k)*w(i,k)
        qsp = SIN(vnp)
        res = res + qsp*qsp
     enddo
  enddo
END SUBROUTINE INVISCIDCENTRALFLUX
