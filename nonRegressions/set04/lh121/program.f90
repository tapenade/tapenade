!Un exemple pour verifier que les diff des ALLOCATE/DEALLOCATE
! sont coherents quand on jongle avec l'aliasing.
! Si on differentie trop, mais que ca reste juste, on est content!
subroutine foo(x,y)
  real, dimension(:), allocatable, target :: mm1, mm2
  real, dimension(33), target :: mm3
  real, dimension(:), pointer :: ptr, qtr

  if (x>1.0) then
     ALLOCATE(mm1(11))
  else if (x>2.0) then
     ALLOCATE(mm2(22))
  endif

  if (x>1.1) then
     ptr => mm1
  else if (x>2.1) then
     ptr => mm2
  else
     ptr => mm3
  endif

  if (x>1.2) then
     ptr = x*x
     y = ptr(3) * ptr(5)
  endif

  if (x>1.3) then
     DEALLOCATE(mm1)
  else if (x>2.3) then
     DEALLOCATE(ptr)
  else
     qtr => ptr
     DEALLOCATE(qtr)
  endif
end subroutine foo

program main
  real :: x,y
  x=1.4
  call foo(x,y)
  print *,y
end program
