   recursive subroutine remove (old_node)
! Remove a first child node and the subtree rooted on it (if any),
! deallocating associated pointer targets.

   type :: node
       character(len=10)     :: name            ! name of node
       real, pointer, dimension(:) :: y               ! stored real data
       type(node), pointer        :: parent           ! parent node
       type(node), pointer        :: sibling          ! next sibling node
       type(node), pointer        :: child            ! first child node
   end type node


      type(node), pointer :: old_node
      type(node), pointer :: child, sibling
!
      child => old_node%child
      do
         if (.not.associated(child)) then
           exit
         end if
         sibling => child%sibling
         call remove(child)
         child => sibling
      end do
! remove leaf node
      if (associated(old_node%parent)) then
         old_node%parent%child => old_node%sibling
      end if
      deallocate (old_node%y)
      deallocate (old_node)
   end subroutine remove
