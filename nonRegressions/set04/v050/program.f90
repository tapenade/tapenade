module m
real i, j
end module m

module n
use m
namelist/nml/ i,j
real z
end module n

subroutine test(a,b)
use n
real a,b
b = a + i
j = b
end subroutine test
