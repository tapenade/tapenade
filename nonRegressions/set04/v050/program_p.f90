!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.6 (r4255M) - 15 Dec 2011 14:41
!
MODULE M
  IMPLICIT NONE
  REAL :: i, j
END MODULE M

MODULE N
  USE M
  IMPLICIT NONE
  NAMELIST /nml/ i, j
  REAL :: z
END MODULE N

SUBROUTINE TEST(a, b)
  USE N
  IMPLICIT NONE
  REAL :: a, b
  b = a + i
  j = b
END SUBROUTINE TEST

