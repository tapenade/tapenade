! To test correct call shapes when context calls
!  differentiated non-context function.
! This test uses -nooptim activity but
!  this is probably not related:
!  bug can happen also with activity turned on.

  REAL(KIND=8) FUNCTION RETARD(CK,I_ZONE,SP,TEMPS,IFIM1)
   INTEGER:: I_ZONE,SP
  INTEGER, OPTIONAL ::IFIM1
   REAL(KIND=8):: CK,SORP,EPSI
  REAL(KIND=8), OPTIONAL ::TEMPS
   EPSI=1.0D-14
   RETARD=CK*I_ZONE
 END FUNCTION

 SUBROUTINE COMP_PRECIPITATION(CK,DELTAT)
  REAL(KIND=8) :: DELTAT, CK(2,2), RN
  INTEGER:: SP, I_ZONE
     RN = RETARD(CK(1,1),I_ZONE,SP)
 END SUBROUTINE
