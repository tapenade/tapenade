subroutine set_q ( h, q )
!
    implicit none
!
! ... variables pass�es en param�tre ...................................
!
    real(8), dimension(2), intent(in)    :: h
    real(8), dimension(2), intent(out)   :: q
!
! ... d�but du traitement ..............................................
!
    q(1) = h(2)*h(1)
    q(2) = h(1)
!
end subroutine set_q
subroutine push_size(x,y)
!
    implicit none
!
! ... variables pass�es en param�tre ...................................
!
    real(8), dimension(2,2),    intent(in)      :: x
    real(8),                    intent(out)     :: y
!
! ... variables locales ................................................
!
    real(8), dimension(2,2) :: xt
    real(8), dimension(2)   :: q
!
! ... d�but du traitement
!
    xt = x
!
    call set_q(xt(:,1),q)
!
    xt(2,1) = q(1)
!
    y = xt(2,1)**2
!
end subroutine push_size
