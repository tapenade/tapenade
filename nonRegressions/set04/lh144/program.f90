! Reproduction d'un bug de Jean en mode -context:
! quand une routine differentiee en mode -context
! appelle une external, il ne faut pas considerer que
! cette external est susceptible d'allouer de la
! memoire pour la fabrication du contexte.
! cf commentaire dans ADDifferentiator:
!                 // [llh] bug Jean: des calls a des external etaient differenties dans la partie contexte,
!                 // et ca causait la creation de tas de dummyzerodiff
!                 // qu on ne savait pas declarer ensuite:
! PLUS
!  reproduction d'un autre bug avec le mode noisize:
! ca fabrique des:
!    DO ii1=1,SIZE(ccd(ii1), 1)
!      ccd(ii1)%val = 0.0
!      ccd(ii1)%ppp => NULL()
!    END DO
! manifestement faux !!!
module MMA
  TYPE TOTO
     REAL :: val
     REAL,DIMENSION(:),POINTER :: ppp
  END TYPE TOTO
  REAL, ALLOCATABLE, DIMENSION(:) :: AA,BB
  TYPE(TOTO),ALLOCATABLE,DIMENSION(:) :: CC
contains
  subroutine initA()
    external SOMEEXTERNAL
    ALLOCATE(AA(20))
    ALLOCATE(BB(30))
    ALLOCATE(CC(40))
    call SOMEEXTERNAL(AA,BB)
    call SOMEEXTERNAL(AA,AA)
  end subroutine initA

end module MMA

module MMB
  INTEGER :: flag
  REAL :: zz
end module MMB

program MAIN
  use MMA
  use MMB
  REAL, ALLOCATABLE, DIMENSION(:) :: X
  INTEGER i
  REAL cva(2)
  common /cc1/cva
  call getflag(flag)
  ALLOCATE(X(30))
  call initA()
  do i = 1,20
     AA(i) = 100.0/i
     X(i) = 3.3*i
  enddo
  cva(1) = 1.8
  cva(2) = 8.1
  call ROOT(X)
  print*,'zz=',zz
  DEALLOCATE(AA)
  DEALLOCATE(X)
end program MAIN

subroutine ROOT(X)
  use MMA
  use MMB
  REAL, DIMENSION(:) :: X
  REAL :: V
  REAL :: cv1,cv2
  common /cc1/cv1,cv2

  zz = cv1*cv2
  CC(5)%val = zz
  v = SUM(AA)
  if (flag>2) then
     zz = zz*v
  endif
  zz = zz + SUM(X) + CC(5)%val
end subroutine ROOT
     
  
  
  

  
