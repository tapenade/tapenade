! bug de generation FG adjoint trouve par Jan Hueckelheim
! message du bug: "Connecting a Flow Arrow twice!"
module flowPublicModule
  implicit none
  type flowPublicType
    real, dimension(:), pointer :: fcr
    integer, dimension(:), pointer :: faceToNode
  end type flowPublicType
  type(flowPublicType) :: flowPublicData
end module flowPublicModule

SUBROUTINE getcrn_all(f,cid,returnNeeded)
   USE flowPublicModule, ONLY : flow => flowPublicData
   IMPLICIT NONE
   REAL, DIMENSION(:), POINTER :: f_node, f
   integer, DIMENSION(:), POINTER :: lfcrn
   integer :: icrn0
   integer :: ibc, icrn, cid
   logical :: returnNeeded
  if(.not. returnNeeded) then
   f_node => flow%fcr
   if(cid.ne.0) then
     lfcrn  => flow%faceToNode
     DO ibc = 1, 10
       DO icrn = 1, 10
         icrn0 = lfcrn(icrn)
         f_node(icrn0) = f(cid)
       END DO
     END DO
  end if
 END IF
END SUBROUTINE getcrn_all
