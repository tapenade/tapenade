! Pour tester un possible bug de l'analyse InOut
! dans le cas ou une variable POINTEE est affectee par
! la routine appelee.

MODULE TEST
  real, dimension(10), allocatable :: glob

CONTAINS
  subroutine top(a,b)
    ! this subroutine will exhibit the in-out bug:
    ! if func is not found to read/write *glob,
    !  then *glob will not be checkpointed !
    real :: a,b
    a = SIN(a)
    call func(a,b)
    b = COS(a*b)
  end subroutine top

  subroutine func(a,b)
    real :: a,b
    a = a*b
    call foo(a,glob)
    b = b*a
  end subroutine main
END MODULE TEST
