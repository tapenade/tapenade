module M
       TYPE INTERVAL
              REAL LOWER, UPPER
              character(256) :: name
       END TYPE INTERVAL

end module

module N
use M, interv => interval
      contains
      real function ff(t)
      use M, interv => interval
      type(interv) :: t
      ff = t%lower + t%upper
      return
      end function
end module
