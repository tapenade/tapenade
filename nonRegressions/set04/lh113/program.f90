! Essai pour reproduire le bug trouve par Dominic Jones
!  -- trop d'arguments differenties pour la head routine
!  -- option "-fixinterface" inoperante.
MODULE MM

TYPE TT1
   INTEGER nn
   REAL ff
   REAL, DIMENSION(:), ALLOCATABLE :: contents
END TYPE TT1

CONTAINS

  SUBROUTINE BAR(state, array)
    real, dimension(:) :: state
    REAL, DIMENSION(:) :: array
    DO i=1,9
       array(i) = SIN(state(i))
    END DO
  END SUBROUTINE BAR

  SUBROUTINE GEE(obj, array)
    real obj
    REAL, DIMENSION(:) :: array
    obj = SIN(obj)
    DO i=1,5
       obj = obj + 2.0*array(i)
    END DO
  END SUBROUTINE GEE

  SUBROUTINE FOO(state, obj, var)
    real, dimension(:) :: state
    real obj
    TYPE(TT1) var
    integer i

    obj = 12.5
    CALL BAR(state, var%contents)
    do i=1,10
       state(i) = state(i)*state(i+1)
       obj = obj + state(i)*obj
    enddo
    CALL GEE(obj, var%contents)
  END SUBROUTINE FOO

end MODULE MM
