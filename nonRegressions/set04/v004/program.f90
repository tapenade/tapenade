module example3
 implicit none

 type vector
     real :: x,y
 end type vector

 interface operator (+)
     module procedure addvector
 end interface

 type(vector) :: u,v,w

contains
 function test(a,b)
  type(vector) :: a,b,test
  test = a + b + u
 end function test

 function addvector(a,b)
  type(vector), intent(in) :: a,b
  type(vector) :: addvector
  addvector%x = a%x + b%x
  addvector%y = a%y + b%y
 end function addvector

end module
