! Bug trouve par Jean Utke.
! On n'a pas le droit d'utiliser la notation tableaux
! quand il y a un pointeur a droite du (:).
! Exemple: ici interdit de generer: globalrecordd%lrecs()%a = 0.0
! => Donc, Tapenade ne doit pas generer d'instructions
! de ce style pour les initializations par exemple.
module test

  type tt2
     real,pointer:: a
     real :: b
  end type tt2

  type rectype
     real :: rr
     type(tt2), dimension(:), allocatable :: lrecs
  end type rectype

  type(rectype) :: globalrecord

contains

  subroutine top(a,x)
    REAL :: a,b,x
    REAL, TARGET :: ra
    b = 5.0
    ra = 0.0
    call initgp(ra)
    ! force entry activity of foo() only on x :
    call foo(b, x)

    call initga(a)
    ! force full entry activity of foo() :
    call foo(a, x)
  end subroutine top


  subroutine initgp(ra)
    REAL, TARGET :: ra
    integer :: i
    ALLOCATE(globalrecord%lrecs(11))
    globalrecord%rr = 1.0
    DO i=1,11
       globalrecord%lrecs(i)%a => ra
       globalrecord%lrecs(i)%a = 2.0
    END DO
    globalrecord%lrecs(:)%b = 3.0
  end subroutine initgp

  subroutine initga(a)
    real :: a
    globalrecord%rr = a
    DO i=1,11
       globalrecord%lrecs(i)%a = a
    END DO
    globalrecord%lrecs(:)%b = a
  end subroutine initga

  subroutine foo(v, x)
    real :: v,x
    integer :: i
    do i=1,5
       v = v + globalrecord%lrecs(i)%a*globalrecord%lrecs(i)%b
    enddo
    x = x*x
  end subroutine foo

end module test
