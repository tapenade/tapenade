subroutine foo(X,Y)
  real :: X,Y
  real, dimension(:), pointer :: P
  real, dimension(:), allocatable :: T
  real, dimension(21) :: U

  allocate(T(19))
  P => T
  P = Y
  X = SUM(P)
  deallocate(T)
  P => U
  P = Y
  X = X + SUM(P)
end subroutine foo
