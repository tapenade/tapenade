!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) - 24 Feb 2022 12:44
!
!  Differentiation of foo in forward (tangent) mode:
!   variations   of useful results: [alloc*t in foo] x
!   with respect to varying inputs: [alloc*t in foo] y
!   RW status of diff variables: [alloc*t in foo]:in-out x:out
!                y:in
SUBROUTINE FOO_D(x, xd, y, yd)
  IMPLICIT NONE
  REAL :: x, y
  REAL :: xd, yd
  REAL, DIMENSION(:), POINTER :: p
  REAL, DIMENSION(:), POINTER :: pd
  REAL, DIMENSION(:), ALLOCATABLE :: t
  REAL, DIMENSION(:), ALLOCATABLE :: td
  REAL, DIMENSION(21) :: u
  REAL, DIMENSION(21) :: ud
  INTRINSIC SUM
  ALLOCATE(td(19))
  td = 0.0
  ALLOCATE(t(19))
  pd => td
  p => t
  pd = yd
  p = y
  xd = SUM(pd)
  x = SUM(p)
  IF (ALLOCATED(td)) THEN
    DEALLOCATE(td)
  END IF
  DEALLOCATE(t)
  pd => ud
  p => u
  ud = 0.0
  pd = yd
  p = y
  xd = xd + SUM(pd)
  x = x + SUM(p)
END SUBROUTINE FOO_D

