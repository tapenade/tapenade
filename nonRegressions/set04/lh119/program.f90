subroutine test(x,y)
real :: x,y
integer i
real :: a,b
real, pointer :: p

i=10
a = 5.5
b = 8.8
p => b
p = p*p
if (i>2) then
   p => x
else
   p => a
endif
y = y * p
end subroutine test
