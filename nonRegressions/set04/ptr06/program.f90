module program
type struct
real*4, pointer :: q

end type struct

contains

subroutine ptr2(a,b,c,r)
 REAL*8 :: a
 REAL*8,target :: b
 REAL*4,target :: c
 REAL*4,pointer :: p
 REAL*8,pointer :: q
 REAL*8 :: r
 TYPE(struct) :: struct

 if (a.gt.0.0) then
    p => b
 else
    p => c
 endif
 p = a*a

 if (a.gt.0.0) then
    p = q
 else
    p = struct%q
 endif
 r = p * a
end subroutine ptr2

end module program
