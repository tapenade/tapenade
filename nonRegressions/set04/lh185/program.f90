! Tests TreeGen in presence of extra loop exits
SUBROUTINE TEST(i)
INTEGER nterm, index
nterm = 1
DO WHILE (.true.)
  index = i
  IF (index .EQ. 0) THEN
    PRINT*, 'index 0'
    exit
  END IF
  PRINT*, 'index != 0'
  nterm = nterm + 1
ENDDO
RETURN
END

program MAIN
INTEGER n
n = 0
CALL TEST(n)
END

