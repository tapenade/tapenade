module Defs

! Size of pb
integer, parameter :: ne = 6
integer, parameter :: nc = 3
integer, save :: ns != 1

! Number of variables
integer, save :: n != (ne+nc)*ns+ne

! Number of constraints
integer, save :: m != (ne+1)*ns

! Number of non zero elements of jacoban
integer, save :: nnzj != 1 + (n+1)*ne*ns

! Number of non zero elements of the hessian of the lagrangian
integer, parameter :: nnz_w = 0

! Problem variables
real (kind=8), save :: Tmax, L0, Lf
integer, parameter :: lpar = 14
real (kind=8), dimension(lpar), save :: par

real (kind=8), allocatable, dimension(:), save :: L

! Some constant
real (kind=8), parameter :: pi = 3.1416d0

end module Defs

! **************************************************************
! **************************************************************

subroutine phifun(l,xu,xp1,h,phi)

use Defs, only : ne, nc
implicit none

! Defines a Runge-Kutta Schemes of order 4
! .. Arguments ..
real (kind=8), intent(in) :: l
real (kind=8), dimension(ne+nc), intent(in) :: xu
real (kind=8), dimension(ne), intent(in) :: xp1
real (kind=8), intent(in) :: h
real (kind=8), dimension(ne), intent(out) :: phi

! .. Local declarations ..
real (kind=8), dimension(ne) :: phi1, phi2, phi3, phi4, x1, x2
real (kind=8), dimension(ne) :: f1, f2, f3, f4
real (kind=8), dimension(nc) :: u

! .. First executable statements ..
x1(:) = xu(1:ne)
u(:) = xu(ne+1:ne+nc)
x2(:) = xp1

phi1(:) = x1(:)
call ffun(l,phi1,u,f1)
phi2(:) = x1(:) + h/2.d0*f1(:)
call ffun(l+h/2.d0,phi2,u,f2)
phi3(:) = x1(:) + h/2.d0*f2(:)
call ffun(l+h/2.d0,phi3,u,f3)
phi4(:) = x1(:) + h*f3(:)
call ffun(l+h,phi4,u,f4)

phi(:) = x1(:) - x2(:) + h/6.d0*(f1(:) + 2.d0*f2(:) + 2.d0*f3(:) + f4(:))

return
end subroutine phifun

! **************************************************************
! **************************************************************

subroutine ffun(l,x,u,f)

use Defs, only : par, ne, nc
implicit none

! .. Arguments ..
real (kind=8), intent(in) :: l
real (kind=8), dimension(ne), intent(in) :: x
real (kind=8), dimension(nc), intent(in) :: u
real (kind=8), dimension(ne), intent(out) :: f

! .. Local declarations ..
real (kind=8) :: Beta, mu0, co, si, q, s, w, Pmu0
real (kind=8) :: Z, H, XX, fq(ne-1), fs(ne-1), fw(ne-1), dt
integer, parameter :: fid = 10

! .. First executable statement ..
Beta = par(1)
mu0 = par(2)

co = cos(l)
si = sin(l)

Z = 1.d0 + x(2)*co + x(3)*si
H = x(4)*si - x(5)*co
XX = 1.d0 + x(4)**2+x(5)**2

q = u(1)
s = u(2)
w = u(3)

Pmu0 = dsqrt(x(1)/mu0)
dt = 1.d0 / (Z**2/x(1)/Pmu0 + Pmu0/x(6)*H/Z*w)

fq(1) = 0.d0
fq(2) = si
fq(3) = -co
fq(4) = 0.d0
fq(5) = 0.d0

fs(1) = 2*x(1)/Z
fs(2) = co + (x(2)+co)/Z
fs(3) = si + (x(3)+si)/Z
fs(4) = 0.d0
fs(5) = 0.d0

fw(1) = 0.d0
fw(2) = -H*x(3)/Z
fw(3) = H*x(2)/Z
fw(4) = XX/2.d0*co/Z
fw(5) = XX/2.d0*si/Z

f(1:5) = dt*Pmu0/x(6)*(fq(:)*q+fs(:)*s+fw(:)*w)
f(6) = -dt*Beta*dsqrt(q**2+s**2+w**2+par(14))

return
end subroutine ffun


