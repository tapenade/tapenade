! "Simple" routine to test my understand of AD via tapenade.
! Routine codes the numerical integration of z^2+2*z+1 from 0 to x
! x and dx given as command line arguments
! Charlotte Kotas, ORNL
!------------------------------------------------
MODULE my_routines
IMPLICIT NONE

CONTAINS

SUBROUTINE SUB1(x,y,dx)
IMPLICIT NONE
REAL, INTENT(IN)  :: x, dx
REAL, INTENT(OUT) :: y
REAL localsum
REAL x1, x2

! Determine range to be computed by this section
x1 = 0.0
x2 = x

! Compute local sum (numerical integration from x1 to x2)
CALL MY_SUM( x1, x2, dx, localsum )

y = localsum

RETURN
END SUBROUTINE SUB1

SUBROUTINE MY_SUM( x1, x2, dx, localsum )
IMPLICIT NONE
REAL, INTENT(IN) :: x1, x2, dx
REAL, INTENT(OUT):: localsum
REAL xt
REAL f1, f2
REAL diff
LOGICAL done

IF (x1 .eq. x2 ) THEN
  localsum=0.0
  RETURN
ENDIF
IF (x1 .gt. x2 ) THEN
  WRITE(*,*) "Error in MY_SUM. x2 < x1"
  STOP
ENDIF
IF ( dx .le. 0 ) THEN
  WRITE(*,*) "Error in MY_SUM. dx must be positive"
  STOP
ENDIF

xt = x1
CALL MY_FUN(xt,f1)
localsum = 0.0
done=.FALSE.
DO
  diff=dx
  xt = xt + diff
  IF( xt .ge. x2 ) THEN
    IF( xt-x2 .le. dx ) THEN
      diff=x2-(xt-diff)
      xt=x2
      done=.TRUE.
    ELSE
      WRITE(*,*) "Something weird in MY_SUM..."
      RETURN
    ENDIF
  ENDIF
  CALL MY_FUN(xt,f2)

  localsum = localsum + (diff)*(0.5)*( f1 + f2 )
  IF ( done ) EXIT
  f1 = f2
ENDDO

RETURN
END SUBROUTINE MY_SUM

SUBROUTINE MY_FUN(x,f)
REAL, INTENT(IN) :: x
REAL, INTENT(OUT) :: f
f = x*x+2*x+1
END SUBROUTINE MY_FUN

END MODULE my_routines

!------------------------------------------------
PROGRAM MYPROG
USE my_routines
IMPLICIT NONE
REAL x, y, dx

y=0.0
x=5.0
dx=0.05
CALL SUB1(x,y,dx)
write(*,*) "y(x=",x,",dx=,",dx,") = ", y
write(*,*) "analytical ", x*x*x/3.0 + x*x + x

STOP
END PROGRAM MYPROG


