! Exemple quasi identique a lh96, mais avec l'option supplementaire
! "-nooptim difftypes" pour ne pas stripper les champs passifs
! des types differenties.
module MM

  type typea
     real :: pp
     real :: y
     integer :: j
  end type typea

  type typeb
     real, dimension(10) :: x
     integer :: i
  end type typeb

  type typetop
     real :: truc
     type (typea) :: chose
     type (typeb), DIMENSION(49,:,:) :: tab
     integer :: iii
  end type typetop

contains

  subroutine top(A,B,x,y,i)
    type(typetop), DIMENSION(5) :: A,B
    real x,y
    integer i

    A%truc = B%truc * 2.0
    A%chose%y = B%chose%y * 3.0
    A%tab = B%tab
    x = y * A(i)%tab(1,2,i)%x(i)
    
  end subroutine top

end module MM
