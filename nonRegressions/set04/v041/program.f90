module M
   real, public :: x
end module

module N
use M, z => x
      contains
      real function ff(t)
      use M, z => x
      real t
      ff = t * z
      z = ff
      return
      end function
end module
