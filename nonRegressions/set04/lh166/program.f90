!Bug trouve par Jan Hueckelheim Fev 2015
!PUSHINTEGER8 couple avec POPINTEGER4


SUBROUTINE comcor(convn, rhof, bctype, nbc)
  IMPLICIT NONE
  INTEGER, PARAMETER :: int_p = 8
  INTEGER, PARAMETER :: real_p = 8
  INTEGER :: bctype, nbc(10)
  REAL(real_p),   DIMENSION(:) ::  rhof, convn
  INTEGER(int_p) :: ibc,ibt,ibtype,nbfaces, bc_pt0

  DO ibt = 1,10
   SELECT CASE (bctype)
   CASE (1)
      DO ibc = 1,nbc(ibt)
         convn(1) = convn(1) + rhof(1)
      END DO
   CASE DEFAULT
     ibc = 1
   END SELECT
  END DO

END SUBROUTINE comcor
