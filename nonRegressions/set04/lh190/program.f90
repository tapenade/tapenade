! Pour tester des bugs dans les types des interfaces
! Bugs trouves par Charlotte Kotas.

MODULE MYGLOB
  IMPLICIT NONE
  REAL glob
END MODULE MYGLOB

MODULE MYMOD
  IMPLICIT NONE

  INTERFACE
     FUNCTION FF1(a) RESULT (res)
       REAL a
       REAL res
     END FUNCTION FF1
  END INTERFACE

  INTERFACE
     FUNCTION MYFUNC(x,y) RESULT(h)
       IMPLICIT NONE
       REAL x,y
       REAL h
     END FUNCTION MYFUNC
  END INTERFACE

CONTAINS
  FUNCTION MYFUNC(x,y) RESULT(h)
    USE MYGLOB
    IMPLICIT NONE
    REAL x,y
    REAL h
    x = x+y
    glob = x
    h = x*y
    h = SQRT(h+glob)
  END FUNCTION MYFUNC
END MODULE MYMOD
  
FUNCTION FF1(a) RESULT (res)
  USE MYGLOB
  USE MYMOD
  IMPLICIT NONE
  REAL a
  REAL res
  a = a*a
  glob = a
  res = MYFUNC(a,a)
  res = res*res + glob
END FUNCTION FF1
