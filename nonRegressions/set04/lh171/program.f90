! Bug trouve par Sylvia Sullivan sylvia.c.sullivan@gmail.com
! Nenes Research Group / Georgia Tech
! La dependance z -> x n'etait pas differentiee en adjoint !!
! parce que la librairie F77GeneralLib etait fausse pour SNGL
subroutine pbsngl(x,y,z,a)
real*8 :: descr, a, z
real :: x, y
descr = a / z**2d0
if (descr .le. 0d0) then
     x = y / SNGL(z)
endif
end subroutine pbsngl
