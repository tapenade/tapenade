! Bug trouve par Jean Utke.
! Instructions d'initialisation a zero de variables
! derivees, avant un call, dans le cas ou cette variable
! provient d'une variable ALLOCATED: ca produit du code
! faux du genre: ALLOCATE(cfproj_get_stere_polar)%pole = 0.0 
! Bizarre: j'arrive a (presque) reproduire le bug, mais avec
!  une version plus ancienne (-r3678) que la mienne (-r3972M) ??

module test

  type tt2
     real a,b
  end type tt2

  type rectype
     real :: rr
     type(tt2), dimension(:), allocatable :: lrecs
  end type rectype

  type(rectype) :: globalrecord

contains

  subroutine top(a,x)
    real :: a,b,x
    b = 5.0
    call initgp()
    ! force entry activity of foo() only on x :
    call foo(b, x)

    call initga(a)
    ! force full entry activity of foo() :
    call foo(a, x)
  end subroutine top


  subroutine initgp()
    ALLOCATE(globalrecord%lrecs(11))
    globalrecord%rr = 1.0
    globalrecord%lrecs(:)%b = 3.0
  end subroutine initgp

  subroutine initga(a)
    real :: a
    globalrecord%rr = a
    globalrecord%lrecs(:)%a = a
    globalrecord%lrecs(:)%b = a
  end subroutine initga

  subroutine foo(v, x)
    real :: v,x
    integer :: i
    do i=1,5
       v = v + globalrecord%lrecs(i)%a*globalrecord%lrecs(i)%b
    enddo
    x = x*x
  end subroutine foo

end module test
