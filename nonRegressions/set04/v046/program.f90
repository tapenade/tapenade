module twice_module 
implicit none
public :: twice
private :: twice_real,twice_int
interface twice
   module procedure twice_real,twice_int
end interface twice
contains
elemental function twice_real(x) result(value)
real, intent(in) :: x 
real             :: value
value = 2*x
end function twice_real
!
elemental function twice_int(i) result(value)
integer, intent(in) :: i
integer             :: value
value = 2*i
end function twice_int
end module twice_module 
!
function test(x, i)
use twice_module, only: twice
real x, test
integer i
test = twice(x) + twice(i)
end function test

program xtwice
use twice_module, only: twice
implicit none
external test
real test
print*,twice(3)
print*,twice((/3,4,5/))
print*,twice(1.2)
print*,twice((/1.2,2.2,3.2/))
print*,test(3.0,3)
end program xtwice

