module M
   real, public :: x
end module

module N
use M, z => x
      contains
      real function ff(t)
      use M, z => x
      real t
      ff = t * z
      z = ff
      return
      end function
end module

module O
use M, zo => x
use N
      contains
      real function gg(t)
      use M, zo => x
      real t
      gg = t * zo * ff(t)
      zo = gg
      return
      end function
end module
