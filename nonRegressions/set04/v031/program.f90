MODULE BO
       INTERFACE SWAP
              MODULE PROCEDURE SWAP_DOUBLE, SWAP_REAL
       END INTERFACE
CONTAINS

       SUBROUTINE SWAP_REAL(A, B)
       IMPLICIT NONE
       REAL, INTENT (INOUT)                 :: A, B
       REAL                                 :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_REAL

       SUBROUTINE SWAP_DOUBLE(A, B)
       IMPLICIT NONE
       double precision, INTENT (INOUT)                 :: A, B
       double precision                                 :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_DOUBLE
END MODULE BO

module DO
use BO
contains
      subroutine head(d1,d2,r1,r2)
      double precision d1,d2
      real r1,r2
      call swap(d1,d2)
      call swap(r1,r2)
      end subroutine head

 end module DO

