module a
save
real :: x
real, private :: y
end module a

module b
real, save :: z
end module b

module c
real, private, save :: u
end module c

module d
real :: v
save v
end module d

module e
real, private :: w
save w
end module e
