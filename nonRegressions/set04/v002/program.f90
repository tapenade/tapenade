module example1
 implicit none
 type vector
     real :: x,y,z
 end type vector
 type(vector) :: u,v,w
contains
 function dot_prod(a,b)
  type(vector) :: a,b
  real :: dot_prod
  dot_prod = a%x * b%x &
& +  a%y * b%y  +  a%z * b%z
 end function dot_prod
end module
