module m_traj

    implicit none

    type traj_t
        real(8)   :: z_0
        real(8)   :: z
    end type traj_t

end module m_traj
subroutine calcul(u,x,y)
!
    use m_traj
!
    implicit none
!
    interface
!
        subroutine f1(x,u)
            use m_traj
            real(8),       dimension(2), intent(in)     :: x
            type (traj_t),               intent(inout)  :: u
        end subroutine f1
!
        subroutine f2 (u,y)
            use m_traj
            type (traj_t), intent(inout)    :: u
            real(8),       intent(inout)    :: y
        end subroutine f2
!
    end interface
!
    type (traj_t),         intent(inout)    :: u
    real(8), dimension(2), intent(in)       :: x
    real(8),               intent(out)      :: y
!
    y = 0d0
!
    call f1(x,u)
    call f2(u,y)
!
end subroutine calcul
subroutine f1(x,u)
!
    use m_traj
!
    implicit none
!
    real(8),       dimension(2), intent(in)     :: x
    type (traj_t),               intent(inout)  :: u
!
    u%z = u%z + x(2)/x(1)
!
end subroutine f1
subroutine f2 (u,y)
!
    use m_traj
!
    implicit none
!
    type (traj_t), intent(inout)    :: u
    real(8),       intent(inout)    :: y
!
    y = y + (u%z-u%z_0)**2
!
end subroutine f2
