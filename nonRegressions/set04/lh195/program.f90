subroutine foo(n, x,y)
  implicit none
  integer :: n
  real :: x(n),y
  integer,parameter :: m=2
  real :: x_a(n)
  real :: pr(m), mdout(n)
  real :: c_pr, c_model

  call cost(n, x, m, y)

end subroutine foo

subroutine cost(n,x,m,y)
  implicit none
  integer :: n,m
  real :: x(n),y
  real :: obsdiff(m), obscost
  real :: prdiff(n), prcost
  integer :: lim

  lim = min(n,m)
  obsdiff = 0
  obsdiff(1:lim) = x(1:lim)
  obscost = 0.5 * sum(obsdiff**2)

  prdiff(1) = 0.
  prdiff(2:n) = x(2:n)
  prcost  = 0.5 * sum(prdiff**2)

  y = obscost + prcost
end subroutine cost
