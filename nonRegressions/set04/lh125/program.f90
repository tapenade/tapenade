! Bug trouve par Bruno Maugars (Onera)
! temporaire temp0b9 utilisee avant sa definition !!
subroutine flux_roe5(surfx,surfy,surfz,rm,um,vm,wm,pm,rp,up,vp,wp,pp,pctrad,gam,fc1,fc2,fc3,fc4,fc5,taille)
    
      implicit none 
      integer taille,n
      double precision surfx,surfy,surfz
      double precision pctrad
      double precision rp(taille),up(taille),vp(taille),wp(taille),pp(taille)
      double precision rm(taille),um(taille),vm(taille),wm(taille),pm(taille)
      double precision fc1(taille),fc2(taille),fc3(taille)
      double precision fc4(taille),fc5(taille)

      double precision HALF,ONE
      double precision gam,gam1,gam1_1
      double precision sn,invsn,nx,ny,nz
      double precision qnp,hp
      double precision qnm,hm
      double precision r,rr,uu,vv,ww,ee,hh,cc,vn         
      double precision small,lambda1,lambda4,lambda5
      double precision aa1,aa4,aa5,q1,q4,q5
      double precision du,dv,dw,dvn,dd1,dd4,dd5
      double precision df11, df12, df13, df14, df15
      double precision df451,df452,df453,df454,df455

      double precision fdx1,fdx2,fdx3,fdx4,fdx5
      double precision fdy1,fdy2,fdy3,fdy4,fdy5
      double precision fdz1,fdz2,fdz3,fdz4,fdz5
      double precision toto

      HALF= 0.5d0
      ONE = 1.d0
      gam    = 1.4d0
      gam1   = gam -1.d0 
      gam1_1 = 1.d0/gam1
!c-----


      do n=1,taille
!c-----
      sn = sqrt(surfx**2+surfy**2+surfz**2)
      invsn = ONE/sn
      nx = surfx*invsn
      ny = surfy*invsn
      nz = surfz*invsn
!c-----      
      qnp = nx*up(n) + ny*vp(n) + nz*wp(n)
      hp  = gam*pp(n)*gam1_1/rp(n)+HALF*(up(n)*up(n)+vp(n)*vp(n)+wp(n)*wp(n))
      
      qnm = nx*um(n) + ny*vm(n) + nz*wm(n)
      hm  = gam*pm(n)*gam1_1/rm(n)+HALF*(um(n)*um(n)+vm(n)*vm(n)+wm(n)*wm(n))
!c------      
      r  = SQRT(rp(n)/rm(n))
      rr = SQRT(rp(n)*rm(n))
      
      uu = (up(n)*r+um(n))/(r+ONE)
      vv = (vp(n)*r+vm(n))/(r+ONE)
      ww = (wp(n)*r+wm(n))/(r+ONE)
      ee = HALF*(uu**2+vv**2+ww**2)
      hh = (hp*r+hm)/(r+ONE)
      cc = SQRT(gam1*(hh-ee))
      vn = uu*nx+vv*ny+ww*nz

      lambda1 = ABS(vn)
      lambda4 = ABS(vn+cc)
      lambda5 = ABS(vn-cc)
      
      small = pctrad*(ABS(vn)+cc)

      q1 = HALF + SIGN(HALF,lambda1-small)
      q4 = HALF + SIGN(HALF,lambda4-small)
      q5 = HALF + SIGN(HALF,lambda5-small)
      
      aa1 = q1*lambda1+(ONE-q1)*HALF*(lambda1*lambda1+small*small)/small
      aa4 = q4*lambda4+(ONE-q4)*HALF*(lambda4*lambda4+small*small)/small
      aa5 = q5*lambda5+(ONE-q5)*HALF*(lambda5*lambda5+small*small)/small

      du  = up(n)-um(n)
      dv  = vp(n)-vm(n)
      dw  = wp(n)-wm(n)
      dvn = du*nx+dv*ny+dw*nz
      
      dd1 = (rp(n)-rm(n))-(pp(n)-pm(n))/cc**2
      dd4 = HALF*(pp(n)-pm(n)+rr*cc*dvn)/cc**2
      dd5 = HALF*(pp(n)-pm(n)-rr*cc*dvn)/cc**2
      
      df11 = aa1*dd1
      df12 = aa1*(dd1*uu+rr*(du-nx*dvn))
      df13 = aa1*(dd1*vv+rr*(dv-ny*dvn))
      df14 = aa1*(dd1*ww+rr*(dw-nz*dvn))
      df15 = aa1*(dd1*ee+rr*(uu*du+vv*dv+ww*dw-vn*dvn))
      
      df451 = aa4*dd4            + aa5*dd5
      df452 = aa4*dd4*(uu+nx*cc) + aa5*dd5*(uu-nx*cc)
      df453 = aa4*dd4*(vv+ny*cc) + aa5*dd5*(vv-ny*cc)
      df454 = aa4*dd4*(ww+nz*cc) + aa5*dd5*(ww-nz*cc)
      df455 = aa4*dd4*(hh+vn*cc) + aa5*dd5*(hh-vn*cc)
      
      fdx1=rp(n)*up(n)+rm(n)*um(n)
      fdy1=rp(n)*vp(n)+rm(n)*vm(n)
      fdz1=rp(n)*wp(n)+rm(n)*wm(n)
      
      fdx2=rp(n)*up(n)*up(n)+pp(n)+rm(n)*um(n)*um(n)+pm(n)
      fdy2=rp(n)*up(n)*vp(n)+rm(n)*um(n)*vm(n)
      fdz2=rp(n)*up(n)*wp(n)+rm(n)*um(n)*wm(n)
      
      fdx3=fdy2
      fdy3=rp(n)*vp(n)*vp(n)+pp(n)+rm(n)*vm(n)*vm(n)+pm(n)
      fdz3=rp(n)*vp(n)*wp(n)+rm(n)*vm(n)*wm(n)
      
      fdx4=fdz2
      fdy4=fdz3
      fdz4=rp(n)*wp(n)*wp(n)+pp(n)+rm(n)*wm(n)*wm(n)+pm(n)
      
      fdx5=up(n)*rp(n)*hp+um(n)*rm(n)*hm
      fdy5=vp(n)*rp(n)*hp+vm(n)*rm(n)*hm
      fdz5=wp(n)*rp(n)*hp+wm(n)*rm(n)*hm
      
     fc1(n) = HALF*sn*(fdx1*nx + fdy1*ny + fdz1*nz - df11 - df451)
     fc2(n) = HALF*sn*(fdx2*nx + fdy2*ny + fdz2*nz - df12 - df452)
     fc3(n) = HALF*sn*(fdx3*nx + fdy3*ny + fdz3*nz - df13 - df453)
     fc4(n) = HALF*sn*(fdx4*nx + fdy4*ny + fdz4*nz - df14 - df454)
     fc5(n) = HALF*sn*(fdx5*nx + fdy5*ny + fdz5*nz - df15 - df455)

      enddo
!c------------------------------------------------------------------
      return
      
end
