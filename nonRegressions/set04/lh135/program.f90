! Bug trouve par Hubert
! Il y a des reinitialisations en trop de tableaux
! differenties, qui se retrouvent
! APRES le deallocate de ces tableaux => segfault !
module M1
  real, dimension(:), allocatable :: T
end module M1

subroutine top(X,Y)
  real :: X,Y
  Y = Y*X
  call bugdealloc(X,Y,12)
  X = X*X
end subroutine top

subroutine bugdealloc(X,Y,nn)
  use M1
  real :: X,Y
  integer :: nn,i

  ALLOCATE(T(nn))
  do i=1,7
     T(i) = X*X
  enddo
  Y = T(3)*T(4)
  DEALLOCATE(T)
  Y = Y*Y
end subroutine bugalloc
