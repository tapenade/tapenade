! Teste que la dimension donnee a un dummyzerodiffd est la
! meme que celle de l'argument "actual" non differentie.
! (sinon, dummyzerodiffd etait declare de taille "sz"
!  dans TOP, alors que TOP ne connait pas "sz"!)
MODULE TEST
  IMPLICIT NONE

  TYPE TTT
     integer nnn
     real :: fff1
     real :: fff2
     real :: fff3
     real, pointer :: rrpp
  END TYPE TTT

  TYPE TOTTT
     TYPE(TTT), dimension(80) :: bigpassive
     integer mm
  END TYPE TOTTT

CONTAINS

  SUBROUTINE FOO(bigarg, x, sz)
    real :: x
    integer sz
    TYPE(TTT), dimension(sz) :: bigarg
    x = x*x
    bigarg%fff1 = bigarg%fff1*2.0
    bigarg%fff2 = bigarg%fff2*bigarg%fff1
    bigarg(3)%rrpp = 3.0 * bigarg(3)%rrpp
  END SUBROUTINE FOO

  SUBROUTINE TOP(x,y)
    IMPLICIT NONE
    real,target :: x,y
    integer nn
    TYPE(TTT), dimension(90) :: bigactive
    TYPE(TOTTT) :: tobig

! un appel tout actif:
    bigactive%fff1 = x*x
    bigactive%fff2 = x*x
    bigactive(3)%rrpp => x
    call FOO(bigactive, x)
    y = y+x+SUM(bigactive%fff1)+SUM(bigactive%fff2)

! un appel avec le 1er param passif:
    call FOO(tobig%bigpassive(20:nn), x, 51)
    y = y + x
  END SUBROUTINE TOP

END MODULE TEST
