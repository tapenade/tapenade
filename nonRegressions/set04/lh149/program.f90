!Copied from lh80 to test vector adjoint with WHERE.
subroutine icethick(ddx,ddd,adv,hhh,sdx,ddbx,nx,zonemx,res,testv)
INTEGER nx
REAL(KIND=wp),dimension(nx) :: ddx,ddd,adv,hhh,sdx,ddbx
REAL(KIND=wp) :: res,testv
REAL(KIND=wp),dimension(nx) :: Dmin,yyh,Udiff,Ubar,zonemx,uslid
integer :: i

if (testv.lt.0.)then
  where (zonemx(:).gt.0)
     ddx(:)=0.
     ddbx(:)=0.
  end where
end if

Udiff(:)=-(ddx(:)+ddbx(:))*Sdx(:)
Ubar(:)=2.0 * Udiff(:)

DO i = 2, nx
   IF (Ubar(i).GE.0) THEN
      DDD(i) = (ddx(i) + ddbx(i)) * HHH(i-1)
   ELSE
      DDD(i) = (ddx(i) + ddbx(i)) * HHH(i)
   END IF
END DO

if (testv.gt.1.) then

   Adv(:)=Ubar(:)
   DDD(:)=0.

else if ((testv.gt.0.).and.(ABS(testv).le.1.)) then

   yyh = hhh
   do i = 2, nx
      yyh(i) = HHH(i-1)
   end do
   where ((abs(Sdx(:)).gt.1.e-8))
      Adv(:)=(Ubar(:)-Udiff(:))*testv
      Dmin(:)=-(Ubar(:)-Adv(:))/Sdx(:)
      where (Ubar(:).ge.0.)
         DDD(:)=Dmin(:)*yyh(:)
      elsewhere
         DDD(:)=Dmin(:)*HHH(:)
      end where
   elsewhere
      DDD(:)=0.
      Adv(:)=Ubar(:)
   end where

end if

res = DDD(10)*Adv(15)
end subroutine icethick
