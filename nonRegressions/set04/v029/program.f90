      subroutine head(i1,i2,o)
      common /zz/ zn,zd
      double precision i1,i2,o,zn,zd
      call sub0(i1,i2)
      o = zn / zd
      return

      contains

      subroutine sub0(u,v)
      common /zz/ zn,zd
      double precision u,v,z1,z2,zn,zd
      call sub1(u,z1)
      call sub2(v,z2)
      zn = z1 - z2
      zd = 1 + z1 + z2
      return
      end subroutine

      subroutine sub1(x,y)
      double precision x,y
      double precision f,g
      y = f(x) + g(x)
      y = sqrt(y)
      return
      end subroutine

      subroutine sub2(x,y)
      double precision x,y
      double precision f,g
      y = f(x) - g(x)
      y = sqrt(y)
      return
      end subroutine

      double precision function f(t)
      double precision t
      f = t * t
      f = exp(f)
      return
      end function

      double precision function g(t)
      double precision t
      g = 1
      if (t .ne. 0.d0) then
        g = sin(t) / t
      endif
      return
      end function

      end subroutine
