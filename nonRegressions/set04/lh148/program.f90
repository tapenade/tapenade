! A copy of lh16 to test vector ("-multi") adjoint
MODULE module1
  public toto
CONTAINS
  SUBROUTINE toto(a, b, c, d)
    real a, b, c, d
    d = a*b*c
  END SUBROUTINE toto
END MODULE module1

	
MODULE module2
  PUBLIC tata
CONTAINS
  SUBROUTINE tata(x, y, z, t)
    real x, y, z, t
    y = x*x
    t = y*y*z
  END SUBROUTINE tata
END MODULE module2

MODULE module3
  USE module1
  USE module2
  PUBLIC titi
CONTAINS
  SUBROUTINE titi(a, b, c, d, x, y, z, t)
    real a, b, c, d, x, y, z, t
    CALL toto(a, b, c, d)
    CALL tata(x, y, z, t)
  END SUBROUTINE titi
END MODULE module3
   
	
SUBROUTINE tutu(x, y, z, t)
  USE module2
  real x, y, z, t
  call tata(x,y,z,t)
END SUBROUTINE tutu
