! Bug trouve par WANG Yang 31/3/2015:
! En mode tangent, la variable tmpresult de GG_D
!  etait declaree de taille SIZE(mat, 1)
! alors que "mat" n'est pas declare dans GG mais dans MATMUL

module sub_m
  implicit none
contains

  subroutine ff(delta,res)
  use func_m
  implicit none
  real(8),dimension(:)::delta,res 
  delta=unit_vec(delta)
  res=2*delta
  end subroutine
    
  subroutine gg(grad,grad_dx_inv,res_grad)
  use func_m
  implicit none
  real(8),dimension(:,:)::grad, grad_dx_inv,res_grad
  integer::i,n=3
  do i=1,n
     grad(:,i)=matmul(grad_dx_inv(:,:),grad(:,i))
  end do
  res_grad=grad 
  end subroutine

end module
module func_m
implicit none
contains 
function matmul (mat, vec) result (res)
  implicit none
  real(8),dimension(:,:)::mat
  real(8),dimension(:)::vec

  real(8),dimension(size(mat,1))::res
  integer::i,m

  m=size(mat,1)

!$AD II-LOOP
  do i=1,m
    res(i) = dot_product(mat(i,:), vec)
  end do
end function

function vec_mag(v1) result(s)
  implicit none
  real(8),dimension(:) ::v1
  real(8)::s
  s=sqrt(dot_product(v1,v1))
end function

function unit_vec(v1) result(v)
  real(8),dimension(:) ::v1
  real(8),dimension(size(v1))::v
  real(8)::s
  s=vec_mag(v1)
  s=s+1.d-20
  v=v1/s
end function

function dot_product (vec1, vec2) result (res)
  double precision,dimension(:)::vec1
  double precision,dimension(:)::vec2

  double precision::res
  integer::i,m

  m=size(vec1,1)
  res = 0

!$AD II-LOOP
  do i=1,m
    res = res + vec1(i) * vec2(i)
  end do
end function
end module
