module p 
  INTEGER, PARAMETER :: sp=4 !KIND(1.0)
  INTEGER, PARAMETER :: dp=8 !KIND(1.0d0)

end module 


module m 
   use p 
  
  type coordsystem_type
     integer :: dim1, dim2
  end type coordsystem_type


  interface coordsystem_allocate
     module procedure coordsystem_allocate_d, coordsystem_allocate_s, coordsystem_allocate_i, coordsystem_allocate_l, &
          coordsystem_allocate_d2, coordsystem_allocate_s2
  end interface

contains 

  subroutine coordsystem_allocate_d(coord, field)
    !*FD allocate memory to pointer field
    implicit none
    type(coordsystem_type), intent(in) :: coord !*FD coordinate system
    real(kind=dp), dimension(:,:), pointer :: field !*FD unallocated field

    allocate(field(coord%dim1,coord%dim2))
    field = 0.d0
  end subroutine coordsystem_allocate_d
  
  subroutine coordsystem_allocate_s(coord, field)
    !*FD allocate memory to pointer field
    implicit none
    type(coordsystem_type), intent(in) :: coord !*FD coordinate system
    real, dimension(:,:), pointer :: field !*FD unallocated field

    allocate(field(coord%dim1,coord%dim2))
    field = 0.e0
  end subroutine coordsystem_allocate_s

  subroutine coordsystem_allocate_i(coord, field)
    !*FD allocate memory to pointer field
    implicit none
    type(coordsystem_type), intent(in) :: coord !*FD coordinate system
    integer, dimension(:,:), pointer :: field !*FD unallocated field

    allocate(field(coord%dim1,coord%dim2))
    field = 0
  end subroutine coordsystem_allocate_i

  subroutine coordsystem_allocate_l(coord, field)
    !*FD allocate memory to pointer field
    implicit none
    type(coordsystem_type), intent(in) :: coord !*FD coordinate system
    logical, dimension(:,:), pointer :: field !*FD unallocated field

    allocate(field(coord%dim1,coord%dim2))
    field = .FALSE.
  end subroutine coordsystem_allocate_l

  subroutine coordsystem_allocate_d2(coord, nup, field)
    !*FD allocate memory to pointer field
    implicit none
    type(coordsystem_type), intent(in) :: coord !*FD coordinate system
    integer, intent(in) :: nup
    real(kind=dp), dimension(:,:,:), pointer :: field !*FD unallocated field

    allocate(field(nup,coord%dim1,coord%dim2))
    field = 0.d0
  end subroutine coordsystem_allocate_d2

  subroutine coordsystem_allocate_s2(coord, nup, field)
    !*FD allocate memory to pointer field
    implicit none
    type(coordsystem_type), intent(in) :: coord !*FD coordinate system
    integer, intent(in) :: nup
    real, dimension(:,:,:), pointer :: field !*FD unallocated field

    allocate(field(nup,coord%dim1,coord%dim2))
    field = 0.d0
  end subroutine coordsystem_allocate_s2

end module 

module data
   use p 
  type fields
    real(sp), dimension(:,:), pointer :: f1=> NULL()
    real(dp), dimension(:,:,:), pointer :: f2=> NULL()
  end type 
  
  type glob
    type(fields) :: theFields
  end type 

contains 

  subroutine initIt(aGlob) 
    use  m
    type(coordsystem_type) :: theCoord
    type(glob) :: aGlob
    theCoord%dim1=50
    theCoord%dim2=75
    call coordsystem_allocate(theCoord,3,aGlob%theFields%f2)
    call coordsystem_allocate(theCoord,aGlob%theFields%f1)
  end subroutine

  subroutine doIt(aGlob)
    type(glob) :: aGlob
    aGlob%theFields%f2=2*aGlob%theFields%f2
    aGlob%theFields%f1=2*aGlob%theFields%f1
  end subroutine
    
  subroutine finishIt(aGlob)
    type(glob) :: aGlob
     deallocate(aGlob%theFields%f2)
     deallocate(aGlob%theFields%f1)
  end subroutine 
end module 

subroutine foo(x,y)
  use data
  real :: x, y
  type(glob):: glob1, glob2
  call initIt(glob1)
  call initIt(glob2)
  glob1%theFields%f2=x
  call doIt(glob1)
  y=0.0
  y=y+sum(glob1%theFields%f2)
  glob2%theFields%f1=x
  call doIt(glob2)
  y=y+sum(glob2%theFields%f1)
  call finishIt(glob1)
  call finishIt(glob2)
end subroutine

program main

  real :: x,y
  x=3.7
  call foo(x,y) 

  print *,y
end program
  


    
    
