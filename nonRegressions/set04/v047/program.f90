module M
       TYPE INTERVAL
              REAL LOWER, UPPER
              character(256) :: name
       END TYPE INTERVAL

       real, public :: x

end module

module N
      contains
      real function ff(t)
      use M, only : interv => interval , x
      type(interv) :: t
      ff = t%lower + t%upper
      x = ff
      return
      end function
end module
