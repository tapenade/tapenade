!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
MODULE M
  IMPLICIT NONE
  TYPE INTERVAL
      REAL :: lower, upper
      CHARACTER(len=256) :: name
  END TYPE INTERVAL
  REAL, PUBLIC :: x
END MODULE M

MODULE N
  IMPLICIT NONE

CONTAINS
  REAL FUNCTION FF(t)
    USE M, ONLY : interv => interval, x
    IMPLICIT NONE
    TYPE(INTERV) :: t
    ff = t%lower + t%upper
    x = ff
    RETURN
  END FUNCTION FF

END MODULE N

