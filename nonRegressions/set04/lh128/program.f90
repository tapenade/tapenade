! Bug trouve par Bruno MAUGARS (Onera)
! Bug de fusion indesirable entre wb(n)=0.0
!  et wb(n) = wb(n) + ...*alphab
! Probablement mauvais graphe de data-dep du block adjoint.
SUBROUTINE test(w)
  IMPLICIT NONE
  DOUBLE PRECISION alpha, w(0:5)
  INTEGER n

  n=1
  alpha=w(n)**2
  n=0
  w(n)=alpha
END SUBROUTINE test
