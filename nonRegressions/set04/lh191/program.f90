! Bug trouve par Michael Vossbeck, Inversion-Lab
! Melange de declarations COMMON a l'ancienne et de modules (beurk):
! foo() ne voyait pas le common a cause du "only" et le contenu du
! common n'etait pas mis en "Side-Effect" => pas d'activite trouvee!
module fix2free
  implicit none
  public
  integer nx,ny
  parameter(nx=3)
  parameter(ny=1)
  real x_glob(nx), y_glob(ny)
  common /vars/ x_glob,y_glob
end module fix2free

subroutine x2model(n,x)
  use fix2free
  implicit none
  integer, intent(in) :: n
  real, intent(in) :: x(n)
  x_glob(1:nx) = x(1:n)
end subroutine x2model

subroutine model2y(m,y)
  use fix2free
  implicit none
  integer, intent(in) :: m
  real, intent(out) :: y(m)
  y(1:m) = y_glob(1:ny)
end subroutine model2y

subroutine model
  integer nx,ny
  parameter(nx=3)
  parameter(ny=1)
  real x_glob(nx), y_glob(ny)
  common /vars/ x_glob,y_glob
  integer i
  y_glob(1) = 0.
  do i=1,nx
     y_glob(1) = y_glob(1) + sin(x_glob(i))
  enddo
end subroutine model

subroutine foo(n,x,y)
  use fix2free, only:nx,ny
  implicit none
  integer,intent(in) :: n
  real, intent(in) :: x(n)
  real, intent(out) :: y
  real ytmp(1)

  call x2model(n,x)
  call model()
  call model2y(1,ytmp)
  y = ytmp(1)
end subroutine foo
