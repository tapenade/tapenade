! bug trouve par Jonathan Chetboun (Dassault): bug dans
! le mecanisme qui remplce des PUSH/POP par des recalculs.
! L'instruction dycl = half * (ep-epp) n'etait ni recalculee
! ni PUSh/POP'pee pour la variable dycl.
      subroutine TEST(i_cl,n_delta,cp,cpgp,rg,&
                      ht,sr,ppo,vn,p_cl,&
                      ut_cl,vt_cl,wt_cl,d_cl,&
                      d1i,ti,fpue,ep)
      implicit   none
      integer,intent(IN)  :: i_cl,n_delta
      real(8),intent(IN)  :: cp,cpgp,rg,ht,sr,ppo
      real(8),intent(OUT) :: d1i,ti,fpue,ep
!
      real(8),dimension(3),intent(IN) :: vn
      real(8),dimension(i_cl),intent(IN) :: p_cl,ut_cl,vt_cl,wt_cl,d_cl
!
      integer    ip
      real(8)    b11,b12,b13,b1norm
      real(8)    dai,dycl,epp
	  real(8)    fpuep,fptep,fpte
      real(8)    ul
      real(8)    utl,vtl,wtl
      real(8)    pl
      real(8)    ulp
!
      integer    nq
      parameter (nq=10)
      real(8),dimension(nq) :: xinteg
!
      intrinsic  SQRT,MIN,MAX,EXP,LOG
!
      real(8)    zero,half,one,mone,two
      data       zero,half,one,mone,two     /0.d0,0.5d0,1.d0,-1.d0,2.d0/
!
      b11 = ut_cl(n_delta)
      b12 = vt_cl(n_delta)
      b13 = wt_cl(n_delta) 
      b1norm = SQRT(b11**2 + b12**2 + b13**2)
      b11 = b11 / b1norm  
      b12 = b12 / b1norm
      b13 = b13 / b1norm
      b11 = MAX(mone,MIN(one,b11)) 
      b12 = MAX(mone,MIN(one,b12))
      b13 = MAX(mone,MIN(one,b13))
!
      ep  = zero
      epp = zero
      ulp = zero
      fptep = EXP ((sr + rg * LOG (ppo)) / cp)
      fpuep = two * (ht - cp * fptep)
      if (fpuep > 1.d-10) then
        fpuep = SQRT (fpuep)
      else
        fpuep = 1.d-5
      endif
!
      xinteg(1:nq) = zero
      do ip=1, n_delta
        utl  = ut_cl(ip)
        vtl  = vt_cl(ip)
        wtl  = wt_cl(ip)
        pl   = p_cl(ip)
        ep   = d_cl(ip)
        dycl = half * (ep-epp)
        ul   = b11*utl + b12*vtl + b13*wtl
        fpte = EXP ((sr + rg*LOG(pl))/cpgp)
        fpue = two * (ht - cpgp * fpte)
        if (fpue > 1.d-10) then
          fpue = SQRT (fpue)
        else
          fpue = 1.d0
        endif
        dai       = dycl * (ul/fpue + ulp/fpuep)
        xinteg(9) = xinteg(9) + dai
        dai       = dycl * ((ul/fpue)**2 + (ulp/fpuep)**2)
        xinteg(10)= xinteg(10) + dai
        epp       = ep
        ulp       = ul
        fpuep     = fpue
      enddo
!
      d1i = ep - xinteg(9)
      ti  = xinteg(9) - xinteg(10)
!
      return
      end subroutine TEST
