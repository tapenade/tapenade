! Encore un exemple de Jean sur les allocations et les contextes.
module mt
  type t 
    real, allocatable, dimension(:):: v
  end type 
end module

module model_data
  use mt
  type(t), save :: tDataIn, tDataOut
  integer, save :: dim

  contains 
    subroutine setupData(someTData)
      type(t) :: someTData
      if (allocated(someTData%v)) deallocate(someTData%v)
      allocate(someTData%v(dim))
    end subroutine
end module
! f90
module ad_data
  real,dimension(:), allocatable :: inputs
  real,dimension(:), allocatable :: outputs
contains
  subroutine ad_data_setup(I,O) 
  real,dimension(:) :: I
  real,dimension(:) :: O
  if (allocated(inputs)) deallocate(inputs)
  allocate(inputs(size(I,1)))
  if (allocated(outputs)) deallocate(outputs)
  allocate(outputs(size(O,1)))
  end subroutine
end module

program driver
  use model_data
  use ad_data
  dim=3
  call setup()
  tDataIn%v=3.0
  call head()
  print *, tDataout%v
  call cleanallocs()
end program 

subroutine handleADinputs()
  use ad_data
  ! do nothing or set some perturbation for finite difference here
end subroutine 

subroutine handleADoutputs()
  use ad_data
 ! do nothing or get perturbed output here
end subroutine 

subroutine setup ()
  use model_data
  use ad_data
  call setupData(tDataIn)
  call setupData(tDataOut)
  call ad_data_setup(tDataIn%v,tDataOut%v)
end subroutine

subroutine cleanallocs()
  use model_data
  use ad_data
  if (allocated(tDataIn%v)) deallocate(tDataIn%v)
  if (allocated(tDataOut%v)) deallocate(tDataOut%v)
  if (allocated(inputs)) deallocate(inputs)
  if (allocated(outputs)) deallocate(outputs)
end subroutine cleanallocs

subroutine head()
  use model_data
  use ad_data
  call handleADinputs()
  tDataIn%v=tDataIn%v+inputs
  tDataOut%v=tDataIn%v*2
  outputs=tDataOut%v
  call handleADoutputs()
end subroutine

subroutine init()
  call setup()
end subroutine
