!Encore un exemple pour les questions de contexte d'appel
! du code differentie, les _D et les _CD.
! Pour verifier les appelants du diff-root.

program toptop
  REAL, DIMENSION(8) :: A1
  REAL, ALLOCATABLE, DIMENSION(:) :: A2
  REAL A3
  ALLOCATE(A2(17))
  A1 = 1.0
  A2 = 2.0
  A3 = 3.0
  call sub1(A1,A2,A3)
  print *,A1,A2,A3
  DEALLOCATE(A2)
end program toptop

subroutine sub1(X1,X2,X3)
  REAL X1(:),X2(:),X3,X4
  X4 = 4.0
  X3 = X3+SUM(X1)+SUM(X2)+X4
  call sub2(X3)
end subroutine sub1

subroutine sub2(Y0)
  REAL Y0
  REAL Y1
  REAL, ALLOCATABLE, DIMENSION(:) :: Y2
  ALLOCATE(Y2(8))
  Y1 = 1.1
  Y2 = 2.2
  call sub3(Y1,Y2)
  Y0 = SUM(Y2)
  DEALLOCATE(Y2)
end subroutine sub2

subroutine sub3(Z1,Z2)
  REAL Z1,Z2(:)
  REAL Z4
  REAL, ALLOCATABLE, DIMENSION(:) :: Z3
  ALLOCATE(Z3(8))
  Z4 = 4.5
  Z3 = 3.7
  call rootdiff(Z1,Z2,Z3,Z4)
  Z2 = Z2/2
  print *,Z3,Z4
  DEALLOCATE(Z3)
end subroutine sub3

subroutine rootdiff(V1,V2,V3,V4)
  REAL V1,V2(:),V3(:),V4
  V2 = V2*V3
  call deep(V1,V2,V3)
  V1 = 0.5
  V3 = V3*V2 + V1
  V4 = SUM(V3)
end subroutine rootdiff

subroutine deep(W1,W2,W3)
  REAL W1,W2(:),W3(:)
  W2 = W2+W3
  W1 = SUM(W2)
end subroutine deep
