MODULE mod
  TYPE tpf
     real, dimension(:), pointer :: fld
  END TYPE tpf
  type(tpf) :: flw
END MODULE mod


SUBROUTINE f2 (i)
  USE mod, ONLY : renamedflw => flw
  INTEGER i
  i = 1
END SUBROUTINE f2


! Bug found by JD Mueller & Sami Bayyuk on code ACE+ ESI:
! Wrong creation of the side-effect type zones for pointed zones
SUBROUTINE top(i)
  INTEGER i
  CALL f2(i)
END SUBROUTINE top
