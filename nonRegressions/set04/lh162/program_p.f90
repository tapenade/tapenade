!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
MODULE MOD
  IMPLICIT NONE
  TYPE TPF
      REAL, DIMENSION(:), POINTER :: fld
  END TYPE TPF
  TYPE(TPF) :: flw
END MODULE MOD

! Bug found by JD Mueller & Sami Bayyuk on code ACE+ ESI:
! Wrong creation of the side-effect type zones for pointed zones
SUBROUTINE TOP(i)
  IMPLICIT NONE
  INTEGER :: i
  CALL F2(i)
END SUBROUTINE TOP

SUBROUTINE F2(i)
  USE MOD, ONLY : renamedflw => flw
  IMPLICIT NONE
  INTEGER :: i
  i = 1
END SUBROUTINE F2

