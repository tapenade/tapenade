! Bug "U T" d'avril 2013.
! confusions COMPLEX <-> REAL
      SUBROUTINE MUL(A,B,C)
      IMPLICIT NONE
      REAL  :: B
      INTEGER :: I
      COMPLEX :: A,C,D
      DO I =1,10
      D= CMPLX(0.1*I,1.0*I)
      C=B*D+A
      ENDDO
      END SUBROUTINE MUL
