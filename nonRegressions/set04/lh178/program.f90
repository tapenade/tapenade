! Problem found by Jan Hueckelheim anout AD08 messages (need init of hidden vars)
! It seems that omega[xyz] were not declared as side-effect in flow_and_cost.
! Also illustrates unsolved problem about array size max_rot not
!  imported because of the ONLY clause.
! Temptative fix exists in RefDescriptor.java, but
!  was commented out because unclean (grep "revoked").
MODULE flow_mod
  INTEGER, PARAMETER :: max_rot = 100
  REAL(8),   DIMENSION(max_rot) :: omegax, omegay, omegaz
 END MODULE flow_mod

 SUBROUTINE flow_and_cost( inlet_velocity, cost_function_value, sometest )
   USE flow_mod, ONLY : max_rot
   IMPLICIT NONE
   logical :: sometest
   REAL(8) :: inlet_velocity, cost_function_value
   real(8) :: sc, ap, u
   IF (sometest) CALL sol_uvw(sc, ap, u)
   cost_function_value = u
 END SUBROUTINE flow_and_cost

 SUBROUTINE sol_uvw(sc, ap, u)
   IMPLICIT NONE
  real(8) :: sc, ap, u
  CALL reserr(sc,u)
  CALL relx_uvw(ap)
  sc = sc + ap
 END SUBROUTINE sol_uvw

subroutine reserr(sc,u)
   USE flow_mod, ONLY : omegax,omegay,omegaz
   IMPLICIT NONE
  real(8) :: sc, u
  sc = u+sc+omegax(1)+omegay(1)+omegaz(1)
  u = sc
end subroutine reserr

SUBROUTINE relx_uvw(ap)
   USE flow_mod, ONLY : omegax,omegay,omegaz
   IMPLICIT NONE
   REAL(8) :: ap
   ap= ap - omegax(1)*omegax(1)+omegay(1)*omegay(1) &
       +omegaz(1)*omegaz(1)
END SUBROUTINE relx_uvw
