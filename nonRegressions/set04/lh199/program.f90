! Bug report Stefano Carli about wrong zero-initializations
!  of derivative arrays, in vector mode, when these arrays
!  have just been declared or allocated.
      module some_module
      implicit none

      real, allocatable, save :: ne(:,:), te(:,:)

      contains

      subroutine alloc_some_module(nxd,nyd,nsd)
      implicit none
      integer nxd,nyd,nsd

      allocate(ne(-1:nxd,-1:nyd))
      allocate(te(-1:nxd,-1:nyd))
      ne=0.0
      te=0.0
      
      return
      end subroutine alloc_some_module

      subroutine dealloc_some_module
      implicit none
      deallocate(ne)
      deallocate(te)
      return
      end subroutine dealloc_some_module

      subroutine read_some_module
      implicit none

      call read_file (size(ne), ne, 'ne')
      call read_file (size(te), te, 'te')

      return
      end subroutine read_some_module

      subroutine write_some_module
      implicit none

      call write_file (size(ne), ne, 'ne')
      call write_file (size(te), te, 'te')

      return
      end subroutine write_some_module

      end module some_module
      subroutine main_calc(J1,J2,nx,ny,ns)
      use some_module
      implicit none
      real :: J1,J2
      integer :: nx,ny,ns

      integer ix,iy
!     ... do some calculations

      do ix=0,nx-1
        do iy=0,ny-1
          ne(ix,iy)=ne(ix,iy)*sin(te(ix,iy))
        end do
      end do
      call cost_function(nx,ny,ns,J1,J2)


      end subroutine main_calc
      subroutine cost_function(nx,ny,ns,J1,J2)
      use some_module

      implicit none
      integer :: nx,ny,ns,ix,iy
      real :: J1,J2,tmax
      tmax=-1.0
      do ix=0,nx-1
        do iy=0,ny-1
          if (te(ix,iy).ge.tmax) then
            tmax=te(ix,iy)
          endif
        end do
      end do
      J1=tmax
      tmax=-1.0
      do ix=0,nx-1
        do iy=0,ny-1
          if (ne(ix,iy).ge.tmax) then
            tmax=ne(ix,iy)
          endif
        end do
      end do
      J2=tmax
 
      end subroutine cost_function





      program myprogram
      use some_module
      implicit none
      real :: J1,J2
      integer :: nx,ny,ns


      call get_dimensions(nx,ny,ns)

      call alloc_some_module(nx,ny,ns)

      call read_some_module

      call main_calc(J1,J2,nx,ny,ns)

      call write_some_module

      call dealloc_some_module

      end program myprogram

