!Bug message (AD08) Differentiated procedure of
!    top needs to initialize to 0 a hidden
!    variable *#td#[1:10]
! => il n'y a aucune raison d'envoyer ce message
!  puisque TD est de-alloue et donc pas necessaire
!  (et meme faux) de le remettre a zero a la fin.
   module m1
     real::C
   contains
     subroutine P1(A,B)
       real :: A,B
       real, allocatable, dimension (:) :: T
       allocate(T(10))
       T = A
       B = B + SUM(T)
       deallocate(T)
       C = 1.5
     end subroutine P1
   end module m1

   subroutine top(A,B)
     use M1
     real :: A,B
     call P1(A,B)
   end subroutine top
