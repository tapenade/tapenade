!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) - 24 Feb 2022 12:13
!
!Bug message (AD08) Differentiated procedure of
!    top needs to initialize to 0 a hidden
!    variable *#td#[1:10]
! => il n'y a aucune raison d'envoyer ce message
!  puisque TD est de-alloue et donc pas necessaire
!  (et meme faux) de le remettre a zero a la fin.
MODULE M1_DIFF
  IMPLICIT NONE
  REAL :: c
  REAL :: cd

CONTAINS
!  Differentiation of p1 in forward (tangent) mode:
!   variations   of useful results: [alloc*t in p1] b
!   with respect to varying inputs: [alloc*t in p1] a b
  SUBROUTINE P1_D(a, ad, b, bd)
    IMPLICIT NONE
    REAL :: a, b
    REAL :: ad, bd
    REAL, DIMENSION(:), ALLOCATABLE :: t
    REAL, DIMENSION(:), ALLOCATABLE :: td
    INTRINSIC SUM
    ALLOCATE(td(10))
    td = 0.0
    ALLOCATE(t(10))
    td = ad
    t = a
    bd = bd + SUM(td)
    b = b + SUM(t)
    IF (ALLOCATED(td)) THEN
      DEALLOCATE(td)
    END IF
    DEALLOCATE(t)
    c = 1.5
  END SUBROUTINE P1_D

  SUBROUTINE P1(a, b)
    IMPLICIT NONE
    REAL :: a, b
    REAL, DIMENSION(:), ALLOCATABLE :: t
    INTRINSIC SUM
    ALLOCATE(t(10))
    t = a
    b = b + SUM(t)
    DEALLOCATE(t)
    c = 1.5
  END SUBROUTINE P1

END MODULE M1_DIFF

!  Differentiation of top in forward (tangent) mode:
!   variations   of useful results: [alloc*t in p1] c b
!   with respect to varying inputs: [alloc*t in p1] a b
!   RW status of diff variables: [alloc*t in p1]:in-out c:zero
!                a:in b:in-out
SUBROUTINE TOP_D(a, ad, b, bd)
  USE M1_DIFF
  IMPLICIT NONE
  REAL :: a, b
  REAL :: ad, bd
  CALL P1_D(a, ad, b, bd)
  cd = 0.0
END SUBROUTINE TOP_D

SUBROUTINE TOP_NODIFF(a, b)
  USE M1_DIFF
  IMPLICIT NONE
  REAL :: a, b
  CALL P1(a, b)
END SUBROUTINE TOP_NODIFF

