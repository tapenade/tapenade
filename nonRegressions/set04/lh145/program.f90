! Test derive de lha22 pour tester l'option "-oldContext"
module MMA
  REAL, ALLOCATABLE, DIMENSION(:) :: AA
contains
  subroutine initA()
    ALLOCATE(AA(20))
  end subroutine initA
end module MMA

module MMB
  INTEGER :: flag
  REAL :: zz
end module MMB

program MAIN
  use MMA
  use MMB
  REAL, ALLOCATABLE, DIMENSION(:) :: X
  INTEGER i
  REAL cva(2)
  common /cc1/cva
  call getflag(flag)
  ALLOCATE(X(30))
  call initA()
  do i = 1,20
     AA(i) = 100.0/i
     X(i) = 3.3*i
  enddo
  cva(1) = 1.8
  cva(2) = 8.1
  call ROOT(X)
  print*,'zz=',zz
  DEALLOCATE(AA)
  DEALLOCATE(X)
end program MAIN

subroutine ROOT(X)
  use MMA
  use MMB
  REAL, DIMENSION(*) :: X
  REAL :: V
  REAL :: cv1,cv2
  common /cc1/cv1,cv2

  zz = cv1*cv2
  v = SUM(AA)
  if (flag>2) then
     zz = zz*v
  endif
  zz = zz + SUM(X)
end subroutine ROOT
     
  
  
  

  
