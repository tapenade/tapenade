! Modifie depuis v437 pour tester le changement de nom en _CD de
! la declaration d'interface dans une autre fonction copiee _CD.

module M_PRECISION
   implicit none
   integer, parameter :: DOUBLE = SELECTED_REAL_KIND(15,100)
end module M_PRECISION

subroutine foo(x)
  interface
     function FCONJ(tirant)
       use M_PRECISION
       implicit none
       real(DOUBLE)                 :: FCONJ
       real(DOUBLE),  intent(in)    :: tirant
     end function FCONJ
  end interface
  real x
  real y

  y = 5.0
  y = fconj(y) ;
  x = x*x*y
end subroutine foo


function FCONJ(tirant)
   use M_PRECISION
   implicit none
   real(DOUBLE)               :: FCONJ, local
   real(DOUBLE),intent(in)    :: tirant

   local = 2.0
   FCONJ = tirant*tirant + local
 end function FCONJ
