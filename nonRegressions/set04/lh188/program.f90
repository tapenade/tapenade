! Bug Kotas/ORNL/Rex
! Il faut proteger les debug_xxx[_passive]_real8array du cas ou
!  le tableau teste est allocatable et non alloue!
! Il y a peut-etre aussi un probleme avec le array notation
!  sur le debug de ent_line(:)%plungediff,
!  meme si je croyais que la norme F90 l'autorise?

MODULE MM
  TYPE TENTLINE
     INTEGER :: nnodes
     REAL*8, DIMENSION(3, ent_line_max_nodes) :: vert
     REAL*8 :: plungediff
  END TYPE TENTLINE
END MODULE MM

SUBROUTINE TEST(x,y,ent_line)
  USE MM
  REAL x,y
  TYPE(TENTLINE), DIMENSION(:), ALLOCATABLE :: ent_line
  y = x*x
END SUBROUTINE TEST
