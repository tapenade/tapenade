! Un exemple pour tester l'option -copyname "",
!  qui permet de conserver les noms d'origine au lieu
!  de les appeler FOO_CD() ou FOO_CB()
module m 
 implicit none
 integer :: i=0 
 real :: r=1.0
end module 

subroutine foo(x,y) 
  use m 
  implicit none
  real :: x,y
  r=x
  y=x/i
end subroutine 

subroutine faa(x,y)
  implicit none
  real :: x,y
  external foo
  call foo(x,y)
end subroutine 

subroutine bar(x,y)
  use m
  implicit none
  real :: x,y
  external faa
  i=1   ! <---critical piece of initialization
  call faa(x,y)
end subroutine 

subroutine head(x,y)
 implicit none
 real :: x,y
 external foo
 call foo(x,y)
end subroutine head

program p 
  implicit none
  real :: x,y
  external bar
  external head
  x=2.0
  call bar(x,y)
  call head(x,y)
end program
