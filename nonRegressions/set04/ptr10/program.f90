MODULE pointersandrecords

  TYPE ch2
     real*8, pointer :: pf1
     TYPE(ch2), pointer :: next
     TYPE(ch1), pointer :: up
  END TYPE ch2

  TYPE ch1
     real*8 f1
     integer f2
     TYPE(ch2), pointer :: inside
  END TYPE ch1

CONTAINS

  SUBROUTINE top5(a,b,c,d)
    real*8, target :: a,b,c,d
    TYPE(ch1) :: xx1

    call create(xx1, 3)
    a = 4.2*b
    call fill(xx1,4,a,b)
    call work(xx1,c)
    call extract(xx1%inside,3,d)
  END SUBROUTINE top5

  SUBROUTINE create(xx1,n)
    TYPE(ch1),TARGET :: xx1
    integer :: n
    TYPE(ch2),POINTER :: inside
    INTEGER i

    ALLOCATE(xx1%inside)
    inside => xx1%inside
    do i=1,n
       ALLOCATE(inside%next)
       inside%up => xx1
       inside => inside%next
    enddo
  END SUBROUTINE create

  SUBROUTINE fill(xx1,n,a,b)
    TYPE(ch1) :: xx1
    integer :: n
    real*8,TARGET :: a,b
    TYPE(ch2),POINTER :: inside
    INTEGER i

    xx1%f1 = b
    xx1%f2 = n
    inside => xx1%inside
    do i=1,n
       inside%pf1 => a
       inside%pf1 = a * a
       inside => inside%next
    enddo
  END SUBROUTINE fill

  SUBROUTINE work(xx1,c)
    TYPE(ch1) :: xx1
    TYPE(ch2),pointer :: inside
    REAL*8 c,k
    INTEGER i

    k = xx1%f1
    inside => xx1%inside
    do i=1,4
       inside%pf1 = inside%pf1/(c*k)
       inside => inside%next
    enddo
  END SUBROUTINE work

  RECURSIVE SUBROUTINE extract(xx2,n,r)
    TYPE(ch2),pointer :: xx2
    integer n
    real*8 r

    if (n.eq.0) then
       r = xx2%pf1
    else
       call extract(xx2%next,n-1,r)
    endif
  END SUBROUTINE extract

END MODULE pointersandrecords
