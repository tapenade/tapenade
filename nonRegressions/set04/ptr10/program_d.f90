!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 14:31
!
MODULE POINTERSANDRECORDS_D
  IMPLICIT NONE
  TYPE CH2
      REAL*8, POINTER :: pf1
      TYPE(CH2), POINTER :: next
      TYPE(CH1), POINTER :: up
  END TYPE CH2
  TYPE CH2_D
      REAL*8, POINTER :: pf1
      TYPE(CH2_D), POINTER :: next
  END TYPE CH2_D
  TYPE CH1
      REAL*8 :: f1
      INTEGER :: f2
      TYPE(CH2), POINTER :: inside
  END TYPE CH1
  TYPE CH1_D
      REAL*8 :: f1
      TYPE(CH2_D), POINTER :: inside
  END TYPE CH1_D

CONTAINS
!  Differentiation of top5 in forward (tangent) mode:
!   variations   of useful results: *([alloc*(*inside.next) in create].pf1)
!                *([alloc*(xx1.inside) in create].pf1) d a
!   with respect to varying inputs: *([alloc*(*inside.next) in create].pf1)
!                *([alloc*(xx1.inside) in create].pf1) b c
!   RW status of diff variables: *([alloc*(*inside.next) in create].pf1):in-out
!                *([alloc*(xx1.inside) in create].pf1):in-out d:out
!                a:out b:in c:in
!   Plus diff mem management of: [alloc*(*inside.next) in create].pf1:in-out
!                [alloc*(*inside.next) in create].next:in-out [alloc*(xx1.inside) in create].pf1:in-out
!                [alloc*(xx1.inside) in create].next:in-out
  SUBROUTINE TOP5_D(a, ad, b, bd, c, cd, d, dd)
    IMPLICIT NONE
    REAL*8, TARGET :: a, b, c, d
    REAL*8, TARGET :: ad, bd, cd, dd
    TYPE(CH1) :: xx1
    TYPE(CH1_D) :: xx1d
    CALL CREATE_D(xx1, xx1d, 3)
    ad = 4.2*bd
    a = 4.2*b
    CALL FILL_D(xx1, xx1d, 4, a, ad, b, bd)
    CALL WORK_D(xx1, xx1d, c, cd)
    CALL EXTRACT_D(xx1%inside, xx1d%inside, 3, d, dd)
  END SUBROUTINE TOP5_D

  SUBROUTINE TOP5(a, b, c, d)
    IMPLICIT NONE
    REAL*8, TARGET :: a, b, c, d
    TYPE(CH1) :: xx1
    CALL CREATE(xx1, 3)
    a = 4.2*b
    CALL FILL(xx1, 4, a, b)
    CALL WORK(xx1, c)
    CALL EXTRACT(xx1%inside, 3, d)
  END SUBROUTINE TOP5

!  Differentiation of create in forward (tangent) mode:
!   Plus diff mem management of: [alloc*(*inside.next) in create].next:in-out
!                [alloc*(xx1.inside) in create].next:in-out xx1.inside:in-out
  SUBROUTINE CREATE_D(xx1, xx1d, n)
    IMPLICIT NONE
    TYPE(CH1), TARGET :: xx1
    TYPE(CH1_D), TARGET :: xx1d
    INTEGER :: n
    TYPE(CH2), POINTER :: inside
    TYPE(CH2_D), POINTER :: insided
    INTEGER :: i
    ALLOCATE(xx1d%inside)
    ALLOCATE(xx1%inside)
    insided => xx1d%inside
    inside => xx1%inside
    DO i=1,n
      ALLOCATE(insided%next)
      ALLOCATE(inside%next)
      inside%up => xx1
      insided => insided%next
      inside => inside%next
    END DO
  END SUBROUTINE CREATE_D

  SUBROUTINE CREATE(xx1, n)
    IMPLICIT NONE
    TYPE(CH1), TARGET :: xx1
    INTEGER :: n
    TYPE(CH2), POINTER :: inside
    INTEGER :: i
    ALLOCATE(xx1%inside)
    inside => xx1%inside
    DO i=1,n
      ALLOCATE(inside%next)
      inside%up => xx1
      inside => inside%next
    END DO
  END SUBROUTINE CREATE

!  Differentiation of fill in forward (tangent) mode:
!   variations   of useful results: *([alloc*(xx1.inside) in create].pf1)
!                xx1.f1 a
!   with respect to varying inputs: *([alloc*(xx1.inside) in create].pf1)
!                a b
!   Plus diff mem management of: [alloc*(xx1.inside) in create].pf1:in-out
!                [alloc*(xx1.inside) in create].next:in xx1.inside:in
  SUBROUTINE FILL_D(xx1, xx1d, n, a, ad, b, bd)
    IMPLICIT NONE
    TYPE(CH1) :: xx1
    TYPE(CH1_D) :: xx1d
    INTEGER :: n
    REAL*8, TARGET :: a, b
    REAL*8, TARGET :: ad, bd
    TYPE(CH2), POINTER :: inside
    TYPE(CH2_D), POINTER :: insided
    INTEGER :: i
    xx1d%f1 = bd
    xx1%f1 = b
    xx1%f2 = n
    insided => xx1d%inside
    inside => xx1%inside
    DO i=1,n
      insided%pf1 => ad
      inside%pf1 => a
      insided%pf1 = 2*a*ad
      inside%pf1 = a*a
      insided => insided%next
      inside => inside%next
    END DO
  END SUBROUTINE FILL_D

  SUBROUTINE FILL(xx1, n, a, b)
    IMPLICIT NONE
    TYPE(CH1) :: xx1
    INTEGER :: n
    REAL*8, TARGET :: a, b
    TYPE(CH2), POINTER :: inside
    INTEGER :: i
    xx1%f1 = b
    xx1%f2 = n
    inside => xx1%inside
    DO i=1,n
      inside%pf1 => a
      inside%pf1 = a*a
      inside => inside%next
    END DO
  END SUBROUTINE FILL

!  Differentiation of work in forward (tangent) mode:
!   variations   of useful results: *([alloc*(xx1.inside) in create].pf1)
!   with respect to varying inputs: *([alloc*(xx1.inside) in create].pf1)
!                xx1.f1 c
!   Plus diff mem management of: [alloc*(xx1.inside) in create].pf1:in
!                [alloc*(xx1.inside) in create].next:in xx1.inside:in
  SUBROUTINE WORK_D(xx1, xx1d, c, cd)
    IMPLICIT NONE
    TYPE(CH1) :: xx1
    TYPE(CH1_D) :: xx1d
    TYPE(CH2), POINTER :: inside
    TYPE(CH2_D), POINTER :: insided
    REAL*8 :: c, k
    REAL*8 :: cd, kd
    INTEGER :: i
    REAL*8 :: temp
    kd = xx1d%f1
    k = xx1%f1
    insided => xx1d%inside
    inside => xx1%inside
    DO i=1,4
      temp = inside%pf1/(c*k)
      insided%pf1 = (insided%pf1-temp*(k*cd+c*kd))/(c*k)
      inside%pf1 = temp
      insided => insided%next
      inside => inside%next
    END DO
  END SUBROUTINE WORK_D

  SUBROUTINE WORK(xx1, c)
    IMPLICIT NONE
    TYPE(CH1) :: xx1
    TYPE(CH2), POINTER :: inside
    REAL*8 :: c, k
    INTEGER :: i
    k = xx1%f1
    inside => xx1%inside
    DO i=1,4
      inside%pf1 = inside%pf1/(c*k)
      inside => inside%next
    END DO
  END SUBROUTINE WORK

!  Differentiation of extract in forward (tangent) mode:
!   variations   of useful results: r
!   with respect to varying inputs: *([alloc*(*inside.next) in create].pf1)
!                *([alloc*(xx1.inside) in create].pf1)
!   Plus diff mem management of: [alloc*(*inside.next) in create].pf1:in
!                [alloc*(*inside.next) in create].next:in [alloc*(xx1.inside) in create].pf1:in
!                [alloc*(xx1.inside) in create].next:in xx2:in
  RECURSIVE SUBROUTINE EXTRACT_D(xx2, xx2d, n, r, rd)
    IMPLICIT NONE
    TYPE(CH2), POINTER :: xx2
    TYPE(CH2_D), POINTER :: xx2d
    INTEGER :: n
    REAL*8 :: r
    REAL*8 :: rd
    IF (n .EQ. 0) THEN
      rd = xx2d%pf1
      r = xx2%pf1
    ELSE
      CALL EXTRACT_D(xx2%next, xx2d%next, n - 1, r, rd)
    END IF
  END SUBROUTINE EXTRACT_D

  RECURSIVE SUBROUTINE EXTRACT(xx2, n, r)
    IMPLICIT NONE
    TYPE(CH2), POINTER :: xx2
    INTEGER :: n
    REAL*8 :: r
    IF (n .EQ. 0) THEN
      r = xx2%pf1
    ELSE
      CALL EXTRACT(xx2%next, n - 1, r)
    END IF
  END SUBROUTINE EXTRACT

END MODULE POINTERSANDRECORDS_D

