!Encore un exemple pour les questions de contexte d'appel
! du code differentie, les _D et les _CD, pour les
! problemes poses par Jean. Cette fois-ci avec des
! records et des cycles de pointeurs.
module MMA
  TYPE :: CELLULE
     REAL :: X=1.1, Y=2.2
     TYPE(CELLULE), POINTER :: next3, prev2, dum0
     TYPE(CELLULE), POINTER :: other => null()
  END TYPE CELLULE

  TYPE(CELLULE), TARGET :: handle

  TYPE(CELLULE),POINTER :: cc1,cc2,cc3

contains

  subroutine alloc6(pc1,pc2,pc3)
    TYPE(CELLULE),POINTER :: pc1,pc2,pc3
    ALLOCATE(pc1)
    ALLOCATE(pc2)
    ALLOCATE(pc3)
    ALLOCATE(cc1)
    ALLOCATE(cc2)
    ALLOCATE(cc3)
  end subroutine alloc6

  subroutine link(pc1,pc2,pc3)
    TYPE(CELLULE),POINTER :: pc1,pc2,pc3
    handle%next3 => pc3
    handle%prev2 => pc2
    pc2%other => cc2
    handle%other => cc1
    handle%next3%prev2 => cc3
    pc2%next3 => cc3
    cc1%next3 => cc3
    cc1%prev2 => cc2
    cc3%other => handle
    cc3%next3 => cc2
    cc3%prev2 => cc1
  end subroutine link

  subroutine dealloc3(pc1,pc2,pc3)
    TYPE(CELLULE),POINTER :: pc1,pc2,pc3
    DEALLOCATE(pc1)
    DEALLOCATE(pc2)
    DEALLOCATE(pc3)
  end subroutine dealloc3

end module MMA

subroutine compute(a,b)
  use MMA
  REAL :: a,b
  TYPE(CELLULE), POINTER :: c1,c2,c3
  c1 => handle%next3%prev2
  c2 => handle%other
  c3 => c1%next3
  c1%x = a
  c2%x = c3%x * c1%x + b
  c3%x = b
  c2%next3%x = c3%x * c2%prev2%x
  b = c1%x * c1%prev2%x + c2%x * c2%next3%x
end subroutine compute

program MAIN
  use MMA
  TYPE(CELLULE),POINTER :: xc1,xc2,xc3
  REAL :: U,V
  call alloc6(xc1, xc2, xc3)
  call link(xc2, xc3, xc1)
  U = 1.72
  V = 0.91
  call compute(U,V)
  print*, U, V
  call dealloc3(cc1,cc2,cc3)
end program MAIN
