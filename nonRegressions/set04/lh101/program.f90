! Bug trouve par Jean Utke:
! le "use m, only:foo" doit etre
! differentie en "use M_D, only:foo_cd"
module m0
  real :: i = 0 
end module 
module m 
 contains
   subroutine foo(name,val)
    use m0
    character(len=*) :: name
    real val
    val=i*x
   end subroutine
end module 

subroutine bar()
 use m, only : foo 
 real x
 call foo('blah',x)
end subroutine

program p 
  call bar()
end program
