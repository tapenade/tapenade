module mymod
  public :: sub1
contains
  subroutine sub1 ()
    call sub2 ()
  contains
    subroutine sub2 ()
    end subroutine sub2
  end subroutine sub1
end module mymod
