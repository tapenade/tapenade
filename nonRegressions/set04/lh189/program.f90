! Repris de lh65,
!  cette fois AVEC les nocheckpoint necessaires.
! Bug fourni par Dominic Jones. Problemes avec des
! allocations via sous-programmes.

module sub
type::var_t
  real,dimension(:),pointer::xx
end type
type(var_t),dimension(:),pointer::var2, var3

contains

subroutine sol_scalar()
  real,dimension(:),pointer::x1p,x2p
  real,dimension(:),pointer::y1p,y2p
  real,dimension(:),pointer::z1p,z2p
!==== 3 EQUIVALENT ? APPROACHES: =============!
! In all 3 approaches, there should be 
! activity and differentiation of the
! underlying global variables, i.e.
! resp. (1):/ccc/cx (2):var2()%xx (3):var3()%xx
   x1p=>ptr1(1); x2p=>ptr1(2)         ! -- (1)
   y1p=>ptr2(1); y2p=>ptr2(2)         ! -- (2)
   call ptr3(1,z1p); call ptr3(2,z2p) ! -- (3)
!=============================================!
  do i=2,size(x1p)
    x2p(i) = i*x1p(i)**2*x2p(i)
    y2p(i) = i*y1p(i)**2*y2p(i)
    z2p(i) = i*z1p(i)**2*z2p(i)
  enddo
end subroutine

function ptr1(i)
  real,dimension(:),pointer::ptr1
  real,dimension(2,5),target::cx
  common /ccc/ cx
  ptr1=>cx(i,:)
end function

function ptr2(i)
  real,dimension(:),pointer::ptr2
  ptr2=>var2(i)%xx
end function

subroutine ptr3(i,ptr)
  real,dimension(:),pointer::ptr
  ptr=>var3(i)%xx
end subroutine
end module


program main
  use sub

  real,dimension(2,5),target::cx
  common /ccc/ cx
  cx(1,:) = 1.0
  cx(2,:) = 2.0

  allocate(var2(2))
  allocate(var2(1)%xx(5))
  allocate(var2(2)%xx(5))
  var2(1)%xx = 1.0
  var2(2)%xx = 2.0

  allocate(var3(2))
  allocate(var3(1)%xx(5))
  allocate(var3(2)%xx(5))
  var3(1)%xx = 1.0
  var3(2)%xx = 2.0

  call sol_scalar()
  print*,sum(cx(2,:)),sum(var2(2)%xx),sum(var3(2)%xx)
end program
