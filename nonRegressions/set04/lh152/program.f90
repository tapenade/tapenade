! Bug trouve par Jonathan Guerrette (U Colorado Boulder)
! avec Tapenade 3.6 (r4756)
! Apparemment mauvais diff-liveness a travers 10.**X 
SUBROUTINE SATVAP(temp2, eval)
      implicit none
      real, intent(in) :: temp2
      real, intent(out) :: eval
      real :: temp, toot, toto, eilog, tsot,  &
     &        ewlog, ewlog2, ewlog3, ewlog4
      temp = temp2-273.155
      if (temp.lt.-20.) then   !!!! ice saturation
        toot = 273.16 / temp2
        toto = 1 / toot
        eilog = -9.09718 * (toot - 1) - 3.56654 * (log(toot) / &
     &    log(10.)) + .876793 * (1 - toto) + (log(6.1071) / log(10.))
        eval = 10. ** eilog
      else
        tsot = 373.16 / temp2
        ewlog = -7.90298 * (tsot - 1) + 5.02808 * &
     &             (log(tsot) / log(10.))
        ewlog2 = ewlog - 1.3816e-07 * &
     &             (10. ** (11.344 * (1 - (1 / tsot))) - 1)
        ewlog3 = ewlog2 + .0081328 * &
     &             (10. ** (-3.49149 * (tsot - 1)) - 1)
        ewlog4 = ewlog3 + (log(1013.246) / log(10.))
        eval = 10. ** ewlog4
      end if
END SUBROUTINE SATVAP
