! Bug de Auroux-Striation en mode adjoint:
! il manquait l'initialisation a zero de u1b et u2b
subroutine root(x,y,N)
  real x,y
  integer N,i
  real, allocatable, dimension(:) :: u1,u2

  allocate(u1(N), u2(2*N))
  do i=1,N
     u1(i) = x*i
  enddo
  u2(:)=x*y
  x = 2*x*y
  y = y+SUM(u1)
  do i=1,N
     x = x + y*u2(i)
  enddo
  deallocate(u1,u2)
end subroutine root


