!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) -  1 Aug 2023 09:28
!
!  Differentiation of root in reverse (adjoint) mode:
!   gradient     of useful results: [alloc*u2 in root] [alloc*u1 in root]
!                x y
!   with respect to varying inputs: [alloc*u2 in root] [alloc*u1 in root]
!                x y
!   RW status of diff variables: [alloc*u2 in root]:in-out [alloc*u1 in root]:in-out
!                x:in-out y:in-out
! Bug de Auroux-Striation en mode adjoint:
! il manquait l'initialisation a zero de u1b et u2b
SUBROUTINE ROOT_B(x, xb, y, yb, n)
  IMPLICIT NONE
  REAL :: x, y
  REAL :: xb, yb
  INTEGER :: n, i
  REAL, DIMENSION(:), ALLOCATABLE :: u1, u2
  REAL, DIMENSION(:), ALLOCATABLE :: u1b, u2b
  INTRINSIC SUM
  REAL :: tempb
  ALLOCATE(u1b(n))
  u1b = 0.0
  ALLOCATE(u1(n))
  ALLOCATE(u2b(2*n))
  u2b = 0.0
  ALLOCATE(u2(2*n))
  DO i=1,n
    u1(i) = x*i
  END DO
  u2(:) = x*y
  CALL PUSHREAL4(y)
  y = y + SUM(u1)
  DO i=n,1,-1
    yb = yb + u2(i)*xb
    u2b(i) = u2b(i) + y*xb
  END DO
  tempb = SUM(u2b)
  CALL POPREAL4(y)
  u1b = u1b + yb
  yb = yb + x*2*xb + x*tempb
  xb = y*2*xb + y*tempb
  u2b = 0.0
  DO i=n,1,-1
    xb = xb + i*u1b(i)
    u1b(i) = 0.0
  END DO
  DEALLOCATE(u2)
  DEALLOCATE(u2b)
  DEALLOCATE(u1)
  DEALLOCATE(u1b)
END SUBROUTINE ROOT_B

