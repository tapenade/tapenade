! Bug found by Paavo Jumppanen
! Incorrect decompilation of too long identifiers
subroutine test(x)
  real :: x
  real :: this_is_certainly_a_very_long_variable_&
       &name_and_maybe_even_longer_than_you_would_think
  !   Now why not test what happend also for very long comments that go over one line
  !   and also how the next comment line is aligned with the previous comment line.
  this_is_certainly_a_very_long_variable_name_&
       &and_maybe_even_longer_than_you_would_think = x*x
  x = this_is_certainly_a_very_long_variable_name_&
       &and_maybe_even_longer_than_you_would_think &
       &  * this_is_certainly_a_very_long_variable_name_and_&
       &maybe_even_longer_than_you_would_think
  call A_PROCEDURE_WITH_A_VERY_VERY_LONG_NAME_&
       &THAT_WILL_CERTAINLY_BE_CUT_ON_MORE_THAN_ONE_LINE(&
       &   x, this_is_certainly_a_very_long_variable_name_&
       &and_maybe_even_longer_than_you_would_think)
end subroutine test
