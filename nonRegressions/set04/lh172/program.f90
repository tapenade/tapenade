! Bug trouve par Jan Hueckelheim 21/05/2015
! L'une des deux diffs de "residual_boundary" avait
! des infos de TBR/recomputation fausses -> adjoint faux.
module m05_residual
 contains
   subroutine residual(dv, sBdf, res)
     implicit none
     real(8) :: dv(10), sBdf(1), res
     call residual_boundary(dv, sBdf, res)
   end subroutine residual

   subroutine residual_boundary(dv, sBdf, res)
     implicit none
     integer :: iBface, i
     real(8) :: pTri(1), dv(10), sBdf(1), res

     do iBface = 1,10
         i = 1
         pTri(1) = dv(i)
         res = res + pTri(1) * sBdf(1)
     end do

   end subroutine residual_boundary
end module m05_residual
