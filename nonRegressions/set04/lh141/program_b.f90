!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 18 Jan 2024 10:24
!
!  Differentiation of getdirvectorforces in reverse (adjoint) mode:
!   gradient     of useful results: alpha beta winddirection
!   with respect to varying inputs: alpha beta winddirection
!   RW status of diff variables: alpha:in-out beta:in-out winddirection:in-out
! Bug soumis par Gaetan Kenway: il semble que les
! adjointes des variables temporaires de split
! (arg4b et arg40b) ne soient pas bien initialisees
! a zero avant les appels a vectorrotationforces_b()
SUBROUTINE GETDIRVECTORFORCES_B(refdirection, alpha, alphab, beta, betab&
& , winddirection, winddirectionb, liftindex)
  IMPLICIT NONE
  REAL(kind=8), DIMENSION(3), INTENT(IN) :: refdirection
  REAL(kind=8), INTENT(IN) :: alpha, beta
  REAL(kind=8) :: alphab, betab
  REAL(kind=8), DIMENSION(3) :: winddirection
  REAL(kind=8), DIMENSION(3) :: winddirectionb
  INTEGER(kind=4), INTENT(IN) :: liftindex
  REAL(kind=8) :: rnorm, x1, y1, z1, xbn, ybn, zbn, xw, yw, zw
  REAL(kind=8) :: x1b, y1b, z1b, xbnb, ybnb, zbnb, xwb, ywb, zwb
  INTRINSIC SQRT
  EXTERNAL VECTORROTATIONFORCES
  EXTERNAL VECTORROTATIONFORCES_B
  EXTERNAL TERMINATE
  REAL(kind=8) :: arg5
  REAL(kind=8) :: arg5b
  REAL(kind=8) :: arg50
  REAL(kind=8) :: arg5b0
  INTEGER*4 :: branch
  rnorm = SQRT(refdirection(1)**2 + refdirection(2)**2 + refdirection(3)&
&   **2)
  xbn = refdirection(1)/rnorm
  ybn = refdirection(2)/rnorm
  zbn = refdirection(3)/rnorm
  IF (liftindex .EQ. 2) THEN
    arg5 = -alpha
    CALL PUSHREAL8(zbn)
    CALL PUSHREAL8(ybn)
    CALL PUSHREAL8(xbn)
    CALL PUSHREAL8(z1)
    CALL PUSHREAL8(y1)
    CALL PUSHREAL8(x1)
    CALL VECTORROTATIONFORCES(x1, y1, z1, 3, arg5, xbn, ybn, zbn)
    arg50 = -beta
    CALL PUSHCONTROL2B(0)
  ELSE IF (liftindex .EQ. 3) THEN
    CALL PUSHREAL8(zbn)
    CALL PUSHREAL8(ybn)
    CALL PUSHREAL8(xbn)
    CALL PUSHREAL8(alpha)
    CALL PUSHREAL8(z1)
    CALL PUSHREAL8(y1)
    CALL PUSHREAL8(x1)
    CALL VECTORROTATIONFORCES(x1, y1, z1, 2, alpha, xbn, ybn, zbn)
    CALL PUSHCONTROL2B(1)
  ELSE
    CALL PUSHCONTROL2B(2)
  END IF
  zwb = winddirectionb(3)
  winddirectionb(3) = 0.0_8
  ywb = winddirectionb(2)
  winddirectionb(2) = 0.0_8
  xwb = winddirectionb(1)
  winddirectionb(1) = 0.0_8
  CALL POPCONTROL2B(branch)
  IF (branch .EQ. 0) THEN
    arg5b0 = 0.0_8
    x1b = 0.0_8
    y1b = 0.0_8
    z1b = 0.0_8
    CALL VECTORROTATIONFORCES_B(xw, xwb, yw, ywb, zw, zwb, 2, arg50, &
&                         arg5b0, x1, x1b, y1, y1b, z1, z1b)
    betab = betab - arg5b0
    CALL POPREAL8(x1)
    CALL POPREAL8(y1)
    CALL POPREAL8(z1)
    CALL POPREAL8(xbn)
    CALL POPREAL8(ybn)
    CALL POPREAL8(zbn)
    arg5b = 0.0_8
    xbnb = 0.0_8
    ybnb = 0.0_8
    zbnb = 0.0_8
    CALL VECTORROTATIONFORCES_B(x1, x1b, y1, y1b, z1, z1b, 3, arg5, &
&                         arg5b, xbn, xbnb, ybn, ybnb, zbn, zbnb)
    alphab = alphab - arg5b
  ELSE IF (branch .EQ. 1) THEN
    x1b = 0.0_8
    y1b = 0.0_8
    z1b = 0.0_8
    CALL VECTORROTATIONFORCES_B(xw, xwb, yw, ywb, zw, zwb, 3, beta, &
&                         betab, x1, x1b, y1, y1b, z1, z1b)
    CALL POPREAL8(x1)
    CALL POPREAL8(y1)
    CALL POPREAL8(z1)
    CALL POPREAL8(alpha)
    CALL POPREAL8(xbn)
    CALL POPREAL8(ybn)
    CALL POPREAL8(zbn)
    xbnb = 0.0_8
    ybnb = 0.0_8
    zbnb = 0.0_8
    CALL VECTORROTATIONFORCES_B(x1, x1b, y1, y1b, z1, z1b, 2, alpha, &
&                         alphab, xbn, xbnb, ybn, ybnb, zbn, zbnb)
  END IF
END SUBROUTINE GETDIRVECTORFORCES_B

