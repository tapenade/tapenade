! Bug soumis par Gaetan Kenway: il semble que les
! adjointes des variables temporaires de split
! (arg4b et arg40b) ne soient pas bien initialisees
! a zero avant les appels a vectorrotationforces_b()
      subroutine getDirVectorForces(refDirection, alpha, beta,&
           windDirection,liftIndex)

      real(kind=8), dimension(3), intent(in) :: refDirection
      real(kind=8), intent(in)  :: alpha, beta
      real(kind=8), dimension(3), intent(out) :: windDirection
      integer(kind=4), intent(in) :: liftIndex
      real(kind=8) :: rnorm,x1,y1,z1,xbn,ybn,zbn,xw,yw,zw

      rnorm = sqrt( refDirection(1)**2 + refDirection(2)**2 + refDirection(3)**2 )
      xbn = refDirection(1) / rnorm
      ybn = refDirection(2) / rnorm
      zbn = refDirection(3) / rnorm
      if (liftIndex==2)then
         call vectorRotationForces(x1, y1, z1, 3, -alpha, xbn, ybn, zbn)
         call vectorRotationForces(xw, yw, zw, 2, -beta, x1, y1, z1)
      elseif(liftIndex==3)then
         call vectorRotationForces(x1, y1, z1, 2, alpha, xbn, ybn, zbn)
         call vectorRotationForces(xw, yw, zw, 3, beta, x1, y1, z1)
      else
         print *,'lift index',liftindex
         call terminate('getDirVectorForces', 'Invalid Lift Direction')
      endif
      windDirection(1) = xw
      windDirection(2) = yw
      windDirection(3) = zw
      
    end subroutine getDirVectorForces
