!cf Jan Hueckelheim. Bugs about initialisations of yb.
module somemod
  real y
end module

subroutine a(x)
  real x,z
  z = x
  call b(x)
  x = x+z
end subroutine

subroutine b(x)
  real x
  call foo(x)
end subroutine

subroutine foo(x)
  use somemod, only: y
  real x
  y = y+x*x
  x = x+y*y
end subroutine
