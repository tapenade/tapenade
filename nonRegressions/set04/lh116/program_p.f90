!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 14:31
!
! Bug fourni par Jean le 30 aout 2011.
! A cause d'un mauvais matching entre les types REAL(sp) et REAL(kind=sp)
! la resolution des interface-call buggait et du coup l'activite aussi.
MODULE P
  IMPLICIT NONE
  INTEGER, PARAMETER :: sp=KIND(1.0)
  INTEGER, PARAMETER :: dp=KIND(1.0d0)
END MODULE P

MODULE M
  USE P
  IMPLICIT NONE
  TYPE COORDSYSTEM_TYPE
      INTEGER :: dim1, dim2
  END TYPE COORDSYSTEM_TYPE
  INTERFACE COORDSYSTEM_ALLOCATE
      MODULE PROCEDURE COORDSYSTEM_ALLOCATE_D, COORDSYSTEM_ALLOCATE_S, &
&     COORDSYSTEM_ALLOCATE_I, COORDSYSTEM_ALLOCATE_L, &
&     COORDSYSTEM_ALLOCATE_D2, COORDSYSTEM_ALLOCATE_S2
  END INTERFACE


CONTAINS
  SUBROUTINE COORDSYSTEM_ALLOCATE_D(coord, field)
    IMPLICIT NONE
!*FD coordinate system
    TYPE(COORDSYSTEM_TYPE), INTENT(IN) :: coord
!*FD unallocated field
    REAL(kind=dp), DIMENSION(:, :), POINTER :: field
    ALLOCATE(field(coord%dim1, coord%dim2))
!     field points to [alloc*field in coordsystem_allocate_d]
    field = 0.d0
  END SUBROUTINE COORDSYSTEM_ALLOCATE_D

  SUBROUTINE COORDSYSTEM_ALLOCATE_S(coord, field)
    IMPLICIT NONE
!*FD coordinate system
!     field points to *field or Undef
    TYPE(COORDSYSTEM_TYPE), INTENT(IN) :: coord
!*FD unallocated field
    REAL(kind=sp), DIMENSION(:, :), POINTER :: field
    ALLOCATE(field(coord%dim1, coord%dim2))
!     field points to [alloc*field in coordsystem_allocate_s]
    field = 0.e0
  END SUBROUTINE COORDSYSTEM_ALLOCATE_S

  SUBROUTINE COORDSYSTEM_ALLOCATE_I(coord, field)
    IMPLICIT NONE
!*FD coordinate system
    TYPE(COORDSYSTEM_TYPE), INTENT(IN) :: coord
!*FD unallocated field
    INTEGER, DIMENSION(:, :), POINTER :: field
    ALLOCATE(field(coord%dim1, coord%dim2))
!     field points to [alloc*field in coordsystem_allocate_i]
    field = 0
  END SUBROUTINE COORDSYSTEM_ALLOCATE_I

  SUBROUTINE COORDSYSTEM_ALLOCATE_L(coord, field)
    IMPLICIT NONE
!*FD coordinate system
    TYPE(COORDSYSTEM_TYPE), INTENT(IN) :: coord
!*FD unallocated field
    LOGICAL, DIMENSION(:, :), POINTER :: field
    ALLOCATE(field(coord%dim1, coord%dim2))
!     field points to [alloc*field in coordsystem_allocate_l]
    field = .false.
  END SUBROUTINE COORDSYSTEM_ALLOCATE_L

  SUBROUTINE COORDSYSTEM_ALLOCATE_D2(coord, nup, field)
    IMPLICIT NONE
!*FD coordinate system
!     field points to *field or Undef
    TYPE(COORDSYSTEM_TYPE), INTENT(IN) :: coord
    INTEGER, INTENT(IN) :: nup
!*FD unallocated field
    REAL(kind=dp), DIMENSION(:, :, :), POINTER :: field
    ALLOCATE(field(nup, coord%dim1, coord%dim2))
!     field points to [alloc*field in coordsystem_allocate_d2]
    field = 0.d0
  END SUBROUTINE COORDSYSTEM_ALLOCATE_D2

  SUBROUTINE COORDSYSTEM_ALLOCATE_S2(coord, nup, field)
    IMPLICIT NONE
!*FD coordinate system
    TYPE(COORDSYSTEM_TYPE), INTENT(IN) :: coord
    INTEGER, INTENT(IN) :: nup
!*FD unallocated field
    REAL(kind=sp), DIMENSION(:, :, :), POINTER :: field
    ALLOCATE(field(nup, coord%dim1, coord%dim2))
!     field points to [alloc*field in coordsystem_allocate_s2]
    field = 0.d0
  END SUBROUTINE COORDSYSTEM_ALLOCATE_S2

END MODULE M

MODULE DATA
  USE P
  IMPLICIT NONE
  TYPE FIELDS
      REAL(sp), DIMENSION(:, :), POINTER :: f1 => NULL()
      REAL(dp), DIMENSION(:, :, :), POINTER :: f2 => NULL()
  END TYPE FIELDS
  TYPE GLOB
      TYPE(FIELDS) :: thefields
  END TYPE GLOB

CONTAINS
  SUBROUTINE INITIT(aglob)
!     aglob.thefields.f1 points to *(aglob.thefields.f1) or Undef
!     aglob.thefields.f2 points to *(aglob.thefields.f2) or Undef
    USE M
    IMPLICIT NONE
    TYPE(COORDSYSTEM_TYPE) :: thecoord
    TYPE(GLOB) :: aglob
    thecoord%dim1 = 50
    thecoord%dim2 = 75
    CALL COORDSYSTEM_ALLOCATE(thecoord, 3, aglob%thefields%f2)
!     aglob.thefields.f2 points to [alloc*field in coordsystem_allocate_d2]
    CALL COORDSYSTEM_ALLOCATE(thecoord, aglob%thefields%f1)
!     aglob.thefields.f1 points to [alloc*field in coordsystem_allocate_s]
  END SUBROUTINE INITIT

  SUBROUTINE DOIT(aglob)
    IMPLICIT NONE
!     aglob.thefields.f1 points to [alloc*field in coordsystem_allocate_s] or *(aglob.thefields.f1)
!     aglob.thefields.f2 points to [alloc*field in coordsystem_allocate_d2] or *(aglob.thefields.f2)
    TYPE(GLOB) :: aglob
    aglob%thefields%f2 = 2*aglob%thefields%f2
    aglob%thefields%f1 = 2*aglob%thefields%f1
  END SUBROUTINE DOIT

  SUBROUTINE FINISHIT(aglob)
    IMPLICIT NONE
!     aglob.thefields.f1 points to [alloc*field in coordsystem_allocate_s] or *(aglob.thefields.f1)
!     aglob.thefields.f2 points to [alloc*field in coordsystem_allocate_d2] or *(aglob.thefields.f2)
    TYPE(GLOB) :: aglob
    DEALLOCATE(aglob%thefields%f2)
!     aglob.thefields.f2 points to Undef
    DEALLOCATE(aglob%thefields%f1)
!     aglob.thefields.f1 points to Undef
  END SUBROUTINE FINISHIT

END MODULE DATA

PROGRAM MAIN
  IMPLICIT NONE
  REAL :: x, y
  x = 3.7
  CALL FOO(x, y)
  PRINT*, y
END PROGRAM MAIN

SUBROUTINE FOO(x, y)
!     glob1.thefields.f1 points to Undef
!     glob1.thefields.f2 points to Undef
!     glob2.thefields.f1 points to Undef
!     glob2.thefields.f2 points to Undef
  USE DATA
  IMPLICIT NONE
  REAL :: x, y
  TYPE(GLOB) :: glob1, glob2
  INTRINSIC SUM
  CALL INITIT(glob1)
!     glob1.thefields.f1 points to [alloc*field in coordsystem_allocate_s]
!     glob1.thefields.f2 points to [alloc*field in coordsystem_allocate_d2]
  CALL INITIT(glob2)
!     glob2.thefields.f1 points to [alloc*field in coordsystem_allocate_s]
!     glob2.thefields.f2 points to [alloc*field in coordsystem_allocate_d2]
  glob1%thefields%f2 = x
  CALL DOIT(glob1)
  y = 0.0
  y = y + SUM(glob1%thefields%f2)
  glob2%thefields%f1 = x
  CALL DOIT(glob2)
  y = y + SUM(glob2%thefields%f1)
  CALL FINISHIT(glob1)
!     glob1.thefields.f1 points to Undef
!     glob1.thefields.f2 points to Undef
  CALL FINISHIT(glob2)
!     glob2.thefields.f1 points to Undef
!     glob2.thefields.f2 points to Undef
END SUBROUTINE FOO

