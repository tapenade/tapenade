! Bug trouve par Jean. Le champ "val" de m1t
! est actif et n'etait pas present dans m1t_d.
! La cause etait un oubli dans annotateDiffDeclsAndTypes()
! pour le cas des variables globales de zones allouees.
module m0
type m0t 
  real :: r
end type
end module 

module m1
use m0
  type m1t
    type(m0t) :: val
    integer :: passive=2
    type(m1t), pointer :: next => NULL()
    type(m1t), pointer :: previous => NULL()
    LOGICAL :: append=.false.
   end type
end module 

module m2
  use m1
  interface add
     module procedure add_i
  end interface 

  interface remove
    module procedure remove_i
  end interface
  
contains 
  function add_i(curr) 
    type(m1t), pointer :: curr, add_i
    allocate(add_i)
    if (associated(curr)) then 
       curr%next=>add_i
       add_i%previous=>curr
    end if
  end function
  
  function remove_i(last) 
    type(m1t), pointer :: last,remove_i
    if (associated(last%previous)) then
       last%previous%next=>last%next
       remove_i=>last%previous
    else 
       remove_i=>NULL()
    end if
    deallocate(last)
  end function
end module 

subroutine foo(x,y)
  use m2
  real :: x,y
  type(m1t), pointer :: ple1, ple2
  ple1=>add(ple1)
  ple2=>add(ple1)
  ple2%val%r=x
  y=ple2%val%r
  ple2%previous%val%r=x
  y=y + ple2%previous%val%r
  ple1=>remove(ple2)
end subroutine

program main
  real :: x,y
  x=2.0
  call foo(x,y)
  print *,y
end program
