! "Simple" routine to test my understand of AD via tapenade.
! Routine codes the numerical integration of z^2+2*z+1 from 0 to x
! x and dx given as command line arguments
! Charlotte Kotas, ORNL
!------------------------------------------------
MODULE mpi_routines
IMPLICIT NONE
include 'ampi/ampif.h'
INTEGER myid
INTEGER nprocs

INTERFACE recvsend
  MODULE PROCEDURE recvsend_int
  MODULE PROCEDURE recvsend_real
END INTERFACE recvsend

CONTAINS

SUBROUTINE recvsend_int(send_array,receive_array)
IMPLICIT NONE
INTEGER send_array(2)
INTEGER receive_array(2)
INTEGER req(4)
INTEGER status_array(MPI_STATUS_SIZE,4)
INTEGER ierr
INTEGER m
INTEGER tmp

! Post receives (from left and right nodes)
  IF( myid .eq. 0 ) THEN
    tmp=nprocs-1
  ELSE
    tmp=myid-1
  ENDIF
  CALL AMPI_IRECV( receive_array(1), 1, MPI_INTEGER, tmp, MPI_ANY_TAG, AMPI_FROM_ISEND_WAIT, MPI_COMM_WORLD, req(1), ierr)

  IF( myid .eq. nprocs-1 ) THEN
    tmp=0
  ELSE
    tmp=myid+1
  ENDIF
  CALL AMPI_IRECV( receive_array(2), 1, MPI_INTEGER, tmp, MPI_ANY_TAG, AMPI_FROM_ISEND_WAIT, MPI_COMM_WORLD, req(2), ierr)

! Send data (to left and right)
  IF( myid .eq. 0 ) THEN
    tmp=nprocs-1
  ELSE
    tmp=myid-1
  ENDIF
  CALL AMPI_ISEND( send_array(1), 1, MPI_INTEGER, tmp, myid, AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD, req(3), ierr )

  IF( myid .eq. nprocs-1 ) THEN
    tmp=0
  ELSE
    tmp=myid+1
  ENDIF
  CALL AMPI_ISEND( send_array(2), 1, MPI_INTEGER, tmp, myid, AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD, req(4), ierr )

! Wait for non-blocking to complete
  DO m=1,4
    CALL AMPI_WAIT(req(m), status_array(:,m), ierr)
  ENDDO

RETURN
END SUBROUTINE recvsend_int


SUBROUTINE recvsend_real(send_array,receive_array)
IMPLICIT NONE
REAL send_array(2)
REAL receive_array(2)
INTEGER req(4)
INTEGER status_array(MPI_STATUS_SIZE,4)
INTEGER ierr
INTEGER m
INTEGER tmp

! Post receives (from left and right nodes)
  IF( myid .eq. 0 ) THEN
    tmp=nprocs-1
  ELSE
    tmp=myid-1
  ENDIF
  CALL AMPI_IRECV( receive_array(1), 1, MPI_REAL, tmp, MPI_ANY_TAG, AMPI_FROM_ISEND_WAIT, MPI_COMM_WORLD, req(1), ierr)

  IF( myid .eq. nprocs-1 ) THEN
    tmp=0
  ELSE
    tmp=myid+1
  ENDIF
  CALL AMPI_IRECV( receive_array(2), 1, MPI_REAL, tmp, MPI_ANY_TAG, AMPI_FROM_ISEND_WAIT, MPI_COMM_WORLD, req(2), ierr)

! Send data (to left and right)
  IF( myid .eq. 0 ) THEN
    tmp=nprocs-1
  ELSE
    tmp=myid-1
  ENDIF
  CALL AMPI_ISEND( send_array(1), 1, MPI_REAL, tmp, myid, AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD, req(3), ierr )

  IF( myid .eq. nprocs-1 ) THEN
    tmp=0
  ELSE
    tmp=myid+1
  ENDIF
  CALL AMPI_ISEND( send_array(2), 1, MPI_REAL, tmp, myid, AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD, req(4), ierr )

! Wait for non-blocking to complete
  DO m=1,4
    CALL AMPI_WAIT(req(m), status_array(:,m), ierr)
  ENDDO

RETURN
END SUBROUTINE recvsend_real

END MODULE mpi_routines

!------------------------------------------------

MODULE my_routines
USE mpi_routines
IMPLICIT NONE

CONTAINS

SUBROUTINE SUB1(x,y,dx)
IMPLICIT NONE
REAL, INTENT(IN)  :: x, dx
REAL, INTENT(OUT) :: y
REAL localsum
REAL x1, x2
INTEGER i_in(2)
INTEGER i_out(2)
REAL r_in(2)
REAL r_out(2)
REAL globalsum
INTEGER ierr

! Test the integer version of recvsend
i_in(:)=myid
CALL recvsend(i_in,i_out)
write(*,*) "I am:", myid, "left neighbor:", i_out(1), "right neighbor:", i_out(2)

! Determine range to be computed by this section
! Clearly MPI isn't really needed for the simple case 
! Each process needs to determine where they start
! And tell their neighbors about it so that they
! know where to end.
! The logic here gets a bit off if nprocs is larger than x/dx
x1 = ((myid)*x)/(nprocs*1.0)
x1 = NINT(x1/dx)*dx
r_in(1)=x1
r_in(2)=x1
CALL recvsend(r_in,r_out)
IF( myid .eq. nprocs-1 ) THEN
  x2=x
ELSE
  x2 = r_out(2)
ENDIF
write(*,*) "I am:", myid, "x1:", x1, "x2:", x2

! Compute local sum (numerical integration from x1 to x2)
CALL MY_SUM( x1, x2, dx, localsum )

! Sum all processes
CALL AMPI_ALLREDUCE( localsum, globalsum, 1, MPI_REAL, MPI_SUM, MPI_COMM_WORLD, ierr )

y = globalsum

RETURN
END SUBROUTINE SUB1

SUBROUTINE MY_SUM( x1, x2, dx, localsum )
IMPLICIT NONE
REAL, INTENT(IN) :: x1, x2, dx
REAL, INTENT(OUT):: localsum
REAL xt
REAL f1, f2
REAL diff
LOGICAL done

IF (x1 .eq. x2 ) THEN
  localsum=0.0
  RETURN
ENDIF
IF (x1 .gt. x2 ) THEN
  WRITE(*,*) "Error in MY_SUM. x2 < x1"
  STOP
ENDIF
IF ( dx .le. 0 ) THEN
  WRITE(*,*) "Error in MY_SUM. dx must be positive"
  STOP
ENDIF

xt = x1
CALL MY_FUN(xt,f1)
localsum = 0.0
done=.FALSE.
DO
  diff=dx
  xt = xt + diff
  IF( xt .ge. x2 ) THEN
    IF( xt-x2 .le. dx ) THEN
      diff=x2-(xt-diff)
      xt=x2
      done=.TRUE.
    ELSE
      WRITE(*,*) "Something weird in MY_SUM..."
      RETURN
    ENDIF
  ENDIF
  CALL MY_FUN(xt,f2)

  localsum = localsum + (diff)*(0.5)*( f1 + f2 )
  IF ( done ) EXIT
  f1 = f2
ENDDO

RETURN
END SUBROUTINE MY_SUM

SUBROUTINE MY_FUN(x,f)
REAL, INTENT(IN) :: x
REAL, INTENT(OUT) :: f
f = x*x+2*x+1
END SUBROUTINE MY_FUN

END MODULE my_routines

!------------------------------------------------
PROGRAM MYPROG
USE my_routines
IMPLICIT NONE
REAL x, y, dx
INTEGER ierr
call AMPI_INIT_NT( ierr )
call AMPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
call AMPI_COMM_SIZE( MPI_COMM_WORLD, nprocs, ierr )

y=0.0
x=5.0
dx=0.05
CALL SUB1(x,y,dx)
IF( myid .eq. 0 ) THEN
  write(*,*) "y(x=",x,",dx=,",dx,") = ", y
  write(*,*) "analytical ", x*x*x/3.0 + x*x + x
ENDIF

call AMPI_FINALIZE_NT( ierr )
STOP
END PROGRAM MYPROG


