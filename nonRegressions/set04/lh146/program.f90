! Test derive de lha27 pour tester l'option "-oldContext"
   module m1
     real, allocatable, dimension(:) :: T
   contains
     subroutine P1(A,B)
       real :: A,B
       allocate(T(10))
       T = A
       B = B + SUM(T)
       deallocate(T)
     end subroutine P1
   end module m1

   subroutine top(A,B)
     use M1
     real :: A,B
     call P1(A,B)
   end subroutine top
