module pm3

TYPE topointer
  REAL*8, pointer ::topo
END TYPE topointer

contains

subroutine ptr3(a,b,c)
 REAL*8 :: a
 REAL*8,target :: b,c
 REAL*8,pointer :: p
 TYPE(topointer) :: topp

 call setpoint(topp,a,b,c)
 p => topp%topo
 p = a*a
end subroutine ptr3

subroutine setpoint(topp,a,b,c)
 TYPE(topointer) :: topp
 REAL*8 :: a
 REAL*8,target :: b,c

 if (a.gt.0.0) then
    topp%topo => b
 else
    topp%topo => c
 endif
end subroutine setpoint

end module pm3
