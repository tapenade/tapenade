module test

       TYPE T
              real :: x,y
              integer :: i
       END TYPE T

       INTERFACE OPERATOR (*)
              MODULE PROCEDURE TMUL
       END INTERFACE

       INTERFACE assignment (=)
              MODULE PROCEDURE TSET
       END INTERFACE

       INTERFACE F
              MODULE PROCEDURE TF, RF
       END INTERFACE

contains

       subroutine TSET(a,b)
         REAL, intent(out) :: a
         TYPE(T), intent(in) :: b
         a = b%x/b%y
       end subroutine TSET

       FUNCTION TMUL(A, B)
              TYPE(T), INTENT(IN) :: A, B
              TYPE(T) :: TMUL
              TMUL%x = A%x * B%x 
              TMUL%y = A%y * B%y 
       END FUNCTION TMUL

      function TF(v,u)
      TYPE(T) :: v
      real u,TF
      TF = v%x*v%y + u
      return
      end function

      function RF(t,u)
      real RF,t,u
       RF = t*u
      return
      end function


end module test



      subroutine head(a,b,c,resu)
        use test
       TYPE(T) :: a,b,c
       real resu
      resu = b*c
      resu = F(a,2.0) * resu
      return
      end
