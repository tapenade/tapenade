subroutine calcul_vectoriel ( dof0, cost )
!
    implicit none
!
! ... variables pass�es en param�tre ...................................
!
    real(8), dimension(2,3),    intent(in)      :: dof0
    real(8),                    intent(out)     :: cost
!
! ... variables locales ................................................
!
    integer                     :: i
    real(8), dimension(3)       :: resc
    real(8), dimension(2,3)     :: dof,res
!
! ... d�but du traitement
!
    cost = 0d0
    dof = dof0
!
    do i = 1,2
        resc     = dof(i,:)
        res(i,:) = 2d0*resc
    enddo
!
    dof = dof + res
!
    do i = 1,2
        cost = cost + dof(i,1)*dof(i,i+1)
    enddo
!
end subroutine calcul_vectoriel
