!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.5 (r3636M) - 12 Jan 2011 17:16
!
!  Differentiation of swap_r in forward (tangent) mode:
!   variations   of useful results: a b
!   with respect to varying inputs: a b
!   RW status of diff variables: a:in-out b:in-out
SUBROUTINE SWAP_R_D(a, ad, b, bd)
  IMPLICIT NONE
  REAL, INTENT(INOUT) :: a, b
  REAL, INTENT(INOUT) :: ad, bd
  REAL :: temp
  REAL :: tempd
  tempd = ad
  temp = a
  ad = bd
  a = b
  bd = tempd
  b = temp
END SUBROUTINE SWAP_R_D

