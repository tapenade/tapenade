! Bug rencontre par Paavo Jumppanen le 17/12/2014
! a propos de la methode "black-box"
! Le traitement de la librairie bb.lib etait trop case-dependent
! et donc "mysin" ne matchait pas "mySin"

REAL FUNCTION test(x)
  INTENT (IN) x
  REAL :: x
  test = mySin(x)
  RETURN
END FUNCTION test
