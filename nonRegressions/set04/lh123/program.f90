!Un bug annexe trouve en cherchant un bug chez Hubert.
! il y avait un usage de finalsize dans TOP_D alors
! que cette variable est locale a PP0.
MODULE MODVARS
  real,dimension(:),allocatable :: pbvar
  real,dimension(100) :: var1,var2,var3
END MODULE MODVARS

SUBROUTINE TOP(x,y,ssz)
  USE MODVARS
  real :: x,y
  integer ssz
  CALL PP0(ssz)
1000 CONTINUE
  CALL PP1(x)
  CALL PP2()
  CALL PP3(y)
  if (y>10) GOTO 2000
  GOTO 1000
2000 CONTINUE
END SUBROUTINE TOP

SUBROUTINE PP0(size)
  USE MODVARS
  INTEGER :: size, finalsize
  finalsize = 30
  if (size.gt.30) then
     finalsize = size
  end if
  ALLOCATE(pbvar(finalsize))
  var1 = 0.0
  var2 = 0.0
  var3 = 0.0
END SUBROUTINE PP0

SUBROUTINE PP1(x)
  USE MODVARS
  real::x
  var1 = var1+x
END SUBROUTINE PP1

SUBROUTINE PP2
  USE MODVARS
  pbvar = var1
  var2 = var2 + SUM(pbvar)
END SUBROUTINE PP2

SUBROUTINE PP3(y)
  USE MODVARS
  real::y
  y = y + SUM(var2*var1)
END SUBROUTINE PP4
