PROGRAM MAIN
  USE MMB_D
  USE MMA_D
  REAL, DIMENSION(:), ALLOCATABLE :: x
  REAL, DIMENSION(:), ALLOCATABLE :: xd
  INTEGER :: i
  REAL :: cvad(2)
  COMMON /cc1_d/ cvad
  REAL :: cva(2)
  COMMON /cc1/ cva
  EXTERNAL GETFLAG
  CALL GETFLAG(flag)
  ALLOCATE(xd(30))
  ALLOCATE(x(30))
  CALL INITA()
  DO i=1,20
    aa(i) = 100.0/i
    aad(i) = 1.0
    x(i) = 3.3*i
    xd(i) = 2.0
  END DO
  cvad(1) = 0.5
  cvad(2) = 0.5
  cva(1) = 1.8
  cva(2) = 8.1
  CALL ROOT_D(x, xd)
  PRINT*, 'zz=', zz, 'and zzd=', zzd
  DEALLOCATE(aad)
  DEALLOCATE(aa)
  DEALLOCATE(xd)
  DEALLOCATE(x)
END PROGRAM MAIN

subroutine getflag(n)
  integer n
  n = 20
end subroutine getflag
