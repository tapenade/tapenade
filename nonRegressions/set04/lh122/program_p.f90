!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
!un exemple pour les questions de contexte d'appel
! du code differentie, les _D et les _CD, et utilise
! comme exemple dans la FAQ #usagecontext
MODULE MMA
  IMPLICIT NONE
  REAL, DIMENSION(:), ALLOCATABLE :: aa

CONTAINS
  SUBROUTINE INITA()
    IMPLICIT NONE
    ALLOCATE(aa(20))
!     aa points to [alloc*aa in inita]
  END SUBROUTINE INITA

END MODULE MMA

MODULE MMB
  IMPLICIT NONE
  INTEGER :: flag
  REAL :: zz
END MODULE MMB

PROGRAM MAIN
!     x points to Undef
  USE MMA
  USE MMB
  IMPLICIT NONE
  REAL, DIMENSION(:), ALLOCATABLE :: x
  INTEGER :: i
  REAL :: cva(2)
  COMMON /cc1/ cva
  EXTERNAL GETFLAG
  CALL GETFLAG(flag)
  ALLOCATE(x(30))
!     x points to [alloc*x in main]
  CALL INITA()
!     aa points to [alloc*aa in inita]
  DO i=1,20
    aa(i) = 100.0/i
    x(i) = 3.3*i
  END DO
  cva(1) = 1.8
  cva(2) = 8.1
  CALL ROOT(x)
  PRINT*, 'zz=', zz
  DEALLOCATE(aa)
!     aa points to Undef
  DEALLOCATE(x)
!     x points to Undef
END PROGRAM MAIN

SUBROUTINE ROOT(x)
!     aa points to [alloc*aa in inita] or *aa
  USE MMA
  USE MMB
  IMPLICIT NONE
  REAL, DIMENSION(*) :: x
  REAL :: v
  REAL :: cv1, cv2
  COMMON /cc1/ cv1, cv2
  INTRINSIC SUM
  zz = cv1*cv2
  v = SUM(aa)
  IF (flag .GT. 2) zz = zz*v
  zz = zz + SUM(x)
END SUBROUTINE ROOT

