subroutine tap_vec(x,u,y)
!
    implicit none
!
    real(8), dimension(3), intent(in)   :: x
    real(8), dimension(2), intent(in)   :: u
    real(8), dimension(2), intent(out)  :: y
!
    y = x(2:3)+x(1)*u
!
end subroutine tap_vec
