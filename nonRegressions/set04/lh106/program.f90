! pour tester le comportement et les initialisations
! dans les cas "stub fourni" ou "GeneralLib fournie"
! Question: que reste-t-il a coder par end-user apres ?
module nf90stub
  public
contains
  subroutine nf90_sub(x)
    real x
    if (x.gt.10) then
       x = 0.0
    endif
  end subroutine nf90_sub
  function nf90_fun(y)
    real y,nf90_fun
    nf90_fun = 1.0
  end function nf90_fun
end module nf90stub

subroutine top(x,y)
  use nf90stub
  real x,y
  external ext_fun
  real ext_fun
  real a,b,c

  a = x*y
  b = x+y
  call nf90_sub(x)
  b = b + x*y + a
  if (y.gt.10) then
     a = nf90_fun(y)
  endif
  x = a*b + x*y
  if (y.gt.10) then
     b = ext_fun(y)
  endif
  x = x + a*y + b
  x = x*x
end
