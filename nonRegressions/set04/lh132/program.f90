! Follow-up to set04/lh131. This time on tangent AD of a
! code (in fact inspired from adjoint AD of set04/lh131...)
! in which dists[1|2]b%point[1|2]%[x|y] are listed as dependent
! outputs but not as independent inputs.
! Therefore their derivative dists[1|2]bd%point[1|2]%[x|y]
! are reset to zero at the beginning.
! This is possibly surprising but normal. To avoid this resetting,
! dists[1,2]b%point[1,2]%[x,y] must be added
! to the independents as well.
MODULE TRIPROBLEM_B
  IMPLICIT NONE
  TYPE TT
      REAL*8 :: x
      REAL*8 :: y
  END TYPE TT
  TYPE DD1
      TYPE(TT), POINTER :: point1
      TYPE(TT), POINTER :: point2
      REAL*8 :: requested
  END TYPE DD1
  TYPE DD1_B
      TYPE(TT), POINTER :: point1
      TYPE(TT), POINTER :: point2
  END TYPE DD1_B
  TYPE DD2
      TYPE(TT), POINTER :: point1
      TYPE(TT), POINTER :: point2
      REAL*8 :: requested
  END TYPE DD2
  TYPE DD2_B
      TYPE(TT), POINTER :: point1
      TYPE(TT), POINTER :: point2
  END TYPE DD2_B

CONTAINS
  SUBROUTINE ENERGY_B(dists1, dists1b, dists2, dists2b, energyb)
    IMPLICIT NONE
    REAL*8 :: energy
    REAL*8 :: energyb
    TYPE(DD1), DIMENSION(*) :: dists1
    TYPE(DD1_B), DIMENSION(*) :: dists1b
    TYPE(DD2), DIMENSION(*) :: dists2
    TYPE(DD2_B), DIMENSION(*) :: dists2b
    REAL*8 :: dx, dy, delta
    REAL*8 :: dxb, dyb, deltab
    INTEGER :: i
    REAL*8 :: tempb0
    REAL*8 :: tempb
    INTEGER :: ii1
    INTRINSIC SQRT
    DO i=1,20
      dx = dists2(i)%point2%x - dists2(i)%point1%x
      dy = dists2(i)%point2%y - dists2(i)%point1%y
      delta = SQRT(dx*dx + dy*dy) - dists2(i)%requested
      deltab = 2*delta*energyb
      IF (dx**2 + dy**2 .EQ. 0.0) THEN
        tempb0 = 0.0
      ELSE
        tempb0 = deltab/(2.0*SQRT(dx**2+dy**2))
      END IF
      dxb = 2*dx*tempb0
      dyb = 2*dy*tempb0
      dists2b(i)%point2%y = dists2b(i)%point2%y + dyb
      dists2b(i)%point1%y = dists2b(i)%point1%y - dyb
      dists2b(i)%point2%x = dists2b(i)%point2%x + dxb
      dists2b(i)%point1%x = dists2b(i)%point1%x - dxb
    END DO
    DO i=1,10
      dx = dists1(i)%point2%x - dists1(i)%point1%x
      dy = dists1(i)%point2%y - dists1(i)%point1%y
      delta = SQRT(dx*dx + dy*dy) - dists1(i)%requested
      deltab = 2*delta*energyb
      IF (dx**2 + dy**2 .EQ. 0.0) THEN
        tempb = 0.0
      ELSE
        tempb = deltab/(2.0*SQRT(dx**2+dy**2))
      END IF
      dxb = 2*dx*tempb
      dyb = 2*dy*tempb
      dists1b(i)%point2%y = dists1b(i)%point2%y + dyb
      dists1b(i)%point1%y = dists1b(i)%point1%y - dyb
      dists1b(i)%point2%x = dists1b(i)%point2%x + dxb
      dists1b(i)%point1%x = dists1b(i)%point1%x - dxb
    END DO
  END SUBROUTINE ENERGY_B
  FUNCTION ENERGY(dists1, dists2)
    IMPLICIT NONE
    REAL*8 :: energy
    TYPE(DD1), DIMENSION(*) :: dists1
    TYPE(DD2), DIMENSION(*) :: dists2
    REAL*8 :: dx, dy, delta
    INTEGER :: i
    INTRINSIC SQRT
    DO i=1,10
      dx = dists1(i)%point2%x - dists1(i)%point1%x
      dy = dists1(i)%point2%y - dists1(i)%point1%y
      delta = SQRT(dx*dx + dy*dy) - dists1(i)%requested
      energy = energy + delta*delta
    END DO
    DO i=1,20
      dx = dists2(i)%point2%x - dists2(i)%point1%x
      dy = dists2(i)%point2%y - dists2(i)%point1%y
      delta = SQRT(dx*dx + dy*dy) - dists2(i)%requested
      energy = energy + delta*delta
    END DO
  END FUNCTION ENERGY
END MODULE TRIPROBLEM_B

