module example2
 implicit none

 type vector
     character(256) :: name
     real :: x,y,z
 end type vector

 type(vector) :: u,v,w

contains
 function test(a,b)
  type(vector) :: a,b
  real :: test
  print *, a%name, b%name
  test = a%x + b%x + u%z
 end function test

end module
