!This is a simplified v502

subroutine top(x,y,z)
real, DIMENSION(:) :: x,y,z
call foo(x,y,z)
end subroutine top

subroutine foo(x,y,z)
real, DIMENSION(:), intent(in) :: x
real, DIMENSION(:) :: y
real, DIMENSION(:), intent(in) :: z
call bar(x,y)
call bar(z,y)
end

subroutine bar(a,b)
real, DIMENSION(:) :: a,b
b = a*b
end 
