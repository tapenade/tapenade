module M
       TYPE INTERVAL
              REAL LOWER, UPPER
              character(256) :: name
       END TYPE INTERVAL

      interface
          function f(t)
            real :: f
            real :: t
          end function f
      end interface

      contains
      real function f(t)
      real t
      f = t * t
      return
      end function
end module

module N
use M, k => f , interv => interval
      interface
          function ff(t)
            use M, k => f , interv => interval
            real :: ff
            type(interv) :: t
          end function ff
      end interface
      real pi 

      contains
      real function ff(t)
      use M, k => f , interv => interval
      type(interv) :: t
      ff = k(t%lower + t%upper)
      ff = ff + 10.0
      return
      end function
end module

program test
use M, h => f, interv => interval
use N, g => ff , ppi => pi
real x,y
type(interv) :: t
ppi = 3.14
x = 3.4
y = h(x)
t%lower = 1.0
t%upper = 2.0
WRITE (*,*) x, y
y = g(t)
WRITE (*,*) x, y
end


