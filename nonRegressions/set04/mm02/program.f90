!Bug de Massimiliano Martinelli du 14/06/2007:
! variable derivee psibd en trop !
! Le bug provient d'une faiblesse de l'activite
!(cf ADDirectDifferentiator ligne 1603 le 14/06/2007)
! qui ne sera corrige que quand ReqExplicit sera
! etendu a tout le mecanisme "implicit zero".
SUBROUTINE STATE_RESIDUALS_B(psi, psib, gamma, gammab, w, wb, n_control&
&  , n_state)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: n_control, n_state
  REAL*8, DIMENSION(n_control), INTENT(IN) :: gamma
  REAL*8, DIMENSION(n_state), INTENT(IN) :: w
  REAL*8 :: gammab(n_control), psi(n_state), psib(n_state), wb(n_state)
  REAL*8 :: tempb
  wb(1:n_state) = 0.0
  wb(1) = 2.0*psib(2)
  wb(2) = -(2*w(2)*psib(2))
  psib(2) = 0.0
  gammab(1:n_control) = 0.0
  tempb = w(1)*2*(gamma(1)-gamma(2))*psib(1)
  gammab(2) = psib(1) - tempb
  gammab(1) = tempb - psib(1)
  wb(1) = wb(1) + ((gamma(1)-gamma(2))**2-1.0)*psib(1)
  wb(2) = wb(2) + 2*w(2)*psib(1)
  psib(1:n_state) = 0.0
END SUBROUTINE STATE_RESIDUALS_B
