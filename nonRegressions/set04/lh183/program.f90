! Bug fourni par Dan Moerder (NASA)
! En mode vectoriel, les tableaux de pointeurs derives x1d, x2d, y1d
!  doivent etre de taille deferred shape (:)
! L'indexage des instructions derivees etait faux x1d => xd(1) au lieu de xd(:,1)
subroutine bug(x,y)
implicit none
real(8),target,intent(in) :: x(3)
real(8),target,intent(out) :: y(2)
real(8),pointer :: x1,x2,y1

x1 => x(1)
x2 => x(2)
y1 => y(1)
y1=x1*x2
end subroutine bug
