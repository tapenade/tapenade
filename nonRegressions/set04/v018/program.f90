module example2
 implicit none

 type vector
     character(256) name
     character*2 n
     real :: x,y,z
 end type vector

 type(vector) :: u,v,w
 character*2 m1,m2
 character o1

contains
 function test(a,b)
  type(vector) :: a,b
  real :: test
  print *, a%name, b%name
  test = a%x + b%x + u%z
 end function test

end module
