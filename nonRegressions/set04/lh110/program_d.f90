!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6730M) - 19 Feb 2018 13:20
!
MODULE M1_DIFF
  USE M0
  IMPLICIT NONE
  TYPE M1T
      TYPE(M0T) :: val
      INTEGER :: passive=2
      TYPE(M1T), POINTER :: next => NULL()
      TYPE(M1T), POINTER :: previous => NULL()
  END TYPE M1T
  TYPE M1T_DIFF
      TYPE(M0T) :: val
      TYPE(M1T_DIFF), POINTER :: next => NULL()
      TYPE(M1T_DIFF), POINTER :: previous => NULL()
  END TYPE M1T_DIFF
END MODULE M1_DIFF

!  Differentiation of foo in forward (tangent) mode:
!   variations   of useful results: y
!   with respect to varying inputs: x
!   RW status of diff variables: x:in y:out
SUBROUTINE FOO_D(x, xd, y, yd)
  USE M1_DIFF
  IMPLICIT NONE
  REAL :: x, y
  REAL :: xd, yd
  TYPE(M1T), TARGET :: le1, le2
  TYPE(M1T_DIFF), TARGET :: le1d, le2d
  le1d%val%r = xd
  le1%val%r = x
  le1d%next => le2d
  le1%next => le2
  le2d%previous => le1d
  le2%previous => le1
  le2d%val%r = le1d%val%r
  le2%val%r = le1%val%r
  yd = le1d%next%val%r
  y = le1%next%val%r
END SUBROUTINE FOO_D

SUBROUTINE FOO_NODIFF(x, y)
  USE M1_DIFF
  IMPLICIT NONE
  REAL :: x, y
  TYPE(M1T), TARGET :: le1, le2
  le1%val%r = x
  le1%next => le2
  le2%previous => le1
  le2%val%r = le1%val%r
  y = le1%next%val%r
END SUBROUTINE FOO_NODIFF

