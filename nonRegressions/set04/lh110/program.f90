! Bug trouve par Jean.
! L'initialisation des derivees de le2
!  qui doivent passer de implicit-zero a explicit-zero
!  au moment ou on passe l'adresse de le2 a le1%next
! causait des initialisations fausses de POINTER's ou
! d'INTEGER's a zero !!
module m0
type m0t 
  real :: r
end type
end module 

module m1
use m0
  type m1t
    type(m0t) :: val
    integer :: passive=2
    type(m1t), pointer :: next => NULL()
    type(m1t), pointer :: previous => NULL()
   end type
end module 

subroutine foo(x,y)
  use m1
  real :: x,y
  type(m1t),target :: le1,le2

  le1%val%r=x
  le1%next=>le2
  le2%previous=>le1
  le2%val%r=le1%val%r
  y=le1%next%val%r
end subroutine

program main
  real :: x,y
  x=2.0
  call foo(x,y)
  print *,y
end program
