! Bug trouve par Jean. l'appel a baralloc_d
! passait un dummyzerodiffd alors que ca doit etre ttd%field
! Le bug etait dans des valeurs "null" passees au lieu de afterReqX.
MODULE MODALLOC
  PUBLIC BARALLOC
  INTERFACE baralloc
     MODULE PROCEDURE allocatereals
  END INTERFACE
CONTAINS
  subroutine allocatereals(sz1,sz2,toArray)
    INTEGER :: sz1,sz2
    REAL, dimension(:,:), pointer :: toArray
    ALLOCATE(toArray(sz1,sz2))
  end subroutine allocatereals
END MODULE MODALLOC

MODULE BUGMKALLOC
  USE modalloc
  TYPE foo
     REAL :: freq=100.0
     REAL :: rr=12.5
     REAL, dimension(:,:), pointer :: field 
  END TYPE foo

CONTAINS

  subroutine top(x,y)
    REAL :: x,y
    TYPE(foo) :: tt
    call baralloc(19,29,tt%field)
    tt%rr = x*tt%rr
    if (x>0) then
       tt%field(:,:) = SQRT(x)
    endif
    y = x*tt%rr
    if (x>10) then
       y = y+SUM(tt%field)
    end if
  end subroutine top

END MODULE BUGMKALLOC
    
