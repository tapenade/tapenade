! Bug fourni par Dan Moerder (NASA Langley)
! f1 considere comme passif: f1d = 0.d0 !!
!  parce que l'ecriture f2=... etait consideree
!  comme une ecriture *totale* de f !!
subroutine bug(f,nf,x)
implicit none
real(double) :: f(nf),x
integer :: nf
real(double),pointer :: f1,f2,xp

xp => x
f1 => f(1)
f2 => f(2)
f1=3*x
f2=x**2
end subroutine bug
