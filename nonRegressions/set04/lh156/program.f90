! Pour tester un bug de declaration d'un param formel d'une routine
!  differentiee qui avait une taille calculee par un SIZE() runtime !
!
!== Copyright (C) 2000-2013 EDF-CETMEF ==
!
!   This file is part of MASCARET.
!
!   MASCARET is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   MASCARET is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with MASCARET.  If not, see <http://www.gnu.org/licenses/>



MODULE M_PERMAT_I
  INTERFACE 
      SUBROUTINE PERMAT(z, cf1, cf2)
        DOUBLE PRECISION, DIMENSION(:) :: z
        DOUBLE PRECISION, DIMENSION(:) :: cf1
        DOUBLE PRECISION, DIMENSION(:) :: cf2
      END SUBROUTINE PERMAT
  END INTERFACE
END MODULE M_PERMAT_I


MODULE M_QREPAR_I
  INTERFACE 
      SUBROUTINE QREPAR(z, cf1, cf2)
        DOUBLE PRECISION, DIMENSION(:) :: z
        DOUBLE PRECISION, DIMENSION(:) :: cf1
        DOUBLE PRECISION, DIMENSION(:) :: cf2
      END SUBROUTINE QREPAR
  END INTERFACE
END MODULE M_QREPAR_I


MODULE M_PERSAR_I
  INTERFACE 
      SUBROUTINE PERSAR(z, cf1, cf2, pcsing, idt)
        USE M_PERMAT_I
        USE M_QREPAR_I
        DOUBLE PRECISION, DIMENSION(:) :: z
        DOUBLE PRECISION, DIMENSION(:) :: cf1
        DOUBLE PRECISION, DIMENSION(:) :: cf2
        DOUBLE PRECISION, DIMENSION(:) :: pcsing
        INTEGER, DIMENSION(:) :: idt
      END SUBROUTINE PERSAR
  END INTERFACE
END MODULE M_PERSAR_I







SUBROUTINE SARAP(z, cf1, cf2, idt, pcsing)
  USE M_PERSAR_I
  DOUBLE PRECISION, DIMENSION(:) :: z
  DOUBLE PRECISION, DIMENSION(:) :: cf1, cf2
  INTEGER, DIMENSION(:) :: idt
  DOUBLE PRECISION, DIMENSION(:) :: pcsing

  CALL PERSAR(z, cf1, cf2, pcsing, idt)
END SUBROUTINE SARAP


SUBROUTINE PERSAR(z, cf1, cf2, pcsing, idt)
  USE M_PERMAT_I
  USE M_QREPAR_I
  DOUBLE PRECISION, DIMENSION(:) :: z
  DOUBLE PRECISION, DIMENSION(:) :: cf1
  DOUBLE PRECISION, DIMENSION(:) :: cf2
  DOUBLE PRECISION, DIMENSION(:) :: pcsing
  INTEGER, DIMENSION(:) :: idt

  CALL PERMAT(z, cf1, cf2)
  CALL QREPAR(z, cf1, cf2)
END SUBROUTINE PERSAR


SUBROUTINE QREPAR(z, cf1, cf2)
  DOUBLE PRECISION, DIMENSION(:) :: z
  DOUBLE PRECISION, DIMENSION(:) :: cf1
  DOUBLE PRECISION, DIMENSION(:) :: cf2
  INTEGER :: i

  do i=10,20
     CALL MYFUNC1(cf1(i),cf2(i),z(i))
  enddo
END SUBROUTINE QREPAR


SUBROUTINE PERMAT(z, cf1, cf2)
  DOUBLE PRECISION, DIMENSION(:) :: z
  DOUBLE PRECISION, DIMENSION(:) :: cf1
  DOUBLE PRECISION, DIMENSION(:) :: cf2
  INTEGER :: i

  do i=10,20
     CALL MYFUNC2(cf1(i),cf2(i),z(i))
  enddo
END SUBROUTINE PERMAT
