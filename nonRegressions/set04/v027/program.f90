module m1
  real, save :: x
end module m1

module m2
  use m1
  contains
    real function f(t)
      real t
      f = t*t + x
      x = f
      return
    end function

end module m2

subroutine g(u,v)
use m2
real u,v
v = f(u)
v = f(u)
end subroutine

