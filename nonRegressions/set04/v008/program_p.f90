!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4904M) - 20 Jun 2013 17:42
!
MODULE T
  IMPLICIT NONE
  REAL :: t1, t2
END MODULE T

REAL FUNCTION G(t)
  USE T, ONLY : t1
  IMPLICIT NONE
  REAL :: t
  g = t*t + t1
  RETURN
END FUNCTION G

