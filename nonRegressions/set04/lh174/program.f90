! Bug "cmplxabs" trouve par Michael Vossbeck
subroutine foo(n, x, y)
  real, intent(in), dimension(n) :: x
  real, intent(out) :: y
  complex c

  if( n .lt. 2 ) then
     print*, 'ERROR::n must be at least 2, abort now'
     stop
  end if

  c = cmplx( x(1), x(2) )

  y = abs(c)

end subroutine foo
