! Bug "useonly1" trouve par Michael Vossbeck
MODULE PARKIND1
  IMPLICIT NONE
  INTEGER, PARAMETER :: JPRM = kind(1.d0)
end MODULE PARKIND1

module yocmempar
  use parkind1
  implicit none

  REAL(KIND = JPRM) :: sintheta

end module yocmempar

!-------------------------------------------------
!
! Purpose:
! - scalar valued function with n independents x
!   and dependent y
!-------------------------------------------------
subroutine foo(n, x, y)
  use yocmempar, only:sintheta
  implicit none
  integer n
  real, intent(in), dimension(n) :: x
  real, intent(out) :: y

  sintheta = x(1)*x(2)

  y = sqrt( x(2)*x(2) + sintheta*sintheta )
end subroutine foo
