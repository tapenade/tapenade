module defpoint
  type point
     real z,x,y
  end type point
end module

subroutine t(u,v,w)
  use defpoint
  type(point) :: u,v,w
  v%x = u%y
  v%y = u%x + u%y
  w = f(u,v)
end subroutine

function f(a,b)
  use defpoint
  type(point) :: f,a,b
  f%y = 0.0
  f%x = a%x * b%x
  a%y = b%x + a%x
  b%y = 0.5
end function
