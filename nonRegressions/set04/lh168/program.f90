!Bug trouve par Jan Hueckelheim Fev 2015
!Bug dans le spaghetti adjoint:
!  CALL POPCONTROL2B() perdu!
!Avec l'option "-nooptim deadcontrol" on obtenait un adjoint vide!!

SUBROUTINE adjspaghetti(switch,countmax, x, y, idcrn)
  IMPLICIT NONE
  INTEGER, PARAMETER :: int_p = 8
  INTEGER, PARAMETER :: real_p = 8
  LOGICAL :: switch(4)
  INTEGER :: countmax
  INTEGER :: counter
  REAL :: x,y
  INTEGER, DIMENSION(10) :: idcrn

  IF (switch(1)) then
     idcrn = abs(idcrn)
  ELSE
     x = x*x
     IF (switch(3)) THEN
        x = x*4
     ELSE
        x = 0
        RETURN
     END IF
     DO counter = 1, countmax
        IF(switch(4)) THEN
           x = x*2
        ELSE
           x = x*3
        END IF
     END DO
  END IF

END SUBROUTINE adjspaghetti

