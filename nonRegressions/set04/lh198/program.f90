SUBROUTINE foo(a,b)
  REAL a,b
  INTEGER i
  a = a*b
  DO i=1,10
     a = i*a
  ENDDO
  call gee(a,b)
  DO i=1,10
     b = i*b
  ENDDO
  b = b*a
CONTAINS
  SUBROUTINE gee(u,v)
    REAL u,v
    u = u*v
    v = v*u
  END SUBROUTINE gee
END SUBROUTINE foo
