module a
  integer, parameter :: ia = 2
end module

module b
  use a
  real ::  xb(ia)
end module

module c
  use b
contains
      subroutine sub(x,y)
      real x,y
      y = x + xb(0)
      end
end module

program main
use c
implicit none
real x,y
xb(0) = 3.0
x = 1.0
y = 2.0
write (*,*) x, y
call sub(x,y)
write (*,*) x, y
end
