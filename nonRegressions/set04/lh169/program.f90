
! Bug trouve par Jan Hueckelheim.
! le mode vectoriel (appel de abs sur un tableau)
! fait rater la differentiation.
SUBROUTINE getcrn_all(switch, x, y, idcrn)
  IMPLICIT NONE
  INTEGER, PARAMETER :: int_p = 8
  INTEGER, PARAMETER :: real_p = 8
  LOGICAL :: switch(3)
  REAL :: x,y
  INTEGER, DIMENSION(10) :: idcrn
  LOGICAL :: needToReturn = .false.
  INTEGER :: i

  IF (switch(1)) then
     ! this works fine:
     !do i=1,10
     !   idcrn(i) = abs(idcrn(i))
     !end do
     ! this triggers the bug:
     idcrn = abs(idcrn)
  ELSE
     x = x*x
     IF (switch(2)) THEN
        x = x*4
     ELSE
        x = 0
        needToReturn = .true.
     END IF
     IF (.not. needToReturn) THEN
        x = x*2
     END IF
  END IF

END SUBROUTINE getcrn_all


