      double precision function f(t)
      double precision t
      real (selected_real_kind(12)) :: workg (10,20)
      integer size 
      size = selected_real_kind(12) + selected_int_kind(4)
      f = t * t + workg(1,1)
      f = exp(f)
      return
      end
