!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.12 (r6574M) -  3 Oct 2017 15:47
!
DOUBLE PRECISION FUNCTION F(t)
  IMPLICIT NONE
  DOUBLE PRECISION :: t
  INTRINSIC SELECTED_REAL_KIND
  REAL(SELECTED_REAL_KIND(12)) :: workg(10, 20)
  INTEGER :: size
  INTRINSIC SELECTED_INT_KIND
  INTRINSIC EXP
  size = SELECTED_REAL_KIND(12) + SELECTED_INT_KIND(4)
  f = t*t + workg(1, 1)
  f = EXP(f)
  RETURN
END FUNCTION F

