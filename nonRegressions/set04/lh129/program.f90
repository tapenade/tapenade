! Essai pour reproduire le bug de Jean dans les
! appels a SIZE() pour les tailles de tableaux
! dans les push/pop: L'arbre dont on prenait la
! SIZE() contenait des indices faux et en plus
! il begayait sur les chaines de fieldAccess.
MODULE TESTM

  TYPE tt2
     real,allocatable :: tab4(:,:)
     real :: tab5(:,:)
  END TYPE tt2

  TYPE tt1
     type(tt2) :: ff2
  END TYPE tt1

  integer maxm
  TYPE(tt1) :: bigstruct(1:maxm)

CONTAINS

  SUBROUTINE FOO(X,Y)
    real X,Y
    integer i

    do i=1,88
       ALLOCATE(bigstruct(i)%ff2%tab4(i,i))
       bigstruct(i)%ff2%tab4(:,:) = X*Y
       bigstruct(i)%ff2%tab5(:,:) = X*Y
    enddo

    bigstruct(5)%ff2%tab4(1,2) = X*bigstruct(5)%ff2%tab4(1,2)
    bigstruct(5)%ff2%tab5(1,2) = X*bigstruct(5)%ff2%tab5(1,2)

    X = X*X

    call bar(X,Y)

    Y = Y*Y

  END SUBROUTINE FOO

  SUBROUTINE BAR(X,Y)
    real X,Y
    call GEE(X,Y,bigstruct)
  END SUBROUTINE BAR

END MODULE TESTM
