module M
      contains
      real function f(t)
      real t
      f = t * t
      return
      end function
end module

module N
use M, k => f

      contains
      real function ff(t)
      use M, k => f
      real t
      ff = k(t)
      ff = ff + 10.0
      return
      end function
end module

module O
use M, l => f
use N

      contains
      real function gg(t)
      use M, l => f
      real t
      gg = l(t) + ff(t)
      gg = gg + 10.0
      return
      end function
end module
