SUBROUTINE foo
 USE flowPublicModule, ONLY : flow
 IMPLICIT NONE
 INTERFACE
   SUBROUTINE solver( fp, sc, ap, coefs )
     USE precisions, ONLY : real_p
     REAL(8), DIMENSION(:), POINTER :: fp, sc, ap, coefs
   END SUBROUTINE solver
 END INTERFACE
 flow%bal(1) = zero
 CALL solver( flow%fp, flow%sc, flow%ap, flow%coefs )
END SUBROUTINE foo
