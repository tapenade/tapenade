! From the bug report "inviscidCentralFlux" from G. Kenway
! about wrong AD of multiply nested II-loops 

SUBROUTINE INVISCIDCENTRALFLUX(w,res)
  implicit none
  real*8 w(30,30)
  integer(kind=intType) :: i, k
!  real(kind=realType) :: vnp,qsp,res
  real*8 :: vnp,qsp,res

  !$AD CHECKPOINT-START
  res = res+2

  !$AD II-LOOP
  do k=2,22
     !$AD II-LOOP
     do i=2,22
        vnp = w(i+1,k)*w(i,k) + w(i+1,k)*w(i,k) + w(i+1,k)*w(i,k)
        qsp = SIN(vnp)
        res = res + qsp*qsp
     enddo
  enddo

  !$AD CHECKPOINT-END
  continue
  res = res+3

  !$AD II-LOOP
  do k=1,11
     !$AD II-LOOP
     do i=2,22
        vnp = w(i+2,k)*w(i,k) + w(i+2,k)*w(i,k) + w(i+2,k)*w(i,k)
        qsp = SIN(vnp)
        res = res + qsp*qsp
     enddo
  enddo
END SUBROUTINE INVISCIDCENTRALFLUX
