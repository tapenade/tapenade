!Exemple de Stefano Carli a propos des choix de quoi
! generer en _NODIFF et de quoi ne pas regenerer pour
! plutot le reprendre depuis le code d'origine.
! -> a la difference du precedent, ce test avec -context
! et *surtout* avec -nooptim stripprimalcode
      module some_module
      implicit none
      real, allocatable :: tr(:), ti(:,:)
      integer :: nx, ny, ns
      end module some_module


      module driver
      use some_module
      implicit none
      contains

        subroutine driver_0
        implicit none
        external get_dimensions
        call get_dimensions(nx,ny,ns)
        allocate(tr(ns))
        allocate(ti(nx,ny))
        return
        end subroutine driver_0

        subroutine driver_1
        implicit none
        real :: bb(-1:nx,-1:ny,0:ns), vol(-1:nx,-1:ny)
        integer is
        real :: myfun
        external myfun, get_ti, get_bb, get_vol
        call get_ti(ti,nx,ns,ny)
        call get_bb(bb,nx,ns,ny)
        call get_vol(vol,nx,ny)
        do is=0,ns
          tr(is)=myfun(ti,bb,vol,nx,ny,ns,is)
        enddo
        return
        end subroutine driver_1

      end module driver


      subroutine call_main(J)
      use driver
      implicit none
      real :: J
      call driver_1
      call cost_function(J)
      end subroutine call_main

      subroutine cost_function(J)
      use some_module
      implicit none
      real :: J,tmax
      integer :: is,ix,iy
      tmax = -1.0
      do is=0,ns
      do ix=0,nx
      do iy=0,ny
        if (tr(is).gt.tmax) then
          tmax = tr(ns)*ti(ix,iy)
        endif
      end do
      end do
      end do
      J = tmax
      end subroutine cost_function

      function myfun(quant,bb,vol,nx,ny,ns,is)
      implicit none
      integer nx, ny, ns, is
      real myfun
      real :: quant(-1:nx,-1:ny), bb(-1:nx,-1:ny,0:ns), vol(-1:nx,-1:ny)
      real :: qsum, qterm, wsum, bface
      integer iy, ix
      qsum=0
      wsum=0
      do ix=0,nx-1
        do iy=0,ny-1
          qterm=quant(ix,iy)-vol(ix,iy)*(-quant(ix,ny)+ &
     &          quant(ix,iy+1))/(vol(ix,ny)+vol(ix-1,iy))
          bface=bb(ix,iy,is)-vol(ix,iy)*(-bb(ix,iy,is)+ &
     &          bb(ix-1,iy,is))/ &
     &          (vol(ix,iy+1)+vol(ix-1,iy))
        enddo
        qsum=qsum+0.5_8*qterm/bface
        wsum=wsum+0.5_8*bface
      enddo
      myfun=qsum/wsum
      return
      end

      program myprogram
      use driver
      implicit none
      real :: J
      call driver_0
      call call_main(J)
      end program myprogram
