module twice_module 
implicit none
private
public :: twice
interface twice
   module procedure twice_real,twice_int
end interface twice
contains
elemental function twice_real(x) result(value)
real, intent(in) :: x 
real             :: value
value = 2*x
end function twice_real
!
elemental function twice_int(i) result(value)
integer, intent(in) :: i
integer             :: value
value = 2*i
end function twice_int
end module twice_module 
!
function test(x, i)
use twice_module, only: twice
real x, test
integer i
test = twice(x) + twice(i)
end function test
