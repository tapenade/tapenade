MODULE INTERVAL_ARITHMETICS
       TYPE INTERVAL
              REAL LOWER, UPPER
       END TYPE INTERVAL
       TYPE IINTERVAL
              integer LOWER, UPPER
       END TYPE IINTERVAL
       INTERFACE OPERATOR (+)
              MODULE PROCEDURE IINTERVAL_ADDITION, INTERVAL_ADDITION
       END INTERFACE
       INTERFACE OPERATOR (-)
              MODULE PROCEDURE INTERVAL_SUBTRACTION
       END INTERFACE
       INTERFACE OPERATOR (*)
              MODULE PROCEDURE INTERVAL_MULTIPLICATION
       END INTERFACE
       INTERFACE OPERATOR (/)
              MODULE PROCEDURE INTERVAL_DIVISION
       END INTERFACE
       INTERFACE assignment (=)
              MODULE PROCEDURE real_from_interval, interval_from_real
       END INTERFACE

CONTAINS
       FUNCTION INTERVAL_ADDITION(A, B)
              TYPE(INTERVAL), INTENT(IN) :: A, B
              TYPE(INTERVAL) :: INTERVAL_ADDITION
              INTERVAL_ADDITION%LOWER = A%LOWER + B%LOWER
              INTERVAL_ADDITION%UPPER = A%UPPER + B%UPPER
       END FUNCTION INTERVAL_ADDITION

       FUNCTION IINTERVAL_ADDITION(A, B)
              TYPE(IINTERVAL), INTENT(IN) :: A, B
              TYPE(IINTERVAL) :: IINTERVAL_ADDITION
              IINTERVAL_ADDITION%LOWER = A%LOWER + B%LOWER
              IINTERVAL_ADDITION%UPPER = A%UPPER + B%UPPER
       END FUNCTION IINTERVAL_ADDITION

       FUNCTION INTERVAL_SUBTRACTION(A, B)
              TYPE(INTERVAL), INTENT(IN) :: A, B
              TYPE (INTERVAL) :: INTERVAL_SUBTRACTION
              INTERVAL_SUBTRACTION%LOWER = A%LOWER - B%UPPER
              INTERVAL_SUBTRACTION%UPPER = A%UPPER - B%LOWER
       END FUNCTION INTERVAL_SUBTRACTION

       FUNCTION INTERVAL_MULTIPLICATION(A, B)
!             POSITIVE NUMBERS ASSUMED
              TYPE(INTERVAL), INTENT(IN) :: A, B
              TYPE (INTERVAL) :: INTERVAL_MULTIPLICATION
              INTERVAL_MULTIPLICATION%LOWER = A%LOWER * B%LOWER
              INTERVAL_MULTIPLICATION%UPPER = A%UPPER * B%UPPER
       END FUNCTION INTERVAL_MULTIPLICATION
       FUNCTION INTERVAL_DIVISION(A, B)
!             POSITIVE NUMBERS ASSUMED
              TYPE(INTERVAL), INTENT(IN) :: A, B
              TYPE(INTERVAL) :: INTERVAL_DIVISION
              INTERVAL_DIVISION%LOWER = A%LOWER / B%UPPER
              INTERVAL_DIVISION%UPPER = A%UPPER / B%LOWER
       END FUNCTION INTERVAL_DIVISION

       subroutine real_from_interval(a,b)
         real, intent(out) :: a
         type(interval), intent(in) :: b
         a = (b%lower + b%upper)/2
       end subroutine real_from_interval

       subroutine interval_from_real(a,b)
         type(interval), intent(out) :: a
         real, intent(in) :: b
         a%lower = b
         a%upper = b
       end subroutine interval_from_real

END MODULE INTERVAL_ARITHMETICS

program test
    USE INTERVAL_ARITHMETICS
    IMPLICIT NONE
    TYPE (INTERVAL) :: A, B, C, D, E, F
    A%LOWER = 6.9            
    A%UPPER = 7.1
    B%LOWER = 10.9           
    B%UPPER = 11.1
    WRITE (*,*) A, B
    C = A + B                
    D = A - B
    E = A * B                
    F = A / B
    WRITE (*,*) C, D         
    WRITE (*,*) E, F

      contains
      real function fun(a,b,ia,ib)
      TYPE (INTERVAL) :: A, B
      TYPE (iINTERVAL) :: iA, iB, ic
      fun = a + b
      ic = ia + ib
      return
      end function


    END
