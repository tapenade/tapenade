!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
MODULE INTERVAL_ARITHMETICS
  IMPLICIT NONE
  TYPE INTERVAL
      REAL :: lower, upper
  END TYPE INTERVAL
  TYPE IINTERVAL
      INTEGER :: lower, upper
  END TYPE IINTERVAL
  INTERFACE OPERATOR(+)
      MODULE PROCEDURE IINTERVAL_ADDITION, INTERVAL_ADDITION
  END INTERFACE

  INTERFACE OPERATOR(-)
      MODULE PROCEDURE INTERVAL_SUBTRACTION
  END INTERFACE

  INTERFACE OPERATOR(*)
      MODULE PROCEDURE INTERVAL_MULTIPLICATION
  END INTERFACE

  INTERFACE OPERATOR(/)
      MODULE PROCEDURE INTERVAL_DIVISION
  END INTERFACE

  INTERFACE ASSIGNMENT(=)
      MODULE PROCEDURE REAL_FROM_INTERVAL, INTERVAL_FROM_REAL
  END INTERFACE


CONTAINS
  FUNCTION INTERVAL_ADDITION(a, b)
    IMPLICIT NONE
    TYPE(INTERVAL), INTENT(IN) :: a, b
    TYPE(INTERVAL) :: interval_addition
    interval_addition%lower = a%lower + b%lower
    interval_addition%upper = a%upper + b%upper
  END FUNCTION INTERVAL_ADDITION

  FUNCTION IINTERVAL_ADDITION(a, b)
    IMPLICIT NONE
    TYPE(IINTERVAL), INTENT(IN) :: a, b
    TYPE(IINTERVAL) :: iinterval_addition
    iinterval_addition%lower = a%lower + b%lower
    iinterval_addition%upper = a%upper + b%upper
  END FUNCTION IINTERVAL_ADDITION

  FUNCTION INTERVAL_SUBTRACTION(a, b)
    IMPLICIT NONE
    TYPE(INTERVAL), INTENT(IN) :: a, b
    TYPE(INTERVAL) :: interval_subtraction
    interval_subtraction%lower = a%lower - b%upper
    interval_subtraction%upper = a%upper - b%lower
  END FUNCTION INTERVAL_SUBTRACTION

  FUNCTION INTERVAL_MULTIPLICATION(a, b)
    IMPLICIT NONE
!             POSITIVE NUMBERS ASSUMED
    TYPE(INTERVAL), INTENT(IN) :: a, b
    TYPE(INTERVAL) :: interval_multiplication
    interval_multiplication%lower = a%lower*b%lower
    interval_multiplication%upper = a%upper*b%upper
  END FUNCTION INTERVAL_MULTIPLICATION

  FUNCTION INTERVAL_DIVISION(a, b)
    IMPLICIT NONE
!             POSITIVE NUMBERS ASSUMED
    TYPE(INTERVAL), INTENT(IN) :: a, b
    TYPE(INTERVAL) :: interval_division
    interval_division%lower = a%lower/b%upper
    interval_division%upper = a%upper/b%lower
  END FUNCTION INTERVAL_DIVISION

  SUBROUTINE REAL_FROM_INTERVAL(a, b)
    IMPLICIT NONE
    REAL, INTENT(OUT) :: a
    TYPE(INTERVAL), INTENT(IN) :: b
    a = (b%lower+b%upper)/2
  END SUBROUTINE REAL_FROM_INTERVAL

  SUBROUTINE INTERVAL_FROM_REAL(a, b)
    IMPLICIT NONE
    TYPE(INTERVAL), INTENT(OUT) :: a
    REAL, INTENT(IN) :: b
    a%lower = b
    a%upper = b
  END SUBROUTINE INTERVAL_FROM_REAL

END MODULE INTERVAL_ARITHMETICS

PROGRAM TEST
  USE INTERVAL_ARITHMETICS
  IMPLICIT NONE
  TYPE(INTERVAL) :: a, b, c, d, e, f
  a%lower = 6.9
  a%upper = 7.1
  b%lower = 10.9
  b%upper = 11.1
  WRITE(*, *) a, b
  c = a + b
  d = a - b
  e = a * b
  f = a / b
  WRITE(*, *) c, d
  WRITE(*, *) e, f

CONTAINS
  REAL FUNCTION FUN(a, b, ia, ib)
    IMPLICIT NONE
    TYPE(INTERVAL) :: a, b
    TYPE(IINTERVAL) :: ia, ib, ic
    fun = a + b
    ic = ia + ib
    RETURN
  END FUNCTION FUN

END PROGRAM TEST

