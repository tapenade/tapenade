!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
! This is almost cm22, but the statement that triggers reinit of ad
!  is not the call top2() but the a=3.0.
! Make sure reinitialization ad = 0.0 not done too late !
SUBROUTINE TOP(a, b, r, p)
  IMPLICIT NONE
  REAL :: a, b, r
  REAL, POINTER :: p
  r = 3.5
  a = 1.0
!$AD NOCHECKPOINT
  CALL TOP2(a, b, r, p)
!     p points to a or b
  a = 3.0
  r = 2*p + a
END SUBROUTINE TOP

SUBROUTINE TOP2(a, b, r, p)
  IMPLICIT NONE
  REAL, TARGET :: a, b
  REAL :: r
  REAL, POINTER :: p
  IF (a .EQ. 2.0) THEN
    p => a
!     p points to a
  ELSE
    p => b
!     p points to b
  END IF
!     p points to a or b
  a = 2.0
END SUBROUTINE TOP2

