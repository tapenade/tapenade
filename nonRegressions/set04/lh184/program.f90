! This is almost cm22, but the statement that triggers reinit of ad
!  is not the call top2() but the a=3.0.
! Make sure reinitialization ad = 0.0 not done too late !
subroutine top(a, b, r, p)
  real :: a, b, r
  real, pointer :: p

  r = 3.5
  a = 1.0
!$AD NOCHECKPOINT
  call top2(a, b, r, p)
  a = 3.0
  r = 2 * p + a
end subroutine top

subroutine top2(a, b, r, p)
  real, target :: a, b
  real :: r
  real, pointer :: p

  if(a == 2.0) then
     p => a
  else
     p => b
  endif
  a = 2.0

end subroutine top2
