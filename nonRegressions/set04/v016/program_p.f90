!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
MODULE A
  IMPLICIT NONE

CONTAINS
  REAL FUNCTION F(t)
    IMPLICIT NONE
    REAL :: t
    INTRINSIC EXP
    f = t*t
    f = EXP(f)
    RETURN
  END FUNCTION F

  REAL FUNCTION G(t)
    IMPLICIT NONE
    REAL :: t
    INTRINSIC SIN
    g = 1
    IF (t .NE. 0.d0) THEN
      g = SIN(t)/t
    ELSE
      CALL ABORT(t)
    END IF
    g = g + t
    RETURN
  END FUNCTION G

  SUBROUTINE ABORT(t)
    IMPLICIT NONE
    REAL :: t
    STOP
  END SUBROUTINE ABORT

END MODULE A

MODULE B
  USE A
  IMPLICIT NONE

CONTAINS
  SUBROUTINE SUB(x, y)
    IMPLICIT NONE
    REAL :: x, y
    y = F(x) + G(x)
  END SUBROUTINE SUB

END MODULE B

