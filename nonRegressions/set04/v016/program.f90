module a
  contains
      real function f(t)
      real t
      f = t * t
      f = exp(f)
      return
      end

      real function g(t)
      real t
      g = 1
      if (t .ne. 0.d0) then
        g = sin(t) / t
      else
         call abort(t)
      endif
      g = g + t
      return
      end

      subroutine abort(t)
      stop
      end

end module

module b
  use a

contains
      subroutine sub(x,y)
      real x,y
      y = f(x) + g(x)
      end
end module
