module M
       TYPE INTERVAL
              REAL LOWER, UPPER
              character(256) :: name
       END TYPE INTERVAL

end module

module N
use M, interv => interval
      contains
      real function ff(t)
      use M, interv => interval
      type(interv) :: t
      ff = t%lower + t%upper
      return
      end function
end module


module O
use M, intrv => interval
use N
      contains
      real function gg(t)
      use M, intrv => interval
      type(intrv) :: t
      gg = t%lower + ff(t)
      return
      end function
end module
