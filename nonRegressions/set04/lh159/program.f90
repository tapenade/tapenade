! Bug found by Jan Hueckelheim.
! Behavior with records with an allocatable array field seems
! to be wrong and different from records with array field or pointer field,
! because understood differently when parsing the "-head" command-line arg.
module tapenadeTestModule
type typeHarmless
  real :: test
  real, dimension(1) :: foo
end type

type typeWithPointer
  double precision :: foo
  real, pointer :: test
end type

type typeWithAllocatable
  double precision :: bar
  real, allocatable, dimension(:) :: test
end type

contains

subroutine tapenadeTestRoutine(a,bone,btwo,bthree,done,dtwo,dthree)
  implicit none
  real, intent(in)  :: a ! harmless input variable
  real, intent(out) :: bone ! harmless output variable
  real, intent(out) :: btwo ! harmless output variable
  real, intent(out) :: bthree ! harmless output variable
  real, allocatable, dimension(:) :: done ! test allocatables
  type(typeWithAllocatable) :: dtwo       ! test type that contains allocatable
  type(typeHarmless) :: dthree            ! test type that contains allocatable

  ! do the same thing, once with an allocatable array, once with an allocatable
  ! array contained in a type. The result and differentiation should be the same
  done(1) = cos(done(1))
  dtwo%test(1) = cos(dtwo%test(1))
  dthree%foo(1) = cos(dthree%foo(1))

  bone = a*sin(done(1))
  btwo = a*sin(dtwo%test(1))
  bthree = a*sin(dthree%foo(1))
end subroutine

subroutine tapenadeTestCollection(a,b,c,d,e,t1,t2,t3)
  implicit none
  real, intent(in)  :: a ! harmless input variable
  real, intent(out) :: b ! harmless output variable
  real, pointer :: c     ! test how tapenade deals with pointers
  real, allocatable, dimension(:) :: d ! test allocatables
  real, dimension(:) :: e ! test assumed-size array
  type(typeHarmless) :: t1 ! test harmless type
  type(typeWithPointer) :: t2 ! test type that contains pointer
  type(typeWithAllocatable) :: t3 ! test type that contains allocatable
  real :: temp

  temp = a+c+d(1)+e(1)
  t3%test(1) = t3%test(1)+ temp+sin(t1%test)+cos(t2%test)+temp+t3%bar
  b = t3%test(1)
end subroutine
end module
