!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (develop) -  7 Oct 2019 15:43
!
!  Differentiation of foo in forward (tangent) mode:
!   variations   of useful results: y
!   with respect to varying inputs: x
!   RW status of diff variables: x:in y:out
! Bug "aimag" trouve par Michael Vossbeck
SUBROUTINE FOO_D(n, x, xd, y, yd)
  IMPLICIT NONE
  INTEGER :: n
  REAL, DIMENSION(n), INTENT(IN) :: x
  REAL, DIMENSION(n), INTENT(IN) :: xd
  REAL, INTENT(OUT) :: y
  REAL, INTENT(OUT) :: yd
  COMPLEX :: c
  COMPLEX :: cd
  INTRINSIC CMPLX
  INTRINSIC AIMAG
  IF (n .LT. 2) THEN
    PRINT*, 'ERROR::n must be >=2'
    STOP
  ELSE
    cd = CMPLX(xd(1), xd(2))
    c = CMPLX(x(1), x(2))
    yd = AIMAG(cd)
    y = AIMAG(c)
  END IF
END SUBROUTINE FOO_D

