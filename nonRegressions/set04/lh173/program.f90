! Bug "aimag" trouve par Michael Vossbeck
subroutine foo(n, x, y)
  implicit none
  integer n
  real, intent(in), dimension(n) :: x
  real, intent(out) :: y
  complex c

  if( n .lt. 2 ) then
     print*, 'ERROR::n must be >=2'
     stop
  end if
  c = cmplx( x(1), x(2) )

  y = aimag(c)

end subroutine foo
