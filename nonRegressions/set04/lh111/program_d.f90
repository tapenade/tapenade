!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) - 24 Feb 2022 12:13
!
! Bug trouve par jean.
! Le champ temporary0 est mal differentie,
!  cree dans GM_GEOM_D la ligne incomplete :
!     REAL, DIMENSION(:, :), POINTER ::  => 
MODULE M_DIFF
  IMPLICIT NONE
  TYPE GM_GEOM
      REAL, DIMENSION(:, :), POINTER :: temporary0 => NULL()
      REAL, DIMENSION(:, :), POINTER :: temporary1 => NULL()
      REAL, DIMENSION(:, :), POINTER :: thck => NULL()
      INTEGER, DIMENSION(:, :), POINTER :: mask => NULL()
      INTEGER, DIMENSION(:, :), POINTER :: thkmask => NULL()
      INTEGER :: totpts=0
      INTEGER, DIMENSION(4) :: dom=0
      LOGICAL :: empty=.true.
      DOUBLE PRECISION :: ivol, iarea, iareag, iareaf
  END TYPE GM_GEOM
  TYPE GM_GEOM_DIFF
      REAL, DIMENSION(:, :), POINTER :: temporary0 => NULL()
      REAL, DIMENSION(:, :), POINTER :: temporary1 => NULL()
      REAL, DIMENSION(:, :), POINTER :: thck => NULL()
  END TYPE GM_GEOM_DIFF

CONTAINS
!  Differentiation of allocg in forward (tangent) mode:
!   Plus diff mem management of: field:out
  SUBROUTINE ALLOCG_D(dim1, dim2, field, fieldd)
    IMPLICIT NONE
    INTEGER :: dim1, dim2
    REAL, DIMENSION(:, :), POINTER :: field
    REAL, DIMENSION(:, :), POINTER :: fieldd
    ALLOCATE(fieldd(dim1, dim2))
    ALLOCATE(field(dim1, dim2))
  END SUBROUTINE ALLOCG_D

  SUBROUTINE ALLOCG(dim1, dim2, field)
    IMPLICIT NONE
    INTEGER :: dim1, dim2
    REAL, DIMENSION(:, :), POINTER :: field
    ALLOCATE(field(dim1, dim2))
  END SUBROUTINE ALLOCG

END MODULE M_DIFF

!  Differentiation of foo in forward (tangent) mode:
!   variations   of useful results: [alloc*field in allocg] y
!   with respect to varying inputs: [alloc*field in allocg] x
!   RW status of diff variables: [alloc*field in allocg]:in-out
!                x:in y:out
SUBROUTINE FOO_D(x, xd, y, yd)
  USE M_DIFF
  IMPLICIT NONE
  TYPE(GM_GEOM) :: g
  TYPE(GM_GEOM_DIFF) :: gd
  REAL :: x
  REAL :: xd
  REAL :: y
  REAL :: yd
  CALL ALLOCG_D(2, 2, g%temporary0, gd%temporary0)
  CALL ALLOCG_D(2, 2, g%temporary1, gd%temporary1)
  CALL ALLOCG_D(2, 2, g%thck, gd%thck)
  gd%temporary0 = xd
  g%temporary0 = x
  gd%temporary1 = gd%temporary0
  g%temporary1 = g%temporary0
  gd%thck = gd%temporary1
  g%thck = g%temporary1
  yd = gd%thck(1, 1)
  y = g%thck(1, 1)
END SUBROUTINE FOO_D

SUBROUTINE FOO_NODIFF(x, y)
  USE M_DIFF
  IMPLICIT NONE
  TYPE(GM_GEOM) :: g
  REAL :: x
  REAL :: y
  CALL ALLOCG(2, 2, g%temporary0)
  CALL ALLOCG(2, 2, g%temporary1)
  CALL ALLOCG(2, 2, g%thck)
  g%temporary0 = x
  g%temporary1 = g%temporary0
  g%thck = g%temporary1
  y = g%thck(1, 1)
END SUBROUTINE FOO_NODIFF

