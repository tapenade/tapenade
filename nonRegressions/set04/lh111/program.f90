! Bug trouve par jean.
! Le champ temporary0 est mal differentie,
!  cree dans GM_GEOM_D la ligne incomplete :
!     REAL, DIMENSION(:, :), POINTER ::  => 
module m
  TYPE GM_GEOM
      REAL, DIMENSION(:, :), POINTER :: temporary0 => NULL()
      REAL, DIMENSION(:, :), POINTER :: temporary1 => NULL()
      REAL, DIMENSION(:, :), POINTER :: thck => NULL()
      INTEGER, DIMENSION(:, :), POINTER :: mask => NULL()
      INTEGER, DIMENSION(:, :), POINTER :: thkmask => NULL()
      INTEGER :: totpts=0
      INTEGER, DIMENSION(4) :: dom=0
      LOGICAL :: empty=.true.
      double precision :: ivol, iarea, iareag, iareaf
  END TYPE GM_GEOM
contains 
subroutine allocG(dim1,dim2, field)
  integer :: dim1, dim2
  REAL, DIMENSION(:, :), POINTER :: field
  allocate(field(dim1,dim2))
end subroutine
end module 


subroutine foo(x,y)
  use m 
  type(GM_GEOM) :: g
  call allocG(2,2,g%temporary0)
  call allocG(2,2,g%temporary1)
  call allocG(2,2,g%thck)
  g%temporary0=x
  g%temporary1=g%temporary0
  g%thck=g%temporary1
  y=g%thck(1,1)
end subroutine

program main
  real :: x,y 

  x=2.0
  call foo(x,y)
  print *,y
end program
