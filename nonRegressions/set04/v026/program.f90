module def
      integer, parameter :: &
&                      int_kind  = kind(1),&
&                      dbl_kind  = selected_real_kind(13)

      integer (kind=int_kind), parameter :: &
&  imt_global=100,&
&  jmt_global=116,&
&  ncat       =   5,&   ! number of categories
&  nilyr      =   4        ! number of layers in a single category


      integer (kind=int_kind), parameter ::  &
&  num_ghost_cells = 1
      integer (kind=int_kind), parameter ::&
&  imt_local = (imt_global-1)/1 + 1 + 2*num_ghost_cells,&
&  jmt_local = (jmt_global-1)/1 + 1 + 2*num_ghost_cells

      integer (kind=int_kind), parameter :: &
&  ilo = num_ghost_cells+1, &! beg index of actual physical subdomain
&  ihi = imt_local - num_ghost_cells, &   ! end index 
&  jlo = num_ghost_cells+1,    &  ! beg index
&  jhi = jmt_local - num_ghost_cells  ! end index 
end module def

module usedef
  use def
      real (kind=dbl_kind), save, private :: qsnon(ncat,ilo:ihi,jlo:jhi)
end module usedef


      double precision function f(t)
      double precision t
      integer, parameter :: &
&                      int_kind  = kind(1),&
&                      dbl_kind  = selected_real_kind(13)

      integer (kind=int_kind), parameter :: &
&  imt_global=100,&
&  jmt_global=116,&
&  ncat       =   5, &       ! number of categories
&  nilyr      =   4        ! number of layers in a single category


      integer (kind=int_kind), parameter :: & 
&  num_ghost_cells = 1
      integer (kind=int_kind), parameter ::&
&  imt_local = (imt_global-1)/1 + 1 + 2*num_ghost_cells,&
&  jmt_local = (jmt_global-1)/1 + 1 + 2*num_ghost_cells

      integer (kind=int_kind), parameter :: &
&  ilo = num_ghost_cells+1, &! beg index of actual physical subdomain
&  ihi = imt_local - num_ghost_cells, &! end index 
&  jlo = num_ghost_cells+1,      &! beg index
&  jhi = jmt_local - num_ghost_cells  ! end index 

      real (kind=dbl_kind), save :: qsnon(ncat,ilo:ihi,jlo:jhi)

      f = t * t
      f = exp(f)
      return
      end
