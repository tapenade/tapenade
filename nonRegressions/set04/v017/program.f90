module z
   type node_type
      sequence
       double precision X
       double precision Y
   end type
   type node_type1
      sequence
      private
       double precision X
       double precision Y
   end type
   type node_type2
      private
      sequence
       double precision X
       double precision Y
   end type

   integer, parameter:: n=100
   type(node_type) :: ff(n)
   double precision :: bval
   COMMON/vars/ff, bval
end module
