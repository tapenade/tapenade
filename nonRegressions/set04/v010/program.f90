module M
  real a,b,c
  private c
contains
  subroutine S1(x)
    real x
    a = x*x
  end
  real function F1()
    F1 = a-2
  end function F1
  real function F2()
    F2 = a-2
  end function F2
end module

subroutine test(x,y)
  use M
  real x,y,ad
  a = 2 * x
  y = a + 1
  a = 0.0
  call S1(y)
  y = 3.0
  y = y+F1()+F2()
end
