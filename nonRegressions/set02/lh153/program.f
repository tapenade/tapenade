C Pour tester le recalcul partiel dans les cas simples,
C  a la place du PUSH/POP TBR standard.
      real FUNCTION comp(AT,BT,N,IND1,IND2)
      real AT(*),BT(*),CT(1000),val,val2
      integer i,N,IND1(*),IND2(*),n1,n2
      integer FUN1
      DO i = 1,N
         CT(i) = 0.0
      ENDDO
      DO i = 1,N
         n1 = FUN1(i,IND1)
         CT(n1) = CT(n1)*AT(i)
         n2 = IND2(n1)
         val = AT(n2)*AT(n1)/2.5
         val2 = SQRT(val*BT(i))
         CT(n2) = CT(n2)+val2
         BT(n1) = n2*val
      ENDDO
      comp = 0.0
      DO i = 1,100
         n2 = IND2(i)
         comp = comp + BT(i)*CT(n2)
      ENDDO
      end

      integer FUNCTION FUN1(j,IND)
      integer j,IND(*)
      FUN1 = j+IND(j)
      END
