      subroutine PROC(y, f)
      REAL*8 y(0:3),f(0:3)
      REAL*8 ONE, eps
c
      ONE = 1.d0
      eps = 1.d-2
      f(0) = (((ONE / (eps * eps * eps)) *
     +     ( -y(0) + ( y(1) / (ONE + y(1)))) -
     +     ( y(1) / ((y(1) + ONE) * (y(1) + ONE)))) -
     +     ( y(2) / ((y(2) + ONE) * (y(2) + ONE)))) -
     +     ( y(3) / ((y(3) + ONE) * (y(3) + ONE)))

      f(1) = (( ONE / (eps * eps)) *
     +     ( -y(1) + ( y(2) / (ONE + y(2)))) -
     +     ( y(2) / ((y(2) + ONE) * (y(2) + ONE)))) -
     +     ( y(3) / ((y(3) + ONE) * (y(3) + ONE)))

      f(2) = ( ONE / eps ) * ( -y(2) +
     +     ( y(3) / (ONE + y(3)))) -
     +     ( y(3) / ((y(3) + ONE) * (y(3) + ONE)))

      f(3) = -y(3)
c
      end
