C Bug rencontre sur selmin-uns2d
C en mode inverse avec l'option -nooptim spareinit
C Il y a des calculs dead-adjoint de
C    tt(i)=tt(i)+-pp(i)
C qui etaient conserves a cause du spareinit.
C (interaction possible avec les II-LOOP).
      SUBROUTINE diffar(pp,tt,rr)
      REAL pp(99),tt(99),rr
      INTEGER i
c
      DO i=1,99
         tt(i) = tt(i)+pp(i)
      ENDDO
c
      rr = tt(17)*tt(14)
c
C$AD II-LOOP
      DO i=1,99
         tt(i) = tt(i)-pp(i)
      ENDDO
c
      END
