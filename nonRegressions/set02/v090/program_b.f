C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.13 (r7047) -  7 Sep 2018 18:03
C
C  Differentiation of top in reverse (adjoint) mode (with options no!SpareInits no!MergeDiffInstrs):
C   gradient     of useful results: i3 o1
C   with respect to varying inputs: i1 i2 i3 o1 o2 o3
C   RW status of diff variables: i1:zero i2:out i3:in-out o1:in-zero
C                o2:zero o3:zero
      SUBROUTINE TOP_B(i1, i1b, i2, i2b, i3, i3b, o1, o1b, o2, o2b, o3, 
     +                 o3b)
      IMPLICIT NONE
      REAL i1, i2, i3
      REAL i1b, i2b, i3b
      REAL o1, o2, o3
      REAL o1b, o2b, o3b
      REAL x1implicit
C      real x1implicit pour faire fiximplicit (from lha06)
      DIMENSION x1implicit(2)
      REAL G
      INTEGER*4 branch
C
      i1 = 2.0
      IF (i3 .LT. 0.0) THEN
        i3 = G(i1, i2)
        CALL PUSHCONTROL1B(0)
      ELSE
        CALL PUSHCONTROL1B(1)
      END IF
      i1 = G(i2, i3)
      i2 = 2.3
      CALL PUSHREAL4(i1)
      CALL SUB1(i1, i2, o1, o2)
      i2b = 0.0
      o3b = 0.0
      o1b = o2*i2*o1b
      CALL POPREAL4(i1)
      CALL SUB1_B(i1, i1b, i2, o1, o1b, o2)
      o3b = 0.0
      i2b = 0.0
      CALL G_B(i2, i2b, i3, i3b, i1b)
      CALL POPCONTROL1B(branch)
      IF (branch .EQ. 0) CALL G_B(i1, i1b, i2, i2b, i3b)
      i1b = 0.0
      END

C  Differentiation of sub1 in reverse (adjoint) mode (with options no!SpareInits no!MergeDiffInstrs):
C   gradient     of useful results: o1
C   with respect to varying inputs: i1
C
      SUBROUTINE SUB1_B(i1, i1b, i2, o1, o1b, o2)
      IMPLICIT NONE
      REAL i1, i2
      REAL i1b
      REAL o1, o2
      REAL o1b
      REAL x1, x2
      REAL x1b, x2b
      INTEGER*4 branch
C
      x2 = 5.0
      x1 = i1*i2
      IF (i1 .GT. 3.0) THEN
        x2 = x2 + i1 - 3*i2
        CALL PUSHCONTROL1B(0)
      ELSE
        x2 = 12
        x1 = 2*x1 + x2
        CALL PUSHCONTROL1B(1)
      END IF
      i1b = 0.0
      x1b = 0.0
      x2b = 0.0
      i1b = 0.0
      x1b = x1b + o1b/x2
      x2b = x2b - x1*o1b/x2**2
      o1b = 0.0
      CALL POPCONTROL1B(branch)
      IF (branch .EQ. 0) THEN
        i1b = i1b + x2b
        x2b = 0.0
      ELSE
        x1b = 2*x1b
        x2b = 0.0
      END IF
      i1b = i1b + i2*x1b
      x1b = 0.0
      x2b = 0.0
      END

C  Differentiation of g in reverse (adjoint) mode (with options no!SpareInits no!MergeDiffInstrs):
C   gradient     of useful results: g i2
C   with respect to varying inputs: i1 i2
C
      SUBROUTINE G_B(i1, i1b, i2, i2b, gb)
      IMPLICIT NONE
      REAL i1, i2
      REAL i1b, i2b
      REAL g
      REAL gb
      i1b = 0.0
      i1b = i1b + gb
      i2b = i2b - gb
      gb = 0.0
      END
C

