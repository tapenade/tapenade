      subroutine top(i1,i2,i3,o1,o2,o3)
      real i1, i2, i3
      real o1, o2, o3
C      real x1implicit pour faire fiximplicit (from lha06)
      dimension x1implicit(2)
c
      i1 = 2.0
      if (i3.lt.0.0) i3 = g(i1,i2)
      i1 = g(i2,i3)
      i2 = 2.3
      x1implicit(1) = g(i1,i3)
      o3 = i3*i2
      call sub1(i1, i2, o1, o2)
      o1 = o1*o2*i2
      o3 = 2.0
      i2 = 5.0
      end

      subroutine sub1(i1, i2, o1, o2)
      real i1, i2
      real o1, o2
      real x1, x2
c
      x2 = 5.0
      x1 = i1*i2
      if (i1.gt.3.0) then
         x2 = x2 + i1-3*i2
      else
         x2 = 12
         x1 = 2*x1+x2
      endif
      o1 = x1/x2
      o2 = 35.0
      i1 = 99.0
      end

      real function g(i1,i2)
      real i1,i2
      g = i1-i2
      end

