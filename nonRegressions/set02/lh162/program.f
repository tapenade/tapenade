C This is the illustration example for the FAQ item
C about initialization of derivative variables and
C the link with independent and dependent sets.
      subroutine F(u2,v1,v3,w2)
      real u1,u2,u3,v1,v2,v3,w1,w2,w3
      common /cc/u1,u3,v2,w1,w3
c
      u3 = u3+v1*u1
      u3 = u3-u2*v2
      u2 = 3.0*u1*v1
      u3 = u3+G(u2)
c
      v3 = v3+2.0*v1
      v3 = v3-v2
      v2 = 3.0*v1
      v3 = v3+G(v2)
c
      w3 = w3+v1*v2
      w3 = w3-w2*u1
      w2 = 3.0*w1
      w3 = w3+G(w2)
c
      end

      real function G(x)
      real x
      G = x*x
      end
