C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.7 (r4904M) - 19 Jun 2013 11:11
C
C  Differentiation of test in reverse (adjoint) mode:
C   gradient     of useful results: d a b c
C   with respect to varying inputs: d a b c
C   RW status of diff variables: d:in-out a:in-zero b:in-out c:in-out
      SUBROUTINE TEST_B(a, ab, b, bb, c, cb, d, db)
      IMPLICIT NONE
      REAL a, b, c, d
      REAL ab, bb, cb, db
      EXTERNAL F2
      EXTERNAL F2_B
      TYPE(UNKNOWNTYPE) F2
      EXTERNAL F1
      EXTERNAL F1_B
      REAL F1
      TYPE(UNKNOWNTYPE) result1
      CALL PUSHREAL4(d)
      CALL PUSHREAL4(c)
      result1 = F2(c, d)
      CALL PUSHUNKNOWNTYPE(result1)
      CALL PUSHREAL4(b)
      a = F1(b, result1)
      ab = 2*a*ab
      CALL POPREAL4(b)
      CALL POPUNKNOWNTYPE(result1)
      CALL F1_B(b, bb, result1, ab)
      CALL POPREAL4(c)
      CALL POPREAL4(d)
      CALL F2_B(c, cb, d, db)
      ab = 0.0
      END

