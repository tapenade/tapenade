C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.15 (feature_bugFixes) - 11 Dec 2020 16:25
C
C  Differentiation of test in forward (tangent) mode:
C   variations   of useful results: d a b c
C   with respect to varying inputs: d b c
C   RW status of diff variables: d:in-out a:out b:in-out c:in-out
      SUBROUTINE TEST_D(a, ad, b, bd, c, cd, d, dd)
      IMPLICIT NONE
      REAL a, b, c, d
      REAL ad, bd, cd, dd
      EXTERNAL F2
      EXTERNAL F2_D
      TYPE(UNKNOWNTYPE) F2
      EXTERNAL F1
      EXTERNAL F1_D
      REAL F1
      REAL F1_D
      TYPE(UNKNOWNTYPE) result1
      result1 = F2_D(c, cd, d, dd)
      ad = F1_D(b, bd, result1, a)
      ad = 2*a*ad
      a = a*a
      END

