C filename="K:\Projects\Beucker\LP\Adifor\funeval.f"
C--------1---------2---------3---------4---------5---------6---------7
C
      SUBROUTINE funeval(A,b,x,n,k,p,funval,h1,lambda)
C
C     Given a k-dimensional vector x  this subroutine evaluates
C     the function
C                        f: R^k  -->  R
C                           x   |-->  || Ax - b ||_p^p
C     where, as usual, ||.||_p is the vector p-norm, 
C     b is an n-dimensional vector and A is an n x k matrix.
C     Here, the matrix A and the vector b involve a lot of
C     structure. More precisely, A and b are of the following form
C
C                      |        A^hat     |       | b^hat |
C                 A =  |                  |   b = |       |
C                      | lambda * I_(kxk) |       |  0_k  |
C
C     where lambda \in R is a regularization parameter, 
C     I_(kxk) is the kxk identity matrix and 0_k is the null vector
C     of dimension k. The code below exploits that structure.
C
C     On Input:
C     A      = n x k matrix
C     b      = n-dimensional vector
C     n      = integer, number of rows of A
C     k      = integer, number of cols of A
C     p      = specifies the norm being used
C     x      = k-dimensional vector
C
C     On Exit:
C     funval = the result f(x)         
C     h1     = residuals := Ax-b
C
C
C     Author: Martin Buecker
C
C     History:
C
C     07/17/01: Original implementation based on a previous
C               version by Roland Beucker.

      IMPLICIT NONE

      INTEGER nmax, kmax
      INCLUDE 'parameter.h' 
C     declaring PARAMETER (nmax=..., kmax=...)

      INTEGER i, j, k, n
      DOUBLE PRECISION funval
C     The result of the function evaluation

      DOUBLE PRECISION lambda, p
C     Regularization parameter and chosen p-norm

      DOUBLE PRECISION A(nmax,kmax)
C     A contains the n x k matrix of Ax=b assuming maximal rank

      DOUBLE PRECISION x(kmax), b(nmax)
C     x contains the k-dimensional input
C     b contains the n-dimensional right-hand side

      DOUBLE PRECISION h1(nmax),h2

      DOUBLE PRECISION dzero, done
      PARAMETER (dzero=0.0d0, done=1.0d0)

C     As described in the above comment, there is a structure:
C     rows 1, ..., (N-K) of A are dense while the remaining
C     rows form a square matrix, more precisely the kxk identity
C     scaled by lambda.

C     Consider the dense part of A and b
      funval = dzero
      DO i = 1,n-k
         h1(I) = dzero
         DO j = 1,k
            h1(I) = h1(I) + A(i,j)*x(j)
         ENDDO
         h1(i) = h1(i) - b(i)
         h2 = DABS(h1(i))
         funval = funval + h2**p
      ENDDO

C     Finally, the "sparse part"
      DO i = n-k+1,n
         h1(i)  = lambda*x(i+k-n)
         funval = funval + DABS(H1(i))**p
      ENDDO

      RETURN
      END

