C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:15
C
C filename="K:\Projects\Beucker\LP\Adifor\funeval.f"
C--------1---------2---------3---------4---------5---------6---------7
C
      SUBROUTINE FUNEVAL(a, b, x, n, k, p, funval, h1, lambda)
      IMPLICIT NONE
C
      INTEGER nmax, kmax
      INCLUDE 'parameter.h'
C     declaring PARAMETER (nmax=..., kmax=...)
C
      INTEGER i, j, k, n
      DOUBLE PRECISION funval
C     The result of the function evaluation
C
      DOUBLE PRECISION lambda, p
C     Regularization parameter and chosen p-norm
C
      DOUBLE PRECISION a(nmax, kmax)
C     A contains the n x k matrix of Ax=b assuming maximal rank
C
      DOUBLE PRECISION x(kmax), b(nmax)
C     x contains the k-dimensional input
C     b contains the n-dimensional right-hand side
C
      DOUBLE PRECISION h1(nmax), h2
C
      DOUBLE PRECISION dzero, done
      PARAMETER (dzero=0.0d0, done=1.0d0)
      INTRINSIC DABS
      DOUBLE PRECISION dabs0
C
C     As described in the above comment, there is a structure:
C     rows 1, ..., (N-K) of A are dense while the remaining
C     rows form a square matrix, more precisely the kxk identity
C     scaled by lambda.
C
C     Consider the dense part of A and b
      funval = dzero
      DO i=1,n-k
        h1(i) = dzero
        DO j=1,k
          h1(i) = h1(i) + a(i, j)*x(j)
        ENDDO
        h1(i) = h1(i) - b(i)
        IF (h1(i) .GE. 0.) THEN
          h2 = h1(i)
        ELSE
          h2 = -h1(i)
        END IF
        funval = funval + h2**p
      ENDDO
C
C     Finally, the "sparse part"
      DO i=n-k+1,n
        h1(i) = lambda*x(i+k-n)
        IF (h1(i) .GE. 0.) THEN
          dabs0 = h1(i)
        ELSE
          dabs0 = -h1(i)
        END IF
        funval = funval + dabs0**p
      ENDDO
C
      RETURN
      END
C

