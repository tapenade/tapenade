C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.6 (r4629M) -  6 Nov 2012 08:31
C
C  Differentiation of test in reverse (adjoint) mode:
C   gradient     of useful results: a b
C   with respect to varying inputs: a b
C   RW status of diff variables: a:in-out b:in-out
C un bug trouve sur lauvernet-multisailsmac
C en mode -nooptim activity, mais independant de ce nooptim
C (1) le CLOSE doit etre recopie dans l'adjoint
C (2) il y a des ad/ab en trop.
      SUBROUTINE TEST_B(a, ab, b, bb)
      IMPLICIT NONE
      REAL a, b
      REAL ab, bb
      CALL PUSHREAL4(b)
      b = a*b
      CALL PUSHREAL4(a)
      CALL MYREAD(a, b)
      bb = bb + a**2*ab
      CALL POPREAL4(a)
      CALL MYREAD_B(a, b, bb)
      CALL POPREAL4(b)
      ab = b*bb
      bb = a*bb
      END

C  Differentiation of myread in reverse (adjoint) mode:
C   gradient     of useful results: b
C   with respect to varying inputs: b
C
      SUBROUTINE MYREAD_B(a, b, bb)
      IMPLICIT NONE
      REAL a, b
      REAL bb
      OPEN(unit=18, file='toto') 
      READ(18, *) 
      CALL PUSHREAL4(a)
      READ(18, *) a
      CLOSE(18) 
      bb = a*bb
      CALL POPREAL4(a)
      END

