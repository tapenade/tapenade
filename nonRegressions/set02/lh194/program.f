c   Mix of BcastAllreduceSumReduceSum and IsendRecvWaitTwoWay

      SUBROUTINE head(x,y)
      implicit none
      include 'ampi/ampif.h'
      DOUBLE PRECISION x,y,t
      INTEGER world_rank, err_code
      INTEGER i
      INTRINSIC SIN

      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      IF (world_rank.EQ.0) THEN
         x = x * 2
      ENDIF
      call AMPI_Bcast(x,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,
     +   err_code)
      x = SIN( x ) * (world_rank + 1) 
      call AMPI_Allreduce(x,t,1,MPI_DOUBLE_PRECISION,MPI_SUM,
     +   MPI_COMM_WORLD,err_code)
      x = SIN( x * t )
      IF (world_rank.EQ.0) THEN
         call AMPI_Send(x, 1, MPI_DOUBLE_PRECISION, 1, 0,
     +        AMPI_TO_RECV, MPI_COMM_WORLD, err_code)
      ELSE IF (world_rank.EQ.1) THEN
         call AMPI_Recv(y, 1, MPI_DOUBLE_PRECISION, 0, 0,
     +        AMPI_FROM_SEND, MPI_COMM_WORLD,
     +        MPI_STATUS_IGNORE, err_code)
      ENDIF
      call AMPI_Reduce(x,y,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,
     +   MPI_COMM_WORLD,err_code)
      IF (world_rank.EQ.0) THEN
         y = y * 3
      ENDIF
 
      END SUBROUTINE

      PROGRAM main
      implicit none
      include 'ampi/ampif.h'
      INTEGER world_rank, err_code
      DOUBLE PRECISION x,y

      call AMPI_Init_NT(err_code)
      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      IF (world_rank.EQ.0) THEN
         x = 3.5
      ENDIF
      call head(x,y)
      IF (world_rank.EQ.0) THEN
         print *, 'rank 0: process 0 got number ',y
      ENDIF
      call AMPI_Finalize_NT(err_code)
      END
