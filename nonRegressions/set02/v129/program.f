      SUBROUTINE DGETRF( M, N, A, LDA, IPIV, INFO )
      INTEGER            INFO, LDA, M, N
      INTEGER            IPIV( * )
      DOUBLE PRECISION   A( LDA, * )
      DOUBLE PRECISION   ONE
      PARAMETER          ( ONE = 1.0D+0 )
      INTEGER            I, IINFO, J, JB, NB
      EXTERNAL           DGEMM, DGETF2, DLASWP, DTRSM, XERBLA
      INTEGER            ILAENV
      EXTERNAL           ILAENV
      INTRINSIC          MAX, MIN

      INFO = 0
      NB = ILAENV( 1, 'DGETRF', ' ', M, N, -1, -1 )
      IF( NB.LE.1 .OR. NB.GE.MIN( M, N ) ) THEN
         CALL DGETF2( M, N, A, LDA, IPIV, INFO )
      ELSE

         DO 20 J = 1, MIN( M, N ), NB

            CALL DGETF2( M-J+1, JB, A( J, J ), LDA, IPIV( J ), IINFO )

            DO 10 I = J, MIN( M, J+JB-1 )
               IPIV( I ) = J - 1 + IPIV( I )
   10       CONTINUE

            CALL DLASWP( J-1, A, LDA, J, J+JB-1, IPIV, 1 )

            IF( J+JB.LE.N ) THEN

               CALL DLASWP( N-J-JB+1, A( 1, J+JB ), LDA, J, J+JB-1,
     $                      IPIV, 1 )

            END IF
   20    CONTINUE
      END IF
      RETURN

      END
