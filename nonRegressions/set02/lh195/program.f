C Test of cases where communications on MPI_INTEGERs
C should or should not be "differentiated".
C Rationale: Since a wait doesn't know statically what
C  it waits for, it may wait for an active value, so it
C  must always become e.g. TLS_AMPI_Wait.
C  Therefore all Isend and Irecv, even on MPI_INTEGERs must
C  always become TLS_AMPI_Ixxxx(), but their buffers, if
C  they are MPI_INTEGERs, must not be differentiated!
C  On the other hand, plain Send or Recv on MPI_INTEGERs
C  need not be differentiated.

      SUBROUTINE head(x,y)
      implicit none
      include 'ampi/ampif.h'
      DOUBLE PRECISION x,y
      INTEGER world_rank, err_code, ival
      INTEGER reqr, reqi, req1, req2
      DOUBLE PRECISION local

      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      ival = 8+world_rank
C In the following the Isend/Irecv on integers must be differentiated!
      IF (world_rank.EQ.0) THEN
         x=x*2
         call AMPI_ISend(x, 1, MPI_DOUBLE_PRECISION, 1, 42,
     +        AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD, reqr, err_code)
         call AMPI_ISend(ival, 1, MPI_INTEGER, 1, 75,
     +        AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD, reqi, err_code)
         IF (ival.le.10) THEN
            req1 = reqr
            req2 = reqi
         ELSE
            req1 = reqi
            req2 = reqr
         ENDIF
C Now we don't know which of req1 and req2 is on MPI_INTEGERs:
         call AMPI_Wait(req1,MPI_STATUS_IGNORE, err_code)
         call AMPI_Wait(req2,MPI_STATUS_IGNORE, err_code)
         y=y*3
      ELSE IF (world_rank.EQ.1) THEN
         call AMPI_IRecv(y, 1, MPI_DOUBLE_PRECISION, 0, 42,
     +        AMPI_FROM_ISEND_WAIT, MPI_COMM_WORLD, reqr, err_code)
         call AMPI_IRecv(ival, 1, MPI_INTEGER, 0, 75,
     +        AMPI_FROM_ISEND_WAIT, MPI_COMM_WORLD, reqi, err_code)
         IF (ival.le.10) THEN
            req1 = reqr
            req2 = reqi
         ELSE
            req1 = reqi
            req2 = reqr
         ENDIF
         call AMPI_Wait(req1,MPI_STATUS_IGNORE, err_code)
         call AMPI_Wait(req2,MPI_STATUS_IGNORE, err_code)
      ENDIF

C But plain send/recv on integers need not be differentiated:
      ival = ival + 5
      IF( world_rank.EQ.0 ) THEN
         call AMPI_Send(ival, 1, MPI_INTEGER, 1, 38,
     +        AMPI_TO_RECV, MPI_COMM_WORLD, err_code)
      ELSE IF ( world_rank.EQ.1 ) THEN
         call AMPI_Recv(ival, 1, MPI_INTEGER, 0, 38,
     +        AMPI_FROM_SEND, MPI_COMM_WORLD, MPI_STATUS_IGNORE,
     +        err_code)
      ENDIF
      y = y*ival

      END

      PROGRAM main
      implicit none
      include 'ampi/ampif.h'
      INTEGER world_rank, err_code
      DOUBLE PRECISION x,y

      call AMPI_Init_NT(err_code)
      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      x=3.5
      y=2.5
      call head(x,y)
      y=y+x
      call AMPI_Finalize_NT(err_code)
      END
