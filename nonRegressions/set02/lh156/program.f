C The 2nd example from our famous "polygon" optimization training !!

C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.3 (r3163) - 09/25/2009 09:03
C
C  Differentiation of polycost in forward (tangent) mode:
C   variations  of output variables: polycost
C   with respect to input variables: x y
C
      FUNCTION POLYCOST_D(x, xd, y, yd, ns, polycost)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 polycost
      REAL*8 polycost_d
      REAL*8 POLYSURF, POLYPERIM
      REAL*8 POLYSURF_D, POLYPERIM_D
      REAL*8 x(ns), y(ns)
      REAL*8 xd(ns), yd(ns)
      REAL*8 perim
      REAL*8 perimd
      REAL*8 result1
      REAL*8 result1d
C
      perimd = POLYPERIM_D(x, xd, y, yd, ns, perim)
      result1d = POLYSURF_D(x, xd, y, yd, ns, result1)
      polycost_d = ((perimd*perim+perim*perimd)*result1-perim**2*
     +  result1d)/result1**2
      polycost = perim*perim/result1
      RETURN
      END

C  Differentiation of polyperim in forward (tangent) mode:
C   variations  of output variables: polyperim
C   with respect to input variables: x y
      FUNCTION POLYPERIM_D(x, xd, y, yd, ns, polyperim)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 polyperim, x(ns), y(ns)
      REAL*8 polyperim_d, xd(ns), yd(ns)
      INTEGER cp, pp
      REAL*8 dx, dy
      REAL*8 dxd, dyd
      REAL*8 arg1
      REAL*8 arg1d
      REAL*8 result1
      REAL*8 result1d
      INTRINSIC SQRT
C
      polyperim = 0.0
      polyperim_d = 0.0
      DO cp=1,ns
        pp = cp - 1
        IF (pp .EQ. 0) pp = ns
        dxd = xd(cp) - xd(pp)
        dx = x(cp) - x(pp)
        dyd = yd(cp) - yd(pp)
        dy = y(cp) - y(pp)
        arg1d = dxd*dx + dx*dxd + dyd*dy + dy*dyd
        arg1 = dx*dx + dy*dy
        IF (arg1 .EQ. 0.0) THEN
          result1d = 0.0
        ELSE
          result1d = arg1d/(2.0*SQRT(arg1))
        END IF
        result1 = SQRT(arg1)
        polyperim_d = polyperim_d + result1d
        polyperim = polyperim + result1
      ENDDO
      RETURN
      END

C  Differentiation of polysurf in forward (tangent) mode:
C   variations  of output variables: polysurf
C   with respect to input variables: x y
C
      FUNCTION POLYSURF_D(x, xd, y, yd, ns, polysurf)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 polysurf, x(ns), y(ns)
      REAL*8 polysurf_d, xd(ns), yd(ns)
      INTEGER cp, pp
C
      polysurf = 0.0
      polysurf_d = 0.0
      DO cp=1,ns
        pp = cp - 1
        IF (pp .EQ. 0) pp = ns
        polysurf_d = polysurf_d + (xd(cp)*y(pp)+x(cp)*yd(pp)-xd(pp)*y(cp
     +    )-x(pp)*yd(cp))/2
        polysurf = polysurf + (x(cp)*y(pp)-x(pp)*y(cp))/2
      ENDDO
      RETURN
      END

