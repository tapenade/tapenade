      subroutine M33INV_DET(A, AINV, DET, OK_FLAG)
      IMPLICIT NONE

      DOUBLE PRECISION, DIMENSION(3,3), INTENT(IN)  :: A
      DOUBLE PRECISION, DIMENSION(3,3), INTENT(OUT) :: AINV
      LOGICAL, INTENT(OUT) :: OK_FLAG

      DOUBLE PRECISION, PARAMETER :: EPS = 1.0D-10
      DOUBLE PRECISION :: DET
      DOUBLE PRECISION, DIMENSION(3,3) :: COFACTOR


      DET =   A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2)  
     &      - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1) 
     &      + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1)

      COFACTOR(1,1) = +(A(2,2)*A(3,3)-A(2,3)*A(3,2))

      RETURN
      END

