c Pour tester la protection contre les discontinuites
c de arc-sinus, arc-cosinus, etc...
      subroutine testNonDiff(x,y)
      real x,y
      y = y + asin(x*x)
      y = y + acos(x)
      y = y + atan(x*x+2.5)
      y = y + sqrt(x*x+5.0)
      y = y*y
      end

