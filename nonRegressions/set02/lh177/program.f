C The example from our famous "polygon" optimization training !!

      SUBROUTINE INCRSQRT(pp, dxx, dyy)
      REAL*8 pp, dxx, dyy, xx2, yy2
      xx2 = dxx*dxx
      yy2 = dyy*dyy
      xx2 = xx2+0.001*yy2
      yy2 = yy2-0.001*xx2
C$AD DEBUG-HERE befsqrt true true
      pp = pp + SQRT(xx2+yy2)
      END

      FUNCTION POLYPERIM(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 POLYPERIM, X(ns), Y(ns)
      INTEGER cp,pp
      REAL*8 dx,dy
c
      POLYPERIM = 0.0
      DO cp=1,ns
         pp = cp-1
         IF (pp.EQ.0) pp=ns
C$AD DEBUG-HERE CP4 cp.eq.4
         dx = X(cp)-X(pp)
         dy = Y(cp)-Y(pp)
         CALL INCRSQRT(POLYPERIM, dx, dy)
         POLYPERIM = POLYPERIM + SQRT(dx*dx+dy*dy)
      ENDDO
      RETURN
      END

      FUNCTION POLYSURF(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 POLYSURF, X(ns), Y(ns)
      INTEGER cp,pp
c
      POLYSURF = 0.0
      DO cp=1,ns
         pp = cp-1
         IF (pp.EQ.0) pp=ns
C$AD DEBUG-HERE inSurf
         POLYSURF = POLYSURF + (X(cp)*Y(pp) - X(pp)*Y(cp))/2
      ENDDO
C$AD DEBUG-HERE endSurf
      CALL INCRSQRT(POLYSURF, X(1), X(2))
      RETURN
      END

      FUNCTION POLYCOST(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns,i
      REAL*8 POLYCOST
      REAL*8 POLYSURF,POLYPERIM
      REAL*8 X(ns),Y(ns)
      REAL*8 perim
c
      perim = 2.2
      DO i = 1,3
C$AD DEBUG-CALL false
       perim = perim+ i*POLYPERIM(X,Y,ns)
      ENDDO
C$AD DEBUG-HERE midpoint true .FALSE.
      POLYCOST = (perim*perim)/POLYSURF(X,Y,ns)
c j'ajoute un if juste pour avoir une chance d'avoir un block milieu.
      if (ns.gt.3) then
         POLYCOST = POLYCOST*3
      endif
      POLYCOST = (POLYCOST*perim)**2
      RETURN
      END
