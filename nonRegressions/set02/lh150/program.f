C Pour tester le bug trouve par Mark Bravington (CSIRO):
C mauvaise interaction NOCHECKPOINT et adjoint-dead, qui
C appelait des FOO_FWD() et pas les FOO_BWD() correspondants
      subroutine top(xx)
      real xx,yy,zz,lc,ld
      lc = 0.0
      ld = 0.0

C$AD NOCHECKPOINT
      call foo(xx,lc)
      lc = lc+xx
      call gee(xx,ld)

C$AD NOCHECKPOINT
      call foo(yy,lc)
      lc = lc+yy
      call gee(yy,ld)

C$AD NOCHECKPOINT
      call foo(zz,lc)
      lc = lc+zz
      call gee(zz,ld)

      end

      subroutine foo(aa,lc)
      real aa,lc
      aa = aa*aa + lc
      aa = cos(aa)
      lc = 2*aa
      end

      subroutine gee(aa,ld)
      real aa,ld
      aa = 2*aa
      ld = ld+aa
      end
