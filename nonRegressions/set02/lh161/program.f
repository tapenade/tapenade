      SUBROUTINE TABLE (P,RDS,DSLSDP,DSVSDP)
C     
      REAL P(10),RDS(10),DSLSDP(10),DSVSDP(10)
      INTEGER I
      REAL PS,X,SLIS,SVAS

      DO 100 , I = 1 , 9
         IF (I.gt.5) THEN
C     
            PS=1.01*P(I)
            X=TBSSSA (1,PS,SLIS,SVAS)
            DSVSDP(I) = ( SVAS - F1(I) ) / (0.01*P(I))
            DSLSDP(I) = ( SLIS - F2(I) ) / (0.01*P(I))
            RDS(I) = DSVSDP(I) * DSLSDP(I)
C     
         ENDIF
 100  CONTINUE
      END
