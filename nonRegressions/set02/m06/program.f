      SUBROUTINE F_piecewise(x, y, z)
      REAL*8 x, y, z
      REAL*8 t
 
      z = 4*sqrt(x*y)
      IF ( (3*y - 5*x).GT. -15 .AND. (3*x + y).GT. -12 ) THEN
        z = x**2*y
      else
        z = 2*z*sin(y)*sin(x)
      ENDIF
      t = abs(x/z)
      t = t*max(t,y)
      IF ( (3*y + 2*x).LT. 6 .OR. (3*x + y).GT. -12 ) THEN 
        z = - (x**2 + y**2)/t
      ENDIF
      END
