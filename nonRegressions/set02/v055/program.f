      subroutine test(T)
C To test a bug that happened when a called subroutine does not terminate properly
C Also test the case of nested loops that share their continue label
      real T(10)
c
      if (T(1).gt.10) then
         call badf(T)
      else
         do 200 i=1,3
            do 200 j = 1,4
               T(i+j) = -T(i)
 200     continue
      endif
      end

      subroutine badf(T)
      real T(10)
c
 100  T(5) = 2.0
      goto 100
      end
