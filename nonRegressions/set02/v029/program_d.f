C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 17 Jun 2019 17:26
C
C  Differentiation of f in forward (tangent) mode:
C   variations   of useful results: f
C   with respect to varying inputs: t
C   RW status of diff variables: f:out t:in
      DOUBLE PRECISION FUNCTION F_D(t, td, f)
      IMPLICIT NONE
      DOUBLE PRECISION t
      DOUBLE PRECISION td
      REAL r
      REAL*4 r4
      REAL*8 r8
      INTEGER a
      INTRINSIC EXP
      DOUBLE PRECISION f
      REAL*8 z
      REAL v
      REAL*8 u
      u = r + 1
      u = r4 + u
      u = r8 + u
C
      z = r
      z = r4
      z = r8
C
      f_d = 2*t*td
      f = t*t
      v = 0.000000005*8
      a = v
      f_d = EXP(f)*f_d
      f = EXP(f)
      RETURN
      END

