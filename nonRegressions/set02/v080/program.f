      subroutine head(i1,i2,o)
      include 'include.h'
      double precision i1,i2,o
      call sub0(i1,i2)
      o = zn / zd
      return
      end

      subroutine sub0(u,v)
      include 'include.h'
      double precision u,v,z1,z2
      call sub1(u,z1)
      call sub2(v,z2)
      zn = z1 - z2
      zd = 1 + z1 + z2
      return
      end

      subroutine sub1(x,y)
      double precision x,y
      double precision f,g
      y = f(x) + g(x)
      y = sqrt(y)
      return
      end

      subroutine sub2(x,y)
      double precision x,y
      double precision f,g
      y = f(x) - g(x)
      y = sqrt(y)
      return
      end

      double precision function f(t)
      double precision t
      f = t * t
      f = exp(f)
      return
      end

      double precision function g(t)
      double precision t
      g = 1
      if (t .ne. 0.d0) then
        g = sin(t) / t
      endif
      return
      end
