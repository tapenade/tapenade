C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of lectable in reverse (adjoint) mode:
C   gradient     of useful results: coefba paramcc
C   with respect to varying inputs: coefba paramcc
C   RW status of diff variables: coefba:in-out paramcc:incr
C
      SUBROUTINE LECTABLE_B()
      IMPLICIT NONE
C
      INTEGER itable, i1, i2, im
      REAL talfa(999, 999), tmach(999, 999), coefba(999, 999, 10)
      REAL coefbab(999, 999, 10)
      REAL paramcc(999), tcmbeta(999), useless
      REAL paramccb(999)
      INTEGER ntcmbeta
      COMMON /allparams/ itable, i1, i2, im, ntcmbeta, talfa, tmach, 
     +coefba, paramcc, tcmbeta
      COMMON /allparams_b/ coefbab, paramccb
      INTEGER ncmbeta
C
      READ(19, *) useless
      READ(19, *) itable
      READ(19, *) (talfa(i1, itable), i1=1,100)
      READ(19, *) (tmach(i2, itable), i2=1,200)
      DO i1=1,100
        READ(19, *) (coefba(i1, i2, itable), i2=1,200)
      ENDDO
C
C
      CALL PUSHINTEGER4(itable)
      itable = 5
      CALL PUSHINTEGER4(i1)
      READ(19, *) (talfa(i1, itable), i1=1,300)
      CALL PUSHINTEGER4(i2)
      READ(19, *) (tmach(i2, itable), i2=1,400)
      CALL PUSHINTEGER4(i1)
      DO i1=1,100
        CALL PUSHINTEGER4(i2)
        READ(19, *) (coefba(i1, i2, itable), i2=1,400)
      ENDDO
C
      IF (ntcmbeta .NE. 0) THEN
        READ(19, *) ntcmbeta, ncmbeta
        DO im=1,ncmbeta
          READ(19, *) tcmbeta(im)
        ENDDO
      END IF
      DO i1=100,1,-1
        DO i2=1,400
          coefbab(i1, i2, itable) = 0.0
        ENDDO
        CALL POPINTEGER4(i2)
      ENDDO
      CALL POPINTEGER4(i1)
      CALL POPINTEGER4(i2)
      CALL POPINTEGER4(i1)
      CALL POPINTEGER4(itable)
      paramccb(i1+i2) = paramccb(i1+i2) + coefbab(i1, i2, itable)
      coefbab(i1, i2, itable) = 0.0
      DO i1=100,1,-1
        DO i2=1,200
          coefbab(i1, i2, itable) = 0.0
        ENDDO
      ENDDO
      END
C

