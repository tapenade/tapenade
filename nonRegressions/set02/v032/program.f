                        SUBROUTINE VISMPG
c                       *****************
c
     * ( XNUQ , QTOT , ROM , TIT ,
     *   ROL , ROV ,  ETAL , ETAV , ALAL , ALAV , CPL , CPV ,
     *   INDPRE , INDENE , IZOCOR , PARCOR , ICOR ,
     *   AUXEDI )
c
c#
c***********************************************************************
c        sous programme de  - t h y c -  version v3p2
c                           -----------
c                       copyright edf 1995
c                           -----------
c
c    date : juin 1995
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c
c
c
c    a pour but de calculer le coefficient de viscosite cinematique
c       turbulente aux points ou pression et energie sont definies.
c       pour les modeles todreas
c       en monophasique
c
c
c    - calcule le tableau xnuq (dimension m2*s-1)
c
c
c-----------------------------------------------------------------------
c
c***********************************************************************
c#
c$
c                               arguments
c .________.____.____.________________________________________________.
c ]  nom   ]type]mode]                    role                        ]
c ]________]____]____]________________________________________________]
c ] alal   ] tr ] l--] conductivite thermique liquide                 ]
c ] alav   ] tr ] l--] conductivite thermique vapeur                  ]
c ] auxedi ] tr ] ---] pas utilise                                    ]
c ] cpl    ] tr ] l--] capacite thermique massique liquide            ]
c ] cpv    ] tr ] l--] capacite thermique massique vapeur             ]
c ] etal   ] tr ] l--] viscosite dynamique liquide                    ]
c ] etav   ] tr ] l--] viscosite dynamique vapeur                     ]
c ] icor   ] ti ] l--] numero des correlations par zone et loi        ]
c ] indene ] ti ] l--] indicateur de d'energie                        ]
c ] indpre ] ti ] l--] indicateur de pression                         ]
c ] izocor ] ti ] l--] numero de zone de correlation par noeud ijk    ]
c ] parcor ] tr ] l-p] parametre associe aux correlations             ]
c ] qtot   ] tr ] l--] module du debit aux noeuds de pression         ]
c ] rol    ] tr ] ---] pas utilise                                    ]
c ] rom    ] tr ] l--] masse  volumique melange                       ]
c ] rov    ] tr ] ---] pas utilise                                    ]
c ] tit    ] tr ] l--] tit projete sur une directions x,y ou z        ]
c ] xnuq   ] tr ] le-] coefficient de diffusion ( debit )             ]
c ]________]____]____]________________________________________________]
c
c    type : e (entier), r (reel), a (alphanumerique), t (tableau)
c           l (logique)   .. et types composes (ex : tr tableau reel)
c    mode : l (lu), e (ecrit), p (passe)
c$
c-----------------------------------------------------------------------
c%
c  sous programme(s) appelant(s) : viscoq,
c
c  sous programme(s) appele(s)   : exp, alog10,
c%
c***********************************************************************
c---
c  0. declarations
c---
c
      INTRINSIC EXP
c
      COMMON / GEOME1 / IM,IMP1,IMM1,JM,JMP1,JMM1,KM,KMP1,KMM1
      COMMON / GEOME2 / IJKM , IJKMP1 , KTOT , KPOT
      COMMON / CORRUT / NZCOR , NCORU , NCOR
      COMMON / CORRPR / NCORP , NPARM , NDIND , NLOI
c
      DIMENSION INDPRE (IMP1, JMP1, KMP1) , INDENE (IMP1, JMP1, KMP1)
      DIMENSION IZOCOR (IMP1, JMP1, KMP1)
c
      DIMENSION  XNUQ (IMP1, JMP1 ,KMP1 )
      DIMENSION  QTOT (IMP1, JMP1, KMP1)
c
      DIMENSION  ROV (IMP1,JMP1,KMP1) , ROL (IMP1,JMP1,KMP1)
      DIMENSION  ETAV(IMP1,JMP1,KMP1) , ETAL(IMP1,JMP1,KMP1)
      DIMENSION  ALAV(IMP1,JMP1,KMP1) , ALAL(IMP1,JMP1,KMP1)
      DIMENSION  CPV (IMP1,JMP1,KMP1) , CPL (IMP1,JMP1,KMP1)
      DIMENSION   TIT(IMP1,JMP1,KMP1) ,  ROM(IMP1,JMP1,KMP1)
      DIMENSION  PARCOR(NLOI , NZCOR , NPARM )
      DIMENSION    ICOR(NLOI , NZCOR )
c
      DIMENSION  AUXEDI(IJKMP1)
c
c
      DO 23   I = 2 , IM
      DO 22   J = 2 , JM
      DO 21   K = 2 , KM
c
      IF ( INDPRE(I,J,K) .LT. 0 .OR. INDENE(I,J,K) .LT. 0 ) GOTO  21
c
      NULO = 1
      NUZO = IZOCOR(I,J,K)
      NUCO = ICOR(NULO,NUZO)
      IF( NUCO .LE. 0 .OR. NUCO .GT. NCORP) GOTO 21
c
c --> proprietes physiques moleculaires
c
      IF ( TIT(I,J,K) .GE. 1. ) THEN
        AMUI = ETAV(I,J,K)
        ALAM = ALAV(I,J,K)
        CP   = CPV(I,J,K)
      ELSE
        AMUI = ETAL(I,J,K)
        ALAM = ALAL(I,J,K)
        CP   = CPL(I,J,K)
      ENDIF
c
      IF( NUCO .EQ. 1 ) THEN
c
c --> diffusivite todreas faisceau lisse
c
      RE4 = (QTOT(I,J,K)*PARCOR(NULO,NUZO,3)/AMUI)**0.45
      PR2 = (AMUI*CP/ALAM)**0.2
      XNUQ (I,J,K) =   PARCOR(NULO,NUZO,4) * QTOT(I,J,K)
     *   / (0.014*RE4*PR2*(1.-EXP(-71.8/RE4/PR2)))
      XNUQ (I,J,K) = ( XNUQ(I,J,K) + AMUI ) / ROM(I,J,K)
c
      ELSE IF ( NUCO .EQ. 20 ) THEN
c
c --> diffusivite todreas faisceau a fil
c
      REY = QTOT(I,J,K)*PARCOR(NULO,NUZO,3)/AMUI
      XNUQ (I,J,K) =   PARCOR(NULO,NUZO,6) * QTOT(I,J,K)
        IF ( REY .LE. PARCOR(NULO,NUZO,7) ) THEN
        XNUQ (I,J,K) = XNUQ(I,J,K) * PARCOR(NULO,NUZO,9)
        ELSE IF ( REY .LT. PARCOR(NULO,NUZO,8) ) THEN
        XNUQ (I,J,K) =  XNUQ(I,J,K) * ( PARCOR(NULO,NUZO,9)
     *    + (1.- PARCOR(NULO,NUZO,9)) *(ALOG10(REY/PARCOR(NULO,NUZO,7))
     *      /ALOG10(PARCOR(NULO,NUZO,8)/PARCOR(NULO,NUZO,7)))**(2./3.))
        ENDIF
      XNUQ (I,J,K) = ( XNUQ(I,J,K) + AMUI ) / ROM(I,J,K)
c
      ELSE IF ( NUCO .EQ. 19 ) THEN
c
c --> viscosite turbulente constante
c
      XNUQ (I,J,K) =   PARCOR(NULO,NUZO,1) + AMUI / ROM(I,J,K)
c
      ELSE IF( NUCO .EQ. 29 ) THEN
c
c --> diffusivite todreas faisceau pas carre
c
      XNUQ (I,J,K) =   PARCOR(NULO,NUZO,4) * QTOT(I,J,K)
      XNUQ (I,J,K) = ( XNUQ(I,J,K) + AMUI ) / ROM(I,J,K)
      ELSE IF( NUCO .EQ. 40 ) THEN
c
c --> diffusivite todreas faisceau a grille melangeuse
c
      XNUQ (I,J,K) =   PARCOR(NULO,NUZO,6) * QTOT(I,J,K)
      XNUQ (I,J,K) = ( XNUQ(I,J,K) + AMUI ) / ROM(I,J,K)
      ENDIF
c
c
 21   CONTINUE
 22   CONTINUE
 23   CONTINUE
      END
