C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 28 Sep 2021 18:24
C
C  Differentiation of testmin4 in reverse (adjoint) mode:
C   gradient     of useful results: a
C   with respect to varying inputs: a
C   RW status of diff variables: a:in-out
      SUBROUTINE TESTMIN4_B(a, ab, n)
      IMPLICIT NONE
      REAL a(10)
      REAL ab(10)
      INTEGER n(10)
      REAL y
      REAL x1
      INTRINSIC MIN
      INTRINSIC MAX
      DOUBLE PRECISION F
      REAL x
      REAL b
      REAL c
      REAL e
      REAL z
      REAL g
      INTEGER y1
      REAL max1
      REAL max1b
      DOUBLE PRECISION result1
      REAL arg1(10)
      INTEGER*4 branch
      IF (y .GT. x1) THEN
        a(3) = x1
        CALL PUSHCONTROL1B(0)
      ELSE
        a(3) = y
        CALL PUSHCONTROL1B(1)
      END IF
      IF (a(7) + a(8) .LT. a(7)*a(8)) THEN
        max1 = a(7)*a(8)
        CALL PUSHCONTROL1B(0)
      ELSE
        max1 = a(7) + a(8)
        CALL PUSHCONTROL1B(1)
      END IF
      ab(6) = ab(6) + max1*ab(5)
      max1b = a(6)*ab(5)
      ab(5) = 0.0
      CALL POPCONTROL1B(branch)
      IF (branch .EQ. 0) THEN
        ab(7) = ab(7) + a(8)*max1b
        ab(8) = ab(8) + a(7)*max1b
      ELSE
        ab(7) = ab(7) + max1b
        ab(8) = ab(8) + max1b
      END IF
      CALL POPCONTROL1B(branch)
      IF (branch .EQ. 0) THEN
        ab(3) = 0.0
      ELSE
        ab(3) = 0.0
      END IF
      END

