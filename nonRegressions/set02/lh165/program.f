C [llh 20-Avr-2010]
C Un premier test de non-regression pour le
C == binomial checkpointing == treeverse ==
C == revolve == iterative loops == (synonymes!)
      subroutine top(x,y,nstp,stp1)
      real x,y
      real A(100),B(100),C(100)
      real v1,v2,v3
      integer i,j,stp,nstp,stp1
c
      if (x.gt.10.0) then
C$AD CHECKPOINT-START
         x = x*y
         if (x.gt.0.0) x =-x
C$AD CHECKPOINT-START
         y = y - x*x
         y = y*y
C$AD CHECKPOINT-END
         x = x + sin(y)
C$AD CHECKPOINT-END
         continue
      endif
C$AD II-LOOP
      do i=1,90
         A(i) = i*x
         B(i) = i*x*y
         C(i) = x*y
      enddo
C$AD BINOMIAL-CKP nstp-stp1+2 4 stp1
      do stp=stp1,nstp
         x = x+y
         y = y-x*x
         v3 = 1.0
         do i=3,98
            j = i-stp/10
            v1 = A(j)*B(i)
            v2 = B(j)/A(i+1)+v3
            A(i) = 2*A(i)+sin(C(j))
            B(i-1) = B(i)*B(i+2) - A(i+1)*A(i-1)
            A(i+1) = A(i)+v1*v2
            B(i-1) = B(i)+v2*v3
            v3 = v3+v2-v1
         enddo
         A(j) = A(i)*B(j)
      enddo
      x = 0.0
      do i=1,100
         x = x+A(i)
         y = y+B(i)
      enddo
      end
