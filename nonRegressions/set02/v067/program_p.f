C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 2.2.2 (r1624) - 02/12/2007 14:04
C  
C--------------------------------------------------------------------------*
C Function subroutine. 
C--------------------------------------------------------------------------*
      SUBROUTINE ADJ_FCN(t, y, yp, result, rp)
      IMPLICIT NONE
      DOUBLE PRECISION result, rp, t, y, yp
      INTEGER neq
      PARAMETER (neq=61)
      DOUBLE PRECISION pi
      PARAMETER (pi=3.14159265358979323846)
      t = y + pi
      END

