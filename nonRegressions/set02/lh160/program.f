      SUBROUTINE FLUROE(un, coor, dx, ce, nubo)

      REAL*8 un(5,9876)
      REAL*8 coor(3,9876)
      REAL*8 dx(5,9876)
      REAL*8 ce(5,9876)
      INTEGER nubo(2,9876)

      INTEGER   iseg, nubo1, nubo2
      REAL*8    aix,  dpm, dpor, dpex
      REAL*8    uas1(2), uas5(2)
      REAL*8    flur1  , flur5

C$AD II-LOOP
      DO iseg=1,99

         nubo1        = nubo(1,iseg)
         nubo2        = nubo(2,iseg)
         uas1(1)      = un(1,nubo1)
         uas1(2)      = un(1,nubo2)
         uas5(1)      = un(5,nubo1)
         uas5(2)      = un(5,nubo2)
         aix          = coor(1,nubo2) - coor(1,nubo1)
         flur1        = aix*dx(1,nubo1) + uas1(2)
         flur5        = aix*dx(5,nubo1) + uas5(2)
         dpm          = uas1(2)
         dpex         = flur1 - dpm
         dpor         = dpm
         flur1        = dpex*dpm
         dpm          = uas5(2)
         dpex         = flur5 - dpm
         dpor         = dpm
         flur5        = dpex*dpm
         uas1(2)      = uas1(2) + flur1
         uas5(2)      = uas5(2) + flur5
         ce(1,nubo1)  = ce(1,nubo1) + uas1(2)*uas5(2)

      ENDDO

      END
