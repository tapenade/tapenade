      subroutine croira
	 include 'comtabloan.inc'
	 include 'comparsol.inc'

         integer nsencourac

          if(n.ge.nger.and.(nstoprac.eq.0.or.n.eq.nstoprac).and.nger.
     s	  gt.0)then

             if(codetemprac.eq.1) then
                      dtj(n)=max(tcult-tcmin,0.)
                      dtj(n)=min(dtj(n),tcmax-tcmin)
	       else
                      dtj(n)=max(tsol(int(zrac))-tgmin,0.)
                      dtj(n)=min(dtj(n),tcmax-tgmin)
	       endif

          endif

      if(coderacine.eq.1) then

       if (nger.gt.0.or.codeinstal.eq.1) then 

           S=-log(1e-2)/(zlabour-zpente) 
           difrac=zprlim-zpente 

          cumlracz=0.

	    cumflrac=0.
          zdemi=amax1((znonli-difrac),(log(4.)/S))
	    racnoy(n)=0.

          do 10 iz=1,1000
              flrac(iz)=1/(1+exp(-S*(float(iz)-zdemi)))
              if (float(iz).gt.zrac) then
                  lracz(iz)=0.
                  goto 20
              endif

              if (float(iz).lt.profsem) flrac(iz)=0.



              racnoy(n)=racnoy(n)+flrac(iz)*anox(iz)
  10     continue 
  20    continue 

       endif     

      else
         if(nger.gt.0) then
c C$AD NOCHECKPOINT
            call densirac
         endif
      endif

      if(rltot.gt.0.and.izrac.lt.1.) then
	   idzrac=izrac/rltot*cumflrac
	else
	   idzrac=1.
	endif

      exolai=exp(-0.055*exofac*100.)
      resrac=0.
      do 50 iz=1,int(zrac)

         resrac=resrac+amax1(hur(iz)-(1.-sensrsec)*humin(iz),0.)


         if(zrac.gt.zracmax) zracmax=zrac
  50  continue

	
      return
      end  

