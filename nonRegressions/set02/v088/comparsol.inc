C include comparsol.inc
c *********************

c  parametres generaux
c  -------------------
	  integer codeh2oact,codeinnact,codecaltemp,codeminhum,codernet
	  integer profdenit,codhnappe,codeminopt,codeprofmes,codeinitprec
      integer codeactimulch,codeulaivernal,codefrmur,codemicheur,codeSIG
	  integer codeclichange,codaltitude,codadret,codetycailloux
	  integer codetypeng,codetypres,codemulch,codedenit
c NB le 13/02/2003 pour Guenaelle
	  integer codazorac,codtrophrac,coderac,codemsfinal
	  real aclim,beta,lvopt,rayon
	  real FHUM,FMIN1,FMIN2,FMIN3,Wh,concrr,FTEM,TREF,zr
	  real difN,ra,plNmin,proprac,coefb,NH3ref,aangst,bangst
	  real zesx,cfes,irrlev,coefdevil,aks,bks,cvent,coefrnet
	  real phiv0,vpotdenit,distdrain,khaut,dacohes,daseuilhaut
	  real daseuilbas,QNpltminINN,ftem1,ftem2,ftem3,ftem4,finert
	  real pHminvol,pHmaxvol,Vabs2, Xorgmax,hminm,hoptm,hminn,hoptn
      real fpHnx,pHminnit,pHmaxnit,tnitopt1,tnitopt2,tnitmax,tnitmin
	  real albveg,CO2,alphaCO2,altistation,altisimul,gradtn,gradtx
	  real altinversion,gradtninv,cielclair,ombragetx,pminruis
	  real z0solnu,diftherm,Bformnappe,rdrain,hcccx(10),masvolcx(10)
	  real engamm(8),voleng(8),orgeng(8),deneng(8),kbio(10),yres(10)
      real akres(10),bkres(10),awb(10),bwb(10),cwb(10),ahres(10)
      real bhres(10),CNresmin(10),CNresmax(10),qmulchruis0,decomposmulch
      real kcouvmlch,mouillabilmulch,albedomulch
c NB le 13/02/2003 pour Guenaelle
	  real minefnra,maxazorac,minazorac,repracmax,repracmin,kreprac
c NB et ML le 23/02/04 pour demarrage plantule
	  real masecplantule, zracplantule	        
	  common /param/aclim,beta,lvopt,rayon,concrr,FTEM,TREF,FHUM,
     s          FMIN1,FMIN2,FMIN3,iniprofil,Wh,difN,ra,plNmin,proprac,
     s          coefb,zesx,cfes,irrlev,coefdevil,aks,bks,cvent,            
     s          coefrnet,phiv0,profdenit,vpotdenit,distdrain
      common /paramcodes/codedenit,codhnappe,codecaltemp,codeh2oact,
     s          codeinnact,codeulaivernal,codefrmur,codemicheur,codeSIG,
     s          codeminopt,codeprofmes,codeinitprec,codeactimulch,
     s          codeminhum,codernet,codeclichange,codaltitude,
     s          codetycailloux,codetypeng,codemulch,codazorac,
     s          codtrophrac,coderac,codemsfinal

      common /param2/khaut,dacohes,daseuilhaut,daseuilbas,QNpltminINN,
     s          ftem1,ftem2,ftem3,ftem4,finert,pHminvol,pHmaxvol,Vabs2,
     s          Xorgmax,hminm,hoptm,hminn,hoptn,zr,NH3ref,aangst,bangst,
     s          albveg,CO2,alphaCO2,altistation,altisimul,gradtn,gradtx
      common/nb0304/fpHnx,pHminnit,pHmaxnit,tnitopt1,tnitopt2,tnitmax,
     s          tnitmin
      common/nb0504/codadret,altinversion,gradtninv,cielclair,ombragetx 
	  common/param3/pminruis,diftherm,Bformnappe,rdrain,masvolcx,
     s          hcccx
      common/rugo/z0solnu
      common/paramengrais/engamm,voleng,orgeng,deneng
	  common/parammine1/akres,bkres,awb,bwb,cwb,ahres,bhres,kbio,yres,
     s          CNresmin,CNresmax,codetypres
	  common /parammulch/qmulchruis0,decomposmulch,kcouvmlch,
     s          mouillabilmulch,albedomulch
c NB le 13/02/2003 pour Guenaelle
	  common/nb1302/minefnra,maxazorac,repracmax,repracmin,kreprac,
     s          minazorac
c NB le 23/02/2004 pour demarrage plantule
	  common/nbml2302/masecplantule, zracplantule
 
c parametres sol
c --------------
	  character*12 typsol	
	  integer numsol,typecailloux(5),epd(5)
      integer codecailloux,codemacropor,codefente,codrainage,codenitrif
	  integer coderemontcap
      real profhum,pH,q0,ruisolnu,obstarac,profimper,ecartdrain,Ksol
      real profdrain,DAF(5),HMINF(5),HCCF(5),da(5),epc(5),hcc(5),hmin(5)
      real cailloux(5),infil(0:6)
  	  real calc,argi,Norg,profsol,albedo,humcapil,capiljour,concseuil

      common /parsolcodes/codecailloux,codemacropor,codefente,
     s          codrainage,codenitrif,coderemontcap
      common /parsol/da,epc,hcc,hmin,calc,argi,Norg,profsol,albedo,epd,
     s          capiljour,humcapil,profhum,pH,concseuil,q0,ruisolnu,
     s          obstarac,profimper,ecartdrain,Ksol,profdrain,numsol        
      common /parsolchar/typsol
      common /parasol/DAF,HMINF,HCCF
      common /nouvsol/cailloux,typecailloux,infil
c
c initialisations
c ---------------
      character*7  usm
      character*16 fpltcourt,ftec
      character*8 preflai
	  character*3  sufflai,stade0
      integer codoptim,codesuite
	  integer izcel(5),izc(5),ncel,icel(0:1000),codeinstal,iwater
	  integer ifwater,ichsl,culturean
      real HUCC(1000),HUMIN(1000),lai0,NO3initf(5),Hinitf(5),resperenne0
	  real densinitial(5),NH4initf(5),masec0,QNplante0,magrain0,zrac0
	  real dacouche(1000)

	  common /initsol1000/HUCC,HUMIN,dacouche,icel
      common /initsol/izcel,izc,ncel
      common /optim/codoptim,codesuite
	  common/initusmchar/usm,ftec,fpltcourt,preflai,sufflai,stade0
	  common/initusm/codeinstal,lai0,iwater,ifwater,ichsl,Hinitf,
     s            NO3initf,culturean,densinitial,NH4initf,masec0,
     s            QNplante0,magrain0,zrac0,resperenne0

      logical culturepure
      character*3 codeversion
	  character*2 codecompil
	  character*7 codesimul
      common/codessticsgen/culturepure,codeversion,codecompil,codesimul

c   lecepitaf
c   ---------
	  integer codepitaf
	  integer epitN,epijul1(7),epijul2(7),codhumoffN,Noffn
	  integer epibN
	  real epiQNmax,juloffN(366),pondealpha,pondebeta,epik(7)
	  real dosiNmin,normealpha,normebeta
c	  real epibN


	  common /nb0407/codepitaf,Noffn,codhumoffN,dosiNmin
	  common /nb0203/epiQNmax,epibN,epitN,juloffN,epik,epijul1,epijul2
	  common /thomas/pondealpha,pondebeta,normealpha,normebeta

