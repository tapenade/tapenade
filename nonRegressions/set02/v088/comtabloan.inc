C include comtabloan.inc
c********************************************
c include des tableaux 
c********************************************
c include des tableaux sur 2 ans
      integer nitetcult(0:731),annee(731)
	real msrac(0:731),lai(0:731),upvt(0:731),upobs(0:731)
	real laisen(0:731),airg(731),anit(731),masec(0:731),ircarb(0:731)
	real dltams(0:731),ms(0:731),magrain(0:731),ulai(0:731),fpv(0:731)
	real deltai(0:731),dtj(0:731)
	real durvie(0:731),tdevelop(0:731)
	real pfeuil(0:731),pfeuilverte(0:731),tauxcouv(0:731),cu(0:731)
	real trosemax(0:731),utp(0:731),abso(0:731)
	real epiQN(720),epikc(720),epiqnplmax(720),tempoffN(720)
	real hoffrn(0:720)
c  domi 24/06/02 ne pas diminuer etpp c'est du climat
      real etpp(0:731)

	common /plantetab/lai,upvt,upobs,ircarb,laisen,deltai,ulai
      common /tempstab/annee
	common /apport/airg,anit
	common /rendementtab/masec,dltams,ms,magrain
	common /fruitab/fpv
	common /dev4tab/dtj
	common /suptab/durvie,tdevelop,pfeuil,pfeuilverte
	common /couverturetab/tauxcouv
	common /nb1303tab/cu,nitetcult,msrac,utp,etpp,trosemax,abso
	common /epitaftab/epiQN,epikc,epiqnplmax,tempoffN,hoffrn

c include des tableaux sur la profondeur de sol
c domi 24/06/03 vire :	macrop(1000)
c domi 24/06/03 vire :    real dnit(1000)

      real drl(0:731,1000),rl(0:731,1000)
	real tsol(1000),tsolveille(1000)
	real lracz(1000),etz(1000),epz(1000),flrac(1000)
	real HUR(1000),hurmini(1000),nit(1000),racnoy(1000)
	real esz(1000),precrac(1000),amm(1000)
c domi 24/06/03 on diminue irrigprof on ne peux pas irriguer � plus de 50 cm en profondeur
c	real irrigprof(50)
c domi 11/07/03 ca pose des problemes !!!!!!!!!!!!!!!!!!!! bilan non boucl�
	real irrigprof(1000)


	real lracsenz(1000),anox(1000),sat(1000)
	common /densractab/rl,drl
	common/transp1000/tsol,tsolveille,racnoy
      common/nitre1000/epz,etz,lracz,flrac,HUR,hurmini,nit,esz
      common/tab1000/precrac,amm,lracsenz,anox,sat,irrigprof


