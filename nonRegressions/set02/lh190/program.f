C To test a bug of missing adjoint diff var reinitializations
C When adjoint block is empty, adjoint FG skips over it even
C  when it shouldn't because of a needed diff reinitialization.
C Bug was due to wrong default ADDeps matrix for InOutN zones.
C Bug found on the GCM
      SUBROUTINE TEST(qnet, n)
      REAL*8 qnet(100)
      INTEGER n
      IF (n.eq.3) THEN
         CALL MYREAD(qnet)
      ELSEIF (n.eq.6) THEN
         CALL MYREAD(qnet)
      ENDIF
      qnet = qnet*qnet
      END

      SUBROUTINE MYREAD(field)
      Real*8 field(100)
      CALL EXTREAD(field)
      END
