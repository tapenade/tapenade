C from nonRegrC/REFERENCES/v103
C
      REAL FUNCTION SWITCH_WITH_FUNCTION_POINTER(a, b, pt2Func)
      IMPLICIT NONE
      REAL a
      REAL b
      REAL PT2FUNC
      EXTERNAL PT2FUNC
      SWITCH_WITH_FUNCTION_POINTER = PT2FUNC(a, b)
      END

      REAL FUNCTION MULTIPLY(a, b)
      IMPLICIT NONE
      REAL a
      REAL b
      MULTIPLY = a*b
      END

      REAL FUNCTION PLUS(a, b)
      IMPLICIT NONE
      REAL a
      REAL b
      PLUS = a + b
      END

      REAL FUNCTION MINUS(a, b)
      IMPLICIT NONE
      REAL a
      REAL b
      MINUS = a - b
      END

      REAL FUNCTION DIVIDE(a, b)
      IMPLICIT NONE
      REAL a
      REAL b
      DIVIDE = a/b
      END

      REAL FUNCTION TEST(x, y)
      IMPLICIT NONE
      REAL r1, r2, r3
      REAL x
      REAL y
      real result
      external SWITCH_WITH_FUNCTION_POINTER
      real SWITCH_WITH_FUNCTION_POINTER
      external Plus, Minus, Multiply, Divide
      r1 = SWITCH_WITH_FUNCTION_POINTER(x, x, Multiply)
      r2 = SWITCH_WITH_FUNCTION_POINTER(y, y, Multiply)
      result = SWITCH_WITH_FUNCTION_POINTER(r1, r2, Plus)
      r3 = SWITCH_WITH_FUNCTION_POINTER(x, y, Minus)
      TEST = SWITCH_WITH_FUNCTION_POINTER(result, r3, Divide)
      END

      PROGRAM MAIN
      IMPLICIT NONE
      REAL a
      REAL b
      external test
      real test
      a = 5.0
      b = 2.0
      print *, '-->', test(a,b)
      END
