C exemple pour tester le bon numerotage des zones
C et en particulier que les zones des elements
C d'un common sont les memes partout ?
      subroutine AAA(x,y)
      real x,y,v1,v2,v3,v4
      common /comF/v1,v2
      y = x+y
      end

      subroutine BBB(x,y)
      real x,y,v1,v2,v3,v4
      common /comF/v1,v2
      common /comG/v3,v4
      v4 = x
      y = x+y
      end

      subroutine CCC(x,y)
      real x,y,v1,v2,v3,v4
      common /comG/v3,v4
      y = y*v4
      y = x+y
      call DDD(x,y)

      contains

      subroutine DDD(x,y)
      real x,y,v5,v6
      common /comG/v5,v6
      y = y*v6
      y = x+y
      end

      end

      subroutine top(x,y)
      real x,y
      call AAA(x,y)
      call BBB(x,y)
      call CCC(x,y)
      end

      program prog
      x = 1.5
      y = 2.0
      call top(x,y)
      print *,'x=',x,' y=',y
      end
