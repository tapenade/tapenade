C exemple from eugene/ftn-3d
C make_rhs_uv calls diff_x, make_vort_divg et solver
C make_vort_divg calls diff_x

      subroutine make_rhs_uv(u,v,rho,omega,divg,w,ssh,
     &           rhs_u,rhs_v, istep,dt,ierr) 
      include 'common.h'

      common/buffers/buf1(nx,ny,nz),buf2(nx,ny,nz),buf3(nx,ny,nz),
     &               buf4(nx,ny,nz) 
      call diff_x(buf2, buf3, 2,  3,avr)
      call make_vort_divg(uold,vold,omegaold,divgold)  
      call solver(z,s,r)     
      end



      subroutine make_vort_divg(u,v,omega,divg) 
      include 'common.h'

      real*8 u(nx,ny,nz),v(nx,ny,nz),
     &       omega(nx,ny,nz),divg(nx,ny,nz)

      common/buffers/buf1(nx,ny,nz),buf2(nx,ny,nz),buf3(nx,ny,nz),
     &               buf4(nx,ny,nz)    
      call diff_x(buf1,buf3, 1,  1,der)
      call diff_x(buf2,buf4, 2,  1,der)
      end  




      subroutine solver(zcoef,sol,rhs) 
      include 'common.h'
      real*8 sol(nx,ny),rhs(nx,ny)
      common/buffers/buf1(nx,ny,nz),
     &       same_as_rhs(nx,ny),same_as_sol(nx,ny),buf2(nx,ny,nz-2),
     &          des(nx,ny),ccd(nx,ny),residl(nx,ny),gresdl(nx,ny),
     &          buf3(nx,ny,nz-4),buf4(nx,ny,nz)  
      end


      subroutine diff_x(u,du,
     &                  ivar,n_oprtr,cntrl_cf) 
      include 'common.h'
      dimension u(nx,ny,nz),du(nx,ny,nz)
      integer ibnd(ny,nz,30,4),jbnd(nx,nz,30,4)

C      equivalence(ibndu(1,1,1),ibnd(1,1,1,1))
C      equivalence(jbndu(1,1,1),jbnd(1,1,1,1))

      real*8 cntrl_cf(4) 
      du(1,1,1)=2*u(2,2,2)
      end
