C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) - 19 Mar 2024 19:54
C
C  Differentiation of s1 in reverse (adjoint) mode:
C   gradient     of useful results: a b
C   with respect to varying inputs: a b
C   RW status of diff variables: a:in-out b:in-out
C from lha18 pour tester NOCHECKPOINT sur une ExternalUnit
      SUBROUTINE S1_B(a, ab, b, bb)
      IMPLICIT NONE
C
      REAL a(100), b(100)
      REAL ab(100), bb(100)
      INTEGER i, j
      EXTERNAL TOTO
      EXTERNAL TOTO_FWD, TOTO_BWD
      INTEGER ii1
      INTEGER*4 branch
C
      DO i=5,95
        IF (a(i) .GT. 0.0) THEN
          CALL PUSHREAL4(a(i))
          a(i) = a(i)*b(i)
          IF (b(i) .GT. 0.0) THEN
            CALL PUSHREAL4(b(i))
            b(i) = b(i) + 1
            CALL PUSHCONTROL2B(0)
          ELSE
            CALL PUSHREAL4(b(i))
            b(i) = b(i) + 2
            CALL PUSHCONTROL2B(1)
          END IF
        ELSE
          CALL PUSHCONTROL2B(2)
        END IF
        CALL PUSHREAL4(b(i+1))
        b(i+1) = (b(i)+b(i+2))/2.0
      ENDDO
      CALL PUSHREAL4ARRAY(b, 100)
      READ*, b
      CALL PUSHREAL4(b(10))
      b(10) = b(10)*a(10)
      CALL PUSHREAL4ARRAY(b, 100)
      CALL TOTO_FWD(a, b)
      bb(10) = bb(10) + a(10)*ab(10)
      ab(10) = b(10)*ab(10)
      CALL TOTO_BWD(a, ab, b, bb)
      CALL POPREAL4ARRAY(b, 100)
      CALL POPREAL4(b(10))
      ab(10) = ab(10) + b(10)*bb(10)
      DO ii1=1,100
        bb(ii1) = 0.0
      ENDDO
      CALL POPREAL4ARRAY(b, 100)
      DO ii1=1,100
        bb(ii1) = 0.0
      ENDDO
      DO 100 i=95,5,-1
        CALL POPREAL4(b(i+1))
        bb(i) = bb(i) + bb(i+1)/2.0
        bb(i+2) = bb(i+2) + bb(i+1)/2.0
        bb(i+1) = 0.0
        CALL POPCONTROL2B(branch)
        IF (branch .EQ. 0) THEN
          CALL POPREAL4(b(i))
        ELSE IF (branch .EQ. 1) THEN
          CALL POPREAL4(b(i))
        ELSE
          GOTO 100
        END IF
        CALL POPREAL4(a(i))
        bb(i) = bb(i) + a(i)*ab(i)
        ab(i) = b(i)*ab(i)
 100  CONTINUE
      END

