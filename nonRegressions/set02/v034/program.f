    	SUBROUTINE CALCUL_J (RES)
	IMPLICIT NONE
C====Include================================================
	INCLUDE 'essai.inc'
	INCLUDE 'init.inc'
C====Output=================================================
      REAL	RES			! Valeur de J
	REAL	RES1		! Valeur de sum(biom-biom_obs)?
C====Variables Locales======================================
	INTEGER	I			! Compteur
C===========================================================
C====Debut du code==========================================
C===========================================================
      RES = 0.
	RES1 =0
	I = 2
      DO 10 I=2,263       			
		RES1 = RES1+((lai(I)-lai_obs(I))**2)*varlaiinv(I)
  10  continue
      RES=RES1+(( Eb - Eb_p )**2)/st_Eb
     :		+(( Eimax - Eimax_p )**2)/st_Eimax
     :		+(( ka - ka_p)**2)/st_ka
     :		+(( Lmax - Lmax_p )**2)/st_Lmax
     :		+(( AA - AA_p )**2)/st_AA
     :		+(( BA - BA_p )**2)/st_BA 
     :		+(( TI - TI_p )**2)/st_TI   		 
      write(*,*) 'RES=',RES 
	write(*,*) Eb,Eimax,KA,Lmax,AA,BA,TI
	TETA(0)=Eb
	TETA(1)=Eimax
	TETA(2)=ka
	TETA(3)=Lmax
	TETA(4)=AA
	TETA(5)=BA
	TETA(6)=TI
	END
