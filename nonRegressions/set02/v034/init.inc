C initialisation des parametres a priori
	  REAL Eb_p,Eimax_p,ka_p,Lmax_p,AA_p,BA_p,TI_p
	  Parameter (Eb_p=1.85)
	  Parameter (Eimax_p=0.945)
	  Parameter (ka_p= 0.7)
	  Parameter (Lmax_p= 7.5)
	  Parameter (AA_p=0.0065)
	  Parameter (BA_p=0.00205)
	  Parameter (TI_p=900)

C les ecarts-types
	  REAL st_Eb,st_Eimax,st_ka,st_Lmax,st_AA,st_BA,st_TI
	  Parameter (st_Eb=0.3**2)
	  Parameter (st_Eimax=0.02**2)
	  Parameter (st_ka= 0.0346**2)
	  Parameter (st_Lmax=2**2)
	  Parameter (st_AA=0.001**2)
	  Parameter (st_BA=0.0005**2)
	  Parameter (st_TI=70**2)
