      subroutine set(x)
       integer n,m
       parameter (n=3)
       parameter (m=9)
       real x(m),A(n,n)
       common /c/ A
       do i =1,n
          do j =1,n
             A(i,j)=x((i-1)*n+j)
          end do
       end do   
      end subroutine
      
      subroutine get(y)
       integer n,m
       parameter (n=3)
       parameter (m=9)
       real y(m),A(n,n)
       common /c/ A
       do i =1,n
          do j =1,n
             y((i-1)*n+j)=A(i,j)
          end do
       end do   
      end subroutine
      
      subroutine foo(x,y) 
       integer n,m
       parameter (n=3)
       parameter (m=9)
       real x(m),y(m),A
       common /c/ A(n,n)
       call set(x)
       do i =1,n
          do j =1,n
             A(i,j)=A(i,j)*2
          end do
       end do   
       call get(y)
      end subroutine 
      
      program p 
      integer n
      parameter (m=9)
      real x(m),y(m)
       x=(/(i,i=1,m)/)
      call foo(x,y)
      print *,y
      end program
      
