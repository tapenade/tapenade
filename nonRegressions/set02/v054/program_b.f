C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:15
C
C  Differentiation of g in reverse (adjoint) mode:
C   gradient     of useful results: g t
C   with respect to varying inputs: t
C   RW status of diff variables: g:in-killed t:incr
      SUBROUTINE G_B(t, tb, gb)
      IMPLICIT NONE
      DOUBLE PRECISION t
      DOUBLE PRECISION tb
      LOGICAL a, b, c
      INTRINSIC SIN
      DOUBLE PRECISION g
      DOUBLE PRECISION gb
      a = t .NE. 0.d0
      b = t .NE. 1.d0
      c = t .NE. 2.d0
      IF (a .AND. (b .OR. c)) tb = tb + (COS(t)/t-SIN(t)/t**2)*gb
      END

