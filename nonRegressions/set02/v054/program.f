      double precision function g(t)
      double precision t
      logical a,b,c
      g = 1
      A = (t .ne. 0.d0)
      B = (t .ne. 1.d0)
      C = (t .ne. 2.d0)
      IF (A .and. (B .or. C)) then
        g = sin(t) / t
      endif
      return
      end
