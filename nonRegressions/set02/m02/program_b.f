C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 17 Jun 2019 17:26
C
C  Differentiation of symmt in reverse (adjoint) mode, forward sweep:
C   gradient     of useful results: t2 t3
C   with respect to varying inputs: t2 t3
C
      SUBROUTINE SYMMT_FWD(noe3, vn3, t1, t2, t3, t4, iarg, icora, th1, 
     +                     th2, th3, th4)
      IMPLICIT NONE
C
      REAL run, vnn, vnx, vny
      INTEGER if3, is, nf3
      REAL t1(2000), t2(2000), t3(2000), t4(2000), th1(2000), th2(2000)
     +     , th3(2000), th4(2000), vn3(2, 1000)
      INTEGER iarg(10), icora(0:10), noe3(1000)
      INTRINSIC SQRT
C
      IF (nf3 .GT. 0) THEN
        DO if3=1,nf3
          vnn = SQRT(vn3(1, if3)**2 + vn3(2, if3)**2)
          CALL PUSHREAL4(vnx)
          vnx = vn3(1, if3)/vnn
          CALL PUSHREAL4(vny)
          vny = vn3(2, if3)/vnn
        ENDDO
        CALL PUSHREAL4(vny)
        CALL PUSHREAL4(vnx)
        CALL PUSHINTEGER4(nf3)
        CALL PUSHCONTROL1B(0)
      ELSE
        CALL PUSHCONTROL1B(1)
      END IF
      END

C  Differentiation of symmt in reverse (adjoint) mode, backward sweep:
C   gradient     of useful results: t2 t3
C   with respect to varying inputs: t2 t3
C   RW status of diff variables: t2:in-out t3:in-out
C
      SUBROUTINE SYMMT_BWD(noe3, vn3, t1, t2, t2b, t3, t3b, t4, iarg, 
     +                     icora, th1, th2, th3, th4)
      IMPLICIT NONE
      REAL run, vnn, vnx, vny
      REAL runb
      INTEGER if3, is, nf3
      REAL t1(2000), t2(2000), t3(2000), t4(2000), th1(2000), th2(2000)
     +     , th3(2000), th4(2000), vn3(2, 1000)
      REAL t2b(2000), t3b(2000)
      INTEGER iarg(10), icora(0:10), noe3(1000)
      INTRINSIC SQRT
      INTEGER*4 branch
      CALL POPCONTROL1B(branch)
      IF (branch .EQ. 0) THEN
        CALL POPINTEGER4(nf3)
        CALL POPREAL4(vnx)
        CALL POPREAL4(vny)
        DO if3=nf3,1,-1
          is = noe3(if3)
          runb = -(vny*t3b(is)) - vnx*t2b(is)
          t2b(is) = t2b(is) + vnx*runb
          t3b(is) = t3b(is) + vny*runb
          CALL POPREAL4(vny)
          CALL POPREAL4(vnx)
        ENDDO
      END IF
      END

