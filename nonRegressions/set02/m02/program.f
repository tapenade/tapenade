      program program02
      REAL t1(2000), t2(2000), t3(2000), t4(2000), th1(2000), th2(2000),
     +     th3(2000), th4(2000), vn3(2, 1000)
      INTEGER iarg(10), icora(0:10), noe3(1000)
      
C $AD NOCHECKPOINT
      call symmt(noe3,vn3,t1,t2,t3,t4,iarg,icora,th1,th2,th3,th4)
      end

      subroutine symmt(noe3,vn3,t1,t2,t3,t4,iarg,icora,th1,th2,th3,th4)
c
      REAL run, vnn, vnx, vny
      INTEGER if3, is, nf3
      REAL t1(2000), t2(2000), t3(2000), t4(2000), th1(2000), th2(2000),
     +     th3(2000), th4(2000), vn3(2, 1000)
      INTEGER iarg(10), icora(0:10), noe3(1000)
c
      IF (NF3.GT.0) THEN
        DO 4 IF3=1,NF3
          IS = NOE3(IF3)
          vnn=sqrt(vn3(1,if3)**2+vn3(2,if3)**2)
          vnx=vn3(1,if3)/vnn
          vny=vn3(2,if3)/vnn
          run= t2(is)*vnx+t3(is)*vny
          t2(is) = t2(is) - run*vnx
          t3(is) = t3(is) - run*vny
    4   CONTINUE
      ENDIF
c
      return
      end
