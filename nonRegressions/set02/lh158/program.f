      SUBROUTINE VCURVM(nf1, noe1, nsfac, sigma, ce, vnsig)
C Delicate degenerate case for recomputation-vs-pushpop mechanism:
      integer nf1,noe1(99),nsfac(3,99)
      real*8 sigma(99),ce(5,99),vnsig(99)
c
      integer if1, ifac
      real*8 pm3
c      
      if1 = 2*nf1
      ifac = noe1(if1)
      pm3  = 2.0 * sigma(ifac)
      ce(5,if1) = ce(5,if1) - vnsig(ifac)*pm3
c
      END
