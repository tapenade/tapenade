c Exemple fourni par Stefano Carli <stefano.carli@kuleuven.be> pour
c illustrer des problemes de regeneration ou pas des fonctions _nodiff
      SUBROUTINE top_routine (NPRT, nx, ny, ns,
     &  ne, na, ua, te, ti, po)
      implicit none

      integer, intent(in) :: nprt, nx, ny, ns
      real :: na, ua, te, ti, po, ne, pr

c     do something
      call external_routine (nx, ny, ns, ne, na, te, ti, pr)
c     do something

      call second_routine(NPRT, nx, ny, ns)
c     do something
      RETURN

      END


      SUBROUTINE second_routine(NPRT, nx, ny, ns)
      implicit none

      real  :: reshlp
      integer, intent(in) :: NPRT, nx, ny, ns
      integer :: iz
      EXTERNAL third_routine

c     do something
      CALL third_routine(NPRT,RESHLP,IZ,NX,NY)
c     do something
      RETURN

      END



      SUBROUTINE third_routine (NPRT, Z, IZ, NX, NY)
C
      IMPLICIT NONE
C
      INTEGER IZ, NPRT, NX, NY
      REAL :: Z

c     do something
 
      RETURN

      END
