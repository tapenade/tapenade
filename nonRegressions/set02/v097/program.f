      subroutine res(x)
      double precision x
      x = f(x) + g(x)
      return
      end subroutine

      double precision function f(t)
      double precision t
      double precision res(4)
      res(1) = t * t
      f = t * t * res(1)
      f = exp(f)
      return
      end function

      double precision function g(t)
      double precision t
      double precision res(4)
      g = 1
      res(1) = t * 2
      if (t .ne. 0.d0) then
        g = sin(t) / t
      endif
      g = g + res(1)
      return
      end function

