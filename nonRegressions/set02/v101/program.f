C ce programme est en fortran90
C program_p.f est en fortran77 avec des features fortran90
C mal decompilees

        subroutine test77(x,y)
        implicit none
        real y
        real x
        real, allocatable :: array1(:), array2(:)

c       first comment, it will be lost because of allocate
        allocate(array1(10))
C       first comment on allocate after

c       second comment, it will survive because of continue
        continue
C       second comment on allocate before
        allocate(array2(100)) 
C       second comment on allocate after

        array1(1) = 2
        array2(2) = 3

c       third comment, it will survive
        y = x * array1(1) + x * array2(2)

        return
        end

