C Autre cas du bug trouve sur lauvernet-multisailsmac
C en mode -nooptim activity, mais independant de ce nooptim
C (le CLOSE doit etre recopie dans l'adjoint)
C Cette fois-ci, c'est plutot un bug de diff-liveness
C dans le cas ou "myread" est appele 2 fois
C => la zone all-IO DOIT etre live en sortie de myread.
      subroutine top(x,y,z)
      real x,y,z
      call test1(x,y)
      x = x*y*z
      call test2(y,z)
      end

      subroutine test1(a,b)
      real a,b
      b = a*b
      call myread(a,b)
      a = a*b*a
      end

      subroutine test2(a,b)
      real a,b
      b = a*b
      call myread(a,b)
      a = a*b*a
      end

      subroutine myread(a,b)
      real a,b
      OPEN(unit=18, file='toto')
      READ(18,*)
      READ(18,*) a
      b = b*a
      CLOSE(18)
      end
