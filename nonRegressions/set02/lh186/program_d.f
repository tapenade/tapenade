C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 18 Apr 2019 18:10
C
C  Differentiation of top in forward (tangent) mode:
C   variations   of useful results: x y z
C   with respect to varying inputs: x y z
C   RW status of diff variables: x:in-out y:in-out z:in-out
C Autre cas du bug trouve sur lauvernet-multisailsmac
C en mode -nooptim activity, mais independant de ce nooptim
C (le CLOSE doit etre recopie dans l'adjoint)
C Cette fois-ci, c'est plutot un bug de diff-liveness
C dans le cas ou "myread" est appele 2 fois
C => la zone all-IO DOIT etre live en sortie de myread.
      SUBROUTINE TOP_D(x, xd, y, yd, z, zd)
      IMPLICIT NONE
      REAL x, y, z
      REAL xd, yd, zd
      CALL TEST1_D(x, xd, y, yd)
      xd = z*(y*xd+x*yd) + x*y*zd
      x = x*y*z
      CALL TEST2_D(y, yd, z, zd)
      END

C  Differentiation of test1 in forward (tangent) mode:
C   variations   of useful results: a b
C   with respect to varying inputs: a b
C
      SUBROUTINE TEST1_D(a, ad, b, bd)
      IMPLICIT NONE
      REAL a, b
      REAL ad, bd
      bd = b*ad + a*bd
      b = a*b
      CALL MYREAD_D(a, b, bd)
      ad = a**2*bd
      a = a*b*a
      END

C  Differentiation of test2 in forward (tangent) mode:
C   variations   of useful results: a b
C   with respect to varying inputs: a b
C
      SUBROUTINE TEST2_D(a, ad, b, bd)
      IMPLICIT NONE
      REAL a, b
      REAL ad, bd
      bd = b*ad + a*bd
      b = a*b
      CALL MYREAD_D(a, b, bd)
      ad = a**2*bd
      a = a*b*a
      END

C  Differentiation of myread in forward (tangent) mode:
C   variations   of useful results: b
C   with respect to varying inputs: b
C
      SUBROUTINE MYREAD_D(a, b, bd)
      IMPLICIT NONE
      REAL a, b
      REAL bd
      OPEN(unit=18, file='toto') 
      READ(18, *) 
      READ(18, *) a
      bd = a*bd
      b = b*a
      CLOSE(18) 
      END

