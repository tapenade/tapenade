C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.6 (r4701M) - 17 Jan 2013 16:44
C
C  Differentiation of adj1 in reverse (adjoint) mode:
C   gradient     of useful results: x y z
C   with respect to varying inputs: x y z
C   RW status of diff variables: x:in-out y:in-out z:incr
      SUBROUTINE ADJ1_B(x, xb, y, yb, z, zb, n, o)
      IMPLICIT NONE
C a more general example of a natural loop:
      REAL a, x, y, z
      REAL ab, xb, yb, zb
      INTEGER n, o
      DIMENSION x(2*n), y(2*n), z(2*n)
      DIMENSION xb(2*n), yb(2*n), zb(2*n)
      INTEGER j
      INTEGER ad_count
      INTEGER i
      INTEGER*4 branch
C
      a = 0.5*x(20)
      IF (a .GT. 0) THEN
        CALL PUSHREAL4(a)
        a = a*a
        j = 0
        CALL PUSHCONTROL1B(1)
      ELSE
        CALL PUSHCONTROL1B(0)
        j = 1
      END IF
      ad_count = 1
 100  CALL PUSHINTEGER4(j)
      j = j + 3
      x(j) = a*y(j-1)
      IF (j .GT. 100) THEN
        CALL PUSHREAL4(a)
        a = 2*a*a + 3
        CALL PUSHREAL4(y(j+1))
        y(j+1) = z(j)*z(3) + x(j+1)
        ad_count = ad_count + 1
        GOTO 100
      END IF
      CALL PUSHINTEGER4(ad_count)
      CALL PUSHREAL4(x(1))
      x(1) = x(2)*y(3)
      xb(1) = 2*x(1)*xb(1)
      CALL POPREAL4(x(1))
      xb(2) = xb(2) + y(3)*xb(1)
      yb(3) = yb(3) + x(2)*xb(1)
      xb(1) = 0.0
      CALL POPINTEGER4(ad_count)
      DO i=1,ad_count
        IF (i .EQ. 1) THEN
          ab = 0.0
        ELSE
          CALL POPREAL4(y(j+1))
          zb(j) = zb(j) + z(3)*yb(j+1)
          zb(3) = zb(3) + z(j)*yb(j+1)
          xb(j+1) = xb(j+1) + yb(j+1)
          yb(j+1) = 0.0
          CALL POPREAL4(a)
          ab = 2**2*a*ab
        END IF
        ab = ab + y(j-1)*xb(j)
        yb(j-1) = yb(j-1) + a*xb(j)
        xb(j) = 0.0
        CALL POPINTEGER4(j)
      ENDDO
      CALL POPCONTROL1B(branch)
      IF (branch .NE. 0) THEN
        CALL POPREAL4(a)
        ab = 2*a*ab
      END IF
      xb(20) = xb(20) + 0.5*ab
      END

