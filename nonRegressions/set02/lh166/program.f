      subroutine adj1(x,y,z,N,o)
C a more general example of a natural loop:
      real a,x,y,z
      integer n,o
      dimension x(2*N),y(2*N),z(2*N)

      a = 0.5 * X(20)
      if (a.gt.0) then
         a = a*a
         j = 0
      else
         j = 1
      endif
 100  j = j+3
      X(j) = a*Y(j-1)
      if (j.le.100) goto 200
      a = 2*a*a+3
      Y(j+1) = Z(j)*Z(3) + X(j+1)
      goto 100
 200  X(1) = X(2)*Y(3)
      X(1) = X(1)*X(1)
      end          
