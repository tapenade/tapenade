!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 18 Jul 2024 15:05
!
!  Differentiation of testiomess in reverse (adjoint) mode, forward sweep:
!   gradient     of useful results: d a b
!   with respect to varying inputs: d e a b c
SUBROUTINE TESTIOMESS_FWD(a, b, c, d, e)
  IMPLICIT NONE
  REAL :: a(100), b(100), c, d, e, t(100)
  INTEGER :: i
  INTEGER :: k1, kend, kstep
!
  k1 = 1
  kend = 100
  kstep = 3
!
  READ*, b(34:66:2)
  WRITE(22, *) , (a(i), i=k1,kend,kstep)
  READ(22, *) , (t(i), i=0,33)
!
  WRITE(22, *) , c
  READ(22, *) , c
  READ*, e
!
  CALL PUSHREAL4ARRAY(t, 100)
END SUBROUTINE TESTIOMESS_FWD

!  Differentiation of testiomess in reverse (adjoint) mode, backward sweep:
!   gradient     of useful results: d a b
!   with respect to varying inputs: d e a b c
!   RW status of diff variables: d:in-out e:zero a:incr b:in-out
!                c:zero
SUBROUTINE TESTIOMESS_BWD(a, ab, b, bb, c, cb, d, db, e, eb)
  IMPLICIT NONE
  REAL :: a(100), b(100), c, d, e, t(100)
  REAL :: ab(100), bb(100), cb, db, eb
  INTEGER :: i
  INTEGER :: k1, kend, kstep
  CALL POPREAL4ARRAY(t, 100)
  eb = 0.0
  db = c*db
  cb = 0.0
  ab(34:66) = ab(34:66) + t(1:33)*bb(1:33)
  bb(1:33) = 0.0
  bb(34:66:2) = 0.0
  eb = 0.0
  cb = 0.0
END SUBROUTINE TESTIOMESS_BWD

