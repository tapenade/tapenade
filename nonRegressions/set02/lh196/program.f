C L'exemple de test standard du mode debug,
C  cf ~/home/tapenade/debugAD au 2 octobre 2017
C Source code, complete with main, Fortran version.

      PROGRAM MAIN
      INTEGER ns
      INTEGER i
      REAL*8 X(5),Y(5)
      REAL*8 cost
      REAL*8 POLYCOST

      ns = 5
      X(1) = 0.0d0
      Y(1) = 0.0d0
      X(2) = 2.0d0
      Y(2) = 0.0d0
      X(3) = 2.0d0
      Y(3) = 3.0d0
      X(4) = 1.0d0
      Y(4) = 6.0d0
      X(5) = -3.0d0
      Y(5) = 3.0d0
      cost = POLYCOST(X,Y,ns)
      print *,'cost=',cost
      END

      FUNCTION POLYCOST(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns,i
      REAL*8 POLYCOST
      REAL*8 POLYSURF,POLYPERIM
      REAL*8 X(ns),Y(ns)
      REAL*8 perim
c
C C$AD DEBUG-CALL false
      perim = POLYPERIM(X,Y,ns)
C C$AD DEBUG-HERE midpoint true .FALSE.
      POLYCOST = (perim*perim)/POLYSURF(X,Y,ns)
      RETURN
      END

      FUNCTION POLYSURF(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 POLYSURF, X(ns), Y(ns)
      INTEGER cp,pp
c
      POLYSURF = 0.0
      DO cp=1,ns
         pp = cp-1
         IF (pp.EQ.0) pp=ns
         POLYSURF = POLYSURF + (X(pp)*Y(cp) - X(cp)*Y(pp))/2
      ENDDO
      RETURN
      END

      FUNCTION POLYPERIM(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 POLYPERIM, X(ns), Y(ns)
      INTEGER cp,pp
      REAL*8 dx,dy
c
      POLYPERIM = 0.0
      DO cp=1,ns
         pp = cp-1
         IF (pp.EQ.0) pp=ns
C C$AD DEBUG-HERE CP4 cp.eq.4
         dx = X(cp)-X(pp)
         dy = Y(cp)-Y(pp)
         CALL INCRSQRT(POLYPERIM, dx, dy)
      ENDDO
      RETURN
      END

      SUBROUTINE INCRSQRT(pp, dxx, dyy)
      REAL*8 pp, dxx, dyy, xx2, yy2
      xx2 = dxx*dxx
      yy2 = dyy*dyy
C$AD DEBUG-HERE befsqrt true true
      pp = pp + SQRT(xx2+yy2)
      END
