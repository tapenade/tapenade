C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 17 Jun 2019 17:26
C
C  Differentiation of main as a context to call adjoint code (with options context debugAdjoint no!SpareInits fixinterface):
C L'exemple de test standard du mode debug,
C  cf ~/home/tapenade/debugAD au 2 octobre 2017
C Source code, complete with main, Fortran version.
C
      PROGRAM MAIN_B
      IMPLICIT NONE
      INTEGER ns
      INTEGER i
      REAL*8 x(5), y(5)
      REAL*8 xb(5), yb(5)
      REAL*8 cost
      REAL*8 costb
      REAL*8 POLYCOST
      EXTERNAL ADDEBUGBWD_HERE
      LOGICAL ADDEBUGBWD_HERE
C
      ns = 5
      x(1) = 0.0d0
      y(1) = 0.0d0
      x(2) = 2.0d0
      y(2) = 0.0d0
      x(3) = 2.0d0
      y(3) = 3.0d0
      x(4) = 1.0d0
      y(4) = 6.0d0
      x(5) = -3.0d0
      y(5) = 3.0d0
      CALL ADDEBUGBWD_INIT(0.1_8, 0.87_8)
      IF (ADDEBUGBWD_HERE('end'//CHAR(0))) THEN
        CALL ADDEBUGADJ_WREAL8(costb)
        CALL ADDEBUGADJ_WDISPLAY('end'//CHAR(0), 0)
      END IF
      CALL ADDEBUGBWD_CALL('POLYCOST'//CHAR(0), 0)
      IF (ADDEBUGBWD_HERE('afterCall'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RREAL8(costb)
        CALL ADDEBUGADJ_RDISPLAY('afterCall'//CHAR(0), -1)
      ELSE
        CALL ADDEBUGADJ_SKIP('afterCall'//CHAR(0))
      END IF
      CALL POLYCOST_B(x, xb, y, yb, ns, costb)
      IF (ADDEBUGBWD_HERE('beforeCall'//CHAR(0))) THEN
        CALL ADDEBUGADJ_WREAL8ARRAY(xb, 5)
        CALL ADDEBUGADJ_WREAL8ARRAY(yb, 5)
        CALL ADDEBUGADJ_WDISPLAY('beforeCall'//CHAR(0), -1)
      ELSE
        CALL ADDEBUGADJ_SKIP('beforeCall'//CHAR(0))
      END IF
      CALL ADDEBUGBWD_EXIT()
      IF (ADDEBUGBWD_HERE('start'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RREAL8ARRAY(xb, 5)
        CALL ADDEBUGADJ_RREAL8ARRAY(yb, 5)
        CALL ADDEBUGADJ_RDISPLAY('start'//CHAR(0), 0)
      END IF
      CALL ADDEBUGADJ_CONCLUDE()
      PRINT*, 'cost=', cost
      END

C  Differentiation of polycost in reverse (adjoint) mode (with options context debugAdjoint no!SpareInits fixinterface):
C   gradient     of useful results: polycost
C   with respect to varying inputs: x y
C   RW status of diff variables: x:out y:out polycost:in-killed
C
      SUBROUTINE POLYCOST_B(x, xb, y, yb, ns, polycostb)
      IMPLICIT NONE
      INTEGER ns, i
      REAL*8 polycost
      REAL*8 polycostb
      REAL*8 POLYSURF, POLYPERIM
      REAL*8 x(ns), y(ns)
      REAL*8 xb(ns), yb(ns)
      REAL*8 perim
      REAL*8 perimb
      REAL*8 result1
      REAL*8 result1b
      INTEGER ii1
      EXTERNAL ADDEBUGBWD_HERE
      LOGICAL ADDEBUGBWD_HERE
C
C C$AD DEBUG-CALL false
      perim = POLYPERIM(x, y, ns)
C C$AD DEBUG-HERE midpoint true .FALSE.
      result1 = POLYSURF(x, y, ns)
      polycost = perim*perim/result1
      CALL PUSHREAL8(result1)
      CALL PUSHREAL8(perim)
      IF (ADDEBUGBWD_HERE('exit'//CHAR(0))) THEN
        CALL ADDEBUGADJ_WREAL8(polycostb)
        CALL ADDEBUGADJ_WDISPLAY('exit'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('exit'//CHAR(0))
      END IF
      CALL POPREAL8(perim)
      CALL POPREAL8(result1)
      DO ii1=1,ns
        xb(ii1) = 0.D0
      ENDDO
      DO ii1=1,ns
        yb(ii1) = 0.D0
      ENDDO
      perimb = 0.D0
      result1b = 0.D0
      perimb = perimb + 2*perim*polycostb/result1
      result1b = result1b - perim**2*polycostb/result1**2
      polycostb = 0.D0
      CALL ADDEBUGBWD_CALL('POLYSURF'//CHAR(0), 0)
      IF (ADDEBUGBWD_HERE('afterCall'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RREAL8(perimb)
        CALL ADDEBUGADJ_RREAL8(result1b)
        CALL ADDEBUGADJ_RDISPLAY('afterCall'//CHAR(0), -1)
      ELSE
        CALL ADDEBUGADJ_SKIP('afterCall'//CHAR(0))
      END IF
      CALL POLYSURF_B(x, xb, y, yb, ns, result1b)
      IF (ADDEBUGBWD_HERE('beforeCall'//CHAR(0))) THEN
        CALL ADDEBUGADJ_WREAL8ARRAY(xb, ns)
        CALL ADDEBUGADJ_WREAL8ARRAY(yb, ns)
        CALL ADDEBUGADJ_WREAL8(perimb)
        CALL ADDEBUGADJ_WDISPLAY('beforeCall'//CHAR(0), -1)
      ELSE
        CALL ADDEBUGADJ_SKIP('beforeCall'//CHAR(0))
      END IF
      CALL ADDEBUGBWD_EXIT()
      CALL ADDEBUGBWD_CALL('POLYPERIM'//CHAR(0), 0)
      IF (ADDEBUGBWD_HERE('afterCall'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RREAL8ARRAY(xb, ns)
        CALL ADDEBUGADJ_RREAL8ARRAY(yb, ns)
        CALL ADDEBUGADJ_RREAL8(perimb)
        CALL ADDEBUGADJ_RDISPLAY('afterCall'//CHAR(0), -1)
      ELSE
        CALL ADDEBUGADJ_SKIP('afterCall'//CHAR(0))
      END IF
      CALL POLYPERIM_B(x, xb, y, yb, ns, perimb)
      IF (ADDEBUGBWD_HERE('beforeCall'//CHAR(0))) THEN
        CALL ADDEBUGADJ_WREAL8ARRAY(xb, ns)
        CALL ADDEBUGADJ_WREAL8ARRAY(yb, ns)
        CALL ADDEBUGADJ_WDISPLAY('beforeCall'//CHAR(0), -1)
      ELSE
        CALL ADDEBUGADJ_SKIP('beforeCall'//CHAR(0))
      END IF
      CALL ADDEBUGBWD_EXIT()
      IF (ADDEBUGBWD_HERE('entry'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RREAL8ARRAY(xb, ns)
        CALL ADDEBUGADJ_RREAL8ARRAY(yb, ns)
        CALL ADDEBUGADJ_RDISPLAY('entry'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('entry'//CHAR(0))
      END IF
      END

C  Differentiation of polysurf in reverse (adjoint) mode (with options context debugAdjoint no!SpareInits fixinterface):
C   gradient     of useful results: polysurf
C   with respect to varying inputs: x y
C
      SUBROUTINE POLYSURF_B(x, xb, y, yb, ns, polysurfb)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 polysurf, x(ns), y(ns)
      REAL*8 polysurfb, xb(ns), yb(ns)
      INTEGER cp, pp
      REAL*8 tempb
      INTEGER*4 branch
      INTEGER ii1
      EXTERNAL ADDEBUGBWD_HERE
      LOGICAL ADDEBUGBWD_HERE
C
      polysurf = 0.0
      DO cp=1,ns
        CALL PUSHINTEGER4(pp)
        pp = cp - 1
        IF (pp .EQ. 0) THEN
          pp = ns
          CALL PUSHCONTROL1B(0)
        ELSE
          CALL PUSHCONTROL1B(1)
        END IF
        polysurf = polysurf + (x(pp)*y(cp)-x(cp)*y(pp))/2
      ENDDO
      CALL PUSHINTEGER4(pp)
      IF (ADDEBUGBWD_HERE('exit'//CHAR(0))) THEN
        CALL ADDEBUGADJ_WREAL8(polysurfb)
        CALL ADDEBUGADJ_WDISPLAY('exit'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('exit'//CHAR(0))
      END IF
      CALL POPINTEGER4(pp)
      DO ii1=1,ns
        xb(ii1) = 0.D0
      ENDDO
      DO ii1=1,ns
        yb(ii1) = 0.D0
      ENDDO
      DO cp=ns,1,-1
        tempb = polysurfb/2
        xb(pp) = xb(pp) + y(cp)*tempb
        yb(cp) = yb(cp) + x(pp)*tempb
        xb(cp) = xb(cp) - y(pp)*tempb
        yb(pp) = yb(pp) - x(cp)*tempb
        CALL POPCONTROL1B(branch)
        IF (branch .EQ. 0) THEN
          IF (.FALSE. .AND. ADDEBUGBWD_HERE('middle'//CHAR(0))) THEN
            CALL ADDEBUGADJ_RWREAL8ARRAY(xb, ns)
            CALL ADDEBUGADJ_RWREAL8ARRAY(yb, ns)
            CALL ADDEBUGADJ_RWREAL8(polysurfb)
            CALL ADDEBUGADJ_RWDISPLAY('middle'//CHAR(0), 0)
          ELSE
            CALL ADDEBUGADJ_SKIP('middle'//CHAR(0))
          END IF
        END IF
        CALL POPINTEGER4(pp)
      ENDDO
      polysurfb = 0.D0
      IF (ADDEBUGBWD_HERE('entry'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RREAL8ARRAY(xb, ns)
        CALL ADDEBUGADJ_RREAL8ARRAY(yb, ns)
        CALL ADDEBUGADJ_RDISPLAY('entry'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('entry'//CHAR(0))
      END IF
      END

C  Differentiation of polyperim in reverse (adjoint) mode (with options context debugAdjoint no!SpareInits fixinterface):
C   gradient     of useful results: x y polyperim
C   with respect to varying inputs: x y
C
      SUBROUTINE POLYPERIM_B(x, xb, y, yb, ns, polyperimb)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 polyperim, x(ns), y(ns)
      REAL*8 polyperimb, xb(ns), yb(ns)
      INTEGER cp, pp
      REAL*8 dx, dy
      REAL*8 dxb, dyb
      INTEGER*4 branch
      EXTERNAL ADDEBUGBWD_HERE
      LOGICAL ADDEBUGBWD_HERE
C
      polyperim = 0.0
      DO cp=1,ns
        CALL PUSHINTEGER4(pp)
        pp = cp - 1
        IF (pp .EQ. 0) THEN
          pp = ns
          CALL PUSHCONTROL1B(0)
        ELSE
          CALL PUSHCONTROL1B(1)
        END IF
C C$AD DEBUG-HERE CP4 cp.eq.4
        dx = x(cp) - x(pp)
        dy = y(cp) - y(pp)
        CALL PUSHREAL8(polyperim)
        CALL INCRSQRT(polyperim, dx, dy)
      ENDDO
      CALL PUSHINTEGER4(pp)
      IF (ADDEBUGBWD_HERE('exit'//CHAR(0))) THEN
        CALL ADDEBUGADJ_WREAL8ARRAY(xb, ns)
        CALL ADDEBUGADJ_WREAL8ARRAY(yb, ns)
        CALL ADDEBUGADJ_WREAL8(polyperimb)
        CALL ADDEBUGADJ_WDISPLAY('exit'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('exit'//CHAR(0))
      END IF
      CALL POPINTEGER4(pp)
      dxb = 0.D0
      dyb = 0.D0
      DO cp=ns,1,-1
        dx = x(cp) - x(pp)
        dy = y(cp) - y(pp)
        CALL ADDEBUGBWD_CALL('INCRSQRT'//CHAR(0), 0)
        IF (ADDEBUGBWD_HERE('afterCall'//CHAR(0))) THEN
          CALL ADDEBUGADJ_RREAL8ARRAY(xb, ns)
          CALL ADDEBUGADJ_RREAL8ARRAY(yb, ns)
          CALL ADDEBUGADJ_RREAL8(polyperimb)
          CALL ADDEBUGADJ_RDISPLAY('afterCall'//CHAR(0), -1)
        ELSE
          CALL ADDEBUGADJ_SKIP('afterCall'//CHAR(0))
        END IF
        CALL POPREAL8(polyperim)
        CALL INCRSQRT_B(polyperim, polyperimb, dx, dxb, dy, dyb)
        IF (ADDEBUGBWD_HERE('beforeCall'//CHAR(0))) THEN
          CALL ADDEBUGADJ_WREAL8ARRAY(xb, ns)
          CALL ADDEBUGADJ_WREAL8ARRAY(yb, ns)
          CALL ADDEBUGADJ_WREAL8(polyperimb)
          CALL ADDEBUGADJ_WREAL8(dxb)
          CALL ADDEBUGADJ_WREAL8(dyb)
          CALL ADDEBUGADJ_WDISPLAY('beforeCall'//CHAR(0), -1)
        ELSE
          CALL ADDEBUGADJ_SKIP('beforeCall'//CHAR(0))
        END IF
        CALL ADDEBUGBWD_EXIT()
        yb(cp) = yb(cp) + dyb
        yb(pp) = yb(pp) - dyb
        dyb = 0.D0
        xb(cp) = xb(cp) + dxb
        xb(pp) = xb(pp) - dxb
        dxb = 0.D0
        CALL POPCONTROL1B(branch)
        IF (branch .EQ. 0) THEN
          IF (.FALSE. .AND. ADDEBUGBWD_HERE('middle'//CHAR(0))) THEN
            CALL ADDEBUGADJ_RWREAL8ARRAY(xb, ns)
            CALL ADDEBUGADJ_RWREAL8ARRAY(yb, ns)
            CALL ADDEBUGADJ_RWREAL8(polyperimb)
            CALL ADDEBUGADJ_RWDISPLAY('middle'//CHAR(0), 0)
          ELSE
            CALL ADDEBUGADJ_SKIP('middle'//CHAR(0))
          END IF
        END IF
        CALL POPINTEGER4(pp)
      ENDDO
      polyperimb = 0.D0
      IF (ADDEBUGBWD_HERE('entry'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RREAL8ARRAY(xb, ns)
        CALL ADDEBUGADJ_RREAL8ARRAY(yb, ns)
        CALL ADDEBUGADJ_RDISPLAY('entry'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('entry'//CHAR(0))
      END IF
      END

C  Differentiation of incrsqrt in reverse (adjoint) mode (with options context debugAdjoint no!SpareInits fixinterface):
C   gradient     of useful results: pp
C   with respect to varying inputs: dxx dyy pp
C
      SUBROUTINE INCRSQRT_B(pp, ppb, dxx, dxxb, dyy, dyyb)
      IMPLICIT NONE
      REAL*8 pp, dxx, dyy, xx2, yy2
      REAL*8 ppb, dxxb, dyyb, xx2b, yy2b
      INTRINSIC SQRT
      REAL*8 result1
      REAL*8 result1b
      REAL*8 tempb
      EXTERNAL ADDEBUGBWD_HERE
      LOGICAL ADDEBUGBWD_HERE
      xx2 = dxx*dxx
      yy2 = dyy*dyy
      result1 = SQRT(xx2 + yy2)
      pp = pp + result1
      CALL PUSHREAL8(xx2)
      CALL PUSHREAL8(yy2)
      IF (ADDEBUGBWD_HERE('exit'//CHAR(0))) THEN
        CALL ADDEBUGADJ_WREAL8(ppb)
        CALL ADDEBUGADJ_WDISPLAY('exit'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('exit'//CHAR(0))
      END IF
      CALL POPREAL8(yy2)
      CALL POPREAL8(xx2)
      dxxb = 0.D0
      dyyb = 0.D0
      result1b = 0.D0
      yy2b = 0.D0
      xx2b = 0.D0
      result1b = result1b + ppb
      IF (xx2 + yy2 .EQ. 0.0) THEN
        tempb = 0.D0
      ELSE
        tempb = result1b/(2.0*SQRT(xx2+yy2))
      END IF
      result1b = 0.D0
      xx2b = xx2b + tempb
      yy2b = yy2b + tempb
      IF (.TRUE. .AND. ADDEBUGBWD_HERE('befsqrt'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RWREAL8(ppb)
        CALL ADDEBUGADJ_RWREAL8(yy2b)
        CALL ADDEBUGADJ_RWREAL8(xx2b)
        CALL ADDEBUGADJ_RWDISPLAY('befsqrt'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('befsqrt'//CHAR(0))
      END IF
      dyyb = dyyb + 2*dyy*yy2b
      yy2b = 0.D0
      dxxb = dxxb + 2*dxx*xx2b
      xx2b = 0.D0
      IF (ADDEBUGBWD_HERE('entry'//CHAR(0))) THEN
        CALL ADDEBUGADJ_RREAL8(dxxb)
        CALL ADDEBUGADJ_RREAL8(dyyb)
        CALL ADDEBUGADJ_RREAL8(ppb)
        CALL ADDEBUGADJ_RDISPLAY('entry'//CHAR(0), 0)
      ELSE
        CALL ADDEBUGADJ_SKIP('entry'//CHAR(0))
      END IF
      END

