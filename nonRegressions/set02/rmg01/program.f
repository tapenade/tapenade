      SUBROUTINE selectcase2(kOutput,NumErrors,x,y) 
      ! SELECT CASE et flow de control complique
      INTEGER		      kOutput
      INTEGER		      NumErrors		
      REAL                    x, y
      INTEGER		      j				
      INTEGER		      iFix
      INTEGER, PARAMETER :: MaxBsec = 10 
      INTEGER         NumBsec
      INTEGER     BsecBcode 
      COMMON /BsecNumData/ NumBsec, BsecBcode(MaxBsec)
      DO j = 1, NumBsec
        iFix = BsecBcode(j)
        x =10.
        if ( y.gt.0. ) goto 1
        SELECT CASE (iFix)
        CASE (2)
          NumErrors=j
          kOutput=2
          if ( x.gt.0. ) goto 10
          x = 2*y
        CASE (3)
          NumErrors=j+2
          kOutput=4
  1       x = 2*y+3
        END SELECT
 10     x = x + 10*y+3
      ENDDO
      RETURN
      END

