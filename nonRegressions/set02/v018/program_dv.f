C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) - 27 Aug 2021 14:24
C
C  Differentiation of f in forward (tangent) mode (with options multiDirectional):
C   variations   of useful results: f
C   with respect to varying inputs: t
C   RW status of diff variables: f:out t:in
      SUBROUTINE F_DV(t, td, f, fd, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      DOUBLE PRECISION t
      DOUBLE PRECISION td(nbdirsmax)
      REAL b
      DIMENSION b(5)
      REAL a_oben
      DIMENSION a_oben(5)
      REAL a_obend
      DIMENSION a_obend(5)
      REAL en
      DIMENSION en(5)
      REAL bd
      DIMENSION bd(5)
      REAL wp
      DIMENSION wp(6, 6)
      REAL daic
      DIMENSION daic(8, 8)
      REAL x_oben
      REAL x_obend
      INTEGER j
      REAL enx
      REAL delrx2
      REAL tyz
      INTRINSIC EXP
      INTEGER nd
      DOUBLE PRECISION f
      DOUBLE PRECISION fd(nbdirsmax)
      INTEGER nbdirs
      f = t*t
      DO nd=1,nbdirs
        fd(nd) = 2*t*td(nd)
        fd(nd) = EXP(f)*fd(nd)
      ENDDO
      bd(2) = -(0.5d0*(a_obend(1)*x_oben**(-0.5d0)-a_oben(1)*(0.5d0*
     +  x_oben**(-0.5d0-1))*x_obend))
      b(2) = -(0.5d0*a_oben(1)*x_oben**(-0.5d0))
      wp(3, j) = .5d0*(-(3.d0*enx*en(3)))
      daic(4, 6) = daic(4, 6) + (-(delrx2*tyz))
      f = EXP(f)
      RETURN
      END

