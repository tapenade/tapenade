C     This file is part of NuWTun, see <http://nuwtun.berlios.de>, and was
C     Portions Copyright (C) 2001 Joseph H. Morrison
C     This code is part of ISAAC.
C
      SUBROUTINE SMILIM(DQP1, DQP2, DQM1)
      SMI(D1, D2, EPSQ) = ( D1*(D2*D2+2.E0*EPSQ) 
     1                    + D2*(2.E0*D1*D1+EPSQ) )
     2                / ( 2.E0*D1*D1 - D1*D2 + 2.E0*D2*D2 + 3.E0*EPSQ )
      EPSQ = 1.0E-4
      XP   = SMI( DQP1, DQP2, EPSQ )
      XM   = SMI( DQP2, DQP1, EPSQ ) 
      DQP1 = - 0.5E0 * XP
      DQM1 =   0.5E0 * XM
      RETURN
      END
