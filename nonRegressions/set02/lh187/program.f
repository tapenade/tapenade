c Modifie depuis lh01 pour couvrir divers
c cas de FunctionDecl dans la SymbolTable
      subroutine top(i1,i2,i3,o1,o2,o3)
      real i1, i2, i3
      real o1, o2, o3
      real l1, l2, l3
      external extf,foo,gee
c
      o3 = i3*i2
      call sub1(i1, i2, o1, o2, foo, gee)
      o1 = o1*o2*i2
      call extf(o2,o3)
      o3 = 2.0
      i2 = 5.0
      end


      subroutine sub1(i1, i2, o1, o2, vf1, vf2)
      external vf1
      external intg
      real i1, i2
      real o1, o2
      real l1, l2, intg
c
      l1 = i1*i2
      call vf2(i1,i2)
      l2 = i1-3*i2
      call sub2(i2,o2,vf1)
      o1 = intg(l1/l2)
      end


      subroutine sub2(x,y,vf)
      external vf
      real x, y, vf
c
      y = vf(x,y)
      end


      function foo(a,b)
      real a, b
      real foo
c
      foo = a*b
      end


      subroutine gee(a,b)
      real a, b
c
      b = b+a
      a = a/b
      end
