      SUBROUTINE XMMVEC_DIAG (NL1,NC1,L1MAX,K1MAX,MAT1,
     +          VECT,VECRES)
      DOUBLE PRECISION MAT1(L1MAX,K1MAX),VECT(K1MAX)
      DOUBLE PRECISION VECRES(L1MAX)
      INTEGER I2,KK,NL1,NC1,L1MAX,K1MAX
      DO 150 I2 = 1,NL1
        VECRES(I2) = MAT1(I2,I2)*VECT(I2)
 150  CONTINUE
      RETURN
      END
