      SUBROUTINE MOTRM2(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_APPORT,
     &      xmo_XEBRIM_APPORT,
     &      xmo_VTBRIM_APPORT,
     &      xmo_PORRIM_APPORT,
     &      xmo_XEPORRIM_APPORT,
     &      xmo_VTPORRIM_APPORT )
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      DOUBLE PRECISION 
     &                 xmo_CXE(10),
     &                 xmo_CXEJ(10),
     &                 xmo_XEHOM_APPORT(10)


C si on remplace cette ligne par le if
C arg1 est declare !!!!!

      xmo_RHOD1 = min(xmo_RHOD, xmo_RHOD_FRLIM)

C      IF (xmo_rhod .GT. xmo_rhod_frlim) THEN
C        xmo_rhod1 = xmo_rhod_frlim
C      ELSE
C        xmo_rhod1 = xmo_rhod
C      END IF


      IF ( xmo_RHOD .LT. xmo_RHOD_FRLIM ) THEN
        IF (mo_MODELE_FRAC .EQ. 1) THEN
          xmo_DFRSDT = xmo_PIZAHL / 4.D0 / xmo_RHODL *
     &      cos( xmo_PIZAHL / 2.D0 / xmo_RHODL *
     &      ( xmo_RHOLIM - xmo_RHOD1 ) ) * xmo_FACRHO * xmo_RHOD1
        ELSEIF (mo_MODELE_FRAC .EQ. 2) THEN
          xmo_DFRSDT = xmo_FACRHO * xmo_RHOD1
     &                 / 2.D0 / xmo_RHODL
        ENDIF
      ELSE
        xmo_DFRSDT = 0.D0
      ENDIF


        DO 100 IVGA = 1, mo_NSEQR
          xmo_XEHOM_APPORT(IVGA) =
     &     xmo_DFRSDT * (xmo_CXEM + xmo_CXEJM 
     &                   + xmo_CXEBJ + xmo_CXEB + xmo_CXEPOR)
     &       / (1.D0-xmo_FR)    

 100    CONTINUE

        xmo_PORRIM_APPORT = 
     &      xmo_DFRSDT * xmo_CPORJ / (1.D0-xmo_FR)

      RETURN
      END
