C The example from our famous "polygon" optimization training !!

      FUNCTION POLYPERIM(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 POLYPERIM, X(ns), Y(ns)
      INTEGER cp,pp
      REAL*8 dx,dy
c
      POLYPERIM = 0.0
      DO cp=1,ns
         pp = cp-1
         IF (pp.EQ.0) pp=ns
         dx = X(cp)-X(pp)
         dy = Y(cp)-Y(pp)
         POLYPERIM = POLYPERIM + SQRT(dx*dx+dy*dy)
      ENDDO
      RETURN
      END

      FUNCTION POLYSURF(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 POLYSURF, X(ns), Y(ns)
      INTEGER cp,pp
c
      POLYSURF = 0.0
      DO cp=1,ns
         pp = cp-1
         IF (pp.EQ.0) pp=ns
         POLYSURF = POLYSURF + (X(cp)*Y(pp) - X(pp)*Y(cp))/2
      ENDDO
      RETURN
      END

      FUNCTION POLYCOST(X,Y,ns)
      IMPLICIT NONE
      INTEGER ns
      REAL*8 POLYCOST
      REAL*8 POLYSURF,POLYPERIM
      REAL*8 X(ns),Y(ns)
      REAL*8 perim
c
      perim = POLYPERIM(X,Y,ns)
      POLYCOST = (perim*perim)/POLYSURF(X,Y,ns)
      RETURN
      END
