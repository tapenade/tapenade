      program program03
      parameter(nsma=2000,ntma=4000,nsgma=6000,nfma=1000)
      parameter(ncama=20,ncorma=10)

      dimension t1(nsma),t2(nsma),t3(nsma),t4(nsma),
     +          pres(nsma),g2(nsma),g3(nsma),iarg(10),
     +          icora(0:ncorma),noe1(nfma),vn1(2,nfma),coor(2,nsma)

C $AD NOCHECKPOINT
      call calcl(t1,t2,t3,t4,pres,g2,g3,iarg,icora,
     .                 noe1,vn1,coor)
      end

      subroutine calcl(t1,t2,t3,t4,pres,g2,g3,iarg,icora,
     .                 noe1,vn1,coor)
c
      parameter(nsma=2000,ntma=4000,nsgma=6000,nfma=1000)
      parameter(ncama=20,ncorma=10)

      dimension t1(nsma),t2(nsma),t3(nsma),t4(nsma),
     +          pres(nsma),g2(nsma),g3(nsma),iarg(10),
     +          icora(0:ncorma),noe1(nfma),vn1(2,nfma),coor(2,nsma)
c
      common/c3/nt,ns,nseg,nf1,nf2,nf3,nca,ncor,nf3ma,nsegns,ncans
      common/c4/gama,ugama,gam1,ugam1,gamg,ugamg
      common/c5/chrd(10),xmom(10),ymom(10),xorg(10),yorg(10)
      common/c7/cla,clpr,cda,cdpr,cma
      common/c9/amach,ecinf,angat,betinf,qinf,hinf
c
      cx=0.0
      cz=0.0
      cmm=0.0
c
      do 2 icor=1,ncor
      nco1 = icora(icor-1)+1
      nco2 = icora(icor)
      coe  = 1. /ecinf
      do 1 if1=nco1,nco2
      is = noe1(if1)
      ppnx=pres(is)*vn1(1,if1)*coe
      ppny=pres(is)*vn1(2,if1)*coe
      cx =cx+ppnx
      cz =cz+ppny
      dx = coor(1,is)-xmom(icor)
      dy = coor(2,is)-ymom(icor)
      cmm=cmm-ppny*dx+ppnx*dy
    1 continue
    2 continue
      cla=cz*cos(angat)-cx*sin(angat)
      cda=cz*sin(angat)+cx*cos(angat)
      cma=cmm
c
      return
      end
