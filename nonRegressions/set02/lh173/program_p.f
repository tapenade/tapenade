C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.5 (r3702M) - 18 Feb 2011 13:30
C
C Ce programme, recursif, faisait boucler
C l'analyse d'activite de Tapenade v3.5,r3702
      SUBROUTINE TOP(x, y)
      IMPLICIT NONE
      REAL x, y, z
      z = 0.0
      y = 1.0
      CALL RECUR(x, y, z)
      y = 2.0
      END

C
      SUBROUTINE RECUR(a, b, c)
      IMPLICIT NONE
      REAL a, b, c
      REAL e
      e = 3.0*a
      a = a - 1.0
      IF (a .GT. 1.0) CALL RECUR(a, e, b)
      a = a*e
      b = 4.0
      END

