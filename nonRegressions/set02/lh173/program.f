C Ce programme, recursif, faisait boucler
C l'analyse d'activite de Tapenade v3.5,r3702
      subroutine top(X,Y)
      REAL X,Y,Z
      Z=0.0
      Y=1.0
      call recur(X,Y,Z)
      Y=2.0
      end

      subroutine recur(A,B,C)
      REAL A,B,C
      REAL E
      E=3.0*A
      A = A - 1.0
      if (A.gt.1.0) then
         call recur(A,E,B)
      endif
      A = A*E
      B = 4.0
      end
