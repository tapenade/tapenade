      subroutine roe_flux(x1,qcr, qvl, qvr, resl) 

      integer nvar
      parameter(nvar=4)

      double precision x1(2), qcr(nvar), qvl(nvar),
     *                 qvr(nvar), resl(nvar)

      integer          i
      double precision rl, ul, vl, pl, al2, hl, rr, ur, vr, pr, ar2, hr,
     *                 ua, va, aa, ha,
     *                 ql2, qr2, rl12, rr12, rd,
     *                 unl, unr, una, vna, ct, st, Fc(4), Fd(4),
     *                 l1, l2, l3, l4,
     *                 aact, aast,
     *                 flux, dl, dr, li(4), limit,
     *                 lent, e1, e4, del, ETOL
      parameter(ETOL=0.01d0)
      intrinsic        dmin1

      lent = dsqrt(ct**2 + st**2)
      ct = ct/lent
      st = st/lent

      do i=1,4
         dr    = qvr(i) - qcr(i)
         li(i) = LIMIT(dl, dr)
      enddo

      rr = qcr(1) - 0.5d0*li(1)
      ur = qcr(2) - 0.5d0*li(2)
      vr = qcr(3) - 0.5d0*li(3)
      pr = qcr(4) - 0.5d0*li(4)
      ql2= ul**2 + vl**2
      al2= GAMMA*pl/rl
      hl = al2/GAMMA1 + 0.5d0*ql2
      qr2= ur**2 + vr**2
      ar2= GAMMA*pr/rr
      hr = ar2/GAMMA1 + 0.5d0*qr2
      unl = ul*ct + vl*st
      unr = ur*ct + vr*st
      Fc(1) = rl*unl            + rr*unr
      Fc(2) = pl*ct + rl*ul*unl + pr*ct + rr*ur*unr
      Fc(3) = pl*st + rl*vl*unl + pr*st + rr*vr*unr
      Fc(4) = rl*hl*unl         + rr*hr*unr

      do i=1,4
         flux    = 0.5d0*lent*( Fc(i) - Fd(i) )
         resl(i) = resl(i) + flux
      enddo

      return
      end
