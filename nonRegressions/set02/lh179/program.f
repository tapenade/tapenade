!Un essai pour reproduire
! le bug de Hubert d'initialisation de tableaux derives
! dans le cas ALLOCATABLE, initialisations peut-etre
! inutiles et surtout qui devraient etre vectorielles.
       MODULE MODVARS
         real,dimension(:),allocatable :: var1,var2,var3
       END MODULE MODVARS

       SUBROUTINE TOP(x,y)
         USE MODVARS
         real :: x,y
         CALL PP1(x)
         CALL PP2()
         CALL PP2()
         CALL PP3(y)
       END SUBROUTINE TOP

       SUBROUTINE PP1(x)
         USE MODVARS
         real::x
         integer i
         do i=1,100
            var1(i) = var1(i)+x
         enddo
       END SUBROUTINE PP1

       SUBROUTINE PP2
         USE MODVARS
         integer i
         do i=1,100
            var3(i) = var3(i) + var2(i)
         enddo
         do i=1,100
            var2(i) = var2(i) + var1(i)
         enddo
       END SUBROUTINE PP2

       SUBROUTINE PP3(y)
         USE MODVARS
         real::y
         y = y + var2(5)*var3(4)
       END SUBROUTINE PP4
