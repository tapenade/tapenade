C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 17 Jun 2019 17:26
C
C  Differentiation of sub1 in forward (tangent) mode:
C   variations   of useful results: y
C   with respect to varying inputs: x
C   RW status of diff variables: x:in y:out
C
      SUBROUTINE SUB1_D(x, xd, y, yd)
      IMPLICIT NONE
      DOUBLE PRECISION x, y
      DOUBLE PRECISION xd, yd
      DOUBLE PRECISION f, g
      DOUBLE PRECISION TESTMIN11
      DOUBLE PRECISION TESTMIN11_D
      INTRINSIC SQRT
      DOUBLE PRECISION temp
      yd = TESTMIN11_D(x, xd, y)
      temp = SQRT(y)
      IF (y .EQ. 0.0) THEN
        yd = 0.D0
      ELSE
        yd = yd/(2.0*temp)
      END IF
      y = temp
      RETURN
      END

C  Differentiation of testmin11 in forward (tangent) mode:
C   variations   of useful results: testmin11
C   with respect to varying inputs: t
      DOUBLE PRECISION FUNCTION TESTMIN11_D(t, td, testmin11)
      IMPLICIT NONE
      DOUBLE PRECISION t
      DOUBLE PRECISION td
      INTRINSIC MIN
      INTRINSIC EXP
      DOUBLE PRECISION min1
      DOUBLE PRECISION min1d
      DOUBLE PRECISION testmin11
      IF (0. .GT. t) THEN
        min1d = td
        min1 = t
      ELSE
        min1 = 0.
        min1d = 0.D0
      END IF
      testmin11_d = min1*td + t*min1d
      testmin11 = t*min1
      testmin11_d = EXP(testmin11)*testmin11_d
      testmin11 = EXP(testmin11)
      RETURN
      END

