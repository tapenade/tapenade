C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.3 (r3316M) - 19 Jan 2010 14:59
C
C  Differentiation of testbug in reverse (adjoint) mode:
C   gradient     of useful results: res arg
C   with respect to varying inputs: res arg
C   RW status of diff variables: res:in-out arg:incr
C bug found in huuneus-lidar when introducing reverse mode recomputation:
C statement wist(n)=2.0 in the inner loop disappears !!!
      SUBROUTINE TESTBUG_B(arg, argb, res, resb)
      IMPLICIT NONE
      REAL arg, res
      REAL argb, resb
      REAL wist(999)
      REAL s2
      INTEGER nmu, n
C
      DO nmu=0,50
C
        DO n=1,39
          wist(n) = 2.0
        ENDDO
        n = 40
        wist(n) = 3.0
        CALL PUSHREAL4(s2)
        s2 = 0.0
        DO n=1,40
          s2 = s2 + wist(n)
        ENDDO
      ENDDO
      DO nmu=50,0,-1
        argb = argb + s2*resb
        CALL POPREAL4(s2)
      ENDDO
      END

