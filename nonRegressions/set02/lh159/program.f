C bug found in huuneus-lidar when introducing reverse mode recomputation:
C statement wist(n)=2.0 in the inner loop disappears !!!
      subroutine testbug(ARG,RES)
      REAL ARG,RES
      REAL wist(1:999)
      REAL s2
      INTEGER nmu,n

      DO Nmu=0, 50

         DO n=1,39
            wist(n)=2.0
         ENDDO
         
         n=40
         wist(n)=3.0
         
         s2=0.0
         DO n=1,40
            s2=s2+wist(n)
         ENDDO
         RES = RES + s2*ARG
         
      ENDDO
         
      END
