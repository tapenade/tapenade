C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.9 (r5103M) -  4 Mar 2014 16:26
C
C  Differentiation of testbug in forward (tangent) mode:
C   variations   of useful results: res
C   with respect to varying inputs: res arg
C   RW status of diff variables: res:in-out arg:in
C bug found in huuneus-lidar when introducing reverse mode recomputation:
C statement wist(n)=2.0 in the inner loop disappears !!!
      SUBROUTINE TESTBUG_D(arg, argd, res, resd)
      IMPLICIT NONE
      REAL arg, res
      REAL argd, resd
      REAL wist(999)
      REAL s2
      INTEGER nmu, n
C
      DO nmu=0,50
C
        DO n=1,39
          wist(n) = 2.0
        ENDDO
        n = 40
        wist(n) = 3.0
        s2 = 0.0
        DO n=1,40
          s2 = s2 + wist(n)
        ENDDO
        resd = resd + s2*argd
        res = res + s2*arg
      ENDDO
      END

