      subroutine testprec(a,x)
c  Pour tester les precedences des expressions logiques.
      real A(100),x
      if ((A(1).gt.0 .or. A(2).gt.0) .or. A(3).gt.0) then
         x = 2*x
      endif
      if (A(1).gt.0 .or. (A(2).gt.0 .or. A(3).gt.0)) then
         x = 2*x
      endif
      if (A(1).gt.0 .or. A(2).gt.0 .or. A(3).gt.0) then
         x = 2*x
      endif

      if ((A(1).gt.0 .or. A(2).gt.0) .and. A(3).gt.0) then
         x = 2*x
      endif
      if (A(1).gt.0 .or. (A(2).gt.0 .and. A(3).gt.0)) then
         x = 2*x
      endif
      if (A(1).gt.0 .or. A(2).gt.0 .and. A(3).gt.0) then
         x = 2*x
      endif

      if ((A(1).gt.0 .and. A(2).gt.0) .or. A(3).gt.0) then
         x = 2*x
      endif
      if (A(1).gt.0 .and. (A(2).gt.0 .or. A(3).gt.0)) then
         x = 2*x
      endif
      if (A(1).gt.0 .and. A(2).gt.0 .or. A(3).gt.0) then
         x = 2*x
      endif

      if ((A(1).gt.0 .and. A(2).gt.0) .and. A(3).gt.0) then
         x = 2*x
      endif
      if (A(1).gt.0 .and. (A(2).gt.0 .and. A(3).gt.0)) then
         x = 2*x
      endif
      if (A(1).gt.0 .and. A(2).gt.0 .and. A(3).gt.0) then
         x = 2*x
      endif



      if (.not.(A(1).gt.0 .or. A(2).gt.0) .or. .not.A(3).gt.0) then
         x = 2*x
      endif
      if (A(1).gt.0 .or. (.not.A(2).gt.0 .or. A(3).gt.0)) then
         x = 2*x
      endif
      if (A(1).gt.0 .or. .not.A(2).gt.0 .or. A(3).gt.0) then
         x = 2*x
      endif

      if ((A(1).gt.0 .or. A(2).gt.0) .and. .not.A(3).gt.0) then
         x = 2*x
      endif
      if (A(1).gt.0 .or. .not.(A(2).gt.0 .and. A(3).gt.0)) then
         x = 2*x
      endif
      if (A(1).gt.0 .or. .not.A(2).gt.0 .and. A(3).gt.0) then
         x = 2*x
      endif

      if ((A(1).gt.0 .and. .not.A(2).gt.0) .or. .not.A(3).gt.0) then
         x = 2*x
      endif
      if (A(1).gt.0 .and. .not.(.not.A(2).gt.0 .or. A(3).gt.0)) then
         x = 2*x
      endif
      if (A(1).gt.0 .and. A(2).gt.0 .or. .not.A(3).gt.0) then
         x = 2*x
      endif

      if ((A(1).gt.0 .and. .not.A(2).gt.0) .and. A(3).gt.0) then
         x = 2*x
      endif
      if (.not.A(1).gt.0 .and. .not.(A(2).gt.0 .and. A(3).gt.0)) then
         x = 2*x
      endif
      if (A(1).gt.0 .and. .not.A(2).gt.0 .and. .not.A(3).gt.0) then
         x = 2*x
      endif

      end
