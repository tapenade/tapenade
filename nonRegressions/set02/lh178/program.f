      SUBROUTINE top(a,b)
c Bug rencontre sur opa-nemo.
c En diff inverse binomiale, le checkpoint
c doit contenir "b", meme s'il n'est pas necessaire
c en diff inverse normale. La raison est que "b" est
c necessaire pour les "advance" suivis d'un "turn"..
      REAL a,b
      
      a = a*a
      b=b*a*b
C$AD BINOMIAL-CKP 11 3 1
      DO i=1,10
         call foo(a,b)
      ENDDO
      a = 2*a*a
      END

      SUBROUTINE foo(a2,b2)
      REAL a2,b2
      b2 = b2+1.0
      a2 = a2*a2 + b2
      END
