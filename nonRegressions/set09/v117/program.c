#include <stdio.h>

void test(float *p1, float **p2, float  *q1, float *r1, 
          float u, float v, int i) {
  // p2 = &p1 || &q1;
  if (i == 0) {
    p2 = &p1;
  } else {
    p2 = &q1;
  }
  //r1 = &u || &v || NULL;
  if (i == 0) {
    r1 = &u;
  } else if (i == 1) {
    r1 = &v;
  } else {
    r1 = NULL;
  }
  *p2 = r1;
  r1 = NULL;
}
