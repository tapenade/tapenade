/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) - 14 Feb 2022 12:36
*/
#include <adStack.h>

/*
  Differentiation of klaus_dieter in reverse (adjoint) mode:
   gradient     of useful results: xa[_:_] x klaus_dieter
   with respect to varying inputs: xa[_:_] x
   RW status of diff variables: xa:(loc) xa[_:_]:incr x:incr klaus_dieter:in-killed
   Plus diff mem management of: xa:in
*/
void klaus_dieter_b(int nue, double xa[], double xab[], double x, double *xb, 
        double klaus_dieterb) {
    double y = 0.0;
    double yb = 0.0;
    yb = klaus_dieterb;
    {
      double temp;
      double tempb;
      for (int i = nue-1; i > -1; --i) {
          temp = xa[nue+i]*x;
          xab[i] = xab[i] + sin(temp)*yb;
          tempb = cos(temp)*xa[i]*yb;
          xab[nue + i] = xab[nue + i] + x*tempb;
          *xb = *xb + xa[nue+i]*tempb;
      }
    }
}
