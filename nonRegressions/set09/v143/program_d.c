/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
/*
  Differentiation of klaus_dieter in forward (tangent) mode:
   variations   of useful results: klaus_dieter
   with respect to varying inputs: xa[_:_] x
   RW status of diff variables: xa:(loc) xa[_:_]:in x:in klaus_dieter:out
   Plus diff mem management of: xa:in
*/
double klaus_dieter_d(int nue, double xa[], double xad[], double x, double xd,
        double *klaus_dieter) {
    double y = 0;
    double yd;
    yd = 0.0;
    {
      double temp;
      double temp0;
      for (int i = 0; i < nue; ++i) {
          temp = xa[nue+i]*x;
          temp0 = sin(temp);
          yd = yd + temp0*xad[i] + xa[i]*cos(temp)*(x*xad[nue+i]+xa[nue+i]*xd)
          ;
          y = y + xa[i]*temp0;
      }
    }
    *klaus_dieter = y;
    return yd;
}
