double f(float x)
{
    const int n=10;
    int i, j;
    double a;

    a=(double)x;
    for (i=0; i<n-1; i++)
        a*=2;

    return a;
}

int main(int argc, char** argv)
{
    double x_in, x_out;

    x_in=10.;
    x_out= f(x_in);

    return 0;
}
