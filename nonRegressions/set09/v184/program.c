double quad(double x, double y, void *params)
{
	double *p = (double *)params;
	double v = x - p[0];
	double w = y - p[1];

	return p[2] * v * v + p[3] * w * w + p[4];
}

