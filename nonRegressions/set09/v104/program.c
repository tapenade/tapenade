float test2 (float, float);

float test1 (float a, float b) { 
  float result;
  if (a < b) {
    result = a * b;
  } else {
    result = test2(a,b);
  }
  return result;
}

float test2 (float a, float b) { 
  float result;
  if (a < b) {
    result = test1(a,b);
  } else {
    result = a * b;
  }
  return result;
}

float test(float x, float y) {
  float result;
  result = test1(x,y);
  return result;
}

main() {
  float a = 5.0;
  float b = 2.0;
  printf("--> %f \n" , test(a,b));
}
