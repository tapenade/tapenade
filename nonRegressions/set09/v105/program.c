float test1 (float, float, int);
float test2 (float, float, int);

float test0 (float a, float b, int i) { 
  float result;
  switch (i) {
   case 0: {
     result = a * b;
     break;
   }
   case 1: {
     result = test1(a,b,i);
     break;
   }
   case 2: {
     result = test2(a,b,i);
     break;
   }
  }
  return result;
}

float test1 (float a, float b, int i) { 
  float result;
  switch (i) {
   case 0: {
     result = test0(a,b,i);
     break;
   }
   case 1: {
     result = a * b;
     break;
   }
   case 2: {
     result = test2(a,b,i);
     break;
   }
  }
  return result;
}

float test2 (float a, float b, int i) { 
  float result;
  switch (i) {
   case 0: {
     result = test0(a,b,i);
     break;
   }
   case 1: {
     result = test1(a,b,i);
     break;
   }
   case 2: {
     result = a * b;
     break;
   }
  }
  return result;
}

float test(float x, float y, int i) {
  float result;
  result = test2(x,y,i);
  return result;
}

main() {
  float a = 5.0;
  float b = 2.0;
  int i = 1;
  printf("--> %f \n" , test(a,b,i));
}
