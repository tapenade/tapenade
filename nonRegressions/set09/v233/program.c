#include <math.h>

# define MY (90) 		
# define MX (90) 		

void init (double F[MX+2][MY+2][5]) {
  int i, j ;
    for ( i = 0 ; i <= MX+1 ; i++ ) {
      for ( j = 0 ; j <= MY+1 ; j++ ) {
	F[i][j][0] = 1.0 ;
	F[i][j][1] = 1.0 ;
	F[i][j][2] = 0 ;
	F[i][j][3] = 0 ;
      }
    }
  return ;
}

void init_solve_cost ( double F[MX+2][MY+2][5], double *pL ) {
       init (F) ; 
       int i,j;
       int nOuterIter;
       for ( nOuterIter = 1 ; nOuterIter < 600 ;nOuterIter+1){
	    nOuterIter = nOuterIter +1;
	    set_bc (F) ;
            for ( i = 0 ; i <= MX+1 ; i++ ) {
               for ( j = 0 ; j <= MY+1 ; j++ ) {
                F[i][j][4] = sqrt ( F[i][j][0]*F[i][j][0] + F[i][j][1]*F[i][j][1] ) ;
               }
            }
       }
       return ;
}
