struct point{
  float x;
  float y;
  struct point *next;
} ;

void sub1(struct point x, float y, float z);

void top(struct point x, float y, float z, struct point a, float b, float c) {
  if (x.x>0.0) {
    y = 1.7 ;
    sub1(x,y,z) ;
    z = 5.1*z ;
    x.x = y+z ;
  } else
    y = 3.3*x.x*3.3*x.x;
  a.x = -2.9 ;
  sub1(a,b,c) ;
}

void sub1(struct point x, float y, float z) {
  x.x = 3.7*y ;
  if (x.x>0.0) {
    top(x,y,z,x,0.0,0.0);
  }
}
