#include <math.h>  

#define    PI             (4.0 * atan(1.0) )
#define    sqr(x)          ((x)*(x))

typedef double  Ffloat;
typedef int     Fint;

typedef struct {
    Ffloat       rho, u, v, w, p;
    Ffloat       tke, tls;
    Ffloat      x,r,phi;
    Ffloat       vR,vPhi,vM;
    Ffloat       T,a;
} FphysVal;

typedef struct {
    Ffloat      x;
} FgeomVal;

typedef struct {
    FphysVal    phys;
} Fnode;

typedef struct {
    Ffloat      gamma;
    Ffloat      length;
} RefVals;

typedef struct {
    Ffloat     massflow, beta_abs, p_total_abs, p_static;
    int        block_number;
} BdVal;

typedef struct {
    RefVals     ref;
    Ffloat      control;
} Fglobal;

Fglobal  global;
#define  GAMMA        global.ref.gamma

void     calcStageData(BdVal *bd, Fint stage, BdVal *radVal) {
    int         blk = bd->block_number;
    Fnode       av_rel, av_abs;
    int         j,k,l;
    float       localMass, sumMass = 0.0, t_av, globalArea = 0., xLocation = 0.0;
    FgeomVal    Area;
    bd->p_total_abs = 0.;
    for ( l = 0; l < 9; l++)
	for ( k = 0; k < 9; k++){
	    bd->p_static    += (localMass * av_rel.phys.p);
	    bd->massflow    += localMass;
	    globalArea      += Area.x;
	}
    if (j == 1) {
	FphysVal  PhysAbs, PhysRel;
	bd->p_static = PhysAbs.p;
	bd->p_total_abs = bd->p_static * (1. + 0.5*GAMMA*sqr(PhysAbs.w));
	bd->beta_abs    = atan(PhysAbs.w / PhysAbs.u) * 180. / PI;
    }
    else if (k == 0) {
	bd->p_total_abs /= bd->massflow;
    }
}
