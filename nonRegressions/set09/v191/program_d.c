/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 28 Aug 2020 10:13
*/
typedef struct {
    float u;
    int i;
} struct1;
typedef struct {
    float u;
} struct1_d;
typedef struct {
    float x;
    int j;
    struct1 *str1;
} struct2;
typedef struct {
    float x;
    struct1_d *str1;
} struct2_d;
typedef struct {
    struct2 *phys;
} struct3;
typedef struct {
    struct2_d *phys;
} struct3_d;
typedef struct {
    struct3 ref;
    struct2 control;
    int k;
} Fglobal;
typedef struct {
    struct3_d ref;
    struct2_d control;
} Fglobal_d;
Fglobal global;
Fglobal_d globald;

/*
  Differentiation of calc in forward (tangent) mode:
   variations   of useful results: global.control.x
   with respect to varying inputs: *bd.control.x
   RW status of diff variables: global.control.x:out bd:(loc)
                *bd.control.x:in
   Plus diff mem management of: bd:in
*/
void calc_d(Fglobal *bd, Fglobal_d *bdd, int stage) {
    globald = *bdd;
    global = *bd;
}
