/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 28 Aug 2020 10:13
*/
#include <adStack.h>
typedef struct {
    float u;
    int i;
} struct1;
typedef struct {
    float u;
} struct1_b;
typedef struct {
    float x;
    int j;
    struct1 *str1;
} struct2;
typedef struct {
    float x;
    struct1_b *str1;
} struct2_b;
typedef struct {
    struct2 *phys;
} struct3;
typedef struct {
    struct2_b *phys;
} struct3_b;
typedef struct {
    struct3 ref;
    struct2 control;
    int k;
} Fglobal;
typedef struct {
    struct3_b ref;
    struct2_b control;
} Fglobal_b;
Fglobal global;
Fglobal_b globalb;

/*
  Differentiation of calc in reverse (adjoint) mode:
   gradient     of useful results: global.control.x *bd.control.x
   with respect to varying inputs: global.control.x *bd.control.x
   RW status of diff variables: global.control.x:in-zero bd:(loc)
                *bd.control.x:incr
   Plus diff mem management of: bd:in
*/
void calc_b(Fglobal *bd, Fglobal_b *bdb, int stage) {
    bdb->control.x = bdb->control.x + globalb.control.x;
    // + les autres champs ...
    globalb.control.x = 0.0;
}
