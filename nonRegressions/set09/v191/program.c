typedef struct {
    float       u;
    int         i;
} struct1;

typedef struct {
    float      x;
    int        j;
    struct1    *str1;
} struct2;

typedef struct {
    struct2    *phys;
} struct3;

typedef struct {
    struct3     ref;
    struct2     control;
    int         k;
} Fglobal;

Fglobal  global;

void     calc(Fglobal *bd, int stage) {
  global = *bd;
}
