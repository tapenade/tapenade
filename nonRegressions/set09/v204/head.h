#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <float.h>
#include <stdbool.h>

#define NN 4
#define NUM 1
extern const double eps;
#define ERROR
#define DOUBLE
  #define Real double
  #define ZERO 0.0
  #define ONE 1.0
  #define TWO 2.0
  #define THREE 3.0
  #define FOUR 4.0
  #define SMALL DBL_EPSILON

typedef unsigned int uint;
typedef unsigned short int usint;
