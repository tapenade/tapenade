#include "head.h"

const Real eps = 1.0e-2;

const Real mu = 0.005;

FILE *file;

uint get_slow_projector ( Real tim, Real * y, Real * Qs, Real * taum1, Real * Rc );
void radical_correction ( Real tim, Real * y, Real * Rc, Real * g );
void RK4 ( Real t, Real h, Real * y0, Real * Q, Real * y );
void RK2 ( Real t, Real h, Real * y0, Real * Q, Real * y );
void euler ( Real t, Real h, Real * y0, Real * Q, Real * y );
void RKB6 ( Real t, Real h, Real * y0, Real * Q, Real * y );

void intDriver ( Real * tim, Real dt, Real * y_global ) {
    for ( uint tid = 0; tid < NUM; ++tid ) {
        Real t0 = *tim;
        int pflag = 0;
        Real y0_local[NN];
        Real yn_local[NN];
        for ( uint i = 0; i < NN; ++i ) {
        y0_local[i] = y_global[tid + NUM*i];
        }
        while ( *tim < ( t0 + dt ) )
        {
            Real Qs[NN*NN];
            Real Rc[NN*NN];
            Real tau;
            Real * ptau = &tau; // pointer to tau
            uint M = get_slow_projector ( *tim, y0_local, Qs, ptau, Rc );
            Real h = mu * tau;
            fprintf ( file, "%17.10le %d %17.10le %17.10le %17.10le %17.10le\n", *tim, M, y0_local[0], y0_local[1], y0_local[2], y0_local[3]);
            
            RK4 ( *tim, h, y0_local, Qs, yn_local );
            *tim += h;

            Real rc_array[NN];
            radical_correction ( *tim, yn_local, Rc, rc_array );
            for ( uint i = 0; i < NN; ++i ) {
                y0_local[i] = yn_local[i] - rc_array[i];
                if ( y0_local[i] > ONE || y0_local[i] < ZERO ) {
                fprintf ( file, "%17.10le %d %17.10le %17.10le %17.10le %17.10le\n", *tim, M, y0_local[0], y0_local[1], y0_local[2], y0_local[3]);
                }
            }
        }

        for ( uint i = 0; i < NN; ++i ) {
        y_global[tid + NUM*i] = y0_local[i];
        }
        
    }
  
}

int main ( void ) {
  Real t0 = 0.0;
  Real tend = 5.0;
  Real tim;
  Real * ptim = &tim;
  uint size = NUM * sizeof(Real) * NN;
  Real *Y = (Real *) malloc (size);
  tim = t0;
  for ( int i = 0; i < NN*NUM; ++i ) {
    Y[i] = 1.0;
  }
  Real dt = 1.0e-7;
  file = fopen("output.txt","w");
  clock_t t_start = clock();

  while ( tim < tend ) {
    if ( (dt < 1.0e-6) && (tim >= 1.0e-6) ) {
      dt = 1.0e-6;     
    } else if ( (dt < 1.0e-5) && (tim >= 1.0e-5) ) {
      dt = 1.0e-5;
    } else if ( (dt < 1.0e-4) && (tim >= 1.0e-4) ) {
      dt = 1.0e-4;
    } else if ( (dt < 1.0e-3) && (tim >= 1.0e-3) ) {
      dt = 1.0e-3;
    } else if ( (dt < 1.0e-2) && (tim >= 1.0e-2) ) {
      dt = 1.0e-2;
    }
    intDriver ( ptim, dt, Y );
  }
  clock_t t_end = clock();
  
  fclose ( file );
  Real cpu_tim = ( t_end - t_start ) / ( (Real)(CLOCKS_PER_SEC) );
  printf("CPU time: %e (s)\n", cpu_tim);
  
  return 0;
}
