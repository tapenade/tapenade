float sub1(float *x, float y, float *z) {
  *x = 3.7*y ;
  *z = *z + 2*y ;
  return y**z ;
}

void top(float a, float *b) {
  float x,y,z ;
  register float result1;
  if (a>0.0) {
    y = 2.0*a ;
    z = a-7.0 ;
    result1 = sub1(&x,y,&z);
    y = y*result1;
    *b = x*y*z ;
  }
  a = -2.9 ;
}

