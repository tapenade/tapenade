#include <sectionA.h>
#include "sectionB.h"
#include <stdio.h>

float foo(float x, StructA *y, float *z) {
   *z = x*(y->ff1) ;
   return (y->ff1)/x ;
}

float myx ;
float myres ;

int main() {
  glob2.ii1 = 42 ;
  glob2.ff1 = 1.5 ;
  myx = 2.0 ;
  myres = foo(myx, &glob2, &myz) ;
  printf("%f %f\n",myres, myz) ;
}
