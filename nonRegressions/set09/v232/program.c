#include <stdio.h>

typedef struct point{
  float x;
  float y;
  struct point *next;
} Point;


void plus(Point *x, struct point y) {
  x->x = x->x + y.x;
}

void main() {
  Point x,y;
  struct point a,b;
  x.x = 1.0;
   printf("x.x %f \n", x.x);
  y.x = 2.0;
  plus(&x,y);
   printf("x.x %f \n", x.x);
   a.x = 3.0;
   b.x = 4.9;
   printf("a.x %f \n", a.x);
  plus(&a,b);
   printf("a.x %f \n", a.x);
}
