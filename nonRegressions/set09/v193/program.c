/* Inspire de lh21, avec ajout d'une globale d'un type complique */

#include <stdlib.h>

typedef struct {
    float entropy;
} BdVal;

typedef struct {
    BdVal radLeft[300], radRight[300];
    BdVal globalLeft, globalRight;
} BdStageVal;

typedef struct {
    BdStageVal stageVal[50];
    int nStages;
} Fglobal;

Fglobal global;

void suba(float x[4], float y[5]) {
  float *lpp = malloc(4*sizeof(float)) ;
  int i ;
  for (i=0 ; i<4 ; ++i) {
    lpp[i] = x[i]*i*y[i+1] ;
  }
  for (i=0 ; i<4 ; ++i) {
    y[i] = y[i+1]-lpp[i] ;
  }
  free(lpp) ;
}

float* subb1(float y[5], float *z) {
  float *gpp = malloc(10*sizeof(float)) ;
  int i ;
  for (i=0 ; i<10 ; ++i) {
    gpp[i] = i*y[i/2] ;
  }
  gpp[6] = gpp[6]*(*z) ;
  return gpp ;
}

float subb2(float *z, float *gpp) {
  float res = 1.0 ;
  int j ;
  for (j=0 ; j<4 ; ++j) {
    res *= gpp[j]*z[5-j] ;
  }
  for (j=0 ; j<7 ; ++j) {
    z[j] = 0.0 ;
  }
  free(gpp) ;
  return res ;
}

/** Un essai pour la diff adjointe de codes
 * faisant des allocations/deallocations */
void testallocs(float x[4], float y[5]) {
  float *z, *pp ;
  x[2] = x[3]* *(y+1) ;
  z = malloc(7*sizeof(float)) ;
  x[2] = x[2] * global.stageVal[2].globalRight.entropy;
  int i ;
  for (i=0 ; i<7 ; ++i) {
    z[i] = x[2]*i ;
  }

  suba(x,y) ;

  int j ;
  for (j=1 ; j<3 ; ++j) {
    x[j] = x[j-1]*z[j] ;
  }

  /* $AD NOCHECKPOINT */
  pp = subb1(y,z) ;

  x[3] = pp[3]*pp[2] ;
  x[0] = x[0]*x[3] ;

  x[2] = subb2(z,pp) ;
}
