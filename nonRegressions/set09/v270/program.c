/* Bug trouve par Sietse Jongsma S.H.Jongsma@ctw.utwente.nl en mai 2011.
 * Le push/pop du tableau "left" dans le snapshot avant l'appel de PrimitiveVariables()
 *  etait mal fait (push/pop du 1er element seulement).
 * Note: j'ai modifie le code pour conserver le snapshot si jamais le data-flow
 *  devenait plus malin et trouvait ce push/pop inutile. 
 * From lh20, pour tester "long double"
*/
 

#include <stdio.h>
#include <stdlib.h>
#include <math.h> 

#define nv 5
  
void PrimitiveVariables(long double wwL[nv], long double left[nv]) {

  const long double tmp = 1.0/wwL[0];
  int k;

  // Changed assignments to "left" into increments, to make sure the 
  // push/pop of array "left" is necessary before calling PrimitiveVariables()

  // The density is a primitive variable itself
  left[0] += wwL[0];

  // Compute velocity components from rho*vel
  for (k = 1; k < 4; k++) left[k] += tmp*wwL[k];

  // Compute pressure
  left[4] += 0.4*(wwL[4] - 0.5*left[0]*(left[1]*left[1] + left[2]*left[2] + left[3]*left[3]));
 
  // Return
  return;
}

void test(long double wwL[nv], long double F[nv]) {

  // Initialise left
  int k; long double  left[5]; for (k = 0; k < 5; k++)  left[k] = 0.0;

  // Call function to compute primitive variables
  PrimitiveVariables(wwL,left); 

  long double HAvg = ((wwL[4] + left[4])/sqrt(left[0])); 
  long double al  = sqrt(left[4] /left[0]);
  long double eta = 0.5*((left[1] + (left[2]) + (left[3])) + al);
  long double unL  = left[1]  + left[2]  + left[3];
  long double runL = left[0] *unL;

  // Provide output
  F[0] = 0.5*(runL - (wwL[0]));
  F[1] = 0.5*(runL*left[1] - wwL[1]);
  F[2] = 0.5*(runL*left[2] - wwL[2]);
  F[3] = 0.5*(runL*left[3] - wwL[3]);
  F[4] = 0.5*(unL*(wwL[4] + left[4]) - (wwL[4] + HAvg));

  // Return                     
  return;
} 

