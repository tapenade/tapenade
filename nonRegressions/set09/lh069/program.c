/** Another test about the adjoint parameter-passing policy */
#include <stdio.h>

double bar(double x, double* y, double *z, double **tt) {
  double res3 ;
  res3 = *y * x + *z * **tt ;
  *z = 2 * *z ;
  *y = *y * *y ;
  **tt = 2.0 * **tt ;
  return res3 ;
}

double foo(double x, double* y, double z, double *tt) {
  double res2 ;
  *y = *y * x ;
  res2 = bar(x,y,&z,&tt) ;
  return res2 ;
}

int main(int argc, char** argv) {
  double x,y,z,t,res1 ;
  x = 3.5 ;
  y = 2.5 ;
  z = 1.5 ;
  t = 0.5 ;
  res1 = foo(x,&y,z,&t) ;
  printf("result: %f\n", res1);
  return 0;
}
