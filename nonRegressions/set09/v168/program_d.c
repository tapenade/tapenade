/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
// avant
#include <stdio.h>
const double C = 0.1;
// apres1
#include "include_d.h"
const double K = a;
// apres2
const double L = 1.0;

/*
  Differentiation of SolveHB in forward (tangent) mode:
   variations   of useful results: *x
   with respect to varying inputs: a *x
   RW status of diff variables: a:in x:(loc) *x:in-out
   Plus diff mem management of: x:in
*/
// apres3
void SolveHB_d(double *x, double *xd) {
    *xd = a*(*xd) + (*x)*ad;
    *x = (*x)*a;
}
