typedef struct point{
  float x;
  float y;
} Point;

void sub1(Point x, float y, float z) {
  x.x = 3.7*y ;
}

void top(Point x, float *y, float *z, Point a, float b, float c) {
  if (x.x>0.0) {
    *y = 1.7 ;
    sub1(x,*y,*z) ;
    *z = 5.1**z ;
    x.x = *y+*z ;
  } else
    *y = 3.3*x.x*3.3*x.x;
  a.x = -2.9 ;
  sub1(a,b,c) ;
}

