/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
/*
  Differentiation of ffcn in forward (tangent) mode:
   variations   of useful results: *y
   with respect to varying inputs: *x *y
   RW status of diff variables: x:(loc) *x:in y:(loc) *y:in-out
   Plus diff mem management of: x:in y:in
*/
static void ffcn_d(double *x, double *xd, double *y, double *yd) {
    yd[0] = -xd[0];
    y[0] = -x[0];
    yd[1] = xd[0] - xd[1];
    y[1] = x[0] - +x[1];
}
