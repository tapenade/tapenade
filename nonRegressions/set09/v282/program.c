#include <stdio.h>
#include <stdlib.h>

void bar(float *a, float *b, float *v, int n);

void result_(int *n, float *a, float *b, float *v);


void foo(float *x, float *y) {
  float *v = (void *)0;
  v = calloc(6, 6*sizeof(float));
  bar(x, y, v, 3);
  v = realloc(v, 8*sizeof(float));
  bar(x, y, v, 4);
}

void bar(float *a, float *b, float *v, int n) {
  printf("bar n %d\n", n);
  result_(&n, a, b, v);
}


void main() {
  if ( 0 == 0) {
    float x, y;
    x = 2.0;
    y = 3.0;
    printf("x %f\n", x);
    foo(&x, &y);
    printf("x %f y %f\n", x, y);
  }
}


void result_(int *n, float *a, float *b, float *v) {
  int i, j, i1;
    for (i = 1; i <= 2; ++i) {
	i1 = *n;
	for (j = 1; j <= i1; ++j) {
	    v[i + (j << 1)] = *a * *b * (i + j);
	    *b += v[i + (j << 1)];
	}
    }
}

