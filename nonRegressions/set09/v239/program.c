# include <stdio.h>
# include <stdlib.h>
#include "ampi/ampi.h"

void msg1(float* val1, float* val2, int numprocprec, int numprocsuiv,
          int etiquette) {
  AMPI_Request reqA, reqB;
  MPI_Status statut ;
  AMPI_Isend ( val1, 1, MPI_REAL, numprocsuiv, etiquette,
               AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD , &reqA);
  AMPI_Irecv ( val2, 1, MPI_REAL, numprocprec, etiquette,
               AMPI_FROM_ISEND_WAIT, MPI_COMM_WORLD, &reqB);
  AMPI_Wait(&reqA, &statut);
  AMPI_Wait(&reqB, &statut);
}

void msg2(float* val1, float* val2, int numprocprec, int numprocsuiv,
          int etiquette) {
  MPI_Status statut ;
  AMPI_Recv ( val2, 1, MPI_REAL, numprocprec, etiquette,
              AMPI_FROM_SEND, MPI_COMM_WORLD, &statut );
  AMPI_Send ( val1, 1, MPI_REAL, numprocsuiv, etiquette,
              AMPI_TO_RECV, MPI_COMM_WORLD );
}



int main ( int argc, char *argv[] ) {

  int        nb_processus,rang,num_proc_precedent,num_proc_suivant ;
  float val, valeur;
  const int  etiquette=100 ;

  int master = 0;
  int my_id;

  AMPI_Init_NT(&argc,&argv) ;
  AMPI_Comm_rank(MPI_COMM_WORLD,&rang) ;
  AMPI_Comm_size(MPI_COMM_WORLD,&nb_processus) ;

  num_proc_precedent = (nb_processus+rang-1) % nb_processus ;
  num_proc_suivant = (rang+1) % nb_processus ;

  val = rang + 1000 ;

  if ( rang == master )  {
    msg1(&val, &valeur, num_proc_precedent, num_proc_suivant, etiquette);
  } else {
    msg2(&val, &valeur, num_proc_precedent, num_proc_suivant, etiquette);
  }

  printf ( "Process %d: val %f, valeur %f\n", rang, val, valeur);

  AMPI_Finalize_NT( );
     return 0;
}
