void f(float y[]) {
  y[1] = 3 * y[1];
}

void test(float *y) {
  float x;
  x = 12;
  int n;
  n = 7;
  float t[n];
  t[1] = *y;
  f(t + 2);
  *y = t[1];
}
