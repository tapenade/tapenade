#include <stdio.h>

typedef char    Fmsg[256];

void     calcStageData(float *bd, char *fileName, int stage, char *mode, float *radVal)
{
    Fmsg        msg;
    int         blk = 0;
    float       av_rel, av_abs;
    int         j, k, l, index, indexKp1, indexLp1, indexKp1Lp1;
    float       localMass, sumMass = 0.0, t_av, globalArea = 0., xLocation = 0.0;
    float    Area;
    float    FluxAbs, FluxRel;

    int         endPtK      =  - 1;
    int         endPtL      =  - 1;

    for ( l = 0; l < endPtL; l++)
    {
	FILE        *fp;
	int        indexHub = 0;
	int        indexTip = 0;
        float      xHub      = 0;
        float      xTip      = 0;
	float      radiusHub = 0;
	float      radiusTip = 0;

	for ( l = 0; l < endPtL; l++){ 
          int   indexRadius;
          int   indexRadiusLp1 = (l+1);
 
	    float tanPressure, tanTemperature, tanDensity, tanMachAbs, tanMachRel, tanPressureA = 0, AreaA;
	    float tanTTAbs, tanTTRel, tanPTRel, tanPTAbs, tanTTRot, globalArea, rot_energy;
	    float tanAlphaRel, tanAlphaAbs, tanBetaRel, tanBetaAbs, radius, tanMass = 0.0, tanEddy = 0.0;
	    float eta_is, radiusReal;
	    float tanV_rel, tanV_rel_tan, tanV_rel_mer, tanV_rel_rad, tanV_rel_ax;
	    float tanV_abs, tanV_abs_tan, tanV_abs_mer, tanV_abs_rad, tanV_abs_ax;
	    float  PhysAbs, PhysRel;

            indexRadius = l;

	    initFlux(&FluxAbs);
	    initFlux(&FluxRel);
	    

	}
    }
}

