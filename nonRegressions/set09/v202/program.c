/** From servlet 20120119 */
 
#include "include.h"

void dydt ( Real t, Real * y, Real * Q, int qflag, Real * dy );

void euler ( Real t, Real h, Real * y0, Real * Q, Real * y ) {
  
  Real k[NN];
  
  dydt ( t, y0, Q, 1, k );
  
  for ( usint i = 0; i < NN; ++i ) {
    y[i] = y0[i] + h * k[i];
  }
      
}
