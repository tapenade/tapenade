#include<stdio.h>


void foo(float x, float *y) {
  *y = x * x;
  x = 0.0;
}

float top(float z) {
  foo(z,z);
  return z;
}

main () {
  float a = 1.0;
  printf("--> %f\n" , a);
  printf("--> %f\n" , top(a));
}
