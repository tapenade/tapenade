/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
#include "DIFFSIZES.inc"
/*  Hint: NBDirsMax should be the maximum number of differentiation directions
*/

/*
  Differentiation of mul in forward (tangent) mode (with options multiDirectional):
   variations   of useful results: *mul
   with respect to varying inputs: a b
   Plus diff mem management of: mul:in
*/
void mul_dv(float a, float ad[NBDirsMax], float b, float bd[NBDirsMax], float 
        *mul, float (*muld)[NBDirsMax], int nbdirs) {
    int nd;
    for (nd = 0; nd < nbdirs; ++nd)
        (*muld)[nd] = b*ad[nd] + a*bd[nd];
    *mul = a*b;
}

/*
  Differentiation of div in forward (tangent) mode (with options multiDirectional):
   variations   of useful results: *result
   with respect to varying inputs: a b
   Plus diff mem management of: result:in
*/
void div_dv(float a, float ad[NBDirsMax], float b, float bd[NBDirsMax], float 
        *result, float (*resultd)[NBDirsMax], int nbdirs) {
    int nd;
    for (nd = 0; nd < nbdirs; ++nd)
        (*resultd)[nd] = (ad[nd]-a*bd[nd]/b)/b;
    *result = a/b;
}

/*
  Differentiation of calc in forward (tangent) mode (with options multiDirectional):
   variations   of useful results: *result
   with respect to varying inputs: *result a b
   RW status of diff variables: result:(loc) *result:in-out a:in
                b:in
   Plus diff mem management of: result:in
*/
void calc_dv(float a, float ad[NBDirsMax], float b, float bd[NBDirsMax], float
        *result, float (*resultd)[NBDirsMax], int nbdirs) {
    //  a < b ? mul(a,mul(b,2,&result), &result) : div(a,div(b,2,result), result);
    float r;
    float rd[NBDirsMax];
    float r1;
    float r1d[NBDirsMax];
    float dummyzerodiffd[NBDirsMax];
    int nd;
    float dummyzerodiffd0[NBDirsMax];
    if (a < b) {
        for (nd = 0; nd < NBDirsMax; ++nd)
            dummyzerodiffd[nd] = 0.0;
        mul_dv(b, bd, 2, dummyzerodiffd, &r, &rd, nbdirs);
        mul_dv(a, ad, r, rd, &r1, &r1d, nbdirs);
    } else {
        for (nd = 0; nd < NBDirsMax; ++nd)
            dummyzerodiffd0[nd] = 0.0;
        div_dv(b, bd, 2, dummyzerodiffd0, &r, &rd, nbdirs);
        div_dv(a, ad, r, rd, &r1, &r1d, nbdirs);
    }
    for (nd = 0; nd < nbdirs; ++nd)
        (*resultd)[nd] = r1d[nd];
    *result = r1;
}
