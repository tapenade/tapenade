/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.11 (r6034M) - 25 Apr 2016 18:14
*/
void mul(float a, float b, float *mul) {
    *mul = a*b;
}

void div(float a, float b, float *result) {
    *result = a/b;
}

void calc(float a, float b, float *result) {
    //  a < b ? mul(a,mul(b,2,&result), &result) : div(a,div(b,2,result), result);
    float r;
    float r1;
    if (a < b) {
        mul(b, 2, &r);
        mul(a, r, &r1);
    } else {
        div(b, 2, &r);
        div(a, r, &r1);
    }
    *result = r1;
}

void main() {
    float i = 2;
    float j = 3;
    float result;
    printf("i %f ...", i);
    printf(" j %f ...\n", j);
    calc(i, j, &result);
    printf("%f \n", result);
}
