#define  maxPrimeFactor        37
#define  maxPrimeFactorDiv2    (maxPrimeFactor+1)/2
#define  maxFactorCount        20

static double  c3_1 = -1.5000000000000E+00;
static double  c3_2 =  8.6602540378444E-01;

static int      groupNo,dataNo,blockNo,twNo;
static double   twiddleRe[maxPrimeFactor], twiddleIm[maxPrimeFactor],
                trigRe[maxPrimeFactor], trigIm[maxPrimeFactor],
                zRe[maxPrimeFactor], zIm[maxPrimeFactor];
static double   vRe[maxPrimeFactorDiv2], vIm[maxPrimeFactorDiv2];
static double   wRe[maxPrimeFactorDiv2], wIm[maxPrimeFactorDiv2];

void twiddleTransf(int sofarRadix, int radix, int remainRadix,
                    double yRe[], double yIm[])

{   /* twiddleTransf */ 
    double  t1_re,t1_im;
    double  m2_re,m2_im;
    double  m1_re,m1_im;
    double  s1_re,s1_im;

    for (dataNo=0; dataNo<sofarRadix; dataNo++)
    {
        for (groupNo=0; groupNo<remainRadix; groupNo++)
        {
                         t1_re=zRe[1] + zRe[2]; t1_im=zIm[1] + zIm[2];
                         zRe[0]=zRe[0] + t1_re; zIm[0]=zIm[0] + t1_im;
                         m1_re=c3_1*t1_re; m1_im=c3_1*t1_im;
                         m2_re=c3_2*(zIm[1] - zIm[2]); 
                         m2_im=c3_2*(zRe[2] -  zRe[1]);
                         s1_re=zRe[0] + m1_re; s1_im=zIm[0] + m1_im;
                         zRe[1]=s1_re + m2_re; zIm[1]=s1_im + m2_im;
                         zRe[2]=s1_re - m2_re; zIm[2]=s1_im - m2_im;
        }
    }
}   /* twiddleTransf */
