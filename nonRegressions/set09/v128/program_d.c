/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
#include <stdio.h>
extern FILE *fich, *f1;
FILE *f2, *f1;

/*
  Differentiation of test in forward (tangent) mode:
   variations   of useful results: *a
   with respect to varying inputs: *a
   Plus diff mem management of: a:in
*/
void test_d(float *a, float *ad, FILE *f) {
    printf(" ... ", *f);
    *ad = 2*(*ad);
    *a = 2*(*a);
    *ad = f->_flags*(*ad);
    *a = (*a)*f->_flags;
}

/*
  Differentiation of test1 in forward (tangent) mode:
   variations   of useful results: *b
   with respect to varying inputs: *b
   RW status of diff variables: b:(loc) *b:in-out
   Plus diff mem management of: b:in
*/
void test1_d(float *b, float *bd) {
    test_d(b, bd, fich);
}
