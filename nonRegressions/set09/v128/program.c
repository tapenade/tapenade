#include <stdio.h>
extern FILE *fich, *f1;
FILE *f2, *f1;

void test(float *a, FILE *f) {
  printf(" ... ", *f);
  *a = 2 * *a;
  *a = *a * (*f)._flags;
}

void test1(float *b) {
  test(b, fich);
}
