#include <stdio.h>

static int nbCall = 0;

void testNbCall() {
  nbCall = nbCall + 1;
  printf("testNbCall program.c %d \n", nbCall);
}

void main() {
  float *x=NULL;
  float *y=NULL;
  testNbCall(),  testNbCall();
  free(x), free(y);
}
