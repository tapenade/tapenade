/* from nonRegrF90 lh64 */
int scalar_normal_flux(float *q, float *qb, float *nxy, float *resq)
{
     int iface_id[99];
     float flux;
     int i, j;
     float w1[99], qf, sf[99];
     int id1, id2;
     float fw1, fw2;
     int idf, facecell_id[99], bdryface_id[99];


    --resq;
    --nxy;
    --qb;
    --q;


    //$AD II-LOOP
    for (i = 1; i <= 51; ++i) {
	idf = iface_id[i - 1];
	j = idf << 1;
	id1 = facecell_id[j - 2];
	id2 = facecell_id[j - 1];
	fw1 = w1[idf - 1];
	fw2 = (float)1. - fw1;
	qf = fw1 * q[id1] + fw2 * q[id2];
	flux = qf * nxy[idf] * sf[idf - 1];
	resq[id1] -= flux;
	resq[id2] += flux;
    }

    //$AD II-LOOP
    for (i = 1; i <= 52; ++i) {
	idf = bdryface_id[i - 1];
	id1 = facecell_id[(idf << 1) - 2];
	flux = qb[i] * nxy[idf] * sf[idf - 1];
	resq[id1] -= flux;
    }
}

