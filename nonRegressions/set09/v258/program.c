#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>

#define ITG int
#define ITGFORMAT "d"
#define     MAX_LINE_LENGTH 256
#define     MAX_INTEGER 2147483647
#define     MAX_FLOAT   1.e32

typedef struct {
  char  **pheader;    /* project header */
  ITG   npheader;              /* number of headers */
  char  **compName;
  char  name[MAX_LINE_LENGTH];
  fpos_t *fileptr;
  ITG   loaded;       /* if data are stored:1 else: 0 */
  ITG ncomps;
  float **dat;        /* node related data */
  float ***edat;      /* element related data */
} Datasets;


void freeDatasets(Datasets *lcase, ITG nr)
{
  register ITG i;

  printf(" free lc[%" ITGFORMAT "] ncomps:%" ITGFORMAT "\n",nr,lcase[nr].ncomps);
  if(lcase[nr].loaded)
  {
    for(i=0; i<lcase[nr].ncomps; i++) SFREE(lcase[nr].dat[i]);
  }
  if(lcase[nr].npheader)
  {
    for(i=0; i<lcase[nr].npheader; i++) SFREE(lcase[nr].pheader[i]);
    SFREE(lcase[nr].pheader);
  }
  for(i=0; i<lcase[nr].ncomps; i++)
  {
    SFREE(lcase[nr].compName[i]);
  }
  SFREE(lcase[nr].compName);
  SFREE(lcase[nr].dat);
  SFREE(lcase[nr].fileptr);
}

