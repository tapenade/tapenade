# include <stdio.h>
# include <stdlib.h>

# include "mpi.h"

int main ( int argc, char *argv[] ) {

  int        nb_processus,rang,num_proc_precedent,num_proc_suivant ;
  float val, valeur;
  float vald, valeurd;
  const int  etiquette=100 ;

  int master = 0;
  int my_id;

  MPI_Init(&argc,&argv) ;
  MPI_Comm_rank(MPI_COMM_WORLD,&rang) ;
  MPI_Comm_size(MPI_COMM_WORLD,&nb_processus) ;

  num_proc_precedent = (nb_processus+rang-1) % nb_processus ;
  num_proc_suivant = (rang+1) % nb_processus ;

  val = rang + 1000 ;
  vald = rang + 2000 ;

  if ( rang == master )  {
    msg1_d(&val, &vald, &valeur, &valeurd, num_proc_precedent, num_proc_suivant, etiquette);
  } else {
    msg2_d(&val, &vald, &valeur, &valeurd, num_proc_precedent, num_proc_suivant, etiquette);
  }

  printf ( "Process %d: val %f, valeur %f\n", rang, val, valeur);
  printf ( "Process %d: vald %f, valeurd %f\n", rang, vald, valeurd);

  MPI_Finalize ( );
     return 0;
}
