#include <string.h>
#include <stdio.h>

void foo(char *str) {
    while(*str != '\0') {
        printf("%s %c \n", str, *str);
        ++str;
    }
}

main() {
    char *inputStr;
    inputStr = "Hello World!";
    printf("length %i \n", strlen(inputStr));
    char *curChar;
    curChar = inputStr;
    foo(inputStr);
    char arrayStr[3];
    arrayStr[0] = '0';
    arrayStr[1] = '1';
    arrayStr[2] = '\0';
    foo(arrayStr);
}
