
#define WRITEDEBUG
#define OPENMP

#include "UpdateP.h"

double UpdateP(double gamma, double W1, double W2, double W3, double W4) 
{ 
  return (gamma-1.0)*(W4-(pow(W2,2)+pow(W3,2))/(2.0*W1)); 
} 


#define WRITEDEBUG
#define OPENMP

#include "UpdateC.h"
 
double UpdateC(double gamma, double P, double W1) 
{ 
  return sqrt(gamma*P/W1); 
} 



#define WRITEDEBUG
#define OPENMP

#include "UpdateM.h"


double UpdateM(double gamma, double W1, double W2, double W3, double C)
{
    	return sqrt(pow(W2,2)+pow(W3,2))/(W1*C);
}
