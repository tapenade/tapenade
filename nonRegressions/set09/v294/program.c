struct _IO_FILE;
typedef struct _IO_FILE __FILE;

struct _IO_FILE;

typedef struct _IO_FILE FILE;

struct _IO_FILE {
  int _flags;
  struct _IO_FILE *_chain;
};

typedef struct _IO_FILE _IO_FILE;

extern FILE *fich, *f1;
FILE *f2, *f1;

void test(float *a, FILE *f) {
  *a = 2 * *a;
  *a = *a * (*f)._flags;
}

void test1(float *b) {
  test(b, fich);
}
