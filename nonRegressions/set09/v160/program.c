/* pour tester un bug avec les return multiple en mode inverse */
double Max( double a, double b)
{
  if (a > b)
    return a;
  else
    return b;
}
