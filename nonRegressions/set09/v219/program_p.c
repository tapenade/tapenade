/*        Generated by TAPENADE     (INRIA, Tropics team)
    Tapenade 3.10 (r5398M) -  3 Oct 2014 10:33
*/
#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

void msg1(float *val1, float *val2, int numprocprec, int numprocsuiv, int 
        etiquette) {
    //     req points to Undef
    MPI_Request req;
    MPI_Status statut;
    MPI_Isend(val1, 1, (MPI_Datatype)(void *)&ompi_mpi_real, numprocsuiv, 
              etiquette, (MPI_Comm)(void *)&ompi_mpi_comm_world, &req);
    MPI_Irecv(val2, 1, (MPI_Datatype)(void *)&ompi_mpi_real, numprocprec, 
              etiquette, (MPI_Comm)(void *)&ompi_mpi_comm_world, &req);
    MPI_Wait(&req, &statut);
    MPI_Wait(&req, &statut);
}

void msg2(float *val1, float *val2, int numprocprec, int numprocsuiv, int 
        etiquette) {
    MPI_Status statut;
    MPI_Recv(val2, 1, (MPI_Datatype)(void *)&ompi_mpi_real, numprocprec, 
             etiquette, (MPI_Comm)(void *)&ompi_mpi_comm_world, &statut);
    MPI_Send(val1, 1, (MPI_Datatype)(void *)&ompi_mpi_real, numprocsuiv, 
             etiquette, (MPI_Comm)(void *)&ompi_mpi_comm_world);
}

int main(int argc, char *argv[]) {
    int nb_processus, rang, num_proc_precedent, num_proc_suivant;
    float val, valeur;
    const int etiquette = 100;
    int master = 0;
    int my_id;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank((MPI_Comm)(void *)&ompi_mpi_comm_world, &rang);
    MPI_Comm_size((MPI_Comm)(void *)&ompi_mpi_comm_world, &nb_processus);
    num_proc_precedent = (nb_processus+rang-1)%nb_processus;
    num_proc_suivant = (rang+1)%nb_processus;
    val = rang + 1000;
    if (rang == master)
        msg1(&val, &valeur, num_proc_precedent, num_proc_suivant, etiquette);
    else
        msg2(&val, &valeur, num_proc_precedent, num_proc_suivant, etiquette);
    printf("Process %d: val %f, valeur %f\n", rang, val, valeur);
    MPI_Finalize();
    return 0;
}
