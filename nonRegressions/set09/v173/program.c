
void mul (float a, float b, float *mul, float *foo) {
  *mul = a * b;
}

void div (float a, float b, float *result) {
  *result = a / b;
}

void calc (float a, float b, float *result, float *foo){
  float r;
  float r1;
  float f;
  if (a<b) {
    mul(b,2,&r, &f);
    mul(a, r, &r1, &f);
  } else {
    div(b,2,&r);
    div(a,r, &r1);
  }
  *result = r1;
  *foo = f;
}

main()
{
  float i=2;
  float j=3;
  float f;
  float result;
  printf("i %f ...", i);
  printf(" j %f ...\n", j);
  calc(i,j,&result,&f);
  printf("%f \n", result);
}

