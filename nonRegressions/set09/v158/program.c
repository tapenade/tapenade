void test(int *p, int z[]) {
  z[0] = *p;
  z[1] = *p / z[2];
}

void fft(int n, double *x) {
  *x = *x * *x * n;
  test(&n);
}
