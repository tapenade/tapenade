/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_llhTests) - 14 Jun 2021 18:36
*/
#include "DIFFSIZES.inc"
/*  Hint: NBDirsMax should be the maximum number of differentiation directions
*/

void test_nodiff(int *p, int z[]) {
    z[0] = *p;
    z[1] = *p/z[2];
}

/*
  Differentiation of fft in forward (tangent) mode (with options multiDirectional):
   variations   of useful results: *x
   with respect to varying inputs: *x
   RW status of diff variables: x:(loc) *x:in-out
   Plus diff mem management of: x:in
*/
void fft_dv(int n, double *x, double (*xd)[NBDirsMax], int nbdirs) {
    int nd;
    for (nd = 0; nd < nbdirs; ++nd)
        (*xd)[nd] = n*2*(*x)*((*xd)[nd]);
    *x = (*x)*(*x)*n;
    test_nodiff(&n);
}
