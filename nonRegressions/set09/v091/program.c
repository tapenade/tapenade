void defaultShowDebito1(double debito) {
	printf("Q = %.16f kg/hr", debito);
}

void (*showDebito1)(double) = defaultShowDebito1;

void defaultShowDebito2(double debito) {
	printf("Q = %.16f kg/hr", debito);
}

void (*showDebito2)(double) = defaultShowDebito2;
