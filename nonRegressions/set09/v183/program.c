static inline double max(const double x, const double y)
{
	return x > y ? x : y;
}
