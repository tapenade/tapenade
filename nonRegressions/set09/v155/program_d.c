/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
/*
  Differentiation of expandintest in forward (tangent) mode:
   variations   of useful results: a[0:100-1]
   with respect to varying inputs: a[0:100-1] b[0:100-1]
   RW status of diff variables: a:(loc) a[0:100-1]:in-out b:(loc)
                b[0:100-1]:in
   Plus diff mem management of: a:in b:in

 from nonRegrF77 lh99 
   pour ne pas mettre de continue 
   qui ne compile pas */
void expandintest_d(float a[100], float ad[100], float b[100], float bd[100]) 
{
    int i;
    float abs0;
    /* check that the expansion of abs does not break the loop !! */
    i = 5;
    while(1) {
        if (a[i] >= 0.)
            abs0 = a[i];
        else
            abs0 = -a[i];
        if (abs0 > 10) {
            ad[i] = 2*bd[i];
            a[i] = b[i]*2;
        } else
            break;
    }
}
