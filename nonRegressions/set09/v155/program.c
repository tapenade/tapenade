/* from nonRegrF77 lh99 
   pour ne pas mettre de continue 
   qui ne compile pas */
void expandintest(float a[100], float b[100]) {
    int i;

/* check that the expansion of abs does not break the loop !! */

    i = 5;
    while (abs(a[i]) > 10) {
	a[i] = b[i] * 2;
    }
}

