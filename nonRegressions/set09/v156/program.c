#include <stdio.h>
#include <math.h>

extern short stop;

extern int erro_c;

extern double extrusao(double debito);

int zbrac(double *x1, double *f1, double *x2, double *f2,double debmax)
{
	int j,j1=0;
	double FACTOR=1.6;

	*f2=extrusao(*x2);
	for (j=1;j<=20;j++) {
		if (stop == 1) {
			return 0;
		}
		if (*f1* *f2 < 0.0) return 1;
		if (fabs(*f1) < fabs(*f2)) {
			if (*x1<=1.0e-15) {
				printf("Output = 0; Solution not valid in zbrak.");
				return 0;
			}
			*f1=extrusao(*x1);
			if (erro_c>0) {

				if (*x1<=1.0e-15) {
					printf("Output = 0; Solution not valid in zbrak.");
					return 0;
				}
				*f1=extrusao(*x1);
			}
		}
		else {
			if (*x2<=1.0e-15) {
				printf("Output = 0; Solution not valid in zbrak.");
				return 0;
			}
			*f2=extrusao(*x2);
			if (erro_c>0) {
				j1=1;
				*x2=*x2/2.0;
				if (*x2<=1.0e-15) {
					printf("Output = 0; Solution not valid in zbrak.");
					return 0;
				}
				*f2=extrusao(*x2);
			}
		}
	}
	return 0;
}
