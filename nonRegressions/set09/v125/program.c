typedef struct _IO_FILE FILE;
struct _IO_FILE;
struct _IO_FILE {
    int _flags;
    int _fileno;
};

extern FILE *fich, *f1;
FILE *f2, *f1;
FILE *fich;

extern int printf(char*, ...);

void test(float *a, FILE *f) {
  printf(" ... ", *f);
  *a = 2 * *a;
  *a = *a * (*f)._flags;
}

void test1(float *b) {
  test(b, fich);
}
