/* from Johannes Willkomm */

void f(double const * v, double * const w) {
  /* v can be changed, but data a *v is readonly
     w can not be changed, but data at *w can be written to */
  *w = *v;
  ++v;
  *w = *v;
}
