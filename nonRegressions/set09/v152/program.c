enum { z, a = 4, b};

float test(float x) {
  float num, den;
  num  = a * x + b * pow(x, 2.0);
  den = b * x;
  return 2.0 * num / den;
}
