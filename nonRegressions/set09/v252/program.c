#include <stdio.h>
#include <math.h>
#include "eventdata.h"
#define LAST 10

double costFunSimple(double x[15], double eventdata[][9])
{
	double obj = 0;
	double p1 = x[0];
	double p2 = x[1];
	double a = x[2];
	double e = x[3];
	double lambda00 = x[4];
	double lambda10 = x[5];
	double lambda01 = x[6];
	double lambda11 = x[7];
	double lambda21 = x[8];
	double lambda12 = x[9];
	double lambda22 = x[10];
	double lambda20 = x[11];
	double lambda02 = x[12];
	double lambda20p = x[13];
	double lambda02p = x[14];

	double b = -(a * p1 - 2 * a + 2 * a * p2*p2 - 2 * e * p2*p2 + 4) / (4 * p1 - 4);
	double f = b - (a - e) * (p2 - 1);
	double c = (a * p1 + b) / p1;
	double g = (a*(0.5 + p1) + b) / p1;
	double h = -0.5 * g;
	int numRanges = 5;
	double rangeStart[5] = { 0, p1, 0.5, 0.5 + p1, 1 - p2 };
	double rangeStop[5] = { p1, 0.5, 0.5 + p1, 1 - p2, 1 };
	double	avec[5] = { c, a, g, a, e };
	double bvec[5] = { 0, b, h, b, f };

	double lambdaxyh;
	double lambdaxya;
	double lambdak;

	for (size_t i = 0; i < numRows; i++)
	{
		double t0 = eventdata[i][0];
		double t1 = eventdata[i][1];
		double isHome = eventdata[i][2];
		double isAway = eventdata[i][3];
		double lambdaH = eventdata[i][4];
		double lambdaA = eventdata[i][5];
		double home = eventdata[i][6];
		double away = eventdata[i][7];
                lambdaxyh = lambda00;
	        lambdaxya = lambda00;
		lambdak = c*t1;
		double A = 0;
		for (size_t i = 0; i < numRanges; i++)
		{
			double stop = rangeStop[i];
			double start = rangeStart[i];
			double localt0 = max(t0, start);
			double localt1 = min(t1, stop);
			double funt0 = avec[i] * localt0 + bvec[i];
			A = A + (funt0) / 2 * (localt1 - localt0);
		}
		double logScoreLambdak = 0;
		logScoreLambdak = log(lambdaxyh * lambdak * lambdaH);
		double areaHome = A * lambdaH * lambdaxyh;
		double areaAway = A * lambdaA * lambdaxya;
		obj = obj - areaHome - areaAway + logScoreLambdak;
	}
	return -obj / 9613;
}
