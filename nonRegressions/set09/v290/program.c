// from CalculiX, Oluwadamilare Rahman Imam-Lawal <o.r.imam-lawal@qmul.ac.uk>

#include <stdio.h>
#include <stdlib.h>
#define MAX_DVAR 2
#define NNEW(a,b,c) a=(b *)u_calloc((c),sizeof(b),__FILE__,__LINE__,#a)
//#define NNEW(a,b,c) a=calloc((c),sizeof(b));


int log_realloc=-1;
void *u_calloc(size_t num,size_t size,const char *file,const int line, const char* ptr_name){

    /* allocating num elements of size bytes and initializing them to zero */

  void *a;
  char *env;

  if(num==0){
    a=NULL;
    return(a);
  }
      
  a=calloc(num,size);
  if(a==NULL){
    printf("*ERROR in u_calloc: error allocating memory\n");
    printf("variable=%s, file=%s, line=%d, num=%ld, size=%ld\n",ptr_name,file,line,num,size);
    exit(16);
  }
  else {
    if(log_realloc==-1) {
      log_realloc=0;
      env=getenv("CCX_LOG_ALLOC");
      if(env) {log_realloc=atoi(env);}
    }      
    if(log_realloc==1) {
	printf("ALLOCATION of variable %s, file %s, line=%d, num=%ld, size=%ld, address= %ld\n",ptr_name,file,line,num,size,(long int)a);
    }      
    return(a);
  }
}

void get_j (double *in,double *input2,double *out) {

  double input[MAX_DVAR] ; 
  int i;
  for(i=0;i<3;i++){
      input[i] = in[i]*in[i];
      input2[i]= in[i]*in[i]; 
      *out += input[i] + input2[i] ;    
  }

}

void call_solver ( int *nnodes, double *in, double *out ) {
  double *input2;  
  NNEW(input2,double,*nnodes);  
 
  get_j ( in,input2,out ); 
  
  
}

void main () {
  double j;
  double *a;
  int nnodes;
  nnodes = 3;  
  NNEW(a,double,MAX_DVAR);
  
  a[0] = 1.0 ;
  a[1] = 2.0 ;
  a[2] = 3.0 ;
  a[3] = 4.0 ;

   call_solver(&nnodes, a, &j);
  
  printf("%f \n", j );

  return ;

}
