/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
#include <stdio.h>
#include <math.h>

/*
  Differentiation of sub1 in forward (tangent) mode:
   variations   of useful results: *y
   with respect to varying inputs: *y
   RW status of diff variables: y:(loc) *y:in-out
   Plus diff mem management of: y:in
*/
void sub1_d(double *x, double *y, double *yd, double *pic) {
    double result1;
    float result2;
    float result2d;
    result1 = f_nodiff(x, pic);
    result2d = g_d(y, yd, &result2);
    *yd = pic*result2d;
    *y = result1 + result2*pic;
}

double f_nodiff(double *t) {
    return t*t;
}
