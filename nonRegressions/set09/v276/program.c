

void bar(float a, float *b) {
  *b = a * a;
}

void foo(float *x, float *y) {
  bar(*x, y);

}

void testNbCall() {
  printf("testNbCall\n");
}

void main() {
  float x, y;
  x = 2.0;
  printf("x %f\n", x);
  foo(&x, &y);
  printf("x %f y %f\n", x, y);
  foo(&x, &y);
  printf("x %f y %f\n", x, y);

  testNbCall();

}
