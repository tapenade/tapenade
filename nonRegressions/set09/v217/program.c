#include "f.h"

double dabs(double x) {
  if(x < 0) {
    return -x;
  }
  else {
    return x;
  }
}

void f(double u, double *y) {
  double yold = 0.0;
  double ynew = 0.0;

  while(1) {
    ynew = 0.9 * yold + u;
    if(dabs(ynew - yold) < 1e-8) {
      break;
    }
    
    yold = ynew;
  }
  
  *y = ynew;
}
