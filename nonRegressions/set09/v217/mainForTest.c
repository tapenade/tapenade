#include "f.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv) {
  double u = 1.0;
  
  double ub = 0.0;
  double y = 0.0;
  double yb = 1.0;
    
  f_b(u, &ub, &y, &yb);
    
  printf("ub = %f\n", ub);
  
  return 0;
}
