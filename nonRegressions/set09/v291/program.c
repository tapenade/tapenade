// from session abc7ss7_mE26p8LPDhmbw, Tue Dec 12 20:08:23 CET 2017

double f(double x[][20], int m, int n) {
  double* t;
  double z = 1.0;
  int i, j;

  for (i = 0; i < m; ++i) {
    t = x[i];
    for (j = 0; j < n; ++j) {
      z *= t[j];
    }
  }

  return z;
}
