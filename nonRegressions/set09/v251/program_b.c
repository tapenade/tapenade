/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
#include <adStack.h>

/*
  Differentiation of asg_pstep2d in reverse (adjoint) mode:
   gradient     of useful results: **p0 **p1
   with respect to varying inputs: **v0 **v1 **bulk **p0 **p1
   RW status of diff variables: v0:(loc) *v0:(loc) **v0:out v1:(loc)
                *v1:(loc) **v1:out bulk:(loc) *bulk:(loc) **bulk:out
                p0:(loc) *p0:(loc) **p0:in-out p1:(loc) *p1:(loc)
                **p1:in-out
   Plus diff mem management of: v0:in *v0:in v1:in *v1:in bulk:in
                *bulk:in p0:in *p0:in p1:in *p1:in sdiv:in

 pour tester modifiedDeclarator restrict, bug servlet 20150902
 et arrayConstructor, bug servlet 20150818 */
void asg_pstep2d_b(float **restrict bulk, float **bulkb, float **restrict p0, 
        float **p0b, float **restrict p1, float **p1b, float **restrict v0, 
        float **v0b, float **restrict v1, float **v1b, float **restrict ep, 
        float **restrict epp, float *restrict sdiv, float *sdivb, int *gsc, 
        int *gec, int *lbc, int *rbc, int maxoff, float **restrict c) {
    int i0, i1;
    int ioff;
    float U_i[5] = {0.0};
    float U_ib[5] = {0.0};
    float tempb;
    float tempb0;
    int ii1;
    float tmp;
    float tmpb;
    float tmp0;
    float tmpb0;
    float tmp1;
    float tmpb1;
    float tmp2;
    float tmpb2;
    int branch;
    for (i1 = gsc[1]; i1 < gec[1]+1; ++i1) {
        for (i0 = gsc[0]; i0 < gec[0]+1; ++i0) {
            pushReal4(sdiv[i0]);
            sdiv[i0] = 0.0f;
        }
        for (ioff = 0; ioff < maxoff; ++ioff)
            for (i0 = gsc[0]; i0 < gec[0]+1; ++i0) {
                pushReal4(sdiv[i0]);
                sdiv[i0] = sdiv[i0] + (c[0][ioff]*(v0[i1][i0+ioff]-v0[i1][i0-
                    ioff-1]) + c[1][ioff]*(v1[i1+ioff][i0]-v1[i1-ioff-1][i0]))
                ;
            }
    }
    if (lbc[0])
        pushControl1b(0);
    else
        pushControl1b(1);
    if (rbc[0])
        pushControl1b(0);
    else
        pushControl1b(1);
    if (lbc[1])
        pushControl1b(0);
    else
        pushControl1b(1);
    if (rbc[1])
        for (i0 = gec[0]; i0 > gsc[0]-1; --i0) {
            for (ioff = maxoff-1; ioff > 0; --ioff) {
                tmpb2 = p1b[gec[1] + ioff + 1][i0];
                p1b[gec[1] + ioff + 1][i0] = 0.0;
                p1b[gec[1] - ioff + 1][i0] = p1b[gec[1] - ioff + 1][i0] - 
                    tmpb2;
            }
            p1b[gec[1] + 1][i0] = 0.0;
        }
    popControl1b(&branch);
    if (branch == 0)
        for (i0 = gec[0]; i0 > gsc[0]-1; --i0) {
            for (ioff = maxoff-1; ioff > 0; --ioff) {
                tmpb1 = p1b[gsc[1] - ioff - 1][i0];
                p1b[gsc[1] - ioff - 1][i0] = 0.0;
                p1b[gsc[1] + ioff - 1][i0] = p1b[gsc[1] + ioff - 1][i0] - 
                    tmpb1;
            }
            p1b[gsc[1] - 1][i0] = 0.0;
        }
    popControl1b(&branch);
    if (branch == 0)
        for (i1 = gec[1]; i1 > gsc[1]-1; --i1) {
            for (ioff = maxoff-1; ioff > 0; --ioff) {
                tmpb0 = p0b[i1][gec[0] + ioff + 1];
                p0b[i1][gec[0] + ioff + 1] = 0.0;
                p0b[i1][gec[0] - ioff + 1] = p0b[i1][gec[0] - ioff + 1] - 
                    tmpb0;
            }
            p0b[i1][gec[0] + 1] = 0.0;
        }
    popControl1b(&branch);
    if (branch == 0)
        for (i1 = gec[1]; i1 > gsc[1]-1; --i1) {
            for (ioff = maxoff-1; ioff > 0; --ioff) {
                tmpb = p0b[i1][gsc[0] - ioff - 1];
                p0b[i1][gsc[0] - ioff - 1] = 0.0;
                p0b[i1][gsc[0] + ioff - 1] = p0b[i1][gsc[0] + ioff - 1] - tmpb
                ;
            }
            p0b[i1][gsc[0] - 1] = 0.0;
        }
    for (ii1 = 0; ii1 < 5; ++ii1)
        U_ib[ii1] = 0.0;
    U_ib[0] = U_ib[0] + p0b[0][0];
    p1b[0][0] = p1b[0][0] + U_ib[0];
    **v0b = 0.0;
    **v1b = 0.0;
    **bulkb = 0.0;
    *sdivb = 0.0;
    for (i1 = gec[1]; i1 > gsc[1]-1; --i1) {
        for (i0 = gec[0]; i0 > gsc[0]-1; --i0) {
            tempb0 = epp[1][i1]*p1b[i1][i0];
            p1b[i1][i0] = ep[1][i1]*tempb0;
            bulkb[i1][i0] = bulkb[i1][i0] - sdiv[i0]*tempb0;
            sdivb[i0] = sdivb[i0] - bulk[i1][i0]*tempb0;
            tempb0 = epp[0][i0]*p0b[i1][i0];
            p0b[i1][i0] = ep[0][i0]*tempb0;
            bulkb[i1][i0] = bulkb[i1][i0] - sdiv[i0]*tempb0;
            sdivb[i0] = sdivb[i0] - bulk[i1][i0]*tempb0;
        }
        for (ioff = maxoff-1; ioff > -1; --ioff)
            for (i0 = gec[0]; i0 > gsc[0]-1; --i0) {
                popReal4(&(sdiv[i0]));
                tempb = c[0][ioff]*sdivb[i0];
                tempb0 = c[1][ioff]*sdivb[i0];
                v1b[i1 + ioff][i0] = v1b[i1 + ioff][i0] + tempb0;
                v1b[i1 - ioff - 1][i0] = v1b[i1 - ioff - 1][i0] - tempb0;
                v0b[i1][i0 + ioff] = v0b[i1][i0 + ioff] + tempb;
                v0b[i1][i0 - ioff - 1] = v0b[i1][i0 - ioff - 1] - tempb;
            }
        for (i0 = gec[0]; i0 > gsc[0]-1; --i0) {
            popReal4(&(sdiv[i0]));
            sdivb[i0] = 0.0;
        }
    }
}
