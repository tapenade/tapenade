#include <math.h>

const double C = 0.1;	// damping
const double K = 1.0;	// spring
const double A = 0.0;	// nonlinear effect
const double F = 3.0;	// forcing amplitude
const double w0 = 1.1;	// forcing frequency

const int N = 15;	// number of harmonics
const int NT = 31;	// total number of points

const double TSTEP = 0.198;

const double DTAU = 0.0001;	// Pseudotime step size
const int MAXITER = 10000000;
const double MINNORM = 1e-6;

void SolveHB(double** DTPTPT, double xStar[], double dtStar[])
{
	double xStarOld[NT];
	double dtStarOld[NT];
	double Ax = 0;
	double f = 0;
	double t = 0;
	double norm = 0;
	int counter = 0;

	do
	{
		for (int ind = 0; ind < NT; ind++)
		{
			xStarOld[ind] = xStar[ind];
			dtStarOld[ind] = dtStar[ind];
		}

		for (int ind = 0; ind < NT; ind++)
		{
			Ax = 0;
			for (int c = 0; c < NT; c++)
				Ax += w0*DTPTPT[ind][c]*xStarOld[c];

			f = dtStarOld[ind];

			xStar[ind] = xStarOld[ind] + DTAU*(f-Ax);

			Ax = 0;
			for (int c = 0; c < NT; c++)
				Ax += w0*DTPTPT[ind][c]*dtStarOld[c];

			t = TSTEP*ind;
			f = F*cos(t)-C*dtStarOld[ind]-K*xStarOld[ind]-K*A*pow(xStarOld[ind],3);

			dtStar[ind] = dtStarOld[ind] + DTAU*(f-Ax);
		}
		counter++;
	} while (counter < MAXITER);
}
