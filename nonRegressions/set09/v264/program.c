static char *str_trim(char *s, unsigned int len);
static char *str_trim(char *s, unsigned int len)
{
  unsigned int i, j=len;
  for (i=0 ; i<len ; ++i) {
    if (s[i] == ' ') {
      if (i < j) j = i;
    } else {
      j = len;
    }
  }
  s[j] = 0;
  return s;
}

static char *str_trim(char *s, unsigned int len);
