/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.13 (r6968M) - 13 Jul 2018 15:58
*/
static char *str_trim(char *s, unsigned int len);

static char * str_trim(char *s, unsigned int len) {
    unsigned int i;
    unsigned int j = len;
    for (i = 0; i < len; ++i)
        if (s[i] == ' ') {
            if (i < j)
                j = i;
        } else
            j = len;
    s[j] = 0;
    return s;
    //     str_trim points to *s
}

static char *str_trim(char *s, unsigned int len);
