#include <stdio.h>

double fomega(double raz) {																
    double a=0.998560547, b=66.94633214, c=56.94171, d=289.5945856;	
	double e=-42.6260035, f=1533.339148, g=248.7977744, h=413.1534199;    
	double i=-12.4938375, j=1050.830267, num, den;							
	
	num=(a+c*pow(raz, 0.5)+e*raz+g*pow(raz, 1.5)+i*pow(raz, 2.0));				
	den=(1.0+b*pow(raz, 0.5)+d*raz+f*pow(raz, 1.5)+h*pow(raz, 2.0)+j*pow(raz, 2.5));

	if (den<pow(10, -10)) printf("Warning: Divide by zero.345\n");
	return  2.0*num/den;	
}	
