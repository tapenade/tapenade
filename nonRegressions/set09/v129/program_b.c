/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) - 14 Feb 2022 12:36
*/
#include <adStack.h>
#include <stdio.h>

/*
  Differentiation of fomega in reverse (adjoint) mode:
   gradient     of useful results: fomega raz
   with respect to varying inputs: raz
   RW status of diff variables: fomega:in-killed raz:incr
*/
void fomega_b(double raz, double *razb, double fomegab) {
    double a = 0.998560547;
    double b = 66.94633214;
    double c = 56.94171;
    double d = 289.5945856;
    double e = -42.6260035;
    double f = 1533.339148;
    double g = 248.7977744;
    double h = 413.1534199;
    double i = -12.4938375;
    double j = 1050.830267;
    double num;
    double numb;
    double den;
    double denb;
    double tempb;
    num = a + c*pow(raz, 0.5) + e*raz + g*pow(raz, 1.5) + i*pow(raz, 2.0);
    den = 1.0 + b*pow(raz, 0.5) + d*raz + f*pow(raz, 1.5) + h*pow(raz, 2.0) + 
        j*pow(raz, 2.5);
    tempb = 2.0*fomegab/den;
    numb = tempb;
    denb = -(num*tempb/den);
    *razb = *razb + (0.5*pow(raz, 0.5-1)*b+d+1.5*pow(raz, 1.5-1)*f+2.0*pow(raz
        , 2.0-1)*h+2.5*pow(raz, 2.5-1)*j)*denb + (0.5*pow(raz, 0.5-1)*c+e+1.5*
        pow(raz, 1.5-1)*g+2.0*pow(raz, 2.0-1)*i)*numb;
}
