#include <stdio.h>
#include <math.h>

enum { a = 2, b};

float test(float x) {
  printf("x %f..\n" , x);
  x = a * x;
  printf("x %f..\n" , x);
  x = b * x;
  if (x < pow(10, -10) )  printf("argggg \n");
  return x;
}

main () {
  float x;
  x = 4.0;
  test(x);
}
