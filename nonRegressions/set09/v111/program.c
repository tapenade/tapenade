float ifExpression (int b, float e1, float e2) {
  float result;
  if (b) {
    result = e1;
  } else {
    result = e2;
  }
  return result;
}
