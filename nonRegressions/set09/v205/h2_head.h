#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <float.h>
#include <stdbool.h>

#define NUM 1 
#define BLOCK 128

extern const double Ru;
extern const double Ruc;
extern double dtp;

#define USEEISPACK
#define ERROR
#define DOUBLE
  #define Real float
  #define ZERO 0.0f
  #define ONE 1.0f
  #define TWO 2.0f
  #define THREE 3.0f
  #define FOUR 4.0f
  #define SMALL FLT_EPSILON  
#define CONP
#define NRXN 38
#define NSP 9
#define NN 10

typedef struct {
  Real A;
  Real b; 
  Real Ta;
} rxn_info;

typedef struct {
  Real mw; 	
  Real c_lo[5]; 
  Real c_hi[5]; 
  Real h_lo[6];
  Real h_hi[6];
  Real T_mid;
} spc_info;

typedef unsigned int uint;
typedef unsigned short int usint;
