
static char adSid[]="$Id: adStack.c,v 1.4 10/.0/.0 .1:.5:.2 llh Exp $";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define ONE_BLOCK_SIZE 16384
#define CHUNK_SIZE 4096

/* The main stack is a double-chain of DoubleChainedBlock objects.
 * Each DoubleChainedBlock holds an array[ONE_BLOCK_SIZE] of char. */
typedef struct _doubleChainedBlock{
  struct _doubleChainedBlock *prev ;
  char                       *stackBottom ;
  char                       *stackTop ;
  struct _doubleChainedBlock *next ;
} DoubleChainedBlock ;

/* Globals that define the current position in the stack: */
static DoubleChainedBlock *curStack = NULL ;
static char               *curStackBottom = NULL ;
static char               *curStackTop    = NULL ;

/* Used before PUSHing.
 * Resets the LOOKing position if it was active.
 * Checks that there is enough space left to hold "nbChars" chars.
 * Otherwise, allocates the necessary space. */
void check(int nbChars) {
    curStackBottom = curStack->stackBottom ;
    curStackTop    = curStackBottom ;
}

/* PUSHes "nbChars" consecutive chars,
 * from a location starting at address "x".
 * nbChars is assumed no larger than CHUNK_SIZE */
void pushN(char *x, int nbChars) {
  check(nbChars) ;
  memcpy(curStackTop,x,nbChars);
  curStackTop+=nbChars ;
}

/* PUSHes a large number "n" of consecutive chars,
 * from a location starting at address "x".
 * This "n"-sized array is cut into pieces no larger than CHUNK_SIZE */
void pushNarray(char *x, int n) {
  int tailSize = n%CHUNK_SIZE ;
  char *xmax = x+n-tailSize ;
  char *xin = x ;
  while(xin<xmax) {
    pushN(xin, CHUNK_SIZE) ;
    xin += CHUNK_SIZE ;
  }
  if (tailSize>0) pushN(xin, tailSize) ;
}

void pushcharacter_(char *x) {
  pushN(x,1) ;
}
