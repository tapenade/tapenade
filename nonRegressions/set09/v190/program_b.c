/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) -  7 Sep 2022 17:17
*/
#include <adStack.h>
#include <math.h>
typedef double Ffloat;
typedef int Fint;
typedef struct {
    Ffloat rho, u, v, w, p;
    Ffloat tke, tls;
    Ffloat x, r, phi;
    Ffloat vR, vPhi, vM;
    Ffloat T, a;
} FphysVal;
typedef struct {
    Ffloat x;
} FgeomVal;
typedef struct {
    FphysVal phys;
} Fnode;
typedef struct {
    Ffloat gamma;
    Ffloat length;
} RefVals;
typedef struct {
    Ffloat massflow, beta_abs, p_total_abs, p_static;
    int block_number;
} BdVal;
typedef struct {
    RefVals ref;
    Ffloat control;
} Fglobal;
Fglobal global;
Fglobal globalb;
FphysVal PhysAbs, PhysRel;
FphysVal PhysAbsb, PhysRelb;

/*
  Differentiation of calcStageData in reverse (adjoint) mode (with options no!Activity no!SpareDiffArgs no!SpareInits no!SplitDiffExprs no!MergeDiffInstrs no!SaveOnlyUsed no!SaveOnlyTBR no!ReducedSnapshots no!SliceDeadControl no!SliceDeadInstrs no!tryRecomputations no!StripDiffTypes):
   gradient     of useful results: *bd.massflow *bd.beta_abs *bd.p_total_abs
                *bd.p_static
   with respect to varying inputs: PhysAbs.u PhysAbs.w PhysAbs.p
                global.ref.gamma *bd.massflow *bd.p_total_abs
                *bd.p_static
   RW status of diff variables: PhysAbs.u:incr PhysAbs.w:incr
                PhysAbs.p:incr global.ref.gamma:incr *bd.massflow:in-out
                *bd.beta_abs:in-out *bd.p_total_abs:in-out *bd.p_static:in-out
*/
void calcStageData_b(BdVal *bd, BdVal *bdb, int stage, BdVal *radVal, BdVal *
        radValb) {
    int blk;
    blk = bd->block_number;
    Fnode av_rel, av_abs;
    Fnode av_relb, av_absb;
    int j, k, l;
    float localMass;
    float localMassb;
    float sumMass;
    float sumMassb;
    sumMass = 0.0;
    float t_av;
    float t_avb;
    float globalArea;
    float globalAreab;
    globalArea = 0.;
    float xLocation;
    float xLocationb;
    xLocation = 0.0;
    FgeomVal Area;
    FgeomVal Areab;
    pushReal8(bd->p_total_abs);
    bd->p_total_abs = 0.;
    for (l = 0; l < 9; ++l) {
        pushInteger4(k);
        for (k = 0; k < 9; ++k) {
            pushReal8(bd->p_static);
            bd->p_static = bd->p_static + localMass*av_rel.phys.p;
            pushReal8(bd->massflow);
            bd->massflow = bd->massflow + localMass;
            pushReal4(globalArea);
            globalArea = globalArea + Area.x;
        }
    }
    if (j == 1) {
        pushReal8(bd->p_static);
        bd->p_static = PhysAbs.p;
        pushReal8(bd->p_total_abs);
        bd->p_total_abs = bd->p_static*(1.+0.5*global.ref.gamma*(PhysAbs.w*
            PhysAbs.w));
        pushReal8(bd->beta_abs);
        bd->beta_abs = atan(PhysAbs.w/PhysAbs.u)*180./(4.0*atan(1.0));
        localMassb = 0.0;
        xLocationb = 0.0;
        av_absb.phys.rho = 0.0;
        av_absb.phys.u = 0.0;
        av_absb.phys.v = 0.0;
        av_absb.phys.w = 0.0;
        av_absb.phys.p = 0.0;
        av_absb.phys.tke = 0.0;
        av_absb.phys.tls = 0.0;
        av_absb.phys.x = 0.0;
        av_absb.phys.r = 0.0;
        av_absb.phys.phi = 0.0;
        av_absb.phys.vR = 0.0;
        av_absb.phys.vPhi = 0.0;
        av_absb.phys.vM = 0.0;
        av_absb.phys.T = 0.0;
        av_absb.phys.a = 0.0;
        av_relb.phys.rho = 0.0;
        av_relb.phys.u = 0.0;
        av_relb.phys.v = 0.0;
        av_relb.phys.w = 0.0;
        av_relb.phys.p = 0.0;
        av_relb.phys.tke = 0.0;
        av_relb.phys.tls = 0.0;
        av_relb.phys.x = 0.0;
        av_relb.phys.r = 0.0;
        av_relb.phys.phi = 0.0;
        av_relb.phys.vR = 0.0;
        av_relb.phys.vPhi = 0.0;
        av_relb.phys.vM = 0.0;
        av_relb.phys.T = 0.0;
        av_relb.phys.a = 0.0;
        sumMassb = 0.0;
        t_avb = 0.0;
        globalAreab = 0.0;
        Areab.x = 0.0;
        popReal8(&(bd->beta_abs));
        PhysAbsb.w = PhysAbsb.w + 180.*bdb->beta_abs/(PhysAbs.u*(1.0+PhysAbs.w
            /PhysAbs.u*(PhysAbs.w/PhysAbs.u))*4.0*atan(1.0));
        PhysAbsb.u = PhysAbsb.u - PhysAbs.w*180.*bdb->beta_abs/(PhysAbs.u*
            PhysAbs.u*(1.0+PhysAbs.w/PhysAbs.u*(PhysAbs.w/PhysAbs.u))*4.0*atan
            (1.0));
        bdb->beta_abs = 0.0;
        popReal8(&(bd->p_total_abs));
        bdb->p_static = bdb->p_static + (0.5*(global.ref.gamma*(PhysAbs.w*
            PhysAbs.w))+1.)*bdb->p_total_abs;
        globalb.ref.gamma = globalb.ref.gamma + PhysAbs.w*PhysAbs.w*0.5*bd->
            p_static*bdb->p_total_abs;
        PhysAbsb.w = PhysAbsb.w + 2*PhysAbs.w*global.ref.gamma*0.5*bd->
            p_static*bdb->p_total_abs;
        bdb->p_total_abs = 0.0;
        popReal8(&(bd->p_static));
        PhysAbsb.p = PhysAbsb.p + bdb->p_static;
        bdb->p_static = 0.0;
    } else if (k == 0) {
        pushReal8(bd->p_total_abs);
        bd->p_total_abs = bd->p_total_abs/bd->massflow;
        localMassb = 0.0;
        xLocationb = 0.0;
        av_absb.phys.rho = 0.0;
        av_absb.phys.u = 0.0;
        av_absb.phys.v = 0.0;
        av_absb.phys.w = 0.0;
        av_absb.phys.p = 0.0;
        av_absb.phys.tke = 0.0;
        av_absb.phys.tls = 0.0;
        av_absb.phys.x = 0.0;
        av_absb.phys.r = 0.0;
        av_absb.phys.phi = 0.0;
        av_absb.phys.vR = 0.0;
        av_absb.phys.vPhi = 0.0;
        av_absb.phys.vM = 0.0;
        av_absb.phys.T = 0.0;
        av_absb.phys.a = 0.0;
        av_relb.phys.rho = 0.0;
        av_relb.phys.u = 0.0;
        av_relb.phys.v = 0.0;
        av_relb.phys.w = 0.0;
        av_relb.phys.p = 0.0;
        av_relb.phys.tke = 0.0;
        av_relb.phys.tls = 0.0;
        av_relb.phys.x = 0.0;
        av_relb.phys.r = 0.0;
        av_relb.phys.phi = 0.0;
        av_relb.phys.vR = 0.0;
        av_relb.phys.vPhi = 0.0;
        av_relb.phys.vM = 0.0;
        av_relb.phys.T = 0.0;
        av_relb.phys.a = 0.0;
        sumMassb = 0.0;
        t_avb = 0.0;
        globalAreab = 0.0;
        Areab.x = 0.0;
        popReal8(&(bd->p_total_abs));
        bdb->massflow = bdb->massflow - bd->p_total_abs*bdb->p_total_abs/(bd->
            massflow*bd->massflow);
        bdb->p_total_abs = bdb->p_total_abs/bd->massflow;
    } else {
        localMassb = 0.0;
        xLocationb = 0.0;
        av_absb.phys.rho = 0.0;
        av_absb.phys.u = 0.0;
        av_absb.phys.v = 0.0;
        av_absb.phys.w = 0.0;
        av_absb.phys.p = 0.0;
        av_absb.phys.tke = 0.0;
        av_absb.phys.tls = 0.0;
        av_absb.phys.x = 0.0;
        av_absb.phys.r = 0.0;
        av_absb.phys.phi = 0.0;
        av_absb.phys.vR = 0.0;
        av_absb.phys.vPhi = 0.0;
        av_absb.phys.vM = 0.0;
        av_absb.phys.T = 0.0;
        av_absb.phys.a = 0.0;
        av_relb.phys.rho = 0.0;
        av_relb.phys.u = 0.0;
        av_relb.phys.v = 0.0;
        av_relb.phys.w = 0.0;
        av_relb.phys.p = 0.0;
        av_relb.phys.tke = 0.0;
        av_relb.phys.tls = 0.0;
        av_relb.phys.x = 0.0;
        av_relb.phys.r = 0.0;
        av_relb.phys.phi = 0.0;
        av_relb.phys.vR = 0.0;
        av_relb.phys.vPhi = 0.0;
        av_relb.phys.vM = 0.0;
        av_relb.phys.T = 0.0;
        av_relb.phys.a = 0.0;
        sumMassb = 0.0;
        t_avb = 0.0;
        globalAreab = 0.0;
        Areab.x = 0.0;
    }
    for (l = 8; l > -1; --l) {
        for (k = 8; k > -1; --k) {
            popReal4(&globalArea);
            Areab.x = Areab.x + globalAreab;
            popReal8(&(bd->massflow));
            localMassb = localMassb + bdb->massflow;
            popReal8(&(bd->p_static));
            localMassb = localMassb + av_rel.phys.p*bdb->p_static;
            av_relb.phys.p = av_relb.phys.p + localMass*bdb->p_static;
        }
        popInteger4(&k);
    }
    popReal8(&(bd->p_total_abs));
    bdb->p_total_abs = 0.0;
    xLocationb = 0.0;
    globalAreab = 0.0;
    sumMassb = 0.0;
}
