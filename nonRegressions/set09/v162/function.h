
#include <stdio.h>
#include <stdlib.h>


double Max( double a, double b);
double Min( double a, double b);
void ComputeIncr(double* Xin, double* Incr,
                 int Nx, double dx, double nu_t, double nu, double r);
