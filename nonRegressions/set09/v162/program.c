
#include <stdlib.h>
#include<stdio.h>
//#include "function.h"

double Max( double a, double b)
{
#include "function.h"
  if (a > b)
    return a;
  else 
    return b;
}

double Min( double a, double b)
{
#include "function.h"
  if (a < b)
    return a;
  else 
    return b;
}

void ComputeIncr(double* Xin, double* Incr, 
                 int Nx, double dx, double nu_t, double nu, double r)
{
#include "function.h"
  /* intermediaire */
  double 
    auxB, Bi, Bim1, Bdx, ULap, Uincr, UdF, FLap, Fincr;
  int i;
  
  /* init resultat a 0: utile pour les extremites */
  for (i = 0 ; i < 2 * Nx; i++)
    Incr[i] = 0.;
  
  /* Spatial Integration */
  
  /* U */
  for(i = 1; i <= Nx - 2;  i++)
    {
      auxB = (Xin[i] + Xin[i + 1]) / 2;
      Bi = 0.5 * auxB * auxB ;
      auxB = (Xin[i - 1] + Xin[i]) / 2;
      Bim1 = 0.5 * auxB * auxB ;
      Bdx = (Bi - Bim1) / dx;
      ULap = 
        (Xin[i - 1] - 2 * Xin[i] +  Xin[i + 1]) / (dx * dx);
      Incr[i] = -Bdx + nu * ULap - r * Xin[i];
    } 

  /*F */
  for(i = Nx + 1; i <= 2 * Nx - 2;  i++)
    {
      UdF = (1 / dx) *
        (
         Max(Xin[i - Nx], 0.) * (Xin[i] - Xin[i - 1] ) +
         Min(Xin[i - Nx + 1], 0.) * ( Xin[i + 1] - Xin[i])
         ); 
      FLap = 
        (Xin[i - 1] - 2 * Xin[i] + Xin[i + 1]) / (dx * dx);
      Incr[i] = -UdF + nu_t * FLap ;
    }    
}
