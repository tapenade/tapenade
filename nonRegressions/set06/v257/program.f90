subroutine cost_function_penal ( manning, bathy_cell,bc_in_ctr,bc_out_ctr, tracer_src_tserie, dof0, tcost_p )
!    use m_common
!    use m_type
    implicit none
    real(8), dimension(nland),      intent(in)   :: manning
    real(8), dimension(nelt),       intent(in)   :: bathy_cell
    type(type_boundary_condition_in_control), dimension(number_bc_in), intent(in)::	bc_in_ctr
    type(type_boundary_condition_out_control), dimension(number_bc_out), intent(in)::	bc_out_ctr 
    real(8), dimension(nbdt),      intent(in)   :: tracer_src_tserie
    real(8), dimension(nelt,neqn),    intent(in)   :: dof0
    real(8),                    intent(out)     :: tcost_p

    integer                 :: k,i,j,index

    do k = 2,nbdt-1
		do index=1,number_bc_in
			do i=1,bc_in(index)%ibc_numpoints
				do j=1,bc_in(index)%ibc_size
    		tcost_p = tcost_p +  alph_penalty * ((bc_in_ctr(index)%inflow_bc_tserie(k+1,i,j)-2*bc_in_ctr(index)%inflow_bc_tserie(k,i,j)&
    				&+bc_in_ctr(index)%inflow_bc_tserie(k-1,i,j))/(dt**2))**2
				enddo
			enddo
		enddo

    enddo
    
end subroutine cost_function_penal
