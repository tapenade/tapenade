!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6960M) -  3 Jul 2018 16:55
!
!  Differentiation of cost_function_penal in reverse (adjoint) mode:
!   gradient     of useful results: tcost_p
!   with respect to varying inputs: tcost_p
!   RW status of diff variables: tcost_p:in-out
SUBROUTINE COST_FUNCTION_PENAL_B(manning, bathy_cell, bc_in_ctr, &
& bc_out_ctr, tracer_src_tserie, dof0, tcost_p, tcost_pb)
  IMPLICIT NONE
  TYPE(UNKNOWNTYPE) :: nland
  REAL*8, DIMENSION(nland), INTENT(IN) :: manning
  TYPE(UNKNOWNTYPE) :: nelt
  REAL*8, DIMENSION(nelt), INTENT(IN) :: bathy_cell
  INTEGER :: number_bc_in
  TYPE(TYPE_BOUNDARY_CONDITION_IN_CONTROL), DIMENSION(number_bc_in), &
& INTENT(IN) :: bc_in_ctr
  TYPE(UNKNOWNTYPE) :: number_bc_out
  TYPE(TYPE_BOUNDARY_CONDITION_OUT_CONTROL), DIMENSION(number_bc_out), &
& INTENT(IN) :: bc_out_ctr
  REAL :: nbdt
  REAL*8, DIMENSION(nbdt), INTENT(IN) :: tracer_src_tserie
  TYPE(UNKNOWNTYPE) :: neqn
  REAL*8, DIMENSION(nelt, neqn), INTENT(IN) :: dof0
  REAL*8 :: tcost_p
  REAL*8 :: tcost_pb
  INTEGER :: k, i, j, index
  EXTERNAL BC_IN
  TYPE UNKNOWNDERIVEDTYPE
      INTEGER :: ibc_numpoints
      INTEGER :: ibc_size
  END TYPE UNKNOWNDERIVEDTYPE
  TYPE(UNKNOWNDERIVEDTYPE) :: BC_IN
  REAL :: alph_penalty
  REAL :: dt
END SUBROUTINE COST_FUNCTION_PENAL_B

