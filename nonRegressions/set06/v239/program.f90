    FUNCTION quadratic_roots(alpha,beta)
      INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(10,50)
      REAL(kind=wp), INTENT(in) :: alpha, beta
      COMPLEX(kind=wp), DIMENSION(2) :: quadratic_roots

      COMPLEX(kind=wp) :: temp, sqdisc, r1, r2
      real(wp) two
      parameter(two=2.0_wp)
      temp = alpha/two; sqdisc = SQRT(temp**2 - beta)
      r1 = -temp + sqdisc; r2 = -temp + sqdisc
      ! il ne faut pas inliner abs sur des complex
        IF (ABS(r1) > ABS(r2)) THEN
          quadratic_roots = (/ r1, r2 /)
        ELSE
          quadratic_roots = (/ r2, r1 /)
        END IF
    END FUNCTION quadratic_roots
