! bug de Hubert
! pb avec globald declaree dans test et differentiee dans module n
! et test_cd use n_d

MODULE M
  IMPLICIT NONE
  real :: pi = 3.14
  real, dimension(2) :: tab = (/1.0,2.0/)
END MODULE M

module n
  use m
  real :: global = sin(3.14)
  real :: gl
end module n

subroutine subr(y)
 use n
 real y
 g1 = tab(1)
 y = y * global * gl
end subroutine subr

program test
use n

real :: globald
print*, global
print*, gl
call subr(global)

end program
