! bug de split du test dans un while

SUBROUTINE test(x, y)
    real :: x, y
    logical :: f
    do while(f(x,y))
        x = x * 3
    end do 

END SUBROUTINE test

function f(val1, val2)
  logical :: f
  real :: val1, val2
  val2 = val1 * 2
  print *,val1
  print *,val2
  f = (val1 .le. val2) .and. (val2 .le. 2000.0)
end function f

program main
  real :: x, y
  x = 20.0
  y = 1.0
  print *,x
  print *,y
  call test(x,y)
end program
