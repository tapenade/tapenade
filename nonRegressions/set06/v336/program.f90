
MODULE GLOBALS
 implicit real*8 (a-z)
 allocatable aa(:), bb(:), cc(:)
 real(8) rx0(1:2,4),rx(1:2,4),rxi(2),nurw(3,5),ftco

CONTAINS
  SUBROUTINE INIT_GLOBALS()
    allocate(AA(100))
    allocate(BB(100))
    allocate(CC(100))
  END SUBROUTINE INIT_GLOBALS
END MODULE GLOBALS




SUBROUTINE COMP(X)
  USE GLOBALS
  REAL X
  BB(2) = BB(3)*BB(4)
  call PCG(AA,BB,X)
  AA(1)=BB(1)*AA(2) * ftco
  ftco = aa(2) * bb(2)
END SUBROUTINE COMP



PROGRAM PP
  USE GLOBALS
  REAL X
  CALL INIT_GLOBALS()
  CALL COMP(X)
END PROGRAM PP
