! tapenade -O /tmp -o p -vars v2 -outvars f program.f90
! ou bien
! tapenade -O /tmp -o p -vars "v2" -outvars "v3" pr2.f90

program testk
 implicit none
 include 'mpif.h'
 integer, dimension( MPI_STATUS_SIZE ) :: statut
 integer, parameter :: etiquette=100
 integer :: nb_procs,rang, code1, code2, code
 real :: x,y,z,b,f
 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 print *,'Proc ' , rang

 x = 0.0
 y = 4.0
 z = 2.0
 b = 7.0
 f = 99.0

 call test(x, y, z, b, rang, code1, code2, etiquette, statut)

 call MPI_FINALIZE (code)
end program testk


subroutine test(v1,v2,v3,v4, rang, code1, code2, etiquette, statut)
include 'mpif.h'

real :: v1,v2,v3,v4
integer :: rang, code1, code2, statut, etiquette


! if (rang == 0) then
      call f1(v1,v2, etiquette, code1)
! else
      call f2(v3,v4, etiquette, statut,code2)
      !z = b * y
! end if

 print *,'z ', z , ', proc ' , rang

 call MPI_REDUCE(v3,f,1,MPI_REAL,MPI_SUM,0,MPI_COMM_WORLD,ierr)

 print *,'Resultat ', f , ', proc ' , rang


end subroutine

subroutine f1(a,b, etiquette, code)
include 'mpif.h'
  real x
  integer :: code, etiquette, err
  INTEGER, DIMENSION(mpi_status_size) :: statut

!  a = x                                  
!  send(a)  ! a ne dépend d'aucun in de f1    
   x = 7.0
   a = x * b
!   send(a, k) ! a dépend de b
print *, 'avant send   '

   call MPI_ISEND (a,1, MPI_REAL , 0, etiquette, &
         MPI_COMM_WORLD ,code)
   call MPI_WAIT(code, statut, err)

print *, 'apres send   '


end

subroutine f2(c,d, etiquette, statut,code)
include 'mpif.h'
  integer :: code, statut, etiquette, err

  c = 0.0
!  recv(c, k)  ! c dépend du b de f1
print *, 'avant recv   '

  call MPI_IRECV (c,1, MPI_REAL , 0,etiquette, &
      MPI_COMM_WORLD ,code,statut)
  call MPI_WAIT(code, statut, err)
  
print *, 'apres recv   '
end

!! si b active, c active

