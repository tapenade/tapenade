!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_fortranparser) - 12 Mar 2020 15:05
!
! bug de Jean Utke du 31/05/2011
! pb avec le type de dumm = MOO1_D(...)
MODULE B_DIFF
  IMPLICIT NONE
  TYPE T
      REAL*8 :: x
  END TYPE T
  TYPE(T) :: ta(4)
  TYPE(T) :: tab(4)

CONTAINS
!  Differentiation of boo in reverse (adjoint) mode:
!   gradient     of useful results: ta.x
!   with respect to varying inputs: ta.x
  SUBROUTINE BOO_B(i)
    IMPLICIT NONE
    INTEGER :: i
    tab(i)%x = 4*tab(i)%x
  END SUBROUTINE BOO_B

  SUBROUTINE BOO(i)
    IMPLICIT NONE
    INTEGER :: i
    ta(i)%x = ta(i)%x*4
    PRINT*, ta(i)%x
  END SUBROUTINE BOO

END MODULE B_DIFF

MODULE M_DIFF
  IMPLICIT NONE
  PRIVATE 
  PUBLIC :: moo1, moo2, maa, mee
  PUBLIC :: moo1_b, moo2_b, mee_b

CONTAINS
!  Differentiation of moo1 in reverse (adjoint) mode:
!   gradient     of useful results: ta.x x moo1.x
!   with respect to varying inputs: ta.x x
  SUBROUTINE MOO1_B(i, x, xb, moo1b)
    USE B_DIFF
    IMPLICIT NONE
    TYPE(T) :: moo1
    TYPE(T) :: moo1b
    INTEGER :: i
    REAL*8 :: x
    REAL*8 :: xb
    CALL BOO_B(i)
    xb = xb + moo1b%x
  END SUBROUTINE MOO1_B

  FUNCTION MOO1(i, x)
    USE B_DIFF
    IMPLICIT NONE
    TYPE(T) :: moo1
    INTEGER :: i
    REAL*8 :: x
    moo1 = ta(i)
    moo1%x = x
    CALL BOO(i)
  END FUNCTION MOO1

!  Differentiation of moo2 in reverse (adjoint) mode:
!   gradient     of useful results: ta.x moo2.x
!   with respect to varying inputs: ta.x
  SUBROUTINE MOO2_B(i, moo2b)
    USE B_DIFF
    IMPLICIT NONE
    TYPE(T) :: moo2
    TYPE(T) :: moo2b
    INTEGER :: i
    tab(i)%x = tab(i)%x + moo2b%x
    CALL BOO_B(i)
  END SUBROUTINE MOO2_B

  FUNCTION MOO2(i)
    USE B_DIFF
    IMPLICIT NONE
    TYPE(T) :: moo2
    INTEGER :: i
    CALL BOO(i)
    moo2 = ta(i)
  END FUNCTION MOO2

  SUBROUTINE MAA(i)
    IMPLICIT NONE
    INTEGER :: i
    PRINT*, i
  END SUBROUTINE MAA

!  Differentiation of mee in reverse (adjoint) mode:
!   gradient     of useful results: ta.x
!   with respect to varying inputs: ta.x at.x
  SUBROUTINE MEE_B(at, atb, i)
    USE B_DIFF
    IMPLICIT NONE
    TYPE(T) :: at
    TYPE(T) :: atb
    INTEGER :: i
    CALL BOO_B(i)
    atb%x = tab(i)%x
    tab(i)%x = 0.0_8
  END SUBROUTINE MEE_B

  SUBROUTINE MEE(at, i)
    USE B_DIFF
    IMPLICIT NONE
    TYPE(T) :: at
    INTEGER :: i
    ta(i)%x = at%x
    CALL BOO(i)
  END SUBROUTINE MEE

END MODULE M_DIFF

!  Differentiation of s in reverse (adjoint) mode:
!   gradient     of useful results: ta.x x y
!   with respect to varying inputs: ta.x x y
!   RW status of diff variables: ta.x:in-out x:incr y:in-zero
SUBROUTINE S_B(x, xb, y, yb)
  USE B_DIFF
  USE M_DIFF
  IMPLICIT NONE
  TYPE(T) :: st, st1
  TYPE(T) :: stb, st1b
  REAL*8 :: x, y
  REAL*8 :: xb, yb
  REAL*8 :: dummydiffb
  stb%x = yb
  CALL MOO2_B(1, stb)
  CALL BOO_B(1)
  CALL MEE_B(st, stb, 1)
  st1b%x = 0.0_8
  CALL MOO1_B(2, 2.0_8, dummydiffb, st1b)
  CALL MOO1_B(1, x, xb, stb)
  yb = 0.0_8
END SUBROUTINE S_B

SUBROUTINE S_NODIFF(x, y)
  USE B_DIFF
  USE M_DIFF
  IMPLICIT NONE
  TYPE(T) :: st, st1
  REAL*8 :: x, y
  st = MOO1(1, x)
  st1 = MOO1(2, 2.0_8)
  CALL MEE(st, 1)
  CALL BOO(1)
  st = MOO2(1)
  y = st%x
END SUBROUTINE S_NODIFF

