! bug de Jean Utke du 31/05/2011
! pb avec le type de dumm = MOO1_D(...)

module b
  type t 
     real*8 :: x 
  end type t
  type(t) :: tA(4)
contains
  subroutine boo(i)
    tA(i)%x=tA(i)%x*4
    print *,tA(i)%x

  end subroutine boo
end module b

module m 
  private
  public moo1, moo2,  maa, mee
contains
  function moo1(i,x)
    use b
    type(t) :: moo1
    integer i
    real*8 :: x
    moo1=tA(i)
    moo1%x=x
    call boo(i)
  end function
  function moo2(i)
    use b
    type(t) :: moo2
    integer i
    call boo(i)
    moo2=tA(i)
  end function
  subroutine maa(i)
    print *,i
  end subroutine maa
  subroutine mee(at,i)
    use b
    type(t) :: at
    ta(i)%x=at%x
    call boo(i)
  end subroutine mee
end module m

subroutine s(x,y)
  use b
  use m
  type(t) :: st,st1
  real*8 :: x, y
  st=moo1(1,x) 
  st1=moo1(2,2.0_8)
  call mee(st,1)
  call boo(1)
  st=moo2(1)
  y=st%x
end subroutine s

program p 
  real*8:: x, y
  x=1.0
  call s(x,y)
end program p
