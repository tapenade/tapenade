module m1
  type t 
    integer, pointer :: p=>null()
  end type
end module 

subroutine p1
  use m1
  integer, pointer :: pp=>null()
  integer,target :: i 
  pp=>i
  i=2
  print *,pp
end subroutine


module m2
  type t 
    integer, pointer :: p=>null()
  end type
end module 

subroutine p2
  use m2, only : t
  integer, pointer :: pp=>null()
  integer,target :: i 
! on pourrait regenerer
!   INTRINSIC NULL
! car l'intrinsic n'est pas importe'e depuis m2
  pp=>i
  i=2
  print *,pp
end subroutine


module m
  type t 
    integer, pointer :: p=>null()
  end type
end module 

module n
use m
end module n

program p 
  use n
  integer, pointer :: pp=>null()
  integer,target :: i 
  pp=>i
  i=2
  print *,pp
end program
