!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.12 (r6321M) -  3 Mar 2017 12:43
!
MODULE M1
  IMPLICIT NONE
  TYPE T
      INTEGER, POINTER :: p => NULL()
  END TYPE T
END MODULE M1

SUBROUTINE P1()
  USE M1
  IMPLICIT NONE
  INTEGER, POINTER, SAVE :: pp => NULL()
!     pp points to *pp or NULL
  INTEGER, TARGET :: i
  pp => i
!     pp points to i
  i = 2
  PRINT*, pp
END SUBROUTINE P1
!     pp points to Undef

MODULE M2
  IMPLICIT NONE
  TYPE T
      INTEGER, POINTER :: p => NULL()
  END TYPE T
END MODULE M2

SUBROUTINE P2()
  USE M2, ONLY : t
  IMPLICIT NONE
  INTEGER, POINTER, SAVE :: pp => NULL()
!     pp points to *pp or NULL
  INTEGER, TARGET :: i
! on pourrait regenerer
!   INTRINSIC NULL
! car l'intrinsic n'est pas importe'e depuis m2
  pp => i
!     pp points to i
  i = 2
  PRINT*, pp
END SUBROUTINE P2
!     pp points to Undef

MODULE M
  IMPLICIT NONE
  TYPE T
      INTEGER, POINTER :: p => NULL()
  END TYPE T
END MODULE M

MODULE N
  USE M
  IMPLICIT NONE
END MODULE N

PROGRAM P
  USE N
  IMPLICIT NONE
  INTEGER, POINTER, SAVE :: pp => NULL()
!     pp points to *pp or NULL
  INTEGER, TARGET :: i
  pp => i
!     pp points to i
  i = 2
  PRINT*, pp
END PROGRAM P
!     pp points to Undef

