
module constants_m

! computational
integer,parameter::wp=8
real(wp),parameter::zero=0._wp
real(wp),parameter::one=1._wp
real(wp),parameter::ten=10._wp
real(wp),parameter::small=ten**(-20)
real(wp),parameter::large=one/small

! mathematical
real(wp),parameter::pi=3.14
real(wp),parameter::rad=pi/180._wp
real(wp)::c_sph=4._wp/3*pi

! physical
real(wp)::c_gas=8.314472_wp
real(wp)::c_grv=-9.81_wp

! Tapenade reverse-mode dependency
interface
  subroutine pushpointer(pp)
    real,pointer::pp
  end subroutine
  subroutine poppointer(pp)
    real,pointer::pp
  end subroutine
end interface
end module


module math_utils_m
use constants_m

contains


function vec_mag(vec)
  real(wp),dimension(:),intent(in)::vec
  real(wp)::vec_mag
  real(wp),dimension(size(vec))::tmp1
  tmp1=vec**2
  vec_mag=sqrt(sum(tmp1))
end function


function unit_vec(vec)
  real(wp),dimension(:),intent(in)::vec
  real(wp),dimension(size(vec))::unit_vec
  unit_vec=vec/(vec_mag(vec)+small)
end function


function dot_product(vec1,vec2)
  real(wp),dimension(:),intent(in)::vec1,vec2
  real(wp)::dot_product
  dot_product=sum(vec1*vec2)
end function


function cross_prod(vec1,vec2)
  real(wp),dimension(3),intent(in)::vec1,vec2
  real(wp),dimension(3)::cross_prod
  integer,dimension(3),parameter::c1=(/2,3,1/),c2=(/3,1,2/)
  cross_prod=vec1(c1)*vec2(c2)-vec2(c1)*vec1(c2)
end function

end module

