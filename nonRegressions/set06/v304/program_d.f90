!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 29 Apr 2021 10:28
!
MODULE M_DIFF
  IMPLICIT NONE
  REAL :: x
  REAL :: xd
END MODULE M_DIFF

SUBROUTINE TEST_NODIFF(v1)
  USE M_DIFF
  IMPLICIT NONE
  REAL :: v1
  REAL :: v2
  REAL :: result1
  v2 = F(v2)
  result1 = F(v1)
  x = x*result1

CONTAINS
  FUNCTION F(a)
    USE M_DIFF
    IMPLICIT NONE
    REAL :: a
    REAL :: f
    a = x*a
  END FUNCTION F

END SUBROUTINE TEST_NODIFF

!  Differentiation of test in forward (tangent) mode:
!   variations   of useful results: x v1
!   with respect to varying inputs: x v1
!   RW status of diff variables: x:in-out v1:in-out
SUBROUTINE TEST_D(v1, v1d)
  USE M_DIFF
  IMPLICIT NONE
  REAL :: v1
  REAL :: v1d
  REAL :: v2
  REAL :: result1
  v2 = F(v2)
  result1 = F_D(v1, v1d)
  xd = result1*xd
  x = x*result1

CONTAINS
!  Differentiation of f in forward (tangent) mode:
!   variations   of useful results: a
!   with respect to varying inputs: x a
  REAL FUNCTION F_D(a, ad) RESULT (f)
    USE M_DIFF
    IMPLICIT NONE
    REAL :: a
    REAL :: ad
    ad = a*xd + x*ad
    a = x*a
  END FUNCTION F_D

  FUNCTION F(a)
    USE M_DIFF
    IMPLICIT NONE
    REAL :: a
    REAL :: f
    a = x*a
  END FUNCTION F

END SUBROUTINE TEST_D

