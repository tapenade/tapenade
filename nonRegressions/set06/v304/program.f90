module m
  real :: x
end module

subroutine test(v1)
use m

real :: v1
real :: v2

v2 = f(v2)
x = x * f(v1)

contains 
function f(a)
  use m
  real a
  real f
  a = x * a
end function f

end subroutine test
