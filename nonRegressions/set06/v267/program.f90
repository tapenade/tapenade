SUBROUTINE BCEULERWALLADJ(nn, norm3, norm4, z)
  IMPLICIT NONE

  INTEGER(kind=4), INTENT(IN) :: nn

  REAL(kind=8), DIMENSION(:, :, :), POINTER :: norm3
  REAL(kind=8), DIMENSION(:, :, :, :), POINTER :: norm4

  REAL(kind=8), DIMENSION(:, :) :: z

  INTEGER(kind=4) :: i, j, k

  norm3 => norm4(1,:,:,:)

  DO i=-2,2
    DO j=-2,2
     z(i,j) = norm3(j,i,1) * norm3(j,i,2)
    END DO
  END DO

END SUBROUTINE BCEULERWALLADJ
