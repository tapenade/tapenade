subroutine multipl(x)
implicit none
real, dimension(5) :: x
real, dimension(:), allocatable, save :: nbrun
integer i
if (.not. allocated(nbrun))  then
   allocate(nbrun(5))
   nbrun = 2
endif
do i=1,5
  x(i)=x(i)*nbrun(i)
end do
nbrun = nbrun + 1
print*,'   nbrun     ', nbrun
end subroutine multipl

program test
implicit none
real, dimension(:), allocatable :: a
integer i
allocate(a(5))
a(1) = 1
a(2) = 2
a(3) = 3
a(4) = 4
a(5) = 5
do i = 1,5
   print*,'   a     ', a(i)
end do
call multipl(a)
do i = 1,5
   print*,'   a     ', a(i)
end do
call multipl(a)
do i = 1,5
   print*,'   a     ', a(i)
end do


end program test
