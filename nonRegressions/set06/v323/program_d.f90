!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 22 May 2024 15:31
!
MODULE SIMTEST1_D
  IMPLICIT NONE
  TYPE CRTYPE
      REAL*8 :: f, fdfac, thres
      REAL*8, DIMENSION(:), ALLOCATABLE :: cmob
      REAL*8, DIMENSION(:, :), ALLOCATABLE :: cinj
      INTEGER*4 :: ncomp, isoterm, nx, nt
      REAL*8, DIMENSION(:, :), ALLOCATABLE :: isodat
  END TYPE CRTYPE
  TYPE CRTYPE_D
      REAL*8, DIMENSION(:), ALLOCATABLE :: cmob
      REAL*8, DIMENSION(:, :), ALLOCATABLE :: isodat
  END TYPE CRTYPE_D

CONTAINS
!  Differentiation of solvereal in forward (tangent) mode:
!   variations   of useful results: c
!   with respect to varying inputs: *(this.isodat)
!   RW status of diff variables: this.f:(loc) this.fdfac:(loc)
!                this.thres:(loc) this.isodat:(loc) *(this.isodat):in
!                c:out
!   Plus diff mem management of: this.cmob:in this.isodat:in
  SUBROUTINE SOLVEREAL_D(this, thisd, c, cd, indexlim)
    USE DIFFSIZES
!  Hint: ISIZE1OFDrfthis_cmob should be the size of dimension 1 of array *this%cmob
    IMPLICIT NONE
    TYPE(CRTYPE), INTENT(IN) :: this
    TYPE(CRTYPE_D), INTENT(IN) :: thisd
    REAL*8, DIMENSION(0:, :), INTENT(OUT) :: c
    REAL*8, DIMENSION(0:, :), INTENT(OUT) :: cd
    REAL*8, DIMENSION(0:, :), INTENT(IN), OPTIONAL :: indexlim
    INTEGER :: allocstat, j, n
    REAL*8, DIMENSION(:), ALLOCATABLE :: qstat, qnew, qold, cnew, cold
    REAL*8, DIMENSION(:), ALLOCATABLE :: qstatd, qnewd, qoldd, cnewd, &
&   coldd
    INTRINSIC PRESENT
    INTRINSIC INT
    LOGICAL :: result1
    REAL*8, DIMENSION(ISIZE1OFDrfthis_cmob) :: dummyzerodiffd
    ALLOCATE(qstatd(1:this%ncomp), stat=allocstat)
    ALLOCATE(qstat(1:this%ncomp), stat=allocstat)
    ALLOCATE(qnewd(1:this%ncomp), stat=allocstat)
    ALLOCATE(qnew(1:this%ncomp), stat=allocstat)
    ALLOCATE(qoldd(1:this%ncomp), stat=allocstat)
    ALLOCATE(qold(1:this%ncomp), stat=allocstat)
    ALLOCATE(cnewd(1:this%ncomp), stat=allocstat)
    ALLOCATE(cnew(1:this%ncomp), stat=allocstat)
    ALLOCATE(coldd(1:this%ncomp), stat=allocstat)
    ALLOCATE(cold(1:this%ncomp), stat=allocstat)
    c = this%cinj(0:this%nt, 1:this%ncomp)
    dummyzerodiffd = 0.0_8
    qstatd = QCALC_D(this, thisd, this%cmob, dummyzerodiffd, qstat)
    result1 = PRESENT(indexlim)
    IF (result1) THEN
      cd = 0.0_8
space_integration1:DO j=1,this%nx
        cnewd = 0.0_8
        cnew = this%cmob
        qnewd = qstatd
        qnew = qstat
time_integration1:DO n=INT(indexlim(j, 1)),INT(indexlim(j, 2))
          coldd = cnewd
          cold = cnew
          cnewd = cd(n, :)
          cnew = c(n, :)
          qoldd = qnewd
          qold = qnew
          qnewd = QCALC_D(this, thisd, cnew, cnewd, qnew)
          cd(n, :) = cnewd - this%fdfac*(cnewd-coldd+this%f*(qnewd-qoldd&
&           ))
          c(n, :) = cnew - this%fdfac*(cnew-cold+this%f*(qnew-qold))
        END DO time_integration1
      END DO space_integration1
    ELSE
      cd = 0.0_8
    END IF
    IF (ALLOCATED(qstatd)) THEN
      DEALLOCATE(qstatd)
    END IF
    DEALLOCATE(qstat, stat=allocstat)
    IF (ALLOCATED(qnewd)) THEN
      DEALLOCATE(qnewd)
    END IF
    DEALLOCATE(qnew, stat=allocstat)
    IF (ALLOCATED(qoldd)) THEN
      DEALLOCATE(qoldd)
    END IF
    DEALLOCATE(qold, stat=allocstat)
    IF (ALLOCATED(cnewd)) THEN
      DEALLOCATE(cnewd)
    END IF
    DEALLOCATE(cnew, stat=allocstat)
    IF (ALLOCATED(coldd)) THEN
      DEALLOCATE(coldd)
    END IF
    DEALLOCATE(cold, stat=allocstat)
  END SUBROUTINE SOLVEREAL_D

  SUBROUTINE SOLVEREAL(this, c, indexlim)
    IMPLICIT NONE
    TYPE(CRTYPE), INTENT(IN) :: this
    REAL*8, DIMENSION(0:, :), INTENT(OUT) :: c
    REAL*8, DIMENSION(0:, :), INTENT(IN), OPTIONAL :: indexlim
    INTEGER :: allocstat, j, n
    REAL*8, DIMENSION(:), ALLOCATABLE :: qstat, qnew, qold, cnew, cold
    INTRINSIC PRESENT
    INTRINSIC INT
    LOGICAL :: result1
    ALLOCATE(qstat(1:this%ncomp), stat=allocstat)
    ALLOCATE(qnew(1:this%ncomp), stat=allocstat)
    ALLOCATE(qold(1:this%ncomp), stat=allocstat)
    ALLOCATE(cnew(1:this%ncomp), stat=allocstat)
    ALLOCATE(cold(1:this%ncomp), stat=allocstat)
    c = this%cinj(0:this%nt, 1:this%ncomp)
    qstat = QCALC(this, this%cmob)
    result1 = PRESENT(indexlim)
    IF (result1) THEN
space_integration1:DO j=1,this%nx
        cnew = this%cmob
        qnew = qstat
time_integration1:DO n=INT(indexlim(j, 1)),INT(indexlim(j, 2))
          cold = cnew
          cnew = c(n, :)
          qold = qnew
          qnew = QCALC(this, cnew)
          c(n, :) = cnew - this%fdfac*(cnew-cold+this%f*(qnew-qold))
        END DO time_integration1
      END DO space_integration1
    END IF
    DEALLOCATE(qstat, stat=allocstat)
    DEALLOCATE(qnew, stat=allocstat)
    DEALLOCATE(qold, stat=allocstat)
    DEALLOCATE(cnew, stat=allocstat)
    DEALLOCATE(cold, stat=allocstat)
  END SUBROUTINE SOLVEREAL

!  Differentiation of qcalc in forward (tangent) mode:
!   variations   of useful results: qcalc
!   with respect to varying inputs: *(this.isodat) cmob
!   Plus diff mem management of: this.isodat:in
  FUNCTION QCALC_D(this, thisd, cmob, cmobd, qcalc)
    USE DIFFSIZES
!  Hint: ISIZE1OFcmob should be the size of dimension 1 of array cmob
    IMPLICIT NONE
    TYPE(CRTYPE), INTENT(IN) :: this
    TYPE(CRTYPE_D), INTENT(IN) :: thisd
    REAL*8, DIMENSION(:), INTENT(IN) :: cmob
    REAL*8, DIMENSION(:), INTENT(IN) :: cmobd
    INTRINSIC UBOUND
    REAL*8, DIMENSION(UBOUND(cmob, dim=1)) :: qcalc
    REAL*8, DIMENSION(UBOUND(cmob, dim=1)) :: QCALC_D
    REAL*8, DIMENSION(ISIZE1OFcmob) :: temp
    SELECT CASE (this%isoterm)
    CASE (1)
      qcalc_d = cmob*thisd%isodat(:, 1) + this%isodat(:, 1)*cmobd
      qcalc = this%isodat(:, 1)*cmob
    CASE (2)
      temp = this%isodat(:, 1)*cmob/(this%isodat(:, 2)*cmob+1.0)
      qcalc_d = (cmob*thisd%isodat(:, 1)+this%isodat(:, 1)*cmobd-temp*(&
&       cmob*thisd%isodat(:, 2)+this%isodat(:, 2)*cmobd))/(this%isodat(:&
&       , 2)*cmob+1.0)
      qcalc = temp
    CASE DEFAULT
      qcalc_d = 0.0_8
    END SELECT
  END FUNCTION QCALC_D

  FUNCTION QCALC(this, cmob)
    IMPLICIT NONE
    TYPE(CRTYPE), INTENT(IN) :: this
    REAL*8, DIMENSION(:), INTENT(IN) :: cmob
    INTRINSIC UBOUND
    REAL*8, DIMENSION(UBOUND(cmob, dim=1)) :: qcalc
    SELECT CASE (this%isoterm)
    CASE (1)
      qcalc = this%isodat(:, 1)*cmob
    CASE (2)
      qcalc = this%isodat(:, 1)*cmob/(1.0+this%isodat(:, 2)*cmob)
    END SELECT
  END FUNCTION QCALC

END MODULE SIMTEST1_D

