MODULE simtest1
IMPLICIT NONE

TYPE CrType
	REAL*8 ::  F, FDFac, thres        
  REAL*8, DIMENSION(:), ALLOCATABLE :: Cmob         
  REAL*8, DIMENSION(:, :), ALLOCATABLE :: Cinj         
  INTEGER*4 :: Ncomp , isoterm,  Nx  ,  Nt           
  REAL*8, DIMENSION(:, :), ALLOCATABLE :: isodat      
END TYPE CrType

CONTAINS

SUBROUTINE solveReal(this, C, indexLim)
	
  IMPLICIT NONE
  TYPE(CrType)               , INTENT(in)           :: this
  REAL*8, DIMENSION(0 :, :)  , INTENT(out)          :: C
  REAL*8, DIMENSION(0 :, :)  , INTENT(in), OPTIONAL :: indexLim 
  INTEGER                            ::	allocstat, j, n
  REAL(8), DIMENSION(:), ALLOCATABLE :: qStat, qNew, qOld, CNew, COld
	 
  ALLOCATE(qStat(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( qNew(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( qOld(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( CNew(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( COld(1 : this%Ncomp), stat = allocstat)
  
  C     = this%Cinj(0:this%Nt, 1:this%Ncomp)
  qStat = qCalc(this, this%Cmob)      

  IF (PRESENT(indexLim)) THEN
    space_integration1: DO j = 1, this%Nx
		  CNew = this%Cmob 
		  qNew = qStat
      time_integration1: DO n = INT(indexLim(j, 1)), INT(indexLim(j, 2))
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalc(this, CNew)
			  C(n, :) = CNew - this%FDFac*( CNew - COld + this%F*(qNew - qOld) ) 
		  END DO time_integration1
    END DO space_integration1
  ENDIF

  DEALLOCATE(qStat, qNew, qOld, CNew, COld, stat = allocstat)
	    
END SUBROUTINE solveReal

FUNCTION qCalc(this, Cmob)
  TYPE(CrType), INTENT(in)               :: this
  REAL*8, DIMENSION(:), INTENT(in)       :: Cmob
	REAL*8, DIMENSION(UBOUND(Cmob, DIM=1)) :: qCalc

  SELECT CASE (this%isoterm)
	CASE (1)	
		qCalc = this%isodat(:, 1) * Cmob
	CASE (2)	
		qCalc = this%isodat(:, 1) * Cmob / (1.0 + this%isodat(:, 2) * Cmob)
  END SELECT

END FUNCTION qCalc    

END MODULE simtest1
