! from v312 pour tester la differentiation de integer :: request dans msg1
program anneau
 use mpi
 implicit none
 integer, dimension( MPI_STATUS_SIZE ) :: statut
 integer, parameter :: etiquette=100
 integer :: nb_procs,rang, &
 num_proc_precedent,num_proc_suivant,code
 real :: val, valeur
 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 num_proc_suivant=mod(rang+1,nb_procs)
 num_proc_precedent=mod(nb_procs+rang-1,nb_procs)

 val = rang+1000

 if (rang == 0) then
   call msg1(val,valeur, num_proc_precedent,num_proc_suivant,etiquette, code)
 else
   call msg2(val,valeur, num_proc_precedent,num_proc_suivant,etiquette, code)
 end if

 print *,'Moi, proc. ',rang,', j''ai recu ',valeur,' du proc. ',num_proc_precedent

 call MPI_FINALIZE (code)
end program anneau


subroutine msg1(val1, val2, numprocprec, numprocsuiv, etiquette, code)
  use mpi
  implicit none
  integer :: numprocprec, numprocsuiv, code, etiquette,err
  integer, dimension(2) :: req
  integer :: request
  real :: val1, val2
  integer, dimension( MPI_STATUS_SIZE,2 ) :: statut

   call MPI_ISEND (val1,1, MPI_REAL ,numprocsuiv,etiquette, &
      MPI_COMM_WORLD , req(1),code)
   request = req(1)

   call MPI_IRECV (val2,1, MPI_REAL ,numprocprec,etiquette, &
      MPI_COMM_WORLD ,req(2),code)

   call MPI_WAIT(request, statut(1,1), err)
   call MPI_WAIT(req(2), statut(1,2), err)

end subroutine msg1


subroutine msg2(val1, val2, numprocprec, numprocsuiv, etiquette, code)
  use mpi
  implicit none
  integer :: numprocprec, numprocsuiv, code, etiquette,err
  integer :: etiquette1, etiquette2
  integer, dimension(2) :: req
  real :: val1, val2
  integer, dimension( MPI_STATUS_SIZE ) :: statut
   etiquette1 = etiquette
   call MPI_RECV (val2,1, MPI_REAL ,numprocprec,etiquette1, &
      MPI_COMM_WORLD , statut, code)
   etiquette2 = etiquette1
   call MPI_SEND (val1,1, MPI_REAL ,numprocsuiv,etiquette2, &
      MPI_COMM_WORLD , statut ,code)

end subroutine msg2
