!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.12 (r6300M) - 22 Feb 2017 16:51
!
!  Differentiation of msg1 in forward (tangent) mode:
!   variations   of useful results: val2
!   with respect to varying inputs: val1
!   RW status of diff variables: val1:in val2:out
SUBROUTINE MSG1_D(val1, val1d, val2, val2d, numprocprec, numprocsuiv, &
& etiquette, code)
  USE MPI
  IMPLICIT NONE
  INTEGER :: numprocprec, numprocsuiv, code, etiquette, err
  INTEGER, DIMENSION(2) :: req
  INTEGER :: request
  REAL :: val1, val2
  REAL :: val1d, val2d
  INTEGER, DIMENSION(mpi_status_size, 2) :: statut
  CALL TLS_MPI_ISEND(val1, val1d, 1, mpi_real, mpi_real, numprocsuiv, &
&              etiquette, mpi_comm_world, req(1), code)
  request = req(1)
  CALL TLS_MPI_IRECV(val2, val2d, 1, mpi_real, mpi_real, numprocprec, &
&              etiquette, mpi_comm_world, req(2), code)
  CALL TLS_MPI_WAIT(request, statut(1, 1), err)
  CALL TLS_MPI_WAIT(req(2), statut(1, 2), err)
END SUBROUTINE MSG1_D

!  Differentiation of msg2 in forward (tangent) mode:
!   variations   of useful results: val2
!   with respect to varying inputs: val1
!   RW status of diff variables: val1:in val2:out
SUBROUTINE MSG2_D(val1, val1d, val2, val2d, numprocprec, numprocsuiv, &
& etiquette, code)
  USE MPI
  IMPLICIT NONE
  INTEGER :: numprocprec, numprocsuiv, code, etiquette, err
  INTEGER :: etiquette1, etiquette2
  INTEGER, DIMENSION(2) :: req
  REAL :: val1, val2
  REAL :: val1d, val2d
  INTEGER, DIMENSION(mpi_status_size) :: statut
  etiquette1 = etiquette
  CALL TLS_MPI_RECV(val2, val2d, 1, mpi_real, mpi_real, numprocprec, &
&             etiquette1, mpi_comm_world, statut, code)
  etiquette2 = etiquette1
  CALL TLS_MPI_SEND(val1, val1d, 1, mpi_real, mpi_real, numprocsuiv, &
&             etiquette2, mpi_comm_world, statut, code)
END SUBROUTINE MSG2_D

