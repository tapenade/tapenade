real function sumarray(t)
real, dimension(5), intent(in) :: t
sumarray = t(1) + t(2) + t(3) + t(4) + t(5)
end function sumarray

subroutine test(a, result, n)
real, dimension(10), intent(in) :: a
real, intent(out) :: result
integer :: n
result = sumarray(a(n:n+4))
end subroutine test
