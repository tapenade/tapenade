! from examplesF90 vmp14

module parameters
   public
   integer, parameter :: a = 3
   integer, parameter :: b = 4
   integer, parameter :: c = 5
end module parameters

subroutine solve(U)
   use parameters
   implicit none
   double precision, intent(inout) :: U(a,b,c)
   U = F(U)

   CONTAINS

   function F(U1)
       use parameters
       implicit none
       double precision, intent(in) :: U1(a,b,c)
       double precision :: F(a,b,c)
       F(1,:,:) = U1(2,:,:)
   end function F

end subroutine solve
