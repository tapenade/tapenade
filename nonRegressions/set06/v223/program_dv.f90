!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
!	Modules - based on Tony's 'blah_modu.f90'
MODULE GENERAL_DIFFV
  USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
  IMPLICIT NONE
  SAVE 
! named constants for single, double and integer precision reals:
  INTEGER, PARAMETER :: sp=KIND(1.0), dp=KIND(1.0d0), in=KIND(1)
! named constants for 4 byte integers and true:
  INTEGER, PARAMETER :: i4b=SELECTED_INT_KIND(9), lgt=KIND(.true.)
END MODULE GENERAL_DIFFV

MODULE GEOMETRY_DIFFV
  USE GENERAL_DIFFV, ONLY : dp
  USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
  IMPLICIT NONE
  SAVE 
! max points
  INTEGER, PARAMETER :: mmax=1200
! # gridpoints
  INTEGER, PARAMETER :: nmax=1000
! last grounded grid point
  INTEGER, PARAMETER :: nedge=956
! type to be read in
  INTEGER, PARAMETER :: infile=12
  REAL(kind=dp), PARAMETER :: thickground=648.797
  REAL(kind=dp), PARAMETER :: heightground=83.354
  REAL(kind=dp), PARAMETER :: bedground=-565.443
  REAL(kind=dp), PARAMETER :: xlength=346066.390
  INTEGER :: iedge, nbasin
  REAL(kind=dp), DIMENSION(mmax) :: bed, bedcheck, depth0, height, &
& height0, thickness, thickness0, oro, width, width0, xx, xbed
!read in data
  REAL(kind=dp), DIMENSION(mmax) :: aas, betas, beta0
  REAL(kind=dp) :: dl
  CHARACTER(len=45) :: file(0:6)
  CHARACTER :: ff1*7, ff2*8, ff3*5, ff4*5, ff5*12

CONTAINS
  SUBROUTINE READ_DATA()
    IMPLICIT NONE
  END SUBROUTINE READ_DATA

END MODULE GEOMETRY_DIFFV

MODULE VELOCITY_DIFFV
  USE GENERAL_DIFFV, ONLY : dp
  USE GEOMETRY_DIFFV, ONLY : mmax, nedge
  USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
  IMPLICIT NONE
  SAVE 
  REAL(kind=dp), PARAMETER :: tolr=1.0d-5
  REAL(kind=dp), DIMENSION(mmax) :: us, us0, us1, ud, ud1, nu1
  REAL(kind=dp), DIMENSION(nedge) :: b1, b2, b3
END MODULE VELOCITY_DIFFV

MODULE PHYSCON_DIFFV
  USE GENERAL_DIFFV, ONLY : dp
  USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
  IMPLICIT NONE
  SAVE 
! s
  REAL(kind=dp), PARAMETER :: scyr=31556926.0
  REAL(kind=dp), PARAMETER :: pi=3.1415926535897d0
!or 917.0        		! kg m^{-3}
  REAL(kind=dp), PARAMETER :: rhoi=910.0
!or 1000.0d0				! kg m^{-3}
  REAL(kind=dp), PARAMETER :: rhow=1028.0d0
! m s^{-2}
  REAL(kind=dp), PARAMETER :: g=9.81
! rate factocr (A from Glen's flow law)
  REAL(kind=dp), PARAMETER :: ga=5.60d-17
!
  REAL(kind=dp), PARAMETER :: fsoft=1
! exponent in Glen's flow law
  INTEGER, SAVE :: n=3
  REAL(kind=dp), SAVE :: rogr=rhoi*g
  REAL(kind=dp), SAVE :: rogrw=rhow*g
  REAL(kind=dp), PARAMETER :: epsilon=0.0001
END MODULE PHYSCON_DIFFV

MODULE CONTROLMETH_DIFFV
  USE GENERAL_DIFFV, ONLY : dp, i4b
  USE GEOMETRY_DIFFV, ONLY : mmax, nedge
  USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
  IMPLICIT NONE
  SAVE 
  INTEGER(kind=i4b) :: iter_cont
  REAL(kind=dp) :: jtot, jtot_old, fret_cont
! weighting factor (observational error)
  REAL(kind=dp), PARAMETER :: sigma=1
! deltaJ/J < ftol
  REAL(kind=dp), PARAMETER :: ftol_cont=1.0d-5
  REAL(kind=dp), DIMENSION(nedge) :: ppall, djdq, djdp, rr, lambda, &
& sqrtbeta, hmatr
END MODULE CONTROLMETH_DIFFV

