
!	Modules - based on Tony's 'blah_modu.f90'
MODULE general

	implicit none; save 
! named constants for single, double and integer precision reals:
	integer, parameter :: sp = kind(1.0), &
						  dp = kind(1.0d0), &
						  in = kind(1)
! named constants for 4 byte integers and true:
	integer, parameter :: i4b = selected_int_kind(9), &
						  lgt = kind(.true.)

END MODULE general

MODULE geometry
	
	use general, only : dp

	implicit none; save 
	integer, parameter :: mmax=1200	  ! max points
	integer, parameter :: nmax=1000	  ! # gridpoints
	integer, parameter :: nedge = 956 ! last grounded grid point
	integer, parameter :: InFile = 12 ! type to be read in

	real(kind=dp), parameter :: thickground=648.797
	real(kind=dp), parameter :: heightground=83.354
	real(kind=dp), parameter :: bedground=-565.443
	real(kind=dp), parameter :: xlength=346066.390

	integer :: iedge, nbasin
	
	real (kind = dp), dimension(1:mmax) :: bed,bedcheck,depth0,height,height0,thickness,thickness0, &
										   oro,width,width0,xx,xbed
	real (kind = dp), dimension(1:mmax) :: aas,betas,beta0 !read in data
	real (kind = dp) :: dl
	character(len=45) :: file(0:6) 
	character :: ff1*7,ff2*8,ff3*5,ff4*5,ff5*12

	contains

	SUBROUTINE read_data

	END SUBROUTINE read_data

END MODULE geometry

MODULE velocity

	use general, only : dp
	use geometry, only: mmax, nedge

	implicit none; save 

	real (kind = dp), parameter :: tolr = 1.0d-5
	real (kind = dp), dimension(1:mmax) :: us, us0, us1, ud, ud1, nu1
	real (kind = dp), dimension(1:nedge) :: b1, b2, b3
END MODULE velocity

MODULE physcon

	use general, only : dp

	implicit none; save 
  
	real (kind = dp), parameter :: scyr = 31556926.0  		! s
	real (kind = dp), parameter :: pi = 3.1415926535897d0

	real (kind = dp), parameter :: rhoi = 910.0 !or 917.0        		! kg m^{-3}
	real (kind = dp), parameter :: rhow = 1028.0d0 !or 1000.0d0				! kg m^{-3}
	real (kind = dp), parameter :: g = 9.81        		! m s^{-2}

	real (kind = dp), parameter :: ga = 5.60D-17 ! rate factocr (A from Glen's flow law)
	real (kind = dp), parameter :: fsoft = 1 !

	integer, save :: n = 3 ! exponent in Glen's flow law
	real (kind = dp), save :: rogr = rhoi * g
	real (kind = dp), save :: rogrw = rhow * g
	real (kind = dp), parameter :: Epsilon = 0.0001



END MODULE physcon


MODULE controlmeth
	
	use general, only : dp,i4b
	use geometry, only: mmax,nedge

	implicit none; save 

	integer (kind = i4b) :: iter_cont
	real (kind = dp) :: jtot, jtot_old,fret_cont
	real (kind = dp), parameter :: sigma=1 ! weighting factor (observational error)
	real (kind = dp), parameter :: ftol_cont = 1.0d-5 ! deltaJ/J < ftol
	real (kind = dp), dimension(1:nedge) :: ppall,djdq,djdp,rr,lambda,sqrtbeta,Hmatr
END MODULE controlmeth
