       module precision
       implicit none
       integer(kind=4), private :: dummyInt
       real(kind=8), private :: dummyReal
       integer, parameter :: intType      = kind(dummyInt)
       integer, parameter :: realType     = kind(dummyReal)
       integer(kind=intType) :: nw
       end module precision


       module flowVarRefState
       use precision
       implicit none
       real(kind=realType), dimension(:), allocatable :: wInf
       end module flowVarRefState


       subroutine residualAdj(wAdj, dwAdj)
       use flowVarRefState
       implicit none
       real(kind=realType), dimension(2,2,2,nw), intent(inout) :: wAdj
       real(kind=realType), dimension(nw), intent(out) :: dwAdj
       integer(kind=intType) :: l
       do l=1,nw
         dwAdj(l) = wInf(l)
         wAdj(0,0,0,l) = wInf(l)
       enddo
       end subroutine residualAdj
