MODULE INTERVAL_ARITHMETICS
       TYPE INTERVAL
              REAL LOWER, UPPER
       END TYPE INTERVAL

       TYPE (INTERVAL) :: X,Y

END MODULE INTERVAL_ARITHMETICS

module test 
contains
function f(t)
    use INTERVAL_ARITHMETICS
    TYPE (INTERVAL) :: f, t
    t%lower = t%lower * 2 + x%lower
    t%upper = t%upper * 2 + y%upper + f_cb(t)
    f = t
end

function f_cd(t)
    real :: f, t
    f = t * t

end function f_cd
end module test
