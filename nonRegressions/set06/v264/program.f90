SUBROUTINE preSolve(this, indexLim)
	
  IMPLICIT NONE

  TYPE CrType
	REAL*8 ::  F, FDFac, thres   
        REAL*8,    Cmob    
        INTEGER*4 :: Nx ,  Nt          
  END TYPE CrType

  TYPE(CrType)              , INTENT(in)  :: this
  REAL*8 , INTENT(out) :: indexLim 

  INTEGER*4     :: j, n
  REAL*8 :: qStat, qNew, CNew, C

  indexLim = 0

  space_integration: DO j = 1, this%Nx
		CNew = this%Cmob 
		qNew = qStat

    indexLim = 0
    DO n = 0, this%Nt
      IF ( (C - this%Cmob) .GT. this%thres) THEN
        indexLim = n-1
        EXIT
      ENDIF
    ENDDO
    indexLim = this%Nt
    DO n = this%Nt, 0, -1
      IF ((C - this%Cmob) .GT. this%thres) THEN
        indexLim = n+1
        EXIT
      ENDIF
    ENDDO	
  END DO space_integration

END SUBROUTINE preSolve

