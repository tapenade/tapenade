!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) - 24 Feb 2022 12:44
!
MODULE COMPHYS_DIFF
  USE PRECIS
  IMPLICIT NONE
  REAL(iprec), SAVE :: rr=8314.41
  REAL(iprec), SAVE :: rrd=0.0
  REAL(iprec), SAVE :: am=28.9645
  REAL(iprec), SAVE :: cpa=1005.7
  REAL(iprec), SAVE :: tka=0.0259
  REAL(iprec), SAVE :: vm=18.01528
  REAL(iprec), SAVE :: vmd=0.0
  REAL(iprec), SAVE :: rhow, drhwt, d2rhwtt
  REAL(iprec), SAVE :: rhowd
  REAL(iprec), SAVE :: cpw, dcpwt, tkw, viscw, dviswt
END MODULE COMPHYS_DIFF

MODULE COMGDL_DIFF
  USE PRECIS
  IMPLICIT NONE
  REAL(iprec), SAVE :: tt, pc, pg, uu, vv
  REAL(iprec), SAVE :: ttd, pcd
  REAL(iprec), SAVE :: ss, dssc, dsst, d2sscc, d2sstt, d2ssct, watcon
  REAL(iprec), SAVE :: ssd, dsscd, dsstd
END MODULE COMGDL_DIFF

!  Differentiation of satur_c in forward (tangent) mode:
!   variations   of useful results: ss dssc dsst
!   with respect to varying inputs: tt pc dssc dsst rr vm rhow
!   RW status of diff variables: tt:in pc:in ss:out dssc:in-out
!                dsst:in-out rr:in vm:in rhow:in
SUBROUTINE SATUR_C_D(w1, c1, irhs)
  USE PRECIS
  USE COMGDL_DIFF
  USE COMPHYS_DIFF
  IMPLICIT NONE
  INTEGER :: irhs
  REAL(iprec) :: w1, c1
  REAL(iprec) :: ttcrit, tamb, tem
  REAL(iprec) :: tt1, mt, dtt1tt, d2tt1tt, dmttt, d2mttt
  REAL(iprec) :: amrr, hr, dhrtt, dhrcc, d2hrtt, d2hrcc, d2hrct
  REAL(iprec) :: amrrd, hrd
  REAL(iprec) :: ssmh, lnh, xfun, dxfuntt, dxfuncc
  REAL(iprec) :: mt1, dm1ttt, d2m1ttt, fcm, dfcmtt, d2fcmtt
  INTRINSIC EXP
  INTRINSIC LOG
  REAL(iprec) :: temp
  ttcrit = 647.3
  tamb = 25.0
  mt1 = 1.04 - tt1/(24.+tt1)
  IF (tt .LT. ttcrit) THEN
    temp = rr*tt*rhow
    amrrd = (vmd-vm*(rhow*(tt*rrd+rr*ttd)+rr*tt*rhowd)/temp)/temp
    amrr = vm/temp
    hrd = -(EXP(-(pc*amrr))*(amrr*pcd+pc*amrrd))
    hr = EXP(-(pc*amrr))
  ELSE
    hrd = 0.0
  END IF
  temp = 1.0/mt
  IF (hr .LE. 0.0 .AND. (temp .EQ. 0.0 .OR. temp .NE. INT(temp))) THEN
    ssd = 0.0
  ELSE
    ssd = temp*hr**(temp-1)*hrd
  END IF
  ss = hr**temp
  lnh = LOG(hr)
  IF (ss .LT. 1.e-10) THEN
    ss = 1.e-10
    dsst = 0.
    dssc = 0.
    ssd = 0.0
    dsscd = 0.0
    dsstd = 0.0
  END IF
  IF (irhs .EQ. 0) THEN
    IF (tt .LT. ttcrit) THEN
      d2tt1tt = dtt1tt/(tem+10.)
      d2m1ttt = dm1ttt*(2.*dtt1tt/(24.+tt1)-d2tt1tt/dtt1tt)
      d2fcmtt = 0.
      d2mttt = d2m1ttt*fcm + 2.*dm1ttt*dfcmtt + mt1*d2fcmtt
      d2hrtt = dhrtt*(dhrtt/hr-1./tt-drhwt/rhow) + amrr*hr*pc*(1./tt**2+&
&       drhwt**2/rhow**2-d2rhwtt/rhow)
      d2hrct = dhrtt*(1./pc+dhrcc/hr)
    END IF
  END IF
END SUBROUTINE SATUR_C_D

SUBROUTINE SATUR_C_NODIFF(w1, c1, irhs)
  USE PRECIS
  USE COMGDL_DIFF
  USE COMPHYS_DIFF
  IMPLICIT NONE
  INTEGER :: irhs
  REAL(iprec) :: w1, c1
  REAL(iprec) :: ttcrit, tamb, tem
  REAL(iprec) :: tt1, mt, dtt1tt, d2tt1tt, dmttt, d2mttt
  REAL(iprec) :: amrr, hr, dhrtt, dhrcc, d2hrtt, d2hrcc, d2hrct
  REAL(iprec) :: ssmh, lnh, xfun, dxfuntt, dxfuncc
  REAL(iprec) :: mt1, dm1ttt, d2m1ttt, fcm, dfcmtt, d2fcmtt
  INTRINSIC EXP
  INTRINSIC LOG
  ttcrit = 647.3
  tamb = 25.0
  mt1 = 1.04 - tt1/(24.+tt1)
  IF (tt .LT. ttcrit) THEN
    amrr = vm/(rr*tt*rhow)
    hr = EXP(-(pc*amrr))
  END IF
  ss = hr**(1./mt)
  lnh = LOG(hr)
  IF (ss .LT. 1.e-10) THEN
    ss = 1.e-10
    dsst = 0.
    dssc = 0.
  END IF
  IF (irhs .EQ. 0) THEN
    IF (tt .LT. ttcrit) THEN
      d2tt1tt = dtt1tt/(tem+10.)
      d2m1ttt = dm1ttt*(2.*dtt1tt/(24.+tt1)-d2tt1tt/dtt1tt)
      d2fcmtt = 0.
      d2mttt = d2m1ttt*fcm + 2.*dm1ttt*dfcmtt + mt1*d2fcmtt
      d2hrtt = dhrtt*(dhrtt/hr-1./tt-drhwt/rhow) + amrr*hr*pc*(1./tt**2+&
&       drhwt**2/rhow**2-d2rhwtt/rhow)
      d2hrct = dhrtt*(1./pc+dhrcc/hr)
    END IF
  END IF
END SUBROUTINE SATUR_C_NODIFF

