module precis
  integer , parameter :: iprec=selected_real_kind(10,50)
end module precis

module comphys 
use precis 
real(iprec), save :: RR=8314.41 , AM=28.9645 , CPA=1005.7 , TKA=0.0259  
real(iprec), save :: VM=18.01528
real(iprec), save :: RHOW, DRHWT, D2RHWTT
real(iprec), save :: CPW, DCPWT, TKW, VISCW, DVISWT

end module comphys 

module comgdl
use precis
   real(iprec), save :: tt, pc, pg, uu, vv
   real(iprec), save :: SS, DSSC, DSST, D2SSCC, D2SSTT, D2SSCT, WATCON
end module comgdl

SUBROUTINE SATUR_C(W1, C1, IRHS)
  use precis
  use comgdl
  use comphys
  IMPLICIT NONE
  integer :: irhs
  real(iprec) :: w1,c1
  real(iprec) :: ttcrit, tamb, tem                                  
  real(iprec) :: tt1, mt, dtt1tt, d2tt1tt, dmttt, d2mttt 
  real(iprec) :: amrr, hr, dhrtt, dhrcc , d2hrtt, d2hrcc, d2hrct  
  real(iprec) :: ssmh, lnh, xfun, dxfuntt, dxfuncc 
  real(iprec) :: mt1, dm1ttt, d2m1ttt, fcm, dfcmtt, d2fcmtt 

  TTcrit= 647.3  ; Tamb= 25.0
  mt1 = 1.04 - (tt1/(24.+tt1))
  if(tt.lt.ttcrit) then
     amrr = vm / (rr*tt*rhow) 
     hr = exp(- pc*amrr)
  endif
  ss = hr ** (1./mt) 
  lnh  = log(hr)
  IF (SS.LT.1.E-10) THEN                                                 
     SS=1.E-10 ; DSST=0. ; DSSC=0.
  ENDIF
  IF (IRHS.EQ.0) THEN                                             
     if(tt.lt.ttcrit) then
        d2tt1tt = dtt1tt / (tem+10.)
        d2m1ttt = dm1ttt * (2.*dtt1tt/(24.+tt1) - d2tt1tt/dtt1tt)  
        d2fcmtt = 0. 
        d2mttt  = d2m1ttt*fcm + 2.*dm1ttt*dfcmtt + mt1*d2fcmtt
        d2hrtt  = dhrtt*(dhrtt/hr-1./tt-drhwt/rhow) + &
             amrr*hr*pc*(1./(tt**2)+(drhwt**2)/(rhow**2)-d2rhwtt/rhow)
        d2hrct = dhrtt * (1./pc+dhrcc/hr)
     endif
  ENDIF
END subroutine SATUR_C
