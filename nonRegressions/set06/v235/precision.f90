       module precision
       implicit none

       integer(kind=4), private :: dummyInt
       integer, parameter       :: sizeOfInteger = 4

       real(kind=8), private :: dummyReal
       integer, parameter    :: sizeOfReal = 8
       real(kind=8), private :: dummyCGNSReal
       integer(kind=1), private :: dummyPor
       real, private :: dummyCGNSPer
             integer, parameter :: intType      = kind(dummyInt)
             integer, parameter :: porType      = kind(dummyPor)
             integer, parameter :: realType     = kind(dummyReal)
       end module precision
