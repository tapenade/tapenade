       module block
       use constants
       implicit none

       type BCDataType
         real(kind=realType), dimension(:,:,:), pointer :: norm
       end type BCDataType

       end module block


       module blockPointers
       use block
       implicit none

       type(BCDataType),      dimension(:), pointer :: BCData

       end module blockPointers
