       module constants
       use precision
       implicit none

       integer, parameter :: irho    =  1  ! Density
       integer, parameter :: itu1    =  6  ! Turbulent kinetic energy,
                                           ! SA viscosity

       real(kind=realType), parameter :: zero  = 0.0
       real(kind=realType), parameter :: half   = 0.5

       end module constants
