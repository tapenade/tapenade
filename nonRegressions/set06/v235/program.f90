       subroutine residualAdj(wAdj)
       use inputDiscretization
       use flowVarRefState
       implicit none

       real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                   intent(inout) :: wAdj

       real(kind=realType), dimension(-2:2,-2:2,-2:2) :: pAdj

       call computePressureAdj(wAdj, pAdj)

       end subroutine residualAdj
