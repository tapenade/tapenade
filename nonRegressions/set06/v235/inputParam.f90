       module accuracy
       use precision
       implicit none

       integer(kind=intType), parameter :: firstOrder  = 1, &
                                           secondOrder = 2, &
                                           thirdOrder  = 3
       end module accuracy


       module inputDiscretization
       use accuracy
       implicit none

       integer(kind=intType), parameter :: dissScalar = 1,  &
                                           dissMatrix = 2,  &
                                           dissCusp   = 3,  &
                                           upwind     = 9

       end module inputDiscretization


       module inputPhysics
       use precision
       implicit none

       integer(kind=intType), parameter :: EulerEquations = 1,  &
                                           NSEquations    = 2,  &
                                           RANSEquations  = 3

       end module inputPhysics

