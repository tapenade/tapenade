       subroutine computePressureAdj(wAdj, pAdj)
       use flowVarRefState
       use inputPhysics
       implicit none
       real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                      intent(in) :: wAdj
       real(kind=realType), dimension(-2:2,-2:2,-2:2) :: pAdj

       pAdj(1,1,1) = irho*(wAdj(1,1,1,irho)    &
                           - half*wAdj(1,1,1,irho)*irho) &
                           + irho*wAdj(1,1,1,irho)*wAdj(1,1,1,itu1)

       end subroutine computePressureAdj
