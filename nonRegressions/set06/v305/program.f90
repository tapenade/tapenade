module m
  real :: x
end module

module n
 contains
 function g(b, op)
  use m
  real b
  real g
  real, optional :: op
  b = b * b
 end function g
end module


subroutine test(v1)
use m

real :: v1
real :: v2

v2 = f(v2)
x = x * f(v1)

contains 
function f(a)
  use n
  real a
  real f
  a = a * g(a)
end function f

end subroutine test
