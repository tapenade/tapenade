module MM

  type tt0
     real :: y
     integer :: j
  end type tt0

  type ttt0
     real :: x
     integer :: i
  end type ttt0

  type ttt1
     real :: truc
     type (tt0) :: chose
     type (ttt0) , dimension(:,:,:), pointer :: tab
     integer :: iii
  end type ttt1

contains

  subroutine top(A,B)
    type(ttt1) A,B

    A = B
  end subroutine top

end module MM

program main
use mm
implicit none
type (ttt1) a,b
b%truc = 1.0
b%chose%y = 2.0
call top(a,b)
write (*,*) a%truc, a%chose%y
end
