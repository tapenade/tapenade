!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 28 Oct 2024 15:32
!
MODULE MM
  IMPLICIT NONE
  TYPE TT0
      REAL :: y
      INTEGER :: j
  END TYPE TT0
  TYPE TTT0
      REAL :: x
      INTEGER :: i
  END TYPE TTT0
  TYPE TTT1
      REAL :: truc
      TYPE(TT0) :: chose
      TYPE(TTT0), DIMENSION(:, :, :), POINTER :: tab
      INTEGER :: iii
  END TYPE TTT1

CONTAINS
  SUBROUTINE TOP(a, b)
    IMPLICIT NONE
!     a.tab points to *(a.tab) or Undef
!     b.tab points to *(b.tab) or Undef
    TYPE(TTT1) :: a, b
    a = b
!     a.tab points to *(b.tab) or Undef
  END SUBROUTINE TOP

END MODULE MM

PROGRAM MAIN
!     a.tab points to Undef
!     b.tab points to Undef
  USE MM
  IMPLICIT NONE
  TYPE(TTT1) :: a, b
  b%truc = 1.0
  b%chose%y = 2.0
  CALL TOP(a, b)
  WRITE(*, *) a%truc, a%chose%y
END PROGRAM MAIN

