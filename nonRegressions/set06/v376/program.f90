! tapenade -O /tmp -o p -vars v2 -outvars f program.f90
! ou bien
! tapenade -O /tmp -o p -vars "v2" -outvars "v3" pr2.f90

program testk
 implicit none
 include 'mpif.h'
 integer, dimension( MPI_STATUS_SIZE ) :: statut
 integer, parameter :: etiquette=100
 integer :: nb_procs,rang, code
 real :: x,y,z,b,f
 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 x = 0.0
 y = 4.0
 z = 2.0
 b = 7.0
 f = 99.0

 call test(x, y, z, b, rang, code, etiquette, statut)

 call MPI_FINALIZE (code)
end program testk


subroutine test(v1,v2,v3,v4, rang, code, etiquette, statut)
include 'mpif.h'

real :: v1,v2,v3,v4, x,y
integer :: rang, code, etiquette, etiquette1
integer, dimension(MPI_STATUS_SIZE) :: statut

f = 999
etiquette1 = etiquette


 if (rang == 0) then
      x = 7.0
      v1 = x * v2
      ! $AD CHANNEL channel1
      call MPI_SEND (v1, 1, MPI_REAL , 1, etiquette, &
         MPI_COMM_WORLD ,code)
       ! $AD CHANNEL channel1
       call MPI_SEND (v4, 1, MPI_REAL , 1, etiquette1, &
         MPI_COMM_WORLD ,code)
 else
      v3 = 0
      ! $AD CHANNEL channel1
      call MPI_RECV (v3,1, MPI_REAL , 0, etiquette, &
            MPI_COMM_WORLD ,code, statut)
      ! $AD CHANNEL channel1
      call MPI_RECV (y,1, MPI_REAL , 0, etiquette1, &
            MPI_COMM_WORLD ,code, statut)
 end if

 print *,'v3 ', v3 , ', proc ' , rang

 call MPI_REDUCE(v3,f,1,MPI_REAL,MPI_SUM,0,MPI_COMM_WORLD,ierr)

 print *,'Resultat ', f , ', proc ' , rang


end subroutine
