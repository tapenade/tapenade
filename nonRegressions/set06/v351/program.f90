module m

     TYPE vector
        REAL(KIND=8), DIMENSION(:), POINTER :: dat => NULL()
     END TYPE vector

     INTERFACE MINVAL
        MODULE PROCEDURE d1minval
        MODULE PROCEDURE d2minval
     END INTERFACE MINVAL

contains

  function d1minval(t)
    type(vector) :: t
    real d1minval
    d1minval = minval(t%dat)
  end function d1minval

  function d2minval(t)
    type(vector), dimension(:) :: t
    real d2minval
    d2minval = minval(t(1)%dat)
  end function d2minval

end module m
