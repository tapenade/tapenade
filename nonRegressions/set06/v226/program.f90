MODULE solpcg

CONTAINS

   SUBROUTINE sol_pcg( kindic,gcr,rr)
      INTEGER, INTENT( inout ) ::   kindic
      REAL, DIMENSION(100) :: gcr
      REAL :: rr
      INTEGER :: ncut
      real :: x
      save x
      namelist/n/ rr, gcr
      rr = SUM(  gcr(:) * gcr(:)  )
      IF(rr > 1.e+20 ) kindic = -2
      gcr(:) = gcr(:)*rr
    END SUBROUTINE sol_pcg

END MODULE solpcg
