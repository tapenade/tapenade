module control_point
	implicit none  

	type cpoint
		real*8 :: x
                real*8 :: y  
        end type cpoint
end module control_point

module commondata
  use control_point
	implicit none
	save
    
  integer :: p 	
  integer :: control
  integer :: m 	
	type(cpoint) :: pts, curve_pts
  real*8, dimension (:), allocatable ::U, N, x, y
  type(cpoint), dimension (:), allocatable :: cpts
end module commondata

subroutine BSPLINE (Resolution , MNdeEff, XNode, YNode, BSPLNO, NURBSR)
  use commondata
  implicit none
  
  integer :: Resolution, MNdeEff, nbspl
  integer :: bsplno
  real*8, dimension (resolution) :: XNode, YNode
  integer :: i, j, nurbsr
  
  allocate (cpts(0:control)) !! <- on teste le allocate 

  do i = 0, control
     cpts(i) = pts
  enddo

  do i = 2, resolution-1
        xnode(i) = curve_pts%x
        ynode(i) = curve_pts%y
  enddo

  ynode(1) = cpts(0)%y
  xnode(1) = cpts(control)%x

return 
end

