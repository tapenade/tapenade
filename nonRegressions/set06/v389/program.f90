module M1
   implicit none
   type, public:: TT
      REAL*8 :: a,b
   end type TT
end module M1

module M2
 use M1
 contains
  subroutine S1
    contains
      subroutine S2(var)
         type(TT) :: var
         var%a = var%a * 2
      end subroutine S2
  endsubroutine S1
end
