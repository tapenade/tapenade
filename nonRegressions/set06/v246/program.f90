 function test(veryveryverylongname, MASK)
   integer, parameter :: r8 = 8
   real (r8), dimension(:,:,:), intent(in) :: &
      veryveryverylongname 
   !! ne pas couper la ligne au milieu de INTENT
   real (r8), dimension(size(veryveryverylongname,dim=1), &
                        size(veryveryverylongname,dim=2), &
                        size(veryveryverylongname,dim=3)), intent(in), &
                 optional :: &
      MASK

 end function test
