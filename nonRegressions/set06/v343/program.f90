module m1
implicit none
integer, parameter, public :: kind=8
real(kind), parameter :: pi = 3.14159265358979
real(kind), parameter :: e = 2.7

contains 

subroutine foo()
 implicit none
 print *,pi
end subroutine 

end module 

module m2
 use m1
 implicit none
 public
 real, private :: r
 real, parameter :: zero = 0.0
end module 

module m3
 use m2, only :pi
 use m2, only :zero
 implicit none
 real mm3
end module

program p 
  use m3
  implicit none
  print *,'pi ', pi
  print *,'zero  ', zero
end program
