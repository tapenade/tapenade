!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
MODULE M1
  IMPLICIT NONE
  INTEGER, PARAMETER, PUBLIC :: kind=8
  REAL(kind), PARAMETER :: pi=3.14159265358979
  REAL(kind), PARAMETER :: e=2.7

CONTAINS
  SUBROUTINE FOO()
    IMPLICIT NONE
    PRINT*, pi
  END SUBROUTINE FOO

END MODULE M1

MODULE M2
  USE M1
  IMPLICIT NONE
  PUBLIC 
  REAL, PRIVATE :: r
  REAL, PARAMETER :: zero=0.0
END MODULE M2

MODULE M3
  USE M2, ONLY : pi
  USE M2, ONLY : zero
  IMPLICIT NONE
  REAL :: mm3
END MODULE M3

PROGRAM P
  USE M3
  IMPLICIT NONE
  PRINT*, 'pi ', pi
  PRINT*, 'zero  ', zero
END PROGRAM P

