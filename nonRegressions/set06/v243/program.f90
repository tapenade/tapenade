MODULE definition
  INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(10,50)
END MODULE definition

MODULE rksuite_90
  USE definition
  IMPLICIT NONE
  PUBLIC :: rk_comm_real_1d
  TYPE rk_comm_real_1d
    REAL(kind=wp), DIMENSION(:), POINTER :: thresh
    REAL(kind=wp), DIMENSION(:), POINTER :: weights
    REAL(kind=wp), DIMENSION(:), POINTER :: v1 
    REAL(kind=wp), DIMENSION(:), POINTER :: v2
    REAL(kind=wp), DIMENSION(:), POINTER :: v3  
    REAL(kind=wp), DIMENSION(:,:), POINTER :: stages 
    REAL(kind=wp), DIMENSION(:,:), POINTER :: ge_stages
    REAL(kind=wp), DIMENSION(:,:), POINTER :: p 
  END TYPE rk_comm_real_1d
CONTAINS

  SUBROUTINE collect_garbage_r1(comm)
    TYPE(rk_comm_real_1d) :: comm
    IF (ASSOCIATED(comm%thresh)) THEN
      DEALLOCATE(comm%thresh); NULLIFY(comm%thresh)
    END IF
    IF (ASSOCIATED(comm%p,comm%stages(:,1:2))) THEN
      NULLIFY(comm%p)
    END IF
    IF (ASSOCIATED(comm%ge_stages,comm%stages)) THEN
      DEALLOCATE(comm%stages); NULLIFY(comm%stages); NULLIFY(comm%ge_stages);
      NULLIFY(comm%v1,comm%v2,comm%v3)
    ELSE IF (ASSOCIATED(comm%ge_stages)) THEN
      DEALLOCATE(comm%ge_stages); NULLIFY(comm%ge_stages)
    END IF
  END SUBROUTINE collect_garbage_r1

END module rksuite_90
