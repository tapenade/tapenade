   elemental function dbeasleyspringermoro( u ) result(bsm)
        implicit none
        double precision, intent(in) :: u
        double precision :: bsm
        double precision :: x, r
        x  = u - 0.5
        if( abs(x) < 0.42 ) then
            bsm = cos( x )
        else
            if( x > 0.0 ) then
                bsm = sin( log(-log(1.0 - u)) )
            else
                bsm = -sin( log(-log(u)) )
            end if
        end if
    end function dbeasleyspringermoro



subroutine dbsm( M, N, A )
    implicit none
    integer, intent(in) :: M, N
    double precision, intent(inout) :: A(M,N)
    double precision ::dbeasleyspringermoro

    A = dbeasleyspringermoro(A)
end subroutine dbsm
