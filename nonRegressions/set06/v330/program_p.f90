!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
! stupid magic file number
MODULE M
  IMPLICIT NONE

CONTAINS
  SUBROUTINE FOO(p, x, y)
    IMPLICIT NONE
    REAL, DIMENSION(:) :: p, x, y
    PRINT*, p, x
    y = p*x
  END SUBROUTINE FOO

END MODULE M

PROGRAM P
  IMPLICIT NONE
  CALL HEAD()
END PROGRAM P

SUBROUTINE HEAD()
  USE M
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=3
  REAL :: x(n), y(n)
  INTEGER :: i
  x = 4.0
  CALL FOO((/(3.0, i=1,n)/), x, y)
  PRINT*, y
END SUBROUTINE HEAD

