module m

! equivalent a nullify(p1)
real, pointer :: p1 => null()
! g95 Error: Pointer initialization at (1) requires '=>', not '='
real, pointer :: p2 = null()
! Error: Pointer initialization requires a NULL at (1)
real, pointer :: p3 => p1
! g95 Error: Pointer initialization at (1) requires '=>', not '='
real, pointer :: p4 = p1

end module m

