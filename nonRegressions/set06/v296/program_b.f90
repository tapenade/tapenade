!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 20 Jun 2019 16:47
!
!  Differentiation of foo in reverse (adjoint) mode:
!   gradient     of useful results: z a b c
!   with respect to varying inputs: z a b c
!   RW status of diff variables: z:in-zero a:incr b:incr c:incr
SUBROUTINE FOO_B(a, ab, b, bb, c, cb, z, zb)
  IMPLICIT NONE
  INTEGER, PARAMETER :: k=8
  REAL*8 :: a, b, c, z
  REAL*8 :: ab, bb, cb, zb
  REAL*8 :: tempb
  REAL*8 :: tempb0
  tempb = zb/(b*c+1.0_k)
  ab = ab + c*tempb
  tempb0 = -(a*c*tempb/(b*c+1.0_k))
  cb = cb + a*tempb + b*tempb0
  bb = bb + c*tempb0
  zb = 0.0_8
END SUBROUTINE FOO_B

