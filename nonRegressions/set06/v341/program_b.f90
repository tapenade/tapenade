!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) - 30 Mar 2023 17:28
!
!  Differentiation of top in reverse (adjoint) mode:
!   gradient     of useful results: a b
!   with respect to varying inputs: a b
!   RW status of diff variables: a:in-out b:incr
! from lh94
! Warning: keep in ming that the results are surprising
! (e.g. c%ff=B considered dead code!) because code is
! wrong because C%ff and C%gg are not allocated.
SUBROUTINE TOP_B(a, ab, b, bb)
  IMPLICIT NONE
  REAL, DIMENSION(10) :: a, b
  REAL, DIMENSION(10) :: ab, bb
  TYPE TTT0
      REAL, DIMENSION(:), ALLOCATABLE :: ff, gg
      INTEGER :: i
  END TYPE TTT0
  TYPE TTT0_DIFF
      REAL, DIMENSION(:), ALLOCATABLE :: ff
      REAL, DIMENSION(:), ALLOCATABLE :: gg
  END TYPE TTT0_DIFF
  TYPE(TTT0) :: c
  TYPE(TTT0_DIFF) :: cb
  REAL, DIMENSION(10) :: d
  REAL, DIMENSION(10) :: db
  REAL, DIMENSION(10) :: dummyzerodiffb
  REAL, DIMENSION(10) :: dummyzerodiffb0
! si on remet les 2 allocate le type ttt0_d est correct
!  allocate(C%ff(10))
!  allocate(C%gg(10))
  CALL PUSHREAL4ARRAY(a, 10)
  CALL CRUNCH(a, b)
  CALL PUSHREAL4ARRAY(a, 10)
  CALL CRUNCH(a, c%ff)
  CALL PUSHREAL4ARRAY(a, 10)
  CALL CRUNCH(a, c%gg)
  d = 0.0
  db = 0.0
  CALL CRUNCH_B(a, ab, d, db)
  CALL POPREAL4ARRAY(a, 10)
  dummyzerodiffb0 = 0.0
  CALL CRUNCH_B(a, ab, c%gg, dummyzerodiffb0)
  CALL POPREAL4ARRAY(a, 10)
  dummyzerodiffb = 0.0
  CALL CRUNCH_B(a, ab, c%ff, dummyzerodiffb)
  CALL POPREAL4ARRAY(a, 10)
  CALL CRUNCH_B(a, ab, b, bb)
END SUBROUTINE TOP_B

!  Differentiation of crunch in reverse (adjoint) mode:
!   gradient     of useful results: x y
!   with respect to varying inputs: x y
SUBROUTINE CRUNCH_B(x, xb, y, yb)
  IMPLICIT NONE
  REAL, DIMENSION(10) :: x, y
  REAL, DIMENSION(10) :: xb, yb
  yb(5) = yb(5) + y(1)*xb(4)
  yb(1) = yb(1) + y(5)*xb(4)
  xb(3) = xb(3) + x(2)*xb(2)
  xb(2) = x(3)*xb(2)
END SUBROUTINE CRUNCH_B

