! from lh94
! Warning: keep in ming that the results are surprising
! (e.g. c%ff=B considered dead code!) because code is
! wrong because C%ff and C%gg are not allocated.
subroutine top(A,B)
  real, dimension(10) :: A,B
  type ttt0
     real, DIMENSION(:), allocatable :: ff,gg
     integer :: i
  end type ttt0
  type(ttt0) :: C
  real, dimension(10) :: D

! si on remet les 2 allocate le type ttt0_d est correct
!  allocate(C%ff(10))
!  allocate(C%gg(10))

  call crunch(A,B)
  C%ff = B
  call crunch(A,C%ff)
  C%gg = 0.0
  call crunch(A,C%gg)
  D = 0.0
  call crunch(A,D)
end subroutine top

subroutine crunch(X,Y)
  real, dimension(10) :: X,Y
  X(2) = X(3)*X(2)
  X(4) = X(4) + Y(5)*Y(1)
end subroutine crunch
