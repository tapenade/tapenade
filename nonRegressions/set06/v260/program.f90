  SUBROUTINE DIF1D_SOLVER(dif1d, q, av)
    REAL(8), DIMENSION(:), ALLOCATABLE :: aw, ae, ap, su

    n =  2
    ALLOCATE(su(n))

    CALL DIF1D_CONSTRUCT(dif1d, aw, ae, ap, su)
    CALL SOL_JACOBI(aw, ae, ap, su)


    DEALLOCATE(su)
  END SUBROUTINE
