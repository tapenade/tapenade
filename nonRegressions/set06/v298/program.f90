      subroutine foo(a,b, phi)
       real*8, dimension(2) :: a,b
       real*8 :: phi
       phi = phi + 0.5 * sum(a(:) - b(:)**2)
      end 
      subroutine foo1(a,b, phi)
       real*8, dimension(2) :: a,b
       real*8 :: phi
       phi = phi + 0.5 * sum(a(:) - b(:)+2)
      end 
      subroutine foo2(a,b, phi)
       real*8, dimension(2) :: a,b
       real*8 :: phi
       phi = phi + 0.5 * sum(2 + a(:) - b(:))
      end 
      subroutine foo3(a,b, phi)
       real*8, dimension(2) :: a,b
       real*8 :: phi
       phi = phi + 0.5 * sum(2.0 + a(:) - b(:))
      end 
      subroutine foo4(a,b, phi)
       double precision, dimension(2) :: a,b
       double precision :: phi
       phi = phi + 0.5 * sum(2.0 + a(:) - b(:))
      end 
      subroutine foo5(a,b, phi)
       real*8, dimension(2) :: a,b
       real*8 :: phi
       real :: x
       phi = phi + 0.5 * sum(x + a(:) - b(:))
      end 
      subroutine foo6(a,b, phi)
       real*4, dimension(2) :: a,b
       real*4 :: phi
       double precision :: x
       phi = phi + 0.5 * sum(x + a(:) - b(:))
      end 
      subroutine foo7(a,b, phi)
       real*8, dimension(2) :: a,b
       real*8 :: phi
       double precision :: x
       phi = phi + 0.5 * sum(x + a(:) - b(:))
      end 
      subroutine foo8(a,b, phi)
       double precision, dimension(2) :: a,b
       double precision :: phi
       real*8 :: x
       phi = phi + 0.5 * sum(x + a(:) - b(:))
      end 

      subroutine top(a,b,phi)
       integer, parameter ::wp = 8
       real(wp), dimension(2) :: a,b
       real(wp) :: phi
       call foo(a,b,phi)
       call foo1(a,b,phi)
       call foo2(a,b,phi)
       call foo3(a,b,phi)
       call foo4(a,b,phi)
       call foo5(a,b,phi)
       call foo6(a,b,phi)
       call foo7(a,b,phi)
       call foo8(a,b,phi)
      end
