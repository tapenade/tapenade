module M
       real x,y
contains
subroutine init
x = 2.0
y = 3.0
end subroutine
end module

module A
use M, ax => x
end module

module B
use M, only: y
end module

module C
contains
real function g(t)
use A
use B
implicit none
real t
g = t * ax * y
return
end function
end module

program main
use M
use C
real result
call init()
result = g(4.0)
print*,'g ' 
print*, result
end program
