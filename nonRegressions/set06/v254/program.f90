module M
       real x,y,z
contains
subroutine init
x = 2.0
y = 3.0
end subroutine
end module

module A
! pb on importe deux fois la symbolTable de M
! au lieu de merger avant d'importer
use M, only: x
use M, ay => y
! equivalent a use M, ay => y
end module

module B
use A
! z est use associated ici, mais pas y
real y
end module

module C
use M, only: x, y, ay => y
end module

module D
contains
subroutine test(z)
use C
implicit none
! y est use associated ici
real z
z = y + ay
end subroutine
end module

module E
use M, only: x
use M, only: y
end module

module F
use M, fy => y
end module

subroutine G(zz)
use F, gy => fy
implicit none
real zz
z = gy
end subroutine
