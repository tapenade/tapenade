module MM

  type ttt0
     real :: x
     integer :: i
  end type ttt0

  type ttt1
     real :: truc
     real :: chose
     type (ttt0) , dimension(:), pointer :: tab
     integer :: iii
  end type ttt1

contains

  subroutine top(A,B)
    type(ttt1) A,B

    A = B
  end subroutine top

end module MM
