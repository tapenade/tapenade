program topd
 use mpi
 implicit none
 integer, dimension( MPI_STATUS_SIZE ) :: statut
 integer, parameter :: etiquette=100
 integer :: nb_procs,rang, &
 num_proc_precedent,num_proc_suivant,code
 real :: val, valeur
 real :: vald, valeurd

 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 num_proc_suivant=mod(rang+1,nb_procs)
 num_proc_precedent=mod(nb_procs+rang-1,nb_procs)

 val = rang+1000
 vald = rang+2000

 if (rang == 0) then
   call msg1_d(val,vald, valeur,valeurd, num_proc_precedent,num_proc_suivant,etiquette, code)
 else
   call msg2_d(val,vald, valeur,valeurd, num_proc_precedent,num_proc_suivant,etiquette, code)
 end if

 print *,'Moi, proc. ',rang,', j''ai recu valeur ',valeur,' du proc. ',num_proc_precedent
 print *,'et , proc. ',rang,', j''ai recu valeur ',valeurd,' du proc. ',num_proc_precedent

 call MPI_FINALIZE (code)
end program topd

