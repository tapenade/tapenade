subroutine test(tg, r)

TYPE vector
        REAL(KIND=8), DIMENSION(:), POINTER :: dat => NULL()
END TYPE vector

TYPE(vector), DIMENSION(:), POINTER :: tg

REAL(KIND=8),DIMENSION(:), ALLOCATABLE  :: r

tg(0)%dat = r

tg(1)%dat => NULL()

tg(2)%dat = tg(1)%dat

end subroutine test
