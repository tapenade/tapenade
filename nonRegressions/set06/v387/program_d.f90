!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
!
!  Differentiation of test in forward (tangent) mode (with options noISIZE):
!   variations   of useful results: *(*tg.dat)
!   with respect to varying inputs: *r *(*tg.dat)
!   RW status of diff variables: r:(loc) *r:in tg:(loc) *tg.dat:(loc)
!                *(*tg.dat):in-out
!   Plus diff mem management of: r:in tg:in *tg.dat:in-out
SUBROUTINE TEST_D(tg, tgd, r, rd)
  IMPLICIT NONE
  TYPE VECTOR
      REAL(kind=8), DIMENSION(:), POINTER :: dat => NULL()
  END TYPE VECTOR
  TYPE(VECTOR), DIMENSION(:), POINTER :: tg
  TYPE(VECTOR), DIMENSION(:), POINTER :: tgd
  REAL(kind=8), DIMENSION(:), ALLOCATABLE :: r
  REAL(kind=8), DIMENSION(:), ALLOCATABLE :: rd
  tgd(0)%dat = rd
  tg(0)%dat = r
  tgd(1)%dat => NULL()
  tg(1)%dat => NULL()
  tgd(2)%dat = tgd(1)%dat
  tg(2)%dat = tg(1)%dat
END SUBROUTINE TEST_D

