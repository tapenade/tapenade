module M
       real, public :: x
contains
subroutine init
x = 2.0
end subroutine

real function get()
get = x
end function

subroutine set(v)
real v
x = v
end subroutine

end module

module A
use M, only : get, set

contains
subroutine f(z)
real z

if (z > 0.0) then
call set(1.0)
else
call set(z)
endif
z = z * get()

end subroutine

end module
