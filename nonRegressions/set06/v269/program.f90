module globalstuff
implicit none
integer, parameter:: dp=selected_real_kind(15,60)
integer, parameter:: obsnm=3157, yr=22
integer, parameter:: d=20
real(dp), save:: w0(7), x0(7,2)
real(dp), save:: w(d*d*7), x(d*d*7,2)
integer evl

contains

subroutine linesearch(fcn,np,pin,fin,gin,pout,fout)
implicit none
integer, intent(in):: np
real(dp), intent(in):: pin(np), fin,gin(np)
real(dp), intent(out):: pout(np), fout
external fcn
integer ii
real(dp) a1,a2,a3,a4,f1,f2,f3,f4
real(dp) x1(np), x2(np),x3(np),x4(np),dv(np)
real(dp) temp1,temp2
	dv=(-1d-4)*max(-1d2,min(gin,1d2))
end subroutine linesearch

end module globalstuff

program main
use globalstuff
implicit none
call est_m2c3
end program main

