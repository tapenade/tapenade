Module observation2c3
use globalstuff
implicit none
integer, parameter:: k2c3=47
integer, save:: i
real(dp), save :: r(yr), an(yr,4)
end module observation2c3


Subroutine est_m2c3
use globalstuff
use observation2c3
implicit none
  integer method,iexp,msg,ndigit,ipr,itnlim,iagflg, iahflg,itrmcd
  real(dp) fscale,dlt,gradtl,stepmx, steptl,fpls,f
  real(dp) p(k2c3),  ppls(k2c3), gpls(k2c3)
  real(dp) fold, fnew
  external LL2c3

call linesearch(LL2c3,k2c3,ppls,fpls,gpls,p,f)
end subroutine est_m2c3


Subroutine LL2c3(np, p,f)
	use globalstuff
	use observation2c3
	implicit none

	integer, intent(in):: np
	real(dp), intent(in) :: p(np)
	real(dp), intent(out) :: f
end subroutine LL2c3

