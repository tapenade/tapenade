subroutine test(p,a,c,dh,hv,tmp1,tmp2,tmp3,tmp4)
implicit none
real, dimension(5) :: p,a,c,dh
real hv
double precision, allocatable :: tmp1(:,:,:), tmp2(:,:,:), &
                                    tmp3(:,:,:), tmp4(:,:,:)

dh = -0.38 * hv
p = dh * p
a = 3 * p
c = 4 * p
tmp1(1,1,1) = p(1)
end subroutine test
