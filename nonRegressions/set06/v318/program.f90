module test
 real, public :: rsmall = 0.5 * epsilon(1.)

contains

subroutine stest(x)
 x = x * rsmall
end subroutine stest


end module test
