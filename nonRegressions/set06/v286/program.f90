! ne pas redeclarer real f dans program a, g95 refuse
program a
 real x,y
 x = 2.0
 y = f(x)
 print*,x
 print*,y
 contains
      real function f(x)
        real x
        f = x * x
      end function f
end program
