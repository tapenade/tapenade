! bug de jean 05.07.2011 pb interface
module m0
  integer,parameter :: sp = kind(1.0) 
  integer,parameter :: dp = kind(1.0d0) 
end module


module m1
 use m0
 type m1t
   real(dp), dimension(:,:), pointer :: field
   real, dimension(:,:), pointer :: fields
 end type 
end module 

module m2
 use m1
  type m2t
    type(m1t) :: reffield
  end type 
end module 

module m3
 use m0
  interface alloc
     module procedure alloc_S, alloc_D
  end interface

  contains 
  subroutine alloc_S(dim, field)
    integer:: dim
    real(kind=sp) , dimension(:,:), pointer :: field
    allocate (field(dim,dim))
  end subroutine 
  subroutine alloc_D(dim, field)
    integer:: dim
    real(kind=dp) , dimension(:,:), pointer :: field
    allocate (field(dim,dim))
  end subroutine 
end module 

subroutine foo(x,y)
  use m2
  use m3
  type(m2t) :: m2tInstance1, m2tInstance2
  call alloc(2,m2tInstance1%reffield%field)
  call alloc(2,m2tInstance1%reffield%fields)
  call alloc(2,m2tInstance2%reffield%field)
  m2tInstance1%reffield%fields=x
  m2tInstance1%reffield%field=m2tInstance1%reffield%fields
  y=m2tInstance1%reffield%fields(1,1)+m2tInstance1%reffield%field(2,2)
end subroutine 

program main 
  real :: x,y
  x=2.0
  call foo(x,y)
  print *,x
end program
