!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6820) - 12 Apr 2018 17:04
!
! test jean 05/05/2011 pour do 100,  Unclassifiable statement
MODULE M_DIFF
  IMPLICIT NONE
  TYPE TINNER
      INTEGER :: val
      TYPE(TINNER), POINTER :: next => NULL()
  END TYPE TINNER
  TYPE TOUTER
      TYPE(TINNER) :: val
      TYPE(TOUTER), POINTER :: next => NULL()
  END TYPE TOUTER

CONTAINS
  SUBROUTINE INIT(touterstart)
    IMPLICIT NONE
    TYPE(TOUTER), POINTER :: touterstart
    TYPE(TOUTER), POINTER, SAVE :: toutercurrent => NULL()
    TYPE(TINNER), POINTER, SAVE :: tinnercurrent => NULL()
    INTEGER :: i
    INTEGER :: j
    ALLOCATE(touterstart)
    toutercurrent => touterstart
    DO i=1,2
      tinnercurrent => toutercurrent%val
      tinnercurrent%val = 0
      DO j=1,2
        ALLOCATE(tinnercurrent%next)
        tinnercurrent => tinnercurrent%next
        tinnercurrent%val = j + i*j
      END DO
      ALLOCATE(toutercurrent%next)
      toutercurrent => toutercurrent%next
    END DO
  END SUBROUTINE INIT

  SUBROUTINE FOO(touterstart)
    IMPLICIT NONE
    TYPE(TOUTER), POINTER :: touterstart
    TYPE(TOUTER), POINTER, SAVE :: toutercurrent => NULL()
    TYPE(TINNER), POINTER, SAVE :: tinnercurrent => NULL()
    INTRINSIC ASSOCIATED
    toutercurrent => touterstart
    DO 100 
      IF (ASSOCIATED(toutercurrent)) THEN
        tinnercurrent => toutercurrent%val
        DO 
          IF (ASSOCIATED(tinnercurrent)) THEN
            IF (tinnercurrent%val .GT. 1) THEN
              tinnercurrent%val = tinnercurrent%val + 1
            ELSE
              tinnercurrent%val = tinnercurrent%val - 1
            END IF
            tinnercurrent => tinnercurrent%next
          ELSE
            toutercurrent => toutercurrent%next
            GOTO 100
          END IF
        END DO
      END IF
      GOTO 110
 100 CONTINUE
 110 CONTINUE
  END SUBROUTINE FOO

  SUBROUTINE DUMP(touterstart)
    IMPLICIT NONE
    TYPE(TOUTER), POINTER :: touterstart
    TYPE(TOUTER), POINTER, SAVE :: toutercurrent => NULL()
    TYPE(TINNER), POINTER, SAVE :: tinnercurrent => NULL()
    INTRINSIC ASSOCIATED
    PRINT*, 'dump it'
    toutercurrent => touterstart
    DO 100 
      IF (ASSOCIATED(toutercurrent)) THEN
        tinnercurrent => toutercurrent%val
        DO 
          IF (ASSOCIATED(tinnercurrent)) THEN
            PRINT*, tinnercurrent%val
            tinnercurrent => tinnercurrent%next
          ELSE
            toutercurrent => toutercurrent%next
            GOTO 100
          END IF
        END DO
      END IF
      GOTO 110
 100 CONTINUE
 110 CONTINUE
  END SUBROUTINE DUMP

END MODULE M_DIFF

SUBROUTINE P_NODIFF()
  USE M_DIFF
  IMPLICIT NONE
  TYPE(TOUTER), POINTER, SAVE :: touterstart => NULL()
  CALL INIT(touterstart)
  CALL DUMP(touterstart)
  CALL FOO(touterstart)
  CALL DUMP(touterstart)
END SUBROUTINE P_NODIFF

