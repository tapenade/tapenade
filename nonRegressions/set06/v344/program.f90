! test jean 05/05/2011 pour do 100,  Unclassifiable statement

module m

type tInner
 integer :: val
 type(tInner), pointer :: next => null()
end type

type tOuter
  type(tInner) :: val
  type(tOuter), pointer :: next => null()
end type  

contains

  subroutine init(touterStart)
    type(tOuter), pointer :: tOuterStart
    type(tOuter), pointer :: tOuterCurrent => null()
    type(tInner), pointer :: tInnerCurrent => null()
    allocate(tOuterStart)
    tOuterCurrent=>tOuterStart
    do i=1,2
      tInnerCurrent=>tOuterCurrent%val
      tInnerCurrent%val=0
      do j=1,2
         allocate(tInnerCurrent%next)
         tInnerCurrent=>tInnerCurrent%next
         tInnerCurrent%val=j+(i*j)
      end do
      allocate(tOuterCurrent%next)
      tOuterCurrent=>tOuterCurrent%next
    end do  
  end subroutine init

  subroutine foo(touterStart)
    type(tOuter), pointer :: tOuterStart
    type(tOuter), pointer :: tOuterCurrent => null()
    type(tInner), pointer :: tInnerCurrent => null()
    tOuterCurrent=>tOuterStart
    do
       if (associated(tOuterCurrent)) then
          tInnerCurrent=>tOuterCurrent%val
          do
             if (associated(tInnerCurrent)) then
                if (tInnerCurrent%val>1) then
                   tInnerCurrent%val=tInnerCurrent%val+1
                else
                   tInnerCurrent%val=tInnerCurrent%val-1
                end if
                tInnerCurrent=>tInnerCurrent%next
             else
                exit
             end if
          end do
          tOuterCurrent=>tOuterCurrent%next
       else
          exit
       end if
    end do
  end subroutine

  subroutine dump(touterStart)
    type(tOuter), pointer :: tOuterStart
    type(tOuter), pointer :: tOuterCurrent => null()
    type(tInner), pointer :: tInnerCurrent => null()
    print *,'dump it'
    tOuterCurrent=>tOuterStart
    do
       if (associated(tOuterCurrent)) then
          tInnerCurrent=>tOuterCurrent%val
          do
             if (associated(tInnerCurrent)) then
                print *,tInnerCurrent%val
                tInnerCurrent=>tInnerCurrent%next
             else
                exit
             end if
          end do
          tOuterCurrent=>tOuterCurrent%next
       else
          exit
       end if
    end do
  end subroutine

end module 

program p 
use m
type(tOuter), pointer :: tOuterStart =>null()

 call init(tOuterStart)
 call dump(tOuterStart)
 call foo(tOuterStart)
 call dump(tOuterStart)

end program
