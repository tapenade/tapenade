!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
!
!  Differentiation of bceulerwalladj in forward (tangent) mode (with options multiDirectional):
!   variations   of useful results: padj0
!   with respect to varying inputs: padj0 *(*bcdata.norm)
!   RW status of diff variables: padj0:in-out norm:(loc) bcdata:(loc)
!                *bcdata.norm:(loc) *(*bcdata.norm):in
!   Plus diff mem management of: bcdata:in *bcdata.norm:in
SUBROUTINE BCEULERWALLADJ_DV(nn, padj0, padj0d, bcdata, bcdatad, norm, &
& normd, nbdirs)
  USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
  IMPLICIT NONE
  TYPE BCDATATYPE
      REAL(kind=8), DIMENSION(:, :, :), POINTER :: norm
  END TYPE BCDATATYPE
  TYPE BCDATATYPE_DIFFV
      REAL(kind=8), DIMENSION(:, :, :, :), POINTER :: norm
  END TYPE BCDATATYPE_DIFFV
  INTEGER(kind=4), INTENT(IN) :: nn
  REAL(kind=8), DIMENSION(-2:2, -2:2) :: padj0
  REAL(kind=8), DIMENSION(nbdirsmax, -2:2, -2:2) :: padj0d
  REAL(kind=8), DIMENSION(:, :, :), POINTER :: norm
  REAL(kind=8), DIMENSION(:, :, :, :), POINTER :: normd
  TYPE(BCDATATYPE), DIMENSION(:), POINTER :: bcdata
  TYPE(BCDATATYPE_DIFFV), DIMENSION(:), POINTER :: bcdatad
  INTEGER(kind=4) :: i, j, k
  INTEGER :: nd
  REAL(kind=8) :: temp
  INTEGER :: nbdirs
  normd => bcdatad(nn)%norm
  norm => bcdata(nn)%norm
  DO i=-2,2
    DO j=-2,2
      temp = bcdata(nn)%norm(j, i, 1)
      DO nd=1,nbdirs
        padj0d(nd, i, j) = norm(j, i, 2)*bcdatad(nn)%norm(nd, j, i, 1) +&
&         temp*normd(nd, j, i, 2)
      END DO
      padj0(i, j) = temp*norm(j, i, 2)
    END DO
  END DO
END SUBROUTINE BCEULERWALLADJ_DV

