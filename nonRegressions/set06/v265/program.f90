SUBROUTINE BCEULERWALLADJ(nn, padj0, bcdata, norm)
  IMPLICIT NONE

  TYPE BCDATATYPE
      REAL(kind=8), DIMENSION(:, :, :), POINTER :: norm
  END TYPE BCDATATYPE

  INTEGER(kind=4), INTENT(IN) :: nn

  REAL(kind=8), DIMENSION(-2:2, -2:2) :: padj0
  REAL(kind=8), DIMENSION(:, :, :), POINTER :: norm

  TYPE(BCDATATYPE), DIMENSION(:), POINTER :: bcdata
  INTEGER(kind=4) :: i, j, k

  norm => bcdata(nn)%norm
  DO i=-2,2
    DO j=-2,2
     padj0(i,j) = bcdata(nn)%norm(j,i,1) * norm(j,i,2)
    END DO
  END DO

END SUBROUTINE BCEULERWALLADJ
