      Subroutine Discret_EQM_GDS(PRG,nface,ncellule,nvariable,
     &                          IpFace,IndFace,
     &                          IpntCF_CC,Indcon_CC,
     &                          Flux_Vit,Cnb,SrcU,SrcV,SrcW,
     &                          N_passe,Ipnt_passe,Ind_passe,
     &                          Iposition_L,Iposition_R)

      implicit double precision (a-h,o-z)
!      integer indface
!      sans cette declaration le type de indface est faux
      Dimension IpFace(*),IndFace(*)
      Dimension Flux_Vit(*)
      integer,parameter::MAXDIM_CHAINE=3
      integer,parameter::IFACE_DESTROYED=100000000
      integer::ia
      integer::iread_marqueur,marqueur,ia1

      iread_marqueur(marqueur,ia1) 
     & =
     & MIN(ABS(marqueur-IFACE_DESTROYED),222)*
     & (marqueur/ia1-(marqueur/ib1)*ia1)  
     & +
     & ( 1 - MIN(ABS(marqueur-IFACE_DESTROYED),333))*marqueur

         marqueur=iread_marqueur(IndFace(ipdebface),ia(1))
      End
