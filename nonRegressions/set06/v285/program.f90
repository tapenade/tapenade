module common_data
      integer::MAXDIM_TCRS,MAXDIM_CONF
      integer::nface,ncellule,nvariable,nnode,N_passe
      integer,allocatable,dimension (:)::IpFace,IndFace,&
          IpntCF_CC,IndCon_CF,IndCon_CC,Iposition_L,Iposition_R
      integer,dimension (1)::Ipnt_Passe,Ind_Passe!inutil
      double precision,dimension(:),allocatable::x,yn,z,u,v,w,&
          dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz,&
          dpdx,dpdy,dpdz,p,&
          Flux_Vit,FluxVdepf,cnb,SrcU,SrcV,SrcW

      double precision,dimension(:),allocatable::XYwall
      integer,allocatable,dimension (:)::Mapping_wall

      integer::i,ipnt,iface,ideb_face,icell_L,icell_R,i1,i2,i3,&
          n1,n2,n3,ipdebface,ind_L,ind_R,inb,ichaine,nchaine
      double precision::t,volmin,volmax

end module common_data

subroutine mainsub( n, x_indep, m, y)
  use common_data
  implicit none
  integer :: n, m, imap, icell, NVM, NFMAX
  double precision :: x_indep(n), y(m)
  double precision :: eps=1.0d-3
  integer::NFCOMMAX=1
  integer::lfil=1,krylov=5,maxits=30,iout=0,mybloc=1,nbloc=1
  logical::update_iluk=.true.
  integer,dimension (1)::nfcom=0,nblcom=0,indmyface=0
  integer,dimension (1,1)::IndInterF=0

  integer::it,itmax=140
  double precision :: diff,alfa=0.5

  NVM=NVariable
  nfmax=nface
     Call Discret_EQM_GDS('Test',nface,ncellule,nvariable,&
          IpFace,IndFace,&
          IpntCF_CC,Indcon_CC,&
          Flux_Vit,Cnb,SrcU,SrcV,SrcW,&
          N_passe,Ipnt_passe,Ind_passe,&
          Iposition_L,Iposition_R)

end subroutine mainsub
