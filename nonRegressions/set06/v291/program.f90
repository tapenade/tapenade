module real_precision

	IMPLICIT NONE

	INTEGER, PARAMETER :: sp = SELECTED_REAL_KIND(6, 37)  !< single precision (real 4)
	INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(12,307) !< double precision (real 8)
	INTEGER, PARAMETER :: wp = dp                         !< working precision

end module real_precision


MODULE type_assimilation

USE real_precision

IMPLICIT NONE

TYPE ctlvec                                
    LOGICAL :: lalloc = .FALSE.             !< Control vector allocation status
    INTEGER :: n1d                          !< Control vector number of 1D fields
    INTEGER :: nsg                          !< Control vector number of single parameters
    INTEGER :: nsize1d                      !< Control vector size of each 1D field
    INTEGER :: nsize                        !< Total size of control vector
    REAL(KIND=wp), POINTER, DIMENSION(:) :: xdata    !< Control vector data
END TYPE ctlvec

TYPE obsvec  
    CHARACTER(LEN=3) :: obsname             !< Nom de la variable observee                       
    LOGICAL :: lalloc = .FALSE.             !< Control vector allocation status
    INTEGER :: ntobs                        !< Nombre d'instants d'observation
    INTEGER :: nsize                        !< Total size of this obs vector
    REAL(KIND=wp), POINTER, DIMENSION(:) :: xdata    !< Control vector data
END TYPE obsvec


END MODULE type_assimilation


MODULE assim_params

USE real_precision
USE type_assimilation

IMPLICIT NONE

TYPE(ctlvec), SAVE :: x0ref !< vecteur de controle de reference pour boucle externe (cf x_r dans \ref assim)

TYPE(obsvec), SAVE :: yobs_equiv_ref_alt !< equivalent modele des obs pour x0ref (altitude)
TYPE(obsvec), SAVE :: yobs_equiv_ref_vol !< equivalent modele des obs pour x0ref (volume)
TYPE(obsvec), SAVE :: yobs_equiv_ref_etd !< equivalent modele des obs pour x0ref (altitude)


INTERFACE
	SUBROUTINE simulator_observations_tap(xctlvec, yobsvec_a, yobsvec_v, yobsvec_e)
		USE real_precision
		USE type_assimilation
    	TYPE(ctlvec) :: xctlvec  !< vecteur d'entree
    	TYPE(obsvec), OPTIONAL :: yobsvec_a !< obs altitude
    	TYPE(obsvec), OPTIONAL :: yobsvec_v !< obs volume
    	TYPE(obsvec), OPTIONAL :: yobsvec_e !< obs etendue
	END SUBROUTINE simulator_observations_tap
END INTERFACE

REAL(KIND=wp) :: J_cost !< fonction cout totale


END MODULE assim_params


SUBROUTINE simulator_observations_tap(xctlvec, yobsvec_a, yobsvec_v, yobsvec_e)
	
	use real_precision
    USE assim_params

    IMPLICIT NONE
        
    ! Arguments
    TYPE(ctlvec) :: xctlvec   !< vecteur d'entree

    TYPE(obsvec) :: yobsvec_a !< obs altitude
    TYPE(obsvec) :: yobsvec_v !< obs volume
    TYPE(obsvec) :: yobsvec_e !< obs etendue
    
    

END SUBROUTINE simulator_observations_tap

SUBROUTINE cost_winnie_tapenade

	use real_precision
	USE assim_params

	IMPLICIT NONE

    CALL simulator_observations_tap(x0ref, yobs_equiv_ref_alt, &
      							yobs_equiv_ref_vol, yobs_equiv_ref_etd)
    J_cost = 0.
	
END SUBROUTINE cost_winnie_tapenade
