!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.12 (r6321M) -  3 Mar 2017 12:43
!
! from Dominic Jones 05 decembre 2011
! intrinsic functions in dimension attribute: ne pas inliner
SUBROUTINE SUBR(n, x, f)
  IMPLICIT NONE
  INTEGER :: n
  INTRINSIC MAX
  REAL, DIMENSION(MAX(n, 1)) :: x
  REAL :: f
  INTRINSIC SUM
  INTRINSIC SQRT
  f = SQRT(SUM(x)**2)
END SUBROUTINE SUBR

