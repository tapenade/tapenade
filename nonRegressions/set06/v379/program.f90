! from Dominic Jones 05 decembre 2011
! intrinsic functions in dimension attribute: ne pas inliner
subroutine subr(n,x,f)
integer::n
real,dimension(max(n,1))::x
real::f
f=sqrt(sum(x)**2)
end subroutine
