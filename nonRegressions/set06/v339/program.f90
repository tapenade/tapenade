MODULE M
  IMPLICIT NONE
  type t
     real :: val
     integer :: i
     type (t), pointer :: next =>NULL()
  end type 
  type(t), pointer :: current =>NULL()
END MODULE M

subroutine test(ptr)
   use m
   type(t), pointer :: next_elem
   type(t), pointer :: ptr
   if (associated(current%next)) then
      next_elem=>current%next
      ptr%val = ptr%val * ptr%val
      current => ptr
      ptr%val = current%next%val * current%next%val
   else
      allocate(current%next)
      next_elem=>current%next

   end if

end subroutine test
