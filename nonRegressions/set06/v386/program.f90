! pour tester les noms de variable ISIZE...

subroutine top(obj)
  TYPE struc
     REAL t1(*)
     REAL,POINTER :: t2(*)
  END TYPE struc
  TYPE(struc) obj(*)
  obj(1)%t1(2) = obj(3)%t1(4) * obj(5)%t1(6)
  call test(obj)
  obj(7)%t1(8) = obj(9)%t1(1) * obj(2)%t1(3)
end subroutine top

subroutine test(obj)
  TYPE struc
     REAL t1(*)
     REAL,POINTER :: t2(*)
  END TYPE struc
  TYPE(struc) obj(*)
  INTEGER i,j

  do i=5,20
     do j = 2,30
        obj(i)%t1(j) = obj(j)%t2(i) * obj(i)%t2(j+2)
        obj(j)%t2(i+1) = sin(obj(i-1)%t1(i))
     end do
  end do
end subroutine test
