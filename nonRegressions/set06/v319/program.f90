program testIntents

	real :: x, b, y;

	x = 10.;

	call someFunc(x, b, y)

	print '(A,F5.2)','x=',x

	CONTAINS

	subroutine someFunc(x,b,y)
		real, intent(INOUT) :: x
		real, intent(OUT) :: b
		real, intent(OUT) :: y

		y = x + b;
		b = y;
		y = b*2.;

	endsubroutine someFunc

endprogram testIntents
