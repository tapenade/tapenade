MODULE definition
  INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(10,50)

  TYPE rk_comm_real_1d
        REAL(kind=wp) :: t
        REAL(kind=wp) :: t_old
        REAL(kind=wp), DIMENSION(:), POINTER :: weights
  END TYPE rk_comm_real_1d
END MODULE definition

  SUBROUTINE step_r1(comm,f,tnow,y,yp,stages,tol,htry,y_new,    &
       errest,err,hmin,phase_2)
    USE definition
    
    TYPE(rk_comm_real_1d), INTENT(inout), TARGET :: comm
    REAL(kind=wp), INTENT(out) :: err
    REAL(kind=wp), INTENT(inout) :: htry 
    REAL(kind=wp), INTENT(in) :: tnow    
    REAL(kind=wp), INTENT(in) :: tol
    REAL(kind=wp), INTENT(in), OPTIONAL :: hmin
    LOGICAL, INTENT(inout), OPTIONAL :: phase_2

    REAL(kind=wp), DIMENSION(:), INTENT(in) :: y, yp
    REAL(kind=wp), DIMENSION(:), INTENT(out) ::  errest, y_new
    REAL(kind=wp), DIMENSION(:,:), INTENT(out) :: stages  

    INTERFACE
      FUNCTION f(t,y)
        USE definition
        REAL(kind=wp), INTENT(in) :: t 
        REAL(kind=wp), DIMENSION(:), INTENT(in) :: y
        REAL(kind=wp), DIMENSION(SIZE(y,1)) :: f
      END FUNCTION f
    END INTERFACE
    !
    REAL(kind=wp) :: tstg  
    INTEGER :: i, j
    LOGICAL :: cutbak, main

    INTRINSIC         abs, max, sign

    REAL(kind=wp), DIMENSION(:), POINTER :: weights, thresh 
    REAL(kind=wp), DIMENSION(:,:), POINTER :: a     
    REAL(kind=wp), DIMENSION(:), POINTER :: b, bhat, c
    INTEGER, DIMENSION(:), POINTER :: ptr  

    REAL(kind=wp), PARAMETER :: zero=0.0_wp, half=0.5_wp, one=1.0_wp

    weights => comm%weights

  END SUBROUTINE

  SUBROUTINE truerr_r1(comm,f,ier)
    USE definition
    TYPE(rk_comm_real_1d), INTENT(inout) :: comm
    INTEGER, INTENT(inout) :: ier
    INTERFACE
      FUNCTION f(t,y)
        USE definition
        REAL(kind=wp), INTENT(in) :: t     
        REAL(kind=wp), DIMENSION(:), INTENT(in) :: y
        REAL(kind=wp), DIMENSION(SIZE(y,1)) :: f    
      END FUNCTION f
    END INTERFACE

    REAL(kind=wp) :: hmin, hsec   
    REAL(kind=wp) :: diff, errmax, mxerlc, tsec, ge_err, ge_test1, ge_test2
    REAL(kind=wp), DIMENSION(:), POINTER :: ge_y, ge_yp, ge_y_new       
    REAL(kind=wp), DIMENSION(:,:), POINTER :: ge_stages         
    REAL(kind=wp), DIMENSION(:), POINTER :: ge_err_estimates, y   

    CALL step_r1(comm,f,tsec,ge_y,ge_yp,ge_stages,ge_test1,hsec,ge_y_new, &
             ge_err_estimates,ge_err)

  END SUBROUTINE truerr_r1
