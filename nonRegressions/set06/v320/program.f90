module test_typedef
    DOUBLE PRECISION , dimension(2) , private :: foobar
    data  foobar /0.D0, 1.D0 /
    type Input
        double precision x, y
        double precision, dimension(5) :: vector
    end type
    type Output
        double precision x, y, xdy, xpy, dot
        double precision, dimension(5) :: vector
    end type
end module 
