! Derived from set06/v324, this time with a dimension in field x.
module MM

  type ttt0
     real, dimension(5) :: x
     integer :: i
  end type ttt0

  type ttt1
     real :: truc
     real :: chose
     type (ttt0) , dimension(:), pointer :: tab
     integer :: iii
  end type ttt1

contains

  subroutine top(A,B)
    type(ttt1) A,B

    A = B
  end subroutine top

end module MM
