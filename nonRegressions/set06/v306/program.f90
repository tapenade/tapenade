  SUBROUTINE method_const(a, b, c, r, e, ptr, no_of_stages, &
       intrp_able, intrp_needs_stages, &
       fsal)
    INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(10,50)
    REAL(kind=wp), INTENT(out) :: a(13,13), b(13), c(13), r(11,6), e(7)
    INTEGER, INTENT(out) :: ptr(13), no_of_stages
    LOGICAL, INTENT(out) :: intrp_able, intrp_needs_stages
    REAL(kind=wp) :: cdiff

    LOGICAL, INTENT(out) :: fsal

    INTEGER :: i
    REAL(kind=wp), PARAMETER :: fivepc=0.05_wp,  one=1.0_wp, two=2.0_wp, &
         fifty=50.0_wp

    DO i = 1, no_of_stages - 1
      cdiff = MIN( cdiff, MINVAL( &
           ABS((c(i)-c(i+1:no_of_stages))), &
           mask=(c(i)-c(i+1:no_of_stages)/=0)) )
    END DO

  END SUBROUTINE method_const
