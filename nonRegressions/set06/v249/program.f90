module M
       real wn
contains
subroutine init(x)
real x
wn = x
end subroutine
end module

module N
use M, zwn => wn
end module

module O
contains

real function g(t)
use N
implicit none
real t
g = t * zwn
return
end function

real function h(t)
use M, zwn => wn
implicit none
real t
h = t * zwn
return
end function
end module O

program main
use M
use O
implicit none
real gresult
real hresult
call init(20.0)
print*,'wn '
print*,wn

gresult = g(3.0)
print*,'g  '
print*,gresult

hresult = h(3.0)
print*,'h  '
print*,hresult

end program
