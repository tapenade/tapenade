! from Pablo Saide 
      subroutine test1(x,y,z,crefin)

        IMPLICIT NONE

      real x,y,z
      real refr,refi,aux
      complex crefin

      refr=real(crefin)
      refi=-imag(crefin)

      aux=crefin*dconjg(crefin)

      x = y + z*z + refr*refi +aux*aux
      end 
