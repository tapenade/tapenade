! bug jean 05 juillet 2011
! il faut typechecker les declarations du module dans la 
! privateSymbolTable

module m0 

  integer :: m0_i=2

end module 

module m1 
  use m0 

  integer, parameter            :: GM_levels = 6
  logical, dimension(GM_levels) :: gm_show = .false.
  integer :: gm_unit=6              
  private gm_show, gm_unit

end module 
