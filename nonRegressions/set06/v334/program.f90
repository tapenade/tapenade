module common_data
      double precision,dimension(:),allocatable:: Flux_Vit

end module common_data

subroutine mainsub( y , m)
  use common_data
  implicit none
  integer :: m
  double precision :: y(m)
  Call Discret_EQM_GDS('Test',y,m,Flux_Vit)
end subroutine mainsub

Subroutine Discret_EQM_GDS(PRG,yy,mm,Flux_Vit)
      implicit double precision (a-h,o-z)
      Dimension Flux_Vit(*)
      print*, 'PRG = ', PRG
      print*, 'yy = ', yy
End

program test
use common_data
  double precision :: y(2)
allocate(Flux_Vit(2))
y(1) = 1
y(2) = 2
call mainsub( y , 2)

end program test
