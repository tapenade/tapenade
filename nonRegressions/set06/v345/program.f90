! test jean 04/01/2011 pour use only

module m1
 real, parameter :: a=1.2
 real :: c
end module 

module m2
 use m1, only: a
 implicit none
 save
 real, parameter :: b=1.0/a
end module 

module m3
contains 
subroutine foo(x)
  use m1, only :c
  use m2, only: b, a
  real x(2)
  real t 
  t=real(b)
  x(:)=sin(x(:)/(t*a))
end subroutine
end module  

program p 
use m2 
use m3
 real x(2)
 x=2.0 
 call foo(x)
 print *,x
end program
