MODULE divcur
!!==============================================================================
!!                       ***  MODULE  divcur  ***
!! Ocean diagnostic variable : horizontal divergence and relative vorticity
!!==============================================================================

!!----------------------------------------------------------------------
!!   div_cur    : Compute the horizontal divergence and relative
!!                vorticity fields
!!----------------------------------------------------------------------
!! * Modules used
   IMPLICIT NONE
   PRIVATE
  integer, parameter :: jpi = 4,jpj = 5, jpk = 6
  real, DIMENSION(jpi,jpj,jpk) :: hdivn, rotn, hdivb, rotb


!! * Accessibility
   PUBLIC div_cur    ! routine called by step.F90 and istate.F90


CONTAINS

   SUBROUTINE div_cur( kt )
      INTEGER, INTENT( in ) ::   kt     ! ocean time-step index
      
!! * Local declarations
      INTEGER  ::   ji, jj, jk          ! dummy loop indices
 integer :: nit000, lwp, numout, jpkm1, jpjm1, jpim1
 real, dimension(jpi,jpj,jpk) :: un, vn, vnm, fmask
 real, dimension(jpi,jpj) :: e2t, e2v, e1u, e1v, e1t, e2u, e1f, e2f

!!----------------------------------------------------------------------

      IF( kt == nit000 ) THEN
         IF(lwp) WRITE(numout,*)
         IF(lwp) WRITE(numout,*) 'div_cur : horizontal velocity divergence and'
         IF(lwp) WRITE(numout,*) '~~~~~~~   relative vorticity'
      ENDIF

!                                                ! ===============
      DO jk = 1, jpkm1                                 ! Horizontal slab
!                                             ! ===============

         hdivb(:,:,jk) = hdivn(:,:,jk)    ! time swap of div arrays
         rotb (:,:,jk) = rotn (:,:,jk)    ! time swap of rot arrays

!                                             ! --------
! Horizontal divergence                       !   div 
!                                             ! --------
         DO jj = 2, jpjm1
            DO ji = 2, jpim1   ! vector opt.

               hdivn(ji,jj,jk) = (  e2u(ji,jj) * un(ji,jj,jk) - e2u(ji-1,jj  ) * un(ji-1,jj  ,jk)      &
               + e1v(ji,jj) * vn(ji,jj,jk) - e1v(ji  ,jj-1) * vn(ji  ,jj-1,jk)  )   &
                  / ( e1t(ji,jj) * e2t(ji,jj) )

            END DO  
         END DO  


!                                             ! --------
! relative vorticity                          !   rot 
!                                             ! --------
         DO jj = 1, jpjm1
            DO ji = 1, jpim1   ! vector opt.
               rotn(ji,jj,jk) = (  e2v(ji+1,jj  ) * vn(ji+1,jj  ,jk) - e2v(ji,jj) * vn(ji,jj,jk)    &
              - e1u(ji  ,jj+1) * un(ji  ,jj+1,jk) + e1u(ji,jj) * un(ji,jj,jk)  ) &
           * fmask(ji,jj,jk) / ( e1f(ji,jj) * e2f(ji,jj) )
            END DO
         END DO
!                                             ! ===============
      END DO                                           !   End of slab
!                                                ! ===============
      
! 4. Lateral boundary conditions on hdivn and rotn
! ---------------------------------=======---======
      CALL lbc_lnk( hdivn, 'T',  1.0 )       ! T-point, no sign change
      CALL lbc_lnk( rotn , 'F', -1.0 )       ! F-point,    sign change

   END SUBROUTINE div_cur
   

!!======================================================================
END MODULE divcur
