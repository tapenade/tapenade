subroutine test(x)
entry other(x)
real x
print*,'x ', x
end subroutine test

program main
real x
x = 1.0
call test(x)
x = 2.0
call other(x)
end program main
