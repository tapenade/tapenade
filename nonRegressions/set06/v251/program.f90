module M
       real x,y,z
contains
subroutine init
x = 2.0
y = 3.0
end subroutine
end module

module A
use M, only: x,y
end module

module B
use A, only: x
real y ! y de b, pas de M
end module

module C
use B
contains
real function g(t)
implicit none
real t
g = t * x
return
end function
end module

program main
use M
use C
real result
call init()
result = g(4.0)
print*,'g ' 
print*, result
end program
