	SUBROUTINE linmin(p,xi)
	IMPLICIT NONE
	interface
		FUNCTION brent(bx,tol,xmin)
		REAL(8), INTENT(IN) :: bx,tol
		REAL(8), INTENT(OUT) :: xmin
		REAL(8) :: brent
		END FUNCTION brent
	end interface
	REAL(8), DIMENSION(:), TARGET, INTENT(INOUT) :: p,xi
	p=p+xi
	END SUBROUTINE linmin

	FUNCTION brent(bx,tol,xmin)
	IMPLICIT NONE
	REAL(8) :: bx,tol
	REAL(8), INTENT(OUT) :: xmin
	REAL(8) :: brent
	INTEGER(4) :: iter
	REAL(8) :: a,b,x,xm
	a=bx
        b=tol
        x=xmin
        xm=tol
	do iter=1,10
		tol=tol*abs(x)
		if (abs(x-xm) <= (tol-0.5*(b-a))) then
			xmin=x
		end if
	end do
	END FUNCTION brent
