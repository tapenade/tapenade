!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.11 (r5911) -  4 Jan 2016 16:40
!
SUBROUTINE LINMIN(p, xi)
  IMPLICIT NONE
  INTERFACE 
      FUNCTION BRENT(bx, tol, xmin)
        REAL*8, INTENT(IN) :: bx, tol
        REAL*8, INTENT(OUT) :: xmin
        REAL*8 :: brent
      END FUNCTION BRENT
  END INTERFACE

  REAL*8, DIMENSION(:), TARGET, INTENT(INOUT) :: p, xi
  p = p + xi
END SUBROUTINE LINMIN

FUNCTION BRENT(bx, tol, xmin)
  IMPLICIT NONE
  REAL*8 :: bx, tol
  REAL*8, INTENT(OUT) :: xmin
  REAL*8 :: brent
  INTEGER*4 :: iter
  REAL*8 :: a, b, x, xm
  INTRINSIC ABS
  REAL*8 :: abs0
  REAL*8 :: abs1
  a = bx
  b = tol
  x = xmin
  xm = tol
  DO iter=1,10
    IF (x .GE. 0.) THEN
      abs0 = x
    ELSE
      abs0 = -x
    END IF
    tol = tol*abs0
    IF (x - xm .GE. 0.) THEN
      abs1 = x - xm
    ELSE
      abs1 = -(x-xm)
    END IF
    IF (abs1 .LE. tol - 0.5*(b-a)) xmin = x
  END DO
END FUNCTION BRENT

