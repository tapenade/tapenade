!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 27 Apr 2021 20:22
!
!  Differentiation of filter_core in reverse (adjoint) mode (with options r8):
!   gradient     of useful results: ll
!   with respect to varying inputs: ll pp
!   RW status of diff variables: ll:in-out wts:(loc) vs:(loc) pp:out
!                ms:(loc)
SUBROUTINE FILTER_CORE_B(ndata, mm, nopt, npars, compresi, pp, ppb, xx, &
& activepars, initpars, rvs1, ms, msb, vs, vsb, wts, wtsb, fmean, fvar, &
& fresi, ll, llb, eflag)
  IMPLICIT NONE
  REAL*8, PARAMETER :: twopi=6.283185307179586_8
  INTEGER, INTENT(IN) :: ndata
  INTEGER, INTENT(IN) :: mm
  INTEGER, INTENT(IN) :: nopt
  INTEGER, INTENT(IN) :: npars
  LOGICAL, INTENT(IN) :: compresi
  REAL*8, DIMENSION(nopt), INTENT(IN) :: pp
  REAL*8, DIMENSION(nopt) :: ppb
  REAL*8, DIMENSION(ndata), INTENT(IN) :: xx
  LOGICAL, DIMENSION(npars), INTENT(IN) :: activepars
  REAL*8, DIMENSION(npars), INTENT(IN) :: initpars
  REAL*8, DIMENSION(0:ndata, mm), INTENT(IN) :: rvs1
  REAL*8, DIMENSION(0:ndata, mm) :: ms
  REAL*8, DIMENSION(0:ndata, mm) :: msb
  REAL*8, DIMENSION(0:ndata, mm) :: vs
  REAL*8, DIMENSION(0:ndata, mm) :: vsb
  REAL*8, DIMENSION(0:ndata, mm) :: wts
  REAL*8, DIMENSION(0:ndata, mm) :: wtsb
  REAL*8, DIMENSION(ndata) :: fmean
  REAL*8, DIMENSION(ndata) :: fvar
  REAL*8, DIMENSION(ndata) :: fresi
  REAL*8 :: ll
  REAL*8 :: llb
  INTEGER :: eflag
  REAL*8, DIMENSION(npars) :: pars
  REAL*8, DIMENSION(npars) :: parsb
  REAL*8, DIMENSION(mm) :: zz_sample, omega
  REAL*8, DIMENSION(mm) :: omegab
  REAL*8 :: zz, ret, dret, ddret, pad, sd, svar, nfac, sumomega, retr, &
& retl
  REAL*8 :: zzb, sumomegab
  REAL*8 :: mmean, mvar, dir, zztest, rettest, resiwt, predmean, predvar&
& , predz, uval
  REAL*8 :: mmeanb, mvarb
  INTEGER :: t, i, iter
  REAL*8, DIMENSION(0:ndata, mm) :: tmp
  EXTERNAL PARS_CV
  EXTERNAL PARS_CV_B
  EXTERNAL MIX_MEAN_VAR
  EXTERNAL MIX_MEAN_VAR_B
  INTRINSIC SQRT
  INTRINSIC EXP
  INTRINSIC SUM
  INTRINSIC LOG
  REAL*8 :: tempb
  CALL PUSHREAL8ARRAY(initpars, npars)
  CALL PUSHBOOLEANARRAY(activepars, npars)
  CALL PUSHREAL8ARRAY(pars, npars)
  CALL PUSHINTEGER4(npars)
  CALL PUSHREAL8ARRAY(pp, nopt)
  CALL PUSHINTEGER4(nopt)
  CALL PARS_CV(nopt, pp, npars, pars, activepars, initpars)
  vs(0, :) = pars(3)**2/(1.0_8-pars(1)**2)
  wts(0, :) = 1.0_8/(1.0_8*mm)
  DO t=1,ndata
    CALL PUSHINTEGER4(mm)
    CALL PUSHREAL8(mvar)
    CALL PUSHREAL8(mmean)
    CALL PUSHREAL8ARRAY(wts(t-1, :), mm)
    CALL PUSHREAL8ARRAY(vs(t-1, :), mm)
    CALL PUSHREAL8ARRAY(ms(t-1, :), mm)
    CALL MIX_MEAN_VAR(mm, ms(t-1, :), vs(t-1, :), wts(t-1, :), mmean, &
&               mvar)
    CALL PUSHREAL8(zz)
    zz = mmean
    CALL PUSHREAL8(omega(1))
    omega(1) = EXP(ret + 0.5_8*(zz_sample(1)-zz)**2/svar - nfac)
    CALL PUSHREAL8(sumomega)
    sumomega = SUM(omega)
    wts(t, :) = omega/sumomega
    vs(t, :) = pars(3)**2*(1.0_8-pars(4)**2)
  END DO
  wtsb = 0.0_8
  vsb = 0.0_8
  msb = 0.0_8
  omegab = 0.0_8
  mvarb = 0.0_8
  parsb = 0.0_8
  mmeanb = 0.0_8
  DO t=ndata,1,-1
    sumomegab = llb/sumomega - SUM(omega*wtsb(t, :))/sumomega**2
    tempb = SUM(vsb(t, :))
    vsb(t, :) = 0.0_8
    parsb(3) = parsb(3) + 2*pars(3)*(1.0_8-pars(4)**2)*tempb
    parsb(4) = parsb(4) - 2*pars(4)*pars(3)**2*tempb
    omegab = omegab + wtsb(t, :)/sumomega + sumomegab
    wtsb(t, :) = 0.0_8
    CALL POPREAL8(sumomega)
    CALL POPREAL8(omega(1))
    zzb = -(2*(zz_sample(1)-zz)*0.5_8*EXP(ret-nfac+0.5_8*((zz_sample(1)-&
&     zz)**2/svar))*omegab(1)/svar)
    omegab(1) = 0.0_8
    CALL POPREAL8(zz)
    mmeanb = mmeanb + zzb
    CALL POPREAL8ARRAY(ms(t-1, :), mm)
    CALL POPREAL8ARRAY(vs(t-1, :), mm)
    CALL POPREAL8ARRAY(wts(t-1, :), mm)
    CALL POPREAL8(mmean)
    CALL POPREAL8(mvar)
    CALL ADSTACK_STARTREPEAT()
    CALL POPINTEGER4(mm)
    CALL ADSTACK_RESETREPEAT()
    CALL ADSTACK_ENDREPEAT()
    CALL MIX_MEAN_VAR_B(mm, ms(t-1, :), msb(t-1, :), vs(t-1, :), vsb(t-1&
&                 , :), wts(t-1, :), wtsb(t-1, :), mmean, mmeanb, mvar, &
&                 mvarb)
    CALL POPINTEGER4(mm)
  END DO
  tempb = SUM(vsb(0, :))/(1.0_8-pars(1)**2)
  parsb(3) = parsb(3) + 2*pars(3)*tempb
  parsb(1) = parsb(1) + 2*pars(1)*pars(3)**2*tempb/(1.0_8-pars(1)**2)
  CALL POPINTEGER4(nopt)
  CALL POPREAL8ARRAY(pp, nopt)
  CALL POPINTEGER4(npars)
  CALL POPREAL8ARRAY(pars, npars)
  CALL POPBOOLEANARRAY(activepars, npars)
  CALL POPREAL8ARRAY(initpars, npars)
  ppb = 0.0_8
  CALL PARS_CV_B(nopt, pp, ppb, npars, pars, parsb, activepars, initpars&
&         )
END SUBROUTINE FILTER_CORE_B

