!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) -  4 Oct 2022 10:03
!
!  Differentiation of traj in reverse (adjoint) mode:
!   gradient     of useful results: u v
!   with respect to varying inputs: u v
!   RW status of diff variables: u:in-out v:in-out
! from Eugene Kazantsev Wed, 16 Nov 2011
SUBROUTINE TRAJ_B(u, ub, v, vb)
  IMPLICIT NONE
  INTEGER :: nx
  INTEGER :: ny
  INTEGER :: nz
  PARAMETER (nx=182, ny=149, nz=31)
  REAL*8 :: buf1, buf2
  REAL*8 :: buf1b, buf2b
  COMMON /buffers/ buf1(nx, ny, nz), buf2(nx, ny, nz)
  COMMON /buffers_b/ buf1b(nx, ny, nz), buf2b(nx, ny, nz)
  REAL*8 :: u(nx, ny, nz), v(nx, ny, nz)
  REAL*8 :: ub(nx, ny, nz), vb(nx, ny, nz)
  INTEGER :: n
  buf1b = 0.0_8
  buf2b = 0.0_8
  DO n=100,1,-1
    CALL TIMESTEP_B(u, ub, v, vb)
  END DO
END SUBROUTINE TRAJ_B

!  Differentiation of timestep in reverse (adjoint) mode:
!   gradient     of useful results: buf1 buf2 u v
!   with respect to varying inputs: buf1 buf2 u v
SUBROUTINE TIMESTEP_B(u, ub, v, vb)
  IMPLICIT NONE
  INTEGER :: nx
  INTEGER :: ny
  INTEGER :: nz
  PARAMETER (nx=182, ny=149, nz=31)
  COMMON /buffers/ buf1(nx, ny, nz), buf2(nx, ny, nz)
  COMMON /buffers_b/ buf1b(nx, ny, nz), buf2b(nx, ny, nz)
  REAL*8 :: buf1, buf2, dt
  REAL*8 :: buf1b, buf2b
  REAL*8 :: u(nx, ny, nz), v(nx, ny, nz)
  REAL*8 :: ub(nx, ny, nz), vb(nx, ny, nz)
  INTEGER :: j
  INTEGER :: i
  INTEGER :: k
  INTEGER :: i1
  INTEGER :: im
  INTEGER :: j1
  INTEGER :: jm
  INTEGER :: ad_from
  INTEGER :: ad_to
  INTEGER :: ad_from0
  INTEGER :: ad_to0
  INTEGER :: ad_from1
  INTEGER :: ad_to1
  dt = 0.01d0
  DO k=1,nz
    DO j=1,ny
      i1 = 20
      im = 30
      ad_from = i1
      i = im + 1
      CALL PUSHINTEGER4(i - 1)
      CALL PUSHINTEGER4(ad_from)
    END DO
  END DO
  DO k=1,nz
    DO j=1,ny
      i1 = 20
      im = 30
      ad_from0 = i1
      i = im + 1
      CALL PUSHINTEGER4(i - 1)
      CALL PUSHINTEGER4(ad_from0)
    END DO
    DO i=1,nx
      j1 = 20
      jm = 30
      ad_from1 = j1
      j = jm + 1
      CALL PUSHINTEGER4(j - 1)
      CALL PUSHINTEGER4(ad_from1)
    END DO
  END DO
  DO k=nz,1,-1
    DO i=nx,1,-1
      CALL POPINTEGER4(ad_from1)
      CALL POPINTEGER4(ad_to1)
      DO j=ad_to1,ad_from1,-1
        buf2b(i, j, 1) = buf2b(i, j, 1) + dt*vb(i, j, k)
      END DO
    END DO
    DO j=ny,1,-1
      CALL POPINTEGER4(ad_from0)
      CALL POPINTEGER4(ad_to0)
      DO i=ad_to0,ad_from0,-1
        buf2b(i, j, 1) = buf2b(i, j, 1) + dt*ub(i, j, k)
      END DO
    END DO
  END DO
  CALL DIFF_X_B(buf1(1, 1, 1), buf1b(1, 1, 1), buf2(1, 1, 1), buf2b(1, 1&
&         , 1))
  buf2b(:, :, 1) = 0.0_8
  DO k=nz,1,-1
    DO j=ny,1,-1
      CALL POPINTEGER4(ad_from)
      CALL POPINTEGER4(ad_to)
      DO i=ad_to,ad_from,-1
        ub(i, j, k) = ub(i, j, k) + buf1b(i, j, 1)
        vb(i, j, k) = vb(i, j, k) + dt*buf1b(i, j, 1)
      END DO
    END DO
  END DO
  DO j=ny,1,-1
    DO i=nx,1,-1
      buf1b(i, j, 1) = 0.0_8
    END DO
  END DO
END SUBROUTINE TIMESTEP_B

!  Differentiation of diff_x in reverse (adjoint) mode:
!   gradient     of useful results: u v
!   with respect to varying inputs: u
SUBROUTINE DIFF_X_B(u, ub, v, vb)
  IMPLICIT NONE
  INTEGER :: nz
  INTEGER :: nx
  INTEGER :: ny
  PARAMETER (nx=182, ny=149, nz=31)
  REAL*8 :: u(nx, ny), v(nx, ny)
  REAL*8 :: ub(nx, ny), vb(nx, ny)
  ub = ub + 2.0d0*vb
END SUBROUTINE DIFF_X_B

