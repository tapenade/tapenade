! test jean 05/05/2011 pour module procedure

module m0
  real :: i = 0 
  character(len=5), dimension(3), target :: ca
  data ca /3*'oink'/
end module 
module m 
 interface foo
   module procedure foo_i, foo_i2
 end interface

 contains
   subroutine foo_i(name,val)
    use m0
    character(len=*) :: name
    real val
    val=i*x
   end subroutine
   subroutine foo_i2(name,val)
    use m0
    character(len=*) :: name
    character(len=5), dimension(:), pointer :: val
    val=>ca
   end subroutine
end module 

subroutine bar()
 use m 
 real x
 character(len=5), dimension(:), pointer :: cap
 call foo('blah',x)
 call foo('bloh',cap)
 print *,cap
end subroutine

program p 
  call bar()
end program
