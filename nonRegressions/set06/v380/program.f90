module get_m
save
type arr_t
  real,dimension(:),pointer::a
  integer,dimension(:),pointer::b
end type
type(arr_t),dimension(:),allocatable::arr

interface get
  module procedure get_rp
  module procedure get_ip
end interface

contains

function get_rp(idx,typ) result(p)
  integer::idx
  real,dimension(:),pointer::typ,p
  p => arr(idx)%a
end function

function get_ip(idx,typ) result(p)
  integer::idx
  integer,dimension(:),pointer::typ,p
  p => arr(idx)%b
end function
end module


subroutine state_obj()
  call calc_u()
  call calc_f()
end subroutine


subroutine calc_u()
  use get_m, only: get
  real,dimension(:),pointer::x,u
  x => get(1,x)
  u => get(2,u)
  u = 2*u + x**2
end subroutine


subroutine calc_f()
  use get_m, only: get
  real,dimension(:),pointer::x,u,f
  x => get(1,x)
  u => get(2,u)
  f => get(3,f)
  f = f + u*x + u**2
end subroutine
