! bug jean 13 juillet 2011

module m 
  integer, parameter :: dp=kind(1.0d0)
  contains 

    subroutine foo(a)
      double precision, dimension(:,:,:) :: a
        do i=lbound(a,3),ubound(a,3)
           do j=lbound(a,2),ubound(a,2)        
              do k=lbound(a,1),ubound(a,1)
                 a(i,j,k)=a(i,j,k)*(I+j+k)
              end do
           end do
        end do
      end subroutine foo
      subroutine bar(b)
        real(dp), dimension(:,:) :: b
        do j=lbound(b,2),ubound(b,2)        
           do k=lbound(b,1),ubound(b,1)
              b(j,k)=b(j,k)*(j+k)
           end do
        end do
      end subroutine bar
end module

subroutine head(x,y)
 use m 
 double precision :: x(27),y
 double precision :: a(3,3,3)
 double precision :: b(9,3)
 a=reshape (x,shape(a))
 call foo(a)
 b=reshape(a,shape(b))
 call bar(b)
 y=sum(b)
end subroutine 

program main 
  double precision :: x(27)=[ (i*2.0,i=1,27)]
  double precision :: y 

  call head(x,y)
  print *,y
end program
