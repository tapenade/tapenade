module m1
 implicit none
 save
 real :: w
 real, pointer, dimension(:) :: six
end module m1

subroutine test(w , six)
 use m1
 implicit none
 w = six(2) * six(3)
end subroutine test
