program xfunc
    implicit none
    integer :: i,func
    print*, func(i)

end program xfunc

!-----

function func(i,z) result(j)
    implicit none
    integer, intent(in)  :: i
    integer, intent(out) :: z
    real                 :: j

    j =real( i**2 + i**3+z)
end function func 
