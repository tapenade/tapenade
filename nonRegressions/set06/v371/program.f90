module m2
  real :: v
end module m2

module m1
  real, dimension(:), allocatable :: t
contains
subroutine foo(n)
 use m2
 integer n
 if (n > 10) then
    allocate(t(n))
 end if
end subroutine foo
end module m1

subroutine top(x,y)
use m1
use m2
real :: x,y
do i = 1,10
   t(i) = sin(x*i)
end do
y = sum(t) * v
end subroutine top

