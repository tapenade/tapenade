module a
  integer, parameter :: ia = 2
  integer, parameter :: dp = 8
end module

module b
  use a
  real ::  xb(ia)
end module

module c
  use b
contains
      subroutine sub(x,y)
        use b
      real(dp) :: x,y
      y = x + xb(0) + func(x)
      end
end module


function test1(x)
use a; use b; use c , only : sub
 real(dp) :: x
INTERFACE 
      FUNCTION FUNC(x)
        USE a
        REAL(dp), INTENT(IN) :: x
        REAL(dp) :: func
      END FUNCTION FUNC
  END INTERFACE
call sub(x,x)
end function test1


function test2(x)
use a; use b; use c , only : sub
 real(dp) :: x
INTERFACE 
      FUNCTION FUNC(x)
        USE a
        REAL(dp), INTENT(IN) :: x
        REAL(dp) :: func
      END FUNCTION FUNC
  END INTERFACE
call sub(x,x)
end function test2

