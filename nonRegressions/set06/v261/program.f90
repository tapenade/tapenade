module m1
  integer , parameter :: iprec=selected_real_kind(10,50)
end module m1

module m2
use m1

real(iprec), save :: a, dh, c

end module m2

subroutine test(p)
use m1
use m2
real(iprec) :: p
dh = -0.38 * hv
p = dh * p
a = 3 * p
c = 4 * p

end subroutine test
