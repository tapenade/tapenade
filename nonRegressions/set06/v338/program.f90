MODULE M
  IMPLICIT NONE
  REAL, POINTER :: p1 => NULL()
  INTRINSIC NULL
END MODULE M

subroutine test(p)
   use m
   real, target :: p
   nullify(p1)
   p = p * p
   p1 => p
   p = p1 * p1
   p1 => null()
end subroutine test
