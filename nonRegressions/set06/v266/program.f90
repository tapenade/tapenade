module m0
   double precision zn
end module m0
    
module m1
   use m0, only : zn
   double precision zd
end module m1

module m2
   use m1, only : zn,zd

contains
      subroutine sub1(x,y)
      double precision x,y
      double precision f,g
      y = sin(x) + cos(x)
      zn = sqrt(y)
      zd = y * y
      return
      end subroutine

end module m2
