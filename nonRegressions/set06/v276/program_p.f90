!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4831) - 11 Apr 2013 15:38
!
PROGRAM PIG_1D_INVERSE_ADM
  IMPLICIT NONE
  INTEGER :: i, ha
  INTEGER :: mmax
  INTEGER :: nmax
  TYPE(UNKNOWNTYPE) :: heightorig(:)
  TYPE(UNKNOWNTYPE) :: height(:)
  OPEN(infile, form='FORMATTED', file='txt', status='OLD') 
  DO i=1,nmax
    READ(infile, *) height(i)
  END DO
  CLOSE(infile) 
  PRINT*, 'height', height(1:10)
  DO i=nmax+1,mmax
    height(i) = height(nmax)
  END DO
  heightorig = height
END PROGRAM PIG_1D_INVERSE_ADM

