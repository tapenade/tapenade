! Pour essayer de reproduire le bug restant
! des renommages _CD oublies dans les USE ONLY,
! qui persiste chez Jean Utke au 25/05/2011
module M1
  public
  real glob
contains
  subroutine M1S1(x,y)
    real x,y
    x = x*y
    y = y*x
  end subroutine M1S1
end module M1

module M2
  public
contains
  subroutine M2S1(u,v)
    use M1
    real u,v
    v = 2*u*glob
  end subroutine M2S1

  subroutine M2S2(a,b)
    use M1
    real a,b
    a = cos(b) + a
    b = 1/(a+b)
    call M1S1(a,b)
  end subroutine M2S2
end module M2

module M3
  use M1
  use M2, ONLY : M2S1, M2S2
contains
  subroutine top (tt)
    real tt
    call M2S2(tt,tt)
  end subroutine top
  subroutine notop (tt)
    real tt
    call M2S1(tt,tt)
  end subroutine notop
end module M3
