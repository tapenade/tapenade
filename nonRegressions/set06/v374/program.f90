
SUBROUTINE MSG1(val1, val11, val2, val22, numprocprec, numprocsuiv, &
&  etiquette, etiquetted, code)
  USE MPI
  IMPLICIT NONE
  INTEGER :: numprocprec, numprocsuiv, code, etiquette, etiquetted, err
  INTEGER, DIMENSION(2) :: req
  INTEGER, DIMENSION(2) :: reqd
  REAL :: val1, val2
  REAL :: val11, val22
  INTEGER, DIMENSION(mpi_status_size, 2) :: statut
  INTEGER, DIMENSION(mpi_status_size, 2) :: statutd
  CALL MPI_ISEND(val11, 1, mpi_real, numprocsuiv, etiquetted, &
&           mpi_comm_world, reqd(1), code)
  CALL MPI_ISEND(val1, 1, mpi_real, numprocsuiv, etiquette, &
&           mpi_comm_world, req(1), code)
  CALL MPI_IRECV(val22, 1, mpi_real, numprocprec, etiquetted, &
&           mpi_comm_world, reqd(2), code)
  CALL MPI_IRECV(val2, 1, mpi_real, numprocprec, etiquette, &
&           mpi_comm_world, req(2), code)
  CALL MPI_WAIT(reqd(1), statutd(1, 1), err)
  CALL MPI_WAIT(req(1), statut(1, 1), err)
  CALL MPI_WAIT(reqd(2), statutd(1, 2), err)
  CALL MPI_WAIT(req(2), statut(1, 2), err)
END SUBROUTINE MSG1

SUBROUTINE MSG2(val1, val11, val2, val22, numprocprec, numprocsuiv, &
&  etiquette, etiquetted, code)
  USE MPI
  IMPLICIT NONE
  INTEGER :: numprocprec, numprocsuiv, code, etiquette, etiquetted, err
  INTEGER, DIMENSION(2) :: req
  REAL :: val1, val2
  REAL :: val11, val22
  INTEGER, DIMENSION(mpi_status_size) :: statut
  INTEGER, DIMENSION(mpi_status_size) :: statutd
  CALL MPI_RECV(val22, 1, mpi_real, numprocprec, etiquetted, &
&          mpi_comm_world, statutd, code)
  CALL MPI_RECV(val2, 1, mpi_real, numprocprec, etiquette, &
&          mpi_comm_world, statut, code)
  CALL MPI_SEND(val11, 1, mpi_real, numprocsuiv, etiquetted, &
&          mpi_comm_world, statutd, code)
  CALL MPI_SEND(val1, 1, mpi_real, numprocsuiv, etiquette, &
&          mpi_comm_world, statut, code)
END SUBROUTINE MSG2

program topd
 use mpi
 implicit none
 integer, dimension( MPI_STATUS_SIZE ) :: statut
 integer, parameter :: etiquette=100
 integer, parameter :: etiquetted=101
 integer :: nb_procs,rang, &
 num_proc_precedent,num_proc_suivant,code
 real :: val, valeur
 real :: vall, valeurr

 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 num_proc_suivant=mod(rang+1,nb_procs)
 num_proc_precedent=mod(nb_procs+rang-1,nb_procs)

 val = rang+1000
 vall = rang+2000

 if (rang == 0) then
   call msg1(val,vall, valeur,valeurr, num_proc_precedent,num_proc_suivant,etiquette, etiquetted, code)
 else
   call msg2(val,vall, valeur,valeurr, num_proc_precedent,num_proc_suivant,etiquette, etiquetted, code)
 end if

 print *,'Moi, proc. ',rang,', j''ai recu valeur ',valeur,' du proc. ',num_proc_precedent
 print *,'et , proc. ',rang,', j''ai recu valeur ',valeurr,' du proc. ',num_proc_precedent

 call MPI_FINALIZE (code)
end program topd

