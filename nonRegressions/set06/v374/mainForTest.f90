program topd
 use mpi
 implicit none
 integer, dimension( MPI_STATUS_SIZE ) :: statut
 integer, parameter :: etiquette=100
 integer, parameter :: etiquetted=101
 integer :: nb_procs,rang, &
 num_proc_precedent,num_proc_suivant,code
 real :: val, valeur
 real :: vald, valeurd
 real :: val1, valeur1
 real :: val1d, valeur1d

 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 num_proc_suivant=mod(rang+1,nb_procs)
 num_proc_precedent=mod(nb_procs+rang-1,nb_procs)

 val = rang+1000
 vald = rang+2000
 val1 = rang+3000
 val1d = rang+4000

 if (rang == 0) then
   call msg1_d(val,vald, val1, val1d, valeur,valeurd, valeur1, valeur1d, &
&        num_proc_precedent,num_proc_suivant,etiquette, etiquetted, code)
 else
   call msg2_d(val,vald, val1, val1d, valeur,valeurd, valeur1, valeur1d, &
&        num_proc_precedent,num_proc_suivant,etiquette, etiquetted, code)
 end if

 print *,'Moi, proc. ',rang,', j''ai recu valeur ',valeur,' du proc. ',num_proc_precedent
 print *,'et , proc. ',rang,', j''ai recu valeur1 ',valeur1,' du proc. ',num_proc_precedent
 print *,'et , proc. ',rang,', j''ai recu valeurd ',valeurd,' du proc. ',num_proc_precedent
 print *,'et , proc. ',rang,', j''ai recu valeur1d ',valeur1d,' du proc. ',num_proc_precedent

 call MPI_FINALIZE (code)
end program topd

