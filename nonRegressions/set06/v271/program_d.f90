!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 22 May 2024 15:31
!
MODULE SIMTEST1_D
  IMPLICIT NONE
  TYPE CRTYPE
      REAL*8 :: f, fdfac, thres
      COMPLEX*16, DIMENSION(:), ALLOCATABLE :: cmob
      INTEGER*4 :: ncomp, isoterm, nx, nt
      REAL*8, DIMENSION(:, :), ALLOCATABLE :: isodat
      COMPLEX*16, DIMENSION(:, :), ALLOCATABLE :: isodatim
  END TYPE CRTYPE
  TYPE CRTYPE_D
      COMPLEX*16, DIMENSION(:), ALLOCATABLE :: cmob
      COMPLEX*16, DIMENSION(:, :), ALLOCATABLE :: isodatim
  END TYPE CRTYPE_D

CONTAINS
!  Differentiation of qcalccomplex in forward (tangent) mode:
!   variations   of useful results: qcalccomplex
!   with respect to varying inputs: *(this.isodatim) cmob
!   Plus diff mem management of: this.isodatim:in
  FUNCTION QCALCCOMPLEX_D(this, thisd, cmob, cmobd, qcalccomplex)
    USE DIFFSIZES
!  Hint: ISIZE1OFcmob should be the size of dimension 1 of array cmob
!  Hint: ISIZE1OFtemp should be the size of dimension 1 of array temp
    IMPLICIT NONE
    TYPE(CRTYPE), INTENT(IN) :: this
    TYPE(CRTYPE_D), INTENT(IN) :: thisd
    COMPLEX*16, DIMENSION(:), INTENT(IN) :: cmob
    COMPLEX*16, DIMENSION(:), INTENT(IN) :: cmobd
    INTRINSIC UBOUND
    COMPLEX*16, DIMENSION(UBOUND(cmob, dim=1)) :: qcalccomplex
    COMPLEX*16, DIMENSION(UBOUND(cmob, dim=1)) :: QCALCCOMPLEX_D
    COMPLEX*16, DIMENSION(2) :: p
    COMPLEX*16, DIMENSION(2) :: pd
    COMPLEX*16 :: l, q, temp
    COMPLEX*16 :: ld, qd, tempd
    INTEGER*4 :: i, nsites, ncomp
    INTRINSIC ABS
    INTRINSIC SUM
    INTRINSIC LOG
    REAL*8, DIMENSION(ISIZE1OFcmob) :: result1
    COMPLEX*16, DIMENSION(ISIZE1OFcmob) :: arg1
    COMPLEX*16, DIMENSION(ISIZE1OFcmob) :: arg1d
    COMPLEX*16, DIMENSION(ISIZE1OFcmob) :: arg2
    COMPLEX*16, DIMENSION(ISIZE1OFcmob) :: arg2d
    COMPLEX*16 :: temp0
    COMPLEX*16 :: temp1
    COMPLEX*16 :: temp2
    COMPLEX*16 :: temp3
    COMPLEX*16 :: temp4
    COMPLEX*16, DIMENSION(ISIZE1OFtemp) :: temp5
    COMPLEX*16, DIMENSION(ISIZE1OFtemp) :: temp6
    COMPLEX*16, DIMENSION(ISIZE1OFtemp) :: temp7
    COMPLEX*16, DIMENSION(ISIZE1OFcmob) :: temp8
    COMPLEX*16, DIMENSION(ISIZE1OFtemp) :: temp9
    COMPLEX*16, DIMENSION(ISIZE1OFtemp) :: temp10
    COMPLEX*16, DIMENSION(ISIZE1OFtemp) :: temp11
    COMPLEX*16, DIMENSION(ISIZE1OFcmob) :: temp12
    SELECT CASE (this%isoterm)
    CASE (4)
      result1 = ABS(cmob)
      IF (SUM(result1) .LE. 1.0e-16) THEN
        qcalccomplex = 0
        qcalccomplex_d = (0.0_8,0.0_8)
      ELSE
        pd = cmob*thisd%isodatim(:, 2) + this%isodatim(:, 2)*cmobd
        p = this%isodatim(:, 2)*cmob
        tempd = SUM(pd)
        temp = 1.0 + SUM(p)
        temp0 = (this%isodatim(1, 1)*cmob(1)+this%isodatim(2, 1)*cmob(2)&
&         )/(temp-1.0)
        qd = (cmob(1)*thisd%isodatim(1, 1)+this%isodatim(1, 1)*cmobd(1)+&
&         cmob(2)*thisd%isodatim(2, 1)+this%isodatim(2, 1)*cmobd(2)-&
&         temp0*tempd)/(temp-1.0)
        q = temp0
        temp0 = LOG(temp)
        temp1 = p(1)*p(2)/((temp-1.0)*(temp-1.0))
        temp2 = this%isodatim(1, 1)/this%isodatim(1, 2)
        temp3 = this%isodatim(2, 1)/this%isodatim(2, 2)
        temp4 = temp2 - temp3
        ld = temp1*temp0*((thisd%isodatim(1, 1)-temp2*thisd%isodatim(1, &
&         2))/this%isodatim(1, 2)-(thisd%isodatim(2, 1)-temp3*thisd%&
&         isodatim(2, 2))/this%isodatim(2, 2)) + temp4*(temp0*(p(2)*pd(1&
&         )+p(1)*pd(2)-temp1*2*(temp-1.0)*tempd)/(temp-1.0)**2+temp1*&
&         tempd/temp)
        l = temp4*(temp1*temp0)
        qcalccomplex_d = (0.0_8,0.0_8)
        temp4 = q/temp
        qcalccomplex_d(1) = temp4*pd(1) + p(1)*(qd-temp4*tempd)/temp + &
&         ld
        qcalccomplex(1) = p(1)*temp4 + l
        temp4 = q/temp
        qcalccomplex_d(2) = temp4*pd(2) + p(2)*(qd-temp4*tempd)/temp - &
&         ld
        qcalccomplex(2) = p(2)*temp4 - l
      END IF
    CASE (14)
      arg1d = cmob*thisd%isodatim(:, 2) + this%isodatim(:, 2)*cmobd
      arg1 = this%isodatim(:, 2)*cmob
      arg2d = cmob*thisd%isodatim(:, 5) + this%isodatim(:, 5)*cmobd
      arg2 = this%isodatim(:, 5)*cmob
      temp5 = 1.0/this%isodatim(:, 3)
      temp4 = SUM(arg1)
      temp6 = temp4**this%isodatim(:, 3)
      temp7 = (temp6+1.0)**temp5
      temp8 = this%isodatim(:, 1)*cmob/temp7
      temp9 = 1.0/this%isodatim(:, 6)
      temp3 = SUM(arg2)
      temp10 = temp3**this%isodatim(:, 6)
      temp11 = (temp10+1.0)**temp9
      temp12 = this%isodatim(:, 4)*cmob/temp11
      qcalccomplex_d = (cmob*thisd%isodatim(:, 1)+this%isodatim(:, 1)*&
&       cmobd-temp8*(temp5*(temp6+1.0)**(temp5-1)*(this%isodatim(:, 3)*&
&       temp4**(this%isodatim(:, 3)-1)*SUM(arg1d)+temp6*LOG(temp4)*thisd&
&       %isodatim(:, 3))-temp7*LOG(temp6+1.0)*temp5*thisd%isodatim(:, 3)&
&       /this%isodatim(:, 3)))/temp7 + (cmob*thisd%isodatim(:, 4)+this%&
&       isodatim(:, 4)*cmobd-temp12*(temp9*(temp10+1.0)**(temp9-1)*(this&
&       %isodatim(:, 6)*temp3**(this%isodatim(:, 6)-1)*SUM(arg2d)+temp10&
&       *LOG(temp3)*thisd%isodatim(:, 6))-temp11*LOG(temp10+1.0)*temp9*&
&       thisd%isodatim(:, 6)/this%isodatim(:, 6)))/temp11
      qcalccomplex = temp8 + temp12
    CASE DEFAULT
      STOP
    END SELECT
  END FUNCTION QCALCCOMPLEX_D

  FUNCTION QCALCCOMPLEX(this, cmob)
    USE DIFFSIZES
!  Hint: ISIZE1OFcmob should be the size of dimension 1 of array cmob
    IMPLICIT NONE
    TYPE(CRTYPE), INTENT(IN) :: this
    COMPLEX*16, DIMENSION(:), INTENT(IN) :: cmob
    INTRINSIC UBOUND
    COMPLEX*16, DIMENSION(UBOUND(cmob, dim=1)) :: qcalccomplex
    COMPLEX*16, DIMENSION(2) :: p
    COMPLEX*16 :: l, q, temp
    INTEGER*4 :: i, nsites, ncomp
    INTRINSIC ABS
    INTRINSIC SUM
    INTRINSIC LOG
    REAL*8, DIMENSION(ISIZE1OFcmob) :: result1
    COMPLEX*16, DIMENSION(ISIZE1OFcmob) :: arg1
    COMPLEX*16, DIMENSION(ISIZE1OFcmob) :: arg2
    SELECT CASE (this%isoterm)
    CASE (4)
      result1 = ABS(cmob)
      IF (SUM(result1) .LE. 1.0e-16) THEN
        qcalccomplex = 0
      ELSE
        p = this%isodatim(:, 2)*cmob
        temp = 1.0 + SUM(p)
        q = (this%isodatim(1, 1)*cmob(1)+this%isodatim(2, 1)*cmob(2))/(&
&         temp-1.0)
        l = (this%isodatim(1, 1)/this%isodatim(1, 2)-this%isodatim(2, 1)&
&         /this%isodatim(2, 2))*p(1)*p(2)*LOG(temp)/(temp-1.0)**2
        qcalccomplex(1) = q*p(1)/temp + l
        qcalccomplex(2) = q*p(2)/temp - l
      END IF
    CASE (14)
      arg1 = this%isodatim(:, 2)*cmob
      arg2 = this%isodatim(:, 5)*cmob
      qcalccomplex = this%isodatim(:, 1)*cmob/(1.0+SUM(arg1)**this%&
&       isodatim(:, 3))**(1.0/this%isodatim(:, 3)) + this%isodatim(:, 4)&
&       *cmob/(1.0+SUM(arg2)**this%isodatim(:, 6))**(1.0/this%isodatim(:&
&       , 6))
    CASE DEFAULT
      STOP
    END SELECT
  END FUNCTION QCALCCOMPLEX

!  Differentiation of solvereal in forward (tangent) mode:
!   variations   of useful results: *(this.cmob)
!   with respect to varying inputs: *(this.cmob) *(this.isodatim)
!   RW status of diff variables: this.f:(loc) this.fdfac:(loc)
!                this.thres:(loc) this.cmob:(loc) *(this.cmob):in-out
!                this.isodatim:(loc) *(this.isodatim):in
!   Plus diff mem management of: this.cmob:in this.isodatim:in
  SUBROUTINE SOLVEREAL_D(this, thisd, c)
    USE DIFFSIZES
!  Hint: ISIZE1OFDrfthis_cmob should be the size of dimension 1 of array *this%cmob
    IMPLICIT NONE
    TYPE(CRTYPE) :: this
    TYPE(CRTYPE_D) :: thisd
    COMPLEX*16, DIMENSION(:), INTENT(OUT) :: c
    COMPLEX*16, DIMENSION(ISIZE1OFDrfthis_cmob) :: tmpresult
    thisd%cmob = QCALCCOMPLEX_D(this, thisd, this%cmob, thisd%cmob, &
&     tmpresult)
    this%cmob = tmpresult
  END SUBROUTINE SOLVEREAL_D

  SUBROUTINE SOLVEREAL(this, c)
    IMPLICIT NONE
    TYPE(CRTYPE) :: this
    COMPLEX*16, DIMENSION(:), INTENT(OUT) :: c
    this%cmob = QCALCCOMPLEX(this, this%cmob)
  END SUBROUTINE SOLVEREAL

END MODULE SIMTEST1_D

