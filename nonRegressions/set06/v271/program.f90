MODULE SIMTEST1
  IMPLICIT NONE

  TYPE CRTYPE
      REAL*8 :: f, fdfac, thres
      COMPLEX*16, DIMENSION(:), ALLOCATABLE :: cmob
      INTEGER*4 :: ncomp, isoterm, nx, nt
      REAL*8, DIMENSION(:, :), ALLOCATABLE :: isodat
      COMPLEX*16, DIMENSION(:, :), ALLOCATABLE :: isodatIm
  END TYPE CRTYPE

CONTAINS
FUNCTION qCalcComplex(this, Cmob)

  TYPE(CrType), INTENT(in)                    :: this
  COMPLEX*16 , DIMENSION(:), INTENT(in)       :: Cmob
  COMPLEX*16 , DIMENSION(UBOUND(Cmob, DIM=1)) :: qCalcComplex
	
  COMPLEX*16, DIMENSION(1:2) :: P
	COMPLEX*16                 :: L, Q, temp
  INTEGER*4                  :: i, Nsites, Ncomp
  
  SELECT CASE (this%isoterm)
	CASE (4)	
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalcComplex=0
		ELSE
			P    = this%isodatIm(:,2) * Cmob
			temp = 1.0 + SUM(P)
			Q    = (this%isodatIm(1,1) * Cmob(1) + &
        this%isodatIm(2, 1) * Cmob(2)) / (temp - 1.0) 
			L    = (this%isodatIm(1, 1) / this%isodatIm(1, 2) - &
        this%isodatIm(2, 1) / this%isodatIm(2, 2)) * &
			  P(1) * P(2) * LOG(temp) / (temp - 1.0)**2     
			qCalcComplex(1) = Q * P(1) / temp + L
			qCalcComplex(2) = Q * P(2) / temp - L
		END IF

	CASE (14)	
		qCalcComplex = this%isodatIm(:, 1) * Cmob / (1.0 + &
      (SUM(this%isodatIm(:, 2) * Cmob)**(this%isodatIm(:, 3))))** &
      (1.0 / this%isodatIm(:, 3)) + this%isodatIm(:, 4) * Cmob / &
      (1.0 + (SUM(this%isodatIm(:, 5) * Cmob)**(this%isodatIm(:, 6))))** &
      (1.0 / this%isodatIm(:, 6))	

	CASE DEFAULT
    STOP
  END SELECT

END FUNCTION qCalcComplex 

  SUBROUTINE SOLVEREAL(this, c)
    IMPLICIT NONE
    TYPE(CrType) :: this
    COMPLEX*16, DIMENSION(:), INTENT(OUT) :: c
    this%cmob = QCALCCOMPLEX(this, this%cmob)
  END SUBROUTINE SOLVEREAL

END MODULE SIMTEST1
