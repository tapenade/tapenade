module m
implicit none
integer, parameter, public :: kind=8
real(kind), parameter :: pi = 3.14159265358979

contains 

subroutine foo()
 implicit none
 print *,pi
end subroutine 

end module 

module m2
 use m, only: pii => pi
 implicit none
 public
 real, private :: r
 real, parameter :: zero = 0.0
end module 

module m3
 implicit none
 private
 real mm3
end module

program p 
  use m
  use m2, only: piii => pii, zero
  use m3
  implicit none
  call foo()
  print *,'pi ', pi
  print *,'piii ', piii
end program
