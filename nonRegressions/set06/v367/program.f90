! bug jean 14 juillet 2011
! a modifier pour generer des dummyzerodiff

module m0 
  real :: g1,g2
 type t0
   integer :: i 
   real :: r
 end type
end module 

module m1
 use m0
 type t1
  type(t0) :: a_T0
  real :: r
end type
 type(t1), allocatable :: m1_t1(:)

contains 

  subroutine foo1(t1_1,t1_2)
   type (t1) :: t1_1,t1_2
   call foo(t1_1,t1_2)
 end subroutine 

 subroutine foo2(t1_1,t1_2)
   type (t1) :: t1_1,t1_2
   call foo(t1_1,t1_2)
 end subroutine 

  subroutine foo(t1_1,t1_2)
   type (t1) :: t1_1,t1_2
   g1=t1_1%a_T0%r
   t1_1%a_T0%r=0.0
   g2=t1_2%a_T0%r
   t1_2%a_T0%r=0.0
 end subroutine 
end module 


subroutine head(x,y) 
  use m1
  type(t1) :: t1_pair(2)
  t1_pair(1)%r=x
  t1_pair(1)%a_T0%r=t1_pair(1)%r
  t1_pair(2)%r=t1_pair(1)%a_T0%r
  t1_pair(2)%a_T0%r=t1_pair(2)%r
  call foo1(t1_pair(1),t1_pair(2))
  y=t1_pair(2)%r*g1*g2
  g1=0
  g2=0
  if (allocated(m1_t1)) then 
    call foo2(t1_pair(2),m1_t1(1))
  end if
end subroutine 

program main 
 use m1
 real :: x,y 
 x=2.0
 call head(x,y) 

 print *,y
end program
  
