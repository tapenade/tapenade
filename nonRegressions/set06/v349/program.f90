! fortran95/2003 page 146

module a
 implicit none
 real :: s
 real :: t
 contains
 subroutine init()
    s = 1.0
    t = 2.0
 end subroutine init
end module 

program p 
 use a, only : s,init
 use a, bs => s
 implicit none
 call init()
 print *,s
 print *,bs
 print *,t
end program
