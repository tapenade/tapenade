! bug jean 13 juillet 2011

module m 

  integer, parameter :: dp=kind(1.0d0)
 contains 

 subroutine foo(a)
   double precision :: a(4)
    a(2)=a(1)
    a(4)=a(3)
 end subroutine 
end module 

subroutine head(x,y)
  use m
  real(dp) :: x,y, p,q
  real(dp) :: a(4)
  a=(/x,y,p,q/)
  call foo(a)
  y=a(2)
end subroutine 

program main 
  double precision :: x,y 
  x=2.0 
  call head(x,y)
  print *,y
end program
  
