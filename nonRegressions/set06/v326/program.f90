MODULE SIMTEST1
  IMPLICIT NONE

  TYPE CRTYPE
      REAL*8 :: f, fdfac, thres
      REAL*8, DIMENSION(:), ALLOCATABLE :: cmob
      INTEGER*4 :: ncomp, isoterm, nx, nt
      REAL*8, DIMENSION(:, :), ALLOCATABLE :: isodat
  END TYPE CRTYPE

  PRIVATE qcalc

CONTAINS

FUNCTION qCalc(this, Cmob)

  TYPE(CrType), INTENT(in)               :: this
  REAL*8, DIMENSION(:), INTENT(in)       :: Cmob
	REAL*8, DIMENSION(UBOUND(Cmob, DIM=1)) :: qCalc
	
  REAL*8, DIMENSION(1:2) :: P
	REAL*8	               :: L, Q, temp
  INTEGER*4              :: i, Nsites, Ncomp

  
  SELECT CASE (this%isoterm)

	
	CASE (1)	
		qCalc = this%isodat(:, 1) * Cmob


	CASE (2)	
		qCalc = this%isodat(:, 1) * Cmob / (1.0 + this%isodat(:, 2) * Cmob)

    
	CASE (3)	
		qCalc = this%isodat(:, 1) * Cmob / (1.0 + SUM(this%isodat(:, 2) * Cmob))


	CASE (4)	
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalc=0
		ELSE
			P    = this%isodat(:,2) * Cmob
			temp = 1.0 + SUM(P)
			Q    = (this%isodat(1,1) * Cmob(1) +        this%isodat(2, 1) * Cmob(2)) / (temp - 1.0) 
			L    = (this%isodat(1, 1) / this%isodat(1, 2) -         this%isodat(2, 1) / this%isodat(2, 2)) * P(1) * P(2) * LOG(temp) / (temp - 1.0)**2   
			qCalc(1) = Q * P(1) / temp + L
			qCalc(2) = Q * P(2) / temp - L
		END IF


  CASE (5)	
		qCalc =       this%isodat(:, 1) * Cmob / (1.0 + this%isodat(:, 2) * Cmob) + this%isodat(:, 3) * Cmob / (1.0 + this%isodat(:, 4) * Cmob)


  CASE (6)
		qCalc =      this%isodat(:, 1) * Cmob / (1.0 + SUM(this%isodat(:, 2) * Cmob)) +     this%isodat(:, 3) * Cmob / (1.0 + SUM(this%isodat(:, 4) * Cmob))


  CASE (24)
    Ncomp = SIZE(this%isodat, 1)
    DO i = 1, Ncomp 
		  qCalc(i) = &
        this%isodat(i, 1) * Cmob(i) / &
        (1.0 + SUM(this%isodat(i, 2 : Ncomp + 1) * Cmob)) + &
        this%isodat(i, Ncomp + 2) * Cmob(i) / &
        (1.0 + SUM(this%isodat(i, Ncomp + 3 : 2*Ncomp + 2) * Cmob))
    END DO


  CASE (21)	
		qCalc = &
      this%isodat(:, 1) * Cmob / (1.0 + this%isodat(:, 2) * Cmob) + &
			this%isodat(:, 3) * Cmob / (1.0 + this%isodat(:, 4) * Cmob) + &
      this%isodat(:, 5) * Cmob / (1.0 + this%isodat(:, 6) * Cmob)


  CASE (22)
		qCalc = &
      this%isodat(:, 1) * Cmob / (1.0 + SUM(this%isodat(:, 2) * Cmob)) + &
      this%isodat(:, 3) * Cmob / (1.0 + SUM(this%isodat(:, 4) * Cmob)) + &
      this%isodat(:, 5) * Cmob / (1.0 + SUM(this%isodat(:, 6) * Cmob))


  CASE (7)
    Nsites = (SIZE(this%isodat, 2))/2
    qCalc = 0.0
    DO i = 0, Nsites - 1
		  qCalc = qCalc + this%isodat(:,  2*i + 1) * Cmob /  & 
        (1.0 + this%isodat(:, 2*i + 2) * Cmob)
    END DO


  CASE (20)
    Nsites = (SIZE(this%isodat, 2))/2
    qCalc = 0.0
    DO i = 0, Nsites - 1
		  qCalc = qCalc + this%isodat(:,  2*i + 1) * Cmob /  & 
        (1.0 + SUM(this%isodat(:, 2*i + 2) * Cmob))
    END DO


	CASE (8) 
		qCalc = ( this%isodat(:, 1) / (2*this%isodat(:, 2))) * &
      LOG( (1 + this%isodat(:, 3) * Cmob * EXP(this%isodat(:, 2))) /&
			(1.0 + this%isodat(:, 3) * Cmob * EXP(-this%isodat(:, 2))) )

	
	CASE (9)	
		qCalc = this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * ((this%isodat(:, 2) * Cmob)**2) ) / &
			(1.0 + 2*this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * ((this%isodat(:, 2) * Cmob)**2) )


	CASE (10)	
		qCalc = this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * ((this%isodat(:, 2) * Cmob)**2) ) / &
			(1.0 + 2*this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * ((this%isodat(:, 2) * Cmob)**2) ) + &
			this%isodat(:, 4) * (this%isodat(:, 5) * Cmob + this%isodat(:, 6) * &
      ((this%isodat(:, 5) * Cmob)**2) ) / (1.0 + 2*this%isodat(:, 5) * Cmob + &
      this%isodat(:, 6) * ((this%isodat(:, 5) * Cmob)**2) )

 
	CASE (11)	
		qCalc = this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * Cmob**2) / (1 + this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * Cmob**2)


	CASE (12)	
		qCalc = this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * Cmob(1) * Cmob(2) + 2*this%isodat(:, 4) * Cmob**2) / &
			(1.0 + this%isodat(1, 2) * Cmob(1) + this%isodat(2, 2) * Cmob(2) + &
      this%isodat(1, 3) * Cmob(1) * Cmob(2) + this%isodat(1, 4) * Cmob(1)**2 + &
      this%isodat(2, 4) * Cmob(2)**2)				

	CASE (23)	
		qCalc = &
      this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * Cmob(1) * Cmob(2) + 2*this%isodat(:, 4) * Cmob**2) / &
			(1.0 + this%isodat(1, 2) * Cmob(1) + this%isodat(2, 2) * Cmob(2) + &
      this%isodat(1, 3) * Cmob(1) * Cmob(2) + this%isodat(1, 4) * Cmob(1)**2 + &
      this%isodat(2, 4) * Cmob(2)**2) + &
      this%isodat(:, 5) * ( this%isodat(:, 6) * Cmob + &
      this%isodat(:, 7) * Cmob(1) * Cmob(2) + 2*this%isodat(:, 8) * Cmob**2) / &
			(1.0 + this%isodat(1, 6) * Cmob(1) + this%isodat(2, 6) * Cmob(2) + &
      this%isodat(1, 7) * Cmob(1) * Cmob(2) + this%isodat(1, 8) * Cmob(1)**2 + &
      this%isodat(2, 8) * Cmob(2)**2)

		
	CASE (13)	
		qCalc = this%isodat(:, 1) * Cmob / ( 1.0 + (SUM(this%isodat(:,2) * Cmob)** &
      (this%isodat(:, 3))))**(1.0/this%isodat(:, 3) )

	
	CASE (14)	
		qCalc = this%isodat(:, 1) * Cmob / (1.0 + &
      (SUM(this%isodat(:, 2) * Cmob)**(this%isodat(:, 3))))** &
      (1.0 / this%isodat(:, 3)) + this%isodat(:, 4) * Cmob / &
      (1.0 + (SUM(this%isodat(:, 5) * Cmob)**(this%isodat(:, 6))))** &
      (1.0 / this%isodat(:, 6))	

	
	CASE(15)	
		qCalc = this%isodat(:, 1) * Cmob**(this%isodat(:, 2))
	
		
	CASE (16)	
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalc = 0.0
		ELSE
		  temp  = SUM( this%isodat(:,2) * Cmob )
		  qCalc = this%isodat(:, 1) * this%isodat(:, 2) * Cmob * ( 1.0 - &
        EXP( -(temp**(this%isodat(:, 3)))) ) / temp
		END IF

	
	CASE (17)	
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalc = 0.0
		ELSE
		  temp  = SUM( this%isodat(:, 2) * Cmob )
		  qCalc = this%isodat(:, 1) * this%isodat(:, 2) * Cmob * ( 1.0 - &
        EXP(-temp) ) / temp
		END IF


	CASE (18) 
		qCalc = ( this%isodat(:, 1)* this%isodat(:, 2)*Cmob** &
      (this%isodat(:, 3)) ) / ( 1.0 + SUM(this%isodat(:,2)*Cmob** &
      (this%isodat(:, 3))) ) 
	
	CASE (19) 
		temp  = SUM(this%isodat(:, 2) * Cmob)
		qCalc = ( this%isodat(:,1) * Cmob / temp ) * &
      ( (temp**(this%isodat(:,3))) / (1.0 + temp**(this%isodat(:, 3))) ) 


	CASE DEFAULT
    STOP

 
  END SELECT


END FUNCTION qCalc    


  SUBROUTINE SOLVEREAL(this, c)
    IMPLICIT NONE
    TYPE(CrType) :: this
    REAL*8, DIMENSION(0:, :), INTENT(OUT) :: c
    this%cmob = QCALC(this, this%cmob)
  END SUBROUTINE SOLVEREAL

END MODULE SIMTEST1
