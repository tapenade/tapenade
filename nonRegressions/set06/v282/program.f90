subroutine filter_core(ndata,mm,nopt,npars,compresi,pp,xx,activepars,initpars,rvs1,ms,vs,wts,&
	fmean,fvar,fresi,ll,eflag)
implicit none
real(8),parameter :: twopi = 6.283185307179586_8;
integer,intent(in) :: ndata
integer,intent(in) :: mm
integer,intent(in) :: nopt
integer,intent(in) :: npars
logical,intent(in) :: compresi
real(8),dimension(nopt),intent(in) :: pp
real(8),dimension(ndata),intent(in) :: xx
logical,dimension(npars),intent(in) :: activepars
real(8),dimension(npars),intent(in) :: initpars
real(8),dimension(0:ndata,1:mm),intent(in) :: rvs1
real(8),dimension(0:ndata,1:mm),intent(out) :: ms
real(8),dimension(0:ndata,1:mm),intent(out) :: vs
real(8),dimension(0:ndata,1:mm),intent(out) :: wts
real(8),dimension(ndata),intent(out) :: fmean
real(8),dimension(ndata),intent(out) :: fvar
real(8),dimension(ndata),intent(out) :: fresi
real(8),intent(out) :: ll
integer,intent(out) :: eflag
real(8),dimension(npars) :: pars
real(8),dimension(mm) :: zz_sample,omega
real(8) :: zz,ret,dret,ddret,pad,sd,svar,nfac,sumomega,retr,retl
real(8) :: mmean,mvar,dir,zztest,rettest,resiwt,predmean,predvar,predz,uval
integer :: t,i,iter
real(8),dimension(0:ndata,1:mm) :: tmp
eflag = 0;
call pars_cv(nopt,pp,npars,pars,activepars,initpars);

vs(0,:) = pars(3)**2/(1.0_8-pars(1)**2);
wts(0,:) = 1.0_8/(1.0_8*mm);

do t=1,ndata
   call mix_mean_var(mm,ms(t-1,:),vs(t-1,:),wts(t-1,:),mmean,mvar)
   fmean(t) = mmean;
   fvar(t) = mvar;
   zz = mmean;
   sd = (1.0_8/sqrt(-ddret))*5.0_8
   omega(1) = exp(ret + 0.5_8*(zz_sample(1)-zz)**2/svar - nfac);

   do i=1,mm
 !! pb ici avec exp
          tmp(t-1,:) = exp(ms(t-1,:)/vs(t-1,:))
          resiwt = tmp(t-1,1)
	  fresi(t) = fresi(t) + uval*resiwt;
   enddo
   fresi(t) = fresi(t)/(1.0_8*mm);	

   sumomega = sum(omega);
   wts(t,:) = omega/sumomega;
   vs(t,:) = pars(3)**2*(1.0_8-pars(4)**2);
   ll = ll + log(sumomega/(1.0_8*mm))
enddo
end subroutine
