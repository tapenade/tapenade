MODULE simtest1
IMPLICIT NONE

TYPE CrType
  REAL*8 ::F
  REAL*8, DIMENSION(:, :), ALLOCATABLE :: Cinj          
  INTEGER*4 :: N
END TYPE CrType

CONTAINS

SUBROUTINE solveReal(this, C, indexLim)
	
  IMPLICIT NONE
  TYPE(CrType)               , INTENT(in)           :: this
  REAL*8, DIMENSION(0 :, :)  , INTENT(out)          :: C
  REAL*8, DIMENSION(0 :, :)  , INTENT(in), OPTIONAL :: indexLim 
  INTEGER                            ::	allocstat, j, n
  REAL(8), DIMENSION(:), ALLOCATABLE ::  CNew
	 
  ALLOCATE( CNew(1 : this%N), stat = allocstat)
  
	C     = this%Cinj(0:this%N, 1:this%N)

  IF (PRESENT(indexLim)) THEN
    space_integration1: DO j = 1, this%N
      time_integration1: DO n = INT(indexLim(j, 1)), INT(indexLim(j, 2))
			  CNew    = C(n, :)
			  C(n, :) = CNew - this%F*( CNew + this%F*(CNew) ) 
		  END DO time_integration1
    END DO space_integration1
  ENDIF

  DEALLOCATE(CNew, stat = allocstat)
	    
END SUBROUTINE solveReal

END MODULE simtest1

