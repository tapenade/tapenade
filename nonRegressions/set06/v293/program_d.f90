!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (develop) - 25 Jul 2023 13:34
!
!  Differentiation of test_contain in forward (tangent) mode:
!   variations   of useful results: f
!   with respect to varying inputs: q
!   RW status of diff variables: f:out q:in
SUBROUTINE TEST_CONTAIN_D(n, q, qd, f, fd)
  IMPLICIT NONE
  INTEGER :: n
  INTEGER :: i
  REAL, INTENT(IN) :: q(n)
  REAL, INTENT(IN) :: qd(n)
  REAL, INTENT(OUT) :: f
  REAL, INTENT(OUT) :: fd
! local
  REAL :: q2(n)
  REAL :: q2d(n)
  q2d = 0.0
  CALL SUB_D()
  f = 0.
  fd = 0.0
  DO i=1,n
    fd = fd + q2d(i)
    f = f + q2(i)
  END DO
  WRITE(*, *) f

CONTAINS
!  Differentiation of sub in forward (tangent) mode:
!   variations   of useful results: q2
!   with respect to varying inputs: q
  SUBROUTINE SUB_D()
    IMPLICIT NONE
    INTRINSIC SQRT
    REAL :: temp
    q2d = 0.0
    DO i=1,n
      temp = SQRT(q(i))
      IF (q(i) .EQ. 0.0) THEN
        q2d(i) = 0.0
      ELSE
        q2d(i) = qd(i)/(2.0*temp)
      END IF
      q2(i) = temp
    END DO
  END SUBROUTINE SUB_D

  SUBROUTINE SUB()
    IMPLICIT NONE
    INTRINSIC SQRT
    DO i=1,n
      q2(i) = SQRT(q(i))
    END DO
  END SUBROUTINE SUB

END SUBROUTINE TEST_CONTAIN_D

