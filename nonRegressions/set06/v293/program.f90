subroutine test_contain(n,q,f)
  integer :: n
  integer :: i
  real, intent(in) :: q(n)
  real, intent(out):: f
! local
  real :: q2(n)

  call sub

  f = 0.
  do i=1,n
     f = f + q2(i)
  end do

  write(*,*) f

  contains

     subroutine sub
       do i=1,n
          q2(i) = sqrt(q(i))
       end do
     end subroutine sub

  end subroutine test_contain
