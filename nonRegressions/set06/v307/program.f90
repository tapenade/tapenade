Module mo
   IMPLICIT NONE
   INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(10,50)
CONTAINS

   real(wp) function dnrm2(n,x)
      integer n
      real(wp) x(n), scale
      scale = 0.0_wp
      scale = max(scale, abs(x(n)))
      return
   end function dnrm2

End Module mo

MODULE rksuite_90
  USE mo
  IMPLICIT NONE

CONTAINS
  SUBROUTINE method_const(rk_method, c, ptr, cost, max_st, cdiff)
    INTEGER, INTENT(in) :: rk_method
    REAL(kind=wp), INTENT(out) :: c(13)
    INTEGER, INTENT(out) :: ptr(13)
    REAL(kind=wp), INTENT(out) :: cost, cdiff
    INTEGER, INTENT(out) :: max_st
    INTEGER :: i
    REAL(kind=wp), PARAMETER :: fivepc=0.05_wp

    ! ICI_BUG_TYPE
    ! car max n'est pas definie dans les librairies
    ! le type resultat calcule' est real dans l'appel dans dnrm2
    max_st = MIN(8,MAX(1,INT(fivepc*cost)))
    DO i = 1, 10 - 1
      cdiff = MIN( cdiff, MINVAL( &
           ABS((c(i)-c(i+1:10))), &
           mask=(c(i)-c(i+1:10)/=0)) )
    END DO

  END SUBROUTINE method_const

! si on enleve step_r1, pas de bug sur le type de y1

  SUBROUTINE step_r1(y,stages,htry,y_new,errest,err,hmin)

    REAL(kind=wp) :: err, htry 
    REAL(kind=wp), INTENT(in), OPTIONAL :: hmin 
    REAL(kind=wp), DIMENSION(:) :: y, errest, y_new 
    REAL(kind=wp), DIMENSION(:,:), INTENT(out) :: stages       
    INTEGER :: i
    LOGICAL :: cutbak, main
    INTRINSIC         abs, max, sign
    REAL(kind=wp), DIMENSION(:), POINTER :: weights, thresh
    INTEGER, DIMENSION(:), POINTER :: ptr                 
    REAL(kind=wp), PARAMETER :: half=0.5_wp

      IF (cutbak) weights = MAX(thresh,ABS(y))
      IF (i<=3 .AND. ABS(htry)>hmin) THEN
              IF (cutbak) THEN
                IF (ABS(htry)<=hmin) THEN
                  htry = SIGN(hmin,htry); cutbak = .FALSE.
                END IF
              END IF
      END IF
      IF (main) weights = MAX(half*(ABS(y)+ABS(y_new)),thresh)
      err = MAXVAL(ABS(errest/weights))          

  END SUBROUTINE step_r1

END MODULE rksuite_90
