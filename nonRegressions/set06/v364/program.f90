! bug jean 14 juillet 2011

module m1
  type m1t
    real :: value
    character(len=4) :: name
    type(m1t), pointer :: next => NULL()
  end type
end module 


SUBROUTINE test(val, value, name)
    use m1
    type(m1t), pointer :: value
    real :: val
    character(len=4) :: name
    do while(associated(value))
       if (name.eq.trim(value%name)) then
          val = value%value
          return
       end if
       value=>value%next
    end do 

END SUBROUTINE test

