!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.3 (r3320M) - 20 Jan 2010 11:37
!
!  Differentiation of test1 in reverse (adjoint) mode:
!   gradient     of useful results: a b
!   with respect to varying inputs: a b
!   RW status of diff variables: a:incr b:in-out
SUBROUTINE TEST1_B(a, ab, b, bb)
  IMPLICIT NONE
  REAL, DIMENSION(0:, :) :: a
  REAL, DIMENSION(0:, :) :: ab
  REAL, DIMENSION(0:, :) :: b
  REAL, DIMENSION(0:, :) :: bb
  INTEGER :: i
  DO i=200,1,-1
    ab(i, 1) = ab(i, 1) + bb(i, 1)
  END DO
END SUBROUTINE TEST1_B

