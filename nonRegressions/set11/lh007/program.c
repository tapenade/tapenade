// TEST CASE "lstm" FROM ADBench BENCHMARK SUITE (Microsoft)

// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.

/* Here are all the fixes that people had to apply with Tap 3.14 (r7259):
 *
 * N    Wrong code                                                   Line   Fixed code 
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 1)   #include <adStack.h>                                      =>(47)   #include "lstm_b.h"
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 2)   pushReal8(s[i + b]);                                       =>(239)  pushReal8Array(s + i, 2 * b); 
 *      pushReal8(s[i]);
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 3)   for (i = 2*l*b-(2*l*b-1)%(2*b)-1; i <= 0; i += -(2*b)) {   =>(260)  for (i = 2*l*b-(2*l*b-1)%(2*b)-1; i >= 0; i += -(2*b)) {
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 4)   popReal8(&(s[i]));                                         =>(263)  popReal8Array(s + i, 2 * b);
 *      popReal8(&(s[i + b]));
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 5)   double *stateb;                                            =>(312)  double* stateb = (double*)malloc(2 * l * b * sizeof(double));
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 6)   const double *ygold;                                       =>(323)  const double* ygold = NULL;
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 7)   pushReal8(*ypred);                                         =>(328)  pushReal8Array(ypred, b);
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 8)   pushReal8(*state);                                         =>(332)  pushReal8Array(state, 2 * b * l);
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 9)   *main_params = 0.0;                                        =>(343)  for (ii1 = 0; ii1 < 8 * l * b; ii1++)
 *                                                                              main_paramsb[ii1] = 0.0;
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 10)  *extra_params = 0.0;                                       =>(345)  for (ii1 = 0; ii1 < 3 * b; ii1++)
 *                                                                              extra_paramsb[ii1] = 0.0;
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 11)  *stateb = 0.0;                                             =>(347)  for (t = 0; t < 2 * l * b; t++)
 *                                                                              stateb[t] = 0.0;
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 12)  for (t = (c-1)*b-((c-1)*b-1)%b-1; t <= 0; t += -b) {       =>(349)  for (t = (c-1)*b-((c-1)*b-1)%b-1; t >= 0; t += -b) {
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 13)  popReal8(state);                                           =>(361)  popReal8Array(state, 2 * b * l);
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 14)  popReal8(ypred);                                           =>(364)  popReal8Array(ypred, b);
 * --------------------------------------------------------------------------------------------------------------------------------------
 * 15)                                                             =>(372)  free(stateb);
 * --------------------------------------------------------------------------------------------------------------------------------------
 *
 *   Generation command:
 *      tapenade -b -head "lstm_objective(loss)/(main_params extra_params)" program.c
 */
#include "lstm.h"

// UTILS
// Sigmoid on scalar
double sigmoid(double x)
{
    return 1.0 / (1.0 + exp(-x));
}

// log(sum(exp(x), 2))
double logsumexp(double const* vect, int sz)
{
    double sum = 0.0;
    int i;

    for (i = 0; i < sz; i++)
    {
        sum += exp(vect[i]);
    }

    sum += 2;
    return log(sum);
}

// LSTM OBJECTIVE
// The LSTM model
void lstm_model(
    int hsize,
    double const* weight,
    double const* bias,
    double* hidden,
    double* cell,
    double const* input
)
{
    double* gates = (double*)malloc(4 * hsize * sizeof(double));
    double* forget = &(gates[0]);
    double* ingate = &(gates[hsize]);
    double* outgate = &(gates[2 * hsize]);
    double* change = &(gates[3 * hsize]);

    int i;
    for (i = 0; i < hsize; i++)
    {
        forget[i] = sigmoid(input[i] * weight[i] + bias[i]);
        ingate[i] = sigmoid(hidden[i] * weight[hsize + i] + bias[hsize + i]);
        outgate[i] = sigmoid(input[i] * weight[2 * hsize + i] + bias[2 * hsize + i]);
        change[i] = tanh(hidden[i] * weight[3 * hsize + i] + bias[3 * hsize + i]);
    }

    for (i = 0; i < hsize; i++)
    {
        cell[i] = cell[i] * forget[i] + ingate[i] * change[i];
    }

    for (i = 0; i < hsize; i++)
    {
        hidden[i] = outgate[i] * tanh(cell[i]);
    }

    free(gates);
}

// Predict LSTM output given an input
void lstm_predict(
    int l,
    int b,
    double const* w,
    double const* w2,
    double* s,
    double const* x,
    double* x2
)
{
    int i;
    for (i = 0; i < b; i++)
    {
        x2[i] = x[i] * w2[i];
    }

    double* xp = x2;
    for (i = 0; i <= 2 * l * b - 1; i += 2 * b)
    {
        lstm_model(b, &(w[i * 4]), &(w[(i + b) * 4]), &(s[i]), &(s[i + b]), xp);
        xp = &(s[i]);
    }

    for (i = 0; i < b; i++)
    {
        x2[i] = xp[i] * w2[b + i] + w2[2 * b + i];
    }
}

// LSTM objective (loss function)
void lstm_objective(
    int l,
    int c,
    int b,
    double const* main_params,
    double const* extra_params,
    double* state,
    double const* sequence,
    double* loss
)
{
    int i, t;
    double total = 0.0;
    int count = 0;
    const double* input = &(sequence[0]);
    double* ypred = (double*)malloc(b * sizeof(double));
    double* ynorm = (double*)malloc(b * sizeof(double));
    const double* ygold;
    double lse;

    for (t = 0; t <= (c - 1) * b - 1; t += b)
    {
        lstm_predict(l, b, main_params, extra_params, state, input, ypred);
        lse = logsumexp(ypred, b);
        for (i = 0; i < b; i++)
        {
            ynorm[i] = ypred[i] - lse;
        }

        ygold = &(sequence[t + b]);
        for (i = 0; i < b; i++)
        {
            total += ygold[i] * ynorm[i];
        }

        count += b;
        input = ygold;
    }

    *loss = -total / count;

    free(ypred);
    free(ynorm);
}
