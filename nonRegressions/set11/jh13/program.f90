subroutine foo(a, b, c, d, y, z, n)
implicit none
integer i, n
real a(n), b(n), c(n), d(n), y, z

!$omp parallel do private(z)
do i=1,n
  z = a(i)
  b(i) = z
end do

!$omp parallel do lastprivate(y)
do i=1,n
  y = a(i)
end do

!$omp parallel do firstprivate(y)
do i=1,n
  c(i) = c(i) + y
end do

!$omp parallel do reduction(+:y)
do i=1,n
  y = y + d(i)
end do

!$omp parallel do reduction(+:y) private(z)
do i=1,n
  z = y + d(i)
  y = z
end do

end subroutine
