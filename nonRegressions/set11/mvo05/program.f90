! The adjoint of this code does
! CALL PUSH/POPREAL4ARRAY(x, r8/4)
! which is not wrong, but not nice.
! The recommended fix is to patch the source e.g.
! sed -e "s/selected_real_kind(12)/8 !!selected_real_kind(12)/g"
subroutine sub1(x,y,a)
  integer, parameter :: r8 = selected_real_kind(12)
  implicit none
  real(r8) x,y
  real(r8) :: a
  a = x * a	
  if (x<0) x=-x
  a = x * a**2
  return
end subroutine sub1
