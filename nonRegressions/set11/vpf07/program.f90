
MODULE moo

IMPLICIT NONE

interface 
   subroutine f1(x)
     real x
   end subroutine f1
end interface

END MODULE moo


subroutine foo(x)
 use moo
 real x
 call f1(x)
end subroutine foo
