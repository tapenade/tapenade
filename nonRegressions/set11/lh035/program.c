//Another instance of the TreeGen bug (lost arrow carry, label 100...)
// this time in C.

int foo(x,y) {
  float *x, *y ;
  int N = 20 ;

  bar(N, x, y) ;
  float maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = max(maxError, abs(y[i]-4.0f));

  *y = maxError ;
}
