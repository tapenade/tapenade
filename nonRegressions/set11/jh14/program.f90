
! Test various OMP clauses, in split (nocheckpoint) and joint (checkpoint) modes
! Add trailing code to prevent simplifications on the adjoint code.
! Warning: due to "private", the code's result is different with OpenMP turned off.
subroutine foo_all(a1,a2,a3,b1,b2,y1,y2,y3,y4,y5,n,res)
  integer n
  real, dimension(n) :: a1,a2,a3,b1,b2
  real :: y1,y2,y3,y4,y5,res
  res = 0
  call foo_private(a1, b2, y1, n)
  call foo_firstprivate(a2, y2, n)
  call foo_reduction(a1, y3, n)
  call foo_lastprivate(a1, y4, n)
  call foo_firstlastprivate(a3, b1, y5, n)
  res = res + SUM(a1) + SUM(a2) + SUM(a3)
  res = res + SUM(b1) + SUM(b2) + y1 + y2 + y3 + y4 + y5
  !$AD NOCHECKPOINT
  call foo_private(a1, b2, y1, n)
  !$AD NOCHECKPOINT
  call foo_firstprivate(a2, y2, n)
  !$AD NOCHECKPOINT
  call foo_reduction(a1, y3, n)
  !$AD NOCHECKPOINT
  call foo_lastprivate(a1, y4, n)
  !$AD NOCHECKPOINT
  call foo_firstlastprivate(a3, b1, y5, n)
  res = res + SUM(a1) + SUM(a2) + SUM(a3)
  res = res + SUM(b1) + SUM(b2) + y1 + y2 + y3 + y4 + y5
  res = res*res
end subroutine foo_all

subroutine foo_private(a, b, y, n)
  implicit none
  integer i, n
  real a(n), b(n), y
  !$omp parallel do private(y)
  do i=1,n
    y = a(i)
    b(i) = y
  end do
end subroutine

subroutine foo_firstprivate(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$omp parallel do firstprivate(y)
  do i=1,n
    a(i) = y
    y = 0
  end do
end subroutine

subroutine foo_reduction(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$omp parallel do reduction(+:y)
  do i=1,n
    y = y + a(i)
  end do
end subroutine

subroutine foo_lastprivate(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$omp parallel do lastprivate(y)
  do i=1,n
    y = a(i)
  end do
end subroutine

subroutine foo_firstlastprivate(a, b, y, n)
  implicit none
  integer i, n
  real a(n), b(n), y
  !$omp parallel do firstprivate(y) lastprivate(y)
  do i=1,n
    a(i) = y
    y = b(i)
  end do
end subroutine

program main
  integer, parameter :: n = 10
  real, dimension(n) :: a1,a2,a3,b1,b2
  real :: y1,y2,y3,y4,y5,res
  a1 = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  a2 = (/2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9/)
  a3 = (/3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9/)
  b1 = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  b2 = (/2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9/)
  y1 = 1.1
  y2 = 1.2
  y3 = 1.3
  y4 = 1.4
  y5 = 1.5
  res = 0.0
  call foo_all(a1,a2,a3,b1,b2,y1,y2,y3,y4,y5,n,res)
  print *, 'result: ', res
end program main
