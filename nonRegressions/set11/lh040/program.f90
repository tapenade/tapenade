! various examples of precision loss in temporaries
! found by C.Kotas.

module MWP
  integer, parameter :: wp=8
end module MWP

subroutine test(x,y)
  USE MWP
  real*8 x,y
  double precision :: sum
  complex*16 a(10,10),z,t(10,10),tau(10),v
  integer i,j
  real*8 ax, ay
  complex(wp) :: u(10)

  a(:,:) = CMPLX(x,y)
  t(:,:) = CMPLX(x,y)
  tau(:) = CMPLX(x,y)
  u(:) = CMPLX(x,y)
  z = CMPLX(x,y)
  v = CMPLX(x,y)
  j = 2

  sum = 0.d0
  do i = 1, 5
     sum = sum + ABS( a( i, j ) )
  enddo

  if (i.le.10) z = z*DCONJG(a(i,i))

  t(j,i) = -tau(i) * CONJG( a(i,j) )

  ax = ABS(REAL(u(i)))
  ay = ABS(AIMAG(u(i)))

  y = REAL(ax*ay*sum + z*t(2,2))
end subroutine test
