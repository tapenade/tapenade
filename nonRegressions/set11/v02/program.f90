subroutine f(u,v)
  implicit none
  real u,v
  real :: w(3), foo, bar
  save w
  integer k
  data foo, (w(k), k=1,3), bar /5 * 2.0/
  print *, ' w '  ,w(1)
  v = u * w(1)
  u = v + u
  w(1) = u * w(1)
end subroutine 

subroutine g(u,v)
  implicit none
  real u,v
  call f(u,v)
  print *, ' 1 ', u , v
  call f(u,v)
  print *, ' 2 ', u , v
end subroutine
