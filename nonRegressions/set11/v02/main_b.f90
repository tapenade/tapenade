!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (r7174) - 23 Nov 2018 17:29
!
!  Differentiation of test as a context to call adjoint code (with options context):
PROGRAM TEST_B
  IMPLICIT NONE
  REAL :: u, v
  REAL :: ub, vb
  u = 2.0
  v = 3.0
  CALL ADCONTEXTADJ_INIT(0.87_8)
  CALL ADCONTEXTADJ_INITREAL4('u'//CHAR(0), u, ub)
  CALL ADCONTEXTADJ_INITREAL4('v'//CHAR(0), v, vb)
  CALL G_B(u, ub, v, vb)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  CALL ADCONTEXTADJ_CONCLUDEREAL4('u'//CHAR(0), u, ub)
  CALL ADCONTEXTADJ_CONCLUDEREAL4('v'//CHAR(0), v, vb)
  CALL ADCONTEXTADJ_CONCLUDE()
END PROGRAM TEST_B

