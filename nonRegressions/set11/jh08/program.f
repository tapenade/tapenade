      subroutine foo (A,B,s)
      integer i,j
      real A(20),B(20),s,p
      i = 5
      j = 0
      s = 0.0
c$omp parallel do private(i), private(p),
c$omp+ shared(a), shared(b), reduction(+:s)
      do i = 1,10
         p = A(i)*B(i)
         s = s + p
      enddo

      end
