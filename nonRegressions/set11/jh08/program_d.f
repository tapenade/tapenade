C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of foo in forward (tangent) mode (with options OpenMP):
C   variations   of useful results: s
C   with respect to varying inputs: a b
C   RW status of diff variables: s:out a:in b:in
      SUBROUTINE FOO_D(a, ad, b, bd, s, sd)
      IMPLICIT NONE
      INTEGER i, j
      REAL a(20), b(20), s, p
      REAL ad(20), bd(20), sd, pd
      i = 5
      j = 0
      s = 0.0
      sd = 0.0
C$OMP PARALLEL DO PRIVATE(i), PRIVATE(p), PRIVATE(pd), SHARED(a), SHARED(
C$OMP+  ad), SHARED(b), SHARED(bd), REDUCTION(+:s), REDUCTION(+:sd)
      DO i=1,10
        pd = b(i)*ad(i) + a(i)*bd(i)
        p = a(i)*b(i)
        sd = sd + pd
        s = s + p
      ENDDO
C
      END

