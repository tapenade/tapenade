C To test FormAD with no primal knowledge
      subroutine foo(x)
      real x
      real t(100)
      real sum

      t(:) = 2*x
      sum = 0.0
C$OMP PARALLEL DO SCHEDULE(static) REDUCTION(+:sum)
      do i = 1,20
         sum = sum + t(2*i) * t(2*i+1)
      enddo
      x = x + sum
      end
