! Bug found by C. Kotas. Due to a wrong treatment in TBR of primitives
! (e.g. complex, cmplx, sngl, dble, real, imag, conjg)
! that have a specific differentiation in ExpressionDifferentiator,
! the index "iel" was not recognized as being TBR.
    subroutine somefun(otpt,inpt,ns, nc, nt)
    implicit none
    COMPLEX(c_double_complex) :: inpt(:)
    COMPLEX(c_double_complex) :: otpt(:)
    integer :: nc, ns, nt, nel
    integer :: is, it, ic, iel
    integer :: idx
    nel = ns*nc*nt

    DO IT = 1, NT
      DO IS = 1, NS
        DO IC = 1, NC
          IEL = IS + (IC-1)*NS + (IT-1)*NS*NC
          IDX = IT + (IS-1)*NT + (IC-1)*NT*NS
          otpt(idx) = CONJG( inpt(iel) )
        ENDDO
      ENDDO
    ENDDO

    return
    end subroutine
