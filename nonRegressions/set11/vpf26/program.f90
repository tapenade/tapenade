! from set04/v030, test module procedure
MODULE INTERVAL_ARITHMETICS
       TYPE INTERVAL
              REAL LOWER, UPPER
       END TYPE INTERVAL
       TYPE IINTERVAL
              integer LOWER, UPPER
       END TYPE IINTERVAL
       INTERFACE OPERATOR (+)
              MODULE PROCEDURE IINTERVAL_ADDITION, INTERVAL_ADDITION
       END INTERFACE

CONTAINS

       subroutine interval_from_real(a,b)
         type(interval), intent(out) :: a
         real, intent(in) :: b
         a%lower = b
         a%upper = b
       end subroutine interval_from_real

END MODULE INTERVAL_ARITHMETICS

program test
    USE INTERVAL_ARITHMETICS
    IMPLICIT NONE
    TYPE (INTERVAL) :: A, B, C, D, E, F
    A%LOWER = 6.9            
    A%UPPER = 7.1
    B%LOWER = 10.9           
    B%UPPER = 11.1
    WRITE (*,*) A, B
    C = A + B                
    D = A - B
    E = A * B                
    F = A / B
    WRITE (*,*) C, D         
    WRITE (*,*) E, F

      contains
      real function fun(a,b,ia,ib)
      TYPE (INTERVAL) :: A, B
      TYPE (iINTERVAL) :: iA, iB, ic
      fun = a + b
      ic = ia + ib
      return
      end function


    END
