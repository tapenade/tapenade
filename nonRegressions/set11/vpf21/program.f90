module mo
  implicit none

  private
  type some_type
     real :: f
  end type some_type
  public some_type

  !-- overloaded operator
  public :: operator(-)
  private :: some_type_difference
  public :: operator(.GT.)
  private :: some_type_gt
  !-- interface blocks
  interface operator(-)
     module procedure some_type_difference
  end interface operator(-)
  interface operator(.GT.)
     module procedure some_type_gt
  end interface operator(.GT.)
  
contains
  function some_type_difference(x, y) result(diff)
    implicit none
    type(some_type) :: diff
    type(some_type), intent(in) :: x,y

    diff%f = x%f - y%f
  end function some_type_difference
  function some_type_gt(x, y) result(gt)
    implicit none
    logical :: gt
    type(some_type), intent(in) :: x,y

    gt = x%f .GT. y%f
  end function some_type_gt
end module mo

subroutine foo(n, x, y)
  use mo
  implicit none
  !-- arguments
  integer, intent(in) :: n
  real, intent(in) :: x(n)
  real, intent(out) :: y
  !-- local
  type(some_type) :: u, v, w


  u%f = x(1)
  v%f = x(2)

  if( u.gt.v ) then
     w = u - v
  else
     w = v - u
  endif
  y = w%f

end subroutine foo

program main
  implicit none
  integer, parameter :: n = 2
  real :: x(n), y
  integer :: i

  do i=1,n
     x(i) = real(i*i)
  end do

  call foo(n,x,y)
  print*, 'INFO::y=',y
end program main
