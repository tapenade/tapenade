C To test FormAD with no primal knowledge
      subroutine foo(x,dk)
      real x
      real t(100)
      real sum
      integer k, dk(20)

      t(:) = 2*x
      sum = 0.0
C$OMP PARALLEL DO SCHEDULE(static) REDUCTION(+:sum) PRIVATE(k)
      do i = 1,20
         k = dk(i)
         sum = sum + t(2*i+k) * t(2*i+1+k)
      enddo
      x = x + sum
      end
