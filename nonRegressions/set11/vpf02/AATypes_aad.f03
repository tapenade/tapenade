!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (feature_assocaddress) - 10 Dec 2019 11:19
!
MODULE AATYPES
  USE ISO_C_BINDING
  IMPLICIT NONE
  TYPE REAL8_DIFF
      SEQUENCE 
      DOUBLE PRECISION :: v
      DOUBLE PRECISION :: d
  END TYPE REAL8_DIFF
END MODULE AATYPES

