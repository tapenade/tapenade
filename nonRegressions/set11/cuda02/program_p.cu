//        Generated by TAPENADE     (INRIA, Ecuador team)
//  Tapenade 3.16 (feature_cuda) - 13 Sep 2021 14:49
//
/* This meaningless code only checks CUDA syntax */
#include <stdio.h>
#include <stdlib.h>

float foo0(float *aa) {
    foo2<<<4, 16>>>(aa);
}

__host__ float foo1(float *aa) {
    int Nb = 5;
    int Nt = 32;
    foo2<<<Nb, Nt>>>(aa);
}

__global__ float foo2(float *aa) {
    int ii = blockIdx.x*blockDim.x + threadIdx.x;
    foo3(*aa);
}

extern __shared__ float gg1[];

__device__ float foo3(float aa) {
    //     vv5 points to Undef
    //     vv6 points to Undef
    float vv1;
    __device__ float vv2;
    __device__ __constant__ float vv21;
    __device__ __shared__ float vv22;
    __device__ __managed__ float vv23;
    float vv4;
    __constant__ float vv31;
    __shared__ float vv42;
    __managed__ float vv43;
    float* __restrict__ vv6;
    int jj = threadIdx.x;
}

__host__ __device__ float foo4(float *aa) {}

__noinline__ __device__ float foo5(float aa) {}
