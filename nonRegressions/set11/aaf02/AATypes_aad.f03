!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (r7246) - 10 Jan 2019 13:51
!
MODULE AATYPES
  USE ISO_C_BINDING
  IMPLICIT NONE
  TYPE REAL8_DIFF
      SEQUENCE 
      REAL(kind=8) :: v
      REAL(kind=8) :: d
  END TYPE REAL8_DIFF
END MODULE AATYPES

