
! Test $AD MULTITHREAD directives
! Test PRIVATE(branch)
subroutine foo3(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$AD MULTITHREAD TANGENT private(a)
  !$Ad MultiThreaD_adj aToMIC(A)
  !$omp parallel do reduction(+:y)
  do i=1,n
     if (a(i).gt.0) then
        y = y + a(i)
     else
        y = y - a(i)
     endif
  end do
end subroutine

program main
  integer, parameter :: n = 10
  real, dimension(n) :: a
  real :: y
  a = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  y = 1.1
  call foo3(a, y, n)
  print *, 'result: ', y
end program main
