! Testing intrinsic ABS on COMPLEX. Written by Charlotte Kotas.

module myfun
  implicit none
contains

real(8) function myabs(z)
  implicit none
  complex(8), intent(in) :: z
  intrinsic real, aimag, sqrt
  myabs = sqrt( real(z)**2 + aimag(z)**2 )
  return
  end function

real(8) function somefun(z)
  implicit none
  complex(8), intent(in) :: z
  real(8) :: a, b, c, d, e 
  intrinsic real, aimag, sqrt, abs
  external mysqrt
  real(8) mysqrt
  external mymagsq
  real(8) mymagsq

  a = real(z)
  b = mymagsq(z)
  c = mysqrt(b)
  d = abs(z)
  e = abs(a)

  ! just to be sure the output depends on everything
  somefun = myabs(z) + a + b + c + d + e

  return
  end function

end module

program test
use myfun
implicit none
complex(8) z
real(8) a

z = (-1_8,-1_8)
a = somefun(z)
write(*,*)  a

stop
end program test
