!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 31 Jan 2022 18:41
!
! Testing intrinsic ABS on COMPLEX. Written by Charlotte Kotas.
MODULE MYFUN
  IMPLICIT NONE

CONTAINS
  REAL*8 FUNCTION MYABS(z)
    IMPLICIT NONE
    COMPLEX*16, INTENT(IN) :: z
    INTRINSIC REAL, AIMAG, SQRT
    myabs = SQRT(REAL(z)**2 + AIMAG(z)**2)
    RETURN
  END FUNCTION MYABS

  REAL*8 FUNCTION SOMEFUN(z)
    IMPLICIT NONE
    COMPLEX*16, INTENT(IN) :: z
    REAL*8 :: a, b, c, d, e
    INTRINSIC REAL, AIMAG, SQRT, ABS
    EXTERNAL MYSQRT
    REAL*8 :: MYSQRT
    EXTERNAL MYMAGSQ
    REAL*8 :: MYMAGSQ
    a = REAL(z)
    b = REAL(z)**2 + AIMAG(z)**2
    c = SQRT(b)
    d = SQRT(REAL(z)**2 + AIMAG(z)**2)
    IF (a .GE. 0.) THEN
      e = a
    ELSE
      e = -a
    END IF
! just to be sure the output depends on everything
    somefun = MYABS(z) + a + b + c + d + e
    RETURN
  END FUNCTION SOMEFUN

END MODULE MYFUN

PROGRAM TEST
  USE MYFUN
  IMPLICIT NONE
  COMPLEX*16 :: z
  REAL*8 :: a
  z = (-1_8,-1_8)
  a = SOMEFUN(z)
  WRITE(*, *) a
  STOP
END PROGRAM TEST

