module M
      interface
          function f(t)
            real :: f
            real :: t
          end function f
      end interface

      contains
      real function fg(t)
      implicit none
      real t
      fg = t * t
      return
      end function
end module

subroutine test()
   implicit none
   real z
   z = f(z, z)
end subroutine test
