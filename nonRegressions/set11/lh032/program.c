#include <math.h>

void testfminf(float a, float *b) {
  float c = 2.5;
  a = cos(a) ;
  float r = sin(a) ;
  *b = fminf(*b,a+r) ;
  a = a*c ;
  c = c+a ;
  *b = *b * c ;
}
