/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_llhTests) - 17 Jun 2021 17:38
*/
#include <adStack.h>
#include <math.h>

/*
  Differentiation of testfminf in reverse (adjoint) mode:
   gradient     of useful results: a *b
   with respect to varying inputs: a *b
   RW status of diff variables: a:in-out b:(loc) *b:in-out
   Plus diff mem management of: b:in
*/
void testfminf_b(float a, float *ab0, float *b, float *bb) {
    float c = 2.5;
    float cb = 0.0;
    float ab;
    pushReal4(a);
    a = cos(a);
    float r;
    float rb;
    r = sin(a);
    pushReal4(*b);
    *b = (*b > a + r ? a + r : *b);
    pushReal4(a);
    a = a*c;
    pushReal4(c);
    c = c + a;
    ab = 0.0;
    cb = (*b)*(*bb);
    *bb = c*(*bb);
    popReal4(&c);
    ab = ab + cb;
    popReal4(&a);
    popReal4(b);
    rb = (*b > a + r ? *bb : 0.0);
    ab = (*b > a + r ? c*ab + *bb + cos(a)*rb : c*ab + cos(a)*rb);
    *bb = (*b > a + r ? 0.0 : *bb);
    popReal4(&a);
    ab = -(sin(a)*ab);
    *ab0 = *ab0 + ab;
}
