! bug report Thu, 26 Mar 2020

module mo1
  implicit none

  type some_type
     real :: f
  end type some_type
  public some_type

  !-- overloaded operator
  public :: operator(-)
  private :: some_type_difference
  !-- interface blocks
  interface operator(-)
     module procedure some_type_difference
  end interface operator(-)
contains
  function some_type_difference(x, y) result(diff)
    implicit none
    type(some_type) :: diff
    type(some_type), intent(in) :: x,y

    diff%f = x%f - y%f
  end function some_type_difference
end module mo1

module mo2
  use mo1
  implicit none
  type some_type2
     type(some_type) :: f
  end type some_type2
  public some_type2

  !-- overloaded operator
  public :: operator(-)
  private :: some_type2_difference
  !-- interface blocks
  interface operator(-)
     module procedure some_type2_difference
  end interface operator(-)
contains
  function some_type2_difference(x, y) result(diff)
    implicit none
    type(some_type2) :: diff
    type(some_type2), intent(in) :: x,y

    diff%f = x%f - y%f
  end function some_type2_difference
end module mo2

subroutine foo(n, x, y)
  use mo1
  use mo2
  implicit none
  !-- arguments
  integer, intent(in) :: n
  real, intent(in) :: x(n)
  real, intent(out) :: y
  !-- local
  type(some_type) :: u, v, w
  type(some_type2) :: uu, vv, ww


  u%f = x(1)
  v%f = x(2)
  uu%f = v
  vv%f = u

  w = u - v
  ww = uu - vv
  y = w%f + ww%f%f

end subroutine foo

program main
  implicit none
  integer, parameter :: n = 2
  real :: x(n), y
  integer :: i

  do i=1,n
     x(i) = real(i*i)
  end do

  call foo(n,x,y)
  print*, 'INFO::y=',y
end program main
