/* Test possible problems when the active part calls
 * an apparently passive but ReqX-active routine. */

#include <stdio.h>

void bar(float **tp1, float **tp2) {
  *tp2 = *tp1 ;
}

void foo(float **tp1, float **tp2) {
  bar(tp1,tp2) ;
}

float root(float x) {
  float temp = x*x ;
  float res = 1.0 ;
  float *ptr1 = &temp ;
  float *ptr2 = &res ;
  foo(&ptr1, &ptr2) ;
  (*ptr2) = (*ptr2)*(*ptr2) ;
  return (*ptr2) ;
}

int main() {
  float x = 2.0 ;
  float y = 3.0 ;
  y = root(x) ;
  printf("y=%f\n",y) ;
}
