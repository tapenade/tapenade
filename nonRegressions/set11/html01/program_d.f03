!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_bugFixes) -  1 Dec 2020 17:00
!
!  Differentiation of barf in forward (tangent) mode:
!   variations   of useful results: statix1 z
!   with respect to varying inputs: statix1 z
!   RW status of diff variables: statix1:in-out z:in-out
! pour tester bind(c) dans une subroutine: bind => save
SUBROUTINE BARF_D(z, zd) BIND(c)
  USE ISO_C_BINDING
  IMPLICIT NONE
  REAL(c_float) :: statix1
  REAL(c_float) :: statix1d
  COMMON /com/ statix1
  COMMON /com_d/ statix1d
  BIND(c, name='Cstatix1') :: /com/
  BIND(c, name='Cstatix1') :: /com_d/
  REAL(c_float) :: z
  REAL(c_float) :: zd
  zd = 2*z*zd + statix1d
  z = z*z + statix1
  statix1d = zd
  statix1 = z
END SUBROUTINE BARF_D

