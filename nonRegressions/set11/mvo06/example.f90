!!!tapenade -msglevel 15 -msginfile -noisize -outputlanguage fortran90 -d -tgtvarname _fw -tgtfuncname _fw -head "cost(x)>(f)" -o example example.f90

module var
  real(kind=8) :: q10
end module var
subroutine cost(n,x,m,f)
  !mvo:adressing Tapenade (AD14) message. otherwise initialisation q10b=0 is missing in backward mode
!  use var,only:q10
  implicit none
  ! arguments
  integer, intent(in) :: n, m
  real(kind=8), intent(in) :: x(n)
  real(kind=8), intent(out) :: f
  ! local
  integer :: i
  real(kind=8) :: b
  call setq10(x(1))
  f = 0.
  do i = 1, 2
     call useq10(b)
     f = f + b
  enddo
end subroutine cost
subroutine setq10(a)
  use var
  implicit none
  ! arguments
  real(kind=8), intent(in) :: a
  !
  q10 = a
end subroutine setq10
subroutine useq10(b)
  use var
  implicit none
  ! arguments
  real(kind=8), intent(out) :: b
  !
  real(kind=8) :: logq10
  !
  logq10 = log(q10+1)/10.
  b = 2*(logq10+1.)
end subroutine useq10
