c Bug found by Shreyas Gaikwad on MIT GCM
c The push/pop's of the call argument has a wrong size,
c due to wrong treatment of the case of an array cell is passed
c and acts in the callee as a pointer into the array.
      SUBROUTINE test(x)
      REAL x
      REAL T(10,20,30,40)
      REAL y

      do i1 = 1,10
         do i2 = 1,20
            do i3 = 1,30
               do i4 = 1,40
                  T(i1,i2,i3,i4) = x+i1+i2+i3+i4
               enddo
            enddo
         enddo
      enddo
      i1 = 2
      i2 = i1+1
      y =2*x
      CALL FOO(i1,i2,1,T(i1,i1,i2,1),y)
      x = y*T(2,3,4,5)
      END

      SUBROUTINE FOO(i,j,k,NT,y)
      INTEGER i,j,k
      REAL y
      INTEGER N1,N2
      PARAMETER (N1 = 5, N2 = 6)
      REAL NT(N1,N2)
      NT(2,2) = NT(1,3)*NT(3,1)
      y = y*NT(3,3)
      END
