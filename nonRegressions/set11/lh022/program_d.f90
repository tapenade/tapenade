!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 19 Jan 2024 17:05
!
!  Differentiation of rosenbrockdriver as a context to call tangent code (with options context fixinterface):
PROGRAM ROSENBROCKDRIVER_D
  IMPLICIT NONE
  REAL*8 :: array(0:9), result
  REAL*8 :: arrayd(0:9), resultd
  REAL*8 :: ROSENBROCK
  REAL*8 :: ROSENBROCK_D
  array = (/-0.37211100d0, 0.26423106d0, -0.18252774d0, -0.7368198d0, -&
&   0.44030386d0, -0.15214427d0, -0.67135360d0, -0.5908642d0, &
&   0.73168874d0, 0.5673025d0/)
  CALL ADCONTEXTTGT_INIT(1.e-8_8, 0.87_8)
  CALL ADCONTEXTTGT_INITREAL8ARRAY('array'//CHAR(0), array, arrayd, 10)
  resultd = ROSENBROCK_D(array, arrayd, 9, result)
  CALL ADCONTEXTTGT_STARTCONCLUDE()
  CALL ADCONTEXTTGT_CONCLUDEREAL8('result'//CHAR(0), result, resultd)
  CALL ADCONTEXTTGT_CONCLUDE()
  PRINT*, 'Rosenbrock result', result
END PROGRAM ROSENBROCKDRIVER_D

!  Differentiation of rosenbrock in forward (tangent) mode (with options context fixinterface):
!   variations   of useful results: y
!   with respect to varying inputs: x
!   RW status of diff variables: x:in y:out
REAL*8 FUNCTION ROSENBROCK_D(x, xd, n, y) RESULT (yd)
  IMPLICIT NONE
  INTEGER :: n
  REAL*8 :: x(0:n)
  REAL*8 :: xd(0:n)
  INTRINSIC SUM
  DOUBLE PRECISION, DIMENSION(n) :: arg1
  DOUBLE PRECISION, DIMENSION(n) :: arg1d
  REAL*8, DIMENSION(n) :: temp
  REAL*8, INTENT(OUT) :: y
  temp = x(1:n) - x(0:n-1)*x(0:n-1)
  arg1d = 100.d0*2*temp*(xd(1:n)-2*x(0:n-1)*xd(0:n-1)) - 2*(1-x(0:n-1))*&
&   xd(0:n-1)
  arg1 = 100.d0*(temp*temp) + (1-x(0:n-1))*(1-x(0:n-1))
  yd = SUM(arg1d)
  y = SUM(arg1)
END FUNCTION ROSENBROCK_D

