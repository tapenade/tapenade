real*8 function rosenbrock(x,n) result(y)
  integer :: n
  real*8 :: x(0:n)
  y = SUM(100.d0*(x(1:n) - x(0:n-1)**2)**2 + (1-x(0:n-1))**2)
end function rosenbrock

program rosenbrockdriver
  real*8 :: array(0:9), result
  real*8 :: rosenbrock
  array = (/-0.37211100d0,  0.26423106d0, -0.18252774d0, -0.7368198d0, &
       &    -0.44030386d0, -0.15214427d0, -0.67135360d0, -0.5908642d0, &
       &     0.73168874d0,  0.5673025d0/)
  result = rosenbrock(array, 9)
  print *,"Rosenbrock result",result
end program rosenbrockdriver
