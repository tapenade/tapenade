
/*=========== NAMESPACES ==============*/

namespace Q {                       // namespace

  namespace V {                     // V is a member of Q, it defines a new scope

    class C {                       // C is a member of V, a class defines a new scope too
      public:
      C(){}
      void m();
    };        
    void g(int){}                   // f is a member of V

  }
  
  void V::C::m() { }                // definition of V::C::m outside of the namespace (and the class body)

}

namespace Q {
  void g(int){}                     // Namespaces can be defined piece by piece and extended elsewhere
}

namespace Q::W { int a = 2; }       // C++17 nested declaration, already okay if compiled with option -std=c++1z

namespace shortcut = Q::V;          // NOT PARSED (YET), but resolved by clang:

int main(){
  shortcut::g(2);                   // calls Q::V::g(2), and parsed so by clang. We will never know about the shortcut.
}

/*========= USING DIRECTIVES ===========*/

using namespace Q::V;             // using( Namespace , none() ) 
C myCobj();                       // makes all declarations in Q::V visible in global namespace

namespace A {
    int i;
}
namespace B {
    int i;
    int j;
    namespace C {
        namespace D {
            using namespace A;    // all names from A injected into global namespace
            int j;
            int k;
            int a = i;            // i is B::i, because A::i is hidden by B::i
        }
        using namespace D;        // names from D are injected into C
                                  // names from A are injected into global namespace
        int k = 89;               // OK to declare name identical to one introduced by a using
        //int l = k;              // ambiguous: C::k or D::k
        int m = i;                // ok: B::i hides A::i
        int n = j;                // ok: D::j hides B::j
    }
}

/*================== USING ===================*/

namespace Q {  void f(int); }
using Q::f;                       // ::f is now a synonym for Q::f(int)
 
namespace Q {  void f(char); }    

void foo() {
    f('a');                       // calls f(int), even though f(char) exists.
}
using Q::f;                       // this f is a synonym for both Q::f(int) and Q::f(char)
void bar() {
    f('a');                       // calls f(char)
}

void f(double){}

namespace X {
    //using ::f;                  // global f is now visible as ::X::f,   NOT PARSED (YET) --> Seg Fault !!
    using Q::g;                   // Q::g is now visible as ::X::g,       NOT PARSED (YET)
}

/*===================== TYPEDEFS ==================*/

typedef int int_t;                    // typeDeclaration
typedef int int_t,                    // declare aliases on one line for the type, the pointer type, the function type, etc...
            *intp_t,           
            //(&fp)(int, ulong),    // function types are incorrectly parsed (yet)
            arr_t[10];
typedef int T[];                      // T is array type int[], without size ( dimColons(none()) )
T a = {1, 2}, b = {3,4,5};            // type of a is int[2], type of b is int[3]

template< class T>
struct add_const {
    typedef const T type;             // rename the template parameter
};

add_const<C> mystruct;

/*================ TYPE ALIASES =================*/

using FT = double;                        // keyword using ==> operator typeDeclaration. just for fun.

template<class T, class allocator>        // type aliases have been thought to work with template parameters
class vector { };

template<class T>
class Alloc { };

template<class T>
using Vec = vector<T, Alloc<T> >;          

Vec<int> v;                               // Vec<int> is the same as vector<int, Alloc<int>>
