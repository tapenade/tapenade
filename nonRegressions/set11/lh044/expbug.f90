MODULE SZMOD
integer, parameter :: c_double_complex=8
END MODULE SZMOD

!Bug found by C.Kotas about wrong type of temp variable
! when splitting EXP() result: should be COMPLEX!
subroutine foo(a,b,c)
USE SZMOD
REAL(8) :: a
COMPLEX(c_double_complex) :: b,c
b = b + c*EXP(cmplx(cos(a),sin(a),c_double_complex))
END subroutine foo
