void Encoder_2(const int nnodes, float params[nnodes],
               float u[nnodes][1], float xout3[nnodes][1] ) {
 int i, inode;
 for(inode = 0; inode < nnodes; ++inode)
   for(i = 0; i < 1; ++i)
     xout3[inode][i]= params[i] * u[inode][i];
}

/** Bug found by Kumar. In C, the temporary arg1 must not be of
 * type float[nnodes], i.e. the type of params, but rather of type
 * float*, because size becomes unknown after pointer arithmetic. */
void run_network(const int nnodes, float params[nnodes],
                 float u[nnodes][1], float xout3[nnodes][1] ) {
  Encoder_2(nnodes, params + 0, u, xout3);
}
