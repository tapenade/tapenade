C This is the Fortran equivalent of (C) set11/lh053
      subroutine fff(X,Y)
      DOUBLE COMPLEX X,Y
      DOUBLE COMPLEX Z
      real*8 u,v

      X = COS(X)
      Y = X*CONJG(X)
      u = REAL(Y)
      v = AIMAG(X)
      Z = ATAN(COMPLEX(u, v))
      X = X + X**Z
      v = ABS(X)
      Y = 3*Y + 6*X + Z*v
      end

      program main
      DOUBLE COMPLEX X,Y
      X = COMPLEX(2.5, 3.2)
      Y = COMPLEX(6.1, -1.5)
      call fff(X,Y)
      print *, 'Res:', REAL(Y), AIMAG(Y)
      end
