C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) - 14 Oct 2022 17:01
C
C  Differentiation of main as a context to call adjoint code (with options context):
C
      PROGRAM MAIN_B
      IMPLICIT NONE
      DOUBLE COMPLEX x, y
      DOUBLE COMPLEX xb, yb
      INTRINSIC COMPLEX
      INTRINSIC REAL
      INTRINSIC AIMAG
      x = COMPLEX(2.5, 3.2)
      y = COMPLEX(6.1, -1.5)
      CALL ADCONTEXTADJ_INIT(0.87_8)
      CALL ADCONTEXTADJ_INITCOMPLEX16('x'//CHAR(0), x, xb)
      CALL ADCONTEXTADJ_INITCOMPLEX16('y'//CHAR(0), y, yb)
      CALL FFF_B(x, xb, y, yb)
      CALL ADCONTEXTADJ_STARTCONCLUDE()
      CALL ADCONTEXTADJ_CONCLUDECOMPLEX16('x'//CHAR(0), x, xb)
      CALL ADCONTEXTADJ_CONCLUDECOMPLEX16('y'//CHAR(0), y, yb)
      CALL ADCONTEXTADJ_CONCLUDE()
      PRINT*, 'Res:', REAL(y), AIMAG(y)
      END

C  Differentiation of fff in reverse (adjoint) mode (with options context):
C   gradient     of useful results: x y
C   with respect to varying inputs: x y
C   RW status of diff variables: x:in-out y:in-zero
C This is the Fortran equivalent of (C) set11/lh053
      SUBROUTINE FFF_B(x, xb, y, yb)
      IMPLICIT NONE
      DOUBLE COMPLEX x, y
      DOUBLE COMPLEX xb, yb
      DOUBLE COMPLEX z
      DOUBLE COMPLEX zb
      REAL*8 u, v
      REAL*8 ub, vb
      INTRINSIC COS
      INTRINSIC CONJG
      INTRINSIC REAL
      INTRINSIC AIMAG
      INTRINSIC COMPLEX
      INTRINSIC ATAN
      INTRINSIC ABS
      INTRINSIC SQRT
      COMPLEX tempb
      DOUBLE PRECISION temp
      DOUBLE PRECISION temp0
      DOUBLE PRECISION tempb0
C
      CALL PUSHCOMPLEX16(x)
      x = COS(x)
      y = x*CONJG(x)
      u = REAL(y)
      v = AIMAG(x)
      z = ATAN(COMPLEX(u, v))
      CALL PUSHCOMPLEX16(x)
      x = x + x**z
      CALL PUSHREAL8(v)
      v = SQRT(REAL(x)**2 + AIMAG(x)**2)
      vb = REAL(CONJG(z)*yb)
      temp = AIMAG(x)
      temp0 = REAL(x)
      IF (temp0**2 + temp**2 .EQ. 0.0) THEN
        tempb0 = 0.D0
      ELSE
        tempb0 = vb/(2.0*SQRT(temp0**2+temp**2))
      END IF
      xb = xb + 6*yb + 2*temp0*tempb0 + CMPLX(0.D0, 2*temp*tempb0, 8)
      zb = v*yb
      CALL POPREAL8(v)
      CALL POPCOMPLEX16(x)
      zb = zb + CONJG(x**z*LOG(x))*xb
      tempb = CONJG(1.0/(1.0+COMPLEX(u, v)**2))*zb
      ub = REAL(tempb)
      yb = 3*yb + ub
      vb = AIMAG(tempb)
      xb = (CONJG(z*x**(z-1))+1.0)*xb + CMPLX(0.D0, vb, 8) + CONJG(CONJG
     +  (x))*yb + CONJG(CONJG(x)*yb)
      CALL POPCOMPLEX16(x)
      xb = CONJG(-SIN(x))*xb
      yb = (0.D0,0.D0)
      END

