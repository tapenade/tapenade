C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) - 14 Oct 2022 17:01
C
C
      PROGRAM MAIN
      IMPLICIT NONE
      DOUBLE COMPLEX x, y
      INTRINSIC COMPLEX
      INTRINSIC REAL
      INTRINSIC AIMAG
      x = COMPLEX(2.5, 3.2)
      y = COMPLEX(6.1, -1.5)
      CALL FFF(x, y)
      PRINT*, 'Res:', REAL(y), AIMAG(y)
      END

C This is the Fortran equivalent of (C) set11/lh053
      SUBROUTINE FFF(x, y)
      IMPLICIT NONE
      DOUBLE COMPLEX x, y
      DOUBLE COMPLEX z
      REAL*8 u, v
      INTRINSIC COS
      INTRINSIC CONJG
      INTRINSIC REAL
      INTRINSIC AIMAG
      INTRINSIC COMPLEX
      INTRINSIC ATAN
      INTRINSIC ABS
      INTRINSIC SQRT
C
      x = COS(x)
      y = x*CONJG(x)
      u = REAL(y)
      v = AIMAG(x)
      z = ATAN(COMPLEX(u, v))
      x = x + x**z
      v = SQRT(REAL(x)**2 + AIMAG(x)**2)
      y = 3*y + 6*x + z*v
      END

