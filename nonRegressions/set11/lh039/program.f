C To test COMMON behavior with the new implementation
C  that has no side-effect zones any more.
      subroutine top(x)
      real comval,x
      common /ext/ comval
      comval = comval+x
      call test(x)
      end

      subroutine test(x)
      real x, y
c
      if (x.gt.20.0) then
         x = 2.0
         y = 5.3
         call F1(x,y)
         x = x+y
      endif
      end

      subroutine F1(x,y)
      real x,y
      real comval
      common /ext/ comval
c
      y = y + 2*x + comval
      comval = comval*comval
      end

