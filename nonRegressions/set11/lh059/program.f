c Bug found by Shreyas Gaikwad on MIT GCM
c The push/pop's of preproc(k2) or of preproc(3)
c  (forced here by -nooptim tbr & saveonlyused) were
c  done wrongly with loops of PUSH/POPCHARACTER().
      subroutine test(x)
      REAL x
      INTEGER max_len_fnam
      PARAMETER (max_len_fnam=512)
      INTEGER ngenpproc
      PARAMETER (ngenpproc=10)
      INTEGER NGENCOST
      PARAMETER ( NGENCOST=30 )
      COMMON /ecco_gencost_c/ gencost_preproc
      CHARACTER*(max_len_fnam) gencost_preproc(ngenpproc, ngencost)
      CHARACTER*(max_len_fnam) preproc(ngenpproc)
      CHARACTER*128 fname1
      INTEGER icount,k2

      DO icount=1,5

         DO k2=1,ngenpproc
            preproc(k2) = gencost_preproc(k2, kgen(icount))
            IF (preproc(k2)(1:7) .EQ. 'dosumsq') THEN
               x = x*x
            ENDIF
         ENDDO

         call foo1(x,preproc)

         call foo2(x,fname1)

      ENDDO

      END
