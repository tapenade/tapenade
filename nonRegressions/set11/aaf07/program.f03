SUBROUTINE HEAD(x, y)
  IMPLICIT NONE
  intrinsic :: sqrt
  real :: x, y
  y = sqrt(x)
END SUBROUTINE

program test
  implicit none
  real :: a,b
  a = 4
  call head(a,b)
  print*, a, ' -> ', b
end program
