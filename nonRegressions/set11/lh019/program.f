C To test issues with new cleaner diff of COMMONs
C in particular that activity must be
C uniform at each invocation of the common.

      subroutine root(x,y)
      real x,y,a1,a2
      real c1,c2,c3
      common /comc/ c1,c2,c3
      a1 = x*x
      a2 = x
      c1 = x
      c2 = 0.5
      c3 = x
      y = y+a1
      call foo(x,y,a1)
      call bar(x,y,a2)
      y = y+x*x
      end

      subroutine foo(x,y,a)
C In diff code, ad need not be an argument (derivative only local)
C Likewise, c2d need not be in the differentiated COMMON
      real x,y,a
      real c1,c2,c3
      common /comc/ c1,c2,c3
      a = x*x
      y = y + a*x + c1*c3
      call funact(y,c2)
      a = 0.0
      end

      subroutine bar(x,y,a)
      real x,y,a
      real c1,c2,c3
      common /comc/ c1,c2,c3
      y = y + a*x + c1*c3
      call funact(y,x)
      end

      subroutine funact(u,v)
      real u,v
      u = u + 2*v
      v = v*u
      end

