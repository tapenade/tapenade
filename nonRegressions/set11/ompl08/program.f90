

! Test pyscopad. Testing detection of instance numbers for variables.
subroutine foo(u, r, n, s)
  implicit none
  integer i, n, indx(n)
!comment here in decls
  real r(n), u(n), s
  !$omp parallel do shared(u), shared(r)
  do i=2,n,2
    r(i-1) = 2*u(i)
    r(i) = 3*u(i-1)
  end do
  s = SUM(r)*SUM(u)
  if (s.gt.10) then
     s = s - 1
     !$omp parallel do shared(u), shared(r), private(jj), private(indx)
     do i=2,n,2
        r(indx(i)) = 2*u(i)
        if (i.eq.3) indx(i) = i+1
        r(indx(i)) = 3*u(i-1)
     end do
  endif
end subroutine foo

!comment here in the middle

program main
  integer, parameter :: n = 10
!comment here in the middle
  real, dimension(n) :: u, r
  real :: s
  u = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  r = (/3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9/)
  call foo(u, r, n, s)
  print *, 'result: ', s
end program main
