! Two bugs here:
! - the vector-tgt diff declaration of field p
!   had received an explicit dimension nbdirsmax,
!   which is forbidden: dimension can be only ":"
! - diff variable m of routine cost was differentiated
!   as m_diffv, due to confusion with module name m
!   (OK the "m" names should be different, but this is
!   still a bug in the process of turning m's into m_diffv)

module m   !incorrect module name, should be "mo"
  implicit none
  private
  type,public::para
     real(kind=8), allocatable :: p
  end type para
  type(para),save,target,public:: assim
end module m

subroutine cost(n,x,m,f)
  use m
  implicit none
  ! arguments
  integer, intent(in) :: n, m
  real(kind=8), intent(in) :: x(n)
  real(kind=8), intent(out) :: f

  allocate(assim%p)

  assim%p = cos(x(1)) + sin(x(2))
  f = assim%p*(1+sum(x))

  deallocate(assim%p)
end subroutine cost
