!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_assocaddress2) - 23 Jan 2020 11:26
!
!  Differentiation of addvector in forward (tangent) mode (with options associationByAddress):
!   variations   of useful results: c.w.x
!   with respect to varying inputs: a.w.x b.w.x
!   RW status of diff variables: a.w.x:in b.w.x:in c.w.x:out
SUBROUTINE ADDVECTOR_AAD(a, b, c)
  USE AATYPES
  IMPLICIT NONE
  TYPE VECTOR
      REAL :: x, y
  END TYPE VECTOR
  TYPE VECTOR_DIFF
      TYPE(REAL4_DIFF) :: x
      REAL :: y
  END TYPE VECTOR_DIFF
  TYPE RECT
      TYPE(VECTOR) :: w, h
  END TYPE RECT
  TYPE RECT_DIFF
      TYPE(VECTOR_DIFF) :: w
      TYPE(VECTOR) :: h
  END TYPE RECT_DIFF
  TYPE(RECT_DIFF), INTENT(IN) :: a, b
  TYPE(RECT_DIFF), INTENT(OUT) :: c
  c%w%x%d = a%w%x%d + b%w%x%d
  c%w%x%v = a%w%x%v + b%w%x%v
END SUBROUTINE ADDVECTOR_AAD

