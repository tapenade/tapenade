c Try reproduce a bug from Dassault about wrong diff include files
c when the diff of some common vars are not put in a diff common.

      subroutine FOO1(a,b)
      real a,b
      include 'decl.h'
      x = 2*a
      y = 2*b
      a = x*y
      x = 1.0
      y = 2.0
      call FOO2(a,b)
      x = 3*a
      y = 3*b
      a = x*y
      x = 1.0
      y = 2.0
      end

      subroutine FOO2(a,b)
      real a,b
      include 'decl.h'
      call FOO3(a,b)
      x = 4*a + x
      a = x*x
      x = 1.0
      end

      subroutine FOO3(a,b)
      real a,b
      include 'decl.h'
      x = 5*a
      y = 5*b
      a = x*y
      y = 2.0
      end
