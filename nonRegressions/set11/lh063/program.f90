module spCost

	!***** DEBUT EN-TETE **********************************************************!
	!_MODULE :
	!***** FIN EN-TETE ************************************************************!

	contains

	subroutine spTestSqrt_bug(xa,ya,za,sigma1)
		real(kind=8),intent(in) :: xa
		real(kind=8),intent(in) :: ya
		real(kind=8),intent(in) :: za
		complex(kind=8),intent(inout):: sigma1
		complex(kind=8) :: test

		test = (1.,0.)

		sigma1=test*(1d9/sqrt(xa**2+ya**2+za**2))
	end subroutine spTestSqrt_bug

	subroutine spTestSqrt_jeSaisPas(xa,ya,za,sigma1)
		real(kind=8),intent(in) :: xa
		real(kind=8),intent(in) :: ya
		real(kind=8),intent(in) :: za
		complex(kind=8),intent(inout):: sigma1
		real(kind=8) :: test

		test = 1.

		sigma1=test*(1d9/sqrt(xa**2+ya**2+za**2))
	end subroutine spTestSqrt_jeSaisPas

	subroutine spTestSqrt_ok_real(xa,ya,za,sigma1)
		real(kind=8),intent(in) :: xa
		real(kind=8),intent(in) :: ya
		real(kind=8),intent(in) :: za
		real(kind=8),intent(inout):: sigma1

		sigma1=sigma1*(1d9/sqrt(xa**2+ya**2+za**2))
	end subroutine spTestSqrt_ok_real

	subroutine spTestSqrt_ok_noFact(xa,ya,za,sigma1)
		real(kind=8),intent(in) :: xa
		real(kind=8),intent(in) :: ya
		real(kind=8),intent(in) :: za
		complex(kind=8),intent(inout):: sigma1

		sigma1=1.*(1d9/sqrt(xa**2+ya**2+za**2))
	end subroutine spTestSqrt_ok_noFact
end module spCost