!Bug Stefano Carli 9Jul2020: allocatable became pointer
! in the vector-tgt diff of type "mapping" !
module mod
  type, public :: mapping
     real, allocatable :: cvfcp (:,:)
     integer :: extra
  end type mapping
  type(mapping) :: x
end module mod

subroutine foo()
  use mod
  x%cvfcp(2,3) = sin(x%cvfcp(4,5))
end subroutine foo
