module m_foo_i
  implicit none

  interface 
   subroutine foo(x)
     real, dimension(:), allocatable :: x
   end subroutine foo
  end interface

end module m_foo_i

module m_bar_i
  implicit none

  interface 
   subroutine bar(x)
     real, dimension(:), allocatable :: x
   end subroutine bar
  end interface

end module m_bar_i

subroutine foo(x)
  use m_bar_i
  real, dimension(:), allocatable :: x
  call bar(x)
end subroutine foo


program main
  use m_foo_i
  use m_bar_i
  real, dimension(:), allocatable :: x
  call bar(x)
  call foo(x)
end program main
