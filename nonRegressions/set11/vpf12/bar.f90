subroutine bar(x)
  real, dimension(:), allocatable :: x
  if (.not. allocated(x)) then
    allocate(x(2))
  end if
  x = x * 2
end subroutine bar
