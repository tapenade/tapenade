! This code tests the sizes that are given to temporary arrays
! Caution: the call to GG assumes that n-n1+1==p : this just
! tests that tapenade can deduce that the temporary array (in
! adjoint) can be given size p instead of an undefined n-n1+1.
subroutine foo(x,y,n,p)
  integer n,p
  real*8 x(n),y
  integer n1,n2
  real*8 tt(p)
  n1 = n/3
  n2 = (2*n)/3
  where (x(n1:n2).gt.0.0)
     x(n1:n2) = log(x(n1:n2))
  end where
  CALL FFF(sin(x(n1:n)+2.0), y)
  CALL GGG(x(n1:n)+tt,y)
  x = x*y
end subroutine foo
