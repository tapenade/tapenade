! from set04/v006
MODULE TEST
  IMPLICIT NONE
  TYPE T
      REAL :: x, y
      INTEGER :: i
  END TYPE T
  INTERFACE OPERATOR(*)
      MODULE PROCEDURE TMUL
  END INTERFACE

  INTERFACE ASSIGNMENT(=)
      MODULE PROCEDURE TSET
  END INTERFACE

  INTERFACE F
      MODULE PROCEDURE TF, RF
  END INTERFACE

  PUBLIC :: ASSIGNMENT( = )
  PUBLIC :: OPERATOR(*)

CONTAINS
  SUBROUTINE TSET(a, b)
    IMPLICIT NONE
    REAL, INTENT(OUT) :: a
    TYPE(T), INTENT(IN) :: b
    a = b%x/b%y
  END SUBROUTINE TSET

  FUNCTION TMUL(a, b)
    IMPLICIT NONE
    TYPE(T), INTENT(IN) :: a, b
    TYPE(T) :: tmul
    tmul%x = a%x*b%x
    tmul%y = a%y*b%y
  END FUNCTION TMUL

  FUNCTION TF(v, u)
    IMPLICIT NONE
    TYPE(T) :: v
    REAL :: u, tf
    tf = v%x*v%y + u
    RETURN
  END FUNCTION TF

  FUNCTION RF(t, u)
    IMPLICIT NONE
    REAL :: rf, t, u
    rf = t*u
    RETURN
  END FUNCTION RF

END MODULE TEST

SUBROUTINE HEAD(a, b, c, resu)
  USE TEST
  IMPLICIT NONE
  TYPE(T) :: a, b, c
  REAL :: resu
  resu = b * c
  resu = F(a, 2.0)*resu
  RETURN
END SUBROUTINE HEAD

