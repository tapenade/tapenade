! Bug found by Michael Vossbeck 20Oct2020
! null pointer during diff of allocate(assim%p_b), vector tangent mode.

module bepstype
  implicit none
  type,public::para
     real(kind=8), pointer :: p_b
  end type para
  type(para),save,target,public:: assim   ! optimization of parameters
contains
  subroutine bepstype_init()
    implicit none
    if( .not. associated(assim%p_b) ) then
       allocate(assim%p_b)
       assim%p_b = 0._8
    end if
  end subroutine bepstype_init
end module bepstype


subroutine foo(n, x, m, y)
  use bepstype
  implicit none
  !-- arguments
  integer, intent(in) :: n
  integer, intent(in) :: m
  real(kind=8), intent(in) :: x(n)
  real(kind=8), intent(out) :: y(m)

  call bepstype_init()
  assim%p_b = x(1)

  y(1) = assim%p_b ** 2
end subroutine foo
