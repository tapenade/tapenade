!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) -  1 Aug 2023 09:28
!
! Bug found by Michael Vossbeck 20Oct2020
! null pointer during diff of allocate(assim%p_b), vector tangent mode.
MODULE BEPSTYPE_DIFFV
  USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
  IMPLICIT NONE
  TYPE, PUBLIC :: PARA
      REAL(kind=8), POINTER :: p_b
  END TYPE PARA
  TYPE, PUBLIC :: PARA_DIFFV
      REAL(kind=8), DIMENSION(:), POINTER :: p_b
  END TYPE PARA_DIFFV
! optimization of parameters
  TYPE(PARA), SAVE, TARGET, PUBLIC :: assim
  TYPE(PARA_DIFFV), SAVE, TARGET, PUBLIC :: assimd

CONTAINS
!  Differentiation of bepstype_init in forward (tangent) mode (with options multiDirectional noISIZE):
!   Plus diff mem management of: assim.p_b:in-out
  SUBROUTINE BEPSTYPE_INIT_DV(nbdirs)
    USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
    IMPLICIT NONE
    INTRINSIC ASSOCIATED
    INTEGER :: nbdirs
    IF (.NOT.ASSOCIATED(assim%p_b)) THEN
      ALLOCATE(assimd%p_b(nbdirsmax))
      ALLOCATE(assim%p_b)
      assim%p_b = 0._8
    END IF
  END SUBROUTINE BEPSTYPE_INIT_DV

  SUBROUTINE BEPSTYPE_INIT()
    IMPLICIT NONE
    INTRINSIC ASSOCIATED
    IF (.NOT.ASSOCIATED(assim%p_b)) THEN
      ALLOCATE(assim%p_b)
      assim%p_b = 0._8
    END IF
  END SUBROUTINE BEPSTYPE_INIT

END MODULE BEPSTYPE_DIFFV

!  Differentiation of foo in forward (tangent) mode (with options multiDirectional noISIZE):
!   variations   of useful results: y
!   with respect to varying inputs: x
!   RW status of diff variables: x:in y:out
!   Plus diff mem management of: assim.p_b:in-out
SUBROUTINE FOO_DV(n, x, xd, m, y, yd, nbdirs)
  USE BEPSTYPE_DIFFV
  USE DIFFSIZES
!  Hint: nbdirsmax should be the maximum number of differentiation directions
  IMPLICIT NONE
!-- arguments
  INTEGER, INTENT(IN) :: n
  INTEGER, INTENT(IN) :: m
  REAL(kind=8), INTENT(IN) :: x(n)
  REAL(kind=8), INTENT(IN) :: xd(nbdirsmax, n)
  REAL(kind=8), INTENT(OUT) :: y(m)
  REAL(kind=8), INTENT(OUT) :: yd(nbdirsmax, m)
  INTEGER :: nd
  INTEGER :: nbdirs
  CALL BEPSTYPE_INIT_DV(nbdirs)
  assim%p_b = x(1)
  yd = 0.0_8
  DO nd=1,nbdirs
    assimd%p_b(nd) = xd(nd, 1)
    yd(nd, 1) = 2*assim%p_b*assimd%p_b(nd)
  END DO
  y(1) = assim%p_b**2
END SUBROUTINE FOO_DV

SUBROUTINE FOO_NODIFF(n, x, m, y)
  USE BEPSTYPE_DIFFV
  IMPLICIT NONE
!-- arguments
  INTEGER, INTENT(IN) :: n
  INTEGER, INTENT(IN) :: m
  REAL(kind=8), INTENT(IN) :: x(n)
  REAL(kind=8), INTENT(OUT) :: y(m)
  CALL BEPSTYPE_INIT()
  assim%p_b = x(1)
  y(1) = assim%p_b**2
END SUBROUTINE FOO_NODIFF

