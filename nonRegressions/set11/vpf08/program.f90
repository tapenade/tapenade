
MODULE moo

IMPLICIT NONE

interface 
   subroutine f1(x)
     real x
   end subroutine f1
end interface

contains

subroutine foo(x)
 real x
 call f1(x)
end subroutine foo


END MODULE moo
