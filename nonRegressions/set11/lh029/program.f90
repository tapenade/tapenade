! Micro example of the strange %val() occuring in CTFEM MHD code.
SUBROUTINE LocalizeInMesh2(X,FoundTri,NmbVer, NmbTri, NmbEdg) 
  INTEGER                      :: FoundTri,NmbVer, NmbTri, NmbEdg
  REAL(KIND=8), DIMENSION(2)   :: X
  INTEGER                      :: nnn
  INTEGER, DIMENSION(:,:), allocatable      :: EdgTab, TriTab
  REAL(kind=8), dimension(:,:), allocatable :: VerTab
  EXTERNAL LOLNEWOCTREE
  INTEGER*8 :: LOLNEWOCTREE

  ALLOCATE(VerTab(3,NmbVer), TriTab(3,NmbTri), EdgTab(2,NmbEdg))

  nnn = lolnewoctree( &
       NmbVer, VerTab(1,1), VerTab(1,2) &
     , NmbEdg, EdgTab(1,1), EdgTab(1,2) &
     , NmbTri, TriTab(1,1), TriTab(1,2) &
     , 0, %val(0), %val(0) &
     , 0, %val(0), %val(0) &
     , 0, %val(0), %val(0) &
     , 0, %val(0), %val(0) &
     , 0, %val(0), %val(0))

  x = x * nnn
      
  DEALLOCATE(VerTab, TriTab, EdgTab)
      
END SUBROUTINE LocalizeInMesh2

