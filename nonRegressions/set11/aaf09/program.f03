  SUBROUTINE HEAD(x, y, n)
  IMPLICIT NONE
  intrinsic :: sqrt
  integer :: n
  real(8), dimension(n) :: x
  real(8), dimension(n) :: y
  intent(in) :: x
  intent(out) :: y
  y(1) = sqrt(x(1))
  END SUBROUTINE

  subroutine top(x, y)
  real(8), dimension(*) :: x, y
  call head(x, y, 3)
  end subroutine


program test
  implicit none
  real(8),dimension(3) :: z, b
  z(1) = 4
  z(2) = 5
  z(3) = 66
  call top(z, b)
  print*, z(1), ' -> ', b(1)
end program
