! Bug missing propagation of the "where" context into the inlined code for "max"
! Signalled by InversionLab Jan 11,2021
subroutine foo(nsp, snow, snowh)
  implicit none
  !-- arguments
  integer, intent(in) :: nsp
  real(kind=8), intent(inout) :: snow(nsp), snowh(nsp)
  !-- local
  real(kind=8) :: dens_sn(nsp)
  dens_sn = 0._8
  where(dens_sn.lt.0)
     snow = max(0.,snow)
  end where

  snowh = (snow - 1) 

end subroutine foo
