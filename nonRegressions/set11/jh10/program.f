      subroutine foo(n, a, b, c, d, e)
c Subroutine with simple parallel loops, using only `private`, `shared`, or
c no clauses (default scoping). Array access localized, except in th final
c loop, where Tapenade should default to 'give up' and generate a serial loop.
      integer i,j
      real a(n), b(n), c, d(n,n), e(n,n)
c OpenMP defaults apply: i is private, b is shared. Since each element in b is
c increased by a constant value, the derivative (in forward or reverse) should
c use the same scope: private for i, shared for b, b_d, b_b
c$omp parallel do private(i) shared(n,b) shared(x)
      do i = 1, n
         b(i) = b(i) + 2.0
      enddo

c Parallel array copy. The derivative should use default scoping here, too.
c$omp parallel do
      do i = 1, n
         a(i) = b(i)
      enddo

c Parallel array increment. Again, default scoping should be used.
c$omp parallel do shared(a)
      do i = 1, n
         a(i) = a(i) * b(i)
      enddo

c Parallel array increment with active scalar. Reduction must be used.
c$omp parallel do
      do i = 1, n
         a(i) = a(i) + c
      enddo

c Parallel gather access leads to scatter access in b. The reverse derivative
c must not use default scoping; probably don't parallelize this for now.
c$omp parallel do
      do i = 1, n-1
         a(i) = a(i) + b(i) + b(i+1)
      enddo

c$omp parallel do private(j)
      do i = 1, n-1
        do j = 1, n-1
           d(i,j) = e(i,2*j) + e(i,2*j+1)
        enddo
      enddo
      end

