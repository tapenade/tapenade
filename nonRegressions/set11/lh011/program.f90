! Inspired from set03/lh092, this time using the -context test mechanism
! Example with plenty of aliasing cases.
! Problem: adjoint diff still doesn't work...  (10Jul2020)
MODULE TEST
  IMPLICIT NONE

  TYPE TTTT
     integer nnn
     double precision, dimension(:, :), allocatable :: fff1
     double precision, dimension(:, :), allocatable :: fff2
     double precision, dimension(:, :), pointer :: fff3
     double precision, dimension(:, :), pointer :: fff4
  END TYPE TTTT

  TYPE(TTTT) :: xxx2

  double precision, dimension(9,9), target :: aaa1,aaa2,aaa3,aaa4

CONTAINS

  SUBROUTINE TOP(xxx1,vvv,resu)
    IMPLICIT NONE
    TYPE(TTTT) :: xxx1
    double precision, dimension(7,7) :: bbb1,bbb2
    double precision :: vvv,resu
    INTEGER :: cpvv

    cpvv=INT(vvv)

    if (MOD(cpvv,2).ge.1) then
       xxx1%fff3 => aaa2
       xxx1%fff4 => aaa4
    endif
    aaa2 = aaa2*vvv
    if (MOD(cpvv,4).ge.2) then
       xxx2%fff3 => aaa2
       xxx2%fff4 => aaa4
    endif
    aaa4 = aaa4*vvv
    aaa1 = aaa1*vvv
    xxx1%fff2 = vvv*aaa2(2,2)
    xxx2%fff1 = vvv

    aaa2 = 0.0
    xxx1%fff1 = 0.0
    xxx2%fff2 = 0.0

    if (MOD(cpvv,8).ge.4) then
       ! before call 1: RESET DIFF x1%1 x2%2 a2
       !  DON'T RESET DIFF x1%2 x1%3 x2%1 x2%3 x2%4 a1 a4
       call CHANGES(xxx1,vvv)
    endif

    vvv = vvv+SUM(aaa4)+SUM(xxx2%fff2)+SUM(xxx2%fff4)

    ! Force full activity of subroutine CHANGE
    if (MOD(cpvv,16).ge.8) then
       xxx1%fff1 = vvv
       xxx1%fff2 = vvv
       xxx1%fff3 = vvv
       xxx1%fff4 = vvv
       xxx2%fff1 = vvv
       xxx2%fff2 = vvv
       xxx2%fff3 = vvv
       xxx2%fff4 = vvv
       ! before call 2: RESET DIFF a2
       !  DON'T RESET DIFF x1%1 x1%2 x1%3 x1%4 x2%1 x2%2 x2%3 x2%4 a1 a4
       call CHANGES(xxx1,vvv)
    endif

    vvv = vvv+SUM(aaa1)+SUM(aaa2)+SUM(aaa3)+SUM(aaa4)
    vvv = vvv+SUM(xxx1%fff1)+SUM(xxx1%fff2)
    resu = vvv+SUM(xxx2%fff1)+SUM(xxx2%fff2)
  END SUBROUTINE TOP
    
  SUBROUTINE CHANGES(xxx1,vvv)
    IMPLICIT NONE
    TYPE(TTTT) :: xxx1
    double precision :: vvv

    vvv = vvv+xxx1%fff1(1,2)+xxx1%fff2(1,3)
    vvv = vvv+xxx1%fff3(1,4)
    vvv = vvv+xxx2%fff1(1,2)+xxx2%fff2(1,3)
    vvv = vvv+xxx2%fff3(1,2)+xxx2%fff4(1,3)
    xxx1%fff2 = 0.0
    xxx2%fff2 = 0.0
    xxx2%fff4 = 0.0
    vvv = 2.1*vvv
  END SUBROUTINE changes
END MODULE TEST

SUBROUTINE FOO (in,xxx,out)
  USE TEST
  TYPE(TTTT) :: xxx
  double precision :: in, out, in2
  integer :: i
  do i=0,15
     in2 = i+in
!$AD NOCHECKPOINT
     call TOP(xxx, in2, out)
  enddo
END SUBROUTINE FOO

PROGRAM MAIN
  USE TEST
  IMPLICIT NONE
  double precision input
  double precision globalsum
  TYPE(TTTT) :: xxx1
  DOUBLE PRECISION ::  origvvv, vvv, keepvvv
  double precision, dimension(9,9), target :: nnn1,nnn2,nnn3,nnn4
  aaa1 = 0.1d0
  aaa2 = 0.2d0
  aaa3 = 0.3d0
  aaa4 = 0.4d0
  nnn1 = 0.1d0
  nnn2 = 0.2d0
  nnn3 = 0.3d0
  nnn4 = 0.4d0
  ALLOCATE(xxx1%fff1(11, 11))
  ALLOCATE(xxx1%fff2(11, 11))
  xxx1%fff3 => nnn1
  xxx1%fff4 => nnn1
  xxx1%fff1 = 1.1d0
  xxx1%fff2 = 1.2d0
  xxx1%fff3 = 1.3d0
  xxx1%fff4 = 1.4d0
  ALLOCATE(xxx2%fff1(8, 8))
  ALLOCATE(xxx2%fff2(8, 8))
  xxx2%fff3 => nnn1
  xxx2%fff4 => nnn1
  xxx2%fff1 = 3.1d0
  xxx2%fff2 = 3.2d0
  xxx2%fff3 = 3.3d0
  xxx2%fff4 = 3.4d0

  input = 0.5d0
  globalsum = 0.d0
  CALL FOO(input, xxx1, globalsum)
  print *,'globalsum=',globalsum

  DEALLOCATE(xxx1%fff1)
  DEALLOCATE(xxx1%fff2)
  DEALLOCATE(xxx2%fff1)
  DEALLOCATE(xxx2%fff2)
END PROGRAM MAIN
