        subroutine test(var1, var2, result)
            implicit none
            real var1
            real var2
            real result
            result = min(abs(var1), var2)
        end

        program main
            implicit none
            real var1
            real var2
            real result
            var1 = 3
            var2 = 2
            call test(var1, var2, result)
            print*, var1 , var2, '-> ', result
        end
