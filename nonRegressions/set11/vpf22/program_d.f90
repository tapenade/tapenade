!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_html) - 10 Dec 2020 16:48
!
!  Differentiation of foo in forward (tangent) mode:
!   variations   of useful results: b
!   with respect to varying inputs: a
!   RW status of diff variables: a:in b:out
SUBROUTINE FOO_D(a, ad, b, bd)
  IMPLICIT NONE
  REAL :: a
  REAL :: ad
  REAL, DIMENSION(3) :: b
  REAL, DIMENSION(3) :: bd
  INTEGER :: i
  INTEGER, DIMENSION(3) :: temp
  temp = (/(i, i=0,2)/)
  bd = temp*ad
  b = temp*a
END SUBROUTINE FOO_D

