void test(float x, double y, float *z) {
  /* first declarations */
  float u = x * 2, v = y * u;
  /* first statement */
  u = u * v;
  /* second declaration */
  float w = *z * u;
  /* second statement */
  *z = w * (*z);
}
