module motest
  implicit none
  real(kind=8) :: a=1._8, b=2._8
end module motest

subroutine foo(x,y)
  use motest, only: a
  implicit none
  real(kind=8), intent(in) :: x
  real(kind=8), intent(out) :: y
  interface
     real(kind=8) function sq(x)
       implicit none
       real(kind=8) :: x
     end function sq
  end interface
  a = 1._8
  y = a*sq(x)
end subroutine foo

real(kind=8) function sq(x) result(y)
  use motest, a=>b
!-- rename prevents 'sq' from access to module's variable 'b'
  implicit none
  real(kind=8) :: x

  y = a*x*x
end function sq
