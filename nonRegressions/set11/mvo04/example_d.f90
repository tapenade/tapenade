!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) -  7 Feb 2023 13:50
!
MODULE MO_DIFF
  IMPLICIT NONE
  PRIVATE 
  TYPE, PUBLIC :: PARA
      REAL(kind=8), ALLOCATABLE :: p
  END TYPE PARA
  TYPE(PARA), SAVE, TARGET, PUBLIC :: assim
  TYPE(PARA), SAVE, TARGET, PUBLIC :: assimd
END MODULE MO_DIFF

!  Differentiation of cost in forward (tangent) mode (with options noISIZE):
!   variations   of useful results: f
!   with respect to varying inputs: x
!   RW status of diff variables: f:out x:in
SUBROUTINE COST_D(n, x, xd, m, f, fd)
  USE MO_DIFF
  IMPLICIT NONE
! arguments
  INTEGER, INTENT(IN) :: n, m
  REAL(kind=8), INTENT(IN) :: x(n)
  REAL(kind=8), INTENT(IN) :: xd(n)
  REAL(kind=8), INTENT(OUT) :: f
  REAL(kind=8), INTENT(OUT) :: fd
  INTRINSIC COS
  INTRINSIC SIN
  INTRINSIC SUM
  REAL(kind=8) :: temp
  ALLOCATE(assimd%p)
  ALLOCATE(assim%p)
  assimd%p = COS(x(2))*xd(2) - SIN(x(1))*xd(1)
  assim%p = COS(x(1)) + SIN(x(2))
  temp = SUM(x) + 1
  fd = temp*assimd%p + assim%p*SUM(xd)
  f = assim%p*temp
  IF (ALLOCATED(assimd%p)) THEN
    DEALLOCATE(assimd%p)
  END IF
  DEALLOCATE(assim%p)
END SUBROUTINE COST_D

SUBROUTINE COST_NODIFF(n, x, m, f)
  USE MO_DIFF
  IMPLICIT NONE
! arguments
  INTEGER, INTENT(IN) :: n, m
  REAL(kind=8), INTENT(IN) :: x(n)
  REAL(kind=8), INTENT(OUT) :: f
  INTRINSIC COS
  INTRINSIC SIN
  INTRINSIC SUM
  ALLOCATE(assim%p)
  assim%p = COS(x(1)) + SIN(x(2))
  f = assim%p*(1+SUM(x))
  DEALLOCATE(assim%p)
END SUBROUTINE COST_NODIFF

