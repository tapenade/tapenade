module mo
  implicit none
  private
  type,public::para
     real(kind=8), allocatable :: p
  end type para
  type(para),save,target,public:: assim
end module mo

subroutine cost(n,x,m,f)
  use mo
  implicit none
  ! arguments
  integer, intent(in) :: n, m
  real(kind=8), intent(in) :: x(n)
  real(kind=8), intent(out) :: f

  allocate(assim%p)

  assim%p = cos(x(1)) + sin(x(2))
  f = assim%p*(1+sum(x))

  deallocate(assim%p)
end subroutine cost
