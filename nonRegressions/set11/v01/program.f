C from aironum-beta-v4.3-tapenade/AIRO_CUcomlib/nsendn.f

      subroutine nsendn(encoding,tagid,buffer,lbuf,node,procid,msgid)
      integer*4 encoding
      integer*4 tagid
      integer*4 lbuf
      real*8 buffer(lbuf)
      integer*4 node
      integer*4 procid
      integer*4 msgid

      include 'mpif.h'

        INTEGER myleader, theirleader
	INTEGER buff_size
        INTEGER maxcolor
        PARAMETER(buff_size=9600000, maxcolor=4)
        INTEGER rname(maxcolor)
	REAL*8 mpi_my_u_buff(buff_size)
	REAL*8 my_u_buff(buff_size)
        INTEGER curp, lastp, fout
	COMMON/MPIBUFCOM/mpi_my_u_buff,
     &     my_u_buff,curp,lastp,myleader,theirleader,fout
        COMMON/MPINAMES/rname

      integer*4 myname
      integer*4 mynods
      integer*4 otname
      integer*4 otnods

      COMMON/THREADS/ mynods, otnods, myname, otname

      integer*4 ierror

      call myclock(3)
      msgid = 0

      call mpi_isend(buffer,lbuf,MPI_DOUBLE_PRECISION,node,tagid,
     $ myname,msgid,ierror)
      call myclock(4)

      return
      end
