#include <math.h>

/* Example provided by Mike Giles. */
void gbm_call(double *Z, double S, double r, double sigma, double K, double T,
	      int N, double *P) {
  int    n;
  double h, c1, c2, disc;

  h = T / ((double) N);
  c1 = r*h;
  c2 = sigma*sqrt(h);

  for (n=0; n<N; n++) {
    S = S + S*(c1 + c2*Z[n]);
  }
  
  disc = exp(-r*T);
  *P = (S>K) ?  disc*(S-K) : 0.0;
}



  //  *P = 0.0;
  //  if (S>K) *P = disc*(S-K);

  //  *P = disc * fmax(0.0,S-K);
