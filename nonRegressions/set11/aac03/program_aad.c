/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_mix25) - 19 Mar 2020 14:47
*/
#include "AATypes_aad.c"

/*
  Differentiation of test in forward (tangent) mode (with options associationByAddress):
   variations   of useful results: *z
   with respect to varying inputs: x y *z
   Plus diff mem management of: z:in
*/
void test_aad(float4_diff x, float4_diff y, float4_diff *z) {
    float4_diff u;
    u.d = 2*x.d;
    u.v = x.v*2;
    float4_diff v;
    v.d = u.v*y.d + y.v*u.d;
    v.v = y.v*u.v;
    /* first statement */
    u.d = v.v*u.d + u.v*v.d;
    u.v = u.v*v.v;
    float4_diff w;
    w.d = u.v*z->d + z->v*u.d;
    w.v = z->v*u.v;
    /* second statement */
    z->d = z->v*w.d + w.v*z->d;
    z->v = w.v*z->v;
}
