/** Inspired from a bug on SEISM.
 *  Test ADMM when applied to arrays declared instead of allocated */

#include <stdio.h>
#include <math.h>

void createKe(double input, double *Ke) {
  int i ;
  for (i=0 ; i<36 ; ++i) {
    Ke[i] = i*input*input ;
  }
}

void createpe(double input, double *pe) {
  int i ;
  for (i=0 ; i<6 ; ++i) {
    pe[i] = i*input*input ;
  }
}

void useKe(double *Ke, double *output) {
  int i ;
  for (i=0 ; i<36 ; ++i) {
    *output += sin(Ke[i]) ;
  }
}

void usepe(double *pe, double *output) {
  int i ;
  for (i=0 ; i<6 ; ++i) {
    *output += sin(pe[i]) ;
  }
}

void SystemMatrices(double input, double *output) {
  int i ;
  for(i=0;i<12;i++){
    double Ke[3*2][3*2] = {0.};
    double pe[3*2]      = {0.};
    createKe(input, &Ke[0][0]);
    createpe(input, pe);
    useKe(&Ke[0][0], output);
    usepe(pe, output) ;
    *output = sin(*output);
  }
}

int main() {
  double input = 2.3 ;
  double output = 0.0 ;
  SystemMatrices(input, &output) ;
  printf("output is %f\n",output) ;
}
