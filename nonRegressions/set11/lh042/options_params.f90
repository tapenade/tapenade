module options_params
  use iso_c_binding, only: c_int32_t, c_double
  use constants
  use data_types


  implicit none
  !! The program options (sandboxed)
  type(program_options_t)       :: options


  !! All of these time-stepping related params should be in options_t.
  !! Flow CFL number, max cfl for line-search. No globalisation for cfl_flow=max_cfl
  real(c_double)                :: cfl_flow
  real(c_double)                :: cfl_min = 1d0
  real(c_double)                :: cfl_max = 1d99

  !! Relative tolerance for solver convergence
  real(c_double)                :: convergence_tol

  !! Enable restarting of solution
  integer(c_int32_t)            :: restart_primal_sol = FALSE
  integer(c_int32_t)            :: restart_adjoint_sol = FALSE
  integer(c_int32_t)            :: write_residual = FALSE
  integer(c_int32_t)            :: restart_sol = FALSE ! restart of the currently computed solution.
  !! Enable restarting of wall-distance
  integer(c_int32_t)            :: restart_walldist = FALSE
  integer(c_int32_t)            :: restart_write_interval = 10

  !! Number of nodes where SA term is negative
  integer(c_int32_t)            :: n_negative_sa = 0

  
  !! The file name of the local rank HDF5 mesh file
  character(len=1024)           :: m_rank_mesh_filename

  !! Number of explict iter at startup, initial sol
  integer(c_int32_t)            :: max_steady_iter
  !! Number of explict iter for residual smoothing preconditioning before an implicit solve.
  integer(c_int32_t)            :: m_expl_startup_iter
  !! The maximum number of iterations to inner iteration in dual time step (unsteady simulation)
  integer(c_int32_t)            :: m_expl_preco_iter


  !! JDM 8/2/21, moved here from Jacobian. Is needed all over the place for blocking exch.
  !! Number of non-zero block entries in SpMat
  integer(c_int32_t)              :: block_size = 0
 

  
  !! write an ascii convHist file, values of the objectives, gradient of the adj obj
  !! to these units
  !! Note, value determined by open call. 0 init only for debugging/check.
  integer(c_int32_t)            :: unitConvHist = 0
  integer(c_int32_t)            :: unitObjectives = 0



  integer(c_int32_t) :: rk_mStages
  real(c_double)     :: rk_coeff(5) =  (/0.0, 0.0, 0.0, 0.0, 0.0/)
  real(c_double)     :: rk_cfl = 0


end module options_params
