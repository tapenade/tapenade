module data_types
  use iso_c_binding, only: c_double, c_int32_t
  use constants

  !! Values for solver control, typically given in the input file,
  !! or derived from values in it.
  type, bind(c) :: program_options_t
    !! The limiter coefficient value
    real(c_double)               :: limiterCoeff = 0.1
    !! The limiter wall-distance value (for limiting near wall nodes) for added stability
    real(c_double)               :: limiterWallDist = 1.e-5
    !! The entropy fix for the Roe Riemann flux function
    real(c_double), dimension(3) :: RoeEntropyFixCoeff = (/0.1d0, 0.0d0, 0.1d0/)
    !! Compute the ILU preco using (beta)*2ndO + (1-beta)*1stO
    real(c_double)               :: betaBlendJacobian = 0.5

    !! type of sys of eq, see EQ_* enums defined in constants.f90
    integer(c_int32_t)           :: eq_type = FALSE
    !! if false, compute adjoint jac just once at init.
    integer(c_int32_t)           :: recompute_adj_jacobian = FALSE
    !! type of adjoint obj fun, see EQ_* enums defined in constants.f90
    integer(c_int32_t)           :: objective_type = FALSE

    !! timestepping_type, see TS_* enums defined in constants.f90
    integer(c_int32_t)           :: timestepping_type = FALSE
    !! if non-zero, use a line-search for Newton, otherwise a const cfl.
    !! no longer used, superseded by setting cfl_start = cfl_max.
    integer(c_int32_t)           :: globalisation = TRUE
    !! Parameter for flow type (steady=0, unsteady=1)
    !! The earlier SX/PM used the unsteady flag to compute
    !! the ptc augmentation of the Jacobian, this is now done explicitly
    !! so needs unsteady=false.
    integer(c_int32_t)           :: unsteady = FALSE
    !! The spatial discretization order
    integer(c_int32_t)           :: order = 1
    !! Enable/Disable viscous terms (false=0, true!=0)
    integer(c_int32_t)           :: viscous = FALSE
    !! Enable/Disable 2d correction
    integer(c_int32_t)           :: TwoDim = FALSE
    !! Enable/Disable gradient limiter
    integer(c_int32_t)           :: limiter = FALSE
    !! Enable/Disable sutherland law for viscosity
    integer(c_int32_t)           :: sutherland = TRUE
    !! Enable/Disable turbulence
    integer(c_int32_t)           :: turbulent = FALSE
    !! Enable/Disable volume scaling (for Newton solver)
    integer(c_int32_t)           :: volumeScaling = FALSE
    !! Enable/Disable flow scaling (for Newton solver)
    integer(c_int32_t)           :: flowScaling = FALSE

    !! transpose Jacobian matvec products need reverse accumulation
    integer(c_int32_t)           :: transpose_halo_comm = FALSE


    !! Counter to store the number of reverse flow nodes at outlet
    !! kept here, rather than in optins_parameters.f90 as this is used
    !! in diff'ed functions.
    integer(c_int32_t)           :: reverseflow_clipping = 0

    !! Some integer value, e.g. the node to perturb with FD testing.
    integer(c_int32_t)           :: iVal = 0

    ! Used for adjoint testing: dJ/d(Alpha) = dJ/dR*dR/d(Alpha)=v dR/d(Alpha)
    integer(c_int32_t)           :: perturb_residual = FALSE
 end type program_options_t

  type :: mesh_topology_t
    !! Basic mesh data-structure (non-diff part)
    integer(c_int32_t)                               :: mNode = 0
    !! Total number of nodes in domain
    integer(c_int32_t)                               :: ninternal_nodes = 0
    !! Total number of internal nodes
    integer(c_int32_t)                               :: nhalol1_nodes = 0
    !! Total number of level-1 halo nodes
    integer(c_int32_t)                               :: nhalol2_nodes = 0
    !! Total number of level-2 halo nodes
    integer(c_int32_t)                               :: mBnodes = 0
    !! Total number of boundary nodes
    integer(c_int32_t)                               :: mBdryGrp = 0
    !! Total number of boundary groups
    integer(c_int32_t)                               :: mPeriodicEdge = 0
    !! Total number of periodic edges
    integer(c_int32_t)                               :: mPeriodicEdgeGrp = 0
    !! Total number of periodic edge groups (multiple periodicity)
    integer(c_int32_t)                               :: mEdge = 0
    !! Total number of flux edges in domain
    integer(c_int32_t)                               :: ninternal_edges = 0
    !! Total number of internal flux edges
    integer(c_int32_t)                               :: nfringe_edges = 0
    !! Total number of fringe flux edges (that are at the processor boudndary)
    integer(c_int32_t)                               :: nhalol1_edges = 0
    !! Total number of level-1 halo flux edges
    integer(c_int32_t)                               :: nhalol2_edges = 0
    !! Total number of level-2 halo flux edges
    integer(c_int32_t), dimension(:, :), allocatable :: edgeList
    !! The edge data containing the left(1) and right(2) node of a flux edge
    integer(c_int32_t), dimension(:), allocatable    :: bdryGrpType
    !! The boundary condition type for each boundary patch

    !! In a periodic case, muliply mass flow with this factor to get the total annulus.
    real(c_double), dimension(:), allocatable        :: bdryGrpPerMultiplicity

    integer(c_int32_t), dimension(:), allocatable    :: bdry2nodeStart
    !! The starting offset of every boundary patch node-id list
    integer(c_int32_t), dimension(:), allocatable    :: bdry2node
    !! The array to convert local boundary node-id to global node-id
    integer(c_int32_t), dimension(:), allocatable    :: nearwallnode
    !! The nearest wall node-id for every wall boundary node (to enforce weak wall)
    integer(c_int32_t), dimension(:, :), allocatable :: periodicEdgeList
    !! The list of periodic edges (original=1 and shadow=2)
    integer(c_int32_t), dimension(:), allocatable    :: periodicEdgeGrpOffset
    !! The periodic edge offset (multiple periodic group angles)
    real(c_double), dimension(:), allocatable        :: periodicGrpAngle
    !! Periodicity angle for multiple periodic groups
    real(c_double), dimension(:), allocatable        :: periodicGrpTranslate
    !! Periodicity angle for multiple periodic groups
    real(c_double), dimension(:, :), allocatable     :: edgeWeight
    !! The dual-mesh edge normal (vector sum over all faces)
    real(c_double), dimension(:), allocatable        :: frictionHeight
    !! The friction height used for limiting near wall nodes for added stability
    real(c_double), dimension(:, :), allocatable     :: x
    !! The mesh x,y,z coordinates
    real(c_double), dimension(:), allocatable        :: vol
    !! The dual-mesh (node) volume
    real(c_double), dimension(:), allocatable        :: dist
    !! The wall distance value for a node
    real(c_double), dimension(:, :), allocatable     :: bdry2weight
    !! Boundary face normal of each boundary node in patch
  end type mesh_topology_t

  type :: flow_t
    !! Flow-field related inputs, variables, and boundary parameters
    !!real(c_double)                                  :: timestep
    !! The physical time step for unsteady simulations
    real(c_double)                                  :: TotalPressure_inlet
    !! Inlet boundary total-pressure value
    real(c_double)                                  :: TotalTemperature_inlet
    !! Inlet boundary total-temperature value
    real(c_double)                                  :: BackPressure_outlet
    !! Outflow boundary back-pressure value
    real(c_double)                                  :: massflow
    !! Outflow boundary target mass-flow rate value
    real(c_double)                                  :: ref_u
    !! Reference velocity value
    real(c_double)                                  :: dv_inf(6)
    !! Far-field boundary reference value
    real(c_double), dimension(:, :), allocatable    :: bvel
    !! Boundary velocity values (temporary storage for weak wall BC)
    real(c_double), dimension(:), allocatable       :: lim
    !! Gradient limiter values
    real(c_double), dimension(:), allocatable       :: Umin
    !! Stencil min value place holder
    real(c_double), dimension(:), allocatable       :: Umax
    !! Stencil max value place holder
    real(c_double), dimension(:, :, :), allocatable :: pgrad
    !! Flow gradients, allocated to 7 state var with temperature gradient in 7th position.
    real(c_double), dimension(:, :), allocatable    :: dv_bound
    !! Boundary value
    integer(c_int32_t)                              :: RPM
    !! TODO: Remove this Rotational frame revolutions-per-minute value (0 value is static)
    real(c_double), dimension(3)                    :: omega
    !! TODO: Angular velocity of rotational frame based on RPM value
    integer(c_int32_t)                              :: Is_Profile_Inlet = FALSE
    !! Has a swirl inlet or otherwise (surface normal)
    integer(c_int32_t)                              :: Is_Radial_Swirl = FALSE
    !! Radial swirl or Axial swirl
    real(c_double)                                  :: Swirl_Angle
    !! Inlet swirl angle value in degrees

    real(c_double)                                  :: Perturb_Radius
    !! Radius of influence for residual perturbation (Adjoint Testing)
    real(c_double)                                  :: Perturb_Val
    !! Max. Perturbation Value for the central node in the cosine bump function
    real(c_double)                                  :: Perturb_Coordinate(3)
    !! Coordinate of the central node
  end type flow_t

  ! JDM 17/2/22: this becomes a mess.
  ! Tapenade thinks is needs a diffed copy of this, but we consider the
  ! mix pl geom frozen.
  ! Maybe an issue with diffing this wrt to x? But where?
  ! The only diff'ed fields that appear to be used is avdv_pol.
  ! Since these line averages are in a sense viewed as additional unknowns,
  ! consider making them an array of pointers an allocate same as dv, dvd.
  ! pass them as dummy args.
  ! See e.g. 11.7 here on arrays of pointers.
  ! https://www.adt.unipd.it/corsi/Bianco/www.pcc.qub.ac.uk/tec/courses/f90/stu-notes/F90_notesMIF_12.html
  type hip_interface_t
     ! JDM 14/2/22: not used.
    !integer(c_int32_t)                                      :: nMLpairs
     !integer(c_int32_t)                                      :: TotalmLines
     ! mLines always has two elements, the no of lines either side,
     ! but is read from hdf with read_vector_int. which always allocates
     ! the right size, so needs to be allocatable.
    integer(c_int32_t), dimension(:), allocatable           :: mLines
    integer(c_int32_t), dimension(:), allocatable           :: nBC
    integer(c_int32_t), dimension(:), allocatable           :: edgelidx
    integer(c_int32_t), dimension(:), allocatable           :: edge2node
    real(c_double), dimension(:), allocatable    :: hML
    real(c_double), dimension(:), allocatable    :: edge2weight
    real(c_double), dimension(:), allocatable    :: edge2arclen
    real(c_double), dimension(:), allocatable    :: node2bdry
    real(c_double), dimension(:, :), allocatable :: wtdv
    ! JDM 14/2/22: is that used?
    !real(c_double), dimension(:, :), allocatable :: Avdv
    real(c_double), dimension(:, :), allocatable :: Avdv_Pol
    real(c_double), dimension(:, :), allocatable :: Node2ML_wt
    integer(c_int32_t), dimension(:, :), allocatable :: Node2ML_Ind
    real(c_double), dimension(:, :), allocatable :: Node2ML_OtherWt
    integer(c_int32_t), dimension(:, :), allocatable :: Node2ML_OtherInd
    integer(c_int32_t), dimension(:), allocatable :: mMixPNodes ! Number of MixP Nodes Per Patch
    integer(c_int32_t), dimension(:), allocatable :: MixPNodeGlobalID ! Global Node Number
    integer(c_int32_t), dimension(:), allocatable :: MixPNode2OtherIndex ! Index on the other patch
    real(c_double), dimension(:), allocatable    :: MixPNode2OtherWeight ! Weight of the Other Index
  end type hip_interface_t

  type zone_info_t
    !! Turbomachinery inputs (rotational frame of reference)
    integer(c_int32_t)              :: nzone = 1
    !! Number of zones (at least one zone to be present)
    real(c_double), allocatable     :: zone_rotvel(:, :)
    !! Rotational velocity of each zone size is (3, nzone): default is (0,0,0)
    integer(c_int32_t), allocatable :: node_to_zone(:)
    !! Convert the node id to zone id: default is all nodes in zone1
    integer(c_int32_t), allocatable :: zone_offset(:)
    !! Zones are numbered contiguously so get the offset info
    !! Note that this does not cover halos and only valid for nInternal_nodes
    integer(c_int32_t)              :: ninterface = 0
    !! Total Number of MP sliding planes, default is none
    type(hip_interface_t), allocatable :: hip_interface(:)
    !! Hip type interface data (will be deprecated)
    integer(c_int32_t), allocatable    :: interface_orient(:)
    !! Orientation of the interface plane
  end type zone_info_t

end module data_types
