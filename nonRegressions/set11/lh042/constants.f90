module constants
  use iso_c_binding, only: c_int32_t, c_double
  !! Model constants
  implicit none
  integer(c_int32_t), parameter :: FALSE = 0
  !! FALSE as integer
  integer(c_int32_t), parameter :: TRUE = 1
  !! TRUE as integer

  !! JDM, 8/2/21: moved here from wrap
  !! such constants should be defined in enum style
  integer(c_int32_t), parameter :: SwirlPatchTag = 6



  !! ENUM emulation, as F90 doesn't really have a safe enum type.
  !! Start a new enum with every 100, i.e. 100 different enum elements
  !! Within each enum, leave 0,1 empty for basic true, false.

  !! Time-stepping scheme indicators:
  integer(c_int32_t), parameter :: TS_BASE = 100
  integer(c_int32_t), parameter :: TS_EXPLICIT = TS_BASE+2
  integer(c_int32_t), parameter :: TS_IMPLICIT_PTC = TS_BASE+3
  integer(c_int32_t), parameter :: TS_IMPLICIT_HYB = TS_BASE+4
  !
  integer(c_int32_t), parameter :: TS_TEST = TS_BASE+90

  !! Which system of eqs?
  integer(c_int32_t), parameter :: EQ_BASE = 200
  integer(c_int32_t), parameter :: EQ_PRIMAL = EQ_BASE+2
  integer(c_int32_t), parameter :: EQ_TANGENT = EQ_BASE+3
  integer(c_int32_t), parameter :: EQ_ADJOINT = EQ_BASE+4
  integer(c_int32_t), parameter :: EQ_VOLSENS = EQ_BASE+5
  !
  integer(c_int32_t), parameter :: EQ_JACTEST_EXACT = EQ_BASE+90
  integer(c_int32_t), parameter :: EQ_JACTEST_AUGMENTED = EQ_BASE+91
  integer(c_int32_t), parameter :: EQ_JACTEST_IMPL_COMP = EQ_BASE+92
  integer(c_int32_t), parameter :: EQ_JACTEST_IMPL_WRITE = EQ_BASE+93
  integer(c_int32_t), parameter :: EQ_JACTEST_IMPL_READ = EQ_BASE+94
  integer(c_int32_t), parameter :: EQ_JACTEST_FD = EQ_BASE+95
  integer(c_int32_t), parameter :: EQ_GRADTEST = EQ_BASE+96
  integer(c_int32_t), parameter :: EQ_JACVECTEST_FD = EQ_BASE+97



  !! Which objective function?
  integer(c_int32_t), parameter :: OB_BASE        = 300
  integer(c_int32_t), parameter :: OB_MFLOW_IN    = OB_BASE+1
  integer(c_int32_t), parameter :: OB_MAVG_PT_IN  = OB_BASE+2
  integer(c_int32_t), parameter :: OB_MAVG_TT_IN  = OB_BASE+3
  integer(c_int32_t), parameter :: OB_MFLOW_OUT   = OB_BASE+4
  integer(c_int32_t), parameter :: OB_MAVG_PT_OUT = OB_BASE+5
  integer(c_int32_t), parameter :: OB_MAVG_TT_OUT = OB_BASE+6
  integer(c_int32_t), parameter :: OB_MAVG_PS_OUT = OB_BASE+7
  !
  integer(c_int32_t), parameter :: OB_NOT_USED1 = OB_BASE+8
  integer(c_int32_t), parameter :: OB_NOT_USED2 = OB_BASE+9
  !
  integer(c_int32_t), parameter :: OB_PT_RATIO = OB_BASE+10
  integer(c_int32_t), parameter :: OB_PT_LOSS = OB_BASE+11
  integer(c_int32_t), parameter :: OB_TOT_TOT_EFF_TURB = OB_BASE+12
  integer(c_int32_t), parameter :: OB_TOT_STAT_EFF_TURB = OB_BASE+13
  !
  integer(c_int32_t), parameter :: OB_LIFT        = OB_BASE+21
  integer(c_int32_t), parameter :: OB_DRAG        = OB_BASE+22
  integer(c_int32_t), parameter :: OB_MOMENT      = OB_BASE+23
  ! any array of objectives needs to be sized to this:
  integer(c_int32_t), parameter :: MAX_OB         = 23




  !! TRUE as intefer
  real(c_double), parameter :: sutherland_c1 = 0.00000145d0
  !! Sutherland law constant
  real(c_double), parameter :: sutherland_ref_T = 110.d0
  !! Sutherland law reference temperature
  real(c_double), parameter :: airViscosity = 1.813d-5!1.80525d-5
  !! Reference gas viscosity (Air at 300K)
  real(c_double), parameter :: Gas_Constant = 287.058d0
  !! Specific gas constant for dry air \( R \)
  real(c_double), parameter :: Cp = Gas_Constant*3.5d0
  !! Specific heat capacity \( C_p \)
  real(c_double), parameter :: Prl = 0.72d0
  !! Laminar Prandtl number \( Pr_L \)
  real(c_double), parameter :: Prt = 0.90d0
  !! Turbulent Prandtl number \( Pr_T \)
  real(c_double), parameter :: gamma = 1.4d0
  !! Adiabatic gas index \( \gamma \)
  real(c_double), parameter :: gamm1 = 0.4d0
  !! Adiabatic gas index minus one \(  ( \gamma - 1) \)

  real(c_double), parameter :: c_2 = 0.7d0
  !! SA constant \( c_2 \)
  real(c_double), parameter :: c_3 = 0.9d0
  !! SA constant \( c_3 \)
  real(c_double), parameter :: c_b1 = 0.1355d0
  !! SA constant \( c_{b_1} \) from Fluent v18.0
  real(c_double), parameter :: c_b2 = 0.6220d0
  !! SA constant \( c_{b_2} \) from Fluent v18.0
  real(c_double), parameter :: c_v1 = 7.1d0
  !! SA constant \( c_{v_1} \) from Fluent v18.0
  real(c_double), parameter :: sigma_sa = 2.d0/3.d0
  !! SA constant \( \sigma_{SA} \)
  real(c_double), parameter :: kappa_sa = 0.41d0
  !! SA constant \( \kappa_{SA} \)
  real(c_double), parameter :: c_w1 = c_b1/kappa_sa**2 + (1.0d0 + c_b2)/sigma_sa
  !! SA derived constant \( c_{w_1} = \frac{ c_{b_1} }{ \kappa_{SA}^2 } + \frac{1 + c_{b_2} }{ \sigma_{SA}  } \)
  real(c_double), parameter :: c_w2 = 0.3d0
  !! SA constant \( c_{w_2} \) from Fluent v18.0
  real(c_double), parameter :: c_w3 = 2.d0
  !! SA constant \( c_{w_3} \) from Fluent v18.0
  real(c_double), parameter :: c_t3 = 1.2d0
  !! SA constant \( c_{t_3} \)
  real(c_double), parameter :: c_t4 = 0.5d0
  !! SA constant \( c_{t_4} \)
  real(c_double), parameter :: c_n1 = 16.0d0
  !! SA constant \( c_{n_1} \)

  real(c_double), parameter :: NUMERIC_DBL_PREC = 1.0d-14

  real(c_double), parameter :: NUMERIC_ZERO = 0.0d0
  real(c_double), parameter :: NUMERIC_ONE = 1.0d0
  real(c_double), parameter :: NUMERIC_TWO = 2.0d0

  real(c_double), parameter :: NUMERIC_HALF = 0.5d0
  real(c_double), parameter :: NUMERIC_QUARTER = 0.25d0

  real(c_double), parameter :: NUMERIC_M_PI = 3.14159265358979323846264338327950288d0
  real(c_double), parameter :: NUMERIC_CONVERT_TO_RADIANS = 1.7453292519943295769236907684886E-02 !PI/180.0_rp

  integer(c_int32_t), parameter :: INTERFACE_CONST_R = 1
  integer(c_int32_t), parameter :: INTERFACE_CONST_Z = 2


  integer(c_int32_t), parameter :: RK5_MSTAGES = 5
  real(c_double), parameter     :: RK5_COEFF_1STO(5) =  (/0.0533d0, 0.1263d0, 0.2375d0, 0.4414d0, 1.0d0/)
  real(c_double), parameter     :: RK5_CFL_1STO = 2.5d0
  real(c_double), parameter     :: RK5_COEFF_2NDO(5) =  (/0.0695d0, 0.1602d0, 0.2898d0, 0.5060d0, 1.0d0/)
  real(c_double), parameter     :: RK5_CFL_2NDO = 1.15d0


  !! useful to set e.g. printing/branching parameters while debugging.
  integer (c_int32_t)  :: debugFlags(10) = 0
  ! tmp storage for f-d comparison of gradients
  real(c_double):: baseVal(10)=0
  real(c_double):: pertVal(10)=0


end module constants
