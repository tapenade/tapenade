! Bug found by Jens-Dominik Mueller about wrong indices used in a
!  tapenade-created loop that zero-initializes a tangent diff array.

module residue_functions
  !! Single module for all residual function for
  !! AD purpose
  use iso_c_binding, only: c_double, c_int32_t
  use data_types, only: mesh_topology_t
  use data_types, only: flow_t
  use data_types, only: program_options_t
  use data_types, only: zone_info_t
  use data_types, only: hip_interface_t
contains

  
  subroutine calc_Interp_dv(MixPl, nBNde, dv2_cyl)
    implicit none
    type(hip_interface_t), intent(in)     :: MixPl
    integer, intent(in)       :: nBNde ! number of the bnd node.
    real(c_double), dimension(6), intent(out)  :: dv2_cyl ! interpolated mix pl state in cyl coor

    real(c_double)      :: OtherWt1, OtherWt2
    integer(c_int32_t)             :: nML1o, nML2o

    nML1o = MixPl%Node2ML_OtherInd(1, nBNde)  ! Index of 1st ML on other/opposite patch
    nML2o = MixPl%Node2ML_OtherInd(2, nBNde)  ! Index of 2nd ML on other/opposite patch
    OtherWt1 = MixPl%Node2ML_OtherWt(1, nBNde)
    OtherWt2 = MixPl%Node2ML_OtherWt(2, nBNde)

    dv2_cyl = MixPl%Avdv_Pol(1:6, nML1o)*abs(1 - OtherWt1) + &
        MixPl%Avdv_Pol(1:6, nML2o)*abs(1 - OtherWt2)

  end subroutine calc_Interp_dv


!  subroutine calc_mpav(zinfo, mesh, dv)
  subroutine calc_mpav(options,mesh,zinfo,  dv)
    ! Calculate the variable average for each Mixing line
    ! This then is used for Roe Flux solve in BCFlux subroutine
    use constants, only: debugFlags
    implicit none
    type(program_options_t), intent(in) :: options
    type(zone_info_t), intent(inout)     :: zinfo
    type(mesh_topology_t), intent(in) :: mesh
    real(c_double), intent(in)        :: dv(6, mesh%mNode)
    integer      :: iMixP !, MixP_Orient !, iML
    !type(hip_interface_t)   :: mixPl

    do iMixP = 1, zinfo%ninterface
      call Calc_MPav_Interface(options,mesh, dv, &
          zinfo%interface_orient(iMixP), zinfo%hip_interface(iMixP))
    enddo
    ! if ( debugflags(1)==1 ) then
    !   write (*,'(A)') ' mp avg for line 2 in calc_mpav'
    !   write (*,'(6E24.16)') zinfo%hip_interface(1)%Avdv_Pol(:,2)
    ! endif

  end subroutine calc_mpav

  subroutine Calc_MPav_Interface(options,mesh, dv, MixP_Orient, mixPl)
    use constants, only: debugFlags
    implicit none
    type(program_options_t), intent(in) :: options
    type(mesh_topology_t), intent(in)   :: mesh
    real(c_double), intent(in)        :: dv(6, mesh%mNode)
    ! currently not used, cylindrical axis fixed in z-axis.
    integer, intent(in)              :: MixP_Orient
    type(hip_interface_t), intent(inout)   :: mixPl

    ! local
    integer      :: iML, iEdge
    real(c_double)      :: delta_arc, sum_arclen
    real(c_double), dimension(6)   :: Edgedv1_cyl, Edgedv2_cyl
    real(c_double), dimension(6)   :: Avg_Edgedv_cyl


    ! For each Mixing line, sum the polar discrete variable (dv_cyl)
    ! over the associated edges and integrate to obtain the Mix line Val.
    ! This Value can be used for Roe Flux Calc.

    mixPl%edgelidx(0) = 0  ! This is essential for the do loop to work!
    do iML = 1, mixPl%mLines(1) + mixPl%mLines(2)
      mixPl%Avdv_Pol(1:6, iML) = 0.d0

      sum_arclen = 0.d0
      do iEdge = mixPl%edgelidx(iML - 1) + 1, mixPl%edgelidx(iML) - 1
        call calc_MixL_EdgeVal(options,mesh, mixPl, dv, iEdge, Edgedv1_cyl)
        call calc_MixL_EdgeVal(options,mesh, mixPl, dv, iEdge + 1, Edgedv2_cyl)

        delta_arc = abs(mixPl%edge2arclen(iEdge + 1) - mixPl%edge2arclen(iEdge))
        sum_arclen = delta_arc + sum_arclen

        Avg_Edgedv_cyl = (Edgedv1_cyl + Edgedv2_cyl)/2.d0*delta_arc
        mixPl%Avdv_Pol(1:6, iML) = Avg_Edgedv_cyl + mixPl%Avdv_Pol(1:6, iML)
    ! if ( debugflags(1)==1 .and. iML==2 ) then
    !   write (*,'(A)') ' mpg avg for line 2 before arclen div'
    !   write (*,'(6E24.16)') mixPl%Avdv_Pol(:,iML)
    ! endif
      enddo
    ! if ( debugflags(1)==1 .and. iML==2 ) then
    !   write (*,'(A)') ' mpg avg for line 2 just before arclen div'
    !   write (*,'(6E24.16)') mixPl%Avdv_Pol(:,iML)
    ! endif
      mixPl%Avdv_Pol(1:6, iML) = mixPl%Avdv_Pol(1:6, iML)/sum_arclen
    ! if ( debugflags(1)==1 .and. iML==2 ) then
    !   write (*,'(A)') ' mp avg for line 2 after arclen div'
    !   write (*,'(6E24.16)') mixPl%Avdv_Pol(:,iML)
    ! endif
    enddo
    

  end subroutine Calc_Mpav_Interface

  subroutine calc_MixL_EdgeVal(options,mesh, mixPl, dv, iEdge, eg_dv_cyl)
    use constants, only: debugFlags
    implicit none
    type(program_options_t), intent(in) :: options
    type(hip_interface_t), intent(in) :: mixPl
    type(mesh_topology_t), intent(in) :: mesh
    real(c_double)       , intent(in) :: dv(6, mesh%mNode)

    integer                      :: iEdge, node2, node1
    real(c_double)               :: weight2, weight1
    real(c_double), dimension(6) :: eg_dv_cyl, dv_cyl2, dv_cyl1

    ! Nodes associated with iEdge
    node2 = mixPl%edge2node(2*iEdge)
    node1 = mixPl%edge2node(2*iEdge - 1)

    dv_cyl2(1:6) = dv(1:6, node2)
    call calc_dv_Flux_Norm_cyl(mesh, dv, node2, dv_cyl2)
    dv_cyl1(1:6) = dv(1:6, node1)
    call calc_dv_Flux_Norm_cyl(mesh, dv, node1, dv_cyl1)

    ! Weight associated the nodes
    weight2 = mixPl%edge2weight(2*iEdge)
    weight1 = mixPl%edge2weight(2*iEdge - 1)

    eg_dv_cyl(1:6) = weight2*dv_cyl2(1:6) + weight1*dv_cyl1(1:6)

    ! if ( debugflags(1)==1 ) then
    ! if ( node1==options%iVal .or. node2==options%iVal ) then
    !   write (*,'(A,I4,a,E24.16)') 'node1',node1,' wt',weight1
    !   write (*,'(A,6E24.16)') ' cart',dv(:,node1)
    !   write (*,'(A,6E24.16)') '  pol',dv_cyl1(:)
    !   write (*,'(A,I4,a,E24.16)') 'node2',node2,' wt',weight2
    !   write (*,'(A,6E24.16)') ' cart',dv(:,node2)
    !   write (*,'(A,6E24.16)') '  pol',dv_cyl2(:)
    !   write (*,'(A,6E24.16)') ' avg ',eg_dv_cyl(1:6)
    ! endif
    ! endif

  end subroutine calc_MixL_EdgeVal

  subroutine calc_dv_Flux_Norm_cyl(mesh, dv, node, dv_cyl)
    implicit none
    type(mesh_topology_t),intent(in) :: mesh
    real(c_double)       ,intent(in) :: dv(6, mesh%mNode)
    integer, intent(in)              :: node
    real(c_double), intent(out)      :: dv_cyl(6)

    real(c_double), dimension(3)       :: x
    real(c_double)                     :: dv_x, dv_y, dv_z, theta

    ! Polar velocity for the node
    x(1:3) = mesh%x(1:3, node)

    dv_x = dv(2, node)
    dv_y = dv(3, node)
    dv_z = dv(4, node)

    ! Initialise
    dv_cyl(1:6) = dv(1:6, node)

    theta = atan2(x(2), x(1))
    dv_cyl(2) = dv_x*cos(theta) + dv_y*sin(theta)
    dv_cyl(3) = -dv_x*sin(theta) + dv_y*cos(theta)

  end subroutine calc_dv_Flux_Norm_cyl



  !! The input to this is dv and the output is r
  !! and the Avdv_pol is just an intermediate variable
  !! JDM, 29/1/22: not called in the primal, this is a wrapper to be AD'ed.
  !!  name slightly misleading, this primal is really a mixplane residual calc.
  subroutine MixPlaneJacVec(options,grid, zinfo, dv, r)
    implicit none
    type(program_options_t), intent(in) :: options
    type(mesh_topology_t), intent(in) :: grid
    !! The mesh topology object
    type(zone_info_t), intent(inout)     :: zinfo
    !! Zone info
    real(c_double), intent(in)        :: dv(6, grid%mNode)
    !! Flow variable vector (input) at current time
    real(c_double), intent(out)        :: r(6, grid%mNode)
    !! Boundary flux residual
    integer(c_int32_t) :: nBdryGrp

    !! Calculate the mixing-line averages on both sides
    call calc_mpav(options, grid, zinfo, dv)
    !! Call the Roe Flux Calc. for MixP Nodes
    if (zinfo%ninterface >= 1) then
      do nBdryGrp = 1, grid%mBdryGrp
        if (grid%bdryGrpType(nBdryGrp) == 7) then
          call calc_MPflux(grid, zinfo, nBdryGrp, dv, r)
        end if
      end do
    endif
    
  end subroutine MixPlaneJacVec

end module residue_functions
