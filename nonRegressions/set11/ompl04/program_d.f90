!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_openmp2) -  4 Jun 2020 17:23
!
!  Differentiation of main as a context to call tangent code (with options OpenMP context):
PROGRAM MAIN_D
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=10
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: ad
  REAL :: y
  REAL :: yd
  a = (/1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9/)
  y = 1.1
  CALL ADCONTEXTTGT_INIT(1.e-4_8, 0.87_8)
  CALL ADCONTEXTTGT_INITREAL4('y'//CHAR(0), y, yd)
  CALL ADCONTEXTTGT_INITREAL4ARRAY('a'//CHAR(0), a, ad, n)
  CALL FOO_ALL_D(a, ad, y, yd, n)
  CALL ADCONTEXTTGT_STARTCONCLUDE()
  CALL ADCONTEXTTGT_CONCLUDEREAL4('y'//CHAR(0), y, yd)
  CALL ADCONTEXTTGT_CONCLUDEREAL4ARRAY('a'//CHAR(0), a, ad, n)
  CALL ADCONTEXTTGT_CONCLUDE()
  PRINT*, 'result: ', y
END PROGRAM MAIN_D

!  Differentiation of foo_all in forward (tangent) mode (with options OpenMP context):
!   variations   of useful results: y
!   with respect to varying inputs: y a
!   RW status of diff variables: y:in-out a:in
! Test OMP schedule
SUBROUTINE FOO_ALL_D(a, ad, y, yd, n)
  IMPLICIT NONE
  INTEGER :: n
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: ad
  REAL :: y
  REAL :: yd
  CALL FOO1_D(a, ad, y, yd, n)
  CALL FOO2_D(a, ad, y, yd, n)
  CALL FOO3_D(a, ad, y, yd, n)
END SUBROUTINE FOO_ALL_D

!  Differentiation of foo1 in forward (tangent) mode (with options OpenMP context):
!   variations   of useful results: y
!   with respect to varying inputs: y a
SUBROUTINE FOO1_D(a, ad, y, yd, n)
  IMPLICIT NONE
  INTEGER :: i, n
  REAL :: a(n), y
  REAL :: ad(n), yd
!$OMP PARALLEL DO REDUCTION(+:y), REDUCTION(+:yd)
  DO i=1,n
    yd = yd + ad(i)
    y = y + a(i)
  END DO
END SUBROUTINE FOO1_D

!  Differentiation of foo2 in forward (tangent) mode (with options OpenMP context):
!   variations   of useful results: y
!   with respect to varying inputs: y a
SUBROUTINE FOO2_D(a, ad, y, yd, n)
  IMPLICIT NONE
  INTEGER :: i, n
  REAL :: a(n), y
  REAL :: ad(n), yd
!$OMP PARALLEL DO REDUCTION(+:y), REDUCTION(+:yd), SHARED(a), SHARED(ad)
  DO i=1,n
    yd = yd + ad(i)
    y = y + a(i)
  END DO
END SUBROUTINE FOO2_D

!  Differentiation of foo3 in forward (tangent) mode (with options OpenMP context):
!   variations   of useful results: y
!   with respect to varying inputs: y a
SUBROUTINE FOO3_D(a, ad, y, yd, n)
  IMPLICIT NONE
  INTEGER :: i, n
  REAL :: a(n), y
  REAL :: ad(n), yd
!$OMP PARALLEL DO REDUCTION(+:y), REDUCTION(+:yd)
  DO i=1,n
    yd = yd + ad(i)
    y = y + a(i)
  END DO
END SUBROUTINE FOO3_D

