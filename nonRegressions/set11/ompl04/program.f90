
! Test OMP schedule
subroutine foo_all(a,y,n)
  integer n
  real, dimension(n) :: a
  real :: y
  call foo1(a, y, n)
  call foo2(a, y, n)
  call foo3(a, y, n)
end subroutine foo_all

subroutine foo1(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$omp parallel do reduction(+:y)
  do i=1,n
    y = y + a(i)
  end do
end subroutine

subroutine foo2(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$AD multithread_ADJ ReduCtion(+:A)
  !$omp parallel do reduction(+:y) shared(a)
  do i=1,n
    y = y + a(i)
  end do
end subroutine

subroutine foo3(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$AD MULTITHREAD_adj atoMIC(a)
  !$omp parallel do reduction(+:y)
  do i=1,n
    y = y + a(i)
  end do
end subroutine

program main
  integer, parameter :: n = 10
  real, dimension(n) :: a
  real :: y
  a = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  y = 1.1
  call foo_all(a, y, n)
  print *, 'result: ', y
end program main
