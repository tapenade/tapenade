! A test to reproduce a TreeGen bug (lost arrow carry, label 100...)
! found by Shreyas Gaikwad
SUBROUTINE OUTPUT2(time, z_sl, mask_region, &
                   opt_flag_compute_flux_vars_only)

implicit none
real(dp), intent(in) :: time, z_sl

logical, optional, intent(in) :: opt_flag_compute_flux_vars_only

integer(i4b) :: i, j, n
real(dp) :: Q_s_flx
real(dp), dimension(0:144,0:84) :: H_cold, H_temp, mask_region
logical :: flag_compute_flux_vars_only
logical, dimension(0:144,0:84) :: flag_region

integer(i4b), save :: counter = 0
logical,      save :: firstcall_output2 = .true.

if (present(opt_flag_compute_flux_vars_only)) then
   flag_compute_flux_vars_only = opt_flag_compute_flux_vars_only
else
   flag_compute_flux_vars_only = .false.
end if

if (.not.flag_compute_flux_vars_only) &
   counter = counter + 1

H_cold = 0.0_dp
H_temp = 0.0_dp
do i=1, 84-1
  do j=1, 144-1
    H_temp(j,i) = H_t(j,i)
  end do
end do
H_cold = -H_temp

if (maxval(mask_region) > 99) then
   call error('coucou')
end if
Q_s_flx = 0.0

do n=0, maxval(mask_region)
   if (n==0) then
      call scalar_variables(time, z_sl)
   else
      flag_region = .false.
      call scalar_variables(time, z_sl)
   end if
   Q_s_flx         = Q_s_flx + 1.0
   if ((n==0).and.(.not.flag_compute_flux_vars_only)) then
      print *, 'salut'
   end if

end do

if (firstcall_output2) firstcall_output2 = .false.

END SUBROUTINE OUTPUT2
