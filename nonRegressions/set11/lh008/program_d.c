/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 11 Dec 2020 10:53
*/
#include <adContext.h>
/*
  A classy FFT and Inverse FFT C++ class library

  Author: Tim Molteno, tim@physics.otago.ac.nz

  Based on the article "A Simple and Efficient FFT Implementation in C++" by Volodymyr Myrnyy
  with just a simple Inverse FFT modification.

  Licensed under the GPL v3.
*/
// Test case used by Billy Moses for comparison with Enzyme/Zygote.
// Billy Moses said Tapenade failed. True, but I claim for "easy" reasons.
// Preparation:
// * There was an issue with using non-standard M_PI: replaced with 3.1415926536
// * Beware that the size of *data passed to (i)fft must be 2*N+1 !
// * Unsigned int for-loop index goes wrong when going back down to zero: replaced with int
// Post-debug:
// * missing 3 decl of int i in for-loops of copied primal code (i.e. *_nodiff())
// * missing setting of offset pointer arg10b mirroring that of arg10
#include <stdio.h>
#include <math.h>

/*
  Differentiation of recursiveApply in forward (tangent) mode (with options context):
   variations   of useful results: *data
   with respect to varying inputs: *data
   Plus diff mem management of: data:in
*/
static void recursiveApply_d(double *data, double *datad, int iSign, unsigned 
        int N) {
    if (N == 1)
        return;
    else {
        recursiveApply_d(data, datad, iSign, N/2);
        recursiveApply_d(data + N, datad + N, iSign, N/2);
        double wtemp = iSign*sin(3.1415926536/N);
        double wpi = -iSign*sin(2*3.1415926536/N);
        double wpr = -2.0*wtemp*wtemp;
        double wr = 1.0;
        double wi = 0.0;
        for (int i = 0; i <= N-1; i += 2) {
            int iN = i + N;
            double tempr = data[iN]*wr - data[iN+1]*wi;
            double temprd = wr*datad[iN] - wi*datad[iN+1];
            double tempi = data[iN]*wi + data[iN+1]*wr;
            double tempid = wi*datad[iN] + wr*datad[iN+1];
            datad[iN] = datad[i] - temprd;
            data[iN] = data[i] - tempr;
            datad[iN + 1] = datad[i + 1] - tempid;
            data[iN + 1] = data[i + 1] - tempi;
            datad[i] = datad[i] + temprd;
            data[i] += tempr;
            datad[i + 1] = datad[i + 1] + tempid;
            data[i + 1] += tempi;
            wtemp = wr;
            wr += wr*wpr - wi*wpi;
            wi += wi*wpr + wtemp*wpi;
        }
    }
}

/*
  Differentiation of swap in forward (tangent) mode (with options context):
   variations   of useful results: *a *b
   with respect to varying inputs: *a *b
   Plus diff mem management of: a:in b:in
*/
static void swap_d(double *a, double *ad, double *b, double *bd) {
    double temp = *a;
    double tempd = *ad;
    *ad = *bd;
    *a = *b;
    *bd = tempd;
    *b = temp;
}

/*
  Differentiation of scramble in forward (tangent) mode (with options context):
   variations   of useful results: *data
   with respect to varying inputs: *data
   Plus diff mem management of: data:in
*/
static void scramble_d(double *data, double *datad, unsigned int N) {
    int j = 1;
    for (int i = 1; i <= 2*N-1; i += 2) {
        if (j > i) {
            swap_d(&(data[j - 1]), &(datad[j - 1]), &(data[i - 1]), &(datad[i 
                   - 1]));
            swap_d(&(data[j]), &(datad[j]), &(data[i]), &(datad[i]));
        }
        int m = N;
        while(m >= 2 && j > m) {
            j -= m;
            m >>= 1;
        }
        j += m;
    }
}

/*
  Differentiation of rescale in forward (tangent) mode (with options context):
   variations   of useful results: *data
   with respect to varying inputs: *data
   Plus diff mem management of: data:in
*/
static void rescale_d(double *data, double *datad, unsigned int N) {
    double scale = (double)1/N;
    for (int i = 0; i < 2*N; ++i) {
        datad[i] = scale*datad[i];
        data[i] *= scale;
    }
}

/*
  Differentiation of fft in forward (tangent) mode (with options context):
   variations   of useful results: *data
   with respect to varying inputs: *data
   Plus diff mem management of: data:in
*/
static void fft_d(double *data, double *datad, unsigned int N) {
    scramble_d(data, datad, N);
    recursiveApply_d(data, datad, 1, N);
}

/*
  Differentiation of ifft in forward (tangent) mode (with options context):
   variations   of useful results: *data
   with respect to varying inputs: *data
   Plus diff mem management of: data:in
*/
static void ifft_d(double *data, double *datad, unsigned int N) {
    scramble_d(data, datad, N);
    recursiveApply_d(data, datad, -1, N);
    rescale_d(data, datad, N);
}

/*
  Differentiation of foobar in forward (tangent) mode (with options context):
   variations   of useful results: *data
   with respect to varying inputs: *data
   RW status of diff variables: data:(loc) *data:in-out
   Plus diff mem management of: data:in
*/
void foobar_d(double *data, double *datad, unsigned int len) {
    fft_d(data, datad, len);
    double chksum = 0.0;
    int i;
    for (i = 0; i < 2*len; ++i)
        chksum += (i%7+10)*data[i];
    printf("Checksum at middle: %f\n", chksum);
    ifft_d(data, datad, len);
}

/*
  Differentiation of main as a context to call tangent code (with options context):
*/
int main() {
    static unsigned int N = 100;
    double data[2*N + 1];
    double datad[2*N + 1];
    double chksum;
    int i;
    chksum = 0.0;
    for (i = 0; i < 2*N; ++i) {
        data[i] = i + sin(i);
        chksum += (i%7+10)*data[i];
    }
    printf("Checksum at start : %f\n", chksum);
    adContextTgt_init(1.e-8, 0.87);
    adContextTgt_initReal8Array("data", data, datad, 2*N + 1);
    foobar_d(data, datad, N);
    adContextTgt_startConclude();
    adContextTgt_concludeReal8Array("data", data, datad, 2*N + 1);
    adContextTgt_conclude();
    chksum = 0.0;
    for (i = 0; i < 2*N; ++i)
        chksum += (i%7+10)*data[i];
    printf("Checksum at end   : %f\n", chksum);
}
