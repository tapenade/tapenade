! from aaf03, array
SUBROUTINE HEAD(x)
  IMPLICIT NONE
  intrinsic :: sin
  external :: bar
  real, dimension(3) :: x, y
  if (y(1) < 0.0) then
    x(1:2) = y(1:2) * 2
  end if
END SUBROUTINE

