/* Bug found by Parth Arora:
 * wrong pointer behavior in adjoint.
 * Actually no bug, rather a case where tapenade
 * rightly asks some_fn to be nocheckpoint. */

#include<stdio.h>

void some_fn(double **p, double *i) {
  *p = i;
}

void fn(double *i, double j, double *p, double *out) {
  *out += *p + j;
  some_fn(&p, i);
  *out += *p;
}

// main function
int main()
{
  double i, j, p, out;
  i = j = p = out = 0;
  fn(&i, j, &p, &out) ;
  printf("i:%f j:%f p:%f out:%f\n", i, j, p, out) ;
}
