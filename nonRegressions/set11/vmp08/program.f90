!c----- module for kinetic cell data
	module modKineticCellData
	implicit none
	save
!c----- simulation data
      type simData
        double precision :: tinit, tfinal
	end type simData

!c----- initial and injection
      double precision, allocatable :: uinit(:), yinj(:)
	double precision :: Tinj, Qinj, rhoinj, Hginj, beta, o2con
	double precision :: Psplit, Tsplit
	double precision, allocatable :: x(:), y(:)
	double precision, allocatable :: Fgin_rate(:), Fgout_rate(:)
	double precision, allocatable :: K(:), rate(:), nvec(:)

!c----- components
	integer :: naq, nol, ncc, ns, nr, ncomp, nstate
	type componentData
	  character(len=10):: name
	  double precision :: Mw, rho
        double precision :: kv1, kv4, kv5
	end type componentData

!c----- reactions
      double precision, allocatable :: A(:,:) ! stoichiometry matrix
	type reactionData
	  double precision :: alpha, Ea, delH, scale
	end type reactionData

!c----- miscellaneous
      double precision, parameter :: R = 8.3144d0
      double precision :: Pext, Pref, Tref
	double precision :: Tc, ua, kvalve
	double precision :: cpwater, cpoil, cpgas, cpsolid
	double precision :: phiv, cprock
	double precision :: Vtotal, Utotal

	type(simData) :: sd
      type(componentData), allocatable :: comp(:)
	type(reactionData), allocatable :: reac(:)


	contains

!c----- subroutine for allocating storage
	subroutine allocateKineticCellStorage()
	use modCheckAlloc
	use modKineticCellScaling
	implicit none

!c----- initial and injection
      allocate(uinit(nstate),stat=allocerror)
	call checkAlloc("uinit")
      allocate(yinj(naq+nol+ncc),stat=allocerror)
	call checkAlloc("yinj")

!c----- funVKC relevant storage
      allocate(Fgin_rate(naq+nol+ncc),stat=allocerror)
	call checkAlloc("Fgin_rate")
      allocate(Fgout_rate(naq+nol+ncc),stat=allocerror)
	call checkAlloc("Fgout_rate")
      allocate(K(naq+nol),rate(nr),nvec(ncomp),stat=allocerror)
      call checkAlloc("K")
	allocate(x(naq+nol+ncc),y(naq+nol+ncc),stat=allocerror)
	call checkAlloc("x")

!c----- other stuff
	allocate(comp(ncomp),stat=allocerror)
	call checkAlloc("comp")
      allocate(reac(nr),stat=allocerror)
	call checkAlloc("reac")
      allocate(A(ncomp,nr),stat=allocerror)
	call checkAlloc("A")
	allocate(DAmass(nr),DAheat(nr),stat=allocerror)
	call checkAlloc("DAmass")
      
	end subroutine allocateKineticCellStorage


!c----- subroutine for deallocating storage
	subroutine deallocateKineticCellStorage()
	use modCheckAlloc
      use modKineticCellScaling
	implicit none
      
	deallocate(uinit,yinj)
	deallocate(comp,reac,A)
	deallocate(DAmass,DAheat)
	deallocate(Fgin_rate,Fgout_rate)
	deallocate(nvec,K,rate,x,y)
            
	end subroutine deallocateKineticCellStorage

	end module modKineticCellData

	module modKineticCellScaling
	implicit none
      save
	double precision :: Tmin, Tmax, Pmin, Pmax
	double precision :: t_scale, nt_scale
	double precision :: Ht_scale, Ut_scale, Ft_scale, QQ_scale
	double precision, allocatable :: DAmass(:), DAheat(:)
	
	end module modKineticCellScaling

	subroutine funKineticCell(n,nd,time,u,f)
!c     main subroutine defining rhs-function for the KC
	use modKineticCellData
	use modKineticCellScaling
	implicit none
	integer :: n, nd
	double precision :: time, u(n), f(n)
!c----- local variables
      double precision, parameter :: eps = 1.d-16
	integer :: i, j
	double precision :: T, P, Ue
	double precision :: nwater,noil,noleic,ngas,nncc,nsolid,ntotal
	double precision :: rhowater, rhooil, rhogas, rhosolid
	double precision :: hwater, hoil, hgas, hsolid, hrock
	double precision :: phif, Qout, Hgin_rate, Hgout_rate, Qext
	double precision :: zhc(nol), xhc(nol), yhc(nol)
	double precision :: sw, so, sg, Vfluid, Vvoid, Vsolid
	double precision :: yH2O, nhcgas, Csolid(ns)
	double precision :: xsum

      ! unpack variables
      nvec = u(1:ncomp) * nt_scale
!c      Ue = u(ncomp+1) * Ut_scale
!c      T  = u(ncomp+2) * (Tmax-Tmin) + Tmin
!c      P  = u(ncomp+3) * (Pmax-Pmin) + Pmin
      T = Tsplit
	P = Psplit

      ! amounts of different component types
	nwater = nvec(1)
      noleic = sum(nvec(naq+1:naq+nol))
	nncc   = sum(nvec(naq+nol+1:naq+nol+ncc))
	nsolid = sum(nvec(naq+nol+ncc+1:ncomp))
      ntotal = sum(nvec(1:ncomp-ns))

      zhc = nvec(naq+1:naq+nol)/noleic      

      ! obtain equilibrium K-values 
      call equilibriumFactor(T,P,K)
      if (K(1) > 1.d0) then
        yH2O = 1.d0
	else
        yH2O = K(1)
	endif

      ! flash HC feed 
	call solrac(nol,xhc,yhc,zhc,K(naq+1),beta)
!c      write(*,*)time
      
	nhcgas = noleic * beta
      ngas   = nhcgas + nncc + nwater * yH2O
      noil   = (1.d0 - beta) * noleic
      nwater = ntotal - ngas - noil
      
      y(1)   = yH2O*nvec(1)/ngas
      do i = 1,nol
        y(naq+i)   = yhc(i)*nhcgas/ngas
      enddo
      do i = 1,ncc-1
        y(naq+nol+i) = nvec(naq+nol+i)/ngas
	enddo
      y(naq+nol+ncc) = 1.d0 - sum(y(1:naq+nol+ncc-1))

      ! compute densities
      call phaseDensity(T,P,xhc,y,nvec,rhowater,rhooil,rhogas,rhosolid)
      
      ! compute phase fractions and saturations
      !beta_w = nwater/ntotal;
      !beta_o = noil/ntotal;
      !beta_g = ngas/ntotal;
      Vfluid = nwater/rhowater + noil/rhooil + ngas/rhogas ! total fluid volume
      Vsolid = nsolid/rhosolid                              ! solid volume
      !Vcheck = 1.d0/phiv * (Vfluid + Vsolid)

      Vvoid  = Vtotal * phiv
      Csolid = nvec(naq+nol+ncc+1:ncomp)/Vvoid

      sw = nwater/rhowater/Vfluid
      so = noil/rhooil/Vfluid
      sg = 1.d0 - sw - so

      ! compute fluid porosity
	xsum = 0.d0
	do i = 1,ns
	  xsum = xsum + Csolid(i)/comp(ncomp-ns+i)%rho
	enddo
      phif = phiv * (1.d0 - xsum)

      ! compute internal energies
!c      call phaseEnthalpy(T,P,xhc,y,hwater,hoil,hgas,hsolid)
!c	hrock = cprock * (T-Tref)
      
      ! compute reaction rates
      call reactionRates(T,P,xhc,y,so,rhooil,phif,Csolid,rate)
	do i = 1,nr
	   rate(i) = rate(i)/reac(i)%scale
	enddo

      ! evaluate source/sink terms
	call sourceSink()

!c----- evaluate rhs functions
      ! ncomp ODE mass balances
	do i = 1,ncomp-ns ! fluid components
	   xsum = 0.d0
	   do j = 1,nr
	      xsum = xsum + A(i,j)*DAmass(j)*rate(j)
	   enddo
         f(i) = xsum + (Fgin_rate(i) - Fgout_rate(i))/Ft_scale
	enddo
	do i = naq+nol+ncc+1,ncomp ! solid components (no in-/outflow)
	   xsum = 0.d0
	   do j = 1,nr 
	      xsum = xsum + A(i,j)*DAmass(j)*rate(j)
	   enddo
	   f(i) = xsum
	enddo      
      
      ! 1 ODE heat balance
!c	xsum = 0.d0
!c	do i = 1,nr
!c	   xsum = xsum + DAheat(i)*rate(i)
!c	enddo
!c      f(ncomp+1) = xsum + QQ_scale*(Hgin_rate-Hgout_rate+Qext)/Ht_scale

	! 2 AEs (energy + volume constraints)
!c	xsum = nsolid*hsolid + noil*hoil + ngas*hgas + nwater*hwater
!c	xsum = Vtotal * (1.d0-phiv)*hrock + xsum
!c	xsum = Ue - xsum
!c      f(ncomp+2) = xsum/Ut_scale
!c      f(ncomp+3) = 1.d0 - (Vfluid + Vsolid)/Vvoid

	contains

!c----- subroutine computing source/sink terms
      subroutine sourceSink()
      implicit none
	double precision :: Tcfinal
      
      ! injection 
      Fgin_rate = Qinj * rhoinj * yinj  ! [mole/hr]
!c      Hgin_rate = Qinj * rhoinj * Hginj ! [kJ/hr]

      ! outflow 
!c      Qout  = kvalve * (P-Pext)     ! [m3/hr]
!c      Fgout_rate = Qout * rhogas * y
      Fgout_rate = Qinj * rhogas * y
!c      Hgout_rate = Qout * rhogas * hgas

      ! heat sink
      !Tcfinal = 450.d0 + 273.15d0
      !Tcool  = (Tcfinal-Tc)*t/tfinal + Tc;
!c      Tc = 50.d0*time*t_scale + 373.15d0
      
!c      Qext = ua * (Tc - T)
	end subroutine sourceSink

	end subroutine funKineticCell
