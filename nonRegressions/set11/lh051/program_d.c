/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_llhTests) - 29 Sep 2022 12:02
*/
/*
  Differentiation of Relu in forward (tangent) mode:
   variations   of useful results: xout[0:n-1]
   with respect to varying inputs: xout[0:n-1] xin[0:n-1]
   RW status of diff variables: xout:(loc) xout[0:n-1]:in-out
                xin:(loc) xin[0:n-1]:in
   Plus diff mem management of: xout:in xin:in
*/
// Suggestion from Kumar: without the II-LOOP,
//  tapenade unfortunately creates a push/pop. Avoidable?
inline void Relu_d(int n, double xin[n], double xind[n], double xout[n], 
        double xoutd[n]) {
    int i;
    for (i = 0; i < n; ++i)
        if (xin[i] <= 0) {
            xoutd[i] = 0.0;
            xout[i] = 0;
        } else {
            xoutd[i] = xind[i];
            xout[i] = xin[i];
        }
}
