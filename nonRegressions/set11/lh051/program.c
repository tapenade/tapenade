// Suggestion from Kumar: without the II-LOOP,
//  tapenade unfortunately creates a push/pop. Avoidable?
inline void Relu(int n, double xin[n], double xout[n]) {
  int i;
  /* $AD II-LOOP */
  for(i=0; i<n; ++i) {
    if(xin[i] <= 0 ) xout[i] = 0;
    else xout[i] = xin[i];
  }
}
