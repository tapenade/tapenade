program driver
use iso_c_binding
use test_module
implicit none
type(inputtype) :: input
type(outputtype) :: output
integer i, j
real(rp) sr
real(rp) r1
integer n
integer, allocatable :: seed(:)
! repeatable "random" input
call random_seed(size=n)
allocate(seed(n))
!call random_seed(get=seed)
!write(*,*) "n   =",n
!write(*,*) "seed=",seed
seed=123456789
call random_seed(put=seed)

! set up test program to try to recreate bug from main program
n = 4096
sr = 20000
call init_input(input,n,sr)
call init_output(output,n)
do i = 1, n
    call random_number( r1 )
    input%x(i) = r1
enddo

! call pre-process data subroutine to get frequency domain data
! from time domain data and calibrate
call foo( input, output )

! short output so we can eyeball similarities/differences
write(*,*) input%nx 
write(*,*) input%sr
write(*,*) input%x(1)
write(*,*) output%y(1)
write(*,*) output%frqy(1)
write(*,*) output%magy(1)
write(*,*) output%phsy(1)

! clean up
call clean_input(input)
call clean_output(output)
deallocate(seed)

! if MKL used, free buffers to make it easier for valgrind to look for
! memory leaks
call mkl_free_buffers()
stop
end program driver

