!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) - 28 Sep 2022 19:38
!
! FFT placeholder for tangent and adjoint computations
! We just need something for tapenade to use to create
! the appropriate interfaces with
MODULE FFT_MODULE_DIFF
  USE ISO_C_BINDING
  IMPLICIT NONE

CONTAINS
!  Differentiation of fft_forward in reverse (adjoint) mode (with options context fixinterface noISIZE):
!   gradient     of useful results: [alloc*y in fft_forward] x
!   with respect to varying inputs: [alloc*y in fft_forward] x
  SUBROUTINE FFT_FORWARD_B(x, xb, nn, nv)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nn, nv
    COMPLEX(c_double_complex), INTENT(INOUT) :: x(:)
    COMPLEX(c_double_complex), INTENT(INOUT) :: xb(:)
    COMPLEX(c_double_complex), ALLOCATABLE :: y(:)
    COMPLEX(c_double_complex), ALLOCATABLE :: yb(:)
    INTEGER :: i, j, k, offset
    COMPLEX(c_double_complex) :: cval
    COMPLEX(c_double_complex) :: cvalb
    REAL(c_double) :: pi
    INTRINSIC SIZE
    INTRINSIC ATAN
    INTRINSIC CMPLX
    INTRINSIC EXP
    ALLOCATE(yb(SIZE(x)))
    yb = (0.0_8,0.0_8)
    ALLOCATE(y(SIZE(x)))
! closer to machine precision pi
    pi = 4.0_c_double*ATAN(1.0_c_double)
    DO k=1,nv
      CALL PUSHINTEGER4(offset)
      offset = (k-1)*nn
    END DO
    yb = yb + xb
    xb = (0.0_8,0.0_8)
    DO k=nv,1,-1
      DO j=nn-1,0,-1
        cvalb = yb(j+1+offset)
        yb(j+1+offset) = (0.0_8,0.0_8)
        DO i=nn-1,0,-1
          xb(i+1+offset) = xb(i+1+offset) + CONJG(EXP(CMPLX(0.0_c_double&
&           , -(pi*2/nn*(i*j)), c_double_complex)))*cvalb
        END DO
      END DO
      CALL POPINTEGER4(offset)
    END DO
    DEALLOCATE(y)
    DEALLOCATE(yb)
  END SUBROUTINE FFT_FORWARD_B

  SUBROUTINE FFT_FORWARD(x, nn, nv)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nn, nv
    COMPLEX(c_double_complex), INTENT(INOUT) :: x(:)
    COMPLEX(c_double_complex), ALLOCATABLE :: y(:)
    INTEGER :: i, j, k, offset
    COMPLEX(c_double_complex) :: cval
    REAL(c_double) :: pi
    INTRINSIC SIZE
    INTRINSIC ATAN
    INTRINSIC CMPLX
    INTRINSIC EXP
    ALLOCATE(y(SIZE(x)))
! closer to machine precision pi
    pi = 4.0_c_double*ATAN(1.0_c_double)
    DO k=1,nv
      offset = (k-1)*nn
      DO j=0,nn-1
        cval = (0.0_c_double_complex,0.0_c_double_complex)
        DO i=0,nn-1
          cval = cval + x(i+1+offset)*EXP(CMPLX(0.0_c_double, -(2*pi*i*j&
&           /nn), c_double_complex))
        END DO
        y(j+1+offset) = cval
      END DO
    END DO
    x = y
    DEALLOCATE(y)
    RETURN
  END SUBROUTINE FFT_FORWARD

  SUBROUTINE FFT_BACKWARD(x, nn, nv)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nn, nv
    COMPLEX(c_double_complex), INTENT(INOUT) :: x(:)
    COMPLEX(c_double_complex), ALLOCATABLE :: y(:)
    INTEGER :: i, j, k, offset
    COMPLEX(c_double_complex) :: cval
    REAL(c_double) :: pi
    INTRINSIC SIZE
    INTRINSIC ATAN
    INTRINSIC CMPLX
    INTRINSIC EXP
    ALLOCATE(y(SIZE(x)))
! closer to machine precision pi
    pi = 4.0_c_double*ATAN(1.0_c_double)
    DO k=1,nv
      offset = (k-1)*nn
      DO j=0,nn-1
        cval = (0.0_c_double_complex,0.0_c_double_complex)
        DO i=0,nn-1
          cval = cval + x(i+1+offset)*EXP(CMPLX(0.0_c_double, 2*pi*i*j/&
&           nn, c_double_complex))
        END DO
        y(j+1+offset) = cval
      END DO
    END DO
    x = y
    DEALLOCATE(y)
    RETURN
  END SUBROUTINE FFT_BACKWARD

END MODULE FFT_MODULE_DIFF

