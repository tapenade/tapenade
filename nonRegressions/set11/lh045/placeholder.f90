! FFT placeholder for tangent and adjoint computations
! We just need something for tapenade to use to create
! the appropriate interfaces with
module fft_module
  use iso_c_binding
  implicit none

contains

  subroutine fft_forward(X,nn,nv)
  implicit none
  integer, intent(in) :: nn, nv
  complex(c_double_complex), intent(inout) :: X(:)
  complex(c_double_complex), allocatable   :: Y(:)
  integer i, j, k, offset
  complex(c_double_complex) :: cval
  real(c_double) pi
  allocate(Y(size(X)))

  pi =  4.0_c_double * atan(1.0_c_double)  ! closer to machine precision pi
  do k = 1,nv
    offset = (k-1)*nn
    do j = 0, nn-1
      cval = (0.0_c_double_complex,0.0_c_double_complex)
      do i = 0, nn-1
        cval = cval + X(i+1+offset)*exp(cmplx(0.0_c_double,-2*pi*i*j/nn,c_double_complex))
      enddo
      Y(j + 1 + offset) = cval
    enddo
  enddo

  X = Y

  deallocate(Y)

  return
  end subroutine

  subroutine fft_backward(X,nn,nv)
  implicit none
  integer, intent(in) :: nn, nv
  complex(c_double_complex), intent(inout) :: X(:)
  complex(c_double_complex), allocatable   :: Y(:)
  integer i, j, k, offset
  complex(c_double_complex) :: cval
  real(c_double) pi
  allocate(Y(size(X)))

  pi =  4.0_c_double * atan(1.0_c_double)  ! closer to machine precision pi
  do k = 1,nv
    offset = (k-1)*nn
    do j = 0, nn-1
      cval = (0.0_c_double_complex,0.0_c_double_complex)
      do i = 0, nn-1
        cval = cval + X(i+1+offset)*exp(cmplx(0.0_c_double,2*pi*i*j/nn,c_double_complex))
      enddo
      Y(j + 1 + offset) = cval
    enddo
  enddo

  X = Y

  deallocate(Y)

  return
  end subroutine

end module
