module test_module
use iso_c_binding
use fft_module
implicit none

integer, parameter :: rp = c_double
integer, parameter :: cp = c_double_complex

type :: inputtype
  real(rp), allocatable :: x(:)    ! time domain data
  real(rp)              :: sr      ! sample rate
  integer               :: nx      ! number of samples in x
  integer               :: init
end type inputtype

type :: outputtype
  complex(cp), allocatable :: y(:)     ! fft of x
  complex(c_float_complex), allocatable :: yy(:)     ! fft of x, but in floating point
  real(rp), allocatable    :: frqy(:)  ! frequency bins of y
  real(rp), allocatable    :: magy(:)  ! magnitude of y
  real(rp), allocatable    :: phsy(:)  ! phase of y
  integer                  :: ny       ! elements in y
  integer                   :: init 
end type outputtype

contains

subroutine init_input( input,nx, sr )
implicit none
type(inputtype) :: input
integer         :: nx
real(rp)        :: sr

call clean_input(input)
allocate(input%x(nx))
input%x = 0.0_rp
input%nx = nx
input%sr = sr
input%init = 1

return
end subroutine

subroutine clean_input( input )
implicit none
type(inputtype) :: input
if(allocated(input%x)) deallocate(input%x)
input%nx = 0
input%sr = 0
input%init = 0
return
end subroutine

subroutine init_output( output,ny )
implicit none
type(outputtype) :: output
integer          :: ny

call clean_output(output)
allocate(output%y(ny))
output%y = (0.0_rp,0.0_rp)
allocate(output%yy(ny))
output%yy = (0.0_rp,0.0_rp)
allocate(output%frqy(ny))
output%frqy = (0.0_rp,0.0_rp)
allocate(output%magy(ny))
output%magy = (0.0_rp,0.0_rp)
allocate(output%phsy(ny))
output%phsy = (0.0_rp,0.0_rp)
output%ny = ny
output%init = 1

return
end subroutine

subroutine clean_output( output )
implicit none
type(outputtype) :: output
if(allocated(output%y)) deallocate(output%y)
if(allocated(output%yy)) deallocate(output%yy)
if(allocated(output%frqy)) deallocate(output%frqy)
if(allocated(output%phsy)) deallocate(output%phsy)
if(allocated(output%magy)) deallocate(output%magy)
output%ny = 0
output%init = 0
return
end subroutine

! Bug found by C.Kotas about unrecognized "elemental" use
! of the call to CMPLX, due to missing explicit
! pointerAccess around the call arguments
subroutine foo( input, output )
implicit none
type(inputtype) :: input
type(outputtype) :: output
integer ne    ! number elements
integer nt    ! total time samples
! local
integer i
real(c_double) pi

pi =  4.0_c_double * atan(1.0_c_double)  ! closer to machine precision pi
if(input%nx .ne. output%ny) then
  write(*,*) "error: foo: input and output sizes don't match."
  stop
endif

output%y = cmplx(input%x,0.0,kind=cp)
call fft_forward(output%y,output%ny,1)

output%yy = cmplx(output%y,kind=c_float_complex)

!output%magy = abs(output%y)
!output%phsy = atan2(aimag(output%y),real(output%y))*180.0_rp / pi
!output%frqy = (input%sr/input%nx)*(/(i,i=0,output%ny-1)/)

do i = 1, output%ny
 output%magy(i) = abs(output%y(i))
 output%phsy(i) = atan2(aimag(output%y(i)),real(output%y(i)))*180.0_rp / pi
 output%frqy(i) = (input%sr/input%nx)*(i-1)
enddo

return
end subroutine foo

end module test_module
