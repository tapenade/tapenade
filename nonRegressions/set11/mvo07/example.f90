!!!tapenade -msglevel 15 -msginfile -noisize -outputlanguage fortran90 -d -tgtvarname _fw -tgtfuncname _fw -head "cost(x)>(f)" -o example example.f90

module mo_model
  implicit none
  integer, parameter :: nsp = 2
  integer, parameter :: nt = 3
  integer, parameter :: nlayer = 2
  real(kind=8) :: p1
  real(kind=8) :: sif743(nsp,nt)
end module mo_model

module l2sm_constants
  implicit none
  real(kind=8) :: gu_scale
end module l2sm_constants

subroutine cost(n,x,m,f)
  implicit none
  ! arguments
  integer, intent(in) :: n, m
  real(kind=8), intent(in) :: x(n)
  real(kind=8), intent(out) :: f
  ! local
  real(kind=8) :: obsdiff(m)

  call misfit(n,x,m,obsdiff)
  f = 0.5 * sum(obsdiff**2)

end subroutine cost

subroutine misfit(n,x,m,obsdiff)
  implicit none
  ! arguments
  integer(kind=4), intent(in) :: n, m
  real(kind=8), intent(in)    :: x(n)
  real(kind=8), intent(out)   :: obsdiff(m)
  ! local
  real(kind=8) :: y(m), yobs(m), syobs(m)
  logical :: maskobs(m)

  call getobs(m,yobs,syobs,maskobs)
  ! simulate obs
  call evalf(n,x,m,y)
  where(maskobs)
     obsdiff = (y-yobs)/syobs
  elsewhere
     obsdiff = 0.
  endwhere
end subroutine misfit

subroutine getobs(m, obs, sobs, mskobs)
  implicit none
  integer, intent(in) :: m
  real(kind=8), intent(out) :: obs(m), sobs(m)
  logical, intent(out) :: mskobs(m)

  obs = 0.5_8
  sobs = 1._8
  mskobs = .true.
end subroutine getobs

subroutine x2model(n, x)
  use mo_model,only:p1
  use l2sm_constants
  implicit none
  integer, intent(in) :: n
  real(kind=8), intent(in) :: x(n)
  !--
  p1 = x(1)
  gu_scale = x(2)
end subroutine x2model

subroutine evalf(n, x, m, y)
  ! evaluates the relevant function, i.e.
  ! computes response of relevant outputs y to a given control vector x
  use mo_model, only: nsp
  implicit none
  ! arguments
  integer, intent(in) :: n, m ! dimensions of control vector and relevant outputs
  real(kind=8), intent(in) :: x(n) ! control vector (to be optimised)
  real(kind=8), intent(out) :: y(m) ! relevant outputs
  ! local
  integer, parameter :: nspdim = nsp ! aux dimension
  ! 
  call x2model(n, x) ! mapping control vector to parameters
  call timeloop(nspdim)
  call operators(m,y) ! mapping simulation results onto output vector
end subroutine evalf

subroutine timeloop(nsp)
  use mo_model, only: nlayer, nt, p1, sif743
  !-- mvo::uncommenting use-declaration below yields proper initialisation of gu_scaleb
  !        in backward mode
  !use l2sm_constants
  implicit none
  !-- arguments
  integer, intent(in) :: nsp
  !-- local
  real(kind=8) :: sif_c(nsp)
  real(kind=8) :: J(nsp, nlayer)
  real(kind=8) :: lai(nsp), leaf_r(nsp), leaf_t(nsp), soil_albedo(nsp)
  integer :: it,il,isp
  

  lai = 0._8
  leaf_r = 0.5_8
  leaf_t = 0.2_8
  soil_albedo = 0.3_8
  do it=1,nt
     lai = (1+p1)*(it+p1) + it*exp(it*p1)
     do il=1,nlayer
        J(:,il) = exp(it*il*p1)
     enddo
     !--mvo ::problems with 'mask' in backward     lai = max(lai,1._8)
     call pseudo_sif(nsp, nlayer, J, sif_c)

     sif743(:,it) = sif_c(:)* 360.528344 * 1000 ! conversion to [mW/m2/sr/nm] in Tropomi 743 nm band
  end do
end subroutine timeloop

subroutine pseudo_sif(nsp,nlayer,J,sif_c)
  use l2sm_constants, only : gu_scale
  implicit none
  !-- arguments
  integer, intent(in) :: nsp, nlayer
  real(kind=8), intent(in) :: J(nsp,nlayer)
  real(kind=8), intent(out) :: sif_c(nsp)
  !-- local
  !maximum photochemical quantum yield of PSII
  !Gu et al: "Highly conserved across species 
  !under unstressed conditions"
  real(kind=8), parameter :: psi_PSIImax=0.83_8

  !fraction of open PSII reaction centres
  !this is not well known and likely variable
  !with environmental conditions
  real(kind=8), parameter :: q_L=0.8_8

  !ratio of k_D to k_F, first order rate constants
  !for fluorescence and heat dissipation
  !Gu et al: "Presumably a constant scalar"
  real(kind=8), parameter :: k_DF=19_8

  real(kind=8), parameter :: factor = (1-psi_PSIImax)/(q_L*psi_PSIImax*(1+k_DF))
  integer :: il
  real(kind=8) :: sif(nsp,nlayer)

  sif_c = 0._8
  do il=1,nlayer
     sif_c = sif_c + J(:,il)*gu_scale*factor
  end do
end subroutine pseudo_sif

subroutine operators(m,y)
  implicit none
  !-- arguments
  integer, intent(in) :: m
  real(kind=8), intent(out) :: y(m)
  !-- local
  real(kind=8) :: sif(m)

  call operator_sif(m, sif)

  y = sif
end subroutine operators

subroutine operator_sif(m,sif)
  use mo_model, only: nsp,nt,sif743
  use l2sm_constants
  implicit none
  integer, intent(in) :: m
  real(kind=8), intent(out):: sif(m)
  integer :: it,isp,im
  sif=0._8
  do it=1,nt
     do isp=1,nsp
        im = nsp*(it-1)+isp
        if(im.le.m) then
           sif(im) = sif743(isp,it)
        endif
     enddo
  enddo

end subroutine operator_sif
