module directory
!
! Strong typing imposed
   implicit none
!
! Only subroutine interfaces, the length of the character
! component, and the I/O unit number are public
!
! Module constants
   character(len=*), private, parameter :: eot = "End-of-Tree....."
   integer, parameter, public :: unit = 4,   & ! I/O unit number
                                 max_char = 16 ! length of character 
                                               ! component
!
! Define the basic tree type
   type, private :: node
       character(len=max_char)     :: name            ! name of node
       real, pointer, dimension(:) :: y               ! stored real data
       type(node), pointer        :: parent           ! parent node
       type(node), pointer        :: sibling          ! next sibling node
       type(node), pointer        :: child            ! first child node
   end type node
!
! Module variables
   type(node), pointer, private   :: current     ! current node
   type(node), pointer, private   :: forest_root ! the root of the forest
   integer,private                :: max_data    ! max size of data array
   character(len=max_char), private, allocatable, target, dimension(:)  &
                                                               :: names
                                           ! for returning list of names
! The module procedures
 
contains

   subroutine dump_tree(root)
      character(len=*), intent(in) :: root
! Write out a complete tree followed by an end-of-tree record
! unformatted on the file unit.
      call find (root)
      if (associated(current)) then
         call tree_out(current)
      end if
      write(unit = unit) eot, 0, eot
   contains 
      recursive subroutine tree_out (localroot)
! Traverse a complete tree or subtree, writing out its contents
         type(node), intent(in) :: localroot     ! root node of tree
! Local variable
         type(node), pointer    :: child
!
         write(unit = unit) localroot%name, size(localroot%y), localroot%y, &
                            localroot%parent%name
         child => localroot%child
         do
            if (.not.associated(child)) then
              exit
            end if
            call tree_out (child)
            child => child%sibling
         end do
      end subroutine tree_out
   end subroutine dump_tree
 
end module directory
