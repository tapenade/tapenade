
/*============ TEMPLATE FUNCTIONS ===============*/
template <class T>
T Min(T x, T y){ return x<y ? x : y; }              // The generic template code is not seen, only the specialization.

class A {
    int i;   
  public:
    template <class T>
    void add(T valeur);
};

template <class T>
void A::add(T valeur) { i=i+((int) valeur); }

/*============ TEMPLATE CLASSES ===============*/




/*============== INSTANCIATIONS ===============*/

int main() {

  int ia=1, ib=2;
  int imin = Min<int>(ia,ib);
  short sa=1, sb=2;
  short smin = Min<short>(sa,sb);

  
  return 0;
}
