! bug from inversionLab, about dubious
! INTENTs placed on primal and adjoint params

subroutine cost(n,x,m,f)
  implicit none
  ! arguments
  integer, intent(in) :: n, m
  real(kind=8), intent(in) :: x(n)
  real(kind=8), intent(out) :: f
  ! local
  integer, parameter :: nt = 3
  integer :: i
  real(kind=8) :: req(n)
  real(kind=8) :: rad(n)
  rad = x
  do i=1,nt
     call foo(n, rad, req)
     rad = x + rad * real(i, kind=8) * req
  enddo

  f = sum(rad)
end subroutine cost

subroutine foo(n, active, reqout)
  implicit none
  integer, intent(in) :: n
  real(kind=8), intent(inout) :: active(n)
  real(kind=8), intent(out) :: reqout(n)

  active = active*real(n, kind=8)
  reqout = 2._8
end subroutine foo
