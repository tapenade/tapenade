! from set03/lh060
! gfortran -> error head is already defined

module DO
   interface 
      subroutine head(i2,i1,vo)
        double precision i1
        double precision i2,vo
      end subroutine head
   end interface

contains
      subroutine head(i1,i2,o)
        double precision i1,i2,o
        o = i1/i2
      end subroutine
 end module DO
