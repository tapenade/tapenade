! Tapenade bug on interrupted propagation of pointers destinations
! in case of calls to interfaces
!----------------------------------------------------------------------
! Function to illustrate bug in tapenade
! <path to tapenade>/bin/tapenade -head "processdata" -d  TapenadeTest1.f90

MODULE TapenadeTest1
    IMPLICIT NONE
    intrinsic ceiling
    intrinsic modulo
    intrinsic reshape

    TYPE :: PrmType
        INTEGER nfx     
        INTEGER nfm      
        INTEGER nel      
        INTEGER nfr      
    END TYPE

    TYPE :: wPrmType
        INTEGER   ns
        INTEGER   nc
        INTEGER   nt
        REAL(8) lfac 
    END TYPE wPrmType

    TYPE :: ParameterDataType
        TYPE(PrmType)  :: Prm
        TYPE(wPrmType) :: wPrm
    END TYPE ParameterDataType

    TYPE :: aWFType
        COMPLEX(8), ALLOCATABLE  :: dRot(:,:,:,:,:)
        REAL(8)   , ALLOCATABLE  :: dScl(:,:,:,:)
    END TYPE aWFType

! tapenade didn't like something in the MKL include files in first draft. 
! mkl_blas.fi, mkl_lapack.fi, mkl_trans.fi
! 1. one of them used "double complex" and that became just "complex" (with warning)
! 2. another used the syntax "use, intrinsic :: iso_c_binding" (which tapenade didn't recognize)
! we'll just cut out the interfaces we need and hope that works for now
! from mkl_trans.fi
      INTERFACE
      subroutine MKL_ZIMATCOPY ( ordering, trans, rows, cols, alpha, AB, lda, ldb )
      character*1        ordering, trans
      integer            rows, cols, lda, ldb
      complex*16     alpha
      complex*16     AB( * )
      END
      END INTERFACE

! from mkl_lapack.fi
      INTERFACE
      SUBROUTINE ZGESDD(JOBZ,M,N,A,LDA,S,U,LDU,VT,LDVT,WORK,LWORK,RWORK,IWORK,INFO)
      CHARACTER          JOBZ
      INTEGER            INFO,LDA,LDU,LDVT,LWORK,M,N
      INTEGER            IWORK(*)
      DOUBLE PRECISION   RWORK(*),S(*)
      COMPLEX*16         A(LDA,*),U(LDU,*),VT(LDVT,*),WORK(*)
      END
      END INTERFACE

! Functions that I've removed from module for clarity
      INTERFACE
      SUBROUTINE  FUN1( Scl, L_X, NFS, NS, NC, NT, nx, lfac )
      INTEGER, VALUE           :: NFS, NS, NT, NC, nx
      REAL(8), VALUE           :: lfac
      REAL(8), intent(out)     :: Scl(:,:,:,:)  
      REAL(8), intent(in)      :: L_X(:) 
      END
      END INTERFACE

      INTERFACE
      SUBROUTINE  FUN2 (dwData, dData, dRot, dScl, nfs, ns, nc, nt, nfx )
      INTEGER   , VALUE        :: nfs, ns, nc, nt, nfx
      COMPLEX(8), intent(out)  :: dwData(NFS,NS*NC*NT,NFX)
      COMPLEX(8), intent(in)   :: dData(NFS,NS*NC*NT,NFX)
      COMPLEX(8), intent(in)   :: dRot(NFS,NS,NC,NC,NT)
      REAL(8)   , intent(in)   :: dScl(NFS,NS,NC,NT)
      END
      END INTERFACE

CONTAINS

!----------------------------------------------------------------------
    SUBROUTINE ProcessData( dwData, dData, aWF, PARAMS )
!----------------------------------------------------------------------
    IMPLICIT NONE
    COMPLEX(8)            :: dwData(:,:,:)  ! nfr-x-nel-x-nfx
    COMPLEX(8)            :: dData(:,:,:)   ! nfr-x-nel-x-nfx
    TYPE( aWFType )         :: aWF
    TYPE(ParameterDataType) :: PARAMS

! Output of SVD routines
    COMPLEX(8), ALLOCATABLE :: U_D(:)
    COMPLEX(8), ALLOCATABLE :: V_D(:)
    REAL(8)   , ALLOCATABLE :: D_D(:)
! Local variables
    COMPLEX(8), ALLOCATABLE :: dlocal(:)
    INTEGER i_sta
    INTEGER numB
    INTEGER nx, nfx
    INTEGER nc, ns, nt, nel
    INTEGER nfr

    INTEGER i, lwork, info, itmp
    INTEGER i1_b, i1_e, i2_b, i2_e
    INTEGER i3_b, i3_e, i4_b, i4_e
    INTEGER, ALLOCATABLE :: iwork(:)
    REAL(8), ALLOCATABLE :: rwork(:)
    COMPLEX(8), ALLOCATABLE :: work(:)

! initialize outputs
    aWF%dRot = (0.0_8,0.0_8)
    aWF%dScl = 0.0_8
    dwData = (0.0_8,0.0_8)

! get some dimensions from the parameter structure
    nx = PARAMS%Prm%nfx
    nfx = PARAMS%Prm%nfx
    ns = PARAMS%wPrm%ns 
    nc = PARAMS%wPrm%nc 
    nt = PARAMS%wPrm%nt 
    nfr = PARAMS%Prm%nfr 
    nel = ns*nc*nt
    ALLOCATE( dlocal( nfr * nel * nx ), STAT = i_sta )

! Fill the local array (need local copy since zgesdd overwrited input)
    numB = nfr * ns * nt
    dlocal = reshape( dData, (/nfr*nel*nx/) )

    ALLOCATE( U_D(nx*nx*numB), STAT = i_sta )
    ALLOCATE( V_D(nc*nc*numB), STAT = i_sta )
    ALLOCATE( D_D(nc*numB), STAT = i_sta )
! call zgesdd once for workspace query
! do I need to allocate the rwork/iwork for this?
! note that here nx >= nc
    itmp = max(5*nc+7, 2*nx+2*nc+1)
    ALLOCATE( rwork(nc*itmp ))
    ALLOCATE( iwork(8*nc) )
    lwork = -1 
    ALLOCATE( work(1) )
    call zgesdd( 'a', &
        nx, nc, &
        dlocal(1:nx*nc), &
        nx, &
        D_D(1:nc), & 
        U_D(1:nx*nx), &
        nx, &
        V_D(1:nc*nc), &
        nc, &
        work, lwork, rwork, iwork, info )
    lwork = work(1)
    DEALLOCATE( work )
    DEALLOCATE( rwork, iwork )

    do i = 1, numB
! compute SVD for each matrix in batch
! leaving the allocations inside the loop since we expect this to be 
! in an openmp loop eventually, and each thread will need its own 
! workspace.
        ALLOCATE( rwork(nc*itmp ))
        ALLOCATE( iwork(8*nc) )
        ALLOCATE( work( lwork ) )
        i1_b = nx*nc*(i-1)+1
        i1_e = i*nx*nc
        i2_b = nc*(i-1)+1
        i2_e = i*nc
        i3_b = nx*nx*(i-1)+1
        i3_e = i*nx*nx
        i4_b = nc*nc*(i-1)+1
        i4_e = i*nc*nc
        work(1) = SIN(work(1))
        call zgesdd( 'a', &
            nx, nc, &
            dlocal(i1_b:i1_e), &
            nx, &
            D_D(i2_b:i2_e), & 
            U_D(i3_b:i3_e), &
            nx, &
            V_D(i4_b:i4_e), &
            nc, &
            work, lwork, rwork, iwork, info )
        call mkl_zimatcopy('c', 'c', nc, nc, (1.0_8,0.0_8), V_D(i4_b:i4_e), nc, nc)
        DEALLOCATE( work, rwork, iwork )
    enddo

    aWF%dRot = reshape( V_D, (/nfr, ns, nc, nc, nt/) )

    CALL FUN1 &
        ( aWF%dScl, D_D, nfr, ns, nc, nt,     &
        nx, PARAMS%wPrm%lfac )

    IF( ALLOCATED( D_D ) ) DEALLOCATE( D_D )
    IF( ALLOCATED( U_D ) ) DEALLOCATE( U_D )
    IF( ALLOCATED( V_D ) ) DEALLOCATE( V_D )
    IF( ALLOCATED( dlocal ) ) DEALLOCATE( dlocal )

    CALL FUN2 &
        (dwData, dData, aWF%dRot, aWF%dScl, &
        nfr, ns, nc, nt, nfx )


    RETURN
    END SUBROUTINE ProcessData

END MODULE TapenadeTest1
