  SUBROUTINE visf(a)
  IMPLICIT NONE

  INTEGER, PARAMETER :: n = 10
  REAL, DIMENSION(n), POINTER :: a, vfac, vfad
  INTEGER :: ibc

  vfac => a
  vfad => a

  DO ibc = 1,n
    vfac(ibc) = ibc**2.0
    vfad(ibc) = vfad(ibc)**2.0
  END DO
 END SUBROUTINE visf

