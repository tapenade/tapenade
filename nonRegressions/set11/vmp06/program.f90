module directory
!
! Strong typing imposed
   implicit none
!
! Only subroutine interfaces, the length of the character
! component, and the I/O unit number are public
   public  :: add_node, remove_node, retrieve
   private :: find, remove
!
! Module constants
   character(len=*), private, parameter :: eot = "End-of-Tree....."
   integer, parameter, public :: unit = 4,   & ! I/O unit number
                                 max_char = 16 ! length of character 
                                               ! component
!
! Define the basic tree type
   type, private :: node
       character(len=max_char)     :: name            ! name of node
       real, pointer, dimension(:) :: y               ! stored real data
       type(node), pointer        :: parent           ! parent node
       type(node), pointer        :: sibling          ! next sibling node
       type(node), pointer        :: child            ! first child node
   end type node
!
! Module variables
   type(node), pointer, private   :: current     ! current node
   type(node), pointer, private   :: forest_root ! the root of the forest
   integer,private                :: max_data    ! max size of data array
   character(len=max_char), private, allocatable, target, dimension(:)  &
                                                               :: names
                                           ! for returning list of names
! The module procedures
 
contains

   subroutine find(name)
      character(len=*), intent(in) :: name
! Make the module variable current point to the node with given name,
! or be null if the name is not there.
      type(node), pointer    :: root
! For efficiency, we search the tree rooted at current, and if this
! fails try its parent and so on until the forest root is reached.
      if (associated(current)) then
         root => current
         nullify (current)
      else
         root => forest_root
      end if
      do
         call look(root)
         if (associated(current)) then
            return
         end if
         root => root%parent
         if (.not.associated(root)) then
            exit
         end if
      end do
   contains 
      recursive subroutine look(root)
         type(node), pointer    :: root
! (type(node), intent(in), target :: root   is standard conforming too)
! Look for name in the tree rooted at root. If found, make the
! module variable current point to the node
         type(node), pointer    :: child
!
         if (root%name == name) then
            current => root
         else
            child => root%child
            do
               if (.not.associated(child)) then
                 exit
               end if
               call look(child)
               if (associated(current)) then
                 return
               end if
               child => child%sibling
            end do
         end if
      end subroutine look
   end subroutine find
 
   subroutine add_node(name, name_of_parent, data)
      character(len=*), intent(in)             :: name, name_of_parent
! For a root, name_of_parent = ""
      real, intent(in), optional, dimension(:) :: data
! Allocate a new tree node of type node, store the given name and
! data there, set pointers to the parent and to its next sibling
! (if any). If the parent is not found, the new node is treated as
! a root. It is assumed that the node is not already present in the
! forest.
      type(node), pointer :: new_node
!
      allocate (new_node)
      new_node%name = name
      if (present(data)) then
         allocate(new_node%y(size(data)))
         new_node%y = data
         max_data = max(max_data, size(data))
      else
         allocate(new_node%y(0))
      end if
!
! If name of parent is not null, search for it. 
! If not found, print message.
      if (name_of_parent == "") then
         current => forest_root
      else
         call find (name_of_parent)
         if (.not.associated(current)) then
            print *, "no parent ", name_of_parent, " found for ", name
            current => forest_root
         end if
      end if
      new_node%parent => current
      new_node%sibling => current%child
      current%child => new_node
      nullify(new_node%child)
   end subroutine add_node
 
   subroutine remove_node(name)
      character(len=*), intent(in) :: name
! Remove node and the subtree rooted on it (if any),
! deallocating associated pointer targets.
      type(node), pointer :: parent, child, sibling
!
      call find (name)
      if (associated(current)) then
         parent =>  current%parent
         child => parent%child
         if (.not.associated(child, current)) then
! Make it the first child, looping through the siblings to find it
! and resetting the links
            parent%child => current
            sibling => child
            do
              if (associated (sibling%sibling, current)) then
                exit
              end if
              sibling => sibling%sibling
            end do
            sibling%sibling => current%sibling
            current%sibling => child
         end if
         call remove(current)
      end if
   end subroutine remove_node

   recursive subroutine remove (old_node)
! Remove a first child node and the subtree rooted on it (if any),
! deallocating associated pointer targets.
      type(node), pointer :: old_node
      type(node), pointer :: child, sibling
!
      child => old_node%child
      do
         if (.not.associated(child)) then
           exit
         end if
         sibling => child%sibling
         call remove(child)
         child => sibling
      end do
! remove leaf node
      if (associated(old_node%parent)) then
         old_node%parent%child => old_node%sibling
      end if
      deallocate (old_node%y)
      deallocate (old_node)
   end subroutine remove
 
   subroutine retrieve(name, data, parent, children)
      character(len=*), intent(in)            :: name
      real, pointer, dimension(:)             :: data
      character(len=*), intent(out)           :: parent
      character(len=*), pointer, dimension(:) :: children
! Returns a pointer to the data at the node, the name of the
! parent, and a pointer to the names of the children.
      integer :: counter, i
      type(node), pointer :: child
!
      call find (name)
      if (associated(current)) then
         data => current%y
         parent = current%parent%name
! Count the number of children
         counter = 0
         child => current%child
         do
           if (.not.associated(child)) then
             exit
           end if
           counter = counter + 1
           child => child%sibling
         end do
         deallocate (names)
         allocate (names(counter))
! and store their names
         children => names
         child => current%child
         do i = 1, counter
            children(i) = child%name
            child => child%sibling
         end do
      else
         nullify(data)
         parent = ""
         nullify(children)
      end if
   end subroutine retrieve

end module directory
