! Bug Shu-Jie Li, shujie <shujie@csrc.ac.cn>, Dec 5, 2020
subroutine bugshujie(a,b,ibc)
  real a,b
  integer ibc
  integer WALL_INV_SOLI, WALL_INV_SYMM, WALL_NS_ADI_TEMP, WALL_HEAT_FLUX
  integer ISOTHERMAL_SURF(13), MOVING_SURF(13), ROT_SURF(13)
  WALL_INV_SOLI = 2
  WALL_INV_SYMM = 3
  WALL_NS_ADI_TEMP = 4
  WALL_HEAT_FLUX = 6
  DO i = 1,13
     ISOTHERMAL_SURF(i) = 20+i
     MOVING_SURF = 30+i
     ROT_SURF(i) = 40+i
  ENDDO
  
  SELECT CASE(IBC)
!------------------------------------------------------------------------------
      CASE(WALL_INV_SOLI,WALL_INV_SYMM,WALL_NS_ADI_TEMP,WALL_HEAT_FLUX,ISOTHERMAL_SURF(10):ISOTHERMAL_SURF(1),MOVING_SURF(10):MOVING_SURF(1),ROT_SURF(10):ROT_SURF(1))
         a = 2*b
      CASE DEFAULT
         a = 3*b
      END SELECT
    end subroutine bugshujie

