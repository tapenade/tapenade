module mo1
  implicit none
  interface
     subroutine iface(x,y)
       implicit none
       real(kind=8) :: x, y
     end subroutine iface
  end interface
end module mo1

module mo2
  implicit none
  interface
     subroutine iface(x,y)
       implicit none
       integer :: x, y
     end subroutine iface
  end interface
end module mo2

subroutine foo(x,y)
  use mo1
!  use mo2
  implicit none
  real(kind=8), intent(in) :: x
  real(kind=8), intent(out) :: y

  call iface(x,y)
  y = y*x
end subroutine foo

subroutine iface(x,y)
  implicit none
       real(kind=8) :: x, y
!  integer :: x, y

  y = x*x
end subroutine iface
