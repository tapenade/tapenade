/* Inspired from Seism/Boreas/Alif
 * Reproduce bug of duplicate declarations of diff variable
 * when initialized, both in TANGENT and in ADJOINT_FWD
 */
#include <stdio.h>
#include <stdlib.h>

typedef struct{
        int M;
        float* values;
} p_Vector;
typedef p_Vector* Vector;

/* $AD NOCHECKPOINT */
Vector VecNew(int size){
  Vector vector = (Vector)malloc(sizeof(p_Vector));
  vector->M = size;
  vector->values = (float*)calloc(size,sizeof(float));
  return vector;
}

void foo(float* x) {
   Vector ys = (void *)0;
   ys = VecNew(5) ;
   ys->values[3] = *x * *x ;
   *x = ys->values[3] * 2.5 ;
}

/* $AD NOCHECKPOINT */
void bar(float* x) {
   Vector ys = (void *)0;
   ys = VecNew(5) ;
   ys->values[3] = *x * *x ;
   *x = ys->values[3] * 2.5 ;
}

float root(float x) {
  foo(&x) ;
  bar(&x) ;
  x = x*x ;
  return x ;
}

int main() {
  float x = 3.2 ;
  float y ;
  y = root(x) ;
  printf ("y=%f\n", y) ;
  return 0;
}
