/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) - 24 Feb 2022 12:44
*/
#include <adContext.h>
/* Inspired from Seism/Boreas/Alif
 * Reproduce bug of duplicate declarations of diff variable
 * when initialized, both in TANGENT and in ADJOINT_FWD
 */
#include <stdio.h>
#include <stdlib.h>
typedef struct {
    int M;
    float *values;
} p_Vector;
typedef struct {
    float *values;
} p_Vector_diff;
typedef p_Vector * Vector;
typedef p_Vector_diff * Vector_diff;

/*
  Differentiation of VecNew in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
                VecNew:in-out
*/
p_Vector_diff * VecNew_d(int size, Vector *VecNew) {
    Vector vector;
    p_Vector_diff *vectord;
    int ii1;
    vectord = (p_Vector_diff *)malloc(sizeof(p_Vector_diff));
    vectord->values = NULL;
    vector = (p_Vector *)malloc(sizeof(p_Vector));
    vector->M = size;
    vectord->values = (float *)calloc(size, sizeof(float));
    for (ii1 = 0; ii1 < size; ++ii1)
        vectord->values[ii1] = 0.0;
    vector->values = (float *)calloc(size, sizeof(float));
    *VecNew = vector;
    return vectord;
}

/*
  Differentiation of foo in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   variations   of useful results: [alloc*(*vector.values) in VecNew]
                *([alloc*vector in VecNew].values) *x
   with respect to varying inputs: *x
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
                x:in
*/
void foo_d(float *x, float *xd) {
    Vector ys = (void *)0;
    p_Vector_diff *ysd = (void *)0;
    ysd = VecNew_d(5, &ys);
    ysd->values[3] = 2*(*x)*(*xd);
    ys->values[3] = (*x)*(*x);
    *xd = 2.5*ysd->values[3];
    *x = ys->values[3]*2.5;
}

/*
  Differentiation of bar in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   variations   of useful results: *x
   with respect to varying inputs: [alloc*(*vector.values) in VecNew]
                *([alloc*vector in VecNew].values) *x
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
                x:in
*/
void bar_d(float *x, float *xd) {
    Vector ys = (void *)0;
    p_Vector_diff *ysd = (void *)0;
    ysd = VecNew_d(5, &ys);
    ysd->values[3] = 2*(*x)*(*xd);
    ys->values[3] = (*x)*(*x);
    *xd = 2.5*ysd->values[3];
    *x = ys->values[3]*2.5;
}

/*
  Differentiation of root in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   variations   of useful results: root
   with respect to varying inputs: x
   RW status of diff variables: root:out x:in
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
*/
float root_d(float x, float xd, float *root) {
    foo_d(&x, &xd);
    bar_d(&x, &xd);
    xd = 2*x*xd;
    x = x*x;
    *root = x;
    return xd;
}

/*
  Differentiation of main as a context to call tangent code (with options no!refineADMM no!SpareInits fixinterface context):
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
*/
int main() {
    float x = 3.2;
    float xd = 0.0;
    float y;
    float yd;
    adContextTgt_init(1.e-4, 0.87);
    adContextTgt_initReal4("x", &x, &xd);
    yd = root_d(x, xd, &y);
    adContextTgt_startConclude();
    adContextTgt_concludeReal4("y", y, yd);
    adContextTgt_conclude();
    printf("y=%f\n", y);
    return 0;
}
