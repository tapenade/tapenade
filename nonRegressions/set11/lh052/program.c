#include <complex.h>

int function(double complex X,double complex *Y)
{
	int i;
	double complex tempI;
	double complex Z;
	double complex plop, plop2;

	Z = 1.+0.*I;
	tempI = 0.+1.*I;
	plop = 0.+0.*I;
	plop2 = 0.;

	for (i = 0;i < 10;i++)
	{
		tempI += (tempI*X+1);
		plop += tempI*i;
		plop2 *= (i+plop) * I;
	}
	for (i = 0;i < 34;i++)
	{
		Z += (tempI+Z)*X;
	}

	*Y = 2*Z*tempI*X/plop+plop2;
	return 0;
}