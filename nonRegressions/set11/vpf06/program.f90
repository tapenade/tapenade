program test 
   interface 
      subroutine head(i2,i1,vo)
        double precision i1
        double precision i2,vo
      end subroutine head
   end interface

  double precision x
  call head(x, x, x)
end program test

module m
   interface 
      subroutine head(i)
        real i
      end subroutine head
   end interface

contains

subroutine t
   call head(0.0)
end subroutine t

end module m

subroutine head(i1,i2,o)
   double precision i1,i2,o
   o = i1/i2
   print*, "head"
end subroutine
