! This code to test the ability to remove
! primal formal arguments from differentiated
! routines, when they are proved diff-dead.
program main
  real*8 :: x,y
  x = 2.5
  call foo(x,y)
  print *,'y=',y
end program main

subroutine foo(x,y)
  real*8 :: x,y,c
  real*8, dimension(20) :: A,B
  c = x
  A(:) = x
  B(:) = 1.2 * x
  call bar(A,B,c)
  y = SUM(A*B) + c
end subroutine foo

subroutine bar(A,B,c)
  real*8, dimension(*) :: A,B
  real *8 :: c
  integer i
  do i=1,15
     A(i) = A(i) + 3.0*B(i+2)
  enddo
  c = c*c
end subroutine bar
  
