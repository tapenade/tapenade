!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (23-bugadto) -  9 Apr 2021 17:50
!
MODULE MODSZ_DIFF
  IMPLICIT NONE
  INTEGER, PARAMETER :: kindary=SELECTED_INT_KIND(r=8)
END MODULE MODSZ_DIFF

!  Differentiation of pp as a context to call tangent code (with options context no!StripPrimalModules):
PROGRAM PP_D
  USE MODSZ_DIFF
  IMPLICIT NONE
  REAL*8 :: x, tt(5)
  REAL*8 :: xd, ttd(5)
  INTEGER :: bcdat4(5)
  INTEGER*8 :: bcdat8(5)
  INTEGER(kindary) :: bcdat(5)
  x = 1.0
  tt(1:5) = 0.5
  bcdat4(1) = 5
  bcdat4(2) = 3
  bcdat4(3) = 2
  bcdat4(4) = 4
  bcdat4(5) = 2
  bcdat8(1) = 5
  bcdat8(2) = 3
  bcdat8(3) = 2
  bcdat8(4) = 4
  bcdat8(5) = 2
  bcdat(1) = 5
  bcdat(2) = 3
  bcdat(3) = 2
  bcdat(4) = 4
  bcdat(5) = 2
  CALL ADCONTEXTTGT_INIT(1.e-8_8, 0.87_8)
  CALL ADCONTEXTTGT_INITREAL8ARRAY('tt'//CHAR(0), tt, ttd, 5)
  CALL ADCONTEXTTGT_INITREAL8('x'//CHAR(0), x, xd)
  CALL FOO_D(x, xd, tt, ttd, bcdat4, bcdat8, bcdat)
  CALL ADCONTEXTTGT_STARTCONCLUDE()
  CALL ADCONTEXTTGT_CONCLUDEREAL8ARRAY('tt'//CHAR(0), tt, ttd, 5)
  CALL ADCONTEXTTGT_CONCLUDEREAL8('x'//CHAR(0), x, xd)
  CALL ADCONTEXTTGT_CONCLUDE()
  PRINT*, 'kindary=', kindary, ' x=', x
END PROGRAM PP_D

!  Differentiation of foo in forward (tangent) mode (with options context no!StripPrimalModules):
!   variations   of useful results: x
!   with respect to varying inputs: tt x
!   RW status of diff variables: tt:in x:in-out
SUBROUTINE FOO_D(x, xd, tt, ttd, bcdat4, bcdat8, bcdat)
  USE MODSZ_DIFF
  IMPLICIT NONE
  REAL*8 :: x, tt(5)
  REAL*8 :: xd, ttd(5)
  INTEGER :: isrf
  INTEGER :: bcdat4(5)
  INTEGER :: ifc4
  INTEGER*8 :: bcdat8(5)
  INTEGER*8 :: ifc8
  INTEGER(kindary) :: bcdat(5)
  INTEGER(kindary) :: ifc
  INTRINSIC SIN
! Standard INTEGER
  DO isrf=1,5
    DO ifc4=1,bcdat4(isrf)
      xd = xd + tt(ifc4)*ttd(isrf) + tt(isrf)*ttd(ifc4)
      x = x + tt(isrf)*tt(ifc4)
    END DO
  END DO
! Static sized INTEGER
  DO isrf=1,5
    DO ifc8=1,bcdat8(isrf)
      xd = xd + tt(ifc8)*ttd(isrf) + tt(isrf)*ttd(ifc8)
      x = x + tt(isrf)*tt(ifc8)
    END DO
  END DO
! Dynamic sized INTEGER
  DO isrf=1,5
    DO ifc=1,bcdat(isrf)
      xd = xd + tt(ifc)*ttd(isrf) + tt(isrf)*ttd(ifc)
      x = x + tt(isrf)*tt(ifc)
    END DO
  END DO
  xd = COS(x)*xd
  x = SIN(x)
END SUBROUTINE FOO_D

SUBROUTINE PP_NODIFF()
  USE MODSZ_DIFF
  IMPLICIT NONE
  REAL*8 :: x, tt(5)
  INTEGER :: bcdat4(5)
  INTEGER*8 :: bcdat8(5)
  INTEGER(kindary) :: bcdat(5)
  x = 1.0
  tt(1:5) = 0.5
  bcdat4(1) = 5
  bcdat4(2) = 3
  bcdat4(3) = 2
  bcdat4(4) = 4
  bcdat4(5) = 2
  bcdat8(1) = 5
  bcdat8(2) = 3
  bcdat8(3) = 2
  bcdat8(4) = 4
  bcdat8(5) = 2
  bcdat(1) = 5
  bcdat(2) = 3
  bcdat(3) = 2
  bcdat(4) = 4
  bcdat(5) = 2
  CALL FOO_NODIFF(x, tt, bcdat4, bcdat8, bcdat)
  PRINT*, 'kindary=', kindary, ' x=', x
END SUBROUTINE PP_NODIFF

SUBROUTINE FOO_NODIFF(x, tt, bcdat4, bcdat8, bcdat)
  USE MODSZ_DIFF
  IMPLICIT NONE
  REAL*8 :: x, tt(5)
  INTEGER :: isrf
  INTEGER :: bcdat4(5)
  INTEGER :: ifc4
  INTEGER*8 :: bcdat8(5)
  INTEGER*8 :: ifc8
  INTEGER(kindary) :: bcdat(5)
  INTEGER(kindary) :: ifc
  INTRINSIC SIN
! Standard INTEGER
  DO isrf=1,5
    DO ifc4=1,bcdat4(isrf)
      x = x + tt(isrf)*tt(ifc4)
    END DO
  END DO
! Static sized INTEGER
  DO isrf=1,5
    DO ifc8=1,bcdat8(isrf)
      x = x + tt(isrf)*tt(ifc8)
    END DO
  END DO
! Dynamic sized INTEGER
  DO isrf=1,5
    DO ifc=1,bcdat(isrf)
      x = x + tt(isrf)*tt(ifc)
    END DO
  END DO
  x = SIN(x)
END SUBROUTINE FOO_NODIFF

