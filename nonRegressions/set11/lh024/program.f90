module MODSZ
  Integer, Parameter :: kindAry = selected_int_kind(r=8)
end module MODSZ

subroutine foo(x,TT,bcdat4,bcdat8,bcdat)
  USE MODSZ
  REAL*8 x, TT(5)
  INTEGER isrf
  INTEGER :: bcdat4(5)
  INTEGER :: ifc4
  INTEGER*8 :: bcdat8(5)
  INTEGER*8 :: ifc8
  INTEGER(kindary) :: bcdat(5)
  INTEGER(kindary) :: ifc

  ! Standard INTEGER
  DO isrf=1,5
     DO ifc4=1,bcdat4(isrf)
        x = x + TT(isrf)*TT(ifc4)
     ENDDO
  ENDDO
  ! Static sized INTEGER
  DO isrf=1,5
     DO ifc8=1,bcdat8(isrf)
        x = x + TT(isrf)*TT(ifc8)
     ENDDO
  ENDDO
  ! Dynamic sized INTEGER
  DO isrf=1,5
     DO ifc=1,bcdat(isrf)
        x = x + TT(isrf)*TT(ifc)
     ENDDO
  ENDDO
  x = sin(x)
END subroutine foo

PROGRAM PP
USE MODSZ
REAL*8 x, TT(5)
INTEGER :: bcdat4(5)
INTEGER*8 :: bcdat8(5)
INTEGER(kindary) :: bcdat(5)
x = 1.0
TT(1:5) = 0.5
bcdat4(1) = 5
bcdat4(2) = 3
bcdat4(3) = 2
bcdat4(4) = 4
bcdat4(5) = 2
bcdat8(1) = 5
bcdat8(2) = 3
bcdat8(3) = 2
bcdat8(4) = 4
bcdat8(5) = 2
bcdat(1) = 5
bcdat(2) = 3
bcdat(3) = 2
bcdat(4) = 4
bcdat(5) = 2
CALL foo(x,TT,bcdat4,bcdat8,bcdat)
print *, 'kindary=',kindary,' x=',x
END PROGRAM PP
