MODULE MESH
  IMPLICIT NONE

CONTAINS
  SUBROUTINE TEST(x)
     real :: x
     x = x * 2
  END SUBROUTINE
END MODULE

program main
  use mesh
  real :: z
  z = 3
  call test(z)
  print*, z
end program
