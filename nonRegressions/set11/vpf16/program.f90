! bug report Thu, 26 Mar 2020

module ESMF_CalendarMod
  implicit none
  integer :: ESMF_Calendar_dummy
  type ESMF_Calendar
     logical :: Set = .false.
  end type ESMF_Calendar
end module ESMF_CalendarMod

module mo
  use ESMF_CalendarMod, only: ESMF_Calendar, ESMF_Calendar_dummy
  implicit none
  private ESMF_Calendar_dummy
  private ESMF_Calendar
end module mo
