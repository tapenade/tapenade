function bar(x) result(barresult)
  real, dimension(:), allocatable :: x
  real :: barresult
  if (.not. allocated(x)) then
    allocate(x(2))
  end if
  x = x * 2
  barresult = x(0)
end function bar
