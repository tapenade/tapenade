module m_foo_i
  implicit none

  interface 
   subroutine foo(x)
     real, dimension(:), allocatable :: x
   end subroutine foo
  end interface

end module m_foo_i

module m_bar_i
  implicit none

  interface 
   function bar(x) result(barresult)
     real, dimension(:), allocatable :: x
     real :: barresult
   end function bar
  end interface

end module m_bar_i

subroutine foo(x)
  use m_bar_i
  real, dimension(:), allocatable :: x
  x(0) = bar(x)
end subroutine foo


program main
  use m_foo_i
  use m_bar_i
  real, dimension(:), allocatable :: x
  x(0) = bar(x)
  call foo(x)
end program main
