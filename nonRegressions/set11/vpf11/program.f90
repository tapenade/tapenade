subroutine top()
   real :: x, y
   y = foo(x)
end subroutine top

subroutine bar(x)
  real :: x
  x = x * x
end subroutine bar

! pour tester differentiation -context d'une fonction:
function foo(cf2)
  REAL :: cf2
  real ::foo
  call bar(cf2)
  foo = cf2
end function foo

