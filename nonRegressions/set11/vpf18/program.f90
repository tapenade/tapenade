module mo
  implicit none

  type some_type
     real :: f
  end type some_type
end module mo

subroutine foo(n, x, y)
  use mo, only:some_type
  implicit none
  !-- arguments
  integer, intent(in) :: n
  real, intent(in) :: x(n)
  real, intent(out) :: y
  !-- local
  type(some_type) :: u, v

  u%f = x(1)
  v%f = x(2)

  y = u%f - v%f

end subroutine foo

program main
  implicit none
  integer, parameter :: n = 2
  real :: x(n), y
  integer :: i

  do i=1,n
     x(i) = real(i*i)
  end do

  call foo(n,x,y)
  print*, 'INFO::y=',y
end program main
