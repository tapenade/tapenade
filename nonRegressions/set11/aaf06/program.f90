MODULE MESH
  IMPLICIT NONE

CONTAINS
  REAL FUNCTION TEST(x)
     real :: x
     test = x * 2
  END FUNCTION
END MODULE

program main
  use mesh
  real :: z
  real :: result
  z = 3
  result = test(z)
  print*, result
end program
