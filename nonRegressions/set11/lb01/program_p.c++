//        Generated by TAPENADE     (INRIA, Ecuador team)
//  Tapenade 3.13 (r7065) - 21 Sep 2018 17:10
//
#include <initializer_list>
void otherFunction(const int&, const char *, bool);
int doStuff(int);

inline int sum(int a, int b) {
    return a + b;
}

int main() {
    //     p points to Undef
    //     ptr points to Undef
    //     ptr2 points to Undef
    int a = 0;
    int b = 0;
    int c;
    int *ptr;
    int *ptr2;
    int k[10];
    int& ref = a;
    int&& number = 2;
    ptr2 = nullptr;
    static double pi = 3.14159265358979;
    extern double theta;
    register bool sense = theta < pi;
    int * autok = &a;
    //     autok points to a
    const int& deux = 2;
    volatile int trois = 3;
    _Atomic int cinq = 5;
    double d = (double)a;
    int *p;
    int i;
    ptr2 = NULL();
    a = (int)a;
    a = b;
    a = b + c;
    a = b - c;
    a = b*c;
    a = b/c;
    a = b%c;
    ptr = &c;
    //     ptr points to c
    a = *ptr2;
    a = b >> c;
    a = b << c;
    b += c;
    b -= c;
    b /= c;
    b *= c;
    b %= c;
    a &= b;
    a &= b;
    a |= b;
    a |= b;
    a ^= b;
    a ^= b;
    a >>= b;
    a <<= b;
    a = b;
    a = -b;
    b++;
    b--;
    ++c;
    --c;
    a = b && c;
    a = b && c;
    a = b || c;
    a = b || c;
    a = !b;
    a = !b;
    a = b ^ c;
    a = b ^ c;
    a = b == c;
    a = b != c;
    a = b != c;
    a = b >= c;
    a = b <= c;
    a = b < c;
    a = b > c;
    a = (b, c);
    a = ~b;
    a = ~b;
    a = b & c;
    a = b & c;
    a = b | c;
    a = b | c;
    a = k[2];
    a = (b > c ? b : c);
    if (b > c)
        a = b;
    if (b > c)
        a = b;
    else
        a = c;
    if (b > c) {
        a = b;
        a++;
    } else
        a = c;
    if (b > c) {
        a = b;
        a++;
    } else {
        a = c;
        a--;
    }
    if (b > c) {
        a = b;
        a++;
    } else if (b < c) {
        a = c;
        a--;
    } else
        a = 0;
    while(a < 100)
        a++;
    while(a < b)
        a += b;
    do {
        a--;
        goto label100;
    } while(a > 0);
  label100:
    for (int i = 0; i < 10; ++i) {

    }
    for (int i = 0, *p = &i; i < 10; i++) {

    }
    char cstr[6] = "Hello";
    for (int n = 0; char c = cstr[n]; ++n)
        a++;
    for (doStuff(1); doStuff(b); doStuff(b++))
        if (b > 10)
            break;
    for (const int& i : k)
        a += i;
    for (int& i : k)
        a += i;
    for (int i : k)
        a += i;
    for (int i : {0, 1, 2, 3, 4, 5})
        a += i;
    otherFunction(2, "s", true);
    switch (a) {
    case 0 :
        a = 2;
    case 1 :
        a = 0;
        break;
    case 3 :
        a = 1;
    default :
        a = 3;
    }
    switch (a) {
    case 0 :
        switch (b) {
        case 1 :
            a = 3;
            break;
        default :
            a = b;
        }
        break;
    default :
        a = 3;
    }
  LOOP:
    a--;
    do {
        if (a == 15)
            goto LOOP;
        else
            a++;
    } while (a < 20);
    try {
        throw 10;
    } catch (int e) {
    } 
    return 0;
}

void otherFunction(int& a, char *b, bool c) {}

int doStuff(int a) {
    return a;
}
