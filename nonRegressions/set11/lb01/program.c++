// #include <cstdlib>
#include <initializer_list>

void otherFunction(const int&, const char*, bool);       // varDeclaration(functionDeclarator) 
int doStuff(int);                                        // idem without argument name
inline int sum(int a, int b){ return a+b; }                  // inline NOT DETECTED (YET)

int main(){

/*========== DECLARATIONS & DEFINITIONS =============*/
    int a=0, b=0, c;                                 // declarations are splitted
    int *ptr, *ptr2;                                 // pointerType
    int k[10];                                       // arrayType
    int& ref = a;                                    // referenceType, annoted by ident:"lvalue"
    int&& number = 2;                                // referenceType, annoted by ident:"rvalue"
    ptr2 = nullptr;                                  // nullptr becomes a call to NULL()


    static double pi = 3.14159265358979;             // Storage-class specifiers: extern, auto, static, register
    extern double theta;                             //        are stored in the type. (modifiedType) 
    register bool sense = (theta < pi);              //
    auto autok = &a;                                 // "auto" types are resolved, we will not remember it was auto !

    const int& deux = 2;                             // Type qualifiers: const, volatile, restrict
    volatile int trois = 3;                          //        are also stored in the type with
    //float * restrict a;    //(C only, not C++)     //        modifiedType.
    _Atomic int cinq = 5;                            // _Atomic is parsed as a type itself: "_Atomic(int)" (DIRTY)

/*===================== CASTS =======================*/
    double d = (double) a;                           // annotation: "castStyle: CStyle"
    a = int(a);                                      // annotation: "castStyle: FunctionnalCast"

/*=================== OPERATORS =====================*/
    a = b;      //assign
    a = b + c;  //add
    a = b - c;  //sub
    a = b * c;  //mul
    a = b / c;  //div
    a = b % c;  //mod
    ptr = &c;   //adress
    a = *ptr2;  //pointerAccess
    a = b >> c; //rightShift
    a = b << c; //leftShift
    b += c;     //plusAssign
    b -= c;     //minusAssign
    b /= c;     //divAssign
    b *= c;     //timesAssign
    b %= c;     //modAssign
    a &= b;     //andAssign
    a and_eq b; //andAssign
    a |= b;     //iorAssign
    a or_eq b;  //iorAssign
    a ^= b;     //xorAssign
    a xor_eq b; //xorAssign
    a >>= b;    //rightShiftAssign
    a <<= b;    //leftShiftAssign
    a = +b;     // NOT PARSED (YET)
    a = -b;     // DIRTY: parsed as sub(0,b)
    b++;        //++postfix
    b--;        //--postfix
    ++c;        //++prefix
    --c;        //--prefix
    a = b && c; // and
    a = b and c;// and
    a = b || c; // or
    a = b or c; // or
    a = !b;     // not
    a = not b;  // not
    a = b ^ c;  // xor
    a = b xor c;// xor
    a = b == c; // equ
    a = b != c; // neq
    a = b not_eq c;// neq
    a = b >= c; // ge
    a = b <= c; // le
    a = b < c;  // lt
    a = b > c;  // gt
    a = (b, c); // expressions
    a = ~b;     // NOT PARSED (YET)
    a = compl b;// NOT PARSED (YET)
    a = b & c;  // NOT PARSED (YET)
    a = b bitand c; // NOT PARSED (YET)
    a = b | c;  // NOT PARSED (YET)
    a = b bitor c; // NOT PARSED (YET)
    a = k[2];   //pointerAccess 


/*================= CONDITIONS =====================*/
    a = (b>c) ? b : c;                               // ifExpression
    if (b>c) a = b;                                  // if
    if (b>c) a = b; else a = c;                      // if then else
    if (b>c) {a = b; a++;} else a = c;               // same with
    if (b>c) {a = b; a++;} else {a = c; a--;}        //       blockstatements
    if (b>c) {a = b; a++;}                           //       or another if
    else if (b<c) {a = c; a--;}                      //
    else a=0;                                        //

/*================== LOOPS =========================*/
    while (a<100)               //
        a++;                    //  while loops (while)
    while (a<b) { a += b;}      //

    do {                        // do loop (until)
        a--;                    //
        break;                  // break
    } while (a>0);              //


    //~ //for(b=0 ; b<100; ){ b++; }                  // NOT PARSED (YET)

    for (int i = 0; i < 10; ++i){
        continue;                               // continue
    }

    for (int i = 0, *p = &i; i < 10; i++) {     // multiple declarations: expressions(varDeclaration, varDeclaration)
                                                // DIRTY: should be one varDeclaration with several declarators
    }

    char cstr[] = "Hello";
    for (int n = 0; char c = cstr[n]; ++n)      // condition may be a declaration
        a++;

    for ( doStuff(1);                           // init-statement can be an expression,
          doStuff(b);                           // condition can be an expression,
          doStuff(b++) )                        // increment can be an expression.
        if(b > 10) break;

    for (const int& i : k)                      // forall loop, access by const reference
        a += i;
 
    for (int& i : k)                            // forall loop, access by reference
        a += i;
        
    for (int i : k)                             // forall loop, access by value
        a += i;
 
    for (int i : {0, 1, 2, 3, 4, 5})            // NOT PARSED (YET)
        a += i;                                 // (because of template vars in inlcude <initializer_list>)
 
/*===================== CALLS =======================*/
    otherFunction(2, "s", true);

/*================= SWITCHS CASES ===================*/
    switch (a){         // switch
        case 0:         // switchCase
            a = 2;
        case 1:
            a = 0;
            break;
        case 3:
            a = 1;
        default:        // default is a "case" with 'none' as first child.
            a = 3;
    }

    switch (a){         // switch in a switch 
        case 0:
            switch (b){
                case 1:
                    a = 3;
                    break;
                default:
                    a = b;
            }
            break;
        default:
            a = 3;
    }

/*================= LABELS & GOTO ====================*/
    LOOP:               // label
    a--;
    do {
        if( a == 15)
            goto LOOP;  //goto
        a++;
    } while( a < 20 );

    ENDLOOP:

/*================== TRY CATCH =======================*/

    try {               // try
        throw 10;       // throw
    }
    catch (int e) {     // catch
        //exit(e);
    }


/*=================== RETURN ==========================*/

    return 0;           //return
}


void otherFunction(int &a, char *b, bool c){
    //new variable scope
}

int doStuff(int a){return a;}
