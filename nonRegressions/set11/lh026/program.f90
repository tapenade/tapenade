! Bug found by InversionLab.
! Another case of wrong inline of MAX in WHERE
subroutine cost(n,x,m,f)
  implicit none
  ! arguments
  integer, intent(in) :: n, m
  real(kind=8), intent(in) :: x(n)
  real(kind=8), intent(out) :: f
  ! local
  real(kind=8) :: snow(n)
  where(x.lt.-1)
     snow = x
     snow = 2*max(0.,max(snow,0.5))
  elsewhere
     snow = 0.
  end where
  f = sum(snow)
end subroutine cost
