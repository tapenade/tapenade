!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 22 Apr 2021 17:07
!
! Bug found by InversionLab.
! Another case of wrong inline of MAX in WHERE
SUBROUTINE COST(n, x, m, f)
  IMPLICIT NONE
! arguments
  INTEGER, INTENT(IN) :: n, m
  REAL(kind=8), INTENT(IN) :: x(n)
  REAL(kind=8), INTENT(OUT) :: f
! local
  REAL(kind=8) :: snow(n)
  INTRINSIC MAX
  INTRINSIC SUM
  REAL(kind=8), DIMENSION(n) :: y1
  REAL(kind=8), DIMENSION(n) :: max1
  WHERE (x .LT. -1) 
    snow = x
    WHERE (snow .LT. 0.5) 
      y1 = 0.5
    ELSEWHERE
      y1 = snow
    END WHERE
    WHERE (0. .LT. y1) 
      max1 = y1
    ELSEWHERE
      max1 = 0.
    END WHERE
    snow = 2*max1
  ELSEWHERE
    snow = 0.
  END WHERE
  f = SUM(snow)
END SUBROUTINE COST

