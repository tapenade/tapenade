!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 22 Apr 2021 17:07
!
!  Differentiation of cost in reverse (adjoint) mode:
!   gradient     of useful results: f x
!   with respect to varying inputs: f x
!   RW status of diff variables: f:in-zero x:incr
! Bug found by InversionLab.
! Another case of wrong inline of MAX in WHERE
SUBROUTINE COST_B(n, x, xb, m, f, fb)
  IMPLICIT NONE
! arguments
  INTEGER, INTENT(IN) :: n, m
  REAL(kind=8), INTENT(IN) :: x(n)
  REAL(kind=8) :: xb(n)
  REAL(kind=8) :: f
  REAL(kind=8) :: fb
! local
  REAL(kind=8) :: snow(n)
  REAL(kind=8) :: snowb(n)
  INTRINSIC MAX
  INTRINSIC SUM
  REAL(kind=8), DIMENSION(n) :: y1
  REAL(kind=8), DIMENSION(n) :: y1b
  REAL(kind=8), DIMENSION(n) :: max1
  REAL(kind=8), DIMENSION(n) :: max1b
  LOGICAL, DIMENSION(n) :: mask
  LOGICAL, DIMENSION(n) :: mask0
  LOGICAL, DIMENSION(n) :: mask1
  mask = x .LT. -1
  WHERE (mask) 
    snow = x
    mask0 = snow .LT. 0.5
    WHERE (mask0) 
      y1 = 0.5
    ELSEWHERE
      y1 = snow
    END WHERE
    mask1 = 0. .LT. y1
    WHERE (mask1) 
      max1 = y1
    ELSEWHERE
      max1 = 0.
    END WHERE
    snow = 2*max1
  ELSEWHERE
    snow = 0.
  END WHERE
  snowb = 0.0_8
  snowb = fb
  max1b = 0.0_8
  y1b = 0.0_8
  WHERE (.NOT.mask) 
    snowb = 0.0_8
  ELSEWHERE
    max1b = 2*snowb
    snowb = 0.0_8
    WHERE (.NOT.mask1) 
      max1b = 0.0_8
    ELSEWHERE
      y1b = max1b
    END WHERE
    WHERE (.NOT.mask0) snowb = snowb + y1b
    xb = xb + snowb
  END WHERE
  fb = 0.0_8
END SUBROUTINE COST_B

