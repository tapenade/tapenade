
program airfoil_profile
  implicit none
  integer :: Resolution , MNdeEff, i, j, nurbsr
  integer, parameter :: nmax = 5000, nbspl = 2
  double precision, dimension (nmax) :: XNode, YNode
  double precision, dimension (nmax,nbspl) :: X, Y

  resolution = 100
  
	do j = 1, nbspl
		if(j == 1) then
			nurbsr = 0
		else
			nurbsr = 1
		endif

  	call BSPLINE (Resolution , MNdeEff, XNode, YNode, j, nurbsr)
		do i = 1, resolution  	
			x(i,j) = xnode(i)
      y(i,j) = ynode(i)
    enddo
  enddo    
  
	open(30, file = 'Bspline.dat')
  do i = 1, resolution
  	write(30,*) (x(i,j), y(i,j), j = 1, nbspl)
	enddo
  close(30)
end program

!this is the code for the drawing of a NURB curve
module control_point
	implicit none  

	type cpoint
		real*8 :: x
    real*8 :: y  
  end type cpoint
end module control_point

module commondata
  use control_point
	implicit none

	save
    
  integer :: p 					! degree of the curve
  integer :: control 		! number of control points: control+1
  integer :: m 					! m=control + p + 1, number of knots: m+1
	type(cpoint) :: pts, curve_pts
  real*8, dimension (:), allocatable ::U, N, x, y
  type(cpoint), dimension (:), allocatable :: cpts
end module commondata

subroutine BSPLINE (Resolution , MNdeEff, XNode, YNode, BSPLNO, NURBSR)
  use commondata
  implicit none
  
  integer :: Resolution, MNdeEff, nbspl ! number of point on spline to be calculated:: resolution
  integer :: bsplno
  real*8, dimension (resolution) :: XNode, YNode

	real*8, dimension(resolution) :: u_pts
	real*8 :: u_point, increment, lower, upper
	integer :: i, j, ndat, nurbsr
  character (20) :: line	
  
  
!read the step file

	open(15, file = 'bspline_input.step', status = 'old')
  
  ndat=0
  do
    read(15,*) line
    
    if(line=="$newdata")then
      ndat=ndat+1
      if(ndat==nbspl)then
        read(15,*)
  			read(15,*) p
			  read(15,*)
			  read(15,*) control
			  read(15,*)
			  read(15,*) m
				
				allocate (U(0:m), N(0:p), x(0:control), y(0:control))
				allocate (cpts(0:control))
 !read control points 
				read(15,*)
				do i = 0, control
			    read(15,*) x(i), y(i)
			  enddo
! read knot vector
				upper = -1.e6
				lower = 1.e6	
				read(15,*)
			  do i = 0, m
			    read(15,*) U(i)
					if(U(i) < lower) lower = U(i)
					if(U(i) > upper) upper = U(i)
				enddo          
      	exit
      end if
    end if
    
  end do
  
  close(15)

  do i = 0, control
    pts%x = x(i)
	  pts%y = y(i)
		cpts(i) = pts
	enddo

!	determine the points to be calculated on Bspline
	increment = (upper - lower) / (resolution	- 1)

	do i = 0, resolution-1
    u_pts(i+1) = lower  + i * increment
	enddo
	
	if(NURBSR == 0) then
		do i = 2, resolution-1
  		u_point = u_pts(i)
  	  call curvepoint(u_point)
  	  xnode(i) = curve_pts%x
  	  ynode(i) = curve_pts%y
		enddo

		xnode(1) = cpts(0)%x
  	ynode(1) = cpts(0)%y
  	xnode(resolution) = cpts(control)%x
  	ynode(resolution) = cpts(control)%y
	elseif(NURBSR == 1) then	 !reverse determination of the nodes on the curve
		do j = resolution-1, 2, -1
			i= resolution - j + 1
  		u_point = u_pts(j)
    	call curvepoint(u_point)
    	xnode(i) = curve_pts%x
    	ynode(i) = curve_pts%y
		enddo

	  xnode(1) = cpts(control)%x
	  ynode(1) = cpts(control)%y
	  xnode(resolution) = cpts(0)%x
	  ynode(resolution) = cpts(0)%y
	endif
  
  deallocate (U, N, x, y)
	deallocate (cpts)
  
	MNdeEff = resolution

	return 
end

subroutine curvepoint(u_point)
	use commondata
  implicit none

  real*8 :: u_point
  integer :: i, span, findspan
  
	span = findspan(u_point)
  call basisfuns(span, u_point)
	curve_pts%x = 0.
  curve_pts%y = 0.
	do i = 0, p
  	curve_pts%x = curve_pts%x + N(i) * cpts(span-p+i)%x
    curve_pts%y = curve_pts%y + N(i) * cpts(span-p+i)%y
	enddo
  return
end	

integer function findspan(u_point)
	use commondata
  implicit none

  integer :: low, high, mid
  real*8 :: u_point

  if(u_point == U(control+1)) findspan = control
  low = p
  high = control+1
  mid = (low + high) / 2
  do while(u_point < U(mid) .or. u_point >= U(mid+1)) 
    if(u_point < U(mid)) then
      high = mid
    else
      low = mid
    endif
    mid = (low + high) / 2
!    if(.not.(u_point < U(mid) .or. u_point >= U(mid+1))) exit
  enddo
  findspan = mid
end function

subroutine basisfuns(i, u_point)
	use commondata
  implicit none

  integer :: i, j, r
  real*8, dimension(1:p) :: left, right
  real*8 :: u_point, saved, temp

  N(0) = 1.0
  do j = 1, p
    left(j) = u_point - U(i+1-j)
    right(j) = U(i+j) - u_point
    saved = 0.0
    do r = 0, j-1
      temp = N(r) / (right(r+1) + left(j-r))
      N(r) = saved + right(r+1) * temp
      saved = left(j-r) * temp
    enddo
    N(j) = saved 
  enddo
	return
end        
    
