template <class TT>
class myHolder {
  TT value ;
public:
  myHolder() {
  }
  ~myHolder() {
  }
  void set(TT vv) {
    value = vv ;
  }
  void operate(TT newval) {
    value = value*newval ;
  }
  TT get() {
    return value ;
  }
};

void test(float in, double ind, double *out) {
  myHolder<float> hf ;
  myHolder<double> hd ;
  hf.set(in) ;
  hd.set(ind) ;
  hf.operate(in) ;
  hd.operate(ind) ;
  *out = hf.get() * hd.get() ;
}

int main() {
  float x ;
  double y ;
  double z ;
  x = 1.5 ;
  y = 2.4 ;
  test(x,y,&z) ;
  return 0 ;
}
