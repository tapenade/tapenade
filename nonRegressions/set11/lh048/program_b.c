/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_cuda) -  2 Jun 2022 17:21
*/
#include <adStack.h>

/*
  Differentiation of foo in reverse (adjoint) mode:
   gradient     of useful results: *x *y
   with respect to varying inputs: *x *y
   RW status of diff variables: x:(loc) *x:incr y:(loc) *y:in-out
   Plus diff mem management of: x:in y:in

 TBR's recomputation algorithm should be extended to diff pointers */
void foo_b(float *x, float *xb, float *y, float *yb) {
    float a, b, c[10], d;
    float ab, bb, cb[10], db;
    float *p;
    float *pb;
    int i;
    int ii1;
    for (i = 0; i < 10; ++i)
        c[i] = i*(*x);
    for (ii1 = 0; ii1 < 10; ++ii1)
        cb[ii1] = 0.0;
    for (i = 9; i > -1; --i) {
        db = *yb;
        pb = &(cb[i]);
        p = &(c[i]);
        a = *x + i;
        b = a/2.0;
        bb = a*(*p)*db;
        ab = b*(*p)*db + bb/2.0;
        *pb = *pb + a*b*db;
        *xb = *xb + ab;
    }
    for (i = 9; i > -1; --i) {
        *xb = *xb + i*cb[i];
        cb[i] = 0.0;
    }
}
