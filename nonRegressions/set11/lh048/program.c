/* TBR's recomputation algorithm should be extended to diff pointers */
void foo(float *x, float *y) {
  float a,b,c[10],d ;
  float *p ;
  int i ;
  for (i=0 ; i<10 ; ++i) {
    c[i] = i*(*x) ;
  }
  for (i=0 ; i<10 ; ++i) {
    a = *x+i ;
    p = &(c[i]) ;
    b = a/2.0 ;
    d = a*b*(*p) ;
    *y = *y + d ;
  }
}
