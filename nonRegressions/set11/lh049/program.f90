! Bug found by CHEN, Jilong, Aug 3 2022.
! because MAXLOC is "half-understood",
!  there was no push/pop of loc.
subroutine minitest(a,b)
  real a(100),b(100)
  integer i,loc
  do i = 1,100
     loc = maxloc(a,1)
     a(loc) = a(loc)*b(i)
  enddo
end subroutine minitest
  
