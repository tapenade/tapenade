c this is a comment
c  over two lines
      program foo
      integer i,j
      real A(20)
      i = 5
      j = 0
C$om this is not an OMP pragma!
      j = j+1
c$omp parallel do private(i), private(j),
c$omp+ shared(a), shared(b), reduction(+:s)
      do i = 1,10
         A(i)=0.0
      enddo
c$omp paralleldo shared(a,b), reduction(+:s)
      do i = 1,11
         A(i)=0.0
      enddo
c$omp end parallel do
c$omp parallel do private(i,j), reduction(+:s)
c and this is a comment
      do i = 1,12
         A(i)=0.0
      enddo

      end
