
! Test OMP schedule
subroutine foo_all(a1,a2,a3,b1,b2,y1,y2,y3,y4,y5,n,res)
  integer n
  real, dimension(n) :: a1,a2,a3,b1,b2
  real :: y1,y2,y3,y4,y5,res
  res = 0
  call foo_firstprivate(a2, y2, n)
  res = res + SUM(a2) + y2
  call foo_reduction(a1, y3, n)
  res = res + SUM(a1) + y3
  !$AD NOCHECKPOINT
  call foo_firstprivate(a2, y2, n)
  res = res + SUM(a2) + y2
  !$AD NOCHECKPOINT
  call foo_reduction(a1, y3, n)
  res = res + SUM(a1) + y3
  res = res*res
end subroutine foo_all

subroutine foo_firstprivate(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$omp parallel do firstprivate(y) schedule(static)
  do i=1,n
    a(i) = y
    y = 0
  end do
end subroutine

subroutine foo_reduction(a, y, n)
  implicit none
  integer i, n
  real a(n), y
  !$omp parallel do reduction(+:y), schedule(dynamic)
  do i=1,n
    y = y + a(i)
  end do
end subroutine

program main
  integer, parameter :: n = 10
  real, dimension(n) :: a1,a2,a3,b1,b2
  real :: y1,y2,y3,y4,y5,res
  a1 = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  a2 = (/2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9/)
  a3 = (/3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9/)
  b1 = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  b2 = (/2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9/)
  y1 = 1.1
  y2 = 1.2
  y3 = 1.3
  y4 = 1.4
  y5 = 1.5
  res = 0.0
  call foo_all(a1,a2,a3,b1,b2,y1,y2,y3,y4,y5,n,res)
  print *, 'result: ', res
end program main
