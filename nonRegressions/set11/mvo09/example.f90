module mo_model
  implicit none
  integer, parameter :: nsp = 2
  integer, parameter :: nt = 3
  integer, parameter :: nlayer = 2
  real(kind=8) :: p1
  real(kind=8) :: sif743(nsp,nt)
  integer, parameter :: ntxture = 3
  real(kind=8) :: gu_scale(ntxture) = 1._8
end module mo_model

subroutine cost(n,x,m,f)
  implicit none
  ! arguments
  integer, intent(in) :: n, m
  real(kind=8), intent(in) :: x(n)
  real(kind=8), intent(out) :: f
  ! local
  real(kind=8) :: obsdiff(m)

  call misfit(n,x,m,obsdiff)
  f = 0.5 * sum(obsdiff**2)

end subroutine cost

subroutine misfit(n,x,m,obsdiff)
  implicit none
  ! arguments
  integer(kind=4), intent(in) :: n, m
  real(kind=8), intent(in)    :: x(n)
  real(kind=8), intent(out)   :: obsdiff(m)
  ! local
  real(kind=8) :: y(m), yobs(m), syobs(m)
  logical :: maskobs(m)

  call getobs(m,yobs,syobs,maskobs)
  ! simulate obs
  call evalf(n,x,m,y)
  where(maskobs)
     obsdiff = (y-yobs)/syobs
  elsewhere
     obsdiff = 0.
  endwhere
end subroutine misfit

subroutine getobs(m, obs, sobs, mskobs)
  implicit none
  integer, intent(in) :: m
  real(kind=8), intent(out) :: obs(m), sobs(m)
  logical, intent(out) :: mskobs(m)

  obs = 0.5_8
  sobs = 1._8
  mskobs = .true.
end subroutine getobs

subroutine x2model(n, x)
  use mo_model, only: p1, gu_scale
  implicit none
  integer, intent(in) :: n
  real(kind=8), intent(in) :: x(n)

  p1 = x(1)
  gu_scale = x(2)
end subroutine x2model

subroutine evalf(n, x, m, y)
  ! evaluates the relevant function, i.e.
  ! computes response of relevant outputs y to a given control vector x
  use mo_model, only: nsp
  implicit none
  ! arguments
  integer, intent(in) :: n, m ! dimensions of control vector and relevant outputs
  real(kind=8), intent(in) :: x(n) ! control vector (to be optimised)
  real(kind=8), intent(out) :: y(m) ! relevant outputs
  ! local
  integer, parameter :: nspdim = nsp ! aux dimension
  ! 
  call x2model(n, x) ! mapping control vector to parameters
  call timeloop(nspdim)
  call operators(m,y) ! mapping simulation results onto output vector
end subroutine evalf

subroutine timeloop(nsp)
  use mo_model, only: nlayer, nt, p1, gu_scale, sif743
  implicit none
  !-- arguments
  integer, intent(in) :: nsp
  !-- local
  real(kind=8) :: sif_c(nsp)
  integer :: it,il
  

  sif_c = 0._8
  do it=1,nt
     sif_c = it*p1*gu_scale(1)
     sif743(:,it) = sif_c(:)* 360.528344 * 1000 ! conversion to [mW/m2/sr/nm] in Tropomi 743 nm band
  end do
end subroutine timeloop

subroutine operators(m,y)
  implicit none
  !-- arguments
  integer, intent(in) :: m
  real(kind=8), intent(out) :: y(m)
  !-- local
  real(kind=8) :: sif(m)

  call operator_sif(m, sif)

  y = sif
end subroutine operators

subroutine operator_sif(m,sif)
  use mo_model, only: nsp,nt,sif743
  implicit none
  integer, intent(in) :: m
  real(kind=8), intent(out):: sif(m)
  integer :: it,isp,im
  sif=0._8
  do it=1,nt
     do isp=1,nsp
        im = nsp*(it-1)+isp
        if(im.le.m) then
           sif(im) = sif743(isp,it)
        endif
     enddo
  enddo

end subroutine operator_sif
