/** Exhibits bugs:
 * - infinite loop in DiffLivenessAnalyzer due to non-increasing bitsets
 * - Missing propagation of "OnDiffPtr" info on call func1
 */
void foo(double *x, double *y, int* ilum) {
  int i,j ;
  int n ;
  n = 1 ;
  *y = 2.0 * *x ;
  for (i=0 ; i<10 ; ++i) {
    for (j=0 ; j<3 ; ++j) {
/*       if (n==10) continue */
      *y = func1(5*n, *y * *x, ilum+4*j) ;
/*       if (*x > *y) { */
/*         *y = sin(*y) ; */
/*         *y = func2(2 * *y) ; */
/*         continue ; */
/*       } else if (*y != 0.0) { */
/*         *y = 1.0 / *y ; */
/*         n = 2*n ; */
/*         *y = func3(3 * *y) ; */
/*         break ; */
/*       } */
/*       *y = func4(4 * *y) ; */
/*       *y = *y + *x ; */
    }
    *y += cos(*x) ;
    *y = func5(5 * *y) ;
  }
}
