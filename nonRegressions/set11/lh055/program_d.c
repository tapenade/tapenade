/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_llhTests) - 13 Oct 2022 13:10
*/
/*
  Differentiation of foo in forward (tangent) mode:
   variations   of useful results: *y
   with respect to varying inputs: *x *y
   RW status of diff variables: x:(loc) *x:in y:(loc) *y:in-out
   Plus diff mem management of: x:in y:in
*/
void foo_d(double *x, double *xd, double *y, double *yd) {
    *yd = 2.0*(*xd);
    *y = 2.0*(*x);
}
