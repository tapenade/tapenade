subroutine foo(x)
  real, dimension(:), allocatable :: x
  x(0) = bar(x)
end subroutine foo


program main
  real, dimension(:), allocatable :: x
  x(0) = bar(x)
  call foo(x)
end program main
