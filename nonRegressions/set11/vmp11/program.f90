MODULE test
  IMPLICIT NONE

CONTAINS

  SUBROUTINE test1()

    REAL :: tolerance
    REAL :: hmin           
    REAL :: ier, fatal

    body: DO

      IF ((tolerance > hmin) .OR. (tolerance < 0)) THEN
        ier = fatal
        EXIT body
      ELSE
        call foo()
      END IF

      EXIT body
    END DO body
    CALL bar(ier)

  CONTAINS

    SUBROUTINE test2
      call foo()
    END SUBROUTINE test2

    SUBROUTINE test3
      call bar()
    END SUBROUTINE test3

  END SUBROUTINE test1

END module test


