subroutine foo(x, column, y, z, t, u)
 real, contiguous :: x(:,:)
 real, pointer, contiguous :: column(:)
 real(kind=8), intent(in), contiguous, dimension(0:, 0:, 0:, 1:) :: y
 real, dimension(:) :: z, t, u
 contiguous z
 contiguous :: t, u
end subroutine foo

subroutine bar(x, column, y, z, t, u)
 real, volatile :: x(:,:)
 real, pointer, volatile :: column(:)
 real(kind=8), volatile, dimension(0:, 0:, 0:, 1:) :: y
 real, dimension(:) :: z, t, u
 volatile z
 volatile :: t, u
end subroutine bar
