typedef struct
{
    int nrows;
    int ncols;
    double* data;                   // matrix is stored in data COLUMN MAJOR!!!
} Matrix;

void mat_mult(const Matrix* lhs, const Matrix* rhs, Matrix* out)
{
    int i, j, k;
    resize(out, lhs->nrows, rhs->ncols);
    for (i = 0; i < lhs->nrows; i++)
    {
        for (k = 0; k < rhs->ncols; k++)
        {
            out->data[i + k * out->nrows] = lhs->data[i + 0 * lhs->nrows] * rhs->data[0 + k * rhs->nrows];
            for (j = 1; j < lhs->ncols; j++)
            {
                out->data[i + k * out->nrows] += lhs->data[i + j * lhs->nrows] * rhs->data[j + k * rhs->nrows];
            }
        }
    }
}
