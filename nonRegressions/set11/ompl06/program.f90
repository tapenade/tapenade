
! Test pyscopad
! With pyscopad, a $AD MULTITHREAD_ADJ GLOBAL(u)
!  should not be necessary because pyscopad can discover that
!  ub can be SHARED and increments on ub need not be ATOMIC.
subroutine foo(u, r, n, s)
  implicit none
  integer i, n
  real r(n), u(n), s
  !$omp parallel do shared(u), shared(r)
  do i=2,n,2
    r(i-1) = 2*u(i)
    r(i) = 3*u(i-1)
  end do
  s = SUM(r)*SUM(u)
end subroutine foo

program main
  integer, parameter :: n = 10
  real, dimension(n) :: u, r
  real :: s
  u = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  r = (/3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9/)
  call foo(u, r, n, s)
  print *, 'result: ', s
end program main
