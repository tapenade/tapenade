! From ompl06, modified to test subroutine calls
program main
  integer, parameter :: n = 10
  real, dimension(n) :: u, r
  real :: s
  u = (/1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9/)
  r = (/3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9/)
  call foo(u, r, n, s)
  print *, 'result: ', s
end program main

subroutine sub1(a1,a2,i,n)
  integer i,n
  real a1(n), a2(n)
  a1(i) = a2(i-1) * a2(i+1)
end subroutine sub1

subroutine sub2(v1,v2)
  real v1,v2
  v1 = v1*v2
end subroutine sub2

subroutine foo(u, r, n, s)
  implicit none
  integer i, n
  real r(n), u(n), s
  !$omp parallel do shared(u), shared(r)
  do i=2,n,2
    r(i-1) = 2*u(i)
    call sub1(r,u,i,n)
    r(i) = 3*u(i-1)
    call sub2(r(i),u(i+1))
  end do
  s = SUM(r)*SUM(u)
end subroutine foo
