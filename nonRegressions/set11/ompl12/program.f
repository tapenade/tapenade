C To test FormAD with no primal knowledge
      subroutine foo(x)
      real x
      real t(100)
      real sum

      t(:) = 2*x
      sum = 0.0
C$OMP PARALLEL  REDUCTION(+:sum)
C$OMP DO SCHEDULE(static)
      do i = 1,20
         if (i.gt.5) then
         sum = sum + t(2*i) * t(2*i+1)
         endif
      enddo
      t(:) = 0.
C$OMP DO SCHEDULE(static)
      do i = 1,100
         t(i) = 4.0
      enddo
C$OMP DO SCHEDULE(static)
      do i = 1,20
         t(i) = i*x
         sum = sum + t(i)
      enddo
C$OMP END PARALLEL
      x = x + sum
      end
