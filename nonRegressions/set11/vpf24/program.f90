subroutine da_uv_to_divergence(xb, u, v, div)

   implicit none

   type (xb_type), intent(in)           :: xb         
   real, intent(in)   :: u(ims:ime,jms:jme,kms:kme)   
   real, intent(in)   :: v(ims:ime,jms:jme,kms:kme)   
   real, intent(inout):: div(ims:ime,jms:jme,kms:kme) 

   integer            :: i, j, k                      
   integer            :: is, ie                       
   integer            :: js, je                       
   real               :: one_third                    

   real               :: coeff, inv_2ds
   real               :: um(ims:ime,jms:jme)          
   real               :: vm(ims:ime,jms:jme)          

   one_third = 1.0 / 3.0
   div = 0.0

   is = its; ie = ite; js = jts; je = jte
   if (.not. global .and. its == ids) is = ids+1  
   if (.not. global .and. ite == ide) ie = ide-1
   if (jts == jds) js = jds+1; if (jte == jde) je = jde-1
  
   if (.not.global) inv_2ds = 0.5 / xb%ds

   if (global) then
      do k = kts, kte

         do j = js, je
            do i = is, ie
               div(i,j,k) = xb%coefx(i,j) * (u(i+1,j,k) - u(i-1,j,k)) + &
                            xb%coefy(i,j) * (v(i,j+1,k) - v(i,j-1,k))
            end do
         end do
      end do
      call da_set_boundary_3d(div)
   else
      do k = kts, kte

         um(is-1:ie+1,js-1:je+1) = u(is-1:ie+1,js-1:je+1,k) / xb%map_factor(is-1:ie+1,js-1:je+1)
         vm(is-1:ie+1,js-1:je+1) = v(is-1:ie+1,js-1:je+1,k) / xb%map_factor(is-1:ie+1,js-1:je+1)


         do j = js, je
            do i = is, ie
               coeff = xb%map_factor(i,j) * xb%map_factor(i,j) * inv_2ds
               div(i,j,k) = (um(i+1,j) - um(i-1,j) + vm(i,j+1) - vm(i,j-1)) * coeff
            end do
         end do

         if (its == ids) then
            i = its 
            do j = jts, jte
               div(i,j,k) = one_third * (4.0 * div(i+1,j,k) - div(i+2,j,k))
            end do
         end if

         if (ite == ide) then
            i = ite
            do j = jts, jte
               div(i,j,k) = one_third * (4.0 * div(i-1,j,k) - div(i-2,j,k))
            end do
         end if

         if (jts == jds) then
            j = jts
            do i = its, ite
               div(i,j,k) = one_third * (4.0 * div(i,j+1,k) - div(i,j+2,k))
            end do
         end if

         if (jte == jde) then
            j = jte
            do i = its, ite
               div(i,j,k) = one_third * (4.0 * div(i,j-1,k) - div(i,j-2,k))
            end do
         end if
      end do
   end if

end subroutine da_uv_to_divergence


