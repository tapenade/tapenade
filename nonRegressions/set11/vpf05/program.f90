SUBROUTINE FOO(co, maxvm)
  IMPLICIT NONE
  REAL :: co(4)
  REAL :: maxvm
  PRINT*, 'foo :: co ', co
  maxvm = co(1)*co(2) + co(3)*co(4)
  co(4) = maxvm
  PRINT*, 'foo -> co maxvm ', co, maxvm
END SUBROUTINE FOO
