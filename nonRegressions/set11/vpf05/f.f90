subroutine foo(co, maxvm)
   integer co(4)
   integer maxvm
   print*, 'foo :: co ', co
   maxvm = (co(1) * co(2)) + (co(3) * co(4))
   co(4) = maxvm;
   print*, 'foo -> co maxvm ', co, maxvm
end

program main 
end program main
