/* Illustrates a bug in TBR where the 2nd phase of TBR (top-down)
 * implies a new TBR request that should be propagated bottom-up.
 * This requires a new TBR algorithm that has, instead of 2 successive
 * phases (bottom-up then top-down), a single phase that mixes
 * top-down and bottom-up propagation on the call graph */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct{
        int M;
        double* values;
} p_Vector;
typedef p_Vector* Vector;

/* $AD NOCHECKPOINT */
Vector VecNew(int size){
  Vector vector = (Vector)malloc(sizeof(p_Vector));
  vector->M = size;
  vector->values = (double*)malloc(size*sizeof(double));
  return vector;
}

/* $AD NOCHECKPOINT */
void foo0(Vector vect, double *data) {
  int i;
  for (i=0 ; i<5 ; ++i) {
    // Because vect->values is TBR
    // (like any Vector->values because of blurring of malloc "multi" zones)
    // this will push/pop vect->values[i],
    // therefore making "vect" tbr, but only in the top-down TBR phase.
    vect->values[i] += data[i] ;
  }
}

/* $AD NOCHECKPOINT */
void foo1(Vector vect,  double *data) {
  int j ;
  for (j=0 ; j<2 ; ++j) {
    foo0(vect, data) ;
  }
}

/* $AD NOCHECKPOINT */
void foo2(double x, double *y) {
  double *data;
  Vector vect = NULL ;
  vect = VecNew(9) ;
  data = (double*)malloc(5*sizeof(double)) ;
  int i;
  for (i=0 ; i<5 ; ++i) {
    data[i] = x ;
  }
  foo1(vect, data) ;
  if (x>2.5) {
    foo1(vect, data) ;
  }
  *y = vect->values[3] ;
  free(data) ;
  // With a 2-phases TBR analysis, foo2 doesn't know that "vect" will be TBR
  // because this is decided in the 2nd, top-down phase
  // and foo0 was not analyzed yet.
}

void root(double x, double *y) {
  // Make sure malloc(vector->values) becomes TBR
  // by side-effect, because its zone is "multi" => blurred.
  Vector othervect = VecNew(2) ;
  othervect->values[0] = x ;
  othervect->values[0] = othervect->values[0] * othervect->values[0];
  *y += othervect->values[0] * othervect->values[0] ;
  foo2(x,y) ;
  *y = *y * x ;
}

int main() {
  double x = 1.5 ;
  double y = 0.0  ;
  root(x, &y) ;
  printf("result %f\n", y) ;
}
