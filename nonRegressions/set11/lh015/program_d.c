/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
*/
#include <adContext.h>
/* Illustrates a bug in TBR where the 2nd phase of TBR (top-down)
 * implies a new TBR request that should be propagated bottom-up.
 * This requires a new TBR algorithm that has, instead of 2 successive
 * phases (bottom-up then top-down), a single phase that mixes
 * top-down and bottom-up propagation on the call graph */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
typedef struct {
    int M;
    double *values;
} p_Vector;
typedef struct {
    double *values;
} p_Vector_diff;
typedef p_Vector * Vector;
typedef p_Vector_diff * Vector_diff;

/*
  Differentiation of VecNew in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
                VecNew:in-out
*/
p_Vector_diff * VecNew_d(int size, Vector *VecNew) {
    Vector vector;
    p_Vector_diff *vectord;
    int ii1;
    vectord = (p_Vector_diff *)malloc(sizeof(p_Vector_diff));
    vectord->values = NULL;
    vector = (p_Vector *)malloc(sizeof(p_Vector));
    vector->M = size;
    vectord->values = (double *)malloc(size*sizeof(double));
    for (ii1 = 0; ii1 < size; ++ii1)
        vectord->values[ii1] = 0.0;
    vector->values = (double *)malloc(size*sizeof(double));
    *VecNew = vector;
    return vectord;
}

/*
  Differentiation of foo0 in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   variations   of useful results: [alloc*(*vector.values) in VecNew]
                *([alloc*vector in VecNew].values) *(*vect.values)
   with respect to varying inputs: [alloc*data in foo2] [alloc*(*vector.values) in VecNew]
                *([alloc*vector in VecNew].values) *data *(*vect.values)
   Plus diff mem management of: [alloc*vector in VecNew].values:in
                data:in vect:in *vect.values:in
*/
void foo0_d(p_Vector *vect, p_Vector_diff *vectd, double *data, double *datad)
{
    int i;
    for (i = 0; i < 5; ++i) {
        // Because vect->values is TBR
        // (like any Vector->values because of blurring of malloc "multi" zones)
        // this will push/pop vect->values[i],
        // therefore making "vect" tbr, but only in the top-down TBR phase.
        vectd->values[i] = vectd->values[i] + datad[i];
        vect->values[i] += data[i];
    }
}

/*
  Differentiation of foo1 in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   variations   of useful results: [alloc*(*vector.values) in VecNew]
                *([alloc*vector in VecNew].values) *(*vect.values)
   with respect to varying inputs: [alloc*data in foo2] [alloc*(*vector.values) in VecNew]
                *([alloc*vector in VecNew].values) *data *(*vect.values)
   Plus diff mem management of: [alloc*vector in VecNew].values:in
                data:in vect:in *vect.values:in
*/
void foo1_d(p_Vector *vect, p_Vector_diff *vectd, double *data, double *datad)
{
    int j;
    for (j = 0; j < 2; ++j)
        foo0_d(vect, vectd, data, datad);
}

/*
  Differentiation of foo2 in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   variations   of useful results: *y
   with respect to varying inputs: [alloc*(*vector.values) in VecNew]
                *([alloc*vector in VecNew].values) x *y
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
                y:in
*/
void foo2_d(double x, double xd, double *y, double *yd) {
    double *data;
    double *datad;
    Vector vect = (void *)0;
    p_Vector_diff *vectd = (void *)0;
    int ii1;
    vectd = VecNew_d(9, &vect);
    datad = (double *)malloc(5*sizeof(double));
    for (ii1 = 0; ii1 < 5; ++ii1)
        datad[ii1] = 0.0;
    data = (double *)malloc(5*sizeof(double));
    int i;
    for (i = 0; i < 5; ++i) {
        datad[i] = xd;
        data[i] = x;
    }
    foo1_d(vect, vectd, data, datad);
    if (x > 2.5)
        foo1_d(vect, vectd, data, datad);
    *yd = vectd->values[3];
    *y = vect->values[3];
    free(datad);
    free(data);
    // With a 2-phases TBR analysis, foo2 doesn't know that "vect" will be TBR
    // because this is decided in the 2nd, top-down phase
    // and foo0 was not analyzed yet.
}

/*
  Differentiation of root in forward (tangent) mode (with options no!refineADMM no!SpareInits fixinterface context):
   variations   of useful results: *y
   with respect to varying inputs: x
   RW status of diff variables: x:in y:(loc) *y:out
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
                y:in
*/
void root_d(double x, double xd, double *y, double *yd) {
    // Make sure malloc(vector->values) becomes TBR
    // by side-effect, because its zone is "multi" => blurred.
    Vector othervect;
    p_Vector_diff *othervectd;
    *yd = 0.0;
    othervectd = VecNew_d(2, &othervect);
    othervectd->values[0] = xd;
    othervect->values[0] = x;
    othervectd->values[0] = 2*othervect->values[0]*othervectd->values[0];
    othervect->values[0] = othervect->values[0]*othervect->values[0];
    *yd = 2*othervect->values[0]*othervectd->values[0];
    *y += othervect->values[0]*othervect->values[0];
    foo2_d(x, xd, y, yd);
    *yd = x*(*yd) + (*y)*xd;
    *y = (*y)*x;
}

/*
  Differentiation of main as a context to call tangent code (with options no!refineADMM no!SpareInits fixinterface context):
   Plus diff mem management of: [alloc*vector in VecNew].values:in-out
*/
int main() {
    double x = 1.5;
    double xd;
    double y = 0.0;
    double yd = 0.0;
    adContextTgt_init(1.e-8, 0.87);
    adContextTgt_initReal8("x", &x, &xd);
    root_d(x, xd, &y, &yd);
    adContextTgt_startConclude();
    adContextTgt_concludeReal8("y", y, yd);
    adContextTgt_conclude();
    printf("result %f\n", y);
}
