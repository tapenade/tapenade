/** Testing C primitives for complex type */
#include <stdio.h>
#include <math.h>
#include <complex.h>

/* This is the C equivalent of (Fortran) set11/lh054 */
void function(double complex *X, double complex *Y)
{
  double complex Z ;
  double u,v ;
  *X = ccos(*X) ;
  *Y = *X*conj(*X) ;
  u = creal(*Y) ;
  v = cimag(*X) ;
  Z = catan(u+I*v) ;
  *X = *X + cpow(*X,Z) ;
  v = cabs(*X) ;
  *Y = 3**Y + 6**X + Z*v ;
}

int main() {
  double complex X, Y ;
  X = 2.5 + I*3.2 ;
  Y = 6.1 - I*1.5 ;
  function(&X,&Y) ;
  printf("Res: %f + i*%f\n", creal(Y), cimag(Y)) ;
  return 0 ;
}
