! Bug found by Charlotte Kotas: wrong precision in the call
! to CMPLX that is placed by adjoint differentiation
! around 2*AIMAG(z(i)). Because CMPLX(A,B) always returns
! 4 bytes real and 4 bytes imaginary, if not told otherwise.
module illustration
use iso_c_binding
implicit none

contains

subroutine sum_magnitude(a,Z,n)
implicit none
integer        :: n
real(c_double) :: a
complex(c_double_complex) :: Z(n)
integer i

a = 0_c_double
do i = 1, n
  a = a + real(z(i))**2 + aimag(z(i))**2
  !a = a + real(conjg(z(i))*z(i))

end do

end subroutine

end module


program test
use illustration
implicit none
real(c_double)  :: otpt
complex(c_double_complex), allocatable :: inpt(:)
integer :: i, n, ns
real(c_double)    :: r1, r2
integer, allocatable :: seed(:)
! repeatable "random" input
call random_seed(size=ns)
allocate(seed(ns))
!call random_seed(get=seed)
!write(*,*) "ns   =",ns
!write(*,*) "seed=",seed
seed=123456789
call random_seed(put=seed)
deallocate(seed)


n = 12
allocate(inpt(n))
do i = 1,n
  call random_number( r1 )
  call random_number( r2 )
  r1 = 2*r1-1
  r2 = 2*r2-1
  inpt(i) = cmplx(r1,r2,c_double_complex)
enddo

call sum_magnitude(otpt,inpt,n)

write(*,*) "otpt=",otpt


stop 
end program test
