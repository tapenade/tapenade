//Illustration templates + typedef (Frederic Cazals)
#include <iostream>

template <class GeometryKernel>
class T_my_class{

public:
  typedef GeometryKernel Geometry_kernel;// renaming
  
  // grab the types defined within the kernel
  typedef typename Geometry_kernel::NT NumType; // the number type
  
  NumType square_number(NumType x) {return x*x;}

};




// a kernel with doubles as number types
class GK_with_double{
public:
  typedef double NT;

};

// a kernel with doubles as number types
class GK_with_float{
public:
  typedef float NT;

};

// a kernel with doubles as number types
class GK_with_int{
public:
  typedef int NT;
  
};

int main(){

  typedef T_my_class<GK_with_double> K_with_double;
  typedef T_my_class<GK_with_float> K_with_float;
  typedef T_my_class<GK_with_int> K_with_int;

  float xxf = 5. ;
  int xxi = 2 ;

  K_with_float objf ;
  K_with_int obji ;

  xxf = objf.square_number(xxf) ;
  xxi = obji.square_number(xxi) ;

  std::cout <<"xxf:"<<xxf<<" xxi:"<<xxi<<"\n" ;
  
}
