!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_assocaddress3) - 27 Jan 2020 14:20
!
!  Differentiation of test as a context to call tangent code (with options context):
PROGRAM TEST_D
  USE CONSTANT
  IMPLICIT NONE
  REAL(wp) :: a, b
  REAL(wp) :: ad, bd
  REAL(wp) :: z
  REAL(wp) :: zd
  DIMENSION z(4)
  DIMENSION zd(4)
  z(1) = 4
  z(2) = 5
  z(3) = 66
  z(4) = 1
  a = 4
  CALL ADCONTEXTTGT_INIT(1.e-8_8, 0.87_8)
  CALL ADCONTEXTTGT_INITREAL8ARRAY('z'//CHAR(0), z, zd, 4)
  CALL ADCONTEXTTGT_INITREAL8('a'//CHAR(0), a, ad)
  CALL ADCONTEXTTGT_INITREAL8('b'//CHAR(0), b, bd)
  CALL HEAD_D(z, zd, a, ad, b, bd)
  CALL ADCONTEXTTGT_STARTCONCLUDE()
  CALL ADCONTEXTTGT_CONCLUDEREAL8ARRAY('z'//CHAR(0), z, zd, 4)
  CALL ADCONTEXTTGT_CONCLUDEREAL8('a'//CHAR(0), a, ad)
  CALL ADCONTEXTTGT_CONCLUDEREAL8('b'//CHAR(0), b, bd)
  CALL ADCONTEXTTGT_CONCLUDE()
  PRINT*, a, ' -> ', b
END PROGRAM TEST_D

!  Differentiation of head in forward (tangent) mode (with options context):
!   variations   of useful results: y a
!   with respect to varying inputs: x
!   RW status of diff variables: x:in y:out a:zero
SUBROUTINE HEAD_D(a, ad, x, xd, y, yd)
  USE CONSTANT
  IMPLICIT NONE
  INTRINSIC SQRT
  REAL(wp) :: x, y
  REAL(wp) :: xd, yd
  REAL(wp), DIMENSION(4) :: a
  REAL(wp), DIMENSION(4) :: ad
  INTRINSIC MINLOC
  REAL(wp) :: temp
  a = MINLOC(a, dim=1)
  temp = SQRT(x)
  IF (x .EQ. 0.0) THEN
    yd = 0.0_8
  ELSE
    yd = xd/(2.0*temp)
  END IF
  y = temp
  ad = 0.0_8
END SUBROUTINE HEAD_D

