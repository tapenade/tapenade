module constant
  integer, parameter :: wp = 8
end module constant

SUBROUTINE HEAD(a, x, y)
  use constant
  IMPLICIT NONE
  intrinsic :: sqrt
  real(wp) :: x, y
  real(wp), dimension(4) :: a
  a = minloc(a, dim=1)
  y = sqrt(x)
END SUBROUTINE


program test
  use constant
  implicit none
  real(wp) :: a,b
  real(wp) :: z
  dimension z(4)
  z(1) = 4
  z(2) = 5
  z(3) = 66
  z(4) = 1
  a = 4
  call head(z, a, b)
  print*, a, ' -> ', b
end program
