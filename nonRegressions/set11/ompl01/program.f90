MODULE MM

CONTAINS

  SUBROUTINE cpu_stencil(c0,c1,A0,Anext,nx,ny,nz)
    REAL*8 :: c0,c1
    REAL *8, DIMENSION(:) :: A0, Anext
    INTEGER :: nx,ny,nz
    INTEGER i,j,k

    DO i=1,nx-2
       DO j=1,ny-2
          DO k=1,nz-2
             Anext(index3d(nx,ny,i,j,k)) =     &
&             (A0(index3d(nx,ny,i,j,k+1))      &
&             + A0(index3d(nx,ny,i,j,k-1))     &
&             + A0(index3d(nx,ny,i,j+1,k))     &
&             + A0(index3d(nx,ny,i,j-1,k))     &
&             + A0(index3d(nx,ny,i+1,j,k))     &
&             + A0(index3d(nx,ny,i-1,j,k)))*c1 &
&             -A0(index3d(nx,ny,i,j,k))*c0
          END DO
       END DO
    END DO
  END SUBROUTINE cpu_stencil
  
  INTEGER FUNCTION index3d(nx,ny,i,j,k)
    INTEGER :: nx,ny,i,j,k
    index3d = (i*nx + j)*ny + k
  END FUNCTION index3d

END MODULE MM

PROGRAM MAIN
  USE MM
  REAL *8, DIMENSION(125) :: A0, A1
  REAL*8 :: c0, c1
  INTEGER i,j,k
  DO i = 1,125
     A0(i) = 1.0 + 100.0/i
     A1(i) = 8.0 + 100.0/i
  ENDDO
  c0 = 1.5
  c1 = 0.6
  CALL CPU_STENCIL(c0,c1,A0,A1,5,5,5)
  print *,'res:',SUM(A1)
END PROGRAM MAIN
