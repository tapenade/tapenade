C from Eugene Kazantsev Wed, 16 Nov 2011

      subroutine traj(u,v)
      parameter(nx=182,ny=149,nz=31) 
      common/buffers/buf1(nx,ny,nz),buf2(nx,ny,nz)
      real*8 u(nx,ny,nz),v(nx,ny,nz)
      
      do n=1,100
         call timestep(u,v)
      end do
      return
      end      

      subroutine  timestep(u,v)
      parameter(nx=182,ny=149,nz=31) 
      common/buffers/buf1(nx,ny,nz),buf2(nx,ny,nz)
      real*8 u(nx,ny,nz),v(nx,ny,nz)

      dt=0.01d0
      do j=1,ny
      do i=1,nx    
      buf1(i,j,1)=0.0d0	
      end do
      end do 

      do k=1,nz
        do j=1,ny
            i1=20
            im=30 
            do i=i1,im    
               buf1(i,j,1)=buf1(i,j,1)+(u(i,j,k)+dt*v(i,j,k))
            end do
        end do
      end do 
      call diff_x(buf1(1,1,1),buf2(1,1,1))
      do k=1,nz
        do j=1,ny
            i1=20
            im=30 
            do i=i1,im               
               u(i,j,k)=u(i,j,k)+dt*buf2(i,j,1)
            end do         
        end do 
        do i=1,nx
            j1=20 
            jm=30
            do j=j1,jm
               v(i,j,k)=v(i,j,k)+dt*buf2(i,j,1)
            end do
        end do 
      end do
      return
      end      

      subroutine  diff_x(u,v)
      parameter(nx=182,ny=149,nz=31) 
      real*8 u(nx,ny),v(nx,ny)
      v=2.0d0*u
      return
      end


