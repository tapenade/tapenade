!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_openmp2) - 19 Apr 2022 11:34
!
!  Differentiation of conv1dcompact in reverse (adjoint) mode (with options OpenMP):
!   gradient     of useful results: unew uold
!   with respect to varying inputs: unew uold
!   RW status of diff variables: unew:in-out uold:incr
SUBROUTINE CONV1DCOMPACT_B(unew, unewb, uold, uoldb, weightsl, weightsr&
& , weightc, n)
  IMPLICIT NONE
  INTEGER :: n, i, s, ir, offset
  DOUBLE PRECISION, DIMENSION(0:n-1) :: unew, uold
  DOUBLE PRECISION, DIMENSION(0:n-1) :: unewb, uoldb
  DOUBLE PRECISION, DIMENSION(1) :: weightsl, weightsr
  DOUBLE PRECISION :: weightc, inval
  DOUBLE PRECISION :: invalb
  INTEGER :: ad_from
  INTEGER :: chunk_start
  INTEGER :: chunk_end
  DO offset=0,1
!$OMP PARALLEL PRIVATE(inval), PRIVATE(i), PRIVATE(ad_from, chunk_start, &
!$OMP&chunk_end)
    ad_from = 2*1 + offset
    CALL GETSTATICSCHEDULE(ad_from, n - 1 - 1, 1 + 1, chunk_start, &
&                    chunk_end)
    CALL PUSHINTEGER4(ad_from)
!$OMP END PARALLEL
  END DO
  DO offset=1,0,-1
!$OMP PARALLEL PRIVATE(inval), PRIVATE(invalb), PRIVATE(i), PRIVATE(&
!$OMP&ad_from, chunk_end, chunk_start)
    invalb = 0.D0
    CALL POPINTEGER4(ad_from)
    CALL GETSTATICSCHEDULE(ad_from, n - 1 - 1, 1 + 1, chunk_start, &
&                    chunk_end)
    DO i=chunk_end,chunk_start,-(1+1)
      invalb = weightsr(1)*unewb(i-1) + weightc*unewb(i)
      uoldb(i) = uoldb(i) + invalb
      invalb = weightsl(1)*unewb(i)
      uoldb(i-1) = uoldb(i-1) + invalb
    END DO
!$OMP END PARALLEL
  END DO
END SUBROUTINE CONV1DCOMPACT_B

