subroutine conv1dcompact(unew, uold, weightsl, weightsr, weightc, n)
  implicit none
  integer :: n, i, s, ir, offset
  double precision, dimension(0:n-1) :: unew, uold
  double precision, dimension(1) :: weightsl, weightsr
  double precision :: weightc, inval

  do offset=0,1
    !$omp parallel do private(inval) schedule(static)
    do i = 2*1 + offset, n-1-1, 1+1
      print *, i
      inval = uold(i-1)
      unew(i) = unew(i) + 1.0*weightsl(1)*inval
      inval = uold(i)
      unew(i) = unew(i) + 1.0*weightc*inval
      unew(i-1) = unew(i-1) + 1.0*weightsr(1)*inval
    end do
  end do

end subroutine
