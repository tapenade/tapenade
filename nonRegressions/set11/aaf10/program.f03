  SUBROUTINE HEAD(x, y)
  IMPLICIT NONE
  intrinsic :: sqrt
  integer, parameter :: il = 3
  real(8), dimension(3,il) :: x
  real(8), dimension(3,il) :: y
  intent(in) :: x
  intent(out) :: y
  y(1,1) = sqrt(x(1,1))
  END SUBROUTINE

  subroutine top(x, y, nn)
  integer :: nn
  real(8), dimension(3,nn) :: x, y
  call head(x, y)
  end subroutine


program test
  implicit none
  integer, parameter :: il = 3
  real(8),dimension(3,il) :: z, b
  z(1,1) = 4
  z(1,2) = 5
  z(1,3) = 66
  call top(z, b, il)
  print*, z(1,1), ' -> ', b(1,1)
end program
