! Reduced test code exhibiting the same problem as set11/mvo10
subroutine x2model(n, x, x_ub, x_lb, p1, gu_scale,i3)
  implicit none
  integer, intent(in) :: n
  real(kind=8), intent(in) :: x(n)
  real(kind=8) :: x_lb(n), x_ub(n)
  real(kind=8) :: p1
  real(kind=8) :: gu_scale(3)
  integer :: i1,i2,i3
  
  i1 = 1
  i2 = i1
  i3 = i3+3
  p1 = x(i1)
  x_lb(i1) = max(0._8, x(i1)-3)
  x_ub(i1) = x(i1) + 3
  i1 = i2
  i2 = i1 + 3
  gu_scale(:) = x(i1:i2)
  x_lb(i1:i2) = max(0._8, x(i1:i2)-3)
  x_ub(i1:i2) = x(i1:i2) + 3
  call foo(x(i1:i3)*p1, p1)
  x(i1:i2) = sin(x(i1+1:i2+1))
end subroutine x2model


