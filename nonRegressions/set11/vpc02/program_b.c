/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) -  8 Dec 2022 10:39
*/
#include <adStack.h>
#include <adContext.h>
#include <stdio.h>
double W;
double Wb;

/*
  Differentiation of A in reverse (adjoint) mode (with options context):
   gradient     of useful results: A
   with respect to varying inputs: x
*/
void A_b(double x, double *xb, double Ab) {
    x = x + 3;
    // <-- This line is crucial
    *xb = 2*x*Ab;
}

double A_nodiff(double x) {
    x = x + 3;
    // <-- This line is crucial
    return x*x;
}

/*
  Differentiation of B in reverse (adjoint) mode (with options context):
   gradient     of useful results: W x B
   with respect to varying inputs: W x
   RW status of diff variables: W:incr x:incr B:in-killed
*/
void B_b(double x, double *xb, double Bb) {
    double ret;
    double retb;
    double arg1;
    double arg1b;
    arg1 = W - x;
    retb = Bb;
    arg1b = 0.0;
    A_b(arg1, &arg1b, retb);
    Wb = Wb + arg1b;
    *xb = *xb - arg1b;
}

/*
  Differentiation of main as a context to call adjoint code (with options context):
*/
void main() {
    double z = 2;
    double zb;
    double tmp;
    double tmpb;
    W = 2;
    adContextAdj_init(0.87);
    adContextAdj_initReal8("W", &W, &Wb);
    adContextAdj_initReal8("z", &z, &zb);
    tmpb = zb;
    zb = 0.0;
    B_b(z, &zb, tmpb);
    adContextAdj_startConclude();
    adContextAdj_concludeReal8("W", W, Wb);
    adContextAdj_concludeReal8("z", z, zb);
    adContextAdj_conclude();
    printf("B -> %f \n", z);
}
