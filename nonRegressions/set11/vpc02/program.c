#include <stdio.h>

double W;

double A(double x) {
  x = x + 3; // <-- This line is crucial
  return x*x;
}

double B(double x) {
  double ret;
  ret = A(W-x);
  return ret;
}

void main() {
  double z = 2;
  W = 2;
  z = B(z);
  printf("B -> %f \n", z);
}
