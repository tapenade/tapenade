#include <iostream>
using namespace std;

void foo(float x, float *y) {
  *y = x*x ;
}

int main() {
  float x,y ;
  x = 1.5 ;
  foo(x,&y) ;
  cout << "Result y=" << y << "\n" ;
  return 0 ;
}
