/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_llhTests) - 12 Apr 2022 09:48
*/
/*
  Differentiation of fn in forward (tangent) mode:
   variations   of useful results: *out
   with respect to varying inputs: i j
   RW status of diff variables: i:in j:in out:(loc) *out:out
   Plus diff mem management of: out:in

 bug found by Parth Arora: illegal merge of last 2 adjoint increments */
void fn_d(double i, double id, double j, double jd, double *out, double *outd)
{
    double *p = (double *)0;
    double *pd = (double *)0;
    double res = 0;
    double resd;
    pd = &id;
    p = &i;
    resd = *pd;
    res += *p;
    pd = &jd;
    p = &j;
    resd = resd + *pd;
    res += *p;
    *outd = resd + id + jd;
    *out = res + i + j;
}
