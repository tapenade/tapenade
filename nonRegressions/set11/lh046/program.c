/* bug found by Parth Arora: illegal merge of last 2 adjoint increments */
void fn(double i, double j, double* out) {
  double *p = (double*)0;
  double res = 0;
  p = &i;
  res += *p;
  p = &j;
  res += *p;
  *out = res + i + j;
}
