#include <math.h>

/** An example where our adjoint model cannot work,
 * (but we send a message AD22, so it could be worse!)
 * because there are two "irreducible" entries into
 * the same scope in the backward sweep, and they cannot
 * be merged into a single entry. */
void foo(int i, double *w) {
  double v = 1.5 * *w ;
  double x = *w;
  x = sin(x+1) ;
  {
    double v ;
    v = sin(x+2) ;
    if (i>10) {
      v = sin(v+3) ;
      x += 2*v ;
      goto label10 ;
    }
    v = sin(v+4) ;
    x += v ;
  }
  x = sin(x+v+5) ;
  goto label20 ;
 label10:
  x = sin(x+v+6) ;
 label20:
  x = sin(x+7) ;
  *w = x ;
}
