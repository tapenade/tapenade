! other examples of precision loss in temporaries
! found by C.Kotas.

module MWP
  integer, parameter :: c_double_complex=8
end module MWP

subroutine test(z)
  USE MWP
  complex(kind=c_double_complex) z,z1,z2,z3

  z1 = cmplx(real(z),aimag(z),kind=c_double_complex)

  z2 = cmplx(real(z,kind=c_double_complex),aimag(z),kind=c_double_complex)

  z3 = conjg(cmplx(real(z),aimag(z),kind=c_double_complex) )

  z = z1*z2*z3
end subroutine test
