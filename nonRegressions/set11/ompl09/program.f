C To test scopes for FormAD
      subroutine top(x)
      real x
      real t(100)

      x = x+0.5
C$OMP PARALLEL
      x = x+1
      if (x.gt.0) then
         x = x+2
 100     x = x+3
      else
         x = x+4
         if (x.lt.3) then
            x = x+10
            GOTO 100
         endif
         x = x+5
      endif
      x = x+6
C$OMP END PARALLEL
      x = x+7
C$OMP PARALLEL shared(t)
      x = x+8
C$OMP DO
      do i = 1,20
         t(i) = x+i
         if (i.gt.5) then
            x = t(i+1)
         endif
         do j=1,2
            t(i-j) = t(i-j) + 1.0
         enddo
         j = 0
      enddo
      j = 12
C$OMP END PARALLEL
      x = x-8
      end
