void foo(float *x, float *y)
{
	*y =  *x > *y ? *x : *y;
}
