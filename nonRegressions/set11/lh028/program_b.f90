!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 27 Apr 2021 19:31
!
!  Differentiation of cost in reverse (adjoint) mode (with options split(foo)):
!   gradient     of useful results: f
!   with respect to varying inputs: f x
!   RW status of diff variables: f:in-zero x:out
! bug from inversionLab, about dubious
! INTENTs placed on primal and adjoint params
! Same as set11/lh027, but in split adjoint mode.
SUBROUTINE COST_B(n, x, xb, m, f, fb)
  IMPLICIT NONE
! arguments
  INTEGER, INTENT(IN) :: n, m
  REAL(kind=8), INTENT(IN) :: x(n)
  REAL(kind=8) :: xb(n)
  REAL(kind=8) :: f
  REAL(kind=8) :: fb
! local
  INTEGER, PARAMETER :: nt=3
  INTEGER :: i
  REAL(kind=8) :: req(n)
  REAL(kind=8) :: rad(n)
  REAL(kind=8) :: radb(n)
  INTRINSIC REAL
  INTRINSIC SUM
  rad = x
  DO i=1,nt
    CALL FOO_FWD(n, rad, req)
    rad = x + rad*REAL(i, kind=8)*req
  END DO
  radb = 0.0_8
  radb = fb
  xb = 0.0_8
  DO i=nt,1,-1
    xb = xb + radb
    radb = req*REAL(i, kind=8)*radb
    CALL FOO_BWD(n, rad, radb, req)
  END DO
  xb = xb + radb
  fb = 0.0_8
END SUBROUTINE COST_B

!  Differentiation of foo in reverse (adjoint) mode, forward sweep (with options split(foo)):
!   gradient     of useful results: active
!   with respect to varying inputs: active
SUBROUTINE FOO_FWD(n, active, reqout)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: n
  REAL(kind=8), INTENT(INOUT) :: active(n)
  REAL(kind=8) :: reqout(n)
  INTRINSIC REAL
  active = active*REAL(n, kind=8)
  CALL PUSHREAL8ARRAY(reqout, n)
  reqout = 2._8
END SUBROUTINE FOO_FWD

!  Differentiation of foo in reverse (adjoint) mode, backward sweep (with options split(foo)):
!   gradient     of useful results: active
!   with respect to varying inputs: active
SUBROUTINE FOO_BWD(n, active, activeb, reqout)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: n
  REAL(kind=8), INTENT(INOUT) :: active(n)
  REAL(kind=8), INTENT(INOUT) :: activeb(n)
  REAL(kind=8) :: reqout(n)
  INTRINSIC REAL
  CALL POPREAL8ARRAY(reqout, n)
  activeb = REAL(n, kind=8)*activeb
END SUBROUTINE FOO_BWD

