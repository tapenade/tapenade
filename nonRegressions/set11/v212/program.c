#include <stdio.h>

void test0(float *z) {
  *z = *z * 2;
}

void test1(float x, float *z) {
  test0(&x);
  *z = x * x;
}

main() {
  float a = 5.0;
  float b;
  test1(a,&b);
  printf("--> %f \n" , b);
}
