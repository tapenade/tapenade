! A test to reproduce a TreeGen bug (lost arrow carry, label 100...)
! found by Shreyas Gaikwad

MODULE MYMOD

CONTAINS

SUBROUTINE ROOT(a)
real(dp) :: a,b
b = 1.5
CALL OUTPUT2(b)
if (b.gt.10.0) then
   a = SIN(a)
end if
end SUBROUTINE ROOT

SUBROUTINE OUTPUT2(somearg, optarg)

implicit none
real(dp) :: somearg

logical, optional, intent(in) :: optarg

integer(i4b) :: i, j, n
real(dp) :: Q_s, Q_s_flx
real(dp), dimension(0:144,0:84) :: H_cold, H_temp
logical :: flag_compute_flux_vars_only
logical, dimension(0:144,0:84) :: flag_region

integer(i4b), save :: counter = 0
logical,      save :: firstcall_output2 = .true.

if (present(optarg)) then
   flag_compute_flux_vars_only = optarg
else
   flag_compute_flux_vars_only = .false.
end if

if (.not.flag_compute_flux_vars_only) &
   counter = counter + 1

H_cold = 0.0_dp
H_temp = 0.0_dp
do i=1, 84-1
  do j=1, 144-1
    H_temp(j,i) = H_t(j,i)
  end do
end do
H_cold = H - H_temp

if (maxval(mask_region) > 99) then
   call error('coucou')
end if

do n=0, maxval(mask_region)
   if (n==0) then
      call foo(somearg)
   else
      flag_region = .false.
      call foo(somearg)
   end if

! snapshots of flux variables
   Q_s_flx         = Q_s


   if ((n==0).and.(.not.flag_compute_flux_vars_only)) then
      print *, 'salut'
   end if

end do

if (firstcall_output2) firstcall_output2 = .false.

END SUBROUTINE OUTPUT2

END MODULE MYMOD
