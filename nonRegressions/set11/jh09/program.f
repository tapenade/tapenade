c this is a comment
c  over two lines
      program foo
      integer i,j
      real A(20)
C c1
      i = 5
      j = 0
C$om this is not an OMP pragma!
      j = j+1
c$omp parallel do private(i) private(j),
c$omp+ shared(a) shared(b), reduction(+:s)
c c2
      do i = 1,10
         A(i)=0.0
      enddo
c c3
      j = 41
c c4
c$omp parallel
c c5
      j = 42
c c6
c$omp do shared(a,b), reduction(+:s)
c c7
      do i = 1,11
c c8
         A(i)=1.0
c c9
      enddo
c c10
c$omp end do
c c11
      j = 43
c c12
c$omp end parallel
c c13
      j = 44
c c14
c$omp parallel do private(i,j), reduction(+:s)
c and this is a comment
      do i = 1,12
         A(i)=2.0
      enddo

      end
