! bug report Thu, 26 Mar 2020

module mo
  implicit none

  !--Note::when disabling the global 'private' statement below,
  !        the code is preprocessed by Tapenade without any complaints
  private
  type some_type
     real :: f
  end type some_type
  public some_type

  !-- overloaded operator
  public :: operator(-)
  private :: some_type_difference
  !-- interface blocks
  interface operator(-)
     module procedure some_type_difference
  end interface operator(-)
contains
  function some_type_difference(x, y) result(diff)
    implicit none
    type(some_type) :: diff
    type(some_type), intent(in) :: x,y

    diff%f = x%f - y%f
  end function some_type_difference
end module mo

subroutine foo(n, x, y)
  use mo
  implicit none
  !-- arguments
  integer, intent(in) :: n
  real, intent(in) :: x(n)
  real, intent(out) :: y
  !-- local
  type(some_type) :: u, v, w


  u%f = x(1)
  v%f = x(2)

  w = u - v
  y = w%f

end subroutine foo

program main
  implicit none
  integer, parameter :: n = 2
  real :: x(n), y
  integer :: i

  do i=1,n
     x(i) = real(i*i)
  end do

  call foo(n,x,y)
  print*, 'INFO::y=',y
end program main
