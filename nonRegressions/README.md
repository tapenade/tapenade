Non-Regression tests of Tapenade
================================

Each sub-directory of each `setNN` contains one non-regression test.

To run one test in `setNN/subdir`:

`./runAD setNN/subdir`

See script runAD for more detail.
Testing uses by default the `tapenade` command in the search path, unless variable `DIFFCOMMAND` is defined. For instance to use latest tapenade docker image:

`DIFFCOMMAND=tapenadocker`

Some tests look for include files in `../ADFirstAidKit/ampi`, `../ADFirstAidKit/mpich/include`, `../ADFirstAidKit/openmpi/include`, so place symbolic links in `../ADFirstAidKit` accordingly.




 
