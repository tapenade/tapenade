#!/usr/bin/env bash

# Usage: execAD [-debug] <directory-list-with-possible*>
# For each non-regression test directory contained in <directory-list-with-possible*>,
# executes and tests correctness of the derivative code produced in RESULTS/.
#
# For each non-regression test, assuming that it
#  1) operates on a SOURCE code that compiles and executes,
#  2) AND applies AD with the "-context" command-line option,
# compiles and executes the differentiated code produced in RESULT/ and (when applicable) checks
# that the "-p" code executes identical to the source, the "-d" code executes identical to the
# source (for the primal values), the "-d" code returns derivatives consistent with Divided
# Differences, and the "-b" code returns derivatives consistent with the "-d" (Dot-Product test).


# The language extensions recognized in source files (after the "."):
LANGEXTENSIONSPATTERN="(f|f90|f95|f03|jl|c|cu|c\+\+)"
# For each testable AD mode, the extension for generated files:
MODEEXTENSIONSPATTERN="(p|d|dv|aad|b|bv|aab|db|diff)"

# The location of the ADFirstAidKit
ADFIRSTAIDKIT=' '
if [ -n "$TAPENADE_HOME" ]
then
    ADFIRSTAIDKIT=$TAPENADE_HOME/ADFirstAidKit
else
    ADFIRSTAIDKIT="$(dirname -- "$(readlink -f -- "$0")")"/../ADFirstAidKit
fi

# Detecting option -debug
DEBUG=0
OTHERARGS=""
for ARGUMENT in $*
  do
  if [[ $ARGUMENT == "-debug" ]] || [[ $ARGUMENT == "-g" ]]
      then
      DEBUG=1
  else
      OTHERARGS=$OTHERARGS" "$ARGUMENT
  fi
done

# Find all tests to execute:
UNAME=`uname`
case $UNAME in
    Linux )
    TESTS=`find $OTHERARGS -name "*.msg" -printf "%h\n" | grep -v RESULTS | sort -u`
    ;;
    * )
    TESTS=$*
    echo " runAD find: -printf unknown option"
    ;;
esac
if (($DEBUG==1)) ; then echo 'EXECUTE TESTS:('$TESTS')' ; fi

for TESTNAME in $TESTS
  do

  TESTNAME=${TESTNAME#./}
  echo '--------------------'
  echo 'Executing '$TESTNAME':'
  COMPILE='gcc -I'$TESTNAME' -lm'
  DIFFSIZESFILE=''

# Collect SOURCE files (from directory $TESTNAME):
#  these are all the *.LANGEXTENSION except the *_MODEEXTENSION.LANGEXTENSION, includes, or other specials.
  SOURCEFILES=""
  SOMESOURCEFILES=`ls -r $TESTNAME | grep -E "\.$LANGEXTENSIONSPATTERN$" | grep -E -v "_$MODEEXTENSIONSPATTERN\.$LANGEXTENSIONSPATTERN$" | grep -E -v "^incl" | grep -E -v "^GlobalDeclarations\." | grep -E -v "^mainForTest\.$LANGEXTENSIONSPATTERN$"`
  for SOURCEFILE in $SOMESOURCEFILES
    do
    if [[ $SOURCEFILE == 'DIFFSIZES.f90' ]]
        then
        COMPILE='gfortran -I'$TESTNAME' -JRESULTS/'$TESTNAME
        DIFFSIZESFILE=$TESTNAME/$SOURCEFILE" "$DIFFSIZESFILE
    elif [[ $SOURCEFILE == *.f || $SOURCEFILE == *.f90 || $SOURCEFILE == *.f95 || $SOURCEFILE == *.f03 ]]
        then
        COMPILE='gfortran -I'$TESTNAME' -JRESULTS/'$TESTNAME
        SOURCEFILES=$TESTNAME/$SOURCEFILE" "$SOURCEFILES
    else
        SOURCEFILES=$TESTNAME/$SOURCEFILE" "$SOURCEFILES
    fi
  done
# Compile and execute SOURCE code: (this MUST work)
  if (($DEBUG==1)) ; then echo $COMPILE' '$SOURCEFILES' -o RESULTS/'$TESTNAME'/orig.exe' ; fi
  $COMPILE $SOURCEFILES -o RESULTS/$TESTNAME/orig.exe
  if [ -f RESULTS/$TESTNAME/orig.exe ]
      then
      if (($DEBUG==1)) ; then echo 'RESULTS/'$TESTNAME'/orig.exe > RESULTS/'$TESTNAME'/orig.trace' ; fi
      RESULTS/$TESTNAME/orig.exe > RESULTS/$TESTNAME/orig.trace
  fi


# Collect source files for the PRIMAL executable (from directory RESULTS/$TESTNAME):
  EXECPRIMAL=0
  PRIMALSOURCEFILES=""
  SOMESOURCEFILES=`ls -r RESULTS/$TESTNAME | grep -E "_p\.$LANGEXTENSIONSPATTERN$" | grep -E -v "^incl"`
  for SOURCEFILE in $SOMESOURCEFILES
    do
    EXECPRIMAL=1
    PRIMALSOURCEFILES=RESULTS/$TESTNAME/$SOURCEFILE" "$PRIMALSOURCEFILES
  done
# Compile and execute PRIMAL code (if present), then test identity with SOURCE code output:
  if (($EXECPRIMAL==1))
      then
      if (($DEBUG==1)) ; then echo $COMPILE' '$PRIMALSOURCEFILES' -o RESULTS/'$TESTNAME'/primal.exe' ; fi
      $COMPILE $PRIMALSOURCEFILES -o RESULTS/$TESTNAME/primal.exe
      rm -rf RESULTS/$TESTNAME/*.mod
      if [ -f RESULTS/$TESTNAME/primal.exe ]
          then
          if (($DEBUG==1)) ; then echo 'RESULTS/'$TESTNAME'/primal.exe > RESULTS/'$TESTNAME'/primal.trace' ; fi
          RESULTS/$TESTNAME/primal.exe > RESULTS/$TESTNAME/primal.trace
          if diff RESULTS/$TESTNAME/orig.trace RESULTS/$TESTNAME/primal.trace > /dev/null 2>&1
              then
              DUMMYOP=1
          else
              echo 'PRIMAL CODE BEHAVES NOT LIKE SOURCE CODE:'
              diff RESULTS/$TESTNAME/orig.trace RESULTS/$TESTNAME/primal.trace
          fi
      fi
  fi


# Collect source files for the TANGENT executable (from directory RESULTS/$TESTNAME):
  EXECTGT=0
  TGTSOURCEFILES=""
  SOMESOURCEFILES=`ls -r RESULTS/$TESTNAME | grep -E "_d\.$LANGEXTENSIONSPATTERN$" | grep -E -v "^incl"`
  for SOURCEFILE in $SOMESOURCEFILES
    do
    EXECTGT=1
    TGTSOURCEFILES=RESULTS/$TESTNAME/$SOURCEFILE" "$TGTSOURCEFILES
  done
# Compile and execute TANGENT code (if present), then test identity with SOURCE code output then Divided Differences test:
  if (($EXECTGT==1))
      then
      if (($DEBUG==1)) ; then echo $COMPILE' -I '$ADFIRSTAIDKIT' '$DIFFSIZESFILE' '$TGTSOURCEFILES' '$ADFIRSTAIDKIT'/json.c '$ADFIRSTAIDKIT'/adContext.c -o RESULTS/'$TESTNAME'/tangent.exe' ; fi
      $COMPILE -I $ADFIRSTAIDKIT $DIFFSIZESFILE $TGTSOURCEFILES $ADFIRSTAIDKIT/json.c $ADFIRSTAIDKIT/adContext.c -o RESULTS/$TESTNAME/tangent.exe
      rm -rf RESULTS/$TESTNAME/*.mod
      if [ -f RESULTS/$TESTNAME/tangent.exe ]
          then
          if (($DEBUG==1)) ; then echo 'DBAD_PHASE=1 RESULTS/'$TESTNAME'/tangent.exe > RESULTS/'$TESTNAME'/tangent1.trace' ; fi
          DBAD_PHASE=1 RESULTS/$TESTNAME/tangent.exe > RESULTS/$TESTNAME/tangent1.trace
          if (($DEBUG==1)) ; then echo 'DBAD_PHASE=2 RESULTS/'$TESTNAME'/tangent.exe > RESULTS/'$TESTNAME'/tangent2.trace' ; fi
          DBAD_PHASE=2 RESULTS/$TESTNAME/tangent.exe > RESULTS/$TESTNAME/tangent2.trace
          # tangent0.trace is the trace of DBAD_PHASE==2, minus the (4) special lines added by the context mode:
          grep -E -v "^Tangent code,  seed=" RESULTS/$TESTNAME/tangent2.trace | grep -E -v "^=============================================$" | grep -E -v "] Condensed result : " | grep -E -v "] Condensed tangent: " > RESULTS/$TESTNAME/tangent0.trace
          if diff RESULTS/$TESTNAME/orig.trace RESULTS/$TESTNAME/tangent0.trace > /dev/null 2>&1
              then
              DUMMYOP=1
          else
              echo 'TANGENT CODE BEHAVES NOT LIKE SOURCE CODE:'
              diff RESULTS/$TESTNAME/orig.trace RESULTS/$TESTNAME/tangent0.trace
          fi
          EPSILON=`grep "Perturbed run, seed=" RESULTS/$TESTNAME/tangent1.trace | grep ", epsilon=" | tail -1 | awk 'BEGIN{FS="="}  {print $3}'`
          EPSILON=${EPSILON/[eE][+]/*10^}
          EPSILON=${EPSILON/[eE][-]/*10^-}
          PERTURBED_RESULT=`grep "Condensed perturbed result" RESULTS/$TESTNAME/tangent1.trace | tail -1 | awk '{print $6}' `
          PERTURBED_RESULT=${PERTURBED_RESULT/[eE][+]/*10^}
          PERTURBED_RESULT=${PERTURBED_RESULT/[eE][-]/*10^-}
          RESULT=`grep "Condensed result" RESULTS/$TESTNAME/tangent2.trace | tail -1 | awk '{print $5}' `
          RESULT=${RESULT/[eE][+]/*10^}
          RESULT=${RESULT/[eE][-]/*10^-}
          TANGENT=`grep "Condensed tangent" RESULTS/$TESTNAME/tangent2.trace | tail -1 | awk '{print $4}' `
          TANGENT=${TANGENT/[eE][+]/*10^}
          TANGENT=${TANGENT/[eE][-]/*10^-}
          if (($DEBUG==1))
              then
              echo -n '  RESULT:      ' ; echo $RESULT | bc -l
              echo -n '  PERTURBED:   ' ; echo $PERTURBED_RESULT | bc -l
          fi
          echo -n '  DIVIDED DIFF:' ; echo "(("$PERTURBED_RESULT" - "$RESULT") / ("$EPSILON"))" | bc -l
          echo -n '  TANGENT:     ' ; echo $TANGENT | bc -l
      fi
  fi


# Collect source files for the ADJOINT executable (from directory RESULTS/$TESTNAME):
  EXECADJ=0
  ADJSOURCEFILES=""
  SOMESOURCEFILES=`ls -r RESULTS/$TESTNAME | grep -E "_b\.$LANGEXTENSIONSPATTERN$" | grep -E -v "^incl"`
  for SOURCEFILE in $SOMESOURCEFILES
    do
    EXECADJ=1
    ADJSOURCEFILES=RESULTS/$TESTNAME/$SOURCEFILE" "$ADJSOURCEFILES
  done
# Compile and execute ADJOINT code (if present), then print Dot Product test:
  if (($EXECADJ==1))
      then
      if (($DEBUG==1)) ; then echo $COMPILE' -I '$ADFIRSTAIDKIT' '$DIFFSIZESFILE' '$ADJSOURCEFILES' '$ADFIRSTAIDKIT'/json.c '$ADFIRSTAIDKIT'/adContext.c '$ADFIRSTAIDKIT'/adFixedPoint.c '$ADFIRSTAIDKIT'/adStack.c '$ADFIRSTAIDKIT'/admm.c -o RESULTS/'$TESTNAME'/adjoint.exe' ; fi
      $COMPILE -I $ADFIRSTAIDKIT $DIFFSIZESFILE $ADJSOURCEFILES $DIFFSIZESFILE $ADFIRSTAIDKIT/json.c $ADFIRSTAIDKIT/adContext.c $ADFIRSTAIDKIT/adFixedPoint.c $ADFIRSTAIDKIT/adStack.c $ADFIRSTAIDKIT/admm.c -o RESULTS/$TESTNAME/adjoint.exe
      rm -rf RESULTS/$TESTNAME/*.mod
      if [ -f RESULTS/$TESTNAME/adjoint.exe ]
          then
          if (($DEBUG==1)) ; then echo 'RESULTS/'$TESTNAME'/adjoint.exe > RESULTS/'$TESTNAME'/adjoint.trace' ; fi
          RESULTS/$TESTNAME/adjoint.exe > RESULTS/$TESTNAME/adjoint.trace
          ADJOINT=`grep "Condensed adjoint" RESULTS/$TESTNAME/adjoint.trace | tail -1 | awk '{print $4}' `
          ADJOINT=${ADJOINT/[eE][+]/*10^}
          ADJOINT=${ADJOINT/[eE][-]/*10^-}
          echo -n '  ADJOINT:     ' ; echo $ADJOINT | bc -l
      fi
  fi

done
