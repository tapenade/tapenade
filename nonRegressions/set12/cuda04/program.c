#include <stdio.h>
#include <math.h>

// Preliminary check for cuda: checking 2 successive II-LOOPs
float foo(float *A, float *B, float p) {
  int i = 100 ;
  float res1, res2 ;

  p = sqrt(p) ;

  //$AD II-LOOP
  for (i=10 ; i<100 ; ++i) {
    A[i] = sin(A[i]) ;
    res1 = res1 + A[i]*A[i] + p;
  }

  p = cos(p) ;

  //$AD II-LOOP
  for (i=0 ; i<90 ; ++i) {
    res2 = res2 + B[i]*B[i+1] + p;
  }

  p = sin(p) ;

  //if (res2<5.0) res2 = 5.0 ; //This prevents FWD loop2 erasal

  return res1 - res2 ;  // res1*res2 //This prevents FWD loops erasal
}

int main() {
  float A[100],B[100],p,res ;
  for (int i=0 ; i<100 ; ++i) {
    A[i] = sin(i) ;
    B[i] = cos(i) ;
  }
  p = 12.5 ;

  res = foo(A,B,p) ;

  printf("result:%f\n", res) ;
  return 0 ;
}
