// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __computeprimitivesu_gpu(// HPC layer
                                         // Topology(ies)
                                         const int n_cellt,
                                         // Connectivity(ies)
                                         // Parameter(s)
                                         const double gam,
                                         const double cv,
                                         const double* __restrict__ density,
                                         const double* __restrict__ momentum,
                                         const double* __restrict__ energy_stagnation_density,
                                               double* __restrict__ velocity,
                                               double* __restrict__ temperature)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < n_cellt)
  {
    if (tx == 0) {
      printf("__computeprimitivesu_gpu tx=%i --> n_cellt = %i\n", tx, n_cellt);
    }
    // printf("tx=%i --> gam   = %f\n", tx, gam);
    // printf("tx=%i --> cv    = %f\n", tx, cv);

    // printf("tx=%i --> density   = %f\n", tx, density[tx]);
    // printf("tx=%i --> momentumx = %f\n", tx, momentum[tx          ]);
    // printf("tx=%i --> momentumy = %f\n", tx, momentum[tx+  n_cellt]);
    // printf("tx=%i --> momentumz = %f\n", tx, momentum[tx+2*n_cellt]);
    // printf("tx=%i --> energy_stagnation_density = %f\n", tx, energy_stagnation_density[tx]);

    double invcv = 1./cv;
    double rhoc  = density[tx];
    double rhouc = momentum[tx         ];
    double rhovc = momentum[tx+  n_cellt];
    double rhowc = momentum[tx+2*n_cellt];
    double rhoec = energy_stagnation_density[tx];

    double roi   = 1./rhoc;
    double roe   = rhoec - 0.5 * roi * (rhouc*rhouc + rhovc*rhovc + rhowc*rhowc);

    velocity[tx          ] = rhouc*roi;
    velocity[tx+  n_cellt] = rhovc*roi;
    velocity[tx+2*n_cellt] = rhowc*roi;

    temperature[tx] = roi*invcv*roe;

    // printf("tx=%i --> velocityx = %f\n", tx, velocity[tx          ]);
    // printf("tx=%i --> velocityy = %f\n", tx, velocity[tx+  n_cellt]);
    // printf("tx=%i --> velocityz = %f\n", tx, velocity[tx+2*n_cellt]);
    // printf("tx=%i --> temperature = %f\n", tx, temperature[tx]);
  }
}

// ===========================================================================
__host__ void computeprimitivesu_gpu(// HPC layer
                                     // Topology(ies)
                                     const int& n_cellt,
                                     // Connectivity(ies)
                                     // Parameter(s)
                                     const double& gam,
                                     const double& cv,
                                     // Field(s)
                                     const double* density,
                                     const double* momentum,
                                     const double* energy_stagnation_density,
                                           double* velocity,
                                           double* temperature)
{
  printf("computeprimitivesu_gpu %i \n", n_cellt);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoCellSdom = dim3(nThrs          ,1,1);
  dim3 blkTopoCellSdom = dim3(n_cellt/nThrs+1,1,1);

  // First Step
  __computeprimitivesu_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cellt,
                                                                 gam, cv,
                                                                 density,
                                                                 momentum,
                                                                 energy_stagnation_density,
                                                                 velocity,
                                                                 temperature);
  cudaError_t err = cudaGetLastError();
  checkCuda( cudaDeviceSynchronize() );
  if (err != cudaSuccess){
    printf("computeprimitivesu_gpu::Error: %s\n", cudaGetErrorString(err));
  }
}
