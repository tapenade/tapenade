// ------------------------------------------------------------------
// External include
//#include <iostream>
// ------------------------------------------------------------------

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __initfield5equ_gpu(const int                  n_cellt,
                                    const double               value,
                                          double* __restrict__ field)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < n_cellt) {
    field[tx          ] = value;
    field[tx+  n_cellt] = value;
    field[tx+2*n_cellt] = value;
    field[tx+3*n_cellt] = value;
    field[tx+4*n_cellt] = value;
  }
}

// ===========================================================================
__host__ void initfield5equ_gpu(const int&    n_cellt,
                                const double& value,
                                      double* field)
{
  printf("begin initfield5equ_gpu(n_cellt=%i, value=%f) \n", n_cellt, value);
  // Target --> Feed Block and Threads
  int n_thrs = 64;
  int n_blks = n_cellt/n_thrs+1;
  printf("n_thrs = %i \n", n_thrs);
  printf("n_blks = %i \n", n_blks);
  dim3 thr_topo_cell = dim3(n_thrs,1,1);
  dim3 blk_topo_cell = dim3(n_blks,1,1);

  // Initialize volume
  __initfield5equ_gpu<<<blk_topo_cell, thr_topo_cell>>>(n_cellt,
                                                        value,
                                                        field);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("initfield5equ_gpu::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );
  printf("end initfield5equ_gpu(n_cellt=%i, value=%f) \n", n_cellt, value);
}
