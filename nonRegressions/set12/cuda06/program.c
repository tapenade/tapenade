#include<stdio.h>

// This code is not really C because it uses the '&'
//  reference notation. But we want to allow Tapenade to parse
//  and differentiate it as C, for CUDA test cases from Onera.

void BAR(float u, float &v) {
  u = u*u;
  v = v*v;
}

float FOO(float *x, float y) {
  *x = 1.5 * *x ;
  BAR(*x,y) ;
  y = 2.5 * y ;
  return *x+y ;
}

int main() {
  float a = 2.0 ;
  float b = 3.0 ;
  float c = FOO(&a,b) ;
  printf("%f %f -> %f\n", a,b,c) ;
}
