// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __spectralradiusidealgasbnd5equ_gpu(const int n_cell_tot,
                                                    const int n_face,
                                                    const int n_face_bnd,
                                                    const double gam,
                                                    int*    __restrict__ indicf,
                                                    double* __restrict__ surf,
                                                    double* __restrict__ wb1,
                                                    double* __restrict__ spectralradius)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  double gam1  = gam - 1.;
  if( tx < n_face_bnd){

    int iface = indicf[tx];
    // int linc  = indicc[tx];

    double tnx = surf[iface        ];
    double tny = surf[iface+  n_face];
    double tnz = surf[iface+2*n_face];
    double tnn = norm3d(tnx, tny, tnz);

    double  ro  = wb1[tx           ];
    double  rou = wb1[tx+  n_face_bnd];
    double  rov = wb1[tx+2*n_face_bnd];
    double  row = wb1[tx+3*n_face_bnd];
    double  roe = wb1[tx+4*n_face_bnd];

    double ovro = 1./ro;
    double uu   = rou * ovro;
    double vv   = rov * ovro;
    double ww   = row * ovro;
    double ee   = 0.5 * ( uu*uu + vv*vv + ww*ww );
    double c2   = gam * gam1 * ( roe * ovro - ee);
    double cc   = sqrt(abs(c2));

    spectralradius[iface] = abs( uu * tnx + vv * tny + ww * tnz ) + cc * tnn;

  };

}

// ===========================================================================
__host__ void spectralradiusidealgasbnd5equ_gpu(const int&    n_cell_tot,
                                                const int&    n_face,
                                                const int&    n_face_bnd,
                                                const double& gam,
                                                int*          pointList,
                                                double*       surf,
                                                double*       wb1,
                                                double*       spectralradius)
{
  // Target --> Feed Block and Threads
  // printf("computefluxbndu_gpu %i \n", n_face);
  // printf("computefluxbndu_gpu %i \n", n_face_bnd);
  int nThrs = 64;
  dim3 thrTopoFaceBndSdom = dim3(nThrs           ,1,1);
  dim3 blkTopoFaceBndSdom = dim3(n_face_bnd/nThrs+1,1,1);

  __spectralradiusidealgasbnd5equ_gpu<<<blkTopoFaceBndSdom, thrTopoFaceBndSdom>>>(n_cell_tot,
                                                                                  n_face,
                                                                                  n_face_bnd,
                                                                                  gam,
                                                                                  pointList,
                                                                                  surf,
                                                                                  wb1,
                                                                                  spectralradius);

}


// ===========================================================================
__global__ void __spectralradiusidealgas5equ_gpu(const int    n_cell_tot,
                                                 const int    n_face,
                                                 const double gam,
                                                 const double rgaz,
                                                 double* __restrict__ rho,
                                                 double* __restrict__ velo,
                                                 double* __restrict__ temp,
                                                 double* __restrict__ surf,
                                                 double* __restrict__ spectralradius,
                                                 int*    __restrict__ face_cell)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  double gam1   = gam - 1.;
  double gam1_1 = 1./gam1;

  if( tx < n_face){
    // printf("tx %i [%i] --> [%i] [%f] \n", tx, n_face, face_vtx_idx[tx], coords[4] );
    int il = face_cell[tx      ];
    int ir = face_cell[tx+n_face];

    double tnx = surf[tx        ];
    double tny = surf[tx+  n_face];
    double tnz = surf[tx+2*n_face];
    double tnn = norm3d(tnx, tny, tnz);

    double vxl = velo[il];
    double vxr = velo[ir];

    double vyl = velo[il+n_cell_tot];
    double vyr = velo[ir+n_cell_tot];

    double vzl = velo[il+2*n_cell_tot];
    double vzr = velo[ir+2*n_cell_tot];

    double hm  = gam*gam1_1*temp[il]*rgaz + 0.5*( vxl*vxl + vyl*vyl + vzl*vzl );
    double hp  = gam*gam1_1*temp[ir]*rgaz + 0.5*( vxr*vxr + vyr*vyr + vzr*vzr );

    double w1mi = 1./rho[il];

    double r  = sqrt(rho[ir]*w1mi);
    double rr = 1./(1.+r);

    double uu = ( vxr * r + vxl )*rr;
    double vv = ( vyr * r + vyl )*rr;
    double ww = ( vzr * r + vzl )*rr;

    double ee = 0.5*(uu*uu + vv*vv + ww*ww);
    double hh = (hp*r+hm)*rr;

    double cc = sqrt(gam1*(hh-ee));

    spectralradius[tx] = abs( uu * tnx + vv * tny + ww * tnz ) + cc * tnn;

  }
}

// ===========================================================================
__host__ void spectralradiusidealgas5equ_gpu(const int&    n_cell_tot,
                                             const int&    n_face,
                                             const double& gam,
                                             const double& rgaz,
                                             int*          face_cell,
                                             double*       rho,
                                             double*       velo,
                                             double*       temp,
                                             double*       surf,
                                             double*       spectralradius)
{
  int nThrs = 64;
  dim3 thrTopoSdom  = dim3(nThrs,1,1);
  dim3 blkTopoSdom  = dim3(n_face/nThrs+1,1,1);

  __spectralradiusidealgas5equ_gpu<<<blkTopoSdom, thrTopoSdom>>>( n_cell_tot,
                                                                  n_face,
                                                                  gam,
                                                                  rgaz,
                                                                  rho,
                                                                  velo,
                                                                  temp,
                                                                  surf,
                                                                  spectralradius,
                                                                  face_cell);

}
