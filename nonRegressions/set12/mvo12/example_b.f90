!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) - 28 Sep 2023 14:56
!
!  Differentiation of cost in reverse (adjoint) mode (with options noISIZE):
!   gradient     of useful results: fc
!   with respect to varying inputs: x fc
!   RW status of diff variables: x:out fc:in-zero
SUBROUTINE COST_B(n, x, xb, m, fc, fcb)
  IMPLICIT NONE
!-- arguments
  INTEGER, INTENT(IN) :: n
  REAL(kind=8), INTENT(IN) :: x(n)
  REAL(kind=8) :: xb(n)
  INTEGER, INTENT(IN) :: m
  REAL(kind=8) :: fc
  REAL(kind=8) :: fcb
!-- local
  REAL(kind=8) :: y(m)
  REAL(kind=8) :: yb(m)
  INTEGER :: i
  INTERFACE 
      SUBROUTINE WRITEOUT(n, val)
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: n
        REAL(kind=8), INTENT(IN) :: val
      END SUBROUTINE WRITEOUT
  END INTERFACE

  INTERFACE 
      SUBROUTINE WRITEOUT_B(n, val, valb)
        IMPLICIT NONE
        INTEGER, INTENT(IN) :: n
        REAL(kind=8), INTENT(IN) :: val
        REAL(kind=8) :: valb
      END SUBROUTINE WRITEOUT_B
  END INTERFACE

  INTRINSIC SUM
  INTEGER :: ad_count
  INTEGER :: i0
  INTEGER*4 :: branch
  CALL PUSHINTEGER4(i)
  ad_count = 1
loop:DO i=1,n
    IF (i .GT. m) THEN
      GOTO 100
    ELSE
      y(i) = x(i)
      CALL PUSHINTEGER4(i)
      ad_count = ad_count + 1
    END IF
  END DO loop
  CALL PUSHCONTROL1B(0)
  CALL PUSHINTEGER4(ad_count)
  GOTO 110
 100 CALL PUSHCONTROL1B(1)
  CALL PUSHINTEGER4(ad_count)
 110 fc = SUM(y)
!-- writeout will be made accessible to tapenade via 
  CALL WRITEOUT_B(1, fc, fcb)
  yb = 0.0_8
  yb = fcb
  CALL POPINTEGER4(ad_count)
  DO i0=1,ad_count
    IF (i0 .EQ. 1) THEN
      CALL POPCONTROL1B(branch)
      IF (branch .EQ. 0) THEN
        xb = 0.0_8
      ELSE
        xb = 0.0_8
      END IF
    ELSE
      xb(i) = xb(i) + yb(i)
      yb(i) = 0.0_8
    END IF
    CALL POPINTEGER4(i)
  END DO
  fcb = 0.0_8
END SUBROUTINE COST_B

