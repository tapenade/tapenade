subroutine cost(n, x, m, fc)
  implicit none
  !-- arguments
  integer, intent(in) :: n
  real(kind=8), intent(in) :: x(n)
  integer, intent(in) :: m
  real(kind=8), intent(out) :: fc
  !-- local
  real(kind=8) :: y(m)
  integer :: i
  interface
     subroutine writeout(n,val)
       implicit none
       integer, intent(in) :: n
       real(kind=8), intent(in) :: val
     end subroutine writeout
  end interface
  loop: do i=1,n
     if( i.gt.m ) then
        exit loop
     else
        y(i) = x(i)
     endif
  end do loop
  fc = sum(y)
  call writeout(1, fc) !-- writeout will be made accessible to tapenade via 
end subroutine foo
