! Tests parsing of array constructors that start with a type cast
subroutine test1(a)
  implicit none
  character(len=16), dimension(3) :: ch_words
  real*8, dimension(3) :: tabr1, tabr2, tabr3
  real a,v

  ch_words = [ character(16) :: 'cat', 'donkey', 'orca' ]
  tabr1 = [real(8) :: 1, 2.0, 3.d0]
  tabr2 = [real(8) :: a*a, 2.0, 1]
  tabr3 = [a, 2*a, 3*a]
  a = foo(ch_words, tabr1, tabr2, tabr3, a)
end subroutine test1
