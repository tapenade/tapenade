// ------------------------------------------------------------------
// External include
//#include <iostream>
// ------------------------------------------------------------------

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __initializeconservatives5equ_gpu(const int                  n_cell,
                                                  const int                  n_cellt,
                                                  const double* __restrict__ density_init,
                                                  const double* __restrict__ momentumx_init,
                                                  const double* __restrict__ momentumy_init,
                                                  const double* __restrict__ momentumz_init,
                                                  const double* __restrict__ energy_stagnation_density_init,
                                                        double* __restrict__ density,
                                                        double* __restrict__ momentum,
                                                        double* __restrict__ energy_stagnation_density)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < n_cell) {
    density[tx]                   = density_init[tx];
    momentum[tx          ]        = momentumx_init[tx];
    momentum[tx+  n_cellt]        = momentumy_init[tx];
    momentum[tx+2*n_cellt]        = momentumz_init[tx];
    energy_stagnation_density[tx] = energy_stagnation_density_init[tx];
    // printf("tx=%i --> density_init=%f\n", tx, density_init[tx]);
    // printf("tx=%i --> density     =%f\n", tx, density[tx]);

    if (tx == 0)
    {
      printf("__initializeconservatives5equ_gpu n_cellt=%i, n_cell=%i\n\n", n_cellt, n_cell);

      printf("tx=%i --> density_init=%f\n", tx, density_init[tx]);
      printf("tx=%i --> momentumx_init=%f\n", tx, momentumx_init[tx]);
      printf("tx=%i --> momentumy_init=%f\n", tx, momentumy_init[tx]);
      printf("tx=%i --> momentumz_init=%f\n", tx, momentumz_init[tx]);
      printf("tx=%i --> energy_stagnation_density_init=%f\n\n", tx, energy_stagnation_density_init[tx]);

      printf("tx=%i --> density                =%f\n", tx, density[tx]);
      printf("tx=%i --> momentum[tx          ] =%f\n", tx, momentum[tx          ]);
      printf("tx=%i --> momentum[tx+  n_cellt] =%f\n", tx, momentum[tx+  n_cellt]);
      printf("tx=%i --> momentum[tx+2*n_cellt] =%f\n", tx, momentum[tx+2*n_cellt]);
      printf("tx=%i --> energy_stagnation_density     =%f\n\n", tx, energy_stagnation_density[tx]);
    }
  }
}

// ===========================================================================
__host__ void initializeconservatives5equ_gpu(const int&    n_cell,
                                              const int&    n_cellt,
                                              const double* density_init,
                                              const double* momentumx_init,
                                              const double* momentumy_init,
                                              const double* momentumz_init,
                                              const double* energy_stagnation_density_init,
                                                    double* density,
                                                    double* momentum,
                                                    double* energy_stagnation_density)
{
  printf("begin initializeconservatives5equ_gpu(n_cell=%i, n_cellt=%i) \n", n_cell, n_cellt);
  // Target --> Feed Block and Threads
  int n_thrs = 64;
  int n_blks = n_cell/n_thrs+1;
  printf("n_thrs = %i \n", n_thrs);
  printf("n_blks = %i \n", n_blks);
  dim3 thr_topo_cell = dim3(n_thrs,1,1);
  dim3 blk_topo_cell = dim3(n_blks,1,1);

  // Initialize volume
  __initializeconservatives5equ_gpu<<<blk_topo_cell, thr_topo_cell>>>(n_cell,
                                                                      n_cellt,
                                                                      density_init,
                                                                      momentumx_init,
                                                                      momentumy_init,
                                                                      momentumz_init,
                                                                      energy_stagnation_density_init,
                                                                      density,
                                                                      momentum,
                                                                      energy_stagnation_density);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("initializeconservatives5equ_gpu::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );
  printf("end initializeconservatives5equ_gpu(n_cell=%i, n_cellt=%i) \n", n_cell, n_cellt);
}
