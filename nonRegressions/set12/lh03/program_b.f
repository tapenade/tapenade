C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) - 30 Aug 2023 15:15
C
C  Differentiation of foo in reverse (adjoint) mode:
C   gradient     of useful results: a1 x y
C   with respect to varying inputs: a1 x y
C   RW status of diff variables: a1:in-out x:in-out y:in-out
C Assume WORK() uses the variable a1 from COMMON /ccc/
C If Tapenade is not given this info (in mylib), it
C considers WORK does not use a1 and therefore initialization
C of a1 in PREPARE() is wrongly considered dead code.
      SUBROUTINE FOO_B(x, xb, y, yb)
      IMPLICIT NONE
      REAL x, y
      REAL xb, yb
      REAL o1, a1(10)
      REAL a1b(10)
      COMMON /ccc/ o1, a1
      COMMON /ccc_b/ a1b
      EXTERNAL WORK_B
      EXTERNAL WORK
C
      CALL PREPARE(x)
      CALL PUSHREAL4(x)
      x = 2.7*x*y
      CALL WORK_B(x, xb, y, yb)
      CALL POPREAL4(x)
      yb = yb + x*2.7*xb
      xb = y*2.7*xb
      CALL PREPARE_B(x, xb)
      END

C  Differentiation of prepare in reverse (adjoint) mode:
C   gradient     of useful results: a1 x
C   with respect to varying inputs: a1 x
C
      SUBROUTINE PREPARE_B(x, xb)
      IMPLICIT NONE
      REAL x, y
      REAL xb
      REAL o1, a1(10)
      REAL a1b(10)
      COMMON /ccc/ o1, a1
      COMMON /ccc_b/ a1b
      INTEGER i
      INTRINSIC SIN
      DO i=10,1,-1
        xb = xb + i*COS(i*x)*a1b(i)
        a1b(i) = 0.0
      ENDDO
      END

