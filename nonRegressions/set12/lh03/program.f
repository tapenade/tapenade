c Assume WORK() uses the variable a1 from COMMON /ccc/
c If Tapenade is not given this info (in mylib), it
c considers WORK does not use a1 and therefore initialization
c of a1 in PREPARE() is wrongly considered dead code.
      SUBROUTINE FOO(x,y)
      REAL x,y
      REAL o1,a1(10)
      COMMON /ccc/ o1,a1
c
      CALL PREPARE(x)
      x = 2.7*x*y
      CALL WORK(x,y)
      END

      SUBROUTINE PREPARE(x)
      REAL x,y
      REAL o1,a1(10)
      COMMON /ccc/ o1,a1
      INTEGER i
c
      DO i=1,10
         a1(i) = SIN(i*x)
      ENDDO
      END
