c Almost same bug as set12/lh10
c The cause seems to be a source declared as *.f, but
c  using F90 features, therefore producing a *.f90 output,
c  but NBDirsMax dimension would not use array notation!

      module b2mod_driver

      type B2PlasmaX
        real(R8), dimension(:,:), allocatable :: ua
      end type B2PlasmaX

      type B2StateX
        type(B2PlasmaX) :: pl
      end type B2StateX

      contains

      subroutine b2mndr_1 (xx, state)
      use b2mod_ad
      type(B2StateX), intent (inout)    :: state
      real*8 xx

      state%pl%ua = 0.D0
      do while (state%pl%ua(1,2).gt.42)
         state%pl%ua(1,2) = state%pl%ua(1,2) + xx
      end do
      end subroutine b2mndr_1

      end module b2mod_driver
