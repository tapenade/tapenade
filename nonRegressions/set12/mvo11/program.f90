! From Calculix. In the declaration of a function,
! one should not declare the dimension of the return value
!MVO:2023-07-27
!- code taken from set07/lh202,
!  interface declaration makes sure the hand-written transpose is really called,
!  program added in order to allow for 'execAD'
      subroutine BUG(AINV, DET)
      IMPLICIT NONE
      DOUBLE PRECISION, DIMENSION(3,3), INTENT(OUT) :: AINV
      DOUBLE PRECISION :: DET
      DOUBLE PRECISION, DIMENSION(3,3) :: COFACTOR
      interface
         function TRANSPOSE(x)
           implicit none
           DOUBLE PRECISION X(3,3),TRANSPOSE(3,3)
         end function TRANSPOSE
      end interface
      COFACTOR(:,:) = DET
      AINV = TRANSPOSE(COFACTOR) / DET
      END

      function TRANSPOSE(X)
      IMPLICIT NONE
      DOUBLE PRECISION X(3,3),TRANSPOSE(3,3)
      print*, 'running hand-written transpose'
      TRANSPOSE(:,:) = X(2,3)
      end function

      program main
        implicit none
        double precision x, y(3,3)
        x = 5_8
        print*, 'x=', x
        call bug(y, x)
        print*, '--------'
        print*, 'y=', y
      end program main
