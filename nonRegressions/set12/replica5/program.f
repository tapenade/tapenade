C Testing functionality "replicateDiff" asked by Onera:
C Bug found when inlining "MAX"
      FUNCTION FOO(sc1,sc2,sc3,minsurf,one,pm,utr,soundm,
     +     soundp,pp,igam6b,gam6b,gam8b)
      IMPLICIT REAL*8 (a-z)

      invsn = one/MAX(SQRT(sc1*sc1 + sc2*sc2 + sc3*sc3), minsurf)

      ptr = pm*(MAX(utr ,0.d0)
     +     /(soundm + soundp*((pm/pp)**igam6b)))**gam6b

      soundm = soundm*SQRT(gam8b*MAX(ptr/pm - 1.d0, 0.D0))

      FOO = invsn+ptr+soundm
      end

