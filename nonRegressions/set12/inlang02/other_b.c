/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (bugfix_inputLanguage) -  7 Mar 2024 11:05
*/
#include <adStack.h>
// This is in fact C code. The extension is oddly wrong... on purpose!
void test_nodiff(float x, float y, float *z);
void test_b(float x, float *xb, float y, float *yb, float *z, float *zb);

/*
  Differentiation of test2 in reverse (adjoint) mode:
   gradient     of useful results: x y *z
   with respect to varying inputs: x y *z
   RW status of diff variables: x:incr y:incr z:(loc) *z:in-out
   Plus diff mem management of: z:in
*/
void test2_b(float x, float *xb, float y, float *yb, float *z, float *zb) {
    /* first declarations */
    float u = x*2;
    float ub = 0.0;
    float v = y*u;
    float vb = 0.0;
    /* first statement */
    pushReal4(u);
    u = u*v;
    /* second declaration */
    float w = (*z)*u;
    float wb = 0.0;
    /* second statement */
    pushReal4(*z);
    *z = w*(*z);
    test_b(x, xb, y, yb, z, zb);
    popReal4(z);
    wb = (*z)*(*zb);
    *zb = w*(*zb) + u*wb;
    ub = (*z)*wb;
    popReal4(&u);
    vb = u*ub;
    ub = v*ub + y*vb;
    *yb = *yb + u*vb;
    *xb = *xb + 2*ub;
}
