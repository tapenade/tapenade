// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __computefluxbnd_gpu(const int                  n_face,
                                     const int                  n_face_bnd,
                                     const int*    __restrict__ indic_face,
                                     const double               gam,
                                     const double* __restrict__ wb1,
                                     const double* __restrict__ surf,
                                           double* __restrict__ flux)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  double gam1  = gam - 1.;
  if (tx < n_face_bnd) {

    int iface = indic_face[tx];

    if (tx == 0){
      printf("__computefluxbnd_gpu [%i/%i] = \n", tx, iface);
    }
    double tnx = surf[iface        ];
    double tny = surf[iface+  n_face];
    double tnz = surf[iface+2*n_face];

    double  ro  = wb1[tx           ];
    double  rou = wb1[tx+  n_face_bnd];
    double  rov = wb1[tx+2*n_face_bnd];
    double  row = wb1[tx+3*n_face_bnd];
    double  roe = wb1[tx+4*n_face_bnd];

    double u = rou / ro;
    double v = rov / ro;
    double w = row / ro;

    double p = gam1 * (roe - 0.5 * (rou*rou + rov*rov + row*row )/ro);

    // C ---   Compute density flux
    double fcx1 = rou;
    double fcy1 = rov;
    double fcz1 = row;

    // C ---   Compute momentum flux
    double fcx2 = rou*u  +  p;
    double fcy2 = rou*v;
    double fcz2 = rou*w;

    // C        fcx3 = rov*u
    double fcy3 = rov*v  +  p;
    double fcz3 = rov*w;

    // C        fcx4 = row*u
    // C        fcy4 = row*v
    double fcz4 = row*w  +  p;

    // C        This coding avoids 3 multiply,
    // C        and insures "symmetry";
    // C        this seems to be necessary for Hirett test case on NEC
    double fcx3 = fcy2;
    double fcx4 = fcz2;
    double fcy4 = fcz3;

    // C ---   Compute energy flux
    double fcx5 = u * (roe + p);
    double fcy5 = v * (roe + p);
    double fcz5 = w * (roe + p);

    flux[iface         ] = fcx1 * tnx + fcy1 * tny + fcz1 * tnz;
    flux[iface+  n_face] = fcx2 * tnx + fcy2 * tny + fcz2 * tnz;
    flux[iface+2*n_face] = fcx3 * tnx + fcy3 * tny + fcz3 * tnz;
    flux[iface+3*n_face] = fcx4 * tnx + fcy4 * tny + fcz4 * tnz;
    flux[iface+4*n_face] = fcx5 * tnx + fcy5 * tny + fcz5 * tnz;
    // if(n_face_bnd == 128){
    //   printf("gpu check computefluxbndu_gpu [%i/%i] = [%f/%f/%f/%f/%f] \n", iface, n_face_bnd, flux[iface        ],
    //                                                                                          flux[iface+  n_face],
    //                                                                                          flux[iface+2*n_face],
    //                                                                                          flux[iface+3*n_face],
    //                                                                                          flux[iface+4*n_face]);
    // }
  }
}

// ===========================================================================
__host__ void computefluxbndu_gpu(const int&    n_face,
                                  const int&    n_face_bnd,
                                  const int*    indic_face,
                                  const double& gam,
                                  const double* wb1,
                                  const double* surf,
                                        double* flux)
{
  // Target --> Feed Block and Threads
  printf("computefluxbndu_gpu n_face=%i \n", n_face);
  printf("computefluxbndu_gpu n_face_bnd=%i \n", n_face_bnd);
  printf("computefluxbndu_gpu gam=%f \n", gam);
  int nThrs = 64;
  dim3 thr_topo_face_bnd_sdom = dim3(nThrs             ,1,1);
  dim3 blk_topo_face_bnd_sdom = dim3(n_face_bnd/nThrs+1,1,1);

  __computefluxbnd_gpu<<<blk_topo_face_bnd_sdom, thr_topo_face_bnd_sdom>>>(n_face,
                                                                           n_face_bnd,
                                                                           indic_face,
                                                                           gam,
                                                                           wb1,
                                                                           surf,
                                                                           flux);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("computefluxbndu_gpu::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );
  // int bytes = n_face_bnd * sizeof(double)*5;
  // std::vector<double> check(n_face_bnd*5);
  // checkCuda( cudaMemcpy(check.data(), flux, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < n_face_bnd; ++i){
  //   printf("cpu check fluxbal [%i] = [%f/%f/%f/%f/%f] \n", i, check[i],
  //                                                             check[i+  n_cellTot],
  //                                                             check[i+2*n_cellTot],
  //                                                             check[i+3*n_cellTot],
  //                                                             check[i+4*n_cellTot]);
  // }

}
