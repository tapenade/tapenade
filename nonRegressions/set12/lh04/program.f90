module test
! module testing bugs of missing declarations
! of temporary vars in adjoint code
  INTEGER n
  REAL*8 :: Rho_i,Rho_a,Dt,tau_socle
  REAL*8, DIMENSION(10) :: wsocle,H,Bsoc,B0,wsocle
  REAL*8 :: xxx, xxx_ctl
  REAL*8 :: yyy, yyy_ctl
  REAL*8 :: zzz, zzz_ctl

contains

subroutine bugtempb
  if (n.gt.0) print *,'coucou'
  wsocle(:)=H(:)*(Rho_i/Rho_a)
  Bsoc(:)=Bsoc(:)+Dt*(B0(:)-Bsoc(:)-wsocle(:))/tau_socle
end subroutine bugtempb

subroutine bugbranch
  if (n.gt.0) print *,'coucou'
  IF (n.eq.1)     xxx = xxx_ctl
  IF (n.eq.2)     yyy = yyy_ctl
  IF (n.eq.3)     zzz = zzz_ctl
end subroutine bugbranch

subroutine refnobug(a,t1,t2,d)
  REAL*8 :: d
  REAL*8, DIMENSION(20) :: a,t1,t2
  a(:) = t1(:)*d + sum(t2(:)) * d 
  if (d.gt.0.0) then
     d = sin(d)
  end if
  d = d*d
end subroutine refnobug



end module test
