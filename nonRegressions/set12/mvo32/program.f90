!
!-- procedure pointers
!
module func

contains

  subroutine sq(x, y)
    implicit none
    real, intent(in)  :: x
    real, intent(out) :: y

    y = x**2
  end subroutine sq

  subroutine id(x,y)
    implicit none
    real, intent(in)  :: x
    real, intent(out) :: y

    y = x
  end subroutine id
  
end module func


program main
  use iso_fortran_env, only : output_unit
  implicit none

  interface
     subroutine f(x,y)
       implicit none
       real, intent(in)  :: x
       real, intent(out) :: y
     end subroutine f
  end interface

  
  real :: x, y

  x = 2.5

  call caller(x,y)
  write(output_unit,*) 'x=',x,'y=',y

contains
  subroutine caller(x,y)
    use func
    implicit none
    real, intent(in) :: x
    real, intent(out) ::y
    procedure(f), pointer :: pp => null()

    if( x.gt.0 ) then
       pp => sq
    else
       pp => id
    endif

    call pp(x, y)
  end subroutine caller
end program main
