! Bug found by Krishna of HFBTHO
! In multidir mode, on an array of pointers to arrays,
! the multi-dir dimension was added on both array levels.

MODULE test

  Integer, parameter :: pr=8

  Type :: ptr_to_2darray
     Real(pr), Dimension(:,:), Allocatable :: arr
!     Integer :: intfield
  End Type ptr_to_2darray

  Type(ptr_to_2darray), Allocatable :: allhfb(:)

contains

  SUBROUTINE foo(x, nb)
    real(pr) x,s
    integer nb, i,j,k

    ALLOCATE(allhfb(nb))
    DO i = 1,nb
       ALLOCATE(allhfb(i)%arr(5, 10))
    END DO
    DO i = 1,nb
       DO j = 1,5
          DO k = 1,10
             allhfb(i)%arr(j,k) = 0.33*j*k+x
          END DO
       END DO
    END DO
    s = 0.0
    DO i = 1,nb
       DO j = 1,5
          DO k = 1,10
             s = s+allhfb(i)%arr(j,k)
          END DO
       END DO
    END DO
    x = s
  END SUBROUTINE foo

END MODULE test

program main
  USE test
  real(pr) a
  a = 1.0
  call foo(a, 7)
  print *,"a:",a
end program main
