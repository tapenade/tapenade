// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __initcentercell_gpu(const int n_cell_tot,
                                     double* __restrict__ centercell,
                                     int*    __restrict__ count_center)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_cell_tot){
    count_center[tx]            = 0;
    centercell[tx             ] = 0.;
    centercell[tx+  n_cell_tot] = 0.;
    centercell[tx+2*n_cell_tot] = 0.;
  };
}

// ===========================================================================
__global__ void __computecentercellu_gpu(const int n_cell_tot,
                                         const int n_face,
                                         const int n_vtx,
                                         const int begFace,
                                         const int endFace,
                                         double* __restrict__ centers,
                                         double* __restrict__ coords,
                                         int*    __restrict__ count_center,
                                         int*    __restrict__ face_vtx_idx,
                                         int*    __restrict__ face_vtx,
                                         int*    __restrict__ face_cell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < endFace){
    // printf("tx %i [%i] --> [%i] [%f] \n", tx, n_face, face_vtx_idx[tx], coords[4] );

    int il = face_cell[tx      ];
    int ir = face_cell[tx+n_face];

    // 1er conflit de bank ...
    int begvtx = face_vtx_idx[tx  ];
    int endvtx = face_vtx_idx[tx+1];

    int nbtrion_face = endvtx-begvtx;

    double xc    = 0.;
    double yc    = 0.;
    double zc    = 0.;
    int    count = 0;

    for(int pp = 1; pp < nbtrion_face+1; ++pp){
      int adrvtx1 = begvtx +   pp - 1 ;
      int indvtx1 = face_vtx[adrvtx1]-1;

      count += 1;

      xc += coords[indvtx1       ];
      yc += coords[indvtx1+  n_vtx];
      zc += coords[indvtx1+2*n_vtx];
    }

    count_center[il] += count;
    count_center[ir] += count;

    centers[il          ] += xc;
    centers[il+  n_cell_tot] += yc;
    centers[il+2*n_cell_tot] += zc;

    centers[ir          ] += xc;
    centers[ir+  n_cell_tot] += yc;
    centers[ir+2*n_cell_tot] += zc;

  };
}

// ===========================================================================
__global__ void __scalecentercell_gpu(const int n_cell_tot,
                                      double* __restrict__ centercell,
                                      int*    __restrict__ count_center)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_cell_tot){
    double oneovercount = 1./count_center[tx];
    centercell[tx             ] = centercell[tx             ]*oneovercount;
    centercell[tx+  n_cell_tot] = centercell[tx+  n_cell_tot]*oneovercount;
    centercell[tx+2*n_cell_tot] = centercell[tx+2*n_cell_tot]*oneovercount;
    // printf("tx %i [%i] --> %i [%f] [%f/%f/%f] \n", tx, n_cell_tot, count_center[tx], oneovercount, centercell[tx          ],
    //                                                                        centercell[tx+  n_cell_tot],
    //                                                                        centercell[tx+2*n_cell_tot] );
  };
}

// ===========================================================================
__host__ void computecentercellu_gpu(const int& n_cell_tot,
                                     const int& n_face,
                                     const int& n_vtx,
                                     const int& n_sdom,
                                     int*    face_vect_tile_idx,
                                     int*    nvect_pack,
                                     int*    nvect_pack_idx,
                                     int*    face_vect_ext_tile_idx,
                                     int*    nvect_ext_pack,
                                     int*    nvect_ext_pack_idx,
                                     double* centers,
                                     double* coords,
                                     int*    count_center,
                                     int*    face_vtx_idx,
                                     int*    face_vtx,
                                     int*    face_cell)
{
  printf("computecentercellu_gpu %i \n", n_vtx);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoCellSdom = dim3(nThrs           ,1,1);
  dim3 blkTopoCellSdom = dim3(n_cell_tot/nThrs+1,1,1);

  // First Step
  __initcentercell_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cell_tot, centers, count_center);

  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int i_vec = 0; i_vec < nvect_pack[i_sub]; ++i_vec){
      int i_beg = nvect_pack_idx[i_sub];
      int n_faceLoc = face_vect_tile_idx[i_beg+i_vec+1]-face_vect_tile_idx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_faceLoc[%i]\n", i_sub, i_vec, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __computecentercellu_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cell_tot, n_face, n_vtx,
                                                                     face_vect_tile_idx[i_beg+i_vec  ],
                                                                     face_vect_tile_idx[i_beg+i_vec+1],
                                                                     centers,
                                                                     coords,
                                                                     count_center,
                                                                     face_vtx_idx,
                                                                     face_vtx,
                                                                     face_cell);
    }
  }

  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int i_vec = 0; i_vec < nvect_ext_pack[i_sub]; ++i_vec){
      int i_beg = nvect_ext_pack_idx[i_sub];
      int n_faceLoc = face_vect_ext_tile_idx[i_beg+i_vec+1]-face_vect_ext_tile_idx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_faceLocBnd[%i]\n", i_sub, i_vec, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __computecentercellu_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cell_tot, n_face, n_vtx,
                                                                     face_vect_ext_tile_idx[i_beg+i_vec],
                                                                     face_vect_ext_tile_idx[i_beg+i_vec+1],
                                                                     centers,
                                                                     coords,
                                                                     count_center,
                                                                     face_vtx_idx,
                                                                     face_vtx,
                                                                     face_cell);
    }
  }

  // First Step
  __scalecentercell_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cell_tot, centers, count_center);


  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("Error: %s\n", cudaGetErrorString(err));
  }
  // checkCuda( cudaDeviceSynchronize() );

  // int bytes = n_vtx * sizeof(double);
  // std::vector<double> array(n_vtx);
  // checkCuda( cudaMemcpy(array.data(), a, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < n_vtx; ++i){
  //   printf("cpu array[%i] = [%f] \n", i, array[i]);
  // }
}
