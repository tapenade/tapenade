// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __computecenterinterfaceu_gpu(const int                  n_vtx,
                                              const int                  n_face,
                                              const int*    __restrict__ face_vtx_array,
                                              const int*    __restrict__ face_vtx_idx,
                                              const double* __restrict__ coords,
                                                    double* __restrict__ centerint)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_face){
    if (tx == 0) {
      printf("__computecenterinterfaceu_gpu: tx=%i [n_vtx=%i, n_face=%i]\n", tx, n_vtx, n_face);
    }

    int begvtx = face_vtx_idx[tx  ];
    int endvtx = face_vtx_idx[tx+1];
    int nbtrion_face = endvtx-begvtx;
    // printf("tx=%i [begvtx=%i, endvtx=%i]\n", tx, begvtx, endvtx);
    // printf("tx=%i [nbtrion_face=%i]\n", tx, nbtrion_face);

    double xcf = 0.;
    double ycf = 0.;
    double zcf = 0.;

    for(int pp = 1; pp < nbtrion_face+1; ++pp){
      int adrvtx1 = begvtx +   pp - 1 ;
      int indvtx1 = face_vtx_array[adrvtx1]-1;
      // printf("tx %i [%i] --> [%i]\n", pp, adrvtx1);
      // printf("tx %i [%i] --> [%i]\n", pp, indvtx1);
      xcf = xcf + coords[indvtx1       ];
      ycf = ycf + coords[indvtx1+  n_vtx];
      zcf = zcf + coords[indvtx1+2*n_vtx];
      // printf("tx %i [%i] --> [xcf=%f,ycf=%f,zcf=%f]\n", pp, xcf, ycf, zcf);
    }

    xcf = xcf/nbtrion_face;
    ycf = ycf/nbtrion_face;
    zcf = zcf/nbtrion_face;

    centerint[tx         ] = xcf;
    centerint[tx+  n_face] = ycf;
    centerint[tx+2*n_face] = zcf;
    // printf("tx=%i --> centerint=[%f/%f/%f]\n", tx, xcf, ycf, zcf);
  }
}

// ===========================================================================
__host__ void computecenterinterfaceu_gpu(const int&    n_vtx,
                                          const int&    n_face,
                                          const int*    face_vtx_array,
                                          const int*    face_vtx_idx,
                                          const double* coords,
                                                double* centerint)
{
  printf("begin computecenterinterfaceu_gpu(n_vtx=%i, n_face=%i) \n", n_vtx, n_face);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  int nBlks = n_face/nThrs+1;
  dim3 thrTopoSdom = dim3(nThrs,1,1);
  dim3 blkTopoSdom = dim3(nBlks,1,1);
  // printf("coords = %p \n", coords);

  __computecenterinterfaceu_gpu<<<blkTopoSdom, thrTopoSdom>>>(n_vtx, n_face,
                                                              face_vtx_array,
                                                              face_vtx_idx,
                                                              coords,
                                                              centerint);

  // cudaError_t err = cudaGetLastError();
  // if (err != cudaSuccess){
  //   printf("Error: %s\n", cudaGetErrorString(err));
  // }
  // checkCuda( cudaDeviceSynchronize() );
  printf("end computecenterinterfaceu_gpu(n_vtx=%i, n_face=%i) \n", n_vtx, n_face);
}
