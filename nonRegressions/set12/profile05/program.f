C cet exemple implemente le cas z = 2/(z+1) ou dans le cas le plus general z = 2/(z+x)
C Dans le cas particulier la solution est 1. Donc si par hasard l'utilisateur donne un intial guess= solution finale =1 le nombre des iterations sera 1  
C
C This example introduces the problem of profiling fixed point in a case where the parameters aren't derived (because of some optimization)
      SUBROUTINE root(x,y, initial)

      Real z, oz,y, tmp
      Real x, initial
      Integer i
     
      z=initial
      oz = z + 1
      i=0

C     $AD FP-LOOP z
      DO WHILE ((z-oz)**2 .GE. 1.e-10) 
         oz = z
         call foo(z,tmp)
         z = sin(tmp*x)
         i=i+1
      ENDDO

      y=z*x
      print *,  "nombre des iterations", i      
      END


      subroutine foo(a,b)
      real a,b 
      b = a*a
      end
