#include <math.h>

// Example of a C program with a basic fixed point used for profiling

float fp_fun(float target_val, float root_pm1, float cur_val) {
	return 1.0/2.0*(cur_val + target_val / pow(cur_val,root_pm1));
}

float proot(float target_val, float p, float init_val){
	float pm1 = p - 1;
	float res = init_val;
	float old_val = res + 1;
	int i = 0;

	float eps_close_enough = 1e-10;

	/* SAD FP-LOOP */
	while( pow(res - old_val,2) > eps_close_enough ) {
		old_val = res;
		res = fp_fun(target_val, pm1, old_val);
		i++;
	}

	return res;
}
