
      program testprog
      real*8  xa(4),ya(4),x,y,dy
      ya = (/-1.0895119519904739e-06,-6.1794582673080356e-07,
     &      -2.7429540105927686e-07,-2.4247451602121828e-08/)
      xa = (/6.2756576986636254e+01,6.4848320007569143e+01,
     &       6.6974353897697668e+01,6.9134678657021794e+01/)
      x  = 1.0585591320688266e+02
      call polint(xa,ya,4,x,y,dy)
      write(*,*) y
      write(*,*) dy
      end


c *id* polint **********************************************************
c given arrays xa and ya, each of length n, and given a value x, this
c routine returns a value y, and an error estimate dy.  if p(x) is the
c polynomial of degree n-1 such that p(xa(i)) = ya(i), i=1,...,n, then
c the returned value y = p(x).
c ----------------------------------------------------------------------
      subroutine polint(xa,ya,n,x,y,dy)
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      parameter (nmax=10)
      dimension xa(n),ya(n),c(nmax),d(nmax)
      ns=1
      dif=abs(x-xa(1))
      do 11 i=1,n
        dift=abs(x-xa(i))
        if (dift.lt.dif) then
          ns=i
          dif=dift
        end if
        c(i)=ya(i)
        d(i)=ya(i)
   11 continue
      y=ya(ns)
      ns=ns-1
      do 13 m=1,n-1
        do 12 i=1,n-m
          ho=xa(i)-x
          hp=xa(i+m)-x
          w=c(i+1)-d(i)
          den=ho-hp
c          if (den.eq.0.) pause
          den=w/den
          d(i)=hp*den
          c(i)=ho*den
   12   continue
        if (2*ns.lt.n-m) then
          dy=c(ns+1)
        else
          dy=d(ns)
          ns=ns-1
        end if
        y=y+dy
   13 continue
      return
      end
      
