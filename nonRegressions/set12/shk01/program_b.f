C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) - 25 Jul 2023 16:34
C
C  Differentiation of testprog as a context to call adjoint code (with options context fixinterface):
C
      PROGRAM TESTPROG_B
      IMPLICIT NONE
      REAL*8 xa(4), ya(4), x, y, dy
      REAL*8 yab(4), yb
      ya = (/-1.0895119519904739e-06, -6.1794582673080356e-07, -
     +  2.7429540105927686e-07, -2.4247451602121828e-08/)
      xa = (/6.2756576986636254e+01, 6.4848320007569143e+01, 
     +  6.6974353897697668e+01, 6.9134678657021794e+01/)
      x = 1.0585591320688266e+02
      CALL ADCONTEXTADJ_INIT(0.87_8)
      CALL ADCONTEXTADJ_INITREAL8('y'//CHAR(0), y, yb)
      CALL POLINT_B(xa, ya, yab, 4, x, y, yb, dy)
      CALL ADCONTEXTADJ_STARTCONCLUDE()
      CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('ya'//CHAR(0), ya, yab, 4)
      CALL ADCONTEXTADJ_CONCLUDE()
      WRITE(*, *) y
      WRITE(*, *) dy
      END

C  Differentiation of polint in reverse (adjoint) mode (with options context fixinterface):
C   gradient     of useful results: y
C   with respect to varying inputs: ya
C   RW status of diff variables: y:in-killed ya:out
C
C
C *id* polint **********************************************************
C given arrays xa and ya, each of length n, and given a value x, this
C routine returns a value y, and an error estimate dy.  if p(x) is the
C polynomial of degree n-1 such that p(xa(i)) = ya(i), i=1,...,n, then
C the returned value y = p(x).
C ----------------------------------------------------------------------
      SUBROUTINE POLINT_B(xa, ya, yab, n, x, y, yb, dy)
      IMPLICIT NONE
      INTEGER*4 nmax
      PARAMETER (nmax=10)
      INTEGER*4 n
      REAL*8 d
      REAL*8 db
      REAL*8 c
      REAL*8 cb
      REAL*8 xa
      REAL*8 ya
      DIMENSION xa(n), ya(n), c(nmax), d(nmax)
      REAL*8 yab
      DIMENSION yab(n), cb(nmax), db(nmax)
      INTEGER*4 ns
      REAL*8 dif
      INTRINSIC ABS
      INTEGER*4 i
      REAL*8 dift
      INTEGER*4 m
      REAL*8 ho
      REAL*8 hp
      REAL*8 w
      REAL*8 wb
      REAL*8 den
      REAL*8 denb
      INTEGER ii1
      INTEGER*4 ad_to
      INTEGER*4 branch
      REAL*8 x
      REAL*8 dy
      REAL*8 dyb
      REAL*8 y
      REAL*8 yb
      ns = 1
      IF (x - xa(1) .GE. 0.) THEN
        dif = x - xa(1)
      ELSE
        dif = -(x-xa(1))
      END IF
      DO i=1,n
        IF (x - xa(i) .GE. 0.) THEN
          dift = x - xa(i)
        ELSE
          dift = -(x-xa(i))
        END IF
        IF (dift .LT. dif) THEN
          ns = i
          dif = dift
        END IF
      ENDDO
      CALL PUSHINTEGER4(ns)
      ns = ns - 1
      DO m=1,n-1
        DO i=1,n-m
          ho = xa(i) - x
          hp = xa(i+m) - x
          CALL PUSHREAL8(den)
          den = ho - hp
C          if (den.eq.0.) pause
        ENDDO
        CALL PUSHINTEGER4(i - 1)
        IF (2*ns .LT. n - m) THEN
          CALL PUSHCONTROL1B(0)
        ELSE
          CALL PUSHINTEGER4(ns)
          ns = ns - 1
          CALL PUSHCONTROL1B(1)
        END IF
      ENDDO
      DO ii1=1,nmax
        db(ii1) = 0.D0
      ENDDO
      DO ii1=1,nmax
        cb(ii1) = 0.D0
      ENDDO
      DO m=n-1,1,-1
        dyb = yb
        CALL POPCONTROL1B(branch)
        IF (branch .EQ. 0) THEN
          cb(ns+1) = cb(ns+1) + dyb
        ELSE
          CALL POPINTEGER4(ns)
          db(ns) = db(ns) + dyb
        END IF
        CALL POPINTEGER4(ad_to)
        DO i=ad_to,1,-1
          hp = xa(i+m) - x
          ho = xa(i) - x
          denb = ho*cb(i) + hp*db(i)
          cb(i) = 0.D0
          wb = denb/den
          db(i) = -wb
          CALL POPREAL8(den)
          cb(i+1) = cb(i+1) + wb
        ENDDO
      ENDDO
      CALL POPINTEGER4(ns)
      DO ii1=1,n
        yab(ii1) = 0.D0
      ENDDO
      yab(ns) = yab(ns) + yb
      DO i=n,1,-1
        yab(i) = yab(i) + db(i) + cb(i)
        db(i) = 0.D0
        cb(i) = 0.D0
      ENDDO
      END

