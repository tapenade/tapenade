c Testing a bug of misplaced declarations of a function
c in its calling routine, when both are differentiated
c in split mode.
c Also a bug of duplicated declaration of index ii1
c Bugs found on Dan Goldberg's MITgcm halfpipe_streamice.
      subroutine TOP(a,b)
      REAL*8 a,b
      call FOO(a,b)
      a = a*b
      end

      subroutine FOO(a,b)
      REAL*8 a,b,c
      CHARACTER*80 fnamegenout(10)
      REAL*8 ttt(20,30)
      INTEGER i
      EXTERNAL BAR,GEE
      REAL*8 BAR
      ttt(6,8) = a
      c = BAR(a,b)
      b = a*c
      do i =1,10
         WRITE(fnamegenout(i),'(2a,i10.10)') b,c,i
         call EXT(ttt)
      enddo
      call GEE(fnamegenout,b)
      a = b*c
      a = a*ttt(5,7)
      end

      REAL*8 FUNCTION BAR(a,b)
      REAL*8 a,b
      bar = SIN(a*b)
      END
