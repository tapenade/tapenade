C Bug de PUSH/POP inutiles (i.e. en trop),
C sur des variables non encore initialisees,
C ce qui peut causer des messages d'erreur du compilo,
C par exemple en mode statictape
      SUBROUTINE test_ad_save(nCel,neq,
     &                        nFac, nFacInt,
     &                        pctrad,harten_type,
     &                        rho,flux)
      IMPLICIT NONE
      REAL*8 HALF
      PARAMETER (HALF = 0.5D0)
      INTEGER*4 nCel, neq
      INTEGER*4 nFac, nFacInt
      REAL*8    pctrad
      INTEGER*4 harten_type
      REAL*8    rho(0:nCel-1)
      REAL*8    flux(0:nFac-1,neq)
      REAL*8    small
      REAL*8    q1
      REAL*8    mask
      INTEGER*4 interf
      REAL*8    E_CUTOFF_HARTEN

C$AD II-LOOP
      DO interf=1,nFac
         small = (2-harten_type)*pctrad + (harten_type-1)*pctrad
         q1 = HALF + SIGN(HALF,-small)
         mask = HALF + SIGN(HALF, small - E_CUTOFF_HARTEN)
         flux(interf,neq) = mask * ( q1*rho(interf)
     &                   + q1*HALF*(rho(interf)*rho(interf)
     &                              +small*small)/small)
     &                   + (1.d0-mask)*rho(interf)
      END DO

C$AD II-LOOP
      DO interf=1,nFacInt
         small = (2-harten_type)*pctrad + (harten_type-1)*pctrad
         q1 = HALF + SIGN(HALF,-small)
         mask = HALF + SIGN(HALF, small - E_CUTOFF_HARTEN)
         flux(interf,neq) = mask * ( q1*rho(interf)
     &                   + q1*HALF*(rho(interf)*rho(interf)
     &                              +small*small)/small)
     &                   + (1.d0-mask)*rho(interf)
      END DO

      END
