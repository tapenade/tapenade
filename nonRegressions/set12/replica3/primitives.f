! Entree : wcons(0:ncell-1,neq)
! Sortie : rho(0:ncell-1)
!          velo(0:ncell-1,3)
!          temp(0:ncell-1)

      SUBROUTINE primitives(neq, ncell,
     &                      gam, cv,
     &                      wcons,
     &                      rho,
     &                      velo,
     &                      temp)
      IMPLICIT NONE


C ======================================================================
C_BEGIN DECLARATION

C_Intrinsic Functions
      INTRINSIC SQRT

C_IN
      INTEGER ncell,  neq

      REAL*8    wcons(0:ncell-1,neq)
      REAL*8    gam, cv

C_IN_OUT
      REAL*8    rho(0:ncell-1)
      REAL*8    velo(0:ncell-1,3)
      REAL*8    temp(0:ncell-1)

C_LOC
      INTEGER   icell
      REAL*8    roi, roe, gam1, invcv, one

C_END DECLARATION
C ======================================================================

      one = 1.0d0
      gam1  = gam-one
      invcv = one/cv

      do icell=0,ncell-1

        roi = one / wcons(icell,1)
        roe = wcons(icell,5) - HALF * roi * (wcons(icell,2)*wcons(icell,2)+
     &                                       wcons(icell,3)*wcons(icell,3)+
     &                                       wcons(icell,4)*wcons(icell,4))

        rho(icell)         = wcons(icell,1)
        velo(icell,1)      = wcons(icell,2)*roi
        velo(icell,2)      = wcons(icell,3)*roi
        velo(icell,3)      = wcons(icell,4)*roi
        temp(icell) = roi*invcv*roe

      end do

      END
