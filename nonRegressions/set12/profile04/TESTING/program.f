C This examples computes the proot of a number using fixed point iterations
C inval corresponds to the value being rooted (i.e. 2.5 for computing root(2.5))
C outval is the output of the computations
C rootp is the rank of roots to be computed (note: it works fine for integer values, but nothing prevents from using something else)
C initial corresponds to the inital guess provided. A good choice is initial = inval
      SUBROUTINE proot(inval, rootp, outresult,  initial)

      Real inval, outresult, initial, rootp
      Real oldval, outval
      Integer i
      Real powm 

      powm = rootp - 1
     
      outval=initial
      oldval =outval + 1
      i=0

C     $AD FP-LOOP outval
      DO WHILE ((outval-oldval)**2 .GE. 1.e-10) 
         oldval = outval
C         call fooFP(oldval,outval, powm, inval)
         outval = 1./2.*(oldval + val/(oldval ** powm))
         i=i+1
      ENDDO

      outresult = outval*1.

      END
