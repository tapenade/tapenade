! designed to test a few bugs on lost $AD LABEL,
! misplaced declarations (wrt LABEL), or duplicate declarations.
subroutine test_primitives_u( &
  i_sub, &
  n_sdom, &
  entity_sdom_pack_beg,&
  entity_sdom_pack_n,&
  entity_vect_pack_beg,&
  entity_vect_pack_n, &
  n_entity, &
  gamma, &
  cv, &
  density, &
  momentum, &
  energy_stagnation_density, &
  velocity, &
  temperature)

  implicit none
!$AD LABEL def_fortran_files
#include "DefFortranLH.h"
#include "DefFortranConstLH.h"
#include "DefFortranFormulaLH.h"
!$AD END-LABEL def_fortran_files

! ======================================================================
!_BEGIN DECLARATION

  !_IN
  INTEGER_E i_sub, n_sdom

  INTEGER_E entity_sdom_pack_beg(0:*)
  INTEGER_E entity_sdom_pack_n  (0:*)
  INTEGER_E entity_vect_pack_beg(0:*)
  INTEGER_E entity_vect_pack_n  (0:*)

  INTEGER_E n_entity

  REAL_E    gamma, cv

  !_IN_OUT
  REAL_E    density(0:n_entity-1)
  REAL_E    momentum(0:n_entity-1,3)
  REAL_E    energy_stagnation_density(0:n_entity-1)
  REAL_E    velocity(0:n_entity-1,3)
  REAL_E    temperature(0:n_entity-1)

  !_LOC
  REAL_E    roi, roe, invcv

  INTEGER_E i_entity, beg_entity, end_entity
      
!_END DECLARATION
! ======================================================================

  invcv = ONE/cv

!FOR_OPT_DIR_NODEP_F77_E
!$AD II-LOOP
  do i_entity = 1,100
    roi = ONE / density(i_entity)
    roe = energy_stagnation_density(i_entity) &
        - HALF*roi*vmodsq3(momentum(i_entity,1),momentum(i_entity,2),momentum(i_entity,3))

    velocity(i_entity,1)  = momentum(i_entity,1)*roi
    velocity(i_entity,2)  = momentum(i_entity,2)*roi
    velocity(i_entity,3)  = momentum(i_entity,3)*roi
    temperature(i_entity) = roi*invcv*roe
  enddo

end subroutine test_primitives_u
