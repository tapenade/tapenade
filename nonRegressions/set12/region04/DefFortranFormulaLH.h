

!     REAL_E    E_BADVALUE_F
!     INTEGER_E E_BADVALUE_I
!     INTEGER_E E_CHECKPHYS

!     COMMON /BADVAL/ E_BADVALUE_F, E_BADVALUE_I, E_CHECKPHYS


! STATEMENT_FUNCTION
      REAL_E    vmod2     ! modulus of 2-D vector
      REAL_E    vmod3     ! modulus of 3-D vector
      REAL_E    vmodsq2   ! squared modulus of 2-D vector
      REAL_E    vmodsq3   ! squared modulus of 3-D vector
      REAL_E    xdum, ydum, zdum

      vmod2  (xdum, ydum)       = SQRT(xdum*xdum + ydum*ydum)
      vmod3  (xdum, ydum, zdum) = SQRT(xdum*xdum + ydum*ydum + zdum*zdum)
      vmodsq2(xdum, ydum)       = xdum*xdum + ydum*ydum
      vmodsq3(xdum, ydum, zdum) = xdum*xdum + ydum*ydum + zdum*zdum
#define e_from_w(W,n) (W(n,5) - HALF*vmodsq3(W(n,2),W(n,3),W(n,4))/W(n,1)) /W(n,1)

! ===== DefFortranFormulaLH === Last line ===
