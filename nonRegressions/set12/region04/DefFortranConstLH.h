
!     -------------------------
!     Integer Fortran constants
!     -------------------------


      INTEGER_E ZERO_I
      INTEGER_E ONE_I
      INTEGER_E TWO_I
      INTEGER_E THREE_I
      INTEGER_E E_MAXITERNWT
      INTEGER_E NB_NUMERROR_MAX

      INTEGER_E NSPLINEMAX

      INTEGER_E X_AXIS_E, Y_AXIS_E, Z_AXIS_E
      INTEGER_E VELO_X,   VELO_Y,   VELO_Z
      INTEGER_E GRAD_XX,  GRAD_XY,  GRAD_XZ
      INTEGER_E GRAD_YX,  GRAD_YY,  GRAD_YZ
      INTEGER_E GRAD_ZX,  GRAD_ZY,  GRAD_ZZ
      INTEGER_E TENSXX,   TENSXY,   TENSXZ
      INTEGER_E TENSYY,   TENSYZ,   TENSZZ
      INTEGER_E DIMSURF

      PARAMETER (ZERO_I = 0)
      PARAMETER (ONE_I  = 1)
      PARAMETER (TWO_I  = 2)
      PARAMETER (THREE_I= 3)
      PARAMETER (E_MAXITERNWT = 30)
      PARAMETER (NB_NUMERROR_MAX = 10)

      PARAMETER (NSPLINEMAX = 1750)

      PARAMETER (X_AXIS_E=1, Y_AXIS_E=2, Z_AXIS_E=3)

      PARAMETER (VELO_X  = 1, VELO_Y  = 2, VELO_Z  = 3)
      PARAMETER (GRAD_XX = 1, GRAD_XY = 2, GRAD_XZ = 3)
      PARAMETER (GRAD_YX = 4, GRAD_YY = 5, GRAD_YZ = 6)
      PARAMETER (GRAD_ZX = 7, GRAD_ZY = 8, GRAD_ZZ = 9)

      PARAMETER (TENSXX =1)
      PARAMETER (TENSXY =2)
      PARAMETER (TENSXZ =3)
      PARAMETER (TENSYY =4)
      PARAMETER (TENSYZ =5)
      PARAMETER (TENSZZ =6)

      PARAMETER (DIMSURF=3)

!     ----------------------
!     Real Fortran constants
!     ----------------------
      REAL_E E_INFINITE
      REAL_E E_MIN_SURFACE
      REAL_E E_CUTOFF
      REAL_E E_CUTOFF_HARTEN

      REAL_E EIGHTH
      REAL_E FOURTH
      REAL_E THIRD
      REAL_E HALF

!     SIX is not defined because of Lhs

      REAL_E ZERO, ONE, TWO, THREE, FOUR, FIVE
      REAL_E SEVEN, EIGHT, NINE, TEN, ELEVEN, TWELVE, SIXTEEN
      REAL_E TWENTY, TWENTYFOUR
      REAL_E THIRTY, THIRTYTWO
      REAL_E FOURTYEIGHT

      REAL_E FIFTY
      REAL_E E_PI
      REAL_E E_LN10
      REAL_E E_MAXEXP
      REAL_E INFTYS
      REAL_E E_MAXFLOAT


      PARAMETER (EIGHTH       =  0.125D0  )
      PARAMETER (FOURTH       =  0.25D0   )
      PARAMETER (THIRD        =  1.D0/3.D0)
      PARAMETER (HALF         =  0.5D0    )

      PARAMETER (ZERO         =  0.0D0    )
      PARAMETER (ONE          =  1.0D0    )
      PARAMETER (TWO          =  2.0D0    )
      PARAMETER (THREE        =  3.0D0    )
      PARAMETER (FOUR         =  4.0D0    )
      PARAMETER (FIVE         =  5.0D0    )
      PARAMETER (SEVEN        =  7.0D0    )
      PARAMETER (EIGHT        =  8.0D0    )
      PARAMETER (NINE         =  9.0D0    )
      PARAMETER (TEN          = 10.0D0    )
      PARAMETER (ELEVEN       = 11.0D0    )
      PARAMETER (TWELVE       = 12.0D0    )
      PARAMETER (SIXTEEN      = 16.0D0    )
      PARAMETER(TWENTY        = 20.D0     )
      PARAMETER(TWENTYFOUR    = 24.D0     )
      PARAMETER(THIRTY        = 30.D0     )
      PARAMETER(THIRTYTWO     = 32.D0     )
      PARAMETER(FOURTYEIGHT   = 48.D0     )
      PARAMETER (FIFTY        = 50.0D0    )

      PARAMETER (E_PI         =  3.14159265358979323846264338328D0)
      PARAMETER (E_LN10       =  2.3025850929940456840179914546843642D0)

!     Avoid exponential overflow
!     exp(50.) = 5.18E+021 ==> not so big!
      PARAMETER (E_MAXEXP     = 708.D0)
      PARAMETER (INFTYS       =  1.0D-30)
      PARAMETER (E_MAXFLOAT   =  1.00000000000000000000D+30)
      PARAMETER (E_INFINITE   =  1.0D20)

      PARAMETER (E_MIN_SURFACE   =  1.d-32)
      PARAMETER (E_CUTOFF        =  1.d-12)
      PARAMETER (E_CUTOFF_HARTEN =  1.d-12)

! ===== DefFortranConstLH.h === Last line ===
