module abstract_data
  implicit none
  private

  public :: data_container
  type, abstract :: data_container
     real :: the_data
   contains
     procedure, non_overridable :: print_data
     procedure, non_overridable :: set_value
     procedure :: get_value
  end type data_container


contains
  subroutine set_value(this, avalue)
    class(data_container) :: this
    real, intent(in) :: avalue

    this%the_data = avalue
  end subroutine set_value

  subroutine get_value(this, avalue)
    class(data_container) :: this
    real, intent(inout) :: avalue

    print*, 'data_container::get_value, the_data=', this%the_data, 'avalue=', avalue
    avalue = avalue + this%the_data
  end subroutine get_value

  subroutine print_data(this)
    class(data_container) :: this

    print*, 'the_data=', this%the_data
  end subroutine print_data
end module abstract_data


module data
  use abstract_data
  implicit none
  private

  public :: data_typ1, data_typ2
  type, extends(data_container) :: data_typ1
   contains
     procedure :: calc_value => calc_value_typ1 !
  end type data_typ1

  type, extends(data_container) :: data_typ2
   contains
     procedure :: get_value
  end type data_typ2

contains

  subroutine calc_value_typ1(this, avalue)
    implicit none
    class(data_typ1) :: this
    real, intent(inout) :: avalue

    print*, 'data_typ1::calc_value, the_data=', this%the_data, 'avalue=', avalue

    avalue = avalue + this%the_data
  end subroutine calc_value_typ1

  subroutine get_value(this, avalue)
    class(data_typ2) :: this
    real, intent(inout) :: avalue
    print*, 'data_typ2::get_value, the_data=', this%the_data, 'avalue=', avalue
    avalue = avalue + this%the_data*3
  end subroutine get_value
  
end module data



program main
  use abstract_data
  use data
  implicit none

  real :: x1, x2, y1, y2

  type(data_typ1) :: t1
  type(data_typ2) :: t2

  print*, 'running with instance of data_typ1...'
  x1 = 1.
  y1 = 0.
  call foo(t1, x1, y1)
  print*, 'x1=',x1,'y1=',y1

  print*, '----------'
  print*, '----------'
  print*, 'running with instancoe of data_typ2...'
  x2 = 1.
  y2 = 0.
  call foo(t2, x2, y2)
  print*, 'x2=',x2,'y2=',y2
  
contains
  subroutine foo(t, x, y)
    class(data_container) :: t
    real, intent(in) :: x
    real, intent(inout) :: y

    call t%set_value(x)
    call t%print_data

    select type (t)
    class is (data_typ1)
       call t%calc_value(y)
    end select
    call t%get_value(y)
  end subroutine foo

  
end program main
