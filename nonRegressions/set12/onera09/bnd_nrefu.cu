
// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __compute_nref_bndu_gpu(
  // Declaration argument(s) for unstructured boundary
  const int               n_cellt,
  const int               n_face,
  const int               n_face_bnd,
  // Component argument(s)
  // --------------------
  // Input/output argument(s)
  // ------------------------
  int const* __restrict__ index_point_list_window_bc_t0,
  int const* __restrict__ index_indic_int_cell_bc_t1,
  double const* __restrict__ field_surface_normal_zone_t2,
  double const* __restrict__ field_conservatives_zone_t3,
  double const param_specific_heat_ratio_bc_t4,
  double const param_density_bc_t5,
  double const param_momentum_x_bc_t6,
  double const param_momentum_y_bc_t7,
  double const param_momentum_z_bc_t8,
  double const param_energy_stagnation_density_bc_t9,
  double* __restrict__ field_conservatives_bc_t10)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  // Local variable(s)
  // -----------------
  int sensinw;
  int ling, linc;
  double sx, sy, sz;
  double snorm;
  double invsfn;
  double nx, ny, nz;
  // Specific local variable(s)
  // --------------------------

  double gam1,gam4,gam5;
  double eps0, epsm, epsp;

  double state_inf[5];
  double ro0,  rod,  ros,   ro;               // Density (0-, d-, s- and 1-states)
  double roc0;                                // Density * Sound Velocissty (0-state)
  double roqn0;                               // Density * Normal Velocity (0-state)
  double pd,   ps,   p;                       // Static pressure (d-, s- and 1-states)
  double qnd,  qns,  qn;                      // Normal velocity (d-, s- and 1-states)
  double qtxd, qtxs, qtx;                     // Tangential velocity - x-component (d-, s- and 1-states)
  double qtyd, qtys, qty;                     // Tangential velocity - y-component (d-, s- and 1-states)
  double qtzd, qtzs, qtz;                     // Tangential velocity - z-component (d-, s- and 1-states)
  double qxd,  qxs;                           // Velocity - x-component (d- and s-states)
  double qyd,  qys;                           // Velocity - y-component (d- and s-states)
  double qzd,  qzs;                           // Velocity - z-component (d- and s-states)
  double am,   ap,   b0,    bs;               // Intermediate values

  double vx1,vy1,vz1;
  double rho0,rhou0,rhov0,rhow0,rhoe0;
  double rhoc,rhouc,rhovc,rhowc,rhoec;
  // Component local variable(s)
  // ---------------------------

  // Initialization(s)
  // -----------------

  // Algebraic expressions from param_specific_heat_ratio_bc_t4
  gam1 = param_specific_heat_ratio_bc_t4 - 1.;
  gam4 = 1./gam1;
  gam5 = param_specific_heat_ratio_bc_t4*gam1;

  state_inf[0] = param_density_bc_t5;
  state_inf[1] = param_momentum_x_bc_t6;
  state_inf[2] = param_momentum_y_bc_t7;
  state_inf[3] = param_momentum_z_bc_t8;
  state_inf[4] = param_energy_stagnation_density_bc_t9;
  // Component computation(s)
  // ------------------------


  if(tx < n_face_bnd) {
    ling = index_point_list_window_bc_t0[tx];
    linc = index_indic_int_cell_bc_t1[tx];

    sensinw = -1;

    // printf("tx %i [%i] --> [%i] [%i] \n", tx, n_face, ling, linc);
    sx     = field_surface_normal_zone_t2[ling         ];
    sy     = field_surface_normal_zone_t2[ling+  n_face];
    sz     = field_surface_normal_zone_t2[ling+2*n_face];
    snorm  = sqrt(sx*sx + sy*sy + sz*sz);
    invsfn = sensinw / snorm;
    nx     = invsfn*sx;
    ny     = invsfn*sy;
    nz     = invsfn*sz;

    // Computation(s)
    // --------------

    rhoc  = field_conservatives_zone_t3[linc          ];
    rhouc = field_conservatives_zone_t3[linc+  n_cellt];
    rhovc = field_conservatives_zone_t3[linc+2*n_cellt];
    rhowc = field_conservatives_zone_t3[linc+3*n_cellt];
    rhoec = field_conservatives_zone_t3[linc+4*n_cellt];

    // If extrapolation is required with gradient for exemple
    rho0  = rhoc;  // - cf*dwcons[linc          ]
    rhou0 = rhouc; // - cf*dwcons[linc+  n_cellt]
    rhov0 = rhovc; // - cf*dwcons[linc+2*n_cellt]
    rhow0 = rhowc; // - cf*dwcons[linc+3*n_cellt]
    rhoe0 = rhoec; // - cf*dwcons[linc+4*n_cellt]

    ro0   = rho0;
    roc0  = gam5 * (ro0*rhoe0 - 0.5*(rhou0*rhou0 + rhov0*rhov0 + rhow0*rhow0));
    roc0  = sqrt(roc0);
    roqn0 = (rhou0*nx + rhov0*ny + rhow0*nz);
    epsm  = 0.5 + copysign(0.5,roc0-roqn0);
    eps0  = 0.5 + copysign(0.5,-roqn0);
    epsp  = 0.5 + copysign(0.5,-roc0-roqn0);

    rod   = state_inf[0];
    qxd   = state_inf[1] / rod;
    qyd   = state_inf[2] / rod;
    qzd   = state_inf[3] / rod;
    pd    = gam1*(state_inf[4] - 0.5*rod*(qxd*qxd + qyd*qyd + qzd*qzd));
    qnd   = (qxd*nx + qyd*ny + qzd*nz);
    qtxd  = qxd - qnd*nx;
    qtyd  = qyd - qnd*ny;
    qtzd  = qzd - qnd*nz;

    ros   = rhoc;
    qxs   = rhouc / ros;
    qys   = rhovc / ros;
    qzs   = rhowc / ros;
    ps    = gam1 * (rhoec - 0.5*ros*(qxs*qxs + qys*qys + qzs*qzs));
    qns   = (qxs*nx + qys*ny + qzs*nz);
    qtxs  = qxs - qns*nx;
    qtys  = qys - qns*ny;
    qtzs  = qzs - qns*nz;

    qtx  = eps0*qtxs + (1.-eps0)*qtxd;
    qty  = eps0*qtys + (1.-eps0)*qtyd;
    qtz  = eps0*qtzs + (1.-eps0)*qtzd;
    am   = epsm*(ps-roc0*qns) + (1.-epsm)*(pd-roc0*qnd);
    ap   = epsp*(ps+roc0*qns) + (1.-epsp)*(pd+roc0*qnd);
    qn   = (ap-am) * 0.5 / roc0;
    p    = (ap+am) * 0.5;
    bs   = (p-ps)*pow(ro0,2)/pow(roc0,2) + ros;
    b0   = (p-pd)*pow(ro0,2)/pow(roc0,2) + rod;
    ro   = eps0*bs + (1.-eps0)*b0;

    vx1 = qtx+qn*nx;
    vy1 = qty+qn*ny;
    vz1 = qtz+qn*nz;

    field_conservatives_bc_t10[tx             ] = ro;
    field_conservatives_bc_t10[tx+  n_face_bnd] = ro * vx1;
    field_conservatives_bc_t10[tx+2*n_face_bnd] = ro * vy1;
    field_conservatives_bc_t10[tx+3*n_face_bnd] = ro * vz1;
    field_conservatives_bc_t10[tx+4*n_face_bnd] = p * gam4 + 0.5*ro*(vx1*vx1 + vy1*vy1 + vz1*vz1);
  }
}

// ===========================================================================
__host__ void compute_nref_bndu_gpu(
  // Declaration argument(s) for unstructured boundary
  const int&    n_cellt,
  const int&    n_face,
  const int&    n_face_bnd,
  // Component argument(s)
  // ---------------------
  // Input/output argument(s)
  // ------------------------
  int const* index_point_list_window_bc_t0,
  int const* index_indic_int_cell_bc_t1,
  double const* field_surface_normal_zone_t2,
  double const* field_conservatives_zone_t3,
  double const& param_specific_heat_ratio_bc_t4,
  double const& param_density_bc_t5,
  double const& param_momentum_x_bc_t6,
  double const& param_momentum_y_bc_t7,
  double const& param_momentum_z_bc_t8,
  double const& param_energy_stagnation_density_bc_t9,
  double* field_conservatives_bc_t10)
{
  // Block and threads definition
  printf("Begin compute_nref_bndu_gpu(n_cellt=%i, n_face=%i, n_face_bnd=%i) \n", n_cellt, n_face, n_face_bnd);
  const int n_blks = n_face_bnd/n_thread_per_block+1;
  dim3 thr_topo_cell = dim3(n_thread_per_block,1,1);
  dim3 blk_topo_cell = dim3(n_blks            ,1,1);

  __compute_nref_bndu_gpu<<<blk_topo_cell, thr_topo_cell>>>(
    // Call argument(s) for unstructured boundary
    n_cellt,
    n_face,
    n_face_bnd,
    // Component argument(s)
    // ---------------------
    // Input/output argument(s)
    // ------------------------
    index_point_list_window_bc_t0,
    index_indic_int_cell_bc_t1,
    field_surface_normal_zone_t2,
    field_conservatives_zone_t3,
    param_specific_heat_ratio_bc_t4,
    param_density_bc_t5,
    param_momentum_x_bc_t6,
    param_momentum_y_bc_t7,
    param_momentum_z_bc_t8,
    param_energy_stagnation_density_bc_t9,
    field_conservatives_bc_t10);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("compute_nref_bndu_gpu::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );
  printf("End compute_nref_bndu_gpu(...)\n");
}

// ===========================================================================
__host__ void compute_nref_bndu_gpu_graph(cudaGraph_t& graph,
  // Declaration argument(s) for unstructured boundary
  const int&    n_cellt,
  const int&    n_face,
  const int&    n_face_bnd,
  // Component argument(s)
  // ---------------------
  // Input/output argument(s)
  // ------------------------
  int const* index_point_list_window_bc_t0,
  int const* index_indic_int_cell_bc_t1,
  double const* field_surface_normal_zone_t2,
  double const* field_conservatives_zone_t3,
  double const& param_specific_heat_ratio_bc_t4,
  double const& param_density_bc_t5,
  double const& param_momentum_x_bc_t6,
  double const& param_momentum_y_bc_t7,
  double const& param_momentum_z_bc_t8,
  double const& param_energy_stagnation_density_bc_t9,
  double* field_conservatives_bc_t10)
{
  // Block and threads definition
  printf("Begin compute_nref_bndu_gpu_graph(n_cellt=%i, n_face=%i, n_face_bnd=%i) \n", n_cellt, n_face, n_face_bnd);
  int n_blks = n_face_bnd/n_thread_per_block+1;
  dim3 thr_topo_cell = dim3(n_thread_per_block,1,1);
  dim3 blk_topo_cell = dim3(n_blks            ,1,1);

  cudaGraphNode_t dependencies_node;
  prepare_kernel_graph_node(graph,
    dependencies_node,
    thr_topo_cell,
    blk_topo_cell,
    __compute_nref_bndu_gpu,
    // Call argument(s) for unstructured boundary
    n_cellt,
    n_face,
    n_face_bnd,
    // Component argument(s)
    // ---------------------
    // Input/output argument(s)
    // ------------------------
    index_point_list_window_bc_t0,
    index_indic_int_cell_bc_t1,
    field_surface_normal_zone_t2,
    field_conservatives_zone_t3,
    param_specific_heat_ratio_bc_t4,
    param_density_bc_t5,
    param_momentum_x_bc_t6,
    param_momentum_y_bc_t7,
    param_momentum_z_bc_t8,
    param_energy_stagnation_density_bc_t9,
    field_conservatives_bc_t10);
  printf("End compute_nref_bndu_gpu_graph(...)\n");
}
