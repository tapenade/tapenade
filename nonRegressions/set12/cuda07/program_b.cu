//        Generated by TAPENADE     (INRIA, Ecuador team)
//  Tapenade 3.16 (feature_cuda) - 31 May 2022 19:34
//
#include <adStack.h>
#include <adContext.h>
#include <stdio.h>
#include <stdlib.h>

/* First experiments with differentiation of memory layouts */
//  Differentiation of subfunc in reverse (adjoint) mode (with options context):
//   gradient     of useful results: a1 *pv
//   with respect to varying inputs: a1 *pv
//   Plus diff mem management of: pv:in
__global__ void subfunc_b(float a1, float *a1b, float *pv, float *pvb) {
    //Must be atomic increment
    *a1b = *a1b + pv[1]*pvb[0];
    pvb[1] = pvb[1] + a1*pvb[0];
}

/* First experiments with differentiation of memory layouts */
__global__ void subfunc_nodiff(float a1, float *pv) {
    pv[0] = pv[0] + a1*pv[1];
}

//  Differentiation of func in reverse (adjoint) mode (with options context):
//   gradient     of useful results: [alloc*d_y in foo] [alloc*d_x in foo]
//                *x *y a
//   with respect to varying inputs: [alloc*d_y in foo] [alloc*d_x in foo]
//                *x *y a
//   Plus diff mem management of: x:in y:in
__global__ void func_b(int n, float a, float *ab, float *x, float *xb, float *
        y, float *yb) {
    // a, x, *x, y, *y are shared for all threads,
    // i, pv[] are local/private to the thread.
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    float pv[2];
    float pvb[2];
    int ii1;
    pv[0] = x[i + 5] + x[i + 3];
    pv[1] = y[i];
    if (i < n - 5) {
        for (ii1 = 0; ii1 < 2; ++ii1)
            pvb[ii1] = 0.0;
        pvb[0] = pvb[0] + yb[i];
        yb[i] = 0.0;
    } else
        for (ii1 = 0; ii1 < 2; ++ii1)
            pvb[ii1] = 0.0;
    subfunc_b(a, ab, pv, pvb);
    //Must be atomic increment
    yb[i] = yb[i] + pvb[1];
    pvb[1] = 0.0;
    //Must be atomic increment
    xb[i + 5] = xb[i + 5] + pvb[0];
    //Must be atomic increment
    xb[i + 3] = xb[i + 3] + pvb[0];
}

__global__ void func_nodiff(int n, float a, float *x, float *y) {
    // a, x, *x, y, *y are shared for all threads,
    // i, pv[] are local/private to the thread.
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    float pv[2];
    pv[0] = x[i + 5] + x[i + 3];
    pv[1] = y[i];
    subfunc_nodiff(a, pv);
    if (i < n - 5)
        y[i] = pv[0];
}

//  Differentiation of foo in reverse (adjoint) mode (with options context):
//   gradient     of useful results: [alloc*y in main] [alloc*x in main]
//                [alloc*d_y in foo] [alloc*d_x in foo] *x *y a
//   with respect to varying inputs: [alloc*y in main] [alloc*x in main]
//                [alloc*d_y in foo] [alloc*d_x in foo] *x *y a
//   RW status of diff variables: [alloc*y in main]:in-out [alloc*x in main]:incr
//                [alloc*d_y in foo]:in-out [alloc*d_x in foo]:in-out
//                x:(loc) *x:incr y:(loc) *y:in-out a:incr
//   Plus diff mem management of: x:in y:in
__host__ void foo_b(int N, float a, float *ab, float *x, float *xb, float *y, 
        float *yb) {
    __device__ float *d_x, *d_y;
    __device__ float *d_xb, *d_yb;
    int ii1;
    cudaMalloc(&d_xb, N*sizeof(float));
    cudaZero(d_xb, N);
    cudaMalloc(&d_x, N*sizeof(float));
    cudaMalloc(&d_yb, N*sizeof(float));
    cudaZero(d_yb, N);
    cudaMalloc(&d_y, N*sizeof(float));
    cudaMemcpy_b(y, yb, d_y, d_yb, N*sizeof(float), cudaMemcpyDeviceToHost);
    func_b<<<(N+255)/256, 256>>>(N, a, ab, d_x, d_xb, d_y, d_yb);
    cudaMemcpy_b(d_y, d_yb, y, yb, N*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy_b(d_x, d_xb, x, xb, N*sizeof(float), cudaMemcpyHostToDevice);
    cudaFree(d_y);
    cudaFree(d_yb);
    cudaFree(d_x);
    cudaFree(d_xb);
}

//  Differentiation of main as a context to call adjoint code (with options context):
main() {
    int N = 1 << 20;
    float a, *x, *y;
    float ab, *xb, *yb;
    float y1;
    int ii1;
    a = 2.0f;
    xb = (float *)malloc(N*sizeof(float));
    for (ii1 = 0; ii1 < N; ++ii1)
        xb[ii1] = 0.0;
    x = (float *)malloc(N*sizeof(float));
    yb = (float *)malloc(N*sizeof(float));
    for (ii1 = 0; ii1 < N; ++ii1)
        yb[ii1] = 0.0;
    y = (float *)malloc(N*sizeof(float));
    for (int i = 0; i < N; i++) {
        x[i] = 1.0f;
        y[i] = 2.0f;
    }
    adContextAdj_init(0.87);
    if (x)
        adContextAdj_initReal4Array("x", x, xb, N);
    if (y)
        adContextAdj_initReal4Array("y", y, yb, N);
    adContextAdj_initReal4("a", &a, &ab);
    foo_b(N, a, &ab, x, xb, y, yb);
    adContextAdj_startConclude();
    if (x)
        adContextAdj_concludeReal4Array("x", *x, *xb, N);
    if (y)
        adContextAdj_concludeReal4Array("y", *y, *yb, N);
    adContextAdj_concludeReal4("a", a, ab);
    adContextAdj_conclude();
    float maxError = 0.0f;
    for (int i = 0; i < N; i++) {
        if (y[i] - 4.0f >= 0.)
            y1 = y[i] - 4.0f;
        else
            y1 = -(y[i]-4.0f);
        if (maxError < y1)
            maxError = y1;
        else
            maxError = maxError;
    }
    printf("Max error: %f\n", maxError);
    free(xb);
    free(x);
    free(yb);
    free(y);
}
