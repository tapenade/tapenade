//        Generated by TAPENADE     (INRIA, Ecuador team)
//  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
//
#include <stdio.h>
#include <stdlib.h>

/* First experiments with differentiation of memory layouts */
__global__ void subfunc(float a1, float *pv) {
    pv[0] = pv[0] + a1*pv[1];
}

__global__ void func(int n, float a, float *x, float *y) {
    // a, x, *x, y, *y are shared for all threads,
    // i, pv[] are local/private to the thread.
    //     x points to [alloc*d_x in foo] or *x
    //     y points to [alloc*d_y in foo] or *y
    //     pv points to pv[0:2-1]
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    float pv[2];
    pv[0] = x[i + 5] + x[i + 3];
    pv[1] = y[i];
    subfunc(a, pv);
    if (i < n - 5)
        y[i] = pv[0];
}

__host__ void foo(int N, float a, float *x, float *y) {
    //     x points to [alloc*x in main] or *x
    //     y points to [alloc*y in main] or *y
    //     d_x points to Undef
    //     d_y points to Undef
    __device__ float *d_x, *d_y;
    cudaMalloc(&d_x, N*sizeof(float));
    //     d_x points to [alloc*d_x in foo]
    cudaMalloc(&d_y, N*sizeof(float));
    //     d_y points to [alloc*d_y in foo]
    cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);
    func<<<(N+255)/256, 256>>>(N, a, d_x, d_y);
    cudaMemcpy(y, d_y, N*sizeof(float), cudaMemcpyDeviceToHost);
    cudaFree(d_x);
    //     d_x points to Undef
    cudaFree(d_y);
    //     d_y points to Undef
}

main() {
    //     x points to Undef
    //     y points to Undef
    int N = 1 << 20;
    float a, *x, *y;
    float y1;
    a = 2.0f;
    x = (float *)malloc(N*sizeof(float));
    //     x points to [alloc*x in main]
    y = (float *)malloc(N*sizeof(float));
    //     y points to [alloc*y in main]
    for (int i = 0; i < N; i++) {
        x[i] = 1.0f;
        y[i] = 2.0f;
    }
    foo(N, a, x, y);
    float maxError = 0.0f;
    for (int i = 0; i < N; i++) {
        if (y[i] - 4.0f >= 0.)
            y1 = y[i] - 4.0f;
        else
            y1 = -(y[i]-4.0f);
        if (maxError < y1)
            maxError = y1;
        else
            maxError = maxError;
    }
    printf("Max error: %f\n", maxError);
    free(x);
    //     x points to Undef
    free(y);
    //     y points to Undef
}
