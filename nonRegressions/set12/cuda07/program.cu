#include <stdio.h>
#include <stdlib.h>

/* First experiments with differentiation of memory layouts */

__global__ void subfunc(float a1, float *pv) {
        pv[0] = pv[0] + a1*pv[1] ;
}


__global__ void func(int n, float a, float *x, float *y)
{
  // a, x, *x, y, *y are shared for all threads,
  // i, pv[] are local/private to the thread.
  int i = blockIdx.x*blockDim.x + threadIdx.x;
  float pv[2] ;
  pv[0] = x[i+5] + x[i+3];
  pv[1] = y[i] ;
  subfunc(a,pv) ;
  if (i < n-5) y[i] = pv[0] ;
}

__host__
void foo(int N, float a, float *x, float *y) {
  __device__ float *d_x, *d_y;
  cudaMalloc(&d_x, N*sizeof(float)); 
  cudaMalloc(&d_y, N*sizeof(float));
  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);

  func<<<(N+255)/256, 256>>>(N, a, d_x, d_y);

  cudaMemcpy(y, d_y, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaFree(d_x);
  cudaFree(d_y);
}

int main(void)
{
  int N = 1<<20;
  float a, *x, *y;
  a = 2.0f ;
  x = (float*)malloc(N*sizeof(float));
  y = (float*)malloc(N*sizeof(float));

  for (int i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  foo(N, a, x, y);

  float maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = max(maxError, abs(y[i]-4.0f));
  printf("Max error: %f\n", maxError);

  free(x);
  free(y);
}
