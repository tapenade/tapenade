!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_complexStepOnera) - 18 Mar 2022 11:37
!
! A silly code to test the -complexStep mode
! First, without -complexStep
MODULE MODTYPE_DIFF
  IMPLICIT NONE
  TYPE MYTYPE
      REAL :: fld1, fld2
      INTEGER :: fld3
  END TYPE MYTYPE
  TYPE MYTYPE_DIFF
      REAL :: fld1
  END TYPE MYTYPE_DIFF
END MODULE MODTYPE_DIFF

MODULE MOD1_DIFF
  USE MODTYPE_DIFF
  IMPLICIT NONE
  REAL :: glob1
  REAL*8 :: glob2
  TYPE(MYTYPE) :: glob3
  TYPE(MYTYPE_DIFF) :: glob3d

CONTAINS
!  Differentiation of sub1 in forward (tangent) mode (with options context no!StripPrimalCode):
!   variations   of useful results: y
!   with respect to varying inputs: x
  SUBROUTINE SUB1_D(x, xd, y, yd, z)
    IMPLICIT NONE
    REAL*8 :: x, y
    REAL*8 :: xd, yd
    REAL :: z
    INTRINSIC SIN
    glob1 = y
    glob2 = x
    yd = COS(x)*xd
    y = SIN(x)
    z = 3*z + 2
  END SUBROUTINE SUB1_D

  SUBROUTINE SUB1(x, y, z)
    IMPLICIT NONE
    REAL*8 :: x, y
    REAL :: z
    INTRINSIC SIN
    glob1 = y
    glob2 = x
    y = SIN(x)
    z = 3*z + 2
  END SUBROUTINE SUB1

  SUBROUTINE SUB2(x, y)
    IMPLICIT NONE
    REAL*8 :: x, y
    x = x/y
  END SUBROUTINE SUB2

!  Differentiation of sub3 in forward (tangent) mode (with options context no!StripPrimalCode):
!   variations   of useful results: vv
!   with respect to varying inputs: glob3.fld1 vv
  SUBROUTINE SUB3_D(vv, vvd)
    IMPLICIT NONE
    REAL :: vv
    REAL :: vvd
    vvd = vvd/2 + glob3d%fld1
    vv = vv/2 + glob3%fld1
  END SUBROUTINE SUB3_D

  SUBROUTINE SUB3(vv)
    IMPLICIT NONE
    REAL :: vv
    vv = vv/2 + glob3%fld1
  END SUBROUTINE SUB3

END MODULE MOD1_DIFF

!  Differentiation of main as a context to call tangent code (with options context no!StripPrimalCode):
PROGRAM MAIN_D
  IMPLICIT NONE
  REAL :: u, w, res
  REAL :: ud, resd
  REAL*8 :: v
  REAL*8 :: vd
  REAL :: FF2_NODIFF
  REAL :: FF1_NODIFF
  REAL :: FF1_D
  u = 2.5
  v = 3.2d0
  v = FF2_NODIFF(u, v)
  w = u*u
  CALL ADCONTEXTTGT_INIT(1.e-4_8, 0.87_8)
  CALL ADCONTEXTTGT_INITREAL4('u'//CHAR(0), u, ud)
  CALL ADCONTEXTTGT_INITREAL8('v'//CHAR(0), v, vd)
  resd = FF1_D(u, ud, v, vd, w, res)
  CALL ADCONTEXTTGT_STARTCONCLUDE()
  CALL ADCONTEXTTGT_CONCLUDEREAL4('res'//CHAR(0), res, resd)
  CALL ADCONTEXTTGT_CONCLUDEREAL8('v'//CHAR(0), v, vd)
  CALL ADCONTEXTTGT_CONCLUDE()
  res = res*w
  PRINT*, 'res=', res
END PROGRAM MAIN_D

!  Differentiation of ff1 in forward (tangent) mode (with options context no!StripPrimalCode):
!   variations   of useful results: ff1 b
!   with respect to varying inputs: a b
!   RW status of diff variables: glob3.fld1:(loc) ff1:out a:in
!                b:in-out
REAL FUNCTION FF1_D(a, ad, b, bd, c, ff1)
  USE MOD1_DIFF
  IMPLICIT NONE
  REAL :: a, c
  REAL :: ad
  REAL*8 :: b
  REAL*8 :: bd
  INTRINSIC INT
  INTRINSIC MAX
  INTRINSIC SIN
  REAL :: ff1
  glob3d%fld1 = ad
  glob3%fld1 = a
  glob3%fld2 = b
  glob3%fld3 = INT(a)
  IF (a*b .LT. 10.0) THEN
    ff1 = 10.0
    ff1_d = 0.0
  ELSE
    ff1_d = b*ad + a*bd
    ff1 = a*b
  END IF
  c = SIN(c)
  CALL SUB1_D(a + 2.d0, DBLE(ad), b, bd, c)
  CALL SUB3_D(ff1, ff1_d)
END FUNCTION FF1_D

SUBROUTINE MAIN_NODIFF()
  IMPLICIT NONE
  REAL :: u, w, res
  REAL*8 :: v
  REAL :: FF2_NODIFF
  REAL :: FF1_NODIFF
  u = 2.5
  v = 3.2d0
  v = FF2_NODIFF(u, v)
  w = u*u
  res = FF1_NODIFF(u, v, w)
  res = res*w
  PRINT*, 'res=', res
END SUBROUTINE MAIN_NODIFF

REAL FUNCTION FF1_NODIFF(a, b, c) RESULT (ff1)
  USE MOD1_DIFF
  IMPLICIT NONE
  REAL :: a, c
  REAL*8 :: b
  INTRINSIC INT
  INTRINSIC MAX
  INTRINSIC SIN
  glob3%fld1 = a
  glob3%fld2 = b
  glob3%fld3 = INT(a)
  IF (a*b .LT. 10.0) THEN
    ff1 = 10.0
  ELSE
    ff1 = a*b
  END IF
  c = SIN(c)
  CALL SUB1(a + 2.d0, b, c)
  CALL SUB3(ff1)
END FUNCTION FF1_NODIFF

REAL FUNCTION FF2_NODIFF(a, b) RESULT (ff2)
  IMPLICIT NONE
  REAL :: a
  REAL*8 :: b
  ff2 = b*b + 2.d0*a*a
END FUNCTION FF2_NODIFF

