! A silly code to test the -complexStep mode
! First, without -complexStep

MODULE modtype
  TYPE :: mytype
     REAL :: fld1,fld2
     INTEGER :: fld3
  END TYPE mytype
END MODULE modtype

MODULE mod1
  USE modtype
  REAL :: glob1
  REAL*8 :: glob2
  TYPE(mytype) :: glob3
CONTAINS

  SUBROUTINE sub1(x,y,z)
    REAL*8 :: x,y
    REAL :: z
    glob1 = y
    glob2 = x
    y = SIN(x)
    z = 3*z + 2
  END SUBROUTINE sub1

  SUBROUTINE sub2(x,y)
    REAL*8 :: x,y
    x = x/y
  END SUBROUTINE sub2

  SUBROUTINE sub3(vv)
    REAL :: vv
    vv = vv/2 + glob3%fld1
  END SUBROUTINE sub3


END MODULE mod1

REAL FUNCTION ff1(a,b,c)
  USE mod1
  REAL :: a,c
  REAL*8 :: b
  glob3%fld1 = a
  glob3%fld2 = b
  glob3%fld3 = INT(a)
  ff1 = MAX(a*b, 10.0)
  c = SIN(c)
  CALL sub1(a+2.d0,b,c)
  CALL sub3(ff1)
END FUNCTION ff1

REAL FUNCTION ff2(a,b)
  REAL :: a
  REAL*8 :: b
  ff2 = b*b+2.d0*a*a
END FUNCTION ff2

PROGRAM main
  REAL :: u,w,res
  REAL*8 :: v
  u = 2.5
  v = 3.2d0
  v = ff2(u,v)
  w = u*u
  res = ff1(u,v,w)
  res = res*w
  print *,'res=',res
END PROGRAM main
