! Another example of vector mode bug on initializations of diff array
! Again, bug related to a F77 file(.f) containing F90 features (_dv.f90)
      subroutine intvertex(nCv,nVx,mpg,vxvol,centre,vertex)
      use b2us_map
      implicit none
      integer nCv,nVx
      type (mapping), intent (in) :: mpg
      real (kind=8) ::
     *   vxvol(mpg%nVmxCv),centre(nCv),vertex(nVx)
  
      integer iVx,iCv
      real (kind=8) :: volsum

      vertex = 0.0_8
      do iVx = 1, nVx
        volsum = 0.0_8
        do iCv = 1,mpg%vxCvP(iVx,2)
          volsum = volsum + 1._8/vxvol(mpg%vxCvP(iVx,1) + iCv - 1)
          vertex(iVx) = vertex(iVx) + 
     &      centre(mpg%vxCv(mpg%vxCvP(iVx,1) + iCv - 1))/
     &      vxvol(mpg%vxCvP(iVx,1) + iCv - 1)
        enddo
        vertex(iVx) = vertex(iVx)/(volsum + 1.0e-30_8)
      enddo

      return
      end
