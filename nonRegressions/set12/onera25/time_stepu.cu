// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __computetimestepu_gpu(const int                  n_cell,
                                       const int                  n_cellt,
                                       const double               gam,
                                       const double               rgaz,
                                       const double               cfl,
                                       const double* __restrict__ celldim,
                                       const double* __restrict__ rho,
                                       const double* __restrict__ velo,
                                       const double* __restrict__ temp,
                                             double* __restrict__ timestep)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_cell){
    double velocell = sqrt(  velo[tx           ]*velo[tx           ]
                           + velo[tx+  n_cellt]*velo[tx+  n_cellt]
                           + velo[tx+2*n_cellt]*velo[tx+2*n_cellt]);
    double sound   = sqrt( gam * rgaz * temp[tx] );

    timestep[tx] = cfl * celldim[tx] / (velocell + sound );

    if (tx == 0) {
      printf("__computetimestepu_gpu n_cellt=%i, n_cell=%i\n\n", n_cellt, n_cell);
    }
  };
}


// ===========================================================================
__host__ void computetimestepu_gpu(const int&    n_cell,
                                   const int&    n_cellt,
                                   const double& gam,
                                   const double& rgaz,
                                   const double& cfl,
                                   const double* celldim,
                                   const double* rho,
                                   const double* velo,
                                   const double* temp,
                                         double* timestep)
{
  printf("computetimestepu_gpu %i / %i \n", n_cell, n_cellt);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoCellSdom = dim3(nThrs       ,1,1);
  dim3 blkTopoCellSdom = dim3(n_cell/nThrs+1,1,1);

  // First Step
  __computetimestepu_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cell,
                                                               n_cellt,
                                                               gam,
                                                               rgaz,
                                                               cfl,
                                                               celldim,
                                                               rho,
                                                               velo,
                                                               temp,
                                                               timestep);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("Error: %s\n", cudaGetErrorString(err));
  }
  // checkCuda( cudaDeviceSynchronize() );
  // cudaError_t err2 = cudaGetLastError();
  // if (err2 != cudaSuccess){
  //   printf("Error: %s\n", cudaGetErrorString(err2));
  // }
  // checkCuda( cudaDeviceSynchronize() );
  // printf("computetimestepu_gpu end %i \n", n_cellt);

  // int bytes = n_vtx * sizeof(double);
  // std::vector<double> array(n_vtx);
  // checkCuda( cudaMemcpy(array.data(), a, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < n_vtx; ++i){
  //   printf("cpu array[%i] = [%f] \n", i, array[i]);
  // }
}
