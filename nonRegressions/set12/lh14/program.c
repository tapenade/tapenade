/* Bug of the C parser found by Max Sagebaum
 * " const* restrict const " does not parse. */

double func1(double const *const x) {
  double * volatile dd0 ;
  double const * volatile dd1 ;
  double * volatile restrict dd2 ;
  double * const volatile restrict dd3 ;
  return x[0] + x[1];
}

double func2(double *restrict x) {
  return x[0] + x[1];
}

double func3(double const *restrict const x) {
  return x[0] + x[1];
}

double entry(double a, double b) {
  double x[2];
  x[0] = a;
  x[1] = b;
  double r = func1(x) + func2(x) + func3(x);

  return r;
}

int main() {
  double a,b,r ;
  a = 2.5 ;
  b = 7.5 ;
  r = entry(a,b) ;
//  printf("result:%f\n",r) ;
}
