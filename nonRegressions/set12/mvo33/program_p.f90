!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 12:15
!
MODULE TYPE_MODULE
  IMPLICIT NONE
  TYPE TEST_TYPE
      PROCEDURE(FUN_INTERFACE), NOPASS, POINTER :: fun_ptr
      PROCEDURE(SUB_INTERFACE), NOPASS, POINTER :: sub_ptr
  END TYPE TEST_TYPE
  ABSTRACT INTERFACE 
      FUNCTION FUN_INTERFACE(n, x) RESULT (f)
        INTEGER, INTENT(IN) :: n
        REAL, INTENT(IN) :: x(n)
        REAL :: f(n)
      END FUNCTION FUN_INTERFACE
      SUBROUTINE SUB_INTERFACE(n, x, f)
        INTEGER, INTENT(IN) :: n
        REAL, INTENT(IN) :: x(n)
        REAL :: f(n)
      END SUBROUTINE SUB_INTERFACE
  END INTERFACE


CONTAINS
  SUBROUTINE TYPE_SET_FUNC(test, FUN)
    IMPLICIT NONE
!     test.fun_ptr points to *(test.fun_ptr) or Undef
!     test.sub_ptr points to *(test.sub_ptr) or Undef
    INTERFACE 
        FUNCTION FUN(n, x) RESULT (f)
          INTEGER, INTENT(IN) :: n
          REAL, INTENT(IN) :: x(n)
          REAL :: f(n)
        END FUNCTION FUN
    END INTERFACE

    TYPE(TEST_TYPE) :: test
    test%fun_ptr => fun
!     test.fun_ptr points to Undef
  END SUBROUTINE TYPE_SET_FUNC

END MODULE TYPE_MODULE

MODULE FUNCS
  IMPLICIT NONE

CONTAINS
  FUNCTION FUN2(n, x) RESULT (f)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n
    REAL, INTENT(IN) :: x(n)
    REAL :: f(n)
    f = 2.0*x
  END FUNCTION FUN2

  FUNCTION FUN3(n, x) RESULT (f)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n
    REAL, INTENT(IN) :: x(n)
    REAL :: f(n)
    f = 3.0*x
  END FUNCTION FUN3

  SUBROUTINE SUB2(n, x, f)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n
    REAL, INTENT(IN) :: x(n)
    REAL :: f(n)
    f = 2.0*x
  END SUBROUTINE SUB2

END MODULE FUNCS

PROGRAM MAIN
!     test.fun_ptr points to Undef
!     test.sub_ptr points to Undef
  USE TYPE_MODULE
  USE FUNCS
  IMPLICIT NONE
  TYPE(TEST_TYPE) :: test
  INTEGER, PARAMETER :: n=2
  REAL :: x(n), f(n)
  x = (/1., 1./)
  CALL TYPE_SET_FUNC(test, fun2)
  f = test%fun_ptr(n, x)
  PRINT*, ' f from  fun2 ', f
  CALL TYPE_SET_FUNC(test, fun3)
  f = test%fun_ptr(n, x)
  PRINT*, ' f from call fun3 ', f
END PROGRAM MAIN

