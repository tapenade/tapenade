module type_module
  implicit none

  type test_type
    procedure(fun_interface), nopass, pointer :: fun_ptr
    procedure(sub_interface), nopass, pointer :: sub_ptr    
  end type test_type

  abstract interface
    function fun_interface(n,x) result(f)
      integer, intent(in) :: n
      real, intent(in) :: x(n)
      real :: f(n)
    end function fun_interface
    subroutine  sub_interface(n,x,f)
      integer, intent(in) :: n
      real, intent(in) :: x(n)
      real :: f(n)
    end subroutine sub_interface
  end interface  

  contains

  subroutine  type_set_func(test, fun)    
    interface
      function fun(n,x) result(f)
        integer, intent(in) :: n
        real, intent(in) :: x(n)
        real :: f(n)
      end function fun
    end interface  

    type(test_type) :: test
    test%fun_ptr => fun;
  end subroutine type_set_func
end module type_module
 
module funcs
  implicit none

  contains

  function fun2 (n,x ) result (f )
    integer, intent(in) :: n
    real, intent(in) :: x(n)
    real :: f(n)
    f = 2.0*x ;
  end function fun2

  function fun3 (n,x ) result (f )
    integer, intent(in) :: n
    real, intent(in) :: x(n)
    real :: f(n)
    f = 3.0*x ;
  end function fun3

  subroutine sub2(n,x,f)
    integer, intent(in) :: n
    real, intent(in) :: x(n)
    real :: f(n)
    f = 2.0*x ;
  end subroutine sub2
end module funcs

program main
  use type_module
  use funcs
  type(test_type) :: test
  integer, parameter :: n = 2
  real :: x(n), f(n)

  x = (/1., 1./)
  
  call type_set_func (test, fun2)
  f = test%fun_ptr(n, x)
  print *, " f from  fun2 ", f;

  call type_set_func (test, fun3)
  f = test%fun_ptr(n,x);
  print *, " f from call fun3 ", f;
end program
