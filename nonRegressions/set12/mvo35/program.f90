module mod_type1
  implicit none

  type :: type1
     real :: val1
     procedure(type1interface), pointer :: point1
  end type type1

  interface
     subroutine type1interface(self, x, y)
       import type1
       implicit none
       class(type1) :: self
       real, intent(in) :: x
       real, intent(out) :: y

     end subroutine type1interface
  end interface

  type, extends(type1) :: type2
     real :: val2
     procedure(type2interface), pointer :: point2 
  end type type2

  interface
     subroutine type2interface(self, x)
       import type2
       implicit none
       class(type2) :: self
       real, intent(in) :: x

     end subroutine type2interface
  end interface

contains

  subroutine type1subroutine(self, x, y)
    implicit none
    class(type1) :: self
    real, intent(in) :: x
    real, intent(out) :: y
    
    write(*,*) 'type1subroutine called with x=',x, 'val1=',self%val1
    y = self%val1 * x

  end subroutine type1subroutine

  subroutine type2subroutine(self, x)
    implicit none
    class(type2) :: self
    real, intent(in) :: x

    write(*,*) 'type2subroutine called with x=',x, &
         'initial fields: val1=', self%val1, 'val2=', self%val2
    self%val1 = self%val2 + x
    write(*,*) '...type2subroutine done, val1=', self%val1

  end subroutine type2subroutine

end module mod_type1

program Main
  use mod_type1 
  implicit none

  type(type1), target :: mytype1
  type(type2), target :: mytype2
  class(type1), pointer :: typ_ptr
  real :: x, y

  x = 2.

  !-- init mytype1
  mytype1%val1 = 1.
  mytype1%point1 => type1subroutine

  !-- init mytype2
  mytype2%val1 = 1.
  mytype2%val2 = 2.
  mytype2%point1 => type1subroutine
  mytype2%point2 => type2subroutine

  typ_ptr => mytype1
  call foo(typ_ptr)
  typ_ptr => mytype2
  call foo(typ_ptr)

contains

  subroutine foo(t)
    implicit none
    class(type1) :: t
    select type (t)
    class is (type1)
       print*, '@type1 now'
       call t%point1(x, y)
       print*, '...yields y=',y
    class is (type2)
       print*, '@type2 now'
       call t%point2(x)
       call t%point1(x,y)
       print*, '...yields y=',y
    end select

  end subroutine foo

end program Main
