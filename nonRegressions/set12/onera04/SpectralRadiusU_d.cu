//        Generated by TAPENADE     (INRIA, Ecuador team)
//  Tapenade 3.16 (feature_cuda) - 30 Sep 2021 12:35
//
// ------------------------------------------------------------------
// External include
// ------------------------------------------------------------------
//#include <iostream>
// ------------------------------------------------------------------
// Internal include
//#include "tmo/cuda_utilities.hpp"
// llh patch:
#include "cuda_utilities.h"

//  Differentiation of __spectralradiusidealgasbnd5equ_gpu in forward (tangent) mode:
//   variations   of useful results: *spectralradius
//   with respect to varying inputs: *surf *wb1 *spectralradius
//                gam
//   Plus diff mem management of: surf:in wb1:in spectralradius:in
// ------------------------------------------------------------------
// ===========================================================================
__global__ void __spectralradiusidealgasbnd5equ_gpu_d(const int nCelTot, const
        int nFace, const int nFaceBnd, const double gam, const double gamd, 
        int *__restrict__ indicf, double *__restrict__ surf, double *
        __restrict__ surfd, double *__restrict__ wb1, double *__restrict__ 
        wb1d, double *__restrict__ spectralradius, double *__restrict__ 
        spectralradiusd) {
    int tx = threadIdx.x + blockIdx.x*blockDim.x;
    double gam1 = gam - 1.;
    double gam1d = gamd;
    if (tx < nFaceBnd) {
        int iface = indicf[tx];
        // int linc  = indicc[tx];
        double tnx = surf[iface];
        double tnxd = surfd[iface];
        double tny = surf[iface + nFace];
        double tnyd = surfd[iface + nFace];
        double tnz = surf[iface + 2*nFace];
        double tnzd = surfd[iface + 2*nFace];
        double tnn;
        double tnnd;
        double abs0;
        double abs0d;
        double abs1;
        double abs1d;
        double temp;
        tnnd = norm3d_d(tnx, tnxd, tny, tnyd, tnz, tnzd, &tnn);
        double ro = wb1[tx];
        double rod = wb1d[tx];
        double rou = wb1[tx + nFaceBnd];
        double roud = wb1d[tx + nFaceBnd];
        double rov = wb1[tx + 2*nFaceBnd];
        double rovd = wb1d[tx + 2*nFaceBnd];
        double row = wb1[tx + 3*nFaceBnd];
        double rowd = wb1d[tx + 3*nFaceBnd];
        double roe = wb1[tx + 4*nFaceBnd];
        double roed = wb1d[tx + 4*nFaceBnd];
        double ovro = 1./ro;
        double ovrod = -(rod/(ro*ro));
        double uu = rou*ovro;
        double uud = ovro*roud + rou*ovrod;
        double vv = rov*ovro;
        double vvd = ovro*rovd + rov*ovrod;
        double ww = row*ovro;
        double wwd = ovro*rowd + row*ovrod;
        double ee = 0.5*(uu*uu+vv*vv+ww*ww);
        double eed = 0.5*(2*uu*uud+2*vv*vvd+2*ww*wwd);
        double c2 = gam*gam1*(roe*ovro-ee);
        double c2d = (roe*ovro-ee)*(gam1*gamd+gam*gam1d) + gam*gam1*(ovro*roed
        +roe*ovrod-eed);
        double x1;
        double x1d;
        if (c2 >= 0.) {
            abs0d = c2d;
            abs0 = c2;
        } else {
            abs0d = -c2d;
            abs0 = -c2;
        }
        double cc;
        double ccd;
        temp = sqrt(abs0);
        ccd = (abs0 == 0.0 ? 0.0 : abs0d/(2.0*temp));
        cc = temp;
        x1d = tnx*uud + uu*tnxd + tny*vvd + vv*tnyd + tnz*wwd + ww*tnzd;
        x1 = uu*tnx + vv*tny + ww*tnz;
        if (x1 >= 0.) {
            abs1d = x1d;
            abs1 = x1;
        } else {
            abs1d = -x1d;
            abs1 = -x1;
        }
        spectralradiusd[iface] = abs1d + tnn*ccd + cc*tnnd;
        spectralradius[iface] = abs1 + cc*tnn;
    }
}

//  Differentiation of spectralradiusidealgasbnd5equ_gpu in forward (tangent) mode:
//   variations   of useful results: *spectralradius
//   with respect to varying inputs: *surf *wb1 *spectralradius
//                gam
//   RW status of diff variables: surf:(loc) *surf:in wb1:(loc)
//                *wb1:in spectralradius:(loc) *spectralradius:in-out
//                gam:in
//   Plus diff mem management of: surf:in wb1:in spectralradius:in
// ===========================================================================
__host__ void spectralradiusidealgasbnd5equ_gpu_d(int &nCelTot, int &nFace, 
        int &nFaceBnd, double &gam, double &gamd, int *pointList, double *surf
        , double *surfd, double *wb1, double *wb1d, double *spectralradius, 
        double *spectralradiusd) {
    // Target --> Feed Block and Threads
    // printf("computefluxbndu_gpu %i \n", nFace);
    // printf("computefluxbndu_gpu %i \n", nFaceBnd);
    int nThrs = 64;
    dim3 thrTopoFaceBndSdom;
    thrTopoFaceBndSdom = dim3(nThrs, 1, 1);
    dim3 blkTopoFaceBndSdom;
    blkTopoFaceBndSdom = dim3(nFaceBnd/nThrs + 1, 1, 1);
    __spectralradiusidealgasbnd5equ_gpu_d<<<blkTopoFaceBndSdom, 
                                         thrTopoFaceBndSdom>>>(nCelTot, nFace,
                                          nFaceBnd, gam, gamd, pointList, surf
                                          , surfd, wb1, wb1d, spectralradius, 
                                          spectralradiusd);
}

//  Differentiation of __spectralradiusidealgas5equ_gpu in forward (tangent) mode:
//   variations   of useful results: *spectralradius
//   with respect to varying inputs: *temp *surf *velo rgaz *rho
//                *spectralradius gam
//   Plus diff mem management of: temp:in surf:in velo:in rho:in
//                spectralradius:in
// ===========================================================================
__global__ void __spectralradiusidealgas5equ_gpu_d(const int nCelTot, const 
        int nFace, const double gam, const double gamd, const double rgaz, 
        const double rgazd, double *__restrict__ rho, double *__restrict__ 
        rhod, double *__restrict__ velo, double *__restrict__ velod, double *
        __restrict__ temp, double *__restrict__ tempd, double *__restrict__ 
        surf, double *__restrict__ surfd, double *__restrict__ spectralradius,
        double *__restrict__ spectralradiusd, int *__restrict__ FaceCell) {
    int tx = threadIdx.x + blockIdx.x*blockDim.x;
    double gam1 = gam - 1.;
    double gam1d = gamd;
    double gam1_1 = 1./gam1;
    double gam1_1d = -(gam1d/(gam1*gam1));
    if (tx < nFace) {
        // printf("tx %i [%i] --> [%i] [%f] \n", tx, nFace, FaceVtxIdx[tx], coords[4] );
        int il = FaceCell[tx];
        int ir = FaceCell[tx + nFace];
        double tnx = surf[tx];
        double tnxd = surfd[tx];
        double tny = surf[tx + nFace];
        double tnyd = surfd[tx + nFace];
        double tnz = surf[tx + 2*nFace];
        double tnzd = surfd[tx + 2*nFace];
        double tnn;
        double tnnd;
        double abs0;
        double abs0d;
        double arg1;
        double arg1d;
        double temp0;
        tnnd = norm3d_d(tnx, tnxd, tny, tnyd, tnz, tnzd, &tnn);
        double vxl = velo[il];
        double vxld = velod[il];
        double vxr = velo[ir];
        double vxrd = velod[ir];
        double vyl = velo[il + nCelTot];
        double vyld = velod[il + nCelTot];
        double vyr = velo[ir + nCelTot];
        double vyrd = velod[ir + nCelTot];
        double vzl = velo[il + 2*nCelTot];
        double vzld = velod[il + 2*nCelTot];
        double vzr = velo[ir + 2*nCelTot];
        double vzrd = velod[ir + 2*nCelTot];
        double hm = gam*gam1_1*temp[il]*rgaz + 0.5*(vxl*vxl+vyl*vyl+vzl*vzl);
        double hmd = temp[il]*rgaz*(gam1_1*gamd+gam*gam1_1d) + gam*gam1_1*(
        rgaz*tempd[il]+temp[il]*rgazd) + 0.5*(2*vxl*vxld+2*vyl*vyld+2*vzl*vzld
        );
        double hp = gam*gam1_1*temp[ir]*rgaz + 0.5*(vxr*vxr+vyr*vyr+vzr*vzr);
        double hpd = temp[ir]*rgaz*(gam1_1*gamd+gam*gam1_1d) + gam*gam1_1*(
        rgaz*tempd[ir]+temp[ir]*rgazd) + 0.5*(2*vxr*vxrd+2*vyr*vyrd+2*vzr*vzrd
        );
        temp0 = 1.0/rho[il];
        double w1mi = temp0;
        double w1mid = -(temp0*rhod[il]/rho[il]);
        double r;
        double rd;
        temp0 = sqrt(rho[ir]*w1mi);
        rd = (rho[ir]*w1mi == 0.0 ? 0.0 : (w1mi*rhod[ir]+rho[ir]*w1mid)/(2.0*
            temp0));
        r = temp0;
        double rr = 1./(1.+r);
        double rrd = -(rd/((r+1.)*(r+1.)));
        double uu = (vxr*r+vxl)*rr;
        double uud = rr*(r*vxrd+vxr*rd+vxld) + (vxr*r+vxl)*rrd;
        double vv = (vyr*r+vyl)*rr;
        double vvd = rr*(r*vyrd+vyr*rd+vyld) + (vyr*r+vyl)*rrd;
        double ww = (vzr*r+vzl)*rr;
        double wwd = rr*(r*vzrd+vzr*rd+vzld) + (vzr*r+vzl)*rrd;
        double ee = 0.5*(uu*uu+vv*vv+ww*ww);
        double eed = 0.5*(2*uu*uud+2*vv*vvd+2*ww*wwd);
        double hh = (hp*r+hm)*rr;
        double hhd = rr*(r*hpd+hp*rd+hmd) + (hp*r+hm)*rrd;
        double cc;
        double ccd;
        arg1d = (hh-ee)*gam1d + gam1*(hhd-eed);
        arg1 = gam1*(hh-ee);
        temp0 = sqrt(arg1);
        ccd = (arg1 == 0.0 ? 0.0 : arg1d/(2.0*temp0));
        cc = temp0;
        double x1;
        double x1d;
        x1d = tnx*uud + uu*tnxd + tny*vvd + vv*tnyd + tnz*wwd + ww*tnzd;
        x1 = uu*tnx + vv*tny + ww*tnz;
        if (x1 >= 0.) {
            abs0d = x1d;
            abs0 = x1;
        } else {
            abs0d = -x1d;
            abs0 = -x1;
        }
        spectralradiusd[tx] = abs0d + tnn*ccd + cc*tnnd;
        spectralradius[tx] = abs0 + cc*tnn;
    }
}

//  Differentiation of spectralradiusidealgas5equ_gpu in forward (tangent) mode:
//   variations   of useful results: *spectralradius
//   with respect to varying inputs: *temp *surf *velo rgaz *rho
//                *spectralradius gam
//   RW status of diff variables: temp:(loc) *temp:in surf:(loc)
//                *surf:in velo:(loc) *velo:in rgaz:in rho:(loc)
//                *rho:in spectralradius:(loc) *spectralradius:in-out
//                gam:in
//   Plus diff mem management of: temp:in surf:in velo:in rho:in
//                spectralradius:in
// ===========================================================================
__host__ void spectralradiusidealgas5equ_gpu_d(int &nCelTot, int &nFace, 
        double &gam, double &gamd, double &rgaz, double &rgazd, int *FaceCell,
        double *rho, double *rhod, double *velo, double *velod, double *temp, 
        double *tempd, double *surf, double *surfd, double *spectralradius, 
        double *spectralradiusd) {
    int nThrs = 64;
    int nBlks = nFace/nThrs + 1;
    dim3 thrTopoSdom;
    thrTopoSdom = dim3(nThrs, 1, 1);
    dim3 blkTopoSdom;
    blkTopoSdom = dim3(nFace/nThrs + 1, 1, 1);
    __spectralradiusidealgas5equ_gpu_d<<<blkTopoSdom, thrTopoSdom>>>(nCelTot, 
                                       nFace, gam, gamd, rgaz, rgazd, rho, 
                                       rhod, velo, velod, temp, tempd, surf, 
                                       surfd, spectralradius, spectralradiusd,
                                       FaceCell);
}
