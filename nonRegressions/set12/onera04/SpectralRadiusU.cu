// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
//#include "tmo/cuda_utilities.hpp"
// llh patch:
#include "../../cuda_utilities.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __spectralradiusidealgasbnd5equ_gpu(const int nCelTot,
                                                    const int nFace,
                                                    const int nFaceBnd,
                                                    const double gam,
                                                    int*    __restrict__ indicf,
                                                    double* __restrict__ surf,
                                                    double* __restrict__ wb1,
                                                    double* __restrict__ spectralradius)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  double gam1  = gam - 1.;
  if( tx < nFaceBnd){

    int iface = indicf[tx];
    // int linc  = indicc[tx];

    double tnx = surf[iface        ];
    double tny = surf[iface+  nFace];
    double tnz = surf[iface+2*nFace];
    double tnn = norm3d(tnx, tny, tnz);

    double  ro  = wb1[tx           ];
    double  rou = wb1[tx+  nFaceBnd];
    double  rov = wb1[tx+2*nFaceBnd];
    double  row = wb1[tx+3*nFaceBnd];
    double  roe = wb1[tx+4*nFaceBnd];

    double ovro = 1./ro;
    double uu   = rou * ovro;
    double vv   = rov * ovro;
    double ww   = row * ovro;
    double ee   = 0.5 * ( uu*uu + vv*vv + ww*ww );
    double c2   = gam * gam1 * ( roe * ovro - ee);
    double cc   = sqrt(abs(c2));

    spectralradius[iface] = abs( uu * tnx + vv * tny + ww * tnz ) + cc * tnn;

  };

}

// ===========================================================================
__host__ void spectralradiusidealgasbnd5equ_gpu(const int&    nCelTot,
                                                const int&    nFace,
                                                const int&    nFaceBnd,
                                                const double& gam,
                                                int*          pointList,
                                                double*       surf,
                                                double*       wb1,
                                                double*       spectralradius)
{
  // Target --> Feed Block and Threads
  // printf("computefluxbndu_gpu %i \n", nFace);
  // printf("computefluxbndu_gpu %i \n", nFaceBnd);
  int nThrs = 64;
  dim3 thrTopoFaceBndSdom = dim3(nThrs           ,1,1);
  dim3 blkTopoFaceBndSdom = dim3(nFaceBnd/nThrs+1,1,1);

  __spectralradiusidealgasbnd5equ_gpu<<<blkTopoFaceBndSdom, thrTopoFaceBndSdom>>>(nCelTot,
                                                                                  nFace,
                                                                                  nFaceBnd,
                                                                                  gam,
                                                                                  pointList,
                                                                                  surf,
                                                                                  wb1,
                                                                                  spectralradius);

}


// ===========================================================================
__global__ void __spectralradiusidealgas5equ_gpu(const int nCelTot,
                                                 const int nFace,
                                                 const double gam,
                                                 const double rgaz,
                                                 double* __restrict__ rho,
                                                 double* __restrict__ velo,
                                                 double* __restrict__ temp,
                                                 double* __restrict__ surf,
                                                 double* __restrict__ spectralradius,
                                                 int*    __restrict__ FaceCell)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  double gam1   = gam - 1.;
  double gam1_1 = 1./gam1;

  if( tx < nFace){
    // printf("tx %i [%i] --> [%i] [%f] \n", tx, nFace, FaceVtxIdx[tx], coords[4] );
    int il = FaceCell[tx      ];
    int ir = FaceCell[tx+nFace];

    double tnx = surf[tx        ];
    double tny = surf[tx+  nFace];
    double tnz = surf[tx+2*nFace];
    double tnn = norm3d(tnx, tny, tnz);

    double vxl = velo[il];
    double vxr = velo[ir];

    double vyl = velo[il+nCelTot];
    double vyr = velo[ir+nCelTot];

    double vzl = velo[il+2*nCelTot];
    double vzr = velo[ir+2*nCelTot];

    double hm  = gam*gam1_1*temp[il]*rgaz + 0.5*( vxl*vxl + vyl*vyl + vzl*vzl );
    double hp  = gam*gam1_1*temp[ir]*rgaz + 0.5*( vxr*vxr + vyr*vyr + vzr*vzr );

    double w1mi = 1./rho[il];

    double r  = sqrt(rho[ir]*w1mi);
    double rr = 1./(1.+r);

    double uu = ( vxr * r + vxl )*rr;
    double vv = ( vyr * r + vyl )*rr;
    double ww = ( vzr * r + vzl )*rr;

    double ee = 0.5*(uu*uu + vv*vv + ww*ww);
    double hh = (hp*r+hm)*rr;

    double cc = sqrt(gam1*(hh-ee));

    spectralradius[tx] = abs( uu * tnx + vv * tny + ww * tnz ) + cc * tnn;

  }
}

// ===========================================================================
__host__ void spectralradiusidealgas5equ_gpu(const int&    nCelTot,
                                             const int&    nFace,
                                             const double& gam,
                                             const double& rgaz,
                                             int*          FaceCell,
                                             double*       rho,
                                             double*       velo,
                                             double*       temp,
                                             double*       surf,
                                             double*       spectralradius)
{
  int nThrs = 64;
  int nBlks = nFace/nThrs+1;
  dim3 thrTopoSdom  = dim3(nThrs,1,1);
  dim3 blkTopoSdom  = dim3(nFace/nThrs+1,1,1);

  __spectralradiusidealgas5equ_gpu<<<blkTopoSdom, thrTopoSdom>>>( nCelTot,
                                                                  nFace,
                                                                  gam,
                                                                  rgaz,
                                                                  rho,
                                                                  velo,
                                                                  temp,
                                                                  surf,
                                                                  spectralradius,
                                                                  FaceCell);

}
