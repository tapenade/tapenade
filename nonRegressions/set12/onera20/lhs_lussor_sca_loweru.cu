// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>
#include <assert.h>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

__global__ void __initatnullimpliciteincr_gpu(const int n_cellTot,
                                              const int n_cellInt,
                                              double* __restrict__ dwimp)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_cellTot){
    dwimp[tx           ] = 0.;
    dwimp[tx+1*n_cellTot] = 0.;
    dwimp[tx+2*n_cellTot] = 0.;
    dwimp[tx+3*n_cellTot] = 0.;
    dwimp[tx+4*n_cellTot] = 0.;
  }
}

// ===========================================================================
__global__ void __initimpliciteincr_gpu(const int n_cellTot,
                                        const int n_cellInt,
                                        double* __restrict__ volume,
                                        double* __restrict__ timestep,
                                        double* __restrict__ dwexp,
                                        double* __restrict__ dwimp,
                                        double* __restrict__ ssor)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_cellInt){
    double dtonvol = 1.*timestep[tx]/volume[tx];
    // W^(n+1) = W^(n) - (dt/vol)*deltaW
    dwexp[tx           ] = -dwexp[tx           ]*dtonvol;
    dwexp[tx+1*n_cellTot] = -dwexp[tx+1*n_cellTot]*dtonvol;
    dwexp[tx+2*n_cellTot] = -dwexp[tx+2*n_cellTot]*dtonvol;
    dwexp[tx+3*n_cellTot] = -dwexp[tx+3*n_cellTot]*dtonvol;
    dwexp[tx+4*n_cellTot] = -dwexp[tx+4*n_cellTot]*dtonvol;

    dwimp[tx           ] = dwexp[tx           ];
    dwimp[tx+1*n_cellTot] = dwexp[tx+1*n_cellTot];
    dwimp[tx+2*n_cellTot] = dwexp[tx+2*n_cellTot];
    dwimp[tx+3*n_cellTot] = dwexp[tx+3*n_cellTot];
    dwimp[tx+4*n_cellTot] = dwexp[tx+4*n_cellTot];

    ssor[tx           ] = 0.;
    ssor[tx+1*n_cellTot] = 0.;
    ssor[tx+2*n_cellTot] = 0.;
    ssor[tx+3*n_cellTot] = 0.;
    ssor[tx+4*n_cellTot] = 0.;
  };
}

// ===========================================================================
__global__ void __updateimpliciteincr_gpu(const int n_cellTot,
                                          const int n_cellInt,
                                          double* __restrict__ dwexp,
                                          double* __restrict__ dwimp,
                                          double* __restrict__ ssor)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_cellInt){
    dwimp[tx           ] = dwexp[tx           ] + ssor[tx           ];
    dwimp[tx+1*n_cellTot] = dwexp[tx+1*n_cellTot] + ssor[tx+1*n_cellTot];
    dwimp[tx+2*n_cellTot] = dwexp[tx+2*n_cellTot] + ssor[tx+2*n_cellTot];
    dwimp[tx+3*n_cellTot] = dwexp[tx+3*n_cellTot] + ssor[tx+3*n_cellTot];
    dwimp[tx+4*n_cellTot] = dwexp[tx+4*n_cellTot] + ssor[tx+4*n_cellTot];

    ssor[tx           ] = 0.;
    ssor[tx+1*n_cellTot] = 0.;
    ssor[tx+2*n_cellTot] = 0.;
    ssor[tx+3*n_cellTot] = 0.;
    ssor[tx+4*n_cellTot] = 0.;
  };
}

// ===========================================================================
__global__ void __lhslussorscaupdatecell(const int n_cellTot,
                                         const int begCell,
                                         const int endCell,
                                         double* __restrict__ timestep,
                                         double* __restrict__ volume,
                                         double* __restrict__ ssor,
                                         double* __restrict__ diag,
                                         double* __restrict__ dwimp)
  {
  int tx = begCell + threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < endCell){
    double dtonvol = 1.*timestep[tx]/volume[tx];
    // W^(n+1) = W^(n) - (dt/vol)*deltaW
    ssor[tx           ] = ssor[tx           ]*dtonvol;
    ssor[tx+1*n_cellTot] = ssor[tx+1*n_cellTot]*dtonvol;
    ssor[tx+2*n_cellTot] = ssor[tx+2*n_cellTot]*dtonvol;
    ssor[tx+3*n_cellTot] = ssor[tx+3*n_cellTot]*dtonvol;
    ssor[tx+4*n_cellTot] = ssor[tx+4*n_cellTot]*dtonvol;

    double diagtmp = diag[tx];
    dwimp[tx           ] = ( dwimp[tx           ] + ssor[tx           ] ) * diagtmp;
    dwimp[tx+1*n_cellTot] = ( dwimp[tx+1*n_cellTot] + ssor[tx+1*n_cellTot] ) * diagtmp;
    dwimp[tx+2*n_cellTot] = ( dwimp[tx+2*n_cellTot] + ssor[tx+2*n_cellTot] ) * diagtmp;
    dwimp[tx+3*n_cellTot] = ( dwimp[tx+3*n_cellTot] + ssor[tx+3*n_cellTot] ) * diagtmp;
    dwimp[tx+4*n_cellTot] = ( dwimp[tx+4*n_cellTot] + ssor[tx+4*n_cellTot] ) * diagtmp;

  };
}

// ===========================================================================
__global__ void __lhslussorscalowersweepface(const int n_cellTot,
                                             const int n_face,
                                             const int begFace,
                                             const int endFace,
                                             const double gam,
                                             double* __restrict__ wcons,
                                             double* __restrict__ dwimp,
                                             double* __restrict__ ssor,
                                             double* __restrict__ surf,
                                             double* __restrict__ specradius,
                                             int*    __restrict__ face_cell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  double gam1   = gam - 1.;

  if( tx < endFace){
    int il = face_cell[tx      ];
    int ir = face_cell[tx+n_face];

    int icell = max(il, ir);
    int iopp  = min(il, ir);

    int sgn = 1;
    if( icell == il ){ sgn = -1;}
#include "sonics/operators/operator_lhs/sweep_euler_ideal_gasu.cuh"

  }
}

// ===========================================================================
__global__ void __lhslussorscauppersweepface(const int n_cellTot,
                                             const int n_face,
                                             const int begFace,
                                             const int endFace,
                                             const double gam,
                                             double* __restrict__ wcons,
                                             double* __restrict__ dwimp,
                                             double* __restrict__ ssor,
                                             double* __restrict__ surf,
                                             double* __restrict__ specradius,
                                             int*    __restrict__ face_cell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  double gam1   = gam - 1.;

  if( tx < endFace){
    int il = face_cell[tx      ];
    int ir = face_cell[tx+n_face];

    int icell = min(il, ir);
    int iopp  = max(il, ir);

    int sgn = 1;
    if( icell == il ){ sgn = -1;}
#include "sonics/operators/operator_lhs/sweep_euler_ideal_gasu.cuh"
  }
}

// ===========================================================================
__host__ void lhslussorscalower_gpu(const int& n_cellTot,
                                    const int& n_cellInt,
                                    const int& n_face,
                                    const int& n_sdom,
                                    const double& gam,
                                    int*  cellVectTileBeg,
                                    int*  cellVectTileN,
                                    int*  faceVectBndTileIdx,
                                    int*  nVectBndPack,
                                    int*  nVectBndPackIdx,
                                    int*  n_cell_vect_pack,
                                    int*  n_cell_vect_pack_idx,
                                    int*  cell_vect_tile_multi_color_idx,
                                    int*  n_multi_vect_pack_face,
                                    int*  n_multi_vect_pack_face_bnd,
                                    int*  n_face_vect_pack_idx,
                                    int*  face_vect_tile_multi_color_idx,
                                    int*  face_cell,
                                    double* wcons,
                                    double* volume,
                                    double* timestep,
                                    double* diag,
                                    double* specradius,
                                    double* surf,
                                    double* dwexp,
                                    double* dwimp,
                                    double* ssor)
{
  printf("lhslussorscalower_gpu %i \n", n_cellTot);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoCellSdom = dim3(nThrs          ,1,1);
  dim3 blkTopoCellSdom = dim3(n_cellInt/nThrs+1,1,1);
  dim3 blkTopoCellTotSdom = dim3(n_cellTot/nThrs+1,1,1);

  // --------------------------------------------------------------------------------
  // 1) - Init implicit residual - Pour l'instant pas de ghostcell ni de sous-dom
  // Not necessary i think
  __initatnullimpliciteincr_gpu<<<blkTopoCellTotSdom, thrTopoCellSdom>>>(n_cellTot, n_cellInt,
                                                                         dwimp);
  __initatnullimpliciteincr_gpu<<<blkTopoCellTotSdom, thrTopoCellSdom>>>(n_cellTot, n_cellInt,
                                                                         ssor);
  assert(n_sdom == 1);
  // __initimpliciteincr_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cellTot, n_cellInt,
  __initimpliciteincr_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cellTot, n_cellInt,
                                                                volume,
                                                                timestep,
                                                                dwexp,
                                                                dwimp,
                                                                ssor);

  // --------------------------------------------------------------------------------
  // 2) Forward Sweep - For each color
  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int icolor_cell = 0; icolor_cell < n_cell_vect_pack[i_sub]; ++icolor_cell){

      int ibeg   = n_cell_vect_pack_idx[i_sub];
      int ibeg_v = ibeg + icolor_cell;

      // 2-1 - Pacquet de faces independantes !
      for(int ivec_face = 0; ivec_face < n_multi_vect_pack_face[ibeg_v]; ++ivec_face){

        int ibegface   = n_face_vect_pack_idx[ibeg_v];
        int ibegface_v = ibegface + ivec_face;

        int n_faceLoc = face_vect_tile_multi_color_idx[ibegface_v+1]-face_vect_tile_multi_color_idx[ibegface_v];
        dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
        dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
        __lhslussorscalowersweepface<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cellTot, n_face,
                                                                           face_vect_tile_multi_color_idx[ibegface_v],
                                                                           face_vect_tile_multi_color_idx[ibegface_v+1],
                                                                           gam,
                                                                           wcons,
                                                                           dwimp,
                                                                           ssor,
                                                                           surf,
                                                                           specradius,
                                                                           face_cell);

      }

      // 2-2 - Update cell color
      int n_cellLoc = cell_vect_tile_multi_color_idx[ibeg_v+1] - cell_vect_tile_multi_color_idx[ibeg_v];
      dim3 thrTopoCellColorSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoCellColorSdom = dim3(n_cellLoc/nThrs+1,1,1);
      __lhslussorscaupdatecell<<<thrTopoCellColorSdom, blkTopoCellColorSdom>>>(n_cellTot,
                                                                               cell_vect_tile_multi_color_idx[ibeg_v],
                                                                               cell_vect_tile_multi_color_idx[ibeg_v+1],
                                                                               timestep,
                                                                               volume,
                                                                               ssor,
                                                                               diag,
                                                                               dwimp);



    }

    // 2-3 - Border (Preparation of next ssdom)
    int icolor_cell = n_cell_vect_pack[i_sub]-1;
    int ibeg        = n_cell_vect_pack_idx[i_sub];
    int ibeg_v      = ibeg + icolor_cell;
    int beg_pack_face = n_multi_vect_pack_face[ibeg_v];
    int end_pack_face = beg_pack_face + n_multi_vect_pack_face_bnd[i_sub];
    for(int ivec_face = beg_pack_face; ivec_face < end_pack_face; ++ivec_face){

      int ibegface   = n_face_vect_pack_idx[ibeg_v];
      int ibegface_v = ibegface + ivec_face;

      int n_faceLoc = face_vect_tile_multi_color_idx[ibegface_v+1]-face_vect_tile_multi_color_idx[ibegface_v];
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __lhslussorscalowersweepface<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cellTot, n_face,
                                                                         face_vect_tile_multi_color_idx[ibegface_v],
                                                                         face_vect_tile_multi_color_idx[ibegface_v+1],
                                                                         gam,
                                                                         wcons,
                                                                         dwimp,
                                                                         ssor,
                                                                         surf,
                                                                         specradius,
                                                                         face_cell);

    }
  }

  // int bytes = n_cellTot * sizeof(double) * 5;
  // std::vector<double> check(n_cellTot*5);
  // std::vector<double> h_ssor(n_cellTot*5);
  // std::vector<double> h_expl(n_cellTot*5);
  // checkCuda( cudaMemcpy(check.data() , dwimp, bytes, cudaMemcpyDeviceToHost) );
  // checkCuda( cudaMemcpy(h_ssor.data(), ssor , bytes, cudaMemcpyDeviceToHost) );
  // checkCuda( cudaMemcpy(h_expl.data(), dwexp, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < n_cellInt; ++i){
  //   printf("cpu check lowerswepp [%i] = dw1 :: [%f/%f/%f] \n", i, check[i           ], h_expl[i           ], h_ssor[i           ]);
  //   printf("cpu check lowerswepp [%i] = dw2 :: [%f/%f/%f] \n", i, check[i+1*n_cellTot], h_expl[i+1*n_cellTot], h_ssor[i+1*n_cellTot]);
  //   printf("cpu check lowerswepp [%i] = dw3 :: [%f/%f/%f] \n", i, check[i+2*n_cellTot], h_expl[i+2*n_cellTot], h_ssor[i+2*n_cellTot]);
  //   printf("cpu check lowerswepp [%i] = dw4 :: [%f/%f/%f] \n", i, check[i+3*n_cellTot], h_expl[i+3*n_cellTot], h_ssor[i+3*n_cellTot]);
  //   printf("cpu check lowerswepp [%i] = dw5 :: [%f/%f/%f] \n", i, check[i+4*n_cellTot], h_expl[i+4*n_cellTot], h_ssor[i+4*n_cellTot]);
  // }
  // checkCuda( cudaDeviceSynchronize() );
  // ::abort();



  // --------------------------------------------------------------------------------
  // 3) Backward Sweep - For each color
  __updateimpliciteincr_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cellTot, n_cellInt,
                                                                  dwexp,
                                                                  dwimp,
                                                                  ssor);

  // 3-1 - Boundary conditions implicitations !
  for(int i_sub = n_sdom-1; i_sub >= 0 ; --i_sub){
    for(int iVec = 0; iVec < nVectBndPack[i_sub]; ++iVec){
      int iBeg = nVectBndPackIdx[i_sub];
      int n_faceLoc = faceVectBndTileIdx[iBeg+iVec+1]-faceVectBndTileIdx[iBeg+iVec];
      // printf(" [%i]/[%i] --> n_faceLocBnd[%i]\n", i_sub, iVec, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __lhslussorscauppersweepface<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cellTot, n_face,
                                                                        faceVectBndTileIdx[iBeg+iVec],
                                                                        faceVectBndTileIdx[iBeg+iVec+1],
                                                                         gam,
                                                                         wcons,
                                                                         dwimp,
                                                                         ssor,
                                                                         surf,
                                                                         specradius,
                                                                         face_cell);
    }
  }

  for(int i_sub = n_sdom-1; i_sub >= 0 ; --i_sub){
    // 3-1 - Boundary conditions implicitations !
    int icolor_cell = n_cell_vect_pack[i_sub]-1;
    int ibeg        = n_cell_vect_pack_idx[i_sub];
    int ibeg_v      = ibeg + icolor_cell;
    int beg_pack_face = n_multi_vect_pack_face[ibeg_v];
    int end_pack_face = beg_pack_face + n_multi_vect_pack_face_bnd[i_sub];
    for(int ivec_face = beg_pack_face; ivec_face < end_pack_face; ++ivec_face){

      int ibegface   = n_face_vect_pack_idx[ibeg_v];
      int ibegface_v = ibegface + ivec_face;

      int n_faceLoc = face_vect_tile_multi_color_idx[ibegface_v+1]-face_vect_tile_multi_color_idx[ibegface_v];
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __lhslussorscauppersweepface<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cellTot, n_face,
                                                                         face_vect_tile_multi_color_idx[ibegface_v],
                                                                         face_vect_tile_multi_color_idx[ibegface_v+1],
                                                                         gam,
                                                                         wcons,
                                                                         dwimp,
                                                                         ssor,
                                                                         surf,
                                                                         specradius,
                                                                         face_cell);

    }

    for(int icolor_cell = n_cell_vect_pack[i_sub]-1; icolor_cell >= 0 ; --icolor_cell){

      int ibeg   = n_cell_vect_pack_idx[i_sub];
      int ibeg_v = ibeg + icolor_cell;

      // 3-2 - Update cell color
      int n_cellLoc = cell_vect_tile_multi_color_idx[ibeg_v+1] - cell_vect_tile_multi_color_idx[ibeg_v];
      dim3 thrTopoCellColorSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoCellColorSdom = dim3(n_cellLoc/nThrs+1,1,1);
      __lhslussorscaupdatecell<<<thrTopoCellColorSdom, blkTopoCellColorSdom>>>(n_cellTot,
                                                                               cell_vect_tile_multi_color_idx[ibeg_v],
                                                                               cell_vect_tile_multi_color_idx[ibeg_v+1],
                                                                               timestep,
                                                                               volume,
                                                                               ssor,
                                                                               diag,
                                                                               dwimp);
      // 3-3 - Pacquet de faces independantes !
      for(int ivec_face = 0; ivec_face < n_multi_vect_pack_face[ibeg_v]; ++ivec_face){

        int ibegface   = n_face_vect_pack_idx[ibeg_v];
        int ibegface_v = ibegface + ivec_face;

        int n_faceLoc = face_vect_tile_multi_color_idx[ibegface_v+1]-face_vect_tile_multi_color_idx[ibegface_v];
        dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
        dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
        __lhslussorscauppersweepface<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cellTot, n_face,
                                                                           face_vect_tile_multi_color_idx[ibegface_v],
                                                                           face_vect_tile_multi_color_idx[ibegface_v+1],
                                                                           gam,
                                                                           wcons,
                                                                           dwimp,
                                                                           ssor,
                                                                           surf,
                                                                           specradius,
                                                                           face_cell);

      }
    }
  }




  // 3-3 - Border (Preparation of next ssdom)



}
