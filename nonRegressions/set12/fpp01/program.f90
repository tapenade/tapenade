! This is to test the case of source code with
! preprocessing directives, that may require running
! through the preprocessor before differentiation, in
! the style of the call to cpp on C source files.
subroutine test(x,y)
  real x,y
  y = x*y
#ifdef MORECODE
  y = sin(y)
#include "yetmorecode"
#else
  y = y+1
#endif
end
