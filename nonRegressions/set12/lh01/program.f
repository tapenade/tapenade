C Bug with the improved management of fpp #include's in Fortran
C that caused some derivative initializations (from -nooptim spareinit)
C placed *before* an include containing declarations.

      SUBROUTINE MORSOL(xmo_CBUL,xmo_VTBUL,xmo_VAT,xmo_RBT,mo_BBJT,
     &                  xmo_BREM,xmo_BBREM)
C comm1
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
C comm2
      CALL MODDES(xmo_CBUL,xmo_VAT,xmo_VTBUL,xmo_RBT,xmo_DDESTR)
      IF (mo_RESOL .EQ. 1) THEN
         xmo_NRE = 
     &         1.D0 -
     &         ( max(0.D0 , 1.D0 - xmo_EPECGD * xmo_VAT /
     &                             xmo_RBT / xmo_BVDW ) )**3.D0  
         xmo_BREM = 
     &     xmo_NELSON * xmo_NRE**(1.D0 - xmo_DDESTR) * xmo_TOFISS
      ENDIF
      RETURN
      END
