#include <stdio.h>
#include <stdlib.h>

__device__
void sub1(float *ax, int i, float *ayip1) {
        *ayip1 = ax[i] * 1.5 ;
}

__global__
void __foo(int n, float a, float *x, float *y)
{
  int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < n) {
    y[i] = a*x[i] + y[i];
    sub1(x, i, &(y[i+1])) ;
  }
}

__host__
void foo(int N, float a, float *x, float *y) {
  __device__ float *d_x, *d_y;
  cudaMalloc(&d_x, N*sizeof(float)); 
  cudaMalloc(&d_y, N*sizeof(float));
  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);

  __foo<<<(N+255)/256, 256>>>(N, a, d_x, d_y);

  cudaMemcpy(y, d_y, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaFree(d_x);
  cudaFree(d_y);
}

int main(void)
{
  int N = 1<<20;
  float a, *x, *y;
  a = 2.0f ;
  x = (float*)malloc(N*sizeof(float));
  y = (float*)malloc(N*sizeof(float));

  for (int i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  foo(N, a, x, y);

  float maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = max(maxError, abs(y[i]-4.0f));
  printf("Max error: %f\n", maxError);

  free(x);
  free(y);
}
