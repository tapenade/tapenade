C Testing functionality "replicateDiff" asked by Onera:
C Same as replica1, but with procedure and function calls
      subroutine foo(a,b,n)
      INTEGER n
      REAL*8 a,c
      REAL*8 b(10,n)

      a = a*b(3,3)
      CALL bar(a,b)
      b(5,n-2) = b(5,2)*b(8,1) + sin(a)
      c = 15.0
      a = gee(a,c,b)
      a = sqrt(a)
      end

      subroutine bar(u,v)
      REAL*8 u
      REAL*8 v(100)
      u = u*u
      v(5)= u*v(6)*v(7)*v(8)*v(9)
      end

      REAL*8 function gee(x,y,z)
      REAL*8 x,y
      REAL*8 z(100)

      z(2) = z(2)*x
      y = 2*y+1.0
      x = y*x
      gee = x*z(10)
      end

