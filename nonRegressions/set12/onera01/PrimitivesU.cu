// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
//#include "tmo/cuda_utilities.hpp"
// llh patch:
#include "../../cuda_utilities.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __computeprimitivesu_gpu(const int nCellTot,
                                         const double gam,
                                         const double cv,
                                         double* __restrict__ conservative,
                                         double* __restrict__ rho,
                                         double* __restrict__ velo,
                                         double* __restrict__ temp)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < nCellTot){
    double invcv = 1./cv;
    double rhoc  = conservative[tx           ];
    double rhouc = conservative[tx+  nCellTot];
    double rhovc = conservative[tx+2*nCellTot];
    double rhowc = conservative[tx+3*nCellTot];
    double rhoec = conservative[tx+4*nCellTot];

    double roi   = 1./rhoc;
    double roe   = rhoec - 0.5 * roi * (rhouc*rhouc + rhovc*rhovc + rhowc*rhowc);

    rho[tx] = rhoc;

    velo[tx           ] = rhouc*roi;
    velo[tx+  nCellTot] = rhovc*roi;
    velo[tx+2*nCellTot] = rhowc*roi;

    temp[tx] = roi*invcv*roe;


  };
}


// ===========================================================================
__host__ void computeprimitivesu_gpu(const int& nCellTot,
                                     const double& gam,
                                     const double& cv,
                                     double* conservative,
                                     double* rho,
                                     double* velo,
                                     double* temp)
{
  printf("computeprimitivesu_gpu %i \n", nCellTot);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoCellSdom = dim3(nThrs          ,1,1);
  dim3 blkTopoCellSdom = dim3(nCellTot/nThrs+1,1,1);

  // First Step
  __computeprimitivesu_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(nCellTot,
                                                                 gam,
                                                                 cv,
                                                                 conservative,
                                                                 rho,
                                                                 velo,
                                                                 temp);
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("computeprimitivesu_gpu::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );

  // cudaError_t err = cudaGetLastError();
  // if (err != cudaSuccess){
  //   printf("Error: %s\n", cudaGetErrorString(err));
  // }
  // checkCuda( cudaDeviceSynchronize() );
  // printf("computeprimitivesu_gpu end %i \n", nCellTot);

  // int bytes = nCellTot * sizeof(double);
  // std::vector<double> array(nCellTot);
  // checkCuda( cudaMemcpy(array.data(), rho, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < nCellTot; ++i){
  //   printf("computeprimitivesu_gpu::cpu array[%i] = [%f] \n", i, array[i]);
  // }
}
