// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __initdiag1eq_gpu(const int n_cellt, double* __restrict__ diaglhs)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_cellt){
    diaglhs[tx] = 0.;
  };
}

// ===========================================================================
__global__ void __computediagsca1eq_gpu(const int n_cellt,
                                        const int n_face,
                                        const int begFace,
                                        const int endFace,
                                        double* __restrict__ diaglhs,
                                        double* __restrict__ specradius,
                                        int*    __restrict__ face_cell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < endFace){
    int il = face_cell[tx      ];
    int ir = face_cell[tx+n_face];

    diaglhs[ir] += 0.5*specradius[tx];
    diaglhs[il] += 0.5*specradius[tx];

  };
}

// ===========================================================================
__global__ void __computediagsca1eqbnd_gpu(const int n_cellt,
                                           const int n_face,
                                           const int begFace,
                                           const int endFace,
                                           double* __restrict__ diaglhs,
                                           double* __restrict__ specradius,
                                           int*    __restrict__ face_cell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < endFace){
    int il = face_cell[tx      ];
    diaglhs[il] += 0.5*specradius[tx];
  };
}
// ===========================================================================
__global__ void __finalizediagsca1eq_gpu(const int n_cell,
                                         const double alpha,
                                         const double beta,
                                         double* __restrict__ dt,
                                         double* __restrict__ vol,
                                         double* __restrict__ diaglhs)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < n_cell){
    double ivol = dt[tx]/vol[tx];
    diaglhs[tx] = 1./(alpha + beta*dt[tx] + diaglhs[tx]*ivol);
  };

}

// ===========================================================================
__host__ void computediagonallhssca_gpu(const int&    n_cell,
                                        const int&    n_cellt,
                                        const int&    n_face,
                                        const int&    n_sdom,
                                        const double& alpha,
                                        const double& beta,
                                        int*          face_vect_tile_idx,
                                        int*          nvect_pack,
                                        int*          nvect_pack_idx,
                                        int*          face_vect_ext_tile_idx,
                                        int*          nvect_ext_pack,
                                        int*          nvect_ext_pack_idx,
                                        double*       dt,
                                        double*       vol,
                                        double*       spectralradius,
                                        double*       diaglhs,
                                        int*          face_cell)
{
  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoCellSdom = dim3(nThrs        ,1,1);
  dim3 blkTopoCellSdom = dim3(n_cell/nThrs+1,1,1); // Normalement pas besoin des ghost-cell

  __initdiag1eq_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cell, diaglhs);

  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int i_vec = 0; i_vec < nvect_pack[i_sub]; ++i_vec){
      int i_beg = nvect_pack_idx[i_sub];
      int n_faceLoc = face_vect_tile_idx[i_beg+i_vec+1]-face_vect_tile_idx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_faceLoc[%i]\n", i_sub, i_vec, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __computediagsca1eq_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cellt, n_face,
                                                                    face_vect_tile_idx[i_beg+i_vec  ],
                                                                    face_vect_tile_idx[i_beg+i_vec+1],
                                                                    diaglhs,
                                                                    spectralradius,
                                                                    face_cell);
    }
  }

  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int i_vec = 0; i_vec < nvect_ext_pack[i_sub]; ++i_vec){
      int i_beg = nvect_ext_pack_idx[i_sub];
      int n_faceLoc = face_vect_ext_tile_idx[i_beg+i_vec+1]-face_vect_ext_tile_idx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_faceLocBnd[%i]\n", i_sub, i_vec, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __computediagsca1eqbnd_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_cellt, n_face,
                                                                       face_vect_ext_tile_idx[i_beg+i_vec],
                                                                       face_vect_ext_tile_idx[i_beg+i_vec+1],
                                                                       diaglhs,
                                                                       spectralradius,
                                                                       face_cell);
    }
  }

  __finalizediagsca1eq_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cell,
                                                                 alpha,
                                                                 beta,
                                                                 dt,
                                                                 vol,
                                                                 diaglhs);

  // int bytes = n_cellt * sizeof(double);
  // std::vector<double> check(n_cellt);
  // std::vector<double> h_dt(n_cellt);
  // std::vector<double> h_vol(n_cellt);
  // checkCuda( cudaMemcpy(check.data(), diaglhs, bytes, cudaMemcpyDeviceToHost) );
  // checkCuda( cudaMemcpy(h_dt.data(), dt, bytes, cudaMemcpyDeviceToHost) );
  // checkCuda( cudaMemcpy(h_vol.data(), vol, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < n_cell; ++i){
  //   printf("cpu check diaglhs [%i] = [%f/%f/%f] \n", i, check[i], h_dt[i], h_vol[i]);
  // }

}
