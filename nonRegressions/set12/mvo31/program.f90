!
!-- type-bound procedure
!
module typeproc

  type t
     real :: a
   contains
     procedure :: calc
  end type t


contains

  subroutine calc(self, x, y)
    implicit none
    ! type(t), intent(in) :: self
    class(t), intent(in) :: self
    real, intent(in)  :: x
    real, intent(out) :: y

    y = self%a * x
  end subroutine calc

end module typeproc


program main
  use iso_fortran_env, only : output_unit
  use typeproc
  implicit none

  real :: x, y
  type(t) :: z

  z%a = 3
  x = 2.5

  call z%calc(x,y)

  write(output_unit,*) 'x=',x,'y=',y
end program main
