! This is to test the possibility of adding labels in the source,
! that percolate to the diff code, and that a script can use to
! add extra code, e.g. here to insert 2 additional loop levels.
subroutine test(x,y,TT)
  real x,y,TT(100)
  implicit none
#include "incldummy.h"
  integer(kind=4) :: ii
  integer(kind=4) :: beg_bnd, end_bnd
  real(kind=8)    :: sx, sy, sz
  ! Comment1
  ! Comment2
  ! $AD LABEL extra-loop-declarations
  !#include "loopDecls.for"
  ! $AD END-LABEL extra-loop-declarations
  ! Comment3
  x = x*x
  ! Comment4
  ! $AD LABEL extra-loop
  !#include "loopBegin.for"
  DO ii = beg_bnd, end_bnd
     TT(ii) = ii*TT(ii)*y
     ! Comment6
  END DO
  ! $AD END-LABEL extra-loop
  !#include "loopEnd.for"
  ! Comment8
end subroutine test
  ! Comment9
