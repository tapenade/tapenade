c Bug found by S.Carli and by Krishna in july 2024
c The initialization of the (multidir tangent) diff of gradr
c was wrong, using a wrong mix of F77 index and array notation.

      subroutine mini(xx, J)
      real(kind=8) :: xx, J(100)
      real (kind=8) :: gradr(20)

      J = 0.0_8
      call foo(xx, gradr)
      J(2) = gradr(2)/4
      end subroutine mini
