module type_module
  implicit none

  type test_type
    real :: val = 5.
    procedure(fun_interface), pointer :: fun_ptr
 end type test_type

  abstract interface
     function fun_interface(self, n,x) result(f)
       import test_type
       class(test_type) :: self
       integer, intent(in) :: n
       real, intent(in) :: x(n)
       real :: f(n)
    end function fun_interface
  end interface  

  contains

  subroutine  type_set_func(test, fun)    
    interface
       function fun(self,n,x) result(f)
         import test_type
         class(test_type) :: self
         integer, intent(in) :: n
         real, intent(in) :: x(n)
         real :: f(n)
      end function fun
    end interface  

    class(test_type) :: test
    test%fun_ptr => fun;
  end subroutine type_set_func

end module type_module
 
module funcs
  use type_module, only: test_type
  implicit none

  contains

    function fun2 (self, n,x ) result (f )
      class(test_type) :: self
      integer, intent(in) :: n
      real, intent(in) :: x(n)
      real :: f(n)
      f = self%val*x ;
      self%val = self%val + x(2)
  end function fun2

  function fun3 (self, n,x ) result (f )
    class(test_type) :: self
    integer, intent(in) :: n
    real, intent(in) :: x(n)
    real :: f(n)
    f = self%val*x ;
    self%val = self%val + x(1)
  end function fun3

end module funcs

program main
  use type_module
  use funcs
  type(test_type) :: test
  integer, parameter :: n = 2
  real :: x(n), f(n)

  x = (/1., 2./)

  print*, 'test%val=', test%val
  call type_set_func (test, fun2)
  f = test%fun_ptr(n, x)
  print *, " f from  fun2 ", "f=",f, 'test%val=', test%val

  call type_set_func (test, fun3)
  f = test%fun_ptr(n,x);
  print *, " f from call fun3 ", "f=",f, 'test%val=', test%val
end program
