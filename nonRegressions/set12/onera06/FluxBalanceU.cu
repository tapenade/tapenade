// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
//#include "tmo/cuda_utilities.hpp"
// llh patch:
#include "../../cuda_utilities.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __initfluxbalu_gpu(const int nCellTot, double* __restrict__ balance)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < nCellTot){
    balance[tx] = 0.;
  };
}

// ===========================================================================
__global__ void __initfluxbal5equ_gpu(const int nCellTot, double* __restrict__ balance)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < nCellTot){
    balance[tx           ] = 0.;
    balance[tx+  nCellTot] = 0.;
    balance[tx+2*nCellTot] = 0.;
    balance[tx+3*nCellTot] = 0.;
    balance[tx+4*nCellTot] = 0.;
  };
}

// ===========================================================================
__global__ void __computefluxbalu_gpu(const int nCellTot,
                                      const int nFace,
                                      const int begFace,
                                      const int endFace,
                                      double* __restrict__ balance,
                                      double* __restrict__ surf,
                                      int*    __restrict__ FaceCell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < endFace){
    int il = FaceCell[tx      ];
    int ir = FaceCell[tx+nFace];

    // printf("tx %i --> [%i/%i] begFace::[%i] --> [%f/%f/%f]\n", tx, il, ir, begFace, surf[tx], surf[tx+nFace], surf[tx+2*nFace]);

    // Suppose to do without conflict ... Caution with multiple Kernel launch
    balance[il] += surf[tx];
    balance[ir] -= surf[tx];
  };

}

// ===========================================================================
__global__ void __computefluxbal5equ_gpu(const int nCellTot,
                                         const int nFace,
                                         const int begFace,
                                         const int endFace,
                                         double* __restrict__ balance,
                                         double* __restrict__ flux,
                                         int*    __restrict__ FaceCell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < endFace){
    int il = FaceCell[tx      ];
    int ir = FaceCell[tx+nFace];

    // printf("tx %i --> [%i/%i] begFace::[%i] --> [%f/%f/%f]\n", tx, il, ir, begFace, surf[tx], surf[tx+nFace], surf[tx+2*nFace]);

    // if( il == 0 || ir == 0 ){
    // if( il == 127 || ir == 127 ){
    //   printf("tx %i --> [%i/%i] debug:: begFace::[%i] --> [%f/%f/%f/%f/%f]\n", tx, il, ir, begFace,
    //                                 flux[tx],
    //                                 flux[tx+nFace],
    //                                 flux[tx+2*nFace],
    //                                 flux[tx+3*nFace],
    //                                 flux[tx+4*nFace]);

    // }

    // Suppose to do without conflict ... Caution with multple Kernel launch
    balance[il           ] += flux[tx        ];
    balance[ir           ] -= flux[tx        ];
    balance[il+  nCellTot] += flux[tx+  nFace];
    balance[ir+  nCellTot] -= flux[tx+  nFace];
    balance[il+2*nCellTot] += flux[tx+2*nFace];
    balance[ir+2*nCellTot] -= flux[tx+2*nFace];
    balance[il+3*nCellTot] += flux[tx+3*nFace];
    balance[ir+3*nCellTot] -= flux[tx+3*nFace];
    balance[il+4*nCellTot] += flux[tx+4*nFace];
    balance[ir+4*nCellTot] -= flux[tx+4*nFace];
  };

}
// ===========================================================================
__global__ void __computefluxbalbndu_gpu(const int nCellTot,
                                         const int nFace,
                                         const int begFace,
                                         const int endFace,
                                         double* __restrict__ balance,
                                         double* __restrict__ surf,
                                         int*    __restrict__ FaceCell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < endFace){
    int il = FaceCell[tx      ];
    // int ir = FaceCell[tx+nFace];

    // printf("tx %i --> [%i/%i] begFace::[%i] --> [%f/%f/%f]\n", tx, il, ir, begFace, surf[tx], surf[tx+nFace], surf[tx+2*nFace]);

    // Suppose to do without conflict ... Caution with multple Kernel launch
    balance[il] += surf[tx];
    // balance[ir] -= surf[tx];
  };

}

// ===========================================================================
__global__ void __computefluxbalbnd5equ_gpu(const int nCellTot,
                                            const int nFace,
                                            const int begFace,
                                            const int endFace,
                                            double* __restrict__ balance,
                                            double* __restrict__ flux,
                                            int*    __restrict__ FaceCell)
{
  int tx = begFace + threadIdx.x + blockIdx.x * blockDim.x;

  if( tx < endFace){
    int il = FaceCell[tx      ];

    balance[il           ] += flux[tx        ];
    balance[il+  nCellTot] += flux[tx+  nFace];
    balance[il+2*nCellTot] += flux[tx+2*nFace];
    balance[il+3*nCellTot] += flux[tx+3*nFace];
    balance[il+4*nCellTot] += flux[tx+4*nFace];
  };

}

// ===========================================================================
__host__ void computefluxbalu_gpu(const int& nCellTot,
                                  const int& nFace,
                                  const int& nSdom,
                                  int*    faceVectTileIdx,
                                  int*    nVectPack,
                                  int*    nVectPackIdx,
                                  int*    faceVectBndTileIdx,
                                  int*    nVectBndPack,
                                  int*    nVectBndPackIdx,
                                  double* balance,
                                  double* flux,
                                  int*    FaceCell)
{
  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoCellSdom = dim3(nThrs           ,1,1);
  dim3 blkTopoCellSdom = dim3(nCellTot/nThrs+1,1,1);

  // cudaError_t err0 = cudaGetLastError();
  // if (err0 != cudaSuccess){
  //   printf("computefluxbalu_gpu::s0::Error: %s\n", cudaGetErrorString(err0));
  // }
  // checkCuda( cudaDeviceSynchronize() );

  __initfluxbal5equ_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(nCellTot, balance);

  // cudaError_t err = cudaGetLastError();
  // if (err != cudaSuccess){
  //   printf("computefluxbalu_gpu::s1::Error: %s\n", cudaGetErrorString(err));
  // }
  // checkCuda( cudaDeviceSynchronize() );

  for(int iSub = 0; iSub < nSdom; ++iSub){
    for(int iVec = 0; iVec < nVectPack[iSub]; ++iVec){
      int iBeg = nVectPackIdx[iSub];
      int nFaceLoc = faceVectTileIdx[iBeg+iVec+1]-faceVectTileIdx[iBeg+iVec];
      // printf(" [%i]/[%i] --> nFaceLoc[%i]\n", iSub, iVec, nFaceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(nFaceLoc/nThrs+1,1,1);
      __computefluxbal5equ_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(nCellTot, nFace,
                                                                     faceVectTileIdx[iBeg+iVec  ],
                                                                     faceVectTileIdx[iBeg+iVec+1],
                                                                     balance,
                                                                     flux,
                                                                     FaceCell);
    }
  }

  // cudaError_t err2 = cudaGetLastError();
  // if (err2 != cudaSuccess){
  //   printf("computefluxbalu_gpu::s2::Error: %s\n", cudaGetErrorString(err2));
  // }
  // checkCuda( cudaDeviceSynchronize() );

  for(int iSub = 0; iSub < nSdom; ++iSub){
    for(int iVec = 0; iVec < nVectBndPack[iSub]; ++iVec){
      int iBeg = nVectBndPackIdx[iSub];
      int nFaceLoc = faceVectBndTileIdx[iBeg+iVec+1]-faceVectBndTileIdx[iBeg+iVec];
      // printf(" [%i]/[%i] --> nFaceLocBnd[%i]\n", iSub, iVec, nFaceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(nFaceLoc/nThrs+1,1,1);
      __computefluxbalbnd5equ_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(nCellTot, nFace,
                                                                        faceVectBndTileIdx[iBeg+iVec],
                                                                        faceVectBndTileIdx[iBeg+iVec+1],
                                                                        balance,
                                                                        flux,
                                                                        FaceCell);
    }
  }

  // cudaError_t err3 = cudaGetLastError();
  // if (err3 != cudaSuccess){
  //   printf("computefluxbalu_gpu::s3::Error: %s\n", cudaGetErrorString(err3));
  // }
  // checkCuda( cudaDeviceSynchronize() );

  // int bytes = nCellTot * sizeof(double)*5;
  // std::vector<double> check(nCellTot*5);
  // checkCuda( cudaMemcpy(check.data(), balance, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < nCellTot; ++i){
  //   printf("cpu check fluxbal [%i] = [%f/%f/%f/%f/%f] \n", i, check[i],
  //                                                             check[i+  nCellTot],
  //                                                             check[i+2*nCellTot],
  //                                                             check[i+3*nCellTot],
  //                                                             check[i+4*nCellTot]);
  // }

}

// ===========================================================================
// __host__ void debugcomputefluxbalu_gpu(const int& nCellTot,
//                                   const int& nFace,
//                                   const int& nSdom,
//                                   int*    faceVectTileIdx,
//                                   int*    nVectPack,
//                                   int*    nVectPackIdx,
//                                   int*    faceVectBndTileIdx,
//                                   int*    nVectBndPack,
//                                   int*    nVectBndPackIdx,
//                                   double* balance,
//                                   double* surf,
//                                   int*    FaceCell)
// {
//   // Target --> Feed Block and Threads
//   int nThrs = 64;
//   dim3 thrTopoCellSdom = dim3(nThrs           ,1,1);
//   dim3 blkTopoCellSdom = dim3(nCellTot/nThrs+1,1,1);

//   __initfluxbalu_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(nCellTot, balance);

//   // cudaError_t err = cudaGetLastError();
//   // if (err != cudaSuccess){
//   //   printf("Error: %s\n", cudaGetErrorString(err));
//   // }
//   // checkCuda( cudaDeviceSynchronize() );

//   // do iVec = 0, nVectPack(iSub)-1
//   // iBeg = nVectPackIdx(iSub)
//   // do iface=FaceVectTile(iBeg+iVec), FaceVectTile(iBeg+iVec+1)-1

//   for(int iSub = 0; iSub < nSdom; ++iSub){
//     for(int iVec = 0; iVec < nVectPack[iSub]; ++iVec){
//       int iBeg = nVectPackIdx[iSub];
//       int nFaceLoc = faceVectTileIdx[iBeg+iVec+1]-faceVectTileIdx[iBeg+iVec];
//       // printf(" [%i]/[%i] --> nFaceLoc[%i]\n", iSub, iVec, nFaceLoc);
//       dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
//       dim3 blkTopoFaceSdom = dim3(nFaceLoc/nThrs+1,1,1);
//       __computefluxbalu_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(nCellTot, nFace,
//                                                                   faceVectTileIdx[iBeg+iVec  ],
//                                                                   faceVectTileIdx[iBeg+iVec+1],
//                                                                   balance,
//                                                                   surf,
//                                                                   FaceCell);
//     }
//   }

//   // cudaError_t err2 = cudaGetLastError();
//   // if (err2 != cudaSuccess){
//   //   printf("Error: %s\n", cudaGetErrorString(err2));
//   // }
//   // checkCuda( cudaDeviceSynchronize() );
//   for(int iSub = 0; iSub < nSdom; ++iSub){
//     for(int iVec = 0; iVec < nVectBndPack[iSub]; ++iVec){
//       int iBeg = nVectBndPackIdx[iSub];
//       int nFaceLoc = faceVectBndTileIdx[iBeg+iVec+1]-faceVectBndTileIdx[iBeg+iVec];
//       // printf(" [%i]/[%i] --> nFaceLocBnd[%i]\n", iSub, iVec, nFaceLoc);
//       dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
//       dim3 blkTopoFaceSdom = dim3(nFaceLoc/nThrs+1,1,1);
//       __computefluxbalbnd5equ_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(nCellTot, nFace,
//                                                                      faceVectBndTileIdx[iBeg+iVec],
//                                                                      faceVectBndTileIdx[iBeg+iVec+1],
//                                                                      balance,
//                                                                      surf,
//                                                                      FaceCell);
//     }
//   }

//   // cudaError_t err3 = cudaGetLastError();
//   // if (err3 != cudaSuccess){
//   //   printf("Error: %s\n", cudaGetErrorString(err3));
//   // }
//   // checkCuda( cudaDeviceSynchronize() );

//   // int bytes = nCellTot * sizeof(double);
//   // std::vector<double> check(nCellTot);
//   // checkCuda( cudaMemcpy(check.data(), balance, bytes, cudaMemcpyDeviceToHost) );
//   // for(int i = 0; i < nCellTot; ++i){
//   //   printf("cpu check[%i] = [%f] \n", i, check[i]);
//   // }


// }

    // if( il == 127 || il == 0){
    //   printf("tx %i --> [%i] debug:: begFace::[%i] --> [%f/%f/%f/%f/%f]\n", tx, il, begFace,
    //                                 flux[tx],
    //                                 flux[tx+nFace],
    //                                 flux[tx+2*nFace],
    //                                 flux[tx+3*nFace],
    //                                 flux[tx+4*nFace]);

    // }
    // Suppose to do without conflict ... Caution with multple Kernel launch
