float sub1(float *x, float y, float *z) {
  *x = 3.7*y ;
  *z = *z + 2*y ;
  y = y*2.1 + 1.0;
  return y**z ;
}

void top(float a, float *b) {
  float x,y,z ;
  if (a>0.0) {
    y = 2.0*a ;
    z = a-7.0 ;
    y = y*sub1(&x,y,&z) ;
    *b = x*y*z ;
  }
  a = -2.9 ;
}

int main() {
  float a = 1.1 ;
  float b = 2.5 ;
  top(a, &b) ;
}
