// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
// __global__ void __initfluxbalu_gpu(const int n_cellt, double* __restrict__ balance)
// {
//   int tx = threadIdx.x + blockIdx.x * blockDim.x;
//   if( tx < n_cellt){
//     balance[tx] = 0.;
//   };
// }

// ===========================================================================
__global__ void __initfluxbal5equ_gpu(const int n_cellt, double* __restrict__ balance)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < n_cellt) {
    balance[tx          ] = 0.;
    balance[tx+  n_cellt] = 0.;
    balance[tx+2*n_cellt] = 0.;
    balance[tx+3*n_cellt] = 0.;
    balance[tx+4*n_cellt] = 0.;
    if (tx == 0) {
      printf("__initfluxbal5equ_gpu tx=%i, n_cellt=%i\n", tx, n_cellt);
      printf("__initfluxbal5equ_gpu tx=%i, balance[tx]=%f\n", tx, balance[tx]);
    }
  }
}

// ===========================================================================
__global__ void __computefluxbal5equ_gpu(const int                  beg_face,
                                         const int                  end_face,
                                         const int                  n_cellt,
                                         const int                  n_face,
                                         const int*    __restrict__ face_cell,
                                         const double* __restrict__ flux,
                                               double* __restrict__ balance)
{
  int tx = beg_face + threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < end_face) {
    int il = face_cell[tx      ];
    int ir = face_cell[tx+n_face];
    // Suppose to do without conflict ... Caution with multple Kernel launch
    balance[il          ] += flux[tx        ];
    balance[ir          ] -= flux[tx        ];
    balance[il+  n_cellt] += flux[tx+  n_face];
    balance[ir+  n_cellt] -= flux[tx+  n_face];
    balance[il+2*n_cellt] += flux[tx+2*n_face];
    balance[ir+2*n_cellt] -= flux[tx+2*n_face];
    balance[il+3*n_cellt] += flux[tx+3*n_face];
    balance[ir+3*n_cellt] -= flux[tx+3*n_face];
    balance[il+4*n_cellt] += flux[tx+4*n_face];
    balance[ir+4*n_cellt] -= flux[tx+4*n_face];
    if ((il == 0) || (ir == 0))
    {
      printf("__computefluxbal5equ_gpu beg_face=%i, end_face=%i, tx=%i\n", beg_face, end_face, tx);
      printf("__computefluxbal5equ_gpu: balance=%p\n", balance);
      printf("beg_face=%i, end_face=%i\n", beg_face, end_face);
      printf("n_cellt=%i, n_face=%i\n", n_cellt, n_face);
      printf("il=%i, ir=%i\n\n", il, ir);

      printf("flux[tx          ] =%f\n", flux[tx          ]);
      printf("flux[tx+1*n_face] =%f\n", flux[tx+1*n_face]);
      printf("flux[tx+2*n_face] =%f\n", flux[tx+2*n_face]);
      printf("flux[tx+3*n_face] =%f\n", flux[tx+3*n_face]);
      printf("flux[tx+4*n_face] =%f\n\n", flux[tx+4*n_face]);

      printf("balance[il          ] =%f\n", balance[il          ]);
      printf("balance[il+1*n_cellt] =%f\n", balance[il+1*n_cellt]);
      printf("balance[il+2*n_cellt] =%f\n", balance[il+2*n_cellt]);
      printf("balance[il+3*n_cellt] =%f\n", balance[il+3*n_cellt]);
      printf("balance[il+4*n_cellt] =%f\n", balance[il+4*n_cellt]);
      printf("balance[ir          ] =%f\n", balance[ir          ]);
      printf("balance[ir+1*n_cellt] =%f\n", balance[ir+1*n_cellt]);
      printf("balance[ir+2*n_cellt] =%f\n", balance[ir+2*n_cellt]);
      printf("balance[ir+3*n_cellt] =%f\n", balance[ir+3*n_cellt]);
      printf("balance[ir+4*n_cellt] =%f\n\n", balance[ir+4*n_cellt]);
    }
  }
}

// ===========================================================================
__global__ void __computefluxbalbnd5equ_gpu(const int                  beg_face,
                                            const int                  end_face,
                                            const int                  n_cellt,
                                            const int                  n_face,
                                            const int*    __restrict__ face_cell,
                                            const double* __restrict__ flux,
                                                  double* __restrict__ balance)
{
  int tx = beg_face + threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < end_face) {
    int il = face_cell[tx];

    balance[il          ] += flux[tx         ];
    balance[il+  n_cellt] += flux[tx+  n_face];
    balance[il+2*n_cellt] += flux[tx+2*n_face];
    balance[il+3*n_cellt] += flux[tx+3*n_face];
    balance[il+4*n_cellt] += flux[tx+4*n_face];

    if (il == 0)
    {
      printf("BND:__computefluxbalbnd5equ_gpu beg_face=%i, end_face=%i, tx=%i\n", beg_face, end_face, tx);
      printf("BND:beg_face=%i, end_face=%i\n", beg_face, end_face);
      printf("BND:n_cellt=%i, n_face=%i\n", n_cellt, n_face);
      printf("BND:il=%i\n\n", il);

      printf("BND:flux[tx          ] =%f\n", flux[tx          ]);
      printf("BND:flux[tx+1*n_face] =%f\n", flux[tx+1*n_face]);
      printf("BND:flux[tx+2*n_face] =%f\n", flux[tx+2*n_face]);
      printf("BND:flux[tx+3*n_face] =%f\n", flux[tx+3*n_face]);
      printf("BND:flux[tx+4*n_face] =%f\n\n", flux[tx+4*n_face]);

      printf("BND:balance[il          ] =%f\n", balance[il          ]);
      printf("BND:balance[il+1*n_cellt] =%f\n", balance[il+1*n_cellt]);
      printf("BND:balance[il+2*n_cellt] =%f\n", balance[il+2*n_cellt]);
      printf("BND:balance[il+3*n_cellt] =%f\n", balance[il+3*n_cellt]);
      printf("BND:balance[il+4*n_cellt] =%f\n", balance[il+4*n_cellt]);
    }
  }
}

// ===========================================================================
__host__ void computefluxbalu_gpu(const int&    n_sdom,
                                  const int*    nVectPack,
                                  const int*    nVectPackIdx,
                                  const int*    faceVectTileIdx,
                                  const int*    nVectBndPack,
                                  const int*    nVectBndPackIdx,
                                  const int*    faceVectBndTileIdx,
                                  const int&    n_cellt,
                                  const int&    n_face,
                                  const int*    face_cell,
                                  const double* flux,
                                        double* balance)
{
  printf("computefluxbalu_gpu n_cellt=%i / n_face=%i \n", n_cellt, n_face);
  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoCellSdom = dim3(nThrs           ,1,1);
  dim3 blkTopoCellSdom = dim3(n_cellt/nThrs+1,1,1);

  printf("computefluxbalu_gpu: flux=%p balance=%p\n", flux, balance);

  __initfluxbal5equ_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cellt, balance);
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("computefluxbalu_gpu::s1::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );

  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    // printf(" [i_sub=%i]\n", i_sub);
    // printf(" [nVectPack[i_sub]=%i]\n", nVectPack[i_sub]);
    for(int i_vec = 0; i_vec < nVectPack[i_sub]; ++i_vec){
      int i_beg = nVectPackIdx[i_sub];
      int n_faceLoc = faceVectTileIdx[i_beg+i_vec+1]-faceVectTileIdx[i_beg+i_vec];
      // printf(" [i_sub=%i]/[i_vec=%i] -->i_beg=%i, n_faceLoc=%i\n", i_sub, i_vec, i_beg, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __computefluxbal5equ_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(faceVectTileIdx[i_beg+i_vec  ],
                                                                     faceVectTileIdx[i_beg+i_vec+1],
                                                                     n_cellt,
                                                                     n_face,
                                                                     face_cell,
                                                                     flux,
                                                                     balance);
    }
  }
  cudaError_t err2 = cudaGetLastError();
  if (err2 != cudaSuccess){
    printf("computefluxbalu_gpu::s2::Error: %s\n", cudaGetErrorString(err2));
  }
  checkCuda( cudaDeviceSynchronize() );

  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    // printf(" [i_sub=%i]\n", i_sub);
    // printf(" [nVectBndPack[i_sub]=%i]\n", nVectBndPack[i_sub]);
    for(int i_vec = 0; i_vec < nVectBndPack[i_sub]; ++i_vec){
      int i_beg = nVectBndPackIdx[i_sub];
      int n_faceLoc = faceVectBndTileIdx[i_beg+i_vec+1]-faceVectBndTileIdx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_faceLocBnd[%i]\n", i_sub, i_vec, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __computefluxbalbnd5equ_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(faceVectBndTileIdx[i_beg+i_vec],
                                                                        faceVectBndTileIdx[i_beg+i_vec+1],
                                                                        n_cellt, n_face,
                                                                        face_cell,
                                                                        flux,
                                                                        balance);
    }
  }
  cudaError_t err3 = cudaGetLastError();
  if (err3 != cudaSuccess){
    printf("computefluxbalu_gpu::s3::Error: %s\n", cudaGetErrorString(err3));
  }
  checkCuda( cudaDeviceSynchronize() );
  // int bytes = n_cellt * sizeof(double)*5;
  // std::vector<double> check(n_cellt*5);
  // checkCuda( cudaMemcpy(check.data(), balance, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < n_cellt; ++i){
  //   printf("cpu check fluxbal [%i] = [%f/%f/%f/%f/%f] \n", i, check[i],
  //                                                             check[i+  n_cellt],
  //                                                             check[i+2*n_cellt],
  //                                                             check[i+3*n_cellt],
  //                                                             check[i+4*n_cellt]);
  // }
}
