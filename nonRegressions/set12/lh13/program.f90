! Bug found by Shreyas:
! if FOO if differentiated in split mode,
!  a USE of FOO's module that had a ONLY FOO,
!  generated a ONLY foo_b instead of foo_fwd, foo_bwd.
MODULE MM1
CONTAINS
  REAL FUNCTION FOO(x)
    REAL x
    x = sin(x)
    FOO = x*x
  END FUNCTION FOO
END MODULE MM1

SUBROUTINE TOP(x,y)
  USE MM1, ONLY: FOO
  REAL x
  x = x*y
  !$AD NOCHECKPOINT
  y = FOO(x)
  x = cos(x*y)
  x = FOO(y)
  x = x*x
END SUBROUTINE TOP
