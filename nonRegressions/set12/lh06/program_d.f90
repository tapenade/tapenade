!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (vossbeck01) -  6 Feb 2024 17:37
!
!various bugs related to dimensions of temporary arrays
MODULE MODWP_DIFF
  IMPLICIT NONE
  INTEGER, SAVE :: wp=8
  INTEGER, SAVE :: max_lines=500
  INTEGER, DIMENSION(max_lines) :: ttt

CONTAINS
!  Differentiation of testarg1 in forward (tangent) mode:
!   variations   of useful results: y
!   with respect to varying inputs: x
!   RW status of diff variables: x:in y:out
  SUBROUTINE TESTARG1_D(x, xd, y, yd, ind, i)
    IMPLICIT NONE
    REAL(wp) :: x, y
    REAL(wp) :: xd, yd
    INTEGER :: i, ind(10, 20, 30)
    INTRINSIC FLOAT
    INTEGER :: arg1
    REAL*8 :: temp
    arg1 = ind(i, 1, i)*ind(2, i, i)*ind(i, i, 4)
    temp = FLOAT(arg1)
    yd = temp*xd
    y = temp*x
    CALL GETIN_CHECKCOHE(100)
    yd = ttt(50)*yd
    y = y*ttt(50)
  END SUBROUTINE TESTARG1_D

  SUBROUTINE TESTARG1(x, y, ind, i)
    IMPLICIT NONE
    REAL(wp) :: x, y
    INTEGER :: i, ind(10, 20, 30)
    INTRINSIC FLOAT
    INTEGER :: arg1
    arg1 = ind(i, 1, i)*ind(2, i, i)*ind(i, i, 4)
    y = FLOAT(arg1)*x
    CALL GETIN_CHECKCOHE(100)
    y = y*ttt(50)
  END SUBROUTINE TESTARG1

  SUBROUTINE GETIN_CHECKCOHE(nb_lines)
    USE DIFFSIZES
!  Hint: ISIZE1OFabsINgetin_checkcohe should be the value of nb_lines-line
    IMPLICIT NONE
    INTEGER :: line, nb_lines
    INTRINSIC ABS
    INTRINSIC COUNT
    INTEGER, DIMENSION(ISIZE1OFabsINgetin_checkcohe) :: abs0
    DO line=1,nb_lines-1
      WHERE (ttt(line+1:nb_lines) - ttt(line) .GE. 0.0) 
        abs0(1:nb_lines-line) = ttt(line+1:nb_lines) - ttt(line)
      ELSEWHERE
        abs0(1:nb_lines-line) = -(ttt(line+1:nb_lines)-ttt(line))
      END WHERE
      WRITE(*, *) 'COUNT : ', COUNT(abs0(1:nb_lines-line) .LT. 1)
      ttt(line) = 1
    END DO
  END SUBROUTINE GETIN_CHECKCOHE

END MODULE MODWP_DIFF

