!various bugs related to dimensions of temporary arrays

module modwp
  integer :: wp=8
  integer :: max_lines=500
  integer, DIMENSION(max_lines) :: ttt

contains

subroutine testarg1(x,y,ind,i)
  REAL(wp) :: x,y
  INTEGER :: i, ind(10,20,30)

  y = FLOAT(ind(i,1,i)*ind(2,i,i)*ind(i,i,4))*x
  CALL getin_checkcohe(100)
  y = y*ttt(50)
end subroutine testarg1

SUBROUTINE getin_checkcohe(nb_lines)
  INTEGER :: line, nb_lines

  DO line=1,nb_lines-1
     WRITE(*,*) 'COUNT : ', &
 &         COUNT(ABS(ttt(line+1:nb_lines)-ttt(line)) < 1)
     ttt(line) = 1
  ENDDO
END SUBROUTINE getin_checkcohe

end module modwp
