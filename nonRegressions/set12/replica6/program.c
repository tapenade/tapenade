#include <stdio.h>
#include <math.h>

typedef struct {
  double c1, c2 ;
} rpair ;

double foo(double u, double *v) {
  *v = *v/u ;
  return 3*u*u ;
}

void top(rpair *pp, double *a, double *b) {
  double c,d;
  c = sin(*a);
  d = foo(pp->c1, b);
  *b += c*d + pp->c2;
}

int main() {
  rpair pp1 ;
  double x,y ;
  pp1.c1 = 1.1 ;
  pp1.c2 = 2.2 ;
  x = 3.3 ;
  y = 4.4 ;
  top(&pp1, &x, &y) ;
  printf("res=%f\n", y) ;
}
