/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (develop) - 27 Jul 2022 15:50
*/
#include <stdio.h>
#include <math.h>
typedef struct {
    double c1, c2;
} rpair;

/*
  Differentiation of foo in forward (tangent) mode (with options diffReplica:2):
   variations   of useful results: *v foo
   with respect to varying inputs: u *v
   Plus diff mem management of: v:in
*/
void foo_d(double u, double udr, double udi, double *v, double *vdr, double *
        vdi, double *foo, double *foodr, double *foodi) {
    double temp;
    temp = *v/u;
    *vdr = (*vdr-temp*udr)/u;
    *vdi = (*vdi-temp*udi)/u;
    *v = temp;
    *foo = 3*u*u;
    *foodr = 3*2*u*udr;
    *foodi = 3*2*u*udi;
}

/*
  Differentiation of top in forward (tangent) mode (with options diffReplica:2):
   variations   of useful results: *b
   with respect to varying inputs: *pp.c1 *pp.c2 *a *b
   RW status of diff variables: pp:(loc) *pp.c1:in *pp.c2:in a:(loc)
                *a:in b:(loc) *b:in-out
   Plus diff mem management of: pp:in a:in b:in
*/
void top_d(rpair *pp, rpair *ppdr, rpair *ppdi, double *a, double *adr, double
        *adi, double *b, double *bdr, double *bdi) {
    double c, d;
    double cdr, ddr;
    double cdi, ddi;
    cdr = cos(*a)*(*adr);
    cdi = cos(*a)*(*adi);
    c = sin(*a);
    foo_d(pp->c1, ppdr->c1, ppdi->c1, b, bdr, bdi, &d, &ddr, &ddi);
    *bdr = *bdr + d*cdr + c*ddr + ppdr->c2;
    *bdi = *bdi + d*cdi + c*ddi + ppdi->c2;
    *b += c*d + pp->c2;
}
