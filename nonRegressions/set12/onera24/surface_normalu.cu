// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __computesurfnormalu_gpu(const int n_vtx,
                                         const int n_face,
                                         const int*    __restrict__ face_vtx_array,
                                         const int*    __restrict__ face_vtx_idx,
                                         const double* __restrict__ coords,
                                         double* __restrict__ surf,
                                         double* __restrict__ surfNormL2)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < n_face) {
    if (tx == 0) {
      printf("__computesurfnormalu_gpu tx=%i [n_vtx=%i, n_face=%i, coords=%p, surf=%p]\n", tx, n_vtx, n_face, coords, surf);
    }

    int begvtx = face_vtx_idx[tx  ];
    int endvtx = face_vtx_idx[tx+1];

    int nbtrion_face = endvtx-begvtx;
    // printf("tx=%i [begvtx=%i, endvtx=%i]\n", tx, begvtx, endvtx);
    // printf("tx=%i [nbtrion_face=%i]\n", tx, nbtrion_face);

    double nx = 0.;
    double ny = 0.;
    double nz = 0.;

    for(int pp = 1; pp < nbtrion_face+1; ++pp){
      int adrvtx1 = begvtx +   pp - 1;
      int adrvtx2 = begvtx + ( pp     )%nbtrion_face;
      int adrvtx3 = begvtx + ( pp + 1 )%nbtrion_face;
      // printf("tx %i [%i] --> [%i/%i/%i]\n", pp, adrvtx1, adrvtx2, adrvtx3);

      int indvtx1 = face_vtx_array[adrvtx1]-1;
      int indvtx2 = face_vtx_array[adrvtx2]-1;
      int indvtx3 = face_vtx_array[adrvtx3]-1;
      // printf("tx %i [%i] --> [%i/%i/%i]\n", pp, indvtx1, indvtx2, indvtx3);

      // Uncoalesend access
      double ux = coords[indvtx2       ]  - coords[indvtx1        ];
      double uy = coords[indvtx2+  n_vtx] - coords[indvtx1+  n_vtx];
      double uz = coords[indvtx2+2*n_vtx] - coords[indvtx1+2*n_vtx];
      // printf("tx %i [%i] --> [ux=%f,uy=%f,uz=%f]\n", pp, ux, uy, uz);

      double vx = coords[indvtx3       ]  - coords[indvtx2        ];
      double vy = coords[indvtx3+  n_vtx] - coords[indvtx2+  n_vtx];
      double vz = coords[indvtx3+2*n_vtx] - coords[indvtx2+2*n_vtx];
      // printf("tx %i [%i] --> [vx=%f,vy=%f,vz=%f]\n", pp, vx, vy, vz);

      nx += 0.5 * (uy * vz - uz * vy);
      ny += 0.5 * (uz * vx - ux * vz);
      nz += 0.5 * (ux * vy - uy * vx);
      // printf("tx %i [%i] --> [nx=%f,ny=%f,nz=%f]\n", pp, nx, ny, nz);
    }

    nx = 0.5 * nx;
    ny = 0.5 * ny;
    nz = 0.5 * nz;

    surf[tx         ] = nx;
    surf[tx+  n_face] = ny;
    surf[tx+2*n_face] = nz;
    surfNormL2[tx] = sqrt(nx*nx + ny*ny + nz*nz);
    // printf("tx=%i --> surf=[%f/%f/%f], surfnorm=%f\n", tx, nx, ny, nz, surfNormL2[tx]);
  }
}

// ===========================================================================
__host__ void computesurfnormalu_gpu(const int&    n_vtx,
                                     const int&    n_face,
                                     const int*    face_vtx_array,
                                     const int*    face_vtx_idx,
                                     const double* coords,
                                     const double* /*center_face*/,
                                           double* surface,
                                           double* surfNormL2)
{
  printf("begin computesurfnormalu_gpu(n_face=%i) \n", n_vtx);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  int nBlks = n_face/nThrs+1;
  dim3 thrTopoSdom = dim3(nThrs,1,1);
  dim3 blkTopoSdom = dim3(nBlks,1,1);
  // printf("coords = %p \n", coords);
  // printf("surface    = %p \n", surface);
  // printf("surfNormL2 = %p \n", surfNormL2);

  __computesurfnormalu_gpu<<<blkTopoSdom, thrTopoSdom>>>(n_vtx, n_face,
                                                         face_vtx_array,
                                                         face_vtx_idx,
                                                         coords,
                                                         surface,
                                                         surfNormL2);

  // cudaError_t err = cudaGetLastError();
  // if (err != cudaSuccess){
  //   printf("Error: %s\n", cudaGetErrorString(err));
  // }
  // checkCuda( cudaDeviceSynchronize() );
  printf("end computesurfnormalu_gpu(n_face=%i) \n", n_face);
}
