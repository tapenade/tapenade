      subroutine tata ( i,a,b,c,d)
      real a(10), b, c(20),d
      INTEGER i
      ! There were bugs in ExpressionDifferentiator for this code:
      ! scalar d is spread then reduced, hence db += 20*tempb!
      a(i) = SUM(b*c(:)+d+1.5)*b
      end
