//        Generated by TAPENADE     (INRIA, Ecuador team)
//  Tapenade 3.16 (feature_cuda) - 31 May 2022 19:34
//
#include <adStack.h>
// ------------------------------------------------------------------
// External include
// ------------------------------------------------------------------
//#include <iostream>
// ------------------------------------------------------------------
// Internal include
//#include "tmo/cuda_utilities.hpp"
// llh patch:
#include "cuda_utilities.h"

//  Differentiation of __computeroefluxo1_gpu in reverse (adjoint) mode:
//   gradient     of useful results: *temp pctrad *flux *surf *velo
//                rgaz *rho gam
//   with respect to varying inputs: *temp pctrad *flux *surf *velo
//                rgaz *rho gam
//   Plus diff mem management of: temp:in flux:in surf:in velo:in
//                rho:in
// ------------------------------------------------------------------
// ===========================================================================
__global__ void __computeroefluxo1_gpu_b(const int nCellTot, const int nFace, 
        const double gam, double *gamb, const double rgaz, double *rgazb, 
        const double cv, const double cp, const double pctrad, double *pctradb
        , const int harten_type, double *__restrict__ rho, double *rhob, 
        double *__restrict__ velo, double *velob, double *__restrict__ temp, 
        double *tempb3, double *__restrict__ flux, double *fluxb, double *
        __restrict__ surf, double *surfb, int *__restrict__ FaceCell) {
    int tx = threadIdx.x + blockIdx.x*blockDim.x;
    double gam1 = gam - 1.;
    double gam1b = 0.0;
    double gam1_1 = 1./gam1;
    double gam1_1b = 0.0;
    if (tx < nFace) {
        // printf("tx %i [%i] --> [%i] [%f] \n", tx, nFace, FaceVtxIdx[tx], coords[4] );
        int il = FaceCell[tx];
        int ir = FaceCell[tx + nFace];
        double sc1 = surf[tx];
        double sc1b = 0.0;
        double sc2 = surf[tx + nFace];
        double sc2b = 0.0;
        double sc3 = surf[tx + 2*nFace];
        double sc3b = 0.0;
        double sn;
        double snb;
        double max0;
        double max0b;
        double max1;
        double max1b;
        double max2;
        double max2b;
        double abs0;
        double abs0b;
        double abs1;
        double abs1b;
        double abs2;
        double abs2b;
        double abs3;
        double abs3b;
        double tempb;
        double tempb0;
        double temp0;
        double tempb1;
        double lambda1b;
        double lambda4b;
        double lambda5b;
        double tempb2;
        int ad_branch;
        int ad_branch0;
        int ad_branch1;
        int ad_branch2;
        int ad_branch3;
        int ad_branch4;
        int ad_branch5;
        int ad_branch6;
        int ad_branch7;
        int ad_branch8;
        sn = sqrt(sc1*sc1 + sc2*sc2 + sc3*sc3);
        // double invsn = ONE/max(sn,1.e-32)
        double invsn = 1./sn;
        double invsnb = 0.0;
        double nx = sc1*invsn;
        double nxb = 0.0;
        double ny = sc2*invsn;
        double nyb = 0.0;
        double nz = sc3*invsn;
        double nzb = 0.0;
        double wfl1 = rho[il];
        double wfl1b = 0.0;
        double wfr1 = rho[ir];
        double wfr1b = 0.0;
        double wfl2 = velo[il];
        double wfl2b = 0.0;
        double wfr2 = velo[ir];
        double wfr2b = 0.0;
        double wfl3 = velo[il + nCellTot];
        double wfl3b = 0.0;
        double wfr3 = velo[ir + nCellTot];
        double wfr3b = 0.0;
        double wfl4 = velo[il + 2*nCellTot];
        double wfl4b = 0.0;
        double wfr4 = velo[ir + 2*nCellTot];
        double wfr4b = 0.0;
        double wfl5 = temp[il];
        double wfl5b = 0.0;
        double wfr5 = temp[ir];
        double wfr5b = 0.0;
        double pm = wfl1*wfl5*rgaz;
        double pmb = 0.0;
        double pp = wfr1*wfr5*rgaz;
        double ppb = 0.0;
        double hm = gam*gam1_1*wfl5*rgaz + 0.5*(wfl2*wfl2+wfl3*wfl3+wfl4*wfl4)
        ;
        double hmb = 0.0;
        double hp = gam*gam1_1*wfr5*rgaz + 0.5*(wfr2*wfr2+wfr3*wfr3+wfr4*wfr4)
        ;
        double hpb = 0.0;
        if (wfr1/wfl1 < 1.e-12) {
            ad_branch = 0;
            max0 = 1.e-12;
        } else {
            max0 = wfr1/wfl1;
            ad_branch = 1;
        }
        double r;
        double rb;
        r = sqrt(max0);
        if (wfr1*wfl1 < 1.e-12) {
            ad_branch0 = 0;
            max1 = 1.e-12;
        } else {
            max1 = wfr1*wfl1;
            ad_branch0 = 1;
        }
        double rr;
        double rrb;
        rr = sqrt(max1);
        double oneonrplusone = 1./(r+1.);
        double oneonrplusoneb = 0.0;
        double uu = (wfr2*r+wfl2)*oneonrplusone;
        double uub = 0.0;
        double vv = (wfr3*r+wfl3)*oneonrplusone;
        double vvb = 0.0;
        double ww = (wfr4*r+wfl4)*oneonrplusone;
        double wwb = 0.0;
        double ee = 0.5*(uu*uu+vv*vv+ww*ww);
        double eeb = 0.0;
        double hh = (hp*r+hm)*oneonrplusone;
        double hhb = 0.0;
        if (gam1*(hh-ee) < 1.e-12) {
            max2 = 1.e-12;
            ad_branch1 = 0;
        } else {
            max2 = gam1*(hh-ee);
            ad_branch1 = 1;
        }
        double cc;
        double ccb;
        cc = sqrt(max2);
        double vn = uu*nx + vv*ny + ww*nz;
        double vnb = 0.0;
        if (vn >= 0.) {
            lambda1 = vn;
            ad_branch2 = 0;
        } else {
            lambda1 = -vn;
            ad_branch2 = 1;
        }
        if (vn + cc >= 0.) {
            lambda4 = vn + cc;
            ad_branch3 = 0;
        } else {
            lambda4 = -(vn+cc);
            ad_branch3 = 1;
        }
        if (vn - cc >= 0.) {
            lambda5 = vn - cc;
            ad_branch4 = 0;
        } else {
            lambda5 = -(vn-cc);
            ad_branch4 = 1;
        }
        if (uu >= 0.) {
            abs0 = uu;
            ad_branch5 = 1;
        } else {
            abs0 = -uu;
            ad_branch5 = 0;
        }
        if (vv >= 0.) {
            abs1 = vv;
            ad_branch6 = 1;
        } else {
            abs1 = -vv;
            ad_branch6 = 0;
        }
        if (ww >= 0.) {
            abs2 = ww;
            ad_branch7 = 1;
        } else {
            abs2 = -ww;
            ad_branch7 = 0;
        }
        if (vn >= 0.) {
            abs3 = vn;
            ad_branch8 = 0;
        } else {
            abs3 = -vn;
            ad_branch8 = 1;
        }
        double small = (2-harten_type)*pctrad*(abs0+abs1+abs2+cc) + (
        harten_type-1)*pctrad*(abs3+cc);
        double smallb = 0.0;
        double q1 = 0.5 + copysign(0.5, lambda1 - small);
        double k1 = 0.5 - copysign(0.5, lambda1 - small);
        double q4 = 0.5 + copysign(0.5, lambda4 - small);
        double k4 = 0.5 - copysign(0.5, lambda4 - small);
        double q5 = 0.5 + copysign(0.5, lambda5 - small);
        double k5 = 0.5 - copysign(0.5, lambda5 - small);
        double oneonsmall = 1.0/small;
        double oneonsmallb = 0.0;
        double mask = 0.5 + copysign(0.5, small - 1.e-32);
        double aa1 = mask*(q1*lambda1+k1*0.5*(lambda1*lambda1+small*small)*
        oneonsmall) + (1.-mask)*lambda1;
        double aa1b = 0.0;
        double aa4 = mask*(q4*lambda4+k4*0.5*(lambda4*lambda4+small*small)*
        oneonsmall) + (1.-mask)*lambda4;
        double aa4b = 0.0;
        double aa5 = mask*(q5*lambda5+k5*0.5*(lambda5*lambda5+small*small)*
        oneonsmall) + (1.-mask)*lambda5;
        double aa5b = 0.0;
        double du = wfr2 - wfl2;
        double dub = 0.0;
        double dv = wfr3 - wfl3;
        double dvb = 0.0;
        double dw = wfr4 - wfl4;
        double dwb = 0.0;
        double dvn = du*nx + dv*ny + dw*nz;
        double dvnb = 0.0;
        double oneonccsquared = 1./(cc*cc);
        double oneonccsquaredb = 0.0;
        double dd1 = wfr1 - wfl1 - (pp-pm)*oneonccsquared;
        double dd1b = 0.0;
        double dd4 = 0.5*(pp-pm+rr*cc*dvn)*oneonccsquared;
        double dd4b = 0.0;
        double dd5 = 0.5*(pp-pm-rr*cc*dvn)*oneonccsquared;
        double dd5b = 0.0;
        double df11 = aa1*dd1;
        double df11b = 0.0;
        double df12 = aa1*(dd1*uu+rr*(du-nx*dvn));
        double df12b = 0.0;
        double df13 = aa1*(dd1*vv+rr*(dv-ny*dvn));
        double df13b = 0.0;
        double df14 = aa1*(dd1*ww+rr*(dw-nz*dvn));
        double df14b = 0.0;
        double df15 = aa1*(dd1*ee+rr*(uu*du+vv*dv+ww*dw-vn*dvn));
        double df15b = 0.0;
        double df451 = aa4*dd4 + aa5*dd5;
        double df451b = 0.0;
        double df452 = aa4*dd4*(uu+nx*cc) + aa5*dd5*(uu-nx*cc);
        double df452b = 0.0;
        double df453 = aa4*dd4*(vv+ny*cc) + aa5*dd5*(vv-ny*cc);
        double df453b = 0.0;
        double df454 = aa4*dd4*(ww+nz*cc) + aa5*dd5*(ww-nz*cc);
        double df454b = 0.0;
        double df455 = aa4*dd4*(hh+vn*cc) + aa5*dd5*(hh-vn*cc);
        double df455b = 0.0;
        double fcdx1 = wfr1*wfr2 + wfl1*wfl2;
        double fcdx1b = 0.0;
        double fcdy1 = wfr1*wfr3 + wfl1*wfl3;
        double fcdy1b = 0.0;
        double fcdz1 = wfr1*wfr4 + wfl1*wfl4;
        double fcdz1b = 0.0;
        double fcdx2 = wfr1*wfr2*wfr2 + pp + wfl1*wfl2*wfl2 + pm;
        double fcdx2b = 0.0;
        double fcdy2 = wfr1*wfr2*wfr3 + wfl1*wfl2*wfl3;
        double fcdy2b = 0.0;
        double fcdz2 = wfr1*wfr2*wfr4 + wfl1*wfl2*wfl4;
        double fcdz2b = 0.0;
        double fcdx3 = fcdy2;
        double fcdx3b = 0.0;
        double fcdy3 = wfr1*wfr3*wfr3 + pp + wfl1*wfl3*wfl3 + pm;
        double fcdy3b = 0.0;
        double fcdz3 = wfr1*wfr3*wfr4 + wfl1*wfl3*wfl4;
        double fcdz3b = 0.0;
        double fcdx4 = fcdz2;
        double fcdx4b = 0.0;
        double fcdy4 = fcdz3;
        double fcdy4b = 0.0;
        double fcdz4 = wfr1*wfr4*wfr4 + pp + wfl1*wfl4*wfl4 + pm;
        double fcdz4b = 0.0;
        double fcdx5 = wfr2*wfr1*hp + wfl2*wfl1*hm;
        double fcdx5b = 0.0;
        double fcdy5 = wfr3*wfr1*hp + wfl3*wfl1*hm;
        double fcdy5b = 0.0;
        double fcdz5 = wfr4*wfr1*hp + wfl4*wfl1*hm;
        double fcdz5b = 0.0;
        snb = (fcdx5*nx+fcdy5*ny+fcdz5*nz-df15-df455)*0.5*fluxb[tx+4*nFace];
        tempb2 = sn*0.5*fluxb[tx+4*nFace];
        fluxb[tx + 4*nFace] = 0.0;
        fcdx5b = nx*tempb2;
        nxb = fcdx5*tempb2;
        fcdy5b = ny*tempb2;
        nyb = fcdy5*tempb2;
        fcdz5b = nz*tempb2;
        nzb = fcdz5*tempb2;
        df15b = -tempb2;
        df455b = -tempb2;
        snb = snb + (fcdx4*nx+fcdy4*ny+fcdz4*nz-df14-df454)*0.5*fluxb[tx+3*
            nFace];
        tempb2 = sn*0.5*fluxb[tx+3*nFace];
        fluxb[tx + 3*nFace] = 0.0;
        fcdx4b = nx*tempb2;
        nxb = nxb + fcdx4*tempb2;
        fcdy4b = ny*tempb2;
        nyb = nyb + fcdy4*tempb2;
        fcdz4b = nz*tempb2;
        nzb = nzb + fcdz4*tempb2;
        df14b = -tempb2;
        df454b = -tempb2;
        snb = snb + (fcdx3*nx+fcdy3*ny+fcdz3*nz-df13-df453)*0.5*fluxb[tx+2*
            nFace];
        tempb2 = sn*0.5*fluxb[tx+2*nFace];
        fluxb[tx + 2*nFace] = 0.0;
        fcdx3b = nx*tempb2;
        nxb = nxb + fcdx3*tempb2;
        fcdy3b = ny*tempb2;
        nyb = nyb + fcdy3*tempb2;
        fcdz3b = nz*tempb2 + fcdy4b;
        nzb = nzb + fcdz3*tempb2;
        df13b = -tempb2;
        df453b = -tempb2;
        snb = snb + (fcdx2*nx+fcdy2*ny+fcdz2*nz-df12-df452)*0.5*fluxb[tx+nFace
            ];
        tempb2 = sn*0.5*fluxb[tx+nFace];
        fluxb[tx + nFace] = 0.0;
        fcdx2b = nx*tempb2;
        nxb = nxb + fcdx2*tempb2;
        fcdy2b = ny*tempb2 + fcdx3b;
        nyb = nyb + fcdy2*tempb2;
        fcdz2b = nz*tempb2 + fcdx4b;
        nzb = nzb + fcdz2*tempb2;
        df12b = -tempb2;
        df452b = -tempb2;
        snb = snb + (fcdx1*nx+fcdy1*ny+fcdz1*nz-df11-df451)*0.5*fluxb[tx];
        tempb2 = sn*0.5*fluxb[tx];
        fluxb[tx] = 0.0;
        fcdx1b = nx*tempb2;
        nxb = nxb + fcdx1*tempb2;
        fcdy1b = ny*tempb2;
        fcdz1b = nz*tempb2;
        df11b = -tempb2;
        df451b = -tempb2;
        hpb = wfr4*wfr1*fcdz5b + wfr3*wfr1*fcdy5b + wfr2*wfr1*fcdx5b;
        hmb = wfl4*wfl1*fcdz5b + wfl3*wfl1*fcdy5b + wfl2*wfl1*fcdx5b;
        tempb1 = aa4*dd4*df455b;
        tempb0 = (hh-vn*cc)*df455b;
        tempb = aa5*dd5*df455b;
        hhb = tempb + tempb1;
        vnb = cc*tempb1 - cc*tempb;
        ccb = vn*tempb1 - vn*tempb;
        aa5b = dd5*tempb0;
        dd5b = aa5*tempb0;
        tempb1 = aa4*dd4*df454b;
        tempb0 = (ww-nz*cc)*df454b;
        tempb = aa5*dd5*df454b;
        nzb = nzb + fcdz1*tempb2 + cc*tempb1 - cc*tempb;
        wwb = tempb + tempb1;
        ccb = ccb + nz*tempb1 - nz*tempb;
        aa5b = aa5b + dd5*tempb0;
        dd5b = dd5b + aa5*tempb0;
        tempb1 = aa4*dd4*df453b;
        tempb0 = (vv-ny*cc)*df453b;
        tempb = aa5*dd5*df453b;
        nyb = nyb + fcdy1*tempb2 + cc*tempb1 - cc*tempb;
        tempb2 = (hh+vn*cc)*df455b;
        aa4b = dd4*tempb2;
        dd4b = aa4*tempb2;
        tempb2 = (ww+nz*cc)*df454b;
        aa4b = aa4b + dd4*tempb2;
        dd4b = dd4b + aa4*tempb2;
        tempb2 = (vv+ny*cc)*df453b;
        vvb = tempb + tempb1;
        ccb = ccb + ny*tempb1 - ny*tempb;
        tempb1 = (uu+nx*cc)*df452b;
        aa4b = aa4b + dd4*tempb2 + dd4*tempb1 + dd4*df451b;
        dd4b = dd4b + aa4*tempb2 + aa4*tempb1 + aa4*df451b;
        tempb = (uu-nx*cc)*df452b;
        aa5b = aa5b + dd5*tempb0 + dd5*tempb + dd5*df451b;
        dd5b = dd5b + aa5*tempb0 + aa5*tempb + aa5*df451b;
        tempb0 = aa4*dd4*df452b;
        tempb2 = aa5*dd5*df452b;
        uub = tempb2 + tempb0;
        nxb = nxb + cc*tempb0 - cc*tempb2;
        ccb = ccb + nx*tempb0 - nx*tempb2;
        temp0 = uu*du + vv*dv + ww*dw - vn*dvn;
        aa1b = (dd1*ee+rr*temp0)*df15b + (dd1*ww+rr*(dw-nz*dvn))*df14b + (dd1*
            vv+rr*(dv-ny*dvn))*df13b + (dd1*uu+rr*(du-nx*dvn))*df12b + dd1*
            df11b;
        tempb0 = aa1*df15b;
        eeb = dd1*tempb0;
        tempb1 = rr*tempb0;
        uub = uub + du*tempb1;
        dub = uu*tempb1;
        vvb = vvb + dv*tempb1;
        dvb = vv*tempb1;
        wwb = wwb + dw*tempb1;
        dwb = ww*tempb1;
        vnb = vnb - dvn*tempb1;
        dvnb = -(vn*tempb1);
        tempb1 = aa1*df14b;
        dd1b = ee*tempb0 + ww*tempb1;
        rrb = temp0*tempb0 + (dw-nz*dvn)*tempb1;
        wwb = wwb + dd1*tempb1;
        tempb0 = rr*tempb1;
        dwb = dwb + tempb0;
        nzb = nzb - dvn*tempb0;
        dvnb = dvnb - nz*tempb0;
        tempb1 = aa1*df13b;
        dd1b = dd1b + vv*tempb1;
        vvb = vvb + dd1*tempb1;
        rrb = rrb + (dv-ny*dvn)*tempb1;
        tempb0 = rr*tempb1;
        dvb = dvb + tempb0;
        nyb = nyb - dvn*tempb0;
        dvnb = dvnb - ny*tempb0;
        tempb1 = aa1*df12b;
        dd1b = dd1b + uu*tempb1 + aa1*df11b;
        wfr1b = wfr4*hp*fcdz5b + wfr3*hp*fcdy5b + wfr2*hp*fcdx5b + wfr4*wfr4*
            fcdz4b + wfr3*wfr4*fcdz3b + wfr3*wfr3*fcdy3b + wfr2*wfr4*fcdz2b + 
            wfr2*wfr3*fcdy2b + wfr2*wfr2*fcdx2b + wfr4*fcdz1b + wfr3*fcdy1b + 
            wfr2*fcdx1b + dd1b;
        wfl1b = wfl4*hm*fcdz5b + wfl3*hm*fcdy5b + wfl2*hm*fcdx5b + wfl4*wfl4*
            fcdz4b + wfl3*wfl4*fcdz3b + wfl3*wfl3*fcdy3b + wfl2*wfl4*fcdz2b + 
            wfl2*wfl3*fcdy2b + wfl2*wfl2*fcdx2b + wfl4*fcdz1b + wfl3*fcdy1b + 
            wfl2*fcdx1b - dd1b;
        uub = uub + dd1*tempb1;
        rrb = rrb + (du-nx*dvn)*tempb1;
        tempb0 = rr*tempb1;
        tempb1 = oneonccsquared*0.5*dd5b;
        ppb = fcdz4b + fcdy3b + fcdx2b + tempb1;
        pmb = fcdz4b + fcdy3b + fcdx2b - tempb1;
        dvnb = dvnb - nx*tempb0 - rr*cc*tempb1;
        oneonccsquaredb = (pp-pm-rr*cc*dvn)*0.5*dd5b + (pp-pm+rr*cc*dvn)*0.5*
            dd4b - (pp-pm)*dd1b;
        rrb = rrb - cc*dvn*tempb1;
        ccb = ccb - rr*dvn*tempb1;
        tempb1 = oneonccsquared*0.5*dd4b;
        ppb = ppb + tempb1 - oneonccsquared*dd1b;
        pmb = pmb + oneonccsquared*dd1b - tempb1;
        rrb = rrb + cc*dvn*tempb1;
        ccb = ccb + rr*dvn*tempb1;
        dvnb = dvnb + rr*cc*tempb1;
        dub = dub + tempb0 + nx*dvnb;
        wfr2b = wfr1*hp*fcdx5b + wfr1*wfr4*fcdz2b + wfr1*wfr3*fcdy2b + 2*wfr2*
            wfr1*fcdx2b + wfr1*fcdx1b + dub;
        wfl2b = wfl1*hm*fcdx5b + wfl1*wfl4*fcdz2b + wfl1*wfl3*fcdy2b + 2*wfl2*
            wfl1*fcdx2b + wfl1*fcdx1b - dub;
        nxb = nxb + du*dvnb - dvn*tempb0;
        dvb = dvb + ny*dvnb;
        wfr3b = wfr1*hp*fcdy5b + wfr1*wfr4*fcdz3b + 2*wfr3*wfr1*fcdy3b + wfr1*
            wfr2*fcdy2b + wfr1*fcdy1b + dvb;
        wfl3b = wfl1*hm*fcdy5b + wfl1*wfl4*fcdz3b + 2*wfl3*wfl1*fcdy3b + wfl1*
            wfl2*fcdy2b + wfl1*fcdy1b - dvb;
        nyb = nyb + dv*dvnb;
        dwb = dwb + nz*dvnb;
        wfr4b = wfr1*hp*fcdz5b + 2*wfr4*wfr1*fcdz4b + wfr1*wfr3*fcdz3b + wfr1*
            wfr2*fcdz2b + wfr1*fcdz1b + dwb;
        wfl4b = wfl1*hm*fcdz5b + 2*wfl4*wfl1*fcdz4b + wfl1*wfl3*fcdz3b + wfl1*
            wfl2*fcdz2b + wfl1*fcdz1b - dwb;
        nzb = nzb + dw*dvnb;
        tempb1 = k5*0.5*mask*aa5b;
        lambda5b = (q5*mask-mask+1.)*aa5b + 2*lambda5*oneonsmall*tempb1;
        smallb = 2*small*oneonsmall*tempb1;
        oneonsmallb = (lambda5*lambda5+small*small)*tempb1;
        tempb1 = k4*0.5*mask*aa4b;
        lambda4b = (q4*mask-mask+1.)*aa4b + 2*lambda4*oneonsmall*tempb1;
        smallb = smallb + 2*small*oneonsmall*tempb1;
        oneonsmallb = oneonsmallb + (lambda4*lambda4+small*small)*tempb1;
        tempb1 = k1*0.5*mask*aa1b;
        lambda1b = (q1*mask-mask+1.)*aa1b + 2*lambda1*oneonsmall*tempb1;
        oneonsmallb = oneonsmallb + (lambda1*lambda1+small*small)*tempb1;
        smallb = smallb + 2*small*oneonsmall*tempb1 - oneonsmallb/(small*small
            );
        tempb1 = pctrad*(2-harten_type)*smallb;
        tempb0 = (harten_type-1)*smallb;
        ccb = ccb + pctrad*tempb0 - 2*oneonccsquaredb/pow(cc, 3) + tempb1;
        //Must be atomic increment
        *pctradb = *pctradb + (abs0+abs1+abs2+cc)*(2-harten_type)*smallb + (
            abs3+cc)*tempb0;
        abs3b = pctrad*tempb0;
        abs0b = tempb1;
        abs1b = tempb1;
        abs2b = tempb1;
        if (ad_branch8 == 0)
            vnb = vnb + abs3b;
        else
            vnb = vnb - abs3b;
        if (ad_branch7 == 0)
            wwb = wwb - abs2b;
        else
            wwb = wwb + abs2b;
        if (ad_branch6 == 0)
            vvb = vvb - abs1b;
        else
            vvb = vvb + abs1b;
        if (ad_branch5 == 0)
            uub = uub - abs0b;
        else
            uub = uub + abs0b;
        if (ad_branch4 == 0) {
            vnb = vnb + lambda5b;
            ccb = ccb - lambda5b;
        } else {
            ccb = ccb + lambda5b;
            vnb = vnb - lambda5b;
        }
        if (ad_branch3 == 0) {
            vnb = vnb + lambda4b;
            ccb = ccb + lambda4b;
        } else {
            vnb = vnb - lambda4b;
            ccb = ccb - lambda4b;
        }
        if (ad_branch2 == 0)
            vnb = vnb + lambda1b;
        else
            vnb = vnb - lambda1b;
        uub = uub + nx*vnb;
        nxb = nxb + uu*vnb;
        vvb = vvb + ny*vnb;
        nyb = nyb + vv*vnb;
        wwb = wwb + nz*vnb;
        nzb = nzb + ww*vnb;
        max2b = (max2 == 0.0 ? 0.0 : ccb/(2.0*sqrt(max2)));
        if (ad_branch1 == 0)
            gam1b = 0.0;
        else {
            gam1b = (hh-ee)*max2b;
            hhb = hhb + gam1*max2b;
            eeb = eeb - gam1*max2b;
        }
        tempb1 = oneonrplusone*hhb;
        hpb = hpb + r*tempb1;
        rb = hp*tempb1;
        hmb = hmb + tempb1;
        tempb1 = 0.5*eeb;
        uub = uub + 2*uu*tempb1;
        vvb = vvb + 2*vv*tempb1;
        wwb = wwb + 2*ww*tempb1;
        oneonrplusoneb = (hp*r+hm)*hhb + (wfr4*r+wfl4)*wwb + (wfr3*r+wfl3)*vvb
            + (wfr2*r+wfl2)*uub;
        tempb1 = oneonrplusone*wwb;
        wfr4b = wfr4b + r*tempb1;
        rb = rb + wfr4*tempb1;
        wfl4b = wfl4b + tempb1;
        tempb1 = oneonrplusone*vvb;
        wfr3b = wfr3b + r*tempb1;
        rb = rb + wfr3*tempb1;
        wfl3b = wfl3b + tempb1;
        tempb1 = oneonrplusone*uub;
        wfr2b = wfr2b + r*tempb1;
        rb = rb + wfr2*tempb1 - oneonrplusoneb/((r+1.)*(r+1.));
        wfl2b = wfl2b + tempb1;
        max1b = (max1 == 0.0 ? 0.0 : rrb/(2.0*sqrt(max1)));
        if (ad_branch0 != 0) {
            wfr1b = wfr1b + wfl1*max1b;
            wfl1b = wfl1b + wfr1*max1b;
        }
        max0b = (max0 == 0.0 ? 0.0 : rb/(2.0*sqrt(max0)));
        if (ad_branch != 0) {
            wfr1b = wfr1b + max0b/wfl1;
            wfl1b = wfl1b - wfr1*max0b/(wfl1*wfl1);
        }
        tempb1 = wfr5*rgaz*hpb;
        tempb0 = gam*gam1_1*hpb;
        tempb = 0.5*hpb;
        wfr2b = wfr2b + 2*wfr2*tempb;
        wfr3b = wfr3b + 2*wfr3*tempb;
        wfr4b = wfr4b + 2*wfr4*tempb;
        wfr5b = rgaz*tempb0 + wfr1*rgaz*ppb;
        //Must be atomic increment
        *rgazb = *rgazb + wfr5*tempb0;
        tempb = wfl5*rgaz*hmb;
        //Must be atomic increment
        *gamb = *gamb + gam1_1*tempb1 + gam1_1*tempb;
        gam1_1b = gam*tempb1 + gam*tempb;
        tempb0 = gam*gam1_1*hmb;
        tempb1 = 0.5*hmb;
        wfl2b = wfl2b + 2*wfl2*tempb1;
        wfl3b = wfl3b + 2*wfl3*tempb1;
        wfl4b = wfl4b + 2*wfl4*tempb1;
        wfl5b = rgaz*tempb0 + wfl1*rgaz*pmb;
        //Must be atomic increment
        *rgazb = *rgazb + wfl5*tempb0 + wfr1*wfr5*ppb + wfl1*wfl5*pmb;
        wfr1b = wfr1b + wfr5*rgaz*ppb;
        wfl1b = wfl1b + wfl5*rgaz*pmb;
        //Must be atomic increment
        tempb3[ir] = tempb3[ir] + wfr5b;
        //Must be atomic increment
        tempb3[il] = tempb3[il] + wfl5b;
        //Must be atomic increment
        velob[ir + 2*nCellTot] = velob[ir + 2*nCellTot] + wfr4b;
        //Must be atomic increment
        velob[il + 2*nCellTot] = velob[il + 2*nCellTot] + wfl4b;
        //Must be atomic increment
        velob[ir + nCellTot] = velob[ir + nCellTot] + wfr3b;
        //Must be atomic increment
        velob[il + nCellTot] = velob[il + nCellTot] + wfl3b;
        //Must be atomic increment
        velob[ir] = velob[ir] + wfr2b;
        //Must be atomic increment
        velob[il] = velob[il] + wfl2b;
        //Must be atomic increment
        rhob[ir] = rhob[ir] + wfr1b;
        //Must be atomic increment
        rhob[il] = rhob[il] + wfl1b;
        invsnb = sc3*nzb + sc2*nyb + sc1*nxb;
        snb = snb - invsnb/(sn*sn);
        tempb = (sc1*sc1 + sc2*sc2 + sc3*sc3 == 0.0 ? 0.0 : snb/(2.0*sqrt(sc1*
            sc1+sc2*sc2+sc3*sc3)));
        sc3b = invsn*nzb + 2*sc3*tempb;
        sc2b = invsn*nyb + 2*sc2*tempb;
        sc1b = invsn*nxb + 2*sc1*tempb;
        //Must be atomic increment
        surfb[tx + 2*nFace] = surfb[tx + 2*nFace] + sc3b;
        //Must be atomic increment
        surfb[tx + nFace] = surfb[tx + nFace] + sc2b;
        //Must be atomic increment
        surfb[tx] = surfb[tx] + sc1b;
    } else {
        gam1b = 0.0;
        gam1_1b = 0.0;
    }
    gam1b = gam1b - gam1_1b/(gam1*gam1);
    //Must be atomic increment
    *gamb = *gamb + gam1b;
}

// ------------------------------------------------------------------
// ===========================================================================
__global__ void __computeroefluxo1_gpu_nodiff(const int nCellTot, const int 
        nFace, const double gam, const double rgaz, const double cv, const 
        double cp, const double pctrad, const int harten_type, double *
        __restrict__ rho, double *__restrict__ velo, double *__restrict__ temp
        , double *__restrict__ flux, double *__restrict__ surf, int *
        __restrict__ FaceCell) {
    int tx = threadIdx.x + blockIdx.x*blockDim.x;
    double gam1 = gam - 1.;
    double gam1_1 = 1./gam1;
    if (tx < nFace) {
        // printf("tx %i [%i] --> [%i] [%f] \n", tx, nFace, FaceVtxIdx[tx], coords[4] );
        int il = FaceCell[tx];
        int ir = FaceCell[tx + nFace];
        double sc1 = surf[tx];
        double sc2 = surf[tx + nFace];
        double sc3 = surf[tx + 2*nFace];
        double sn;
        double max0;
        double max1;
        double max2;
        double abs0;
        double abs1;
        double abs2;
        double abs3;
        sn = sqrt(sc1*sc1 + sc2*sc2 + sc3*sc3);
        // double invsn = ONE/max(sn,1.e-32)
        double invsn = 1./sn;
        double nx = sc1*invsn;
        double ny = sc2*invsn;
        double nz = sc3*invsn;
        double wfl1 = rho[il];
        double wfr1 = rho[ir];
        double wfl2 = velo[il];
        double wfr2 = velo[ir];
        double wfl3 = velo[il + nCellTot];
        double wfr3 = velo[ir + nCellTot];
        double wfl4 = velo[il + 2*nCellTot];
        double wfr4 = velo[ir + 2*nCellTot];
        double wfl5 = temp[il];
        double wfr5 = temp[ir];
        double pm = wfl1*wfl5*rgaz;
        double pp = wfr1*wfr5*rgaz;
        double hm = gam*gam1_1*wfl5*rgaz + 0.5*(wfl2*wfl2+wfl3*wfl3+wfl4*wfl4)
        ;
        double hp = gam*gam1_1*wfr5*rgaz + 0.5*(wfr2*wfr2+wfr3*wfr3+wfr4*wfr4)
        ;
        if (wfr1/wfl1 < 1.e-12)
            max0 = 1.e-12;
        else
            max0 = wfr1/wfl1;
        double r;
        r = sqrt(max0);
        if (wfr1*wfl1 < 1.e-12)
            max1 = 1.e-12;
        else
            max1 = wfr1*wfl1;
        double rr;
        rr = sqrt(max1);
        double oneonrplusone = 1./(r+1.);
        double uu = (wfr2*r+wfl2)*oneonrplusone;
        double vv = (wfr3*r+wfl3)*oneonrplusone;
        double ww = (wfr4*r+wfl4)*oneonrplusone;
        double ee = 0.5*(uu*uu+vv*vv+ww*ww);
        double hh = (hp*r+hm)*oneonrplusone;
        if (gam1*(hh-ee) < 1.e-12)
            max2 = 1.e-12;
        else
            max2 = gam1*(hh-ee);
        double cc;
        cc = sqrt(max2);
        double vn = uu*nx + vv*ny + ww*nz;
        if (vn >= 0.)
            lambda1 = vn;
        else
            lambda1 = -vn;
        if (vn + cc >= 0.)
            lambda4 = vn + cc;
        else
            lambda4 = -(vn+cc);
        if (vn - cc >= 0.)
            lambda5 = vn - cc;
        else
            lambda5 = -(vn-cc);
        if (uu >= 0.)
            abs0 = uu;
        else
            abs0 = -uu;
        if (vv >= 0.)
            abs1 = vv;
        else
            abs1 = -vv;
        if (ww >= 0.)
            abs2 = ww;
        else
            abs2 = -ww;
        if (vn >= 0.)
            abs3 = vn;
        else
            abs3 = -vn;
        double small = (2-harten_type)*pctrad*(abs0+abs1+abs2+cc) + (
        harten_type-1)*pctrad*(abs3+cc);
        double q1 = 0.5 + copysign(0.5, lambda1 - small);
        double k1 = 0.5 - copysign(0.5, lambda1 - small);
        double q4 = 0.5 + copysign(0.5, lambda4 - small);
        double k4 = 0.5 - copysign(0.5, lambda4 - small);
        double q5 = 0.5 + copysign(0.5, lambda5 - small);
        double k5 = 0.5 - copysign(0.5, lambda5 - small);
        double oneonsmall = 1.0/small;
        double mask = 0.5 + copysign(0.5, small - 1.e-32);
        double aa1 = mask*(q1*lambda1+k1*0.5*(lambda1*lambda1+small*small)*
        oneonsmall) + (1.-mask)*lambda1;
        double aa4 = mask*(q4*lambda4+k4*0.5*(lambda4*lambda4+small*small)*
        oneonsmall) + (1.-mask)*lambda4;
        double aa5 = mask*(q5*lambda5+k5*0.5*(lambda5*lambda5+small*small)*
        oneonsmall) + (1.-mask)*lambda5;
        double du = wfr2 - wfl2;
        double dv = wfr3 - wfl3;
        double dw = wfr4 - wfl4;
        double dvn = du*nx + dv*ny + dw*nz;
        double oneonccsquared = 1./(cc*cc);
        double dd1 = wfr1 - wfl1 - (pp-pm)*oneonccsquared;
        double dd4 = 0.5*(pp-pm+rr*cc*dvn)*oneonccsquared;
        double dd5 = 0.5*(pp-pm-rr*cc*dvn)*oneonccsquared;
        double df11 = aa1*dd1;
        double df12 = aa1*(dd1*uu+rr*(du-nx*dvn));
        double df13 = aa1*(dd1*vv+rr*(dv-ny*dvn));
        double df14 = aa1*(dd1*ww+rr*(dw-nz*dvn));
        double df15 = aa1*(dd1*ee+rr*(uu*du+vv*dv+ww*dw-vn*dvn));
        double df451 = aa4*dd4 + aa5*dd5;
        double df452 = aa4*dd4*(uu+nx*cc) + aa5*dd5*(uu-nx*cc);
        double df453 = aa4*dd4*(vv+ny*cc) + aa5*dd5*(vv-ny*cc);
        double df454 = aa4*dd4*(ww+nz*cc) + aa5*dd5*(ww-nz*cc);
        double df455 = aa4*dd4*(hh+vn*cc) + aa5*dd5*(hh-vn*cc);
        double fcdx1 = wfr1*wfr2 + wfl1*wfl2;
        double fcdy1 = wfr1*wfr3 + wfl1*wfl3;
        double fcdz1 = wfr1*wfr4 + wfl1*wfl4;
        double fcdx2 = wfr1*wfr2*wfr2 + pp + wfl1*wfl2*wfl2 + pm;
        double fcdy2 = wfr1*wfr2*wfr3 + wfl1*wfl2*wfl3;
        double fcdz2 = wfr1*wfr2*wfr4 + wfl1*wfl2*wfl4;
        double fcdx3 = fcdy2;
        double fcdy3 = wfr1*wfr3*wfr3 + pp + wfl1*wfl3*wfl3 + pm;
        double fcdz3 = wfr1*wfr3*wfr4 + wfl1*wfl3*wfl4;
        double fcdx4 = fcdz2;
        double fcdy4 = fcdz3;
        double fcdz4 = wfr1*wfr4*wfr4 + pp + wfl1*wfl4*wfl4 + pm;
        double fcdx5 = wfr2*wfr1*hp + wfl2*wfl1*hm;
        double fcdy5 = wfr3*wfr1*hp + wfl3*wfl1*hm;
        double fcdz5 = wfr4*wfr1*hp + wfl4*wfl1*hm;
        flux[tx] = 0.5*sn*(fcdx1*nx+fcdy1*ny+fcdz1*nz-df11-df451);
        flux[tx + nFace] = 0.5*sn*(fcdx2*nx+fcdy2*ny+fcdz2*nz-df12-df452);
        flux[tx + 2*nFace] = 0.5*sn*(fcdx3*nx+fcdy3*ny+fcdz3*nz-df13-df453);
        flux[tx + 3*nFace] = 0.5*sn*(fcdx4*nx+fcdy4*ny+fcdz4*nz-df14-df454);
        flux[tx + 4*nFace] = 0.5*sn*(fcdx5*nx+fcdy5*ny+fcdz5*nz-df15-df455);
    }
}

//  Differentiation of computeroefluxo1_gpu in reverse (adjoint) mode:
//   gradient     of useful results: *temp pctrad *flux *surf *velo
//                rgaz *rho gam
//   with respect to varying inputs: *temp pctrad *flux *surf *velo
//                rgaz *rho gam
//   RW status of diff variables: temp:(loc) *temp:incr pctrad:incr
//                flux:(loc) *flux:in-out surf:(loc) *surf:incr
//                velo:(loc) *velo:incr rgaz:incr rho:(loc) *rho:incr
//                gam:incr
//   Plus diff mem management of: temp:in flux:in surf:in velo:in
//                rho:in
// ===========================================================================
__host__ void computeroefluxo1_gpu_b(int &nCellTot, int &nFace, double &gam, 
        double &gamb, double &rgaz, double &rgazb, double &cv, double &cp, 
        double &pctrad, double &pctradb, int &harten_type, double *rho, double
        *rhob, double *velo, double *velob, double *temp, double *tempb, 
        double *flux, double *fluxb, double *surf, double *surfb, int *
        FaceCell) {
    // printf("computeroefluxo1_gpu %i \n", nCellTot);
    // Target --> Feed Block and Threads
    int nThrs = 64;
    // cudaError_t err = cudaGetLastError();
    // if (err != cudaSuccess){
    //   printf("computeroefluxo1_gpu::Error: %s\n", cudaGetErrorString(err));
    // }
    // checkCuda( cudaDeviceSynchronize() );
    // cudaError_t err2 = cudaGetLastError();
    // if (err2 != cudaSuccess){
    //   printf("Error: %s\n", cudaGetErrorString(err2));
    // }
    // printf("computeroefluxo1_gpu end %i \n", nCellTot);
    // int bytes = nFace * sizeof(double)*5;
    // std::vector<double> check(nFace*5);
    // checkCuda( cudaMemcpy(check.data(), flux, bytes, cudaMemcpyDeviceToHost) );
    // for(int i = 0; i < nFace; ++i){
    //   printf("cpu check computeroefluxo1_gpu [%i] = [%f/%f/%f/%f/%f] \n", i, check[i],
    //                                                             check[i+  nFace],
    //                                                             check[i+2*nFace],
    //                                                             check[i+3*nFace],
    //                                                             check[i+4*nFace]);
    // }
    int nBlks = nFace/nThrs + 1;
    dim3 thrTopoSdom;
    thrTopoSdom = dim3(nThrs, 1, 1);
    dim3 blkTopoSdom;
    blkTopoSdom = dim3(nFace/nThrs + 1, 1, 1);
    __computeroefluxo1_gpu_b<<<blkTopoSdom, thrTopoSdom>>>(nCellTot, nFace, 
                             gam, &gamb, rgaz, &rgazb, cv, cp, pctrad, &
                             pctradb, harten_type, rho, rhob, velo, velob, 
                             temp, tempb, flux, fluxb, surf, surfb, FaceCell);
}
