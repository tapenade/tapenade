// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
//#include "tmo/cuda_utilities.hpp"
// llh patch:
#include "../../cuda_utilities.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __computeroefluxo1_gpu(const int nCellTot,
                                       const int nFace,
                                       const double gam,
                                       const double rgaz,
                                       const double cv,
                                       const double cp,
                                       const double pctrad,
                                       const int   harten_type,
                                       double* __restrict__ rho,
                                       double* __restrict__ velo,
                                       double* __restrict__ temp,
                                       double* __restrict__ flux,
                                       double* __restrict__ surf,
                                       int*    __restrict__ FaceCell)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  double gam1   = gam - 1.;
  double gam1_1 = 1./gam1;

  if( tx < nFace){
    // printf("tx %i [%i] --> [%i] [%f] \n", tx, nFace, FaceVtxIdx[tx], coords[4] );
    int il = FaceCell[tx      ];
    int ir = FaceCell[tx+nFace];

    double sc1 = surf[tx        ];
    double sc2 = surf[tx+  nFace];
    double sc3 = surf[tx+2*nFace];
    double sn  = sqrt(sc1*sc1 + sc2*sc2 + sc3*sc3);

    // double invsn = ONE/max(sn,1.e-32)
    double invsn = 1./sn;
    double nx    = sc1*invsn;
    double ny    = sc2*invsn;
    double nz    = sc3*invsn;

    double wfl1 = rho[il];
    double wfr1 = rho[ir];

    double wfl2 = velo[il];
    double wfr2 = velo[ir];

    double wfl3 = velo[il+nCellTot];
    double wfr3 = velo[ir+nCellTot];

    double wfl4 = velo[il+2*nCellTot];
    double wfr4 = velo[ir+2*nCellTot];

    double wfl5 = temp[il];
    double wfr5 = temp[ir];

    double pm = wfl1*wfl5*rgaz;
    double pp = wfr1*wfr5*rgaz;

    double hm = gam*gam1_1*wfl5*rgaz + 0.5*(wfl2*wfl2 + wfl3*wfl3 + wfl4*wfl4);
    double hp = gam*gam1_1*wfr5*rgaz + 0.5*(wfr2*wfr2 + wfr3*wfr3 + wfr4*wfr4);

    double r  = sqrt(max(wfr1/wfl1, 1.e-12));
    double rr = sqrt(max(wfr1*wfl1, 1.e-12));

    double oneonrplusone = 1./(r+1.);

    double uu = (wfr2*r+wfl2)*oneonrplusone;
    double vv = (wfr3*r+wfl3)*oneonrplusone;
    double ww = (wfr4*r+wfl4)*oneonrplusone;
    double ee = 0.5*(uu*uu + vv*vv + ww*ww);
    double hh = (hp*r+hm)*oneonrplusone;
    double cc = sqrt(max(gam1*(hh-ee), 1.e-12));
    double vn = uu*nx + vv*ny + ww*nz;

    double lambda1 = abs(vn   );
    double lambda4 = abs(vn+cc);
    double lambda5 = abs(vn-cc);

    double small = (2-harten_type)*pctrad*(abs(uu)+abs(vv)+abs(ww)+cc)
                 + (harten_type-1)*pctrad*(abs(vn)+cc);

    double q1 = 0.5 + copysign(0.5,lambda1-small);
    double k1 = 0.5 - copysign(0.5,lambda1-small);
    double q4 = 0.5 + copysign(0.5,lambda4-small);
    double k4 = 0.5 - copysign(0.5,lambda4-small);
    double q5 = 0.5 + copysign(0.5,lambda5-small);
    double k5 = 0.5 - copysign(0.5,lambda5-small);

    double oneonsmall = 1.0/small;
    double mask       = 0.5 + copysign(0.5, small - 1.e-32);

    double aa1 = mask * (  q1*lambda1
                  + k1*0.5*(lambda1*lambda1+small*small)*oneonsmall)
                  + (1.-mask)*lambda1;

    double aa4 = mask * (  q4*lambda4
                 + k4*0.5*(lambda4*lambda4+small*small)*oneonsmall)
                 + (1.-mask)*lambda4;

    double aa5 = mask * (  q5*lambda5
                  + k5*0.5*(lambda5*lambda5+small*small)*oneonsmall)
                  + (1.-mask)*lambda5;

    double du  = wfr2-wfl2;
    double dv  = wfr3-wfl3;
    double dw  = wfr4-wfl4;
    double dvn = du*nx+dv*ny+dw*nz;

    double oneonccsquared = 1. / (cc*cc);

    double dd1 = (wfr1-wfl1)-(pp-pm)*oneonccsquared;
    double dd4 = 0.5*(pp-pm+rr*cc*dvn)*oneonccsquared;
    double dd5 = 0.5*(pp-pm-rr*cc*dvn)*oneonccsquared;

    double df11 = aa1*dd1;
    double df12 = aa1*(dd1*uu+rr*(du-nx*dvn));
    double df13 = aa1*(dd1*vv+rr*(dv-ny*dvn));
    double df14 = aa1*(dd1*ww+rr*(dw-nz*dvn));
    double df15 = aa1*(dd1*ee+rr*(uu*du+vv*dv+ww*dw-vn*dvn));

    double df451 = aa4*dd4            + aa5*dd5;
    double df452 = aa4*dd4*(uu+nx*cc) + aa5*dd5*(uu-nx*cc);
    double df453 = aa4*dd4*(vv+ny*cc) + aa5*dd5*(vv-ny*cc);
    double df454 = aa4*dd4*(ww+nz*cc) + aa5*dd5*(ww-nz*cc);
    double df455 = aa4*dd4*(hh+vn*cc) + aa5*dd5*(hh-vn*cc);

    double fcdx1 = wfr1*wfr2 + wfl1*wfl2;
    double fcdy1 = wfr1*wfr3 + wfl1*wfl3;
    double fcdz1 = wfr1*wfr4 + wfl1*wfl4;

    double fcdx2 = wfr1*wfr2*wfr2 + pp + wfl1*wfl2*wfl2 + pm;
    double fcdy2 = wfr1*wfr2*wfr3      + wfl1*wfl2*wfl3;
    double fcdz2 = wfr1*wfr2*wfr4      + wfl1*wfl2*wfl4;

    double fcdx3 = fcdy2;
    double fcdy3 = wfr1*wfr3*wfr3 + pp + wfl1*wfl3*wfl3 + pm;
    double fcdz3 = wfr1*wfr3*wfr4      + wfl1*wfl3*wfl4;

    double fcdx4 = fcdz2;
    double fcdy4 = fcdz3;
    double fcdz4 = wfr1*wfr4*wfr4 + pp + wfl1*wfl4*wfl4 + pm;

    double fcdx5 = wfr2*wfr1*hp + wfl2*wfl1*hm;
    double fcdy5 = wfr3*wfr1*hp + wfl3*wfl1*hm;
    double fcdz5 = wfr4*wfr1*hp + wfl4*wfl1*hm;

    flux[tx        ] = 0.5*sn*(fcdx1*nx + fcdy1*ny + fcdz1*nz - df11 - df451);
    flux[tx+  nFace] = 0.5*sn*(fcdx2*nx + fcdy2*ny + fcdz2*nz - df12 - df452);
    flux[tx+2*nFace] = 0.5*sn*(fcdx3*nx + fcdy3*ny + fcdz3*nz - df13 - df453);
    flux[tx+3*nFace] = 0.5*sn*(fcdx4*nx + fcdy4*ny + fcdz4*nz - df14 - df454);
    flux[tx+4*nFace] = 0.5*sn*(fcdx5*nx + fcdy5*ny + fcdz5*nz - df15 - df455);

  };
}

// ===========================================================================
__host__ void computeroefluxo1_gpu(const int& nCellTot,
                                   const int& nFace,
                                   const double& gam,
                                   const double& rgaz,
                                   const double& cv,
                                   const double& cp,
                                   const double& pctrad,
                                   const int  & harten_type,
                                   double* rho,
                                   double* velo,
                                   double* temp,
                                   double* flux,
                                   double* surf,
                                   int*    FaceCell)
{
  // printf("computeroefluxo1_gpu %i \n", nCellTot);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  int nBlks = nFace/nThrs+1;
  dim3 thrTopoSdom  = dim3(nThrs,1,1);
  dim3 blkTopoSdom  = dim3(nFace/nThrs+1,1,1);

  __computeroefluxo1_gpu<<<blkTopoSdom, thrTopoSdom>>>( nCellTot,
                                                        nFace,
                                                        gam,
                                                        rgaz,
                                                        cv,
                                                        cp,
                                                        pctrad,
                                                        harten_type,
                                                        rho,
                                                        velo,
                                                        temp,
                                                        flux,
                                                        surf,
                                                        FaceCell);

  // cudaError_t err = cudaGetLastError();
  // if (err != cudaSuccess){
  //   printf("computeroefluxo1_gpu::Error: %s\n", cudaGetErrorString(err));
  // }
  // checkCuda( cudaDeviceSynchronize() );
  // cudaError_t err2 = cudaGetLastError();
  // if (err2 != cudaSuccess){
  //   printf("Error: %s\n", cudaGetErrorString(err2));
  // }
  // printf("computeroefluxo1_gpu end %i \n", nCellTot);

  // int bytes = nFace * sizeof(double)*5;
  // std::vector<double> check(nFace*5);
  // checkCuda( cudaMemcpy(check.data(), flux, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < nFace; ++i){
  //   printf("cpu check computeroefluxo1_gpu [%i] = [%f/%f/%f/%f/%f] \n", i, check[i],
  //                                                             check[i+  nFace],
  //                                                             check[i+2*nFace],
  //                                                             check[i+3*nFace],
  //                                                             check[i+4*nFace]);
  // }

}
