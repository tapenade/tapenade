__host__ void testRegion(double* T1, double* T2) {
    int i, j ;

    T1[10] = T1[9]*T2[11] ;
    if (T1[10]>=0.0) {
        T1[10] = T1[10] + 1 ;
        // $AD LABEL if_true
        T2[5] = T2[5] * T1[4] ;
        for (i=0 ; i<=10 ; ++i) {
           T2[i] = T1[i]*T2[i+20] ;
        }
        // $AD END-LABEL if_true
    }

    T2[9] = 2.5 ;

    for (j=0 ; j<=20 ; ++j) {
      // $AD LABEL loop_body
      T1[j] = 1.0/T1[j] ;
      if (T1[j]<=1.0) {
        // $AD LABEL if_true2
        T1[j] = T1[j]*T1[j] ;
        // $AD END-LABEL if_true2
      } else {
        T1[j] = T2[j]*T2[j] ;
      }
      T2[j] = 1.0/T2[j] ;
      // $AD END-LABEL loop_body
    }
}
