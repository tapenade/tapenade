__host__ void testRegion(double* T1, double* T2) {
    int i, j ;

    T1[10] = T1[9]*T2[11] ;
    // $AD LABEL RR1
    // $AD LABEL RR11
    if (T1[10]>=0.0) {
        T1[10] = T1[10] + 1 ;
        // $AD LABEL RX1
        T2[5] = T2[5] * T1[4] ;
        // $AD LABEL RX11
        for (i=0 ; i<=10 ; ++i) {
           T2[i] = T1[i]*T2[i+20] ;
        }
        // $AD END-LABEL RX11
        // $AD END-LABEL RX1
    }
    // $AD END-LABEL RR11
    T2[9] = 2.5 ;
    // $AD END-LABEL RR1
    // $AD LABEL RR2
    for (j=0 ; j<=20 ; ++j) {
      T1[j] = 1.0/T1[j] ;
      // $AD LABEL RY1
      if (T1[j]<=1.0) {
        T1[j] = T1[j]*T1[j] ;
      } else {
        // $AD LABEL RY11
        T1[j] = T2[j]*T2[j] ;
        // $AD END-LABEL RY11
      }
      // $AD END-LABEL RY1
      T2[j] = 1.0/T2[j] ;
    }
    // $AD END-LABEL RR2
}
