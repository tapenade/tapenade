// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __print_celldim_gpu(const int                  n_cell,
                                          double* __restrict__ dimension_cell)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  if (tx < n_cell){
    printf("tx %i [n_cell=%i] --> dimension_cell=[%f] \n", tx, n_cell, dimension_cell[tx]);
  }
}

// ===========================================================================
__global__ void __initcelldim_gpu(const int                  n_cell,
                                        double* __restrict__ dimension_cell)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < n_cell){
    dimension_cell[tx] = 0.;
  };
}

// ===========================================================================
__global__ void __computecelldimu_gpu_graph(const int                  n_face,
                                            const int                  beg_face,
                                            const int                  end_face,
                                            const int*    __restrict__ face_cell,
                                            const double* __restrict__ surface_norm_l2,
                                                  double* __restrict__ dimension_cell)
{
  int tx = beg_face + threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < end_face) {
    if (tx == 0) {
      printf("__computecelldimu_gpu_graph tx %i [%i] \n", tx, n_face);
    }

    int il = face_cell[tx      ];
    int ir = face_cell[tx+n_face];

    double tmp = surface_norm_l2[tx]*surface_norm_l2[tx];

    dimension_cell[il] += tmp;
    dimension_cell[ir] += tmp;
  }
}

// ===========================================================================
__global__ void __computecelldimbndu_gpu(const int                  n_face,
                                         const int                  beg_face,
                                         const int                  end_face,
                                         const int*    __restrict__ face_cell,
                                         const double* __restrict__ surface_norm_l2,
                                               double* __restrict__ dimension_cell)
{
  int tx = beg_face + threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < end_face) {
    // printf("tx %i [%i] --> [%i] [%f] \n", tx, n_face, face_vtx_idx[tx], coords[4] );

    int il = face_cell[tx];

    dimension_cell[il] += surface_norm_l2[tx]*surface_norm_l2[tx];
  }
}

// ===========================================================================
__global__ void __finalizecelldim_gpu(const int                  n_cell,
                                      const double* __restrict__ volume,
                                            double* __restrict__ dimension_cell)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < n_cell) {
    double dsurf = sqrt(dimension_cell[tx]);
    dimension_cell[tx] = volume[tx] / dsurf;
  }
}

// ===========================================================================
__host__ void computecelldimu_gpu(const int&    n_sdom,
                                  const int*    face_vect_tile_idx,
                                  const int*    nvect_pack,
                                  const int*    nvect_pack_idx,
                                  const int*    face_vect_ext_tile_idx,
                                  const int*    nvect_ext_pack,
                                  const int*    nvect_ext_pack_idx,
                                  const int&    n_face,
                                  const int&    n_cell,
                                  const int*    face_cell,
                                  const double* surface_norm_l2,
                                  const double* volume,
                                        double* dimension_cell)
{
  printf("begin computecelldimu_gpu(n_sdom=%i, n_face=%i, n_cell=%i) \n", n_sdom, n_face, n_cell);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  int nBlks = n_cell/nThrs+1;
  printf("nThrs = %i \n", nThrs);
  printf("nBlks = %i \n", nBlks);
  dim3 thrTopoCellSdom = dim3(nThrs,1,1);
  dim3 blkTopoCellSdom = dim3(nBlks,1,1);

  // Initialize dimension_cell cells
  __initcelldim_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cell, dimension_cell);

  // Compute contribution from interior faces
  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int i_vec = 0; i_vec < nvect_pack[i_sub]; ++i_vec){
      int i_beg     = nvect_pack_idx[i_sub];
      int n_faceLoc = face_vect_tile_idx[i_beg+i_vec+1]-face_vect_tile_idx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_faceLoc[%i]\n", i_sub, i_vec, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __computecelldimu_gpu_graph<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_face,
                                                                        face_vect_tile_idx[i_beg+i_vec  ],
                                                                        face_vect_tile_idx[i_beg+i_vec+1],
                                                                        face_cell,
                                                                        surface_norm_l2,
                                                                        dimension_cell);
    }
  }

  // Compute contribution from boundary face(s)
  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int i_vec = 0; i_vec < nvect_ext_pack[i_sub]; ++i_vec){
      int i_beg     = nvect_ext_pack_idx[i_sub];
      int n_faceLoc = face_vect_ext_tile_idx[i_beg+i_vec+1]-face_vect_ext_tile_idx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_faceLocBnd[%i]\n", i_sub, i_vec, n_faceLoc);
      dim3 thrTopoFaceSdom = dim3(nThrs           ,1,1);
      dim3 blkTopoFaceSdom = dim3(n_faceLoc/nThrs+1,1,1);
      __computecelldimbndu_gpu<<<blkTopoFaceSdom, thrTopoFaceSdom>>>(n_face,
                                                                     face_vect_ext_tile_idx[i_beg+i_vec],
                                                                     face_vect_ext_tile_idx[i_beg+i_vec+1],
                                                                     face_cell,
                                                                     surface_norm_l2,
                                                                     dimension_cell);
    }
  }

  // Finalize computation
  __finalizecelldim_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cell,
                                                              volume,
                                                              dimension_cell);
  // __print_celldim_gpu<<<blkTopoCellSdom, thrTopoCellSdom>>>(n_cell, dimension_cell);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("computecelldimu_gpu::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );
  printf("bendegin computecelldimu_gpu(n_face=%i, n_cell=%i, n_sdom=%i) \n", n_face, n_cell, n_sdom);
}
