// ===========================================================================
__global__ void __knl_machidealgaz_cuda(
  const int                  n_cell,
  const double* __restrict__ gamma,
  const double* __restrict__ cv,
  const double* __restrict__ rgaz,
  const double* __restrict__ density,
  const double* __restrict__ momentum,
  const double* __restrict__ energy_stagnation_density,
        double* __restrict__ mach)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  if (tx < n_cell) {
    
    double invcv = 1./(*cv);
    double rhoc  = density[tx];
    double rhouc = momentum[tx         ];
    double rhovc = momentum[tx+  n_cell];
    double rhowc = momentum[tx+2*n_cell];
    double rhoec = energy_stagnation_density[tx];
    
    double roi = 1./rhoc;

    double velox  = rhouc*roi;
    double veloy  = rhovc*roi;
    double veloz  = rhowc*roi;
    double velon  = sqrt(velox*velox + veloy*veloy + veloz*veloz);

    double roe   = rhoec - 0.5 * roi * (rhouc*rhouc + rhovc*rhovc + rhowc*rhowc);

    double temperature = roi*invcv*roe;

    double sound = sqrt((*gamma) * (*rgaz) * temperature);

    mach[tx] = velon/sound;
  }
}
