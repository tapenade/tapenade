C Testing functionality "replicateDiff" asked by Onera:
C Similar to "multi", but for a given fixed number of
C directions, and derivative variable is not expanded
C with a new dimension, but rather replicated
C as several "independent" derivative variables.
      subroutine foo(a,b,n)
      INTEGER n
      REAL*8 a
      REAL*8 b(10,n)

      a = a*b(3,3)
      b(5,n-2) = b(5,2)*b(8,1) + sin(a)

      end
