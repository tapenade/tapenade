

extern double f_(), g_();

void sub1 (double *x, double *y)
{

    *y = f_(x) + g_(y);
}

main()
{
  double i=2;
  double j=3;
  double result;
  printf("i %f ...", i);
  printf(" j %f ...", j);
  sub1(&i, &j);
  printf("%f --> \n",j);
}
