module abstract_interface
  abstract interface
     subroutine sub(x,y)
       real ,intent(in)::x
       real ,intent(out)::y
     end subroutine sub
  end interface
end module abstract_interface

program main
  implicit none

  integer:: i
  real :: x,y1,y2

  i = 1
  x = 1._8
  call foo(i, x, y1)
  i = 2
  call foo(i, x, y2)

  print*, 'y1+y2 = ', y1+y2
end program main

subroutine foo(i,x,y)
  use abstract_interface
  integer, intent(in) :: i
  real, intent(in) :: x
  real, intent(out) ::y
  !-- procedure pointers
  procedure(sub)  sub1, sub2 !-- no '::' allowed before declarator...
  procedure (sub), pointer :: fptr
 
  if( i.eq.1 ) then
     fptr=>sub1
  else
     fptr=>sub2
  endif
  call fptr(x, y)

end subroutine foo


subroutine sub1 (x,y)
  real ,intent(in)::x
  real ,intent(out)::y

  y = x
  print*, "sub1: x=", x, "y=", y
end subroutine sub1

subroutine sub2 (x,y)
  real ,intent(in)::x
  real ,intent(out)::y

  y = x*2
  print*, "sub2: x=", x, "y=", y
end subroutine sub2
