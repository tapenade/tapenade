// ------------------------------------------------------------------
// External include

// ------------------------------------------------------------------
//#include <iostream>

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __computeroefluxo1_gpu(const int n_cellt,
                                       const int n_face,
                                       const int* __restrict__ face_cell,
                                       const double gam,
                                       const double rgaz,
                                       const double pctrad,
                                       const int    harten_type,
                                       const double* __restrict__ surf,
                                       const double* __restrict__ rho,
                                       const double* __restrict__ velo,
                                       const double* __restrict__ temp,
                                             double* __restrict__ flux)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;

  double gam1   = gam - 1.;
  double gam1_1 = 1./gam1;

  if (tx < n_face) {
    // printf("tx %i [%i] --> [%i] [%f] \n", tx, n_face, face_vtx_idx[tx], coords[4]);
    int il = face_cell[tx      ];
    int ir = face_cell[tx+n_face];

    double sc1 = surf[tx        ];
    double sc2 = surf[tx+  n_face];
    double sc3 = surf[tx+2*n_face];
    double sn  = sqrt(sc1*sc1 + sc2*sc2 + sc3*sc3);

    // double invsn = ONE/max(sn,1.e-32)
    double invsn = 1./sn;
    double nx    = sc1*invsn;
    double ny    = sc2*invsn;
    double nz    = sc3*invsn;

    double wfl1 = rho[il];
    double wfr1 = rho[ir];

    double wfl2 = velo[il];
    double wfr2 = velo[ir];

    double wfl3 = velo[il+n_cellt];
    double wfr3 = velo[ir+n_cellt];

    double wfl4 = velo[il+2*n_cellt];
    double wfr4 = velo[ir+2*n_cellt];

    double wfl5 = temp[il];
    double wfr5 = temp[ir];

    double pm = wfl1*wfl5*rgaz;
    double pp = wfr1*wfr5*rgaz;

    double hm = gam*gam1_1*wfl5*rgaz + 0.5*(wfl2*wfl2 + wfl3*wfl3 + wfl4*wfl4);
    double hp = gam*gam1_1*wfr5*rgaz + 0.5*(wfr2*wfr2 + wfr3*wfr3 + wfr4*wfr4);

    double r  = sqrt(max(wfr1/wfl1, 1.e-12));
    double rr = sqrt(max(wfr1*wfl1, 1.e-12));

    double oneonrplusone = 1./(r+1.);

    double uu = (wfr2*r+wfl2)*oneonrplusone;
    double vv = (wfr3*r+wfl3)*oneonrplusone;
    double ww = (wfr4*r+wfl4)*oneonrplusone;
    double ee = 0.5*(uu*uu + vv*vv + ww*ww);
    double hh = (hp*r+hm)*oneonrplusone;
    double cc = sqrt(max(gam1*(hh-ee), 1.e-12));
    double vn = uu*nx + vv*ny + ww*nz;

    double lambda1 = abs(vn   );
    double lambda4 = abs(vn+cc);
    double lambda5 = abs(vn-cc);

    double small = (2-harten_type)*pctrad*(abs(uu)+abs(vv)+abs(ww)+cc)
                 + (harten_type-1)*pctrad*(abs(vn)+cc);

    double q1 = 0.5 + copysign(0.5,lambda1-small);
    double k1 = 0.5 - copysign(0.5,lambda1-small);
    double q4 = 0.5 + copysign(0.5,lambda4-small);
    double k4 = 0.5 - copysign(0.5,lambda4-small);
    double q5 = 0.5 + copysign(0.5,lambda5-small);
    double k5 = 0.5 - copysign(0.5,lambda5-small);

    double oneonsmall = 1.0/small;
    double mask       = 0.5 + copysign(0.5, small - 1.e-32);

    double aa1 = mask * (  q1*lambda1
                  + k1*0.5*(lambda1*lambda1+small*small)*oneonsmall)
                  + (1.-mask)*lambda1;

    double aa4 = mask * (  q4*lambda4
                 + k4*0.5*(lambda4*lambda4+small*small)*oneonsmall)
                 + (1.-mask)*lambda4;

    double aa5 = mask * (  q5*lambda5
                  + k5*0.5*(lambda5*lambda5+small*small)*oneonsmall)
                  + (1.-mask)*lambda5;

    double du  = wfr2-wfl2;
    double dv  = wfr3-wfl3;
    double dw  = wfr4-wfl4;
    double dvn = du*nx+dv*ny+dw*nz;

    double oneonccsquared = 1. / (cc*cc);

    double dd1 = (wfr1-wfl1)-(pp-pm)*oneonccsquared;
    double dd4 = 0.5*(pp-pm+rr*cc*dvn)*oneonccsquared;
    double dd5 = 0.5*(pp-pm-rr*cc*dvn)*oneonccsquared;

    double df11 = aa1*dd1;
    double df12 = aa1*(dd1*uu+rr*(du-nx*dvn));
    double df13 = aa1*(dd1*vv+rr*(dv-ny*dvn));
    double df14 = aa1*(dd1*ww+rr*(dw-nz*dvn));
    double df15 = aa1*(dd1*ee+rr*(uu*du+vv*dv+ww*dw-vn*dvn));

    double df451 = aa4*dd4            + aa5*dd5;
    double df452 = aa4*dd4*(uu+nx*cc) + aa5*dd5*(uu-nx*cc);
    double df453 = aa4*dd4*(vv+ny*cc) + aa5*dd5*(vv-ny*cc);
    double df454 = aa4*dd4*(ww+nz*cc) + aa5*dd5*(ww-nz*cc);
    double df455 = aa4*dd4*(hh+vn*cc) + aa5*dd5*(hh-vn*cc);

    double fcdx1 = wfr1*wfr2 + wfl1*wfl2;
    double fcdy1 = wfr1*wfr3 + wfl1*wfl3;
    double fcdz1 = wfr1*wfr4 + wfl1*wfl4;

    double fcdx2 = wfr1*wfr2*wfr2 + pp + wfl1*wfl2*wfl2 + pm;
    double fcdy2 = wfr1*wfr2*wfr3      + wfl1*wfl2*wfl3;
    double fcdz2 = wfr1*wfr2*wfr4      + wfl1*wfl2*wfl4;

    double fcdx3 = fcdy2;
    double fcdy3 = wfr1*wfr3*wfr3 + pp + wfl1*wfl3*wfl3 + pm;
    double fcdz3 = wfr1*wfr3*wfr4      + wfl1*wfl3*wfl4;

    double fcdx4 = fcdz2;
    double fcdy4 = fcdz3;
    double fcdz4 = wfr1*wfr4*wfr4 + pp + wfl1*wfl4*wfl4 + pm;

    double fcdx5 = wfr2*wfr1*hp + wfl2*wfl1*hm;
    double fcdy5 = wfr3*wfr1*hp + wfl3*wfl1*hm;
    double fcdz5 = wfr4*wfr1*hp + wfl4*wfl1*hm;

    flux[tx         ] = 0.5*sn*(fcdx1*nx + fcdy1*ny + fcdz1*nz - df11 - df451);
    flux[tx+  n_face] = 0.5*sn*(fcdx2*nx + fcdy2*ny + fcdz2*nz - df12 - df452);
    flux[tx+2*n_face] = 0.5*sn*(fcdx3*nx + fcdy3*ny + fcdz3*nz - df13 - df453);
    flux[tx+3*n_face] = 0.5*sn*(fcdx4*nx + fcdy4*ny + fcdz4*nz - df14 - df454);
    flux[tx+4*n_face] = 0.5*sn*(fcdx5*nx + fcdy5*ny + fcdz5*nz - df15 - df455);

    if (tx == 0)
    {
      printf("__computeroefluxo1_gpu n_cellt=%i, n_face=%i\n", n_cellt, n_face);
      // printf("il=%i, ir=%i\n\n", il, ir);
      // printf("gam=%f, rgaz=%f, pctrad=%f\n", gam, rgaz, pctrad);
      // printf("gam1=%f, gam1_1=%f\n", gam1, gam1_1);
      // printf("sc1=%f, sc2=%f, sc3=%f, sn=%f\n", sc1, sc2, sc3, sn);
      // printf("nx=%f, ny=%f, nz=%f\n", nx, ny, nz);

      // printf("wfl1=%f, wfr1=%f\n", wfl1, wfr1);
      // printf("wfl2=%f, wfr2=%f\n", wfl2, wfr2);
      // printf("wfl3=%f, wfr3=%f\n", wfl3, wfr3);
      // printf("wfl4=%f, wfr4=%f\n", wfl4, wfr4);
      // printf("wfl5=%f, wfr5=%f\n", wfl5, wfr5);

      // printf("pm=%f, pp=%f\n", pm, pp);
      // printf("hm=%f, hp=%f\n", hm, hp);

      // printf("tx %i --> [flux[tx         ]=%f] \n", tx, flux[tx         ]);
      // printf("tx %i --> [flux[tx+  n_face]=%f] \n", tx, flux[tx+  n_face]);
      // printf("tx %i --> [flux[tx+2*n_face]=%f] \n", tx, flux[tx+2*n_face]);
      // printf("tx %i --> [flux[tx+3*n_face]=%f] \n", tx, flux[tx+3*n_face]);
      // printf("tx %i --> [flux[tx+4*n_face]=%f] \n", tx, flux[tx+4*n_face]);
    }
  };
}

// ===========================================================================
__host__ void computeroefluxo1_gpu(// HPC layer
                                   // Topology(ies)
                                   const int& n_cellt,
                                   const int& n_face,
                                   // Connectivity(ies)
                                   const int* face_cell,
                                   // Parameter(s)
                                   const double& gam,
                                   const double& rgaz,
                                   const double& pctrad,
                                   const int& harten_type,
                                   // Field(s)
                                   const double* surf,
                                   const double* rho,
                                   const double* velo,
                                   const double* temp,
                                         double* flux)
{
  printf("computeroefluxo1_gpu %i \n", n_cellt);

  // Target --> Feed Block and Threads
  int nThrs = 64;
  dim3 thrTopoSdom  = dim3(nThrs,1,1);
  dim3 blkTopoSdom  = dim3(n_face/nThrs+1,1,1);

  __computeroefluxo1_gpu<<<blkTopoSdom, thrTopoSdom>>>(n_cellt,
                                                       n_face,
                                                       face_cell,
                                                       gam,
                                                       rgaz,
                                                       pctrad,
                                                       harten_type,
                                                       surf,
                                                       rho,
                                                       velo,
                                                       temp,
                                                       flux);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("computeroefluxo1_gpu::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );
  // cudaError_t err2 = cudaGetLastError();
  // if (err2 != cudaSuccess){
  //   printf("Error: %s\n", cudaGetErrorString(err2));
  // }
  // printf("computeroefluxo1_gpu end %i \n", n_cellt);

  // int bytes = n_face * sizeof(double)*5;
  // std::vector<double> check(n_face*5);
  // checkCuda( cudaMemcpy(check.data(), flux, bytes, cudaMemcpyDeviceToHost) );
  // for(int i = 0; i < n_face; ++i){
  //   printf("cpu check computeroefluxo1_gpu [%i] = [%f/%f/%f/%f/%f] \n", i, check[i],
  //                                                             check[i+  n_face],
  //                                                             check[i+2*n_face],
  //                                                             check[i+3*n_face],
  //                                                             check[i+4*n_face]);
  // }

}
