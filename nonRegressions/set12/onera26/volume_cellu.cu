// ------------------------------------------------------------------
// External include
//#include <iostream>
// ------------------------------------------------------------------

// ------------------------------------------------------------------
// Internal include
#include "../../cuda_utilities.h"
#include "../../cuda_utilities_graph.h"
// ------------------------------------------------------------------

// ===========================================================================
__global__ void __print_volumecell_gpu(const int n_cellt,
                                       const double* __restrict__ volume)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  if( tx < n_cellt){
    printf("tx %i [n_cellt=%i] --> volume=[%f] \n", tx, n_cellt, volume[tx]);
  }
}

// ===========================================================================
__global__ void __initvolumecell_gpu(const int                  n_cellt,
                                           double* __restrict__ volume)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  if( tx < n_cellt) {
    volume[tx] = 0.;
  }
}

// ===========================================================================
__global__ void __computevolumecellu_gpu(const int                  beg_face,
                                         const int                  end_face,
                                         const int                  n_face,
                                         const int                  n_cellt,
                                         const int*    __restrict__ face_cell,
                                         const double* __restrict__ center_face,
                                         const double* __restrict__ surface,
                                               double* __restrict__ volume)
{
  int tx = beg_face + threadIdx.x + blockIdx.x * blockDim.x;
  // printf("n_face=%i, n_cellt=%i, beg_face=%i, end_face=%i\n", n_face, n_cellt, beg_face, end_face);

  if( tx < end_face) {
    if (tx == 0) {
      printf("__computevolumecellu_gpu tx=%i [n_face=%i, n_cellt=%i]\n", tx, n_face, n_cellt);
    }
    int il = face_cell[tx       ];
    int ir = face_cell[tx+n_face];
    // printf("tx %i [n_face=%i] --> face_cell[tx:%i]=%i, face_cell[tx+n_face:%i]=%i\n", tx, n_face, tx, face_cell[tx], tx+n_face, face_cell[tx+n_face]);
    // printf("tx %i [n_face=%i] --> centerint=[%f/%f/%f]\n", tx, n_face, center_face[tx], center_face[tx+n_face], center_face[tx+2*n_face]);
    // printf("tx %i [n_face=%i] --> surface=[%f/%f/%f]\n", tx, n_face, surface[tx], surface[tx+n_face], surface[tx+2*n_face]);
    double tmp = (1./3.) * (center_face[tx         ]*surface[tx        ]
                          + center_face[tx+  n_face]*surface[tx+  n_face]
                          + center_face[tx+2*n_face]*surface[tx+2*n_face]);
    volume[il] += tmp;
    volume[ir] -= tmp;
  }
}

// ===========================================================================
__global__ void __computevolumecellbndu_gpu(const int                  beg_face,
                                            const int                  end_face,
                                            const int                  n_face,
                                            const int                  n_cellt,
                                            const int*    __restrict__ face_cell,
                                            const double* __restrict__ center_face,
                                            const double* __restrict__ surface,
                                                  double* __restrict__ volume)
{
  int tx = beg_face + threadIdx.x + blockIdx.x * blockDim.x;
  // printf("n_face=%i, n_cellt=%i, beg_face=%i, end_face=%i\n", n_face, n_cellt, beg_face, end_face);

  if( tx < end_face) {
    // printf("tx=%i [n_face=%i, n_cellt=%i]\n", tx, n_face, n_cellt);
    int il = face_cell[tx];
    // printf("tx %i [n_face=%i] --> face_cell[tx:%i]=%i, face_cell[tx+n_face:%i]=%i\n", tx, n_face, tx, face_cell[tx], tx+n_face, face_cell[tx+n_face]);
    // printf("tx %i [n_face=%i] --> centerint=[%f/%f/%f]\n", tx, n_face, center_face[tx], center_face[tx+n_face], center_face[tx+2*n_face]);
    // printf("tx %i [n_face=%i] --> surface=[%f/%f/%f]\n", tx, n_face, surface[tx], surface[tx+n_face], surface[tx+2*n_face]);
    double tmp = (1./3.) * (center_face[tx        ]*surface[tx        ]
                          + center_face[tx+  n_face]*surface[tx+  n_face]
                          + center_face[tx+2*n_face]*surface[tx+2*n_face]);
    volume[il] += tmp;
  }
}

// ===========================================================================
__host__ void computevolumecellu_gpu(const int&    n_sdom,
                                     const int*    face_vect_tile_idx,
                                     const int*    nvect_pack,
                                     const int*    nvect_pack_idx,
                                     const int*    face_vect_ext_tile_idx,
                                     const int*    nvect_ext_pack,
                                     const int*    nvect_ext_pack_idx,
                                     const int&    n_face,
                                     const int&    n_cellt,
                                     const int*    face_cell,
                                     const double* center_face,
                                     const double* surface,
                                           double* volume)
{
  printf("begin computevolumecellu_gpu(n_face=%i, n_cellt=%i, n_sdom=%i) \n", n_face, n_cellt, n_sdom);
  // Target --> Feed Block and Threads
  int n_thrs = 64;
  int n_blks = n_cellt/n_thrs+1;
  printf("n_thrs = %i \n", n_thrs);
  printf("n_blks = %i \n", n_blks);
  dim3 thr_topo_cell = dim3(n_thrs,1,1);
  dim3 blk_topo_cell = dim3(n_blks,1,1);

  // Initialize volume
  __initvolumecell_gpu<<<blk_topo_cell, thr_topo_cell>>>(n_cellt, volume);

  // Compute contribution from interior faces
  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int i_vec = 0; i_vec < nvect_pack[i_sub]; ++i_vec){
      int i_beg = nvect_pack_idx[i_sub];
      int n_face_loc = face_vect_tile_idx[i_beg+i_vec+1]-face_vect_tile_idx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_face_loc[%i]\n", i_sub, i_vec, n_face_loc);
      dim3 thr_topo_face_sdom = dim3(n_thrs           ,1,1);
      dim3 blk_topo_face_sdom = dim3(n_face_loc/n_thrs+1,1,1);
      __computevolumecellu_gpu<<<blk_topo_face_sdom, thr_topo_face_sdom>>>(face_vect_tile_idx[i_beg+i_vec  ],
                                                                           face_vect_tile_idx[i_beg+i_vec+1],
                                                                           n_face, n_cellt,
                                                                           face_cell,
                                                                           center_face,
                                                                           surface,
                                                                           volume);
    }
  }

  // Compute contribution from boundary face(s)
  for(int i_sub = 0; i_sub < n_sdom; ++i_sub){
    for(int i_vec = 0; i_vec < nvect_ext_pack[i_sub]; ++i_vec){
      int i_beg = nvect_ext_pack_idx[i_sub];
      int n_face_loc = face_vect_ext_tile_idx[i_beg+i_vec+1]-face_vect_ext_tile_idx[i_beg+i_vec];
      // printf(" [%i]/[%i] --> n_face_locBnd[%i]\n", i_sub, i_vec, n_face_loc);
      dim3 thr_topo_face_sdom = dim3(n_thrs           ,1,1);
      dim3 blk_topo_face_sdom = dim3(n_face_loc/n_thrs+1,1,1);
      __computevolumecellbndu_gpu<<<blk_topo_face_sdom, thr_topo_face_sdom>>>(face_vect_ext_tile_idx[i_beg+i_vec],
                                                                              face_vect_ext_tile_idx[i_beg+i_vec+1],
                                                                              n_face, n_cellt,
                                                                              face_cell,
                                                                              center_face,
                                                                              surface,
                                                                              volume);
    }
  }
  // __print_volumecell_gpu<<<blk_topo_cell, thr_topo_cell>>>(n_cellt, volume);

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess){
    printf("computevolumeu_gpu::Error: %s\n", cudaGetErrorString(err));
  }
  checkCuda( cudaDeviceSynchronize() );
  printf("end computevolumeu_gpu(n_face=%i, n_cellt=%i, n_sdom=%i) \n", n_face, n_cellt, n_sdom);
}
