static inline int fun(int i, float *f, double *d, char c, ...)
{
    int i2 = +1;
    static long i3;
    int i5;
    
    /* boucle while */
    while (i > 0) {
        i2 = i - 4;
        i--;
    }
    /* boucle do while  */
    do {
        if (i5 + 1 > 12 - i)
            break;
        i5 += 21;
    } while (1);
    
    /* boucle for */
    for (i3 = 0; i3 < i; i++) {
        if (i2 == 0)
            // break out of the loop
            break;
        *d = (float)(i3);
    }
    /* boucle for avec une declaration en tete */
    for (int i4 = 2; i4 > 0; --i4) {
        i3--;
        /* dernier commentaire */
    }
    /* autre cas de boucle for. En C89, on ne peut pas reutiliser i4
     * ici, en C99 c'est possible. De la meme maniere i2 ici n'est pas
     * legal en C89, c'est une redefinition. Mais en C99 c'est possible, le
     * i2 defini ici est bien different du precedent */
    for (int i6 = 4, i2 = 0; i6 > 0 || i2 < 8; i6--, ++i2) {
        /* statement NoOP */
        ;
        /* corps de boucle quasi vide */
    }
    if (0) {
        return 0;
    } else
        goto l1;
    /* Addition */
    *d += 0.3e-1;
  l1:
    i5 = sizeof(i3);
    i5 = sizeof(char[5]);
    i5 = sizeof(double *);
    i5 = (int)((float)c + 1.5);
    
    switch (i2 - 1) {
    case 3 :
        break;
    case 1 + 1 : {
        *d = 0.0;
        i5 -= 12;
        break;
    }
    default :
        break;   
    }
    
    
    /* On finit par un return */
    return (int)c + 3;
}
