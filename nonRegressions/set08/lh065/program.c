/* L'exemple de test standard du mode debug */
/*  cf ~/home/tapenade/debugAD au 2 octobre 2017 */
/* Source code, complete with main, C version. */
#include <stdio.h>
#include <math.h>

void incrsqrt(double *pp, double dxx, double dyy) {
  double xx2 = dxx*dxx ;
  double yy2 = dyy*dyy ;
/*$AD DEBUG-HERE befsqrt true true */
  *pp += sqrt(xx2+yy2) ;
}

double polyperim(double *X, double *Y, int ns) {
  double perim = 0.0 ;
  int cp,pp ;
  double dx,dy ;
  
  for (cp=0 ; cp<ns ; ++cp) {
    pp = cp - 1 ;
    if (pp==-1) pp = ns-1 ;
    dx = X[cp] - X[pp] ;
    dy = Y[cp] - Y[pp] ;
    incrsqrt(&perim, dx, dy) ;
  }
  return perim ;
}

double polysurf(double *X, double *Y, int ns) {
  double surf = 0.0 ;
  int cp,pp ;

  for (cp=0 ; cp<ns ; ++cp) {
    pp = cp - 1 ;
    if (pp==-1) pp = ns-1 ;
    surf += (X[pp]*Y[cp] - X[cp]*Y[pp])/2 ;
  }
  return surf ;
}

double polycost(double *X, double *Y, int ns) {
  double perim = polyperim(X,Y,ns) ;
  return perim*perim/polysurf(X,Y,ns) ;
}

int main() {
  int ns ;
  double X[5], Y[5] ;
  double cost ;

  ns = 5 ;
  X[0] = 0.0 ;
  Y[0] = 0.0 ;
  X[1] = 2.0 ;
  Y[1] = 0.0 ;
  X[2] = 2.0 ;
  Y[2] = 3.0 ;
  X[3] = 1.0 ;
  Y[3] = 6.0 ;
  X[4] = -3.0 ;
  Y[4] = 3.0 ;
  cost = polycost(X,Y,ns) ;
  printf(" cost=   %3.15f\n",cost) ;
}
