/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 28 Aug 2020 10:13
*/
#include <adStack.h>
#include <adDebug.h>
/* L'exemple de test standard du mode debug 
  cf ~/home/tapenade/debugAD au 2 octobre 2017 
 Source code, complete with main, C version. */
#include <stdio.h>
#include <math.h>

/*
  Differentiation of incrsqrt in reverse (adjoint) mode (with options context debugAdjoint no!SpareInits fixinterface):
   gradient     of useful results: *pp
   with respect to varying inputs: dxx dyy *pp
   Plus diff mem management of: pp:in
*/
void incrsqrt_b(double *pp, double *ppb, double dxx, double *dxxb, double dyy,
        double *dyyb) {
    double xx2 = dxx*dxx;
    double xx2b = 0.0;
    double yy2 = dyy*dyy;
    double yy2b = 0.0;
    double result1;
    double result1b;
    double tempb;
    result1 = sqrt(xx2 + yy2);
    *pp = *pp + result1;
    pushReal8(xx2);
    pushReal8(yy2);
    if (adDebugBwd_here("exit")) {
        adDebugAdj_wReal8(ppb);
        adDebugAdj_wDisplay("exit", 0);
    } else
        adDebugAdj_skip("exit");
    popReal8(&yy2);
    popReal8(&xx2);
    *dxxb = 0.0;
    *dyyb = 0.0;
    result1b = 0.0;
    yy2b = 0.0;
    xx2b = 0.0;
    result1b = result1b + *ppb;
    tempb = (xx2 + yy2 == 0.0 ? 0.0 : result1b/(2.0*sqrt(xx2+yy2)));
    result1b = 0.0;
    xx2b = xx2b + tempb;
    yy2b = yy2b + tempb;
    if (1 && adDebugBwd_here("befsqrt")) {
        adDebugAdj_rwReal8(ppb);
        adDebugAdj_rwReal8(&yy2b);
        adDebugAdj_rwReal8(&xx2b);
        adDebugAdj_rwDisplay("befsqrt", 0);
    } else
        adDebugAdj_skip("befsqrt");
    *dyyb = *dyyb + 2*dyy*yy2b;
    yy2b = 0.0;
    *dxxb = *dxxb + 2*dxx*xx2b;
    xx2b = 0.0;
    if (adDebugBwd_here("entry")) {
        adDebugAdj_rReal8(dxxb);
        adDebugAdj_rReal8(dyyb);
        adDebugAdj_rReal8(ppb);
        adDebugAdj_rDisplay("entry", 0);
    } else
        adDebugAdj_skip("entry");
}

void incrsqrt_nodiff(double *pp, double dxx, double dyy) {
    double xx2 = dxx*dxx;
    double yy2 = dyy*dyy;
    double result1;
    //$AD DEBUG-HERE befsqrt true true 
    result1 = sqrt(xx2 + yy2);
    *pp = *pp + result1;
}

/*
  Differentiation of polyperim in reverse (adjoint) mode (with options context debugAdjoint no!SpareInits fixinterface):
   gradient     of useful results: *X *Y polyperim
   with respect to varying inputs: *X *Y
   Plus diff mem management of: X:in Y:in
*/
void polyperim_b(double *X, double *Xb, double *Y, double *Yb, int ns, double 
        polyperimb) {
    double perim = 0.0;
    double perimb = 0.0;
    int cp, pp;
    double dx, dy;
    double dxb, dyb;
    int branch;
    for (cp = 0; cp < ns; ++cp) {
        pushInteger4(pp);
        pp = cp - 1;
        if (pp == -1) {
            pp = ns - 1;
            pushControl1b(0);
        } else
            pushControl1b(1);
        dx = X[cp] - X[pp];
        dy = Y[cp] - Y[pp];
        pushReal8(perim);
        incrsqrt_nodiff(&perim, dx, dy);
    }
    polyperim = perim;
    pushInteger4(pp);
    if (adDebugBwd_here("exit")) {
        adDebugAdj_wReal8Array(Xb, ns);
        adDebugAdj_wReal8Array(Yb, ns);
        adDebugAdj_wReal8(&polyperimb);
        adDebugAdj_wDisplay("exit", 0);
    } else
        adDebugAdj_skip("exit");
    popInteger4(&pp);
    dxb = 0.0;
    dyb = 0.0;
    perimb = 0.0;
    perimb = perimb + polyperimb;
    polyperimb = 0.0;
    for (cp = ns-1; cp > -1; --cp) {
        dx = X[cp] - X[pp];
        dy = Y[cp] - Y[pp];
        adDebugBwd_call("incrsqrt", 0);
        if (adDebugBwd_here("afterCall")) {
            adDebugAdj_rReal8Array(Xb, ns);
            adDebugAdj_rReal8Array(Yb, ns);
            adDebugAdj_rReal8(&perimb);
            adDebugAdj_rDisplay("afterCall", -1);
        } else
            adDebugAdj_skip("afterCall");
        popReal8(&perim);
        dxb = 0.0;
        dyb = 0.0;
        incrsqrt_b(&perim, &perimb, dx, &dxb, dy, &dyb);
        if (adDebugBwd_here("beforeCall")) {
            adDebugAdj_wReal8Array(Xb, ns);
            adDebugAdj_wReal8Array(Yb, ns);
            adDebugAdj_wReal8(&dxb);
            adDebugAdj_wReal8(&dyb);
            adDebugAdj_wReal8(&perimb);
            adDebugAdj_wDisplay("beforeCall", -1);
        } else
            adDebugAdj_skip("beforeCall");
        adDebugBwd_exit();
        Yb[cp] = Yb[cp] + dyb;
        Yb[pp] = Yb[pp] - dyb;
        dyb = 0.0;
        Xb[cp] = Xb[cp] + dxb;
        Xb[pp] = Xb[pp] - dxb;
        dxb = 0.0;
        popControl1b(&branch);
        if (branch == 0)
            if (0 && adDebugBwd_here("middle")) {
                adDebugAdj_rwReal8Array(Xb, ns);
                adDebugAdj_rwReal8Array(Yb, ns);
                adDebugAdj_rwReal8(&perimb);
                adDebugAdj_rwDisplay("middle", 0);
            } else
                adDebugAdj_skip("middle");
        popInteger4(&pp);
    }
    perimb = 0.0;
    if (adDebugBwd_here("entry")) {
        adDebugAdj_rReal8Array(Xb, ns);
        adDebugAdj_rReal8Array(Yb, ns);
        adDebugAdj_rDisplay("entry", 0);
    } else
        adDebugAdj_skip("entry");
}

double polyperim_nodiff(double *X, double *Y, int ns) {
    double perim = 0.0;
    int cp, pp;
    double dx, dy;
    for (cp = 0; cp < ns; ++cp) {
        pp = cp - 1;
        if (pp == -1)
            pp = ns - 1;
        dx = X[cp] - X[pp];
        dy = Y[cp] - Y[pp];
        incrsqrt_nodiff(&perim, dx, dy);
    }
    return perim;
}

/*
  Differentiation of polysurf in reverse (adjoint) mode (with options context debugAdjoint no!SpareInits fixinterface):
   gradient     of useful results: polysurf
   with respect to varying inputs: *X *Y
   Plus diff mem management of: X:in Y:in
*/
void polysurf_b(double *X, double *Xb, double *Y, double *Yb, int ns, double 
        polysurfb) {
    double surf = 0.0;
    double surfb = 0.0;
    int cp, pp;
    double tempb;
    int branch;
    int i;
    for (cp = 0; cp < ns; ++cp) {
        pushInteger4(pp);
        pp = cp - 1;
        if (pp == -1) {
            pp = ns - 1;
            pushControl1b(0);
        } else
            pushControl1b(1);
        surf = surf + (X[pp]*Y[cp]-X[cp]*Y[pp])/2;
    }
    polysurf = surf;
    pushInteger4(pp);
    if (adDebugBwd_here("exit")) {
        adDebugAdj_wReal8(&polysurfb);
        adDebugAdj_wDisplay("exit", 0);
    } else
        adDebugAdj_skip("exit");
    popInteger4(&pp);
    for (i=0 ; i<ns ; ++i) {
      Xb[i] = 0.0;
      Yb[i] = 0.0;
    }
    surfb = 0.0;
    surfb = surfb + polysurfb;
    polysurfb = 0.0;
    for (cp = ns-1; cp > -1; --cp) {
        tempb = surfb/2;
        Xb[pp] = Xb[pp] + Y[cp]*tempb;
        Yb[cp] = Yb[cp] + X[pp]*tempb;
        Xb[cp] = Xb[cp] - Y[pp]*tempb;
        Yb[pp] = Yb[pp] - X[cp]*tempb;
        popControl1b(&branch);
        if (branch == 0)
            if (0 && adDebugBwd_here("middle")) {
                adDebugAdj_rwReal8Array(Xb, ns);
                adDebugAdj_rwReal8Array(Yb, ns);
                adDebugAdj_rwReal8(&surfb);
                adDebugAdj_rwDisplay("middle", 0);
            } else
                adDebugAdj_skip("middle");
        popInteger4(&pp);
    }
    surfb = 0.0;
    if (adDebugBwd_here("entry")) {
        adDebugAdj_rReal8Array(Xb, ns);
        adDebugAdj_rReal8Array(Yb, ns);
        adDebugAdj_rDisplay("entry", 0);
    } else
        adDebugAdj_skip("entry");
}

double polysurf_nodiff(double *X, double *Y, int ns) {
    double surf = 0.0;
    int cp, pp;
    for (cp = 0; cp < ns; ++cp) {
        pp = cp - 1;
        if (pp == -1)
            pp = ns - 1;
        surf += (X[pp]*Y[cp]-X[cp]*Y[pp])/2;
    }
    return surf;
}

/*
  Differentiation of polycost in reverse (adjoint) mode (with options context debugAdjoint no!SpareInits fixinterface):
   gradient     of useful results: polycost
   with respect to varying inputs: *X *Y
   RW status of diff variables: X:(loc) *X:out Y:(loc) *Y:out
                polycost:in-killed
   Plus diff mem management of: X:in Y:in
*/
void polycost_b(double *X, double *Xb, double *Y, double *Yb, int ns, double 
        polycostb) {
    double perim;
    double perimb;
    double result1;
    double result1b;
    int i;
    perim = polyperim_nodiff(X, Y, ns);
    result1 = polysurf_nodiff(X, Y, ns);
    polycost = perim*perim/result1;
    pushReal8(result1);
    pushReal8(perim);
    if (adDebugBwd_here("exit")) {
        adDebugAdj_wReal8(&polycostb);
        adDebugAdj_wDisplay("exit", 0);
    } else
        adDebugAdj_skip("exit");
    popReal8(&perim);
    popReal8(&result1);
    for (i=0 ; i<ns ; ++i) {
      Xb[i] = 0.0;
      Yb[i] = 0.0;
    }
    perimb = 0.0;
    result1b = 0.0;
    perimb = perimb + 2*perim*polycostb/result1;
    result1b = result1b - perim*perim*polycostb/(result1*result1);
    polycostb = 0.0;
    adDebugBwd_call("polysurf", 0);
    if (adDebugBwd_here("afterCall")) {
        adDebugAdj_rReal8(&perimb);
        adDebugAdj_rReal8(&result1b);
        adDebugAdj_rDisplay("afterCall", -1);
    } else
        adDebugAdj_skip("afterCall");
    polysurf_b(X, Xb, Y, Yb, ns, result1b);
    if (adDebugBwd_here("beforeCall")) {
        adDebugAdj_wReal8Array(Xb, ns);
        adDebugAdj_wReal8Array(Yb, ns);
        adDebugAdj_wReal8(&perimb);
        adDebugAdj_wDisplay("beforeCall", -1);
    } else
        adDebugAdj_skip("beforeCall");
    adDebugBwd_exit();
    adDebugBwd_call("polyperim", 0);
    if (adDebugBwd_here("afterCall")) {
        adDebugAdj_rReal8Array(Xb, ns);
        adDebugAdj_rReal8Array(Yb, ns);
        adDebugAdj_rReal8(&perimb);
        adDebugAdj_rDisplay("afterCall", -1);
    } else
        adDebugAdj_skip("afterCall");
    polyperim_b(X, Xb, Y, Yb, ns, perimb);
    if (adDebugBwd_here("beforeCall")) {
        adDebugAdj_wReal8Array(Xb, ns);
        adDebugAdj_wReal8Array(Yb, ns);
        adDebugAdj_wDisplay("beforeCall", -1);
    } else
        adDebugAdj_skip("beforeCall");
    adDebugBwd_exit();
    if (adDebugBwd_here("entry")) {
        adDebugAdj_rReal8Array(Xb, ns);
        adDebugAdj_rReal8Array(Yb, ns);
        adDebugAdj_rDisplay("entry", 0);
    } else
        adDebugAdj_skip("entry");
}

/*
  Differentiation of main as a context to call adjoint code (with options context debugAdjoint no!SpareInits fixinterface):
*/
int main() {
    int ns;
    double X[5], Y[5];
    double Xb[5], Yb[5];
    double cost;
    double costb;
    ns = 5;
    X[0] = 0.0;
    Y[0] = 0.0;
    X[1] = 2.0;
    Y[1] = 0.0;
    X[2] = 2.0;
    Y[2] = 3.0;
    X[3] = 1.0;
    Y[3] = 6.0;
    X[4] = -3.0;
    Y[4] = 3.0;
    adDebugBwd_init(1.e-1, 0.87);
    if (adDebugBwd_here("end")) {
        adDebugAdj_wReal8(&costb);
        adDebugAdj_wDisplay("end", 0);
    }
    adDebugBwd_call("polycost", 0);
    if (adDebugBwd_here("afterCall")) {
        adDebugAdj_rReal8(&costb);
        adDebugAdj_rDisplay("afterCall", -1);
    } else
        adDebugAdj_skip("afterCall");
    polycost_b(X, Xb, Y, Yb, ns, costb);
    if (adDebugBwd_here("beforeCall")) {
        adDebugAdj_wReal8Array(Xb, 5);
        adDebugAdj_wReal8Array(Yb, 5);
        adDebugAdj_wDisplay("beforeCall", -1);
    } else
        adDebugAdj_skip("beforeCall");
    adDebugBwd_exit();
    if (adDebugBwd_here("start")) {
        adDebugAdj_rReal8Array(Xb, 5);
        adDebugAdj_rReal8Array(Yb, 5);
        adDebugAdj_rDisplay("start", 0);
    }
    adDebugAdj_conclude();
    printf(" cost=   %3.15f\n", cost);
}
