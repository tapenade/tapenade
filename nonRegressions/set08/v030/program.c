#include <math.h>
typedef struct fooelement {
  double *value_ptr;
  char tag;
} footype;

double func(footype *a, double b, double x[], int n)
{
  double r,t; int i;
  r = 1.0;
  for (i=0; i<n; i++) {
    t = (*a->value_ptr)*x[i]*b;
    if (t >= 0.0) {
      r *= sqrt(t);
    }
  }
  return r;
}
