enum _myenum {
  rouge, vert, bleu
} ;

typedef enum _myenum MyEnum ;

typedef enum _myotherenum {
  jaune, cyan, violet
} MyOtherEnum ;


struct _mystruct {
  float x;
  float y;
  int ii;
} ;

typedef struct _mystruct MyStruct ;

typedef struct _myotherstruct {
  float z,t ;
} MyOtherStruct ;

union _myunion {
  float fv ;
  int iv ;
} ;

typedef union _myunion MyUnion ;

typedef union _myotherunion {
  double fv ;
  long iv ;
} MyOtherUnion ;

void test(double *x) {
  MyEnum c1 = rouge ;
  MyEnum c2 = vert ;
  MyStruct s1 ;
  MyOtherStruct s2 ;
  MyUnion u1 ;
  MyOtherUnion u2 ;
  if (c2==vert) {
    MyOtherEnum c3 = jaune ;
    MyOtherEnum c4 = violet ;
    if (c3==rouge) {
      u2.fv = 2 * (*x) ;
      s2.t = 3 * u2.fv ;
      u1.fv = 4 * s2.t ;
      s1.y = 5 * u1.fv ;
      *x = s1.y ;
    }
  }
}
