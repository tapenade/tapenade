struct __darwin_pthread_handler_rec
{
	void           (*__routine)(void *);	/* Routine to call */
	void           *__arg;			/* Argument to pass */
	struct __darwin_pthread_handler_rec *__next;
};
