double RootFunction(double x[], double y[], int n)
{
   double acc;
   int i;
   int j;
   i = 0;
   j = n-1;
   acc = 0.0;
   while(i < n) {
       acc += x[i] * y[j];
       i++;
       j--;
   }
   j=2*j ;
   return j*acc;
}
