/** This is example "test3" of the ADMM test suite.
 * This example is inspired from multibody,
 * to test their method about allocating and freeing vectors
 * with our ADMM library */

#include <stdlib.h>
#include <stdio.h>

/* $AD NOCHECKPOINT */
double * createVector(int n){
    double* x = (double*)malloc(n*sizeof(double));
    return x;
}

/* $AD NOCHECKPOINT */
void freeVector(double *a){
    free(a);
    return;
}

void head(double x, double* y) {
  int i,j,k,l ;
  int n = 10 ;
  int m = 15;
  double *vecm = createVector(m) ;
  double *vecn = createVector(n) ;
  for (i=0 ; i<m ; ++i) {
    vecm[i] = (i+0.5)*x ;
  }
  for (i=0 ; i<n ; ++i) {
    vecn[i] = (i+0.3)*x ;
  }
  *y = 0.0 ;
  for (i=0 ; i<n ; i+=2) {
    *y = *y + vecm[i+3]*vecn[i] ;
  }
  freeVector(vecm) ;
  freeVector(vecn) ;
}

int main(int argc, char** argv) {
  double x,y ;
  x  = 3.50000 ;
  /*  x += 0.00001 ; */
  printf("input x is set to [3.500000==] %f \n", x);
  head(x,&y);
  printf("result y gets val [2465.312500==] %f \n", y);
  return 0;
}
