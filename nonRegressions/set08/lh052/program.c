/*
 * Pour reproduire le bug des oublis de TBR sur les pointeurs et
 * leurs pointeurs derives. (Bugs trouves sur Cazals-ABS)
 */

typedef struct {
  char name ;
  double val ;
} Obj ;

typedef struct {
  unsigned index;
} Ref ;

typedef struct {
  char strucname ;
  Obj objs[69];
} Stock ;

/* $AD NOCHECKPOINT */
Obj* getobj(const Stock* stk, const Ref* ref) {
  return &stk->objs[ref->index] ;
}

double getval(const Stock* stk, const Obj* p) {
  return p->val ;
}

double getdval(const Stock* stk, const Obj* p, const Obj* q) {
  return getval(stk, p) - getval(stk, q) ;
}

double compute(const Stock* stk, const Ref* ref1, const Ref* ref2) {
  double vala = getdval(stk, getobj(stk, ref1), getobj(stk, ref2)) ;
  double valb = getdval(stk, getobj(stk, ref2), getobj(stk, ref1)) ;
  return vala*valb ;
}

double root(const Stock* stk, const Ref* ref1, const Ref* ref2) {
  double comp = compute(stk, ref1, ref2) ;
  return comp*comp ;
}
