/* Bug found by Dan Moerder daniel.d.moerder@nasa.gov
 * xd should be declared as double xd[NBDIRS]
 * rather than (*xd)[NBDIRS]  */

void fun0(const double *x, const double z[2], double *y)
{
  *y = *x + z[1];
}
