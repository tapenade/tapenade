/* Pour reproduire un bug de ALIF qui cause des dummyzerodiff.
 * Une partie du bug vient de la creation de 2 ActivityPattern's
 *  pour InputsSet, lie a l'ordre dans lequel on rencontre les
 *  call-contexts et dans lequel on cree les ActivityPattern's.
 * L'autre partie du bug est (une fois prise cette decision
 *  discutable d'avoir 2 ActivityPattern's) une incoherence entre
 *  l'activite du call et l'activite de ses actual-arguments */

#include <stdio.h>
#include <stdlib.h>

typedef struct {
  double *store[10] ;
  int ii1 ;
} Wrap ;

typedef struct {
  Wrap *inside ;
  int ii2 ;
} Box ;

/* $AD NOCHECKPOINT */
void InputsSet(Wrap* wrap, int index, double* vals) {
  if (wrap->store[index]) free(wrap->store[index]) ;
  wrap->store[index] = vals ;
}

void top(double xx, Box* box, double *yy) {
  double *V1 = (double*)malloc(7*sizeof(double));
  double *V2 = (double*)malloc(7*sizeof(double));
  double *V3 = (double*)malloc(7*sizeof(double));
  int i ;
  for (i=0 ; i<7 ; ++i) {
    V1[i] = xx ;
    V2[i] = xx ;
    V3[i] = xx ;
  }
  if (xx>0) {
    InputsSet(box->inside, 1, V1) ;
    InputsSet(box->inside, 2, V2) ;
    InputsSet(box->inside, 3, V3) ;
  } else {
    InputsSet(box->inside, 3, V3) ;
  }
  *yy = (box->inside->store[1])[5] * (box->inside->store[2])[4] ;
}

int main() {
  double xx = 1.5 ;
  double yy = 0.0 ;
  Box box ;
  Wrap wrap ;
  int ii3 ;
  box.inside = &wrap ;
  for (ii3=0 ; ii3<10 ; ++ii3)
    wrap.store[ii3] = (double*)malloc((ii3+3)*sizeof(double)) ;
  top(xx, &box, &yy) ;
  printf("yy=%f\n",yy) ;
}
