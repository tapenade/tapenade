

double ADMax(double x, double y)
{
    if(x > y)
        return x;
    else
        return y;
}

#include <math.h>

double RootFunction(double x[], int n)
{
    double y;
    double z;
    int i;
    y = x[0];
    for(i=1;i<n;i++) {
      z = sqrt(x[i]);
      y = y+ADMax(y, z);
      
    }
    return y;
}
