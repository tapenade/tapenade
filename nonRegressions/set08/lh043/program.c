/* Bug1 Morlighem 01/10/2015
 * Pbs about C structs, initialization-in-declarations */

#include<stdio.h>
#include <stdlib.h>

void CostFunction(double*,double);

main(){
	double a = 2.;
	double J;
	
	CostFunction(&J,a);
	printf("Cost function = %g\n",J);
}

/*Define GaussTria structure*/
typedef struct{
	int    numgauss;
	double weights[3];
	double weight;
} GaussTria;

GaussTria* GaussTriaNew(double a){
	GaussTria* gauss = (GaussTria*)malloc(sizeof(GaussTria));
	gauss->numgauss = 3;
	gauss->weights[0] = a;
	gauss->weights[1] = 2.;
	gauss->weights[2] = 3.;
	gauss->weight     = -1.;
	return gauss;
}
void GaussTriaPoint(GaussTria* gauss,int ig){
	gauss->weight=gauss->weights[ig];
}

void CostFunction(double* pJ,double a){
	int    ig;
	double J = 0.;

	GaussTria* gauss ;
        gauss=GaussTriaNew(a);
	for(ig=0;ig<gauss->numgauss;ig++){
		GaussTriaPoint(gauss,ig);

		J += gauss->weight;
	}

	/*Assign output pointer*/
	*pJ = J;
}
