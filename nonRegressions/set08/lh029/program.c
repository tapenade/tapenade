/* 2nd mini example of bugs found in "multibody" application (cf Krishna) */

double ** createMatrix(int m, int n);
double ** createMatrix0(int m, int n);
void eraseMatrix(int m, int n, double** A);
void freeMatrix(double **A);

double ** createMatrix(int m, int n){
    int i;
    double **matrix, *linemat;
    matrix = (double**)calloc(m, sizeof(double *));
    matrix[0] = linemat = (double*)calloc(m*n, sizeof(double));
    for (i=1; i<m; i++){
        matrix[i] = linemat+n*i;
    }
    return matrix;
}

double **createMatrix0(int m, int n){
    double** A = createMatrix(m, n);
    eraseMatrix(m, n, A);
    return A;
}

void eraseMatrix(int m, int n, double** A){
    int i, j;
    for (i=0; i<m; i++){
        for (j=0; j<n; j++){
            A[i][j] = 0.0;
        }
    }
    return;
}

void freeMatrix(double **A){
    free(A[0]);
    free(A);
    return;
}

void eval1(double **mat, double val) {
  mat[2][3] = val ;
}

void addtores(double **mat, double *res) {
  *res = *res + mat[6][7] ;
}

void eval2(double **mat, double passive, double *res) {
  mat[5][10] = mat[5][10]*passive ;
}

int RTDynTRC(double *xx, double *yy, double pp) {
  double **matrix1, **matrix2 ;
  int i,j ;

  matrix1 = createMatrix0(99,6);
  eval1(matrix1, *xx) ;
  addtores(matrix1, yy) ;

  matrix2 = createMatrix(99,6);
  for (i=0 ; i<99 ; ++i) {
    for (j=0 ; j<6 ; ++j) {
      matrix2[i][j] = pp ;
    }
  }
  eval2(matrix2, pp, yy) ;

  freeMatrix(matrix1);
  freeMatrix(matrix2);
  return 0 ;
}
