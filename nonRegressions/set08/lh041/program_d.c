/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Jan 2021 11:51
*/
/*
  Differentiation of VecFree in forward (tangent) mode:
   variations   of useful results: *rr
   with respect to varying inputs: *rr
   RW status of diff variables: rr:(loc) *rr:in-out
   Plus diff mem management of: rr:in
*/
void VecFree_d(double **pvec, double *rr, double *rrd) {
    if (*pvec)
        free(*pvec);
    *pvec = (char *)0;
    *rrd = 2*(*rrd);
    *rr = 2*(*rr);
}
