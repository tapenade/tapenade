/*
 * Le meme que lh53 mais tout inline
 * pour tester TBR pur sans checkpoint
 */

typedef struct {
  char name ;
  double val ;
} Obj ;

typedef struct {
  unsigned index;
} Ref ;

typedef struct {
  char strucname ;
  Obj objs[69];
} Stock ;

double compute(const Stock* stk, const Ref* ref1, const Ref* ref2) {
  Obj* obj1 = &stk->objs[ref1->index] ;
  Obj* obj2 = &stk->objs[ref2->index] ;
  double vala = obj1->val * obj2->val ;
  obj1 = &stk->objs[ref2->index] ;
  obj2 = &stk->objs[ref1->index] ;
  double valb = obj1->val * obj2->val ;
  return vala * valb ;
}

double root(const Stock* stk, const Ref* ref1, const Ref* ref2) {
  double comp = compute(stk, ref1, ref2) ;
  return comp*comp ;
}
