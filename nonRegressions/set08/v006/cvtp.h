/* $Id: cvtp.h,v 1.1 10/.0/.0 .1:.0:.2 vmp Exp $ */
#ifndef CVTP_LOADED
#define CVTP_LOADED

#include <stdio.h>

/******************************* Structures: */

typedef char *AtomValue ;

typedef struct _Operator{
  char              *name ;
  short              arity ;
  short              rank ;
} Operator ;

typedef struct _Tree{
  Operator          *oper ;
  union{
    struct _ListTree  *sons ;
    AtomValue          value ;
  }                  contents ;
  struct _KeyListTree  *annotations ;
} Tree ;

typedef struct _ListTree{
  struct _Tree      *tree ;
  struct _ListTree  *next ;
} ListTree ;

typedef struct _KeyListTree{
  char                 *key ;
  struct _Tree         *tree ;
  struct _KeyListTree  *next ;
} KeyListTree ;

/******************************* Public Forward Declarations: */

Tree *mkTree(Operator *oper, ListTree *sons) ;
Tree *mkAtom(Operator *oper, AtomValue value) ;
ListTree *mkSons(Tree *tree, ListTree *sons) ;
Tree *mkPost(Tree *lt, Tree *nt) ;
Tree  *mkPre(Tree *nt, Tree *lt) ;
Tree **getSetToAnnotationTree(Tree *tree, char *key) ;
void deleteAnnotation(Tree *tree, char *key) ;

short treeOpCode(Tree *t) ;
short treeNbSons(Tree *t) ;
AtomValue treeAtomValue(Tree *t) ;
ListTree *treeSons(Tree *t) ;
Tree *treeDown(Tree *t, short rank) ;
Tree *tailSon(ListTree *listTree, short tailRank) ;
int listTreeLength(ListTree *listTree) ;
Tree *listTreeNth(ListTree *listTree, int rank) ;
Tree *lastTree(ListTree *listTree) ;
KeyListTree *getAnnotationPlace(Tree *tree, char *key) ;
Tree *getAnnotationTree(Tree *tree, char *key) ;

void freeTreeNode (Tree *tree) ;
void freeTree (Tree *tree) ;
void showTree(Tree *t, int indent) ;

/******************************* End cvtp.h */

#endif 
