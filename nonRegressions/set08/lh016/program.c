#include <math.h>

double ADFloor(double x)
{
    if(x > 0.0)
        return x;
    else
        return 0.0;
}

double RootFunction(double x[], double y, int n)
{
    double z;
    int i;
    z = y * 3;
    for(i=1;i<n;i++) {
      z = y + x[i];
      y = ADFloor(y + z);
    }
    return y;
}


