/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_llhTests) - 14 Jun 2021 15:32
*/
/*
  Differentiation of sub1 in forward (tangent) mode:
   variations   of useful results: *x *z sub1
   with respect to varying inputs: y *z
   Plus diff mem management of: x:in z:in
*/
float sub1_d(float *x, float *xd, float y, float yd, float *z, float *zd, 
        float *sub1) {
    *xd = 3.7*yd;
    *x = 3.7*y;
    *zd = *zd + 2*yd;
    *z = *z + 2*y;
    *sub1 = y*(*z);
    return (*z)*yd + y*(*zd);
}

/*
  Differentiation of top in forward (tangent) mode:
   variations   of useful results: *b
   with respect to varying inputs: a *b
   RW status of diff variables: a:in b:(loc) *b:in-out
   Plus diff mem management of: b:in
*/
void top_d(float a, float ad, float *b, float *bd) {
    float x, y, z;
    float xd, yd, zd;
    float result1;
    float result1d;
    if (a > 0.0) {
        yd = 2.0*ad;
        y = 2.0*a;
        zd = ad;
        z = a - 7.0;
        result1d = sub1_d(&x, &xd, y, yd, &z, &zd, &result1);
        yd = result1*yd + y*result1d;
        y = y*result1;
        *bd = z*(y*xd+x*yd) + x*y*zd;
        *b = x*y*z;
    }
    a = -2.9;
    ad = 0.0;
}
