/* program.f -- translated by f2c (version 19991025).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    doublereal zn, zd;
} zz_;

#define zz_1 zz_

/* Subroutine */ int head_ (doublereal *i1, doublereal *i2, doublereal *o)
{
    extern /* Subroutine */ int sub0_();

    sub0_(i1, i2);
    *o = zz_1.zn / zz_1.zd;
    return 0;
} /* head_ */

/* Subroutine */ int sub0_
  (doublereal *u, doublereal *v)
{
    static doublereal z1, z2;
    extern /* Subroutine */ int sub1_(), sub2_();

    sub1_(u, &z1);
    sub2_(v, &z2);
    zz_1.zn = z1 - z2;
    zz_1.zd = z1 + 1 + z2;
    return 0;
} /* sub0_ */

/* Subroutine */ int sub1_
  (doublereal *x, doublereal *y)
{
    /* Builtin functions */
    double sqrt();

    /* Local variables */
    extern doublereal f_(), g_();

    *y = f_(x) + g_(x);
    *y = sqrt(*y);
    return 0;
} /* sub1_ */

/* Subroutine */ int sub2_
  (doublereal *x, doublereal *y)
{
    /* Builtin functions */
    double sqrt();

    /* Local variables */
    extern doublereal f_(), g_();

    *y = f_(x) - g_(x);
    *y = sqrt(*y);
    return 0;
} /* sub2_ */

doublereal f_(doublereal *t)
{
    /* System generated locals */
    doublereal ret_val;

    /* Builtin functions */
    double exp();

    ret_val = *t * *t;
    ret_val = exp(ret_val);
    return ret_val;
} /* f_ */

doublereal g_(doublereal *t)
{
    /* System generated locals */
    doublereal ret_val;

    /* Builtin functions */
    double sin();

    ret_val = 1.;
    if (*t != 0.) {
	ret_val = sin(*t) / *t;
    }
    return ret_val;
} /* g_ */

