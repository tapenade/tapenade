#ifndef SECTIONA_D_LOADED
#define SECTIONA_D_LOADED
/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.13 (r6885M) - 28 May 2018 11:30
*/
typedef struct _StructA {
    int ii1;
    float ff1;
} StructA;
typedef struct _StructA_diff {
    float ff1;
} StructA_diff;
#endif
