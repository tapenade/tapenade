/* One step in the direction of treating aliasing from outside the diff part.
 * Here we moved the procedure creating aliasing into the diff part.
 * The next step could be leave the aliasing procedure outside, then
 * diff with -context option and make sure this option detects and propagates
 * aliasing inside the diff part.
 * Also, we want the diff main to be called main() and not main_d()
 * Also the -context option should prepare the derivative inputs
 * and maybe print the derivative results */

#include <stdio.h>
#include <stdlib.h>

typedef struct {
  double * array1 ;
  double * array2 ;
} mymod ;

void connect(mymod *model) {
  model->array2 = model->array1 ;
}

void start(double x,  mymod *model) {
  int i ;
  for (i=3 ; i>=0 ; --i)
    model->array1[i] = x*x/(i+1) ;
}

void finish(mymod *model, double *y) {
  int i ;
  *y = 1.0 ;
  for (i=3 ; i>=0 ; --i)
    *y = *y * model->array2[i] ;
}
  
void root(double x, mymod *model, double *y) {
  connect(model) ;
  start(x,model) ;
  finish(model,y) ;
}

int main(int argc, char **argv) {
  double x, y ;
  mymod model ;
  int i ;
  model.array1 = malloc(4*sizeof(double)) ;
  model.array2 = NULL ;
  x = 1.0 ;
  y = 0.0 ;
  printf("x=%f\n",x) ;
  root(x,&model,&y) ;
  printf("y=%f\n",y) ;
}
