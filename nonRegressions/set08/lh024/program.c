#include <math.h>

/* Faire bien attention que program_b.c DOIT
 * declarer polysurf() et polyperim() en extern double,
 * parce que polycost_b les utilise ! */
double polyperim(double *X, double *Y, int ns) {
  double perim = 0.0 ;
  int cp,pp ;
  double dx,dy ;
  
  for (cp=0 ; cp<ns ; ++cp) {
    pp = cp - 1 ;
    if (pp==-1) pp = ns-1 ;
    dx = X[cp] - X[pp] ;
    dy = Y[cp] - Y[pp] ;
    perim += sqrt(dx*dx + dy*dy) ;
  }
  return perim ;
}

double polysurf(double *X, double *Y, int ns) {
  double surf = 0.0 ;
  int cp,pp ;

  for (cp=0 ; cp<ns ; ++cp) {
    pp = cp - 1 ;
    if (pp==-1) pp = ns-1 ;
    surf += (X[cp]*Y[pp] - X[pp]*Y[cp])/2 ;
  }
  return surf ;
}

double polycost(double *X, double *Y, int ns) {
  double perim = polyperim(X,Y,ns) ;
  return perim*perim/polysurf(X,Y,ns) ;
}
