#include <stdlib.h>

void IntegreAll(double* UH, double *VH, double *H, double *Q,
                double* UHint, double *VHint, double *Qint,
                int Nx, int Ny, double dx, double dy, double dt)
{
  int i, j;
  
  double flux_UH_b = 0.0, flux_UH_u = 0.0;
  double flux_VH_b = 0.0, flux_VH_u = 0.0;
  
  double ZQ, U, V;
  
  //Compute UHint, VHint along y
  for(i = 1; i < Nx - 1; i++)
    {
      ComputeFluxHLL_y( UH[i + 0 * Nx], UH[i + 1 * Nx],
                        VH[i + 0 * Nx], VH[i + 1 * Nx],
                        &flux_UH_u, &flux_VH_u);
      for(j = 1; j < Ny - 1; j++)
        {
          flux_UH_b = flux_UH_u;
          flux_VH_b = flux_VH_u;

          ComputeFluxHLL_y( UH[i + j * Nx], UH[i + (j + 1) * Nx],
                            VH[i + j * Nx], VH[i + (j + 1) * Nx],
                            &flux_UH_u, &flux_VH_u);
       
          UHint[i + j * Nx] += - dt / dy * (flux_UH_u - flux_UH_b);
          VHint[i + j * Nx] += - dt / dy * (flux_VH_u - flux_VH_b);
        }
    }

  for(j = 1; j < Ny - 1; j++)
    for(i = 1; i < Nx - 1; i++)
      {
        U = UH[i + j * Nx] / H[i + j * Nx];
        V = VH[i + j * Nx] / H[i + j * Nx];
        ZQ =
          - 0.5 * (U * (Q[i + 1 + j * Nx] - Q[i + j * Nx]) +
                   U * (Q[i + j * Nx] - Q[i - 1 + j * Nx])) / dx
          - 0.5 * (V * (Q[i + (j + 1) * Nx] - Q[i + j * Nx]) +
                   V * (Q[i + j * Nx] - Q[i + (j - 1) * Nx])) / dy;
        
        Qint[i + j * Nx] = Q[i + j * Nx] + dt * ZQ;
      }
}
