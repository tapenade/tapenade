extern int abs(int __x);

extern int mac(int a, int b)
{
  return a > b ? a : abs(b);
}
