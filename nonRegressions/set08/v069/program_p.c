/*        Generated by TAPENADE     (INRIA, Tropics team)
    Tapenade 3.5 (r3791M) - 25 Mar 2011 09:27
*/
extern int abs(int __x);

extern int mac(int a, int b) {
    int abs0;
    if (b >= 0.)
        abs0 = b;
    else
        abs0 = -b;
    return (a > b ? a : abs0);
}
