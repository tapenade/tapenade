/** This is example "test2" of the ADMM test suite.
 * This example is inspired from multibody,
 * to test their method about allocating and freeing matrices
 * with our ADMM library */

#include <stdlib.h>
#include <stdio.h>

/* $AD NOCHECKPOINT */
double ** createMatrix(int m, int n) {
    int i;
    double **matrix, *mat;
    matrix = (double**)malloc(m*sizeof(double *));
    mat = (double*)malloc(m*n*sizeof(double));
    matrix[0] = mat ;
    for (i=1; i<m; i++){
        matrix[i] = mat+n*i;
    }
    return matrix;
}

/* $AD NOCHECKPOINT */
void freeMatrix(double **A) {
    free(A[0]);
    free(A);
    return;
}

void head(double x, double* y) {
  int i,j,k,l ;
  int n = 10 ;
  int m = 15;
  double **mymat = createMatrix(m,n) ;
  for (i=0 ; i<m ; ++i) {
    for (j=0 ; j<n ; ++j) {
      mymat[i][j] = (i+0.5)*x/(j+1.0) ;
    }
  }
  *y = 0.0 ;
  for (i=0 ; i<m ; ++i) {
    for (j=0 ; j<n ; ++j) {
      k = i-1 ;
      l = j-1 ;
      if (k<0) k=1 ;
      if (l<0) l=1 ;
      *y = *y + mymat[i][j]*mymat[k][l] + 1.0 ;
    }
  }
  freeMatrix(mymat) ;
}

int main(int argc, char** argv) {
  double x,y ;
  x  = 3.50000 ;
  printf("input x is set to [3.500000==] %f \n", x);
  head(x,&y);
  printf("result y gets val [17510.0875==] %f \n", y);
  return 0;
}
