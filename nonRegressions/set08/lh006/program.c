float sub1(float x, float y, float z, float t, float *u, float *a) {
  x = 4.3*x - 2.1 ;
  x = 2.0*y + *u ;
  z = z*y + 1.2*(*u) ;
  *u = z*(*u) ;
  return x*z + t*(*a) ;
}

void top(float *a) {
  float x,y,z,b,c,u ;
  x = *a ;
  y = *a ;
  z = *a ;
  b = *a ;
  c = *a ;
  u = *a ;
  y = y*sub1(x,y,z,b*c,&u,a) ;
  *a = *a*x*y*z*b*c*u ;
}

