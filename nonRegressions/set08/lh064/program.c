/* pour tester modifiedDeclarator restrict, bug servlet 20150902
 et arrayConstructor, bug servlet 20150818 */

void asg_pstep2d(float ** const v0, float ** restrict v1,
		 float *  const v2, float *  restrict v3,
		 float const   *v4, float restrict   *v5,
		 float *sdiv) {
  *sdiv = *sdiv + **v0 + **v1 + *v2 + *v3 + *v4 + *v5;
}
