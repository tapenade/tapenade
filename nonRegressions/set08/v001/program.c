/**
 *  Usage: ftoctab
 *
 *  Prints a table of conversions from degrees
 *  Fahrenheit to degrees Celsius for Fahrenheit
 *  temperatures from -40.0 to 100.0.
 *
 *  This program demonstrates the use of for loops,
 *  functions, and floating point numbers.
 */

#include <stdio.h>

const float fMin  = -40.0;	// min F temperature
const float fStep =   5.0;	// F temperature step
const float fMax  = 100.0;	// max F temperature

/**
 *  fToC(fdeg) returns the Celsius equivalent of fdeg
 *  degrees Fahrenheit.
 */
float fToC(float fdeg) {			// Note 1.
    return (fdeg - 32.0)/1.8;			// Note 2.
}  // int fToC(int)

int main(void) {
    float f;

    printf("Degrees F   Degrees C\n");		// Note 3.
    printf("---------   ---------\n");		// Note 3.
    for (f = fMin; f <= fMax; f += fStep) {	// Note 4.
	printf("%7.1f%12.1f\n", f, fToC(f));	// Note 5.
    }						// Note 4.
    printf("\n");
    return 0;
}  // int main(void)
