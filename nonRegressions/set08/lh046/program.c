/** Bug Morlighem du 6 oct 2015.
 * Avec l'option nooptim activity, certains champs
 * du record (e.g. weight) ne sont pas actives. */

#include "bug3.h"

main(){
	double a = 2.;
	double J;
	
	CostFunction(&J,a);
	printf("Cost function = %g\n",J);
}
GaussTria* GaussTriaNew(double a){
	int i;
	GaussTria* gauss = (GaussTria*)malloc(sizeof(GaussTria));
	gauss->numgauss = 3;
	gauss->weights[0] = a;
	gauss->weights[1] = 2.;
	gauss->weights[2] = 3.;
	gauss->weight     = -1.;
	for(i=0;i<3;i++) gauss->coords1[i]=0.;
	for(i=0;i<3;i++) gauss->coords2[i]=0.;
	for(i=0;i<3;i++) gauss->coords3[i]=a;
	return gauss;
}
void GaussTriaPoint(GaussTria* gauss,int ig){
	gauss->weight=gauss->weights[ig];
	gauss->coord1=gauss->coords1[ig];
	gauss->coord2=gauss->coords2[ig];
	gauss->coord3=gauss->coords3[ig];
}

void CostFunction(double* pJ,double a){
	int    ig;
	double J = 0.;

	GaussTria* gauss=NULL;
	gauss=GaussTriaNew(a);
	for(ig=0;ig<gauss->numgauss;ig++){
		GaussTriaPoint(gauss,ig);

		J += gauss->weight + gauss->coord3;
	}

	/*Assign output pointer*/
	*pJ = J;
}
