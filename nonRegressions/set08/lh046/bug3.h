#include<stdio.h>
#include <stdlib.h>

void CostFunction(double*,double);

/*Define GaussTria structure*/
typedef struct{
	int    numgauss;
	double weights[3];
	double coords1[3];
	double coords2[3];
	double coords3[3];

	/*Current gauss point*/
	double weight;
	double coord1;
	double coord2;
	double coord3;
} GaussTria;

GaussTria* GaussTriaNew(double a);
void GaussTriaPoint(GaussTria* gauss,int ig);
void CostFunction(double* pJ,double a);
