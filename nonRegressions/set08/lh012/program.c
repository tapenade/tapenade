double **A ;

void F( double *X , int n , double *Y , int m )
{
 int i,j ;

 for (j=0 ; j<m ; j++ ) {
   Y[j] = 0.0 ;
   for (i=0 ; i<n ; i++)
     Y[j] = Y[j] + A[j][i]*X[i] ;
   Y[j] /= 2.0;
 }
} 
