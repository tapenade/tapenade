void qr(double *R) {
  double V = 3.5*(*R) ;
  for (int n = 0; n!=17; ++n) {
    *R = (*R)*V ;
  }
}

void inv(double *R2) {
  qr(R2);
}
