//----------------------------------------------------------------------
//
// Lighthouse
//
//----------------------------------------------------------------------
#include <math.h>
#include <stdio.h>           

//----------------------------------------------------------------------
// lighthouse function

void eval_f(double x[4], double y[2]);

//----------------------------------------------------------------------
// main program

main (void)
{ int i;
  // independents
  double x[4];
  // dependents
  double y[2];

  //--------------------------------------------------------------------
  // Initialization of independents 

  x[0] = 3.7; 
  x[1] = 0.7; 
  x[2] = 0.5; 
  x[3] = 0.5; 

  //--------------------------------------------------------------------
  // Function evaluation

  eval_f(x,y);

  //--------------------------------------------------------------------
  // Output

  fprintf(stdout,"x=[");
  for (i=0; i<4; i++)
    fprintf(stdout,"%14.6le",x[i]);
  fprintf(stdout," ]\n");
  fprintf(stdout,"y=[");
  for (i=0; i<2; i++)
    fprintf(stdout,"%14.6le",y[i]);
  fprintf(stdout," ]\n");
}

//----------------------------------------------------------------------

void eval_f(double x[4], double y[2])
 {

  // intermediates
   double v[7];

  v[0] = x[2] * x[3];
  v[1] = tan(v[0]);
  v[2] = x[1] - v[1];
  v[3] = x[0] * v[1];
  v[4] = v[3] / v[2];
  v[5] = v[4];
  v[6] = v[4] * x[1];

  y[0] = v[5]; 
  y[1] = v[6];
 }

//----------------------------------------------------------------------
// that's it



