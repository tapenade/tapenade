#include <math.h>

void test (float a, float *b) {
#include "locals.h"
  *b = a;
  /* pointer p is local */
  float *p;
  if (*b > 0) {
    p = &y;
  } else {
    p = &x;
    /* p doesn't point to y */
    *p = sin(a);
  }
  /* y is never active */
  *b = *b + (*p)*y;
}
