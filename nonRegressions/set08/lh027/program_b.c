/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) - 14 Feb 2022 12:36
*/
#include <adStack.h>

/*
  Differentiation of test in reverse (adjoint) mode:
   gradient     of useful results: *A B[_:_]
   with respect to varying inputs: *A B[_:_]
   RW status of diff variables: A:(loc) *A:in-out B:(loc) B[_:_]:in-out
   Plus diff mem management of: A:in B:in

 Un essai pour tester l'arithmetique des pointeurs */
void test_b(double *A, double *Ab, double B[], double Bb[], int N) {
    double *p1, *p2;
    double *p1b, *p2b;
    int i, j;
    double tmp;
    double tmpb;
    p1b = Ab;
    i = 2;
    j = N/10;
    p2b = p1b + i*j;
    ++p2b;
    pushPointer8(p1b);
    p1b = p1b + 3;
    tmpb = *p1b;
    *p1b = 0.0;
    *p2b = *p2b + 2*tmpb;
    popPointer8((void **)&p1b);
    --p2b;
    Bb[8] = Bb[8] + *p2b;
    *p2b = 0.0;
    *p1b = *p1b + 2*Bb[5];
    Bb[5] = 0.0;
}
