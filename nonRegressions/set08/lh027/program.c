/* Un essai pour tester l'arithmetique des pointeurs */
int test(double *A, double B[], int N) {
  double *p1, *p2 ;
  int i,j ;
  p1 = A ;
  B[5] = 2**p1 ;
  i = 2 ;
  j = N/10 ; ;
  p2 = p1 + i*j ;
  *p2 = B[8] ;
  ++p2 ;
  p1 = p1 + 3 ;
  *p1 = 2**p2 ;
}
