/* Test that checks that "-context" mode does detect
 * the aliasing issue between X and Y in foo.
 * Assignment Y[] += X[] can be correctly split
 * because of aliasing only if PointerAnalysis propagates top-down,
 * but even if it doesn't, Tapenade sends a warning message.
 * Aliasing also makes validation wrong because initializations
 * on A and B overlap. */
#include <stdlib.h>
#include <stdio.h>

void foo(double *X, double *Y, int l) {
  for (int i=0 ; i<l ; ++i) {
    Y[i] += X[i] ;
  }
}

int main() {
  double *A = malloc(20*sizeof(double)) ;
  for (int i=0 ; i<20 ; ++i) A[i] = 2.5*i ;
  double *B = &A[5] ;
  double sum = 0.0 ;
  foo(A, B, 15) ;
  for (int i=0 ; i<20 ; ++i) sum += A[i] ;
  printf("sum=%f\n", sum) ;
  free(A) ;
}

