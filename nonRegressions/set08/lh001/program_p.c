/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.13 (r6809:6810M) - 30 Mar 2018 13:21
*/
/* comment on Sub1 */
void sub1(float i1, float i2, float o1, float o2) {
    float l1, l2;
    /* comment before statements */
    l1 = i1*i2;
    l2 = i1 - 3*i2;
    /* comment at the middle */
    o1 = l1/l2;
    o2 = 35.0;
    i1 = 99.0;
    /* comment at the end */
}

/* comment between subs */
void top(float i1, float i2, float i3, float o1, float o2, float o3) {
    float l1, l2, l3;
    o3 = i3*i2;
    sub1(i1, i2, o1, o2);
    o1 = o1*o2*i2;
    o3 = 2.0;
    i2 = 5.0;
}
/* comment at end of file */
