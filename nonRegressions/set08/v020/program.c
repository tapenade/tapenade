typedef union float_or_double {
    float f;
    double d;
} f_or_d;

void test(f_or_d x, f_or_d *y){
    y->d = x.dfaux * x.d;
    return;
} 
