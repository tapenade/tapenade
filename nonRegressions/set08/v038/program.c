typedef unsigned int size_t;
__extension__ typedef int __ssize_t;

typedef __ssize_t __io_read_fn (void *__cookie, char *__buf, size_t __nbytes);
