/** Exemple illustrant un bug du a l'absence d'une propagation de
 * diff-liveness sur les instructions derivees de calcul d'adresse. */

static double A[10] =
  {0., 1., 2., 3., 4., 5., 6., 7., 8., 9.} ;

double test(double B, int n) {
  int i, j ;
  double *p1, *p2 ;
  p1 = A ;
  i = 2 ;
  j= n/10 ;
  p2 = p1 + i*j ;
  *p2 = B ;
  p1 = p1 + 3 ;
  *p1 = *p2 * 2.0 ;
  return *p1 ;
}
  
