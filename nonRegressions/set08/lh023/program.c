double ** Lower_Triangular_Inverse_Simpler(double **L, int n)
{
  double **Linv;
  int i, j, k;
  double sum;

  Linv= (double**) malloc(n *sizeof(double*));
  for( i = 0 ; i < n ; i++ )
    Linv[i] = (double*) malloc (n *sizeof(double));

  //         Invert the diagonal elements of the lower triangular matrix L.
  for( k = 0 ; k < n ; k++ )
    Linv[k][k]=1.0/L[k][k];

  //         Invert the remaining lower triangular matrix L row by row.
  for (i = 1; i < n; i++)
    {
      Linv[i][0]=L[i][0];
      for (j = 0; j < i;  j++)
        {
          sum = 0.0;
          for (k = j; k < i; k++)
            sum += L[i][k] * Linv[k][j];
          Linv[i][j] = - sum / L[i][i];
        }
    }
  return Linv;
}
