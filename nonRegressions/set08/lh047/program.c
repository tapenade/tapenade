void CostFunction(double *xx, double* pJ) {
  double J = 0. ;
  double J2 ;
  J = J + (*xx)*(*xx) ;
  pass1(&J,&J2) ;
  pass2(&J2) ;
  J = J2 ;
  *pJ = J ;
}

void pass1(void *fromv, void *tov) {
  *((double *)tov) = *((double *)fromv) ;
}

void pass2(void *pvar) {
  *((double *)pvar) = 2.0* *((double*)pvar) ;
}

  
