static int g1 = -12;
static const long l1 = 12;
const static long l2 = 11;
int g2;
int g3, *pg4;
unsigned int g5;
static unsigned char g6;
static long int g7;
const char *p1;
const unsigned int *p2;
long * const p3 = 0;
const int i1 = 3;
const int *p4 = &i1;
const int * const p5 = &i1;
const int * const p6, p7;  /* p6 ET p7 sont const ! */
char **p8, p9;
unsigned char * const *p10;
unsigned char * const *p11, p12;
unsigned char * const *p13, *p14;
long int * const * restrict p15;     /* C99 only */
long int l3, * const p16 = &g7;
long int l4, * const * const p17 = &p16;
long int l5, * const * p18;

int len, maxlen;
int *lengths[];
int *lengths2[12];
int *lengths3[5][6];
const int lengths4[2];

char p, lineptr[100];

double f1;
double *f2;
