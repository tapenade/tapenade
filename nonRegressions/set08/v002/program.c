/************************************************************************
 *
 * Purpose: Program to demonstrate functions.
 * Author:  M J Leslie.
 * Date:    28-Feb-94
 *
 ************************************************************************/
#include <stdio.h>

float add( float, float); 			
/* Function declaration 	*/

main()
{
  float i=1;
  printf("i %f ...", i);

  i = add(1, 1);			/* Function call       		*/

  printf("%f \n",i);
}

/************************************************************************/

float add( float a, float b) 			/* Function definition 		*/
{
  float c;
  c = a + b;
  return c;
}
