/* #include<stdio.h> */

typedef struct _point1 {
  float x,y,z ;
} POINT1 ;

static POINT1 glob0 ;
POINT1 glob1 ;
extern POINT1 glob2 ;
extern POINT1 glob3 ;

float FF1(float, float) ;
static void FF0(float, float, float*, float*) ;
extern float FF2(float*, float*) ;
extern void FF3(float*, float*) ;

static void setglob(POINT1 *pt, float val) {
  pt->x = val+1.0 ;
  pt->y = val+2.0 ;
  pt->z = val+3.0 ;
}

static void FF0(float aa, float bb, float *cc, float *dd) {
  *cc = FF2(&aa,&bb) ;
  *dd = FF1(aa,*cc) ;
}

float FF1(float xx, float yy) {
  return xx*yy ;
}

int main() {
  float aa = 1.0 ;
  float bb = 2.0 ;
  setglob(&glob0,  0.0) ;
  setglob(&glob1, 10.0) ;
  setglob(&glob2, 20.0) ;
  setglob(&glob3, 30.0) ;
  float cc,dd ;
  FF0(aa,bb,&cc,&dd) ;
/*   printf(" ==> %f %f\n",cc,dd) ; */
}
