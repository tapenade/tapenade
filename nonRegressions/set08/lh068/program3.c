
typedef struct _point3 {
  float v1,v2,v3 ;
} POINT3 ;

static POINT3 glob0 ;
extern POINT3 glob1 ;
extern POINT3 glob2 ;
POINT3 glob3 ;

extern float FF1(float arg1, float arg2) ;
extern void FF3(float *gg, float *hh) ;
extern float FF2(float*, float*) ;

static float FF4(float v) {
  return v+glob1.v3 ;
}

void FF3(float *gg, float *hh) {
  float res = FF1(*gg,*hh) ;
  *gg = 2.0 * *gg + FF4(res) ;
  *hh = 3.0 * *hh - *gg ;
}
