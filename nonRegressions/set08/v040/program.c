#include <stdio.h>

void add (float a, float b, float *result) {
  *result = a + b;
}


void mul (float a, float b, float *result) {
  *result = a * b;
}

void calc (float a, float b, float *result){
  float aa, bb;
  mul(a, a, &aa);
  mul(b, b, &bb);
  add(aa, bb , result);
}

main()
{
  float i=2;
  float j=3;
  float result;
  printf("i %f ...", i);
  printf(" j %f ...", j);
  calc(i, j, &result);
  printf("%f \n",result);
}

