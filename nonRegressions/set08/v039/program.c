typedef struct _IO_FILE FILE;


typedef __builtin_va_list __gnuc_va_list;

struct _IO_FILE {
  int _flags;
  char* _IO_buf_end;
  int _mode;
};

typedef struct _IO_FILE _IO_FILE;

extern int _IO_vfscanf (_IO_FILE * __restrict, const char * __restrict,
   __gnuc_va_list, int *__restrict);
