/* Bug found by Michael Vossbeck:
 * const modifier must go away for the adjoint param */

static void foo(const double in, double *out) {
    *out = in*in;
}
