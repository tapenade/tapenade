int strcmp(const char *, const char *);
int mf1();
int mf2(void);
void mf3(void);

int (*pointer_to_int);
int (*ptr_to_fct)(char *, float);

int * fct(char, unsigned int, ...);
int (* apfi[3])(int, double);

int (* fpfi(
            float (*)(long int),
            _Bool )
    )(char, double);

/* */
float f(long int);
int fpfi_use_example(void) {
    int (*p)(char, double);
    p = fpfi(&f, 1);
    int i = (*p)('c', 0.3);
    return i;
}


char f1(int *,
        unsigned int [],
        float *[],
        char * volatile [3],
        float (*[4])(int),
        char (* const [5])(float) );

/*
 * f2 is a function that takes an unspecified number of arguments and
 * returns a pointer to a function that takes no argument and returns an int.
 */
int (*f2())(void);

int f2_example(void) {
    int (*ptr)(void);
    ptr = f2();
    int i = (*ptr)();
    return i;
}

/*
 * f3 is a function returning an int that takes as argument a ptr to a
 * function as in the previous f2 example.
 */
int f3( int ( * (*)())(void) );
/*
 * C accepts the following declarations for f3 : it confuse the function
 * and a pointer to the function. If bad_f3 is used in the f3_example() code
 * the compiler is still happy.
 */
int bad_f3( int (* ())(void));

int f3_example(void) {
    int j = f3(&f2);
    return j;
}
/*
 * declarations equivalentes alternatives avec typedef intermediaires
 */
typedef int (*return_type_of_f2)(void);
typedef return_type_of_f2 (*ptr_on_f2)();

int f3_alternative1(ptr_on_f2);

int f3_alternative1_example(void) {
    int k = f3_alternative1(&f2);
    return k;
}

typedef int ( * (*f2_ptr)())(void);
int f3_alternative2(f2_ptr);

int f3_alternative2_example(void) {
    int k = f3_alternative2(&f2);
    return k;
}
