/** Exemple a faire tourner en mode -p uniquement, simplement pour verifier
 * dans le dump que les types sont bien analyses. En version r6776, le bug
 * se manifeste par des erreur injustifiees emises par typeCheck sur le
 * type de v4, qui doit etre du type a 4 champs et non celui a 2 champs!
 * Note: ce code est bizarre, mais correctement accepte par gcc. */

/* #include <stdio.h> */

struct ptype1 var1 ;

typedef struct ptype1 {
  float aa ;
  float bb ;
} myptype1 ;

struct myptype1 {int ii0; int ii1; double uu ; long int i2;} var2 ;

myptype1 var3 ;

struct myptype1 var4 ;

int main() {
  var1.aa = 1.1 ;
  var1.bb = 2.2 ;

  var2.ii0 = 0 ;
  var2.ii1 = 1 ;
  var2.uu = 3.3 ;
  var2.i2 = 444444444 ;

  var3.aa = 5.5 ;
  var3.bb = 6.6 ;

  var4.ii0 = 0 ;
  var4.ii1 = 1 ;
  var4.uu = 7.7 ;
  var4.i2 = 888888888 ;

  var1.aa = var2.uu * var3.bb * var4.ii1;
/*   printf("var1:%f %f ; var2:%i %i %f %li ; var3:%f %f ; var4:%i %i %f %li\n", */
/*       var1.aa, var1.bb, var2.ii0, var2.ii1, var2.uu, var2.i2, var3.aa, var3.bb, var4.ii0, var4.ii1, var4.uu, var4.i2) ; */
}
