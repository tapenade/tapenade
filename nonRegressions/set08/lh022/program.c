/* Un exemple complet de ce qu'on voudrait faire en tgt et
 * adj dans le cas de malloc/free. Le fichier main.c contient
 * de quoi lancer la validation (via le script "mytest")
 * Difference p/r lh20: ajoute instr finale pour forcer
 * adj-live du call qui fait "free". */

#include <stdio.h>
#include <stdlib.h>

void suba(float x[4], float y[5]) {
  float *lpp = malloc(4*sizeof(float)) ;
  int i ;
  for (i=0 ; i<4 ; ++i) {
    lpp[i] = x[i]*i*y[i+1] ;
  }
  for (i=0 ; i<4 ; ++i) {
    y[i] = y[i+1]-lpp[i] ;
  }
  free(lpp) ;
}

/* $AD NOCHECKPOINT */
float* subb1(float y[5], float *z) {
  float *gpp = malloc(10*sizeof(float)) ;
  int i ;
  for (i=0 ; i<10 ; ++i) {
    gpp[i] = i*y[i/2] ;
  }
  gpp[6] = gpp[6]*(*z) ;
  return gpp ;
}

/* $AD NOCHECKPOINT */
float subb2(float *z, float *gpp) {
  float res = 1.0 ;
  int j ;
  for (j=0 ; j<4 ; ++j) {
    res *= gpp[j]*z[5-j] ;
  }
  for (j=0 ; j<7 ; ++j) {
    z[j] = 0.0 ;
  }
  free(gpp) ;
  return res ;
}

/** Un essai pour la diff adjointe de codes
 * faisant des allocations/deallocations */
void testallocs(float x[4], float y[5]) {
  float *z, *pp ;
  x[2] = x[3]* *(y+1) ;
  z = malloc(7*sizeof(float)) ;
  int i ;
  for (i=0 ; i<7 ; ++i) {
    z[i] = x[2]*i ;
  }

  /* 1er appel: code qui alloue/desalloue, reentrant: */
  suba(x,y) ;

  int j ;
  for (j=1 ; j<3 ; ++j) {
    x[j] = x[j-1]*z[j] ;
  }

  /* 2eme appel: code qui alloue, non reentrant. */
  pp = subb1(y,z) ;

  x[3] = pp[3]*pp[2] ;
  x[0] = x[0]*x[3] ;

  /* 3eme appel: code qui desalloue, non reentrant. */
  x[2] = subb2(z,pp) ;

  /* Encore du code, pour forcer subb2 a adj-live. */
  x[1] = y[1]*x[2] ;
}

int main () {
  float x[4], y[5], sum ;
  for (int i=0 ; i<4 ; ++i) x[i]=1.0/(i+1.0) ;
  for (int i=0 ; i<5 ; ++i) y[i]=2.0/(i+2.0) ;
  testallocs(x,y) ;
  sum = 0.0 ;
  for (int i=0 ; i<4 ; ++i) sum += x[i] ;
  for (int i=0 ; i<5 ; ++i) sum += y[i] ;
  printf("Sum of result: %f16\n",sum) ;
}
