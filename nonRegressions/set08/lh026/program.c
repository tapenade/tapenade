void foo(float *a, float *b, float *c) {
  *b = *b+*b ;
  *a = *a**b ;
  *c = 3**c ;
}

void bar(float t1, float t2, float t3, float *res) {
  *res += t1*t2 - t3 ;
}

void root(float x, float *y) {
  float z,t ;
  t = 2.2 ;
  *y += 2*x ;
  z = x+*y ;
  /* $AD DO-NOT-DIFF part1 */
  z = sin(cos(z)) ;
  foo(&x,&z,&t) ;
  t = 2*t ;
  /* $AD END-DO-NOT-DIFF part1 */
  bar(x,z,t,y) ;
}
