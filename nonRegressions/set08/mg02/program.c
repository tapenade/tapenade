
double portfolio(int N, int Nmat, int Nopt, double L[],
                 double delta, int maturities[], double swaprates[])
{
  int    i,j,k,n,t,m;
  double B[120], S[120], swapval, v;

  for (i=0; i<N; i+=1) B[i] = 0;
  B[Nmat-1] = 1./(1.+delta*L[Nmat-1]);
  for (i=Nmat; i<N; i+=1) B[i] = B[i-1]/(1.+delta*L[i]);

  S[0] = 0;
  for (i=1; i<N; i+=1) S[i] = S[i-1] + delta*B[i];

  v = 0;

  for (i=0; i<Nopt; i+=1){
    m = maturities[i] + Nmat-2;
    swapval = B[m] + swaprates[i]*S[m] - 1.0;
    v += 100*max(-swapval,0.0);
  }

  /* apply discount */

  B[0] = 1/(1.+delta*L[0]);
  for (i=1; i<Nmat; i+=1) B[i] = B[i-1]/(1.+delta*L[i]);

  v = v*B[Nmat-2];

  return v;
}
