/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) - 14 Feb 2022 12:36
*/
#include <adStack.h>

/*
  Differentiation of portfolio in reverse (adjoint) mode:
   gradient     of useful results: swaprates[_:_] portfolio delta
                L[_:_]
   with respect to varying inputs: swaprates[_:_] delta L[_:_]
   RW status of diff variables: swaprates:(loc) swaprates[_:_]:incr
                portfolio:in-killed delta:incr L:(loc) L[_:_]:incr
   Plus diff mem management of: swaprates:in L:in
*/
void portfolio_b(int N, int Nmat, int Nopt, double L[], double Lb[], double 
        delta, double *deltab, int maturities[], double swaprates[], double 
        swapratesb[], double portfoliob) {
    int i, j, k, n, t, m;
    double B[120], S[120], swapval, v;
    double Bb[120], Sb[120], swapvalb, vb;
    double max0;
    double max0b;
    double temp;
    double tempb;
    int ii1;
    int branch;
    for (i = 0; i < N; ++i)
        B[i] = 0;
    B[Nmat - 1] = 1./(1.+delta*L[Nmat-1]);
    for (i = Nmat; i < N; ++i) {
        pushReal8(B[i]);
        B[i] = B[i-1]/(1.+delta*L[i]);
    }
    S[0] = 0;
    for (i = 1; i < N; ++i)
        S[i] = S[i - 1] + delta*B[i];
    v = 0;
    for (i = 0; i < Nopt; ++i) {
        pushInteger4(m);
        m = maturities[i] + Nmat - 2;
        swapval = B[m] + swaprates[i]*S[m] - 1.0;
        if (-swapval < 0.0) {
            max0 = 0.0;
            pushControl1b(0);
        } else {
            max0 = -swapval;
            pushControl1b(1);
        }
        v = v + 100*max0;
    }
    /* apply discount */
    pushReal8(B[0]);
    B[0] = 1/(1.+delta*L[0]);
    for (i = 1; i < Nmat; ++i) {
        pushReal8(B[i]);
        B[i] = B[i-1]/(1.+delta*L[i]);
    }
    vb = portfoliob;
    for (ii1 = 0; ii1 < 120; ++ii1)
        Bb[ii1] = 0.0;
    Bb[Nmat - 2] = Bb[Nmat - 2] + v*vb;
    vb = B[Nmat-2]*vb;
    for (i = Nmat-1; i > 0; --i) {
        popReal8(&(B[i]));
        temp = delta*L[i] + 1.;
        Bb[i - 1] = Bb[i - 1] + Bb[i]/temp;
        tempb = -(B[i-1]*Bb[i]/(temp*temp));
        Bb[i] = 0.0;
        *deltab = *deltab + L[i]*tempb;
        Lb[i] = Lb[i] + delta*tempb;
    }
    popReal8(&(B[0]));
    tempb = -(Bb[0]/((delta*L[0]+1.)*(delta*L[0]+1.)));
    Bb[0] = 0.0;
    *deltab = *deltab + L[0]*tempb;
    Lb[0] = Lb[0] + delta*tempb;
    for (ii1 = 0; ii1 < 120; ++ii1)
        Sb[ii1] = 0.0;
    for (i = Nopt-1; i > -1; --i) {
        max0b = 100*vb;
        popControl1b(&branch);
        if (branch == 0)
            swapvalb = 0.0;
        else
            swapvalb = -max0b;
        Bb[m] = Bb[m] + swapvalb;
        swapratesb[i] = swapratesb[i] + S[m]*swapvalb;
        Sb[m] = Sb[m] + swaprates[i]*swapvalb;
        popInteger4(&m);
    }
    for (i = N-1; i > 0; --i) {
        Sb[i - 1] = Sb[i - 1] + Sb[i];
        *deltab = *deltab + B[i]*Sb[i];
        Bb[i] = Bb[i] + delta*Sb[i];
        Sb[i] = 0.0;
    }
    for (i = N-1; i > Nmat-1; --i) {
        popReal8(&(B[i]));
        temp = delta*L[i] + 1.;
        Bb[i - 1] = Bb[i - 1] + Bb[i]/temp;
        tempb = -(B[i-1]*Bb[i]/(temp*temp));
        Bb[i] = 0.0;
        *deltab = *deltab + L[i]*tempb;
        Lb[i] = Lb[i] + delta*tempb;
    }
    temp = delta*L[Nmat-1] + 1.;
    tempb = -(Bb[Nmat-1]/(temp*temp));
    *deltab = *deltab + L[Nmat-1]*tempb;
    Lb[Nmat - 1] = Lb[Nmat - 1] + delta*tempb;
}
