/* Pour tester les if-expressions */

double toto(double inv[5], double outv[5]) {
  double x,y,z ;
  x = inv[0]*inv[1] ;
  y = inv[2]+inv[3] ;
  z = inv[4] ;
  y = y*z ;
  z = x *(z>0.0?x+y:2*x-y) ;
  x = y *(x>0.0?x:y*z) ;
  outv[0] = y ;
  outv[1] = x*x ;
  outv[2] = 2.0*(z+y) ;
  outv[3] = 0.0 ;
  outv[4] = 0.0 ;
  return (outv[1]<outv[2]?2*x:y*z) + (x>0.0?x:-x) ;
}
