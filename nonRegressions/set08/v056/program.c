#include <math.h>

void test (float a1, float a2, float *b1, float b2) {
  float x = 2.0;
  float y = 1.0;
  float *p;
  if (a1 < *b1) {
    p = &y;
  } else {
    p = &x;
    *p = sin(a1);
  }
  *b1 = (*p) * y;
}
