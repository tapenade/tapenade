/* Pour montrer qu'il est dangereux de supprimer des niveaux de pointeurs
 * dans les variables differentiees en mode vectoriel. Ca peut marcher
 * dans des cas tres simples, mais pas sur des pointeurs-tableaux */

#include <stdio.h>

void top(float in, float *out) ;
void foo(const float *x, float *y, int len) ;

int main() {
  float ii = 1.5 ;
  float oo = 0.0 ;
  top(ii, &oo) ;
  printf("%f -> %f\n",ii,oo) ;
}

void top(const float in, float *out) {
  float xx[3] ;
  float yy[3] ;
  int i ;
  for (i=0 ; i<3 ; ++i) {
    xx[i] = in ;
    yy[i] = in * 2.0;
  }
  foo(xx, yy, 3) ;
  *out = 0.0 ;
  for (i=0 ; i<3 ; ++i) {
    *out += xx[i]+yy[i] ;
  }
}

void foo(const float *x, float *y, int len) {
  int i ;
  *y = *x**y*2.0 ;
  for (i=len ; i>=0 ; --i) {
    y[i] = x[i]*y[i] ;
  }
}
