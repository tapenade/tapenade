#include <stdio.h>

void test (float a[3], float b[5][3])
{
  a = b[4];
  printf("a[0] %f \n" , a[0]);
  printf("a[1] %f \n" , a[1]);
  printf("a[2] %f \n" , a[2]);
}

main()
{
  float ta[3] = { 10.0, 11.0, 12.0};
  float tb[5][3] = {{1.0,2.0,3.0},{4.0,5.0,6.0},{7.0,8.0,.09},
                    {10.0,11.0,12.0},{13.0,14.0,15.0}};
  printf("tb[0 0] %f \n" , tb[0][0]);
  printf("tb[0 1] %f \n" , tb[0][1]);
  printf("tb[0 2] %f \n" , tb[0][2]);
  test(ta,tb);
  printf("ta[0] %f \n" , ta[0]);
  printf("ta[1] %f \n" , ta[1]);
  printf("ta[2] %f \n" , ta[2]);
  
}
