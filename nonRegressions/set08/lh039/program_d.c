/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_functionPointers) -  6 Dec 2024 11:13
*/
#include <adContext.h>
/* This is example SendIrecvWaitTwoWay of the C test suite for AMPI 
 Simple Irecv+Send+Wait || Irecv+Wait+Send */
#include <stdio.h>
#include <math.h>
#include "ampi/ampi.h"

/*
  Differentiation of head in forward (tangent) mode (with options context fixinterface):
   variations   of useful results: *y
   with respect to varying inputs: *x
   RW status of diff variables: x:(loc) *x:in-killed y:(loc) *y:out
   Plus diff mem management of: x:in y:in
*/
void head_d(double *x, double *xd, double *y, double *yd) {
    AMPI_Request r;
    int world_rank;
    AMPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    if (world_rank == 0) {
        TLS_AMPI_Irecv(y, yd, 1, MPI_DOUBLE, MPI_DOUBLE, 1, 0, AMPI_FROM_SEND,
                       MPI_COMM_WORLD, &r);
        *xd = 2*(*xd);
        *x = (*x)*2;
        TLS_AMPI_Send(x, xd, 1, MPI_DOUBLE, MPI_DOUBLE, 1, 0, 
                      AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD);
        TLS_AMPI_Wait(&r, MPI_STATUS_IGNORE);
        *yd = 3*(*yd);
        *y = (*y)*3;
    } else if (world_rank == 1) {
        double local;
        double locald;
        TLS_AMPI_Irecv(&local, &locald, 1, MPI_DOUBLE, MPI_DOUBLE, 0, 0, 
                       AMPI_FROM_SEND, MPI_COMM_WORLD, &r);
        TLS_AMPI_Wait(&r, MPI_STATUS_IGNORE);
        locald = cos(local)*locald;
        local = sin(local);
        TLS_AMPI_Send(&local, &locald, 1, MPI_DOUBLE, MPI_DOUBLE, 0, 0, 
                      AMPI_TO_IRECV_WAIT, MPI_COMM_WORLD);
        *yd = 0.0;
    } else
        *yd = 0.0;
}

/*
  Differentiation of main as a context to call tangent code (with options context fixinterface):
*/
int main(int argc, char **argv) {
    AMPI_Init_NT(0, 0);
    int world_rank;
    AMPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    double x, y;
    double xd, yd;
    if (world_rank == 0) {
        xd = 0.0;
        x = 2.5;
        printf("set08/lh039/program.c: process %i sends val [ 2.500000==]%f \n"
               , world_rank, x);
        adContextTgt_init(1.e-8, 0.87);
        adContextTgt_initReal8("x", &x, &xd);
        head_d(&x, &xd, &y, &yd);
        adContextTgt_startConclude();
        adContextTgt_concludeReal8("y", y, yd);
        adContextTgt_conclude();
        printf("set08/lh039/program.c: process %i recvs val [-2.876773==]%f \n"
               , world_rank, y);
        y = y + x;
    } else if (world_rank == 1) {
        adContextTgt_init(1.e-8, 0.87);
        adContextTgt_initReal8("x", &x, &xd);
        head_d(&x, &xd, &y, &yd);
        adContextTgt_startConclude();
        adContextTgt_concludeReal8("y", y, yd);
        adContextTgt_conclude();
    }
    AMPI_Finalize_NT();
    return 0;
}
