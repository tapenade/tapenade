/* Mini example of bugs found in "multibody" application (cf Krishna) */

double ** createMatrix(int m, int n);
double * createVector(int n);
void freeMatrix(double **A);
void freeVector(double *a);

double ** createMatrix(int m, int n){
    int i;
    double **matrix, *linemat;
    matrix = (double**)calloc(m, sizeof(double *));
    matrix[0] = linemat = (double*)calloc(m*n, sizeof(double));
    for (i=1; i<m; i++){
        matrix[i] = linemat+n*i;
    }
    return matrix;
}

double* createVector(int n){
    double* x = (double*)calloc(n, sizeof(double));
    return x;
}

void freeMatrix(double **A){
    free(A[0]);
    free(A);
    return;
}

void freeVector(double *a){
    free(a);
    return;
}

void smallcomp(double *vecoffset) {
  vecoffset[0] = vecoffset[0] * vecoffset[2] ;
}

void compute(double *vec, double **mat, double **pmat) {
  int i,j,k,i6 ;
  double *bi, *bj, foo;
  smallcomp(vec+3) ;
  for (i=0 ; i<6 ; ++i) {
    i6 = i*6 ;
    bi = vec+i6 ;
    foo = bi[2]*pmat[i][i+1] ;
    for (j=i+1; j<10 ; ++j) {
      bj = vec+j*6;

      for (k=1 ; k<4 ; ++k) {
        mat[i][j] += bj[k]*bi[k] ;
      }
    }
  }
}

int RTDynTRC(int nRods, double *xx, double *yy, double pp) {
  double **matrix, **passivematrix;
  double *vector ;
  int i,j ;

  matrix = createMatrix(6*nRods,6);
  passivematrix = createMatrix(11,22);
  vector = createVector(nRods) ;
  for (i=0 ; i<6*nRods ; ++i) {
    for (j=0 ; j<6 ; ++j) {
      passivematrix[i][j] = pp ;
      matrix[i][j] = *xx ;
    }
  }
  for (i=0 ; i<nRods ; ++i)
    vector[i] = *xx ;
  compute(vector, matrix, passivematrix) ;
  *yy = matrix[2][3] * matrix[4][5] ;
  freeMatrix(matrix);
  freeMatrix(passivematrix);
  freeVector(vector);
  return 0 ;
}
