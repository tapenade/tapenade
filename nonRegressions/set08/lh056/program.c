/* From Morlighem: request-help messages from debug calls */
typedef struct {
  double *contents[100] ;
  int length ;
} Inputs ;

double testget(Inputs *inputs, int i, int j) {
  int index = i*j*(i+1)/(i+j) ;
  double *T2 = inputs->contents[index] ;
  return T2[i] ;
}
