/* Inspired from Seism/Boreas/Alif */
typedef struct{
        int M;
        double* values;
} p_Vector;
typedef p_Vector* Vector;

/* $AD NOCHECKPOINT */
Vector VecNew(int size){
  Vector vector = (Vector)malloc(sizeof(p_Vector));
  vector->M = size;
  vector->values = (double*)calloc(size,sizeof(double));
  return vector;
}

/* $AD NOCHECKPOINT */
void VecFree(Vector* pvec){
  if(*pvec){
    free((*pvec)->values);
    free(*pvec);
  }
  *pvec=(void*)0;
}

float test(float x) {
  Vector vvv ;
  int ii ;
  float y=0.5 ;
  vvv = VecNew(10) ;
  for (ii=0 ; ii<vvv->M ; ++ii)
    vvv->values[ii] = ii/x ;
  y = y*x ;
  for (ii=0 ; ii<vvv->M ; ++ii)
    y += x*vvv->values[ii] ;
  VecFree(&vvv) ;
  y = y*x ;
  return y ;
}
  
  
