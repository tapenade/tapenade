// from set08/lh011 
void qr(double *R) {
  double V = 3.5*(*R) ;
  for (float t = 0.0; t < 1.0; t += 0.1) {
    *R = (*R)*V ;
  }
}

void inv(double *R2) {
  qr(R2);
}
