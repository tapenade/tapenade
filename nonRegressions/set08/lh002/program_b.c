/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_llhTests) - 14 Jun 2021 15:32
*/
#include <adStack.h>
#include <adContext.h>

void sub1_nodiff(float x, float y, float z) {
    x = 3.7*y;
}

void top_b() {}

void top_nodiff(float x, float y, float z, float a, float b, float c) {
    if (x > 0.0) {
        y = 1.7;
        sub1_nodiff(x, y, z);
        z = 5.1*z;
        x = y + z;
    } else
        //    y = 3.3*x**2.0 ;
        y = 3.3*x*3.3*x;
    a = -2.9;
    sub1_nodiff(a, b, c);
}

/*
  Differentiation of main as a context to call adjoint code (with options context):
*/
void main() {
    float x = 2.1;
    float y = 3.1;
    float z = 4.1;
    float a = 1.2;
    float b = 1.3;
    float c = 1.4;
    top_b(x, y, z, a, b, c);
}
