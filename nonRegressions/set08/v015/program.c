typedef long mylong;
typedef unsigned char mybyte;

typedef int (*fct_t)(void *);
typedef void (*thd_t)(int);

typedef void fv(int), (*pfv)(int);
//void (signal(int, void (*)(int)))(int);
fv *signal1(int, fv *);
pfv signal2(int, pfv);
