#include <stdio.h>

float test(void) {
  float i = 1 + 2, j = i + 4;
  i = i + i;
  float k = j + j;
  j = k + i;
  printf("result %7.1f%12.1f\n", j);
  return j;
}

int main(void) {
  printf("result %7.1f%12.1f\n", test());
}
