#include <math.h>

void test(float x, float y, float *z) {
  /* first declarations */
  float u = x * 2, v = y * u;
  /* first statement */
  u = u * v;
  /* second declaration */
  float w = *z * exp(sin(u)), t = w * y;
  /* second statement */
  *z = t * (*z);
}
