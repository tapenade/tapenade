/* Bug trouve dans Cazals-ABS: mauvaise propagation DiffLiveness+TBR
 * qui ne tient pas compte des pointeurs derives calcules dans la passe FWD
 * Ce test tourne sans l'optimisation diffLiveness nulle part */

#include<stdio.h>

typedef struct {
  int index ;
  double value ;
} Box ;

typedef struct {
  Box boxlist[100] ;
} Stock ;

/* $AD NOCHECKPOINT */
Box *getBox(Stock *C, int index) {
  return &C->boxlist[index] ;
}

double getBoxVal(Box *bb) {
  return bb->value ;
}

double test(Stock *C, int index1, int index2) {
  double v1 = getBoxVal(getBox(C, index1)) ;
  double v2 = getBoxVal(getBox(C, index2)) ;
  return v1*v2 ;
}

int main(int argc, char *argv[]) {
  Stock C ;
  Box *bb ;
  int i ;
  for (i=0 ; i<100 ; ++i) {
    bb = getBox(&C, i) ;
    bb->index = i ;
    bb->value = i+0.5 ;
  }
  printf("test(C,3,6)=%f\n", test(&C,3,6)) ;
}
