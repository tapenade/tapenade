/* Bug2 Morlighem 01/10/2015
 * Pbs about C structs, initialization-in-declarations */

#include<stdio.h>
#include <stdlib.h>

void CostFunction(double*,double);

main(){
	double a = 2.;
	double J;
	
	CostFunction(&J,a);
	printf("Cost function = %g\n",J);
}

/*Define Tria*/
typedef struct{
	double temperature;
} Tria;
Tria* TriaNew(double a){

        Tria* tria ;
        tria = (Tria*)malloc(sizeof(Tria));
	tria->temperature = a;
	return tria;
}

/*Define Elements structure*/
typedef struct{
	int     numberofelements;
	Tria** elements;
} Elements;
Elements* ElementsNew(double a){
	int i;
	Elements* elements;
        elements = (Elements*)malloc(sizeof(Elements));
	elements->numberofelements = 3;
	elements->elements = (Tria**)malloc(elements->numberofelements*sizeof(Tria*));
	for(i=0;i<elements->numberofelements;i++) elements->elements[i] = TriaNew(a);

	return elements;
}
Tria* ElementsGetByOffset(Elements* elements,int index){
	return elements->elements[index];
}

void CostFunction(double* pJ,double a){
	int    i;
	double J = 0.;

	Elements* elements;
        elements = ElementsNew(a);

	for(i=0;i<elements->numberofelements;i++){
		Tria* element;
                element = ElementsGetByOffset(elements,i);
		J+=element->temperature;
	}

	/*Assign output pointer*/
	*pJ = J;
}
