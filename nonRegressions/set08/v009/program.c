enum myenum { one, two, three };
enum myenum x, *y, z[4];

enum {e0, e1};

typedef enum {
    F_FE,
    F_U,
    F_E,
} yy;

struct abc_ {
        struct abc_ *next;
        enum { AAA, DDD = -7, EEE } tp;
};
