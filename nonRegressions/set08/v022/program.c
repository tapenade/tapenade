#include <stdio.h>
#include <stdarg.h>

/* minprintf: minimal printf with variable argument list */
void minprintf(const char *fmt, ...)
{
   va_list ap;   /* points to each unnamed arg in turn */
   char *p;

   va_start(ap, fmt);   /* make ap point to first unnamed arg */
   for (p = fmt; *p; ++p)
      if (*p != '%')
         putchar(*p);
      else
         switch (*++p) {
         case 'd':
            {
               int ival = va_arg(ap, int);

               printf("%d", ival);
            }
            break;
         case 'f':
            {
               double dval = va_arg(ap, double);

               printf("%f", dval);
            }
            break;
         case 's':
            {
               char *sval = va_arg(ap, char *);

               printf("%s", sval);
            }
            break;
         default:
            putchar(*p);
         }
   va_end(ap);   /* clean up when done */
}


int main(void) {
  minprintf("%d %f %s", '1', 2.0, "fin\n");
}
