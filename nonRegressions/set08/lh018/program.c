/* A bug about propagation of info across arguments passed by value:
 * sometimes (e.g. for unitW) the info must not pass,
 * sometimes (e.g. for unitR) the info must pass */

#include <math.h>

double SquareIt(double x)
{
    return x*x;
}

double ASubFunction(double x)
{
    return SquareIt(x);
}

double ADMax(double x, double y)
{
    if(x>y)
        return x;
    else
        return y;
}

double RootFunction(double x[], double y, int n)
{
    int i;
    double rv;
    double myTemp1;
    double myTemp2;
    double myTemp3;
    double acc;

    rv = 1.0;
    acc = 0.0;
    doit = 0;
    for(i=0;i<n;i++) {

            myTemp1 = ASubFunction(x[i]*rv);
            myTemp2 = ADMax(rv , myTemp1);
            rv = myTemp2;
            acc = ADMax(acc, rv);

    }
    return rv+ acc;
}
