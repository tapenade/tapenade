/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
#include "DIFFSIZES.inc"
/*  Hint: NBDirsMax should be the maximum number of differentiation directions
*/

/*
  Differentiation of mul in forward (tangent) mode (with options multiDirectional):
   variations   of useful results: *result
   with respect to varying inputs: *result a[_:_] b[_:_]
   RW status of diff variables: result:(loc) *result:in-out a:(loc)
                a[_:_]:in b:(loc) b[_:_]:in
   Plus diff mem management of: result:in a:in b:in
*/
void mul_dv(float a[], float ad[][NBDirsMax], float b[], float bd[][NBDirsMax]
        , int size, float *result, float (*resultd)[NBDirsMax], int nbdirs) {
    int nd;
    for (nd = 0; nd < nbdirs; ++nd)
        (*resultd)[nd] = 0.0;
    *result = 1;
    {
      float temp;
      for (int i = 0; i < size+1; ++i) {
          temp = (*result)*a[i];
          for (nd = 0; nd < nbdirs; ++nd)
              (*resultd)[nd] = b[i]*(a[i]*((*resultd)[nd])+(*result)*ad[i][nd]
                  ) + temp*bd[i][nd];
          *result = temp*b[i];
      }
    }
}
