#include <stdlib.h>
#include <stdio.h>


void ComputeZAll(double* U, double *V, double *H, double *Q,   
                   double* ZU, double *ZV, double *ZH , double *ZQ, 
                   int Nx, int Ny, double dx, double dy, 
                   double nu_t, double nu, double r,
                   double f0, double beta, double g, double Long);

void IntegreAll(double* U, double *V, double *H, double *Q,
                  double* Uint, double *Vint, double *Hint , double *Qint, 
                  int Nx, int Ny, double dx, double dy, double dt,
                  double nu_t, double nu, double r,
                  double f0, double beta, double g, double Long);
  
void IntegreRKAll(double* U, double *V, double *H, double *Q,
                    double* Uint, double *Vint, double *Hint , double *Qint,
                    double* Aux1U, double *Aux1V, double *Aux1H, double *Aux1Q,
                    double* Aux2U, double *Aux2V, double *Aux2H, double *Aux2Q,
                    double* Aux3U, double *Aux3V, double *Aux3H, double *Aux3Q,
                    int Nx, int Ny, double dx, double dy, double dt,
                    double nu_t, double nu, double r,
                    double f0, double beta, double g, double Long);
  
void ComputeF(double *U, double *V, double *F,
                int Nx, int Ny, double dx, double dy,
                double alpha_regul, double beta_regul);
  


void ComputeZAll(double* U, double *V, double *H, double *Q,   
                 double* ZU, double *ZV, double *ZH , double *ZQ, 
                 int Nx, int Ny, double dx, double dy, 
                 double nu_t, double nu, double r,
                 double f0, double beta, double g, double Long)
{
  int i, j;
  
  /*Auxiliaire*/
  double aux1, aux2, aux3;
  
  /*Coriolis*/
  double f;
  
  /*Bernouilli*/
  double B_i_j, B_im1_j, B_i_jm1; 
  /* B(i, j), B(i - 1, j), B(i, j- 1)*/
  
  /*Vorticité relative*/
  double Vort_i_j, Vort_ip1_j, Vort_i_jp1;
  /*Vort(i, j), Vort(i + 1, j), Vort(i, j + 1)*/
  
  /*Laplacien de U et V*/
  double Ulap, Vlap;
 
  /* RAZ */
  for(i = 0; i < Nx * Ny; i++ )
    {
      ZU[i] = 0.;
      ZV[i] = 0.;
      ZH[i] = 0.;
      ZQ[i] = 0.;
    }
  
  /*Boucle spatiale*/
  for(j = 1; j < Ny - 1; j++)
    for(i = 1; i < Nx - 1; i++) 
      {      
        /*Calcul du parametre de coriolis*/
        f = f0 + beta * (j * dy - Long / 2);
        /*Calcul du potentiel de bernouilli*/
        /* en i et j*/
        aux1 = 0.5 * ( U[i + j * Nx] + U[(i + 1) + j * Nx] );
        aux2 = 0.5 * ( V[i + j * Nx ] + V[i + (j + 1) * Nx ] );
        B_i_j = 0.5 * (aux1 * aux1 + aux2 * aux2) + g * H[i + j * Nx ];
	  
        /* en i - 1 et j*/
        aux1 = 0.5 * ( U[(i - 1) + j * Nx] + U[i + j * Nx] );
        aux2 = 0.5 * ( V[(i - 1) + j * Nx ] + V[(i - 1) + (j + 1) * Nx ] );
        B_im1_j = 0.5 * (aux1 * aux1 + aux2 * aux2) + g * H[(i - 1) + j * Nx ];
	  
        /* en i et j - 1*/
        aux1 = 0.5 * ( U[i + (j - 1) * Nx] + U[(i + 1) + (j - 1) * Nx] );
        aux2 = 0.5 * ( V[i + (j - 1) * Nx ] + V[i + j * Nx ] );
        B_i_jm1 = 0.5 * (aux1 * aux1 + aux2 * aux2) + g * H[i + (j - 1) * Nx ];
	  
        /*Calcul de la vorticité relative*/
        /* en i et j */    
        Vort_i_j = 
          (V[i + j * Nx ] - V[(i - 1) + j * Nx ] ) / dx -
          (U[i + j * Nx] - U[i + (j - 1) * Nx]) / dy;
	  
        /* en i + 1 et j*/
        Vort_ip1_j = 
          (V[(i + 1) + j * Nx ] - V[i + j * Nx ] ) / dx -
          (U[(i + 1) + j * Nx] - U[(i + 1) + (j - 1) * Nx]) / dy;
	  
        /* en i et j + 1*/
        Vort_i_jp1 = 
          (V[i + (j + 1) * Nx ] - V[(i - 1) + (j + 1) * Nx ] ) / dx -
          (U[i + (j + 1) * Nx] - U[i + j * Nx]) / dy;
	  
        /*Calcul du laplacien de U et de V*/
        Ulap = 
          ( U[(i - 1) + j * Nx ] + U[i + 1 + j * Nx ] - 2 * U[i + j * Nx ] ) / (dx * dx) +
          ( U[i +  (j - 1) * Nx] + U[i + (j + 1) * Nx] - 2 * U[i + j * Nx] ) / (dy * dy);
        Vlap = 
          ( V[(i - 1) + j * Nx ] + V[i + 1 + j * Nx ] - 2 * V[i + j * Nx ] ) / (dx * dx) +
          ( V[i +  (j - 1) * Nx] + V[i + (j + 1) * Nx] - 2 * V[i + j * Nx] ) / (dy * dy);
        
        /*Equation d'évolution de U*/
        aux1 = f + 0.5 * (Vort_i_j + Vort_i_jp1);
        aux2 = 0.25 * (V[i - 1 +  j * Nx] + V[i + j * Nx] + V[i - 1 +  (j + 1) * Nx] + V[i +  (j + 1) * Nx]);
        aux3 = (B_i_j - B_im1_j) / dx;
        ZU[i + j * Nx] = aux1 * aux2 - aux3 - r * U[i + j * Nx] + nu * Ulap;
        
        /*Equation d'évolution de V*/
        aux1 = f + 0.5 * (Vort_i_j + Vort_ip1_j);
        aux2 = 0.25 * (U[i + (j - 1) * Nx] + U[i + j * Nx] + U[i + 1 + (j - 1) * Nx] + U[i + 1 + j * Nx]);
        aux3 = (B_i_j - B_i_jm1) / dy;
        ZV[i + j * Nx] = - aux1 * aux2 - aux3 - r * V[i + j * Nx] + nu * Vlap;
        
        /*Equation d'evolution de H*/
        aux1 = ( (H[i + 1 + j * Nx] + H[i + j * Nx]) * U[i + 1 + j * Nx] - 
                 (H[i - 1 + j * Nx] + H[i + j * Nx]) * U[i + j * Nx] ); 
        aux2 = ( (H[i + (j + 1) * Nx] + H[i + j * Nx]) * V[i + (j + 1) * Nx] - 
                 (H[i + (j - 1) * Nx] + H[i + j * Nx]) * V[i + j * Nx] );     
        ZH[i + j * Nx] = - 1 / (2 * dx) * aux1 - 1 / (2 * dy) * aux2;

        /*Equation d'evolution de Q*/
        aux1 = (U[i + j * Nx] + U[i + 1 + j * Nx]) / 2;
        aux2 = (V[i + j * Nx] + V[i + (j + 1) * Nx]) / 2;
        ZQ[i + j * Nx] = 
          - aux1 * (Q[i + 1 + j * Nx] - Q[i - 1 + j * Nx]) / (2 * dx) 
          - aux2 * (Q[i + (j + 1) * Nx] - Q[i + (j - 1) * Nx]) / (2 * dy)
          + nu_t * (Q[i - 1 + j * Nx] - 2 * Q[i + j * Nx] + Q[i + 1 + j * Nx]) / (dx * dx)
          + nu_t * (Q[i + (j - 1) * Nx] - 2 * Q[i + j * Nx] + Q[i + (j + 1) * Nx]) / (dy * dy);
        
      }/*end spatial*/ 
}

void IntegreRKAll(double* U, double *V, double *H, double *Q,
                  double* Uint, double *Vint, double *Hint , double *Qint, 
                  double* Aux1U, double *Aux1V, double *Aux1H, double *Aux1Q,
                  double* Aux2U, double *Aux2V, double *Aux2H, double *Aux2Q,
                  double* Aux3U, double *Aux3V, double *Aux3H, double *Aux3Q,
                  int Nx, int Ny, double dx, double dy, double dt,
                  double nu_t, double nu, double r,
                  double f0, double beta, double g, double Long)
{
  int i;

  double K1U[Nx * Ny];
  double K1V[Nx * Ny];
  double K1H[Nx * Ny];
  double K1Q[Nx * Ny];
  
  double K2U[Nx * Ny];
  double K2V[Nx * Ny];
  double K2H[Nx * Ny];
  double K2Q[Nx * Ny];
  
  double K3U[Nx * Ny];
  double K3V[Nx * Ny];
  double K3H[Nx * Ny];
  double K3Q[Nx * Ny];
  
  double K4U[Nx * Ny];
  double K4V[Nx * Ny];
  double K4H[Nx * Ny];
  double K4Q[Nx * Ny];
  
  for(int i = 0; i < Nx * Ny; i++)
    {
      K1U[i] = 0.;
      K1V[i] = 0.;
      K1H[i] = 0.;
      K1Q[i] = 0.;
      
      K2U[i] = 0.;
      K2V[i] = 0.;
      K2H[i] = 0.;
      K2Q[i] = 0.;
      
      K3U[i] = 0.;
      K3V[i] = 0.;
      K3H[i] = 0.;
      K3Q[i] = 0.;
      
      K4U[i] = 0.;
      K4V[i] = 0.;
      K4H[i] = 0.;
      K4Q[i] = 0.;
      
    }
  
  ComputeZAll(U, V, H, Q,   
              K1U, K1V, K1H, K1Q, 
              Nx, Ny, dx, dy, 
              nu_t, nu, r,
              f0, beta, g, Long);
  
  for(i = 0; i < Nx * Ny; i++)
    {
      Aux1U[i] = U[i] + 0.5 * dt * K1U[i];
      Aux1V[i] = V[i] + 0.5 * dt * K1V[i];
      Aux1H[i] = H[i] + 0.5 * dt * K1H[i];
      Aux1Q[i] = Q[i] + 0.5 * dt * K1Q[i];
    }
  
  ComputeZAll(Aux1U, Aux1V, Aux1H, Aux1Q,   
              K2U, K2V, K2H, K2Q,
              Nx, Ny, dx, dy, 
              nu_t, nu, r,
              f0, beta, g, Long);
  
  for(i = 0; i < Nx * Ny; i++)
    {
      Aux2U[i] = U[i] + 0.5 * dt * K2U[i];
      Aux2V[i] = V[i] + 0.5 * dt * K2V[i];
      Aux2H[i] = H[i] + 0.5 * dt * K2H[i];
      Aux2Q[i] = Q[i] + 0.5 * dt * K2Q[i];
    }

  ComputeZAll(Aux2U, Aux2V, Aux2H, Aux2Q,   
              K3U, K3V, K3H, K3Q,
              Nx, Ny, dx, dy, 
              nu_t, nu, r,
              f0, beta, g, Long);
  
    for(i = 0; i < Nx * Ny; i++)
    {
      Aux3U[i] = U[i] + dt * K3U[i];
      Aux3V[i] = V[i] + dt * K3V[i];
      Aux3H[i] = H[i] + dt * K3H[i];
      Aux3Q[i] = Q[i] + dt * K3Q[i];
    }
  
    ComputeZAll(Aux3U, Aux3V, Aux3H, Aux3Q,
                K4U, K4V, K4H, K4Q, 
                Nx, Ny, dx, dy, 
                nu_t, nu, r,
                f0, beta, g, Long);
    
    for(i = 0; i < Nx * Ny; i++)
      {
        Uint[i] = U[i] + dt * (K1U[i] + 2 * K2U[i] + 2 * K3U[i] + K4U[i]) / 6.;
        Vint[i] = V[i] + dt * (K1V[i] + 2 * K2V[i] + 2 * K3V[i] + K4V[i]) / 6.;
        Hint[i] = H[i] + dt * (K1H[i] + 2 * K2H[i] + 2 * K3H[i] + K4H[i]) / 6.;
        Qint[i] = Q[i] + dt * (K1Q[i] + 2 * K2Q[i] + 2 * K3Q[i] + K4Q[i]) / 6.;
      }
    
    
}
