/* From Morlighem: push/pop of index was missing */
typedef struct {
  double *contents[100] ;
  int length ;
} Inputs ;

void InputsSet(Inputs *inputs, int i, int j, double *values) {
  int index = i*j*(i+1)/(i+j) ;
  if (inputs->contents[index]) {
    free(inputs->contents[index]) ;
  }
  inputs->contents[index] = values ;
}
