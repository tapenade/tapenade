/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
#include <adStack.h>
#include <math.h>

/*
  Differentiation of ncfinv in reverse (adjoint) mode:
   gradient     of useful results: *x *y
   with respect to varying inputs: *x *y
   RW status of diff variables: x:(loc) *x:incr y:(loc) *y:in-out
   Plus diff mem management of: x:in y:in
*/
void ncfinv_b(float *x, float *xb, float *y, float *yb) {
    float POLY[] = {4.560330337e-12f, 1.891879037e-09f, 6.384940120e-11f, 
        2.112199920e-08f, 3.709377697e-10f, -4.363333596e-09f, 
        1.235508984e-09f, -1.340785237e-06f, -2.085483499e-08f, -
        7.816014247e-06f, -8.052312355e-07f, 2.418058136e-05f, -
        1.789424115e-05f, 0.0005984976896f, -0.0003717238455f, 0.003071246478f
        , -0.009026965781f, -0.01373818033f, -0.4222077998f, -0.6926406482f, 
        9.066901046f, 4.381417061f};
    float MEAN[] = {-11.36390346f, -2.693891179f};
    float MID = -6.114811502f;
    float w, p;
    float wb, pb;
    int n;
    float tempb;
    int adCount;
    int i;
    w = log(4.0f*(*x)*(1.0f-*x));
    // n = w>MID;
    // n = 0; if (w>MID) n = 1;
    n = (w > MID ? 1 : 0);
    w = w - MEAN[n];
    p = POLY[n];
    adCount = 1;
    for (; n < 22; n = n + 2) {
        pushReal4(p);
        p = p*w + POLY[n];
        adCount = adCount + 1;
    }
    pushInteger4(adCount);
    *xb = *xb + p*(*yb);
    pb = (*x-0.5f)*(*yb);
    *yb = 0.0;
    popInteger4(&adCount);
    for (i = 1; i < adCount+1; ++i)
        if (i == 1)
            wb = 0.0;
        else {
            popReal4(&p);
            wb = wb + p*pb;
            pb = w*pb;
        }
    tempb = wb/((*x)*(1.0f-*x));
    *xb = *xb + (1.0f-2*(*x))*tempb;
}
