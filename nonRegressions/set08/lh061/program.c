/* Exemple d'interaction difficile entre TBR et recalculs:
 * L'affectation qui tue la variable TBR vv est recalculee,
 * donc elle n'apparait plus dans la passe FWD, donc on pourrait
 * vouloir ne pas faire PUSH, mais la variable vv est
 * non-TBR apres cette affectation et donc on risque d'oublier de
 * faire le PUSH avant la *prochaine* affectation diff-live de vv */

void ComputeZAll(double *x, double *y, int i) {
  double vv ;
  vv = 1.0/(sin(*x) + cos(*x)) ;
  *y += vv*vv*1.2;
  vv = 2*(*x) ;
  *y += vv*vv*2.7;
  vv = 2.0/(sin(*x) + cos(*x)) ;
  *y += vv*vv*3.1;
}
