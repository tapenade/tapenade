module lib
   IMPLICIT NONE
   integer, parameter :: i = 10
   integer, parameter :: j = 20
   integer, parameter :: k = 30
    INTEGER,PUBLIC,PARAMETER :: wp=8
   INTERFACE mpp_sum
      MODULE PROCEDURE mppsum_real2, mppsum_real3
   END INTERFACE

contains
  SUBROUTINE mppsum_real2( ptab, cst, str)
    real(wp) :: cst
    REAL(wp), INTENT(inout), DIMENSION(i,j) ::   ptab
    CHARACTER(len=1), INTENT( in ) ::   str
    integer ji
      DO ji = 1, 10
         ptab(ji,1) = ptab(ji,1) * cst
      END DO
  END SUBROUTINE mppsum_real2

  SUBROUTINE mppsum_real3( ptab, cst, str )
    real(wp) :: cst
    REAL(wp), INTENT(inout), DIMENSION(i,j,k) ::   ptab
    CHARACTER(len=1), INTENT( in ) ::   str
    integer ji
      DO ji = 1, 10
         ptab(ji,1,1) = ptab(ji,1,1) * cst
      END DO
  END SUBROUTINE mppsum_real3

  subroutine test( pta2, pta3 )
    real(wp), dimension(10,20) :: pta2
    real(wp), dimension(10,20,30) :: pta3
    real(wp) :: c
    c = 1.0
    CALL mpp_sum( pta2, c, 'T' )  
    CALL mpp_sum( pta3, c, 'T' )
  end subroutine test

end module lib
