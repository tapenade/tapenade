!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_fortranparser) - 12 Mar 2020 15:05
!
MODULE COIN_DIFF
  IMPLICIT NONE
  PUBLIC :: coine, x, head
  PUBLIC :: coine_fwd, coine_bwd, xb, head_fwd, head_bwd
  REAL :: x
  REAL :: xb

CONTAINS
!  Differentiation of coine in reverse (adjoint) mode, forward sweep:
!   gradient     of useful results: x a b
!   with respect to varying inputs: x a b
  SUBROUTINE COINE_FWD(a, b)
    IMPLICIT NONE
    REAL :: arg1
    REAL :: a
    REAL :: b
    CALL HEAD_FWD(a, arg1)
  END SUBROUTINE COINE_FWD

!  Differentiation of coine in reverse (adjoint) mode, backward sweep:
!   gradient     of useful results: x a b
!   with respect to varying inputs: x a b
!   RW status of diff variables: x:incr a:in-zero b:incr
  SUBROUTINE COINE_BWD(a, ab, b, bb)
    IMPLICIT NONE
    REAL :: arg1
    REAL :: arg1b
    REAL :: a
    REAL :: ab
    REAL :: b
    REAL :: bb
    CALL HEAD_BWD(a, ab, arg1, arg1b)
    bb = bb + x*arg1b
    xb = xb + b*arg1b
    ab = 0.0
  END SUBROUTINE COINE_BWD

  SUBROUTINE COINE(a, b)
    IMPLICIT NONE
    REAL :: arg1
    REAL :: a
    REAL :: b
    arg1 = b*x
    CALL HEAD(a, arg1)
  END SUBROUTINE COINE

!  Differentiation of head in reverse (adjoint) mode, forward sweep:
!   gradient     of useful results: a
!   with respect to varying inputs: b
  SUBROUTINE HEAD_FWD(a, b)
    IMPLICIT NONE
    REAL :: a, b
  END SUBROUTINE HEAD_FWD

!  Differentiation of head in reverse (adjoint) mode, backward sweep:
!   gradient     of useful results: a
!   with respect to varying inputs: b
  SUBROUTINE HEAD_BWD(a, ab, b, bb)
    IMPLICIT NONE
    REAL :: a, b
    REAL :: ab, bb
    bb = ab
  END SUBROUTINE HEAD_BWD

  SUBROUTINE HEAD(a, b)
    IMPLICIT NONE
    REAL :: a, b
    a = b
  END SUBROUTINE HEAD

END MODULE COIN_DIFF

