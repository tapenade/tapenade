MODULE coin

PUBLIC coine, x, head

real x

contains
! $AD NOCHECKPOINT
subroutine coine(a,b)
        call head(a,b*x)
end subroutine

! $AD NOCHECKPOINT
subroutine head(a,b)
        real a, b
        a = b
end subroutine

end module
