! from examplesF90/REFERENCES/vmp11
![llh 9Nov2017] Je ne comprends plus quel etait le but de ce test
! avec des intrinsics MAX et ABS qui prendraient des pointeurs?
! => J'ai change la librairie en ce sens, sans les pointeurs.

subroutine test(comm)
INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(10,50)
  TYPE rk_comm_real_1d
    REAL(kind=wp), DIMENSION(:), POINTER :: thresh
    REAL(kind=wp), DIMENSION(:), POINTER :: weights
    REAL(kind=wp), DIMENSION(:), POINTER :: y
  END TYPE rk_comm_real_1d

   TYPE(rk_comm_real_1d), INTENT(inout) :: comm
   comm%weights = MAX(ABS(comm%y),comm%thresh)
end subroutine 
