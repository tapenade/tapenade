module test
         implicit none
	     integer*4,parameter:: nspchnl= 2001, &
	                           ncomp0 = 10

	     real*8,dimension(2:nspchnl,20:nspchnl):: acoef1

	     real*8,dimension(ncomp0:nspchnl,1:4):: phis

	     real*8,dimension(1:nspchnl):: refr

end module test
