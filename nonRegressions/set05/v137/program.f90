SUBROUTINE p(rho, value, ec, x)
    IMPLICIT NONE
    REAL :: rho, ec, x, value
    optional value
    x = rho + ec * x + value
END SUBROUTINE p

SUBROUTINE test(r,e)
  real :: r, e
  interface 
     subroutine p(rho, value, ec, x)
       real:: rho, ec, x, value
    optional value
     end subroutine p
  end interface
  call p(r, x= r*e, ec= r-e)
END SUBROUTINE test

function s(x,y)
real :: x,y,s
s = x*y + x
return
end function s
