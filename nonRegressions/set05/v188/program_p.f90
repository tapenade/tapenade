!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.11 (r5911) -  4 Jan 2016 16:40
!
SUBROUTINE EOS_INSITU_POT(zws, psal)
  IMPLICIT NONE
  INTEGER :: jpi
  INTEGER :: jpj
  INTEGER :: jpk
  REAL, DIMENSION(jpi, jpj, jpk), INTENT(IN) :: psal
  REAL, DIMENSION(jpi, jpj, jpk), INTENT(OUT) :: zws
  INTEGER :: i
  INTRINSIC ABS
  INTRINSIC SQRT
  REAL, DIMENSION(jpi, jpk) :: abs0
  DO i=1,jpj
    WHERE (psal(:, i, :) .GE. 0.0) 
      abs0 = psal(:, i, :)
    ELSEWHERE
      abs0 = -psal(:, i, :)
    END WHERE
    zws(:, i, :) = SQRT(abs0)
  END DO
END SUBROUTINE EOS_INSITU_POT

