SUBROUTINE eos_insitu_pot (zws, psal)
  REAL, DIMENSION(jpi,jpj,jpk), INTENT( in ) :: psal
  REAL, DIMENSION(jpi,jpj,jpk), INTENT( out ) :: zws
  integer i
  do i = 1,jpj
    zws(:,i,:) = SQRT( ABS( psal(:,i,:) ) )
  end do
END SUBROUTINE eos_insitu_pot
