!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:15
!
SUBROUTINE S279(ntimes, ld, n, ctime, dtime, a, b, c, d, e, aa, bb, cc)
  IMPLICIT NONE
  INTEGER :: ntimes, ld, n, i, nl
  REAL :: a(n), b(n), c(n), d(n), e(n), aa(ld, n), bb(ld, n), cc(ld, n)
  REAL :: t1, t2, chksum, ctime, dtime, CS1D
  EXTERNAL DUMMY
  INTRINSIC FLOAT
  EXTERNAL CS1D
!      call init(ld,n,a,b,c,d,e,aa,bb,cc,'s279 ')
!      call forttime(t1)
  DO nl=1,ntimes
    WHERE (a(:n) .GT. 0.) 
      c(:n) = -c(:n) + e(:n)*e(:n)
    ELSEWHERE
      b(:n) = -b(:n) + d(:n)*d(:n)
      WHERE (b(:n) .GT. a(:n)) c(:n) = c(:n) + d(:n)*e(:n)
    END WHERE
    a(:n) = b(:n) + c(:n)*d(:n)
    CALL DUMMY(ld, n, a, b, c, d, e, aa, bb, cc, 1.)
  END DO
!      call forttime(t2)
  t2 = t2 - t1 - ctime - dtime*FLOAT(ntimes)
  chksum = CS1D(n, a) + CS1D(n, b) + CS1D(n, c)
!      call check(chksum,ntimes*n,n,t2,'s279 ')
  RETURN
END SUBROUTINE S279

