      subroutine s279(ntimes,ld,n,ctime,dtime,a,b,c,d,e,aa,bb,cc)
      integer ntimes,ld,n,i,nl
      real a(n),b(n),c(n),d(n),e(n),aa(ld,n),bb(ld,n),cc(ld,n)
      real t1,t2,chksum,ctime,dtime,cs1d
!      call init(ld,n,a,b,c,d,e,aa,bb,cc,'s279 ')
!      call forttime(t1)
      do nl= 1,ntimes
	      where(a(:n) >  0.)
		c(:n)= -c(:n)+e(:n)*e(:n)
		elsewhere
		    b(:n)= -b(:n)+d(:n)*d(:n)
		    where(b(:n) > a(:n)) c(:n)= c(:n)+d(:n)*e(:n)
		endwhere
	      a(:n)= b(:n)+c(:n)*d(:n)
	  call dummy(ld,n,a,b,c,d,e,aa,bb,cc,1.)
	enddo
!      call forttime(t2)
      t2= t2-t1-ctime-(dtime*float(ntimes))
      chksum= cs1d(n,a)+cs1d(n,b)+cs1d(n,c)
!      call check(chksum,ntimes*n,n,t2,'s279 ')
      return
      end
