subroutine test(x,y)
   implicit none
   real x(4)
   real y(4)

   y = x + 1;
   where (x > 1)
        y = log(x * 2);     
        y = y * 3;
   end where
   y = y * 5;

end subroutine test
