FUNCTION F(t)
  REAL :: f, t
  f = t*t
  RETURN
END FUNCTION F

PROGRAM TEST
  REAL :: x, y
  INTERFACE 
      FUNCTION F(t)
        REAL*8 :: f, t
      END FUNCTION F
  END INTERFACE

  x = 3.4
  y = F(x)
  WRITE(*, *) x, y
END PROGRAM TEST

