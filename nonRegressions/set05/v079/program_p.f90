!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.1 (r2947M) - 05/07/2009 14:43
!  
PROGRAM TEST
  IMPLICIT NONE
  REAL :: x, y
  INTERFACE 
      FUNCTION F(t)
        REAL*8 :: f, t
      END FUNCTION F
  END INTERFACE

  x = 3.4
  y = F(x)
  WRITE(*, *) x, y
END PROGRAM TEST

FUNCTION F(t)
  IMPLICIT NONE
  REAL :: f, t
  f = t*t
  RETURN
END FUNCTION F

