! Extend assignment and + operators, & define two new ops
!
Module Operators
!
! Description:
!
! Module to extend the assignment operation so that it will
! convert characters to an integer, extend the + operator to
! add logical values, and define two new operators.
!
! Current Code Owner: A N Other
!
! History:
! Version   Date   Comment
! -------  ------  -------
!   1.0    5/5/95  Original code. (A N Other)
!
! Code Description:
! Language: Fortran 90.
!
! The procedure named must be a subroutine with one
! intent(OUT) argument and one intent(in) argument.

  Implicit none

! Override operators:
  Interface assignment(=)
    Module procedure Chars_to_Integer

  End interface

! Similarly the next interface will arrange for Add_Logicals
! to be called if two logical values are added together. The
! procedure named must be a function with two intent(in)
! arguments. The result of the addition is the result of the
! function.

  Interface operator(+)
    Module procedure Add_Logicals

  End interface

! Define new operators:

! This creates a new operator .Half. It is a unary operator,
! because Divide_By_Two only takes one argument.

  Interface operator(.Half.)
    Module procedure Divide_By_Two

  End interface



! This creates a new operator .JPlusKSq. It is a binary
! operator, because J_Plus_K_Squared takes two arguments.

  Interface operator(.JPlusKSq.)
    Module procedure J_Plus_K_Squared

  End interface

Contains

! Define procedures contained in this module

  subroutine Chars_to_Integer(int, int_as_chars)
  
  ! Subroutine to convert a character string containing
  ! digits to an integer.
          
    Character(len=5), intent(in)  :: int_as_chars
    Integer, intent(OUT)          :: int

    Read (int_as_chars, FMT = '(I5)') int
    
  End subroutine Chars_to_Integer

  Function Add_Logicals(a, b) result(c)
  
  ! Description:
  ! Function to implement addition of logical values as an
  ! OR operation.
          
    Logical, intent(in)  :: a
    Logical, intent(in)  :: b
    Logical              :: c
            
    c = a .OR. b
            
  End function Add_Logicals

  Function Divide_By_Two(a) result(b)
    Integer, intent(in)  :: a
    Integer              :: b
            
    b = a / 2
            
  End function Divide_By_Two

  Function J_Plus_K_Squared(j, k) result(l)
    Integer, intent(in)  :: j
    Integer, intent(in)  :: k
    Integer              :: l
    
            l = (j + k) * (j + k)
    
  End function J_Plus_K_Squared
End module Operators

!- End of module header
!+ Test program for operator module
!
Program Try_Operators

! Description:
! Program to test the operators defined in the module
! Operators
!
! Current Code Owner: A N Other
! History:
! Version   Date   Comment
! -------  ------  -------
!   1.0    5/5/95  Original code. (A N Other)
!
! Code Description:
! Language: Fortran 90.

! Modules used:

Use Operators

Implicit none

! Local scalars:
Character(len=5)  :: string5 = '-1234'   ! Char. to integer

Integer           :: i_value = 99999     ! and half operator 
Integer           :: half_value          ! test variables.
Integer           :: n                   ! Test variables  
Integer           :: l = 4               ! for add & square
Integer           :: m = 8               ! operation.

Logical           :: x = .false.         ! Test variables     
Logical           :: y = .true.          ! for logical    
Logical           :: z = .false.         ! addition ops.

!- End of header

  i_value = string5     ! Convert from a string to an integer.
  z       = x + y       ! Add together some logicals.

  Print *, string5, i_value, x, y, z

  half_value = .Half. i_value   ! Use user def unary operator
  n          = l .JPlusKSq. m   ! Use user def binary operator

  Print *, i_value, half_value, l, m, n

End program Try_Operators

