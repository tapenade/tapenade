subroutine test(x, y, z, zz, zzz)
real x, y
complex z, zz
complex*8 zzz
z = cmplx(x, y)
zz = cmplx(z)
zzz = cmplx(x, y, kind=8)
end

program main
real t, u
complex v, w, z
t = 3.0
u = 4.0
call test(t, u, v, w, z)
 print*,v
 print*,w
 print*,z
end program
