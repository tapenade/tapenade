 module blocks

   implicit none
   private

   type, public :: block ! block data type
      integer (4) :: &
         block_id ,&! global block number
         local_id ,&! local address of block in current distrib
         ib, ie, jb, je ,&! begin,end indices for physical domain
         iblock, jblock ! cartesian i,j position for bloc

      integer (4) :: &
         i_glob(5),j_glob(6)
   end type

 end module blocks
