subroutine test(x,y)
   implicit none
   real x(4)
   real y(4)

   y = x + 1;
   where (x > 1)
        y = x + 2;     
        y = x + 3;
   elsewhere (x > 2)
        y = x + 4;
   elsewhere (x > 3)
        y = x + 5;
   end where
   y = x + 6;

end subroutine test
