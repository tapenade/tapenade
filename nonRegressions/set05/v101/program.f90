MODULE BO
       INTERFACE SWAPINTERFACE
              MODULE PROCEDURE SWAP_R, SWAP_I, SWAP_C, swap_d
       END INTERFACE
CONTAINS

       SUBROUTINE SWAP_R(A, B)
       IMPLICIT NONE
       REAL, INTENT (INOUT)                 :: A, B
       REAL                                 :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_R

       SUBROUTINE SWAP_I(A, B)
       IMPLICIT NONE
       INTEGER, INTENT (INOUT)              :: A, B
       INTEGER                              :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_I

       SUBROUTINE SWAP_C(A, B)
       IMPLICIT NONE
       CHARACTER, INTENT (INOUT)            :: A, B
       CHARACTER                            :: TEMP
                  TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_C

       SUBROUTINE SWAP_D(A, B)
       IMPLICIT NONE
       double precision, INTENT (INOUT)                 :: A, B
       double precision                                 :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_D
END MODULE BO

module DO
use BO, swap => swapinterface
      interface 
      subroutine head(i1,i2,o)
      double precision i1,i2,o
      end subroutine head
      end interface

contains

      subroutine head(i1,i2,o)
      common /zz/ zn,zd
      double precision i1,i2,o,zn,zd
      call sub0(i1,i2)
      o = zn / zd
      return
      end subroutine

      subroutine sub0(u,v)
      common /zz/ zn,zd
      double precision u,v,z1,z2,zn,zd
      call swap(u,v)
      call sub1(u,z1)
      call sub2(v,z2)
      zn = z1 - z2
      zd = 1 + z1 + z2
      return
      end subroutine

      subroutine sub1(x,y)
      double precision x,y
      y = f(x) + g(x)
      y = sqrt(y)
      return
      end subroutine

      subroutine sub2(x,y)
      double precision x,y
      y = f(x) - g(x)
      y = sqrt(y)
      return
      end subroutine

      double precision function f(t)
      double precision t
      f = t * t
      f = exp(f)
      return
      end function

      double precision function g(t)
      double precision t
      g = 1
      if (t .ne. 0.d0) then
        g = sin(t) / t + factorial(t)
      endif
      return
      end function




       RECURSIVE FUNCTION FACTORIAL(N) RESULT (FAC_RESULT)
       IMPLICIT NONE
       double precision, INTENT(IN)      :: N
       double precision               :: FAC_RESULT
       IF ( N <=1 ) THEN
               FAC_RESULT = 1
       ELSE
               FAC_RESULT = int(N) * FACTORIAL(N-1)
       END IF
       END FUNCTION FACTORIAL

      end module DO



PROGRAM SWAP_MAIN
USE BO , swap => swapinterface
       IMPLICIT NONE
       INTEGER                    :: I, J, K, L
       REAL                       :: A, B, X, Y
       CHARACTER                  :: C, D, E, F


       I = 1   ;  J = 2         ;     K = 100 ; L = 200
       A = 7.1 ;  B = 10.9      ;     X = 11.1; Y = 17.0
       C = 'a' ;  d = 'b'       ;     E = '1' ; F = '"'

       WRITE (*,*) I, J, K, L, A, B, X, Y, C, D, E, F
       CALL  SWAP (I, J)  ;  CALL SWAP (K, L)
       CALL  SWAP (A, B)  ;  CALL SWAP (X, Y)
       CALL  SWAP (C, D)  ;  CALL SWAP (E, F)
       WRITE (*,*) I, J, K, L, A, B, X, Y, C, D, E, F

END program
