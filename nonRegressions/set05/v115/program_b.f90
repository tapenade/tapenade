!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:15
!
!  Differentiation of sub1 in reverse (adjoint) mode:
!   gradient     of useful results: x a b
!   with respect to varying inputs: x a b
!   RW status of diff variables: x:in-zero a:incr b:incr
SUBROUTINE SUB1_B(x, xb, a, ab, b, bb)
  IMPLICIT NONE
  DOUBLE PRECISION :: x
  DOUBLE PRECISION :: xb
  DOUBLE PRECISION, DIMENSION(4) :: a, b
  DOUBLE PRECISION, DIMENSION(4) :: ab, bb
  INTRINSIC SUM
  ab = ab + b*xb
  bb = bb + a*xb
  xb = 0.D0
END SUBROUTINE SUB1_B

