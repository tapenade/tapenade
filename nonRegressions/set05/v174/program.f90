!! pour tester les pushpop dans un where
subroutine test1(a,b,x)
real, dimension (200) :: a
real, dimension (200) :: b
real :: x
where (b(:) > 0) 
        a(:) = b(:) + x
        b(:) = 2*b(:) -8
end where
end subroutine test1

