SUBROUTINE eos_insitu_pot (zws, psal)
  REAL, DIMENSION(jpi,jpj,jpk), INTENT( in ) :: psal
  REAL, DIMENSION(jpi,jpj,jpk), INTENT( out ) :: zws
  REAL, DIMENSION(jpi,jpj,jpk):: truclocal
  zws(:,:,:) = SQRT( ABS( psal(:,:,:) ) )
END SUBROUTINE eos_insitu_pot
