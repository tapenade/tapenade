MODULE ldfdyn
   IMPLICIT NONE

CONTAINS

   SUBROUTINE ldf_dyn_c3d_orca( ld_print )
      !!----------------------------------------------------------------------
      !!                  ***  ROUTINE ldf_dyn_c3d  ***
      !!                   
      !! ** Purpose :   ORCA R2 an R4 only
      !!
      !! ** Method  :   blah blah blah ....
      !!----------------------------------------------------------------------
      !! * Modules used
 
      !! * Arguments
      LOGICAL, INTENT (in) :: ld_print   ! If true, output arrays on numout

      !! * local variables
      INTEGER ::   ji, jj, jk, jn, jp_cfg, aht0, ahm0 ,rpi   ! dummy loop indices
      INTEGER ::   ii0, ii1, ij0, ij1  ! temporary integers
      INTEGER ::   inum = 11           ! temporary logical unit
      INTEGER ::   iost, iim, ijm
      INTEGER ::   ifreq, il1, il2, ij, ii, jpidta, jpjdta, jpi   , jpj  ,jpk
      INTEGER, DIMENSION(jpidta, jpjdta) ::   idata
      INTEGER, DIMENSION(jpi   , jpj   ) ::   icof

      REAL(8) ::   &
         zahmeq, zcoff, zcoft, zmsk,   & ! ???
         zemax, zemin, zeref, zahmm
      REAL(8), DIMENSION(jpi,jpj) ::   zahm0, e1t, e2t,gphit
      REAL(8), DIMENSION(jpi,jpj,jpk) ::  tmask
      REAL(8), DIMENSION(jpk) ::   zcoef

      CHARACTER (len=15) ::   clexp
      !!----------------------------------------------------------------------

      ! Read 2d integer array to specify western boundary increase in the
      ! ===================== equatorial strip (20N-20S) defined at t-points
      IF( jp_cfg == 4 )   THEN
         zahmeq = 5.0 * aht0

         zemax = MAXVAL ( e1t(:,:) * e2t(:,:), tmask(:,:,1) .GE. 0.5 )
         zemin = MINVAL ( e1t(:,:) * e2t(:,:), tmask(:,:,1) .GE. 0.5 )
         zeref = MAXVAL ( e1t(:,:) * e2t(:,:),   &
             &   tmask(:,:,1) .GE. 0.5 .AND. ABS(gphit(:,:)) .GT. 50. )
!!!!!ICI!!!!! 
         DO jj = 1, jpj
           DO ji = 1, jpi
              zmsk = e1t(ji,jj) * e2t(ji,jj)
              IF( abs(gphit(ji,jj)) .LE. 15 ) THEN 
                 zahm0(ji,jj) = ahm0
              ELSE 
                 IF( zmsk .GE. zeref ) THEN 
                    zahm0(ji,jj) = ahm0
                 ELSE 
                    zahm0(ji,jj) = zahmm + (ahm0-zahmm)*(1.0 -   &
                        &          cos((rpi*0.5*(zmsk-zemin)/(zeref-zemin))))
                 ENDIF 
              ENDIF 
           END DO 
         END DO 
      ENDIF

   END SUBROUTINE ldf_dyn_c3d_orca

END MODULE ldfdyn
