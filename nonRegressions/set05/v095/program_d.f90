!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 18 Apr 2019 18:10
!
!  Differentiation of sub1 in forward (tangent) mode:
!   variations   of useful results: d o
!   with respect to varying inputs: d o a b
!   RW status of diff variables: d:in-out o:in-out a:in b:in
SUBROUTINE SUB1_D(a, ad, b, bd, d, dd, n, o, od)
  IMPLICIT NONE
  DOUBLE PRECISION, DIMENSION(9, 9) :: a, b, d
  DOUBLE PRECISION, DIMENSION(9, 9) :: ad, bd, dd
  INTEGER :: n, m
  INTEGER :: i
  INTEGER :: j
  INTRINSIC SUM
  REAL :: o
  REAL :: od
  DO i=1,4
    DO j=1,4
      dd(i, j) = SUM(b(2:9:n, j)*ad(i, 1:8:n) + a(i, 1:8:n)*bd(2:9:n, j)&
&       )
      d(i, j) = SUM(a(i, 1:8:n)*b(2:9:n, j))
      dd(i, j) = SUM(b(2:9:m, j)*ad(i, 1:8:m) + a(i, 1:8:m)*bd(2:9:m, j)&
&       )
      d(i, j) = SUM(a(i, 1:8:m)*b(2:9:m, j))
      o = j
      dd(i, j) = SUM(b(2:9:o, j)*ad(i, 1:8:o) + a(i, 1:8:o)*bd(2:9:o, j)&
&       )
      d(i, j) = SUM(a(i, 1:8:o)*b(2:9:o, j))
      od = 0.0
    END DO
  END DO
  RETURN
END SUBROUTINE SUB1_D

