SUBROUTINE p(rho, ec, order)
    IMPLICIT NONE

    REAL :: rho
    INTEGER :: ec
    INTEGER :: order
    optional order
    print*,'call p'
    IF (PRESENT(order)) print*,'order ', order
    print*,'order ', order
END SUBROUTINE p

SUBROUTINE test()
  integer :: ord, opt, e
  real :: r, x
  opt = 1
  ord = 2
  call p()
  call p(r)
  call p(r, e)
  call p(r, e, ord)
END SUBROUTINE test

program pr
call test()
end program
