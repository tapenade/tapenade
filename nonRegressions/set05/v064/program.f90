module lib
   IMPLICIT NONE
    INTEGER,PUBLIC,PARAMETER :: wp=8
   INTERFACE mpp_sum
      MODULE PROCEDURE mppsum_a_real, mppsum_real
   END INTERFACE

contains
  SUBROUTINE mppsum_a_real( ptab, kdim )
    INTEGER , INTENT( in  )                  ::   kdim
    REAL(wp), INTENT(inout), DIMENSION(kdim) ::   ptab
    integer ji
      DO ji = 1, kdim
         ptab(ji) = ptab(ji) + 1.0
      END DO
  END SUBROUTINE mppsum_a_real

  SUBROUTINE mppsum_real( ptab )
    REAL(wp), INTENT( inout ) ::   ptab 
    ptab = ptab + 1.0
  END SUBROUTINE mppsum_real

  subroutine test( a_emp, pta, k )
    integer k
    real(wp) :: a_emp
    real(wp), dimension(k) :: pta
    CALL mpp_sum( a_emp )  
    CALL mpp_sum( pta, k )
  end subroutine test

end module lib
