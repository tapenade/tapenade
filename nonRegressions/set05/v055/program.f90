subroutine test(a,b)
integer, parameter ::  n=200
real, dimension (n) :: a
real, dimension (n) :: b
forall(i = 2:n-1) b(i) = b(i) + a(i)
named1:forall (i = 10:20)
  a(i) = a(i) * b(i)
end forall named1
named2:forall (i = 3:5)
  a(i) = a(i) * b(i)
  b(i) = 0.0
end forall named2
end subroutine test
