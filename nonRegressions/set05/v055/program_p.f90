!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 2.2.3 (r2101M) - 09/14/2007 11:29
!  
SUBROUTINE TEST(a, b)
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=200
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: b
  INTEGER :: i
  FORALL (i=2:n-1)  b(i) = b(i) + a(i)
named1:FORALL (i=10:20)  a(i) = a(i)*b(i)
named2:FORALL (i=3:5) 
    a(i) = a(i)*b(i)
    b(i) = 0.0
  END FORALL named2
END SUBROUTINE TEST

