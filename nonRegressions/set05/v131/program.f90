      function F(t,u)
      real,dimension(2):: F,t,u
       F = t*u
      return
      end

      subroutine head(a,b,c,resu)
       real,dimension(2) :: a,b,c
       real,dimension(2):: resu
      resu = b*c
      resu = sum(F(a,2.0) * resu, mask = resu>0)
      return
      end
