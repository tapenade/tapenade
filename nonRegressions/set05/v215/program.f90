 Subroutine ForcesEval(r,v,u,drag,grav,thrust)
    implicit none
    real(kind=8), intent(in), dimension(3) :: r, v
    real(kind=8), intent(in), dimension(5) :: u
    real(kind=8), intent(out), dimension(3) :: drag, grav, thrust
    drag = r * drag
 end subroutine

