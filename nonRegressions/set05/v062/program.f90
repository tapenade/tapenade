module M
   contains
     function func(t,u)
        real func,t,u
        func = (t + u) / 2
     end function func
end module

module N
      contains
      real function minimum(a, b, f)
        implicit none
        real, intent(in) :: a,b
        interface
           real function f(t,u)
             real t, u
           end function f
        end interface
        minimum = f(a,b)
      end function minimum
end module

module O
   contains
    real function ftest(r,s,g)
      use N
      implicit none
      interface
           real function g(t,u)
             real t, u
           end function g
        end interface
      real r,s
      ftest= minimum(r,s,g)
    end function ftest
end module

program test
use M, g => func
use O
implicit none
real r,s
r = 3.0
s = 6.0
print*,ftest(r,s,g)
end program test
