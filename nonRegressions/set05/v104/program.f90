  SUBROUTINE mppsum_real2( ptab, cst, str)
    use lib
    real(wp) :: cst
    REAL(wp), INTENT(inout) ::   ptab
    dimension ptab(i,j)
    CHARACTER(len=1), INTENT( in ) ::   str
    integer ji
      DO ji = 1, 10
         ptab(ji,1) = ptab(ji,1) * cst
      END DO
  END SUBROUTINE mppsum_real2


module lib
   IMPLICIT NONE
   integer, parameter :: i = 10
   integer, parameter :: j = 20
   integer, parameter :: k = 30
    INTEGER,PUBLIC,PARAMETER :: wp=8

end module lib
