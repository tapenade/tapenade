subroutine test1(a,b)
real, dimension (200) :: a
real, dimension (200) :: b
b = b + a
end subroutine test1

subroutine test2(a,b)
real, dimension (200) :: a
real, dimension (200) :: b
do i=1,200
  b(i) = b(i) + a(i)
end do
end subroutine test2

subroutine test3(a,b)
real, dimension (200) :: a
real, dimension (200) :: b
b(:) = b(:) + a(:)
end subroutine test3

subroutine test(a,b)
call test1(a,b)
call test2(a,b)
call test3(a,b)
end subroutine test
