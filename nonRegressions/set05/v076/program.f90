module Init
  IMPLICIT NONE
  INTEGER,PUBLIC,PARAMETER :: dp=selected_real_kind(12, 307)
  INTEGER,PUBLIC,PARAMETER :: jpi = 4
 INTEGER,PUBLIC,PARAMETER :: wp=dp
end module Init

module M
use Init
       real(wp), public, DIMENSION(jpi) ::  sa,t2
end module

module N
use M
contains
      real(wp) function g(t)
      real(wp) t
      g = t * t + sa(1)
      return
      end function
end module N


module O
use M
contains
      real(wp) function h(t)
       use M, only : y1 => sa
      real(wp) t
      h = t * t + y1(1)
      return
      end function
end module O

module All
contains
subroutine top(t)
use N
use O
use M
real(wp) t
t = g(t) + h(t) + t2(1)
end subroutine
end module All
