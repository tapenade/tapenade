MODULE lbclnk
   IMPLICIT NONE
 INTEGER, PUBLIC, PARAMETER :: jpi=1
 INTEGER, PUBLIC, PARAMETER :: jpj=2
 INTEGER, PUBLIC, PARAMETER :: jpk=3
 INTEGER, PUBLIC, PARAMETER :: wp=8


   PRIVATE

   INTERFACE lbc_lnk
      MODULE PROCEDURE lbc_lnk_3d, lbc_lnk_2d
   END INTERFACE

   PUBLIC lbc_lnk       ! ocean/ice  lateral boundary conditions

CONTAINS

   SUBROUTINE lbc_lnk_3d( pt3d, cd_type, psgn )
      CHARACTER(len=1), INTENT( in ) ::   &
         cd_type       ! nature of pt3d grid-points
      REAL(wp), DIMENSION(jpi,jpj,jpk), INTENT( inout ) ::   pt3d 
      REAL(wp), INTENT( in ) ::   &
         psgn          ! control of the sign change

      INTEGER  ::   ji, jk
      INTEGER  ::   ijt, iju

       pt3d(1,1,1) = pt3d(1,1,1) *  pt3d(1,1,1) * psgn

   END SUBROUTINE lbc_lnk_3d


   SUBROUTINE lbc_lnk_2d( pt2d, cd_type, psgn )
      CHARACTER(len=1), INTENT( in ) ::   &
         cd_type       ! nature of pt2d grid-point
       REAL(wp), INTENT( in ) ::    psgn

      REAL(wp), DIMENSION(jpi,jpj), INTENT( inout ) ::    pt2d 

      INTEGER  ::   ji
      INTEGER  ::   ijt, iju

       pt2d(1,1) = pt2d(1,1) *  pt2d(1,1) * psgn

   END SUBROUTINE lbc_lnk_2d

END MODULE lbclnk

subroutine test(x,y)
use lbclnk
REAL(wp), DIMENSION(jpi,jpj,jpk) :: x
CHARACTER(len=1), INTENT( in ) :: y
call lbc_lnk(x,y,-1._wp)
end subroutine test
