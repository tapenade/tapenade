subroutine test1(a,b)
real, dimension (*) :: a
real, dimension (*) :: b
do i=1,200
  b(i) = b(i) + a(i)
end do
call test2(a,b)
do i=1,200
  b(i) = b(i) * a(i)
end do
end subroutine test1

subroutine test2(a,b)
real, dimension (*) :: a
real, dimension (*) :: b
do i=1,200
  b(i) = b(i) + a(i)
end do
end subroutine test2

subroutine test3(a,b)
real, dimension (*) :: a
real, dimension (*) :: b
do i=1,200
  b(i) = b(i) - a(i)
end do
end subroutine test3

subroutine test(a,b)
real, dimension (*) :: a
real, dimension (*) :: b
call test1(a,b)
do i=1,200
  b(i) = a(i) / b(i)
end do
call test3(a,b)
end subroutine test
