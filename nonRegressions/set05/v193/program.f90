   subroutine find(name, current, root, forest_root)
   type :: node
       character(len=12)     :: name
       type(node), pointer        :: parent
       type(node), pointer        :: sibling 
       type(node), pointer        :: child
   end type node
   type(node), pointer :: current, forest_root
      character(len=12), intent(in) :: name
      type(node), pointer    :: root
      do
         if (associated(current)) then
            return
         end if
         if (.not.associated(root)) then
            exit
         end if
      end do
   contains 
      recursive subroutine look(root)
         type(node), pointer    :: root
         type(node), pointer    :: child
         if (.NOT. root%name == name) then
            do
               if (.not.associated(child)) then
                 exit
               end if
               if (associated(current)) then
                 return
               end if
            end do
         end if
      end subroutine look
   end subroutine find
 
