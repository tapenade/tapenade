module m1
contains
subroutine test1(a,b)
real, dimension (200) :: a
real, dimension (200) :: b
b = b + a
end subroutine test1
end module

module m2
contains
subroutine test2(a,b)
real, dimension (200) :: a
real, dimension (200) :: b
do i=1,200
  b(i) = b(i) + a(i)
end do
end subroutine test2
end module

module m3
contains
subroutine test3(a,b)
real, dimension (200) :: a
real, dimension (200) :: b
b(:) = b(:) + a(:)
end subroutine test3
end module

module m
use m1
use m2
use m3
contains
subroutine test(a,b)
real, dimension (200) :: a
real, dimension (200) :: b
call test1(a,b)
call test2(a,b)
call test3(a,b)
end subroutine test
end module
