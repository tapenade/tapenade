real function somcarre(x)
external carre
real, dimension(:), intent(in) :: x
! y array[1:10]
! z array[1:5]
real, dimension(5), save:: y(10), z
call carre(x,y)
somcarre=sum(y)
end function somcarre

subroutine carre(x,y)
real, dimension(:), intent(in) :: x
real, dimension(:), intent(out) :: y
do i=1,10
  y(i)=x(i)*x(i)
end do
end subroutine carre
