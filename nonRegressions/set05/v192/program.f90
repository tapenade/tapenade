SUBROUTINE TEST(x, y)
  IMPLICIT NONE
  REAL :: x(4), y(4)
  LOGICAL :: mask(4), mask0(4)
  mask = x .GT. 1
  WHERE (mask) 
    mask0 = x .GT. 4
    x = x + 5*y
    WHERE (mask0)
      x = x + 3*y
    END WHERE
  END WHERE
  x = x + y
END SUBROUTINE TEST

