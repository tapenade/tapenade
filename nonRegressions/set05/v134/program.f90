      subroutine sub1(x,a)
      double precision, dimension (4,6) :: x
      double precision, dimension (4,5,6) :: a
      x(:,:) = sum(a(:,:,:), 2)
      return
      end
