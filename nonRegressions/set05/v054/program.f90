module test_mod
   implicit none
   interface f
      module procedure f_vector, f_elemental
   end interface f
   contains
      pure function f_vector(x)
         real, intent(in) :: x(:)
         real f_vector(size(x))

         f_vector = 1/x
      end function f_vector

      elemental function f_elemental(x)
         real, intent(in) :: x
         real f_elemental

         f_elemental = 1/x
      end function f_elemental
end module test_mod

program test
   use test_mod
   implicit none
   real x(4)
   real y(4)

   x = [0,1,2,3]
   y = 42
   where(x /= 0)
      y = f(x)
   endwhere
   write(*,*) y
   where(x /= 0)
      y = f(x)
   elsewhere
      y = x
   endwhere
   write(*,*) y
   where(x == 1)
      y = f(x)
   elsewhere (x == 2)
      y = f(x)
   elsewhere (x == 3)
      y = f(x)
   elsewhere
      y = 0
   endwhere
   write(*,*) y
end program test
