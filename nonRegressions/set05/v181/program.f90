module test
  interface func
     module procedure func1, func2
  end interface
contains
     subroutine func1(mb)
        real, dimension(10) ::  mb
        mb(:) = mb(:) * mb(:)
        print*,'func1 '
        print*,mb
     end subroutine func1

     subroutine func2(mb, i)
        real ::  mb
        integer :: i
        mb = mb * mb * 2
        print*,'func2 '
        print*,mb
     end subroutine func2
end module

module run
use test
contains
subroutine s(mb1, mb2)
integer, dimension(10) ::  mb1
real, dimension(10) ::  mb3
real*8, dimension(10) ::  mb4
real ::  mb2
mb3(:) = real(mb1(:))
mb4(:) = real(mb1(:), kind=8)
mb3(:) = sin(mb3(:))
call func(real(mb1(:)))
call func(mb2, 2)
call func(mb3)
call func(sin(mb3))
end subroutine s
end module

program test2
use run
integer, dimension(10) :: m1
real :: m2
m1(1) = 2
m2 = 2.0
call s(m1, m2)
end program test2
