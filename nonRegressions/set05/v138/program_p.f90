!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:15
!
SUBROUTINE P()
  IMPLICIT NONE
  INTEGER, PARAMETER :: jpi=10
  INTEGER, PARAMETER :: jpj=20
  INTEGER, PARAMETER :: jpk=30
  INTEGER, PARAMETER :: wp=8
  REAL(wp), DIMENSION(jpi, jpj) :: e2t
  REAL(wp), DIMENSION(jpi, jpj, jpk) :: tmask
  INTEGER, DIMENSION(2) :: iloc
  INTRINSIC MINLOC
  iloc = MINLOC(e2t(:, :), mask=tmask(:, :, 1).EQ.1.e0)
END SUBROUTINE P

