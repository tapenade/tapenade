SUBROUTINE p()
    IMPLICIT NONE
     INTEGER, PARAMETER :: jpi = 10
     INTEGER, PARAMETER :: jpj = 20
     INTEGER, PARAMETER :: jpk = 30
     INTEGER, PARAMETER :: wp = 8
     REAL(wp), DIMENSION(jpi,jpj) :: e2t
     REAL(wp), DIMENSION(jpi,jpj,jpk) :: tmask
     INTEGER, DIMENSION(2) ::   iloc    
    iloc  = MINLOC( e2t(:,:), mask = tmask(:,:,1) == 1.e0 )
END SUBROUTINE p

