SUBROUTINE p(rho, ec, order, value, x)
    IMPLICIT NONE

    REAL :: rho, ec, x, value
    INTEGER :: order
    optional order, value
    if (order .eq. 0) then
       rho = rho * value * ec  + x
    else
       rho = rho * value / ec + x
    endif
    value = rho + value
END SUBROUTINE p

SUBROUTINE test(r,e)
  real :: r, e, y, v
  integer :: i(2)
 interface
   SUBROUTINE p(rho, ec, order, value, x)
    REAL :: rho, ec, x, value
    INTEGER :: order
    optional order, value
   END SUBROUTINE p
 end interface
  call p(ec=e, rho=r)
  call p(ec=e, rho=r, x)
  call p(ec=e, x=r, rho=r)
  call p(order=0, ec=e, x=y, rho=r)
  call p(order=0, ec=e, x=y, rho=r, value=v)
  call p(order=0, ec=e, x=y, rho=i)
  call p(r, ec=e, x=r)
  call p(r, x=r, ec=e)
  call p(r, ec=e, y)
  call p(r, e, 0, y)
  call p(r, e, y)
  call p(rhofaux=r, x=r, ec=e)
  call p(x, order=0, ec=e, x=y, rho=r, value=v)
END SUBROUTINE test
