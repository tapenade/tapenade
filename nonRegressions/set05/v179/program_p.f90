!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_cuda) - 21 Nov 2022 11:22
!
MODULE FLIOCOM
  IMPLICIT NONE
  PRIVATE :: flio_uga

CONTAINS
  SUBROUTINE FLIO_UGA(f_i, v_n, a_n, avr_4_0, avr_4_1, avr_8_0, avr_8_1&
&   , avi_4_0, avi_4_1, avtx)
    IMPLICIT NONE
!----------------------
    CHARACTER(len=*), INTENT(IN) :: a_n
    INTEGER(kind=4), INTENT(OUT), OPTIONAL :: avi_4_0
    INTEGER(kind=4), DIMENSION(:), INTENT(OUT), OPTIONAL :: avi_4_1
    REAL(kind=4), INTENT(OUT), OPTIONAL :: avr_4_0
    REAL(kind=4), DIMENSION(:), INTENT(OUT), OPTIONAL :: avr_4_1
    REAL(kind=8), INTENT(OUT), OPTIONAL :: avr_8_0
    REAL(kind=8), DIMENSION(:), INTENT(OUT), OPTIONAL :: avr_8_1
    CHARACTER(len=*), INTENT(OUT), OPTIONAL :: avtx
    INTEGER, INTENT(IN) :: f_i
    CHARACTER(len=*), INTENT(IN) :: v_n
    INTEGER :: f_e, i_rc, i_v, l_ea, l_ua, t_ea
    LOGICAL :: l_dbg
    INTRINSIC TRIM, PRESENT, SIZE, LEN
    IF (l_dbg) WRITE(*, *) '->fliogeta ', TRIM(v_n), ' ', TRIM(a_n)
!-

  END SUBROUTINE FLIO_UGA

!===
  SUBROUTINE FLIOGA_TX_0D(f_i, v_n, a_n, a_v)
    IMPLICIT NONE
!---------------------------
    CHARACTER(len=*), INTENT(IN) :: a_n
    CHARACTER(len=*), INTENT(OUT) :: a_v
    INTEGER, INTENT(IN) :: f_i
    CHARACTER(len=*), INTENT(IN) :: v_n
!-
!---------------------------------------------------------------------
    CALL FLIO_UGA(f_i, v_n, a_n, avtx=a_v)
  END SUBROUTINE FLIOGA_TX_0D
!===

END MODULE FLIOCOM

