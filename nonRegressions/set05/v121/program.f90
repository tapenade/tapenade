module b
  use a
  real ::  xb(ia)
end module

module c
  use b
contains
      subroutine sub(x,y)
      real x,y
      y = x + xb(0)
      end
end module
