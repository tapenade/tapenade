module a
  logical, dimension(:), pointer :: iCo
  type BCDataType
    real :: rho
  end type BCDataType
  type(BCDataType), dimension(:), pointer :: BCData
end module
