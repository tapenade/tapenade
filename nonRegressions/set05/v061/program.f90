! This tests the 2nd stage of AD, where the end-user has seen the message
! "Please provide a differential of function func for arguments ..."
! and has modified the command-line accordingly by adding func as a diff root.
! It just so happens that there's no activity in the end,
!  but Tapenade can't detect that so all this is normal.
module M
   contains
     function func(t,u)
        real func,t,u
        func = (t + u) / 2
     end function func
end module

module N
      contains
      real function minimum(a, b, f)
        implicit none
        real, intent(in) :: a,b
        interface
           real function f(t,u)
             real t, u
           end function f
        end interface
        minimum = f(a,b)
      end function minimum
end module

module O
   contains
    real function ftest(r,s,g)
      use N
      implicit none
      interface
           real function g(t,u)
             real t, u
           end function g
        end interface
      real r,s
      ftest= minimum(r,s,g)
    end function ftest

    subroutine test(r,s)
      use M, g => func
      implicit none
      real r,s
      print*,ftest(r,s,g)
    end subroutine test
end module
