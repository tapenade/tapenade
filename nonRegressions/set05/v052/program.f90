module twice_module 
implicit none
public :: twice_real, twice_int
contains
elemental function twice_real(x) result(value)
real, intent(in) :: x 
real             :: value
value = 2*x 
end function twice_real

elemental function twice_int(i) result(value)
integer, intent(in) :: i
integer             :: value
value = 2*i 
end function twice_int
end module twice_module 
!
function test(x, i)
use twice_module
real x, test
integer i
test = twice_real(x) + twice_int(i)
end function test

program xtwice
use twice_module
implicit none
external test
real test
print*,twice_int(3)
print*,twice_int((/3,4,5/))
print*,twice_real(1.2)
print*,twice_real((/1.2,2.2,3.2/))
print*,test(3.0,3)
end program xtwice

