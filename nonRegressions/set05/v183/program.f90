module test
  integer, parameter :: wp = 8
INTEGER,PUBLIC,PARAMETER :: dp=selected_real_kind(12, 307)
  interface func
     module procedure func1, func2
  end interface
contains
     subroutine func1(mb)
        real(wp) ::  mb
        mb = mb * mb * mb
        print*,'func1 '
        print*,mb
     end subroutine func1

     subroutine func2(mb)
        real*4 ::  mb
        mb = mb * mb * 2
        print*,'func2 '
        print*,mb
     end subroutine func2
end module

module run
use test
contains
subroutine s(mb1, mb2, mb3)
real(wp) ::  mb1
real ::  mb2
real*8 ::  mb3
call func(mb1)
call func(mb2)
call func(mb3)
end subroutine s
end module

program test2
use run
real(wp) :: m1
real :: m2
real*8 :: m3
print*,dp
m1 = 2.0
m2 = 2.0
m3 = 2.0
call s(m1, m2, m3)
end program test2
