module test
  interface func
     module procedure func1, func2
  end interface
contains
     subroutine func1(m, string1, string2)
        character(len=*), intent(in) ::  string1, string2
        real :: m
        print*,'func1 '
        m = m * m
     end subroutine func1

     subroutine func2(m, string, n)
        real ::  m, n
        character(len=*), intent(in) ::  string
        m = m * n * 2
        print*,'func2 '
     end subroutine func2
end module

module run
use test
contains
subroutine s(x, y)
real ::  x,y
call func(x, 'string', 'char')
call func(x, 'string', 'X')
call func(x, 'truc', y)
end subroutine s
end module

program test2
use run
real :: x,y
x = 2.0
y = 2.0
call s(x, y)
end program test2
