! from validations nemo

MODULE defprec 
  INTEGER,PARAMETER :: i_2=SELECTED_INT_KIND(4)
  INTEGER,PARAMETER :: i_4=SELECTED_INT_KIND(9)
  INTEGER,PARAMETER :: i_8=SELECTED_INT_KIND(13)
  INTEGER,PARAMETER :: r_4=SELECTED_REAL_KIND(6,37)
  INTEGER,PARAMETER :: r_8=SELECTED_REAL_KIND(15,307)

   INTEGER,PARAMETER :: i_std=i_4, r_std=r_8

END MODULE defprec

module typeSizes
  implicit none
  public
  integer, parameter ::   OneByteInt = selected_int_kind(2), &
                          TwoByteInt = selected_int_kind(4), &
                         FourByteInt = selected_int_kind(9), &
                        EightByteInt = selected_int_kind(18)

  integer, parameter ::                                          &
                        FourByteReal = selected_real_kind(P =  6, R =  37), &
                       EightByteReal = selected_real_kind(P = 13, R = 307)
end module typeSizes


 module netcdf
  use typeSizes, only: OneByteInt, TwoByteInt, FourByteInt, EightByteInt, &
                       FourByteReal, EightByteReal
  implicit none
  private

  integer, parameter, public :: &
    nf90_max_dims     = 1024,    &
    nf90_max_var_dims = nf90_max_dims
  
  interface nf90_get_var
    module procedure  &
    nf90_get_var_1D_FourByteReal, &
    nf90_get_var_2D_FourByteReal,&
    nf90_get_var_3D_FourByteReal,&
    nf90_get_var_4D_FourByteReal,&
    nf90_get_var_5D_FourByteReal,&
    nf90_get_var_6D_FourByteReal,&
    nf90_get_var_7D_FourByteReal

  end interface ! nf90_get_var
  
  ! overloaded functions
  public :: nf90_get_var
contains

   function nf90_get_var_1D_FourByteReal(ncid, varid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid, varid
     real (kind = FourByteReal), dimension(:), &
                                      intent(out) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
     integer                                      :: nf90_get_var_1D_FourByteReal
 
     integer, dimension(nf90_max_var_dims) :: localStart, localCount, localStride, localMap
     integer                               :: numDims, counter
 
   end function nf90_get_var_1D_FourByteReal


   function nf90_get_var_2D_FourByteReal(ncid, varid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid, varid
     real (kind = FourByteReal), dimension(:, :), &
                                      intent(out) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
     integer                                      :: nf90_get_var_2D_FourByteReal
 
     integer, dimension(nf90_max_var_dims) :: localStart, localCount, localStride, localMap
     integer                               :: numDims, counter
 
   end function nf90_get_var_2D_FourByteReal


   function nf90_get_var_3D_FourByteReal(ncid, varid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid, varid
     real (kind = FourByteReal), dimension(:, :, :), &
                                      intent(out) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
     integer                                      :: nf90_get_var_3D_FourByteReal
 
     integer, dimension(nf90_max_var_dims) :: localStart, localCount, localStride, localMap
     integer                               :: numDims, counter
 
   end function nf90_get_var_3D_FourByteReal


   function nf90_get_var_4D_FourByteReal(ncid, varid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid, varid
     real (kind = FourByteReal), dimension(:, :, :, :), &
                                      intent(out) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
     integer                                      :: nf90_get_var_4D_FourByteReal
 
     integer, dimension(nf90_max_var_dims) :: localStart, localCount, localStride, localMap
     integer                               :: numDims, counter
 
   end function nf90_get_var_4D_FourByteReal


   function nf90_get_var_5D_FourByteReal(ncid, varid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid, varid
     real (kind = FourByteReal), dimension(:, :, :, :, :), &
                                      intent(out) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
     integer                                      :: nf90_get_var_5D_FourByteReal
 
     integer, dimension(nf90_max_var_dims) :: localStart, localCount, localStride, localMap
     integer                               :: numDims, counter
 
   end function nf90_get_var_5D_FourByteReal


   function nf90_get_var_6D_FourByteReal(ncid, varid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid, varid
     real (kind = FourByteReal), dimension(:, :, :, :, :, :), &
                                      intent(out) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
     integer                                      :: nf90_get_var_6D_FourByteReal
 
     integer, dimension(nf90_max_var_dims) :: localStart, localCount, localStride, localMap
     integer                               :: numDims, counter
   end function nf90_get_var_6D_FourByteReal


   function nf90_get_var_7D_FourByteReal(ncid, varid, values, start, count, stride, map)
     integer,                         intent( in) :: ncid, varid
     real (kind = FourByteReal), dimension(:, :, :, :, :, :, :), &
                                      intent(out) :: values
     integer, dimension(:), optional, intent( in) :: start, count, stride, map
     integer                                      :: nf90_get_var_7D_FourByteReal
 
     integer, dimension(nf90_max_var_dims) :: localStart, localCount, localStride, localMap
     integer                               :: numDims, counter
 
   end function nf90_get_var_7D_FourByteReal


end module netcdf



!$Header: /home/ioipsl/CVSROOT/IOIPSL/src/fliocom.f90,v 2.29 2006/01/26 11:43:23 adm Exp $
!-
MODULE fliocom
!---------------------------------------------------------------------
!-
USE defprec
USE netcdf
IMPLICIT NONE
!-
PRIVATE
!-
PUBLIC :: &

 fliogstc

CONTAINS


SUBROUTINE fliogstc &
 & (f_i,x_axis,x_axis_2d,y_axis,y_axis_2d,z_axis, &
 &      t_axis,t_init,t_step,t_calendar, &
 &      x_start,x_count,y_start,y_count, &
 &      z_start,z_count,t_start,t_count)
!---------------------------------------------------------------------
  IMPLICIT NONE
!-
  INTEGER,INTENT(IN) :: f_i
  REAL,DIMENSION(:),OPTIONAL,INTENT(OUT)    :: x_axis,y_axis
  REAL,DIMENSION(:,:),OPTIONAL,INTENT(OUT)  :: x_axis_2d,y_axis_2d
  REAL,DIMENSION(:),OPTIONAL,INTENT(OUT)    :: z_axis
  INTEGER,DIMENSION(:),OPTIONAL,INTENT(OUT) :: t_axis
  REAL,OPTIONAL,INTENT(OUT)                 :: t_init,t_step
  CHARACTER(LEN=*),OPTIONAL,INTENT(OUT)     :: t_calendar
  INTEGER,OPTIONAL,INTENT(IN) :: &
 &  x_start,x_count,y_start,y_count,z_start,z_count,t_start,t_count
!-

  INTEGER :: m_x,i_x,l_x,m_y,i_y,l_y,m_z,i_z,l_z,m_t,i_t,l_t
 INTEGER :: i_rc, f_e, i_v, nbdim

!---------------------------------------------------------------------
!-
! Extracting the x coordinate, if needed
!-
  IF (PRESENT(x_axis).OR.PRESENT(x_axis_2d)) THEN

    IF (i_v > 0) THEN
      IF      (nbdim == 1) THEN
        IF (PRESENT(x_axis)) THEN
          i_rc = NF90_GET_VAR(f_e,i_v,x_axis, &
 &                 start=(/i_x/),count=(/l_x/))
        ELSE
        ENDIF
      ELSE IF (nbdim == 2) THEN

      ENDIF


    ENDIF
  ENDIF
!-
! Extracting the y coordinate, if needed

END SUBROUTINE fliogstc
!===
!===
END MODULE fliocom
