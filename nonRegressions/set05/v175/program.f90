MODULE BO
       INTERFACE SWAPINTERFACE1
              MODULE PROCEDURE SWAP_R, SWAP_I
       END INTERFACE

       INTERFACE SWAPINTERFACE2
              MODULE PROCEDURE SWAP_D, SWAP_C
       END INTERFACE
       real::inutile

CONTAINS

       SUBROUTINE SWAP_R(A, B)
       IMPLICIT NONE
       REAL, INTENT (INOUT)                 :: A, B
       REAL                                 :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_R

       SUBROUTINE SWAP_I(A, B)
       IMPLICIT NONE
       INTEGER, INTENT (INOUT)              :: A, B
       INTEGER                              :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_I

       SUBROUTINE SWAP_C(A, B)
       IMPLICIT NONE
       CHARACTER, INTENT (INOUT)            :: A, B
       CHARACTER                            :: TEMP
                  TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_C

       SUBROUTINE SWAP_D(A, B)
       IMPLICIT NONE
       double precision, INTENT (INOUT)                 :: A, B
       double precision                                 :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_D
END MODULE BO

module DO
use BO, only : swap1 => swapinterface1, swap2 => swapinterface2

contains

      subroutine sub0(u,v,x,y)
      common /zz/ zn,zd
      double precision u,v,z1,z2,zn,zd
      real x,y
      call swap2(u,v)
      call swap1(x,y)
      zn = z1 - z2
      zd = 1 + z1 + z2 + v - u + x -y
      return
      end subroutine

end module DO
