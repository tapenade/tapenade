      real function g(t)
      real t
      real a, matrix
      dimension a(100)
      dimension matrix(100)
         a = (/ (matrix(0.0), i = 1, 100) /)
      g = 1
      if (t .ne. 0.d0) then
        g = sin(t) / t
      endif
      return
      end
