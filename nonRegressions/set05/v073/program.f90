module test
  IMPLICIT NONE
  INTEGER,PARAMETER :: char_len=32
  INTEGER,PARAMETER :: int_kind=4
  INTEGER,PARAMETER :: R4=4
  INTEGER,PARAMETER :: R8=8

   interface broadcast_scalar
     module procedure broadcast_scalar_dbl, &
                      broadcast_scalar_real, &
                      broadcast_scalar_int, &
                      broadcast_scalar_char
   end interface

contains
  SUBROUTINE BROADCAST_SCALAR_DBL(scalar, root_pe)
    IMPLICIT NONE
    INTEGER(INT_KIND) :: root_pe
    REAL(R8) :: scalar
  END SUBROUTINE BROADCAST_SCALAR_DBL
  SUBROUTINE BROADCAST_SCALAR_REAL(scalar, root_pe)
    IMPLICIT NONE
    INTEGER(INT_KIND) :: root_pe
    REAL(R4) :: scalar
  END SUBROUTINE BROADCAST_SCALAR_REAL
  SUBROUTINE BROADCAST_SCALAR_INT(scalar, root_pe)
    IMPLICIT NONE
    INTEGER(INT_KIND) :: root_pe
    INTEGER(INT_KIND) :: scalar
  END SUBROUTINE BROADCAST_SCALAR_INT
  SUBROUTINE BROADCAST_SCALAR_CHAR(scalar, root_pe)
    IMPLICIT NONE
    INTEGER(INT_KIND) :: root_pe
    CHARACTER(len=char_len) :: scalar
  END SUBROUTINE BROADCAST_SCALAR_CHAR

end module

module run
use test
contains
subroutine s(init_ts_option, x)
  implicit none
  character (char_len) :: init_ts_option
  REAL(R4) :: x
! INTEGER(INT_KIND) :: master_task

   call broadcast_scalar(init_ts_option , master_task)
   call broadcast_scalar(x , master_task)

end subroutine s
end module
