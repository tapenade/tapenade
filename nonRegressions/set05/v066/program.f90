module test
  interface func
     module procedure func1, func2, func3
  end interface
contains
     subroutine func1(mb)
        real, dimension(10,20) ::  mb
        mb(1,1) = mb(1,1) * mb(2,2)
     end subroutine func1

     subroutine func2(mb)
        real, dimension(10,40) ::  mb
        mb(1,1) = mb(1,1) * mb(2,2)
     end subroutine func2

     subroutine func3(mb)
        real, dimension(10,60) ::  mb
        mb(1,1) = mb(1,1) * mb(2,2)
     end subroutine func3
end module

module run
use test
integer, parameter :: j1 = 20
contains
subroutine s(mb1, mb2, mb3, mb4)
real, dimension(10,j1) ::  mb1
real, dimension(10,40) ::  mb2
real, dimension(10,50) ::  mb3
real, dimension(10,70) ::  mb4
call func(mb1)
call func(mb2)
call func(mb3)
call func(mb4)
end subroutine s
end module
