      PROGRAM burgers
! code direct de l'�quation de Burgers
            
	IMPLICIT NONE
! d�claration des variables      
	REAL 			dx		! pas d'espace
	REAL 			dt       	! pas de temps
	REAL			u0		! cindition initiale
	REAL			tfin
	REAL			v		!viscosit�
	INTEGER 		n, nmax   	!indice temporelle
	INTEGER		j, jmax	!indice d'espace	
	DOUBLE PRECISION 	u1,u2		!vecteurs vitesse au temps t et au temps t+1
	ALLOCATABLE u1(:),u2(:)

	WRITE(*,*)'Entrer le nombre de points de grille'
	READ(*,*) jmax
	WRITE(*,*)'Entrer le temps de simulation'
	READ(*,*) tfin
	WRITE(*,*)"Entrer le pas d'espace"
	READ(*,*) dx
	WRITE(*,*)'Entrer le pas de temps de simulation'
	READ(*,*) dt
	WRITE(*,*)'Entrer la valeur de la viscosit�'
	READ(*,*) v
	WRITE(*,*)'Entrer la vitesse initiale'
	READ(*,*) u0


	nmax=tfin/dt

! initialisation des matrices
	ALLOCATE(u1(jmax),u2(jmax))
	DO j=1,jmax
		u1(j)=u0
		u2(j)=0.
	ENDDO

	OPEN(10,file='vitesse.txt')

	DO n=1,nmax			!boucle en temps
		CALL EQUATION(u1,u2,dx,dt,v,jmax)
		WRITE(10,*) 't=', dt*n
		DO j=1,jmax
			u1(j)=u2(j)
			WRITE(10,*) u1(j)
		ENDDO	
	ENDDO	
	CLOSE(10)

	END PROGRAM burgers		




	SUBROUTINE EQUATION(u1,u2,dx,dt,v,jmax)
	IMPLICIT NONE
	REAL 			dx		! pas d'espace
	REAL 			dt       	! pas de temps
	REAL			v		!viscosit�
	INTEGER		j, jmax	!indice d'espace	
	DOUBLE PRECISION 	u1(jmax),u2(jmax)		!vecteurs vitesse au temps t et au temps t+1
	
	DO j=2,jmax-1		!boucle en espace
			u2(j)=u1(j)-dt/(2.*dx)*((u1(j))**2.-(u1(j-1))**2.)+v*dt/(dx*dx)* &
     &							       (u1(j+1)-2*u1(j)+u1(j-1))
			u2(1)=0.
			u2(jmax)=0.
	ENDDO
	END SUBROUTINE EQUATION
