subroutine test(a,b,x,y)
integer, parameter ::  n=200
integer, parameter ::  m=300
real, dimension (n,m) :: x, y
forall(i = 1:n, j = 1:m, y(i,j) /= 0.0)  x(j,i) = 1.0 / y(i,j)
end subroutine test
