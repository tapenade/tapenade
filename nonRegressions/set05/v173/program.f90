!! pour tester les pushpop dans un where
subroutine test1(a,b,c,x,t)
real, dimension (200) :: a
real, dimension (200) :: b
real, dimension (200) :: c
real, dimension (100) :: t
real :: x
where (b(:) > 0) 
        c(:) = c(:) + sum(t(:), t(:)>0)
        a(:) = b(:) + x
        b(:) = 2*b(:) -8
end where
end subroutine test1

program test
real, dimension(200) :: a,b,c
real :: x
real, dimension(100) :: t
t(1)=1
t(2)=-2
t(3)=3
b(1)=0
b(2)=4
b(3)=6
print*,'xx    t     ', t
print*,'xx    b     ', b
c(:)=0.0
x = 5
call test1(a,b,c,x,t)
print*,'xx    c     ', c
end program test
