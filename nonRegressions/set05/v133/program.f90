      subroutine test(t,a,b)
      double precision, dimension (5) :: t,a,b
      where (t(:) > 0)
         a(:) = b(:)*t(:)
         t(:) = -t(:)
      end where
      return
      end
