! from examplesF90/REFERENCES/vmp11

MODULE definition
  INTEGER, PARAMETER :: wp = SELECTED_REAL_KIND(10,50)
END MODULE definition

MODULE rk
  USE definition
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: wp
  REAL(kind=wp) :: t
END MODULE rk
