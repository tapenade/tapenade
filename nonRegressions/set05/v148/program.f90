SUBROUTINE p(rho, ec, order, value, x)
    IMPLICIT NONE

    REAL :: rho, ec, x, value
    INTEGER :: order
    optional order, value
    if (order .eq. 0) then
       rho = rho * value * ec  + x
    else
       rho = rho * value / ec + x
    endif
    value = rho + value
END SUBROUTINE p

SUBROUTINE test(r,e)
  real :: r, e, y, v
  integer :: i(2)
 interface
   SUBROUTINE p(rho, ec, order, value, x)
    REAL :: rho, ec, x, value
    INTEGER :: order
    optional order, value
   END SUBROUTINE p
 end interface
  call p(r, ec=e, x=r)
  e = e + r
END SUBROUTINE test
