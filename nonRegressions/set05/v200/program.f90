  subroutine test(comm, y_start)
    TYPE rk_comm_real_1d
      REAL(kind=8), DIMENSION(:), POINTER :: ymax
    END TYPE rk_comm_real_1d

    REAL(kind=8), DIMENSION(:), INTENT(in) :: y_start 
    TYPE(rk_comm_real_1d) :: comm
    comm%ymax = ABS(y_start) 
  end subroutine test
