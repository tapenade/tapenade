     subroutine func1(mb)
        real, dimension(10,20) ::  mb
        mb(:,:) = MAX( 0.0, mb(:,:) )
        mb(1,1) = max (1.0, mb(1,1))
     end subroutine func1

     subroutine func2(mb)
        real, dimension(10,20) ::  mb
        mb(:,:) = MIN( 0.0, mb(:,:) )
        mb(1,1) = min (1.0, mb(1,1))
     end subroutine func2

     subroutine func3(mb)
        real, dimension(10,20) ::  mb
        mb(:,:) = ABS( mb(:,:) )
        mb(1,1) = ABS( mb(1,1) )
     end subroutine func3
