
	   module mcrm2par

	!  #########################################################
	!  # this module is used to store parameters for mcrm2.
	!  #
	!  # author: QIN Jun
	!  # time : Dec 17 2005
	!  # place: UMD USA
	!  #########################################################

         implicit none

	     integer*4,parameter:: nknots =9,  &
	                           nsunmax=9,  &
						       nangls =18, &
							   ncub   =48

	     integer*4,parameter:: nspchnl= 2001, &
	                           ncomp0 = 10

	     real*8,parameter:: wlstep = 1.0_8

	     real*8,dimension(1:nspchnl,1:ncomp0):: acoef1, &
	                                            acoef2

	     real*8,dimension(1:nspchnl,1:4):: phis

	     real*8,dimension(1:nspchnl):: refr, &
	                                   alpl, &
									   gamml

	     real*8::pi,dr



	   end module mcrm2par
