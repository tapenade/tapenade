subroutine test4(a,b,c)
    integer, parameter ::  n=200
    real, dimension (n) :: a
    real, dimension (n) :: b
    real, dimension (n) :: c
    forall(i = 1:100) 
       a(i) = 2 * b(i)
       c(i) = 3 * a(i+1)
    end forall
end subroutine test4
