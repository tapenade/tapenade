!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4912M) - 30 Jul 2013 16:45
!
!  Differentiation of test4 in reverse (adjoint) mode:
!   gradient     of useful results: a b c
!   with respect to varying inputs: a b c
!   RW status of diff variables: a:in-out b:incr c:in-out
SUBROUTINE TEST4_B(a, ab, b, bb, c, cb)
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=200
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: ab
  REAL, DIMENSION(n) :: b
  REAL, DIMENSION(n) :: bb
  REAL, DIMENSION(n) :: c
  REAL, DIMENSION(n) :: cb
  INTEGER :: i
  FORALL (i=1:100) 
    ab(i+1) = ab(i+1) + 3*cb(i)
    cb(i) = 0.0
    bb(i) = bb(i) + 2*ab(i)
    ab(i) = 0.0
  END FORALL
END SUBROUTINE TEST4_B

