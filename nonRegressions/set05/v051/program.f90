subroutine f(a,b,c,phalf,p_surface,dim_z,gridX,gridY,x,y)
 INTEGER dim_z
 DOUBLE PRECISION Phalf(0:DIM_Z)
 DOUBLE PRECISION A(DIM_Z + 1)
 DOUBLE PRECISION B(DIM_Z + 1)
 DOUBLE PRECISION C(DIM_Z + 1)
 DOUBLE PRECISION P_Surface(gridX,gridY)
 DOUBLE PRECISION x
 DOUBLE PRECISION y
 INTEGER gridX,gridY
 DO i = 1, gridX
    DO j = 1, gridY
     Phalf(:) =  A(:) + x * B(:) * P_Surface(i,j) * C(:)
    ENDDO
 ENDDO
end
