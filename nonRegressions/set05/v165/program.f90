subroutine test(x,y)
   implicit none
   real x(4)
   real y(4)
   y = x * 1;

assign_1:   where (x > 1)

assign_2:      where (x > 4)
                  y = x * 3;
               elsewhere
                  y = x * 4;
               end where assign_2
               y = x * 5;
            elsewhere (x > 2)
               y = x * 6;
            elsewhere (x > 3)
assign_3:      where (x > 40)
                  y = x * 30;
               elsewhere
                  y = x * 40;
               end where assign_3
               y = x * 7;
            elsewhere
               y = x * 8;
            end where assign_1
end subroutine test
