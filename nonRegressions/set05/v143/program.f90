      function F(t,u)
      real,dimension(3):: F,t,u
       F = t*u
      return
      end

      subroutine head(a,b,c,resu)
       real,dimension(3) :: a,b,c
       real,dimension(2):: resu
! dim =  0 2 argument is out of range
      resu = sum(F(a,2.0) * c, dim = 0, mask = c>0)
      resu = sum(F(a,2.0) * c, dim = 2, mask = c>0)
      return
      end
