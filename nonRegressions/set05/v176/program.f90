MODULE BO
       INTERFACE SWAPINTERFACE1
              MODULE PROCEDURE SWAP_R, SWAP_I
       END INTERFACE

       INTERFACE SWAPINTERFACE2
              MODULE PROCEDURE SWAP_D, SWAP_C
       END INTERFACE

CONTAINS

       SUBROUTINE SWAP_R(A, B)
       IMPLICIT NONE
       REAL, INTENT (INOUT)                 :: A, B
       REAL                                 :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_R

       SUBROUTINE SWAP_I(A, B)
       IMPLICIT NONE
       INTEGER, INTENT (INOUT)              :: A, B
       INTEGER                              :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_I

       SUBROUTINE SWAP_C(A, B)
       IMPLICIT NONE
       CHARACTER, INTENT (INOUT)            :: A, B
       CHARACTER                            :: TEMP
                  TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_C

       SUBROUTINE SWAP_D(A, B)
       IMPLICIT NONE
       double precision, INTENT (INOUT)                 :: A, B
       double precision                                 :: TEMP
               TEMP = A ; A = B ; B = TEMP
       END SUBROUTINE SWAP_D
END MODULE BO

module DO1
use BO, swap1 => swapinterface1, swap2 => swapinterface2

contains

      subroutine sub1(u,v,x,y)
      common /zz/ zn,zd
      double precision u,v,z1,z2,zn,zd
      real x,y
      call swap2(u,v)
      call swap1(x,y)
      zn = z1 - z2
      zd = 1 + z1 + z2 + v - u + x -y
      return
      end subroutine

end module DO1


module DO2
use BO, swap11 => swapinterface1, swap22 => swapinterface2

contains

      subroutine sub2(u,v,x,y)
      common /zz/ zn,zd
      double precision u,v,z1,z2,zn,zd
      real x,y
      call swap22(u,v)
      call swap11(x,y)
      zn = z1 - z2
      zd = 1 + z1 + z2 + v - u + x -y
      return
      end subroutine

end module DO2


module TOP
use do1
use do2

contains
subroutine head(u,v,x,y)
double precision u,v
real x,y
call sub1(u,v,x,y)
call sub2(u,v,x,y)
end subroutine

end module TOP
