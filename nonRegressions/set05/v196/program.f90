       RECURSIVE PURE FUNCTION FACTORIAL(N) RESULT (FAC_RESULT)
       IMPLICIT NONE
       double precision, INTENT(IN)      :: N
       double precision               :: FAC_RESULT
       IF ( N <=1 ) THEN
               FAC_RESULT = 1
       ELSE
               FAC_RESULT = int(N) * FACTORIAL(N-1)
       END IF
       END FUNCTION FACTORIAL
