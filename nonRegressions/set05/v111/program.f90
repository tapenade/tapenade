      subroutine sub1(x,a)
      double precision, dimension (4,6) :: x,a
      x(:,:) = sum(a(:,:) * a(:,:) * x(:,:), mask = a>0)
      return
      end
