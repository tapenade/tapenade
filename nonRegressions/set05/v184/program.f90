SUBROUTINE EOS_INSITU_POT(zws, psal, jpi, jpj, jpk)
  IMPLICIT NONE
  INTEGER :: jpi, jpj, jpk
  REAL, DIMENSION(jpi, jpj, jpk), INTENT(IN) :: psal
  REAL, DIMENSION(jpi, jpj, jpk), INTENT(OUT) :: zws
  REAL :: abs1(jpi, jpj, jpk)
  INTRINSIC ABS, SQRT
  WHERE (psal(:, :, :) .GE. 0.0) 
    abs1 = psal(:, :, :) + sum(psal(:,:,:))
  ELSEWHERE
    abs1 = -psal(:, :, :) + sum(zws(:,:,:))
  END WHERE
  zws(:, :, :) = SQRT(abs1)
END SUBROUTINE EOS_INSITU_POT

