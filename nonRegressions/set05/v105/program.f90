FUNCTION outerand(a,b)
IMPLICIT NONE
LOGICAL, DIMENSION(:), INTENT(IN) :: a,b
LOGICAL, DIMENSION(size(a),size(b)) :: outerand
outerand = spread(a,dim=2,ncopies=size(b)) .and. &
           spread(b,dim=1,ncopies=size(a))
END FUNCTION outerand
