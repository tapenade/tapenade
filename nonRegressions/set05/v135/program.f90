      subroutine sub1(x,a,b)
      double precision x
      double precision, dimension (5) :: a
      double precision, dimension (3) :: b
      x = sum(a(:), mask = b>0)
      return
      end
