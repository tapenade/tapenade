!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:15
!
SUBROUTINE SUB1(x, a, b)
  IMPLICIT NONE
  DOUBLE PRECISION :: x
  DOUBLE PRECISION, DIMENSION(5) :: a
  DOUBLE PRECISION, DIMENSION(3) :: b
  INTRINSIC SUM
  x = SUM(a(:), mask=b.GT.0)
  RETURN
END SUBROUTINE SUB1

