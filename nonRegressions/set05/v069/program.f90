module test
  interface func
     module procedure func1, func2, func3
  end interface
contains
     subroutine func1(mb)
        real, dimension(3) ::  mb
        mb(1) = mb(1) * mb(2)
        print*,'func1 '
        print*,mb
     end subroutine func1

     elemental subroutine func2(mb)
        real ::  mb
        mb = mb * mb * 2
        print*,'func2 '
        print*,mb
     end subroutine func2

     elemental subroutine func3(mb)
        real*8 ::  mb
        mb = mb * 3
        print*,'func3 '
        print*,mb
     end subroutine func3

end module

module run
use test
contains
subroutine s(mb1, mb2, mb3)
real, dimension(3) ::  mb1
real ::  mb2
real*8, dimension(3) :: mb3
call func(mb1)
call func(mb2)
call func(mb3)
call func(mb3(1))
end subroutine s
end module

program test2
use run
real, dimension(3) :: m1
real :: m2
real*8, dimension(3) :: m3
m1(1) = 2.0
m1(2) = 4.0
m2 = 3.0
m3(1) = 1.0
m3(2) = 2.0
m3(3) = 3.0
call s(m1, m2, m3)
end program test2
