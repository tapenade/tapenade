elemental real function twice_real(x) result(value)
real, intent(in) :: x 
value = 2*x
end function twice_real
