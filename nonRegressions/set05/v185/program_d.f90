!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_fortranparser) - 12 Mar 2020 15:05
!
MODULE MODULE1_D
  IMPLICIT NONE
  PUBLIC :: toto
  PUBLIC :: toto_d

CONTAINS
!  Differentiation of toto in forward (tangent) mode:
!   variations   of useful results: c
!   with respect to varying inputs: a b
  SUBROUTINE TOTO_D(a, ad, b, bd, c, cd)
    IMPLICIT NONE
    REAL :: a, b, c
    REAL :: ad, bd, cd
    cd = ad + bd
    c = a + b
  END SUBROUTINE TOTO_D

  SUBROUTINE TOTO(a, b, c)
    IMPLICIT NONE
    REAL :: a, b, c
    c = a + b
  END SUBROUTINE TOTO

END MODULE MODULE1_D

MODULE MODULE2_D
  USE MODULE1_D
  IMPLICIT NONE
  PUBLIC :: tata
  PUBLIC :: tata_d
! tres important ce private 
! pour cacher toto pour les 
! modules qui importent module2
  PRIVATE 

CONTAINS
!  Differentiation of tata in forward (tangent) mode:
!   variations   of useful results: z
!   with respect to varying inputs: x y
  SUBROUTINE TATA_D(x, xd, y, yd, z, zd)
    IMPLICIT NONE
    REAL :: x, y, z
    REAL :: xd, yd, zd
    CALL TOTO_D(x, xd, y, yd, z, zd)
  END SUBROUTINE TATA_D

  SUBROUTINE TATA(x, y, z)
    IMPLICIT NONE
    REAL :: x, y, z
    CALL TOTO(x, y, z)
  END SUBROUTINE TATA

END MODULE MODULE2_D

MODULE MODULE3_D
  USE MODULE2_D
  IMPLICIT NONE

CONTAINS
!  Differentiation of titi in forward (tangent) mode:
!   variations   of useful results: z a b c
!   with respect to varying inputs: x y a b c
!   RW status of diff variables: x:in y:in z:out a:in-out b:in-out
!                c:in-out
  SUBROUTINE TITI_D(a, ad, b, bd, c, cd, x, xd, y, yd, z, zd)
    IMPLICIT NONE
    REAL :: a, b, c, x, y, z
    REAL :: ad, bd, cd, xd, yd, zd
    EXTERNAL TOTO
    EXTERNAL TOTO_D0
    CALL TOTO_D0(a, ad, b, bd, c, cd)
    CALL TATA_D(x, xd, y, yd, z, zd)
  END SUBROUTINE TITI_D

  SUBROUTINE TITI(a, b, c, x, y, z)
    IMPLICIT NONE
    REAL :: a, b, c, x, y, z
    EXTERNAL TOTO
    CALL TOTO(a, b, c)
    CALL TATA(x, y, z)
  END SUBROUTINE TITI

END MODULE MODULE3_D

SUBROUTINE TEST_CD(a, b, c, x, y, z)
  USE MODULE2_D
  IMPLICIT NONE
  REAL :: a, b, c, x, y, z
  EXTERNAL TOTO
  CALL TOTO(a, b, c)
  CALL TATA(x, y, z)
END SUBROUTINE TEST_CD

