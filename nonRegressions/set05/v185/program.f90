        MODULE module1
        implicit none
        public toto
	CONTAINS
	SUBROUTINE toto(a, b, c)
	real a, b, c
	c = a+b
	END SUBROUTINE 
        END MODULE

	
	MODULE module2
        use  module1
        implicit none
        PUBLIC tata
        ! tres important ce private 
        ! pour cacher toto pour les 
        ! modules qui importent module2
        private
	CONTAINS
	
	SUBROUTINE tata(x, y, z)
	real x, y, z
		call toto(x, y, z) 
	END SUBROUTINE 
	
        END MODULE

	
	MODULE module3
        USE module2
        implicit none
        CONTAINS
        SUBROUTINE titi(a, b, c, x, y, z)
	real a, b, c, x, y, z
               CALL toto(a, b, c)
	       CALL tata(x, y, z)
        END SUBROUTINE
        END MODULE
   
        SUBROUTINE test(a, b, c, x, y, z)
        use module2
	real a, b, c, x, y, z
               CALL toto(a, b, c)
	       CALL tata(x, y, z)
        END SUBROUTINE


program main
real a, b, c, x, y, z
a = 1.0
b = 2.0
c = 3.0
x = 4.0
y = 5.0
z = 6.0
call test(a,b,c,x,y,z)
write (*,*) a,b
write (*,*) c,x
write (*,*) y,z
end
