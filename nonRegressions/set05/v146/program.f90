module a
  integer, parameter :: wp = 2
  real(wp) :: epsil
  epsil = 1.d-7_wp
end module
