!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_fortranparser) - 12 Mar 2020 15:05
!
MODULE M
  IMPLICIT NONE
  REAL :: a, b
  PUBLIC :: a
  PRIVATE :: b
END MODULE M

SUBROUTINE TEST(u, v, w)
  IMPLICIT NONE
!     son points to Undef
!     a points to Undef
  REAL :: a, son, y
  ALLOCATABLE :: a(:, :)
  POINTER :: son
  TARGET :: a, y(10)
  REAL :: u, v, w
  INTENT(IN) :: u
  INTENT(OUT) :: v
  OPTIONAL :: w
END SUBROUTINE TEST

