!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 2.2.3 (r1922M) - 06/20/2007 10:41
!  
!! pour tester les pushpop dans un where
SUBROUTINE TEST1(a, b, x)
  IMPLICIT NONE
  REAL, DIMENSION(200) :: a
  REAL, DIMENSION(200) :: b
  REAL :: x
  WHERE (b(:) .GT. 0) 
    a(:) = b(:) + x
    b(:) = 2*b(:) - 8
  END WHERE
END SUBROUTINE TEST1

