      MODULE INCLUDE_MODULE

        IMPLICIT NONE
!
!...... DERIVED DATA TYPES
        TYPE :: NODE_TYPE
           REAL                      :: X
           REAL                      :: Y
           REAL                      :: RHO
           REAL                      :: RHOU
           REAL                      :: RHOV
           REAL                      :: RHOE
           REAL                      :: P
           REAL                      :: U
           REAL                      :: V
           REAL                      :: ENTROPY
           REAL                      :: ENTHALPY
        END TYPE NODE_TYPE

        TYPE :: EDGE_TYPE
           REAL                      :: DX
           REAL                      :: DY
           REAL                      :: NX
           REAL                      :: NY
           TYPE (NODE_TYPE), POINTER :: GRID_NODE_1
           TYPE (NODE_TYPE), POINTER :: GRID_NODE_2
        END TYPE EDGE_TYPE

        TYPE :: CELL_TYPE
           REAL                      :: DT
           REAL                      :: AREA
           TYPE (EDGE_LIST), POINTER :: FIRST_EDGE
           TYPE (NODE_LIST), POINTER :: FIRST_NODE
        END TYPE CELL_TYPE

        TYPE :: NODE_LIST
           TYPE (NODE_TYPE), POINTER :: GRID_NODE
           TYPE (NODE_LIST), POINTER :: NEXT_NODE
        END TYPE NODE_LIST

        TYPE :: EDGE_LIST
           TYPE (EDGE), POINTER      :: GRID_EDGE
           TYPE (EDGE_LIST), POINTER :: NEXT_EDGE
        END TYPE EDGE_LIST
!
!...... DECLARE VARIABLES
        TYPE (CELL_TYPE), DIMENSION (:), ALLOCATABLE, TARGET :: CELL
        TYPE (NODE_TYPE), DIMENSION (:), ALLOCATABLE, TARGET :: GRID
        INTEGER                                              :: N_CELLS
        INTEGER                                              :: N_GRID_POINTS
        REAL                                                 :: AREA

      CONTAINS
!
!...... 
        FUNCTION NEW_CELL () RESULT (NEW_RESULT)
          TYPE (CELL_TYPE) :: NEW_RESULT
          NULLIFY (NEW_RESULT % FIRST_NODE)
        END FUNCTION NEW_CELL

        RECURSIVE SUBROUTINE ADD_GRID_POINT (X, GRID)
          TYPE (NODE_LIST), POINTER :: X
          TYPE (NODE_TYPE), TARGET :: GRID
          
          IF (ASSOCIATED(X)) THEN
             CALL ADD_GRID_POINT(X % NEXT_NODE, GRID)
             RETURN
          ELSE
             ALLOCATE(X)
             X % GRID_NODE => GRID
             NULLIFY (X % NEXT_NODE)
          END IF
        END SUBROUTINE ADD_GRID_POINT
        
        FUNCTION CELL_AREA (X) RESULT (AREA)
          TYPE (CELL_TYPE)          :: X
          REAL                      :: AREA
          AREA = 0.0
          AREA = D_AREA(X % FIRST_NODE)
          write(*,*)
        END FUNCTION CELL_AREA
        
        RECURSIVE FUNCTION D_AREA (W) RESULT (D_AREA_RESULT)
          TYPE (NODE_LIST) :: W
          REAL :: DX,DY,XBAR,YBAR
          REAL    :: D_AREA_RESULT

          IF (.NOT. ASSOCIATED(W % NEXT_NODE)) THEN
             D_AREA_RESULT = 0.0
             RETURN
          ELSE
             DX   = W % NEXT_NODE % GRID_NODE % X - W % GRID_NODE % X
             DY   = W % NEXT_NODE % GRID_NODE % Y - W % GRID_NODE % Y
             XBAR = 0.5*(W % NEXT_NODE % GRID_NODE% X + W % GRID_NODE% X)
             YBAR = 0.5*(W % NEXT_NODE % GRID_NODE% Y + W % GRID_NODE% Y)
             D_AREA_RESULT = 0.5*(XBAR*DY - YBAR*DX) + D_AREA(W % NEXT_NODE)
          END IF
        END FUNCTION D_AREA

      END MODULE INCLUDE_MODULE

      PROGRAM MAIN
        USE INCLUDE_MODULE
        IMPLICIT NONE
        INTEGER :: II
        CALL GENERATE_GRID
        CALL COMPUTE_CELL_AREA
        DO II=1,N_GRID_POINTS
           WRITE(*,*) GRID(II) % X,GRID(II) % Y
        END  DO
      END PROGRAM MAIN
      
      SUBROUTINE GENERATE_GRID        
        USE INCLUDE_MODULE        
!
!...... GENERATE GRID
        IMAX = 21
        JMAX = 11
        ALLOCATE (GRID(IMAX*JMAX))
        II = 0
        DO I=1,IMAX
           DO J=1,JMAX
              II = II + 1
              WRITE(*,*) II
              GRID(II) % X = (I - 1.0)/(IMAX - 1.0)
              GRID(II) % Y = (J - 1.0)/(JMAX - 1.0)
           END DO
        END DO
        N_GRID_POINTS = II
!
!...... CONNECT GRID POINTS INTO CELLS
        ALLOCATE (CELL((IMAX-1)*(JMAX-1)))
        II = 0
        DO J=1,JMAX-1
           DO I=1,IMAX-1
              II = II + 1
              I1 = (I - 1)*JMAX + J
              I2 = (I    )*JMAX + J
              I3 = (I    )*JMAX + J + 1
              I4 = (I - 1)*JMAX + J + 1
              CALL ADD_GRID_POINT (CELL(II) % FIRST_NODE, GRID(I1))
              CALL ADD_GRID_POINT (CELL(II) % FIRST_NODE, GRID(I2))
              CALL ADD_GRID_POINT (CELL(II) % FIRST_NODE, GRID(I3))
              CALL ADD_GRID_POINT (CELL(II) % FIRST_NODE, GRID(I4))
              CALL ADD_GRID_POINT (CELL(II) % FIRST_NODE, GRID(I1))
           END DO
        END DO
        N_CELLS = II
!
!.... END OF SUBROUTINE
      END SUBROUTINE GENERATE_GRID

      SUBROUTINE COMPUTE_CELL_AREA
        USE INCLUDE_MODULE
        IMPLICIT NONE
        INTEGER :: II
        DO II = 1,N_CELLS
           CELL(II)%AREA = CELL_AREA(CELL(II))
           write(*,*) cell(ii)%area
        END DO
        AREA = 0.0
        DO II=1,N_CELLS
           AREA = AREA + CELL(II)%AREA
        END DO
      END SUBROUTINE COMPUTE_CELL_AREA
