!! pour tester les pushpop dans un where
subroutine test1(a,b,c)
real, dimension (200) :: a
real, dimension (200) :: b
real, dimension (200) :: c
where (a(:) > b(:))
   b(:) = b(:) * a(:)
   a(:) = b(:) * c(:)
   c(:) = b(:) * a(:)
end where
end subroutine test1

