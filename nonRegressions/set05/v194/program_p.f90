!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 2.2.3 (r2101M) - 09/14/2007 11:29
!  
SUBROUTINE HEAD(a, b, c, t, u, x)
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=200
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: b
  REAL, DIMENSION(n) :: c
  REAL, DIMENSION(n) :: t
  REAL, DIMENSION(n, n) :: u
  REAL :: x
  CALL TEST1(a, b)
  CALL TEST2(a, b, c, t)
  CALL TEST3(a, b, u)
  CALL TEST4(a, b, c, t, x)
  CALL TEST5(a, b, c)
END SUBROUTINE HEAD

SUBROUTINE TEST1(a, b)
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=200
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: b
  INTEGER :: i
  FORALL (i=1:100:2)  a(i) = 2*b(i)
END SUBROUTINE TEST1

SUBROUTINE TEST2(a, b, c, t)
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=200
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: b
  REAL, DIMENSION(n) :: c
  REAL, DIMENSION(n) :: t
  INTEGER :: i
  FORALL (i=1:100:2,t(i) .GT. 0) 
    a(i) = 2*b(i)
    c(i+1) = 3*a(i)
  END FORALL
END SUBROUTINE TEST2

SUBROUTINE TEST3(a, b, t)
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=200
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: b
  REAL, DIMENSION(n, n) :: t
  INTEGER :: i
  INTEGER :: j
  FORALL (i=1:100) 
    a(i) = 2*b(i)
    FORALL (j=1:50)  t(i, j) = j*a(i)
  END FORALL
END SUBROUTINE TEST3

SUBROUTINE TEST4(a, b, c, t, x)
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=200
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: b
  REAL, DIMENSION(n) :: c
  REAL, DIMENSION(n) :: t
  REAL :: x
  INTEGER :: i
  FORALL (i=1:100,t(i) .GT. 0)  a(i) = a(i) + x
END SUBROUTINE TEST4

SUBROUTINE TEST5(a, b, c)
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=200
  REAL, DIMENSION(n) :: a
  REAL, DIMENSION(n) :: b
  REAL, DIMENSION(n) :: c
  INTEGER :: i
  FORALL (i=1:100) 
    a(i) = 2*b(i)
    c(i) = 2*b(i+2)
  END FORALL
END SUBROUTINE TEST5

