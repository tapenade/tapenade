subroutine test1(a,b)
    integer, parameter ::  n=200
    real, dimension (n) :: a
    real, dimension (n) :: b
    forall(i = 1:100:2) 
       a(i) = 2 * b(i)
    end forall
end subroutine test1

subroutine test2(a,b,c,t)
    integer, parameter ::  n=200
    real, dimension (n) :: a
    real, dimension (n) :: b
    real, dimension (n) :: c
    real, dimension (n) :: t
    forall(i = 1:100:2, t(i)>0) 
       a(i) = 2 * b(i)
       c(i+1) = 3 * a(i)
    end forall
end subroutine test2

subroutine test3(a,b,t)
    integer, parameter ::  n=200
    real, dimension (n) :: a
    real, dimension (n) :: b
    real, dimension (n,n) :: t
    forall(i = 1:100) 
       a(i) = 2 * b(i)
       forall(j = 1:50)
          t(i,j) = j * a(i)
       end forall
    end forall
end subroutine test3

subroutine test4(a,b,c,t,x)
    integer, parameter ::  n=200
    real, dimension (n) :: a
    real, dimension (n) :: b
    real, dimension (n) :: c
    real, dimension (n) :: t
    real x
    forall(i = 1:100, t(i) > 0) 
       a(i) = a(i) + x
    end forall
end subroutine test4

subroutine test5(a,b,c)
    integer, parameter ::  n=200
    real, dimension (n) :: a
    real, dimension (n) :: b
    real, dimension (n) :: c
    forall(i = 1:100) 
       a(i) = 2 * b(i)
       c(i) = 2 * b(i+2)
    end forall
end subroutine test5

subroutine head(a,b,c,t,u,x)
    integer, parameter ::  n=200
    real, dimension (n) :: a
    real, dimension (n) :: b
    real, dimension (n) :: c
    real, dimension (n) :: t
    real, dimension (n,n) :: u
    real :: x
    call test1(a,b)
    call test2(a,b,c,t)
    call test3(a,b,u)
    call test4(a,b,c,t,x)
    call test5(a,b,c)
end subroutine head
