!! pour tester les pushpop dans du vectoriel
subroutine test1(a,b,c)
real, dimension (200) :: a
real, dimension (200) :: b
real, dimension (200) :: c
b(:) = b(:) * a(:)
a(:) = b(:) * c(:)
c(:) = b(:) * a(:)
end subroutine test1

