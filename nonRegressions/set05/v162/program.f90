! exemple Metcalf page 110 fortran95/2003 explained
subroutine test(x,y)
   implicit none
   real x(4)
   real y(4)
   y = x + 1;

assign_1:   where (x > 1)
               y = x + 2;     
               y = x + 3;
            elsewhere (x > 2)
               y = x + 4;
assign_2:      where (x > 4)
                  y = x + 5;
               elsewhere
                  y = x + 6;
               end where assign_2
            elsewhere (x > 3) assign_1
               y = x + 7;
            elsewhere assign_1
               y = x + 8;
            end where assign_1
end subroutine test
