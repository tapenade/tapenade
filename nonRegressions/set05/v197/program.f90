program test

  !-- program to test if tapenade can diff blocks

  !-- dp is the default double precision real
  integer, parameter :: dp = kind(1.d0)

  type Block
    integer :: jkmmax(3)
    real(kind=dp), allocatable :: &
         q(:,:,:,:), s(:,:,:,:)
  end type Block

  integer, parameter :: jmax = 5, kmax = 5, mmax = 5
  type(Block) :: blk

  blk%jkmmax(1) = jmax; blk%jkmmax(2) = kmax; blk%jkmmax(3) = mmax
  allocate(blk%q(jmax,kmax,mmax,5),blk%s(jmax,kmax,mmax,5))
  blk%q = 1.d0
  blk%s = 0.d0
  
  call testBlockFun(blk)

contains

  subroutine testBlockFun(blk)
    !-- takes each q, sums them up, and adds to s
    type(Block), intent(inout) :: blk
    

    integer :: j, k, m, di
    real(kind=dp) :: tmp

    do m = 1,blk%jkmmax(3)
      do k = 1,blk%jkmmax(2)
        do j = 1,blk%jkmmax(1)

          do di = 1,5
            tmp = tmp + blk%q(j,k,m,di)*blk%q(j,k,m,di)
          end do
          blk%s(j,k,m,1:5) = blk%s(j,k,m,1:5) + tmp

        end do
      end do
    end do

  end subroutine testBlockFun

end program test
