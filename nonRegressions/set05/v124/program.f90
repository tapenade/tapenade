! Programme faux: les champs surface%vx et %vy n'existent pas !!
MODULE INTERVAL_ARITHMETICS
       TYPE INTERVAL
              REAL LOWER, UPPER
       END TYPE INTERVAL

       TYPE BOX
          TYPE(INTERVAL) :: INTVX, INTVY
       END TYPE BOX

       TYPE (INTERVAL) :: X,Y

       TYPE(BOX) :: B1

END MODULE INTERVAL_ARITHMETICS

function surface(bintvxlower, bintvxupper, bintvylower, bintvyupper)
  use INTERVAL_ARITHMETICS
  type(box) :: surface
  bintvxlower = (bintvxupper - bintvxlower) * (bintvyupper - bintvylower)
  bintvxupper = (bintvxupper - bintvxlower) * (bintvyupper - bintvylower)
  surface%vx%lower = bintvxlower
  surface%vy%lower = bintvylower
  surface%vx%upper = bintvxupper
  surface%vy%upper = bintvyupper
end 

