!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 2.2.3 (r2101M) - 09/14/2007 11:29
!  
SUBROUTINE TEST(a)
  IMPLICIT NONE
! Same as FORALL (I=1:100,J=1:100,I.NE.J) A(I,J)=A(J,I)
  REAL :: a(100, 100)
  INTEGER :: i
  INTEGER :: j
outer:FORALL (i=1:100) 
inner:FORALL (j=1:100,i .NE. j)  a(i, j) = a(j, i)
  END FORALL outer
END SUBROUTINE TEST

