subroutine test(a)
    ! Same as FORALL (I=1:100,J=1:100,I.NE.J) A(I,J)=A(J,I)

    REAL A(100,100)
    OUTER: FORALL (I=1:100)
      INNER: FORALL (J=1:100,I.NE.J)
        A(I,J)=A(J,I)
      END FORALL INNER
    END FORALL OUTER
end subroutine test
