C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_profiling) -  9 Apr 2024 18:02
C
      SUBROUTINE HEAD(a, b, c)
      IMPLICIT NONE
      REAL a(10), b, c
      INTEGER i
      c = 1.0
      CALL LEAF(a, b, c)
      DO i=1,10
        c = c*a(i)
      ENDDO
      END

      SUBROUTINE LEAF(a, b, c)
      IMPLICIT NONE
      REAL a(10), b, c
      INTEGER i
      DO i=1,10
        a(i) = b
        c = c*a(i)
      ENDDO
      END
C
C

