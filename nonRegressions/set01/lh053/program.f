      subroutine cg12v4(Z,TK,GAMAI,V,W,G,TAU,NCMAX)
C
      implicit real*8 (A-H,O-Z)
      common/THJCOD/JCOD,NC,IETAT,IERTH,NOC,IERTH1(10)
      common/THCONS/CONST(5)
      dimension Z(NCMAX),GAMAI(NCMAX)
      dimension V(NCMAX),W(NCMAX)
      dimension G(NCMAX,NCMAX),TAU(NCMAX,NCMAX)
      equivalence (CONST(2),RCAL)
C
      NC1=NC-1
      RCALTK=RCAL*TK
      do  11  I=1,NC
      TAU(I,I)=0.
 11   G(I,I)=0.
      do  12  I=1,NC1
      J1=I+1
      do  12  J=J1,NC
      TAU(I,J)=BINAIR(I,J,1)/RCALTK
      TAU(J,I)=BINAIR(J,I,1)/RCALTK
      G(I,J)=BINAIR(I,J,2)
      G(J,I)=G(I,J)
 12   continue
      do  73  J=1,NC
      do  73  I=1,NC
 73   G(J,I)=EXP(-G(J,I)*TAU(J,I))
      do  75  I=1,NC
      V(I)=0.
      W(I)=0.
      do  76  J=1,NC
      ZG=Z(J)*G(J,I)
      V(I)=V(I)+ZG
 76   W(I)=W(I)+ZG*TAU(J,I)
      W(I)=W(I)/V(I)
 75   GAMAI(I)=W(I)
      do  77  I=1,NC
      do  78  J=1,NC
 78   GAMAI(I)=GAMAI(I)+Z(J)*G(I,J)*(TAU(I,J)-W(J))/V(J)
      GAMAI(I)=EXP(GAMAI(I))
 77   continue
C
      return
      end
