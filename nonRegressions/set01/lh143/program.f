      subroutine diffar(nubo,vnocl,pres,dm,coor,f1,f2,g1,g2,g3,
     &                  g4,t1,t2,t3,t4,ti1,ti2,ti3,ti4,rh1,rh2,rh3,rh4,
     &                  vn1,vn2,vn3,noe1,noe2,noe3,icola,
     &                  f3,f4,cson)
c    
c     ----------------------------------------------------------------
      dimension nubo(2,6000),vnocl(2,6000)
      dimension pres(*),dm(*),coor(2,2000),cson(*)
      dimension f1(2000),f2(2000),f3(2000),f4(2000)
      dimension g1(2000),g2(2000),g3(2000),g4(2000)
      dimension t1(2000),t2(2000),t3(2000),t4(2000)
      dimension ti1(2000),ti2(2000),ti3(2000),ti4(2000)
      dimension rh1(2000),rh2(2000),rh3(2000),rh4(2000)
      dimension vn1(2,1000),vn2(2,1000),vn3(2,1000)
      dimension noe1(*),noe2(*),noe3(*)
      dimension icola(0:nca)
      dimension d4(4),duor(4),duex(4)
c
      real gam1
      common/c3/nt,ns,nseg,nf1,nf2,nf3,nca,ncor,nf3ma,nsegns,ncans
      common/c4/gama,ugama,gam1,ugam1,gamg,ugamg
      common/c11/klim,ak2,ak4,alp

CDIR$ NEXTSCALAR
C$AD II-LOOP
      DO 21 IC=1,NCA
      NSG1 = ICOLA(IC-1)+1
      NSG2 = ICOLA(IC)
CDIR$ IVDEP
c C$AD II-LOOP
      DO 20 ISEG=NSG1,NSG2
      NUOR = NUBO(1,ISEG)
      NUEX = NUBO(2,ISEG)
      DPL = T1(NUOR)*(T1(NUEX)-T1(NUOR))
      TI4(NUOR) = TI4(NUOR)+DPL
      TI4(NUEX) = TI4(NUEX)-DPL
   20 CONTINUE
   21 CONTINUE
      COEF    = 1.0 


C   1. Do loop on the segments
C   --------------------------

c C$AD II-LOOP
      do 110 iseg=nsg1,nsg2
c
      nuor = nubo(1,iseg)
      nuex = nubo(2,iseg)
      dpl = (t1(nuex)-t1(nuor))*(ti1(nuor)-ti1(nuex))
      rh4(nuex) = rh4(nuex)-dpl
c
  110 continue

      return
      end
