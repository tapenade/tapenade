C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of loop2 in reverse (adjoint) mode:
C   gradient     of useful results: a b
C   with respect to varying inputs: a b
C   RW status of diff variables: a:in-out b:incr
      SUBROUTINE LOOP2_B(a, ab, b, bb)
      IMPLICIT NONE
      REAL a(10), b(10)
      REAL ab(10), bb(10)
      INTEGER i, n
      INTEGER ad_count
      INTEGER i0
C
      a(1) = 2*a(2)
      ad_count = 1
 100  a(3) = 4*a(4)
      IF (a(3) .GT. a(8)) THEN
        a(5) = 6*a(6)
        DO i=5,n
          a(i) = b(i)
        ENDDO
        ad_count = ad_count + 1
        GOTO 100
      END IF
      CALL PUSHINTEGER4(ad_count)
      ab(8) = ab(8) + 8*ab(7)
      ab(7) = 0.0
      CALL POPINTEGER4(ad_count)
      DO i0=1,ad_count
        IF (i0 .NE. 1) THEN
          DO i=n,5,-1
            bb(i) = bb(i) + ab(i)
            ab(i) = 0.0
          ENDDO
          ab(6) = ab(6) + 6*ab(5)
          ab(5) = 0.0
        END IF
        ab(4) = ab(4) + 4*ab(3)
        ab(3) = 0.0
      ENDDO
      ab(2) = ab(2) + 2*ab(1)
      ab(1) = 0.0
      END

