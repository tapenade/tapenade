C cet exemple implemente le cas z = 2/(z+1) ou dans le cas le plus general z = 2/(z+x)
C Dans le cas particulier la solution est 1. Donc si par hasard l'utilisateur donne un intial guess= solution finale =1 le nombre des iterations sera 1  

      Program test 
      real y
      real x
      real initial

      x=1.
      initial=24.
      y=0.0

      call root(x,y,initial)
     
      Print *, "La solution est y" ,y

      end 


      SUBROUTINE root(x,y, initial)

      Real z, oz,y
      Real x, initial
      Integer i
     
      z=initial
      oz = z + 1
      i=0

C     $AD FP-LOOP z
      DO WHILE ((z-oz)**2 .GE. 1.e-10) 
         oz = z
         z = z*z
         z = sin(z*x)
         i=i+1
      ENDDO

      y=z*x
      print *,  "nombre des iterations", i      
      END


      subroutine toto(z,x,oz)
      Real z, oz,x
      z = 2.0/(oz+x)
      End
      
