C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (vossbeck01) - 22 May 2024 15:31
C
C  Differentiation of top in reverse (adjoint) mode:
C   gradient     of useful results: x y
C   with respect to varying inputs: x y
C   RW status of diff variables: x:incr y:in-out
      SUBROUTINE TOP_B(x, xb, y, yb, n)
      IMPLICIT NONE
      REAL x, y
      REAL xb, yb
      INTEGER n
      INTRINSIC ABS
      REAL abs0
C
      SELECT CASE (n)
      CASE (0)
        xb = xb + 2*x*yb
        yb = 0.0
      CASE (1)
        xb = xb + y*2.0*yb
        yb = x*2.0*yb
      END SELECT
      END

