C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 30 Aug 2019 15:53
C
      DOUBLE PRECISION FUNCTION F(t)
      IMPLICIT NONE
      DOUBLE PRECISION t
      DOUBLE PRECISION aj
      DIMENSION aj(5)
      DOUBLE PRECISION toto
      COMMON /pandq/ aj, toto
      DOUBLE PRECISION ajd
      DIMENSION ajd(3)
      INTRINSIC EXP
      DOUBLE PRECISION G
      f = t*t
      f = EXP(f) + ajd(1) + toto + G(toto)
      RETURN
      END

C
      DOUBLE PRECISION FUNCTION G(t)
      IMPLICIT NONE
      DOUBLE PRECISION t
      REAL aj
      REAL toto
      COMMON /pandq/ aj, toto
      INTRINSIC SIN
      g = 1
      IF (t .NE. 0.d0) g = SIN(t)/aj(1)
      RETURN
      END

