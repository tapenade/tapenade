C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_nopubliczones) - 16 Feb 2022 18:30
C
C  Differentiation of f in forward (tangent) mode:
C   variations   of useful results: f
C   with respect to varying inputs: aj[0:0] aj[1:1] aj[2:9] toto
C                t
C   RW status of diff variables: aj[0:0]:in aj[1:1]:in aj[2:9]:in
C                toto:in f:out t:in
      DOUBLE PRECISION FUNCTION F_D(t, td, f)
      IMPLICIT NONE
      DOUBLE PRECISION t
      DOUBLE PRECISION td
      DOUBLE PRECISION aj
      DIMENSION aj(5)
      DOUBLE PRECISION ajd0
      DIMENSION ajd0(5)
      DOUBLE PRECISION toto
      COMMON /pandq/ aj, toto
      DOUBLE PRECISION totod
      COMMON /pandq_d/ ajd0, totod
      DOUBLE PRECISION ajd
      DIMENSION ajd(3)
      INTRINSIC EXP
      DOUBLE PRECISION G
      DOUBLE PRECISION G_D
      DOUBLE PRECISION result1
      DOUBLE PRECISION result1d
      DOUBLE PRECISION f
      f_d = 2*t*td
      f = t*t
      result1d = G_D(toto, totod, result1)
      f_d = EXP(f)*f_d + totod + result1d
      f = EXP(f) + ajd(1) + toto + result1
      RETURN
      END

C  Differentiation of g in forward (tangent) mode:
C   variations   of useful results: g
C   with respect to varying inputs: aj t
C
      DOUBLE PRECISION FUNCTION G_D(t, td, g)
      IMPLICIT NONE
      DOUBLE PRECISION t
      DOUBLE PRECISION td
      REAL aj
      REAL toto
      COMMON /pandq/ aj, toto
      REAL ajd
      REAL totod
      COMMON /pandq_d/ ajd, totod
      INTRINSIC SIN
      DOUBLE PRECISION temp
      DOUBLE PRECISION g
      g = 1
      IF (t .NE. 0.d0) THEN
        temp = SIN(t)/aj(1)
        g_d = (COS(t)*td-temp*ajd(1))/aj(1)
        g = temp
      ELSE
        g_d = 0.D0
      END IF
      RETURN
      END

