C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_nopubliczones) - 16 Feb 2022 18:30
C
C  Differentiation of f in forward (tangent) mode (with options multiDirectional):
C   variations   of useful results: f
C   with respect to varying inputs: aj[0:0] aj[1:1] aj[2:9] toto
C                t
C   RW status of diff variables: aj[0:0]:in aj[1:1]:in aj[2:9]:in
C                toto:in f:out t:in
      SUBROUTINE F_DV(t, td, f, fd, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      DOUBLE PRECISION t
      DOUBLE PRECISION td(nbdirsmax)
      DOUBLE PRECISION aj
      DIMENSION aj(5)
      DOUBLE PRECISION ajd0
      DIMENSION ajd0(nbdirsmax, 5)
      DOUBLE PRECISION toto
      COMMON /pandq/ aj, toto
      DOUBLE PRECISION totod(nbdirsmax)
      COMMON /pandq_dv/ ajd0, totod
      DOUBLE PRECISION ajd
      DIMENSION ajd(3)
      INTRINSIC EXP
      DOUBLE PRECISION G
      DOUBLE PRECISION result1
      DOUBLE PRECISION result1d(nbdirsmax)
      INTEGER nd
      DOUBLE PRECISION f
      DOUBLE PRECISION fd(nbdirsmax)
      INTEGER nbdirs
      f = t*t
      CALL G_DV(toto, totod, result1, result1d, nbdirs)
      DO nd=1,nbdirs
        fd(nd) = 2*t*td(nd)
        fd(nd) = EXP(f)*fd(nd) + totod(nd) + result1d(nd)
      ENDDO
      f = EXP(f) + ajd(1) + toto + result1
      RETURN
      END

C  Differentiation of g in forward (tangent) mode (with options multiDirectional):
C   variations   of useful results: g
C   with respect to varying inputs: aj t
C
      SUBROUTINE G_DV(t, td, g, gd, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      DOUBLE PRECISION t
      DOUBLE PRECISION td(nbdirsmax)
      REAL aj
      REAL toto
      COMMON /pandq/ aj, toto
      REAL ajd(nbdirsmax)
      REAL totod(nbdirsmax)
      COMMON /pandq_dv/ ajd, totod
      INTRINSIC SIN
      INTEGER nd
      DOUBLE PRECISION temp
      DOUBLE PRECISION g
      DOUBLE PRECISION gd(nbdirsmax)
      INTEGER nbdirs
      g = 1
      IF (t .NE. 0.d0) THEN
        temp = SIN(t)/aj(1)
        DO nd=1,nbdirs
          gd(nd) = (COS(t)*td(nd)-temp*ajd(nd, 1))/aj(1)
        ENDDO
        g = temp
      ELSE
        DO nd=1,nbdirsmax
          gd(nd) = 0.D0
        ENDDO
      END IF
      RETURN
      END

