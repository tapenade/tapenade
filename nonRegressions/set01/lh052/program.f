      double precision function f(t)
      implicit double precision (a-h,o-z)
      double precision t
      dimension aj(5)
      COMMON /pandq/aj, toto
      dimension ajd(3)
      f = t * t
      f = exp(f)  + ajd(1) +toto + g(toto)
      return
      end

      double precision function g(t)
      double precision t
      COMMON /pandq/aj, toto
      g = 1
      if (t .ne. 0.d0) then
        g = sin(t) / aj(1)
      endif
      return
      end
