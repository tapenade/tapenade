      SUBROUTINE H(A,B,C,r)
C To test regeneration of simple loop bounds in the
C reverse sweep.
      REAL A(100),B(100),C(100,100),r
      INTEGER i,j,k,jmax,imax,ifirst,jfirst
c
      DO i = 1,50
         A(i) = 2.0*A(i)
      ENDDO
      DO j = 1,jmax
         B(j) = 3.0*B(j)
      ENDDO
      DO j = 1,jmax
         DO i = j,imax
            C(i,j) = A(i)*B(j)
         ENDDO
      ENDDO
      DO i=ifirst,imax
         A(i) = 5.2*A(i)
      ENDDO
      DO j=jfirst,jmax
         B(j) = 3.0*B(j)
      ENDDO
      r = A(j)*B(j)*C(j,j)
      ifirst = jfirst
c
      END
