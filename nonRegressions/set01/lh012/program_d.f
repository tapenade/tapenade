C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 18 Apr 2019 18:10
C
C  Differentiation of test in forward (tangent) mode:
C   variations   of useful results: a
C   with respect to varying inputs: a b c
C   RW status of diff variables: a:in-out b:in c:in
C  ~/ali/llh/sources/topLevel/ali -classes -tl -head test -vars "T1 x" -o result program.f 
      SUBROUTINE TEST_D(a, ad, b, bd, c, cd)
      IMPLICIT NONE
      REAL a(100), b(100), c(100)
      REAL ad(100), bd(100), cd(100)
      INTEGER indx(100)
      INTEGER i, j
C
      DO j=1,50
        i = indx(j)
        ad(i) = c(i)*bd(i) + b(i)*cd(i)
        a(i) = b(i)*c(i)
      ENDDO
      DO i=1,100
        j = j + i
      ENDDO
      END
C

