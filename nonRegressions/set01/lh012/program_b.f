C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.3 (r3316M) - 19 Jan 2010 14:59
C
C  Differentiation of test in reverse (adjoint) mode:
C   gradient     of useful results: a b c
C   with respect to varying inputs: a b c
C   RW status of diff variables: a:in-out b:incr c:incr
C  ~/ali/llh/sources/topLevel/ali -classes -tl -head test -vars "T1 x" -o result program.f 
      SUBROUTINE TEST_B(a, ab, b, bb, c, cb)
      IMPLICIT NONE
      REAL a(100), b(100), c(100)
      REAL ab(100), bb(100), cb(100)
      INTEGER indx(100)
      INTEGER i, j
C
      j = 51
      DO j=50,1,-1
        i = indx(j)
        bb(i) = bb(i) + c(i)*ab(i)
        cb(i) = cb(i) + b(i)*ab(i)
        ab(i) = 0.0
      ENDDO
      END
C

