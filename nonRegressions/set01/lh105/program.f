      subroutine top(a,b,i)
C To test a very special case of TBR:
C   i should not be made TBR by 1st instruction!
      REAL a(10), b
      INTEGER i
c
      a(i) = a(i) + 2
      i = 2*i
      a(i) = b*a(i)
c
      end
      
