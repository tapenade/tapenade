      subroutine newton(x,n,alpha)
C Exemple avec lequel Pironneau a eu un bug en mode inverse?
      do i=1,n
        f = x-alpha*cos(x)
        fp= 1+alpha*sin(x)
        x=x-f/fp
      enddo
      return
      end
