      subroutine bugResetAdj(T1,T2,T3,T4)
C Bug vu dans lauvernet-multisailSpec:
C  les derivees adjointes des variables qui ne sont
C  que des sorties n'etaient pas remises a zero.
      DOUBLE PRECISION T1(100),T2(100),T3(100),T4(100)
      DOUBLE PRECISION c1,c2,TX(100),TV(100)
      INTEGER i
C
      do i = 1,100
         TV(i) = 0.d0
      enddo
      call prospect(TV(5), T3(6), T4(7))
      do i = 1,20
         TX(i)= c1*T1(i)+c2*T2(i)+TX(i-1)
         call prospect(T3(i), TX(i), T4(i))
      enddo
      return
      end

      subroutine prospect(x,y,z)
      DOUBLE PRECISION x,y,z
C
      z = x*y
      end
