      subroutine test(A,B,C,ne)
C Bugs found looking at Praveen code:
C -1- ie should be PUSHed at end of loop 1
C -2- ie should not be PUSHed before any call foo
C -3- unfortunately, A must be PUSH bef. foo(,a)!
C -4- but C need not be PUSH before foo(,c).
C -5- foo(,c) should be found "adjoint dead"
C -6- and foo_b does nothing => should not exist?
      real A(100),B(100),C(100)
      integer ne, ie
      do ie=1,ne
         call roe(ie)
         A(ie) = B(ie)*A(ie)
         ie = ie*2
         B(ie) = 2*B(ie)
      enddo
      A(0) = 3*A(0)
      do ie=1,ne
         call foo(ie,A)
         B(ie) = 4.0*B(ie)*A(ia)
      enddo
      do ie=1,ne
         call foo(ie,C)
         B(ie) = B(ie) + C(ie)
      enddo
      end

      subroutine roe(ie)
      integer ie
      ie = 2*ie -1
      end

      subroutine foo(ie,A)
      integer ie
      real A(100)
      A(ie) = A(ie)+3.5
      end
