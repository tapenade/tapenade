      subroutine flw2d1COL(nsg1,nsg2,nubo,t3,pres,vnocl,
     +     g3,g4,rh3,rh4,ns,nseg,sq)
C Un exemple de code pour lequel on devrait trouver
C  plein de dead code pour le mode inverse,
C  surtout si on genere proprement la boucle en "iteration-wise adjoint"
      integer nsg1,nsg2,ns,nseg,nubo(2,nseg),is1,is2
      real*8 vnocl(2,nseg),t3(ns),g3(ns),g4(ns),rh3(ns),rh4(ns),pres(ns)
      real*8 qsor,qsex,pm,dplim,sq
c
      DO 30 iseg=nsg1,nsg2
         is1 = nubo(1,iseg)
         is2 = nubo(2,iseg)
         qsor=t3(is1)*vnocl(2,iseg)
         qsex=t3(is2)*vnocl(2,iseg)
         dplim=qsor*g4(is1)+qsex*g4(is2)
         rh4(is1) = rh4(is1) + dplim
         rh4(is2) = rh4(is2) - dplim
         pm=pres(is1)+pres(is2)
         dplim=qsor*g3(is1)+qsex*g3(is2)+pm*vnocl(2,iseg)
         rh3(is1) = rh3(is1) + dplim
         rh3(is2) = rh3(is2) - dplim
         call CHECK(pm,sq)
 30   CONTINUE
      end

      subroutine check(pp,sq)
      real*8 pp,sq
      sq = sq + pp*pp
      end
