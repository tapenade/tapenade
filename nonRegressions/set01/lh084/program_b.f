C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) -  4 Jun 2019 19:12
C
C  Differentiation of flw2d1col in reverse (adjoint) mode:
C   gradient     of useful results: rh3 rh4 vnocl g3 g4 t3 sq pres
C   with respect to varying inputs: rh3 rh4 vnocl g3 g4 t3 sq pres
C   RW status of diff variables: rh3:in-out rh4:in-out vnocl:incr
C                g3:incr g4:incr t3:incr sq:in-out pres:incr
      SUBROUTINE FLW2D1COL_B(nsg1, nsg2, nubo, t3, t3b, pres, presb, 
     +                       vnocl, vnoclb, g3, g3b, g4, g4b, rh3, rh3b
     +                       , rh4, rh4b, ns, nseg, sq, sqb)
      IMPLICIT NONE
C Un exemple de code pour lequel on devrait trouver
C  plein de dead code pour le mode inverse,
C  surtout si on genere proprement la boucle en "iteration-wise adjoint"
      INTEGER nsg1, nsg2, ns, nseg, nubo(2, nseg), is1, is2
      REAL*8 vnocl(2, nseg), t3(ns), g3(ns), g4(ns), rh3(ns), rh4(ns), 
     +       pres(ns)
      REAL*8 vnoclb(2, nseg), t3b(ns), g3b(ns), g4b(ns), rh3b(ns), rh4b(
     +       ns), presb(ns)
      REAL*8 qsor, qsex, pm, dplim, sq
      REAL*8 qsorb, qsexb, pmb, dplimb, sqb
      INTEGER iseg
      DO iseg=nsg2,nsg1,-1
        is1 = nubo(1, iseg)
        is2 = nubo(2, iseg)
        pm = pres(is1) + pres(is2)
        CALL CHECK_B(pm, pmb, sq, sqb)
        dplimb = rh3b(is1) - rh3b(is2)
        qsex = t3(is2)*vnocl(2, iseg)
        qsor = t3(is1)*vnocl(2, iseg)
        qsorb = g3(is1)*dplimb
        g3b(is1) = g3b(is1) + qsor*dplimb
        qsexb = g3(is2)*dplimb
        g3b(is2) = g3b(is2) + qsex*dplimb
        pmb = pmb + vnocl(2, iseg)*dplimb
        vnoclb(2, iseg) = vnoclb(2, iseg) + pm*dplimb
        presb(is1) = presb(is1) + pmb
        presb(is2) = presb(is2) + pmb
        dplimb = rh4b(is1) - rh4b(is2)
        qsorb = qsorb + g4(is1)*dplimb
        g4b(is1) = g4b(is1) + qsor*dplimb
        qsexb = qsexb + g4(is2)*dplimb
        g4b(is2) = g4b(is2) + qsex*dplimb
        t3b(is2) = t3b(is2) + vnocl(2, iseg)*qsexb
        vnoclb(2, iseg) = vnoclb(2, iseg) + t3(is2)*qsexb + t3(is1)*
     +    qsorb
        t3b(is1) = t3b(is1) + vnocl(2, iseg)*qsorb
      ENDDO
      END

C  Differentiation of check in reverse (adjoint) mode:
C   gradient     of useful results: sq
C   with respect to varying inputs: sq pp
C
      SUBROUTINE CHECK_B(pp, ppb, sq, sqb)
      IMPLICIT NONE
      REAL*8 pp, sq
      REAL*8 ppb, sqb
      ppb = 2*pp*sqb
      END

