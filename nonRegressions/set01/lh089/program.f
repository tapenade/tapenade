C To test tangent mode applied on the output of the reverse mode.
      subroutine pushpop(a,b)
      real*8 a, b
      call PUSHREAL8(a)
      a = a*b
      b = b+a
      call POPREAL8(a)
      b = a/b
      end
