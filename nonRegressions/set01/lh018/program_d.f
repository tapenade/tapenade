C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 18 Apr 2019 18:10
C
C  Differentiation of top in forward (tangent) mode:
C   variations   of useful results: d a
C   with respect to varying inputs: b c
C   RW status of diff variables: d:out a:out b:in c:in
      SUBROUTINE TOP_D(a, ad, b, bd, c, cd, d, dd)
      IMPLICIT NONE
      REAL a, b, c(20), d
      REAL ad, bd, cd(20), dd
      REAL x, y, z
      REAL F
      REAL F_D
C
      ad = F_D(b*c(10), c(10)*bd + b*cd(10), d, dd, a)
      y = 3.5
      z = 4.5
      x = F(8*y, z)
      ad = x*ad
      a = a*x
      END

C  Differentiation of f in forward (tangent) mode:
C   variations   of useful results: f v
C   with respect to varying inputs: u
C
      REAL FUNCTION F_D(u, ud, v, vd, f)
      IMPLICIT NONE
      REAL u, v
      REAL ud, vd
      REAL f
      vd = 2.5*ud
      v = 2.5*u
      f_d = ud + vd
      f = u + v
      END

