C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.3 (r3316M) - 19 Jan 2010 14:59
C
C  Differentiation of test in reverse (adjoint) mode:
C   gradient     of useful results: a b c
C   with respect to varying inputs: a b c
C   RW status of diff variables: a:incr b:incr c:in-zero
      SUBROUTINE TEST_B(a, ab, b, bb, c, cb)
      IMPLICIT NONE
      REAL a, b, c, d
      REAL ab, bb, cb, db
C
      bb = bb + 2*b*cb
      db = cb
      ab = ab + db/100.d0
      cb = 0.0
      END
C

