      SUBROUTINE FLUX (QL,QR,SinAl,CosAl,DS,R,Lambda)

      implicit none

      double precision gamma, gm1
      PARAMETER (Gamma=1.4d0,Gm1=Gamma-1.d0)
      double precision QL(4),QR(4), SinAl, CosAl, DS, Lambda

      double precision  R(4),FL(4),FR(4),FD(4),F(4),L(4),Alpha(4),
     &       REvec(4,4),QLN(2:3),QRN(2:3)
      
      double precision  PL,PR,AL,AR,hL,hR,S,Sp1,RHat,
     &     UHat,VHat,hHat,AHat,
     &     DL1,DL1Star,DL2,DL2Star,DR,DU,DV,DP

      integer nvar
C
C     Projected components.
      QLN(2) = QL(2)*CosAl+QL(3)*SinAl
      QLN(3) = QL(3)*CosAl-QL(2)*SinAl
      QRN(2) = QR(2)*CosAl+QR(3)*SinAl
      QRN(3) = QR(3)*CosAl-QR(2)*SinAl
C
C     Pressure, sound speed, enthalpy.
      PL = Gm1*(QL(4)-.5d0*(QLN(2)**2+QLN(3)**2)/QL(1))
      PR = Gm1*(QR(4)-.5d0*(QRN(2)**2+QRN(3)**2)/QR(1))
      AL = SQRT(Gamma*PL/QL(1))
      AR = SQRT(Gamma*PR/QR(1))
      hL = (QL(4)+PL)/QL(1)
      hR = (QR(4)+PR)/QR(1)
C
C     Rho averages.
      S = SQRT(QL(1)/QR(1))
      Sp1 = S+1.d0
      RHat = S*QR(1)
      UHat = (S*QLN(2)/QL(1)+QRN(2)/QR(1))/Sp1
      VHat = (S*QLN(3)/QL(1)+QRN(3)/QR(1))/Sp1
      hHat = (S*hL+hR)/Sp1
      AHat = SQRT(Gm1*(hHat-.5d0*(UHat**2+VHat**2)))
C
C     Timestep criterion.
      Lambda = MAX(0.d0,UHat*CosAl+VHat*SinAl+AHat)*DS
C
C     Central fluxes.
      FL(1) = QLN(2)
      FL(2) = QLN(2)**2/QL(1)+PL
      FL(3) = QLN(2)*QLN(3)/QL(1)
      FL(4) = QLN(2)*hL
      FR(1) = QRN(2)
      FR(2) = QRN(2)**2/QR(1)+PR
      FR(3) = QRN(2)*QRN(3)/QR(1)
      FR(4) = QRN(2)*hR
C
C     Absolute eigenvalues, acoustic waves with entropy fix.
      L(1) = ABS(UHat-AHat)
      DL1 = QRN(2)/QR(1)-AR-QLN(2)/QL(1)+AL
      DL1Star = MAX(4.d0*DL1,0.d0)
      IF (L(1).LT..5d0*DL1Star) L(1) = L(1)*L(1)/DL1Star+.25d0*DL1Star
      L(2) = ABS(UHat)
      L(3) = ABS(UHat)
      L(4) = ABS(UHat+AHat)
      DL2 = DL1+2.d0*(AR-AL)
      DL2Star = MAX(4.d0*DL2,0.d0)
      IF (L(4).LT..5d0*DL2Star) L(4) = L(4)*L(4)/DL2Star+.25d0*DL2Star
C
C     Wavestrengths.
      DR = QR(1)-QL(1)
      DU = QRN(2)/QR(1)-QLN(2)/QL(1)
      DV = QRN(3)/QR(1)-QLN(3)/QL(1)
      DP = PR-PL
      Alpha(1) = .5d0*(DP/AHat-RHat*DU)/AHat
      Alpha(2) = DR-DP/AHat**2
      Alpha(3) = RHat*DV
      Alpha(4) = .5d0*(DP/AHat+RHat*DU)/AHat
C
C     Right eigenvectors.
      REvec(1,1) = 1.d0
      REvec(1,2) = UHat-AHat
      REvec(1,3) = VHat
      REvec(1,4) = hHat-UHat*AHat
      REvec(2,1) = 1.d0
      REvec(2,2) = UHat
      REvec(2,3) = VHat
      REvec(2,4) = .5d0*(UHat**2+VHat**2)
      REvec(3,1) = 0.d0
      REvec(3,2) = 0.d0
      REvec(3,3) = 1.d0
      REvec(3,4) = VHat
      REvec(4,1) = 1.d0
      REvec(4,2) = UHat+AHat
      REvec(4,3) = VHat
      REvec(4,4) = hHat+UHat*AHat
C
C     Jumps across simple waves.
      FD(1) = 0.d0
      FD(2) = 0.d0
      FD(3) = 0.d0
      FD(4) = 0.d0
      DO NVar = 1,4
        FD(1) = FD(1)+L(NVar)*Alpha(NVar)*REvec(NVar,1)
        FD(2) = FD(2)+L(NVar)*Alpha(NVar)*REvec(NVar,2)
        FD(3) = FD(3)+L(NVar)*Alpha(NVar)*REvec(NVar,3)
        FD(4) = FD(4)+L(NVar)*Alpha(NVar)*REvec(NVar,4)
      END DO
C
C     Sum of fluxes.
      F(1) = .5d0*(FL(1)+FR(1)-FD(1))
      F(2) = .5d0*(FL(2)+FR(2)-FD(2))
      F(3) = .5d0*(FL(3)+FR(3)-FD(3))
      F(4) = .5d0*(FL(4)+FR(4)-FD(4))
C
C     Calculate the residual.
      R(1) = DS*F(1)
      R(2) = DS*(F(2)*CosAl-F(3)*SinAl)
      R(3) = DS*(F(3)*CosAl+F(2)*SinAl)
      R(4) = DS*F(4)
C     
      RETURN
      END




