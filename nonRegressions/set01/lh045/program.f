      subroutine S2(i1,p,i2)
C  P.2<=ID c2.1<=P.2,c2.2  c2.2<=ID 
      integer i1,i2,i3
      real v1,p,v2
      common /c2/ v1,v2
      i3 = 2*i1
      v1 = p*v2 + i1 - i2*i3
      return
      end

      real function S3(i1,p)
C  Res<=c2.1,c1.2  P.2<=c2.1
C  c1.1<=ID  c1.2<=c2.1,c2.2
C  c2.1<=ID  c2.2<=ID
      integer i1, i2
      real v1,v2,p
      integer i3,i4
      real w3,w4
      common /c1/ w3,w4
      common /c2/ v1,v2
      p = v1 + i1
      S3 = 2*p + w4
      w4 = v1*v2
      return
      end

      subroutine S1(x,i1,y,i2,z)
C  P.1<=P.1,P.3  P.3<=P.1,c2.2  P.5<=P.1,c1.2,c2.2
C  c1.1<=ID  c1.2<=P.1,c2.2
C  c2.1<=P.1,c2.2  c2.2<=ID
      integer i1, i2
      real x, y, z, t
      real w1, w2
      common /c1/ w1,w2
      call S2(7,x,8)
      t = y-10.0
      if (x.gt.y) then
         x = t
      endif
      z = 2*S3(6,y)
      return
      end
