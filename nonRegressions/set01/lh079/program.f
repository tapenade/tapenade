      double precision function f(t,a,ad,b,bd,x)
      double precision t,x
      dimension a(5), b(5), ad(5), bd(5)

      f = t * t
      bd(2) = -(0.5d0*(ad(1)*x**-0.5d0-a(1)*(0.5d0*
     &    x**(-0.5d0-1))*xd))
      b(2) = -(0.5d0*a(1)*x**-0.5d0)
      f = exp(f)
      return

      end
