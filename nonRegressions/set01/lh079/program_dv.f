C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of f in forward (tangent) mode (with options multiDirectional):
C   variations   of useful results: f bd b
C   with respect to varying inputs: ad t x bd a b
C   RW status of diff variables: f:out ad:in t:in x:in bd:in-out
C                a:in b:in-out
      SUBROUTINE F_DV(t, td, a, ad0, ad, add, b, bd0, bd, bdd, x, xd0, f
     +                , fd, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      DOUBLE PRECISION t, x
      DOUBLE PRECISION td(nbdirsmax), xd0(nbdirsmax)
      REAL ad
      REAL bd
      REAL a
      REAL b
      DIMENSION a(5), b(5), ad(5), bd(5)
      REAL add
      REAL bdd
      REAL ad0
      REAL bd0
      DIMENSION ad0(nbdirsmax, 5), bd0(nbdirsmax, 5), add(nbdirsmax, 5)
     +    , bdd(nbdirsmax, 5)
      REAL xd
      INTRINSIC EXP
      INTEGER nd
      DOUBLE PRECISION temp
      DOUBLE PRECISION temp0
      DOUBLE PRECISION f
      DOUBLE PRECISION fd(nbdirsmax)
      INTEGER nbdirs
      f = t*t
      temp = x**(-0.5d0)
      temp0 = x**(-1.5D0)
      DO nd=1,nbdirs
C
        fd(nd) = 2*t*td(nd)
        bdd(nd, 2) = -(0.5d0*(temp*add(nd, 1)-ad(1)*0.5d0*x**(-1.5D0)*
     +    xd0(nd)-xd*0.5d0*(temp0*ad0(nd, 1)-a(1)*1.5D0*x**(-2.5D0)*xd0(
     +    nd))))
        fd(nd) = EXP(f)*fd(nd)
      ENDDO
      bd(2) = -(0.5d0*(ad(1)*temp-xd*0.5d0*(a(1)*temp0)))
      temp0 = x**(-0.5d0)
      DO nd=1,nbdirs
        bd0(nd, 2) = -(0.5d0*(temp0*ad0(nd, 1)-a(1)*0.5d0*x**(-1.5D0)*
     +    xd0(nd)))
      ENDDO
      b(2) = -(0.5d0*(a(1)*temp0))
      f = EXP(f)
      RETURN
C
      END

