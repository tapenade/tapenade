c  To test if TBR functions when a variable
C  is overwritten by an io call
      subroutine testiotbr (a,b,c)
      real a,b,c
c
      b = a*a
      READ (12,*), a
      c = a*b
c
      end
