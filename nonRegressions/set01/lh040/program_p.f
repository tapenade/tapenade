C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:35
C
      DOUBLE PRECISION FUNCTION F(t)
      IMPLICIT NONE
      DOUBLE PRECISION t
      CHARACTER*10 sdval(4)
      INTRINSIC EXP
      DATA sdval /4*'          '/
      f = t*t
      f = EXP(f)
      RETURN
      END
C
C

