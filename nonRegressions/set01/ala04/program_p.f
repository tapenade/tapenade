C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) -  7 Oct 2022 19:08
C
C Cleaner example (than ala02!) with 2 nested Fixed-Point loops,
C  plus checkpointed calls inside the loops
      PROGRAM TEST
      IMPLICIT NONE
      REAL*8 x, y
      x = 1.0
      CALL FP2(x, y)
      PRINT*, 'x=', x, '--> y=', y
      END

C
      SUBROUTINE FP2(x, y)
      IMPLICIT NONE
      REAL*8 x, y, z, t, ox, oz, ot
      INTEGER i1, i2
      z = 24.0
      oz = z + 1
      i1 = 0
C$AD FP-LOOP z
C z <= fixpoint(x (,z))
      DO WHILE ((z-oz)**2 .GE. 1.e-20)
        oz = z
        t = 17.0
        ot = t + 1
        i2 = 0
C$AD FP-LOOP t
C  t <= fixpoint(z (,t))
        DO WHILE ((t-ot)**2 .GE. 1.e-20)
          ot = t
          CALL TOTO(t, z, ot)
          i2 = i2 + 1
        ENDDO
        CALL TOTO(z, x, oz)
        z = t*z
C        PRINT *,"outer",i1,":",i2,"iterations"
        i1 = i1 + 1
      ENDDO
      y = z*x
      END

C
      SUBROUTINE TOTO(z, x, oz)
      IMPLICIT NONE
      REAL*8 z, oz, x
      z = 2.0/(oz+x)
      END

