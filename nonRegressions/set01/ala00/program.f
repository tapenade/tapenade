C point fixe de z = 2/(z+x) --> z = (-x +/- SQRT(x*x + 8))/2
C Pour x = 1, z = -2 ou 1

      Program test 
      real y
      real x
      real initial

      x=1.
      initial=24.
      y=0.0

      call root(x,y,initial)
     
      Print *, "x=",x," solution y=" ,y

      end 


      SUBROUTINE root(x,y, initial)

      Real z, oz,y
      Real x, initial
      Integer i
     
      z=initial
      oz = z + 1
      i=0

C     $AD FP-LOOP z
      DO WHILE ((z-oz)**2 .GE. 1.e-10) 
         oz = z
         z = z+x
         z = 2.0/z
         i=i+1
      ENDDO

      y=z*x
      print *,  "#iterations:", i      
      END
