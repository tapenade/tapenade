      SUBROUTINE VCURVM(nf1, noe1, nsfac, sigma, ce, vnsig, vnfac, ua)
c
C Bug found in II-loop for sonicboom;vcurvm
C   ==> PUSH of ifac was inconsistant with POP.
      integer nf1,noe1(99),nsfac(3,99)
      real*8 sigma(99),ce(5,99),vnsig(99),vnfac(2,99),ua(4,99)
c
      integer if1, ifac, j, is
      real*8 pm3, vnx, ro
c
c      
      IF (nf1 .GT. 0) THEN
C$AD II-LOOP
         DO 1000 if1=1,nf1
            ifac                   = noe1(if1)
            pm3                    = 2.0 * sigma(ifac)
            CE(5,if1)              = CE(5,if1) - vnsig(ifac)*pm3
1000     CONTINUE
c
      ENDIF
c
c
      IF (nf1 .GT. 10) THEN
C$AD II-LOOP
         DO 2000 if1=1,nf1
            ifac                   = noe1(if1)
            vnx                    = vnfac(1,ifac)
            DO 2100 j=1,3    
               is                  = nsfac(j,ifac)
               ro                  = ua(1,is)
               CE(5,is)            = CE(5,is) - ro*ro + vnx*vnx
2100        CONTINUE  
2000     CONTINUE
c
      ENDIF
      CE(5,2) = CE(5,2)*CE(5,2)

      END
