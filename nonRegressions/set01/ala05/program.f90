! Un exemple pour tester les histoires de warm-start d'Ala
! C'est l'exemple que j'ai utilise pour l'expose NIPS 2017
! Ceci est l'adjoint qui essaye de reproduire un warm-start qui marche.
! cf testsEnVrac/testWarmStart
program main
  double precision x, y, res
  x = 5.0
  y = 0.0
  call NFP(x,y)
  print *, '  y:', y
end program main

subroutine NFP(x,y)
  double precision x,y
  integer i, slices
  double precision z, oldz
  double precision epsilon

  slices = 50
  z = 1.0
  epsilon = 1.d-10
  DO i=1, slices
     x = x + 1.0/slices
     ! warm start
     oldz = z+1.0
     !$AD FP-LOOP z adj_residual=1.0e-30
     DO WHILE ((oldz-z)*(oldz-z)>epsilon)
        oldz = z
        z = 0.75d0*z + 0.25d0*x/z
     END DO
     y = y + z/slices
  END DO
END subroutine NFP
