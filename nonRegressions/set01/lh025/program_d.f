C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 18 Apr 2019 18:10
C
C  Differentiation of funeval in forward (tangent) mode:
C   variations   of useful results: y z
C   with respect to varying inputs: x y z lambda a
C   RW status of diff variables: x:in y:in-out z:in-out lambda:in
C                a:in
C--------1---------2---------3---------4---------5---------6---------7
C
      SUBROUTINE FUNEVAL_D(a, ad, x, xd, n, k, lambda, lambdad, y, yd, z
     +                     , zd)
      IMPLICIT NONE
C
      INTEGER i, j, k, n
      DOUBLE PRECISION x(k), y(k)
      DOUBLE PRECISION xd(k), yd(k)
C
      DOUBLE PRECISION z(n)
      DOUBLE PRECISION zd(n)
C
      DOUBLE PRECISION a(n-k, k)
      DOUBLE PRECISION ad(n-k, k)
      DOUBLE PRECISION lambda
      DOUBLE PRECISION lambdad
C
C     --------------------------------------------------------
C     Phase 1: Build the product Z = M * X
C
C     Consider the dense part of M; that is, A
      DO i=1,n-k
        zd(i) = 0.D0
        z(i) = 0.0d0
        DO j=1,k
          zd(i) = zd(i) + x(j)*ad(i, j) + a(i, j)*xd(j)
          z(i) = z(i) + a(i, j)*x(j)
        ENDDO
      ENDDO
C
C     Finally, the "sparse part"
      DO i=n-k+1,n
        zd(i) = x(i+k-n)*lambdad + lambda*xd(i+k-n)
        z(i) = lambda*x(i+k-n)
      ENDDO
C
C     --------------------------------------------------------
C     Phase 2: Build the product Y = M^T * Z
C
C     Consider the dense part of M^T
      DO j=1,k
        yd(j) = 0.D0
        y(j) = 0.0d0
        DO i=1,n-k
          yd(j) = yd(j) + a(i, j)*zd(i) + z(i)*ad(i, j)
          y(j) = y(j) + z(i)*a(i, j)
        ENDDO
C     the "sparse part"
        yd(j) = yd(j) + z(j+n-k)*lambdad + lambda*zd(j+n-k)
        y(j) = y(j) + lambda*z(j+n-k)
      ENDDO
C
      RETURN
      END
C
C
C
C
C
C

