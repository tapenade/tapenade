C--------1---------2---------3---------4---------5---------6---------7
C
      SUBROUTINE FUNEVAL(A,X,N,K,lambda,Y,Z)
C
C     Given a K-dimensional vector X and an N X K matrix M whose
C     structure is 
C
C                      |        A         |  
C                 M =  |                  |     ,
C                      | lambda * I_(KxK) | 
C
C     this subroutine computes the K-dimensional vector
C
C                       Y  = M^T * M * X .
C
C     ON INPUT:
C     A      = (N - K) X K MATRIX
C     N      = INTEGER, NUMBER OF ROWS OF M
C     K      = INTEGER, NUMBER OF COLS OF M
C     X      = K-DIMENSIONAL VECTOR
C     lambda = Regularization Parameter
C     ON EXIT:
C     Y      = THE RESULT K-DIMENSIONAL VECTOR
C------------------------------------------------
C     Z      = local work array of size N
C
C     AUTHOR: Martin BUECKER, Arno Rasch
C
C     History:
C     08/13/02: adapted from previous versions


      IMPLICIT NONE

      INTEGER I,J,K,N
 
      DOUBLE PRECISION X(K), Y(K)

      DOUBLE PRECISION Z(N)

      DOUBLE PRECISION A(N-K,K)
      DOUBLE PRECISION lambda

C     --------------------------------------------------------
C     Phase 1: Build the product Z = M * X

C     Consider the dense part of M; that is, A
      DO I = 1,N-K
         Z(I) = 0.0d0
         DO J = 1,K
            Z(I) = Z(I) + A(I,J)*X(J)
         ENDDO
      ENDDO

C     Finally, the "sparse part"
      DO I = N-K+1,N
         Z(I)  = lambda*X(I+K-N)
      ENDDO

C     --------------------------------------------------------
C     Phase 2: Build the product Y = M^T * Z

C     Consider the dense part of M^T
      DO J = 1,K
         Y(J) = 0.0d0
         DO I = 1,N-K
            Y(J) = Y(J) + Z(I)* A(I,J)
         ENDDO
C     the "sparse part"
         Y(J) = Y(J) + lambda*Z(J+N-K)
      ENDDO

      RETURN
      END






