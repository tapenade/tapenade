C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (develop) - 24 Oct 2019 16:52
C
C  Differentiation of top in forward (tangent) mode (with options multiDirectional no!SpareInits no!MergeDiffInstrs):
C   variations   of useful results: i1 i2 i3 o1 o2 o3
C   with respect to varying inputs: i2 i3
C   RW status of diff variables: i1:zero i2:in-zero i3:in-out o1:out
C                o2:zero o3:zero
      SUBROUTINE TOP_DV(i1, i1d, i2, i2d, i3, i3d, o1, o1d, o2, o2d, o3
     +                  , o3d, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      REAL i1, i2, i3
      REAL i1d(nbdirsmax), i2d(nbdirsmax), i3d(nbdirsmax)
      REAL o1, o2, o3
      REAL o1d(nbdirsmax), o2d(nbdirsmax), o3d(nbdirsmax)
      REAL x1
      REAL G
      INTEGER nd
      INTEGER nbdirs
C
      DO nd=1,nbdirs
        i1d(nd) = 0.0
      ENDDO
      i1 = 2.0
      IF (i3 .LT. 0.0) CALL G_DV(i1, i1d, i2, i2d, i3, i3d, nbdirs)
      CALL G_DV(i2, i2d, i3, i3d, i1, i1d, nbdirs)
      DO nd=1,nbdirs
        i2d(nd) = 0.0
      ENDDO
      i2 = 2.3
      x1 = G(i1, i3)
      DO nd=1,nbdirs
        o3d(nd) = 0.0
      ENDDO
      o3 = i3*i2
      CALL SUB1_DV(i1, i1d, i2, o1, o1d, o2, nbdirs)
      DO nd=1,nbdirs
        o1d(nd) = o2*i2*o1d(nd)
      ENDDO
      o1 = o1*o2*i2
      DO nd=1,nbdirs
        o3d(nd) = 0.0
      ENDDO
      o3 = 2.0
      DO nd=1,nbdirs
        i2d(nd) = 0.0
      ENDDO
      i2 = 5.0
      END

C  Differentiation of sub1 in forward (tangent) mode (with options multiDirectional no!SpareInits no!MergeDiffInstrs):
C   variations   of useful results: o1
C   with respect to varying inputs: i1
C
      SUBROUTINE SUB1_DV(i1, i1d, i2, o1, o1d, o2, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      REAL i1, i2
      REAL i1d(nbdirsmax)
      REAL o1, o2
      REAL o1d(nbdirsmax)
      REAL x1, x2
      REAL x1d(nbdirsmax), x2d(nbdirsmax)
      INTEGER nd
      INTEGER nbdirs
      DO nd=1,nbdirsmax
        o1d(nd) = 0.0
      ENDDO
      DO nd=1,nbdirsmax
        x1d(nd) = 0.0
      ENDDO
      DO nd=1,nbdirsmax
        x2d(nd) = 0.0
      ENDDO
C
      DO nd=1,nbdirs
        x2d(nd) = 0.0
      ENDDO
      x2 = 5.0
      DO nd=1,nbdirs
        x1d(nd) = i2*i1d(nd)
      ENDDO
      x1 = i1*i2
      IF (i1 .GT. 3.0) THEN
        DO nd=1,nbdirs
          x2d(nd) = i1d(nd)
        ENDDO
        x2 = x2 + i1 - 3*i2
      ELSE
        DO nd=1,nbdirs
          x2d(nd) = 0.0
        ENDDO
        x2 = 12
        DO nd=1,nbdirs
          x1d(nd) = 2*x1d(nd)
        ENDDO
        x1 = 2*x1 + x2
      END IF
      DO nd=1,nbdirs
        o1d(nd) = (x1d(nd)-x1*x2d(nd)/x2)/x2
      ENDDO
      o1 = x1/x2
      o2 = 35.0
      DO nd=1,nbdirs
        i1d(nd) = 0.0
      ENDDO
      i1 = 99.0
      END

C  Differentiation of g in forward (tangent) mode (with options multiDirectional no!SpareInits no!MergeDiffInstrs):
C   variations   of useful results: g
C   with respect to varying inputs: i1 i2
C
      SUBROUTINE G_DV(i1, i1d, i2, i2d, g, gd, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      REAL i1, i2
      REAL i1d(nbdirsmax), i2d(nbdirsmax)
      INTEGER nd
      REAL g
      REAL gd(nbdirsmax)
      INTEGER nbdirs
      DO nd=1,nbdirs
        gd(nd) = i1d(nd) - i2d(nd)
      ENDDO
      g = i1 - i2
      END
C

