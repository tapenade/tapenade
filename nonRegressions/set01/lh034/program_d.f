C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.15 (feature_bugFixes) - 14 Dec 2020 17:39
C
C  Differentiation of invert in forward (tangent) mode:
C   variations   of useful results: invert
C   with respect to varying inputs: a0 b0
C   RW status of diff variables: invert:out a0:in b0:in
C
      REAL FUNCTION INVERT_D(F, F_D, x, a0, a0d, b0, b0d, n, invert)
      IMPLICIT NONE
C
      EXTERNAL F
      EXTERNAL F_D
      REAL F, x, a0, b0, a, b, m
      REAL F_D, a0d, b0d, ad, bd, md
      INTEGER n, i
      REAL result1
      REAL result2
      REAL invert
C
      ad = a0d
      a = a0
      bd = b0d
      b = b0
C
      DO i=1,n
        md = (ad+bd)/2
        m = (a+b)/2
C
        result1 = F_D(a, ad)
        result2 = F_D(m, md)
        IF ((result1-x)*(result2-x) .LE. 0) THEN
          bd = md
          b = m
        ELSE
          ad = md
          a = m
        END IF
      ENDDO
C
      invert_d = (ad+bd)/2
      invert = (a+b)/2
C
      RETURN
      END

