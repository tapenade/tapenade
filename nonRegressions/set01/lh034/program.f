
      REAL FUNCTION INVERT(F,X,A0,B0,N)

      EXTERNAL F
      REAL F, X, A0, B0, A, B, M
      INTEGER N, I

      A = A0
      B = B0

      DO I = 1, N
        M = (A+B)/2

        IF ( (F(A)-X) * (F(M)-X) .le. 0 ) THEN
          B = M
        ELSE
          A = M
        END IF
      END DO

      INVERT = (A+B)/2

      RETURN
      END
