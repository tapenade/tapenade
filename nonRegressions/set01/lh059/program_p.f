C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:15
C
      SUBROUTINE SUB2(t, u, n, i)
      IMPLICIT NONE
      REAL t, u
      INTEGER n, i
      DIMENSION t(n), u(n)
      INTRINSIC LOG
      DO WHILE (t(i) .GT. 0.0)
        IF (t(i) .GT. 1.0) THEN
          u(i) = u(i) + LOG(t(i))
        ELSE
          u(i) = u(i) + t(i-5)
          IF (u(i) .LT. 0.0) GOTO 5
        END IF
        t(i) = 3*u(i)
 5      i = i + 5
        t(i) = 2*t(i) + 1
      ENDDO
      END

