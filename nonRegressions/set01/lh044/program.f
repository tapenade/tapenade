
      REAL FUNCTION INVERT(FX0,FX1,X,A0,B0,N,FX2,FX3)

      EXTERNAL FX0
      INTRINSIC FX1
      REAL FX0, X, A0, B0, A, B, M
      INTEGER N, I
      EXTERNAL FXlocal

      A = A0
      B = B0

      DO I = 1, N
        M = (A+B)/2

        IF ( (FX0(A)-X) * (FX1(M)-X) .le. 0 ) THEN
          B = M
        ELSE
          A = FX2(M)
        END IF
      END DO

      call FX3(A,B,2.5,"coucou")

      call FXlocal(A,B,2.5, "coucou")

      INVERT = (A+B)/2

      RETURN
      END
