C Example with 2 nested Fixed-Point loops,
c  plus checkpointed calls inside the loops
C NOTE: devious code: the inside FP loop obviously never runs!
C On this example, there was a "Wrong flow graph structure"
C  error in the adjoint fixed-loop
      PROGRAM test 
      real y
      real x
      Integer initial

      x=1.
      initial=24.
      y=0.0
      call root(x,y,initial)
      Print *, "x=",x,"-> solution y=" ,y
      END 

      SUBROUTINE root(x, y, initial)
      Real z, oz,y
      Real x , ox
      Integer i, initial
     
      z=initial
      oz = z + 1
      i=0
C$AD FP-LOOP z adj_reduction=5.e-6
      DO WHILE ((z-oz)**2 .GE. 1.e-10) 
         oz = z
         ox=x+1
C$AD FP-LOOP z adj_reduction=5.e-5
         DO WHILE ((z-oz)**2 .GE. 1.e-10) 
            ox=x
            call  toto(z,x,oz)
         ENDDO
         call  toto(z,x,oz)
         i=i+1
      ENDDO
      y=z*x
      print *,  "nombre d iterations", i      
      END

      SUBROUTINE toto(z,x,oz)
      Real z, oz,x
      z = 2.0/(oz+x)
      END
