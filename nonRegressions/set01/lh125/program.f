c Checking removal of useless PUSH/POP of control
c for empty adjoint conditionals etc...
      function tetacrit(tgh)
c  From lauvernet-stics/sources/raytrans.for
      real teta(180),h,hcrit,tgh
      real acrit,tetacrit

      tetacrit=0.0
      i = 7
      teta(i)= 0.2*i
      if(teta(i).ne.0.0) then
         azim=teta(i)/abs(teta(i))
      else
         azim=0.0
      endif
      if(teta(i).gt.2.0) h=0.0
      hcrit = 0.0
      do i = 1,4
         hcrit=hcrit + tgh*abs(azim)
      enddo
      acrit=hcrit*h
      if(acrit.gt.5.0) tetacrit=acrit*teta(i)
c
      return
      end 
