      subroutine adj1(x,y,z,N,o)
c  Premier essai pour le mode inverse sur une
c  boucle, pour tester l'inversion du controle
c  des boucles, principalement.
      real a,x,y,z
      integer n,o
      dimension x(2*N),y(2*N),z(2*N)
c c1
      a = 0.5 * X(20)
C a normal loop:
      DO 200 i=5+o,N,2
         Z(i) = Z(i-1)-2*Z(i)+Z(i+1)
         X(i) = 3*X(i) - Y(i+1)*Y(i-1)
 200  continue
      a = X(10) + a
C a natural loop:
      j = 0
 100  j = j+3
      X(j) = a*Y(j-1)
      Y(j+1) = Z(j)*Z(3) + X(j+1)
      if (j.le.100) goto 100
      a = 2*a+3
c c4
      end          
