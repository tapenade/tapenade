c Cet exemple est mal differentie a cause des casts
c entre real et integer, qui perdent l'activite.
c C'est inevitable. Cet exemple sert donc uniquement a tester
c qu'on envoie bien un warning qui correspond au probleme
      subroutine top(in,out,N)
      integer N,i
      real*8 in(-10:N), out(-10:N)
      real*8 varcom
      common /comvarcom/ varcom

      do i=5,100,5
         in(i) = in(i)+in(i+2)*in(i-2)
         out(i) = 0
      enddo
      varcom = in(-10)
c sale effet de bord: la var active "in" est passee comme un entier
c pareil pour la globale active varcom
      call dirtyWrite(in(1),200)
      varcom = 0.0
      do i=-10,N
         out(i) = 3*in(i)
         in(i) = 0.0
      enddo
c sale effet de bord: la var utile "out" est prise dans un entier
c pareil pour la globale utile varcom
      call dirtyRead(out(1),200)
      out(-10) = varcom
      do i=0,100,10
         out(i) = 2*out(i)+cos(0.1*i)
      enddo
      end

      subroutine dirtyWrite(a,len)
      integer a(*),len,i
      integer bigbuf(10000)
      common /bigcommon/bigbuf
      integer varCom
      common /comvarcom/ varcom

      bigbuf(10000) = varcom
      do i=1,len
         bigbuf(i) = a(i)
      enddo
      end

      subroutine dirtyRead(a,len)
      integer a(*),len,i
      integer bigbuf(10000)
      common /bigcommon/bigbuf
      integer varCom
      common /comvarcom/ varcom

      varcom = bigbuf(10000)
      do i=1,len
         a(i) = bigbuf(i)
      enddo
      end
      
