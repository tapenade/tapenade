C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.7 (r4945M) - 18 Sep 2013 10:15
C
C Cet exemple est mal differentie a cause des casts
C entre real et integer, qui perdent l'activite.
C C'est inevitable. Cet exemple sert donc uniquement a tester
C qu'on envoie bien un warning qui correspond au probleme
      SUBROUTINE TOP(in, out, n)
      IMPLICIT NONE
      INTEGER n, i
      REAL*8 in(-10:n), out(-10:n)
      REAL*8 varcom
      COMMON /comvarcom/ varcom
      INTRINSIC COS
C
      DO i=5,100,5
        in(i) = in(i) + in(i+2)*in(i-2)
        out(i) = 0
      ENDDO
      varcom = in(-10)
C sale effet de bord: la var active "in" est passee comme un entier
C pareil pour la globale active varcom
      CALL DIRTYWRITE(in(1), 200)
      varcom = 0.0
      DO i=-10,n
        out(i) = 3*in(i)
        in(i) = 0.0
      ENDDO
C sale effet de bord: la var utile "out" est prise dans un entier
C pareil pour la globale utile varcom
      CALL DIRTYREAD(out(1), 200)
      out(-10) = varcom
      DO i=0,100,10
        out(i) = 2*out(i) + COS(0.1*i)
      ENDDO
      END

C
      SUBROUTINE DIRTYWRITE(a, len)
      IMPLICIT NONE
      INTEGER a(*), len, i
      INTEGER bigbuf(10000)
      COMMON /bigcommon/ bigbuf
      INTEGER varcom
      COMMON /comvarcom/ varcom
C
      bigbuf(10000) = varcom
      DO i=1,len
        bigbuf(i) = a(i)
      ENDDO
      END

C
      SUBROUTINE DIRTYREAD(a, len)
      IMPLICIT NONE
      INTEGER a(*), len, i
      INTEGER bigbuf(10000)
      COMMON /bigcommon/ bigbuf
      INTEGER varcom
      COMMON /comvarcom/ varcom
C
      varcom = bigbuf(10000)
      DO i=1,len
        a(i) = bigbuf(i)
      ENDDO
      END

