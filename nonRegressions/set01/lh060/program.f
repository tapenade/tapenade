
      subroutine INVERT(neq,y,savf,FX3,FX4)

      external FX3
      integer neq
      double precision y, savf, tn
      dimension neq(1), y(*), savf(*)
      common /ls0001/tn

      call FX3(neq, tn, y, savf)
      call FX4(neq, tn, y, savf, FX3)
      call FX3(neq, tn, y, savf)

      END
