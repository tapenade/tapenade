C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
      SUBROUTINE BUGEQUIV(c)
      IMPLICIT NONE
C Pour reproduire un bug d'initialisation d'une derivee en trop!
C Situation: 1) Le debut de a est equivalence avec temp dans
C              bugequiv(), mais pas dans FF()
C            2) seul temp est utilise, mais a est a priori actif.
      REAL a(1000, 2000), b, c(10)
      COMMON /ccc/ a, b
      REAL temp(1000), x1(2000), x2, x3
      INTEGER n
      EQUIVALENCE (a(1, 1), temp)
      INTEGER i
      REAL FF
C
      DO i=1,n
        x1(i) = FF(temp(i))
      ENDDO
      b = b*x1(n-8)
C
      END

C
      FUNCTION FF(x)
      IMPLICIT NONE
      REAL a(1000, 2000), b
      COMMON /ccc/ a, b
      REAL x, ff
      ff = 2*x
      END

