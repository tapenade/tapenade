      subroutine bugequiv(c)
c Pour reproduire un bug d'initialisation d'une derivee en trop!
c Situation: 1) Le debut de a est equivalence avec temp dans
c              bugequiv(), mais pas dans FF()
c            2) seul temp est utilise, mais a est a priori actif.
      real  a(1000,2000), b,c(10)
      COMMON /CCC/a,b
      real temp(1000),x1(2000),x2,x3
      integer n
      equivalence (a(1,1),temp)
c
      Do i = 1,n
         x1(i) = FF(temp(i))
      enddo
      b = b*x1(n-8)
c
      end

      function FF(x)
      real  a(1000,2000), b
      COMMON /CCC/a,b
      real x,FF
      FF = 2*x
      end
