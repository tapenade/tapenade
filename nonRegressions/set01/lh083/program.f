      subroutine aa(X,Y)
c pour tester les PUSH/POP dans le cas d'un sous-programme appele
c qui reste pour le forward sweep, mais pas dans le backward sweep !
      real X(100), Y(100)
      integer i,j
      j = 5
      do i = 1,10
         j = j+2
         X(j) = X(j)*Y(i)
         call modify(j)
      enddo
      return
      end

      subroutine modify(n)
      integer n
      n = 2*n+1
      return
      end
