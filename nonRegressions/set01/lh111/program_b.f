C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of top in reverse (adjoint) mode:
C   gradient     of useful results: t u v w
C   with respect to varying inputs: t u v w
C   RW status of diff variables: t:incr u:incr v:in-out w:incr
      SUBROUTINE TOP_B(t, tb, u, ub, v, vb, w, wb)
      IMPLICIT NONE
      REAL t(1000), u(1000), v(1000), w(1000)
      REAL tb(1000), ub(1000), vb(1000), wb(1000)
      REAL l1(1000), l2(1000), l3(1000)
      REAL l1b(1000), l2b(1000), l3b(1000)
C To test the difference between the possible
C formulas for the snapshots (for checkpointing)
C We are testing new formulas that should avoid
C useless storage of arrays that are only used.
      CALL MAKE_L1(t, u, l1)
      CALL USE1_L1(t, u, l1, l2)
      CALL PUSHREAL4ARRAY(l2, 1000)
      CALL USE2_L1(t, v, l1, l2, l3)
C
      CALL OVERW_L1_B(v, vb, w, wb, l1, l1b, l2, l2b, l3, l3b)
      CALL POPREAL4ARRAY(l2, 1000)
      CALL USE2_L1_B(t, tb, v, vb, l1, l1b, l2, l2b, l3, l3b)
      CALL USE1_L1_B(t, tb, u, ub, l1, l1b, l2, l2b)
      CALL MAKE_L1_B(t, tb, u, ub, l1, l1b)
      END

C  Differentiation of make_l1 in reverse (adjoint) mode:
C   gradient     of useful results: t u l1
C   with respect to varying inputs: t u
C
      SUBROUTINE MAKE_L1_B(t, tb, u, ub, l1, l1b)
      IMPLICIT NONE
      REAL t(1000), u(1000), l1(1000)
      REAL tb(1000), ub(1000), l1b(1000)
      INTEGER i
      DO i=999,2,-1
        tb(i-1) = tb(i-1) + u(i+1)*l1b(i)
        ub(i+1) = ub(i+1) + t(i-1)*l1b(i)
        l1b(i) = 0.0
      ENDDO
      END

C  Differentiation of use1_l1 in reverse (adjoint) mode:
C   gradient     of useful results: t u l1 l2
C   with respect to varying inputs: t u l1
C
      SUBROUTINE USE1_L1_B(t, tb, u, ub, l1, l1b, l2, l2b)
      IMPLICIT NONE
      REAL t(1000), u(1000), l1(1000), l2(1000)
      REAL tb(1000), ub(1000), l1b(1000), l2b(1000)
      INTEGER i
      DO i=999,2,-1
        tb(i-1) = tb(i-1) + l1(i+1)*l2b(i)
        l1b(i+1) = l1b(i+1) + t(i-1)*l2b(i)
        ub(i) = ub(i) + l2b(i)
        l2b(i) = 0.0
      ENDDO
      END

C  Differentiation of use2_l1 in reverse (adjoint) mode:
C   gradient     of useful results: t v l1 l2 l3
C   with respect to varying inputs: t v l1 l2
C
      SUBROUTINE USE2_L1_B(t, tb, v, vb, l1, l1b, l2, l2b, l3, l3b)
      IMPLICIT NONE
      REAL t(1000), v(1000), l1(1000), l2(1000), l3(1000)
      REAL tb(1000), vb(1000), l1b(1000), l2b(1000), l3b(1000)
      INTEGER i
      DO i=2,999
        l3(i) = l2(i-1)*l1(i+1)
        CALL PUSHREAL4(l2(i))
        l2(i) = l3(i)*(t(i)+v(i))
      ENDDO
      DO i=999,2,-1
        CALL POPREAL4(l2(i))
        l3b(i) = l3b(i) + (t(i)+v(i))*l2b(i)
        tb(i) = tb(i) + l3(i)*l2b(i)
        vb(i) = vb(i) + l3(i)*l2b(i)
        l2b(i) = 0.0
        l2b(i-1) = l2b(i-1) + l1(i+1)*l3b(i)
        l1b(i+1) = l1b(i+1) + l2(i-1)*l3b(i)
        l3b(i) = 0.0
      ENDDO
      END

C  Differentiation of overw_l1 in reverse (adjoint) mode:
C   gradient     of useful results: v w
C   with respect to varying inputs: v w l1 l2 l3
C
      SUBROUTINE OVERW_L1_B(v, vb, w, wb, l1, l1b, l2, l2b, l3, l3b)
      IMPLICIT NONE
      REAL v(1000), w(1000), l1(1000), l2(1000), l3(1000)
      REAL vb(1000), wb(1000), l1b(1000), l2b(1000), l3b(1000)
      INTEGER i
      INTEGER ii1
      DO ii1=1,1000
        l1b(ii1) = 0.0
      ENDDO
      DO ii1=1,1000
        l2b(ii1) = 0.0
      ENDDO
      DO ii1=1,1000
        l3b(ii1) = 0.0
      ENDDO
      DO i=999,2,-1
        wb(i-1) = wb(i-1) + w(i+1)*vb(i)
        wb(i+1) = wb(i+1) + w(i-1)*vb(i)
        l1b(i) = l1b(i) - vb(i)
        vb(i) = l1(i)*l1b(i)
        l2b(i) = l2b(i) + l3(i)*l1b(i)
        l3b(i) = l3b(i) + l2(i)*l1b(i)
        l1b(i) = v(i)*l1b(i)
      ENDDO
      END
C

