      subroutine top(T,U,V,W)
      real T(1000), U(1000), V(1000), W(1000)
      real L1(1000), L2(1000), L3(1000)
c To test the difference between the possible
c formulas for the snapshots (for checkpointing)
C We are testing new formulas that should avoid
c useless storage of arrays that are only used.
      call make_L1(T,U,L1)
      call use1_L1(T,U,L1,L2)
      call use2_L1(T,V,L1,L2,L3)
      call overw_L1(V,W,L1,L2,L3)
c
      end

      subroutine make_L1(T,U,L1)
      real T(1000), U(1000), L1(1000)
      integer i
      do i = 2,999
         L1(i) = T(i-1)*U(i+1)
      enddo
      end

      subroutine Use1_L1(T,U,L1,L2)
      real T(1000), U(1000), L1(1000), L2(1000)
      integer i
      do i = 2,999
         L2(i) = T(i-1)*L1(i+1) + U(i)
      enddo
      end

      subroutine Use2_L1(T,V,L1,L2,L3)
      real T(1000), V(1000), L1(1000), L2(1000), L3(1000)
      integer i
      do i = 2,999
         L3(i) = L2(i-1)*L1(i+1) 
         L2(i) = L3(i)*(T(i)+V(i))
      enddo
      end

      subroutine overw_L1(V,W,L1,L2,L3)
      real V(1000), W(1000), L1(1000), L2(1000), L3(1000)
      integer i
      do i = 2,999
         L1(i) = L1(i)*V(i) + L2(i)*L3(i)
         V(i) = W(i-1)*W(i+1) - L1(i)
      enddo
      end
     

