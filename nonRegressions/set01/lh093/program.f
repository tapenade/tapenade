C To test warning messages about I-O on active variables.
C Bug: messages are missing for c,d and e because the detection
C  comes too late and activity has already been simplified!
      subroutine testIOmess(a,b,c,d,e)
      real a(100),b(100),c,d,e,t(100)
      integer i
c
      read *,b(34:66:2)
      write (22,*), (a(i),i=1,100,3)
      read (22,*), (t(i),i=0,33)
      b(1:33) = a(34:66)*t(1:33)
c
      write (22,*), c
      c = 0.0
      read (22,*), c
      d = d*c
      read *,e
c
      end
