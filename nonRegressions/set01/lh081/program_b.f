C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.7 (r4904M) - 18 Jun 2013 18:35
C
C  Differentiation of test2 in reverse (adjoint) mode:
C   gradient     of useful results: a
C   with respect to varying inputs: a
C   RW status of diff variables: a:in-out
C
      SUBROUTINE TEST2_B(a, ab, F, JAC, JAC_B, PJAC, PJAC_B)
      IMPLICIT NONE
      EXTERNAL F, JAC, PJAC
      EXTERNAL JAC_B, PJAC_B
      REAL a
      REAL ab
      CALL TEST_B(a, ab, F, JAC, JAC_B, PJAC, PJAC_B)
      END

C  Differentiation of test in reverse (adjoint) mode:
C   gradient     of useful results: y
C   with respect to varying inputs: y
C
      SUBROUTINE TEST_B(y, yb, F, JAC, JAC_B, PJAC, PJAC_B)
      IMPLICIT NONE
      EXTERNAL F, JAC, PJAC
      EXTERNAL JAC_B, PJAC_B
      REAL y
      REAL yb
      CALL PJAC_B(y, yb, JAC, JAC_B)
      END

