      SUBROUTINE VCURVM(nf1, noe1, nsfac, sigma, ce, vnsig, vnfac, ua)
c 7th test on II-LOOPS : nested II-Loops with dead-loops
      integer nf1,noe1(99),nsfac(3,99)
      real*8 sigma(99),ce(5,99),vnsig(99),vnfac(2,99),ua(4,99)

      integer if1, ifac, j, is
      real*8 pm3, vnx, ro,bss

      IF (nf1 .GT. 0) THEN
C$AD II-LOOP
         DO if1=3,nf1
            ifac         = noe1(if1)
            pm3          = 2.0 * sigma(ifac)
            CE(5,if1)    = CE(5,if1) - vnsig(ifac)*pm3
         ENDDO
      ENDIF

      bss=0.0

      IF (nf1 .GT. 10) THEN
C$AD II-LOOP
         DO if1=1,nf1
            ifac         = noe1(if1)
            vnx          = vnfac(1,ifac)
C$AD II-LOOP
            DO j=1,3    
               is        = nsfac(j,ifac)
               ro        = ua(1,is)*ua(4,is)
               CE(5,is)  = CE(5,is) - ro*ro + vnx*vnx
            ENDDO
            bss = bss+CE(5,ifac)
         ENDDO
      ENDIF

      CE(5,2) = CE(5,2)+bss

      END
