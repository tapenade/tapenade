C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 21 Nov 2022 11:22
C
C  Differentiation of testinitadj in forward (tangent) mode:
C   variations   of useful results: x y
C   with respect to varying inputs: x
C   RW status of diff variables: x:in-out y:out
C Programme qui resume le bug sur l'exemple Selmin-Alenia:
C manque une reinitialisation de l'adjoint de y a la fin
C de chaque boucle de la passe backward.
C Aussi: bug de mauvais branchement de la passe backward
C a la fin de la passe forward.
      SUBROUTINE TESTINITADJ_D(x, xd, y, yd)
      IMPLICIT NONE
      REAL x, y
      REAL xd, yd
C
 100  yd = 2*x*xd
      y = x*x
      xd = 2*yd
      x = y*2
      IF (y .GT. 0.0) GOTO 100
C

      END

