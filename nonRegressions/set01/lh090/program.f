C Programme qui resume le bug sur l'exemple Selmin-Alenia:
C manque une reinitialisation de l'adjoint de y a la fin
C de chaque boucle de la passe backward.
C Aussi: bug de mauvais branchement de la passe backward
C a la fin de la passe forward.
      subroutine testInitAdj(x,y)
      real x,y
c
 100  y = x*x
      x = y*2
      if (y.gt.0.0) goto 100 
c
      end
