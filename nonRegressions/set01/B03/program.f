C Example from Mike Giles/Oxford 20/3/3
C    the INDEPENDENT are: qpi2 fn vres6 qli1 qli2 qi1 qi2 qpi1
C    the DEPENDENT   are: fn vres6
      subroutine viscflux (npdes,ewt,ubn,nl,beta,     
     &      xi1,qi1,xdoti1, qli1,qpi1,disti1,epst,
     &      xi2,qi2,xdoti2, qli2,qpi2,disti2,vres6,
     &      fn,second_order)

c independents qi1, qi2, qli1, qli2, qpi1, qpi2
c dependents vres6, fn

      implicit  none
c     arguments
      integer*4 npdes,nl
      real*8 ewt(3),ubn,beta,
     &     xi1(3), qi1(7), xdoti1(3), 
     &     qli1(7),qpi1(7,3),disti1,
     &     xi2(3), qi2(7), xdoti2(3),
     &     qli2(7),qpi2(7,3),disti2
C     output arguments: fn(7), vres6

c********************************************************************c
c                                                                    c
c     COMMON blocks of constants used by all routines                c
c                                                                    c
c********************************************************************c
c
c---- Files ---------------------------------------------------------c
c
      integer*4    maxdata,    mpdes
      parameter   (maxdata=500,mpdes=7)
c
      common / files / inputfile, gridfile(10), flowfile, flowfileuns,
     &                 flowfilelin, flowfileadj, flowfileadjh,
     &                 histfile, funcfile(2), option, 
     &                 casetitle, code, data(maxdata)
c
      character*80     inputfile, gridfile, flowfile, flowfileuns,
     &                 flowfilelin, flowfileadj, flowfileadjh, 
     &                 histfile, funcfile, option,
     &                 casetitle, code, data
c
c  inputfile    -- formatted input file
c  gridfile     -- unformatted grid files for each multigrid level
c  flowfile     -- unformatted flow file for finest grid
c  flowfileuns  -- unformatted nonlinear unsteady flow file
c  flowfilelin  -- unformatted linear unsteady flow file
c  flowfileadj  -- unformatted adjoint flow file
c  flowfileadjh -- unformatted harmonic adjoint flow file
c  histfile     -- formatted file for iteration history
c  funcfile     -- formatted files for output functions
c  option       -- output option (none, grafic, Visual3, pV3, pV3mg)
c  casetitle    -- case identifier
c  code         -- name of code being run
c  data         -- data from input.dat
c
c---- Logicals ------------------------------------------------------c
c
      common / logics / h2d, annular, sa_model, ke_model, wall_fn,
     &                  moving, low_mach, pswitch, harten,
     &                  fmg2ndo, cfl_ramp, sa_strain, a3_2ndo,
     &                  tang_sw, gmres, res_corr, rpm
c
      logical           h2d, annular, sa_model, ke_model, wall_fn,
     &                  moving, low_mach, pswitch, harten,
     &                  fmg2ndo, cfl_ramp, sa_strain, a3_2ndo,
     &                  tang_sw, gmres, res_corr, rpm
c
c  h2d      -- 2D grid
c  annular  -- 3D annular grid
c  sa_model -- Spalart-Allmaras turbulence model
c  ke_model -- k-epsilon turbulence model
c  wall_fn  -- use wall functions with turbulence model
c  moving   -- moving grid
c  low_mach -- low Mach capability
c  pswitch  -- use pressure switch for 2nd order smoothing
c  harten   -- harten entropy fix
c  fmg2ndo  -- 2nd order accuracy on nl = nl_fmg
c  cfl_ramp -- cfl ramping applied for nstart iters on nl_fmg
c  sa_strain-- strain mod to SA prod term on
c  a3_2ndo  -- second order smoothing for entropy characteristic
c  tang_sw  -- use velocity switch for tangential momentum smoothing
c  res_corr -- use boundary residuals to correct functionals
c
c---- Multigrid -----------------------------------------------------c
c
      common / multi / reslev(10),
     &                 nlevel,     nl_crs,     nl_fmg,
     &                 npre,       npost,      ncrs,
     &                 ncycle(10), nitsav,     nprint,
     &                 nstart,     ncyfmg(10), debug,  
     &                 cycle
c
      real*8           reslev
      integer*4        nlevel,     nl_crs,     nl_fmg,
     &                 npre,       npost,      ncrs,
     &                 ncycle,     nitsav,     nprint,
     &                 nstart,     ncyfmg
      logical          debug
      character*1      cycle
c
c  nlevel -- number of multigrid levels
c  nl_crs -- finest grid level for first order smoothing
c  nl_fmg -- initial finest grid level in FMG startup
c  npre   -- number of smoothing iteration before restriction
c  npost  -- number of smoothing iterations after prolongation
c  ncrs   -- number of smoothing iterations on coarsest grid
c  ncycle -- maximum number of multigrid cycles on finest level
c  nitsav -- number of cycles between calls to output routine
c  nprint -- number of cycles between printing residual
c  reslev -- residual level for termination in FMG startup
c  nstart -- number of fine grid iterations for CFL ramping
c  ncyfmg -- number of cycles for termination in FMG startup
c  debug  -- logical flag for debugging
c  cycle  -- multigrid cycle type (V, W)
c
c
c---- Runge-Kutta ---------------------------------------------------c
c
      common / RK / alfas(6), betas(6), cfl, cfl_init, nstep
c
      integer*4     nstep
      real*8        alfas, betas, cfl, cfl_init
c
c  alfas  -- RK multistep coefficients for inviscid fluxes
c  betas  -- RK multistep coefficients for viscous/smoothing fluxes
c  cfl    -- CFL number
c  cfl_init -- Stored CFL for initial ramping
c  nstep   -- number of RK steps
c
c
c---- Sutherlands Law and gas constants -----------------------------c
c
      common / gas / muref, slaw, prli, prti, gam, gm1, gm1i, gamgm1i,
     &               rref, tref, pref, uref,
     &               aref, mfree, pfree, ldvec(3,2)
c
      real*8         muref, slaw, prli, prti, gam, gm1, gm1i, gamgm1i,
     &               rref, tref, pref, uref,
     &               aref, mfree, pfree, ldvec
c
c  muref  -- reference viscosity (inverse Reynolds number)
c  slaw   -- Sutherlands Law constant
c  prli   -- inverse laminar Prandtl number
c  prti   -- inverse turbulent Prandtl number
c  gam    -- gamma
c  gm1    -- gamma-1
c  gm1i   -- 1/(gamma-1)
c  gamgm1i-- gamma/(gamma-1)
c  rref   -- reference density
c  tref   -- reference temperature
c  pref   -- reference pressure
c  uref   -- reference velocity
c  aref   -- Reference area for force coeffs Cx, Cy, Cz
c  mfree  -- freestream Mach number
c  pfree  -- freestream pressure
c  ldvec  -- unit vectors for lift/drag calculations
c
c
c---- Boundary condition data ---------------------------------------c
c
      integer*4  maxgrp,     maxgrp2
      parameter (maxgrp=200, maxgrp2=1000)
c
      common / bcdata / bcs_r(maxgrp2), bcs_i(maxgrp), grptyp(maxgrp),
     &                  off_r(maxgrp),  off_i(maxgrp), grplbl(maxgrp),
     &                  ngrp
c
      integer*4         bcs_i, grptyp, off_r, off_i, ngrp
      real*8            bcs_r
      character*80      grplbl
c
c  bcs_r  -- real data
c  bcs_i  -- integer data
c  off_r  -- offset for real data
c  off_i  -- offset for integer data
c  grptyp -- surface group types:  1 = inviscid wall
c                                  2 = viscous wall
c                                  3 = frestream
c                                  4 = subsonic inflow
c                                  5 = subsonic outflow
c                                  6 = supersonic inflow
c                                  7 = supersonic outflow
c  grplbl -- surface group label
c  ngrp   -- number of groups
c
c
c---- Circumferential line data -------------------------------------c
c
      integer*4  maxcline
      parameter (maxcline=100)
c
      common / cldata / rcline(maxcline),btar(maxcline),
     &                  ncline, mcline(maxcline), tcline(maxcline),
     &                  outang(maxcline),outanglin(maxcline)
c
      integer*4         ncline, mcline, tcline
      real*8            rcline, btar,outang,outanglin
c
c  ncline -- number of circumferential lines
c  mcline -- surface group mask
c  tcline -- integer: 1 --> fixed x,  4 --> fixed r
c  rcline -- fixed x or r value
c
c---- Flow minima ---------------------------------------------------c
c
      common / flow / qmin(7)
c
      real*8          qmin
c
c  qmin -- minimum flow values 
c
c
c---- Periodic info -------------------------------------------------c
c
      common / periodic / pi, alpha, r22, r23, r32, r33, ibpa, ca, sa,
     &                    pitch
c
      real*8              pi, alpha, r22, r23, r32, r33, ibpa, ca, sa,
     &                    pitch
c
c  pi        -- Pi
c  alpha     -- rotation angle for periodic boundary 
c  (r22 r23)
c  (r32 r33) -- v,w rotation matrix
c  ibpa      -- inter-blade phase angle for linear unsteadiness
c  ca        -- cos(ibpa)
c  sa        -- sin(ibpa)
c  pitch     -- periodic shift for 2D and 3D linear cascades
c
c
c---- Unsteady conditions -------------------------------------------c
c
      common / unsteady / omega, dti, theta_inv, ntime, nsave
c
      real*8              omega, dti, theta_inv
      integer*4           ntime, nsave
c
c  omega     -- unsteady frequency
c  dti       -- inverse timestep for nonlinear unsteady calculations
c  theta_inv -- inverse of theta parameter for implicit discretisation
c               (1=backward Euler; 2=Crank-Nicholson)
c  ntime     -- number of nonlinear unsteady timesteps
c  nsave     -- how often to save the nonlinear unsteady flow field
c
c---- Rotating terms -------------------------------------------------c
c
      common / rotation / rot
c
      real*8              rot
c
c  rot -- rate of rotation of coordinate system about x-axis
c
c
c---- Spalart-Allmaras turbulence model -----------------------------c
c
      common / sa / sa_sigmai, sa_kappa2, sa_cb1, sa_cb2,
     &              sa_cw1, sa_cw2, sa_cw36, sa_cv13, sa_cv2,
     &              sa_ct3, sa_ct4, sa_fwcnst, sa_kappa, sa_elogl,
     &              sa_blogl, sa_ekb, sa_cw1kap2, sa_pow6
c
      real*8        sa_sigmai, sa_kappa2, sa_cb1, sa_cb2,
     &              sa_cw1, sa_cw2, sa_cw36, sa_cv13, sa_cv2,
     &              sa_ct3, sa_ct4, sa_fwcnst, sa_kappa, sa_elogl,
     &              sa_blogl, sa_ekb, sa_cw1kap2, sa_pow6
c
c all constants correspond to those in Spalart-Allmaras papers
c with  sigmai = 1/sigma
c       kappa2 = kappa**2
c       cw36   = cw3**6
c and   cv13   = cv1**3
c
c
c---- Odds and ends -------------------------------------------------c
c
      common / odds / eps1, eps2, eps3, eps4, 
     &                eps4l(10), eps5, eps5l(10), nswitch, rms1
c
      real*8          eps1, eps2, eps3, eps4, eps4l, eps5, eps5l, rms1
      integer*4       nswitch
c
c  eps1 -- edge length exponent in defining pseudo-Laplacian
c  eps2 -- factor in smoothing
c  eps3 -- factor in pressure switch
c  eps4 -- factor in harten fix
c  eps4l-- factor in entropy correction for all grid levels
c  eps5 -- factor in entropy correction for current grid level
c  eps5l-- factor in entropy correction for all grid levels
c  nswitch -- 1st order artificial dissipation switch
c
c
c---- debugging stuff ----------------------------------------------c
c
      common / debug_info / x_dbg(3), in_dbg
c
      integer*4             in_dbg
      real*8                x_dbg
c
c---- iteration history info
c
      common / hist_info / rms(mpdes+4), dmx(4), resjc27(4), fsum(2)
c
      real*8               rms,          dmx,    resjc27,    fsum
c---- GMRES info
c
      integer*4    mkrylov, mout
      parameter   (mkrylov=155,mout=30)
c
      common / gmres_info / nrest, nkrylov, nout
c
      integer*4             nrest, nkrylov, nout
c
c---- Functionals
c   
      common / funcs / btar2d, btar3d, nbtar, file4adjf, file4linf
c
      real*8 btar2d
      integer*4 nbtar
      character*80 btar3d,file4adjf,file4linf

      integer*4 istart,ifinish,ic,ipde,in,ie,i1,i2
      real*8
     &          rd1,r1,u1,v1,w1,p1,c1,un1,nu1, rl1,ul1,vl1,wl1,pl1,nul1,
     &          rd2,r2,u2,v2,w2,p2,c2,un2,nu2, rl2,ul2,vl2,wl2,pl2,nul2,
     &          rd, r, u, v, w, p, c, un, nu, ri,t,rc,cc,qq,h, 
     &          nx,ny,nz,aa, del,dx,dy,dz,di, 
     &          div, txx,tyy,tzz,txy,txz,tyz, txn,tyn,tzn, 
     &          rn,pn,tn,qn, nun,
     &          mul,mut,chi3,fv1, rnu,con,cn1,cn2, 
     &          delr,delut,delvt,delwt,qdelq,delp,delc,delun,delnu, 
     &          delq, ret, dmin, dmax,
     &          argu,argp,sw,swc, em1,em2,em3,em4, dele1,dele2,dele3,
     &          ec3,a1,a2,a3,d12,a123,b123,fac,
     &          teta, tau, sp, sm, tau1, tau2, deltau,
     &          fn(7), grad(7,3), eps, epst, muwf,vres6(2),
     &          args, arg1, arg2, sw3, swc3, delp3, sw4, swc4
c
      real*8    trat,trat_ret,yplus 
      logical   second_order
c
c.... external complex functions
c

c
c         get face area, normal and grid velocity
c         ---------------------------------------
          nx = ewt(1)
          ny = ewt(2)
          nz = ewt(3)
          aa = sqrt(nx**2 + ny**2 + nz**2)
          nx = nx / aa
          ny = ny / aa
          nz = nz / aa
c
          if(moving) then
            ubn = 0.5d0*(xdoti1(1)*nx+xdoti1(2)*ny+xdoti1(3)*nz
     &                  +xdoti2(1)*nx+xdoti2(2)*ny+xdoti2(3)*nz)
          endif
c
c         set variables
c         -------------
          rd1 = xi1(2)**2 + xi1(3)**2
          r1  = qi1(1)
          u1  = qi1(2)
          v1  = qi1(3)
          w1  = qi1(4)
          p1  = qi1(5)
          c1  = sqrt(gam*p1/r1)
          un1 = u1*nx + v1*ny + w1*nz - ubn
c
          rd2 = xi2(2)**2 + xi2(3)**2
          r2  = qi2(1)
          u2  = qi2(2)
          v2  = qi2(3)
          w2  = qi2(4)
          p2  = qi2(5)
          c2  = sqrt(gam*p2/r2)
          un2 = u2*nx + v2*ny + w2*nz - ubn
c
c         get averages of variables
c         -------------------------
          rd = 0.5d0*(rd1 + rd2)
          r  = 0.5d0*( r1 +  r2)
          u  = 0.5d0*( u1 +  u2)
          v  = 0.5d0*( v1 +  v2)
          w  = 0.5d0*( w1 +  w2)
          un = 0.5d0*(un1 + un2)
          p  = 0.5d0*( p1 +  p2)
          ri = 1.d0 / r
          cc = gam*p*ri
          c  = sqrt(cc)
          rc = r*c
          qq = 0.5d0*(u**2+v**2+w**2 - rd*rot**2)
          h  = gm1i*cc + qq
c
c         low mach treatment
c         ------------------
c
c         regular treatment
c         -----------------

c
c           get |eigenvalues| + Entropy fix
c           ---------------------------------
            delun = un2 - un1
            delc  =  c2 -  c1
c
            dele1 = 2.d0*(delun-delc)
            dele2 = 2.d0*(delun+delc)
            dele3 = 2.d0*abs(delun)
c
c.......... M.West 21/08/00 Apply van Leer fix in same manner as Harten fix
c           ---------------------------------------------------------------
            em1 = abs(un-c)
            eps = max(dele1,eps4*c)
            if(em1.lt.eps) then
              em1 = 0.5d0*(em1*em1/eps + eps)
            endif
c
            em2 = abs(un+c)
            eps = max(dele2,eps4*c)
            if(em2.lt.eps) then
              em2 = 0.5d0*(em2*em2/eps + eps)
            endif
c
            em3 = abs(un  )
            eps = max(dele3,eps4*c)
            if(em3.lt.eps) then
              em3 = 0.5d0*(em3*em3/eps + eps)
            endif   
c
            em4 = abs(un  )
            eps = max(dele3,eps5*c)
            if(em4.lt.eps) then
              em4 = 0.5d0*(em4*em4/eps + eps)
            endif            
c
c         set pseudo-Laplacians
c         ---------------------
          rl1 = qli1(1)
          ul1 = qli1(2)
          vl1 = qli1(3)
          wl1 = qli1(4)
          pl1 = qli1(5)
c
          rl2 = qli2(1)
          ul2 = qli2(2)
          vl2 = qli2(3)
          wl2 = qli2(4)
          pl2 = qli2(5)
c
c         set switch
c         ----------
          sw  = 1.d0
          sw3 = 1.d0
          sw4 = 1.d0
c
c........ M.West 21/08/00 Use nswitch to set switch
c         -----------------------------------------
          if(second_order) then
c
c           Pressure switch
c           ---------------
            if (nswitch.eq.1) then
              argp = abs(pl1)/(abs(pl1)+2.d0*p1)
     &             + abs(pl2)/(abs(pl2)+2.d0*p2)
              sw   = min(sw, eps3*argp)
c
c           Velocity switch
c           ---------------
            else if(nswitch.eq.2) then
              argu = ((nx*(u2-u1)+ny*(v2-v1)+nz*(w2-w1)) / c)**2
              sw   = min(sw, eps3*argu)
c
c           Combined switch
c           ---------------
            else if(nswitch.eq.3) then
              argp = abs(pl1)/(abs(pl1)+2.d0*p1)
     &             + abs(pl2)/(abs(pl2)+2.d0*p2)
              argu = ((nx*(u2-u1)+ny*(v2-v1)+nz*(w2-w1)) / c)**2
              sw   = min(sw, eps3*max(argp,argu))
            endif
c
c           M.West 19/01/01 Entropy Switch and Tang Momentum Switch
c           -------------------------------------------------------
            sw4 = sw
            if(a3_2ndo.or.tang_sw) then
              arg1 = abs(pl1) + c1*c1*abs(rl1)
              arg2 = abs(pl2) + c2*c2*abs(rl2)
              args = arg1/(arg1+2.d0*p1) + arg2/(arg2+2.d0*p2)
              if (a3_2ndo) sw3  = min(sw3,  eps3*args)
              if (tang_sw) sw4  = min(1.d0, eps3*args)
            endif
          endif
c
          swc  = eps2*(1.d0 - sw)
          swc3 = eps2*(1.d0 - sw3)
          swc4 = eps2*(1.d0 - sw4)
c
c         get differences of Q
c         --------------------
c
          delut = sw4*(u2 - u1) - swc4*(ul2 - ul1)
          delvt = sw4*(v2 - v1) - swc4*(vl2 - vl1)
          delwt = sw4*(w2 - w1) - swc4*(wl2 - wl1)
          delun = delut*nx + delvt*ny + delwt*nz
          delut = delut - delun*nx
          delvt = delvt - delun*ny
          delwt = delwt - delun*nz
          qdelq = u*delut+v*delvt+w*delwt
c
          delun = (sw*(u2 - u1) - swc*(ul2 - ul1))*nx
     &          + (sw*(v2 - v1) - swc*(vl2 - vl1))*ny
     &          + (sw*(w2 - w1) - swc*(wl2 - wl1))*nz
          delp  =  sw*(p2 - p1) - swc*(pl2 - pl1)
c
          delr  = sw3*(r2 - r1) - swc3*(rl2 - rl1)
          delp3 = sw3*(p2 - p1) - swc3*(pl2 - pl1)
c
c         low Mach treatment
c         ------------------
c
c         regular treatment
c         -----------------
c
c           characteristic variables
c           ------------------------
            a1 = 0.5d0*em1*(delp - rc*delun)
            a2 = 0.5d0*em2*(delp + rc*delun)
c
c.......... M.West 28/11/00 First order treatment or second order with 
c           entropy switch for the entropy wave and using em4 with harten fix
c           -----------------------------------------------------------------
            a3 =      -em4*(delp3- cc*delr)
c
c           bits and bobs
c           -------------
            a123 = a1 + a2 + a3
            b123 = (a1 + a2)*h + a3*qq
            d12  = (a2 - a1)*c
            ec3  = rc*c*em3
c
c           1/2 area * |A| (Q2 - Q1)
c           ------------------------
            fac = 0.5d0*aa/cc
            fn(1) = -fac*(a123)
            fn(2) = -fac*(a123*u + d12*nx       + ec3*delut)
            fn(3) = -fac*(a123*v + d12*ny       + ec3*delvt)
            fn(4) = -fac*(a123*w + d12*nz       + ec3*delwt)
            fn(5) = -fac*(b123   + d12*(un+ubn) + ec3*qdelq)
c
c         get average gradient
c         --------------------
          do ipde=1,npdes
            grad(ipde,1) = 0.5d0*(qpi1(ipde,1)+qpi2(ipde,1))
            grad(ipde,2) = 0.5d0*(qpi1(ipde,2)+qpi2(ipde,2))
            grad(ipde,3) = 0.5d0*(qpi1(ipde,3)+qpi2(ipde,3))
          enddo
c
c         get improved gradient
c         ---------------------
          dx = xi2(1) - xi1(1)
          dy = xi2(2) - xi1(2)
          dz = xi2(3) - xi1(3)
          di = 1.d0 / (dx**2 + dy**2 + dz**2)
c
          do ipde=1,npdes  
            del = di * (  grad(ipde,1)*dx
     &                  + grad(ipde,2)*dy
     &                  + grad(ipde,3)*dz
     &                  - (qi2(ipde) - qi1(ipde)) )
            grad(ipde,1) = grad(ipde,1) - del*dx
            grad(ipde,2) = grad(ipde,2) - del*dy
            grad(ipde,3) = grad(ipde,3) - del*dz
          enddo
c
c         calculate laminar viscosity
c         ---------------------------
          t   = ri*p
          mul = muref*t*sqrt(t)/(1.d0 + slaw*(t-1.d0))
c
c         Spalart-Allmaras turbulence model
c         ---------------------------------
          if(sa_model) then
            nu1 = qi1(6)
            nu2 = qi2(6)
c
            nul1 = qli1(6)
            nul2 = qli2(6)
c
            nun  = grad(6,1)*nx + grad(6,2)*ny + grad(6,3)*nz
c
            nu   = 0.5d0*(nu1+nu2)
            rnu  = r*nu
            chi3 = (rnu/mul)**3
            fv1  = chi3 / (chi3+sa_cv13)
            mut  = rnu*fv1
            muwf = mul+mut
c
c ......... wall function modification
c.......... M.West 21/08/00 Use muwf for stresses and mut for heat flux
            if(wall_fn) then
              dmin = min(disti1,disti2)
              dmax = max(disti1,disti2)
c
              if(dmin.eq.0.d0 .and. dmax.gt.0.d0) then
                delq = sqrt((u2-u1)**2+(v2-v1)**2+(w2-w1)**2)
                ret  = r*delq*dmax/mul
                call low(ret,trat,trat_ret,yplus)
                muwf = trat*mul
              endif
            endif
c
            con = sa_sigmai*aa*nun*beta
            cn1 = mul + rnu
            cn2 = 0.5d0*sa_cb2*r*(nu2-nu1)
c
c           set switch
c           ----------
            sw  = 1.d0
            if(second_order) then
              sw  = min(sw,
     &                  (abs(nul1)/(abs(nul1)+2.d0*nu1+epst)
     &                  +abs(nul2)/(abs(nul2)+2.d0*nu2+epst)))
            endif
            swc = eps2*(1.d0 - sw)
c
            delnu =  sw*(nu2 - nu1) - swc*(nul2 - nul1)
c
c.......... M.West 04/09/00 Beta included in con
            vres6(1) =  - (cn1+cn2)*con
            vres6(2) =  + (cn1-cn2)*con
            fn(6)     = -fac*ec3*delnu 
c
c         laminar alternative
c         -------------------
          else
            mut  = 0.d0
            muwf = mul
          endif

c
c         tensor stuff
c         ------------
c
          txx = 2.d0*grad(2,1)
          tyy = 2.d0*grad(3,2)
          tzz = 2.d0*grad(4,3)
          div = (txx+tyy+tzz) / 3.d0
          txx = txx - div
          tyy = tyy - div
          tzz = tzz - div
          txy = grad(2,2) + grad(3,1)
          txz = grad(2,3) + grad(4,1)
          tyz = grad(3,3) + grad(4,2)
c
c........ M.West 21/08/00 muwf used for stresses
          txn = muwf*(txx*nx + txy*ny + txz*nz)
          tyn = muwf*(txy*nx + tyy*ny + tyz*nz)
          tzn = muwf*(txz*nx + tyz*ny + tzz*nz)
c
          rn  = grad(1,1)*nx + grad(1,2)*ny + grad(1,3)*nz
          pn  = grad(5,1)*nx + grad(5,2)*ny + grad(5,3)*nz
          tn  = ri*(pn-t*rn)
          con = (mul*prli + mut*prti) * gamgm1i
          qn  = -con*tn
c  
c         the conservative viscous flux
c         -----------------------------
          fn(2) = fn(2) - aa*txn
          fn(3) = fn(3) - aa*tyn
          fn(4) = fn(4) - aa*tzn
          fn(5) = fn(5) - aa*(u*txn + v*tyn + w*tzn - qn)

      return
      end
