      subroutine bugDeadLoop(A,n)
C Bug vu dans ifremer-opa: le do est adjoint-dead,
C et i est inutile downstream, donc i n'est meme pas
C explicitement calcule a "n+1". Mais il y a
C un PUSH(i-1) pour stocker l'indice de depart
C de la boucle inverse! Donc il faut tout de meme
C calculer i = n+1 !!!
      real A(100)
      integer i,n
c
      do i = 1,n
         A(i) = 2*A(i)
      enddo
      n = 2*n
      A(n) = 3*A(n)
      end
