C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (master) - 18 Apr 2019 18:10
C
C  Differentiation of top in forward (tangent) mode:
C   variations   of useful results: x
C   with respect to varying inputs: x y
C   RW status of diff variables: x:in-out y:in
      SUBROUTINE TOP_D(x, xd, y, yd, n, *)
      IMPLICIT NONE
      INTEGER i, n
      DOUBLE PRECISION x(n), y(n)
      DOUBLE PRECISION xd(n), yd(n)
C
      DO i=1,n
        xd(i) = y(i)*xd(i) + x(i)*yd(i)
        x(i) = x(i)*y(i)
        RETURN 1
      ENDDO
      RETURN
      END

