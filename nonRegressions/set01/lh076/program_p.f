C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
      SUBROUTINE ONEGVERT(pin4, emipint)
      IMPLICIT NONE
      REAL*8 pin4
      COMPLEX*16 emipint
      INTRINSIC DCOS
      INTRINSIC DSIN
      INTRINSIC CMPLX
C
      emipint = CMPLX(DCOS(pin4), -DSIN(pin4))
C
      END
C

