      subroutine onegvert(pin4, emipint)
      implicit none
      real*8 pin4 
      complex*16 emipint

      emipint = cmplx( dcos(pin4),-dsin(pin4) )

      end

