C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of sub0 in forward (tangent) mode:
C   variations   of useful results: y z
C   with respect to varying inputs: x y z
C   RW status of diff variables: x:in y:in-out z:in-out
      SUBROUTINE SUB0_D(x, xd, y, yd, z, zd)
      IMPLICIT NONE
      REAL x, y, z, u, v, w
      REAL xd, yd, zd, ud
C
      ud = y*xd + x*yd
      u = x*y
      IF (x .GT. 0) THEN
        zd = 3*2*u*ud + xd
        z = 3*u**2 + x
        u = 2.0
        ud = 0.0
      END IF
      yd = x*ud + u*xd
      y = u*x
C
      END

