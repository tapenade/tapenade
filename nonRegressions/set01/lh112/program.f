      subroutine top(A,B,T)
C Bug trouve chez Cargill: manque une data-dep entre
C  l'initialisation de initvald et son usage dans "sub_dv"
      real A,B,T,R
      A = sub(T,A)
      if (A.gt.0.0) B = 0.0
      R = sub(273.15, B)
      T = 1/R
      end subroutine

      function sub(T,X)
      real T,X,sub
      sub = X*T
      end function
