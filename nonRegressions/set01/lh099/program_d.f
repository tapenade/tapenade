C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.11 (r6079:6080M) -  3 Jun 2016 17:27
C
C  Differentiation of expandintest in forward (tangent) mode:
C   variations   of useful results: a
C   with respect to varying inputs: a b
C   RW status of diff variables: a:in-out b:in
      SUBROUTINE EXPANDINTEST_D(a, ad, b, bd)
      IMPLICIT NONE
C check that the expansion of abs does not break the loop !!
      REAL a(100), b(100)
      REAL ad(100), bd(100)
      INTEGER i
      INTRINSIC ABS
      REAL abs0
      i = 5
      DO WHILE (.true.)
        IF (a(i) .GE. 0.) THEN
          abs0 = a(i)
        ELSE
          abs0 = -a(i)
        END IF
        IF (abs0 .GT. 10.0) THEN
          ad(i) = 2*bd(i)
          a(i) = 2*b(i)
        ELSE
          EXIT
        END IF
      ENDDO
      END

