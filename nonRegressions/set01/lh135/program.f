      SUBROUTINE METRIC(R, S)
C Encore un bug de mauvais branchement de control-flow entre fwd et bwd sweep
C a cause du slicing de l'adjoint dead code.
      INTEGER I,J,K
      REAL R, S(77,77,77,10,10),vv

C
      R = R*S(1,2,3,4,5)
C
      DO K = 10,20
         DO J = 20,30
            DO I = 30,40
               vv = S(J,K,I-1,1,1)*S(J,K,I-1,4,1)*R
            ENDDO
         ENDDO
      ENDDO
C     
      END
