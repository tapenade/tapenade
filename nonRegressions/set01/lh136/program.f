C----------------------------------------------------------------------
C
C  lighthouse example
C
C----------------------------------------------------------------------                               

      program lighthouse

      double precision x(4), y(2)

      x(1) = 3.7; 
      x(2) = 0.7; 
      x(3) = 0.5; 
      x(4) = 0.5; 

      call eval_f(x, y)

      write(*,*) 'Function value:'
      write(*,*) y(1), y(2)

      end

C----------------------------------------------------------------------                               

      subroutine eval_f(x,y)
C
      double precision x(4), y(2)
C
      double precision v(7)
      INTRINSIC TAN
C
      v(1) = x(3) * x(4)
      v(2) = tan(v(1))
      v(3) = x(2) - v(2)
      v(4) = x(1) * v(2)
      v(5) = v(4)/v(3)
      v(6) = v(5)
      v(7) = v(5) * x(2)
C     
      y(1) = v(6)
      y(2) = v(7)
      end

