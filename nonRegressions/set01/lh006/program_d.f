C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) - 30 Nov 2022 18:54
C
C  Differentiation of adj6 in forward (tangent) mode:
C   variations   of useful results: a b x
C   with respect to varying inputs: a b x y z
C   RW status of diff variables: a:in-out b:in-out x:in-out y:in
C                z:in
      SUBROUTINE ADJ6_D(x, xd, y, yd, z, zd)
      IMPLICIT NONE
      INTEGER nsuite
C      test : boucle do construite avec un if ..goto
C             fonction intrinsic min dans un bloc header
C             fonctions 
C             io calls
C             modification du type avec instruction ;
C               implicit double precision 
      PARAMETER (nsuite=100)
      INTEGER nfile
      PARAMETER (nfile=80)
      CHARACTER*7 namef
      PARAMETER (namef='donnees')
      DOUBLE PRECISION x
      DIMENSION x(2)
      DOUBLE PRECISION xd
      DIMENSION xd(2)
      LOGICAL FUNC1, zexist
      DOUBLE PRECISION a
      DOUBLE PRECISION b
      COMMON /donnees/ a, b
      DOUBLE PRECISION ad
      DOUBLE PRECISION bd
      COMMON /donnees_d/ ad, bd
      INTEGER iter
      INTRINSIC MIN
      INTEGER irang
      DOUBLE PRECISION FUNC2
      DOUBLE PRECISION FUNC2_D
      DOUBLE PRECISION min1
      DOUBLE PRECISION min1d
      LOGICAL result1
      DOUBLE PRECISION tmpresult
      DOUBLE PRECISION y
      DOUBLE PRECISION yd
      DOUBLE PRECISION z
      DOUBLE PRECISION zd
C
      OPEN(1, file=namef, status='old', err=98) 
      ad = 0.D0
      bd = 0.D0
      READ(1, *, err=99) a, b
C
      xd(1) = 0.D0
      x(1) = 0.d0
      xd(2) = 0.D0
      x(2) = 0.d0
      iter = 0
 10   iter = iter + 1
      IF (z .GT. y) THEN
        min1d = yd
        min1 = y
      ELSE
        min1d = zd
        min1 = z
      END IF
      xd(1) = xd(1) + min1d
      x(1) = x(1) + min1
      xd(2) = xd(2) + yd
      x(2) = x(2) + y
      IF (x(1) .LE. y .AND. iter .LE. 100) GOTO 10
C
      PRINT*, 'x(1) x(2) y z iter =', x(1), x(2), y, z, iter
C
C
      xd(1) = 0.D0
      x(1) = 0.0
      DO irang=1,nsuite
        result1 = FUNC1(x(1))
        IF (result1) THEN
          GOTO 30
        ELSE
          xd(1) = FUNC2_D(x(1), xd(1), tmpresult)
          x(1) = tmpresult
        END IF
      ENDDO
      PRINT*, 'erreur pas de solution '
      ad = 0.D0
      bd = 0.D0
      RETURN
 30   PRINT*, 'resultat=', x(1), ' irang=', irang
      ad = 0.D0
      bd = 0.D0
      RETURN
 99   PRINT*, 'probleme a la lecture des donnees sur le', ' fichier  :'
     +      , ' ', namef
      ad = 0.D0
      bd = 0.D0
      GOTO 100
C
C
 98   INQUIRE(file=namef, exist=zexist) 
C      
      IF (.NOT.zexist) THEN
        PRINT*, namef, ':  fichier non trouve', 'coucou'
      ELSE
        PRINT*, namef, ':  fichier existant', ' anomalie a l''ouverture'
      END IF
      RETURN
 100  CONTINUE
      END

C  Differentiation of func2 in forward (tangent) mode:
C   variations   of useful results: func2
C   with respect to varying inputs: x
      DOUBLE PRECISION FUNCTION FUNC2_D(x, xd, func2)
      IMPLICIT NONE
      DOUBLE PRECISION a
      DOUBLE PRECISION b
      COMMON /donnees/ a, b
      DOUBLE PRECISION ad
      DOUBLE PRECISION bd
      COMMON /donnees_d/ ad, bd
      DOUBLE PRECISION func2
      DOUBLE PRECISION x
      DOUBLE PRECISION xd
      func2_d = 2.0*xd
      func2 = 2.0*x + b
      END

