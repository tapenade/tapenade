      subroutine adj6 ( x, y, z ) 
c      test : boucle do construite avec un if ..goto
c             fonction intrinsic min dans un bloc header
c             fonctions 
c             io calls
c             modification du type avec instruction ;
c               implicit double precision 
      implicit double precision ( a-f, o-z )
      parameter ( nsuite    = 100 )  
      parameter ( nfile     =  80 )  
      character *7 namef
      parameter ( namef     = 'donnees' )  
      dimension  x(2)
      logical func1, zexist
      common/ donnees / a, b
c
      open(1,file=namef,status='old',err=98) 
      read(1,*,err=99) a,b
c
      x(1) = 0.d0 
      x(2) = 0.d0 
      iter = 0
 10   iter = iter + 1
      x(1) = x(1)+ min(z,y) 
      x(2) = x(2) + y
      if ( x(1).le.y .and. iter.le.100 ) goto 10 
c
      print*,'x(1) x(2) y z iter =',x(1),x(2),y,z,iter 
c
c
      x(1) = 0.0
      do 20 irang = 1, nsuite
         if ( func1( x(1) ) ) goto 30
         x(1) = func2(x(1))
 20   continue     
      print*, 'erreur pas de solution '
      return
 30   print*, 'resultat=',x(1),' irang=',irang 
      return
c
c
 98   inquire(file=namef,exist=zexist)
c      
      if ( .not. zexist ) then
          print*,namef,':  fichier non trouve',
     &        'coucou'
      else
          print*,namef,':  fichier existant', 
     &        ' anomalie a l\'ouverture'
      endif  
      return 
 99   print*,'probleme a la lecture des donnees sur le' 
     &,' fichier  :',' ',namef
      end 
      logical function func1( x )
      implicit double precision ( a-f, o-z )
      common/ donnees / a, b
        func1 = x .ge. a
      end
      function func2( x )
      implicit double precision ( a-f, o-z )
      common/ donnees / a, b
        func2 = 2.0* x + b 
      end
