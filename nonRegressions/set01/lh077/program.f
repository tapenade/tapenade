c pour tester le bug de l'initialisation a zero des derivees
C de variables nonactives que l'on passe en argument a une routine
c qui les considere comme active: dans le cas vectoriel, yavait un bug!
      subroutine testInit(A,B,C)
      real A(100), B, C
      real L1(100)
      integer i

      do i=1,100
         L1(i) = 1/i
      enddo
      call toto(L1,B,C)
      call toto(A,8.5,C)
      end

      subroutine toto(T,S,R)
      real T(100),S,R
      integer ii

      do ii=1,100
         R = R + T(ii)*T(ii)
      enddo
      R = R*S
      end
