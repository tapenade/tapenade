      subroutine iiloop(A,B,m,n,v1,SUMALL)
C 4e test de non-regression pour
C les Independent-Iterations Loops (II-Loops)
C Les 2 boucles sont mutuellement independantes,
C  donc il est inutile de faire tourner la 1ere a vide!
      integer m,n
      real A(n), B(n), SUMALL
      integer s1(100),s2(100)
      integer i,n1,n2
      real v1,v2,v3,v4,u1,u2

      SUMALL = A(5)*B(5) + v1*v3*i*n1
C$AD II-LOOP
      do i = 1,m
         n1 = s1(i)
         n2 = s2(i)
         v1 = A(n1)
         v2 = A(n2)
         v3 = B(n1)
         v4 = B(n2)
         if (v3.gt.v4) then
            v3 = v3*v4
         else
            v4 = v4/v3
         endif
         u1 = (v1-v2)**2 + (v3-v4)**2
         u2 = v1*(v3-v4)*(v2+u1)
         u2 = ABS(u2)
         v1 = SQRT(u1)
         v2 = SQRT(u2)
         A(n1) = A(n1) + v1*v2
         A(n2) = A(n2) - v1*v2
      enddo
      SUMALL = 2*SUMALL ;
C$AD II-LOOP
      do i = 1,m-2
         n1 = s1(i+1)
         n2 = s2(i+2)
         v3 = B(n1)
         v4 = B(n2)
         v3 = v3*v4
         v4 = B(n1)
         v3 = v3*v4
         A(n2) = A(n2) - v3*v4
      enddo
      SUMALL = 3*SUMALL +1.0 ;

      end

         
