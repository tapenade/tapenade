      SUBROUTINE NL_MODEL_MIE (PP, phase, number, sigma)
c
      DOUBLE PRECISION PP(10,20), PP1(10,20)
      DOUBLE PRECISION phase(10), number(5), sigma(10)
      INTEGER i,j,k
c

      DO i=1,10
         DO j=1,20
            PP1(j,i)=0.0
         ENDDO
         DO j=1,20
            DO k=1,5
               PP1(j,i)=PP1(j,i)+
     +              phase(j)*number(k)
            ENDDO
         ENDDO
         DO j=1,20
            PP(j,i)=sigma(i)*PP1(j,i)
         ENDDO
      ENDDO
c
      END
