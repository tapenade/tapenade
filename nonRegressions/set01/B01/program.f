      subroutine psiroe(ctrl,ctrlno)
c---------------------------------------------------------------------   
c 
c 
c 
c     ce     -->  psi (ctrl,ua)
c     ctrl   -->  gamma
c     ua     -->  w
c 
c 
c 
c---------------------------------------------------------------------   
      INCLUDE 'Param3D.h'
      INCLUDE 'Paramopt3D.h'
c---------------------------------------------------------------------

      integer*4 is,ia
      real*8    ctrlno,ctrl(nnsp)


      DO  is=1,ns
        DO  ia=1,5
          dx(ia,is)            = 0.0
          dy(ia,is)            = 0.0
          dz(ia,is)            = 0.0
        END DO
      END DO

      DO is=1,nsmax
         ce(1,is)                  = 0.d0
         ce(2,is)                  = 0.d0
         ce(3,is)                  = 0.d0
         ce(4,is)                  = 0.d0
         ce(5,is)                  = 0.d0
      END DO

c...  Compute the hermitian nodal gradients
      CALL GRADNOD

c...  Compute the convective fluxes
      CALL FLUROE

      
c...  Compute the boundary conditions

      CALL VCURVM(ctrlno)      
      IF (itrans.eq.1 .and. ctrlno.gt.1.0d-10)
     .  CALL TRANSPIRATION(CE,CTRL)        
      CALL CONDDIRFLUX        
      
      END
      SUBROUTINE CONDDIRFLUX
C
c     Introduction des conditions de Dirichlet:
c     si logfac(ifac)=5 (et logfr(is)=5)   ou   si logfr(is)=1
c
c     On met les flux explicites a 0 en ces noeuds.
c
      include 'Param3D.h'
C
      INTEGER IS, I
C
      DO IS = 1,NS
        IF (LOGFR(IS).EQ.1 .or. LOGFR(IS).EQ.5) THEN
          DO I = 1,5
            CE(I,IS) = 0.d0
          END DO
        ENDIF
      END DO

      END
      SUBROUTINE FLUROE
c---------------------------------------------------------------------   
c Computes the convective fluxes using the approximate Riemann
c solver of Roe adapted to dynamic meshes
c A Van Albada limiter is used in the M.U.S.C.L. procedure
c---------------------------------------------------------------------   
      INCLUDE 'Param3D.h'
c---------------------------------------------------------------------   
c     Local variables definition
      INTEGER is     , iseg   , nubo1  , nubo2
      REAL*8    gamo   , usg0   , pow    , coeff  , dtpred 
      REAL*8    ro     , usro   , u ,v, w, p, rnorm
      REAL*8    aix    , aiy    , aiz    , delta  , switch
      REAL*8    squsr1 , squsr2
      REAL*8    beta2  , beta3  , beta
      REAL*8    dpm    , dpor   , dpex   , aux1   , aux2 , e2 
      REAL*8    xsigm  , vp1    , vp4    , vp5
      REAL*8    uas1(2), uas2(2), uas3(2), uas4(2), uas5(2)
      REAL*8    vno(3)
      REAL*8    prod1  , prod2  , bsign
      REAL*8    tet1   , tet2   , tet3
      REAL*8    cr     , cr2    , ener1  , ener2  , pror , qir
      REAL*8    uar1   , uar2   , uar3   , uar4   , uar5
      REAL*8    dif1   , dif2   , dif3   , dif4   , dif5 
      REAL*8    flur1  , flur2  , flur3  , flur4  , flur5
      REAL*8    fltr1  , fltr2  , fltr3  , fltr4  , fltr5
      REAL*8    flum1  , flum2  , flum3  , flum4  , flum5
c
      bsign                        = 1.0d0
c
      IF (nordre .EQ. 2) bsign     =-1.0d0   
c
      beta                         = 0.5d0
c
      beta2                        = beta
      beta3                        = 0.5d0*(1.0d0 - 2.0d0*beta)
c
      e2                           = 1.0d-16
c
      gamo                         = 0.5d0*(gam - 1.0d0)
      usg0                         = 1.0d0/gamo
      pow                          = 1.0d0/(2.0d0*gam)
      coeff                        = gam1/(gam + 1.0d0)
c
      DO 5 is=1,ns
c
         dtpred                    = 0.5d0*dtl(is)*ipred
c
         ro                        = ua(1,is)
         usro                      = 1.0d0/ro
         u                         = ua(2,is)*usro
         v                         = ua(3,is)*usro
         w                         = ua(4,is)*usro
         p                         = gam1*(ua(5,is) - 
     &                                     0.5d0*ro*(u*u + v*v + w*w))
c
         un(1,is)                  = ro - dtpred*(u*dx(1,is) + 
     &                                            v*dy(1,is) + 
     &                                            w*dz(1,is) + 
     &                             ro*(dx(2,is) + dy(3,is) + dz(4,is)))
         un(2,is)                  = u  - dtpred*(u*dx(2,is) + 
     &                                            v*dy(2,is) + 
     &                                            w*dz(2,is) + 
     &                                            dx(5,is)*usro)
         un(3,is)                  = v  - dtpred*(u*dx(3,is) + 
     &                                            v*dy(3,is) + 
     &                                            w*dz(3,is) +
     &                                            dy(5,is)*usro)
         un(4,is)                  = w  - dtpred*(u*dx(4,is) + 
     &                                            v*dy(4,is) + 
     &                                            w*dz(4,is) +
     &                                            dz(5,is)*usro)
         un(5,is)                  = p  - dtpred*(u*dx(5,is) + 
     &                                            v*dy(5,is) + 
     &                                            w*dz(5,is) +
     &                                            gam*p*(dx(2,is) + 
     &                                                   dy(3,is) + 
     &                                                   dz(4,is)))
c
5     CONTINUE
c
      DO 10 iseg=1,nseg
c
         nubo1                     = nubo(1,iseg)
         nubo2                     = nubo(2,iseg)
c
c        Indirect addressing on vertices physical states
c
         uas1(1)                   = un(1,nubo1)
         uas1(2)                   = un(1,nubo2)
         uas2(1)                   = un(2,nubo1)
         uas2(2)                   = un(2,nubo2)
         uas3(1)                   = un(3,nubo1)
         uas3(2)                   = un(3,nubo2)
         uas4(1)                   = un(4,nubo1)
         uas4(2)                   = un(4,nubo2)
         uas5(1)                   = un(5,nubo1)
         uas5(2)                   = un(5,nubo2)
c
         flur1                     = 0.0d0
         flur2                     = 0.0d0
         flur3                     = 0.0d0
         flur4                     = 0.0d0
         flur5                     = 0.0d0
c         
         fltr1                     = 0.0d0
         fltr2                     = 0.0d0
         fltr3                     = 0.0d0
         fltr4                     = 0.0d0
         fltr5                     = 0.0d0
c
         IF (nordre .EQ. 1) GOTO 275
c
         aix                       = coor(1,nubo2) - coor(1,nubo1)
         aiy                       = coor(2,nubo2) - coor(2,nubo1)
         aiz                       = coor(3,nubo2) - coor(3,nubo1)
c
         flur1                     = beta2*
     &         (aix*dx(1,nubo1) + aiy*dy(1,nubo1) + aiz*dz(1,nubo1)) +
     &                               beta3*(uas1(2) - uas1(1))
         flur2                     = beta2*
     &         (aix*dx(2,nubo1) + aiy*dy(2,nubo1) + aiz*dz(2,nubo1)) +
     &                               beta3*(uas2(2) - uas2(1))
         flur3                     = beta2*
     &         (aix*dx(3,nubo1) + aiy*dy(3,nubo1) + aiz*dz(3,nubo1)) + 
     &                               beta3*(uas3(2) - uas3(1))
         flur4                     = beta2*
     &         (aix*dx(4,nubo1) + aiy*dy(4,nubo1) + aiz*dz(4,nubo1)) +
     &                               beta3*(uas4(2) - uas4(1))
         flur5                     = beta2*
     &         (aix*dx(5,nubo1) + aiy*dy(5,nubo1) + aiz*dz(5,nubo1)) +
     &                               beta3*(uas5(2) - uas5(1))
c
         fltr1                     = beta2*
     &         (aix*dx(1,nubo2) + aiy*dy(1,nubo2) + aiz*dz(1,nubo2)) +
     &                               beta3*(uas1(2) - uas1(1))
         fltr2                     = beta2*
     &         (aix*dx(2,nubo2) + aiy*dy(2,nubo2) + aiz*dz(2,nubo2)) +
     &                               beta3*(uas2(2) - uas2(1))
         fltr3                     = beta2*
     &         (aix*dx(3,nubo2) + aiy*dy(3,nubo2) + aiz*dz(3,nubo2)) +
     &                               beta3*(uas3(2) - uas3(1))
         fltr4                     = beta2*
     &         (aix*dx(4,nubo2) + aiy*dy(4,nubo2) + aiz*dz(4,nubo2)) +
     &                               beta3*(uas4(2) - uas4(1))
         fltr5                     = beta2*
     &         (aix*dx(5,nubo2) + aiy*dy(5,nubo2) + aiz*dz(5,nubo2)) +
     &                               beta3*(uas5(2) - uas5(1))
c
         IF ((nordre .EQ. 3) .OR. (nordre .EQ. 4)) GOTO 275
c
c        Auxiliary Values for the Van Albada Procedure
c
         dpm                       =-(uas1(2) - uas1(1))
         dpex                      =-4.0d0*flur1 - dpm
         aux1                 = 0.25*(1.0d0 + SIGN(1.0d0, dpex*dpm))
         dpor                      =-4.0d0*fltr1 - dpm
         aux2                 = 0.25*(1.0d0 + SIGN(1.0d0, dpor*dpm))
c
         flur1                     = aux1*
     &                               ((dpex*dpex + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpex)/
     &                               (dpex*dpex  + dpm*dpm + 2.0d0*e2)
         fltr1                     = aux2*
     &                               ((dpor*dpor + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpor)/
     &                               (dpor*dpor  + dpm*dpm + 2.0d0*e2)
c
         dpm                       =-(uas2(2) - uas2(1))
         dpex                      =-4.0d0*flur2 - dpm
         aux1                 = 0.25*(1.0d0 + SIGN(1.0d0, dpex*dpm))
         dpor                      =-4.0d0*fltr2 - dpm
         aux2                 = 0.25*(1.0d0 + SIGN(1.0d0, dpor*dpm))
c
         flur2                     = aux1*
     &                               ((dpex*dpex + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpex)/
     &                               (dpex*dpex  + dpm*dpm + 2.0d0*e2)
         fltr2                     = aux2*
     &                               ((dpor*dpor + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpor)/
     &                               (dpor*dpor  + dpm*dpm + 2.0d0*e2)
c
         dpm                       =-(uas3(2) - uas3(1))
         dpex                      =-4.0d0*flur3 - dpm
         aux1                 = 0.25*(1.0d0 + SIGN(1.0d0, dpex*dpm))
         dpor                      =-4.0d0*fltr3 - dpm
         aux2                 = 0.25*(1.0d0 + SIGN(1.0d0, dpor*dpm))
c
         flur3                     = aux1*
     &                               ((dpex*dpex + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpex)/
     &                               (dpex*dpex  + dpm*dpm + 2.0d0*e2)
         fltr3                     = aux2*
     &                               ((dpor*dpor + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpor)/
     &                               (dpor*dpor  + dpm*dpm + 2.0d0*e2)
c
         dpm                       =-(uas4(2) - uas4(1))
         dpex                      =-4.0d0*flur4 - dpm
         aux1                 = 0.25*(1.0d0 + SIGN(1.0d0, dpex*dpm))
         dpor                      =-4.0d0*fltr4 - dpm
         aux2                 = 0.25*(1.0d0 + SIGN(1.0d0, dpor*dpm))
c      
         flur4                     = aux1*
     &                               ((dpex*dpex + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpex)/
     &                               (dpex*dpex  + dpm*dpm + 2.0d0*e2)
         fltr4                     = aux2*
     &                               ((dpor*dpor + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpor)/
     &                               (dpor*dpor  + dpm*dpm + 2.0d0*e2)
c
         dpm                       =-(uas5(2) - uas5(1))
         dpex                      =-4.0d0*flur5 - dpm
         aux1                 = 0.25*(1.0d0 + SIGN(1.0d0, dpex*dpm))
         dpor                      =-4.0d0*fltr5 - dpm
         aux2                 = 0.25*(1.0d0 + SIGN(1.0d0, dpor*dpm))
c
         flur5                     = aux1*
     &                               ((dpex*dpex + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpex)/
     &                               (dpex*dpex  + dpm*dpm + 2.0d0*e2)
         fltr5                     = aux2*
     &                               ((dpor*dpor + e2)*dpm +
     &                                (dpm*dpm   + e2)*dpor)/
     &                               (dpor*dpor  + dpm*dpm + 2.0d0*e2)
c
275      CONTINUE
c
         uas1(1)                   = uas1(1) + bsign*flur1
         uas2(1)                   = uas2(1) + bsign*flur2
         uas3(1)                   = uas3(1) + bsign*flur3
         uas4(1)                   = uas4(1) + bsign*flur4
         uas5(1)                   = uas5(1) + bsign*flur5
c
         uas1(2)                   = uas1(2) - bsign*fltr1
         uas2(2)                   = uas2(2) - bsign*fltr2
         uas3(2)                   = uas3(2) - bsign*fltr3
         uas4(2)                   = uas4(2) - bsign*fltr4
         uas5(2)                   = uas5(2) - bsign*fltr5
c
         rnorm                     = 1.0d0/SQRT(
     &                               vnocl(1,iseg)*vnocl(1,iseg) +
     &                               vnocl(2,iseg)*vnocl(2,iseg) + 
     &                               vnocl(3,iseg)*vnocl(3,iseg))
c
         vno(1)                    =-vnocl(1,iseg)*rnorm
         vno(2)                    =-vnocl(2,iseg)*rnorm
         vno(3)                    =-vnocl(3,iseg)*rnorm
c
         xsigm                     =-sigma(iseg)*rnorm
c
         prod1                     = uas2(1)*vno(1) + uas3(1)*vno(2) + 
     &                               uas4(1)*vno(3)
c
         ener1                     = uas5(1)/gam1 + 
     &                               0.5d0*uas1(1)*(uas2(1)*uas2(1) + 
     &                                            uas3(1)*uas3(1) + 
     &                                            uas4(1)*uas4(1))
c 
         prod2                     = uas2(2)*vno(1) + uas3(2)*vno(2) + 
     &                               uas4(2)*vno(3)
c
         ener2                     = uas5(2)/gam1 + 
     &                               0.5d0*uas1(2)*(uas2(2)*uas2(2) +
     &                                            uas3(2)*uas3(2) + 
     &                                            uas4(2)*uas4(2))
c
         flum1                     = uas1(1)*(prod1 - xsigm) + 
     &                               uas1(2)*(prod2 - xsigm)
c
         flum2                     = uas1(1)*uas2(1)*(prod1 - xsigm) + 
     &                               uas1(2)*uas2(2)*(prod2 - xsigm) + 
     &                               (uas5(1) + uas5(2))*vno(1)
     &                               
c
         flum3                     = uas1(1)*uas3(1)*(prod1 - xsigm) +
     &                               uas1(2)*uas3(2)*(prod2 - xsigm) + 
     &                               (uas5(1) + uas5(2))*vno(2)
c
         flum4                     = uas1(1)*uas4(1)*(prod1 - xsigm) + 
     &                               uas1(2)*uas4(2)*(prod2 - xsigm) + 
     &                               (uas5(1) + uas5(2))*vno(3)
c
         flum5                     = ener1*(prod1 - xsigm) + 
     &                               ener2*(prod2 - xsigm) + 
     &                               uas5(1)*prod1 + uas5(2)*prod2 
c
         squsr1                    = SQRT(uas1(1))
         squsr2                    = SQRT(uas1(2))
c
         usro                      = 1.0d0/(squsr1 + squsr2)
c
         uar1                      = (squsr1*uas1(1) + 
     &                                squsr2*uas1(2))*usro
c
         uar2                      = (squsr1*uas2(1) + 
     &                                squsr2*uas2(2))*usro
c
         uar3                      = (squsr1*uas3(1) + 
     &                                squsr2*uas3(2))*usro
c
         uar4                      = (squsr1*uas4(1) + 
     &                                squsr2*uas4(2))*usro
c
         uar5                      = ((ener1 + uas5(1))/
     &                                squsr1 + 
     &                                (ener2 + uas5(2))/
     &                                squsr2)*usro
c
         pror                      = vno(1)*uar2 + vno(2)*uar3 +
     &                               vno(3)*uar4
c
         qir                       = 0.5d0*(uar2*uar2 + uar3*uar3 +
     &                                    uar4*uar4)
c
         tet1                      = vno(3)*uar3 - vno(2)*uar4
         tet2                      = vno(1)*uar4 - vno(3)*uar2
         tet3                      = vno(2)*uar2 - vno(1)*uar3
c
         cr2                       = gam1*(uar5 - qir)
         cr                        = SQRT(cr2)
         cr2                       = 1.0d0/cr2
c
         dif1                      = uas1(1) - uas1(2)
         dif2                      = uas1(1)*uas2(1) - uas1(2)*uas2(2)
         dif3                      = uas1(1)*uas3(1) - uas1(2)*uas3(2)
         dif4                      = uas1(1)*uas4(1) - uas1(2)*uas4(2)
         dif5                      = ener1 - ener2
c
         vp1                       = pror - xsigm
         vp4                       = pror + cr - xsigm
         vp5                       = pror - cr - xsigm
c

         IF (ient .EQ. 1) THEN
c
           delta                  = dabs(vp4)/100.d0
c
           vp1                    = dsqrt(vp1*vp1+delta)
           vp4                    = dsqrt(vp4*vp4+delta)
           vp5                    = dsqrt(vp5*vp5+delta)
c
c
         ENDIF
c
         flur1                     = ABS(vp1)*
     &              ((vno(1)*(1.0d0 - gam1*qir*cr2) - tet1)*dif1 + 
     &               (vno(1)*gam1*uar2*cr2)*dif2  +  
     &               (vno(3)  + (vno(1)*gam1*uar3*cr2))*dif3   +
     &               (-vno(2) + (vno(1)*gam1*uar4*cr2))*dif4   -
     &               (vno(1)*gam1*cr2)*dif5)
c
         flur2                     = ABS(vp1)*
     &              ((vno(2)*(1.0d0 - gam1*qir*cr2) - tet2)*dif1 + 
     &               (-vno(3) + (vno(2)*gam1*uar2*cr2))*dif2   + 
     &               (vno(2)*gam1*uar3*cr2)*dif3  + 
     &               (vno(1)  + (vno(2)*gam1*uar4*cr2))*dif4   -
     &               (vno(2)*gam1*cr2)*dif5)
c
         flur3                     = ABS(vp1)*
     &              ((vno(3)*(1.0d0 - gam1*qir*cr2) - tet3)*dif1 + 
     &               (vno(2)  + (vno(3)*gam1*uar2*cr2))*dif2   + 
     &               (-vno(1) + (vno(3)*gam1*uar3*cr2))*dif3   + 
     &               (vno(3)*gam1*uar4*cr2)*dif4  - 
     &               (vno(3)*gam1*cr2)*dif5)
c
         flur4                     = ABS(vp4)*
     &              ((-cr*pror   + gam1*qir)*dif1  + 
     &               ( cr*vno(1) - gam1*uar2)*dif2 + 
     &               ( cr*vno(2) - gam1*uar3)*dif3 + 
     &               ( cr*vno(3) - gam1*uar4)*dif4 + 
     &               gam1*dif5)
c
         flur5                     = ABS(vp5)*
     &              (( cr* pror  + gam1* qir)*dif1 + 
     &               (-cr*vno(1) - gam1*uar2)*dif2 + 
     &               (-cr*vno(2) - gam1*uar3)*dif3 +
     &               (-cr*vno(3) - gam1*uar4)*dif4 + 
     &               gam1*dif5)
c
         fltr1                     = vno(1)*flur1 + vno(2)*flur2 +
     &                               vno(3)*flur3 + 
     &                               0.5d0*(flur4 + flur5)*cr2 
c
         fltr2                     = 
     &              (uar2*vno(1))*flur1 +
     &              (uar2*vno(2) - vno(3))*flur2 + 
     &              (uar2*vno(3) + vno(2))*flur3 + 
     &              0.5d0*vno(1)*(flur4 - flur5)/cr + 
     &              0.5d0*uar2*(flur4 + flur5)*cr2 
c
         fltr3                     = 
     &              (uar3*vno(1) + vno(3))*flur1 +
     &              (uar3*vno(2))*flur2 + 
     &              (uar3*vno(3) - vno(1))*flur3 +
     &              0.5d0*vno(2)*(flur4 - flur5)/cr + 
     &              0.5d0*uar3*(flur4 + flur5)*cr2
c
         fltr4                     = 
     &              (uar4*vno(1) - vno(2))*flur1 +
     &              (uar4*vno(2) + vno(1))*flur2 +
     &              (uar4*vno(3))*flur3 + 
     &              0.5d0*vno(3)*(flur4 - flur5)/cr + 
     &              0.5d0*uar4*(flur4 + flur5)*cr2
c
         fltr5                     = 
     &              (qir*vno(1) + tet1)*flur1 +
     &              (qir*vno(2) + tet2)*flur2 +
     &              (qir*vno(3) + tet3)*flur3 +
     &              0.5d0*pror*(flur4 - flur5)/cr + 
     &              0.5d0*uar5*(flur4 + flur5)*cr2
c
         rnorm                     = 0.5d0/rnorm
c
         ce(1,nubo1)               = ce(1,nubo1) - (flum1 + fltr1)*rnorm
         ce(2,nubo1)               = ce(2,nubo1) - (flum2 + fltr2)*rnorm
         ce(3,nubo1)               = ce(3,nubo1) - (flum3 + fltr3)*rnorm  
         ce(4,nubo1)               = ce(4,nubo1) - (flum4 + fltr4)*rnorm
         ce(5,nubo1)               = ce(5,nubo1) - (flum5 + fltr5)*rnorm
c
         ce(1,nubo2)               = ce(1,nubo2) + (flum1 + fltr1)*rnorm
         ce(2,nubo2)               = ce(2,nubo2) + (flum2 + fltr2)*rnorm
         ce(3,nubo2)               = ce(3,nubo2) + (flum3 + fltr3)*rnorm
         ce(4,nubo2)               = ce(4,nubo2) + (flum4 + fltr4)*rnorm
         ce(5,nubo2)               = ce(5,nubo2) + (flum5 + fltr5)*rnorm
c
10    CONTINUE
c
      RETURN
      END
      SUBROUTINE GRADNOD
c---------------------------------------------------------------------   
c Computes the hermitian nodal gradients
c---------------------------------------------------------------------   
      INCLUDE 'Param3D.h'
c---------------------------------------------------------------------
c     Local variables definition
      INTEGER ivar  , k     , jt   , ia  , ib
      INTEGER is    , is1   , is2  , is3 , is4  , id1 , id2
      INTEGER iseg , nub1, nub2 , js1 , js2
      REAL*8    usro  
      REAL*8    vol6  , ait   , ais  
      REAL*8    ds3   , us4  , us6
      REAL*8    x(4)  , y(4)  , z(4) , b(4), c(4) , d(4)
      REAL*8    dbx(5), dby(5), dbz(5)
      REAL*8    dxt(5), dyt(5), dzt(5)
      REAL*8    uph(5,4) 
      REAL*8    pentel(nsmax)
c
c     Initialisations
c
      ds3                          = 2.0d0/3.0d0
      us4                          = 1.0d0/4.0d0
      us6                          = 1.0d0/6.0d0
c
c
      DO 10 is=1,ns
         pentel(is)             = 0.0d0
10    CONTINUE
c
c
c
      DO 1000 jt=1,nt
c     ===============
c
c
c
         DO 30 k=1,4
c        -----------
            is                     = nu(k,jt)
c
            uph(1,k)               = ua(1,is)
            usro                   = 1.0d0/uph(1,k)
c
            uph(2,k)               = ua(2,is)*usro
            uph(3,k)               = ua(3,is)*usro
            uph(4,k)               = ua(4,is)*usro
c
            uph(5,k)               = gam1*(ua(5,is) -
     &           0.5d0*uph(1,k)*(
     &           uph(2,k)*uph(2,k) +
     &           uph(3,k)*uph(3,k) +
     &           uph(4,k)*uph(4,k)))
c
30       CONTINUE
c        --------
c
c
         is1                       = nu(1,jt)
         is2                       = nu(2,jt)
         is3                       = nu(3,jt)
         is4                       = nu(4,jt)
c
         x(1)                      = coor(1,is1)
         y(1)                      = coor(2,is1)
         z(1)                      = coor(3,is1)
c
         x(2)                      = coor(1,is2)
         y(2)                      = coor(2,is2)
         z(2)                      = coor(3,is2)
c
         x(3)                      = coor(1,is3)
         y(3)                      = coor(2,is3)
         z(3)                      = coor(3,is3)
c
         x(4)                      = coor(1,is4)
         y(4)                      = coor(2,is4)
         z(4)                      = coor(3,is4)
c     
c
c        Computing the basis function gradients
c
         CALL GRADFB(x, y, z, b, c, d, vol6)
c
         ait                       = us6/volt(jt)
c
         dbx(1)                    = b(1)*ait
         dbx(2)                    = b(2)*ait
         dbx(3)                    = b(3)*ait
         dbx(4)                    = b(4)*ait
c
         dby(1)                    = c(1)*ait
         dby(2)                    = c(2)*ait
         dby(3)                    = c(3)*ait
         dby(4)                    = c(4)*ait
c
         dbz(1)                    = d(1)*ait
         dbz(2)                    = d(2)*ait
         dbz(3)                    = d(3)*ait
         dbz(4)                    = d(4)*ait
c
c
c        Computing the P1-gradients on each tetraedra
c
c
         DO 60 ivar=1,5
c        --------------
            dxt(ivar)              = uph(ivar,1)*dbx(1) +
     &                               uph(ivar,2)*dbx(2) +
     &                               uph(ivar,3)*dbx(3) +
     &                               uph(ivar,4)*dbx(4)
c
            dyt(ivar)              = uph(ivar,1)*dby(1) +
     &                               uph(ivar,2)*dby(2) +
     &                               uph(ivar,3)*dby(3) +
     &                               uph(ivar,4)*dby(4)
c
            dzt(ivar)              = uph(ivar,1)*dbz(1) +
     &                               uph(ivar,2)*dbz(2) +
     &                               uph(ivar,3)*dbz(3) +
     &                               uph(ivar,4)*dbz(4)
c
60       CONTINUE
c        --------
c
         IF (nordre .EQ. 1) GOTO 1110
         IF (nordre .EQ. 4) GOTO 1105
c
c        Computing the hermitian nodal gradients
c
         ait                       = us4*volt(jt)
c
         DO 95 ivar=1,5
c        --------------
            dx(ivar,is1)           = dx(ivar,is1) + dxt(ivar)*ait
            dx(ivar,is2)           = dx(ivar,is2) + dxt(ivar)*ait
            dx(ivar,is3)           = dx(ivar,is3) + dxt(ivar)*ait
            dx(ivar,is4)           = dx(ivar,is4) + dxt(ivar)*ait
c
            dy(ivar,is1)           = dy(ivar,is1) + dyt(ivar)*ait
            dy(ivar,is2)           = dy(ivar,is2) + dyt(ivar)*ait
            dy(ivar,is3)           = dy(ivar,is3) + dyt(ivar)*ait
            dy(ivar,is4)           = dy(ivar,is4) + dyt(ivar)*ait
c     
            dz(ivar,is1)           = dz(ivar,is1) + dzt(ivar)*ait
            dz(ivar,is2)           = dz(ivar,is2) + dzt(ivar)*ait
            dz(ivar,is3)           = dz(ivar,is3) + dzt(ivar)*ait
            dz(ivar,is4)           = dz(ivar,is4) + dzt(ivar)*ait
c
95       CONTINUE
c        --------
c
1105     CONTINUE    
c
         IF (nordre .EQ. 4) THEN
c
            DO 1090 k=1,4
c           -------------
               is                  = nu(k,jt)
c
               DO 1095 ivar=1,5
c
                  dx(ivar,is)      = (1.0d0 - pentel(is))*dxt(ivar) + 
     &            pentel(is)*0.5d0*(SIGN(1.0d0, dxt(ivar)) +
     &                            SIGN(1.0d0, dx(ivar,is)))*
     &            MIN(ABS(dxt(ivar)), ABS(dx(ivar,is)))
c
                  dy(ivar,is)      = (1.0d0 - pentel(is))*dyt(ivar) + 
     &            pentel(is)*0.5d0*(SIGN(1.0d0, dyt(ivar)) +
     &                            SIGN(1.0d0, dy(ivar,is)))*
     &            MIN(ABS(dyt(ivar)), ABS(dy(ivar,is)))
c
                  dz(ivar,is)      = (1.0d0 - pentel(is))*dzt(ivar) + 
     &            pentel(is)*0.5d0*(SIGN(1.0d0, dzt(ivar)) +
     &                            SIGN(1.0d0, dz(ivar,is)))*
     &            MIN(ABS(dzt(ivar)), ABS(dz(ivar,is)))
c
                  pentel(is)       = 1.0d0
c
1095           CONTINUE
c
1090        CONTINUE
c           --------
c
         ENDIF    !          IF (nordre .EQ. 4) 
c
1110     CONTINUE
c
1000  CONTINUE    ! FIN DE LA BOUCLE SUR LES ELEMENTS
c     ========
c
c
c
c
c
c     Completing the non-limited nodal gradients
c
      IF ((nordre .EQ. 2) .OR. (nordre .EQ. 3)) THEN 
c
        DO 1040 ivar=1,5
c        ================
            DO 1050 is=1,ns
c           ---------------
               ais                 = 1.0d0/vols(is)
c
               dx(ivar,is)         = dx(ivar,is)*ais
               dy(ivar,is)         = dy(ivar,is)*ais
               dz(ivar,is)         = dz(ivar,is)*ais
c
1050        CONTINUE
c           --------
1040     CONTINUE
c        ========
c
      ENDIF !       IF ((nordre .EQ. 2) .OR. (nordre .EQ. 3)) THEN 
c
c
c
      RETURN
      END
      SUBROUTINE TRANSPIRATION(PSI,CTRL)
C
      INCLUDE 'Param3D.h'
      INCLUDE 'Paramopt3D.h'
C
      INTEGER ISP,IS,I
      REAL*8 QT, PRESSION, U, V, W
      REAL*8 PI, AALPHA
      REAL*8 PSI(5,NSMAX), CTRL(NNSP), VNCOQ(3,nnsp)
      REAL*8 ray, normalis, x, ddx
c
c*** Inclusion des conditions de transpiration : ITRANS=1
c    On ne les inclut que sur la carlingue (cas du falcon) ou bien l'aile.
c    La carlingue et l'aile sont reperes si lo1fac = 200 ou si logfac = -2.
c    Transpiration : on traite le plan de symetrie comme avant
c                       et on applique la transpiration sur la coque.
C
C**** cas ou l'on se trouve soit sur la carlingue, soit sur l'aile
C     ==> TRANSPIRATION
C
C     PSI_bords = Q(T)*(RHO,RHOU,RHOV,RHOW,E) + (0,P.nx,P.ny,P.nz,P.Q(T))
c                -->   -->  --->    -->
C     avec Q(T) = V . (NO - N(T))   NO = (nx,ny,nz) est stoque dans VNO
C                                   --->
c                                   N(T) est stoque dans VNCOQ
C
c      IF ((COEFM1.NE.10).AND.(NPRES.EQ.0)) THEN 
c   
c  Cas ou l'on ne se trouve pas sur la tuyere et ou l'on veut calculer
c  la pression desiree par incidence simulee par transpiration.
C
C*** INCIDENCE DE 3.06 DEGRES. ROTATION 2D, DANS LE PLAN (X,Y).
C
c         PI = 4.0 * ATAN(1.0)
c         AALPHA = 3.06*PI/180
C
c         DO ISP = 1,NSP
c          VNCOQ(1,ISP) = VNO(1,ISP)*COS(AALPHA)+VNO(2,ISP)*SIN(AALPHA)
c          VNCOQ(2,ISP) = -VNO(1,ISP)*SIN(AALPHA)+VNO(2,ISP)*COS(AALPHA)
c          VNCOQ(3,ISP) = VNO(3,ISP)
c         END DO
c
c      ENDIF
C
c      IF ((COEFM1.EQ.10).OR.((COEFM1.NE.10).AND.(NPRES.NE.0))) THEN
c
         CALL NORMCOQ(CTRL,VNCOQ)
c
c$$$         if (kt.eq.1) then
c$$$c
c$$$            print*,'Transpiration.'
c$$$         do isp = 1,nsp
c$$$            is = node2d3d(isp)
c$$$            write(6,222) isp,vncoq(1,isp),vncoq(2,isp),coor(1,is)
c$$$     $                   ,coor(2,is)
c$$$c
c$$$ 222        format(i2,1x,4(e10.3,1x))
c$$$         end do
c$$$c
c$$$         write(6,*)'  '
c$$$         write(6,*) 'Normales specifiees'
c$$$c
c$$$      endif
c
c$$$
c$$$c
c$$$               ray=2.99719
c$$$c
c$$$               DO ISP = 1,NSP
c$$$c
c$$$                  NORMALIS = 0.
c$$$                  DO I = 1,3
c$$$                     NORMALIS = NORMALIS + VNO(I,ISP)*VNO(I,ISP)
c$$$                  END DO
c$$$                  NORMALIS = SQRT(NORMALIS)
c$$$c
c$$$                  VNCOQ(1,ISP) = 0.
c$$$                  VNCOQ(2,ISP) = - NORMALIS
c$$$                  VNCOQ(3,ISP) = 0.
c$$$c
c$$$                  IS = NODE2D3D(ISP) 
c$$$                  IF (((COOR(1,IS).GE.0.).AND.(COOR(1,IS).LE.2.)).AND.
c$$$     $                 (COOR(2,IS).EQ.5.)) THEN
c$$$                     x = 0.5*coor(1,is) + 0.5
c$$$                     if (abs(x-1.0).le.0.5) then
c$$$                     ddx=x-1.0
c$$$                     vncoq(1,isp) = ddx/sqrt(ray*ray-ddx*ddx)
c$$$                     ddx = sqrt(vncoq(1,isp)**2 + 1.)
c$$$                     vncoq(1,isp) = -vncoq(1,isp)*normalis/ddx
c$$$                     vncoq(2,isp) = normalis/ddx
c$$$c
c$$$
c$$$                     endif
c$$$                  ENDIF
c$$$c
c$$$         if (kt.eq.1) then
c$$$            write(6,222) isp,vncoq(1,isp),vncoq(2,isp),coor(1,is)
c$$$     $                   ,coor(2,is)
c$$$         endif
c$$$
c$$$               END DO
      
c
c      ENDIF
C



         DO ISP = 1,NSP
C
            IS = NODE2D3D(isp)
c
            U = UA(2,IS)/UA(1,IS)
            V = UA(3,IS)/UA(1,IS)
            W = UA(4,IS)/UA(1,IS)
c
            QT = U*(VNO(1,ISP)-VNCOQ(1,ISP)) +
     $           V*(VNO(2,ISP)-VNCOQ(2,ISP)) +
     $           W*(VNO(3,ISP)-VNCOQ(3,ISP))
c
            PRESSION = GAM1*(UA(5,IS)-(UA(2,IS)**2 +
     $           UA(3,IS)**2 + UA(4,IS)**2)/(2.d0*UA(1,IS)))
C
            PSI(1,IS) = PSI(1,IS) - UA(1,IS)*QT
            PSI(2,IS) = PSI(2,IS) - UA(2,IS)*QT - PRESSION*VNO(1,ISP)
            PSI(3,IS) = PSI(3,IS) - UA(3,IS)*QT - PRESSION*VNO(2,ISP)
            PSI(4,IS) = PSI(4,IS) - UA(4,IS)*QT - PRESSION*VNO(3,ISP)
            PSI(5,IS) = PSI(5,IS) - (UA(5,IS) + PRESSION)*QT
         END DO            
C 
      RETURN
      END
      SUBROUTINE VCURVM(ctrlno)
c---------------------------------------------------------------------   
c        Traitement des conditions aux bords
c---------------------------------------------------------------------   
      INCLUDE 'Param3D.h'
      INCLUDE 'Paramopt3D.h'
c---------------------------------------------------------------------   
c     Local variables definition
c
      INTEGER if1 , if2  , if3  , is1 , is2 , is3, ifac, is , j,ilogf
      REAL*8    rhom, rhum , rhvm , rhwm, rhem, pm , vnx , vny, vnz
      REAL*8    sig , pm1  , pm2  , pm3
      REAL*8    cap , capd1, capd2, capd3
      REAL*8    ro  , usro , u , v, w, p, c, e, usc, unx
      REAL*8    vp1 , vp4  , vp5  , vpp , vpm
      REAL*8    x1  , x2   , x3   , x4  , x5  , vit, c2  , cc
      REAL*8    xn1 , xn2  , ff, gg
      REAL*8    fgp(5) , fgm(5),ctrlno
C
c*** Traitement des bords glissants
c
      
      IF (nf1 .GT. 0) THEN
c         
         DO 1000 if1=1,nf1
c
            ifac                   = noe1(nf11+if1)
c
c*** Inclusion des conditions de transpiration : ITRANS=1
c    On ne les inclut que sur la carlingue (cas du falcon) ou bien l'aile
c    ou bien la tuyere.
c    La carlingue, l'aile et la tuyere sont reperees si logfac = 200 ou 
c    si logfac = -2 ou si logfac = 7.
c    Test : Si transpiration, on traite le plan de symetrie comme avant
c                             et on applique la transpiration sur la coque.
c           Si non transpiration, on traite la coque et l'axe de symetrie
c                                 de la meme facon.
c
c*** Non transpiration :
c      Psi_bords = (0,P.nx,P.ny,P.nz,0)
c

            ilogf=logfac(ifac)
            if (ilogf.eq.-2 .and. ctrlno.lt.1.d-10) ilogf=2
            
            if (((ITRANS.EQ.1).AND.(ilogf.ne.-2).and.
     $        (ilogf.ne.200).and.(ilogf.ne.7))
     $        .OR. (ITRANS.EQ.0)) then
c
              is1                    = nsfac(1,ifac)
            is2                    = nsfac(2,ifac)
            is3                    = nsfac(3,ifac)
c
            rhom                   = ua(1,is1) 
            rhum                   = ua(2,is1)
            rhvm                   = ua(3,is1) 
            rhwm                   = ua(4,is1)
            rhem                   = ua(5,is1) 
c
            pm1                    = gam1*(rhem - 
     &        0.5d0*(rhum*rhum + rhvm*rhvm +
     &        rhwm*rhwm)/rhom)
c
            rhom                   = ua(1,is2) 
            rhum                   = ua(2,is2) 
            rhvm                   = ua(3,is2) 
            rhwm                   = ua(4,is2) 
            rhem                   = ua(5,is2) 
c
            pm2                    = gam1*(rhem - 
     &        0.5d0*(rhum*rhum + rhvm*rhvm +
     &        rhwm*rhwm)/rhom)
c
            rhom                   = ua(1,is3)
            rhum                   = ua(2,is3)
            rhvm                   = ua(3,is3)
            rhwm                   = ua(4,is3)
            rhem                   = ua(5,is3)
c
            pm3                    = gam1*(rhem - 
     &        0.5d0*(rhum*rhum + rhvm*rhvm +
     &        rhwm*rhwm)/rhom)
c
            CE(2,is1)              = CE(2,is1) - vnfac(1,ifac)*pm1
            CE(3,is1)              = CE(3,is1) - vnfac(2,ifac)*pm1
            CE(4,is1)              = CE(4,is1) - vnfac(3,ifac)*pm1
            CE(5,is1)              = CE(5,is1) - vnsig(ifac)*pm1
c
            CE(2,is2)              = CE(2,is2) - vnfac(1,ifac)*pm2
            CE(3,is2)              = CE(3,is2) - vnfac(2,ifac)*pm2
            CE(4,is2)              = CE(4,is2) - vnfac(3,ifac)*pm2
            CE(5,is2)              = CE(5,is2) - vnsig(ifac)*pm2
c
            CE(2,is3)              = CE(2,is3) - vnfac(1,ifac)*pm3
            CE(3,is3)              = CE(3,is3) - vnfac(2,ifac)*pm3
            CE(4,is3)              = CE(4,is3) - vnfac(3,ifac)*pm3
            CE(5,is3)              = CE(5,is3) - vnsig(ifac)*pm3
c
            endif
c
1000     CONTINUE
c
      ENDIF
c
c*** Traitement des conditions a l'infini (Steger-Warming)
c
      IF (nf3 .GT. 0) THEN
c
         DO 2000 if3=1,nf3
c
            ifac                   = noe1(nf11+nf1+nf2+if3)
c     
            vnx                    = vnfac(1,ifac)
            vny                    = vnfac(2,ifac)
            vnz                    = vnfac(3,ifac)
c  
c            sig                    = vnsig(ifac)
c
            cap                    = SQRT(vnx*vnx+ vny*vny + vnz*vnz)
            capd1                  = vnx/cap
            capd2                  = vny/cap
            capd3                  = vnz/cap
c              
            DO 2100 j=1,3    
c
c   Partie +
c
               is                  = nsfac(j,ifac)
c
               ro                  = ua(1,is)
               usro                = 1.0d0/ro
c
               u                   = ua(2,is)*usro
               v                   = ua(3,is)*usro
               w                   = ua(4,is)*usro
c
               vit                 = 0.5d0*(u*u + v*v + w*w)
               pm                  = gam1*(ua(5,is) - ro*vit)
c      
               c2                  = gam*pm*usro
               c                   = SQRT(c2)
               usc                 = 1.0d0/c
               cc                  = vit + c2/gam1               
               unx                 = capd1*u + capd2*v + capd3*w

c
c
               x1                  = ro
               x2                  = ro*u
               x3                  = ro*v
               x4                  = ro*w
               x5                  = pm/gam1 + ro*vit  
c                   
               vp1                 = MAX(cap*unx, 0.0d0)
               vp4                 = MAX(cap*(unx + c), 0.0d0)
               vp5                 = MAX(cap*(unx - c), 0.0d0)
c
               vpp                 = vp1 - 0.5d0*(vp4 + vp5)
               vpm                 = 0.5d0*(vp5 - vp4)
c
               xn1                 = gam1*usc*(-vit*x1 + 
     &                                         u*x2 + v*x3 + w*x4 - x5)
               xn2                 = unx*x1 - 
     &                               capd1*x2 - capd2*x3 - capd3*x4
c
               ff                  = usc*(vpp*xn1 + vpm*xn2)
               gg                  = vpm*xn1 + vpp*xn2
c
               fgp(1)              = vp1*x1 + ff
               fgp(2)              = vp1*x2 + u*ff  + capd1*gg
               fgp(3)              = vp1*x3 + v*ff  + capd2*gg
               fgp(4)              = vp1*x4 + w*ff  + capd3*gg
               fgp(5)              = vp1*x5 + cc*ff + unx*gg

c
c   Partie -
c
               is                  = nsfac(j,ifac)
c
               ro                  = ub3(1,if3)               
               usro                = 1.0d0/ro
               u                   = ub3(2,if3)
               v                   = ub3(3,if3)
               w                   = ub3(4,if3)
               vit                 = 0.5d0*(u*u + v*v + w*w)               
               pm                   = ub3(5,if3)
c
               c2                  = gam*pm*usro
               c                   = SQRT(c2)
               usc                 = 1.0d0/c
               cc                  = vit + c2/gam1               
               unx                 = capd1*u + capd2*v + capd3*w
c
               x1                  = ro
               x2                  = ro*u
               x3                  = ro*v
               x4                  = ro*w
               x5                  = pm/gam1 + ro*vit  
c                   
               vp1                 = MIN(cap*unx, 0.0d0)
               vp4                 = MIN(cap*(unx + c) , 0.0d0)
               vp5                 = MIN(cap*(unx - c) , 0.0d0)
               
c
               vpp                 = vp1 - 0.5d0*(vp4 + vp5)
               vpm                 = 0.5d0*(vp5 - vp4)
c
               xn1                 = gam1*usc*(-vit*x1 + 
     &                                         u*x2 + v*x3 + w*x4 - x5)
               xn2                 = unx*x1 - 
     &                               capd1*x2 - capd2*x3 - capd3*x4
c
               ff                  = usc*(vpp*xn1 + vpm*xn2)
               gg                  = vpm*xn1 + vpp*xn2
c
c
c
               fgm(1)              = vp1*x1 + ff
               fgm(2)              = vp1*x2 + u*ff  + capd1*gg
               fgm(3)              = vp1*x3 + v*ff  + capd2*gg
               fgm(4)              = vp1*x4 + w*ff  + capd3*gg
               fgm(5)              = vp1*x5 + cc*ff + unx*gg
c
               CE(1,is)            = CE(1,is) - fgp(1) - fgm(1)
               CE(2,is)            = CE(2,is) - fgp(2) - fgm(2)
               CE(3,is)            = CE(3,is) - fgp(3) - fgm(3)
               CE(4,is)            = CE(4,is) - fgp(4) - fgm(4)
               CE(5,is)            = CE(5,is) - fgp(5) - fgm(5)

               
2100        CONTINUE  
c
2000     CONTINUE
c
      ENDIF
c

      
c      stop
      
      END
      SUBROUTINE GRADFB(x, y, z, b, c, d, vol6)
c---------------------------------------------------------------------   
      IMPLICIT NONE
c---------------------------------------------------------------------
c     Local variables definition
      REAL*8 b(4), c(4), d(4), x(4), y(4), z(4), vol6
      REAL*8 x1  , y1  , z1  , x2  , y2  , z2  
      REAL*8 x3  , y3  , z3  , x4  , y4  , z4
      REAL*8 z12 , z13 , z14 , z23 , z24 , z34
      REAL*8 y12 , y13 , y14 , y23 , y24 , y34
c
      x1                           = x(1)
      y1                           = y(1)
      z1                           = z(1)
      x2                           = x(2)
      y2                           = y(2)
      z2                           = z(2)
      x3                           = x(3)
      y3                           = y(3)
      z3                           = z(3)
      x4                           = x(4)
      y4                           = y(4)
      z4                           = z(4)
c
      z12                          = z2 - z1
      z13                          = z3 - z1
      z14                          = z4 - z1
      z23                          = z3 - z2
      z24                          = z4 - z2
      z34                          = z4 - z3
c
      b(1)                         = y2*z34 - y3*z24 + y4*z23
      b(2)                         =-y1*z34 + y3*z14 - y4*z13
      b(3)                         = y1*z24 - y2*z14 + y4*z12
      b(4)                         =-y1*z23 + y2*z13 - y3*z12
c
      c(1)                         =-x2*z34 + x3*z24 - x4*z23
      c(2)                         = x1*z34 - x3*z14 + x4*z13
      c(3)                         =-x1*z24 + x2*z14 - x4*z12
      c(4)                         = x1*z23 - x2*z13 + x3*z12
c
      y12                          = y2 - y1
      y13                          = y3 - y1
      y14                          = y4 - y1
      y23                          = y3 - y2
      y24                          = y4 - y2
      y34                          = y4 - y3
c
      d(1)                         = x2*y34 - x3*y24 + x4*y23
      d(2)                         =-x1*y34 + x3*y14 - x4*y13
      d(3)                         = x1*y24 - x2*y14 + x4*y12
      d(4)                         =-x1*y23 + x2*y13 - x3*y12
c
c     Six times the volume
c
      vol6                         = x1*b(1) + x2*b(2) + 
     &                               x3*b(3) + x4*b(4)
c
      RETURN
      END
      SUBROUTINE NORMCOQ(CTRL,VNCOQ)
C
c     Cette procedure calcule les normales VNCOQ a la coque.
c     Ces normales sont utilisees pour la transpiration explicite
c     (procedure Transpiration.f) et la transpiration implicite
c     (procedure ImpTranspiration.f)
c
      INCLUDE 'Paramopt3D.h'
C
      REAL*8 CTRL(NNSP), VNCOQ(3,NNSP), COORPBAR(3,NNSP)
      INTEGER ISP, K
C
      DO ISP = 1,NSP
         DO K = 1,3
            COORPBAR(K,ISP) = COORP(K,ISP) + CTRL(ISP)*VNON(K,ISP)
         END DO
      END DO
C        
      CALL CALCNORMPEAU(VNCOQ,COORPBAR)    
C
      RETURN
      END
      SUBROUTINE CALCNORMPEAU(VN_CELL,COORD)
C
C*** Cette procedure calcule :
C     * Les normales en chaque triangle de la coque (stockees dans vnp(3,ntp),
c       tableau mis en COMMON)
C     * Les normales en chaque cellule duale de la coque (stockees dans
C       vn_cell(3,nsp))
C
C*** ATTENTION : sur le maillage 3D, les normales sont orientees vers 
C                l'interieur de la coque. En ne considerant que la coque, on
C                aurait tendance a les orienter vers l'exterieur. Pour rester
C                rationnel, on les oriente vers l'interieur (meme sens que
C                les Vnfac).
C
      INCLUDE 'Param3D.h'
      INCLUDE 'Paramopt3D.h'
c
      real*8 x(3), y(3), z(3), vnx, vny, vnz
      integer jtp, l, isp, i
      real*8 somment(3), sommeni(3)
      real*8 vn_cell(3,nnsp), coord(3,nnsp)
c
      real*8 airx, airy, airz, airfac, diff
      real*8  vx, vy, vz, psca1, psca2
      real*8  sumx, sumy, sumz
c
c*** 1ere etape : calcul des normales en chaque triangle
c
      DO 1 jtp = 1,ntp
         Do 2 l = 1,3
            isp = nup(l,jtp)
            x(l)=coord(1,isp)
            y(l)=coord(2,isp)
            z(l)=coord(3,isp)
2        CONTINUE

c     Calcul du produit vectoriel entre 2 vecteurs-aretes d'un triangle
c     La norme du vecteur resultant est l'aire du triangle.
c                         --->    --->    
c     Produit vectoriel : A1A2 /\ A1A3
c
         vnx= - y(1)*(z(3)-z(2)) + y(2)*(z(3)-z(1)) - y(3)*(z(2)-z(1))
C
         vny= + x(1)*(z(3)-z(2)) - x(2)*(z(3)-z(1)) + x(3)*(z(2)-z(1))
C
         vnz= - x(1)*(y(3)-y(2)) + x(2)*(y(3)-y(1)) - x(3)*(y(2)-y(1))

         vnp(1,jtp)= - 0.5*vnx
         vnp(2,jtp)= - 0.5*vny
         vnp(3,jtp)= - 0.5*vnz
c
c*** On ne passe pas par les verifications, on va directement au calcul
c    de la normale en les noeuds de la coque.
c
         goto 1000
c
c         open(90)
c
C    ON VERIFIE QUE C'EST NORMAL AUX COTES DE LA FACE :
C    --------------------------------------------------
C
c         VX= X(2) - X(1)
c         VY= Y(2) - Y(1)
c         VZ= Z(2) - Z(1)
c         PSCA1= VNP(1,jtp)*VX + VNP(2,jtp)*VY + VNP(3,jtp)*VZ
c
c         if (PSCA1.gt.1.e-07) then
c         write(90,*) 'Produit scalaire Normale.A1A2', jtp, ' ', PSCA1
c         endif
C
c         VX= X(3) - X(1)
c         VY= Y(3) - Y(1)
c         VZ= Z(3) - Z(1)
c         PSCA2= VNP(1,jtp)*VX + VNP(2,jtp)*VY + VNP(3,jtp)*VZ
c
c         if (PSCA2.gt.1.e-07) then         
c         write(90,*) 'Produit scalaire Normale.A1A3', jtp, ' ', PSCA2
c         endif
C
C    ON VERIFIE QUE LA NORME EST BIEN LA SURFACE DE LA FACE :
C    --------------------------------------------------------
C
c         AIRTP(JTP)= SQRT(VNP(1,jtp)**2 + VNP(2,jtp)**2 + VNP(3,jtp)**2)
c
c         write(90,*) 'Calcul de la norme :',jt,' ', AIRTP(JT)
C
c         AIRZ= 0.5*( X(1)*Y(2) - X(2)*Y(1) + X(2)*Y(3) -
c     1          X(3)*Y(2) + X(3)*Y(1) - X(1)*Y(3) )
c         AIRX= 0.5*( Z(1)*Y(2) - Z(2)*Y(1) + Z(2)*Y(3) -
c     1          Z(3)*Y(2) + Z(3)*Y(1) - Z(1)*Y(3) )
c         AIRY= 0.5*( X(1)*Z(2) - X(2)*Z(1) + X(2)*Z(3) -
c     1          X(3)*Z(2) + X(3)*Z(1) - X(1)*Z(3) )
c         AIRFAC= SQRT( AIRX*AIRX + AIRY*AIRY + AIRZ*AIRZ )
c
c         write(90,*) 'Calcul de la surface du triangle :',jtp,' ', AIRFAC
C
c         DIFF= AIRFAC - AIRTP(JTP)
c
c         if (DIFF.gt.1.e-07) then
c         write(90,*) 'Calcul de la difference :', jtp, ' ', DIFF
c         endif
c
 1000   continue
c
1       CONTINUE 
C
C  INTEGRALE DES NORMALES EXTERIEURES
C  ----------------------------------
c
c      SUMX= 0.
c      SUMY= 0.
c      SUMZ= 0.
c      DO 300 jtp = 1 , ntp
c         SUMX= SUMX + VNP(1,jtp)
c         SUMY= SUMY + VNP(2,jtp)
c         SUMZ= SUMZ + VNP(3,jtp)
c300   CONTINUE
c
c      if (sumx.gt.1.e-07) write(90,*) 'sumx = ',sumx
c      if (sumy.gt.1.e-07) write(90,*) 'sumy =',sumy
c      if (sumz.gt.1.e-01) write(90,*) 'sumz =',sumz

c      WRITE( 90, * )'       INT. DES NORMALES EXT. : ',
c     &             SUMX, ' ', SUMY, ' ', SUMZ
c
c
c**** 2eme etape : on deduit vn_cell
c
c
c     newnormale(i) = (Somme sur les triangles ayant pour sommet i des
c                     normales a ces triangles)/3. 
c
c     Initialisation a 0 des nouvelles normales vnorp
c     -----------------------------------------------
c

c
        DO 4 i = 1,3
           DO 3 isp = 1,nsp
              vn_cell(i,isp)=0.
 3         CONTINUE
 4      CONTINUE
c
      DO 5 l = 1,3
         DO 101 jtp = 1,ntp
            vn_cell(1,nup(l,jtp)) = vn_cell(1,nup(l,jtp)) + vnp(1,jtp)/3.
            vn_cell(2,nup(l,jtp)) = vn_cell(2,nup(l,jtp)) + vnp(2,jtp)/3.
            vn_cell(3,nup(l,jtp)) = vn_cell(3,nup(l,jtp)) + vnp(3,jtp)/3.
 101     CONTINUE
 5    CONTINUE

c
c     Test sur le fait que la somme des normales aux triangles sur tous les
c     triangles doit etre nulle, ainsi que la somme des normales en chaque
c     noeud.
c
c      DO 7 i = 1,3
c         somment(i)=0
c         sommeni(i)=0
c7     CONTINUE
c
c      DO 8 jtp = 1,ntp
c         DO 9 i = 1,3
c           somment(i) = somment(i) + vnp(i,jtp)
c9        CONTINUE
c8     CONTINUE
c
c      DO 10 isp = 1,nsp
c         DO 11 i = 1,3
c           sommeni(i) = sommeni(i) + vn_cell(i,isp) 
c11       CONTINUE
c10    CONTINUE
c
c      do i = 1,2
c       if (sommeni(i).gt.1.e-07) write(90,*) 'somme is',sommeni(i)
c      end do
c      if (sommeni(3).gt.1.-01) write(90,*) 'somme jt',sommeni(3)

c      write(90,*) 'Somme des vncoq =',(sommeni(i),i=1,3)
c      write(6,*) 'Somme des normales aux triangles ='
c      write(6,*) (somment(i),i=1,3)
c
c REMARQUE : si nous travaillons sur l'aile M6, la somme en z ne fait
c            pas 0, car la geometrie n'est pas fermee et le plan de symetrie
c            est en z.
c            si nous travaillons sur le falcon, la somme en y ne fait
c            pas 0, car la geometrie n'est pas fermee et le plan de symetrie
c            est en y.
c            
      return
      end
