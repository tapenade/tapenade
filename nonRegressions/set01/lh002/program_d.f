C        Generated by TAPENADE     (INRIA, Tropics team)
C  Tapenade 3.3 (r3316M) - 19 Jan 2010 14:59
C
C  Differentiation of top in forward (tangent) mode:
C   variations   of useful results: x y z a
C   with respect to varying inputs: x z b
C   RW status of diff variables: x:in-out y:out z:in-out a:out
C                b:in
      SUBROUTINE TOP_D(x, xd, y, yd, z, zd, a, ad, b, bd, c)
      IMPLICIT NONE
      REAL x, y, z, a, b, c
      REAL xd, yd, zd, ad, bd
      IF (x .GT. 0.0) THEN
        y = 1.7
        CALL SUB1(x, y, z)
        zd = 5.1*zd
        z = 5.1*z
        xd = zd
        x = y + z
        yd = 0.0
      ELSE
        yd = 3.3*2.0*x*xd
        y = 3.3*x**2.0
      END IF
      a = -2.9
      CALL SUB1_D(a, ad, b, bd, c)
      END

C  Differentiation of sub1 in forward (tangent) mode:
C   variations   of useful results: x
C   with respect to varying inputs: y
C
      SUBROUTINE SUB1_D(x, xd, y, yd, z)
      IMPLICIT NONE
      REAL x
      REAL xd
      REAL y
      REAL yd
      REAL z
      xd = 3.7*yd
      x = 3.7*y
      END
C

