      subroutine adj4 ( y )
c      test : flots de controle ( if elseif else  contenant des goto )
c             fonction intrinsic abs dans un bloc basic
      common/ varz / z
      common/ varx / x(0:20)
c
      read(5,1000) y, z
c        ========== 
      if ( y.gt.z ) then
c        ==========
         do 10 i= 0, 10
            x(i) = z
 10      continue    
         do 20 i= 1, 10
            x(i+10) = x(i) + y
 20      continue
c            ============= 
      elseif ( z.le. 0.0 ) then
c            =============
         x(0) = y 
         do 30 i= 1, 10
            x(i)   = y
            x(i+1) = x(i) - abs(z) 
 30      continue    
c     ====
      else
c     ====
         do 40 i= 0, 20
            x(i)   = 0.0
 40      continue    
         goto 2000
c     ===== 
      endif
c     =====        
c
      write(6,1000) y,z
      do 100 ii = 0, 20
         print * , 'x(',ii,')=',x(ii) 
 100  continue     
c
 2000 continue 
 1000 format( 'y=', e15.8 , 'z=', e15.8 )
      end 
