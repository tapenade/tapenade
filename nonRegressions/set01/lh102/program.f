      SUBROUTINE TESTPROTECT(xx,yy,zz,vv1,vv2,vv3)
C To test protections introduced in tangent and reverse mode
C around functions such as power, SQRT, log, etc...
      REAL xx,yy,zz, vv1,vv2,vv3
      REAL tmpX, tmpY
C
      IF (xx .GT. 0.D0) THEN
         tmpX = 1.D0 - vv2
         tmpY = xx * vv3 / vv1
         yy = tmpX**tmpY
         xx = SQRT(xx)*tmpY
         zz = vv2**tmpY
      ELSE
         yy = 1.D0
      ENDIF
c
      END
