C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 21 Nov 2022 11:22
C
C  Differentiation of testprotect in reverse (adjoint) mode:
C   gradient     of useful results: zz vv1 vv2 vv3 xx yy
C   with respect to varying inputs: zz vv1 vv2 vv3 xx yy
C   RW status of diff variables: zz:in-out vv1:incr vv2:incr vv3:incr
C                xx:in-out yy:in-zero
      SUBROUTINE TESTPROTECT_B(xx, xxb, yy, yyb, zz, zzb, vv1, vv1b, vv2
     +                         , vv2b, vv3, vv3b)
      IMPLICIT NONE
C To test protections introduced in tangent and reverse mode
C around functions such as power, SQRT, log, etc...
      REAL xx, yy, zz, vv1, vv2, vv3
      REAL xxb, yyb, zzb, vv1b, vv2b, vv3b
      REAL tmpx, tmpy
      REAL tmpxb, tmpyb
      INTRINSIC SQRT
      REAL temp
      REAL tempb
C
      IF (xx .GT. 0.d0) THEN
        tmpx = 1.d0 - vv2
        tmpy = xx*vv3/vv1
        IF (tmpx .LE. 0.0 .AND. (tmpy .EQ. 0.0 .OR. tmpy .NE. INT(tmpy))
     +  ) THEN
          tmpxb = 0.0
        ELSE
          tmpxb = tmpy*tmpx**(tmpy-1)*yyb
        END IF
        IF (vv2 .LE. 0.0 .AND. (tmpy .EQ. 0.0 .OR. tmpy .NE. INT(tmpy))
     +  ) THEN
          vv2b = vv2b - tmpxb
        ELSE
          vv2b = vv2b + tmpy*vv2**(tmpy-1)*zzb - tmpxb
        END IF
        IF (vv2 .LE. 0.0) THEN
          tmpyb = 0.0
        ELSE
          tmpyb = vv2**tmpy*LOG(vv2)*zzb
        END IF
        temp = SQRT(xx)
        IF (tmpx .LE. 0.0) THEN
          tmpyb = tmpyb + temp*xxb
        ELSE
          tmpyb = tmpyb + temp*xxb + tmpx**tmpy*LOG(tmpx)*yyb
        END IF
        tempb = tmpyb/vv1
        IF (xx .EQ. 0.0) THEN
          xxb = vv3*tempb
        ELSE
          xxb = tmpy*xxb/(2.0*temp) + vv3*tempb
        END IF
        vv3b = vv3b + xx*tempb
        vv1b = vv1b - xx*vv3*tempb/vv1
        zzb = 0.0
      END IF
      yyb = 0.0
      END

