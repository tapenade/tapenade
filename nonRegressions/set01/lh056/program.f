      double precision function f(t)
      double precision t
      INTRINSIC AMIN1

      f = t * t
      f = exp(f)
           f=AMIN1( f, 0.9, 5 )

      return
      end
