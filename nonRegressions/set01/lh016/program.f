!
      subroutine ctest(in,out)
      integer :: i,j
      complex :: in(2),out(2),a(2,2)
!
      a(1,1) = (1.,1.)
      a(1,2) = (1.,2.)
      a(2,1) = (2.,1.)
      a(2,2) = (2.,2.)
!
      do i = 1,2
         out(i) = (0.,0.)
         do j = 1,2
            out(i) = a(i,j)*in(1)
         enddo
      enddo
!
      end subroutine ctest
!
