C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.10 (r5618M) - 26 May 2015 15:54
C
C  Differentiation of read7 in reverse (adjoint) mode:
C   gradient     of useful results: z read7
C   with respect to varying inputs: z
C   RW status of diff variables: z:incr read7:in-killed
      SUBROUTINE READ7_B(z, zb, read7b)
      IMPLICIT NONE
      REAL z
      REAL zb
      INTEGER ncmax
      INTEGER nmai
      INTEGER nfic17
      INTEGER ilign
      REAL read7
      REAL read7b
      ncmax = 10
      READ(nfic17, *, err=31, end=30) nmai
      ncmax = z + ncmax
      zb = zb + ncmax*read7b
      GOTO 100
 30   WRITE(nfic12, 20000) nfic17, ilign
 31   WRITE(nfic12, 20001) nfic17, ilign
 100  CONTINUE
20000 FORMAT(/,5x,'FICHIER ',i6,/,1x,
     +       '''REPARTITION DES CRAYONS PAR MAILLE: ERREUR A LA LIGNE'''
     +       ,i5)
20001 FORMAT(/,5x,
     + 'LECTURE DU NOMBRE DE MAILLES AVEC AU MOINS UN CRAYON IMPOSSIBLE'
     +      )
      END
C

