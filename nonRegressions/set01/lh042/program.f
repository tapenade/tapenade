c      typedef integer*16 bigint ;

      subroutine decls1(t1,t2,t3,n,m,t4,t5)
C
      integer n1,n2
      integer n,m
      parameter (n1 = 500)
      dimension t1(n)
      dimension t2(0:n)
      implicit integer (i-j)
      dimension it1(100),it21(10,15)
      parameter (n2 = 3*n1+100)
      dimension it2(n2)
      dimension it3(n1:n2)
      dimension t3(-10:m)
      integer it1,it2,it21
c      typedef struct {real x; real y;} point ;
      type point
        real x,y
      end type point
      integer*8 it3
      equivalence (it2(1),it21(3,4))
      common /c1/ it1, i2, it2
      real*8 t1, t2
      real t5(2000)
      type(point) t4
      dimension t4(n1)
      integer*16 t3
      real t6(2:20,0:15,3:9)
      equivalence (t5(1532),t6(14,0,8))
C
      t1(10) = it21(2,2)*t5(10)
      t1(5) = t4(5)%x + t4(5)%y
      t4(2)%x = 3*t4(3)%y
C
      end
