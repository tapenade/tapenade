c Exemple trouve par Ala pour tester des combinaisons de
c checkpointing manuel ($AD NOCHECKPOINT et $AD CHECKPOINT-START[-END])
c avec des MPI_Send et MPI_Recv. Auteur initial John Burkardt 2013.
      subroutine wave_resolution(id, p, n_global, n_local, nsteps,
     &     c, u_global )
      
      integer n_global
      double precision c
      double precision dt
      parameter ( dt = 0.00125 )
      double precision u_global(n_global)
      integer id
      integer n_local
      integer nsteps
      integer p
      double precision u1_local(n_global)

C     $AD NOCHECKPOINT
      call update ( id, p, n_global, n_local, nsteps, dt, u1_local, c )
      
C     $AD NOCHECKPOINT
      call collect ( id, p, n_global, n_local, nsteps, dt, u1_local,
     &     u_global)
      end

      
c  Parameters:
c    Input, integer ID, the identifier of this process.
c    Input, integer P, the number of processes.
c    Input, integer N_GLOBAL, the total number of points.
c    Input, integer N_LOCAL, the number of points visible to this process.
c    Input, integer NSTEPS, the number of time steps.
c    Input, double precision DT, the size of the time step.
c    Output, double precision U1_LOCAL(N_LOCAL), the portion of the solution
c    at the last time, as evaluated by this process.
      subroutine update ( id, p, n_global, n_local, nsteps, dt, 
     &  u1_local, c ) 
      implicit none
      include 'mpif.h'
      integer n_local
      double precision alpha, alpha2, c, dt, dudt, dx
      integer error
      double precision exact
      integer i, i_global, i_global_hi, i_global_lo,
     &     i_local, i_local_hi, i_local_lo, id, j
      integer ltor
      parameter ( ltor = 20 )
      integer n_global, nsteps, p
      integer rtol
      parameter ( rtol = 10 )
      integer status(MPI_STATUS_SIZE)
      double precision t, x
      double precision u0_local(n_local)
      double precision u1_local(n_local), u2_local(n_local)

c  Determine the value of ALPHA.
      dx = 1.0D+00 / dble ( n_global - 1 )
      alpha = c * dt / dx
      alpha2 = alpha * alpha

      if ( 1.0D+00 .le. abs ( alpha ) ) then
        if ( id .eq. 0 ) then
          write ( *, '(a)' ) '  Computation will not be stable!'
        end if
        call MPI_Finalize ( error )
        stop 1
      end if
c
      i_global_lo = (   id       * ( n_global - 1 ) ) / p
      i_global_hi = ( ( id + 1 ) * ( n_global - 1 ) ) / p
      if ( 0 .lt. id ) then
        i_global_lo = i_global_lo - 1
      end if

      i_local_lo = 0
      i_local_hi = i_global_hi - i_global_lo

      t = 0.0D+00
      do i_global = i_global_lo, i_global_hi
        x = dble ( i_global ) / dble ( n_global - 1 )
        i_local = i_global - i_global_lo
        u1_local(i_local+1) = exact ( x, t )
      end do

      do i_local = i_local_lo, i_local_hi
        u0_local(i_local+1) = u1_local(i_local+1)
      end do

c  Take NSTEPS time steps.
      do i = 1, nsteps
        t = dt * dble ( i )
        if ( i .eq. 1 ) then
          do i_local = i_local_lo + 1, i_local_hi - 1
            i_global = i_global_lo + i_local
            x = dble ( i_global ) / dble ( n_global - 1 )
            u2_local(i_local+1) = 
     &        +         0.5D+00 * alpha2   * u1_local(i_local-1+1) 
     &        + ( 1.0D+00 -       alpha2 ) * u1_local(i_local+0+1) 
     &        +         0.5D+00 * alpha2   * u1_local(i_local+1+1) 
     &        +                         dt * dudt ( x, t )
          end do

c  After the first time step, we can use the previous two solution estimates.
        else

          do i_local = i_local_lo + 1, i_local_hi - 1
             u2_local(i_local+1) = 
     &        +               alpha2   * u1_local(i_local-1+1) 
     &        + 2.0 * ( 1.0 - alpha2 ) * u1_local(i_local+0+1) 
     &        +               alpha2   * u1_local(i_local+1+1) 
     &        -                          u0_local(i_local+0+1)
          end do

        end if

c  Exchange data with "left-hand" neighbor. 
        if ( 0 .lt. id ) then
           call MPI_Send ( u2_local(i_local_lo+2), 1,
     &        MPI_DOUBLE_PRECISION, id - 1, rtol, MPI_COMM_WORLD, error)
          call MPI_Recv ( u2_local(i_local_lo+1), 1, 
     &      MPI_DOUBLE_PRECISION, id - 1, ltor, MPI_COMM_WORLD, status, 
     &      error )
        else
          x = 0.0D+00
          u2_local(i_local_lo+1) = exact ( x, t )
        end if

c  Exchange data with "right-hand" neighbor.
        if ( id .lt. p - 1 ) then
          call MPI_Send ( u2_local(i_local_hi), 1, MPI_DOUBLE_PRECISION, 
     &          id + 1, ltor, MPI_COMM_WORLD, error )
          call MPI_Recv ( u2_local(i_local_hi+1),   1, 
     &      MPI_DOUBLE_PRECISION, id + 1, rtol, MPI_COMM_WORLD, status, 
     &      error )
        else
          x = 1.0D+00
          u2_local(i_local_hi+1) = exact ( x, t )
        end if

c  Shift data for next time step.
        do i_local = i_local_lo, i_local_hi
          u0_local(i_local+1) = u1_local(i_local+1)
          u1_local(i_local+1) = u2_local(i_local+1)
        end do

      end do

      return
      end


      subroutine collect ( id, p, n_global, n_local, nsteps, dt, 
     &  u_local, u_global ) 

      implicit none

      include 'mpif.h'

      integer n_global
      integer n_local

      integer buffer(2)
      integer collect1
      parameter ( collect1 = 10 )
      integer collect2
      parameter ( collect2 = 20 )
      double precision dt
      integer error
      double precision exact
      integer i
      integer i_global
      integer i_global_hi
      integer i_global_lo
      integer i_local
      integer i_local_hi
      integer i_local_lo
      integer id
      integer j
      integer n_local2
      integer nsteps
      integer p
      integer status(MPI_STATUS_SIZE)
      double precision t, sum
      double precision u_global(n_global)
      double precision u_local(n_local)
      double precision x
      

      i_global_lo = (   id       * ( n_global - 1 ) ) / p
      i_global_hi = ( ( id + 1 ) * ( n_global - 1 ) ) / p
      if ( 0 < id ) then
        i_global_lo = i_global_lo - 1
      end if

      i_local_lo = 0
      i_local_hi = i_global_hi - i_global_lo

c  Master collects worker results into the U_GLOBAL array.
      if ( id .eq. 0 ) then

c     Copy the master's results into the global array.
         do i_local = i_local_lo, i_local_hi
            i_global = i_global_lo + i_local - i_local_lo
            u_global(i_global+1) = u_local(i_local+1)
         end do

c  Contact each worker.
        do i = 1, p - 1

c     Message "collect1" contains the global index and number of values.
          call MPI_Recv ( buffer, 2, MPI_INTEGER, i, collect1, 
     &      MPI_COMM_WORLD, status, error )

          i_global_lo = buffer(1)
          n_local2 = buffer(2)

          if ( i_global_lo < 0 ) then
            write ( *, '(a,i6)' ) 
     &        '  Illegal I_GLOBAL_LO = ', i_global_lo
            call MPI_Finalize ( error )
            stop 1
          else if ( n_global <= i_global_lo + n_local2 - 1 ) then
            write ( *, '(a,i6)' ) 
     &        '  Illegal I_GLOBAL_LO + N_LOCAL = ',
     &        i_global_lo + n_local2
            call MPI_Finalize ( error )
            stop 1
          end if

c     Message "collect2" contains the values.
          call MPI_Recv ( u_global(i_global_lo+1), n_local2, 
     &      MPI_DOUBLE_PRECISION, i, collect2, MPI_COMM_WORLD, 
     &      status, error )

        end do

c  Print the results.
        t = dt * dble ( nsteps )
        do i_global = 0, n_global - 1
           x = dble ( i_global ) / dble ( n_global - 1 )
        end do

c  Workers send results to process 0.
      else

c  Message "collect1" contains the global index and number of values.

C$AD CHECKPOINT-START
        buffer(1) = i_global_lo
        buffer(2) = n_local
        call MPI_Send ( buffer, 2, MPI_INTEGER, 0, collect1, 
     &       MPI_COMM_WORLD, error )
c     Message "collect2" contains the values.
        call MPI_Send ( u_local, n_local, MPI_DOUBLE_PRECISION, 0, 
     &       collect2, MPI_COMM_WORLD, error )
C$AD CHECKPOINT-END
        continue

      end if
      return
      end

      
      function exact ( x, t )
      implicit none
      double precision c
      parameter ( c = 1.0D+00 )
      double precision exact
      double precision pi
      parameter ( pi = 3.141592653589793D+00 )
      double precision t
      double precision x
      exact = sin ( 2.0D+00 * pi * ( x - c * t ) )
      return
      end

      function dudt ( x, t )
      implicit none
      double precision c
      parameter ( c = 1.0D+00 )
      double precision dudt
      double precision pi
      parameter ( pi = 3.141592653589793D+00 )
      double precision t
      double precision x
      dudt = - 2.0D+00 * pi * c * cos ( 2.0D+00 * pi * ( x - c * t ) )
      return
      end
   
