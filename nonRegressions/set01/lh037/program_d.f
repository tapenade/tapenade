C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of assgoto1 in forward (tangent) mode:
C   variations   of useful results: a b c
C   with respect to varying inputs: a b c
C   RW status of diff variables: a:in-out b:in-out c:in-out
      SUBROUTINE ASSGOTO1_D(a, ad, b, bd, c, cd)
      IMPLICIT NONE
      REAL*8 a, b, c, d
      REAL*8 ad, bd, cd
      INTEGER i, j, k
C
      ASSIGN 200 TO i
      ASSIGN 200 TO j
      ad = ad + bd
      a = a + b
 200  bd = bd - cd
      b = b - c
      IF (b .GT. 8.0) THEN
        ASSIGN 300 TO i
        ASSIGN 100 TO j
      END IF
      d = a - b
      GOTO i
 300  cd = 2.0*(b*ad+a*bd)
      c = 2.0*a*b
      GOTO j
 100  a = a + 25.5
      ASSIGN 400 TO j
      GOTO 300
 400  ad = 8.0*ad
      a = 8.0*a
C
      END

