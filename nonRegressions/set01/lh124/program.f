C nonRegrF77:lha24
      subroutine ttt(r,b,T)
C To test localized variables analysis (applied to InOut):
C  T should not be in the snapshot of "call fff"
C  because T is actually not read by fff
      real T(100),r,b,tmp
c
      call fff(r,b,T)
      T(10) = T(9)*T(11)
      end

      subroutine fff(r,b,T)
C To test localized variables analysis (applied to TBR):
C  A should not be PUSH/POP'ed by the 1st loop
C  because the original values in A are never read.
C Same for T in the 2nd loop.
      real A(100),T(100),r,b,tmp
      integer i
c
      do i = 1,100
         A(i) = b*b
         r = r+A(i)*A(i)
      enddo
      do i = 10,40
         T(2*i-3) = b*(b+1)
         b = T(-3+i*2)*T(i-6+i+3)
      enddo
      end
