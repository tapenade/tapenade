      subroutine nestedcounters(T1,A)
C nested while loops, requiring different ad_count counters.
      integer T1(100)
      real A(100)
      integer i,j
c
      i = 1
      do 30 while (T1(i).gt.0)
         j = 1
         do 40 while (T1(j).lt.10)
            A(i+j) = A(i)*A(j)
            j = j+2
 40      continue
         i = i+1
 30   continue
c
      end
