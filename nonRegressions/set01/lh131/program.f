      subroutine top(a,b,c,d)
      REAL a,b,c
C To test snapshot reduction using Block.constantZones
      a = 3.8 * a
      if (a>10.0) call S1(a,b)
      call S1(a,c)
      if (a<10.0) call S1(a,d)
      a = a*b + c*d
      end

      subroutine S1(a,x)
      REAL a,x
      x = a*x
      end

      
