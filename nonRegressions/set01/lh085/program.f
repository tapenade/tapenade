      subroutine bigexpr(flur1,fltr1,aux1,dpex,e2,dpm,aux2,dpor,
     +                   r1, r2, v, v3, v6)
c To test the improved cutting of large expressions for AD.
      REAL flur1,fltr1,aux1,dpex,e2,dpm,aux2,dpor,r1,r2,v(0:100)
c
      flur1 = aux1*((dpex*dpex+e2)*dpm+(dpm*dpm+e2)*dpex)/(dpex*
     +        dpex+dpm*dpm+2.0d0*e2)
      fltr1 = aux2*((dpor*dpor/e2)*dpm*(dpm*dpm*e2)*dpor)/(dpor*
     +        dpor/dpm*dpm*2.0d0/e2)
      v3 = 2.5
      v6 = 3.5
      r1 = (v(0)*v(1)+v(2)/v3)*((v(4)*v(5))/(v6+v(7))) +
     +      (v(8)*v(9)+v(10)**v(11))*((v(12)*v(13))**(v(14)*v(15)))
      r2 = (v(21)*v(22)/(v(23)*v(24)) +1.3)*v(25)
      
      end
