C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 21 Nov 2022 11:22
C
C  Differentiation of test in forward (tangent) mode (with options multiDirectional):
C   variations   of useful results: x y
C   with respect to varying inputs: x y
C   RW status of diff variables: x:in-out y:in-out
      SUBROUTINE TEST_DV(x, xd, y, yd, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      REAL x(100), y(100)
      REAL xd(nbdirsmax, 100), yd(nbdirsmax, 100)
      REAL a(2)
      INTEGER i
      REAL v1
      REAL v1d(nbdirsmax)
      REAL v2
      REAL v2d(nbdirsmax)
      INTEGER nd
      INTEGER nbdirs
C
      DO i=1,100
        DO nd=1,nbdirs
          v1d(nd) = (xd(nd, i)+yd(nd, i))/2.0
          v2d(nd) = y(i)*xd(nd, i) + x(i)*yd(nd, i)
        ENDDO
        v1 = (x(i)+y(i))/2.0
        v2 = x(i)*y(i)
        CALL SUB1_DV(v1, v1d, x, xd, y(i), yd(1, i), nbdirs)
        DO nd=1,nbdirs
          xd(nd, i) = v1d(nd) + v2d(nd)
          v1d(nd) = v2*v1d(nd) + v1*v2d(nd)
        ENDDO
        x(i) = v1 + v2
        v1 = v1*v2
        y(i) = v1*v2
        DO nd=1,nbdirs
          yd(nd, i) = v2*v1d(nd) + v1*v2d(nd)
          yd(nd, i) = x(i)*yd(nd, i) + y(i)*xd(nd, i)
        ENDDO
        y(i) = y(i)*x(i)
      ENDDO
C

      END

C  Differentiation of sub1 in forward (tangent) mode (with options multiDirectional):
C   variations   of useful results: t v y
C   with respect to varying inputs: t v y
C
      SUBROUTINE SUB1_DV(v, vd, t, td, y, yd, nbdirs)
      IMPLICIT NONE
      INCLUDE 'DIFFSIZES.inc'
C  Hint: nbdirsmax should be the maximum number of differentiation directions
      REAL v, t(100), y
      REAL vd(nbdirsmax), td(nbdirsmax, 100), yd(nbdirsmax)
      INTEGER nd
      INTEGER nbdirs
      DO nd=1,nbdirs
C
        vd(nd) = y*vd(nd) + v*yd(nd)
        yd(nd) = yd(nd) + t(20)*td(nd, 10) + t(10)*td(nd, 20)
      ENDDO
      v = v*y
      y = y + t(10)*t(20)
      DO nd=1,nbdirs
        td(nd, 15) = t(10)*vd(nd) + v*td(nd, 10)
      ENDDO
      t(15) = v*t(10)
C
      END
C

