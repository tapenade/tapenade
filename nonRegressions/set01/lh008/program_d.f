C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of adjblock1 in forward (tangent) mode:
C   variations   of useful results: x y z
C   with respect to varying inputs: y
C   RW status of diff variables: x:out y:in-zero z:out
      SUBROUTINE ADJBLOCK1_D(x, xd, y, yd, z, zd)
      IMPLICIT NONE
      REAL a, x, y, z
      REAL xd, yd, zd
C
      a = 10.5
      xd = a*yd
      x = a*y
      a = a + 1
      zd = a*yd
      z = a*y
      y = 0
C
      yd = 0.0
      END

