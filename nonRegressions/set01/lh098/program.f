      subroutine ff(N,t,xbt,x)
c
      implicit none
c
      integer N,M,K
      double precision x(0:N),
     & xbt,dxbt,t,cnklog
      external cnklog   
c
      xbt=0.d0
c  
      do k=0,N
        xbt=xbt+dexp(cnklog(N,k))*(t**(k))*((1-t)**(N-k))*x(k)
      enddo
      xbt = xbt + (t+3)**(1-t)
c  
      end
