      subroutine adj10 ( tab, q ) 
c      test concernant les loops imbriquees et differents types d'appel :
c        - 1ere  appel a sub avec un variable active en entree "sub(tab,q)"
c        - 2ieme appel a sub avec un variable non active " sub(tab,10.d0)"  
c        
      implicit double precision ( a-f, o-z)
      dimension tab(5,3)
      common / donnees / a, b, c 
      common / lmax    / imax, jmax, kmax
      common / lmin    / imin, jmin, kmin
      common / trav    / x(5,3,2)
c
      z = a*tab(2,2)+b
c
      call sub( tab ,q )
c
      i = imin 
      do 30 while (i.le.imax)
         do 20 j = jmin, jmax
            tab(i,j) = a*x(i,j,1) + b
 20      continue       
         i = i + 1
 30   continue    
c
      call sub( tab , 10.d0 )
c
      return
      end
      subroutine sub ( y ,q )
      implicit double precision ( a-f, o-z)
      common / donnees / a, b, c 
      common / lmax    / imax, jmax, kmax
      common / lmin    / imin, jmin, kmin
      common / trav    / x(5,3,2)
      dimension y(5,3) 
c
c      
      do 20 i = imin,imax
         do 10 j = jmin, jmax 
            do 5 k = kmin, kmax  
               x(i,j,k) = q * x(i,j,k)
 5          continue    
 10      continue    
 20   continue       
c
c   
      do 40 i = imin,imax
         j = jmin
         do 30 while ( (j.le.i) .and. (j.le.jmax) )
            y(i,j) = x(i,j,1)*a + float(i)
            j = j + 1
 30      continue    
         j = jmin
         do 35 while ( (i.lt.j) .and. (j.le.jmax)  )
            y(i,j) = float(j)
            j = j + 1
 35      continue
 40   continue   
c
c
      
      return
      end
