#include <stdio.h>
#include <stdlib.h>

extern void initf_(float *);
extern void foo_(float *, float *);
extern void bar_(float *, float *);
extern void ftoc_(float *, float *);

void topc(float *co, float* maxvm) {
  foo_(co, maxvm);
  foo_(co, maxvm);
  bar_(co, maxvm);
  ftoc_(co, maxvm);
}

void testc_(float *co, float* maxvm) {
  printf("test_c co :: %f %f %f%f \n", co[0], co[1], co[2], co[3]);
  *maxvm = co[2] * co[3];
}


void main() {
  float maxvm;
  float *co=NULL;

  co = malloc(4 * sizeof(float));
  initf_(co);
  printf("co :: %f %f %f%f \n", co[0], co[1], co[2], co[3]);
  topc(co, &maxvm);

  printf("maxvm -> %f \n" , maxvm);
}
