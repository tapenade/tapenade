subroutine initf(co)
   real co(4)
   co(1) = 2
   co(2) = 5
   co(3) = 10
   co(4) = 20
end

subroutine foo(co, maxvm)
   real co(4)
   real maxvm
   print*, 'foo :: co ', co
   maxvm = (co(1) * co(2)) + (co(3) * co(4))
   co(4) = maxvm;
   print*, 'foo -> co maxvm ', co, maxvm
end

subroutine bar(co, maxvm)
   real co(4)
   real maxvm
   call test(co, maxvm)
   call test(co, maxvm)
end

subroutine test(co, maxvm)
   real co(4)
   real maxvm
   print*, 'test :: co ', co
   maxvm = co(1) * co(2) * co(3)
   co(4) = maxvm
   print*, 'test -> co maxvm ', co, maxvm
end
