#include <stdio.h>

void foo1(float *x, float *y);

static void init() {
  printf("init test2 \n");
}

void foo2(float *x, float *y) {
  init();
  foo1(x,y);
}
