/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.13 (r6803M) - 26 Mar 2018 17:46
*/
#include <stdio.h>
void foo2(float *x, float *y);

void main() {
    float x, y;
    x = 2.0;
    y = 3.0;
    printf("x %f\n", x);
    foo2(&x, &y);
    printf("x %f y %f\n", x, y);
}
