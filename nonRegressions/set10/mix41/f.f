      block data globalfortran
      real xglobalfortran
      common /globalcommon/ xglobalfortran(10)
      end


      double precision function f(t, calcul)
      implicit none
      double precision t, calcul
      integer i
      external truc
      logical truc
      nbglobal = floor(calcul)
      DO i=1,nbglobal
        f = t * calcul
        if (truc(calcul)) then
           calcul = calcul + 1
        end if
        print*, 't ', t, ' calcul ', calcul, ' -> ', f
      END DO
      return
      end
