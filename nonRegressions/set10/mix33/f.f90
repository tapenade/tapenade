function bar(u, v) bind(c)
implicit none
double precision, value :: u
double precision :: v
double precision :: bar

u = 2 * u
v = u * u

bar = u

end function
