module m1
   interface
     subroutine test1(x)
        real :: x
     end subroutine test1
   end interface
end module m1

module m2
   interface
     subroutine test2(x)
        real :: x
     end subroutine test2
   end interface
end module m2

subroutine test1(var) 
   use m2
   implicit none 
   real var
   if (var > 0) then
     call test2(var)
   endif
end subroutine test1

subroutine test2(var) 
   use m1
   implicit none 
   real var
   var = -var
   call test1(var)
end subroutine test2

program top
   use m1
   implicit none
   real x
   x = 3.0
   call test1(x)
   print*, 'x = ', x
end program top
