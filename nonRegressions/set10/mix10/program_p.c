/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.13 (r6954) - 25 Jun 2018 16:00
*/
#include <stdio.h>
double c_mesh_exp(double r_min, double r_max, double *a, int *N, double *mesh)
;

float top(double x, double y) {
    //     mesh points to mesh[0:N-1]
    int N = 5;
    double a, mesh[N];
    return (float)c_mesh_exp(x, y, &a, &N, mesh);
}

void main() {
    //     mesh points to mesh[0:N-1]
    int N = 5;
    double r_min, r_max, a, mesh[N];
    float result;
    r_min = 2;
    r_max = 4;
    result = top(r_min, r_max);
    printf("result %f\n", result);
}
