module fmesh
contains
 function mesh_exp(min, max, a , n, mesh)
    implicit none
    integer :: n
    double precision :: min, max, a, mesh(n), mesh_exp
    print*, ' mesh_exp'
    mesh_exp = min * max
 end function
end module


module fmesh_wrapper

use iso_c_binding, only: c_double, c_int
use fmesh, only: mesh_exp

implicit none

contains

function c_mesh_exp(r_min, r_max, a, N, mesh) bind(c)
implicit none
real(c_double), value :: r_min
real(c_double), value :: r_max
real(c_double), intent(in) :: a
integer(c_int), intent(in) :: N
real(c_double), intent(out) :: mesh(N)
real(c_double) :: c_mesh_exp
c_mesh_exp =  mesh_exp(r_min, r_max, a, N, mesh)
r_min = 0
r_max = 0
end function


end module
