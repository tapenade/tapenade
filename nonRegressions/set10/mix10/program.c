#include <stdio.h>
double c_mesh_exp(double r_min, double r_max, double *a, int *N,
        double *mesh);

float top(double x, double y) {
   int N=5;
   double a, mesh[N];
   return (float) c_mesh_exp(x , y, &a, &N, mesh);
}

void main() {
   int N=5;
   double r_min, r_max, a, mesh[N];
   float result;
   r_min = 2;
   r_max = 4;
   result = top(r_min, r_max);
   printf("result %f\n", result);
}
