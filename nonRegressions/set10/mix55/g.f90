module example
      use iso_c_binding
      implicit none
      real(c_float) :: r,s
      common /com/ r,s
      bind(c, name='rr') :: /com/
end module

subroutine barf(z) bind(c)
      use example
      real(c_float) :: z
      r = r + 1
      z = r * 3
      print*, 'z= ', z
end subroutine

 

subroutine toto(z)
      z = z * 3
      print*, 'z= ', z
end subroutine
