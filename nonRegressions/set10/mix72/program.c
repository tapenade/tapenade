#include <stdio.h>

extern float Cstatix1;

extern void barf(float*);

void foo(float *x) {
  printf("1 Cstatix1 %f\n", Cstatix1);
  Cstatix1 = *x;
  barf(x);
  printf("2 Cstatix1 %f\n", Cstatix1);
  barf(x);
  printf("3 Cstatix1 %f\n", Cstatix1);
}

void main() {
  float x;
  Cstatix1 = 999;
  x = 2.0;
  foo(&x);
  printf("x %f Cstatix1 %f \n", x, Cstatix1);
}
