! pour tester bind(c) dans une subroutine: bind => save

subroutine barf(z) bind(c)
      use iso_c_binding
      real(c_float) :: statix1
      common /com/ statix1
      bind(c, name='Cstatix1') :: /com/
      real(c_float) :: z
      z = (z * z) + statix1
      statix1 = z
end subroutine
