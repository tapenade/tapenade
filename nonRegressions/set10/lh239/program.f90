! Bug found in ordering the diff Block's data-dep graph, in _DV
Subroutine MOVING_ROE_SOLVER (conv2,pres2,rhom,rho1,rho2,U1,U2,V1,V2,W1,W2,H1,H2,Q2,GAM1,C)
  IMPLICIT NONE
  REAL(8),DIMENSION(5),INTENT( IN) :: CONV2
  REAL(8) :: GAMMA,GAM1 
  REAL(8) :: EPST = 1.D-6
  REAL(8) :: rho1,rhom,u1,v1,w1,h1
  REAL(8) :: rho2,u2,v2,w2,pres2,csou2,h2
  REAL(8) :: wt1,wt2, rho,u,v,w,h,q2,c

  GAMMA = 1.5
  CSOU2 = DSQRT(DMAX1(EPST,GAMMA*PRES2*RHOM))
  H2    = (CONV2(5) + PRES2)*RHOM
  RHO = DSQRT(RHO1*RHO2)
  WT1 = RHO/(RHO1 + RHO2)
  WT2 = 1.D0 - WT1
  U   = U1*WT1 + U2*WT2 
  V   = V1*WT1 + V2*WT2 
  W   = W1*WT1 + W2*WT2 
  H   = H1*WT1 + H2*WT2
  Q2  = U*U + V*V + W*W 
  C   = DSQRT(DMAX1(EPST,GAM1*(H - 0.5D0*Q2)))
END Subroutine MOVING_ROE_SOLVER
