subroutine bar(u, v) bind(c)
implicit none
real :: u
real :: v

u = 2 * u
v = u * u

end subroutine
