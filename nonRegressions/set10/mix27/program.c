float f_ (float *u, float *v) {
  *v = 2.5 * *u;
  return *u + *v;
}
