MODULE List_Tab

  TYPE Cell
     ! A pointer structure with one value 
     INTEGER             :: Val
     TYPE(Cell), POINTER :: Next=>NULL()
  END TYPE Cell

CONTAINS

  FUNCTION COUNTLINKEDLIST(head) RESULT (NEDGE)
    IMPLICIT NONE
! count the number of element in linked list 
    TYPE(CELL), INTENT(IN) :: head
    TYPE(CELL), POINTER, SAVE :: thecell => NULL()
    INTEGER :: nedge
! -------------------------------------
    nedge = 0
    thecell => head%next
    DO WHILE (ASSOCIATED(thecell))
      nedge = nedge + 1
      thecell => thecell%next
    END DO
  END FUNCTION COUNTLINKEDLIST
END MODULE List_Tab

SUBROUTINE head(X,LIST)
  USE List_Tab
  REAL(KIND=8)                :: X
  TYPE(Cell) :: LIST
  INTEGER :: len

  len = COUNTLINKEDLIST(LIST)
  X = len*X*X
END SUBROUTINE head
