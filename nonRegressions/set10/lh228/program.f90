! another bug found by Boeing
! null pointer during ExpressionDifferentiator on
! ur = REAL(omy*rz-omz*ry,kindq)
subroutine test(ry,rz,omy,omz,kindq,ur)
 REAL ry,rz,omy,omz,ur
 INTEGER kindq
 ur = REAL(omy*rz-omz*ry,kindq)
end subroutine test
