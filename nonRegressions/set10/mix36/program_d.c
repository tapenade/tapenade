/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
typedef struct {
    float *max;
    int *min;
} Datasets;
typedef struct {
    float *max;
} Datasets_diff;
void freeD(Datasets *lcase, int nr);
void freeD_d(Datasets *lcase, Datasets_diff *lcased, int nr);

/*
  Differentiation of freeD in forward (tangent) mode:
   variations   of useful results: *(*lcase.max)
   with respect to varying inputs: *(*lcase.max)
   RW status of diff variables: lcase:(loc) *lcase.max:(loc) *(*lcase.max):in-out
   Plus diff mem management of: lcase:in *lcase.max:in
*/
void freeD_d(Datasets *lcase, Datasets_diff *lcased, int nr) {
    foof_d(lcase[nr], lcased[nr]);
}
