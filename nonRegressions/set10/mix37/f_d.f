C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_llhTests) - 29 Apr 2021 11:20
C
C  Differentiation of f in forward (tangent) mode:
C   variations   of useful results: f
C   with respect to varying inputs: tie
      REAL FUNCTION F_D(ch, tie, tied, siz, doub1, doub2, doub3, f)
      IMPLICIT NONE
      CHARACTER*81 ch(3, *)
      REAL tie, f
      REAL tied
      REAL*8 doub1
      REAL*8 doub2
      REAL*8 doub3
      INTEGER siz
      DIMENSION doub1(3, siz)
      DIMENSION doub2(4, *)
      DIMENSION doub3(2, siz, siz)
      f_d = 2*tie*tied
      f = tie*tie
C
      PRINT*, 'tie ', tie, ' -> f ', f
      END

