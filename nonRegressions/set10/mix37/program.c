void top(float *x, float *y)
{
  extern float f_(char*, float*, int*, double[][2], double[][3], double*);
    char* ch;
    double doub1[1][2];
    double doub2[1][3];
    double *doub3; 
    int *s;
    ch = "foo";
    *s = 1;
    *y = f_(ch, x, s, doub1, doub2, doub3);
}

main()
{
  float i=2;
  float result;
  printf("i %f ...\n", i);
  top(&i, &result);
  printf("i--> %f\n", i);
  printf("result--> %f\n", result);
}
