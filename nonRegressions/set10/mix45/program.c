#include <stdio.h>
#include <math.h>

static double *co1;

void linstatic(double *co, double *maxvm_pnorm)
{
  resultsvm(co, maxvm_pnorm);
}

void resultsvm(double *co, double *maxvm_pnorm)
{
  extern double f_();
  int i;
  co1 = co;
  f_(co1);
  *maxvm_pnorm =0.0;
  for(i=0;i<6;i++){
    printf("i %i norm %f\n", i, co[i]);
    *maxvm_pnorm = *maxvm_pnorm + (co[i] * co[i]);
  }
  printf("max %f \n", *maxvm_pnorm);
}



main()
{
  double i[] = {1,2,3,4,5,6};
  double j;
  double result;
  printf("i[0] %f ...\n", i[0]);
  printf("i[1] %f ...\n", i[1]);
  printf("i[5] %f ...\n", i[5]);
  linstatic(&i, &j);
  printf("j--> %f\n", j);
}
