

#include <stdio.h>

#define N  100

int     a[N][N];
int     n;

void Init (int n)
{
    int i, j;

    for (i = 0 ; i < n; ++i)
        for (j = 0; j < n; ++j)
            a[i][j] = 0;
}

void Magique (int n)
{
    int i, j, k;

    i = n - 1; j = n / 2;
    for (k = 1; k <=  n * n; ++k) {
        a[i][j] = k;
        if ((k % n) == 0)
            i = i - 1;
        else {
            i = (i + 1) % n; 
            j = (j + 1) % n;
        }
    }
}

void Erreur (char s[])
{
    printf ("Erreur fatale: %s\n", s);
    return;
}

void Lire (int *n)
{
    printf ("Taille du carre' magique, svp?::  ");
    scanf ("%d", n);
    if ((*n <= 0) || (*n > N) || (*n % 2 == 0)) 
        Erreur ("Taille impossible.");
}

void Imprimer (int n)
{
    int i, j;

    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) 
            printf ("%4d ", a[i][j]);
        printf ("\n");
    }
}

int main ()
{
    Lire(&n);
    Init(n);       /* - */
    Magique(n);
    Imprimer(n);
    return 0;
}
