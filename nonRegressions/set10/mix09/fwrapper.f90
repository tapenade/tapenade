module fmesh
contains
 subroutine mesh_exp(min, max, a , n, mesh)
    integer :: n
    double precision :: min, max, a, mesh(n)
    print*, ' mesh_exp init'
    mesh(1) = min
    mesh(4) = max
    mesh(2) = mesh(1) * mesh(4)
    print*,' mesh 2 ', mesh(2)
 end subroutine
end module


module fmesh_wrapper

use iso_c_binding, only: c_double, c_int
use fmesh, only: mesh_exp

implicit none

contains

subroutine c_mesh_exp(r_min, r_max, a, N, mesh) bind(c)
real(c_double), intent(in) :: r_min
real(c_double), intent(in) :: r_max
real(c_double), intent(in) :: a
integer(c_int), intent(in) :: N
real(c_double), intent(out) :: mesh(N)
call mesh_exp(r_min, r_max, a, N, mesh)
end subroutine

! wrap more functions here
! ...

end module
