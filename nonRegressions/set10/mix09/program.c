void c_mesh_exp(double *r_min, double *r_max, double *a, int *N,
        double *mesh);

void top(double *r_min, double *r_max, double *a, int *N,
        double *mesh) {
   c_mesh_exp(r_min, r_max, a, N, mesh);
}

void main() {
   int N=5;
   double r_min, r_max, a, mesh[N];
   r_min = 1.1;
   r_max = 4.4;
   top(&r_min, &r_max, &a, &N, mesh);
   printf(" mesh(2) %f\n", mesh[1]);
}
