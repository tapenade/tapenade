! same as set10/lh230, but with an extra call tree level.
  PROGRAM main
    REAL, POINTER, DIMENSION(:) :: pp1
    REAL, POINTER, DIMENSION(:) :: pp2
    COMMON /PTRCOMMON/ pp1,pp2
    SAVE /PTRCOMMON/
    REAL :: a,b
    INTEGER i
    REAL, TARGET, DIMENSION(30) :: tt1
    REAL, TARGET, DIMENSION(30) :: tt2
    a = 2.2
    b = 3.3
    do i=1,30
       tt1(i) = 10.0+i
       tt2(i) = 10.5+i
    enddo
    pp1 => tt1
    pp2 => tt2
    call foo(a,b)
    print *,b,tt1(10),tt1(20)
  END PROGRAM main

  SUBROUTINE foo(a,b)
    REAL :: a,b
    a = a*b
    CALL bar(a,b)
    b = b*a
  END SUBROUTINE foo

! mixing COMMONs and pointers
  SUBROUTINE bar(a,b)
    REAL :: a,b
    REAL, POINTER, DIMENSION(:) :: pp1
    REAL, POINTER, DIMENSION(:) :: pp2
    COMMON /PTRCOMMON/ pp1,pp2
    SAVE /PTRCOMMON/
    b = a*a
    pp1(10) = a*a
    b = b + pp2(5)*pp2(15)
    pp1(20) = SIN(pp2(20))
  END SUBROUTINE bar
