C Hand-turned into AMPI. Not differentiated.
C Playing with communicators
C MUST RUN ONLY WITH -np 4

      SUBROUTINE head(x,y)
      implicit none
      include 'ampi/ampif.h'
      DOUBLE PRECISION x,y
      INTEGER err_code, req
      INTEGER horizColor, vertiColor
      INTEGER globComm, horizComm, vertiComm
      INTEGER globRank, horizRank, vertiRank
      DOUBLE PRECISION local

      call AMPI_Comm_dup(MPI_COMM_WORLD, globComm, err_code)
      call AMPI_Comm_rank(globComm, globRank, err_code)
      horizColor = globRank/2
      call AMPI_Comm_split(globComm, horizColor, globRank,
     +     horizComm, err_code)
      call AMPI_Comm_rank(horizComm, horizRank, err_code)
      if (horizRank.eq.0) THEN
         call AMPI_Send(x, 1, MPI_DOUBLE_PRECISION, 1, 0,
     +        AMPI_TO_RECV, horizComm, err_code)
         local = x
      ELSE
         call AMPI_Recv(local, 1, MPI_DOUBLE_PRECISION, 0, 0,
     +        AMPI_FROM_SEND, horizComm, MPI_STATUS_IGNORE, err_code)
      ENDIF
      y = y + x*local
      vertiColor = MOD(globRank,2)
      call AMPI_Comm_split(globComm, vertiColor, globRank,
     +     vertiComm, err_code)
      call AMPI_Comm_rank(vertiComm, vertiRank, err_code)
      if (vertiRank.eq.0) THEN
         call AMPI_Send(local, 1, MPI_DOUBLE_PRECISION, 1, 0,
     +        AMPI_TO_RECV, vertiComm, err_code)
      ELSE
         call AMPI_Recv(local, 1, MPI_DOUBLE_PRECISION, 0, 0,
     +        AMPI_FROM_SEND, vertiComm, MPI_STATUS_IGNORE, err_code)
      ENDIF
      y = y + x*local
      call AMPI_Comm_free(vertiComm, err_code)
      call AMPI_Comm_free(horizComm, err_code)
      call AMPI_Comm_free(globComm, err_code)
      END

      PROGRAM main
      implicit none
      include 'ampi/ampif.h'
      INTEGER world_rank, err_code
      DOUBLE PRECISION x,y,res

      call AMPI_Init_NT(err_code)
      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      x = world_rank + 0.6 ;
      y = 0.0 ;
      call head(x,y)
      call AMPI_Reduce(y,res,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,
     +     MPI_COMM_WORLD,err_code)
      IF (world_rank.EQ.0) THEN
         print *,"process 0 holds res [22.48==]",res
      ENDIF
      call AMPI_Finalize_NT(err_code)
      END
