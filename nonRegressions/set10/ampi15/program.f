c ScatterAllgatherReduceSum based on Adol-C example

      SUBROUTINE head(fin,fout)
      implicit none
      include 'ampi/ampif.h'
      integer world_rank
      integer, parameter :: vSize = 3
      integer, parameter :: root = 0
      double precision fin
      double precision fout
      double precision x
      double precision y
      integer gSize
      integer i
      integer r
      integer err_code
      double precision :: buf1(vSize,10)
      double precision :: buf2(vSize)
      double precision :: buf3(vSize,10)
      double precision :: buf4(vSize,10)

      x = fin
      y = 0.0d0

      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      call AMPI_Comm_size(MPI_COMM_WORLD, gSize, err_code)
      IF( gSize > 10 ) THEN
         print *, 'Error! launch with 10 or fewer processes!'
         stop
      ENDIF
      IF (world_rank.EQ.root) THEN
         DO r = 1, gSize
             DO i = 1, vSize
                 buf1(i,r) = x*i*r
             ENDDO
         ENDDO
      ENDIF

      call AMPI_Scatter( buf1, vSize, MPI_DOUBLE_PRECISION, buf2, vSize,
     +    MPI_DOUBLE_PRECISION, root, MPI_COMM_WORLD, err_code )
      DO i = 1, vSize
          buf2(i) = buf2(i) * 3.0d0
      ENDDO

      call AMPI_Allgather( buf2, vSize, MPI_DOUBLE_PRECISION,
     +    buf3, vSize, MPI_DOUBLE_PRECISION, MPI_COMM_WORLD, err_code )
      DO r = 1, gSize
          DO i = 1, vSize
              buf3(i,r) = buf3(i,r) * (world_rank + 1)
          ENDDO
      ENDDO

      call AMPI_Reduce( buf3, buf4, vSize*gSize,  MPI_DOUBLE_PRECISION,
     +    MPI_SUM, root, MPI_COMM_WORLD, err_code )

      IF (world_rank.EQ.root) THEN
         DO r = 1, gSize
             DO i = 1, vSize
                 y = y + buf4(i,r)
             ENDDO
         ENDDO
         fout = y
      ENDIF

      RETURN
      END SUBROUTINE head


      PROGRAM main
      implicit none
      include 'ampi/ampif.h'
      INTEGER world_rank, err_code
      DOUBLE PRECISION x,y

      call AMPI_Init_NT(err_code)
      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      x = 0.d0
      y = 0.d0
      IF (world_rank.EQ.0) THEN
         x = 3.14d0
      ENDIF
      call head(x,y)
      IF (world_rank.EQ.0) THEN
         print *, 'rank 0: process 0 got number ',y
      ENDIF
      call AMPI_Finalize_NT(err_code)
      END

