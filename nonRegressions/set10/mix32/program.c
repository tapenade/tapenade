void mul( float* a, int* b , float* result) {
    *result = (*a) * (*b);
    printf("mul of %f and %i is %f\n", (*a), (*b), *result );
}
