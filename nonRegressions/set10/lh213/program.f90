! Bug example code given by UTRC / Razvan Florea / 12 Dec 2018

subroutine utampi_reduce_1dr8(comm,rank,root,op,srbuf)
  include 'ampi/ampif.h'
  integer(4), intent(in   ) :: comm
  integer(4), intent(in   ) :: rank       ! Current rank
  integer(4), intent(in   ) :: root       ! Rank to gather to (-1 == use all_gather).
  integer(4), intent(in   ) :: op
  real(8),    intent(inout) :: srbuf(:)
  integer(4) :: n,err

!  call ADTOOL_AMPI_fortranSetupTypes(AMPI_ADOUBLE_PRECISION, AMPI_AREAL)

  n = size(srbuf)
  if (root<0) then
     call ampi_allreduce(MPI_IN_PLACE,srbuf,n,MPI_DOUBLE_PRECISION,op,comm,err)
  else
     if (root==rank) then
        call ampi_reduce(MPI_IN_PLACE,srbuf,n,MPI_DOUBLE_PRECISION,op,root,comm,err)
     else
        call ampi_reduce(srbuf,srbuf,n,MPI_DOUBLE_PRECISION,op,root,comm,err)
     end if
  end if
end subroutine utampi_reduce_1dr8

subroutine test(comm, rank, op, x, y, z)
  IMPLICIT NONE
  include 'ampi/ampif.h'
  integer(4), intent(in   ) :: comm
  integer(4), intent(in   ) :: rank
  integer(4), intent(in   ) :: op
  real(8),    intent(inout) :: x(5), y(5), z(5)
  interface
     subroutine utampi_reduce_1dr8(comm,rank,root,op,srbuf)
       integer(4), intent(in   ) :: comm
       integer(4), intent(in   ) :: rank       ! Current rank
       integer(4), intent(in   ) :: root       ! Rank to gather to (-1 == use all_gather).
       integer(4), intent(in   ) :: op
       real(8),    intent(inout) :: srbuf(:)
     end subroutine utampi_reduce_1dr8
  end interface
  CALL utampi_reduce_1dr8(comm, rank, -1, op, x)
  CALL utampi_reduce_1dr8(comm, rank,  0, op, y)
  CALL utampi_reduce_1dr8(comm, rank,  1, op, z)
end subroutine test

!------------------------------------------------
PROGRAM MYPROG
IMPLICIT NONE
include 'ampi/ampif.h'
real(8) :: x(5), y(5), z(5)
INTEGER ierr, myid, nprocs,i

call AMPI_INIT_NT(ierr)
call AMPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
call AMPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)

do i=1,5
   x(i) = 10.0*myid + i - 0.5
   y(i) = 10.0*myid + i
   z(i) = 10.0*myid + i + 0.5
end do
IF( myid .eq. 0 ) THEN
  write(*,*) "x=",x
  write(*,*) "y=",y
  write(*,*) "z=",z
ENDIF
CALL test(MPI_COMM_WORLD, myid, MPI_SUM, x, y, z)
IF( myid .eq. 0 ) THEN
  write(*,*) "x=",x
  write(*,*) "y=",y
  write(*,*) "z=",z
ENDIF
call AMPI_FINALIZE_NT( ierr )
END PROGRAM MYPROG

