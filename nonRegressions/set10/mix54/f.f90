MODULE m1
    USE ISO_C_BINDING
    integer(C_INT), bind(C, name="flag_C") :: flag_f
    real(c_float), bind(C) :: tp
END MODULE

MODULE m2
    USE ISO_C_BINDING
    real(c_float), bind(C) :: x, y, z
    interface
         subroutine test() bind(c, name="Tes_t")
         end subroutine
    end interface

END MODULE

program main
    use m1
    use m2
    flag_f = 2
    x = 2.0
    call test()
    print*, flag_f , x, y, z
end program
