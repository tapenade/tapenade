float mul0 (float a, float b) {
  return a * b;
}

void mul1 (float a, float b, float *result) {
  *result = a * mul0(b,b);
}
