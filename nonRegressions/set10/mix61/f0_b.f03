!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.13 (r6885M) - 30 May 2018 13:46
!
MODULE M_DIFF
  USE ISO_C_BINDING
  IMPLICIT NONE

CONTAINS
  SUBROUTINE TOP(x, y, z)
    IMPLICIT NONE
    REAL :: x, y, z
    INTERFACE 
        SUBROUTINE FOO(x, y, z) BIND(c)
          USE ISO_C_BINDING
          REAL(c_float) :: x
          REAL(c_float) :: y
          REAL(c_float) :: z
        END SUBROUTINE FOO
    END INTERFACE

    CALL FOO0(x, y, z)
  END SUBROUTINE TOP

  SUBROUTINE FOO0(x, y, z)
    IMPLICIT NONE
    REAL, INTENT(IN) :: x
    REAL :: y
    REAL, INTENT(IN) :: z
    CALL BAR(x, y)
    CALL BAR(z, y)
  END SUBROUTINE FOO0

!  Differentiation of bar in reverse (adjoint) mode (with options context):
!   gradient     of useful results: a b
!   with respect to varying inputs: a b
  SUBROUTINE BAR_B(a, ab, b, bb) BIND(c)
    IMPLICIT NONE
    REAL :: a, b
    REAL :: ab, bb
    ab = ab + b*bb
    bb = a*bb
  END SUBROUTINE BAR_B

  SUBROUTINE BAR(a, b) BIND(c)
    IMPLICIT NONE
    REAL :: a, b
    PRINT*, 'bar::  a b ', a, b
    b = a*b
    PRINT*, 'bar->  a b ', a, b
  END SUBROUTINE BAR

END MODULE M_DIFF

SUBROUTINE TEST_NODIFF()
  USE M_DIFF
  IMPLICIT NONE
  REAL :: x, y, z
  x = 2
  y = 1
  z = 3
  CALL TOP(x, y, z)
  PRINT*, 'x z', x, z
  PRINT*, 'y  ', y
END SUBROUTINE TEST_NODIFF

