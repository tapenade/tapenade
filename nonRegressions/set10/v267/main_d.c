/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
#include "include_d.h"
void top_d(float *x, float *xd);

long int p17 = 2;
static long int trois = 3;

/*
  Differentiation of test in forward (tangent) mode:
   variations   of useful results: *x
   with respect to varying inputs: p18 *x
   RW status of diff variables: p18:in x:(loc) *x:in-out
   Plus diff mem management of: x:in
*/
void test_d(float *x, float *xd) {
    printf("test: p17 %i \n", p17);
    printf("test: p18 %f \n", p18);
    top_d(x, xd);
}
