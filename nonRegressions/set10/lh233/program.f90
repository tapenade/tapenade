MODULE DiscreteNorms
    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: tt2
CONTAINS

  FUNCTION L2H1H2error(psi0, psi) result(error)
    REAL(KIND=8), DIMENSION(10) :: psi0, psi
    REAL(KIND=8)                :: error
    REAL(KIND=8)                :: Aire, tt1
    INTEGER i

    error = 0.0
    ALLOCATE(tt2(10))
    aire = SUM(psi0*psi)
    tt2 = psi0(:)**2 + psi(:)**2
    error = error + (Aire/3.)*SUM(tt2)                               
    DEALLOCATE(tt2)
    error = sqrt(error)
  END FUNCTION L2H1H2error
END MODULE DiscreteNorms

SUBROUTINE abcn_rmhd(X0,X)
  USE DiscreteNorms
  REAL(KIND=8), DIMENSION(10) :: X0,X
  REAL(KIND=8)                :: err2

  X(:) = X(:)*X0(:)
  X(5) = L2H1H2error(X0,X)
END SUBROUTINE abcn_rmhd

PROGRAM redmhd 
  USE DiscreteNorms
  REAL(KIND=8), DIMENSION(10) :: psi0, psi
  REAL(KIND=8)                :: L2error
  INTEGER i

  DO i=1,10
     psi0(i) = 1.0+i
     psi(i) = 2.0+i
  END DO
  CALL abcn_rmhd(psi0,psi)  
  L2error = L2H1H2error(psi0, psi)
  print*, 'norm psi (L2 H1 H2) = ', L2error
END PROGRAM redmhd

