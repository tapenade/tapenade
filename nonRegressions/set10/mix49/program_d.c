/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_functionPointers) - 19 Nov 2024 18:23
*/
#include <adContext.h>
#include <stdio.h>
#include <stdlib.h>
void bar(float *a, float *b, float *v, int n);
void bar_d(float *a, float *ad, float *b, float *bd, float *v, float *vd, int 
    n);
void result_(int *n, float *a, float *b, float *v);
void result_d_( int *, float *, float *, float *, float *, float *, float *);

/*
  Differentiation of foo in forward (tangent) mode (with options context):
   variations   of useful results: [alloc*v in foo] *y
   with respect to varying inputs: [alloc*v in foo] *x *y
   RW status of diff variables: [alloc*v in foo]:in-out x:(loc)
                *x:in y:(loc) *y:in-out
   Plus diff mem management of: x:in y:in
*/
void foo_d(float *x, float *xd, float *y, float *yd) {
    float *v = (void *)0;
    float *vd = (void *)0;
    int ii1;
    float *tmpresult;
    vd = calloc(6, 6*sizeof(float));
    for (ii1 = 0; ii1 < 6; ++ii1)
        vd[ii1] = 0.0;
    v = calloc(6, 6*sizeof(float));
    bar_d(x, xd, y, yd, v, vd, 3);
    vd = realloc_d(v, vd, 8*sizeof(float), &tmpresult);
    v = tmpresult;
    bar_d(x, xd, y, yd, v, vd, 4);
}

/*
  Differentiation of bar in forward (tangent) mode (with options context):
   variations   of useful results: [alloc*v in foo] *v *b
   with respect to varying inputs: [alloc*v in foo] *v *a *b
   Plus diff mem management of: v:in a:in b:in
*/
void bar_d(float *a, float *ad, float *b, float *bd, float *v, float *vd, int 
        n) {
    printf("bar n %d\n", n);
    result_d_(&n, a, ad, b, bd, v, vd);
}

/*
  Differentiation of main as a context to call tangent code (with options context):
*/
void main() {
    float x, y;
    float xd, yd;
    xd = 0.0;
    x = 2.0;
    yd = 0.0;
    y = 3.0;
    printf("x %f\n", x);
    adContextTgt_init(1.e-4, 0.87);
    adContextTgt_initReal4("x", &x, &xd);
    adContextTgt_initReal4("y", &y, &yd);
    foo_d(&x, &xd, &y, &yd);
    adContextTgt_startConclude();
    adContextTgt_concludeReal4("x", x, xd);
    adContextTgt_concludeReal4("y", y, yd);
    adContextTgt_conclude();
    printf("x %f y %f\n", x, y);
}
