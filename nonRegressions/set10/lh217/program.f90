! Plenty of tests on diff of array operations
! with SUMs, SPREADs, MASKS/WHERE, DIMs...
subroutine foo(A2,B2,C1,R0)
  real :: R0
  real, dimension(15) :: C1
  real, dimension(10,15) :: A2, B2

  R0 = SUM(A2*SPREAD(C1,1,10),MASK=A2.GT.B2)
end subroutine foo

program main
  real :: R0
  real, dimension(15) :: C1
  real, dimension(10,15) :: A2, B2
  integer i
  do i=1,15
     C1(i) = 10.0+i
     do j=1,10
        A2(j,i) = i+j-8.0
        B2(j,i) = i-j*2.5
     enddo
  enddo
  R0 = 0.0
  call foo(A2,B2,C1,R0)
  print *,'All results:',R0
end program main
