/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) - 25 Jan 2023 12:56
*/
#include <adContext.h>
#include <stdio.h>
#include <math.h>
#include "program_d.inc"

/*
  Differentiation of sub1 in forward (tangent) mode (with options context):
   variations   of useful results: *y
   with respect to varying inputs: pic *x *y
   RW status of diff variables: pic:in x:(loc) *x:in y:(loc) *y:in-out
   Plus diff mem management of: x:in y:in
*/
void sub1_d(double *x, double *xd, double *y, double *yd) {
    extern double f_(), g_();
    extern double f_d_(double *, double *, double *, double *, double *), g_d_
        (double *, double *, double *);
    double result1;
    double result1d;
    double result2;
    double result2d;
    result1d = f_d_(x, xd, &pic, &picd, &result1);
    result2d = g_d_(y, yd, &result2);
    *yd = result1d + pic*result2d + result2*picd;
    *y = result1 + result2*pic;
}

/*
  Differentiation of main as a context to call tangent code (with options context):
*/
void main() {
    double i = 2;
    double id = 0.0;
    double j = 3;
    double jd = 0.0;
    double result;
    #include "program1.inc"
    double picd;
    printf("i %f ...", i);
    printf(" j %f ...", j);
    adContextTgt_init(1.e-8, 0.87);
    adContextTgt_initReal8("pic", &pic, &picd);
    adContextTgt_initReal8("i", &i, &id);
    adContextTgt_initReal8("j", &j, &jd);
    sub1_d(&i, &id, &j, &jd);
    adContextTgt_startConclude();
    adContextTgt_concludeReal8("pic", pic, picd);
    adContextTgt_concludeReal8("i", i, id);
    adContextTgt_concludeReal8("j", j, jd);
    adContextTgt_conclude();
    printf("%f --> \n", j);
}
