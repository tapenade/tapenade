#include <stdio.h>
#include <math.h>

#include "program.inc"


void sub1 (double *x, double *y)
{
    extern double f_(), g_();

    *y = f_(x, &pic) + g_(y) * pic;
}

main()
{
  double i=2;
  double j=3;
  double result;
#include "program1.inc" // on redeclare pic localement
  printf("i %f ...", i);
  printf(" j %f ...", j);
  sub1(&i, &j);
  printf("%f --> \n",j);
}
