#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void top(double *A, int neq, int len, int *ia, int *iz, 
	double alpha, int precFlg, double *C, int *ier) {
	int	i=0, j=0, jlo=0, jup=2, k=0, klo=0, kup=2, l=0, llo=0, lup=2;
	int	id=0, nILU=2, m=0;
	double	factor;

	for (i=1; i<nILU; i++)
	{
                printf("for i %d\n", i);
		jlo = 0;
		jup = 1;
		for (j=jlo; j<jup; j++)
		{
                        printf("for j %d\n", j);
			C[j] /= C[iz[ia[j]]];
			klo = 0;	
			kup = 1;	
			for (k=klo; k<=kup; k++)
			{
                                printf("for k %d\n", k);
				m = ia[k];
				llo = 0;
				lup = 1;
                                ia[0] = 0;
                                ia[1] = 1;
				for (l=llo; l<=lup; l++)
				{
                                  if (ia[l]>ia[j]) {
                                    printf("break1 for l\n");
                                    break;
                                  }
                                  if (ia[0]<ia[1]) {
                                    printf("continue for l\n");
                                    continue;
                                  }
   				  C[k] -= C[j]*C[l];
                                  printf("break2 for l\n");
				  break;
				}
			}
		}
		id = iz[i];
		if (C[id]<1.0e-6){
		  return;
		}
	}
}

int main() {
  float x = 1;
  double *A, *C;
  int neq, len, *ia, *iz, precFlg;
  double alpha=0.0;
  int ier=0;
  iz = (int *) calloc(2, sizeof(int));
  ia = (int *) calloc(2, sizeof(int));
  C = (double *) calloc(2, sizeof(double));
  top(A,neq,len,ia,iz,alpha,precFlg,C,&ier);
}
