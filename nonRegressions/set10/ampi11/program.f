c   Bcast Allreduce Sum Reduce Sum original fortran (based Adol-C example) 
c   Modified by hand for AMPI

      SUBROUTINE head(x,y)
      implicit none
      include 'ampi/ampif.h'
      DOUBLE PRECISION x,y,t
      INTEGER world_rank, err_code
      INTEGER i
      INTRINSIC SIN

      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      IF (world_rank.EQ.0) THEN
         x = x * 2
      ENDIF
      call AMPI_Bcast(x,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,
     +   err_code)
      x = SIN( x ) * (world_rank + 1) 
      call AMPI_Allreduce(x,t,1,MPI_DOUBLE_PRECISION,MPI_SUM,
     +   MPI_COMM_WORLD,err_code)
      x = SIN( x * t )
      call AMPI_Reduce(x,y,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,
     +   MPI_COMM_WORLD,err_code)
      IF (world_rank.EQ.0) THEN
         y = y * 3
      ENDIF
 
      END SUBROUTINE

      PROGRAM main
      implicit none
      include 'ampi/ampif.h'
      INTEGER world_rank, err_code
      DOUBLE PRECISION x,y

      call AMPI_Init_NT(err_code)
      call AMPI_Comm_rank(MPI_COMM_WORLD, world_rank, err_code)
      IF (world_rank.EQ.0) THEN
         x = 3.5
      ENDIF
      call head(x,y)
      IF (world_rank.EQ.0) THEN
         print *, 'rank 0: process 0 got number ',y
      ENDIF
      call AMPI_Finalize_NT(err_code)
      END
