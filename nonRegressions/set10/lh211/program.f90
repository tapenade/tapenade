! Bug Migliore-Andra en mode -optim statictape:
! Dans le cas de boucles "naturelles" ou "wild",
! il ne faut pas faire de statictape, meme si
!  les boucles sont en apparence des "DO".

SUBROUTINE COMP_TRANS(X,Y,DT)
  IMPLICIT NONE
  REAL :: X,Y,TIME,DT
  INTEGER LL1,LL2
  INTEGER:: SP,CMPTR,NDELTAT_TRANS,STEP_TRANS
  LOGICAL:: FLOW
  INTEGER::METHOD,k,IDIFF
  INTEGER::I_PTF_PRECIP,ICONV_PRECIP,NB_FK_NEG
  INTEGER::ICONV_ADS,ICONV_ALL

  IF (X.GT.5) THEN
     X = 2*X
  ENDIF

  DO ST=1,LL1

     TIME=TIME+DT
     IF (ST.gt.15) THEN
        TIME=TIME+2*DT
     ENDIF

     DO SP=1,LL2
        CALL UPDATE_PARA_TRANS(X,Y,TIME)
        IF (X.GT.25) EXIT
        CALL VERIFY_BALANCE(X,Y,TIME)
        IF (X.gt.35) GOTO 222
     END DO

  END DO
  goto 999
 222 print *,'fin'
  stop
 999 continue

END SUBROUTINE COMP_TRANS
