C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (r7283) - 11 Feb 2019 16:09
C
C  Differentiation of main as a context to call adjoint code (with options context):
C
      PROGRAM MAIN_B
      IMPLICIT NONE
      INCLUDE 'ampi/ampif.h'
      INTEGER world_rank, err_code
      DOUBLE PRECISION x, y
      DOUBLE PRECISION xb, yb
C
      CALL AMPI_INIT_NT(err_code)
      CALL AMPI_COMM_RANK(mpi_comm_world, world_rank, err_code)
      IF (world_rank .EQ. 0) THEN
        x = 3.5
        PRINT*, 'process', world_rank, ' sends val  [3.50000000==]', x
        CALL ADCONTEXTADJ_INIT(0.87_8)
        CALL ADCONTEXTADJ_INITREAL8('x'//CHAR(0), x, xb)
        CALL ADCONTEXTADJ_INITREAL8('y'//CHAR(0), y, yb)
        CALL HEAD_B(x, xb, y, yb)
        CALL ADCONTEXTADJ_STARTCONCLUDE()
        CALL ADCONTEXTADJ_CONCLUDEREAL8('x'//CHAR(0), x, xb)
        CALL ADCONTEXTADJ_CONCLUDEREAL8('y'//CHAR(0), y, yb)
        CALL ADCONTEXTADJ_CONCLUDE()
        PRINT*, 'process', world_rank, ' recvs val  [1.97095979==]', y
        y = y + x
      ELSE IF (world_rank .EQ. 1) THEN
        x = 0.0
        y = 0.0
        CALL ADCONTEXTADJ_INIT(0.87_8)
        CALL ADCONTEXTADJ_INITREAL8('x'//CHAR(0), x, xb)
        CALL ADCONTEXTADJ_INITREAL8('y'//CHAR(0), y, yb)
        CALL HEAD_B(x, xb, y, yb)
        CALL ADCONTEXTADJ_STARTCONCLUDE()
        CALL ADCONTEXTADJ_CONCLUDEREAL8('x'//CHAR(0), x, xb)
        CALL ADCONTEXTADJ_CONCLUDEREAL8('y'//CHAR(0), y, yb)
        CALL ADCONTEXTADJ_CONCLUDE()
      ELSE
        CALL ADCONTEXTADJ_INIT(0.87_8)
        CALL ADCONTEXTADJ_STARTCONCLUDE()
        CALL ADCONTEXTADJ_CONCLUDE()
      END IF
      CALL AMPI_FINALIZE_NT(err_code)
      END

C  Differentiation of head in reverse (adjoint) mode (with options context):
C   gradient     of useful results: x y
C   with respect to varying inputs: x y
C   RW status of diff variables: x:in-out y:in-out
C Hand-turned into AMPI. Not differentiated.
C Simple Isend+Recv+Wait || Recv+Isend+Wait
C
C
      SUBROUTINE HEAD_B(x, xb, y, yb)
      IMPLICIT NONE
      INCLUDE 'ampi/ampif.h'
      DOUBLE PRECISION x, y
      DOUBLE PRECISION xb, yb
      INTEGER world_rank, err_code, req
      DOUBLE PRECISION local
      DOUBLE PRECISION localb
      INTRINSIC SIN
C
      CALL AMPI_COMM_RANK(mpi_comm_world, world_rank, err_code)
      IF (world_rank .EQ. 0) THEN
        x = x*2
        CALL FW_AMPI_ISEND(x, 1, mpi_double_precision, 1, 0, 
     +                     ampi_to_recv, mpi_comm_world, req, err_code)
        CALL FW_AMPI_RECV(y, 1, mpi_double_precision, 1, 0, 
     +                    ampi_from_isend_wait, mpi_comm_world, 
     +                    mpi_status_ignore, err_code)
        CALL FW_AMPI_WAIT(req, mpi_status_ignore, err_code)
        yb = 3*yb
        CALL BW_AMPI_WAIT(req, mpi_status_ignore, err_code)
        CALL BW_AMPI_RECV(yb, 1, mpi_double_precision, 1, 0, 
     +                    ampi_from_isend_wait, mpi_comm_world, 
     +                    mpi_status_ignore, err_code)
        CALL BW_AMPI_ISEND(xb, 1, mpi_double_precision, 1, 0, 
     +                     ampi_to_recv, mpi_comm_world, req, err_code)
        xb = 2*xb
        yb = 0.D0
      ELSE IF (world_rank .EQ. 1) THEN
        CALL FW_AMPI_RECV(local, 1, mpi_double_precision, 0, 0, 
     +                    ampi_from_isend_wait, mpi_comm_world, 
     +                    mpi_status_ignore, err_code)
        CALL PUSHREAL8(local)
        local = SIN(local)
        CALL FW_AMPI_ISEND(local, 1, mpi_double_precision, 0, 0, 
     +                     ampi_to_recv, mpi_comm_world, req, err_code)
        CALL FW_AMPI_WAIT(req, mpi_status_ignore, err_code)
        CALL BW_AMPI_WAIT(req, mpi_status_ignore, err_code)
        localb = 0.D0
        CALL BW_AMPI_ISEND(localb, 1, mpi_double_precision, 0, 0, 
     +                     ampi_to_recv, mpi_comm_world, req, err_code)
        CALL POPREAL8(local)
        localb = COS(local)*localb
        CALL BW_AMPI_RECV(localb, 1, mpi_double_precision, 0, 0, 
     +                    ampi_from_isend_wait, mpi_comm_world, 
     +                    mpi_status_ignore, err_code)
      END IF
      END

