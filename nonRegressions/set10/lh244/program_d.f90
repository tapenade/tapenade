!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_functionPointers) - 31 Oct 2024 16:16
!
! bug S.Carli July 2020: Overapproximation in ReqExplicit on calls, causing
! introduction of unused differentiated arguments. Needs a fix that detects that a
! subroutine doesn't at all use the diff of a pointer argument, and in that case
! avoids overapproximation of "identity" rows in RE matrices into rows with a "1".
MODULE MYMOD_DIFF
  IMPLICIT NONE
  TYPE TTT
      REAL, ALLOCATABLE :: vals(:)
  END TYPE TTT

CONTAINS
!  Differentiation of alloc_geometry as a context to call tangent code (with options context):
!   Plus diff mem management of: gg.vals:out
  SUBROUTINE ALLOC_GEOMETRY_D(gg, ggd)
    IMPLICIT NONE
    TYPE(TTT) :: gg
    TYPE(TTT) :: ggd
    ALLOCATE(ggd%vals(99))
    ggd%vals = 0.0
    ALLOCATE(gg%vals(99))
    gg%vals(:) = 1.5
  END SUBROUTINE ALLOC_GEOMETRY_D

  SUBROUTINE ALLOC_GEOMETRY(gg)
    IMPLICIT NONE
    TYPE(TTT) :: gg
    ALLOCATE(gg%vals(99))
    gg%vals(:) = 1.5
  END SUBROUTINE ALLOC_GEOMETRY

END MODULE MYMOD_DIFF

!  Differentiation of main as a context to call tangent code (with options context):
PROGRAM MAIN_D
  USE MYMOD_DIFF
  USE DIFFSIZES
!  Hint: ISIZE1OFDrfgeo_vals should be the size of dimension 1 of array *geo%vals
  IMPLICIT NONE
  REAL :: j
  REAL :: jd
  TYPE(TTT), SAVE :: geo
  TYPE(TTT), SAVE :: geod
  CALL ALLOC_GEOMETRY_D(geo, geod)
  CALL ADCONTEXTTGT_INIT(1.e-4_8, 0.87_8)
  CALL ADCONTEXTTGT_INITREAL4ARRAY('geo'//CHAR(0), geo%vals, geod%vals, &
&                            ISIZE1OFDrfgeo_vals)
  CALL ADCONTEXTTGT_INITREAL4('j'//CHAR(0), j, jd)
  CALL ROOT_D(geo, geod, j, jd)
  CALL ADCONTEXTTGT_STARTCONCLUDE()
  CALL ADCONTEXTTGT_CONCLUDEREAL4ARRAY('geo'//CHAR(0), geo%vals, geod%&
&                                vals, ISIZE1OFDrfgeo_vals)
  CALL ADCONTEXTTGT_CONCLUDEREAL4('j'//CHAR(0), j, jd)
  CALL ADCONTEXTTGT_CONCLUDE()
END PROGRAM MAIN_D

!  Differentiation of root in forward (tangent) mode (with options context):
!   variations   of useful results: [alloc*(gg.vals) in alloc_geometry]
!                c_corr_core_dn[save in foo2] j *(geo.vals)
!   with respect to varying inputs: [alloc*(gg.vals) in alloc_geometry]
!                c_corr_core_dn[save in foo2] j *(geo.vals)
!   RW status of diff variables: [alloc*(gg.vals) in alloc_geometry]:in-out
!                c_corr_core_dn[save in foo2]:in-out j:in-out geo.vals:(loc)
!                *(geo.vals):in-out
!   Plus diff mem management of: geo.vals:in-out
SUBROUTINE ROOT_D(geo, geod, j, jd)
  USE MYMOD_DIFF
  IMPLICIT NONE
  TYPE(TTT), INTENT(IN) :: geo
  TYPE(TTT), INTENT(IN) :: geod
  REAL :: j
  REAL :: jd
  EXTERNAL COST_FUNCTION
  EXTERNAL COST_FUNCTION_D
  CALL FOO1_D(geo)
  CALL COST_FUNCTION_D(geo, geod, j, jd)
  RETURN
END SUBROUTINE ROOT_D

!  Differentiation of foo1 in forward (tangent) mode (with options context):
!   variations   of useful results: c_corr_core_dn[save in foo2]
!   with respect to varying inputs: c_corr_core_dn[save in foo2]
SUBROUTINE FOO1_D(geo)
  USE MYMOD_DIFF
  IMPLICIT NONE
  TYPE(TTT), INTENT(IN) :: geo
  CALL FOO2_D(geo)
  RETURN
END SUBROUTINE FOO1_D

!  Differentiation of foo2 in forward (tangent) mode (with options context):
!   variations   of useful results: c_corr_core_dn
!   with respect to varying inputs: c_corr_core_dn
SUBROUTINE FOO2_D(geo)
  USE MYMOD_DIFF
  IMPLICIT NONE
  TYPE(TTT), INTENT(IN) :: geo
  REAL, SAVE :: c_corr_core_dn=1.0
  REAL, SAVE :: c_corr_core_dnd=0.0
  REAL :: res
  CALL FOO3_NODIFF(geo, res)
  RETURN
END SUBROUTINE FOO2_D

SUBROUTINE MAIN_NODIFF()
  USE MYMOD_DIFF
  IMPLICIT NONE
  REAL :: j
  TYPE(TTT), SAVE :: geo
  CALL ALLOC_GEOMETRY(geo)
  CALL ROOT_NODIFF(geo, j)
END SUBROUTINE MAIN_NODIFF

SUBROUTINE ROOT_NODIFF(geo, j)
  USE MYMOD_DIFF
  IMPLICIT NONE
  TYPE(TTT), INTENT(IN) :: geo
  REAL :: j
  EXTERNAL COST_FUNCTION
  CALL FOO1_NODIFF(geo)
  CALL COST_FUNCTION(geo, j)
  RETURN
END SUBROUTINE ROOT_NODIFF

SUBROUTINE FOO1_NODIFF(geo)
  USE MYMOD_DIFF
  IMPLICIT NONE
  TYPE(TTT), INTENT(IN) :: geo
  CALL FOO2_NODIFF(geo)
  RETURN
END SUBROUTINE FOO1_NODIFF

SUBROUTINE FOO2_NODIFF(geo)
  USE MYMOD_DIFF
  IMPLICIT NONE
  TYPE(TTT), INTENT(IN) :: geo
  REAL, SAVE :: c_corr_core_dn=1.0
  REAL :: res
  CALL FOO3_NODIFF(geo, res)
  RETURN
END SUBROUTINE FOO2_NODIFF

SUBROUTINE FOO3_NODIFF(geo, res)
  USE MYMOD_DIFF
  IMPLICIT NONE
  TYPE(TTT), INTENT(IN) :: geo
  REAL :: res
  PRINT*, 'geo%vals[2]:', geo%vals(2)
  res = geo%vals(2)*geo%vals(3)
END SUBROUTINE FOO3_NODIFF

