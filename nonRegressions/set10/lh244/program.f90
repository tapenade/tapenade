! bug S.Carli July 2020: Overapproximation in ReqExplicit on calls, causing
! introduction of unused differentiated arguments. Needs a fix that detects that a
! subroutine doesn't at all use the diff of a pointer argument, and in that case
! avoids overapproximation of "identity" rows in RE matrices into rows with a "1".
module mymod
type :: ttt
   real, allocatable :: vals(:)
end type ttt
contains

  subroutine alloc_geometry(gg)
    type(ttt) :: gg
    allocate(gg%vals(99))
    gg%vals(:) = 1.5
  end subroutine alloc_geometry
end module mymod


subroutine foo3(geo,res)
use mymod
type(ttt), INTENT(IN) :: geo
real res
print *,'geo%vals[2]:',geo%vals(2)
res = geo%vals(2)*geo%vals(3)
end subroutine foo3

subroutine foo2(geo)
use mymod
type(ttt), INTENT(IN) :: geo
real, save :: c_corr_core_dn = 1.0
real :: res
call foo3(geo, res)
return
end subroutine foo2

subroutine foo1(geo)
use mymod
type(ttt), INTENT(IN) :: geo
call foo2(geo)
return
end subroutine foo1

subroutine root(geo,j)
use mymod
type(ttt), INTENT(IN) :: geo
real :: j
call foo1(geo)
call cost_function(geo, j)
return
end subroutine root

program main
use mymod
real j
type(ttt), save :: geo
call alloc_geometry(geo)
call root(geo,j)
end program main
