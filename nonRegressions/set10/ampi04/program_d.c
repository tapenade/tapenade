/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_functionPointers) -  6 Dec 2024 11:13
*/
#include <adContext.h>
/* Program ported to AMPI. Porting should be made by the user
 * in general (e.g. for pairedWith), but could be automated sometimes.
 * Should behave identical to program.c
 * One proc just sends, one proc just receives */
#include <stdio.h>
#include <math.h>
#include "ampi/ampi.h"

/*
  Differentiation of head in forward (tangent) mode (with options context):
   variations   of useful results: *v2
   with respect to varying inputs: *v1 *v2
   RW status of diff variables: v1:(loc) *v1:in v2:(loc) *v2:in-out
   Plus diff mem management of: v1:in v2:in
*/
void head_d(double *v1, double *v1d, double *v2, double *v2d) {
    int world_rank;
    double temp;
    AMPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    double value;
    double valued;
    if (world_rank == 0) {
        temp = sqrt(*v1);
        valued = (*v1 == 0.0 ? 0.0 : *v1d/(2.0*temp));
        value = temp;
        TLS_AMPI_Send(&value, &valued, 1, MPI_DOUBLE, MPI_DOUBLE, 1, 0, 
                      AMPI_TO_RECV, MPI_COMM_WORLD);
    } else if (world_rank == 1) {
        TLS_AMPI_Recv(&value, &valued, 1, MPI_DOUBLE, MPI_DOUBLE, 0, 0, 
                      AMPI_FROM_SEND, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        *v2d = cos(value)*valued;
        *v2 = sin(value);
    }
}

/*
  Differentiation of main as a context to call tangent code (with options context):
*/
int main(int argc, char **argv) {
    AMPI_Init_NT(0, 0);
    int world_rank;
    AMPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    double x, y;
    double xd, yd;
    if (world_rank == 0) {
        xd = 0.0;
        x = 3.5;
        printf("set10/ampi04/program.c: process %i sets val  [3.500000==] %f \n"
               , world_rank, x);
        adContextTgt_init(1.e-8, 0.87);
        adContextTgt_initReal8("x", &x, &xd);
        adContextTgt_initReal8("y", &y, &yd);
        head_d(&x, &xd, &y, &yd);
        adContextTgt_startConclude();
        adContextTgt_concludeReal8("x", x, xd);
        adContextTgt_concludeReal8("y", y, yd);
        adContextTgt_conclude();
    } else if (world_rank == 1) {
        xd = 0.0;
        x = 0.0;
        yd = 0.0;
        y = 0.0;
        adContextTgt_init(1.e-8, 0.87);
        adContextTgt_initReal8("x", &x, &xd);
        adContextTgt_initReal8("y", &y, &yd);
        head_d(&x, &xd, &y, &yd);
        adContextTgt_startConclude();
        adContextTgt_concludeReal8("x", x, xd);
        adContextTgt_concludeReal8("y", y, yd);
        adContextTgt_conclude();
        printf("set10/ampi04/program.c: process %i gets val  [0.955327==] %f \n"
               , world_rank, y);
    }
    AMPI_Finalize_NT();
    return 0;
}
