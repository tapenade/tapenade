#include <stdio.h>
double mesh_en_c(double r_min, double r_max, double *a, int *N,
        double *mesh);

float top(double x, double y) {
   int N=5;
   double a, mesh[N];
   return mesh_en_c(x , y, &a, &N, mesh);
}

void main() {
   int N=5;
   double r_min, r_max, a, mesh[N];
   double result;
   r_min = 2;
   r_max = 4;
   result = top(r_min, r_max);
   printf("result %f\n", result);
}
