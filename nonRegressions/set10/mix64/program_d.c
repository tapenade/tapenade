/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) - 15 Dec 2022 13:06
*/
#include <adContext.h>
#include <stdio.h>
extern float Cstatix1;
extern float Cstatix1d;
extern float Cstatix2;
extern float Cstatix2d;
extern float CSTATIX3;
extern float CSTATIX3d;
extern float CSTATIX4;
extern void barf_nodiff(float *);
extern void barf_d(float *, float *);

/*
  Differentiation of foo in forward (tangent) mode (with options context):
   variations   of useful results: Cstatix1 Cstatix2 CSTATIX3
                *x
   with respect to varying inputs: *x
   RW status of diff variables: Cstatix1:out Cstatix2:out CSTATIX3:zero
                x:(loc) *x:in-out
   Plus diff mem management of: x:in
*/
void foo_d(float *x, float *xd) {
    printf("1 statix1 %f\n", Cstatix1);
    Cstatix1d = *xd;
    Cstatix1 = *x;
    CSTATIX3 = (*x)*(*x);
    printf("2 statix1 %f\n", Cstatix1);
    barf_d(x, xd);
    printf("3 statix1 %f\n", Cstatix1);
    CSTATIX3d = 0.0;
}

/*
  Differentiation of main as a context to call tangent code (with options context):
*/
void main() {
    float x, y, z;
    float xd;
    Cstatix1 = 999;
    xd = 0.0;
    x = 2.0;
    adContextTgt_init(1.e-4, 0.87);
    adContextTgt_initReal4("Cstatix1", &Cstatix1, &Cstatix1d);
    adContextTgt_initReal4("Cstatix2", &Cstatix2, &Cstatix2d);
    adContextTgt_initReal4("CSTATIX3", &CSTATIX3, &CSTATIX3d);
    adContextTgt_initReal4("x", &x, &xd);
    foo_d(&x, &xd);
    adContextTgt_startConclude();
    adContextTgt_concludeReal4("Cstatix1", Cstatix1, Cstatix1d);
    adContextTgt_concludeReal4("Cstatix2", Cstatix2, Cstatix2d);
    adContextTgt_concludeReal4("CSTATIX3", CSTATIX3, CSTATIX3d);
    adContextTgt_concludeReal4("x", x, xd);
    adContextTgt_conclude();
    printf("x %f statix1 %f statix2 %f \n", x, Cstatix1, Cstatix2);
}
