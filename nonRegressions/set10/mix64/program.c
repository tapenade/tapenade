#include <stdio.h>

extern float Cstatix1;
extern float Cstatix2;
extern float CSTATIX3;
extern float CSTATIX4;

extern void barf(float*);

void foo(float *x) {
  printf("1 statix1 %f\n", Cstatix1);
  Cstatix1 = *x;
  CSTATIX3 = *x * *x;
  printf("2 statix1 %f\n", Cstatix1);
  barf(x);
  printf("3 statix1 %f\n", Cstatix1);
}

void main() {
  float x, y, z;
  Cstatix1 = 999;
  x = 2.0;
  foo(&x);
  printf("x %f statix1 %f statix2 %f \n", x, Cstatix1, Cstatix2);
}
