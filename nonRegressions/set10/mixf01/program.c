#include <stdio.h>
#include <math.h>

void sub1 (double *x, double *y)
{
    extern double f(), g();

    *y = f(x) + g(y);
}

main()
{
  double i=2;
  double j=3;
  double result;
  printf("i %f ...", i);
  printf(" j %f ...", j);
  sub1(&i, &j);
  printf("%f --> \n",j);
}
