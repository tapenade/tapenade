double bar(double a, double *b);

double foo(double *x, double *y) {
  return bar(sin(*x), y);
}

void main() {
  double x, y, z;
  x = 2.0;
  printf("x %f\n", x);
  z = foo(&x, &y);
  printf("x %f y %f z %f\n", x, y, z);
}
