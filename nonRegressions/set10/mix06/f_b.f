C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.13 (r6785M) - 20 Mar 2018 09:23
C
C  Differentiation of add in reverse (adjoint) mode:
C   gradient     of useful results: d add b
C   with respect to varying inputs: d b
C Fortran function add.f - for C interlanguage call example
C Compile separately, then link to C program
      SUBROUTINE ADD_B(a, b, bb, c, d, db, addb)
      IMPLICIT NONE
      REAL*8 b, d
      REAL*8 bb, db
      INTEGER*4 a, c
      DIMENSION b(4), d(4)
      DIMENSION bb(4), db(4)
      REAL*8 add
      REAL*8 addb
      bb(a) = bb(a) + addb
      db(c) = db(c) + addb
      END

