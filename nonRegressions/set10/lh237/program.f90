      subroutine GRAD_solve3x3( status, n, A2, b2, x )

        Integer, Parameter :: kindAry = selected_int_kind(r=8)
        Integer, Parameter :: kindXYZ = selected_real_kind(p=13,r=200)

      Integer(kindAry)                , INTENT(IN)  :: n
      Integer(kindAry)                , INTENT(OUT) :: status

      Real(kindXYZ)   , dimension(3,3), INTENT(IN)  :: A2

      Real(kindXYZ)   , dimension(3,n), INTENT(IN)  :: b2

      Real(kindXYZ)   , dimension(3,n), INTENT(OUT) :: x

      Real(kindDQ) :: den

      Integer(kindAry)                :: i
      Integer(kindAry)                :: irow
      Real(kindXYZ)                   :: mag
      Real(kindXYZ), dimension(3,3)   :: a
      Real(kindXYZ), dimension(3,n)   :: b

      Real(kindDQ), parameter :: small = 1.0e-10_kindDQ
      Real(kindDQ), parameter :: one   = 1.0_kindDQ


      status = 0

      if ( idebug(225) == 0 ) then
        a = a2
        b = b2
      else
        do irow = 1, 3
          mag = maxval(abs(a(irow,:)))
          if ( mag < small ) then
            status = 1
            return
          end if
          a(irow,:) = a2(irow,:) / mag
          do i = 1, ngv
            b(irow,n) = b2(irow,n) / mag
          end do
        end do
      end if

      den = GRAD_det3D( A )
      if ( abs(den) < small ) then
        status = 1
        return
      else
        den = one / den
      end if

      x(1,:) = (  A(3,2)*A(2,3)*b(1,:) - A(2,2)*A(3,3)*b(1,:) - &
                  A(3,2)*A(1,3)*b(2,:) + A(1,2)*A(3,3)*b(2,:) + &
                  A(2,2)*A(1,3)*b(3,:) - A(1,2)*A(2,3)*b(3,:) )

      x(2,:) = ( -A(3,1)*A(2,3)*b(1,:) + A(2,1)*A(3,3)*b(1,:) + &
                  A(3,1)*A(1,3)*b(2,:) - A(1,1)*A(3,3)*b(2,:) - &
                  A(2,1)*A(1,3)*b(3,:) + A(1,1)*A(2,3)*b(3,:) )

      x(3,:) = (  A(3,1)*A(2,2)*b(1,:) - A(2,1)*A(3,2)*b(1,:) - &
                  A(3,1)*A(1,2)*b(2,:) + A(1,1)*A(3,2)*b(2,:) + &
                  A(2,1)*A(1,2)*b(3,:) - A(1,1)*A(2,2)*b(3,:) )

      x(:,:) = x(:,:) * den

      return
      end subroutine GRAD_solve3x3

