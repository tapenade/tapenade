! Bug signale par Jan Hueckelheim, 8 nov 2018
! La differentiation ajout(ait?e) une diff init yd=0.d0
! juste avant la boucle derivee
! Compilo refuse car assumed size yd (double yd(*))
SUBROUTINE BUGINIT(transa, nrows, csr_a, csr_ia, csr_ja, x, y)
  IMPLICIT NONE
  CHARACTER :: transa*1
  INTEGER :: nrows
  DOUBLE PRECISION :: csr_a(*)
  INTEGER :: csr_ja(*)
  INTEGER :: csr_ia(*)
  DOUBLE PRECISION :: x(*), y(*)
  INTEGER :: i, j
  IF (transa .NE. 'n' .AND. transa .NE. 'N') THEN
    WRITE(*, *) , 'Transposed matrices are not supported.'
    STOP
  ELSE
    DO i=1,nrows
      y(i) = 0
      DO j=csr_ia(i),csr_ia(i+1)-1
        y(i) = y(i) + x(csr_ja(j))*csr_a(j)
      END DO
    END DO
  END IF
END SUBROUTINE BUGINIT
