! Plenty of tests on diff of array operations
! with SUMs, SPREADs, MASKS/WHERE, DIMs...
subroutine foo(X,Y1,Y2,Z1,Z2)
  real, dimension(5) :: X
  real, dimension(10) :: Y1,Y2
  real, dimension(15) :: Z1
  real, dimension(10,15) :: Z2
  real a,b
  a = Y1(3)
  b = Y1(4)
  Y1(2:6) = X(:)**2 + SUM(X(:))
  Y1(:) = Y1(:) + SUM(Z2,DIM=2)
  Z1(:) = Z1(:) + SUM(Z2,DIM=1,MASK=Z2.GT.7)
  WHERE (Y2(:).GT.11.0)
     Y2(:) = Y2(:) + a*SUM(Z2*b,DIM=2,MASK=Z2.GT.6)
  ELSEWHERE
     Y2(:) = Y2(:) + SUM(Z2*SPREAD(Y1,2,15)*SPREAD(Z1,1,10))
  END WHERE
  WHERE (Y1.GT.3.1)
     Y2(:) = Y2(:) + SUM(Z2, 2, Z2.GT.3.2)
  END WHERE
  WHERE (Z2.GT.4.1)
     Z2 = Z2 + SPREAD(b*Y2, 2, 15)
  END WHERE
  X(4) = a+b
end subroutine foo

program main
  real, dimension(5) :: X
  real, dimension(10) :: Y1,Y2
  real, dimension(15) :: Z1
  real, dimension(10,15) :: Z2
  integer i
  do i=1,5
     X(i) = 10.0+i
  enddo
  do i=1,10
     Y1(i) = 10.0+i
     Y2(i) = 20.0+i
  enddo
  do i=1,15
     Z1(i) = 10.0+i
     do j=1,10
        Z2(j,i) = i+j+1.5
     enddo
  enddo
  call foo(X,Y1,Y2,Z1,Z2)
  print *,'Sum of all results:',SUM(X),SUM(Y1+Y2),SUM(Z1),SUM(Z2)
end program main
