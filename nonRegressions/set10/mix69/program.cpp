extern"C" {
  void mul_int_( float* a, int* b , float* result);
}

void mul_int_( float* a, int* b , float* result) {
    *result = (*a) * (*b);
}

