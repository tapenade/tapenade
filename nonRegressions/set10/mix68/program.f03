module m1
   use iso_c_binding
   type, bind(c) :: myftype
     integer(c_int) :: i, j
     real(c_float) :: s
   end type myftype
end module m1

module m2
   interface
     subroutine test_in_c(x) bind(c, name = 'testc')
        use iso_c_binding, only : c_int, c_float
        use m1
        type(myftype) :: x
     end subroutine test_in_c
   end interface
end module m2

subroutine test(var) 
   use m1
   use m2
   type(myftype) :: var
   print*, '1 xx%s = ', var%s
   var%s = var%i * var%j * var%s
   print*, '2 xx%s = ', var%s
   call test_in_c(var)
end subroutine test

program top
   use m1
   type(myftype) :: xx
   xx%i = 2
   xx%j = 3
   xx%s = 1
   call test(xx)
   print*, 'xx%s = ', xx%s
end program top
