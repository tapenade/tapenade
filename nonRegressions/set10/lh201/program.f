c Bug found by Dare Imam-Lawal: wrong propagation in data-flow (e.g.TBR),
c in the part devoted to the "pseudo" array-section analysis,
c due to wrong use of loopRunsAtLeastOnce() instead of cleaner infoFromEntryMask()
      SUBROUTINE foo(co,kon,v,shp,vkl,xl)
      IMPLICIT none
      REAL*8 co(3,100), v(0:3,26),  shp(4,26),
     &       xl(3,26),  vl(0:3,26), vkl(0:3,3)
      INTEGER kon(*),konl(26)
      INTEGER i,j,k,indexe,m1,m3

      indexe = 7
      do i=1,10
         do j=1,26
            konl(j)=kon(indexe+j)
            do k=1,3
               xl(k,j)=co(k,konl(j))
               vl(k,j)=v(k,konl(j))
            enddo
            vl(0,j)=v(0,konl(j))
         enddo
 
         do m1=1,26
            do m3=1,3
               vkl(0,m3)=vkl(0,m3)+shp(m3,m1)*vl(0,m1)
            enddo
         enddo

      enddo

      end
