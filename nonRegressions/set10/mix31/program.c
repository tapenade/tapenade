// from lh39

/* Subroutine */ int sub1_(i1, i2, o1, o2)
float *i1, *i2, *o1, *o2;
{
    static float l1, l2;


    l1 = *i1 * *i2;
    l2 = *i1 - *i2 * 3;
    *o1 = l1 / l2;
    *o2 = (float)35.;
    *i1 = (float)99.;
} /* sub1_ */

