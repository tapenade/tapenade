!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (r7283) - 11 Feb 2019 16:09
!
!  Differentiation of myprog as a context to call adjoint code (with options context):
!------------------------------------------------
PROGRAM MYPROG_B
  IMPLICIT NONE
  INCLUDE 'ampi/ampif.h'
  REAL*8 :: x(5), y(5), z(5)
  REAL*8 :: xb(5), yb(5), zb(5)
  INTEGER :: ierr, myid, nprocs, i
  CALL AMPI_INIT_NT(ierr)
  CALL AMPI_COMM_RANK(mpi_comm_world, myid, ierr)
  CALL AMPI_COMM_SIZE(mpi_comm_world, nprocs, ierr)
  DO i=1,5
    x(i) = 10.0*myid + i - 0.5
    y(i) = 10.0*myid + i
    z(i) = 10.0*myid + i + 0.5
  END DO
  IF (myid .EQ. 0) THEN
    WRITE(*, *) 'x=', x
    WRITE(*, *) 'y=', y
    WRITE(*, *) 'z=', z
  END IF
  CALL ADCONTEXTADJ_INIT(0.87_8)
  CALL ADCONTEXTADJ_INITREAL8ARRAY('x'//CHAR(0), x, xb, 5)
  CALL ADCONTEXTADJ_INITREAL8ARRAY('y'//CHAR(0), y, yb, 5)
  CALL ADCONTEXTADJ_INITREAL8ARRAY('z'//CHAR(0), z, zb, 5)
  CALL TEST_B(mpi_comm_world, myid, mpi_sum, x, xb, y, yb, z, zb)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('x'//CHAR(0), x, xb, 5)
  CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('y'//CHAR(0), y, yb, 5)
  CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('z'//CHAR(0), z, zb, 5)
  CALL ADCONTEXTADJ_CONCLUDE()
  IF (myid .EQ. 0) THEN
    WRITE(*, *) 'x=', x
    WRITE(*, *) 'y=', y
    WRITE(*, *) 'z=', z
  END IF
  CALL AMPI_FINALIZE_NT(ierr)
END PROGRAM MYPROG_B

!  Differentiation of test in reverse (adjoint) mode (with options context):
!   gradient     of useful results: x y z
!   with respect to varying inputs: x y z
!   RW status of diff variables: x:in-out y:in-out z:in-out
SUBROUTINE TEST_B(comm, rank, op, x, xb, y, yb, z, zb)
  IMPLICIT NONE
  INCLUDE 'ampi/ampif.h'
  INTEGER*4, INTENT(IN) :: comm
  INTEGER*4, INTENT(IN) :: rank
  INTEGER*4, INTENT(IN) :: op
  REAL*8, INTENT(INOUT) :: x(5), y(5), z(5)
  REAL*8, INTENT(INOUT) :: xb(5), yb(5), zb(5)
  INTERFACE 
      SUBROUTINE UTAMPI_REDUCE_1DR8(comm, rank, root, op, srbuf)
        INTEGER*4, INTENT(IN) :: comm
! Current rank
        INTEGER*4, INTENT(IN) :: rank
! Rank to gather to (-1 == use all_gather).
        INTEGER*4, INTENT(IN) :: root
        INTEGER*4, INTENT(IN) :: op
        REAL*8, INTENT(INOUT) :: srbuf(:)
      END SUBROUTINE UTAMPI_REDUCE_1DR8
  END INTERFACE

  INTERFACE 
      SUBROUTINE UTAMPI_REDUCE_1DR8_B(comm, rank, root, op, srbuf, &
&       srbufb)
        INTEGER*4, INTENT(IN) :: comm
! Current rank
        INTEGER*4, INTENT(IN) :: rank
! Rank to gather to (-1 == use all_gather).
        INTEGER*4, INTENT(IN) :: root
        INTEGER*4, INTENT(IN) :: op
        REAL*8, INTENT(INOUT) :: srbuf(:)
        REAL*8, INTENT(INOUT) :: srbufb(:)
      END SUBROUTINE UTAMPI_REDUCE_1DR8_B
  END INTERFACE

  CALL UTAMPI_REDUCE_1DR8_B(comm, rank, 1, op, z, zb)
  CALL UTAMPI_REDUCE_1DR8_B(comm, rank, 0, op, y, yb)
  CALL UTAMPI_REDUCE_1DR8_B(comm, rank, -1, op, x, xb)
END SUBROUTINE TEST_B

!  Differentiation of utampi_reduce_1dr8 in reverse (adjoint) mode (with options context):
!   gradient     of useful results: srbuf
!   with respect to varying inputs: srbuf
! Bug example code given by UTRC / Razvan Florea / 12 Dec 2018
SUBROUTINE UTAMPI_REDUCE_1DR8_B(comm, rank, root, op, srbuf, srbufb)
  IMPLICIT NONE
  INCLUDE 'ampi/ampif.h'
  INTEGER*4, INTENT(IN) :: comm
! Current rank
  INTEGER*4, INTENT(IN) :: rank
! Rank to gather to (-1 == use all_gather).
  INTEGER*4, INTENT(IN) :: root
  INTEGER*4, INTENT(IN) :: op
  REAL*8, INTENT(INOUT) :: srbuf(:)
  REAL*8, INTENT(INOUT) :: srbufb(:)
  INTEGER*4 :: n, err
  INTRINSIC SIZE
  n = SIZE(srbuf)
  IF (root .LT. 0) THEN
    CALL FW_AMPI_ALLREDUCE(mpi_in_place, srbuf, n, mpi_double_precision&
&                    , op, comm, err)
    CALL BWS_AMPI_ALLREDUCE(mpi_in_place, mpi_in_place, srbuf, srbufb, n&
&                     , mpi_double_precision, mpi_double_precision, op, &
&                     0, comm, err)
  ELSE IF (root .EQ. rank) THEN
    CALL FW_AMPI_REDUCE(mpi_in_place, srbuf, n, mpi_double_precision, op&
&                 , root, comm, err)
    CALL BWS_AMPI_REDUCE(mpi_in_place, mpi_in_place, srbuf, srbufb, n, &
&                  mpi_double_precision, mpi_double_precision, op, 0, &
&                  root, comm, err)
  ELSE
    CALL FW_AMPI_REDUCE(srbuf, srbuf, n, mpi_double_precision, op, root&
&                 , comm, err)
    CALL BWS_AMPI_REDUCE(srbuf, srbufb, srbuf, srbufb, n, &
&                  mpi_double_precision, mpi_double_precision, op, 0, &
&                  root, comm, err)
  END IF
END SUBROUTINE UTAMPI_REDUCE_1DR8_B

