! To reproduce bug opa-nemo with -optim statictape
! ad_save arrays sometimes declared as e.g. DIMENSION(71, 81), DIMENSION(90)
! and also wrongly used in the code.
SUBROUTINE dyn_vor_ene_ens(ua, un, rotn, va, ztdva, vn, ztdua, ze3f,  &
     &                     e1u,e2u,e1v,e2v,ff)
  REAL(wp), DIMENSION(71, 81, 91) :: ua,un,rotn,va,ztdva,vn,ztdua,ze3f
  REAL(wp), DIMENSION(71,81) :: e1u,e2u,e1v,e2v,ff

  INTEGER ::   ji, jj, jk
  REAL(wp) ::   zfac12, zua, zva
  REAL(wp), DIMENSION(71,81) :: zwx, zwy, zwz, ztnw, ztne, ztsw, ztse, zcor
  REAL(wp), DIMENSION(71,81,91) :: zcu, zcv

  zfac12 = 1.e0 / 12.e0

  DO jk = 1, 90 
     zwz(:,:) = ( rotn(:,:,jk) + ff(:,:) ) * ze3f(:,:,jk)
     zwx(:,:) = e2u(:,:) * un(:,:,jk)
     zwy(:,:) = e1v(:,:) * vn(:,:,jk)
     zcor(:,:) = ff(:,:) * ze3f(:,:,jk)
     jj=2
     ztne(1,:) = 0 ; ztnw(1,:) = 0 ; ztse(1,:) = 0 ; ztsw(1,:) = 0
     DO ji = 2, 71   
        ztne(ji,jj) = zwz(ji-1,jj  ) + zwz(ji  ,jj  ) + zwz(ji  ,jj-1)
        ztnw(ji,jj) = zwz(ji-1,jj-1) + zwz(ji-1,jj  ) + zwz(ji  ,jj  )
        ztse(ji,jj) = zwz(ji  ,jj  ) + zwz(ji  ,jj-1) + zwz(ji-1,jj-1)
        ztsw(ji,jj) = zwz(ji  ,jj-1) + zwz(ji-1,jj-1) + zwz(ji-1,jj  )
     END DO
     DO jj = 3, 81
        DO ji = 2, 71
           ztne(ji,jj) = zwz(ji-1,jj  ) + zwz(ji  ,jj  ) + zwz(ji  ,jj-1)
           ztnw(ji,jj) = zwz(ji-1,jj-1) + zwz(ji-1,jj  ) + zwz(ji  ,jj  )
           ztse(ji,jj) = zwz(ji  ,jj  ) + zwz(ji  ,jj-1) + zwz(ji-1,jj-1)
           ztsw(ji,jj) = zwz(ji  ,jj-1) + zwz(ji-1,jj-1) + zwz(ji-1,jj  )
        END DO
     END DO

     DO jj = 2, 80
        DO ji = 2, 70
           zua = + zfac12 / e1u(ji,jj) * (  ztne(ji,jj  ) * zwy(ji  ,jj  ) + ztnw(ji+1,jj) * zwy(ji+1,jj  )   &
                &                           + ztse(ji,jj  ) * zwy(ji  ,jj-1) + ztsw(ji+1,jj) * zwy(ji+1,jj-1) )
           zva = - zfac12 / e2v(ji,jj) * (  ztsw(ji,jj+1) * zwx(ji-1,jj+1) + ztse(ji,jj+1) * zwx(ji  ,jj+1)   &
                &                           + ztnw(ji,jj  ) * zwx(ji-1,jj  ) + ztne(ji,jj  ) * zwx(ji  ,jj  ) )
           ua(ji,jj,jk) = ua(ji,jj,jk) + zua
           va(ji,jj,jk) = va(ji,jj,jk) + zva
        END DO
     END DO
  END DO

  DO jk = 1, 90
     DO jj = 2, 80
        DO ji = 2, 70
           zcu(ji,jj,jk) = + zfac12 / e1u(ji,jj) * (  zcor(ji,jj  ) * zwy(ji  ,jj  ) + zcor(ji+1,jj) * zwy(ji+1,jj  )   &
                &                                      + zcor(ji,jj  ) * zwy(ji  ,jj-1) + zcor(ji+1,jj) * zwy(ji+1,jj-1) )
           zcv(ji,jj,jk) = - zfac12 / e2v(ji,jj) * (  zcor(ji,jj+1) * zwx(ji-1,jj+1) + zcor(ji,jj+1) * zwx(ji  ,jj+1)   &
                &                                      + zcor(ji,jj  ) * zwx(ji-1,jj  ) + zcor(ji,jj  ) * zwx(ji  ,jj  ) )
        END DO
     END DO
  END DO

  ztdua(:,:,:) = ua(:,:,:) - ztdua(:,:,:) - zcu(:,:,:) 
  ztdva(:,:,:) = va(:,:,:) - ztdva(:,:,:) - zcv(:,:,:) 

END SUBROUTINE dyn_vor_ene_ens
