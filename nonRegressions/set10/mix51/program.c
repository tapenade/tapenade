#include <stdio.h>

enum
{
  ZERO = 0,
  UN, DEUX, TROIS,
  QUATRE = 4,
  HUIT = 5 + TROIS,
  NEUF = TROIS + 6
};

const int SIX = 4 + DEUX;

void main() {
  printf("SIX %d..\n" , SIX);
  printf("HUIT %d..\n" , HUIT);
  printf("NEUF %d..\n" , NEUF);
}
