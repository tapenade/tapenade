subroutine top(x,y) bind(c)
    integer :: STATUS = 99
    real, value :: x
    real :: y

     if (x .ge. 0.0) then
      print *, 'This program is going to exit. ', x
      y = x * y 
      call EXIT(STATUS)
     else 
      y = x * x * y
     endif

    print *, 'End of top'
end subroutine top
