void top(float a, float *b);

void foo(float *x, float *y) {
  top(*x, y);
}

void main() {
  float x, y;
  x = 2.0;
  y = 1.0;
  printf("x %f\n", x);
  foo(&x, &y);
  printf("x %f y %f\n", x, y);
  exit(0);
}
