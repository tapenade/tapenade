
! bug S.Carli June 2020: introduced an uninitialized cutlod!
      function expu (arg,xx,yy)
      implicit none
      real :: xx,yy
      real :: expu, arg, cutlo
      intrinsic dim

      cutlo = 0.0
      expu = dim(arg,cutlo)
      xx = dim(xx,yy)
 
      return
      end function expu
