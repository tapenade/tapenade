       program mul
       use iso_c_binding, only : c_int, c_float
       integer(c_int) :: b
       real(c_float) :: a, r
       interface
         subroutine mul_int(x, y, r) bind(c, name='mul_int_C')
           use iso_c_binding, only : c_int, c_float
           real(c_float) :: x, r
           integer(c_int) :: y
         end subroutine mul_int
       end interface
       a=9.0
       b=2
       call mul_int( a, b , r)
       print*, ' r', r
       end
