
module real_precision
  implicit none
  INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(12,307)
  INTEGER, PARAMETER :: wp = dp
end module real_precision

module dimension_1D
  use real_precision
  INTEGER,parameter :: nx=251
  REAL(KIND=wp),dimension(nx) :: bm
  REAL(KIND=wp),dimension(nx) :: Ts
  REAL(KIND=wp),dimension(nx) :: Acc
  REAL(KIND=wp),dimension(nx) :: Abl
end module dimension_1D

module testmod
  use real_precision
  use dimension_1D
  REAL(KIND=wp) :: beta_acc
  REAL(KIND=wp) :: Acc0
  REAL(KIND=wp) :: Abl_0
  REAL(KIND=wp) :: T_nomelt

contains

  ! Bug git#9, on nodet-winnie
  subroutine bilan_masse
    implicit none

    Abl(:)=0.
    Acc(:)=Acc0*exp(-beta_acc*Ts(:))
    where (Ts(:).ge.T_nomelt)  Abl(:)=Abl_0*(Ts(:)-T_nomelt)**2/T_nomelt**2
    bm(:)=Acc(:)+Abl(:)
  end subroutine bilan_masse

end module testmod
