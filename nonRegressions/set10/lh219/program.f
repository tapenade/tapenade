C To test complex numbers
      SUBROUTINE test(a,b,x,y)
      real a,b,x,y
      complex z1,z2
      complex*8 zz1,zz2,zz3
      z1 = cmplx(a,b)
      z2 = cmplx(a)
      zz1 = cmplx(a,b, kind=8)
      zz2 = cmplx(a, kind=8)
      z1 = z1*z1
      z2 = z1*z2
      zz1 = zz1*zz1
      zz2 = zz1*zz2
      x = REAL(z1)*REAL(z2)
      y = AIMAG(z1)*AIMAG(z2)
      y = y + REAL(zz1)*AIMAG(zz2)
      zz3 = CMPLX(zz1)
      x = x + AIMAG(zz3)
      END

      REAL a,b,x,y
      a = 1.5
      b = -3.5
      call test(a,b,x,y)
      print*,x
      print*,y
      end
