module example
      implicit none
      type rect
         real :: x, y
      end type rect

contains

      subroutine foo(w, z)
      type(rect) :: w
      real :: z
      external surface
      call surface(w,z) 
      end subroutine
end module

      program top
      use example
      real :: w, h, b
      type(rect) :: a
      w = 20.0
      h = 3.0
      a%x = w
      a%y = h
      call foo(a, b)
      print*, 'w ', w, ' h ', h , ' b ', b
      end program
