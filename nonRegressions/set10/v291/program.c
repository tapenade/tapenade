/* pour tester les CompositeTypeSpec non declare's
 */
void boundaryConditionT(double *lycb, double h_05b, double ro_05b, double u_05b, double v_05b) {
   double ro;
   int i, gapSize, Nx, Ny;

   if(i + gapSize >= Nx) {
      if(v_05b > 0.0) {
         ro = 2.0 * getInvy05(2, h_05b, ro_05b, u_05b, v_05b, MPB.GetC(i, Ny-1))
                  - getInvy(2, h_05b, ro_05b, u_05b, v_05b, n.h.GetB(i, Ny-1), n.ro.GetB(i, Ny-1), 
                            n.u.GetB(i, Ny-1), n.v.GetB(i, Ny-1), MPB.GetB(i, Ny-1));
         cFY(2, i, Ny-1, ro, params, MPB, n, lycb[2], h_05b, ro_05b, u_05b, v_05b);
         double u = 2.0 * getInvy05(3, h_05b, ro_05b, u_05b, v_05b, MPB.GetC(i, Ny-1))
                        - getInvy(3, h_05b, ro_05b, u_05b, v_05b, n.h.GetB(i, Ny-1), n.ro.GetB(i, Ny-1), 
                                  n.u.GetB(i, Ny-1), n.v.GetB(i, Ny-1), MPB.GetB(i, Ny-1));
         cFY(3, i, Ny-1, u, params, MPB, n, lycb[3], h_05b, ro_05b, u_05b, v_05b);
         n1.u.SetT(i, Ny-1, u);
      } else {
         ro = params.ro_T;
         n1.u.SetT(i, Ny-1, 0.0);
      }
      n1.ro.SetT(i, Ny-1, ro);
      double rop0 = (sqrt(MPg * h_05b) / (2.0 * ro_05b)) * ro;
      double rop1 = (sqrt(MPg * params.H_T) / (2.0 * params.ro_T)) * ro;
      double Iy0 = 2.0 * getInvy05(0, h_05b, ro_05b, u_05b, v_05b, MPB.GetC(i, Ny-1))
                       - getInvy(0, h_05b, ro_05b, u_05b, v_05b, n.h.GetB(i, Ny-1), n.ro.GetB(i, Ny-1), 
                                 n.u.GetB(i, Ny-1), n.v.GetB(i, Ny-1), MPB.GetB(i, Ny-1));
      cFY(0, i, Ny-1, Iy0, params, MPB, n, lycb[0], h_05b, ro_05b, u_05b, v_05b);
      double Iy1 = -sqrt(MPg / (params.H_T)) * params.H_T - rop1;
      double a0 = sqrt(MPg / h_05b);
      double a1 = sqrt(MPg / params.H_T);
      n1.h.SetT(i, Ny-1, ((Iy0 - rop0) - (Iy1 + rop1)) / (a0 + a1) - MPB.GetB(i, Ny-1));
      n1.v.SetT(i, Ny-1, (a1 * (Iy0 - rop0) + a0 * (Iy1 + rop1)) / (a0 + a1));
   }
}
