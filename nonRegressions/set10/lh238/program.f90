! Bug found by Michael Vossbeck:
! wrong cumulWithOper -> wrong activity analysis.
subroutine foo(x, m, y)
  implicit none
  !-- arguments
  integer, intent(in) :: m
  real, intent(in) :: x
  real, intent(out) :: y(m)
  !-- local
  type restyp
     real :: f1(1)
  end type restyp
  type(restyp) :: mid_res
  integer :: j

  y = 0.
  do j=1,m
     call init_res(mid_res)
     mid_res%f1 = mid_res%f1*x
     y(j) = y(j) + mid_res%f1(1)
  end do

contains
  subroutine init_res(t)
    implicit none
    type(restyp) :: t
    t%f1 = (/1.5/)
  end subroutine init_res

end subroutine foo
