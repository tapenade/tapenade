MODULE okmod_interf
  INTERFACE oksubr
     SUBROUTINE oksubr(x)
       REAL x
     END SUBROUTINE oksubr
  END INTERFACE
END MODULE okmod_interf

SUBROUTINE oksubr(x)
  REAL x
  x = x*x
END SUBROUTINE oksubr

! This module declares an interface (1st time)
Module BC_FaceSimple_interface

  Interface BC_FaceSimple

      subroutine BC_FaceSimple( bccode, bldrgn, srfid, bcmode, bcvals, mul, &
                                qp, qbnd, fnrm, fvel, fxyz, pxyz, nxyz, qf )

        Integer(kindAry), INTENT(IN)                         :: bccode
        Integer(kindAry), INTENT(IN)                         :: bldrgn
        Integer(kindAry), INTENT(IN)                         :: srfid
        Character(*), INTENT(IN)                             :: bcmode
        Real(kindQ), dimension(1), INTENT(IN)                :: bcvals
        Real(kindQ), INTENT(IN)                              :: mul
        Real(kindQ), dimension(1), INTENT(IN)                :: qp
        Real(kindQ), dimension(1), INTENT(IN)                :: qbnd
        Real(kindXYZ), dimension(1), INTENT(IN)              :: fnrm
        Real(kindQ), dimension(1), INTENT(IN)                :: fvel
        Real(kindXYZ), dimension(1), INTENT(IN)              :: fxyz
        Real(kindXYZ), dimension(1), INTENT(IN)              :: pxyz
        Real(kindXYZ), dimension(1), INTENT(OUT)             :: nxyz
        Real(kindQ), dimension(1), INTENT(OUT)               :: qf
      end subroutine BC_FaceSimple

  End Interface

End Module BC_FaceSimple_interface


! This is a SECOND declaration of the same interface,
! which is perfectly legal and Tapenade should react well.
Module othermod_interface

  Interface BC_FaceSimple

      subroutine BC_FaceSimple( bccode, bldrgn, srfid, bcmode, bcvals, mul, &
                                qp, qbnd, fnrm, fvel, fxyz, pxyz, nxyz, qf )

        Integer(kindAry), INTENT(IN)                         :: bccode
        Integer(kindAry), INTENT(IN)                         :: bldrgn
        Integer(kindAry), INTENT(IN)                         :: srfid
        Character(*), INTENT(IN)                             :: bcmode
        Real(kindQ), dimension(1), INTENT(IN)                :: bcvals
        Real(kindQ), INTENT(IN)                              :: mul
        Real(kindQ), dimension(1), INTENT(IN)                :: qp
        Real(kindQ), dimension(1), INTENT(IN)                :: qbnd
        Real(kindXYZ), dimension(1), INTENT(IN)              :: fnrm
        Real(kindQ), dimension(1), INTENT(IN)                :: fvel
        Real(kindXYZ), dimension(1), INTENT(IN)              :: fxyz
        Real(kindXYZ), dimension(1), INTENT(IN)              :: pxyz
        Real(kindXYZ), dimension(1), INTENT(OUT)             :: nxyz
        Real(kindQ), dimension(1), INTENT(OUT)               :: qf
      end subroutine BC_FaceSimple

  End Interface

End Module othermod_interface

! This is the final definition of the subroutine whose interface was given twice.
subroutine BC_FaceSimple( bccode, bldrgn, srfid, bcmode, bcvals, mul, &
     qp, qbnd, fnrm, fvel, fxyz, pxyz, nxyz, qf )

  Integer(kindAry), INTENT(IN)                         :: bccode
  Integer(kindAry), INTENT(IN)                         :: bldrgn
  Integer(kindAry), INTENT(IN)                         :: srfid
  Character(*), INTENT(IN)                             :: bcmode
  Real(kindQ), dimension(:), INTENT(IN)                :: bcvals
  Real(kindQ), INTENT(IN)                              :: mul
  Real(kindQ), dimension(:), INTENT(IN)                :: qp
  Real(kindQ), dimension(:), INTENT(IN)                :: qbnd
  Real(kindXYZ), dimension(:), INTENT(IN)              :: fnrm
  Real(kindQ), dimension(:), INTENT(IN)                :: fvel
  Real(kindXYZ), dimension(:), INTENT(IN)              :: fxyz
  Real(kindXYZ), dimension(:), INTENT(IN)              :: pxyz
  Real(kindXYZ), dimension(:), INTENT(OUT)             :: nxyz
  Real(kindQ), dimension(:), INTENT(OUT)               :: qf

  fnrm(:) = SIN(SUM(qp))

end subroutine BC_FaceSimple


! The following module declaration is wrong code, because this
! module was declared before, but it is important that Tapenade
! reacts/recovers well (as we didn't choose to exit gracefully on DD16)
Module BC_FaceSimple_interface

  Interface BC_FaceSimple

      subroutine BC_FaceSimple( bccode, bldrgn, srfid, bcmode, bcvals, mul, &
                                qp, qbnd, fnrm, fvel, fxyz, pxyz, nxyz, qf )

        Integer(kindAry), INTENT(IN)                         :: bccode
        Integer(kindAry), INTENT(IN)                         :: bldrgn
        Integer(kindAry), INTENT(IN)                         :: srfid
        Character(*), INTENT(IN)                             :: bcmode
        Real(kindQ), dimension(1), INTENT(IN)                :: bcvals
        Real(kindQ), INTENT(IN)                              :: mul
        Real(kindQ), dimension(1), INTENT(IN)                :: qp
        Real(kindQ), dimension(1), INTENT(IN)                :: qbnd
        Real(kindXYZ), dimension(1), INTENT(IN)              :: fnrm
        Real(kindQ), dimension(1), INTENT(IN)                :: fvel
        Real(kindXYZ), dimension(1), INTENT(IN)              :: fxyz
        Real(kindXYZ), dimension(1), INTENT(IN)              :: pxyz
        Real(kindXYZ), dimension(1), INTENT(OUT)             :: nxyz
        Real(kindQ), dimension(1), INTENT(OUT)               :: qf
      end subroutine BC_FaceSimple

  End Interface

End Module BC_FaceSimple_interface

! This top diff unit should trigger standard diff
!  of BC_FaceSimple and aeroaxis_force
SUBROUTINE US_Loads2( q, len )

  Use BC_FaceSimple_interface
  USE okmod_interf

  Implicit none

!-----Argument variables

  Integer :: len
  Real(kindQ) , dimension(len,len), INTENT(IN) :: q

! Common with a pointer:

  Real(kindQ), dimension(10), POINTER :: flift
  COMMON /ccc/ flift

!-----Local variables

  Integer(kindInt)                         :: bccode
  Integer(kindInt)                         :: bldrgn
  Integer(kindInt)                         :: srfid
  Character(32)                            :: bcmode
  Real(kindQ)  , dimension(3)              :: bcvals
  Real(kindQ)                              :: mul
  Real(kindQ)  , dimension(5)              :: qc
  Real(kindQ)  , dimension(5)              :: qbnd
  Real(kindXYZ), dimension(4)              :: fnrm
  Real(kindQ)  , dimension(3)              :: fvel
  Real(kindXYZ), dimension(3)              :: fxyz
  Real(kindXYZ), dimension(3)              :: pxyz
  Real(kindXYZ), dimension(3)              :: nxyz
  Real(kindQ)  , dimension(5)              :: qf

  Real(kindXYZ), dimension(4)              :: frcx

  qc = q(1,1:5)

  call BC_FaceSimple( bccode, bldrgn, srfid, bcmode, bcvals, &
       mul, qc, qbnd, fnrm, fvel, fxyz, pxyz, nxyz, qf )

  call oksubr(fnrm(1))
  frcx(1) = SIN(fnrm(1))

  call aeroaxis_force(frcx(1), flift(1))

END SUBROUTINE US_Loads2
