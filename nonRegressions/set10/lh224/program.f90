! BUG J.Cardesa nov. 2109
! Sizes of PUSH/POP array T before call bar were too large
!  because not taking care of offset off.
! Results in a wrong result in adjoint mode
!  because PUSH/POPping after end of array T.
      MODULE mymod
        TYPE mystruct
           INTEGER :: dum
           REAL, DIMENSION(*) :: T
        END TYPE mystruct
      END MODULE mymod

      SUBROUTINE foo(obj, off)
        USE mymod
        TYPE(mystruct) obj
        INTEGER off,i
        call bar(obj%T(off:))
        DO i=1,100
           obj%T(i) = obj%T(i)*obj%T(i)
           obj%T(i) = sin(obj%T(i))
        ENDDO
      END
