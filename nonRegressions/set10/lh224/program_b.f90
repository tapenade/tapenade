!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (develop) - 14 Nov 2019 15:07
!
! BUG J.Cardesa nov. 2109
! Sizes of PUSH/POP array T before call bar were too large
!  because not taking care of offset off.
! Results in a wrong result in adjoint mode
!  because PUSH/POPping after end of array T.
MODULE MYMOD_DIFF
  IMPLICIT NONE
  TYPE MYSTRUCT
      INTEGER :: dum
      REAL, DIMENSION(*) :: t
  END TYPE MYSTRUCT
  TYPE MYSTRUCT_DIFF
      REAL, DIMENSION(*) :: t
  END TYPE MYSTRUCT_DIFF
END MODULE MYMOD_DIFF

!  Differentiation of foo in reverse (adjoint) mode:
!   gradient     of useful results: obj.t
!   with respect to varying inputs: obj.t
!   RW status of diff variables: obj.t:in-out
SUBROUTINE FOO_B(obj, objb, off)
  USE MYMOD_DIFF
  USE DIFFSIZES
!  Hint: ISIZE1OFobj_t should be the size of dimension 1 of array obj%t
  IMPLICIT NONE
  TYPE(MYSTRUCT) :: obj
  TYPE(MYSTRUCT_DIFF) :: objb
  INTEGER :: off, i
  EXTERNAL BAR
  EXTERNAL BAR_B
  INTRINSIC SIN
  CALL PUSHREAL4ARRAY(obj%t(off:*), ISIZE1OFobj_t - off + 1)
  CALL BAR(obj%t(off:))
  DO i=1,100
    CALL PUSHREAL4(obj%t(i))
    obj%t(i) = obj%t(i)*obj%t(i)
  END DO
  DO i=100,1,-1
    objb%t(i) = COS(obj%t(i))*objb%t(i)
    CALL POPREAL4(obj%t(i))
    objb%t(i) = 2*obj%t(i)*objb%t(i)
  END DO
  CALL POPREAL4ARRAY(obj%t(off:*), ISIZE1OFobj_t - off + 1)
  CALL BAR_B(obj%t(off:), objb%t(off:))
END SUBROUTINE FOO_B

SUBROUTINE FOO_NODIFF(obj, off)
  USE MYMOD_DIFF
  IMPLICIT NONE
  TYPE(MYSTRUCT) :: obj
  INTEGER :: off, i
  EXTERNAL BAR
  INTRINSIC SIN
  CALL BAR(obj%t(off:))
  DO i=1,100
    obj%t(i) = obj%t(i)*obj%t(i)
    obj%t(i) = SIN(obj%t(i))
  END DO
END SUBROUTINE FOO_NODIFF

