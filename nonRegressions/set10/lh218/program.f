c Had a bug for: -b -nooptim splitdiff
      SUBROUTINE FOO(con, cte, ck)
      INTEGER i
      REAL*8 con, cte, ck(10)
      REAL*8 co
      REAL*8 one, b, a
      one = 1.0
      b = 2.1
      a = 3.2
      DO i = 1,10
         co = ck(i)
         con = co**(one/b)
         ck(i) = co*(a*con*(one-b)+cte*b)/(a*con+co*b)
      ENDDO
      END

      REAL*8 con, cte, ck(10), sum
      INTEGER i
      DO i = 1,10
         ck(i) = 5.0/i
      ENDDO
      con = 1.2
      cte = 5.4
      CALL FOO(con, cte, ck)
      sum = 0.0
      DO i = 1,10
         sum = sum + ck(i)
      ENDDO
      print *,'res:',sum
      END
