C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.14 (feature/bug7) -  1 Oct 2019 10:22
C
C  Differentiation of _main_ as a context to call adjoint code (with options context no!SplitDiffExprs):

      IMPLICIT NONE
C
      REAL*8 con, cte, ck(10), sum
      REAL*8 conb, cteb, ckb(10)
      INTEGER i
      DO i=1,10
        ck(i) = 5.0/i
      ENDDO
      con = 1.2
      cte = 5.4
      CALL ADCONTEXTADJ_INIT(0.87_8)
      CALL ADCONTEXTADJ_INITREAL8('cte'//CHAR(0), cte, cteb)
      CALL ADCONTEXTADJ_INITREAL8ARRAY('ck'//CHAR(0), ck, ckb, 10)
      CALL ADCONTEXTADJ_INITREAL8('con'//CHAR(0), con, conb)
      CALL FOO_B(con, conb, cte, cteb, ck, ckb)
      CALL ADCONTEXTADJ_STARTCONCLUDE()
      CALL ADCONTEXTADJ_CONCLUDEREAL8('cte'//CHAR(0), cte, cteb)
      CALL ADCONTEXTADJ_CONCLUDEREAL8ARRAY('ck'//CHAR(0), ck, ckb, 10)
      CALL ADCONTEXTADJ_CONCLUDEREAL8('con'//CHAR(0), con, conb)
      CALL ADCONTEXTADJ_CONCLUDE()
      sum = 0.0
      DO i=1,10
        sum = sum + ck(i)
      ENDDO
      PRINT*, 'res:', sum
      END

C  Differentiation of foo in reverse (adjoint) mode (with options context no!SplitDiffExprs):
C   gradient     of useful results: cte ck con
C   with respect to varying inputs: cte ck con
C   RW status of diff variables: cte:incr ck:in-out con:in-out
C Had a bug for: -b -nooptim splitdiff
      SUBROUTINE FOO_B(con, conb, cte, cteb, ck, ckb)
      IMPLICIT NONE
      INTEGER i
      REAL*8 con, cte, ck(10)
      REAL*8 conb, cteb, ckb(10)
      REAL*8 co
      REAL*8 cob
      REAL*8 one, b, a
      one = 1.0
      b = 2.1
      a = 3.2
      DO i=1,10
        CALL PUSHREAL8(co)
        co = ck(i)
      ENDDO
      DO i=10,1,-1
        con = co**(one/b)
        conb = conb + (a*(one-b)*co/(a*con+b*co)-a*co*((one-b)*(a*con)+b
     +    *cte)/(a*con+b*co)**2)*ckb(i)
        cteb = cteb + b*co*ckb(i)/(a*con+b*co)
        IF (co .LE. 0.0 .AND. (one/b .EQ. 0.0 .OR. one/b .NE. INT(one/b)
     +      )) THEN
          cob = (((one-b)*(a*con)+b*cte)/(a*con+b*co)-b*co*((one-b)*(a*
     +      con)+b*cte)/(a*con+b*co)**2)*ckb(i)
        ELSE
          cob = (((one-b)*(a*con)+b*cte)/(a*con+b*co)-b*co*((one-b)*(a*
     +      con)+b*cte)/(a*con+b*co)**2)*ckb(i) + one*co**(one/b-1)*conb
     +      /b
        END IF
        ckb(i) = cob
        CALL POPREAL8(co)
        conb = 0.D0
      ENDDO
      END

