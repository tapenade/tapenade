// from 77/lh29 f2c program.f


/* Subroutine */ int s1_(t, n, xx, z__)
float *t;
int *n;
float *xx, *z__;
{
    /* System generated locals */
    int i__1;

    /* Local variables */
    static int i__;
    static float v[100];
    extern /* Subroutine */ int s2_(), s3_();
    static float x1, x2;

    /* Parameter adjustments */
    --t;

    /* Function Body */
    s3_(&t[1], n, &x1, z__);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	v[i__ - 1] = (float)1. / (i__ + (float).5);
    }
    s2_(v, 100.0, &x2, z__);
    *xx = x1 + x2;
} /* s1_ */
