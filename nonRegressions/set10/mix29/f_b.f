C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.13 (r6878) - 22 May 2018 16:29
C
C  Differentiation of s3 in reverse (adjoint) mode:
C   gradient     of useful results: res t z
C   with respect to varying inputs: t z
C
      SUBROUTINE S3_B(t, tb, n, res, resb, z, zb)
      IMPLICIT NONE
      INTEGER n
      REAL t(n), res, z
      REAL tb(n), resb, zb
      INTEGER i
      zb = -(3.0*zb/z**2)
      DO i=n,1,-1
        tb(i) = tb(i) + resb/i
      ENDDO
      END

C  Differentiation of s2 in reverse (adjoint) mode:
C   gradient     of useful results: z
C   with respect to varying inputs: z
      SUBROUTINE S2_B(v, vb, nn, res, z, zb)
      IMPLICIT NONE
      INTEGER nn
      REAL v(nn), res, z
      REAL vb(nn), resb, zb
      INTEGER ii1
      DO ii1=1,nn
        vb(ii1) = 0.0
      ENDDO
      resb = 0.0
      CALL S3_B(v, vb, nn, res, resb, z, zb)
      END

