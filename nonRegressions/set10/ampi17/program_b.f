C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.16 (feature_cuda) - 15 Nov 2022 17:03
C
C  Differentiation of main as a context to call adjoint code (with options context):
C
      PROGRAM MAIN_B
      IMPLICIT NONE
      INCLUDE 'ampi/ampif.h'
      INTEGER world_rank, err_code
      DOUBLE PRECISION x, y
      DOUBLE PRECISION xb, yb
C
      CALL AMPI_INIT_NT(err_code)
      CALL AMPI_COMM_RANK(mpi_comm_world, world_rank, err_code)
      x = 3.5
      y = 2.5
      CALL ADCONTEXTADJ_INIT(0.87_8)
      CALL ADCONTEXTADJ_INITREAL8('x'//CHAR(0), x, xb)
      CALL ADCONTEXTADJ_INITREAL8('y'//CHAR(0), y, yb)
      CALL HEAD_B(x, xb, y, yb)
      CALL ADCONTEXTADJ_STARTCONCLUDE()
      CALL ADCONTEXTADJ_CONCLUDEREAL8('x'//CHAR(0), x, xb)
      CALL ADCONTEXTADJ_CONCLUDEREAL8('y'//CHAR(0), y, yb)
      CALL ADCONTEXTADJ_CONCLUDE()
      y = y + x
      PRINT*, 'process', world_rank, ' gets y=', y
      CALL AMPI_FINALIZE_NT(err_code)
      END

C  Differentiation of head in reverse (adjoint) mode (with options context):
C   gradient     of useful results: x y
C   with respect to varying inputs: x y
C   RW status of diff variables: x:in-out y:in-out
C Test of cases where communications on MPI_INTEGERs
C should or should not be "differentiated".
C Rationale: Since a wait doesn't know statically what
C  it waits for, it may wait for an active value, so it
C  must always become e.g. TLS_AMPI_Wait.
C  Therefore all Isend and Irecv, even on MPI_INTEGERs must
C  always become TLS_AMPI_Ixxxx(), but their buffers, if
C  they are MPI_INTEGERs, must not be differentiated!
C  On the other hand, plain Send or Recv on MPI_INTEGERs
C  need not be differentiated.
C
      SUBROUTINE HEAD_B(x, xb, y, yb)
      IMPLICIT NONE
      INCLUDE 'ampi/ampif.h'
      DOUBLE PRECISION x, y
      DOUBLE PRECISION xb, yb
      INTEGER world_rank, err_code, ival
      INTEGER ivalb
      INTEGER reqr, reqi, req1, req2
      DOUBLE PRECISION local
      INTEGER*4 branch
C
      CALL AMPI_COMM_RANK(mpi_comm_world, world_rank, err_code)
      ival = 8 + world_rank
C In the following the Isend/Irecv on integers must be differentiated!
      IF (world_rank .EQ. 0) THEN
        x = x*2
        CALL FW_AMPI_ISEND(x, 1, mpi_double_precision, 1, 42, 
     +                     ampi_to_irecv_wait, mpi_comm_world, reqr, 
     +                     err_code)
        CALL FW_AMPI_ISEND(ival, 1, mpi_integer, 1, 75, 
     +                     ampi_to_irecv_wait, mpi_comm_world, reqi, 
     +                     err_code)
        IF (ival .LE. 10) THEN
          req1 = reqr
          req2 = reqi
        ELSE
          req1 = reqi
          req2 = reqr
        END IF
C Now we don't know which of req1 and req2 is on MPI_INTEGERs:
        CALL FW_AMPI_WAIT(req1, mpi_status_ignore, err_code)
        CALL FW_AMPI_WAIT(req2, mpi_status_ignore, err_code)
        CALL PUSHCONTROL2B(0)
      ELSE IF (world_rank .EQ. 1) THEN
        CALL FW_AMPI_IRECV(y, 1, mpi_double_precision, 0, 42, 
     +                     ampi_from_isend_wait, mpi_comm_world, reqr, 
     +                     err_code)
        CALL FW_AMPI_IRECV(ival, 1, mpi_integer, 0, 75, 
     +                     ampi_from_isend_wait, mpi_comm_world, reqi, 
     +                     err_code)
        IF (ival .LE. 10) THEN
          req1 = reqr
          req2 = reqi
        ELSE
          req1 = reqi
          req2 = reqr
        END IF
        CALL FW_AMPI_WAIT(req1, mpi_status_ignore, err_code)
        CALL FW_AMPI_WAIT(req2, mpi_status_ignore, err_code)
        CALL PUSHCONTROL2B(1)
      ELSE
        CALL PUSHCONTROL2B(2)
      END IF
C
C But plain send/recv on integers need not be differentiated:
      ival = ival + 5
      IF (world_rank .EQ. 0) THEN
        CALL PUSHCONTROL1B(0)
        CALL AMPI_SEND(ival, 1, mpi_integer, 1, 38, ampi_to_recv, 
     +                 mpi_comm_world, err_code)
      ELSE IF (world_rank .EQ. 1) THEN
        CALL PUSHCONTROL1B(1)
        CALL AMPI_RECV(ival, 1, mpi_integer, 0, 38, ampi_from_send, 
     +                 mpi_comm_world, mpi_status_ignore, err_code)
      ELSE
        CALL PUSHCONTROL1B(1)
      END IF
      CALL ADTOOL_AMPI_TURN(y, yb)
      yb = ival*yb
      CALL POPCONTROL1B(branch)
      CALL POPCONTROL2B(branch)
      IF (branch .EQ. 0) THEN
        yb = 3*yb
        CALL BW_AMPI_WAIT(req2, mpi_status_ignore, err_code)
        CALL BW_AMPI_WAIT(req1, mpi_status_ignore, err_code)
        CALL BW_AMPI_ISEND(ivalb, 1, mpi_integer, 1, 75, 
     +                     ampi_to_irecv_wait, mpi_comm_world, reqi, 
     +                     err_code)
        CALL BW_AMPI_ISEND(xb, 1, mpi_double_precision, 1, 42, 
     +                     ampi_to_irecv_wait, mpi_comm_world, reqr, 
     +                     err_code)
        xb = 2*xb
      ELSE IF (branch .EQ. 1) THEN
        CALL BW_AMPI_WAIT(req2, mpi_status_ignore, err_code)
        CALL BW_AMPI_WAIT(req1, mpi_status_ignore, err_code)
        CALL BW_AMPI_IRECV(ivalb, 1, mpi_integer, 0, 75, 
     +                     ampi_from_isend_wait, mpi_comm_world, reqi, 
     +                     err_code)
        CALL BW_AMPI_IRECV(yb, 1, mpi_double_precision, 0, 42, 
     +                     ampi_from_isend_wait, mpi_comm_world, reqr, 
     +                     err_code)
        yb = 0.D0
      END IF
      END

