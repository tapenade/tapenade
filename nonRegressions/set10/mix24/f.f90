subroutine bar(a, b) bind(c, name='fortranBar')
implicit none
real, value :: a
real :: b

a = 2 * a
b = a * a

end subroutine
