void fortranBar(float a, float *b);

void foo(float *x, float *y) {
  fortranBar(*x, y);
}

void main() {
  float x, y;
  x = 2.0;
  printf("x %f\n", x);
  foo(&x, &y);
  printf("x %f y %f\n", x, y);
}
