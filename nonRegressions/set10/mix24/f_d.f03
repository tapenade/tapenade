!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nodiffs) - 31 Mar 2021 14:31
!
!  Differentiation of bar in forward (tangent) mode:
!   variations   of useful results: b
!   with respect to varying inputs: a
SUBROUTINE BAR_D(a, ad, b, bd) BIND(c, name='fortranBar_d')
  IMPLICIT NONE
  REAL, VALUE :: a
  REAL, VALUE :: ad
  REAL :: b
  REAL :: bd
  ad = 2*ad
  a = 2*a
  bd = 2*a*ad
  b = a*a
END SUBROUTINE BAR_D

