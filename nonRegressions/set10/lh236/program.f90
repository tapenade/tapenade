! Module that gives only interfaces
MODULE FOO_INTERFACE
  INTERFACE FOO
      SUBROUTINE FOO(arg1, arg2 )
        Real(kindQ), dimension(1), INTENT(IN)    :: arg1
        Real(kindXYZ), dimension(1), INTENT(OUT) :: arg2
      END SUBROUTINE FOO
  END INTERFACE
END MODULE FOO_INTERFACE

! Full definition of FOO
SUBROUTINE FOO(fvel, nxyz )
  COMMON /dims/ len
  INTEGER :: len
  Real(kindQ), dimension(len), INTENT(IN)    :: fvel
  Real(kindXYZ), dimension(len), INTENT(OUT) :: nxyz
  nxyz(:) = fvel(:)*fvel(:)
END SUBROUTINE FOO


! This top diff unit should trigger standard diff
!  of BC_FaceSimple and aeroaxis_force
SUBROUTINE TOP(q)
  USE FOO_INTERFACE
  REAL :: q
  INTEGER :: len
  COMMON /dims/ len
  Real(kindQ)  , dimension(len) :: fvel
  Real(kindXYZ), dimension(len) :: nxyz

  fvel = q
  call FOO(fvel, nxyz)
  q = SUM(nxyz)
END SUBROUTINE TOP
