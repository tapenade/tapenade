/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 28 Aug 2020 10:13
*/
#include "AATypes_aab.c"
#include <adStack.h>
#include <adContext.h>
#include <stdio.h>
#include <math.h>

/*
  Differentiation of sub1 in reverse (adjoint) mode (with options associationByAddress context):
   gradient     of useful results: *x *y
   with respect to varying inputs: *x *y
   RW status of diff variables: x:(loc) *x:incr y:(loc) *y:in-out
   Plus diff mem management of: x:in y:in
*/
void sub1_aab(float8_diff *x, float8_diff *y) {
    extern double f_(), g_();
    extern void f_aab_(float8_diff *, double *), g_aab_(float8_diff *, double *);
    double result1;
    double result2;
    result1 = y->d;
    result2 = y->d;
    y->d = 0.0;
    g_aab_(y, &result2);
    f_aab_(x, &result1);
}

/*
  Differentiation of main as a context to call adjoint code (with options associationByAddress context):
*/
void main() {
    float8_diff i;
    i.v = 2;
    float8_diff j;
    j.v = 3;
    double result;
    printf("i %f ...\n", i.v);
    printf("j %f ...\n", j.v);
    adContextAdj_init(0.87);
    adContextAdj_initReal8("i", &(i.v), &(i.d));
    adContextAdj_initReal8("j", &(j.v), &(j.d));
    sub1_aab(&i, &j);
    adContextAdj_startConclude();
    adContextAdj_concludeReal8("i", i.v, i.d);
    adContextAdj_concludeReal8("j", j.v, j.d);
    adContextAdj_conclude();
    printf("i--> %f\n", i.v);
    printf("j--> %f\n", j.v);
}
