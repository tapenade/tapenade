/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 28 Aug 2020 10:13
*/
#include <adContext.h>
#include <stdio.h>
#include <math.h>

/*
  Differentiation of sub1 in forward (tangent) mode (with options context):
   variations   of useful results: *y
   with respect to varying inputs: *x *y
   RW status of diff variables: x:(loc) *x:in y:(loc) *y:in-out
   Plus diff mem management of: x:in y:in
*/
void sub1_d(double *x, double *xd, double *y, double *yd) {
    extern double f_(), g_();
    extern double f_d_(double *, double *, double *), g_d_(double *, double *,
        double *);
    double result1;
    double result1d;
    double result2;
    double result2d;
    result1d = f_d_(x, xd, &result1);
    result2d = g_d_(y, yd, &result2);
    *yd = result1d + result2d;
    *y = result1 + result2;
}

/*
  Differentiation of main as a context to call tangent code (with options context):
*/
void main() {
    double i = 2;
    double id = 0.0;
    double j = 3;
    double jd = 0.0;
    double result;
    printf("i %f ...\n", i);
    printf("j %f ...\n", j);
    adContextTgt_init(1.e-8, 0.87);
    adContextTgt_initReal8("i", &i, &id);
    adContextTgt_initReal8("j", &j, &jd);
    sub1_d(&i, &id, &j, &jd);
    adContextTgt_startConclude();
    adContextTgt_concludeReal8("i", i, id);
    adContextTgt_concludeReal8("j", j, jd);
    adContextTgt_conclude();
    printf("i--> %f\n", i);
    printf("j--> %f\n", j);
}
