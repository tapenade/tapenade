#include <stdio.h>
#include <math.h>

void sub1 (double *x, double *y)
{
    extern double f_(), g_();

    *y = f_(x) + g_(y);
}

main()
{
  double i=2;
  double j=3;
  double result;
  printf("i %f ...\n", i);
  printf("j %f ...\n", j);
  sub1(&i, &j);
  printf("i--> %f\n", i);
  printf("j--> %f\n", j);
}
