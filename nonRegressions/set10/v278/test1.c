#include "program.h"
#include <stdio.h>

void bar1(float a, float *b) {
  *b = a * a;
}

void foo1(float *x, float *y) {
  bar1(*x, y);

}

void testNbCall1() {
  nbCall = nbCall + 10;
  printf("testNbCall1 test1 %d \n", nbCall);
}
