module example
      use iso_c_binding
      implicit none
      real(c_float), bind(c):: statix1
      real :: statix2
      bind(c) :: statix2
      real(c_float), bind(c, name = 'STATIX3'):: statix3
      real :: statix4
      bind(c, name = 'STATIX4') :: statix4
end module

 subroutine barf(z) bind(c)
      use example
      real(c_float) :: z
      z = (z * z) + statix1
      statix1 = z
      statix2 = z + 2
      statix3 = 3
 end subroutine
