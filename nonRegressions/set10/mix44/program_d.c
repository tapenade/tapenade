/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_nopubliczones) - 15 Dec 2022 12:13
*/
#include <adContext.h>
#include <stdio.h>
extern float statix1;
extern float statix1d;
extern float statix2;
extern float statix2d;
extern float STATIX3;
extern float STATIX3d;
extern float STATIX4;
extern void barf_nodiff(float *);
extern void barf_d(float *, float *);

/*
  Differentiation of foo in forward (tangent) mode (with options context):
   variations   of useful results: statix1 statix2 STATIX3 *x
   with respect to varying inputs: *x
   RW status of diff variables: statix1:out statix2:out STATIX3:zero
                x:(loc) *x:in-out
   Plus diff mem management of: x:in
*/
void foo_d(float *x, float *xd) {
    printf("1 statix1 %f\n", statix1);
    statix1d = *xd;
    statix1 = *x;
    STATIX3 = (*x)*(*x);
    printf("2 statix1 %f\n", statix1);
    barf_d(x, xd);
    printf("3 statix1 %f\n", statix1);
    STATIX3d = 0.0;
}

/*
  Differentiation of main as a context to call tangent code (with options context):
*/
void main() {
    float x, y, z;
    float xd;
    statix1 = 999;
    xd = 0.0;
    x = 2.0;
    adContextTgt_init(1.e-4, 0.87);
    adContextTgt_initReal4("statix1", &statix1, &statix1d);
    adContextTgt_initReal4("statix2", &statix2, &statix2d);
    adContextTgt_initReal4("STATIX3", &STATIX3, &STATIX3d);
    adContextTgt_initReal4("x", &x, &xd);
    foo_d(&x, &xd);
    adContextTgt_startConclude();
    adContextTgt_concludeReal4("statix1", statix1, statix1d);
    adContextTgt_concludeReal4("statix2", statix2, statix2d);
    adContextTgt_concludeReal4("STATIX3", STATIX3, STATIX3d);
    adContextTgt_concludeReal4("x", x, xd);
    adContextTgt_conclude();
    printf("x %f statix1 %f statix2 %f \n", x, statix1, statix2);
}
