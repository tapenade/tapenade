! Bug git#9, on nodet-winnie
subroutine diffusiv(ddx,n,rho_ig,sdx,btsia,hmx,phisia)
  REAL*8, DIMENSION(10) :: ddx, sdx,hmx
  REAL*8 :: n,rho_ig,btsia,phisia

  ! formulation loi n=3 
  ddx(:)=((1./(n+2.))*((Rho_ig)**n)*(abs(Sdx(:))**(n-1.))*BtSIA*(Hmx(:)**(n+1.))) 
  ! ajout de la loi en n=1
  ddx(:)= ddx(:)+((1./3.)*(Rho_ig*Hmx(:)**2*PhiSIA))

end subroutine diffusiv

