!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) - 30 Aug 2021 17:54
!
!  Differentiation of diffusiv in forward (tangent) mode:
!   variations   of useful results: ddx
!   with respect to varying inputs: n phisia sdx hmx rho_ig btsia
!   RW status of diff variables: n:in ddx:out phisia:in sdx:in
!                hmx:in rho_ig:in btsia:in
! Bug git#9, on nodet-winnie
SUBROUTINE DIFFUSIV_D(ddx, ddxd, n, nd, rho_ig, rho_igd, sdx, sdxd, &
& btsia, btsiad, hmx, hmxd, phisia, phisiad)
  IMPLICIT NONE
  REAL*8, DIMENSION(10) :: ddx, sdx, hmx
  REAL*8, DIMENSION(10) :: ddxd, sdxd, hmxd
  REAL*8 :: n, rho_ig, btsia, phisia
  REAL*8 :: nd, rho_igd, btsiad, phisiad
  INTRINSIC ABS
  REAL*8, DIMENSION(10) :: abs0
  REAL*8, DIMENSION(10) :: abs0d
  REAL*8, DIMENSION(10) :: temp
  REAL*8, DIMENSION(10) :: temp0
  REAL*8 :: temp1
  REAL*8 :: temp2
  REAL*8 :: tempd
  REAL*8, DIMENSION(10) :: tempd0
  REAL*8, DIMENSION(10) :: tempd1
  abs0d = 0.0_8
  WHERE (sdx(:) .GE. 0.0) 
    abs0d = sdxd(:)
    abs0 = sdx(:)
  ELSEWHERE
    abs0d = -sdxd(:)
    abs0 = -sdx(:)
  END WHERE
! formulation loi n=3 
  temp = hmx(:)**(n+1.)
  temp0 = abs0**(n-1.)
  temp1 = btsia/(n+2.)
  temp2 = rho_ig**n
  IF (rho_ig .LE. 0.0 .AND. (n .EQ. 0.0 .OR. n .NE. INT(n))) THEN
    tempd = 0.0_8
  ELSE IF (rho_ig .LE. 0.0) THEN
    tempd = n*rho_ig**(n-1)*rho_igd
  ELSE
    tempd = n*rho_ig**(n-1)*rho_igd + temp2*LOG(rho_ig)*nd
  END IF
  WHERE (abs0 .LE. 0.0 .AND. (n - 1. .EQ. 0.0 .OR. n - 1. .NE. INT(n - &
&     1.))) 
    tempd0 = 0.0_8
  ELSEWHERE (abs0 .LE. 0.0) 
    tempd0 = (n-1.)*abs0**(n-2.0)*abs0d
  ELSEWHERE
    tempd0 = (n-1.)*abs0**(n-2.0)*abs0d + temp0*LOG(abs0)*nd
  END WHERE
  WHERE (hmx(:) .LE. 0.0 .AND. (n + 1. .EQ. 0.0 .OR. n + 1. .NE. INT(n +&
&     1.))) 
    tempd1 = 0.0_8
  ELSEWHERE (hmx(:) .LE. 0.0) 
    tempd1 = (n+1.)*hmx(:)**n*hmxd(:)
  ELSEWHERE
    tempd1 = (n+1.)*hmx(:)**n*hmxd(:) + temp*LOG(hmx(:))*nd
  END WHERE
  ddxd(:) = temp0*temp*(temp1*tempd+temp2*(btsiad-temp1*nd)/(n+2.)) + &
&   temp2*temp1*(temp*tempd0+temp0*tempd1)
  ddx(:) = temp2*temp1*(temp0*temp)
! ajout de la loi en n=1
  temp2 = rho_ig/3.
  ddxd(:) = ddxd(:) + hmx(:)**2*(phisia*rho_igd/3.+temp2*phisiad) + &
&   temp2*phisia*2*hmx(:)*hmxd(:)
  ddx(:) = ddx(:) + temp2*phisia*(hmx(:)*hmx(:))
END SUBROUTINE DIFFUSIV_D

