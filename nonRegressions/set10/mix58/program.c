#include <stdio.h>

extern void bar(float*, float*);

void foo(float* x, float* y, float* z) {
  bar(x, y);
  bar(z, y);
}
