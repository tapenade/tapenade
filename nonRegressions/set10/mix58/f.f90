module m
use iso_c_binding
contains
subroutine top(x,y,z)
implicit none
real :: x,y,z
interface
    subroutine foo(x, y, z) bind(C)
       use iso_c_binding
       real(c_float) :: x
       real(c_float) :: y
       real(c_float) :: z
    end subroutine foo
end interface

call foo(x,y,z)
end subroutine top

subroutine foo0(x,y,z)
implicit none
real, intent(in) :: x
real :: y
real, intent(in) :: z
call bar(x,y)
call bar(z,y)
end

subroutine bar(a,b) bind(C)
implicit none
real :: a,b
print*, 'bar::  a b ', a, b
b = a*b
print*, 'bar->  a b ', a, b
end 
end module m

program test
use m
implicit none
real :: x,y,z
x = 2
y = 1
z = 3
call top(x,y,z)
print*, 'x z', x, z
print*, 'y  ', y
end program test
