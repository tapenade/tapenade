void d_(double* w, double* x, double* y, double* z, int i) {
  // x is inout, only dependent on itself.
  // z is inout and depends on w
    if(i == 0) {
      *x = *x*2;
      *z = sin(*z)* *w;
    }  else {
      d_(w,x,y,z,0);
    }
}

