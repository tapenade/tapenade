! Bug found by Boeing people: infinite recursion
!  in ADTBRAnalysis -> cumulWithOperRec()

      module data_types

      Implicit None

      Public

      type ArrayDimensions
        sequence

        Integer(kindAry) :: idim
        Integer(kindAry) :: jdim
        Integer(kindAry) :: kdim
        Integer(kindAry) :: iseq
        Integer(kindAry) :: jseq
        Integer(kindAry) :: kseq
        Integer(kindAry) :: mxclplin
        Integer(kindAry) :: mxfcneig
        Integer(kindAry) :: mxlsstn
        Integer(kindAry) :: ncells
        Integer(kindAry) :: ndimu
        Integer(kindAry) :: nfaces
        Integer(kindAry) :: nfacu
        Integer(kindAry) :: nbfaces
        Integer(kindAry) :: nifaces
        Integer(kindAry) :: nlines
        Integer(kindAry) :: nsurf
        Integer(kindAry) :: nsurfi
        Integer(kindAry) :: nvrtcs
        Integer(kindAry) :: nvperc
        Integer(kindAry) :: nvperf
        Integer(kindAry) :: nghost
        Integer(kindAry) :: nghostR
        Integer(kindAry) :: ngrad
        Integer(kindAry) :: nGradStncls
        Integer(kindAry) :: nStnclCells
      end type ArrayDimensions

      type ArbFlSpec
        sequence

        Real(kindQ)                        :: M
        Real(kindQ)                        :: p
        Real(kindQ)                        :: T
        Real(kindQ)                        :: aoa
        Real(kindQ)                        :: aos
        Real(kindQ)                        :: pt
        Real(kindQ)                        :: Tt
        Real(kindQ)                        :: R
        Real(kindQ)                        :: gamma

        Real(kindQ)                        :: enthalpy
        Real(kindQ)                        :: entropy 

        Real(kindQ), dimension(2)          :: turb

        Real(kindQ), dimension(:), pointer :: species

        Integer(kindInt)                   :: istto

        Logical                            :: frozen
        Logical                            :: current

      end type ArbFlSpec

      type ArbInit
        sequence

        Character(32)             :: mode
        Character(32)             :: name

        Integer(kindInt)          :: angflow
        Integer(kindInt)          :: ibcmod 
        Integer(kindInt)          :: ibczon
        Integer(kindAry)          :: icond 

        Real(kindQ), dimension(9) :: angflow_axis
        Real(kindQ), dimension(6) :: region

        Type(ArbFlSpec)           :: flspec
      end type ArbInit
      
      type ArbRegion
        sequence

        Character(32)             :: type

        Integer(kindInt)          :: ibczon
        Real(kindQ), dimension(6) :: region


        Type(ArbFlSpec)           :: flspec

        Type(ArbRegion), pointer  :: next

      end type ArbRegion
      
      type ArbBc
        sequence

        Character(32)                          :: mode
        Character(32)                          :: name
        Character(32)                          :: grpname
        Integer(kindAry)                       :: icond

        Integer(kindInt)                       :: angflow
        Real(kindQ), dimension(9)              :: angflow_axis

        Integer(kindInt)                       :: ihold

        Type(ArbFlSpec)                        :: flspec

        Integer(kindInt)                       :: ibcstrt
        Integer(kindInt)                       :: ibcfreq
        Real(kindQ)                            :: bcrlx
        Real(kindQ)                            :: Mdot
        Real(kindQ)                            :: ptmax
        Real(kindQ)                            :: ptmin

        Real(kindQ), dimension(3)              :: arbxc
        Real(kindXYZ), dimension(3)            :: arbstr

        Integer(kindInt)                       :: mxfreq
        Integer(kindInt)                       :: nfreq
        Real(kindQ), dimension(:,:), pointer   :: unsdat
        Character(32), dimension(:), pointer   :: unsvar
        Real(kindQ)                            :: amdot
        Real(kindQ)                            :: amdotLast
        Real(kindQ)                            :: gamavg
        Real(kindQ)                            :: avmach
        Real(kindQ)                            :: rhoavg
        Real(kindQ)                            :: ttavg
        Real(kindQ)                            :: atot

        Character(32)                          :: rakeName1
        Character(32)                          :: rakeName2
        Real(kindQ)                            :: rakeVal1
        Real(kindQ)                            :: rakeVal2

        Logical                                :: phys_kp
        Logical                                :: ready
        Integer(kindInt)                       :: nsiz
        Real(kindDouble)                       :: seed
        Real(kindDouble)                       :: xsize
        Real(kindDouble)                       :: blhgt
        Real(kindDouble)                       :: kpeak
        Real(kindDouble)                       :: start_time
        Real(kindDouble)                       :: vpmag

        Integer(kindInt)                       :: nbcreg
        Type(ArbRegion), pointer               :: regions

        Integer(kindAry)                       :: inflowSynthetic
        character(96)                          :: inFilename
        character(8)                           :: inflowMode

      end type ArbBc
      
      type BCGrid
        sequence

        Integer(kindAry)                                   :: bcnedges
        Integer(kindAry)                                   :: bcnpts

        Integer(kindAry), dimension(:),     pointer        :: emap
        Integer(kindAry), dimension(:,:),   pointer        :: edgelist
        Integer(kindAry), dimension(:),     pointer        :: pmap
        Integer(kindAry), dimension(:),     pointer        :: symmEdge

        Real(kindXYZ),    dimension(:),     pointer        :: apsum
        Real(kindXYZ),    dimension(:,:),   pointer        :: adpsum
        Real(kindXYZ),    dimension(:,:),   pointer        :: atpsum
        Real(kindXYZ),    dimension(:),     pointer        :: alpha
        Real(kindXYZ),    dimension(:,:),   pointer        :: delta
        Real(kindXYZ),    dimension(:,:,:), pointer        :: nrml
        Real(kindXYZ),    dimension(:,:),   pointer        :: sens
      end type BCGrid

      type GradData
        sequence
        Real(kindQ), dimension(:,:,:),     pointer        :: data1
        Real(kindQ), dimension(:,:,:),     pointer        :: data2
      end type GradData

      type RindData
        sequence

        Integer(kindAry)                                  :: ngcells
        Integer(kindAry), dimension(:),       pointer     :: gcells
        Integer(kindAry), dimension(:),       pointer     :: gptmap
        Integer(kindAry)                                  :: ncstncl
        Integer(kindAry), dimension(:),       pointer     :: cstncl
        Integer(kindAry), dimension(:),       pointer     :: cstnclp
        Integer(kindAry)                                  :: nfstncl
        Integer(kindAry), dimension(:),       pointer     :: fstncl
        Integer(kindAry), dimension(:),       pointer     :: fstnclp
        Integer(kindAry)                                  :: nvstncl
        Integer(kindAry), dimension(:),       pointer     :: vstncl
        Integer(kindAry), dimension(:),       pointer     :: vstnclp

      end type RindData

      type BcData
        sequence

        Character(32)                                      :: srfnam
        Character(32)                                      :: bqlcoord

        Integer(kindAry)                                   :: bc1to1
        Integer(kindAry)                                   :: bccode
        Integer(kindAry)                                   :: bcnpts
        Integer(kindAry)                                   :: bcsize
        Integer(kindAry)                                   :: bcsizef
        Integer(kindAry)                                   :: mxcpnd
        Integer(kindAry)                                   :: srfid
        Integer(kindAry)                                   :: srfindx
        Integer(kindAry)                                   :: isrfc
        Integer(kindAry)                                   :: srfmod

        Integer(kindAry), dimension(:),     pointer        :: bldrgn
        Integer(kindAry), dimension(:),     pointer        :: dqp
        Integer(kindAry), dimension(:),     pointer        :: dqpi
        Integer(kindAry), dimension(:),     pointer        :: fg2cg
        Integer(kindAry), dimension(:),     pointer        :: nbd
        Integer(kindAry), dimension(:),     pointer        :: nbdf
        Integer(kindAry), dimension(:),     pointer        :: nbtype
        Integer(kindAry), dimension(:),     pointer        :: nend
        Integer(kindAry), dimension(:),     pointer        :: pntmapl
        Integer(kindAry), dimension(:),     pointer        :: sunShadowFact

        Integer(kindAry), dimension(:,:),   pointer        :: FaceP
        Integer(kindAry), dimension(:,:),   pointer        :: bcsrfmap
        Integer(kindAry), dimension(:,:),   pointer        :: i123
        Integer(kindAry), dimension(:,:),   pointer        :: ijk
        Integer(kindAry), dimension(:,:),   pointer        :: ijkf
        Integer(kindAry), dimension(:,:),   pointer        :: obsShadowFact

        Real(kindBC),     dimension(:),     pointer        :: blarea
        Real(kindBC),     dimension(:)  ,   pointer        :: cvol

        Real(kindBC),     dimension(:,:),   pointer        :: f123
        Real(kindBC),     dimension(:,:),   pointer        :: xyz
        Real(kindBC),     dimension(:,:),   pointer        :: xyzc
        Real(kindBC),     dimension(:,:),   pointer        :: locxyz
        Real(kindBC),     dimension(:,:),   pointer        :: locxyzc

        Real(kindQ),      dimension(:),     pointer        :: ptarget
        Real(kindQ),      dimension(:),     pointer        :: temp
        Real(kindQ),      dimension(:),     pointer        :: trans

        Real(kindQ),      dimension(:,:),   pointer        :: bqc
        Real(kindQ),      dimension(:,:),   pointer        :: bqg
        Real(kindQ),      dimension(:,:),   pointer        :: bql
        Real(kindQ),      dimension(:,:),   pointer        :: bqlsv
        Real(kindQ),      dimension(:,:),   pointer        :: bqn
        Real(kindQ),      dimension(:,:),   pointer        :: bqr
        Real(kindQ),      dimension(:,:),   pointer        :: dbql
        Real(kindQ),      dimension(:,:),   pointer        :: dqcz
        Real(kindQ),      dimension(:,:),   pointer        :: qadjust
        Real(kindQ),      dimension(:,:),   pointer        :: utvel

        Real(kindQ),      dimension(:,:,:), pointer        :: AEmodes
        Real(kindQ),      dimension(:,:,:), pointer        :: dbqlu

        Real(kindXYZ),    dimension(:,:),   pointer        :: normal

        Real(kindQ),      dimension(:),     pointer        :: surfRough

        Type(perrot),                       pointer        :: perrotp

        Type(RindData),                     pointer        :: rind

        Type(BCGrid)                                       :: gsolver

        Type(GradData)                                     :: graddat

        Type(GroupBoundaryCondition),       pointer        :: groupBC
      end type BCData

      type BCDatac
        sequence

        Integer(kindAry)                                    :: mxcpz
        Integer(kindAry)                                    :: zone

        Integer(kindAry), dimension(:,:), pointer           :: cpzone

        Type(bcdata),     dimension(:),   pointer           :: bcdatc

        Type(bcdatac),                    pointer           :: ptr
      end type BCDatac

      type BCSurf
        sequence
        Character(32)                                      :: srfnam
        Integer(kindAry)                                   :: srfid
        Integer(kindAry)                                   :: srfsiz
        Integer(kindAry)                                   :: srfmod
        Integer(kindAry)                                   :: bccode
        Integer(kindAry)                                   :: nssrf
        Type(BCsurf), dimension(:), pointer                :: ssurf
      end type BCSurf

      type BCZone
        sequence

        Integer(kindAry)                                   :: nsurf

        Type(BCsurf), dimension(:), pointer                :: surf
      end type BCZone

      type BCSJetData
        sequence

        Real(kindQ)                                        :: frequency
        Real(kindQ)                                        :: amplitude
        Real(kindQ)                                        :: phase
        Real(kindQ)                                        :: cavDepth
        Real(kindQ)                                        :: cavOrificeRat
        Real(kindQ)                                        :: lossCoeff
        Real(kindQ)                                        :: dampCoeff
        Real(kindQ)                                        :: diaDensThk
        Real(kindQ)                                        :: natFreq
        Real(kindQ)                                        :: helmFreq
        Real(kindQ)                                        :: xdot
        Real(kindQ)                                        :: u
        Real(kindQ)                                        :: p
        Real(kindQ)                                        :: x

        Real(kindDouble)                                   :: time
      end type BCSJetData

      type BCBloVlvData
        sequence

        Integer, dimension(:), pointer                     :: curstate
        Integer, dimension(:), pointer                     :: nextstate
      end type BCBloVlvData

      type BCMoveData
        sequence

        Character(8)                                       :: pmode
        Character(8)                                       :: axis

        Logical                                            :: nonorm

        Real(kindQ)                                        :: xc
        Real(kindQ)                                        :: yc
        Real(kindQ)                                        :: zc
        Real(kindQ)                                        :: xa
        Real(kindQ)                                        :: ya
        Real(kindQ)                                        :: za
        Real(kindQ)                                        :: tht
        Real(kindQ)                                        :: phi
        Real(kindQ)                                        :: psi
        Real(kindQ)                                        :: velx
        Real(kindQ)                                        :: vely
        Real(kindQ)                                        :: velz
      end type BCMoveData

      type BCBLeedData
        sequence

        Real(kindQ)                                        :: flrate
        Real(kindQ)                                        :: pratio
        Real(kindQ)                                        :: press
        Real(kindQ)                                        :: Temp
        Real(kindQ)                                        :: angle_inc
        Real(kindQ)                                        :: mdot
        Real(kindQ)                                        :: mdottarg
        Real(kindQ)                                        :: porosity
        Real(kindQ)                                        :: discharge
        Real(kindQ)                                        :: ExitArea
        Real(kindQ)                                        :: mach
        Real(kindQ)                                        :: pbavg
        Real(kindQ)                                        :: qsonic
        Integer(kindAry)                                   :: ifreq
        Integer(kindAry)                                   :: mode
        Integer(kindAry)                                   :: nbl
        Integer(kindAry)                                   :: qsmode

        Logical                                            :: noblow

        Real(kindQ)                                        :: angle_az

        Integer(kindAry)                                   :: axis

        Real(kindQ)                                        :: deltastar
        Real(kindQ)                                        :: hole_AR

        Real(kindQ), dimension(:), pointer                 :: species

        Real(kindQ)                                        :: frc_amp
        Real(kindQ)                                        :: frc_freq
        Real(kindQ)                                        :: frc_phase

        Integer(kindAry)                                   :: frc_mode

        Character(32)                                      :: frc_wave

        type(BCMoveData), pointer                          :: mvwall

        type(BCSJetData), pointer                          :: sjet

        Character(32)                                      :: model

        Logical                                            :: Static

        Real(kindQ), dimension(:), pointer                 :: ublotime
        Real(kindQ), dimension(:), pointer                 :: ublopres
        Real(kindQ), dimension(:), pointer                 :: ublotemp
      end type BCBleedData

      type BCBleed
        sequence

        Integer(kindAry)                                   :: nblrgns

        Character(32)    , dimension(:)  , pointer         :: blareanm

        Real(kindSingle) , dimension(:)  , pointer         :: acap
        Real(kindSingle) , dimension(:)  , pointer         :: blarea

        Type(BCBleedData), dimension(:,:), pointer         :: mdata

        Type(BCBloVlvData)               , pointer         :: blovlv          
      end type BCBleed

      type curveFit
        sequence

        Character(32)                        :: fitType

        Real(kindQ)  , dimension(:), pointer :: coeff
        Real(kindQ)                          :: lowerLim
        Real(kindQ)                          :: upperLim
      end type curveFit

      type rakeData
        sequence

        Character(32)                             :: name
        Character(32)                             :: outputFn

        Character(6)                              :: write
        Integer                                   :: writeFreq

        Integer(kindAry)                          :: npoints
        Integer(kindAry), dimension(:)  , pointer :: cell
        Integer(kindAry), dimension(:)  , pointer :: zone

        Real(kindQ)                               :: pgaugeInt
        Real(kindQ)     , dimension(:)  , pointer :: Ptotal
        Real(kindQ)     , dimension(:)  , pointer :: Ttotal
        Real(kindQ)     , dimension(:)  , pointer :: Mach
        Real(kindQ)     , dimension(:,:), pointer :: velocity

        Real(kindXYZ)   , dimension(:,:), pointer :: xyz

        Type(rakeData)                  , pointer :: next
      end type rakeData

      type MGData
        sequence

        Integer(kindAry)                            :: nCGcells
        Integer(kindAry)                            :: nCGfaces
        Integer(kindAry)                            :: mxFGinCGlcl

        Integer(kindAry),  pointer, dimension(:)    :: CGfaces
        Integer(kindAry),  pointer, dimension(:)    :: CGfacesP
        Integer(kindAry),  pointer, dimension(:)    :: CGofFG
        Integer(kindAry),  pointer, dimension(:)    :: FG2CG
        Integer(kindAry),  pointer, dimension(:)    :: FGinCG
        Integer(kindAry),  pointer, dimension(:)    :: FGinCGp
      end type MGData

      type PerRot
        sequence

        Character(8)              :: type

        Integer(kindAry)          :: ipbnd
        Integer(kindAry)          :: ipzon

        Real(kindQ)               :: xrot
        Real(kindQ)               :: yrot
        Real(kindQ)               :: zrot
        Real(kindQ)               :: xtrans
        Real(kindQ)               :: ytrans
        Real(kindQ)               :: ztrans

        Type(perrot)    , pointer :: perrotp
      end type PerRot

      type GroupRigidMotion
        sequence

        Real(kindXYZ), dimension(3)          :: xref
        Real(kindXYZ), dimension(3)          :: angCurrent
        Real(kindXYZ), dimension(3)          :: xdot
        Real(kindXYZ), dimension(3)          :: angNew
        Real(kindXYZ), dimension(3)          :: angRate

        Real(kindDouble)                     :: curTim

        Real(kindXYZ)              , pointer :: properties

        Type(PrescribedMotionData) , pointer :: PrescribeDat

        Integer(kindAry)                     :: motiontyp
        Integer(kindAry)                     :: itmp
      end type GroupRigidMotion

      type ModeParams
        sequence

        Integer(kindAry) :: motionType

        Real(kindQ)      :: amp
        Real(kindQ)      :: damp
        Real(kindQ)      :: freq
        Real(kindQ)      :: gmass
        Real(kindQ)      :: gf0
        Real(kindQ)      :: x0
        Real(kindQ)      :: v0
        Real(kindQ)      :: prescAmpl
        Real(kindQ)      :: prescFreq
        Real(kindQ)      :: prescPhase
      end type ModeParams
      
      type GroupDeformingMotion
        sequence

        Logical                                   :: reinitAE

        Integer(kindAry)                          :: nmodes

        Real(kindQ)                               :: flow2strctLEN
        Real(kindQ)                               :: flow2strctFRC

        Real(kindQ)     , dimension(:,:), pointer :: AEgenf
        Real(kindQ)     , dimension(:)  , pointer :: AEgforcn
        Real(kindQ)     , dimension(:)  , pointer :: AEgforcnm
        Real(kindQ)     , dimension(:)  , pointer :: AEgforcs
        Real(kindQ)     , dimension(:)  , pointer :: AExs
        Real(kindQ)     , dimension(:)  , pointer :: AExxn
        Real(kindQ)     , dimension(:)  , pointer :: AExs_old
        Real(kindQ)     , dimension(:,:), pointer :: AEstm
        Real(kindQ)     , dimension(:,:), pointer :: AEstmi

        Type(ModeParams), dimension(:)  , pointer :: mparams
      end type GroupDeformingMotion

      type LoadPlaneData
        sequence

        Real(kindQ)                        :: area
        Real(kindQ)                        :: flmomxg
        Real(kindQ)                        :: flmomyg
        Real(kindQ)                        :: flmomzg
        Real(kindQ)                        :: flmomxt
        Real(kindQ)                        :: flmomyt
        Real(kindQ)                        :: flmomzt
        Real(kindQ)                        :: flmomxgm
        Real(kindQ)                        :: flmomygm
        Real(kindQ)                        :: flmomzgm
        Real(kindQ)                        :: flmomxtm
        Real(kindQ)                        :: flmomytm
        Real(kindQ)                        :: flmomztm
        Real(kindQ)                        :: flhx
        Real(kindQ)                        :: flmass
        Real(kindQ)                        :: flmx
        Real(kindQ)                        :: flmy
        Real(kindQ)                        :: flmz
        Real(kindQ)                        :: fls
        Real(kindQ)                        :: flvortmag
        Real(kindQ)                        :: fx
        Real(kindQ)                        :: fxv
        Real(kindQ)                        :: fy
        Real(kindQ)                        :: fyv
        Real(kindQ)                        :: fz
        Real(kindQ)                        :: fzv
        Real(kindQ)                        :: perrSq
        Real(kindQ)                        :: pRms
        Real(kindQ)                        :: spl
        Real(kindQ)                        :: recovery
        Real(kindQ)                        :: temperature
        Real(kindQ)                        :: xmx
        Real(kindQ)                        :: xmxv
        Real(kindQ)                        :: xmy
        Real(kindQ)                        :: xmyv
        Real(kindQ)                        :: xmz
        Real(kindQ)                        :: xmzv

        Real(kindQ), dimension(:), pointer :: areaVisible

        Real(kindQ), dimension(:), pointer :: flmxi
        Real(kindQ), dimension(:), pointer :: flmyi
        Real(kindQ), dimension(:), pointer :: flmzi

        Real(kindDouble), dimension(:), pointer :: radiance
        Real(kindDouble)                        :: test
      end type LoadPlaneData

      type GroupLoadInfo
        sequence

        Logical                            :: isValid

        Real(kindQ)                        :: pOffset
        Real(kindQ)                        :: refS, refL, refX, refY, refZ
        Real(kindQ)                        :: hngX, hngY, hngZ
        Real(kindQ)                        :: idealThrust 
        Real(kindQ)                        :: forcex, forcey, forcez 
        Real(kindQ)                        :: forcexv, forceyv, forcezv 
        Real(kindQ)                        :: momx, momy, momz 
        Real(kindQ)                        :: momxv, momyv, momzv 
        Real(kindQ)                        :: flmassx, flmassy, flmassz, flmass 
        Real(kindQ)                        :: flmomtmxg, flmomtmyg, flmomtmzg
        Real(kindQ)                        :: flmomtmxt, flmomtmyt, flmomtmzt
        Real(kindQ)                        :: flmomtmxgm, flmomtmygm, flmomtmzgm
        Real(kindQ)                        :: flmomtmxtm, flmomtmytm, flmomtmztm
        Real(kindQ)                        :: flheatN
        Real(kindQ)                        :: flentropy
        Real(kindQ)                        :: flvortmag
        Real(kindQ)                        :: area
        Real(kindQ)                        :: recovery
        Real(kindQ)                        :: perrSq
        Real(kindQ)                        :: pRms
        Real(kindQ)                        :: spl
        Real(kindQ)                        :: temperature
        Real(kindQ), dimension(:), pointer :: flmassxi, flmassyi, flmasszi
      end type GroupLoadInfo

      type OutflowBC
        sequence
        Character(32) :: type
        Character(32) :: MFRmode
        Character(32) :: MFRtype
        Character(32) :: extrapMode
        Character(32) :: AIPrake
        Integer       :: extrapOrd
        Integer       :: updateFreq
        Real(kindQ)   :: massFlowTarget
        Real(kindQ)   :: machds
        Real(kindQ)   :: pds
        Real(kindQ)   :: relax
        Real(kindQ)   :: captureArea
        Real(kindQ)   :: area
        Real(kindQ)   :: area_reversed
        Real(kindQ)   :: areaAve_rho
        Real(kindQ)   :: areaAve_p
        Real(kindQ)   :: areaAve_gamma
        Real(kindQ)   :: areaAve_pt
        Real(kindQ)   :: areaAve_tt
        Real(kindQ)   :: massAve_mach
        Real(kindQ)   :: machCorrFac
        Real(kindQ)   :: massCorrFac
        Real(kindQ)   :: massFlowCurrent
        Real(kindQ)   :: massFlowLast
        Integer       :: cfmode
        Real(kindQ)   :: cfalph
        Real(kindQ)   :: cfbeta
        Real(kindQ)   :: cfcmf
        Real(kindQ)   :: cfgamc
        Real(kindQ)   :: cfgamh
        Real(kindQ)   :: cfmach
        Real(kindQ)   :: cftt
        Character(32) :: unsteady_mode
        Integer       :: unsteady_npts
        Real(kindQ)   :: unsteady_ampl
        Real(kindQ)   :: unsteady_freq
        Real(kindQ)   :: unsteady_mean
        Real(kindQ)   :: unsteady_phas
        Real(kindQ), dimension(:,:), pointer :: unsteady_profile
        Type(GroupLL), pointer :: groupP
      end type OutflowBC

      type WallBC
        sequence
        Integer       :: slipIters
        Character(32) :: wallFunction
        Character(32) :: type
        Real(kindQ)   :: roughness
        Real(kindQ)   :: temperature
        Real(kindQ)   :: emissivity
        Real(kindQ)   :: emissHi
        Real(kindQ)   :: emissLo
        Real(kindQ)   :: backside_flux
        Real(kindQ)   :: backside_h
        Real(kindQ)   :: effective_ht_coeff
        Real(kindQ)   :: temp_gradient
        Real(kindQ)   :: temp_gradient_refFS
        Real(kindQ)   :: enclosure_temp
        Real(kindQ)   :: env_directSolarFlux
        Real(kindQ)   :: env_solarAbsorptivity
        Real(kindQ)   :: env_solarElev
        Real(kindQ)   :: env_solarAzi
        Real(kindQ)   :: env_scatSolarFluxUp
        Real(kindQ)   :: env_scatSolarFluxLo
        Real(kindQ)   :: env_thermalFluxUp
        Real(kindQ)   :: env_thermalFluxLo
        Integer(kindInt), dimension(3) :: aeroDirs
        Real(kindXYZ), dimension(3) :: env_sunDirection
        Real(kindXYZ), dimension(3) :: env_skyDirection
        Character(32), dimension(:), pointer :: material_name
        Real(kindQ)  , dimension(:), pointer :: material_conductivity
        Real(kindQ)  , dimension(:), pointer :: material_thickness
        Real(kindQ)  , dimension(:), pointer :: material_heating
        Real(kindQ)  , dimension(:), pointer :: material_emissivity
        Type(GroupLL), pointer :: groupP
      end type WallBC

      type GroupBoundaryCondition
        sequence
        Integer(kindAry)                       :: discID
        Type(OutflowBC), dimension(:), pointer :: outflow
        Type(ArbBC)    , dimension(:), pointer :: inflow
        Type(WallBC)   , dimension(:), pointer :: wall
      end type GroupBoundaryCondition

      type GroupProperties
        sequence
        Character(32)                  :: name
        Real(kindDouble)               :: value
        Type(GroupProperties), pointer :: next
      end type GroupProperties

      type GroupLL
        sequence
        Character(32)                         :: name
        Logical                               :: optactive
        Integer(kindInt)                      :: senactive
        Type(GroupLL)               , pointer :: next
        Type(GroupLL)               , pointer :: parent
        Type(GroupProperties)       , pointer :: properties
        Type(GroupDefLL)            , pointer :: entities
        Type(GroupLoadInfo)         , pointer :: loadInfo(:)
        Type(GroupRigidMotion)      , pointer :: RigidMotion
        Type(GroupDeformingMotion)  , pointer :: DeformingMotion
        Type(GroupBoundaryCondition), pointer :: BoundaryCondition
      end type GroupLL

      type GroupDefLL 
        sequence
        Integer(kindAry)                           :: zone
        Integer(kindAry)                           :: srfindx
        Integer(kindAry)                           :: srfid
        Type(GroupLL)                    , pointer :: subGrp
        Integer(kindAry)                           :: loadMode
        Integer(kindAry)                           :: normalDir
        Integer(kindAry)   , dimension(:), pointer :: subset
        Type(LoadPlaneData), dimension(:,:), pointer :: LoadDat
        Type(GroupDefLL)                 , pointer :: next
      end type GroupDefLL

      type LoadIntegrationEntity
        sequence
        Integer(kindAry)                     :: reportflag
        Type(GroupLL)              , pointer :: groupP
        Type(LoadIntegrationEntity), pointer :: next
      end type LoadIntegrationEntity

      type acdiskGroups
        sequence
        Type(GroupLL)     , pointer :: groupP
        Type(acdiskGroups), pointer :: next
      end type acdiskGroups

    end module data_types

subroutine testit( x, q, bcdat )

  use data_types
  
  implicit none

  real                       :: q
  real                       :: x
  type(bcdata), dimension(3) :: bcdat

  call sub( x, q, bcdat )

  return
end subroutine testit

subroutine sub( x, q, bcdat )

  use data_types
  
  implicit none

  real                       :: q
  real                       :: x
  type(bcdata), dimension(3) :: bcdat

  q = x
  return
end subroutine sub
