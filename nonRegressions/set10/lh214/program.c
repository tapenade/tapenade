#include <stdio.h>
#include <math.h>

float sub1(float *x, float y, float *z) {
  *x = 3.7*y ;
  *z = *z + 2*y ;
  y = y*2.1 + 1.0;
  return y**z ;
}

void top(float a, float *b) {
  float x,y,z,t ;
  y = 2.0*a ;
  z = a-7.0 ;
  t = sqrt(a*z) ;
  t = a*y*z*t ;
  y = y*sub1(&x,y,&z) ;
  *b = x*y*z*t ;
  a = -2.9 ;
}

int main() {
  float a = 9.1 ;
  float b = 2.5 ;
  top(a, &b) ;
  printf("%f => %f\n",a,b) ;
}
