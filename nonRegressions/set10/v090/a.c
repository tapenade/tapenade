extern void test(double x, double y, double *z);

void f(double a, double b, double c) {
  test(a,b,&c);
}
