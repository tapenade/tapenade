/* f.f -- translated by f2c (version 19991025).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
  from F77 lh38
*/

/* Common Block Declarations */

struct {
    float pi;
} ext_;

#define ext_1 ext_

/* Subroutine */ int top_(x)
float *x;
{
    extern /* Subroutine */ int test_();

    ext_1.pi = (float)3.14;
    test_(x);
} /* top_ */

void main() {
    float x;
    x = 22.0;
    top_(&x);
    printf(" %f \n ", x);
}

