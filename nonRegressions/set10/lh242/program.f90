PROGRAM main
  real*4 a,b
  a = 2.
  b = 3.
  call top(a,b)
  print *,'res:',a+b
END PROGRAM main

SUBROUTINE TOP(x,y)
  real x,y
  complex*8 cc1,cc2
  complex*8 MYCONJG1, MYCONJG2
  cc1 = COMPLEX(x,x)
  cc2 = COMPLEX(x,y)
  cc1 = cc1 + cc2*MYCONJG1(cc2)
  cc1 = cc1 + cc2*MYCONJG2(cc2)
  x = REAL(cc1)
  y = AIMAG(cc1)
  y = y*x
END SUBROUTINE TOP

FUNCTION MYCONJG1(per)
  IMPLICIT NONE
  COMPLEX*8 :: per, MYCONJG1
  MYCONJG1 = COMPLEX(REAL(per),-AIMAG(per))
END FUNCTION MYCONJG1

FUNCTION MYCONJG2(per)
  IMPLICIT NONE
  COMPLEX*8 :: per, MYCONJG2, per2, res2
  REAL*4, DIMENSION(2) :: eqper,eqres
  EQUIVALENCE (per2,eqper)
  EQUIVALENCE (res2,eqres)
  per2 = per
  eqres(1) = eqper(1)
  eqres(2) = -eqper(2)
  MYCONJG2 = res2
END FUNCTION MYCONJG2
