! Another problem in sizing temporary arrays:
  SUBROUTINE test(inValue,GaussWeight,Bfloc,VtriLoc)
    ! It is hard to guess the size of the result of Bracket()
    IMPLICIT NONE
    REAL(KIND=8)                              :: inValue
    REAL(kind=8),DIMENSION(:)                 :: GaussWeight
    REAL(KIND=8),DIMENSION(:,:,:)             :: Bfloc
    REAL(KIND=8),DIMENSION(:,:)               :: VtriLoc
    INTEGER                                      :: si,sj
    INTEGER                                      :: ng
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE    :: Gradphi
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE    :: Gradpsi

    ng = SIZE(GaussWeight)
    ALLOCATE(Gradphi(2,ng), Gradpsi(2,ng))
    !! Evaluation of grad of phi and psi on Gauss points 
    call myfill(inValue,Gradphi)
    call myfill(inValue,Gradpsi)

    VtriLoc = 0.
    DO si=1,3
       DO sj = 1,3
          VtriLoc(si,sj) = (1./3.)*SUM( GaussWeight * Bracket(Gradphi,Gradpsi) * Bfloc(si,sj,:) )
       END DO
    END DO

    DEALLOCATE(Gradphi, Gradpsi)
   
  END SUBROUTINE test

  FUNCTION Bracket(u,v) RESULT(w)
    ! Compute the poisson bracket of two functions 
    REAL(KIND=8), DIMENSION(:,:)       :: u
    REAL(KIND=8), DIMENSION(:,:)       :: v
    REAL(KIND=8), DIMENSION(size(u,2)) :: w

    w = 0.0d0 
    w = u(1,:)*v(2,:) - u(2,:)*v(1,:) 
  END FUNCTION Bracket
