! Bug report by Jose Cardesa (IMFT) on Jan 22, 2019
! Bug due to wrong detection of Statement-Functions in compfunc()

module params
  implicit none
  integer, parameter            :: sp            = kind(0.0) 
  integer, parameter            :: dp            = kind(0.0d0)
  integer, parameter            :: int32         = selected_int_kind(9)
  real(dp), parameter           :: pi            = 4.0_dp*atan(1.0_dp)
  integer(int32), parameter     :: Nt            = 5001
  real(dp), parameter           :: T_intbound    = pi 
  real(dp), parameter           :: dt            = T_intbound/real(Nt-1,dp)
  integer, parameter            :: char_len      = 150  
end module params


module common
  use params
  implicit none
  real(dp)         :: x
  real(dp)         :: time
  real(dp)         :: alfa
  real(dp)         :: A
  real(dp)         :: omega
  real(dp)         :: x0
  integer(int32)   :: inititer
  integer(int32)   :: intfreq
  integer(int32)   :: maxiter
  integer(int32)   :: currentiteration
  real(dp), allocatable :: costfunckern(:)
  real(dp), allocatable :: tveccost(:)    
end module common


module adjointtools
  use params
  use common
  implicit none

contains

  subroutine compfunc
    implicit none
! NOTE: The following must *not* be detected as statement-functions!
    costfunckern(currentiteration) = x**2.0_dp*cos(time)/2.0_dp
    tveccost(currentiteration)     = time
  end subroutine compfunc
end module adjointtools


module timeloop
  use params
  use common
  use adjointtools
  implicit none

contains

  subroutine solve_ode
    implicit none
    real(dp)       :: dxdt
    integer(int32) :: iter
    iter = inititer
    time_l: do while (iter.LE.maxiter)
       time = dt*real(iter-1,dp)
       currentiteration = iter
       if ( (iter .GT. inititer).AND.&
            (iter.NE.(inititer+maxiter-1)).AND.&
            (mod(iter,intfreq).EQ.0) ) then
! The following caused a bug due to wrong detection of statement-function in compfunc()
           call compfunc
! NOTE: the following did work with Tapenade:
!          costfunckern(currentiteration) = x**2.0_dp*cos(time)/2.0_dp
!          tveccost(currentiteration)     = time          

       end if
       call comp_dxdt(x, time, dxdt)     
       x = x + dt*dxdt
       iter = iter + 1
    end do time_l
  end subroutine solve_ode
  
  ! Based on explicit, 4th-order classic Runge-Kutta
  subroutine comp_dxdt(x_in,t_in,argout)
    implicit none
    real(dp), intent(out) :: argout
    real(dp), intent(in)  :: x_in,t_in
    real(dp)              :: k(1:4),b(1:4)
    b(1:4) = (/ 1.0_dp, 2.0_dp, 2.0_dp, 1.0_dp/)/6.0_dp
    call ode_eval(x_in,t_in,k(1))
    call ode_eval(x_in+dt/2.0_dp*k(1),t_in+dt/2.0_dp,k(2))
    call ode_eval(x_in+dt/2.0_dp*k(2),t_in+dt/2.0_dp,k(3))
    call ode_eval(x_in+dt*k(3),t_in+dt,k(4))
    argout = sum(k(:)*b(:))
  end subroutine comp_dxdt

  subroutine ode_eval(x_in,t_in,argout)
    implicit none
    real(dp), intent(in)  :: x_in,t_in
    real(dp), intent(out) :: argout
    argout = alfa*x_in + A*cos(omega*t_in)
  end subroutine ode_eval
end module timeloop


module adjointtopsub
  use params
  use common
  use timeloop
  implicit none

contains

  subroutine topsub
    x = x0
    call solve_ode
  end subroutine topsub
end module adjointtopsub


subroutine setparamvalues
  use params
  use common
  implicit none
  A              = 1.0_dp
  omega          = 2.0_dp
  alfa           = 0.3_dp
  x0             = 1.0_dp
  inititer       = 1
  maxiter        = Nt
  intfreq        = 100
  allocate(costfunckern(inititer:maxiter))
  allocate(tveccost(inititer:maxiter))
end subroutine setparamvalues


subroutine terminate
  use common
  implicit none
  deallocate(costfunckern)
  deallocate(tveccost)
  write(*,*) ' Normal program termination '
end subroutine terminate


program main
  use params
  use common
  use adjointtopsub
  implicit none
  call setparamvalues  
  call topsub
  call terminate
end program main
