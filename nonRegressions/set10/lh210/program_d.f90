!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (develop) - 25 Jul 2023 13:34
!
MODULE COMMON_DIFF
  USE PARAMS
  IMPLICIT NONE
  REAL(dp) :: x
  REAL(dp) :: xd
  REAL(dp) :: time
  REAL(dp) :: alfa
  REAL(dp) :: a
  REAL(dp) :: omega
  REAL(dp) :: x0
  REAL(dp) :: x0d
  INTEGER(int32) :: inititer
  INTEGER(int32) :: intfreq
  INTEGER(int32) :: maxiter
  INTEGER(int32) :: currentiteration
  REAL(dp), ALLOCATABLE :: costfunckern(:)
  REAL(dp), ALLOCATABLE :: costfunckernd(:)
  REAL(dp), ALLOCATABLE :: tveccost(:)
END MODULE COMMON_DIFF

MODULE ADJOINTTOOLS_DIFF
  USE PARAMS
  USE COMMON_DIFF
  IMPLICIT NONE

CONTAINS
!  Differentiation of compfunc in forward (tangent) mode:
!   variations   of useful results: *costfunckern
!   with respect to varying inputs: *costfunckern x
!   Plus diff mem management of: costfunckern:in
  SUBROUTINE COMPFUNC_D()
    IMPLICIT NONE
    INTRINSIC COS
    REAL(dp) :: temp
! NOTE: The following must *not* be detected as statement-functions!
    temp = COS(time)
    costfunckernd(currentiteration) = temp*x*xd
    costfunckern(currentiteration) = temp*(x**2.0_dp/2.0_dp)
    tveccost(currentiteration) = time
  END SUBROUTINE COMPFUNC_D

  SUBROUTINE COMPFUNC()
    IMPLICIT NONE
    INTRINSIC COS
! NOTE: The following must *not* be detected as statement-functions!
    costfunckern(currentiteration) = x**2.0_dp*COS(time)/2.0_dp
    tveccost(currentiteration) = time
  END SUBROUTINE COMPFUNC

END MODULE ADJOINTTOOLS_DIFF

MODULE TIMELOOP_DIFF
  USE PARAMS
  USE COMMON_DIFF
  USE ADJOINTTOOLS_DIFF
  IMPLICIT NONE

CONTAINS
!  Differentiation of solve_ode in forward (tangent) mode:
!   variations   of useful results: *costfunckern
!   with respect to varying inputs: x
!   Plus diff mem management of: costfunckern:in
  SUBROUTINE SOLVE_ODE_D()
    IMPLICIT NONE
    REAL(dp) :: dxdt
    REAL(dp) :: dxdtd
    INTEGER(int32) :: iter
    INTRINSIC REAL
    INTRINSIC MOD
    iter = inititer
    IF (ALLOCATED(costfunckernd)) costfunckernd = 0.0_8
time_l:DO WHILE (iter .LE. maxiter)
      time = dt*REAL(iter-1, dp)
      currentiteration = iter
      IF (iter .GT. inititer .AND. iter .NE. inititer + maxiter - 1 &
&         .AND. MOD(iter, intfreq) .EQ. 0) THEN
! The following caused a bug due to wrong detection of statement-function in compfunc()
        CALL COMPFUNC_D()
! NOTE: the following did work with Tapenade:
!          costfunckern(currentiteration) = x**2.0_dp*cos(time)/2.0_dp
!          tveccost(currentiteration)     = time          
      END IF
      CALL COMP_DXDT_D(x, xd, time, dxdt, dxdtd)
      xd = xd + dt*dxdtd
      x = x + dt*dxdt
      iter = iter + 1
    END DO time_l
  END SUBROUTINE SOLVE_ODE_D

  SUBROUTINE SOLVE_ODE()
    IMPLICIT NONE
    REAL(dp) :: dxdt
    INTEGER(int32) :: iter
    INTRINSIC REAL
    INTRINSIC MOD
    iter = inititer
time_l:DO WHILE (iter .LE. maxiter)
      time = dt*REAL(iter-1, dp)
      currentiteration = iter
      IF (iter .GT. inititer .AND. iter .NE. inititer + maxiter - 1 &
&         .AND. MOD(iter, intfreq) .EQ. 0) THEN
! The following caused a bug due to wrong detection of statement-function in compfunc()
        CALL COMPFUNC()
! NOTE: the following did work with Tapenade:
!          costfunckern(currentiteration) = x**2.0_dp*cos(time)/2.0_dp
!          tveccost(currentiteration)     = time          
      END IF
      CALL COMP_DXDT(x, time, dxdt)
      x = x + dt*dxdt
      iter = iter + 1
    END DO time_l
  END SUBROUTINE SOLVE_ODE

!  Differentiation of comp_dxdt in forward (tangent) mode:
!   variations   of useful results: argout
!   with respect to varying inputs: x_in
! Based on explicit, 4th-order classic Runge-Kutta
  SUBROUTINE COMP_DXDT_D(x_in, x_ind, t_in, argout, argoutd)
    IMPLICIT NONE
    REAL(dp), INTENT(OUT) :: argout
    REAL(dp), INTENT(OUT) :: argoutd
    REAL(dp), INTENT(IN) :: x_in, t_in
    REAL(dp), INTENT(IN) :: x_ind
    REAL(dp) :: k(4), b(4)
    REAL(dp) :: kd(4)
    INTRINSIC SUM
    b(1:4) = (/1.0_dp, 2.0_dp, 2.0_dp, 1.0_dp/)/6.0_dp
    kd = 0.0_8
    CALL ODE_EVAL_D(x_in, x_ind, t_in, k(1), kd(1))
    CALL ODE_EVAL_D(x_in + dt/2.0_dp*k(1), x_ind + dt*kd(1)/2.0_dp, t_in&
&             + dt/2.0_dp, k(2), kd(2))
    CALL ODE_EVAL_D(x_in + dt/2.0_dp*k(2), x_ind + dt*kd(2)/2.0_dp, t_in&
&             + dt/2.0_dp, k(3), kd(3))
    CALL ODE_EVAL_D(x_in + dt*k(3), x_ind + dt*kd(3), t_in + dt, k(4), &
&             kd(4))
    argoutd = SUM(b(:)*kd(:))
    argout = SUM(k(:)*b(:))
  END SUBROUTINE COMP_DXDT_D

! Based on explicit, 4th-order classic Runge-Kutta
  SUBROUTINE COMP_DXDT(x_in, t_in, argout)
    IMPLICIT NONE
    REAL(dp), INTENT(OUT) :: argout
    REAL(dp), INTENT(IN) :: x_in, t_in
    REAL(dp) :: k(4), b(4)
    INTRINSIC SUM
    b(1:4) = (/1.0_dp, 2.0_dp, 2.0_dp, 1.0_dp/)/6.0_dp
    CALL ODE_EVAL(x_in, t_in, k(1))
    CALL ODE_EVAL(x_in + dt/2.0_dp*k(1), t_in + dt/2.0_dp, k(2))
    CALL ODE_EVAL(x_in + dt/2.0_dp*k(2), t_in + dt/2.0_dp, k(3))
    CALL ODE_EVAL(x_in + dt*k(3), t_in + dt, k(4))
    argout = SUM(k(:)*b(:))
  END SUBROUTINE COMP_DXDT

!  Differentiation of ode_eval in forward (tangent) mode:
!   variations   of useful results: argout
!   with respect to varying inputs: x_in
  SUBROUTINE ODE_EVAL_D(x_in, x_ind, t_in, argout, argoutd)
    IMPLICIT NONE
    REAL(dp), INTENT(IN) :: x_in, t_in
    REAL(dp), INTENT(IN) :: x_ind
    REAL(dp), INTENT(OUT) :: argout
    REAL(dp), INTENT(OUT) :: argoutd
    INTRINSIC COS
    argoutd = alfa*x_ind
    argout = alfa*x_in + a*COS(omega*t_in)
  END SUBROUTINE ODE_EVAL_D

  SUBROUTINE ODE_EVAL(x_in, t_in, argout)
    IMPLICIT NONE
    REAL(dp), INTENT(IN) :: x_in, t_in
    REAL(dp), INTENT(OUT) :: argout
    INTRINSIC COS
    argout = alfa*x_in + a*COS(omega*t_in)
  END SUBROUTINE ODE_EVAL

END MODULE TIMELOOP_DIFF

MODULE ADJOINTTOPSUB_DIFF
  USE PARAMS
  USE COMMON_DIFF
  USE TIMELOOP_DIFF
  IMPLICIT NONE

CONTAINS
!  Differentiation of topsub in forward (tangent) mode:
!   variations   of useful results: *costfunckern
!   with respect to varying inputs: x0
!   RW status of diff variables: *costfunckern:out x:(loc) x0:in
!   Plus diff mem management of: costfunckern:in
  SUBROUTINE TOPSUB_D()
    IMPLICIT NONE
    xd = x0d
    x = x0
    CALL SOLVE_ODE_D()
  END SUBROUTINE TOPSUB_D

  SUBROUTINE TOPSUB()
    IMPLICIT NONE
    x = x0
    CALL SOLVE_ODE()
  END SUBROUTINE TOPSUB

END MODULE ADJOINTTOPSUB_DIFF

SUBROUTINE MAIN_NODIFF()
  USE PARAMS
  USE COMMON_DIFF
  USE ADJOINTTOPSUB_DIFF
  IMPLICIT NONE
  CALL SETPARAMVALUES_NODIFF()
  CALL TOPSUB()
  CALL TERMINATE_NODIFF()
END SUBROUTINE MAIN_NODIFF

SUBROUTINE SETPARAMVALUES_NODIFF()
  USE PARAMS
  USE COMMON_DIFF
  IMPLICIT NONE
  a = 1.0_dp
  omega = 2.0_dp
  alfa = 0.3_dp
  x0 = 1.0_dp
  inititer = 1
  maxiter = nt
  intfreq = 100
  ALLOCATE(costfunckern(inititer:maxiter))
  ALLOCATE(tveccost(inititer:maxiter))
END SUBROUTINE SETPARAMVALUES_NODIFF

SUBROUTINE TERMINATE_NODIFF()
  USE COMMON_DIFF
  IMPLICIT NONE
  DEALLOCATE(costfunckern)
  DEALLOCATE(tveccost)
  WRITE(*, *) ' Normal program termination '
END SUBROUTINE TERMINATE_NODIFF

