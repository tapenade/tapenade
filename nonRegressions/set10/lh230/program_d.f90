!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.15 (feature_bugFixes) -  1 Dec 2020 19:47
!
!  Differentiation of main as a context to call tangent code (with options context noISIZE):
PROGRAM MAIN_D
  IMPLICIT NONE
  REAL, DIMENSION(:), POINTER :: pp1
  REAL, DIMENSION(:), POINTER :: pp1d
  REAL, DIMENSION(:), POINTER :: pp2
  REAL, DIMENSION(:), POINTER :: pp2d
  COMMON /ptrcommon/ pp1, pp2
  COMMON /ptrcommon_d/ pp1d, pp2d
  SAVE /ptrcommon/
  SAVE /ptrcommon_d/
  REAL :: a, b
  REAL :: ad, bd
  INTEGER :: i
  REAL, DIMENSION(30), TARGET :: tt1
  REAL, DIMENSION(30), TARGET :: tt1d
  REAL, DIMENSION(30), TARGET :: tt2
  REAL, DIMENSION(30), TARGET :: tt2d
  a = 2.2
  b = 3.3
  DO i=1,30
    tt1d(i) = 0.0
    tt1(i) = 10.0 + i
    tt2d(i) = 0.0
    tt2(i) = 10.5 + i
  END DO
  pp1d => tt1d
  pp1 => tt1
  pp2d => tt2d
  pp2 => tt2
  CALL ADCONTEXTTGT_INIT(1.e-4_8, 0.87_8)
  IF (ASSOCIATED(pp1)) CALL ADCONTEXTTGT_INITREAL4ARRAY('pp1'//CHAR(0), &
&                                                 pp1, pp1d, SIZE(pp1, 1&
&                                                 ))
  IF (ASSOCIATED(pp2)) CALL ADCONTEXTTGT_INITREAL4ARRAY('pp2'//CHAR(0), &
&                                                 pp2, pp2d, SIZE(pp2, 1&
&                                                 ))
  CALL ADCONTEXTTGT_INITREAL4('a'//CHAR(0), a, ad)
  CALL ADCONTEXTTGT_INITREAL4('b'//CHAR(0), b, bd)
  CALL FOO_D(a, ad, b, bd)
  CALL ADCONTEXTTGT_STARTCONCLUDE()
  IF (ASSOCIATED(pp1)) CALL ADCONTEXTTGT_CONCLUDEREAL4ARRAY('pp1'//CHAR(&
&                                                     0), pp1, pp1d, &
&                                                     SIZE(pp1, 1))
  IF (ASSOCIATED(pp2)) CALL ADCONTEXTTGT_CONCLUDEREAL4ARRAY('pp2'//CHAR(&
&                                                     0), pp2, pp2d, &
&                                                     SIZE(pp2, 1))
  CALL ADCONTEXTTGT_CONCLUDEREAL4('a'//CHAR(0), a, ad)
  CALL ADCONTEXTTGT_CONCLUDEREAL4('b'//CHAR(0), b, bd)
  CALL ADCONTEXTTGT_CONCLUDE()
  PRINT*, b, tt1(10), tt1(20)
END PROGRAM MAIN_D

!  Differentiation of foo in forward (tangent) mode (with options context noISIZE):
!   variations   of useful results: *pp1 b
!   with respect to varying inputs: *pp1 *pp2 a
!   RW status of diff variables: *pp1:in-out *pp2:in a:in b:out
!   Plus diff mem management of: pp1:in pp2:in
! mixing COMMONs and pointers
SUBROUTINE FOO_D(a, ad, b, bd)
  IMPLICIT NONE
  REAL :: a, b
  REAL :: ad, bd
  REAL, DIMENSION(:), POINTER :: pp1
  REAL, DIMENSION(:), POINTER :: pp1d
  REAL, DIMENSION(:), POINTER :: pp2
  REAL, DIMENSION(:), POINTER :: pp2d
  COMMON /ptrcommon/ pp1, pp2
  COMMON /ptrcommon_d/ pp1d, pp2d
  SAVE /ptrcommon/
  SAVE /ptrcommon_d/
  INTRINSIC SIN
  bd = 2*a*ad
  b = a*a
  pp1d(10) = 2*a*ad
  pp1(10) = a*a
  bd = bd + pp2(15)*pp2d(5) + pp2(5)*pp2d(15)
  b = b + pp2(5)*pp2(15)
  pp1d(20) = COS(pp2(20))*pp2d(20)
  pp1(20) = SIN(pp2(20))
END SUBROUTINE FOO_D

