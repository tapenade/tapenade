! another bug found by Boeing
! null wrapped type for result1 (because precision() not in lib)
! secondary problem: vbytes_double_d should not return "sized"
        Integer(kindAry) function vbytes_double( vkind, val ) result( size )
          Implicit None
          Real(kindDouble) :: vkind, val

          if ( precision(vkind) > 15 ) then
            size = 16
          else if ( precision(vkind) > 7 ) then
            size = 8
          else
            size = 4
          end if

          val = val*size

          return
        end function vbytes_double
