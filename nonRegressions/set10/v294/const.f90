MODULE const
    IMPLICIT NONE
    SAVE
    !Variable Declarations
    INTEGER, parameter :: MY = 10
    INTEGER, parameter :: MX = 4*MY
    INTEGER, parameter :: MAX_DVAR = 100
    DOUBLE PRECISION, parameter :: MY_PI = 3.14159
END MODULE const
