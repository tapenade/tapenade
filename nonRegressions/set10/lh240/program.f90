! Bug found by Shu-Jie Li, shujie@csrc.ac.cn, 4May2020
! Wrong op_where in inlined function
subroutine buginline(x)
  real, dimension(4) :: array
  real :: scalar
  scalar = x
  array(1:3) = ABS(scalar)
  x = array(2)
end subroutine buginline
