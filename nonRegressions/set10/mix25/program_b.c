/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_functionPointers) - 27 Nov 2024 12:23
*/
#include <adStack.h>

/*
  Differentiation of top_ in reverse (adjoint) mode:
   gradient     of useful results: *b
   with respect to varying inputs: a *b
   Plus diff mem management of: b:in
*/
void top_b_(float a, float *ab1, float *b, float *bb, float ab0) {
    float ab;
    a = 2*a;
    ab = 2*a*(*bb);
    *bb = 0.0;
    ab = 2*ab;
    *ab1 = *ab1 + ab;
}

void top_nodiff_(float a, float *b, float ab0) {
    printf("a %f\n", a);
    a = 2*a;
    *b = a*a;
    printf("a %f b %f\n", a, *b);
}
