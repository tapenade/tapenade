program main
implicit none
real :: x, y
x = 2.0
print*, 'x ', x
call foo(x,y)
print*, 'x ', x, ' y ', y

contains
subroutine foo(x, y)
implicit none
real :: x, y
interface
  subroutine top(u,v, k)
      real, value :: u
      real :: v
      real, value :: k
  end subroutine top
end interface
call top(x, y, 0.0)
end subroutine

end program
