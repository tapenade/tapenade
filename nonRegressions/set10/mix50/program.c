// from mix41, pour tester NULL
#include <stdio.h>
int nbglobal = 2;
double *calcul_global = NULL;
extern double f_(double*, double*);

extern int truc_(double*);

void top (double *x, double *y) {
    double calcul_glob;
    calcul_glob = *calcul_global + 1;
    *y = f_(x, &calcul_glob);
    *y = *y + *calcul_global * *x;
    *y = f_(x, &calcul_glob);
    *calcul_global = calcul_glob;
    *y = *y + *calcul_global * *x;
}

/*
int truc_(double *x) {
  calcul_global = NULL;
  }*/

main()
{
  double i=2;
  double j=3;
  calcul_global[0] = 1;
  calcul_global[1] = 2;
  printf("calcul_global     %f\n", *calcul_global);
  printf("i %f * j %f ...\n", i, j);
  top(&i, &j);
  printf("j --> %f\n", j);
  printf("calcul_global --> %f\n", *calcul_global);

}
