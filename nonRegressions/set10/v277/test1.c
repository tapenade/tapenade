static int nbCall1 = 0;

void bar1(float a, float *b) {
  *b = a * a;
}

void foo1(float *x, float *y) {
  bar1(*x, y);

}

void testNbCall1() {
  printf("testNbCall\n");
}
