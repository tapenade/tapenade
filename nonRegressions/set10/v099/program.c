double MAX=100;

void defaultShowD(double d) {
  d = d + MAX;
}

// showD est une functionDecl
void (*showD)(double) = defaultShowD;

extern void (*showD)(double);
