#include <stdio.h>

extern void barf(float*);

void foo(float *x) {
  extern float statix1; // pour tester extern dans une Unit
  extern float statix2;
  extern float STATIX3;
  extern float STATIX4;
  printf("1 statix1 %f\n", statix1);
  statix1 = *x;
  STATIX3 = *x * *x;
  printf("2 statix1 %f\n", statix1);
  barf(x);
  printf("3 statix1 %f\n", statix1);
}

void main() {
  extern float statix1;
  extern float statix2;
  extern float STATIX3;
  extern float STATIX4;
  float x, y, z;
  statix1 = 999;
  x = 2.0;
  foo(&x);
  printf("x %f statix1 %f statix2 %f \n", x, statix1, statix2);
}
