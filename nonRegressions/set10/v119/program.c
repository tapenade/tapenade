#include <stdio.h>
#include "extr.h"

r1 global;

float mulp (float a) {
  return(a * global.v[1]);
}

float mula (float a) {
  return(a * global.v[1]);
}

float mulb (float a) {
  float result;
  result = mula(global.h[1]);
  result = mulp(result);
  return(result);
}
