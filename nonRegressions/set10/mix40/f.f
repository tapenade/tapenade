      double precision function f(t, calcul)
      double precision t, calcul
      integer nb,i
      external truc
      logical truc
      nb = floor(calcul)
      DO i=1,nb
        f = t * calcul
        if (truc(calcul)) then
           calcul = calcul + 1
        end if
        print*, 't ', t, ' calcul ', calcul, ' -> ', f
      END DO
      return
      end
