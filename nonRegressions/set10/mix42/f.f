C from 77/lh02, from C mix26 + Options
      subroutine top(x,y,z,a,b,c)
      real x,y,z,a,b,c
      external sub1
      if (x.gt.0.0) then
         y = 1.7
         call sub1(x,y,z)
         z = 5.1*z
         x = y+z
      else
         y = 3.3*x**2.0
      endif
      a = -2.9
      call sub1(a,b,c)
      end
