/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.15 (feature_bugFixes) - 27 Aug 2020 18:38
*/
#include <adStack.h>
#include <stdio.h>
#include <math.h>

/*
  Differentiation of sub1 in reverse (adjoint) mode:
   gradient     of useful results: x[_:_]
   with respect to varying inputs: x[_:_]
   RW status of diff variables: x:(loc) x[_:_]:in-out
   Plus diff mem management of: x:in
*/
void sub1_b(float x[], float xb[], int n) {
    extern float f_();
    extern void f_b_(float [], float [], float *);
    float tmp;
    float tmpb;
    tmpb = xb[0];
    xb[0] = 0.0;
    f_b_(x, xb, &tmpb);
}
