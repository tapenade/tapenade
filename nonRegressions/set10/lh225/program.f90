! Bug found by Herve Guillard: option -noisize did not work in those cases:
subroutine test(A,B,C,D,res)
REAL*8, DIMENSION(:)     :: A
REAL*8, DIMENSION(:,:)   :: B,C
REAL*8, DIMENSION(:,:,:) :: D
REAL*8                   :: res1,res2,res3,res4,res

res1 = SUM(A(:)*B(3,:))
res2 = SQRT(SUM(B(2,:)*C(:,4)))
res3 = SUM(D(2,2,:)*C(5,:)*SUM(D(1,:,3)*B(6,:)+B(7,:)))
res4 = A(10) * MAXVAL(SQRT(A(:)*B(2,:)))
res = res1*res2 + res3*res4
end subroutine test
