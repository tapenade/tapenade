#include <math.h>

/* A test to check that TBR does extend to pointer destinations:
 * here p may point to either a or b, so when *p is used non-linearly,
 * this should cause both a and b to become TBR.
 * However, current adjoint is not optimal because
 * either *p or (a and b) need to be TBR, but not both! */
double test(double x, double y, int n) {
  double a, b ;
  double *p ;
  a = x*x ;
  b = x+y ;
  if (n>0) {
    p = &a ;
  } else {
    p = &b ;
  }
  *p = sin(*p) ;
  a = 2.3*a + 1.0 ;
  b = b + 2.0 ;
  *p = *p + sin(a) + sin(b) ;
  return *p ;
}

int main() {
  double x = 2.5 ;
  double y = -1.1 ;
  double res = test(x,y,5) ;
}
