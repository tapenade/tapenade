extern void fsim_(int *i, float *r);

void top(int i, float *r) {
  fsim_(&i, r); 
  printf("r %f\n", *r);

}

void main() {
  int i=100;
  float r;
  top(i, &r);
}
