! INSPIRE DE auroux-striation
! pour etudier un probleme avec la diff -b no!SliceDeadControl no!SliceDeadInstrs

MODULE mod_StriDenamb

  IMPLICIT NONE

CONTAINS

SUBROUTINE foo(Nalpha, Nbeta, Ngamma, &
               Mat_magnetique, dt, u_ini, density )

  !INTENT
  INTEGER,INTENT(IN)::Nalpha, Nbeta, Ngamma
  REAL (DOUBLE),INTENT(IN)::dt
  REAL (DOUBLE),DIMENSION(:,:,:),INTENT(INOUT)::density,u_ini
  REAL(DOUBLE),DIMENSION(:,:,:),INTENT(IN)::Mat_magnetique
 
  !LOCAL
  INTEGER::i,j,k,flag,RK !indices
  REAL(DOUBLE)::valphamdemi, valphapdemi, vbetamdemi, &
                vbetapdemi, vgammamdemi, vgammapdemi !vitesses
  REAL(DOUBLE)::Fleft, Fright, Gleft, Gright, Hleft, Hright !flux
  REAL(DOUBLE),ALLOCATABLE,DIMENSION(:,:,:)::u0, u1, u2, u3, u_ext, mat_flux
  REAL(DOUBLE)::deltau
  REAL(DOUBLE)::sigma, sigma1, sigma2, vleft, vright
 
  !Initialisation
  ALLOCATE( u0( Nalpha-1, Nbeta-1, Ngamma-1 ), u1( Nalpha-1, Nbeta-1, Ngamma-1 ),&
            u2( Nalpha-1, Nbeta-1, Ngamma-1 ), u3( Nalpha-1, Nbeta-1, Ngamma-1 ),&
            u_ext( Nalpha+3, Nbeta+3, Ngamma+3 ),&
            mat_flux( Nalpha-1, Nbeta-1, Ngamma-1 ) )

  !Donnee initiale
  DO i=1,Nalpha-1
    DO j=1,Nbeta-1
      DO k=1,Ngamma-1
        u0(i,j,k) = density(i,j,k) / Mat_magnetique(i,j,k)**2
      END DO
    END DO
  END DO

  flag = 4
  RK   = 3
  IF( RK == 1 ) THEN
    CALL flux_RK(dt, u_ini, u0, flag, u1 )
    u0 = u1
  ELSE IF( RK == 3 ) THEN
    CALL flux_RK(dt, u_ini, u0, flag, u1 )
    CALL flux_RK(dt, u_ini, u1, flag, mat_flux )
    u2 = 3.0D0/4.D0*u0 + 1.0D0/4.D0*mat_flux
    CALL flux_RK(dt, u_ini, u2, flag, mat_flux )
    u3 = 1.0D0/3.D0*u0 + 2.0D0/3.D0*mat_flux
    u0 = u3
  END IF

  DO i=1,Nalpha-1
    DO j=1,Nbeta-1
      DO k=1,Ngamma-1              
        Density(i,j,k) = u0(i,j,k)*Mat_magnetique(i,j,k)**2
      END DO
    END DO
  END DO
  DEALLOCATE( u0, u1, u2, u3, u_ext )
END SUBROUTINE StriDenamb_splitting


! Dummy implementation just to reproduce dependencies.
SUBROUTINE flux_RK(dt, u_ini, u, flag, u_out )
!----------------------------------------------------------------------------
  !INTENT
  INTEGER,INTENT(IN):: flag
  REAL (DOUBLE),INTENT(IN):: dt
  REAL (DOUBLE),DIMENSION(:,:,:),INTENT(IN)::u,u_ini
  REAL (DOUBLE),DIMENSION(:,:,:),INTENT(OUT)::u_out

  u_out(1,2,3) = dt*flag*u(1,2,3)*u_ini(1,2,3)

END SUBROUTINE flux_RK

END MODULE mod_StriDenamb
