module real_precision
  implicit none
  INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(12,307)
  INTEGER, PARAMETER :: wp = dp
end module real_precision

module dimension_1D
  use real_precision
  INTEGER,parameter :: nx=251
  REAL(KIND=wp),dimension(nx) :: flotte
  REAL(KIND=wp),dimension(nx) :: Bsoc
  REAL(KIND=wp),dimension(nx) :: H
  REAL(KIND=wp),dimension(nx) :: H_buoy
  REAL(KIND=wp),dimension(nx) :: H_Rho
  REAL(KIND=wp),dimension(nx) :: B
  REAL(KIND=wp),dimension(nx) :: S
  REAL(KIND=wp) :: sealevel, Rho_sea, Rho_i
end module dimension_1D

module testmod
  use real_precision
  use dimension_1D

contains

  ! Bug git#9, on nodet-winnie
  subroutine flottab_1D()
    implicit none

    H_Rho(:)=H(:)*Rho_i
    where(Bsoc(:).le.(sealevel-(H_Rho(:)/Rho_sea)))
       B(:)=sealevel-(H_Rho(:)/Rho_sea)
       flotte(:)=1
    elsewhere
       B(:)=Bsoc(:)
       flotte(:)=0
       H_buoy(:)= (sealevel-Bsoc(:))*Rho_sea/Rho_i
    end where
    H_buoy(:)= (sealevel-Bsoc(:))*Rho_sea/Rho_i
    H_buoy(:)=H(:)-H_buoy(:)
    S(:)=H(:)+B(:)
  end subroutine flottab_1D

end module testmod
