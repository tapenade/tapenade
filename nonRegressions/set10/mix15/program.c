void modify_(float val, float *p, float * q, float * o)
{
  printf("modify_ val %f p %f q %f o %f\n", val, p, q, o);
  printf("modify_ val %f *p %f *q %f *o %f\n", val, *p, *q, *o);
    *p = 27 * *p; // passed by value - only the local parameter is modified
    *q = 27 * *p; // passed by value or reference, check call site to determine which
    *o = 27 * *p; // passed by value or reference, check call site to determine which
    printf("modif-> val %f *p %f *q %f *o %f\n", val, *p, *q, *o);
}
/*
int main()
{
    float a = 1;
    float b = 1;
    float x = 1;
    float * c = &x;
    float val = 100;
    Modify(val, &a, &b, c);   // a is passed by value, b is passed by reference by creating a pointer,
                        // c is a pointer passed by value
    // b and x are changed
    return(0);
}
*/
