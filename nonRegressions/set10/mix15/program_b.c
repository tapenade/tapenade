/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.14 (master) -  7 Aug 2019 15:00
*/
#include <adStack.h>

/*
  Differentiation of modify_ in reverse (adjoint) mode:
   gradient     of useful results: *o *p *q
   with respect to varying inputs: *o *p *q
   Plus diff mem management of: o:in p:in q:in
*/
void modify_b_(float val, float *p, float *pb, float *q, float *qb, float *o, 
        float *ob) {
    *pb = *pb + 27*(*ob) + 27*(*qb);
    *ob = 0.0;
    *qb = 0.0;
    *pb = 27*(*pb);
}
/*
int main()
{
    float a = 1;
    float b = 1;
    float x = 1;
    float * c = &x;
    float val = 100;
    Modify(val, &a, &b, c);   // a is passed by value, b is passed by reference by creating a pointer,
                        // c is a pointer passed by value
    // b and x are changed
    return(0);
}
*/

void modify_nodiff_(float val, float *p, float *q, float *o) {
    printf("modify_ val %f p %f q %f o %f\n", val, p, q, o);
    printf("modify_ val %f *p %f *q %f *o %f\n", val, *p, *q, *o);
    *p = 27*(*p);
    // passed by value - only the local parameter is modified
    *q = 27*(*p);
    // passed by value or reference, check call site to determine which
    *o = 27*(*p);
    // passed by value or reference, check call site to determine which
    printf("modif-> val %f *p %f *q %f *o %f\n", val, *p, *q, *o);
}
/*
int main()
{
    float a = 1;
    float b = 1;
    float x = 1;
    float * c = &x;
    float val = 100;
    Modify(val, &a, &b, c);   // a is passed by value, b is passed by reference by creating a pointer,
                        // c is a pointer passed by value
    // b and x are changed
    return(0);
}
*/
