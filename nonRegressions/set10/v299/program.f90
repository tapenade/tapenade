! from ralf.seidler@uni-jena.de

      subroutine enzyme(x,fvec,n,m)
         double precision, intent(in):: x(n)
         double precision, intent(out):: fvec(m)

         integer:: i
         double precision:: temp1, temp2
         double precision:: v(11), y(11)

         data v/4.0d0, 2.0d0, 1.0d0, 5.0d-1, 2.5d-1, 1.67d-1, 1.25d-1, &
              1.0d-1, 8.33d-2, 7.14d-2, 6.25d-2/
         data y/1.957d-1, 1.947d-1, 1.735d-1, 1.6d-1, 8.44d-2, 6.27d-2,&
              4.56d-2, 3.42d-2, 3.23d-2, 2.35d-2, 2.46d-2/

          write(*,*) "in Enzyme x=",x
          write(*,*) "n=",n,"m=",m

         do 10 i = 1, m
           temp1 = v(i)*(v(i)+x(2))
           temp2 = v(i)*(v(i)+x(3)) + x(4)
           fvec(i) = y(i) - x(1)*temp1/temp2
         10 continue

      end


