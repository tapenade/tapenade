! Bug TBR: PUSH/POP(i) were missing
  SUBROUTINE ILU_CR(n, nz_num, ia, ja, a, ua, l)
    IMPLICIT NONE
    INTEGER(kind=4) :: n
    INTEGER(kind=4) :: nz_num
    REAL(kind=8) :: a(nz_num)
    INTEGER(kind=4) :: i
    INTEGER(kind=4) :: ia(n+1)
    INTEGER(kind=4) :: iw(n)
    INTEGER(kind=4) :: j
    INTEGER(kind=4) :: ja(nz_num)
    INTEGER(kind=4) :: jj
    INTEGER(kind=4) :: jrow
    INTEGER(kind=4) :: jw
    INTEGER(kind=4) :: k
    REAL(kind=8) :: l(nz_num)
    REAL(kind=8) :: tl
    INTEGER(kind=4) :: ua(n)
!
!  Copy A.
    l(1:nz_num) = a(1:nz_num)
    DO i=1,n
!
!  IW points to the nonzero entries in row I.
!
      iw(1:n) = -1
      DO k=ia(i),ia(i+1)-1
        iw(ja(k)) = k
      END DO
      DO j=ia(i),ia(i+1)-1
        jrow = ja(j)
        IF (i .LE. jrow) THEN
          EXIT
        ELSE
          tl = l(j)*l(ua(jrow))
          l(j) = tl
          DO jj=ua(jrow)+1,ia(jrow+1)-1
            jw = iw(ja(jj))
            IF (jw .NE. -1) l(jw) = l(jw) - tl*l(jj)
          END DO
        END IF
      END DO
      ua(i) = j
      IF (jrow .NE. i) THEN
        GOTO 100
      ELSE IF (l(j) .EQ. 0.0d+00) THEN
        GOTO 110
      ELSE
        l(j) = 1.0d+00/l(j)
      END IF
    END DO
    l(ua(1:n)) = 1.0d+00/l(ua(1:n))
    RETURN
 100 WRITE(*, '(a)') '1'
    STOP
 110 WRITE(*, '(a)') '2'
    STOP
  END SUBROUTINE ILU_CR
