!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (master) - 24 May 2019 16:07
!
PROGRAM MAIN
  IMPLICIT NONE
  REAL, DIMENSION(5) :: x
  REAL, DIMENSION(10) :: y1, y2
  REAL, DIMENSION(15) :: z1
  REAL, DIMENSION(10, 15) :: z2
  INTEGER :: i
  INTEGER :: j
  INTRINSIC SUM
  DO i=1,5
    x(i) = 10.0 + i
  END DO
  DO i=1,10
    y1(i) = 10.0 + i
    y2(i) = 20.0 + i
  END DO
  DO i=1,15
    z1(i) = 10.0 + i
    DO j=1,10
      z2(j, i) = i + j + 1.5
    END DO
  END DO
  CALL FOO(x, y1, y2, z1, z2)
  PRINT*, 'Sum of all results:', SUM(x), SUM(y1 + y2), SUM(z1), SUM(z2)
END PROGRAM MAIN

! Plenty of tests on diff of array operations
! with SUMs, SPREADs, MASKS/WHERE, DIMs...
SUBROUTINE FOO(x, y1, y2, z1, z2)
  IMPLICIT NONE
  REAL, DIMENSION(5) :: x
  REAL, DIMENSION(10) :: y1, y2
  REAL, DIMENSION(15) :: z1
  REAL, DIMENSION(10, 15) :: z2
  REAL :: a, b, c
  INTRINSIC SUM
  a = x(1)
  b = x(2)
  c = x(3)
  x(:) = SUM(x(:))
  y1(:) = y2(:)*a*z1(1:10)*SUM(b*z1*SUM(x(:)*c))
  WHERE (y2(:) .GT. 10.0) 
    y2(:) = SUM(y1(:), mask=y1(:).GT.5.0)
    y2(:) = y1(:)*b*SUM(z1)*SUM(c*y1(:)*SUM(x(:)*x(:), mask=x(:).GT.12)&
&     , mask=y1(:).GT.5.0)
  END WHERE
  z1(:) = SUM(z1(:), mask=z1(:).GT.15.0)
  x(:) = x(:)**2 + SUM(x(:))
  z1(:) = z1(:)**2 + SUM(z1(:), mask=z1(:).GT.1.1)
END SUBROUTINE FOO

