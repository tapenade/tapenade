! Plenty of tests on diff of array operations
! with SUMs, SPREADs, MASKS/WHERE, DIMs...
subroutine foo(X,Y1,Y2,Z1,Z2)
  real, dimension(5) :: X
  real, dimension(10) :: Y1,Y2
  real, dimension(15) :: Z1
  real, dimension(10,15) :: Z2
  real a,b,c
  a = X(1)
  b = X(2)
  c = X(3)
  X(:) = SUM(X(:))
  Y1(:) = Y2(:)*a*Z1(1:10)*SUM(b*Z1*SUM(X(:)*c))
  WHERE (Y2(:).GT.10.0)
     Y2(:) = SUM(Y1(:), MASK=Y1(:).GT.5.0)
  END WHERE
  WHERE (Y2(:).GT.10.0)
     Y2(:) = Y1(:)*b*SUM(Z1)*SUM(c*Y1(:)*SUM(X(:)*X(:),MASK=X(:).gt.12), MASK=Y1(:).GT.5.0)
  END WHERE
  Z1(:) = SUM(Z1(:), MASK=Z1(:).gt.15.0)
  X(:) = X(:)**2 + SUM(X(:))
  Z1(:) = Z1(:)**2 + SUM(Z1(:), MASK=Z1(:).GT.1.1)
end subroutine foo

program main
  real, dimension(5) :: X
  real, dimension(10) :: Y1,Y2
  real, dimension(15) :: Z1
  real, dimension(10,15) :: Z2
  integer i
  do i=1,5
     X(i) = 10.0+i
  enddo
  do i=1,10
     Y1(i) = 10.0+i
     Y2(i) = 20.0+i
  enddo
  do i=1,15
     Z1(i) = 10.0+i
     do j=1,10
        Z2(j,i) = i+j+1.5
     enddo
  enddo
  call foo(X,Y1,Y2,Z1,Z2)
  print *,'Sum of all results:',SUM(X),SUM(Y1+Y2),SUM(Z1),SUM(Z2)
end program main
