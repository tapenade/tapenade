C Problem (?) found by Mladen Banovic (Rolls-Royce) on Nov 2018:
C "SAVE" variables of the head routine can be misinterpreted.
C They are indeed a special case of formal parameter.
C If the user is not aware of that, they might forget to declare
C them as (in)dependents, and this of course changes the result
C (extra or missing diff initializations of DELTAM, DELTAP, causing
C  problems if the diff head routine is called several times)
C
C This is indeed an user interface problem,
C not really a bug, still we should send a warning!
      SUBROUTINE DFSPVN (T, JHIGH, INDEX, X, ILEFT, VNIKX)
c
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION T(*),VNIKX(*)
      DIMENSION DELTAM(20),DELTAP(20)
      SAVE J, DELTAM, DELTAP
      DATA J/1/,(DELTAM(I),I=1,20),(DELTAP(I),I=1,20)/40*0.0D0/
c
      GO TO (10,20),INDEX
 10   J = 1
      VNIKX(1) = 1.D0
      IF (J .GE. JHIGH)  GO TO 99
c
 20   IPJ = ILEFT+J
      DELTAP(J) = T(IPJ) - X
      IMJP1 = ILEFT-J+1
      DELTAM(J) = X - T(IMJP1)
      VMPREV = 0.D0
      JP1 = J+1
      DO 26 L=1,J
         JP1ML = JP1-L
         VM = VNIKX(L)/(DELTAP(L) + DELTAM(JP1ML))
         VNIKX(L) = VM*DELTAP(L) + VMPREV
         VMPREV = VM*DELTAM(JP1ML)
 26   CONTINUE
      VNIKX(JP1) = VMPREV
      J = JP1
      IF (J .LT. JHIGH) GO TO 20
c
 99   RETURN
      END
