      real function f(t, u)
      real t, u ,v
      external mul
      call mul(t,u,v)
      print*, 'mul ', t, ' ', u, ' -> ', v
      f = t * u * v
      return
      end

       program test
       implicit none
       real a, b, r, f
       a=9.0
       b=2
       print*, 'a ', a, ' b ', b
       r = f(a,b)
       print*, 'r ', r
       end
