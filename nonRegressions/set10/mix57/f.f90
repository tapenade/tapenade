! metcalf page 252
! Fortran arrays do not interoperate directly with C
! -> use allocatable array

module m
use iso_c_binding
contains
subroutine top(x,y,z)
implicit none
real, DIMENSION(:), allocatable, target :: x,y,z
interface
    subroutine foo(x, y, z) bind(C)
       use iso_c_binding
       type(c_ptr) :: x
       type(c_ptr) :: y
       type(c_ptr) :: z
    end subroutine foo
end interface

call foo(c_loc(x),c_loc(y),c_loc(z))
end subroutine top

subroutine bar(aloc,bloc) bind(C)
implicit none
type(c_ptr) :: aloc, bloc
real, DIMENSION(:), pointer :: a, b

call c_f_pointer(aloc, a, [2])
call c_f_pointer(bloc, b, [2])
print*, 'bar::  a b ', a(1), a(2), b(1), b(2)
b = a*b
print*, 'bar->  a b ', a(1), a(2), b(1), b(2)
end 
end module m

program test
use m
implicit none
real, DIMENSION(:), allocatable, target :: x,y,z
allocate(x(2))
allocate(y(2))
allocate(z(2))
x(1) = 2
x(2) = 4
y(1) = 1
y(2) = 1
z(1) = 3
z(2) = 5
print*, 'F :: x y z', x(1), x(2), y(1), y(2), z(1), z(2)
call top(x,y,z)
print*, 'F -> x y z', x(1), x(2), y(1), y(2), z(1), z(2)
end program test
