#include <stdio.h>

extern void bar(float**, float**);

void foo(float** x, float** y, float** z) {
  printf("C foo y :: %f %f  %f %f %f %f \n", (*x)[0], (*x)[1], (*y)[0], (*y)[1], (*z)[0], (*z)[1]);
  bar(x, y);
  bar(z, y);
  printf("C foo y -> %f %f  %f %f %f %f \n", (*x)[0], (*x)[1], (*y)[0], (*y)[1], (*z)[0], (*z)[1]);
}
