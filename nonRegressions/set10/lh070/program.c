/* Try to reproduce a bug of ordering of #include's when #include's are repeated */

#include <stdio.h>
#include "myinclude.h"
#include <stdio.h>

float foo(float x) {
  return x * x ;
}
