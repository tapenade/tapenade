!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_llhTests) -  1 Jul 2021 14:43
!
! metcalf page 253
! Fortran interoperability of derived types
MODULE RECTTYPE_DIFF
  USE ISO_C_BINDING
  IMPLICIT NONE
  TYPE, BIND(c) :: MYFTYPE
      REAL(c_float) :: w, h
  END TYPE MYFTYPE
END MODULE RECTTYPE_DIFF

MODULE M_DIFF
  USE ISO_C_BINDING
  USE RECTTYPE_DIFF
  IMPLICIT NONE

CONTAINS
!  Differentiation of top in reverse (adjoint) mode (with options context):
!   gradient     of useful results: x.w x.h y.w y.h z.w z.h
!   with respect to varying inputs: x.w x.h y.w y.h z.w z.h
!   RW status of diff variables: x.w:incr x.h:incr y.w:in-out y.h:in-out
!                z.w:incr z.h:incr
  SUBROUTINE TOP_B(x, xb, y, yb, z, zb)
    IMPLICIT NONE
    TYPE(MYFTYPE) :: x, y, z
    TYPE(MYFTYPE) :: xb, yb, zb
    INTERFACE 
        SUBROUTINE FOO_NODIFF(x, y, z) BIND(c)
          USE ISO_C_BINDING
          USE RECTTYPE_DIFF
          TYPE(MYFTYPE) :: x, y, z
        END SUBROUTINE FOO_NODIFF
    END INTERFACE

    INTERFACE 
        SUBROUTINE FOO_B(x, xb, y, yb, z, zb) BIND(c)
          USE ISO_C_BINDING
          USE RECTTYPE_DIFF
          TYPE(MYFTYPE) :: x, y, z
          TYPE(MYFTYPE) :: xb, yb, zb
        END SUBROUTINE FOO_B
    END INTERFACE

    CALL FOO_B(x, xb, y, yb, z, zb)
  END SUBROUTINE TOP_B

  SUBROUTINE TOP(x, y, z)
    IMPLICIT NONE
    TYPE(MYFTYPE) :: x, y, z
    INTERFACE 
        SUBROUTINE FOO_NODIFF(x, y, z) BIND(c)
          USE ISO_C_BINDING
          USE RECTTYPE_DIFF
          TYPE(MYFTYPE) :: x, y, z
        END SUBROUTINE FOO_NODIFF
    END INTERFACE

    CALL FOO_NODIFF(x, y, z)
  END SUBROUTINE TOP

  SUBROUTINE FOO0(x, y, z)
    IMPLICIT NONE
    TYPE(MYFTYPE), INTENT(IN) :: x
    TYPE(MYFTYPE) :: y
    TYPE(MYFTYPE), INTENT(IN) :: z
    CALL BAR(x, y)
    CALL BAR(z, y)
  END SUBROUTINE FOO0

!  Differentiation of bar in reverse (adjoint) mode (with options context):
!   gradient     of useful results: a.w a.h b.w b.h
!   with respect to varying inputs: a.w a.h b.w b.h
  SUBROUTINE BAR_B(a, ab, b, bb) BIND(c)
    IMPLICIT NONE
    TYPE(MYFTYPE) :: a, b
    TYPE(MYFTYPE) :: ab, bb
    ab%h = ab%h + b%h*bb%h
    bb%h = a%h*bb%h
    ab%w = ab%w + b%w*bb%w
    bb%w = a%w*bb%w
  END SUBROUTINE BAR_B

  SUBROUTINE BAR(a, b) BIND(c)
    IMPLICIT NONE
    TYPE(MYFTYPE) :: a, b
    PRINT*, 'bar::  a b ', a, b
    b%w = a%w*b%w
    b%h = a%h*b%h
    PRINT*, 'bar->  a b ', a, b
  END SUBROUTINE BAR

END MODULE M_DIFF

!  Differentiation of test as a context to call adjoint code (with options context):
PROGRAM TEST_B
  USE M_DIFF
  IMPLICIT NONE
  TYPE(MYFTYPE) :: x, y, z
  TYPE(MYFTYPE) :: xb, yb, zb
  x%w = 2
  x%h = 4
  y%w = 1
  y%h = 1
  z%w = 3
  z%h = 5
  CALL ADCONTEXTADJ_INIT(0.87_8)
  CALL ADCONTEXTADJ_INITREAL4('x'//CHAR(0), x%w, xb%w)
  CALL ADCONTEXTADJ_INITREAL4('x'//CHAR(0), x%h, xb%h)
  CALL ADCONTEXTADJ_INITREAL4('y'//CHAR(0), y%w, yb%w)
  CALL ADCONTEXTADJ_INITREAL4('y'//CHAR(0), y%h, yb%h)
  CALL ADCONTEXTADJ_INITREAL4('z'//CHAR(0), z%w, zb%w)
  CALL ADCONTEXTADJ_INITREAL4('z'//CHAR(0), z%h, zb%h)
  CALL TOP_B(x, xb, y, yb, z, zb)
  CALL ADCONTEXTADJ_STARTCONCLUDE()
  CALL ADCONTEXTADJ_CONCLUDEREAL4('x'//CHAR(0), x%w, xb%w)
  CALL ADCONTEXTADJ_CONCLUDEREAL4('x'//CHAR(0), x%h, xb%h)
  CALL ADCONTEXTADJ_CONCLUDEREAL4('y'//CHAR(0), y%w, yb%w)
  CALL ADCONTEXTADJ_CONCLUDEREAL4('y'//CHAR(0), y%h, yb%h)
  CALL ADCONTEXTADJ_CONCLUDEREAL4('z'//CHAR(0), z%w, zb%w)
  CALL ADCONTEXTADJ_CONCLUDEREAL4('z'//CHAR(0), z%h, zb%h)
  CALL ADCONTEXTADJ_CONCLUDE()
  PRINT*, 'x z', x%w, x%h, z%w, z%h
  PRINT*, 'y  ', y%w, y%h
END PROGRAM TEST_B

SUBROUTINE TEST_NODIFF()
  USE M_DIFF
  IMPLICIT NONE
  TYPE(MYFTYPE) :: x, y, z
  x%w = 2
  x%h = 4
  y%w = 1
  y%h = 1
  z%w = 3
  z%h = 5
  CALL TOP(x, y, z)
  PRINT*, 'x z', x%w, x%h, z%w, z%h
  PRINT*, 'y  ', y%w, y%h
END SUBROUTINE TEST_NODIFF

