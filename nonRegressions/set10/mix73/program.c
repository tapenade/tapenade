#include <stdio.h>

typedef struct {
  float w1, h1;
} myctype;

extern void bar(myctype *, myctype *);

void foo(myctype *x, myctype *y, myctype *z) {
  bar(x, y);
  bar(z, y);
}
