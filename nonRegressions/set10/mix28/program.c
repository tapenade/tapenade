// from 77/lh02
// pour tester: extern C sub1 != Fortran sub1

extern void sub1();

void top(float* x, float* y, float* z, float* a, float* b, float* c) {
    if (*x > 0.0) {
        *y = 1.7;
        sub1(x, y, z); // n'appelle pas sub1 Fortran
        *z = 5.1 * *z;
        *x = *y + *z;
    } else
        *y = 3.3*pow(*x, 2.0);
    *a = -2.9;
    sub1(a, b, c);
}
