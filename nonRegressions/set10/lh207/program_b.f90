!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.16 (feature_nopubliczones) - 17 Jan 2023 11:23
!
! Bug report fourni par Onera.
! Le bug etait un nullPointerException tout bete mais je pense qu'il
!  reste d'autres choses a verifier sur ce code (e.g. plein d'II-LOOPs).
MODULE MOD_TYPES_DIFF
  IMPLICIT NONE
  PRIVATE 
  INTEGER, PARAMETER :: rsp=SELECTED_REAL_KIND(6, 37)
  INTEGER, PARAMETER :: rdp=SELECTED_REAL_KIND(15, 307)
  REAL(rsp) :: arsp
  REAL(rdp) :: ardp
  EXTERNAL SIZEOF
  REAL :: SIZEOF
  INTEGER, PARAMETER :: vlsp=32/SIZEOF(arsp)
  INTEGER, PARAMETER :: vldp=32/SIZEOF(ardp)
  INTEGER, PARAMETER :: vl=vldp
  INTEGER, PARAMETER :: mxd=SELECTED_REAL_KIND(15, 307)
  REAL(rdp), PARAMETER :: twothd=2.d+0/3.d+0
  PUBLIC :: type_aghora_phypar
  PUBLIC :: type_mesh_elt
  PUBLIC :: type_mesh
  PUBLIC :: type_phypar_eos
  PUBLIC :: rsp, rdp, mxd, twothd, vl
! Specific heat at constant volume
! Turbulent conductivity/viscosity ratio
! equidistant grid in (density,energy)  plane
  TYPE TYPE_PHYPAR_EOS
      REAL :: cv
      REAL :: coefftt
      LOGICAL :: grid_re
  END TYPE TYPE_PHYPAR_EOS
! Definition of the equation of state
! Use logarithmic limiter in eta function of the SA model
  TYPE TYPE_AGHORA_PHYPAR
      TYPE(TYPE_PHYPAR_EOS) :: eos
      LOGICAL :: isloglimetasa
  END TYPE TYPE_AGHORA_PHYPAR
! Dynamic viscosity coefficient
! Thermic conductivity
! Vector of DOF at time (n)
! Explicit residuals
! Global lifting operator
! basis|basisx|basisy|basisz, large array
! Dimension of the basis
! Number of Gauss points 
! DOFs of parameter in Data Assimilation
! Jacobian determinent * quadrature weight
  TYPE TYPE_MESH_ELT
      REAL :: mu
      REAL :: k_th
      REAL, DIMENSION(:, :), POINTER :: ro => NULL()
      REAL, DIMENSION(:, :), POINTER :: dro => NULL()
      REAL, DIMENSION(:, :), POINTER :: gloxyz => NULL()
      REAL, DIMENSION(:, :), POINTER :: mbasisxyz => NULL()
      INTEGER :: np
      INTEGER :: ng
      REAL, DIMENSION(:), POINTER :: paramda => NULL()
      REAL, DIMENSION(:), POINTER :: detjom => NULL()
  END TYPE TYPE_MESH_ELT
  TYPE TYPE_MESH_ELT_DIFF
      REAL, DIMENSION(:, :), POINTER :: dro => NULL()
      REAL, DIMENSION(:), POINTER :: paramda => NULL()
  END TYPE TYPE_MESH_ELT_DIFF
! List of elements
! ro | basisx | basisy |basisz 
! Number of elements
  TYPE TYPE_MESH
      TYPE(TYPE_MESH_ELT), DIMENSION(:), POINTER :: elt => NULL()
      REAL, DIMENSION(:, :), POINTER :: rotmp => NULL()
      INTEGER :: nelt
  END TYPE TYPE_MESH
  TYPE TYPE_MESH_DIFF
      TYPE(TYPE_MESH_ELT_DIFF), DIMENSION(:), POINTER :: elt => NULL()
      REAL, DIMENSION(:, :), POINTER :: rotmp => NULL()
  END TYPE TYPE_MESH_DIFF
END MODULE MOD_TYPES_DIFF

MODULE MOD_CALC_VOL_DIFF
  USE MOD_TYPES_DIFF, ONLY : type_aghora_phypar, type_mesh_elt, &
& type_mesh_elt_diff, type_mesh, type_mesh_diff, meshnelt
  IMPLICIT NONE
  PRIVATE 
  REAL, PARAMETER :: twothd=2.d0/3.d0
  TYPE(TYPE_MESH_ELT), POINTER, SAVE :: elt => NULL()
  TYPE(TYPE_MESH_ELT_DIFF), POINTER, SAVE :: eltb => NULL()
  REAL, DIMENSION(:, :), POINTER, SAVE :: gloxyz => NULL()
  REAL, DIMENSION(:, :), POINTER, SAVE :: ro => NULL()
  REAL, DIMENSION(:, :), POINTER, SAVE :: dro => NULL()
  REAL, DIMENSION(:, :), POINTER, SAVE :: drob => NULL()
  REAL, DIMENSION(:, :), POINTER, SAVE :: mbasisxyz => NULL()
  PUBLIC :: intvol_base
  PUBLIC :: intvol_base_b

CONTAINS
!  Differentiation of intvol_base in reverse (adjoint) mode (with options fixinterface):
!   gradient     of useful results: *(*(mesh.elt).dro)
!   with respect to varying inputs: *(*(mesh.elt).paramda)
!   RW status of diff variables: mesh.elt:(loc) *(mesh.elt).mu:(loc)
!                *(mesh.elt).k_th:(loc) *(mesh.elt).dro:(loc) *(*(mesh.elt).dro):in-killed
!                *(mesh.elt).paramda:(loc) *(*(mesh.elt).paramda):out
!   Plus diff mem management of: mesh.elt:in *(mesh.elt).dro:in
!                *(mesh.elt).paramda:in
  SUBROUTINE INTVOL_BASE_B(phypar, mesh, meshb)
    USE DIFFSIZES
!  Hint: ISIZE1OFDrfmesh_elt should be the size of dimension 1 of array *mesh%elt
    IMPLICIT NONE
    TYPE(TYPE_AGHORA_PHYPAR), INTENT(IN) :: phypar
    TYPE(TYPE_MESH), INTENT(INOUT) :: mesh
    TYPE(TYPE_MESH_DIFF), INTENT(INOUT) :: meshb
!Local variables
    INTEGER :: ij, n, neq, nb, ng1d, ib, ind
    INTEGER :: cpt, ng2d, meshnelt
    INTEGER, PARAMETER :: phyneq=5
    REAL, DIMENSION(7) :: r, rx, ry, rz
    REAL :: vp(7), vpxyz(3, 7), fv(3, 7)
    REAL :: fvb(3, 7)
    REAL :: cc, iro, irans, fs(7), mutda
    REAL :: mutdab
    REAL :: nrj, id2
    INTEGER :: ii1
    DO ii1=1,ISIZE1OFDrfmesh_elt
      meshb%elt(ii1)%paramda = 0.0
    END DO
    fvb = 0.0
!$BWD-OF II-LOOP 
    DO ij=1,mesh%nelt
      eltb => meshb%elt(ij)
      elt => mesh%elt(ij)
      ro => elt%ro
      drob => eltb%dro
      mbasisxyz => elt%mbasisxyz
      gloxyz => elt%gloxyz
!$BWD-OF II-LOOP 
      DO n=1,elt%ng
        r = 0.d0
!$FWD-OF II-LOOP 
        DO neq=1,5
          DO nb=1,elt%np
            r(neq) = r(neq) + elt%mbasisxyz(nb, n)*ro(nb, neq)
          END DO
        END DO
        iro = 1.d0/r(1)
        vp(2) = r(2)*iro
        vp(3) = r(3)*iro
        vp(4) = r(4)*iro
        vp(5) = r(5)*iro
        rx = 0.d0
        ry = 0.d0
        rz = 0.d0
!$FWD-OF II-LOOP 
        DO neq=1,5
          DO nb=1,elt%np
            rx(neq) = rx(neq) + elt%mbasisxyz(nb, elt%ng+n)*ro(nb, neq) &
&             + elt%mbasisxyz(nb, n)*gloxyz(nb, neq)
            ry(neq) = ry(neq) + elt%mbasisxyz(nb, 2*elt%ng+n)*ro(nb, neq&
&             ) + elt%mbasisxyz(nb, n)*gloxyz(nb, phyneq+neq)
            rz(neq) = rz(neq) + elt%mbasisxyz(nb, 3*elt%ng+n)*ro(nb, neq&
&             ) + elt%mbasisxyz(nb, n)*gloxyz(nb, 2*phyneq+neq)
          END DO
        END DO
        vpxyz(1, 2) = rx(2) - vp(2)*rx(1)
        vpxyz(2, 2) = ry(2) - vp(2)*ry(1)
        vpxyz(3, 2) = rz(2) - vp(2)*rz(1)
        vpxyz(1, 3) = rx(3) - vp(3)*rx(1)
        vpxyz(2, 3) = ry(3) - vp(3)*ry(1)
        vpxyz(3, 3) = rz(3) - vp(3)*rz(1)
        vpxyz(1, 4) = rx(4) - vp(4)*rx(1)
        vpxyz(2, 4) = ry(4) - vp(4)*ry(1)
        vpxyz(3, 4) = rz(4) - vp(4)*rz(1)
        vpxyz(1, 5) = rx(5) - vp(5)*rx(1)
        vpxyz(2, 5) = ry(5) - vp(5)*ry(1)
        vpxyz(3, 5) = rz(5) - vp(5)*rz(1)
!$BWD-OF II-LOOP 
        DO neq=2,5
!$BWD-OF II-LOOP 
          DO nb=1,elt%np
            fvb(1, neq) = fvb(1, neq) + mbasisxyz(nb, elt%ng+n)*drob(nb&
&             , neq)
            fvb(2, neq) = fvb(2, neq) + mbasisxyz(nb, 2*elt%ng+n)*drob(&
&             nb, neq)
            fvb(3, neq) = fvb(3, neq) + mbasisxyz(nb, 3*elt%ng+n)*drob(&
&             nb, neq)
          END DO
        END DO
!$BWD-OF II-LOOP 
        DO neq=2,5
          fvb(3, neq) = elt%detjom(n)*fvb(3, neq)
          fvb(2, neq) = elt%detjom(n)*fvb(2, neq)
          fvb(1, neq) = elt%detjom(n)*fvb(1, neq)
        END DO
        CALL INTVOL_VIS_B(phypar, fv, fvb, r, vp, rx, ry, rz, vpxyz, iro&
&                   , nrj, mutda, mutdab, elt)
        mutdab = elt%mu*mutdab
        DO nb=elt%np,1,-1
          eltb%paramda(nb) = eltb%paramda(nb) + mbasisxyz(nb, n)*mutdab
        END DO
      END DO
      drob = 0.0
    END DO
  END SUBROUTINE INTVOL_BASE_B

  SUBROUTINE INTVOL_BASE(phypar, mesh)
    IMPLICIT NONE
    TYPE(TYPE_AGHORA_PHYPAR), INTENT(IN) :: phypar
    TYPE(TYPE_MESH), INTENT(INOUT) :: mesh
!Local variables
    INTEGER :: ij, n, neq, nb, ng1d, ib, ind
    INTEGER :: cpt, ng2d, meshnelt
    INTEGER, PARAMETER :: phyneq=5
    REAL, DIMENSION(7) :: r, rx, ry, rz
    REAL :: vp(7), vpxyz(3, 7), fv(3, 7)
    REAL :: cc, iro, irans, fs(7), mutda
    REAL :: nrj, id2
!$AD II-LOOP
    DO ij=1,mesh%nelt
      elt => mesh%elt(ij)
      ro => elt%ro
      dro => elt%dro
      mbasisxyz => elt%mbasisxyz
      gloxyz => elt%gloxyz
      dro = 0.d0
!$AD II-LOOP
      DO n=1,elt%ng
        r = 0.d0
!$AD II-LOOP
        DO neq=1,5
!$AD II-LOOP
          DO nb=1,elt%np
            r(neq) = r(neq) + elt%mbasisxyz(nb, n)*ro(nb, neq)
          END DO
        END DO
        iro = 1.d0/r(1)
        nrj = r(5) - 0.5d0*(r(2)*r(2)+r(3)*r(3)+r(4)*r(4))*iro
        vp(2) = r(2)*iro
        vp(3) = r(3)*iro
        vp(4) = r(4)*iro
        vp(5) = r(5)*iro
        rx = 0.d0
        ry = 0.d0
        rz = 0.d0
!$AD II-LOOP
        DO neq=1,5
!$AD II-LOOP
          DO nb=1,elt%np
            rx(neq) = rx(neq) + elt%mbasisxyz(nb, elt%ng+n)*ro(nb, neq) &
&             + elt%mbasisxyz(nb, n)*gloxyz(nb, neq)
            ry(neq) = ry(neq) + elt%mbasisxyz(nb, 2*elt%ng+n)*ro(nb, neq&
&             ) + elt%mbasisxyz(nb, n)*gloxyz(nb, phyneq+neq)
            rz(neq) = rz(neq) + elt%mbasisxyz(nb, 3*elt%ng+n)*ro(nb, neq&
&             ) + elt%mbasisxyz(nb, n)*gloxyz(nb, 2*phyneq+neq)
          END DO
        END DO
        vpxyz(1, 2) = rx(2) - vp(2)*rx(1)
        vpxyz(2, 2) = ry(2) - vp(2)*ry(1)
        vpxyz(3, 2) = rz(2) - vp(2)*rz(1)
        vpxyz(1, 3) = rx(3) - vp(3)*rx(1)
        vpxyz(2, 3) = ry(3) - vp(3)*ry(1)
        vpxyz(3, 3) = rz(3) - vp(3)*rz(1)
        vpxyz(1, 4) = rx(4) - vp(4)*rx(1)
        vpxyz(2, 4) = ry(4) - vp(4)*ry(1)
        vpxyz(3, 4) = rz(4) - vp(4)*rz(1)
        vpxyz(1, 5) = rx(5) - vp(5)*rx(1)
        vpxyz(2, 5) = ry(5) - vp(5)*ry(1)
        vpxyz(3, 5) = rz(5) - vp(5)*rz(1)
        mutda = 0.d0
        DO nb=1,elt%np
          mutda = mutda + mbasisxyz(nb, n)*elt%paramda(nb)
        END DO
        mutda = mutda*elt%mu
        CALL INTVOL_VIS(phypar, fv, r, vp, rx, ry, rz, vpxyz, iro, nrj, &
&                 mutda, elt)
!$AD II-LOOP
        DO neq=2,5
          fv(1, neq) = elt%detjom(n)*fv(1, neq)
          fv(2, neq) = elt%detjom(n)*fv(2, neq)
          fv(3, neq) = elt%detjom(n)*fv(3, neq)
        END DO
!$AD II-LOOP
        DO neq=2,5
!$AD II-LOOP
          DO nb=1,elt%np
            dro(nb, neq) = dro(nb, neq) + fv(1, neq)*mbasisxyz(nb, elt%&
&             ng+n) + fv(2, neq)*mbasisxyz(nb, 2*elt%ng+n) + fv(3, neq)*&
&             mbasisxyz(nb, 3*elt%ng+n)
          END DO
        END DO
!n =1,elt%Ng

      END DO
!ij=1,Nelt

    END DO
    NULLIFY(elt, ro, dro, mbasisxyz, gloxyz)
  END SUBROUTINE INTVOL_BASE

!  Differentiation of intvol_vis in reverse (adjoint) mode (with options fixinterface):
!   gradient     of useful results: f
!   with respect to varying inputs: f mutda
  SUBROUTINE INTVOL_VIS_B(phypar, f, fb, r, vp, rx, ry, rz, vpxyz, iro, &
&   nrj, mutda, mutdab, elt)
    IMPLICIT NONE
    TYPE(TYPE_AGHORA_PHYPAR), INTENT(IN) :: phypar
    REAL, INTENT(INOUT) :: f(3, 7), r(7)
    REAL, INTENT(INOUT) :: fb(3, 7)
    REAL, INTENT(IN) :: vp(7), rx(7), ry(7), rz(7), vpxyz(3, 7)
    REAL, INTENT(IN) :: iro, nrj, mutda
    REAL :: mutdab
    TYPE(TYPE_MESH_ELT), POINTER, INTENT(IN) :: elt
    REAL :: tau(3, 3), dtdx, dtdy, dtdz
    REAL :: mct_tot, dtdro, dtde, divu
    REAL :: mct_totb
    REAL :: dij(3, 3), nutot, coeff
    REAL :: nutotb
    dtdro = 0.
    dtde = 1./phypar%eos%cv*iro
    dtdx = (vpxyz(1, 5)-vp(2)*vpxyz(1, 2)-vp(3)*vpxyz(1, 3)-vp(4)*vpxyz(&
&     1, 4))*dtde + rx(1)*dtdro
    dtdy = (vpxyz(2, 5)-vp(2)*vpxyz(2, 2)-vp(3)*vpxyz(2, 3)-vp(4)*vpxyz(&
&     2, 4))*dtde + ry(1)*dtdro
    dtdz = (vpxyz(3, 5)-vp(2)*vpxyz(3, 2)-vp(3)*vpxyz(3, 3)-vp(4)*vpxyz(&
&     3, 4))*dtde + rz(1)*dtdro
    divu = -(twothd*(vpxyz(1, 2)+vpxyz(2, 3)+vpxyz(3, 4)))
    tau(1, 1) = divu + 2.d0*vpxyz(1, 2)
    tau(1, 2) = vpxyz(2, 2) + vpxyz(1, 3)
    tau(2, 2) = divu + 2.d0*vpxyz(2, 3)
    tau(1, 3) = vpxyz(3, 2) + vpxyz(1, 4)
    tau(2, 3) = vpxyz(3, 3) + vpxyz(2, 4)
    tau(3, 3) = divu + 2.d0*vpxyz(3, 4)
    fb(1, 4) = fb(1, 4) + vp(2)*fb(3, 5)
    fb(2, 4) = fb(2, 4) + vp(3)*fb(3, 5)
    fb(3, 4) = fb(3, 4) + vp(4)*fb(3, 5)
    mct_totb = dtdz*fb(3, 5) + dtdy*fb(2, 5) + dtdx*fb(1, 5)
    fb(3, 5) = 0.0
    fb(1, 3) = fb(1, 3) + vp(2)*fb(2, 5)
    fb(2, 3) = fb(2, 3) + vp(3)*fb(2, 5)
    fb(3, 3) = fb(3, 3) + vp(4)*fb(2, 5) + fb(2, 4)
    fb(2, 5) = 0.0
    fb(1, 2) = fb(1, 2) + vp(2)*fb(1, 5)
    fb(2, 2) = fb(2, 2) + vp(3)*fb(1, 5) + fb(1, 3)
    fb(3, 2) = fb(3, 2) + vp(4)*fb(1, 5) + fb(1, 4)
    fb(1, 5) = 0.0
    nutotb = tau(3, 3)*fb(3, 4) + tau(2, 3)*fb(3, 3) + tau(2, 2)*fb(2, 3&
&     ) + tau(1, 3)*fb(3, 2) + tau(1, 2)*fb(2, 2) + tau(1, 1)*fb(1, 2)
    fb(3, 4) = 0.0
    fb(2, 4) = 0.0
    fb(1, 4) = 0.0
    fb(3, 3) = 0.0
    fb(2, 3) = 0.0
    fb(1, 3) = 0.0
    fb(3, 2) = 0.0
    fb(2, 2) = 0.0
    fb(1, 2) = 0.0
    mutdab = phypar%eos%coefftt*mct_totb + iro*nutotb
  END SUBROUTINE INTVOL_VIS_B

  SUBROUTINE INTVOL_VIS(phypar, f, r, vp, rx, ry, rz, vpxyz, iro, nrj, &
&   mutda, elt)
    IMPLICIT NONE
    TYPE(TYPE_AGHORA_PHYPAR), INTENT(IN) :: phypar
    REAL, INTENT(INOUT) :: f(3, 7), r(7)
    REAL, INTENT(IN) :: vp(7), rx(7), ry(7), rz(7), vpxyz(3, 7)
    REAL, INTENT(IN) :: iro, nrj, mutda
    TYPE(TYPE_MESH_ELT), POINTER, INTENT(IN) :: elt
    REAL :: tau(3, 3), dtdx, dtdy, dtdz
    REAL :: mct_tot, dtdro, dtde, divu
    REAL :: dij(3, 3), nutot, coeff
    dtdro = 0.
    dtde = 1./phypar%eos%cv*iro
    dtdx = (vpxyz(1, 5)-vp(2)*vpxyz(1, 2)-vp(3)*vpxyz(1, 3)-vp(4)*vpxyz(&
&     1, 4))*dtde + rx(1)*dtdro
    dtdy = (vpxyz(2, 5)-vp(2)*vpxyz(2, 2)-vp(3)*vpxyz(2, 3)-vp(4)*vpxyz(&
&     2, 4))*dtde + ry(1)*dtdro
    dtdz = (vpxyz(3, 5)-vp(2)*vpxyz(3, 2)-vp(3)*vpxyz(3, 3)-vp(4)*vpxyz(&
&     3, 4))*dtde + rz(1)*dtdro
    divu = -(twothd*(vpxyz(1, 2)+vpxyz(2, 3)+vpxyz(3, 4)))
    tau(1, 1) = divu + 2.d0*vpxyz(1, 2)
    tau(1, 2) = vpxyz(2, 2) + vpxyz(1, 3)
    tau(2, 2) = divu + 2.d0*vpxyz(2, 3)
    tau(1, 3) = vpxyz(3, 2) + vpxyz(1, 4)
    tau(2, 3) = vpxyz(3, 3) + vpxyz(2, 4)
    tau(3, 3) = divu + 2.d0*vpxyz(3, 4)
    nutot = elt%mu*iro + mutda*iro
    mct_tot = elt%k_th + mutda*phypar%eos%coefftt
    f(1, 2) = nutot*tau(1, 1)
    f(2, 2) = nutot*tau(1, 2)
    f(3, 2) = nutot*tau(1, 3)
    f(1, 3) = f(2, 2)
    f(2, 3) = nutot*tau(2, 2)
    f(3, 3) = nutot*tau(2, 3)
    f(1, 4) = f(3, 2)
    f(2, 4) = f(3, 3)
    f(3, 4) = nutot*tau(3, 3)
    f(1, 5) = f(1, 2)*vp(2) + f(2, 2)*vp(3) + f(3, 2)*vp(4) + mct_tot*&
&     dtdx
    f(2, 5) = f(1, 3)*vp(2) + f(2, 3)*vp(3) + f(3, 3)*vp(4) + mct_tot*&
&     dtdy
    f(3, 5) = f(1, 4)*vp(2) + f(2, 4)*vp(3) + f(3, 4)*vp(4) + mct_tot*&
&     dtdz
  END SUBROUTINE INTVOL_VIS

END MODULE MOD_CALC_VOL_DIFF

