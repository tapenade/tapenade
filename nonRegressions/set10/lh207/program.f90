! Bug report fourni par Onera.
! Le bug etait un nullPointerException tout bete mais je pense qu'il
!  reste d'autres choses a verifier sur ce code (e.g. plein d'II-LOOPs).
MODULE mod_types
  IMPLICIT NONE ; PRIVATE
  INTEGER,PARAMETER   :: RSP = SELECTED_REAL_KIND(6,37)
  INTEGER,PARAMETER   :: RDP = SELECTED_REAL_KIND(15,307)
  REAL(RSP)           :: arsp
  REAL(RDP)           :: ardp
  INTEGER,PARAMETER   :: VLSP = 32 / (sizeof(arsp))
  INTEGER,PARAMETER   :: VLDP = 32 / (sizeof(ardp))
  INTEGER,PARAMETER   :: VL      = VLDP
  INTEGER,PARAMETER   :: MXD     = SELECTED_REAL_KIND(15,307)
  REAL(RDP),PARAMETER :: TWOTHD  = 2.D+0/3.D+0
  PUBLIC :: type_aghora_phypar
  PUBLIC :: type_mesh_elt
  PUBLIC :: type_mesh
  PUBLIC :: type_phypar_eos
  PUBLIC :: RSP,RDP,MXD,TWOTHD,VL

  TYPE type_phypar_eos
     REAL                            :: cv              ! Specific heat at constant volume
     REAL                            :: coeffTt         ! Turbulent conductivity/viscosity ratio
     LOGICAL                         :: grid_re         ! equidistant grid in (density,energy)  plane
  END TYPE type_phypar_eos

  TYPE type_aghora_phypar
     TYPE(type_phypar_eos) :: eos          ! Definition of the equation of state
     LOGICAL               :: isLogLimEtaSA! Use logarithmic limiter in eta function of the SA model
  END TYPE type_aghora_phypar

  TYPE type_mesh_elt
     REAL                           :: mu                  ! Dynamic viscosity coefficient
     REAL                           :: k_th                ! Thermic conductivity
     REAL,POINTER,DIMENSION(:,:)    :: ro        => NULL() ! Vector of DOF at time (n)
     REAL,POINTER,DIMENSION(:,:)    :: dro       => NULL() ! Explicit residuals
     REAL,POINTER,DIMENSION(:,:)    :: gloxyz    => NULL() ! Global lifting operator
     REAL,POINTER,DIMENSION(:,:)    :: Mbasisxyz => NULL() ! basis|basisx|basisy|basisz, large array
     INTEGER                        :: Np                  ! Dimension of the basis
     INTEGER                        :: Ng                  ! Number of Gauss points 
     REAL,POINTER,DIMENSION(:)      :: paramDA   => NULL() ! DOFs of parameter in Data Assimilation
     REAL,POINTER,DIMENSION(:)      :: detJom    => NULL() ! Jacobian determinent * quadrature weight
  END TYPE type_mesh_elt

  TYPE type_mesh
      TYPE(type_mesh_elt),POINTER,DIMENSION(:) :: elt  => NULL() ! List of elements
      REAL,POINTER,DIMENSION(:,:)              :: rotmp   => NULL() ! ro | basisx | basisy |basisz 
     INTEGER                                   :: Nelt              ! Number of elements
  END TYPE type_mesh

END MODULE mod_types

MODULE mod_calc_vol
  USE mod_types, ONLY : type_aghora_phypar, type_mesh_elt, type_mesh, meshNelt
  IMPLICIT NONE ; PRIVATE
  REAL,PARAMETER                           :: TWOTHD   = 2.D0/3.D0
  TYPE(type_mesh_elt),POINTER              :: elt       => NULL()
  REAL,POINTER,DIMENSION(:,:)   :: gloxyz    => NULL()
  REAL,POINTER,DIMENSION(:,:)   :: ro        => NULL()
  REAL,POINTER,DIMENSION(:,:)   :: dro       => NULL()
  REAL,POINTER,DIMENSION(:,:)   :: Mbasisxyz => NULL()
  PUBLIC :: intvol_base

CONTAINS

  SUBROUTINE intvol_base(phypar,mesh)
    TYPE(type_aghora_phypar),INTENT(IN)       :: phypar
    TYPE(type_mesh),INTENT(INOUT)             :: mesh
    !Local variables
    INTEGER                                   :: ij,n,neq,nb,ng1d,ib,ind
    INTEGER                                   :: cpt,ng2d,meshNelt
    INTEGER,PARAMETER                         :: phyNeq = 5
    REAL,DIMENSION(7)                         :: r,rx,ry,rz
    REAL                                      :: vp(7),vpxyz(3,7),fv(3,7)
    REAL                                      :: cc,iro,irans,fs(7),mutDA
    REAL                                      :: nrj,id2

    ! $AD II-LOOP
    DO ij=1,mesh%Nelt
       elt       => mesh%elt(ij)
       ro        => elt%ro
       dro       => elt%dro
       Mbasisxyz => elt%Mbasisxyz
       gloxyz    => elt%gloxyz
       dro = 0.D0       

       ! $AD II-LOOP
       DO n=1,elt%Ng
          r = 0.D0

          ! $AD II-LOOP
          DO neq = 1,5
             ! $AD II-LOOP
             DO nb = 1, elt%Np
                r(neq) = r(neq) + elt%Mbasisxyz(nb,n)*ro(nb,neq)
             ENDDO
          ENDDO
          iro = 1.D0/r(1)
          nrj = r(5) - 0.5D0*(r(2)*r(2)+r(3)*r(3)+r(4)*r(4))*iro
          vp(2) = r(2)*iro
          vp(3) = r(3)*iro
          vp(4) = r(4)*iro
          vp(5) = r(5)*iro
          rx = 0.D0 ; ry = 0.D0 ; rz = 0.D0

          ! $AD II-LOOP
          DO neq = 1,5
             ! $AD II-LOOP
             DO nb=1,elt%Np
                rx(neq)=rx(neq)+elt%Mbasisxyz(nb,  elt%Ng+n)*ro(nb,neq) &
                     +          elt%Mbasisxyz(nb,         n)*gloxyz(nb,         neq)
                ry(neq)=ry(neq)+elt%Mbasisxyz(nb,2*elt%Ng+n)*ro(nb,neq) &
                     +          elt%Mbasisxyz(nb,         n)*gloxyz(nb,  phyNeq+neq)
                rz(neq)=rz(neq)+elt%Mbasisxyz(nb,3*elt%Ng+n)*ro(nb,neq) &
                     +          elt%Mbasisxyz(nb,         n)*gloxyz(nb,2*phyNeq+neq)
             ENDDO
          ENDDO
          vpxyz(1,2) = rx(2) - vp(2)*rx(1)
          vpxyz(2,2) = ry(2) - vp(2)*ry(1)
          vpxyz(3,2) = rz(2) - vp(2)*rz(1)
          vpxyz(1,3) = rx(3) - vp(3)*rx(1)
          vpxyz(2,3) = ry(3) - vp(3)*ry(1)
          vpxyz(3,3) = rz(3) - vp(3)*rz(1)
          vpxyz(1,4) = rx(4) - vp(4)*rx(1)
          vpxyz(2,4) = ry(4) - vp(4)*ry(1)
          vpxyz(3,4) = rz(4) - vp(4)*rz(1)
          vpxyz(1,5) = rx(5) - vp(5)*rx(1)
          vpxyz(2,5) = ry(5) - vp(5)*ry(1)
          vpxyz(3,5) = rz(5) - vp(5)*rz(1)
          mutDA = 0.D0
          DO nb = 1,elt%Np
             mutDA =  mutDA + Mbasisxyz(nb,n)*elt%paramDA(nb)
          ENDDO
          mutDA = mutDA*elt%mu
          CALL intvol_vis(phypar,fv,r,vp,rx,ry,rz,vpxyz,iro,nrj,mutDA,elt)

          ! $AD II-LOOP
          DO neq = 2,5
             fv(1,neq) = elt%detJom(n)*fv(1,neq)
             fv(2,neq) = elt%detJom(n)*fv(2,neq)
             fv(3,neq) = elt%detJom(n)*fv(3,neq)
          ENDDO

          ! $AD II-LOOP
          DO neq = 2,5
             ! $AD II-LOOP
             DO nb=1,elt%Np
                dro(nb,neq) = dro(nb,neq) + fv(1,neq)*Mbasisxyz(nb,  elt%Ng+n) &
                     +                      fv(2,neq)*Mbasisxyz(nb,2*elt%Ng+n) &
                     +                      fv(3,neq)*Mbasisxyz(nb,3*elt%Ng+n)
             ENDDO
          ENDDO

       ENDDO  !n =1,elt%Ng

    ENDDO !ij=1,Nelt
    NULLIFY(elt,ro,dro,Mbasisxyz,gloxyz)
  END SUBROUTINE intvol_base

  SUBROUTINE intvol_vis(phypar,f,r,vp,rx,ry,rz,vpxyz,iro,nrj,mutDA,elt)
    TYPE(type_aghora_phypar),INTENT(IN)    :: phypar
    REAL,INTENT(INOUT)                     :: f(3,7),r(7)
    REAL,INTENT(IN)                        :: vp(7),rx(7),ry(7),rz(7),vpxyz(3,7)
    REAL,INTENT(IN)                        :: iro,nrj,mutDA
    TYPE(type_mesh_elt),POINTER,INTENT(IN) :: elt
    REAL                                   :: tau(3,3),dTdx,dTdy,dTdz
    REAL                                   :: mct_tot,dTdro,dTde,divU
    REAL                                   :: dij(3,3),nutot,coeff

    dTdro= 0.
    dTde = 1./phypar%eos%Cv * iro
    dTdx =(vpxyz(1,5)-vp(2)*vpxyz(1,2)-vp(3)*vpxyz(1,3)-vp(4)*vpxyz(1,4))*dTde+rx(1)*dTdro
    dTdy =(vpxyz(2,5)-vp(2)*vpxyz(2,2)-vp(3)*vpxyz(2,3)-vp(4)*vpxyz(2,4))*dTde+ry(1)*dTdro
    dTdz =(vpxyz(3,5)-vp(2)*vpxyz(3,2)-vp(3)*vpxyz(3,3)-vp(4)*vpxyz(3,4))*dTde+rz(1)*dTdro

    divU     =-TWOTHD*(vpxyz(1,2) + vpxyz(2,3) + vpxyz(3,4))
    tau(1,1) = divU +  2.D0*vpxyz(1,2) 
    tau(1,2) = vpxyz(2,2)+vpxyz(1,3)
    tau(2,2) = divU +  2.D0*vpxyz(2,3)
    tau(1,3) = vpxyz(3,2)+vpxyz(1,4)
    tau(2,3) = vpxyz(3,3)+vpxyz(2,4)
    tau(3,3) = divU +  2.D0*vpxyz(3,4)

    nutot   = elt%mu*iro + mutDA*iro
    mct_tot = elt%k_th + mutDA*phypar%eos%coeffTt
    f(1,2) = nutot*tau(1,1)
    f(2,2) = nutot*tau(1,2)
    f(3,2) = nutot*tau(1,3)

    f(1,3) = f(2,2)
    f(2,3) = nutot*tau(2,2)
    f(3,3) = nutot*tau(2,3)

    f(1,4) = f(3,2)
    f(2,4) = f(3,3)
    f(3,4) = nutot*tau(3,3)

    f(1,5) = f(1,2)*vp(2)+f(2,2)*vp(3)+f(3,2)*vp(4) + mct_tot*dTdx
    f(2,5) = f(1,3)*vp(2)+f(2,3)*vp(3)+f(3,3)*vp(4) + mct_tot*dTdy
    f(3,5) = f(1,4)*vp(2)+f(2,4)*vp(3)+f(3,4)*vp(4) + mct_tot*dTdz
  END SUBROUTINE intvol_vis

END MODULE mod_calc_vol
