#include <stdio.h>

struct rect {float w1, h1;};
extern void bar(struct rect *, struct rect *);

void foo(struct rect *x, struct rect *y, struct rect *z) {
  bar(x, y);
  bar(z, y);
}
