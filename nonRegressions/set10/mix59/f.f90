! metcalf page 252
! Fortran arrays do not interoperate directly with C
! -> use allocatable array

module recttype
type, bind(C) :: rect
    real :: w, h
end type rect
end module

module m
use iso_c_binding
use recttype
contains
subroutine top(x,y,z)
implicit none
type(rect) :: x,y,z
interface
    subroutine foo(x, y, z) bind(C)
       use iso_c_binding
       use recttype
       type(rect):: x, y, z
    end subroutine foo
end interface

call foo(x,y,z)
end subroutine top

subroutine foo0(x,y,z)
implicit none
type(rect), intent(in) :: x
type(rect) :: y
type(rect), intent(in) :: z
call bar(x,y)
call bar(z,y)
end

subroutine bar(a,b) bind(C)
implicit none
type(rect) :: a,b
print*, 'bar::  a b ', a, b
b%w = a%w * b%w
b%h = a%h * b%h
print*, 'bar->  a b ', a, b
end 
end module m

program test
use m
implicit none
type(rect) :: x,y,z
x%w = 2
x%h = 4
y%w = 1
y%h = 1
z%w = 3
z%h = 5
call top(x,y,z)
print*, 'x z', x%w, x%h, z%w, z%h
print*, 'y  ', y%w, y%h
end program test
