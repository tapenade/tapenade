/* Program ported to AMPI. Porting should be made by the user
 * in general (e.g. for pairedWith), but could be automated sometimes.
 * Should behave identical to program.c
 * MPI_Reduce with MPI_PROD */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ampi/ampi.h"

void head(double *x, double *y, double *z, int len, MPI_Comm comm) {
  int rk;
  MPI_Comm_rank(MPI_COMM_WORLD, &rk);
  int i ;
  AMPI_Reduce(x, y, len, MPI_DOUBLE, MPI_PROD, 0, comm);
  if (rk==0)
    AMPI_Reduce(MPI_IN_PLACE, z, len, MPI_DOUBLE, MPI_PROD, 0, comm);
  else
    AMPI_Reduce(z,            z, len, MPI_DOUBLE, MPI_PROD, 0, comm);
  for (i=0 ; i<len ; ++i) {
    z[i] = z[i] * y[i] ;
    x[i] = x[i] * y[i] ;
  }
}

int main(int argc, char** argv) {
  AMPI_Init_NT(0,0);
  int world_rank;
  AMPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  double *x, *y, *z ;
  int i ;
  int len = 3 ;
  x = (double*) malloc(len*sizeof(double)) ;
  y = (double*) malloc(len*sizeof(double)) ;
  z = (double*) malloc(len*sizeof(double)) ;
  for (i=0 ; i<len ; ++i) {
    x[i] = fmod(i + 1.0 + 5*world_rank, 2.31) ;
    y[i] = fmod(i + 2.0 + 5*world_rank, 2.31) ;
    z[i] = fmod(i + 3.0 + 5*world_rank, 2.31) ;
  }

  head(x,y,z,len,MPI_COMM_WORLD) ;

  double *sum = (double*) malloc(sizeof(double)) ;
  sum[0] = 0.0 ;
  for (i=0 ; i<len ; ++i) {
    sum[0] += x[i]+y[i]+z[i] ;
  }
  if (world_rank==0) {
    MPI_Reduce(MPI_IN_PLACE,sum,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD) ;
  } else {
    MPI_Reduce(sum,         sum,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD) ;
  }
  if (world_rank==0) {
    printf(__FILE__ ": process 0 (root) result  sum: %9.6f \n", sum[0]) ;
  }

  AMPI_Finalize_NT();
  return 0;
}
