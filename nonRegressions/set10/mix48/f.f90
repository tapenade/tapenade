subroutine bar(u, v) bind(c)
implicit none
real, value :: u
real :: v

u = 2 * u
v = u * u

end subroutine
