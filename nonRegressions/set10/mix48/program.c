static int nbCall = 0;

void bar(float a, float *b);

void foo(float *x, float *y) {
  bar(*x, y);
  nbCall = nbCall + 1;
}

void testNbCall() {
  nbCall = nbCall + 100;
}

void main() {
  float x, y;
  x = 2.0;
  printf("x %f\n", x);
  foo(&x, &y);
  printf("x %f y %f\n", x, y);
  foo(&x, &y);
  printf("x %f y %f\n", x, y);
  printf("nbCall %d\n", nbCall);
  testNbCall();
  printf("nbCall %d\n", nbCall);
}
