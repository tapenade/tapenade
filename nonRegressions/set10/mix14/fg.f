      double precision function f(t)
      double precision t
      f = t * t
      print*, 'call f: t ', t, ' t*t ', f
      return
      end

      double precision function g(t)
      double precision t
      g = 2 * t
      print*, 'call g: t ', t, ' 2*t ', g
      return
      end
