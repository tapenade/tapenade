#include <stdio.h>
#include <math.h>

extern double f_(), g_();

void add2 (double *x, double *y) {
    *y = f_(x) + g_(y);
    printf("add2 %f\n", *y);
}

double c_mesh_exp(double r_min, double r_max, double *a, int *N,
        double *mesh);

float top(double x, double y) {
   int N=5;
   double a, mesh[N];
   add2(&x, &y);
   return c_mesh_exp(x , y, &a, &N, mesh);
}

void main() {
   int N=5;
   double r_min, r_max, a, mesh[N];
   double result;
   r_min = 2;
   r_max = 4;
   result = top(r_min, r_max);
   printf("result %f\n", result);
}


