      double precision function f(t)
      double precision t
      double precision g
      external g
      f = t * t + g(t)
      print*, 'f: t ', t, ' t*t ', f
      return
      end

      double precision function g(t)
      double precision t
      g = 2 * t
      print*, 'g: t ', t, ' 2*t ', g
      return
      end
