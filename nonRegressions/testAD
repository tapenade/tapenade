#!/usr/bin/env bash

# Usage: testAD <directory-list-with-possible*>
#  tests regressions on every sub-directory of the arguments that contains a *.msg

if [ -z "$TAPENADE_HOME" ]
then
  if [ "$(uname)" = "Linux" ]
  then
   TAPENADE_HOME="$(dirname -- "$(readlink -f -- "$0")")"/..
  else
   REFDIR="$( cd "$( dirname "$0" )" && pwd )"
   TAPENADE_HOME="$REFDIR"/..
  fi
fi

if [ "$(uname)" = "Darwin" ]
then
    LC_CTYPE=C
    export LC_CTYPE
fi

# The language extensions recognized in source files (after the "."):
LANGEXTENSIONS="f f90 f95 f03 jl c cu c\+\+"
LANGEXTENSIONSPATTERN="(f|f90|f95|f03|jl|c|cu|c\+\+)"
# For each testable AD mode, the extension for generated files:
declare -a MODEEXTENSIONS=("p" "d" "dv" "aad" "b" "bv" "aab" "db" "diff")
MODEEXTENSIONSPATTERN="(p|d|dv|aad|b|bv|aab|db|diff)"

compareOneTestOneFile() {
    if [ -f $TESTNAME/$TESTONFILE ]
        then
        if [ -f RESULTS/$TESTNAME/$TESTONFILE ]
            then
            # Both REFERENCE and RESULT files exist: first script them according to their kind:
            if [[ $TESTONFILE == *.msg ]]
                then
                sed -f $TAPENADE_HOME/nonRegressions/scriptMsgAD $TESTNAME/$TESTONFILE > RESULTS/$TESTNAME/scriptREF
                sed -f $TAPENADE_HOME/nonRegressions/scriptMsgAD RESULTS/$TESTNAME/$TESTONFILE > RESULTS/$TESTNAME/scriptRES
            elif [[ $TESTONFILE == *.f ]] || [[ $TESTONFILE == *.f90 ]] || [[ $TESTONFILE == *.f95 ]] || [[ $TESTONFILE == *.f03 ]] || [[ $TESTONFILE == *.jl ]] || [[ $TESTONFILE == *.c ]] || [[ $TESTONFILE == *.cu ]] || [[ $TESTONFILE == *.c++ ]]
                then
                sed -f $TAPENADE_HOME/nonRegressions/scriptAD $TESTNAME/$TESTONFILE > RESULTS/$TESTNAME/scriptREF
                sed -f $TAPENADE_HOME/nonRegressions/scriptAD RESULTS/$TESTNAME/$TESTONFILE > RESULTS/$TESTNAME/scriptRES
            elif [[ $TESTONFILE == *.html ]] && [[ $TESTONFILE != tapenade.html ]] && [[ $TESTONFILE != empty.html ]] && [[ $TESTONFILE != tapenadediff.html ]] && [[ $TESTONFILE != tapenadeorig.html ]]
                then
                sed -f $TAPENADE_HOME/nonRegressions/scriptHtml $TESTNAME/$TESTONFILE > RESULTS/$TESTNAME/scriptREF
                sed -f $TAPENADE_HOME/nonRegressions/scriptHtml RESULTS/$TESTNAME/$TESTONFILE > RESULTS/$TESTNAME/scriptRES
            else
                sed -f $TAPENADE_HOME/nonRegressions/scriptIncludeAD $TESTNAME/$TESTONFILE > RESULTS/$TESTNAME/scriptREF
                sed -f $TAPENADE_HOME/nonRegressions/scriptIncludeAD RESULTS/$TESTNAME/$TESTONFILE > RESULTS/$TESTNAME/scriptRES
            fi
            # ...then compare scripted REFERENCE and RESULT. If no diff, remove new RESULT file, else emit message:
            if diff RESULTS/$TESTNAME/scriptREF RESULTS/$TESTNAME/scriptRES > /dev/null 2>&1
                then
                DUMMYOP=1
#                /bin/rm -f RESULTS/$TESTNAME/$TESTONFILE
            else
                echo ""
                echo '>>>> DIFFERENCE ON '$TESTNAME': kompare '$TESTNAME'/'$TESTONFILE' RESULTS/'$TESTNAME'/'$TESTONFILE
                diff RESULTS/$TESTNAME/scriptREF RESULTS/$TESTNAME/scriptRES
#                kompare $TESTNAME/$TESTONFILE RESULTS/$TESTNAME/$TESTONFILE &
                ALLEQUAL=0
            fi
        # Can't execute the test below if RESULT files+dir are removed when found equal to REFERENCE!
        else
            # This run has failed to produce the expected REFERENCE:
            echo ""
            echo '>>>> DIFFERENCE ON '$TESTNAME': reference '$TESTNAME'/'$TESTONFILE' not produced in RESULTS!'
            ALLEQUAL=0
        fi
    else
        if [ -f RESULTS/$TESTNAME/$TESTONFILE ]
            then
            # This run has produced an unexpected RESULT, not in the REFERENCE:
            echo ""
            echo '>>>> DIFFERENCE ON '$TESTNAME': produced RESULTS/'$TESTNAME'/'$TESTONFILE', not in reference!'
            ALLEQUAL=0
        # Very bizarre case: no REFERENCE and no RESULT. Should not happen!
        #else
        #    echo ""
        #    echo '>>>> DIFFERENCE ON '$TESTNAME' ???: ' $TESTONFILE
        #    ALLEQUAL=0
        fi
    fi
}

# Create a RESULTS directory if absent:
if [ ! -d RESULTS ]; then mkdir RESULTS; fi

# Find all tests to run on:
TESTS=`find $* -name "*.msg" -printf "%h\n" | grep -v RESULTS | sort -u`

# Will be set to zero if anything differs:
ALLEQUAL=1

for TESTNAME in $TESTS
  do
  TESTNAME=${TESTNAME#./}
  ALLEQUAL=1
  if [ -d RESULTS/$TESTNAME ]
      then
      # Collect all result files produced by differentiation, in a reasonable order:
      RESULTFILES=""
      for MODEEXTENSION in "${MODEEXTENSIONS[@]}"
        do
        # show diff on .msg file first
        for TESTONFILE in `ls $TESTNAME | grep -E "_$MODEEXTENSION\.msg$"`
          do
          RESULTFILES=$RESULTFILES" "$TESTONFILE
        done
        # show diff on differentiated language files (and not include files)
        for TESTONFILE in `ls $TESTNAME | grep -E "_$MODEEXTENSION\.$LANGEXTENSIONSPATTERN$" | grep -E -v "^incl" `
          do
          RESULTFILES=$RESULTFILES" "$TESTONFILE
        done
        # then show diff on differentiated include files
        for TESTONFILE in `ls $TESTNAME | grep -E "(^incl.*_$MODEEXTENSION\..*|.*_$MODEEXTENSION\.(inc|h))"`
          do
          RESULTFILES=$RESULTFILES" "$TESTONFILE
        done
        for TESTONFILE in `ls $TESTNAME | grep -E "^GlobalDeclarations_$MODEEXTENSION\.c$"`
          do
          RESULTFILES=$RESULTFILES" "$TESTONFILE
        done
      done
      # then show diff on HTML files
      if [ -d $TESTNAME/tapenadehtml ]
          then
          for TESTONFILE in `ls $TESTNAME/tapenadehtml | grep -E "\.html$" | grep -v tapenade.html | grep -v empty.html | grep -v tapenadediff.html | grep -v tapenadeorig.html`
            do
            RESULTFILES=$RESULTFILES" "tapenadehtml/$TESTONFILE
          done
      fi
      # Also collect files effectively produced in RESULT, when they do not appear in the reference:
      for TESTONFILE in `ls RESULTS/$TESTNAME | grep -E -v "^(trace|dump)$MODEEXTENSIONSPATTERN$" | grep -E -v "~$"`
        do
        FOUND=0
        for i in $RESULTFILES
          do
          if [[ $i == $TESTONFILE ]] ; then FOUND=1 ; fi
        done
        if (( FOUND==0 ))
            then
            RESULTFILES=$TESTONFILE" "$RESULTFILES
        fi
      done
      # same as above (collect from RESULTS), this time for HTML files
      if [ -d RESULTS/$TESTNAME/tapenadehtml ]
          then
          for TESTONFILE in `ls RESULTS/$TESTNAME/tapenadehtml | grep -E "\.html$" | grep -v tapenade.html | grep -v empty.html | grep -v tapenadediff.html | grep -v tapenadeorig.html`
            do
            FOUND=0
            for i in $RESULTFILES
              do
              if [[ $i == tapenadehtml/$TESTONFILE ]] ; then FOUND=1 ; fi
            done
            if (( FOUND==0 ))
                then
                RESULTFILES="tapenadehtml/"$TESTONFILE" "$RESULTFILES
            fi
          done
      fi
      #echo 'RESULTFILES:' $RESULTFILES
      # Run comparison on each result file:
      for TESTONFILE in $RESULTFILES
        do
        compareOneTestOneFile
      done
  fi
  # Conclude and clean up:
  if [ $ALLEQUAL -eq 1 ]
      then
      echo ': ok !'
  fi
  /bin/rm -rf RESULTS/$TESTNAME/scriptREF RESULTS/$TESTNAME/scriptRES
done
