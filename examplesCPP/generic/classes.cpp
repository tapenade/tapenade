
/*============ CLASS DECLARATION ===============*/

class Ecuador {                                         // class
    public :                                            // accessDecl
        Ecuador();                                      // varDeclaration of functionDeclarator (because this is a declaration)
        Ecuador(const char*);                           // idem
        ~Ecuador();                                     // idem
        void promote();                                 // idem
        const char* getName() const {                   // function (declaration + definition). const as modifier.
            return nom_;
        };
        Ecuador operator+(const Ecuador&);              // varDeclaration of functionDeclarator
        static int n_membres_;                          // modifiedTypeSpec : static
        const char* nom_;                               // modifiedTypeSpec : const
        int age_;
    protected :
        bool diplome_;
};

Ecuador::Ecuador(const char* nom) : nom_(nom) {         // constructor, with 2 blockStatements
    diplome_ = true;
}

void Ecuador::promote(void) {                           // function
    diplome_ = true;                                    // fieldAccess on "*this", ident:diplome_
}

Ecuador Ecuador::operator+(const Ecuador& rhs) {        // function (operator overload is just a method)
  Ecuador result = Ecuador();
  result.diplome_ = this->diplome_ or rhs.diplome_;
  return result;
}
                                                        // second example of overload:
int operator*(const Ecuador& lhs, const Ecuador& rhs) { // function (this is a real function, not a method)
  return lhs.age_ + rhs.age_;
}


/*=============== INHERITANCE ==================*/

class StagiaireEcuador : public Ecuador {               // parentClass
    public :
        StagiaireEcuador();
        StagiaireEcuador(const char*);
        StagiaireEcuador(const char*, float a);
        static void sayHello() {};                // static method declaration
        static void sayGoodBye();                 // static method declaration
    private :
        float duree_stage_;
        class WorkTopic{
            public:                               // accessDecl
                char* name_;
            private:
                typedef
                struct date_ {
                    int year;
                    int month;
                    int day;
                } date;
            public:                                // accessDecls can be redundant
                date deadline_;
        };
};

StagiaireEcuador::StagiaireEcuador(const char* nom) : Ecuador(nom){
    diplome_ = false;
}

StagiaireEcuador::StagiaireEcuador(const char* nom, float a) : Ecuador(nom) , duree_stage_(a){
    diplome_ = false;
}

//~ class AAA {
    //~ public:
        //~ void f();
        //~ void k();
//~ };

//~ class BBB
//~ {
    //~ public:
        //~ void f();
//~ };

//~ class CCC : public AAA, public BBB {                    // several parentClasses  
    //~ public:
        //~ int a;
        //~ void g() {
            //~ k();                                        // call of (*this).k()
            //~ BBB::f();                                   // scopeAccess
            //~ a = 2;                                      // field access of (*this).a
        //~ }
//~ };

//~ /*================ FRIENDSHIP ==================*/

//~ class DDD{
  //~ private:
    //~ char private_member_;
    
  //~ friend char getDDDchar(const DDD& obj);                   // friend declaration NOT SUPPORTED (YET)  
//~ };

//~ char getDDDchar(const DDD& obj) {                           // friend definition
  //~ return obj.private_member_;                               // friend functions/methods/classes/records/etc...
//~ }                                                           //          have the right to use private members.

//~ class EEE {
  //~ private:
    //~ void thisIsAMethod() const;
    //~ friend void butThisIsNot(const EEE& obj){               // friend function declaration & definition
      //~ obj.thisIsAMethod();
    //~ }
//~ };



int main()
{

/*========== CLASS INSTANCIATIONS =============*/


    Ecuador Laurent("Laurent");                             // Custom constructors   :
    Ecuador Valerie("Valerie");                             //   constructorCall(Class, ident:"other")
    //~ StagiaireEcuador Louis("Louis", 4);                     //
    StagiaireEcuador Benoit("Benoit", 2.5);                 //
    //~ StagiaireEcuador SchizoLouis(Louis.getName(), 4);       // 
    
    StagiaireEcuador SchizoBenoit(Benoit);                  // copy constructor: constructorCall(Class, ident:"copy")
    StagiaireEcuador MisterX;                               // defaultConstructor: constructorCall(Class, ident:"default")
    StagiaireEcuador MisterY = StagiaireEcuador();          // move constructor on a default constructor

/*============ MEMBERS USE ===================*/

    //~ Benoit.promote();                                       // call(fieldAccess(Benoit, promote), expressions())
    
    //~ int N = Ecuador::n_membres_;                            // access to a static attribute via scopeAccess
    //~ StagiaireEcuador::sayHello();                           // call a static method
    Ecuador team_de_choc = Laurent + Valerie;               // call (operator overloading through a method)
    //~ int average_age = (Laurent * Valerie)/N;                // call (operator overloading through a function)
    return 0;
}

