/* These are the structures already present in C ANSI.
 * The C++ classes are in a separate example file.
 * Remember that C structs are able to do most of what
 * C++ classes do, and we don't support this (yet). */

int main(){

/*============ ARRAYS ===============*/

    float ar[10];                        // arrayType with dimColon(0,10)
    float *arp[10];                      //

    int a[5] = {1,2,3};                  // arrayConsructor. Declares int[5] initalized to 1,2,3,0,0
    char str[] = "abc";                  // declares char[4] initialized to 'a','b','c','\0'
    int AA[2][3] = {{4, 5, 6}, {7, 8, 9}}; // arrayConstructor on several levels.

    a[2]++;                              // pointerAccess 

    int n = 10;
    int BB[n][2*n];                       // Variable-lengths-arrays are parsed, but with 'none' as dimColon.

/*=========== ENUMS =================*/
    enum color { RED, GREEN, BLUE};      // enumType
    enum grayscale {                     // splits the declaration of the enum and 
        BLACK, GRAY, WHITE               // the variable in 2 var declarations
    } shadow1 = GRAY;
    enum color border = GREEN;           // var declaration references the type-ident "enum color"
    color content = BLUE;                // var declaration references the type-ident "enum color"
    color corners;
    corners = color::RED;

    typedef enum color color_t;
    color_t x = GREEN; // OK

    enum Foo { A, B=10, C=A+B };         // 

    enum { TEN = 10 };
    struct S { int x : TEN; };

    enum direction { left = 'l', right = 'r' };
    direction dir1 = left;              // direct access
    char dir2 = direction::left;        // uses the full scopeAccess  main()::direction::left
    char dir3 = left;

    enum smallenum: short { aaa, bbb, ccc };
    smallenum small = aaa;
    short q = x;
    short k = small;

/*=========== UNIONS ================*/
    union Data {
       int i;
       float f;
       char str[20];
    };

    Data data;                      // calls the default constructor;

    union {                         // un-named union
        int Number;
        const char* String;
    };
    Number = 1;                     //
    String = "Jennifer";            // Un-named fieldAccess
    int h = Number;                 //

/*=========== STRUCTS ===============*/
    struct example {                // recordType
      int x;
      double d[];                   // Declartion of an array without size is allowed
    };                              // in this context. Size of array type is set to "unknown"

    struct example an_example;      // C-style, with "struct" keyword
    example another_example;        // C++-style, without "struct" keyword
    an_example.x = 33;              // fieldAccess

    struct { long k, l; } w;        // Object w of anonymous structure type. : Clang splits the recordType node
                                    //       and the varDeclaration node. It uses a temp type ident:
                                    //       "struct (anonymous struct at data_structures.cpp:76:11)"

    struct v {
       union {                      // anonymous union
          struct { int i, j; };     // anonymous structure declaration
          struct { long k, l; } w;  // declaration of an object w
       };                           
       int m;                       
    } v1;                           // Declaration of object v1
     
    v1.i = 2;                       // valid, because the structure is anonymous
    v1.w.k = 5;                     // valid

    struct s* p = nullptr;          // tag naming an unknown struct declares it
    struct s { int a; };            // definition for the struct pointed to by p
    while (false) {
        struct s;                   // forward declaration of a new, local struct s
                                    // this hides global struct s until the end of this block
                                    // This uses recordType, without any declarations.
                                    
        struct s *ptr;              // pointer to local struct s
                                    // without the forward declaration above,
                                    // this would point at the file-scope s
        struct s { char* p; };      // definitions of the local struct s
    }

    
    return 0;
}
