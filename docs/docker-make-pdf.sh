#!/usr/bin/env bash

if [ "$(uname)" = "Linux" ]
then
   docker run --rm -u $(stat -c "%u:%g" ./) -v ${PWD}/..:/docs -w /docs/docs registry.gitlab.inria.fr/tapenade/tapenade/doc make latex
else
   docker run --rm -v ${PWD}/..:/docs -w /docs/docs registry.gitlab.inria.fr/tapenade/tapenade/doc make latex
fi

