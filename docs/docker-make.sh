#!/usr/bin/env bash

if [ "$(uname)" = "Linux" ]
then
   docker run --rm -u $(stat -c "%u:%g" ./) -v ${PWD}/..:/docs -w /docs/docs registry.gitlab.inria.fr/tapenade/tapenade/doc make html
else
   docker run --rm -v ${PWD}/..:/docs -w /docs/docs registry.gitlab.inria.fr/tapenade/tapenade/doc make html
fi

