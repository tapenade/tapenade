How to build the developer documentation
========================================

- Dependencies

docker [https://www.docker.com/](https://www.docker.com/)

To get the last tapenade/doc docker image:
`docker pull registry.gitlab.inria.fr/tapenade/tapenade/doc`

pandoc [https://pandoc.org/](https://pandoc.org/)

- Generation from all README*.md and Javadoc comments

Run `./gradlew devdocker` to get the developer documentation in
[tapenade/build/docs/html/index.html](../index.html).

- Generation of tapenade/doc docker image after a modification 
 of docs/Dockerfile

`docker build -t registry.gitlab.inria.fr/tapenade/tapenade/doc:latest .`

`docker push registry.gitlab.inria.fr/tapenade/tapenade/doc`