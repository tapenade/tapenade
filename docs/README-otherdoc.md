Javadoc and other sources of information for developers
=======================================================

Developers can find information about the internals of Tapenade from
other sources:
    
-   Tapenade API documentation  
    `./gradlew devdocker` generates this developer documentation and 
    the [Tapenade API documentation](../build/packages) from the javadoc comments.
 
-   Tapenade complete Javadoc implementation documentation  
    `./gradlew alljavadoc` generates the documentation 
    from the Javadoc comments of all classes and members
    in [tapenade/build/docs/implementation/index.html](../../implementation/index.html).

-   Run Tapenade with command-line option `-dump <dump-file>`.  
    This dumps the Internal Representation of the source, the results of
    data-flow analysis, and the Internal Representation of the
    differentiated code. This dump can illustrate what we describe in
    [representation/README-basic.md](../src/main/java/fr/inria/tapenade/representation/README-basic.md),
    [representation/README-dataflow.md](../src/main/java/fr/inria/tapenade/representation/README-dataflow.md), 
    and [analysis/README.md](../src/main/java/fr/inria/tapenade/analysis/README.md). 
    Further options can be used to refine,
    focus, or augment this dump:
     ```
    -dumpunit <unit-names>
    -dumpCallGraph
    -dumpSymbolTables
    -dumpDiffSymbolTables
    -dumpFlowGraphs
    -dumpDiffFlowGraphs
    -dumpZones
    -dumpDataFlow
    -dumpInOut
    -dumpPointers
    -dumpDeps
    ```

-   Run Tapenade with command-line option:
    ```
    -traceParserC
    -traceInputIL
    -traceXXX <unit-names>, where -traceXXX is one of 
       -traceFlowGraphBuild
       -traceTypeCheck
       -tracePointer
       -traceInOut
       -traceDeps
       -traceADDeps
       -traceActivity`
       -traceReqExplicit
       -traceDiffLiveness
       -traceTBR
       -traceDifferentiation
       -traceTreeGen
    ```
     
    Tapenade will then write
    on standard output a detailled trace of the corresponding operation
    on the given *unit-names*.

