typedef int int32_T;
typedef double real_T;


extern real_T rtNaN;
extern real_T rt_powd_snf(real_T u0, real_T u1);

extern real_T norm(const real_T x[3]);

void EQM(const real_T y[7], real_T mdot, real_T T_mag, real_T S_beta, real_T 
        C_beta, real_T S_alpha, real_T C_alpha, real_T Con_fac, real_T dy[7]) 
{
    int32_T i;
    real_T r_mag;
    real_T h_vec[3];
    real_T c_B;
    real_T r_hat[3];
    real_T b_h_vec[3];
    r_mag = norm(*(real_T (*)[3])&y[0]);
    h_vec[0] = y[1]*y[5] - y[2]*y[4];
    c_B = norm(h_vec);
    r_mag = -1.0/rt_powd_snf(r_mag, 3.0);
    dy[6] = -mdot;
}
