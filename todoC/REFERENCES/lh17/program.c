/* Bug found by A.Sorrentino: one adjoint dead code too much. */
#include <math.h>

#include "ADConstantify.h"

double f(double x, double p1, double p2)
{
    return (x +p1) * x * cos(p2*x);
}

double interp(double x, double x1, double y1, double x2, double y2)
{
    return y1 + (x-x1) / (x2 - x1) * (y2-y1);
}

double GetValue(double x, double xm, double step, double cache[]) 
{
    double s_x;
    double x1, x2;
    double y1, y2;
    int i;
    
    i = (int)((x-xm)/step);
    x1 = xm + (double)i * step;
    x2 = x1 + step;

    y1 = cache[i];
    y2 = cache[i+1];

    return interp(x, x1, y1, x2, y2);
}

void CreateCache(double p1, double p2, double xmin, double step, int n, double o_cache[])
{
    int i;
    double x = xmin;

    for(i=0; i<n; i++) {
        o_cache[i] = f(x, p1, p2);
        x = x + step;
    }
}

double AdjointMe(double p1, double p2)
{
    double cache[1000];
    double xmin;
    double step;
    double acc;
    double x;

    int i;
    int n;

    xmin = 0.0;
    step = atan2(-1.0, 0.0)/1000.0;

    CreateCache(p1, p2, xmin, step, 1000, cache);

    n = 999;
    acc = 0.0;
    for(i=0; i<n; i++) {
        x = xmin + i * step;
        acc = acc + GetValue(x, xmin, step, cache);
    }
    return acc;
}
