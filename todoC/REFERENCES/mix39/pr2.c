#include <stdarg.h>
#include <stdio.h>

double add_em_up_ (double* a, double* b)
{
  double sum;
  sum = *a + *b;
  return sum;
}
/*
int main (void)
{
  printf ("%d\n", add_em_up_ (3, 5, 5, 6));
  printf ("%d\n", add_em_up_ (10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
  return 0;
}
*/
