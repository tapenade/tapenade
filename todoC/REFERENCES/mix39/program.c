#include <stdarg.h>
#include <stdio.h>

double add_em_up_variadic (int count,...)
{
  va_list ap;
  int i;
  double sum;

  va_start (ap, count);         /* Initialize the argument list. */

  sum = 0;
  for (i = 0; i < count; i++)
    sum += va_arg (ap, double);    /* Get the next argument value. */

  va_end (ap);                  /* Clean up. */
  return sum;
}

double add_em_up_ (double* a, double* b)
{
  double sum2, sum4, sum;
  sum2 = add_em_up_variadic(2, *a, *b);
  printf("2 %f\n", sum2);
  sum4 = add_em_up_variadic(4, *a, *b, *a, *b);
  printf("4 %f\n", sum4);
  sum = sum2 + sum4 + *a + *b;
  return sum;
}
