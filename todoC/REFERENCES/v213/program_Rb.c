/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.14 (r7122) - 26 Oct 2018 09:56
*/
typedef struct float4_Rb {
    float v;
    float d;
} float4_Rb;
#include <adBuffer.h>
#include <adContext.h>
#include <stdio.h>

/*
  Differentiation of top in reverse (adjoint) mode (with options associationByAddress context):
   gradient     of useful results: x[0:3-1] top
   with respect to varying inputs: x[0:3-1]
   RW status of diff variables: x[0:3-1]:incr top:in-killed
   Plus diff mem management of: x:in
*/
void top_Rb(float4_Rb x[3]) {
    float4_Rb r;
    float4_Rb x0[3];
    r = top;
    x0[0].d = x0[0].d + (x[2]*x[1]*r).d;
    x0[1].d = x0[1].d + (x[2]*x[0]*r).d;
    x0[2].d = x0[2].d + (x[0]*x[1]*r).d;
}

/*
  Differentiation of main as a context to call adjoint code (with options associationByAddress context):
*/
void main() {
    float4_Rb a[3];
    float4_Rb b;
    float4_Rb a0[3];
    float4_Rb b0;
    a[0] = 2;
    a[1] = 3;
    a[2] = 5;
    adContextAdj_init(0.87);
    adContextAdj_initReal4Array("a", a[].d, a0[].d, 3);
    adContextAdj_initReal4("b", &(b.d), &(b0.d));
    top_Rb(a);
    adContextAdj_startConclude();
    adContextAdj_concludeReal4Array("a", a[].d, a0[].d, 3);
    adContextAdj_conclude();
    printf("--> %f \n", b);
}
