typedef struct real4_Rd {
    float v;
    float d;
} real4_Rd;

real4_Rd top_Rd(real4_Rd xRd[3]);

main() {
  real4_Rd ad[3];
  real4_Rd bd;
  ad[0].v = 2;
  ad[1].v = 3;
  ad[2].v = 5;
  ad[0].d = 20;
  ad[1].d = 30;
  ad[2].d = 50;
  bd = top_Rd(ad);
  printf("--> %f \n" , bd.v);
  printf("--> %f \n" , bd.d);
}
