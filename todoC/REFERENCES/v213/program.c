#include <stdio.h>

float top(float x[3]) {
  float r;
  r =  x[0] * x[1] * x[2];
  return r;
}

void main() {
  float a[3];
  float b;
  a[0] = 2;
  a[1] = 3;
  a[2] = 5;
  b = top(a);
  printf("--> %f \n" , b);
}
