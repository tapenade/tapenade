struct cpx {float r, i;};

struct dpx {double r,i;};

void ccmplx_(struct cpx *w, struct dpx *z) {
     w -> r = 32.;
     w -> i = .007;
     z -> r = 66.67;
     z -> i = 94.1;
} 

void mul2cmplx_(struct cpx *w, struct dpx *z) {
     z -> r = 2 * w->r;
     z -> i = 2 * w->i;
} 
