      subroutine init(w, z)
      complex w
      double complex z
      external CCmplx
      call CCmplx(w,z) 
      end

      subroutine mul2(w, z)
      complex w
      double complex z
      external mul2Cmplx
      call mul2cmplx(w,z) 
      end


      program top
      complex w
      double complex z
      call init(w, z)
      print*, 'w ', w, ' z ', z
      call mul2(w, z)
      print*, 'w ', w, ' z ', z
      end program
