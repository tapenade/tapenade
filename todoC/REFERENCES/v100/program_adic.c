/************************** DISCLAIMER ********************************/
/*                                                                    */
/*   This file was generated on 12/09/10 14:58:47 by                  */
/*   ADIC version 2.1.0 compiled on Thu Dec  9 14:58:21 CST 2010      */
/*                                                                    */
/*   ADIC was prepared as an account of work sponsored by an          */
/*   agency of the United States Government and the University of     */
/*   Chicago.  NEITHER THE AUTHOR(S), THE UNITED STATES GOVERNMENT    */
/*   NOR ANY AGENCY THEREOF, NOR THE UNIVERSITY OF CHICAGO, INCLUDING */
/*   ANY OF THEIR EMPLOYEES OR OFFICERS, MAKES ANY WARRANTY, EXPRESS  */
/*   OR IMPLIED, OR ASSUMES ANY LEGAL LIABILITY OR RESPONSIBILITY FOR */
/*   THE ACCURACY, COMPLETENESS, OR USEFULNESS OF ANY INFORMATION OR  */
/*   PROCESS DISCLOSED, OR REPRESENTS THAT ITS USE WOULD NOT INFRINGE */
/*   PRIVATELY OWNED RIGHTS.                                          */
/*                                                                    */
/**********************************************************************/
#include "/disks/large/home/snarayan/soft/adic/include/ad_types.h"
typedef struct _iobuf FILE;
extern int ad_getc(FILE *);
extern int ad_putc(char ,FILE *);
int ad_ad_GRAD_MAX;
#include <math.h>

void ad_mini1(DERIV_TYPE *y,DERIV_TYPE *x)
{
  int ad_i;
  for (ad_i = 0; ad_i < 2; ad_i = (ad_i + 1)) {
    DERIV_TYPE ad_TempVarprp_2;
    DERIV_TYPE ad_TempVarprp_1;
    DERIV_TYPE ad_TempVarprp_0;
    double ad_TempVaracc_1;
    double ad_TempVaracc_0;
    double ad_TempVarlin_3;
    double ad_TempVarlin_1;
    double ad_TempVarlin_0;
    double ad_TempVardly_0;
    double ad_TempVarlin_2;
    ad_TempVarlin_2 = DERIV_val(x[ad_i]) * DERIV_val(x[ad_i]);
    ad_TempVardly_0 = DERIV_val(x[ad_i]) + sin(ad_TempVarlin_2);
    ad_TempVarlin_0 = DERIV_val(x[ad_i]);
    ad_TempVarlin_1 = DERIV_val(x[ad_i]);
    ad_TempVarlin_3 = cos(ad_TempVarlin_2);
    DERIV_val(y[ad_i]) = ad_TempVardly_0;
    ad_TempVaracc_0 = ad_TempVarlin_0 * ad_TempVarlin_3;
    ad_TempVaracc_1 = ad_TempVarlin_1 * ad_TempVarlin_3;
    ADIC_SetDeriv(DERIV_TYPE_ref(x[ad_i]),DERIV_TYPE_ref(ad_TempVarprp_0));
    ADIC_SetDeriv(DERIV_TYPE_ref(x[ad_i]),DERIV_TYPE_ref(ad_TempVarprp_1));
    ADIC_SetDeriv(DERIV_TYPE_ref(x[ad_i]),DERIV_TYPE_ref(ad_TempVarprp_2));
    ADIC_SetDeriv(DERIV_TYPE_ref(ad_TempVarprp_0),DERIV_TYPE_ref(y[ad_i]));
    ADIC_Saxpy(ad_TempVaracc_0,DERIV_TYPE_ref(ad_TempVarprp_1),DERIV_TYPE_ref(y[ad_i]));
    ADIC_Saxpy(ad_TempVaracc_1,DERIV_TYPE_ref(ad_TempVarprp_2),DERIV_TYPE_ref(y[ad_i]));
  }
}
