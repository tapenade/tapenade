/*        Generated by TAPENADE     (INRIA, Tropics team)
    Tapenade 3.9 (r5163) - 14 Apr 2014 16:05
*/
#include <math.h>
typedef struct float8_Rb {
    double v;
    double d;
} float8_Rb;
typedef struct float4_Rb {
    float v;
    float d;
} float4_Rb;

/*
  Differentiation of mini1 in reverse (adjoint) mode (with options associationByAddress):
   gradient     of useful results: *y
   with respect to varying inputs: *x *y
   RW status of diff variables: *x:out *y:in-out
   Plus diff mem management of: x:in y:in
*/
void mini1_Rb(float8_Rb *yRb, float8_Rb *xRb) {
    int i;
    float8_Rb arg1Rb;
    xRb->d = 0.0;
    for (i = 1; i > -1; --i) {
        arg1Rb.v = xRb[i].v*xRb[i].v;
        xRb[i].d = xRb[i].d + yRb[i].d;
        arg1Rb = cos(arg1)*yRb[i].d;
        yRb[i].d = 0.0;
        xRb[i].d = xRb[i].d + (2*xRb[i].v*arg1Rb).d;
    }
}
