#include <math.h>

static double BassiRebay( double* ui1one,double* ui2one,double* dui1one, double* dui2one,
    double* ui1two,double* ui2two, double* dui1two,double* dui2two,
    double* ui1three, double* ui2three, double* dui1three, double* dui2three, double vol){

  double eps_viscous = 0.0;
  int nip = 10;

  for (int j = 0; j < nip; j++){

    double u1[4],  u2[4];
    double du1[8], du2[8];
    for (int kk = 0; kk < 4; kk++) {
      u1[kk]      = ui1one[j*4+kk];
      u2[kk]      = ui2one[j*4+kk];
      du1[2*kk]   = dui1one[j*8+2*kk];
      du1[2*kk+1] = dui1one[j*8+2*kk+1];
      du2[2*kk]   = dui2one[j*8+2*kk];
      du2[2*kk+1] = dui2one[j*8+2*kk+1];
    }
    double n1[2];
    double fn1[4];

    double ixi[10];
    for (int l = 0; l < 4; l++) {
      ixi[l]  = u1[l];
      ixi[l+4]= u2[l];
    }
    ixi[2*4]   = n1[0]; 
    ixi[2*4+1] = n1[1];
    double numerical_flux[4];
    for (int ll = 0; ll < 4; ll++) {
      eps_viscous += pow((numerical_flux[ll] - fn1[ll]),2) * 3; 
    }
  }

    double eps_viscous_0;

eps_viscous += eps_viscous_0 * pow(eps_viscous,.5) / vol;
return eps_viscous;
}
