Tapenade Developer Documentation
================================

.. toctree::
   :maxdepth: 2

   README
   contrib
   indexDesign
   indexTestDebug


Indices and tables
==================
   
* :ref:`genindex`
* :ref:`search`

.. raw:: html
    :file: build/resources/main/gittagdoc.html
